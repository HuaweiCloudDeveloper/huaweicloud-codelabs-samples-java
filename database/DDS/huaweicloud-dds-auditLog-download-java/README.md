## 1. 介绍

文档数据库服务（document database
service，简称dds）完全兼容mongodb协议，提供安全、高可用、高可靠、弹性伸缩和易用的数据库服务，同时提供一键部署、弹性扩容、容灾、备份、恢复、监控和告警等功能。此示例展示如何获取审计日志下载链接，获取审计日志链接后可直接下载审计日志。

## 2. 流程图

![获取审计日志下载链接流程图](./assets/auditLog.png)

## 3. 前置条件

1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

## 4. SDK获取和安装

您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-dds</artifactId>
    <version>3.1.1</version>
</dependency>
```

## 5. 关键代码片段

以下代码展示如何使用SDK获取审计日志下载链接：

```java

public class AuditLogDownloadDemo {
    private static final Logger logger = LoggerFactory.getLogger(AuditLogDownloadDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     * args[4] = "<YOUR START_TIME>"
     * args[5] = "<YOUR END_TIME>"
     *
     * 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
     * 本示例以ak和sk保存在环境变量中为例，运行本示例前请先在本地环境变量中设置环境变量 HUAWEICLOUD_SDK_AK 和 HUAWEICLOUD_SDK_SK
     *
     * @param args
     */

    public static void main(String[] args) {
        if (args.length != 6) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];

        String startTime = args[4];
        String endTime = args[5];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        // 根据需要配置是否跳过SSL证书验证
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);
        DdsClient client = DdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .withHttpConfig(httpConfig)
                .build();
        //获取审计日志id列表，根据审计日志列表，查询审计日志下载链接
        List<String> ids = queryAuditLogIds(client, instanceId, startTime, endTime);
        getAuditLogLinks(client, ids, instanceId);
    }

    private static List<String> queryAuditLogIds(DdsClient client, String instanceId, String startTime, String endTime) {
        ListAuditlogsRequest request = new ListAuditlogsRequest();
        //设置实例id，起始时间，结束时间
        request.setInstanceId(instanceId);
        request.setStartTime(startTime);
        request.setEndTime(endTime);
        ListAuditlogsResponse response;
        List<String> ids = new ArrayList<>();
        //获取审计日志id列表
        try {
            response = client.listAuditlogs(request);
            logger.info(response.toString());
            for (int i = 0; i < response.getAuditLogs().size(); i++) {
                ids.add(response.getAuditLogs().get(i).getId());
            }
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return ids;
    }

    private static void getAuditLogLinks(DdsClient client, List<String> ids, String instanceId) {
        ListAuditlogLinksRequest request = new ListAuditlogLinksRequest();
        ProduceAuditlogLinksRequestBody body = new ProduceAuditlogLinksRequestBody();
        body.setIds(ids);

        request.setInstanceId(instanceId);
        request.withBody(body);
        //获取审计日志下载链接
        try {
            ListAuditlogLinksResponse response = client.listAuditlogLinks(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}


```

## 6.返回结果示例

- 获取审计日志列表（ListAuditlogs）接口的返回值：

```
{
  "total_record": 1,
  "audit_logs": [
    {
      "id": "10190012aae94b38a10269b8ad025fc1no02_1607681849871",
      "name": "0a84b6e97780d3271fd0c00f2db42932_audit_log_65d3fe0c50984b35bc1a36e9b7c7de98in02_10190012aae94b38a10269b8ad025fc1no02_1607681849871",
      "size": "2473",
      "node_id": "10190012aae94b38a10269b8ad025fc1no02",
      "start_time": "2020-12-11T18:14:49+0800",
      "end_time": "2020-12-11T18:17:25+0800"
    }
  ]
}
```

- 获取审计日志下载链接（ListAuditlogLinks）接口的返回值：

```
{
  "links": [
    "https://obs.domainname.com/ddsbucket.username.1/xxxxxx",
    "https://obs.domainname.com/ddsbucket.username.2/xxxxxx"
  ]
}
```

## 7. 参考链接

请见 [获取审计日志列表API](https://support.huaweicloud.com/api-dds/dds_api_0099.html)
您可以在 [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DDS/doc?api=ListAuditlogs) 中直接运行调试该接口。

请见 [获取审计日志下载链接API](https://support.huaweicloud.com/api-dds/dds_api_0100.html)
您可以在 [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DDS/doc?api=ListAuditlogLinks)
中直接运行调试该接口。

## 修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2023-07-17 |   1.0    | 文档首次发布 |

