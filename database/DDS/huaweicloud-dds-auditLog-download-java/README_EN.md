## 1. Introduction

Document Database Service (DDS) is a MongoDB-compatible database service that is secure, highly available, reliable,
scalable, and easy to use. It provides easy DB instance creation, scaling, disaster recovery, backup, restoration,
monitoring, and alarm reporting functions on the DDS console. This example shows how to obtain the links for downloading
audit logs.

## 2. Flowchart

![Flowchart for obtaining the links for downloading audit logs](./assets/auditLog-en.png)

## 3. Prerequisites

1. You have created [a HUAWEI ID](https://id5.cloud.huawei.com/CAS/portal/userRegister/regbyphone.html?&lang=en-us) and
   completed [real-name authentication](https://auth.huaweicloud.com/authui/login.html?service=https%3A%2F%2Faccount.huaweicloud.com%2Fusercenter%2F%3Fregion%3Dcn-north-4%26cloud_route_state%3D%2Faccountindex%2Frealnameauth&locale=en-us#/login)
   .

2. You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3. You have obtained the access key ID (AK) and secret access key (SK) of the HUAWEI ID. You can create and view your
   AK/SK on the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details,
   see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html).

4. You have set up the development environment with Java JDK 1.8 or later.

## 4. Obtaining and Installing an SDK

You can obtain and install an SDK in Maven mode. Download and install Maven in your operating system (OS). After the
installation is complete, add the required dependencies to the **pom.xml** file of the Java project.

For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-dds</artifactId>
    <version>3.1.1</version>
</dependency>
```

## 5. Key Code Snippets

The following code shows how to use the SDK to obtain the links for downloading audit logs:

```java

public class AuditLogDownloadDemo {
    private static final Logger logger = LoggerFactory.getLogger(AuditLogDownloadDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     * args[4] = "<YOUR START_TIME>"
     * args[5] = "<YOUR END_TIME>"
     *
     * Hard-coded or plaintext AK and SK are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     *
     * @param args
     */

    public static void main(String[] args) {
        if (args.length != 6) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];

        String startTime = args[4];
        String endTime = args[5];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        // Configures whether to skip SSL certificate verification as required.
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);
        DdsClient client = DdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .withHttpConfig(httpConfig)
                .build();
        // Obtains the audit log ID list, which is used for querying the audit log download links.
        List<String> ids = queryAuditLogIds(client, instanceId, startTime, endTime);
        getAuditLogLinks(client, ids, instanceId);
    }

    private static List<String> queryAuditLogIds(DdsClient client, String instanceId, String startTime, String endTime) {
        ListAuditlogsRequest request = new ListAuditlogsRequest();
        // Sets the instance ID, start time, and end time.
        request.setInstanceId(instanceId);
        request.setStartTime(startTime);
        request.setEndTime(endTime);
        ListAuditlogsResponse response;
        List<String> ids = new ArrayList<>();
        // Obtains the audit log ID list.
        try {
            response = client.listAuditlogs(request);
            logger.info(response.toString());
            for (int i = 0; i < response.getAuditLogs().size(); i++) {
                ids.add(response.getAuditLogs().get(i).getId());
            }
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return ids;
    }

    private static void getAuditLogLinks(DdsClient client, List<String> ids, String instanceId) {
        ListAuditlogLinksRequest request = new ListAuditlogLinksRequest();
        ProduceAuditlogLinksRequestBody body = new ProduceAuditlogLinksRequestBody();
        body.setIds(ids);

        request.setInstanceId(instanceId);
        request.withBody(body);
        // Obtains the links for downloading audit logs.
        try {
            ListAuditlogLinksResponse response = client.listAuditlogLinks(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}


```

## 6. Sample Return Values

- Return value from the API (ListAuditlogs) for obtaining the audit log list:

```
{
  "total_record": 1,
  "audit_logs": [
    {
      "id": "10190012aae94b38a10269b8ad025fc1no02_1607681849871",
      "name": "0a84b6e97780d3271fd0c00f2db42932_audit_log_65d3fe0c50984b35bc1a36e9b7c7de98in02_10190012aae94b38a10269b8ad025fc1no02_1607681849871",
      "size": "2473",
      "node_id": "10190012aae94b38a10269b8ad025fc1no02",
      "start_time": "2020-12-11T18:14:49+0800",
      "end_time": "2020-12-11T18:17:25+0800"
    }
  ]
}
```

- Return value from the API (ListAuditlogLinks) for obtaining the audit log download links:

```
{
  "links": [
    "https://obs.domainname.com/ddsbucket.username.1/xxxxxx",
    "https://obs.domainname.com/ddsbucket.username.2/xxxxxx"
  ]
}
```

## 7. References

For details, see [Obtaining the Audit Log List](https://support.huaweicloud.com/intl/en-us/api-dds/dds_api_0099.html).
You can directly run and debug the API
in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DDS/doc?api=ListAuditlogs).

For details,
see [Obtaining Links for Downloading Audit Logs](https://support.huaweicloud.com/intl/en-us/api-dds/dds_api_0100.html).
You can directly run and debug the API
in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DDS/doc?api=ListAuditlogLinks).

## Change History

|    Date   | Issue|   Description  |
|:----------:| :------: | :----------: |
| 2023-07-17 |   1.0    | This issue is the first official release.|
