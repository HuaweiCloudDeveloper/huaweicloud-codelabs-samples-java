package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.dds.v3.DdsClient;
import com.huaweicloud.sdk.dds.v3.model.BalancerActiveWindow;
import com.huaweicloud.sdk.dds.v3.model.SetBalancerSwitchRequest;
import com.huaweicloud.sdk.dds.v3.model.SetBalancerSwitchResponse;
import com.huaweicloud.sdk.dds.v3.model.SetBalancerWindowRequest;
import com.huaweicloud.sdk.dds.v3.model.SetBalancerWindowResponse;
import com.huaweicloud.sdk.dds.v3.model.ShowShardingBalancerRequest;
import com.huaweicloud.sdk.dds.v3.model.ShowShardingBalancerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClusterConfigDemo {
    private static final Logger logger = LoggerFactory.getLogger(ClusterConfigDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     * <p>
     * Hard-coded or plaintext AK and SK are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 4) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        DdsClient client = DdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .build();

        // Enables or disables cluster balancing.
        setClusterBalancerSwitch(client, instanceId);

        // Sets the activity time window for cluster balancing.
        setClusterBalancerTimeWindow(client, instanceId);

        // Queries cluster balancing settings.
        queryClusterBalancerConfig(client, instanceId);
    }

    private static void queryClusterBalancerConfig(DdsClient client, String instanceId) {
        ShowShardingBalancerRequest request = new ShowShardingBalancerRequest();
        request.setInstanceId(instanceId);

        try {
            ShowShardingBalancerResponse response = client.showShardingBalancer(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void setClusterBalancerTimeWindow(DdsClient client, String instanceId) {
        SetBalancerWindowRequest request = new SetBalancerWindowRequest();

        BalancerActiveWindow balancerActiveWindow = new BalancerActiveWindow();
        balancerActiveWindow.setStartTime("20:00");
        balancerActiveWindow.setStopTime("23:00");

        request.setInstanceId(instanceId);
        request.setBody(balancerActiveWindow);

        try {
            SetBalancerWindowResponse response = client.setBalancerWindow(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void setClusterBalancerSwitch(DdsClient client, String instanceId) {
        SetBalancerSwitchRequest request = new SetBalancerSwitchRequest();
        request.setInstanceId(instanceId);
        request.setAction(SetBalancerSwitchRequest.ActionEnum.START);

        try {
            SetBalancerSwitchResponse response = client.setBalancerSwitch(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}
