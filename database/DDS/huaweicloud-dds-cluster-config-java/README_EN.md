## 1. Introduction

Document Database Service (DDS) is a MongoDB-compatible database service that is secure, highly available, reliable,
scalable, and easy to use. It provides easy DB instance creation, scaling, disaster recovery, backup, restoration,
monitoring, and alarm reporting functions on the DDS console.

## 2. Flowchart

![Flowchart for enabling or disabling cluster balancing](assets/cluster-en.png)

## 3. Prerequisites

1. You have created [a HUAWEI ID](https://id5.cloud.huawei.com/CAS/portal/userRegister/regbyphone.html?&lang=en-us) and
   completed [real-name authentication](https://auth.huaweicloud.com/authui/login.html?service=https%3A%2F%2Faccount.huaweicloud.com%2Fusercenter%2F%3Fregion%3Dcn-north-4%26cloud_route_state%3D%2Faccountindex%2Frealnameauth&locale=en-us#/login)
   .

2. You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3. You have obtained the access key ID (AK) and secret access key (SK) of the HUAWEI ID. You can create and view your
   AK/SK on the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details,
   see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html).

4. You have set up the development environment with Java JDK 1.8 or later.

## 4. Obtaining and Installing an SDK

You can obtain and install an SDK in Maven mode. Download and install Maven in your operating system (OS). After the
installation is complete, add the required dependencies to the **pom.xml** file of the Java project.

For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-dds</artifactId>
    <version>3.1.1</version>
</dependency>
```

## 5. Key Code Snippets

The following code shows how to enable or disable cluster balancing using the SDK.

```java

public class ClusterConfigDemo {
    private static final Logger logger = LoggerFactory.getLogger(ClusterConfigDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     *
     * Hard-coded or plaintext AK and SK are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 4) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        DdsClient client = DdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .build();

        // Enables or disables cluster balancing.
        setClusterBalancerSwitch(client, instanceId);

        // Sets the activity time window for cluster balancing.
        setClusterBalancerTimeWindow(client, instanceId);

        // Queries cluster balancing settings.
        queryClusterBalancerConfig(client, instanceId);
    }

    private static void queryClusterBalancerConfig(DdsClient client, String instanceId) {
        ShowShardingBalancerRequest request = new ShowShardingBalancerRequest();
        request.setInstanceId(instanceId);

        try {
            ShowShardingBalancerResponse response = client.showShardingBalancer(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void setClusterBalancerTimeWindow(DdsClient client, String instanceId) {
        SetBalancerWindowRequest request = new SetBalancerWindowRequest();

        BalancerActiveWindow balancerActiveWindow = new BalancerActiveWindow();
        balancerActiveWindow.setStartTime("20:00");
        balancerActiveWindow.setStopTime("23:00");

        request.setInstanceId(instanceId);
        request.setBody(balancerActiveWindow);

        try {
            SetBalancerWindowResponse response = client.setBalancerWindow(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void setClusterBalancerSwitch(DdsClient client, String instanceId) {
        SetBalancerSwitchRequest request = new SetBalancerSwitchRequest();
        request.setInstanceId(instanceId);
        request.setAction(SetBalancerSwitchRequest.ActionEnum.START);

        try {
            SetBalancerSwitchResponse response = client.setBalancerSwitch(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}


```

## 6. Sample Return Values

- Return value from the API (SetBalancerSwitch) for enabling or disabling cluster balancing:

```
class SetBalancerSwitchResponse {
    jobId: d890eb80-d16c-438f-a623-376f02f0db3d
}
```

- Return value from the API (SetBalancerWindow) for setting the activity time window for cluster balancing:

```
class SetBalancerWindowResponse {
    jobId: 9e1ae2e6-bffd-4c95-8dc0-46b73432cf95
}
```

- Return value from the API (ShowShardingBalancer) for querying the cluster balancing settings:

```
class ShowShardingBalancerResponse {
    isOpen: true
    activeWindow: class BalancerActiveWindow {
        startTime: 20:00
        stopTime: 23:00
    }
}
```

## 7. References

For details,
see [Enabling or Disabling Cluster Balancing](https://support.huaweicloud.com/intl/en-us/api-dds/dds_api_0111.html). You
can directly run and debug the API
in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DDS/doc?api=SetBalancerSwitch).

For details,
see [Setting the Activity Time Window for Cluster Balancing](https://support.huaweicloud.com/intl/en-us/api-dds/dds_api_0112.html)
. You can directly run and debug the API
in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DDS/doc?api=SetBalancerWindow).

For details,
see [Querying Cluster Balancing Settings](https://support.huaweicloud.com/intl/en-us/api-dds/dds_api_0110.html). You can
directly run and debug the API
in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DDS/doc?api=ShowShardingBalancer).

## Change History

|    Date   | Issue|   Description  |
|:----------:| :------: | :----------: |
| 2022-10-20 |   1.0    | This issue is the first official release.|
