## 1. 介绍

文档数据库服务（document database service，简称dds）完全兼容mongodb协议，提供安全、高可用、高可靠、弹性伸缩和易用的数据库服务，同时提供一键部署、弹性扩容、容灾、备份、恢复、监控和告警等功能。

## 2. 流程图

![设置集群均衡开关的流程图](assets/cluster.png)

## 3. 前置条件

1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

## 4. SDK获取和安装

您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-dds</artifactId>
    <version>3.1.1</version>
</dependency>
```

## 5. 关键代码片段

以下代码展示如何使用SDK设置集群均衡开关：

```java

public class ClusterConfigDemo {
    private static final Logger logger = LoggerFactory.getLogger(ClusterConfigDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     *
     * 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
     * 本示例以ak和sk保存在环境变量中为例，运行本示例前请先在本地环境变量中设置环境变量 HUAWEICLOUD_SDK_AK 和 HUAWEICLOUD_SDK_SK
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 4) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        DdsClient client = DdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .build();

        // 设置集群均衡开关
        setClusterBalancerSwitch(client, instanceId);

        // 设置集群均衡活动时间窗
        setClusterBalancerTimeWindow(client, instanceId);

        // 查询集群均衡设置
        queryClusterBalancerConfig(client, instanceId);
    }

    private static void queryClusterBalancerConfig(DdsClient client, String instanceId) {
        ShowShardingBalancerRequest request = new ShowShardingBalancerRequest();
        request.setInstanceId(instanceId);

        try {
            ShowShardingBalancerResponse response = client.showShardingBalancer(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void setClusterBalancerTimeWindow(DdsClient client, String instanceId) {
        SetBalancerWindowRequest request = new SetBalancerWindowRequest();

        BalancerActiveWindow balancerActiveWindow = new BalancerActiveWindow();
        balancerActiveWindow.setStartTime("20:00");
        balancerActiveWindow.setStopTime("23:00");

        request.setInstanceId(instanceId);
        request.setBody(balancerActiveWindow);

        try {
            SetBalancerWindowResponse response = client.setBalancerWindow(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void setClusterBalancerSwitch(DdsClient client, String instanceId) {
        SetBalancerSwitchRequest request = new SetBalancerSwitchRequest();
        request.setInstanceId(instanceId);
        request.setAction(SetBalancerSwitchRequest.ActionEnum.START);

        try {
            SetBalancerSwitchResponse response = client.setBalancerSwitch(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}


```

## 6. 返回结果示例

- 设置集群均衡开关（SetBalancerSwitch）接口的返回值：

```
class SetBalancerSwitchResponse {
    jobId: d890eb80-d16c-438f-a623-376f02f0db3d
}
```

- 设置集群均衡活动时间窗（SetBalancerWindow）接口的返回值：

```
class SetBalancerWindowResponse {
    jobId: 9e1ae2e6-bffd-4c95-8dc0-46b73432cf95
}
```

- 查询集群均衡设置（ShowShardingBalancer）接口的返回值：

```
class ShowShardingBalancerResponse {
    isOpen: true
    activeWindow: class BalancerActiveWindow {
        startTime: 20:00
        stopTime: 23:00
    }
}
```

## 7.参考链接

请见 [设置集群均衡开关API](https://support.huaweicloud.com/api-dds/dds_api_0111.html)
您可以在 [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=DDS&api=SetBalancerSwitch)
中直接运行调试该接口。

请见 [设置集群均衡活动时间窗API](https://support.huaweicloud.com/api-dds/dds_api_0112.html)
您可以在 [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=DDS&api=SetBalancerWindow)
中直接运行调试该接口。

请见 [查询集群均衡设置API](https://support.huaweicloud.com/api-dds/dds_api_0110.html)
您可以在 [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=DDS&api=ShowShardingBalancer)
中直接运行调试该接口。

## 修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2022-10-20 |   1.0    | 文档首次发布 |
