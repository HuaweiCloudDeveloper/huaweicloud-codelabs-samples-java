## 1. Introduction

Document Database Service (DDS) is a MongoDB-compatible database service that is secure, highly available, reliable,
scalable, and easy to use. It provides easy DB instance creation, scaling, disaster recovery, backup, restoration,
monitoring, and alarm reporting functions on the DDS console.

## 2. Flowchart

![Flowchart for modifying instance parameters](assets/param-en.png)

## 3. Prerequisites

1. You have created [a HUAWEI ID](https://id5.cloud.huawei.com/CAS/portal/userRegister/regbyphone.html?&lang=en-us) and
   completed [real-name authentication](https://auth.huaweicloud.com/authui/login.html?service=https%3A%2F%2Faccount.huaweicloud.com%2Fusercenter%2F%3Fregion%3Dcn-north-4%26cloud_route_state%3D%2Faccountindex%2Frealnameauth&locale=en-us#/login)
   .

2. You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3. You have obtained the access key ID (AK) and secret access key (SK) of the HUAWEI ID. You can create and view your
   AK/SK on the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details,
   see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html).

4. You have set up the development environment with Java JDK 1.8 or later.

## 4. Obtaining and Installing an SDK

You can obtain and install an SDK in Maven mode. Download and install Maven in your operating system (OS). After the
installation is complete, add the required dependencies to the **pom.xml** file of the Java project.

For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-dds</artifactId>
    <version>3.1.1</version>
</dependency>
```

## 5. Key Code Snippets

The following code shows how to modify parameters of a DDS instance using the SDK.

```java

public class ParamsConfigDemo {
    private static final Logger logger = LoggerFactory.getLogger(ParamsConfigDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     * args[4] = "<YOUR ENTITY_ID>"
     *
     * Hard-coded or plaintext AK and SK are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 5) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];
        String entityId = args[4];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        DdsClient client = DdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .build();

        // Queries parameters of a specified instance.
        queryInstanceParamGroup(client, instanceId, entityId);

        // Modifies parameters that require a restart to apply changes.
        updateInstanceParamGroup(client, instanceId, entityId);

        // Restarts the instance if needed.
        restartInstance(client, instanceId);

        // Queries parameters of the specified instance again.
        queryInstanceParamGroup(client, instanceId, entityId);
    }

    private static void queryInstanceDetail(DdsClient client, String instanceId) {
        ListInstancesRequest request = new ListInstancesRequest();
        request.setId(instanceId);

        try {
            ListInstancesResponse response = client.listInstances(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void restartInstance(DdsClient client, String instanceId) {

        RestartInstanceRequest request = new RestartInstanceRequest();
        RestartInstanceRequestBody body = new RestartInstanceRequestBody();
        body.setTargetId(instanceId);

        request.setInstanceId(instanceId);
        request.withBody(body);

        try {
            RestartInstanceResponse response = client.restartInstance(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void updateInstanceParamGroup(DdsClient client, String instanceId, String entityId) {
        UpdateEntityConfigurationRequest request = new UpdateEntityConfigurationRequest();
        UpdateConfigurationParameterResult body = new UpdateConfigurationParameterResult();

        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("net.maxIncomingConnections", "400");
        paramMap.put("connPoolMaxConnsPerHost", "600");
        body.setParameterValues(paramMap);
        body.setEntityId(entityId);

        request.setInstanceId(instanceId);
        request.setBody(body);

        try {
            UpdateEntityConfigurationResponse response = client.updateEntityConfiguration(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void queryInstanceParamGroup(DdsClient client, String instanceId, String entityId) {
        ShowEntityConfigurationRequest request = new ShowEntityConfigurationRequest();
        request.setInstanceId(instanceId);
        request.setEntityId(entityId);

        try {
            ShowEntityConfigurationResponse response = client.showEntityConfiguration(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}


```

## 6. Sample Return Values

- Return value from the API (ShowEntityConfiguration) for querying parameters of a specified instance:

```
class ShowEntityConfigurationResponse {
    datastoreVersion: 3.4
    datastoreName: mongodb
    created: 2022-10-11T08:22:50+0000
    updated: 2022-10-20T02:15:49+0000
    parameters: [class EntityConfigurationParametersResult {
        name: connPoolMaxConnsPerHost
        value: 500
        valueRange: 200-2000
        restartRequired: true
        readonly: false
        type: integer
        description: Maximum size of the connection pools for connections to other mongod instances.
    }, class EntityConfigurationParametersResult {
        name: cursorTimeoutMillis
        value: 600000
        valueRange: 600000-1000000
        restartRequired: false
        readonly: false
        type: integer
        description: Expiration threshold for idle cursors before DDS removes them.
    }, class EntityConfigurationParametersResult {
        name: disableJavaScriptJIT
        value: true
        valueRange: true|false
        restartRequired: false
        readonly: false
        type: boolean
        description: Enable or disable JavaScriptJIT.
    }, class EntityConfigurationParametersResult {
        name: failIndexKeyTooLong
        value: true
        valueRange: true|false
        restartRequired: false
        readonly: true
        type: boolean
        description: If the length of an indexed field value is longer than Index Key Length Limit, an error will be reported.
    }, class EntityConfigurationParametersResult {
        name: net.maxIncomingConnections
        value: 300
        valueRange: 200-500
        restartRequired: false
        readonly: false
        type: integer
        description: Maximum number of simultaneous connections that mongos or mongod will accept. The default value depends on system architecture.
    }, class EntityConfigurationParametersResult {
        name: operationProfiling.mode
        value: slowOp
        valueRange: off|slowOp|all
        restartRequired: true
        readonly: false
        type: string
        description: Level of database profiling.
    }, class EntityConfigurationParametersResult {
        name: operationProfiling.slowOpThresholdMs
        value: 500
        valueRange: 10-10000
        restartRequired: false
        readonly: false
        type: integer
        description: Slow request threshold. If there is no special requirement, it is recommended to use the default 500ms. Requests that exceed this value will be recorded in the system.profile collection of the corresponding db.
    }, class EntityConfigurationParametersResult {
        name: replication.enableMajorityReadConcern
        value: false
        valueRange: true|false
        restartRequired: true
        readonly: true
        type: boolean
        description: Whether to enable enableMajorityReadConcern.
    }, class EntityConfigurationParametersResult {
        name: security.authorization
        value: enabled
        valueRange: disabled|enabled
        restartRequired: true
        readonly: true
        type: string
        description: Enable or disable Access Control.
    }, class EntityConfigurationParametersResult {
        name: storage.directoryPerDB
        value: true
        valueRange: true|false
        restartRequired: true
        readonly: true
        type: boolean
        description: Whether DDS uses a separate directory to store data for each database.
    }, class EntityConfigurationParametersResult {
        name: storage.engine
        value: wiredTiger
        valueRange: mmapv1|wiredTiger|inmem
        restartRequired: true
        readonly: true
        type: string
        description: Storage engine of mongod.
    }, class EntityConfigurationParametersResult {
        name: storage.indexBuildRetry
        value: true
        valueRange: true|false
        restartRequired: true
        readonly: false
        type: boolean
        description: Whether mongod rebuilds indexes on the next start up after an index building failure.
    }, class EntityConfigurationParametersResult {
        name: storage.journal.commitIntervalMs
        value: 100
        valueRange: 1-500
        restartRequired: false
        readonly: false
        type: integer
        description: Interval in milliseconds between journal operations.
    }, class EntityConfigurationParametersResult {
        name: storage.journal.enabled
        value: true
        valueRange: true|false
        restartRequired: true
        readonly: true
        type: boolean
        description: Enable or disable the durability journal.
    }, class EntityConfigurationParametersResult {
        name: storage.syncPeriodSecs
        value: 60
        valueRange: 60
        restartRequired: false
        readonly: true
        type: integer
        description: Interval in seconds at which DDS flushes modified data to the data files via an fsync operation.
    }, class EntityConfigurationParametersResult {
        name: storage.wiredTiger.engineConfig.cacheSizeGB
        value: 1.5
        valueRange: 1.2-1.5
        restartRequired: false
        readonly: false
        type: float
        description: Maximum size of the internal cache used by WiredTiger.
    }, class EntityConfigurationParametersResult {
        name: storage.wiredTiger.engineConfig.directoryForIndexes
        value: true
        valueRange: true|false
        restartRequired: true
        readonly: true
        type: boolean
        description: Whether mongod stores indexes and collections in separate directories.
    }, class EntityConfigurationParametersResult {
        name: storage.wiredTiger.engineConfig.journalCompressor
        value: snappy
        valueRange: none|snappy|zlib
        restartRequired: true
        readonly: true
        type: string
        description: Compression type used to compress WiredTiger journal.
    }, class EntityConfigurationParametersResult {
        name: wiredTigerConcurrentWriteTransactions
        value: 128
        valueRange: 64-256
        restartRequired: false
        readonly: false
        type: integer
        description: Maximum number of concurrent write transactions allowed into the WiredTiger storage engine.
    }]
}
```

- Return value from the API (UpdateEntityConfiguration) for modifying parameters that require a restart to apply
  changes:

```
class UpdateEntityConfigurationResponse {
    jobId: b523ca1a-1d3b-4751-802c-3312e0b34e5b
    restartRequired: true
}
```

- Return value from the API (RestartInstance) for restarting a DB instance:

```
class RestartInstanceResponse {
    jobId: da4f001f-3abc-4c99-9bbc-a85ae9e2ec33
}
```

- Return value from the API (ShowEntityConfiguration) for querying parameters of a specified instance:

```
class ShowEntityConfigurationResponse {
    datastoreVersion: 3.4
    datastoreName: mongodb
    created: 2022-10-11T08:22:50+0000
    updated: 2022-10-20T02:15:49+0000
    parameters: [class EntityConfigurationParametersResult {
        name: connPoolMaxConnsPerHost
        value: 600
        valueRange: 200-2000
        restartRequired: true
        readonly: false
        type: integer
        description: Maximum size of the connection pools for connections to other mongod instances.
    }, class EntityConfigurationParametersResult {
        name: cursorTimeoutMillis
        value: 600000
        valueRange: 600000-1000000
        restartRequired: false
        readonly: false
        type: integer
        description: Expiration threshold for idle cursors before DDS removes them.
    }, class EntityConfigurationParametersResult {
        name: disableJavaScriptJIT
        value: true
        valueRange: true|false
        restartRequired: false
        readonly: false
        type: boolean
        description: Enable or disable JavaScriptJIT.
    }, class EntityConfigurationParametersResult {
        name: failIndexKeyTooLong
        value: true
        valueRange: true|false
        restartRequired: false
        readonly: true
        type: boolean
        description: If the length of an indexed field value is longer than Index Key Length Limit, an error will be reported.
    }, class EntityConfigurationParametersResult {
        name: net.maxIncomingConnections
        value: 400
        valueRange: 200-500
        restartRequired: false
        readonly: false
        type: integer
        description: Maximum number of simultaneous connections that mongos or mongod will accept. The default value depends on system architecture.
    }, class EntityConfigurationParametersResult {
        name: operationProfiling.mode
        value: slowOp
        valueRange: off|slowOp|all
        restartRequired: true
        readonly: false
        type: string
        description: Level of database profiling.
    }, class EntityConfigurationParametersResult {
        name: operationProfiling.slowOpThresholdMs
        value: 500
        valueRange: 10-10000
        restartRequired: false
        readonly: false
        type: integer
        description: Slow request threshold. If there is no special requirement, it is recommended to use the default 500ms. Requests that exceed this value will be recorded in the system.profile collection of the corresponding db.
    }, class EntityConfigurationParametersResult {
        name: replication.enableMajorityReadConcern
        value: false
        valueRange: true|false
        restartRequired: true
        readonly: true
        type: boolean
        description: Whether to enable enableMajorityReadConcern.
    }, class EntityConfigurationParametersResult {
        name: security.authorization
        value: enabled
        valueRange: disabled|enabled
        restartRequired: true
        readonly: true
        type: string
        description: Enable or disable Access Control.
    }, class EntityConfigurationParametersResult {
        name: storage.directoryPerDB
        value: true
        valueRange: true|false
        restartRequired: true
        readonly: true
        type: boolean
        description: Whether DDS uses a separate directory to store data for each database.
    }, class EntityConfigurationParametersResult {
        name: storage.engine
        value: wiredTiger
        valueRange: mmapv1|wiredTiger|inmem
        restartRequired: true
        readonly: true
        type: string
        description: Storage engine of mongod.
    }, class EntityConfigurationParametersResult {
        name: storage.indexBuildRetry
        value: true
        valueRange: true|false
        restartRequired: true
        readonly: false
        type: boolean
        description: Whether mongod rebuilds indexes on the next start up after an index building failure.
    }, class EntityConfigurationParametersResult {
        name: storage.journal.commitIntervalMs
        value: 100
        valueRange: 1-500
        restartRequired: false
        readonly: false
        type: integer
        description: Interval in milliseconds between journal operations.
    }, class EntityConfigurationParametersResult {
        name: storage.journal.enabled
        value: true
        valueRange: true|false
        restartRequired: true
        readonly: true
        type: boolean
        description: Enable or disable the durability journal.
    }, class EntityConfigurationParametersResult {
        name: storage.syncPeriodSecs
        value: 60
        valueRange: 60
        restartRequired: false
        readonly: true
        type: integer
        description: Interval in seconds at which DDS flushes modified data to the data files via an fsync operation.
    }, class EntityConfigurationParametersResult {
        name: storage.wiredTiger.engineConfig.cacheSizeGB
        value: 1.5
        valueRange: 1.2-1.5
        restartRequired: false
        readonly: false
        type: float
        description: Maximum size of the internal cache used by WiredTiger.
    }, class EntityConfigurationParametersResult {
        name: storage.wiredTiger.engineConfig.directoryForIndexes
        value: true
        valueRange: true|false
        restartRequired: true
        readonly: true
        type: boolean
        description: Whether mongod stores indexes and collections in separate directories.
    }, class EntityConfigurationParametersResult {
        name: storage.wiredTiger.engineConfig.journalCompressor
        value: snappy
        valueRange: none|snappy|zlib
        restartRequired: true
        readonly: true
        type: string
        description: Compression type used to compress WiredTiger journal.
    }, class EntityConfigurationParametersResult {
        name: wiredTigerConcurrentWriteTransactions
        value: 128
        valueRange: 64-256
        restartRequired: false
        readonly: false
        type: integer
        description: Maximum number of concurrent write transactions allowed into the WiredTiger storage engine.
    }]
}
```

## 7. References

For details,
see [Obtaining Parameters of a Specified DB Instance](https://support.huaweicloud.com/intl/en-us/api-dds/dds_api_0108.html)
. You can directly run and debug the API
in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DDS/doc?api=ShowEntityConfiguration).

For details,
see [Modifying Parameters of a Specified DB Instance](https://support.huaweicloud.com/intl/en-us/api-dds/dds_api_0109.html)
. You can directly run and debug the API
in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DDS/doc?api=UpdateEntityConfiguration).

For details, see [Restarting a DB Instance](https://support.huaweicloud.com/intl/en-us/api-dds/dds_api_0021.html). You
can directly run and debug the API
in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DDS/doc?api=RestartInstance).

## Change History

|    Date   | Issue|   Description  |
|:----------:| :------: | :----------: |
| 2022-10-20 |   1.0    | This issue is the first official release.|
