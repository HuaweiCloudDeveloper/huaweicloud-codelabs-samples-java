package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.dds.v3.DdsClient;
import com.huaweicloud.sdk.dds.v3.model.ListInstancesRequest;
import com.huaweicloud.sdk.dds.v3.model.ListInstancesResponse;
import com.huaweicloud.sdk.dds.v3.model.RestartInstanceRequest;
import com.huaweicloud.sdk.dds.v3.model.RestartInstanceRequestBody;
import com.huaweicloud.sdk.dds.v3.model.RestartInstanceResponse;
import com.huaweicloud.sdk.dds.v3.model.ShowEntityConfigurationRequest;
import com.huaweicloud.sdk.dds.v3.model.ShowEntityConfigurationResponse;
import com.huaweicloud.sdk.dds.v3.model.UpdateConfigurationParameterResult;
import com.huaweicloud.sdk.dds.v3.model.UpdateEntityConfigurationRequest;
import com.huaweicloud.sdk.dds.v3.model.UpdateEntityConfigurationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class ParamsConfigDemo {
    private static final Logger logger = LoggerFactory.getLogger(ParamsConfigDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     * args[4] = "<YOUR ENTITY_ID>"
     * <p>
     * Hard-coded or plaintext AK and SK are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 5) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];
        String entityId = args[4];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        DdsClient client = DdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .build();

        // Queries parameters of a specified instance.
        queryInstanceParamGroup(client, instanceId, entityId);

        // Modifies parameters that require a restart to apply changes.
        updateInstanceParamGroup(client, instanceId, entityId);

        // Restarts the instance if needed.
        restartInstance(client, instanceId);

        // Queries parameters of the specified instance again.
        queryInstanceParamGroup(client, instanceId, entityId);
    }

    private static void queryInstanceDetail(DdsClient client, String instanceId) {
        ListInstancesRequest request = new ListInstancesRequest();
        request.setId(instanceId);

        try {
            ListInstancesResponse response = client.listInstances(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void restartInstance(DdsClient client, String instanceId) {

        RestartInstanceRequest request = new RestartInstanceRequest();
        RestartInstanceRequestBody body = new RestartInstanceRequestBody();
        body.setTargetId(instanceId);

        request.setInstanceId(instanceId);
        request.withBody(body);

        try {
            RestartInstanceResponse response = client.restartInstance(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void updateInstanceParamGroup(DdsClient client, String instanceId, String entityId) {
        UpdateEntityConfigurationRequest request = new UpdateEntityConfigurationRequest();
        UpdateConfigurationParameterResult body = new UpdateConfigurationParameterResult();

        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("net.maxIncomingConnections", "400");
        paramMap.put("connPoolMaxConnsPerHost", "600");
        body.setParameterValues(paramMap);
        body.setEntityId(entityId);

        request.setInstanceId(instanceId);
        request.setBody(body);

        try {
            UpdateEntityConfigurationResponse response = client.updateEntityConfiguration(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void queryInstanceParamGroup(DdsClient client, String instanceId, String entityId) {
        ShowEntityConfigurationRequest request = new ShowEntityConfigurationRequest();
        request.setInstanceId(instanceId);
        request.setEntityId(entityId);

        try {
            ShowEntityConfigurationResponse response = client.showEntityConfiguration(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}
