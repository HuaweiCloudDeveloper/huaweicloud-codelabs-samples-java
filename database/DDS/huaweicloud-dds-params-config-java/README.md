## 1. 介绍

文档数据库服务（document database service，简称dds）完全兼容mongodb协议，提供安全、高可用、高可靠、弹性伸缩和易用的数据库服务，同时提供一键部署、弹性扩容、容灾、备份、恢复、监控和告警等功能。

## 2. 流程图

![修改实例参数的流程图](assets/param.png)

## 3. 前置条件

1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

## 4. SDK获取和安装

您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-dds</artifactId>
    <version>3.1.1</version>
</dependency>
```

## 5. 关键代码片段

以下代码展示如何使用SDK修改实例参数：

```java

public class ParamsConfigDemo {
    private static final Logger logger = LoggerFactory.getLogger(ParamsConfigDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     * args[4] = "<YOUR ENTITY_ID>"
     *
     * 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
     * 本示例以ak和sk保存在环境变量中为例，运行本示例前请先在本地环境变量中设置环境变量 HUAWEICLOUD_SDK_AK 和 HUAWEICLOUD_SDK_SK
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 5) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];
        String entityId = args[4];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        DdsClient client = DdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .build();

        // 查询指定实例的参数信息
        queryInstanceParamGroup(client, instanceId, entityId);

        // 修改需要重启生效的参数
        updateInstanceParamGroup(client, instanceId, entityId);

        // 重启实例 被修改参数如果需要重启则调用该方法，否则可以不调用该方法
        restartInstance(client, instanceId);

        // 重启完毕后再次查询指定实例的参数信息
        queryInstanceParamGroup(client, instanceId, entityId);
    }

    private static void queryInstanceDetail(DdsClient client, String instanceId) {
        ListInstancesRequest request = new ListInstancesRequest();
        request.setId(instanceId);

        try {
            ListInstancesResponse response = client.listInstances(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void restartInstance(DdsClient client, String instanceId) {

        RestartInstanceRequest request = new RestartInstanceRequest();
        RestartInstanceRequestBody body = new RestartInstanceRequestBody();
        body.setTargetId(instanceId);

        request.setInstanceId(instanceId);
        request.withBody(body);

        try {
            RestartInstanceResponse response = client.restartInstance(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void updateInstanceParamGroup(DdsClient client, String instanceId, String entityId) {
        UpdateEntityConfigurationRequest request = new UpdateEntityConfigurationRequest();
        UpdateConfigurationParameterResult body = new UpdateConfigurationParameterResult();

        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("net.maxIncomingConnections", "400");
        paramMap.put("connPoolMaxConnsPerHost", "600");
        body.setParameterValues(paramMap);
        body.setEntityId(entityId);

        request.setInstanceId(instanceId);
        request.setBody(body);

        try {
            UpdateEntityConfigurationResponse response = client.updateEntityConfiguration(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void queryInstanceParamGroup(DdsClient client, String instanceId, String entityId) {
        ShowEntityConfigurationRequest request = new ShowEntityConfigurationRequest();
        request.setInstanceId(instanceId);
        request.setEntityId(entityId);

        try {
            ShowEntityConfigurationResponse response = client.showEntityConfiguration(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}


```

## 6. 返回结果示例

- 查询指定实例的参数信息（ShowEntityConfiguration）接口的返回值：

```
class ShowEntityConfigurationResponse {
    datastoreVersion: 3.4
    datastoreName: mongodb
    created: 2022-10-11T08:22:50+0000
    updated: 2022-10-20T02:15:49+0000
    parameters: [class EntityConfigurationParametersResult {
        name: connPoolMaxConnsPerHost
        value: 500
        valueRange: 200-2000
        restartRequired: true
        readonly: false
        type: integer
        description: Maximum size of the connection pools for connections to other mongod instances.
    }, class EntityConfigurationParametersResult {
        name: cursorTimeoutMillis
        value: 600000
        valueRange: 600000-1000000
        restartRequired: false
        readonly: false
        type: integer
        description: Expiration threshold for idle cursors before DDS removes them.
    }, class EntityConfigurationParametersResult {
        name: disableJavaScriptJIT
        value: true
        valueRange: true|false
        restartRequired: false
        readonly: false
        type: boolean
        description: Enable or disable JavaScriptJIT.
    }, class EntityConfigurationParametersResult {
        name: failIndexKeyTooLong
        value: true
        valueRange: true|false
        restartRequired: false
        readonly: true
        type: boolean
        description: If the length of an indexed field value is longer than Index Key Length Limit, an error will be reported.
    }, class EntityConfigurationParametersResult {
        name: net.maxIncomingConnections
        value: 300
        valueRange: 200-500
        restartRequired: false
        readonly: false
        type: integer
        description: Maximum number of simultaneous connections that mongos or mongod will accept. The default value depends on system architecture.
    }, class EntityConfigurationParametersResult {
        name: operationProfiling.mode
        value: slowOp
        valueRange: off|slowOp|all
        restartRequired: true
        readonly: false
        type: string
        description: Level of database profiling.
    }, class EntityConfigurationParametersResult {
        name: operationProfiling.slowOpThresholdMs
        value: 500
        valueRange: 10-10000
        restartRequired: false
        readonly: false
        type: integer
        description: Slow request threshold. If there is no special requirement, it is recommended to use the default 500ms. Requests that exceed this value will be recorded in the system.profile collection of the corresponding db.
    }, class EntityConfigurationParametersResult {
        name: replication.enableMajorityReadConcern
        value: false
        valueRange: true|false
        restartRequired: true
        readonly: true
        type: boolean
        description: Whether to enable enableMajorityReadConcern.
    }, class EntityConfigurationParametersResult {
        name: security.authorization
        value: enabled
        valueRange: disabled|enabled
        restartRequired: true
        readonly: true
        type: string
        description: Enable or disable Access Control.
    }, class EntityConfigurationParametersResult {
        name: storage.directoryPerDB
        value: true
        valueRange: true|false
        restartRequired: true
        readonly: true
        type: boolean
        description: Whether DDS uses a separate directory to store data for each database.
    }, class EntityConfigurationParametersResult {
        name: storage.engine
        value: wiredTiger
        valueRange: mmapv1|wiredTiger|inmem
        restartRequired: true
        readonly: true
        type: string
        description: Storage engine of mongod.
    }, class EntityConfigurationParametersResult {
        name: storage.indexBuildRetry
        value: true
        valueRange: true|false
        restartRequired: true
        readonly: false
        type: boolean
        description: Whether mongod rebuilds indexes on the next start up after an index building failure.
    }, class EntityConfigurationParametersResult {
        name: storage.journal.commitIntervalMs
        value: 100
        valueRange: 1-500
        restartRequired: false
        readonly: false
        type: integer
        description: Interval in milliseconds between journal operations.
    }, class EntityConfigurationParametersResult {
        name: storage.journal.enabled
        value: true
        valueRange: true|false
        restartRequired: true
        readonly: true
        type: boolean
        description: Enable or disable the durability journal.
    }, class EntityConfigurationParametersResult {
        name: storage.syncPeriodSecs
        value: 60
        valueRange: 60
        restartRequired: false
        readonly: true
        type: integer
        description: Interval in seconds at which DDS flushes modified data to the data files via an fsync operation.
    }, class EntityConfigurationParametersResult {
        name: storage.wiredTiger.engineConfig.cacheSizeGB
        value: 1.5
        valueRange: 1.2-1.5
        restartRequired: false
        readonly: false
        type: float
        description: Maximum size of the internal cache used by WiredTiger.
    }, class EntityConfigurationParametersResult {
        name: storage.wiredTiger.engineConfig.directoryForIndexes
        value: true
        valueRange: true|false
        restartRequired: true
        readonly: true
        type: boolean
        description: Whether mongod stores indexes and collections in separate directories.
    }, class EntityConfigurationParametersResult {
        name: storage.wiredTiger.engineConfig.journalCompressor
        value: snappy
        valueRange: none|snappy|zlib
        restartRequired: true
        readonly: true
        type: string
        description: Compression type used to compress WiredTiger journal.
    }, class EntityConfigurationParametersResult {
        name: wiredTigerConcurrentWriteTransactions
        value: 128
        valueRange: 64-256
        restartRequired: false
        readonly: false
        type: integer
        description: Maximum number of concurrent write transactions allowed into the WiredTiger storage engine.
    }]
}
```

- 修改需要重启生效的参数（UpdateEntityConfiguration）接口的返回值：

```
class UpdateEntityConfigurationResponse {
    jobId: b523ca1a-1d3b-4751-802c-3312e0b34e5b
    restartRequired: true
}
```

- 重启实例（RestartInstance）接口的返回值：

```
class RestartInstanceResponse {
    jobId: da4f001f-3abc-4c99-9bbc-a85ae9e2ec33
}
```

- 查询指定实例的参数信息（ShowEntityConfiguration）接口的返回值：

```
class ShowEntityConfigurationResponse {
    datastoreVersion: 3.4
    datastoreName: mongodb
    created: 2022-10-11T08:22:50+0000
    updated: 2022-10-20T02:15:49+0000
    parameters: [class EntityConfigurationParametersResult {
        name: connPoolMaxConnsPerHost
        value: 600
        valueRange: 200-2000
        restartRequired: true
        readonly: false
        type: integer
        description: Maximum size of the connection pools for connections to other mongod instances.
    }, class EntityConfigurationParametersResult {
        name: cursorTimeoutMillis
        value: 600000
        valueRange: 600000-1000000
        restartRequired: false
        readonly: false
        type: integer
        description: Expiration threshold for idle cursors before DDS removes them.
    }, class EntityConfigurationParametersResult {
        name: disableJavaScriptJIT
        value: true
        valueRange: true|false
        restartRequired: false
        readonly: false
        type: boolean
        description: Enable or disable JavaScriptJIT.
    }, class EntityConfigurationParametersResult {
        name: failIndexKeyTooLong
        value: true
        valueRange: true|false
        restartRequired: false
        readonly: true
        type: boolean
        description: If the length of an indexed field value is longer than Index Key Length Limit, an error will be reported.
    }, class EntityConfigurationParametersResult {
        name: net.maxIncomingConnections
        value: 400
        valueRange: 200-500
        restartRequired: false
        readonly: false
        type: integer
        description: Maximum number of simultaneous connections that mongos or mongod will accept. The default value depends on system architecture.
    }, class EntityConfigurationParametersResult {
        name: operationProfiling.mode
        value: slowOp
        valueRange: off|slowOp|all
        restartRequired: true
        readonly: false
        type: string
        description: Level of database profiling.
    }, class EntityConfigurationParametersResult {
        name: operationProfiling.slowOpThresholdMs
        value: 500
        valueRange: 10-10000
        restartRequired: false
        readonly: false
        type: integer
        description: Slow request threshold. If there is no special requirement, it is recommended to use the default 500ms. Requests that exceed this value will be recorded in the system.profile collection of the corresponding db.
    }, class EntityConfigurationParametersResult {
        name: replication.enableMajorityReadConcern
        value: false
        valueRange: true|false
        restartRequired: true
        readonly: true
        type: boolean
        description: Whether to enable enableMajorityReadConcern.
    }, class EntityConfigurationParametersResult {
        name: security.authorization
        value: enabled
        valueRange: disabled|enabled
        restartRequired: true
        readonly: true
        type: string
        description: Enable or disable Access Control.
    }, class EntityConfigurationParametersResult {
        name: storage.directoryPerDB
        value: true
        valueRange: true|false
        restartRequired: true
        readonly: true
        type: boolean
        description: Whether DDS uses a separate directory to store data for each database.
    }, class EntityConfigurationParametersResult {
        name: storage.engine
        value: wiredTiger
        valueRange: mmapv1|wiredTiger|inmem
        restartRequired: true
        readonly: true
        type: string
        description: Storage engine of mongod.
    }, class EntityConfigurationParametersResult {
        name: storage.indexBuildRetry
        value: true
        valueRange: true|false
        restartRequired: true
        readonly: false
        type: boolean
        description: Whether mongod rebuilds indexes on the next start up after an index building failure.
    }, class EntityConfigurationParametersResult {
        name: storage.journal.commitIntervalMs
        value: 100
        valueRange: 1-500
        restartRequired: false
        readonly: false
        type: integer
        description: Interval in milliseconds between journal operations.
    }, class EntityConfigurationParametersResult {
        name: storage.journal.enabled
        value: true
        valueRange: true|false
        restartRequired: true
        readonly: true
        type: boolean
        description: Enable or disable the durability journal.
    }, class EntityConfigurationParametersResult {
        name: storage.syncPeriodSecs
        value: 60
        valueRange: 60
        restartRequired: false
        readonly: true
        type: integer
        description: Interval in seconds at which DDS flushes modified data to the data files via an fsync operation.
    }, class EntityConfigurationParametersResult {
        name: storage.wiredTiger.engineConfig.cacheSizeGB
        value: 1.5
        valueRange: 1.2-1.5
        restartRequired: false
        readonly: false
        type: float
        description: Maximum size of the internal cache used by WiredTiger.
    }, class EntityConfigurationParametersResult {
        name: storage.wiredTiger.engineConfig.directoryForIndexes
        value: true
        valueRange: true|false
        restartRequired: true
        readonly: true
        type: boolean
        description: Whether mongod stores indexes and collections in separate directories.
    }, class EntityConfigurationParametersResult {
        name: storage.wiredTiger.engineConfig.journalCompressor
        value: snappy
        valueRange: none|snappy|zlib
        restartRequired: true
        readonly: true
        type: string
        description: Compression type used to compress WiredTiger journal.
    }, class EntityConfigurationParametersResult {
        name: wiredTigerConcurrentWriteTransactions
        value: 128
        valueRange: 64-256
        restartRequired: false
        readonly: false
        type: integer
        description: Maximum number of concurrent write transactions allowed into the WiredTiger storage engine.
    }]
}
```

## 7.参考链接

请见 [获取指定实例的参数API](https://support.huaweicloud.com/api-dds/dds_api_0108.html)
您可以在 [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=DDS&api=ShowEntityConfiguration)
中直接运行调试该接口。

请见 [修改指定实例的参数API](https://support.huaweicloud.com/api-dds/dds_api_0109.html)
您可以在 [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=DDS&api=UpdateEntityConfiguration)
中直接运行调试该接口。

请见 [重启实例的数据库服务API](https://support.huaweicloud.com/api-dds/dds_api_0021.html)
您可以在 [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=DDS&api=RestartInstance)
中直接运行调试该接口。

## 修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2022-10-20 |   1.0    | 文档首次发布 |
