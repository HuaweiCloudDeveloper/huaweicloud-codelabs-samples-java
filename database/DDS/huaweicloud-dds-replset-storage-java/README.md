## 1. 介绍

文档数据库服务（document database service，简称dds）完全兼容mongodb协议，提供安全、高可用、高可靠、弹性伸缩和易用的数据库服务，同时提供一键部署、弹性扩容、容灾、备份、恢复、监控和告警等功能。

## 2. 流程图

![副本集实例扩容存储容量的流程图](assets/volume.png)

## 3. 前置条件

1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realnameauth) 。。

2.获取华为云开发工具包（sdk），您也可以查看安装java sdk。

3.已获取华为云账号对应的access key（ak）和secret access key（sk）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的ak/sk。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持java jdk 1.8及其以上版本。

## 4. SDK获取和安装

您可以通过maven方式获取和安装sdk，首先需要在您的操作系统中下载并安装maven ，安装完成后您只需要在java项目的pom.xml文件中加入相应的依赖项即可。

具体的sdk版本号请参见 [sdk开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupid>com.huaweicloud.sdk</groupid>
    <artifactid>huaweicloud-sdk-dds</artifactid>
    <version>3.1.1</version>
</dependency>
```

## 5.关键代码片段

以下代码展示如何通过DDS SDK扩容副本集实例存储容量：

```java
public class ReplicaSetEnlargeVolumeDemo {

    private static final Logger logger = LoggerFactory.getLogger(ReplicaSetEnlargeVolumeDemo.class.getName());

    /**
     * args[0] = "<YOUR INSTANCE_ID>"
     * args[1] = "<YOUR REGION_ID>"
     *
     * 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
     * 本示例以ak和sk保存在环境变量中为例，运行本示例前请先在本地环境变量中设置环境变量 HUAWEICLOUD_SDK_AK 和 HUAWEICLOUD_SDK_SK
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 2) {
            logger.info("Illegal Arguments");
        }

        String instanceId = args[0];
        String regionId = args[1];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        DdsClient client = DdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(DdsRegion.valueOf(regionId))
                .build();

        // 查看副本集实例存储空间
        showDdsReplicaSetDetail(client, instanceId);

        // 扩容副本集实例存储空间
        String jobId = enlargeDdsReplicaSetVolume(client, instanceId);

        // 等待实例扩容成功,根据返回的jobId，查询进度，当返回的任务status为“Completed” 时表示扩容成功
        waitEnlargeSuccess(client, jobId);

        // 查看副本集实例存储空间
        showDdsReplicaSetDetail(client, instanceId);
    }

    private static void showDdsReplicaSetDetail(DdsClient client, String instanceId) {
        ListInstancesRequest request = new ListInstancesRequest();
        request.withId(instanceId);
        try {
            ListInstancesResponse response = client.listInstances(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static String enlargeDdsReplicaSetVolume(DdsClient client, String instanceId) {
        ResizeInstanceVolumeRequest request = new ResizeInstanceVolumeRequest();
        ResizeInstanceVolumeRequestBody body = new ResizeInstanceVolumeRequestBody();
        ResizeInstanceVolumeOption option = new ResizeInstanceVolumeOption();

        // 需要大于实例当前磁盘大小
        String targetVolumeSize = "50";
        option.setSize(targetVolumeSize);
        body.setVolume(option);
        request.withBody(body);
        request.setInstanceId(instanceId);

        ResizeInstanceVolumeResponse response = new ResizeInstanceVolumeResponse();
        try {
            response = client.resizeInstanceVolume(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return response.getJobId();
    }

    private static void waitEnlargeSuccess(DdsClient client, String jobId) {
        ShowJobDetailRequest request = new ShowJobDetailRequest();
        request.withId(jobId);
        try {
            ShowJobDetailResponse response = client.showJobDetail(request);
            logger.info(response.getJob().getStatus());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}

```

## 6.返回结果示例

* 查看实例详情（ListInstances）接口返回值:

```
{
 "instances": [
  {
   "id": "17d2c555623e4435acafc7e11a4d4275in02",
   "name": "dds-eaa4-test",
   "status": "normal",
   "port": "8635",
   "mode": "ReplicaSet",
   "region": "cn-north-7",
   "datastore": {
    "type": "DDS-Community",
    "version": "4.0",
    "patch_available": false
   },
   "engine": "wiredTiger",
   "created": "2022-10-18T03:46:48",
   "updated": "2022-10-18T11:50:15",
   "db_user_name": "rwuser",
   "ssl": 0,
   "vpc_id": "69fd9e54-013d-43ee-9a42-2ed68a4b4bac",
   "subnet_id": "645d9721-d785-4c56-a8f1-4c2f07fbf322",
   "security_group_id": "9eee68d3-cacd-49ad-bd57-064d7d69ad9f",
   "backup_strategy": {
    "start_time": "16:00-17:00",
    "keep_days": 7
   },
   "pay_mode": "0",
   "maintenance_window": "14:00-18:00",
   "groups": [
    {
     "type": "replica",
     "nodes": [
      {
       "id": "053169ab1a764900993e32e37790350dno02",
       "name": "dds-eaa4-test_replica_node_3",
       "status": "normal",
       "role": "Hidden",
       "private_ip": "192.168.0.19",
       "public_ip": "",
       "spec_code": "dds.mongodb.s6.medium.4.repset",
       "availability_zone": "cn-north-7c"
      },
      {
       "id": "38056407fb9540ed9dc295623b6d9310no02",
       "name": "dds-eaa4-test_replica_node_1",
       "status": "normal",
       "role": "Secondary",
       "private_ip": "192.168.0.251",
       "public_ip": "",
       "spec_code": "dds.mongodb.s6.medium.4.repset",
       "availability_zone": "cn-north-7c"
      },
      {
       "id": "b71958f1e35b4ee080abc808b2bd6ad3no02",
       "name": "dds-eaa4-test_replica_node_2",
       "status": "normal",
       "role": "Primary",
       "private_ip": "192.168.0.182",
       "public_ip": "",
       "spec_code": "dds.mongodb.s6.medium.4.repset",
       "availability_zone": "cn-north-7c"
      }
     ],
     "volume": {
      "size": "30",
      "used": "0.3426170349121094"
     }
    }
   ],
   "enterprise_project_id": "0",
   "time_zone": "",
   "actions": [],
   "tags": []
  }
 ],
 "total_count": 1
}
```

* 扩容实例存储容量 （ResizeInstanceVolume）接口返回值:

```
{
 "job_id": "31a22119-58b4-4894-a541-96db20fde980"
}
```

* 查询任务详情(showJobDetail) 接口返回值:

```
{
    "id": "ebd9a1b1-8652-416a-892a-64825cffc489",
    "name": "Enlarge_MongoDB_Volume",
    "status": "Completed",
    "created": "2022-10-24T08:43:16+0000",
    "ended": "2022-10-24T08:43:17+0000",
    "progress": "",
    "instance": {
        "id": "457ca2c4d5b44719a4ada969a0dae75cin02",
        "name": "dds-dbe1"
    },
    "failReason": null
}
```

## 7.参考链接

请见 [查询实例列表和详情](https://support.huaweicloud.com/intl/zh-cn/api-dds/dds_api_0023.html)
您可以在 [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/sdk?product=DDS&api=ListInstances)
中直接运行调试该接口。

请见 [扩容实例存储容量](https://support.huaweicloud.com/intl/zh-cn/api-dds/dds_api_0024.html)
您可以在 [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/sdk?product=DDS&api=ResizeInstanceVolume)
中直接运行调试该接口。

请见 查询数据库实例列表

## 修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2022-10-19 |   1.0    | 文档首次发布 |
