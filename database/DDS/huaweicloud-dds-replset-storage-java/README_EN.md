## 1. Introduction

Document Database Service (DDS) is a MongoDB-compatible database service that is secure, highly available, reliable,
scalable, and easy to use. It provides easy DB instance creation, scaling, disaster recovery, backup, restoration,
monitoring, and alarm reporting functions on the DDS console.

## 2. Flowchart

![Flowchart for scaling up the storage space of a replica set instance](assets/volume-en.png)

## 3. Prerequisites

1. You have created [a HUAWEI ID](https://id5.cloud.huawei.com/CAS/portal/userRegister/regbyphone.html?&lang=en-us) and
   completed [real-name authentication](https://auth.huaweicloud.com/authui/login.html?service=https%3A%2F%2Faccount.huaweicloud.com%2Fusercenter%2F%3Fregion%3Dcn-north-4%26cloud_route_state%3D%2Faccountindex%2Frealnameauth&locale=en-us#/login)
   .

2. You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3. You have obtained the access key ID (AK) and secret access key (SK) of the HUAWEI ID. You can create and view your
   AK/SK on the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details,
   see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html).

4. You have set up the development environment with Java JDK 1.8 or later.

## 4. Obtaining and Installing an SDK

You can obtain and install an SDK in Maven mode. Download and install Maven in your operating system (OS). After the
installation is complete, add the required dependencies to the **pom.xml** file of the Java project.

For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter).

```xml

<dependency>
    <groupid>com.huaweicloud.sdk</groupid>
    <artifactid>huaweicloud-sdk-dds</artifactid>
    <version>3.1.1</version>
</dependency>
```

## 5. Key Code Snippets

The following code shows how to scale up the storage space of a replica set instance using the SDK.

```java
public class ReplicaSetEnlargeVolumeDemo {

    private static final Logger logger = LoggerFactory.getLogger(ReplicaSetEnlargeVolumeDemo.class.getName());

    /**
     * args[0] = "<YOUR INSTANCE_ID>"
     * args[1] = "<YOUR REGION_ID>"
     *
     * Hard-coded or plaintext AK and SK are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 2) {
            logger.info("Illegal Arguments");
        }

        String instanceId = args[0];
        String regionId = args[1];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        DdsClient client = DdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(DdsRegion.valueOf(regionId))
                .build();

        // Views the storage space of the replica set instance.
        showDdsReplicaSetDetail(client, instanceId);

        // Scales up the storage space of the replica set instance.
        String jobId = enlargeDdsReplicaSetVolume(client, instanceId);

        // Queries the progress based on the returned jobId. If the returned task status is **Completed**, the scale-up is successful.
        waitEnlargeSuccess(client, jobId);

        // Views the storage space of the replica set instance.
        showDdsReplicaSetDetail(client, instanceId);
    }

    private static void showDdsReplicaSetDetail(DdsClient client, String instanceId) {
        ListInstancesRequest request = new ListInstancesRequest();
        request.withId(instanceId);
        try {
            ListInstancesResponse response = client.listInstances(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static String enlargeDdsReplicaSetVolume(DdsClient client, String instanceId) {
        ResizeInstanceVolumeRequest request = new ResizeInstanceVolumeRequest();
        ResizeInstanceVolumeRequestBody body = new ResizeInstanceVolumeRequestBody();
        ResizeInstanceVolumeOption option = new ResizeInstanceVolumeOption();

        // The value must be greater than the current disk size of the instance.
        String targetVolumeSize = "50";
        option.setSize(targetVolumeSize);
        body.setVolume(option);
        request.withBody(body);
        request.setInstanceId(instanceId);

        ResizeInstanceVolumeResponse response = new ResizeInstanceVolumeResponse();
        try {
            response = client.resizeInstanceVolume(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return response.getJobId();
    }

    private static void waitEnlargeSuccess(DdsClient client, String jobId) {
        ShowJobDetailRequest request = new ShowJobDetailRequest();
        request.withId(jobId);
        try {
            ShowJobDetailResponse response = client.showJobDetail(request);
            logger.info(response.getJob().getStatus());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}

```

## 6. Sample Return Values

* Return value from the API (ListInstances) for querying instance details:

```
{
 "instances": [
  {
   "id": "17d2c555623e4435acafc7e11a4d4275in02",
   "name": "dds-eaa4-test",
   "status": "normal",
   "port": "8635",
   "mode": "ReplicaSet",
   "region": "cn-north-7",
   "datastore": {
    "type": "DDS-Community",
    "version": "4.0",
    "patch_available": false
   },
   "engine": "wiredTiger",
   "created": "2022-10-18T03:46:48",
   "updated": "2022-10-18T11:50:15",
   "db_user_name": "rwuser",
   "ssl": 0,
   "vpc_id": "69fd9e54-013d-43ee-9a42-2ed68a4b4bac",
   "subnet_id": "645d9721-d785-4c56-a8f1-4c2f07fbf322",
   "security_group_id": "9eee68d3-cacd-49ad-bd57-064d7d69ad9f",
   "backup_strategy": {
    "start_time": "16:00-17:00",
    "keep_days": 7
   },
   "pay_mode": "0",
   "maintenance_window": "14:00-18:00",
   "groups": [
    {
     "type": "replica",
     "nodes": [
      {
       "id": "053169ab1a764900993e32e37790350dno02",
       "name": "dds-eaa4-test_replica_node_3",
       "status": "normal",
       "role": "Hidden",
       "private_ip": "192.168.0.19",
       "public_ip": "",
       "spec_code": "dds.mongodb.s6.medium.4.repset",
       "availability_zone": "cn-north-7c"
      },
      {
       "id": "38056407fb9540ed9dc295623b6d9310no02",
       "name": "dds-eaa4-test_replica_node_1",
       "status": "normal",
       "role": "Secondary",
       "private_ip": "192.168.0.251",
       "public_ip": "",
       "spec_code": "dds.mongodb.s6.medium.4.repset",
       "availability_zone": "cn-north-7c"
      },
      {
       "id": "b71958f1e35b4ee080abc808b2bd6ad3no02",
       "name": "dds-eaa4-test_replica_node_2",
       "status": "normal",
       "role": "Primary",
       "private_ip": "192.168.0.182",
       "public_ip": "",
       "spec_code": "dds.mongodb.s6.medium.4.repset",
       "availability_zone": "cn-north-7c"
      }
     ],
     "volume": {
      "size": "30",
      "used": "0.3426170349121094"
     }
    }
   ],
   "enterprise_project_id": "0",
   "time_zone": "",
   "actions": [],
   "tags": []
  }
 ],
 "total_count": 1
}
```

* Return value from the API (ResizeInstanceVolume) for scaling up storage space of a DB instance:

```
{
 "job_id": "31a22119-58b4-4894-a541-96db20fde980"
}
```

* Return value from the API (showJobDetail) for querying information about a task with a specified ID:

```
{
    "id": "ebd9a1b1-8652-416a-892a-64825cffc489",
    "name": "Enlarge_MongoDB_Volume",
    "status": "Completed",
    "created": "2022-10-24T08:43:16+0000",
    "ended": "2022-10-24T08:43:17+0000",
    "progress": "",
    "instance": {
        "id": "457ca2c4d5b44719a4ada969a0dae75cin02",
        "name": "dds-dbe1"
    },
    "failReason": null
}
```

## 7. References

For details, see [Querying Instances and Details](https://support.huaweicloud.com/intl/en-us/api-dds/dds_api_0023.html).
You can directly run and debug the API
in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DDS/doc?api=ListInstances).

For details, see [Scaling Up Storage Space](https://support.huaweicloud.com/intl/en-us/api-dds/dds_api_0024.html). You
can directly run and debug the API
in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DDS/doc?api=ResizeInstanceVolume).

For details, see "Querying DB instances."

## Change History

|    Date   | Issue|   Description  |
|:----------:| :------: | :----------: |
| 2022-10-19 |   1.0    | This issue is the first official release.|
