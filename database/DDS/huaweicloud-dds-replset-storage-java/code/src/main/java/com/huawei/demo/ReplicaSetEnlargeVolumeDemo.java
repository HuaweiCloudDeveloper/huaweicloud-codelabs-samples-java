package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.dds.v3.DdsClient;
import com.huaweicloud.sdk.dds.v3.model.ListInstancesRequest;
import com.huaweicloud.sdk.dds.v3.model.ListInstancesResponse;
import com.huaweicloud.sdk.dds.v3.model.ResizeInstanceVolumeOption;
import com.huaweicloud.sdk.dds.v3.model.ResizeInstanceVolumeRequest;
import com.huaweicloud.sdk.dds.v3.model.ResizeInstanceVolumeRequestBody;
import com.huaweicloud.sdk.dds.v3.model.ResizeInstanceVolumeResponse;
import com.huaweicloud.sdk.dds.v3.model.ShowJobDetailRequest;
import com.huaweicloud.sdk.dds.v3.model.ShowJobDetailResponse;
import com.huaweicloud.sdk.dds.v3.region.DdsRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReplicaSetEnlargeVolumeDemo {

    private static final Logger logger = LoggerFactory.getLogger(ReplicaSetEnlargeVolumeDemo.class.getName());

    /**
     * args[0] = "<YOUR INSTANCE_ID>"
     * args[1] = "<YOUR REGION_ID>"
     * <p>
     * Hard-coded or plaintext AK and SK are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 2) {
            logger.info("Illegal Arguments");
        }

        String instanceId = args[0];
        String regionId = args[1];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        DdsClient client = DdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(DdsRegion.valueOf(regionId))
                .build();

        // Views the storage space of the replica set instance.
        showDdsReplicaSetDetail(client, instanceId);

        // Scales up the storage space of the replica set instance.
        String jobId = enlargeDdsReplicaSetVolume(client, instanceId);

        // Queries the progress based on the returned jobId. If the returned task status is **Completed**, the scale-up is successful.
        waitEnlargeSuccess(client, jobId);

        // Views the storage space of the replica set instance.
        showDdsReplicaSetDetail(client, instanceId);
    }

    private static void showDdsReplicaSetDetail(DdsClient client, String instanceId) {
        ListInstancesRequest request = new ListInstancesRequest();
        request.withId(instanceId);
        try {
            ListInstancesResponse response = client.listInstances(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static String enlargeDdsReplicaSetVolume(DdsClient client, String instanceId) {
        ResizeInstanceVolumeRequest request = new ResizeInstanceVolumeRequest();
        ResizeInstanceVolumeRequestBody body = new ResizeInstanceVolumeRequestBody();
        ResizeInstanceVolumeOption option = new ResizeInstanceVolumeOption();

        // The value must be greater than the current disk size of the instance.
        String targetVolumeSize = "50";
        option.setSize(targetVolumeSize);
        body.setVolume(option);
        request.withBody(body);
        request.setInstanceId(instanceId);

        ResizeInstanceVolumeResponse response = new ResizeInstanceVolumeResponse();
        try {
            response = client.resizeInstanceVolume(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return response.getJobId();
    }

    private static void waitEnlargeSuccess(DdsClient client, String jobId) {
        ShowJobDetailRequest request = new ShowJobDetailRequest();
        request.withId(jobId);
        try {
            ShowJobDetailResponse response = client.showJobDetail(request);
            logger.info(response.getJob().getStatus());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}