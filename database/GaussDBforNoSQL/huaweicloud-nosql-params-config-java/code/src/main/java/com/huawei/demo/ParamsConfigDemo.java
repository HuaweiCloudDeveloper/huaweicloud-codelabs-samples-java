package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.gaussdbfornosql.v3.GaussDBforNoSQLClient;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.ShowInstanceConfigurationRequest;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.ShowInstanceConfigurationResponse;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.UpdateInstanceConfigurationRequest;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.UpdateInstanceConfigurationRequestBody;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.UpdateInstanceConfigurationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class ParamsConfigDemo {
    private static final Logger logger = LoggerFactory.getLogger(ParamsConfigDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     * <p>
     * Hard-coded or plaintext AK and SK are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 4) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        GaussDBforNoSQLClient client = GaussDBforNoSQLClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .build();

        // Query parameters of a specified instance.
        queryInstanceParamGroup(client, instanceId);

        // Modify parameters that do not require a restart to apply changes.
        Map<String, String> values = new HashMap<String, String>() {
            {
                put("concurrent_reads", "64");
            }
        };
        updateInstanceParamGroup(client, instanceId, values);

        // Query parameters of a specified instance.
        queryInstanceParamGroup(client, instanceId);
    }

    private static ShowInstanceConfigurationResponse queryInstanceParamGroup(GaussDBforNoSQLClient client, String instanceId) {
        ShowInstanceConfigurationRequest request = new ShowInstanceConfigurationRequest();
        request.setInstanceId(instanceId);
        ShowInstanceConfigurationResponse response = new ShowInstanceConfigurationResponse();
        try {
            response = client.showInstanceConfiguration(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return response;
    }

    private static void updateInstanceParamGroup(GaussDBforNoSQLClient client, String instanceId, Map<String, String> values) {
        UpdateInstanceConfigurationRequest request = new UpdateInstanceConfigurationRequest();
        UpdateInstanceConfigurationRequestBody body = new UpdateInstanceConfigurationRequestBody();
        body.setValues(values);
        request.setInstanceId(instanceId);
        request.withBody(body);
        try {
            UpdateInstanceConfigurationResponse response = client.updateInstanceConfiguration(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}