# 修改不需要重启的参数信息

## 版本说明

本示例配套的sdk版本为：3.1.5

## 1. 简介

云数据库 GeminiDB NoSQL是一款基于计算存储分离架构的分布式多模NoSQL数据库服务，在云计算平台高性能、高可用、高可靠、高安全、可弹性伸缩的基础上，提供了一键部署、备份恢复、监控报警等服务能力。云数据库 GeminiDB
NoSQL目前包含GaussDB(for Cassandra)、GeminiDB(for Mongo)、GeminiDB(for Influx)和GaussDB(for Redis)
四款产品，分别兼容Cassandra、MongoDB、InfluxDB和Redis主流NoSQL接口，并提供高读写性能，具有高性价比，适用于IoT、气象、互联网、游戏等领域。

## 2. 流程图

![副本集实例扩容存储容量的流程图](assets/params.png)

## 3. 前置条件

1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realnameauth) 。。

2.获取华为云开发工具包（sdk），您也可以查看安装java sdk。

3.已获取华为云账号对应的access key（ak）和secret access key（sk）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的ak/sk。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持java jdk 1.8及其以上版本。

## 4. SDK获取和安装

您可以通过maven方式获取和安装sdk，首先需要在您的操作系统中下载并安装maven ，安装完成后您只需要在java项目的pom.xml文件中加入相应的依赖项即可。

具体的sdk版本号请参见 [sdk开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-gaussdbfornosql</artifactId>
    <version>3.1.5</version>
</dependency>
```

## 5.关键代码片段

以下代码展示如何通过GaussDB NoSQL SDK修改不需要重启的参数

```java
public class ParamsConfigDemo {
    private static final Logger logger = LoggerFactory.getLogger(ParamsConfigDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     *
     * 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
     * 本示例以ak和sk保存在环境变量中为例，运行本示例前请先在本地环境变量中设置环境变量 HUAWEICLOUD_SDK_AK 和 HUAWEICLOUD_SDK_SK
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 4) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        GaussDBforNoSQLClient client = GaussDBforNoSQLClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .build();

        // 查询指定实例的参数信息
        queryInstanceParamGroup(client, instanceId);

        // 修改不需要重启失效的参数
        Map<String, String> values = new HashMap<String, String>() {
            {
                put("concurrent_reads", "64");
            }
        };
        updateInstanceParamGroup(client, instanceId, values);

        // 查询指定实例参数信息
        queryInstanceParamGroup(client, instanceId);
    }

    private static ShowInstanceConfigurationResponse queryInstanceParamGroup(GaussDBforNoSQLClient client, String instanceId) {
        ShowInstanceConfigurationRequest request = new ShowInstanceConfigurationRequest();
        request.setInstanceId(instanceId);
        ShowInstanceConfigurationResponse response = new ShowInstanceConfigurationResponse();
        try {
            response = client.showInstanceConfiguration(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return response;
    }

    private static void updateInstanceParamGroup(GaussDBforNoSQLClient client, String instanceId, Map<String, String> values) {
        UpdateInstanceConfigurationRequest request = new UpdateInstanceConfigurationRequest();
        UpdateInstanceConfigurationRequestBody body = new UpdateInstanceConfigurationRequestBody();
        body.setValues(values);
        request.setInstanceId(instanceId);
        request.withBody(body);
        try {
            UpdateInstanceConfigurationResponse response = client.updateInstanceConfiguration(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}
```

## 6.返回结果示例

* 查看实例参数组详情（showInstanceConfiguration）接口返回值:

```
{
    "datastore_version_name":"3.11",
    "datastore_name":"cassandra",
    "created":"2022-06-22T08:07:59+0000",
    "updated":"2022-08-02T03:33:11+0000",
    "configuration_parameters":[
        {
            "name":"authenticator",
            "value":"PasswordAuthenticator",
            "restart_required":true,
            "readonly":true,
            "value_range":"PasswordAuthenticator|AllowAllAuthenticator",
            "type":"string",
            "description":"Backend username verification."
        },
        {
            "name":"authorizer",
            "value":"CassandraAuthorizer",
            "restart_required":true,
            "readonly":true,
            "value_range":"AllowAllAuthorizer|CassandraAuthorizer",
            "type":"string",
            "description":"Backend user authorization verification."
        },
        {
            "name":"batch_size_fail_threshold_in_kb",
            "value":"5000",
            "restart_required":true,
            "readonly":true,
            "value_range":"1-10000",
            "type":"integer",
            "description":"When the size of a batch processing request exceeds the threshold, the request fails to be processed."
        },
        {
            "name":"batch_size_warn_threshold_in_kb",
            "value":"5",
            "restart_required":false,
            "readonly":false,
            "value_range":"1-1000",
            "type":"integer",
            "description":"When the size of batch processing requests exceeds the threshold, logs of the WARN level are printed."
        },
        {
            "name":"cas_contention_timeout_in_ms",
            "value":"1000",
            "restart_required":false,
            "readonly":false,
            "value_range":"100-100000",
            "type":"integer",
            "description":"Timeout duration for the coordinator to wait for CAS operations to complete."
        },
        {
            "name":"concurrent_alter_schema_forwards",
            "value":"8",
            "restart_required":true,
            "readonly":true,
            "value_range":"4-512",
            "type":"integer",
            "description":"Number of threads for concurrently executing the ALTER statement."
        },
        {
            "name":"concurrent_counter_writes",
            "value":"32",
            "restart_required":true,
            "readonly":true,
            "value_range":"4-512",
            "type":"integer",
            "description":"Number of concurrent write threads of the counter."
        },
        {
            "name":"concurrent_materialized_view_writes",
            "value":"32",
            "restart_required":true,
            "readonly":true,
            "value_range":"4-512",
            "type":"integer",
            "description":"Number of concurrent write threads for materialized views."
        },
        {
            "name":"concurrent_reads",
            "value":"32",
            "restart_required":true,
            "readonly":true,
            "value_range":"4-512",
            "type":"integer",
            "description":"Number of concurrent read threads. The default value depends on the CPU specifications."
        },
        {
            "name":"concurrent_writes",
            "value":"16",
            "restart_required":true,
            "readonly":true,
            "value_range":"4-512",
            "type":"integer",
            "description":"Number of concurrent write threads. The default value depends on the CPU specifications."
        },
        {
            "name":"counter_write_request_timeout_in_ms",
            "value":"5000",
            "restart_required":false,
            "readonly":false,
            "value_range":"100-100000",
            "type":"integer",
            "description":"Timeout duration for the coordinator to wait for counter write operations to complete."
        },
        {
            "name":"range_request_timeout_in_ms",
            "value":"10000",
            "restart_required":false,
            "readonly":false,
            "value_range":"100-100000",
            "type":"integer",
            "description":"Timeout duration for the coordinator to wait for range operations to complete."
        },
        {
            "name":"read_request_timeout_in_ms",
            "value":"5000",
            "restart_required":false,
            "readonly":false,
            "value_range":"100-100000",
            "type":"integer",
            "description":"Timeout duration for the coordinator to wait for read operations to complete."
        },
        {
            "name":"request_timeout_in_ms",
            "value":"10000",
            "restart_required":false,
            "readonly":false,
            "value_range":"100-100000",
            "type":"integer",
            "description":"Timeout duration for the coordinator to wait for the completion of other operations except read, write, range, counter write, CAS, and truncate."
        },
        {
            "name":"role_manager",
            "value":"CassandraRoleManager",
            "restart_required":true,
            "readonly":true,
            "value_range":"CassandraRoleManager",
            "type":"string",
            "description":"Inter-role authorization management."
        },
        {
            "name":"slow_query_log_timeout_in_ms",
            "value":"500",
            "restart_required":true,
            "readonly":false,
            "value_range":"1-60000",
            "type":"integer",
            "description":"Slow query time threshold, in milliseconds."
        },
        {
            "name":"tombstone_failure_threshold",
            "value":"100000",
            "restart_required":false,
            "readonly":false,
            "value_range":"10-10000000",
            "type":"integer",
            "description":"If the number of tombstones in a query result exceeds the threshold, the query fails."
        },
        {
            "name":"tombstone_warn_threshold",
            "value":"1000",
            "restart_required":false,
            "readonly":false,
            "value_range":"10-100000",
            "type":"integer",
            "description":"When the number of tombstones in a query result exceeds the threshold, logs of the WARN level are printed."
        },
        {
            "name":"truncate_request_timeout_in_ms",
            "value":"60000",
            "restart_required":false,
            "readonly":false,
            "value_range":"100-100000",
            "type":"integer",
            "description":"Timeout duration for the coordinator to wait for truncate operations to complete."
        },
        {
            "name":"unlogged_batch_across_partitions_warn_threshold",
            "value":"10",
            "restart_required":false,
            "readonly":false,
            "value_range":"1-10000",
            "type":"integer",
            "description":"When the number of partitions for batch processing exceeds the threshold, logs of the WARN level are printed."
        },
        {
            "name":"write_request_timeout_in_ms",
            "value":"2000",
            "restart_required":false,
            "readonly":false,
            "value_range":"100-100000",
            "type":"integer",
            "description":"Timeout duration for the coordinator to wait for write operations to complete."
        }
    ]
}
```

* 修改实例不需要重启的参数组 （updateInstanceConfiguration）接口返回值:

```
{
    jobId: f44ecd0f-1763-431c-8dd9-1fabb7e811e5
    restartRequired: false
}
```

## 7.参考链接

请见 [获取指定实例的参数](https://support.huaweicloud.com/intl/zh-cn/api-nosql/nosql_06_0007.html)
您可以在 [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=GaussDBforNoSQL&api=ShowInstanceConfiguration)
中直接运行调试该接口。

请见 [修改指定实例的参数](https://support.huaweicloud.com/intl/zh-cn/api-nosql/nosql_06_0006.html)
您可以在 [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=GaussDBforNoSQL&api=UpdateInstanceConfiguration)
中直接运行调试该接口。

## 修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2022-10-25 |   1.0    | 文档首次发布 |


