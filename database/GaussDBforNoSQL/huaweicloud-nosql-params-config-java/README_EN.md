# Modifying Parameters That Do Not Require A Restart to Apply Changes

## Version Description

In this sample, the SDK version is 3.1.5.

## 1. Introduction

GeminiDB is a distributed, multi-model NoSQL database service with decoupled storage and compute. It is highly
available, secure, and scalable and can provide robust performance and service capabilities like quick deployment,
backup, restoration, monitoring, and alarm reporting. GeminiDB supports APIs for Cassandra, Mongo, Influx, and Redis.
They are compatible with mainstream NoSQL APIs of Cassandra, MongoDB, InfluxDB, and Redis, respectively, and provide
high read/write performance at low costs, making it well suited to IoT, meteorology, Internet, and gaming sectors.

## 2. Flowchart

![Modifying parameters that do not require a restart to apply changes](assets/params-en.png)

## 3. Prerequisites

1. You have created [a HUAWEI ID](https://id5.cloud.huawei.com/CAS/portal/userRegister/regbyphone.html?&lang=en-us) and
   completed [real-name authentication](https://auth.huaweicloud.com/authui/login.html?service=https%3A%2F%2Faccount.huaweicloud.com%2Fusercenter%2F%3Fregion%3Dcn-north-4%26cloud_route_state%3D%2Faccountindex%2Frealnameauth&locale=en-us#/login)
   .

2. You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3. You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
   view your AK/SK on the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details,
   see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html).

4. You have set up the development environment with Java JDK 1.8 or later.

## 4. Obtaining and Installing an SDK

You can obtain and install an SDK in Maven mode. Download and install Maven in your operating system (OS). After the
installation is complete, add the required dependencies to the **pom.xml** file of the Java project.

For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-gaussdbfornosql</artifactId>
    <version>3.1.5</version>
</dependency>
```

## 5. Key Code Snippets

The following code shows how to modify parameters that do not require a restart to apply changes using the GeminiDB SDK.

```java
public class ParamsConfigDemo {
    private static final Logger logger = LoggerFactory.getLogger(ParamsConfigDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     *
     * Hard-coded or plaintext AK and SK are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 4) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        GaussDBforNoSQLClient client = GaussDBforNoSQLClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .build();

        // Query parameters of a specified instance.
        queryInstanceParamGroup(client, instanceId);

        // Modify parameters that do not require a restart to apply changes.
        Map<String, String> values = new HashMap<String, String>() {
            {
                put("concurrent_reads", "64");
            }
        };
        updateInstanceParamGroup(client, instanceId, values);

        // Query parameters of a specified instance.
        queryInstanceParamGroup(client, instanceId);
    }

    private static ShowInstanceConfigurationResponse queryInstanceParamGroup(GaussDBforNoSQLClient client, String instanceId) {
        ShowInstanceConfigurationRequest request = new ShowInstanceConfigurationRequest();
        request.setInstanceId(instanceId);
        ShowInstanceConfigurationResponse response = new ShowInstanceConfigurationResponse();
        try {
            response = client.showInstanceConfiguration(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return response;
    }

    private static void updateInstanceParamGroup(GaussDBforNoSQLClient client, String instanceId, Map<String, String> values) {
        UpdateInstanceConfigurationRequest request = new UpdateInstanceConfigurationRequest();
        UpdateInstanceConfigurationRequestBody body = new UpdateInstanceConfigurationRequestBody();
        body.setValues(values);
        request.setInstanceId(instanceId);
        request.withBody(body);
        try {
            UpdateInstanceConfigurationResponse response = client.updateInstanceConfiguration(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}
```

## 6. Sample Return Values

* Return value of the API (showInstanceConfiguration) for querying a parameter group

```
{
    "datastore_version_name":"3.11",
    "datastore_name":"cassandra",
    "created":"2022-06-22T08:07:59+0000",
    "updated":"2022-08-02T03:33:11+0000",
    "configuration_parameters":[
        {
            "name":"authenticator",
            "value":"PasswordAuthenticator",
            "restart_required":true,
            "readonly":true,
            "value_range":"PasswordAuthenticator|AllowAllAuthenticator",
            "type":"string",
            "description":"Backend username verification."
        },
        {
            "name":"authorizer",
            "value":"CassandraAuthorizer",
            "restart_required":true,
            "readonly":true,
            "value_range":"AllowAllAuthorizer|CassandraAuthorizer",
            "type":"string",
            "description":"Backend user authorization verification."
        },
        {
            "name":"batch_size_fail_threshold_in_kb",
            "value":"5000",
            "restart_required":true,
            "readonly":true,
            "value_range":"1-10000",
            "type":"integer",
            "description":"When the size of a batch processing request exceeds the threshold, the request fails to be processed."
        },
        {
            "name":"batch_size_warn_threshold_in_kb",
            "value":"5",
            "restart_required":false,
            "readonly":false,
            "value_range":"1-1000",
            "type":"integer",
            "description":"When the size of batch processing requests exceeds the threshold, logs of the WARN level are printed."
        },
        {
            "name":"cas_contention_timeout_in_ms",
            "value":"1000",
            "restart_required":false,
            "readonly":false,
            "value_range":"100-100000",
            "type":"integer",
            "description":"Timeout duration for the coordinator to wait for CAS operations to complete."
        },
        {
            "name":"concurrent_alter_schema_forwards",
            "value":"8",
            "restart_required":true,
            "readonly":true,
            "value_range":"4-512",
            "type":"integer",
            "description":"Number of threads for concurrently executing the ALTER statement."
        },
        {
            "name":"concurrent_counter_writes",
            "value":"32",
            "restart_required":true,
            "readonly":true,
            "value_range":"4-512",
            "type":"integer",
            "description":"Number of concurrent write threads of the counter."
        },
        {
            "name":"concurrent_materialized_view_writes",
            "value":"32",
            "restart_required":true,
            "readonly":true,
            "value_range":"4-512",
            "type":"integer",
            "description":"Number of concurrent write threads for materialized views."
        },
        {
            "name":"concurrent_reads",
            "value":"32",
            "restart_required":true,
            "readonly":true,
            "value_range":"4-512",
            "type":"integer",
            "description":"Number of concurrent read threads. The default value depends on the CPU specifications."
        },
        {
            "name":"concurrent_writes",
            "value":"16",
            "restart_required":true,
            "readonly":true,
            "value_range":"4-512",
            "type":"integer",
            "description":"Number of concurrent write threads. The default value depends on the CPU specifications."
        },
        {
            "name":"counter_write_request_timeout_in_ms",
            "value":"5000",
            "restart_required":false,
            "readonly":false,
            "value_range":"100-100000",
            "type":"integer",
            "description":"Timeout duration for the coordinator to wait for counter write operations to complete."
        },
        {
            "name":"range_request_timeout_in_ms",
            "value":"10000",
            "restart_required":false,
            "readonly":false,
            "value_range":"100-100000",
            "type":"integer",
            "description":"Timeout duration for the coordinator to wait for range operations to complete."
        },
        {
            "name":"read_request_timeout_in_ms",
            "value":"5000",
            "restart_required":false,
            "readonly":false,
            "value_range":"100-100000",
            "type":"integer",
            "description":"Timeout duration for the coordinator to wait for read operations to complete."
        },
        {
            "name":"request_timeout_in_ms",
            "value":"10000",
            "restart_required":false,
            "readonly":false,
            "value_range":"100-100000",
            "type":"integer",
            "description":"Timeout duration for the coordinator to wait for the completion of other operations except read, write, range, counter write, CAS, and truncate."
        },
        {
            "name":"role_manager",
            "value":"CassandraRoleManager",
            "restart_required":true,
            "readonly":true,
            "value_range":"CassandraRoleManager",
            "type":"string",
            "description":"Inter-role authorization management."
        },
        {
            "name":"slow_query_log_timeout_in_ms",
            "value":"500",
            "restart_required":true,
            "readonly":false,
            "value_range":"1-60000",
            "type":"integer",
            "description":"Slow query time threshold, in milliseconds."
        },
        {
            "name":"tombstone_failure_threshold",
            "value":"100000",
            "restart_required":false,
            "readonly":false,
            "value_range":"10-10000000",
            "type":"integer",
            "description":"If the number of tombstones in a query result exceeds the threshold, the query fails."
        },
        {
            "name":"tombstone_warn_threshold",
            "value":"1000",
            "restart_required":false,
            "readonly":false,
            "value_range":"10-100000",
            "type":"integer",
            "description":"When the number of tombstones in a query result exceeds the threshold, logs of the WARN level are printed."
        },
        {
            "name":"truncate_request_timeout_in_ms",
            "value":"60000",
            "restart_required":false,
            "readonly":false,
            "value_range":"100-100000",
            "type":"integer",
            "description":"Timeout duration for the coordinator to wait for truncate operations to complete."
        },
        {
            "name":"unlogged_batch_across_partitions_warn_threshold",
            "value":"10",
            "restart_required":false,
            "readonly":false,
            "value_range":"1-10000",
            "type":"integer",
            "description":"When the number of partitions for batch processing exceeds the threshold, logs of the WARN level are printed."
        },
        {
            "name":"write_request_timeout_in_ms",
            "value":"2000",
            "restart_required":false,
            "readonly":false,
            "value_range":"100-100000",
            "type":"integer",
            "description":"Timeout duration for the coordinator to wait for write operations to complete."
        }
    ]
}
```

* Return value of the API (updateInstanceConfiguration) for modifying parameter groups that do not require a restart to
  apply changes

```
{
    jobId: f44ecd0f-1763-431c-8dd9-1fabb7e811e5
    restartRequired: false
}
```

## 7. References

For details,
see [Querying Instance Parameter Settings] (https://support.huaweicloud.com/intl/en-us/api-nosql/nosql_06_0007.html).
You can debug this API
in [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=GaussDBforNoSQL&api=ShowInstanceConfiguration)
.

For details,
see [Modifying Parameters of a Specified Instance] (https://support.huaweicloud.com/intl/en-us/api-nosql/nosql_06_0006.html)
. You can debug the API
in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/GaussDBforNoSQL/doc?api=UpdateInstanceConfiguration)
.

## Change History

|    Released On   | Issue|   Description  |
|:----------:| :------: | :----------: |
| 2022-10-25 |   1.0    | First official release|
