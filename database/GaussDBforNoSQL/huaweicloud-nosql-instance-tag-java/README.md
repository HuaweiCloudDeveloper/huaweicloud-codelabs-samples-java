# 设置实例标签

## 版本说明

本示例配套的sdk版本为：3.1.5

## 1. 介绍

云数据库 GeminiDB NoSQL是一款基于计算存储分离架构的分布式多模NoSQL数据库服务，在云计算平台高性能、高可用、高可靠、高安全、可弹性伸缩的基础上，提供了一键部署、备份恢复、监控报警等服务能力。云数据库 GeminiDB
NoSQL目前包含GaussDB(for Cassandra)、GeminiDB(for Mongo)、GeminiDB(for Influx)和GaussDB(for Redis)
四款产品，分别兼容Cassandra、MongoDB、InfluxDB和Redis主流NoSQL接口，并提供高读写性能，具有高性价比，适用于IoT、气象、互联网、游戏等领域。

## 2. 流程图

![修改实例标签的流程图](assets/instance-tag.png)

## 3. 前置条件

1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

## 4. SDK获取和安装

您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-gaussdbfornosql</artifactId>
    <version>3.1.5</version>
</dependency>
```

## 5. 关键代码片段

以下代码展示如何使用SDK修改实例标签：

```java
public class NoSqlInstanceTagDemo {
    private static final Logger logger = LoggerFactory.getLogger(NoSqlInstanceTagDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     *
     * 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
     * 本示例以ak和sk保存在环境变量中为例，运行本示例前请先在本地环境变量中设置环境变量 HUAWEICLOUD_SDK_AK 和 HUAWEICLOUD_SDK_SK
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 4) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        GaussDBforNoSQLClient client = GaussDBforNoSQLClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .build();

        // 获取已有实例
        getInstanceList(client);

        // 查询实例标签
        getInstanceTags(client, instanceId);

        // 设置实例标签
        setInstanceTags(client, instanceId);

        // 根据标签查询实例
        getInstanceByResourceTags(client);
    }

    public static void getInstanceList(GaussDBforNoSQLClient client) {
        ListInstancesRequest request = new ListInstancesRequest();
        try {
            ListInstancesResponse response = client.listInstances(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    public static void getInstanceTags(GaussDBforNoSQLClient client, String instanceId) {
        ListInstanceTagsRequest request = new ListInstanceTagsRequest();
        request.setInstanceId(instanceId);
        try {
            ListInstanceTagsResponse response = client.listInstanceTags(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    public static void setInstanceTags(GaussDBforNoSQLClient client, String instanceId) {
        BatchTagActionRequest request = new BatchTagActionRequest();
        request.setInstanceId(instanceId);
        BatchTagActionRequestBody body = new BatchTagActionRequestBody();
        BatchTagActionTagOption tagsItem = new BatchTagActionTagOption();
        tagsItem.withKey("test_key_new").withValue("test_value_new");
        body.withAction(BatchTagActionRequestBody.ActionEnum.CREATE);
        body.withTags(Arrays.asList(tagsItem));
        request.setBody(body);
        try {
            BatchTagActionResponse response = client.batchTagAction(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    public static void getInstanceByResourceTags(GaussDBforNoSQLClient client) {
        ListInstancesByResourceTagsRequest request = new ListInstancesByResourceTagsRequest();
        ListInstancesByTagsRequestBody body = new ListInstancesByTagsRequestBody();
        TagOption tagOption = new TagOption();
        tagOption.withKey("test_key_new").withValues(Arrays.asList("test_value_new"));
        body.setTags(Arrays.asList(tagOption));
        body.setAction(ListInstancesByTagsRequestBody.ActionEnum.FILTER);
        request.setBody(body);
        try {
            ListInstancesByResourceTagsResponse response = client.listInstancesByResourceTags(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}

```

## 6. 返回结果示例

- 查询实例列表和详情（ListInstances）接口的返回值：

```
class ListInstancesResponse {
    instances: [class ListInstancesResult {
        id: 8144ff47736b45ed9c4232bb5247feb2in06
        name: nosql-f8a7
        status: normal
        port: 8635
        mode: Cluster
        region: cn-north-7
        datastore: class ListInstancesDatastoreResult {
            type: cassandra
            version: 3.11
            patchAvailable: false
        }
        engine: rocksDB
        created: 2022-10-24T11:05:59
        updated: 2022-10-24T11:05:59
        dbUserName: rwuser
        vpcId: 69fd9e54-013d-43ee-9a42-2ed68a4b4bac
        subnetId: 645d9721-d785-4c56-a8f1-4c2f07fbf322
        securityGroupId: 9eee68d3-cacd-49ad-bd57-064d7d69ad9f
        backupStrategy: class ListInstancesBackupStrategyResult {
            startTime: 17:00-18:00
            keepDays: 7
        }
        payMode: 0
        maintenanceWindow: 02:00-06:00
        groups: [class ListInstancesGroupResult {
            id: 9ec989dade8e40578ba937aaa8e91ea7gr06
            status: normal
            volume: class Volume {
                size: 500
                used: 0.003
            }
            nodes: [class ListInstancesNodeResult {
                id: 092d6a8117cc4f4ea4bf31a7241f735cno06
                name: nosql-f8a7_kypp_priam_node_3
                status: normal
                role: null
                subnetId: 645d9721-d785-4c56-a8f1-4c2f07fbf322
                privateIp: 192.168.0.90
                publicIp: null
                specCode: geminidb.cassandra.xlarge.arm.4
                availabilityZone: cn-north-7c
                supportReduce: false
            }, class ListInstancesNodeResult {
                id: 1e358500565e42fa959bf2e13d6c09a1no06
                name: nosql-f8a7_kypp_priam_node_1
                status: normal
                role: null
                subnetId: 645d9721-d785-4c56-a8f1-4c2f07fbf322
                privateIp: 192.168.0.156
                publicIp: null
                specCode: geminidb.cassandra.xlarge.arm.4
                availabilityZone: cn-north-7c
                supportReduce: false
            }, class ListInstancesNodeResult {
                id: fe9d252d4d704f2e86bf92abd24017a0no06
                name: nosql-f8a7_kypp_priam_node_2
                status: normal
                role: null
                subnetId: 645d9721-d785-4c56-a8f1-4c2f07fbf322
                privateIp: 192.168.0.32
                publicIp: null
                specCode: geminidb.cassandra.xlarge.arm.4
                availabilityZone: cn-north-7c
                supportReduce: false
            }]
        }]
        enterpriseProjectId: 0
        dedicatedResourceId: null
        timeZone: 
        actions: [BACKUP]
        lbIpAddress: null
        lbPort: null
    }, class ListInstancesResult {
        id: e958b0012f0d4a30836407617f8a9bc6in06
        name: nosql-3955
        status: createfail
        port: 8635
        mode: Cluster
        region: cn-north-7
        datastore: class ListInstancesDatastoreResult {
            type: cassandra
            version: 3.11
            patchAvailable: false
        }
        engine: rocksDB
        created: 2022-09-20T01:48:00
        updated: 2022-10-16T14:10:50
        dbUserName: rwuser
        vpcId: 69fd9e54-013d-43ee-9a42-2ed68a4b4bac
        subnetId: 645d9721-d785-4c56-a8f1-4c2f07fbf322
        securityGroupId: 9eee68d3-cacd-49ad-bd57-064d7d69ad9f
        backupStrategy: class ListInstancesBackupStrategyResult {
            startTime: 00:00-01:00
            keepDays: 0
        }
        payMode: 0
        maintenanceWindow: 02:00-06:00
        groups: [class ListInstancesGroupResult {
            id: d8c0edff6d1b44119b5418996c3bb944gr06
            status: createfail
            volume: class Volume {
                size: 100
                used: 0
            }
            nodes: [class ListInstancesNodeResult {
                id: 062104152aaf4c6e864f3bb7b3ddcb6ano06
                name: nosql-3955_j6x2_priam_node_3
                status: createfail
                role: null
                subnetId: 
                privateIp: 
                publicIp: null
                specCode: geminidb.cassandra.xlarge.arm.4
                availabilityZone: cn-north-7c
                supportReduce: false
            }, class ListInstancesNodeResult {
                id: 2e1844a4965d43d4a4b17279a2351dcbno06
                name: nosql-3955_j6x2_priam_node_2
                status: createfail
                role: null
                subnetId: 
                privateIp: 
                publicIp: null
                specCode: geminidb.cassandra.xlarge.arm.4
                availabilityZone: cn-north-7c
                supportReduce: false
            }, class ListInstancesNodeResult {
                id: 851d8b3242e84c12aa3e8c0752c8d703no06
                name: nosql-3955_j6x2_priam_node_1
                status: createfail
                role: null
                subnetId: 
                privateIp: 
                publicIp: null
                specCode: geminidb.cassandra.xlarge.arm.4
                availabilityZone: cn-north-7c
                supportReduce: false
            }]
        }]
        enterpriseProjectId: 0
        dedicatedResourceId: null
        timeZone: 
        actions: []
        lbIpAddress: null
        lbPort: null
    }, class ListInstancesResult {
        id: a3bfe5ec41c74f86b4444ece9544a20din06
        name: nosql-1c03
        status: createfail
        port: 8635
        mode: Cluster
        region: cn-north-7
        datastore: class ListInstancesDatastoreResult {
            type: cassandra
            version: 3.11
            patchAvailable: false
        }
        engine: rocksDB
        created: 2022-08-05T08:05:06
        updated: 2022-10-16T14:10:50
        dbUserName: rwuser
        vpcId: 69fd9e54-013d-43ee-9a42-2ed68a4b4bac
        subnetId: 645d9721-d785-4c56-a8f1-4c2f07fbf322
        securityGroupId: 9eee68d3-cacd-49ad-bd57-064d7d69ad9f
        backupStrategy: class ListInstancesBackupStrategyResult {
            startTime: 00:00-01:00
            keepDays: 0
        }
        payMode: 0
        maintenanceWindow: 02:00-06:00
        groups: [class ListInstancesGroupResult {
            id: 8b2d13d50b5f4a1d93abf981b2a0b4cbgr06
            status: createfail
            volume: class Volume {
                size: 100
                used: 0
            }
            nodes: [class ListInstancesNodeResult {
                id: 0d455b620ac34b6c954189324c2cfcefno06
                name: nosql-1c03_dbxt_priam_node_1
                status: createfail
                role: null
                subnetId: 645d9721-d785-4c56-a8f1-4c2f07fbf322
                privateIp: 192.168.0.242
                publicIp: null
                specCode: geminidb.cassandra.xlarge.arm.4
                availabilityZone: cn-north-7c
                supportReduce: false
            }, class ListInstancesNodeResult {
                id: 3568f1eb895d4ae1ac90dc516b30d410no06
                name: nosql-1c03_dbxt_priam_node_2
                status: createfail
                role: null
                subnetId: 645d9721-d785-4c56-a8f1-4c2f07fbf322
                privateIp: 192.168.0.135
                publicIp: null
                specCode: geminidb.cassandra.xlarge.arm.4
                availabilityZone: cn-north-7c
                supportReduce: false
            }, class ListInstancesNodeResult {
                id: 9179c406718d443d81bad43a3b930649no06
                name: nosql-1c03_dbxt_priam_node_3
                status: createfail
                role: null
                subnetId: 645d9721-d785-4c56-a8f1-4c2f07fbf322
                privateIp: 192.168.0.13
                publicIp: null
                specCode: geminidb.cassandra.xlarge.arm.4
                availabilityZone: cn-north-7c
                supportReduce: false
            }]
        }]
        enterpriseProjectId: 62bb0ce0-b91b-41f6-9950-dce001a0c202
        dedicatedResourceId: null
        timeZone: 
        actions: []
        lbIpAddress: null
        lbPort: null
    }]
    totalCount: 3
}
```

- 查询资源标签（ListInstanceTags）接口的返回值：

```
class ListInstanceTagsResponse {
    tags: [class ListInstanceTagsResult {
        key: test_key_1
        value: test_value_1
    }]
}
```

- 批量添加或删除资源标签（BatchTagAction）接口的返回值：

```
class BatchTagActionResponse {

}
```

- 查询资源实例（ListInstancesByResourceTags）接口的返回值：

```
class ListInstancesByResourceTagsResponse {
    instances: [class InstanceResult {
        instanceId: 8144ff47736b45ed9c4232bb5247feb2in06
        instanceName: nosql-f8a7
        tags: [class InstanceTagResult {
            key: test_key_1
            value: test_value_1
        }, class InstanceTagResult {
            key: test_key_new
            value: test_value_new
        }]
    }]
    totalCount: 1
}
```

## 7.参考链接

请见 [查询实例列表和详情](https://support.huaweicloud.com/intl/zh-cn/api-nosql/nosql_05_0016.html)
您可以在 [API Explorer](https://apiexplorer.developer.cn-north-7.ulanqab.huawei.com/apiexplorer/doc?product=GaussDBforNoSQL&api=ListInstances)
中直接运行调试该接口。

请见 [查询资源标签](https://support.huaweicloud.com/intl/zh-cn/api-nosql/nosql_05_0019.html)
您可以在 [API Explorer](https://apiexplorer.developer.cn-north-7.ulanqab.huawei.com/apiexplorer/doc?product=GaussDBforNoSQL&api=ListInstanceTags)
中直接运行调试该接口。

请见 [批量添加或删除资源标签](https://support.huaweicloud.com/intl/zh-cn/api-nosql/nosql_05_0018.html)
您可以在 [API Explorer](https://apiexplorer.developer.cn-north-7.ulanqab.huawei.com/apiexplorer/doc?product=GaussDBforNoSQL&api=BatchTagAction)
中直接运行调试该接口。

请见 [查询资源实例](https://support.huaweicloud.com/intl/zh-cn/api-nosql/nosql_05_0017.html)
您可以在 [API Explorer](https://apiexplorer.developer.cn-north-7.ulanqab.huawei.com/apiexplorer/doc?product=GaussDBforNoSQL&api=ListInstancesByResourceTags)
中直接运行调试该接口。

## 修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2022-10-24 |   1.0    | 文档首次发布 |
