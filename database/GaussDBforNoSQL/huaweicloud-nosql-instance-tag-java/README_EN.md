# Adding a Tag for an Instance

## Version Description

In this sample, the SDK version is 3.1.5.

## 1. Introduction

GeminiDB is a distributed, multi-model NoSQL database service with decoupled storage and compute. It is highly
available, secure, and scalable and can provide robust performance and service capabilities like quick deployment,
backup, restoration, monitoring, and alarm reporting. GeminiDB supports APIs for Cassandra, Mongo, Influx, and Redis.
They are compatible with mainstream NoSQL APIs of Cassandra, MongoDB, InfluxDB, and Redis, respectively, and provide
high read/write performance at low costs, making it well suited to IoT, meteorology, Internet, and gaming sectors.

## 2. Flowchart

![Modifying an instance tag](assets/instance-tag-en.png)

## 3. Prerequisites

1. You have created [a HUAWEI ID](https://id5.cloud.huawei.com/CAS/portal/userRegister/regbyphone.html?&lang=en-us) and
   completed [real-name authentication](https://auth.huaweicloud.com/authui/login.html?service=https%3A%2F%2Faccount.huaweicloud.com%2Fusercenter%2F%3Fregion%3Dcn-north-4%26cloud_route_state%3D%2Faccountindex%2Frealnameauth&locale=en-us#/login)
   .

2. You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3. You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
   view your AK/SK on the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details,
   see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html).

4. You have set up the development environment with Java JDK 1.8 or later.

## 4. Obtaining and Installing an SDK

You can obtain and install an SDK in Maven mode. Download and install Maven in your operating system (OS). After the
installation is complete, add the required dependencies to the **pom.xml** file of the Java project.

For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-gaussdbfornosql</artifactId>
    <version>3.1.5</version>
</dependency>
```

## 5. Key Code Snippets

The following code shows how to modify an instance tag using the SDK.

```java
public class NoSqlInstanceTagDemo {
    private static final Logger logger = LoggerFactory.getLogger(NoSqlInstanceTagDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     *
     * Hard-coded or plaintext AK and SK are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 4) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        GaussDBforNoSQLClient client = GaussDBforNoSQLClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .build();

        // Obtain an existing instance.
        getInstanceList(client);

        // Query instance tags.
        getInstanceTags(client, instanceId);

        // Add a tag for the instance.
        setInstanceTags(client, instanceId);

        // Query instances by tag.
        getInstanceByResourceTags(client);
    }

    public static void getInstanceList(GaussDBforNoSQLClient client) {
        ListInstancesRequest request = new ListInstancesRequest();
        try {
            ListInstancesResponse response = client.listInstances(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    public static void getInstanceTags(GaussDBforNoSQLClient client, String instanceId) {
        ListInstanceTagsRequest request = new ListInstanceTagsRequest();
        request.setInstanceId(instanceId);
        try {
            ListInstanceTagsResponse response = client.listInstanceTags(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    public static void setInstanceTags(GaussDBforNoSQLClient client, String instanceId) {
        BatchTagActionRequest request = new BatchTagActionRequest();
        request.setInstanceId(instanceId);
        BatchTagActionRequestBody body = new BatchTagActionRequestBody();
        BatchTagActionTagOption tagsItem = new BatchTagActionTagOption();
        tagsItem.withKey("test_key_new").withValue("test_value_new");
        body.withAction(BatchTagActionRequestBody.ActionEnum.CREATE);
        body.withTags(Arrays.asList(tagsItem));
        request.setBody(body);
        try {
            BatchTagActionResponse response = client.batchTagAction(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    public static void getInstanceByResourceTags(GaussDBforNoSQLClient client) {
        ListInstancesByResourceTagsRequest request = new ListInstancesByResourceTagsRequest();
        ListInstancesByTagsRequestBody body = new ListInstancesByTagsRequestBody();
        TagOption tagOption = new TagOption();
        tagOption.withKey("test_key_new").withValues(Arrays.asList("test_value_new"));
        body.setTags(Arrays.asList(tagOption));
        body.setAction(ListInstancesByTagsRequestBody.ActionEnum.FILTER);
        request.setBody(body);
        try {
            ListInstancesByResourceTagsResponse response = client.listInstancesByResourceTags(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}

```

## 6. Sample Return Values

- Return value of the API (ListInstances) for querying instances and details

```
class ListInstancesResponse {
    instances: [class ListInstancesResult {
        id: 8144ff47736b45ed9c4232bb5247feb2in06
        name: nosql-f8a7
        status: normal
        port: 8635
        mode: Cluster
        region: cn-north-7
        datastore: class ListInstancesDatastoreResult {
            type: cassandra
            version: 3.11
            patchAvailable: false
        }
        engine: rocksDB
        created: 2022-10-24T11:05:59
        updated: 2022-10-24T11:05:59
        dbUserName: rwuser
        vpcId: 69fd9e54-013d-43ee-9a42-2ed68a4b4bac
        subnetId: 645d9721-d785-4c56-a8f1-4c2f07fbf322
        securityGroupId: 9eee68d3-cacd-49ad-bd57-064d7d69ad9f
        backupStrategy: class ListInstancesBackupStrategyResult {
            startTime: 17:00-18:00
            keepDays: 7
        }
        payMode: 0
        maintenanceWindow: 02:00-06:00
        groups: [class ListInstancesGroupResult {
            id: 9ec989dade8e40578ba937aaa8e91ea7gr06
            status: normal
            volume: class Volume {
                size: 500
                used: 0.003
            }
            nodes: [class ListInstancesNodeResult {
                id: 092d6a8117cc4f4ea4bf31a7241f735cno06
                name: nosql-f8a7_kypp_priam_node_3
                status: normal
                role: null
                subnetId: 645d9721-d785-4c56-a8f1-4c2f07fbf322
                privateIp: 192.168.0.90
                publicIp: null
                specCode: geminidb.cassandra.xlarge.arm.4
                availabilityZone: cn-north-7c
                supportReduce: false
            }, class ListInstancesNodeResult {
                id: 1e358500565e42fa959bf2e13d6c09a1no06
                name: nosql-f8a7_kypp_priam_node_1
                status: normal
                role: null
                subnetId: 645d9721-d785-4c56-a8f1-4c2f07fbf322
                privateIp: 192.168.0.156
                publicIp: null
                specCode: geminidb.cassandra.xlarge.arm.4
                availabilityZone: cn-north-7c
                supportReduce: false
            }, class ListInstancesNodeResult {
                id: fe9d252d4d704f2e86bf92abd24017a0no06
                name: nosql-f8a7_kypp_priam_node_2
                status: normal
                role: null
                subnetId: 645d9721-d785-4c56-a8f1-4c2f07fbf322
                privateIp: 192.168.0.32
                publicIp: null
                specCode: geminidb.cassandra.xlarge.arm.4
                availabilityZone: cn-north-7c
                supportReduce: false
            }]
        }]
        enterpriseProjectId: 0
        dedicatedResourceId: null
        timeZone: 
        actions: [BACKUP]
        lbIpAddress: null
        lbPort: null
    }, class ListInstancesResult {
        id: e958b0012f0d4a30836407617f8a9bc6in06
        name: nosql-3955
        status: createfail
        port: 8635
        mode: Cluster
        region: cn-north-7
        datastore: class ListInstancesDatastoreResult {
            type: cassandra
            version: 3.11
            patchAvailable: false
        }
        engine: rocksDB
        created: 2022-09-20T01:48:00
        updated: 2022-10-16T14:10:50
        dbUserName: rwuser
        vpcId: 69fd9e54-013d-43ee-9a42-2ed68a4b4bac
        subnetId: 645d9721-d785-4c56-a8f1-4c2f07fbf322
        securityGroupId: 9eee68d3-cacd-49ad-bd57-064d7d69ad9f
        backupStrategy: class ListInstancesBackupStrategyResult {
            startTime: 00:00-01:00
            keepDays: 0
        }
        payMode: 0
        maintenanceWindow: 02:00-06:00
        groups: [class ListInstancesGroupResult {
            id: d8c0edff6d1b44119b5418996c3bb944gr06
            status: createfail
            volume: class Volume {
                size: 100
                used: 0
            }
            nodes: [class ListInstancesNodeResult {
                id: 062104152aaf4c6e864f3bb7b3ddcb6ano06
                name: nosql-3955_j6x2_priam_node_3
                status: createfail
                role: null
                subnetId: 
                privateIp: 
                publicIp: null
                specCode: geminidb.cassandra.xlarge.arm.4
                availabilityZone: cn-north-7c
                supportReduce: false
            }, class ListInstancesNodeResult {
                id: 2e1844a4965d43d4a4b17279a2351dcbno06
                name: nosql-3955_j6x2_priam_node_2
                status: createfail
                role: null
                subnetId: 
                privateIp: 
                publicIp: null
                specCode: geminidb.cassandra.xlarge.arm.4
                availabilityZone: cn-north-7c
                supportReduce: false
            }, class ListInstancesNodeResult {
                id: 851d8b3242e84c12aa3e8c0752c8d703no06
                name: nosql-3955_j6x2_priam_node_1
                status: createfail
                role: null
                subnetId: 
                privateIp: 
                publicIp: null
                specCode: geminidb.cassandra.xlarge.arm.4
                availabilityZone: cn-north-7c
                supportReduce: false
            }]
        }]
        enterpriseProjectId: 0
        dedicatedResourceId: null
        timeZone: 
        actions: []
        lbIpAddress: null
        lbPort: null
    }, class ListInstancesResult {
        id: a3bfe5ec41c74f86b4444ece9544a20din06
        name: nosql-1c03
        status: createfail
        port: 8635
        mode: Cluster
        region: cn-north-7
        datastore: class ListInstancesDatastoreResult {
            type: cassandra
            version: 3.11
            patchAvailable: false
        }
        engine: rocksDB
        created: 2022-08-05T08:05:06
        updated: 2022-10-16T14:10:50
        dbUserName: rwuser
        vpcId: 69fd9e54-013d-43ee-9a42-2ed68a4b4bac
        subnetId: 645d9721-d785-4c56-a8f1-4c2f07fbf322
        securityGroupId: 9eee68d3-cacd-49ad-bd57-064d7d69ad9f
        backupStrategy: class ListInstancesBackupStrategyResult {
            startTime: 00:00-01:00
            keepDays: 0
        }
        payMode: 0
        maintenanceWindow: 02:00-06:00
        groups: [class ListInstancesGroupResult {
            id: 8b2d13d50b5f4a1d93abf981b2a0b4cbgr06
            status: createfail
            volume: class Volume {
                size: 100
                used: 0
            }
            nodes: [class ListInstancesNodeResult {
                id: 0d455b620ac34b6c954189324c2cfcefno06
                name: nosql-1c03_dbxt_priam_node_1
                status: createfail
                role: null
                subnetId: 645d9721-d785-4c56-a8f1-4c2f07fbf322
                privateIp: 192.168.0.242
                publicIp: null
                specCode: geminidb.cassandra.xlarge.arm.4
                availabilityZone: cn-north-7c
                supportReduce: false
            }, class ListInstancesNodeResult {
                id: 3568f1eb895d4ae1ac90dc516b30d410no06
                name: nosql-1c03_dbxt_priam_node_2
                status: createfail
                role: null
                subnetId: 645d9721-d785-4c56-a8f1-4c2f07fbf322
                privateIp: 192.168.0.135
                publicIp: null
                specCode: geminidb.cassandra.xlarge.arm.4
                availabilityZone: cn-north-7c
                supportReduce: false
            }, class ListInstancesNodeResult {
                id: 9179c406718d443d81bad43a3b930649no06
                name: nosql-1c03_dbxt_priam_node_3
                status: createfail
                role: null
                subnetId: 645d9721-d785-4c56-a8f1-4c2f07fbf322
                privateIp: 192.168.0.13
                publicIp: null
                specCode: geminidb.cassandra.xlarge.arm.4
                availabilityZone: cn-north-7c
                supportReduce: false
            }]
        }]
        enterpriseProjectId: 62bb0ce0-b91b-41f6-9950-dce001a0c202
        dedicatedResourceId: null
        timeZone: 
        actions: []
        lbIpAddress: null
        lbPort: null
    }]
    totalCount: 3
}
```

- Return value of the API (ListInstanceTags) for querying instance tags

```
class ListInstanceTagsResponse {
    tags: [class ListInstanceTagsResult {
        key: test_key_1
        value: test_value_1
    }]
}
```

- Return value of the API (BatchTagAction) for adding or deleting instance tags in batches

```
class BatchTagActionResponse {

}
```

- Return value of the API (ListInstancesByResourceTags) for querying instances

```
class ListInstancesByResourceTagsResponse {
    instances: [class InstanceResult {
        instanceId: 8144ff47736b45ed9c4232bb5247feb2in06
        instanceName: nosql-f8a7
        tags: [class InstanceTagResult {
            key: test_key_1
            value: test_value_1
        }, class InstanceTagResult {
            key: test_key_new
            value: test_value_new
        }]
    }]
    totalCount: 1
}
```

## 7. References

For details,
see [Querying Instances and Details](https://support.huaweicloud.com/intl/en-us/api-nosql/nosql_05_0016.html). You can
directly run and debug the API
in [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/GaussDBforNoSQL/doc?api=ListInstances).

For details,
see [Querying Tags of an Instance] (https://support.huaweicloud.com/intl/en-us/api-nosql/ListInstanceTags.html). You can
directly run and debug the API
in [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/GaussDBforNoSQL/doc?api=ListInstanceTags).

For details,
see [Adding or Deleting Resource Tags in Batches] (https://support.huaweicloud.com/intl/en-us/api-nosql/BatchTagAction.html)
. You can directly run and debug the API
in [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/GaussDBforNoSQL/doc?api=BatchTagAction).

For details,
see [Querying an Instance by Tag] (https://support.huaweicloud.com/intl/en-us/api-nosql/ListInstancesByResourceTags.html)
. You can directly run and debug the API
in [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/GaussDBforNoSQL/doc?api=ListInstancesByResourceTags)
.

## Change History

|    Released On   | Issue|   Description  |
|:----------:| :------: | :----------: |
| 2022-10-24 |   1.0    | First official release|
