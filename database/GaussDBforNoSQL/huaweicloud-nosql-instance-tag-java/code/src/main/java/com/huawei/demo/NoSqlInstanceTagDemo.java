package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.gaussdbfornosql.v3.GaussDBforNoSQLClient;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.BatchTagActionRequest;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.BatchTagActionRequestBody;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.BatchTagActionResponse;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.BatchTagActionTagOption;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.ListInstanceTagsRequest;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.ListInstanceTagsResponse;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.ListInstancesByResourceTagsRequest;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.ListInstancesByResourceTagsResponse;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.ListInstancesByTagsRequestBody;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.ListInstancesRequest;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.ListInstancesResponse;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.TagOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class NoSqlInstanceTagDemo {
    private static final Logger logger = LoggerFactory.getLogger(NoSqlInstanceTagDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     * <p>
     * Hard-coded or plaintext AK and SK are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 4) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        GaussDBforNoSQLClient client = GaussDBforNoSQLClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .build();

        // Obtain an existing instance.
        getInstanceList(client);

        // Query instance tags.
        getInstanceTags(client, instanceId);

        // Add a tag for the instance.
        setInstanceTags(client, instanceId);

        // Query instances by tag.
        getInstanceByResourceTags(client);
    }

    public static void getInstanceList(GaussDBforNoSQLClient client) {
        ListInstancesRequest request = new ListInstancesRequest();
        try {
            ListInstancesResponse response = client.listInstances(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    public static void getInstanceTags(GaussDBforNoSQLClient client, String instanceId) {
        ListInstanceTagsRequest request = new ListInstanceTagsRequest();
        request.setInstanceId(instanceId);
        try {
            ListInstanceTagsResponse response = client.listInstanceTags(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    public static void setInstanceTags(GaussDBforNoSQLClient client, String instanceId) {
        BatchTagActionRequest request = new BatchTagActionRequest();
        request.setInstanceId(instanceId);
        BatchTagActionRequestBody body = new BatchTagActionRequestBody();
        BatchTagActionTagOption tagsItem = new BatchTagActionTagOption();
        tagsItem.withKey("test_key_new").withValue("test_value_new");
        body.withAction(BatchTagActionRequestBody.ActionEnum.CREATE);
        body.withTags(Arrays.asList(tagsItem));
        request.setBody(body);
        try {
            BatchTagActionResponse response = client.batchTagAction(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    public static void getInstanceByResourceTags(GaussDBforNoSQLClient client) {
        ListInstancesByResourceTagsRequest request = new ListInstancesByResourceTagsRequest();
        ListInstancesByTagsRequestBody body = new ListInstancesByTagsRequestBody();
        TagOption tagOption = new TagOption();
        tagOption.withKey("test_key_new").withValues(Arrays.asList("test_value_new"));
        body.setTags(Arrays.asList(tagOption));
        body.setAction(ListInstancesByTagsRequestBody.ActionEnum.FILTER);
        request.setBody(body);
        try {
            ListInstancesByResourceTagsResponse response = client.listInstancesByResourceTags(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}