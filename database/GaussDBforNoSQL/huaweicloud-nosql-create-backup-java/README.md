# 创建手动备份

## 1. 简介

云数据库 GeminiDB NoSQL是一款基于计算存储分离架构的分布式多模NoSQL数据库服务，在云计算平台高性能、高可用、高可靠、高安全、可弹性伸缩的基础上，提供了一键部署、备份恢复、监控报警等服务能力。云数据库 GeminiDB
NoSQL目前包含GaussDB(for Cassandra)、GeminiDB(for Mongo)、GeminiDB(for Influx)和GaussDB(for Redis)
四款产品，分别兼容Cassandra、MongoDB、InfluxDB和Redis主流NoSQL接口，并提供高读写性能，具有高性价比，适用于IoT、气象、互联网、游戏等领域。此示例展示如何创建手动备份，并通过备份id查询备份列表详情。

## 2. 流程图

![创建手动备份](./assets/backup.png)

## 3. 前置条件

1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realnameauth) 。。

2.获取华为云开发工具包（sdk），您也可以查看安装java sdk。

3.已获取华为云账号对应的access key（ak）和secret access key（sk）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的ak/sk。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持java jdk 1.8及其以上版本。

## 4. SDK获取和安装

您可以通过maven方式获取和安装sdk，首先需要在您的操作系统中下载并安装maven ，安装完成后您只需要在java项目的pom.xml文件中加入相应的依赖项即可。

具体的sdk版本号请参见 [sdk开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-gaussdbfornosql</artifactId>
    <version>3.1.43</version>
</dependency>
```

## 5.关键代码片段

以下代码展示如何通过GaussDB NoSQL SDK创建手动备份

```java

public class NosqlCreateBackupDemo {
    private static final Logger logger = LoggerFactory.getLogger(NosqlCreateBackupDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     * args[4] = "<YOUR BACKUP_NAME>"
     * args[5] = "<YOUR BACKUP_DESCRIPTION>"
     *
     * 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
     * 本示例以ak和sk保存在环境变量中为例，运行本示例前请先在本地环境变量中设置环境变量 HUAWEICLOUD_SDK_AK 和 HUAWEICLOUD_SDK_SK
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 6) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];
        String name = args[4];
        String description = args[5];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        // 根据需要配置是否跳过SSL证书验证
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);
        GaussDBforNoSQLClient client = GaussDBforNoSQLClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .withHttpConfig(httpConfig)
                .build();
        String backupId = createNosqlBackup(client, instanceId, name, description);
        showAllInstancesBackups(client, backupId);
    }

    private static String createNosqlBackup(GaussDBforNoSQLClient client, String instanceId, String name, String description) {
        CreateBackRequest request = new CreateBackRequest();
        //设置实例id，备份名称，备份描述
        request.setInstanceId(instanceId);
        NoSqlCreateBackupRequestBody body = new NoSqlCreateBackupRequestBody();
        body.setName(name);
        body.setDescription(description);
        request.withBody(body);
        //获取手动备份id
        CreateBackResponse response = new CreateBackResponse();
        try {
            response = client.createBack(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return response.getBackupId();
    }

    private static void showAllInstancesBackups(GaussDBforNoSQLClient client, String backupId) {
        ShowAllInstancesBackupsNewRequest request = new ShowAllInstancesBackupsNewRequest();
        request.setBackupId(backupId);
        //查询备份列表 展示备份详情
        try {
            ShowAllInstancesBackupsNewResponse response = client.showAllInstancesBackupsNew(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}

```

## 6.返回结果示例

* 创建手动备份（CreateBack）接口返回值:

```
{
  job_id: fbdac261-1edc-4598-b5c9-77fea181a1e7
  backup_id: 8cf8cc8b9b9146998dab8b943fe0c38bbr06
}
```

* 查询备份列表（ShowAllInstancesBackups）接口返回值:

```
{
    totalCount: 1
    backups: [{
        id: 8cf8cc8b9b9146998dab8b943fe0c38bbr06
        description: description_123
        instanceId: fa2e6aa4412842a7bba6d3be813ae119in06
        instanceName: nosql-8299
        datastore: {
            type: cassandra
            version: 3.11.3.230430
        }
        name: name_123
        type: Manual
        size: 0.0
        status: BUILDING
        beginTime: 2023-07-31T08:42:05+0000
        endTime: 2023-07-31T08:42:05+0000
        databaseTables: []
    }]
}

```

## 7.参考链接

请见 [创建手动备份](https://support.huaweicloud.com/intl/zh-cn/api-nosql/zh-cn_topic_0000001409030149.html)
您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/GaussDBforNoSQL/doc?api=CreateBack)中直接运行调试该接口

## 修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2023-07-17 |   1.0    | 文档首次发布 |