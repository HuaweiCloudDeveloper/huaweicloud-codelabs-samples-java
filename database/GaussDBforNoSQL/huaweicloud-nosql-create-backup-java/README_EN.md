# Creating a Manual Backup

## 1. Introduction

GeminiDB is a distributed, multi-model NoSQL database service with decoupled storage and compute. It is highly
available, secure, and scalable and can provide robust performance and service capabilities like quick deployment,
backup, restoration, monitoring, and alarm reporting. GeminiDB supports APIs for Cassandra, Mongo, Influx, and Redis.
They are compatible with mainstream NoSQL APIs of Cassandra, MongoDB, InfluxDB, and Redis, respectively, and provide
high read/write performance at low costs, making it well suited to IoT, meteorology, Internet, and gaming sectors. This
example shows how to create a manual backup and query backup details using the backup ID.

## 2. Flowchart

![Creating a manual backup](./assets/backup-en.png)

## 3. Prerequisites

1. You have created [a HUAWEI ID](https://id5.cloud.huawei.com/CAS/portal/userRegister/regbyphone.html?&lang=en-us) and
   completed [real-name authentication](https://auth.huaweicloud.com/authui/login.html?service=https%3A%2F%2Faccount.huaweicloud.com%2Fusercenter%2F%3Fregion%3Dcn-north-4%26cloud_route_state%3D%2Faccountindex%2Frealnameauth&locale=en-us#/login)
   .

2. You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3. You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
   view your AK/SK on the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details,
   see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html).

4. You have set up the development environment with Java JDK 1.8 or later.

## 4. Obtaining and Installing an SDK

You can obtain and install an SDK in Maven mode. Download and install Maven in your operating system (OS). After the
installation is complete, add the required dependencies to the **pom.xml** file of the Java project.

For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-gaussdbfornosql</artifactId>
    <version>3.1.43</version>
</dependency>
```

## 5. Key Code Snippets

The following code shows how to create a manual backup using the GeminiDB SDK.

```java

public class NosqlCreateBackupDemo {
    private static final Logger logger = LoggerFactory.getLogger(NosqlCreateBackupDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     * args[4] = "<YOUR BACKUP_NAME>"
     * args[5] = "<YOUR BACKUP_DESCRIPTION>"
     *
     * Hard-coded or plaintext AK and SK are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 6) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];
        String name = args[4];
        String description = args[5];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        // Configure whether to skip SSL certificate verification as required.
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);
        GaussDBforNoSQLClient client = GaussDBforNoSQLClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .withHttpConfig(httpConfig)
                .build();
        String backupId = createNosqlBackup(client, instanceId, name, description);
        showAllInstancesBackups(client, backupId);
    }

    private static String createNosqlBackup(GaussDBforNoSQLClient client, String instanceId, String name, String description) {
        CreateBackRequest request = new CreateBackRequest();
        // Set the instance ID, backup name, and backup description.
        request.setInstanceId(instanceId);
        NoSqlCreateBackupRequestBody body = new NoSqlCreateBackupRequestBody();
        body.setName(name);
        body.setDescription(description);
        request.withBody(body);
        // Obtain the manual backup ID.
        CreateBackResponse response = new CreateBackResponse();
        try {
            response = client.createBack(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return response.getBackupId();
    }

    private static void showAllInstancesBackups(GaussDBforNoSQLClient client, String backupId) {
        ShowAllInstancesBackupsNewRequest request = new ShowAllInstancesBackupsNewRequest();
        request.setBackupId(backupId);
        // Query instance backups.
        try {
            ShowAllInstancesBackupsNewResponse response = client.showAllInstancesBackupsNew(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}

```

## 6. Sample Return Values

* Return value of the API (CreateBack) for creating a manual backup

```
{
  job_id: fbdac261-1edc-4598-b5c9-77fea181a1e7
  backup_id: 8cf8cc8b9b9146998dab8b943fe0c38bbr06
}
```

* Return value of the API (ShowAllInstancesBackups) for querying instance backups

```
{
    totalCount: 1
    backups: [{
        id: 8cf8cc8b9b9146998dab8b943fe0c38bbr06
        description: description_123
        instanceId: fa2e6aa4412842a7bba6d3be813ae119in06
        instanceName: nosql-8299
        datastore: {
            type: cassandra
            version: 3.11.3.230430
        }
        name: name_123
        type: Manual
        size: 0.0
        status: BUILDING
        beginTime: 2023-07-31T08:42:05+0000
        endTime: 2023-07-31T08:42:05+0000
        databaseTables: []
    }]
}

```

## 7. References

For details,
see [Creating a Manual Backup] (https://support.huaweicloud.com/intl/en-us/api-nosql/en-us_topic_0000001409030149.html).
You can directly run and debug the API
in [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/GaussDBforNoSQL/doc?api=CreateBack).

## Change History

|    Released On   | Issue|   Description  |
|:----------:| :------: | :----------: |
| 2023-07-17 |   1.0    | First official release|
