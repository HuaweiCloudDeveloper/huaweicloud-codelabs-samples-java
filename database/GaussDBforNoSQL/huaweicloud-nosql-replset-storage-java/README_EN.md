# Scaling Up the Storage Space of a Replica Set Instance

## Version Description

In this sample, the SDK version is 3.1.5.

## 1. Introduction

GeminiDB is a distributed, multi-model NoSQL database service with decoupled storage and compute. It is highly
available, secure, and scalable and can provide robust performance and service capabilities like quick deployment,
backup, restoration, monitoring, and alarm reporting. GeminiDB supports APIs for Cassandra, Mongo, Influx, and Redis.
They are compatible with mainstream NoSQL APIs of Cassandra, MongoDB, InfluxDB, and Redis, respectively, and provide
high read/write performance at low costs, making it well suited to IoT, meteorology, Internet, and gaming sectors.

## 2. Flowchart

![Scaling up the storage space of a replica set instance](assets/replset-en.png)

## 3. Prerequisites

1. You have created [a HUAWEI ID](https://id5.cloud.huawei.com/CAS/portal/userRegister/regbyphone.html?&lang=en-us) and
   completed [real-name authentication](https://auth.huaweicloud.com/authui/login.html?service=https%3A%2F%2Faccount.huaweicloud.com%2Fusercenter%2F%3Fregion%3Dcn-north-4%26cloud_route_state%3D%2Faccountindex%2Frealnameauth&locale=en-us#/login)
   .

2. You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3. You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
   view your AK/SK on the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details,
   see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html).

4. You have set up the development environment with Java JDK 1.8 or later.

## 4. Obtaining and Installing an SDK

You can obtain and install an SDK in Maven mode. Download and install Maven in your operating system (OS). After the
installation is complete, add the required dependencies to the **pom.xml** file of the Java project.

For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-gaussdbfornosql</artifactId>
    <version>3.1.5</version>
</dependency>
```

## 5. Key Code Snippets

The following code shows how to scale up the storage space of a replica set instance using the GeminiDB SDK.

```java
public class ReplicaSetEnlargeVolumeDemo {

    private static final Logger logger = LoggerFactory.getLogger(ReplicaSetEnlargeVolumeDemo.class.getName());

    private static final String RESIZE_VOLUME_ACTION = "RESIZE_VOLUME";

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     *
     * Hard-coded or plaintext AK and SK are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     *
     * @param args
     */
    public static void main(String[] args) throws InterruptedException {
        if (args.length != 4) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        GaussDBforNoSQLClient client = GaussDBforNoSQLClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .build();

        // View the storage space of the replica set instance.
        // Obtain instance details by ID using the API (ListInstances).
        ListInstancesResponse currentResponse = showGaussDBforNoSQLReplicaSetDetail(client, instanceId);

        // currentSize: Obtain the storage space of the instance on the instance details page.
        String currentSize = currentResponse.getInstances().get(0).getGroups().get(0).getVolume().getSize();
        logger.info("Replicaset storage space is {}", currentSize);

        // Scae up the storage space of a replica set instance.
        // targetSize: Target storage space. The value must be an integer multiple of 10 and greater than the current space. In this example, the actual storage space is the sum of current storage space plus 10 GB.
        int targetSize = Integer.valueOf(currentSize) + 10;
        enlargeGaussDBforNoSQLReplicaSetVolume(client, instanceId, targetSize);

        // Wait until the scaling is complete. In the example, the sleep mode is set to 1 minute to avoid too many requests being sent.
        while (!completeEnlargeVolume(client, instanceId)) {
            Thread.sleep(60000L);
        }

        // Call the API for querying instance details by ID to query the target storage space after the scaling is successful.
        ListInstancesResponse targetResponse = showGaussDBforNoSQLReplicaSetDetail(client, instanceId);
        String expansionSize = targetResponse.getInstances().get(0).getGroups().get(0).getVolume().getSize();
        logger.info("The  Replicaset storage space after enlarge volume is {}", expansionSize);
    }

    private static ListInstancesResponse showGaussDBforNoSQLReplicaSetDetail(GaussDBforNoSQLClient client, String instanceId) {
        ListInstancesRequest request = new ListInstancesRequest();
        request.withId(instanceId);
        ListInstancesResponse response = new ListInstancesResponse();
        try {
            response = client.listInstances(request);
            logger.info("show instance detail response:{}", response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return response;
    }

    private static boolean completeEnlargeVolume(GaussDBforNoSQLClient client, String instanceId) {
        // The actions field of the API for querying instances and details does not contain **RESIZE_VOLUME action**.
        ListInstancesRequest request = new ListInstancesRequest();
        request.withId(instanceId);
        try {
            ListInstancesResponse rsp = client.listInstances(request);
            if (!rsp.getInstances().get(0).getActions().contains(RESIZE_VOLUME_ACTION)) {
                return true;
            }
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return false;
    }

    private static String enlargeGaussDBforNoSQLReplicaSetVolume(GaussDBforNoSQLClient client, String instanceId, int targetSize) {
        ResizeInstanceVolumeRequest request = new ResizeInstanceVolumeRequest();
        ResizeInstanceVolumeRequestBody body = new ResizeInstanceVolumeRequestBody();
        body.setSize(targetSize);
        request.withBody(body);
        request.setInstanceId(instanceId);
        ResizeInstanceVolumeResponse response = new ResizeInstanceVolumeResponse();
        try {
            response = client.resizeInstanceVolume(request);
            logger.info("enlarge volume size rsp:{}", response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return response.getJobId();
    }
}
```

## 6. Sample Return Values

* Return value of the API (ListInstances) for querying instance details

```
{
    "instances": [
        {
            "id": "fea387764a9e43b48481ba801580baf1in10",
            "name": "nosql-0101",
            "status": "normal",
            "port": "8635",
            "mode": "ReplicaSet",
            "region": "xxxx",
            "datastore": {
                "type": "mongodb",
                "version": "4.0",
                "patch_available": false
            },
            "engine": "rocksDB",
            "created": "2022-10-20T07:01:10",
            "updated": "2022-10-25T02:57:31",
            "db_user_name": "rwuser",
            "vpc_id": "e1791c42-34d8-4248-b862-32c41fb25e7c",
            "subnet_id": "f376b6e6-af3d-40f7-b7ad-90f3437a98cc",
            "security_group_id": "646cc6c0-91c7-4679-b4f2-1e84778e55f7",
            "backup_strategy": {
                "start_time": "16:00-17:00",
                "keep_days": 7
            },
            "pay_mode": "0",
            "maintenance_window": "02:00-06:00",
            "groups": [
                {
                    "id": "612971d3e4544e99ae3f0386fcf76634gr10",
                    "status": "normal",
                    "volume": {
                        "size": "100",
                        "used": "0.727"
                    },
                    "nodes": [
                        {
                            "id": "7dc77e5c57c84cbeb7a24095ebd04322no10",
                            "name": "nosql-0101_shos_replica_node_1",
                            "status": "normal",
                            "role": "Secondary",
                            "subnet_id": "f376b6e6-af3d-40f7-b7ad-90f3437a98cc",
                            "private_ip": "192.168.19.213",
                            "spec_code": "geminidb.mongodb.repset.medium.4",
                            "availability_zone": "xxx",
                            "support_reduce": false
                        },
                        {
                            "id": "c5d6757af84a4f26a281901f1ba04df9no10",
                            "name": "nosql-0101_shos_replica_node_2",
                            "status": "normal",
                            "role": "Secondary",
                            "subnet_id": "f376b6e6-af3d-40f7-b7ad-90f3437a98cc",
                            "private_ip": "192.168.24.179",
                            "spec_code": "geminidb.mongodb.repset.medium.4",
                            "availability_zone": "xxx",
                            "support_reduce": false
                        },
                        {
                            "id": "feff21a67093436c9b0857fa425906cbno10",
                            "name": "nosql-0101_shos_replica_node_3",
                            "status": "normal",
                            "role": "Primary",
                            "subnet_id": "f376b6e6-af3d-40f7-b7ad-90f3437a98cc",
                            "private_ip": "192.168.26.48",
                            "spec_code": "geminidb.mongodb.repset.medium.4",
                            "availability_zone": "xxx",
                            "support_reduce": false
                        }
                    ]
                }
            ],
            "enterprise_project_id": "0",
            "time_zone": "",
            "actions": []
        }
    ],
    "total_count": 1
}
```

* Return value of the API (ResizeInstanceVolume) for scaling up the storage space of a replica set instance

```
{
    jobId: f44ecf0f-1763-431c-8dd9-1fabb7e811e5
}
```

## 7. References

For details,
see [Querying Instances and Details](https://support.huaweicloud.com/intl/en-us/api-nosql/nosql_05_0016.html). You can
directly run and debug the API
in [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=GaussDBforNoSQL&api=ListInstances)
.

You can directly run and debug the API
in [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/GaussDBforNoSQL/doc?api=UpdateInstanceConfiguration)
.

## Change History

|    Released On   | Issue|   Description  |
|:----------:| :------: | :----------: |
| 2022-10-25 |   1.0    | First official release|
