package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.gaussdbfornosql.v3.GaussDBforNoSQLClient;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.ListInstancesRequest;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.ListInstancesResponse;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.ResizeInstanceVolumeRequest;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.ResizeInstanceVolumeRequestBody;
import com.huaweicloud.sdk.gaussdbfornosql.v3.model.ResizeInstanceVolumeResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReplicaSetEnlargeVolumeDemo {

    private static final Logger logger = LoggerFactory.getLogger(ReplicaSetEnlargeVolumeDemo.class.getName());

    private static final String RESIZE_VOLUME_ACTION = "RESIZE_VOLUME";

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     * <p>
     * Hard-coded or plaintext AK and SK are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     *
     * @param args
     */
    public static void main(String[] args) throws InterruptedException {
        if (args.length != 4) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        GaussDBforNoSQLClient client = GaussDBforNoSQLClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .build();

        // View the storage space of the replica set instance.
        // Obtain instance details by ID using the API (ListInstances).
        ListInstancesResponse currentResponse = showGaussDBforNoSQLReplicaSetDetail(client, instanceId);

        // currentSize: Obtain the storage space of the instance on the instance details page.
        String currentSize = currentResponse.getInstances().get(0).getGroups().get(0).getVolume().getSize();
        logger.info("Replicaset storage space is {}", currentSize);

        // Scae up the storage space of a replica set instance.
        // targetSize: Target storage space. The value must be an integer multiple of 10 and greater than the current space. In this example, the actual storage space is the sum of current storage space plus 10 GB.
        int targetSize = Integer.valueOf(currentSize) + 10;
        enlargeGaussDBforNoSQLReplicaSetVolume(client, instanceId, targetSize);

        // Wait until the scaling is complete. In the example, the sleep mode is set to 1 minute to avoid too many requests being sent.
        while (!completeEnlargeVolume(client, instanceId)) {
            Thread.sleep(60000L);
        }

        // Call the API for querying instance details by ID to query the target storage space after the scaling is successful.
        ListInstancesResponse targetResponse = showGaussDBforNoSQLReplicaSetDetail(client, instanceId);
        String expansionSize = targetResponse.getInstances().get(0).getGroups().get(0).getVolume().getSize();
        logger.info("The  Replicaset storage space after enlarge volume is {}", expansionSize);
    }

    private static ListInstancesResponse showGaussDBforNoSQLReplicaSetDetail(GaussDBforNoSQLClient client, String instanceId) {
        ListInstancesRequest request = new ListInstancesRequest();
        request.withId(instanceId);
        ListInstancesResponse response = new ListInstancesResponse();
        try {
            response = client.listInstances(request);
            logger.info("show instance detail response:{}", response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return response;
    }

    private static boolean completeEnlargeVolume(GaussDBforNoSQLClient client, String instanceId) {
        // The actions field of the API for querying instances and details does not contain **RESIZE_VOLUME action**.
        ListInstancesRequest request = new ListInstancesRequest();
        request.withId(instanceId);
        try {
            ListInstancesResponse rsp = client.listInstances(request);
            if (!rsp.getInstances().get(0).getActions().contains(RESIZE_VOLUME_ACTION)) {
                return true;
            }
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return false;
    }

    private static String enlargeGaussDBforNoSQLReplicaSetVolume(GaussDBforNoSQLClient client, String instanceId, int targetSize) {
        ResizeInstanceVolumeRequest request = new ResizeInstanceVolumeRequest();
        ResizeInstanceVolumeRequestBody body = new ResizeInstanceVolumeRequestBody();
        body.setSize(targetSize);
        request.withBody(body);
        request.setInstanceId(instanceId);
        ResizeInstanceVolumeResponse response = new ResizeInstanceVolumeResponse();
        try {
            response = client.resizeInstanceVolume(request);
            logger.info("enlarge volume size rsp:{}", response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return response.getJobId();
    }
}