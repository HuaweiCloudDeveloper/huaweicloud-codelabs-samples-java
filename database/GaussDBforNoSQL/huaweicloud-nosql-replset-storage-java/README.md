# 副本集实例扩容存储容量

## 版本说明

本示例配套的sdk版本为：3.1.5

## 1. 简介

云数据库 GeminiDB NoSQL是一款基于计算存储分离架构的分布式多模NoSQL数据库服务，在云计算平台高性能、高可用、高可靠、高安全、可弹性伸缩的基础上，提供了一键部署、备份恢复、监控报警等服务能力。云数据库 GeminiDB
NoSQL目前包含GaussDB(for Cassandra)、GeminiDB(for Mongo)、GeminiDB(for Influx)和GaussDB(for Redis)
四款产品，分别兼容Cassandra、MongoDB、InfluxDB和Redis主流NoSQL接口，并提供高读写性能，具有高性价比，适用于IoT、气象、互联网、游戏等领域。

## 2. 流程图

![副本集实例扩容存储容量的流程图](assets/replset.png)

## 3. 前置条件

1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realnameauth) 。。

2.获取华为云开发工具包（sdk），您也可以查看安装java sdk。

3.已获取华为云账号对应的access key（ak）和secret access key（sk）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的ak/sk。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持java jdk 1.8及其以上版本。

## 4. SDK获取和安装

您可以通过maven方式获取和安装sdk，首先需要在您的操作系统中下载并安装maven ，安装完成后您只需要在java项目的pom.xml文件中加入相应的依赖项即可。

具体的sdk版本号请参见 [sdk开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-gaussdbfornosql</artifactId>
    <version>3.1.5</version>
</dependency>
```

## 5.关键代码片段

以下代码展示如何通过GaussDB NoSQL SDK扩容副本集实例存储空间

```java
public class ReplicaSetEnlargeVolumeDemo {

    private static final Logger logger = LoggerFactory.getLogger(ReplicaSetEnlargeVolumeDemo.class.getName());

    private static final String RESIZE_VOLUME_ACTION = "RESIZE_VOLUME";

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR INSTANCE_ID>"
     * args[3] = "<YOUR REGION_ID>"
     *
     * 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
     * 本示例以ak和sk保存在环境变量中为例，运行本示例前请先在本地环境变量中设置环境变量 HUAWEICLOUD_SDK_AK 和 HUAWEICLOUD_SDK_SK
     *
     * @param args
     */
    public static void main(String[] args) throws InterruptedException {
        if (args.length != 4) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String instanceId = args[2];
        String regionId = args[3];

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        GaussDBforNoSQLClient client = GaussDBforNoSQLClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region(regionId, endpoint))
                .build();

        // 查看副本集实例存储空间
        // 通过实例ID调用ListInstances接口获取实例详情信息
        ListInstancesResponse currentResponse = showGaussDBforNoSQLReplicaSetDetail(client, instanceId);

        // currentSize: 在实例详情中获取实例当前存储空间
        String currentSize = currentResponse.getInstances().get(0).getGroups().get(0).getVolume().getSize();
        logger.info("Replicaset storage space is {}", currentSize);

        // 扩容副本集存储空间
        // targetSize:待扩容到的磁盘容量。取值为10的整数倍，并且大于当前磁盘容量,这里当前容量(currentSize)大小加上10为例
        int targetSize = Integer.valueOf(currentSize) + 10;
        enlargeGaussDBforNoSQLReplicaSetVolume(client, instanceId, targetSize);

        // 等待扩容完成,示例中为了避免过多发出请求，设置了1min睡眠。
        while (!completeEnlargeVolume(client, instanceId)) {
            Thread.sleep(60000L);
        }

        // 等待扩容成功后再通过调用实例ID详情接口查询扩容后实例存储空间
        ListInstancesResponse targetResponse = showGaussDBforNoSQLReplicaSetDetail(client, instanceId);
        String expansionSize = targetResponse.getInstances().get(0).getGroups().get(0).getVolume().getSize();
        logger.info("The  Replicaset storage space after enlarge volume is {}", expansionSize);
    }

    private static ListInstancesResponse showGaussDBforNoSQLReplicaSetDetail(GaussDBforNoSQLClient client, String instanceId) {
        ListInstancesRequest request = new ListInstancesRequest();
        request.withId(instanceId);
        ListInstancesResponse response = new ListInstancesResponse();
        try {
            response = client.listInstances(request);
            logger.info("show instance detail response:{}", response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return response;
    }

    private static boolean completeEnlargeVolume(GaussDBforNoSQLClient client, String instanceId) {
        // 通过查询实例详情与列表接口的actions字段中不包含RESIZE_VOLUME action
        ListInstancesRequest request = new ListInstancesRequest();
        request.withId(instanceId);
        try {
            ListInstancesResponse rsp = client.listInstances(request);
            if (!rsp.getInstances().get(0).getActions().contains(RESIZE_VOLUME_ACTION)) {
                return true;
            }
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return false;
    }

    private static String enlargeGaussDBforNoSQLReplicaSetVolume(GaussDBforNoSQLClient client, String instanceId, int targetSize) {
        ResizeInstanceVolumeRequest request = new ResizeInstanceVolumeRequest();
        ResizeInstanceVolumeRequestBody body = new ResizeInstanceVolumeRequestBody();
        body.setSize(targetSize);
        request.withBody(body);
        request.setInstanceId(instanceId);
        ResizeInstanceVolumeResponse response = new ResizeInstanceVolumeResponse();
        try {
            response = client.resizeInstanceVolume(request);
            logger.info("enlarge volume size rsp:{}", response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return response.getJobId();
    }
}
```

## 6.返回结果示例

* 查看实例详情详情（ListInstances）接口返回值:

```
{
    "instances": [
        {
            "id": "fea387764a9e43b48481ba801580baf1in10",
            "name": "nosql-0101",
            "status": "normal",
            "port": "8635",
            "mode": "ReplicaSet",
            "region": "xxxx",
            "datastore": {
                "type": "mongodb",
                "version": "4.0",
                "patch_available": false
            },
            "engine": "rocksDB",
            "created": "2022-10-20T07:01:10",
            "updated": "2022-10-25T02:57:31",
            "db_user_name": "rwuser",
            "vpc_id": "e1791c42-34d8-4248-b862-32c41fb25e7c",
            "subnet_id": "f376b6e6-af3d-40f7-b7ad-90f3437a98cc",
            "security_group_id": "646cc6c0-91c7-4679-b4f2-1e84778e55f7",
            "backup_strategy": {
                "start_time": "16:00-17:00",
                "keep_days": 7
            },
            "pay_mode": "0",
            "maintenance_window": "02:00-06:00",
            "groups": [
                {
                    "id": "612971d3e4544e99ae3f0386fcf76634gr10",
                    "status": "normal",
                    "volume": {
                        "size": "100",
                        "used": "0.727"
                    },
                    "nodes": [
                        {
                            "id": "7dc77e5c57c84cbeb7a24095ebd04322no10",
                            "name": "nosql-0101_shos_replica_node_1",
                            "status": "normal",
                            "role": "Secondary",
                            "subnet_id": "f376b6e6-af3d-40f7-b7ad-90f3437a98cc",
                            "private_ip": "192.168.19.213",
                            "spec_code": "geminidb.mongodb.repset.medium.4",
                            "availability_zone": "xxx",
                            "support_reduce": false
                        },
                        {
                            "id": "c5d6757af84a4f26a281901f1ba04df9no10",
                            "name": "nosql-0101_shos_replica_node_2",
                            "status": "normal",
                            "role": "Secondary",
                            "subnet_id": "f376b6e6-af3d-40f7-b7ad-90f3437a98cc",
                            "private_ip": "192.168.24.179",
                            "spec_code": "geminidb.mongodb.repset.medium.4",
                            "availability_zone": "xxx",
                            "support_reduce": false
                        },
                        {
                            "id": "feff21a67093436c9b0857fa425906cbno10",
                            "name": "nosql-0101_shos_replica_node_3",
                            "status": "normal",
                            "role": "Primary",
                            "subnet_id": "f376b6e6-af3d-40f7-b7ad-90f3437a98cc",
                            "private_ip": "192.168.26.48",
                            "spec_code": "geminidb.mongodb.repset.medium.4",
                            "availability_zone": "xxx",
                            "support_reduce": false
                        }
                    ]
                }
            ],
            "enterprise_project_id": "0",
            "time_zone": "",
            "actions": []
        }
    ],
    "total_count": 1
}
```

* 扩容副本集存储空间 （ResizeInstanceVolume）接口返回值:

```
{
    jobId: f44ecf0f-1763-431c-8dd9-1fabb7e811e5
}
```

## 7.参考链接

请见 [获取指定实例详情](https://support.huaweicloud.com/intl/zh-cn/api-nosql/nosql_05_0016.html)
您可以在 [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=GaussDBforNoSQL&api=ListInstances)
中直接运行调试该接口。

请见 [修改指定实例的参数](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=GaussDBforNoSQL&api=ResizeInstanceVolume)
中直接运行调试该接口。

## 修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2022-10-25 |   1.0    | 文档首次发布 |


