## 1. Introduction
TaurusDB is a MySQL-compatible, enterprise-grade distributed database. Data functions virtualization (DFV) is used to decouple storage from compute and can auto scale up to 128 TB per instance. With TaurusDB, there is no need for sharding, and no need to worry about data loss. It provides superior performance of commercial databases at the price of open-source databases. This example shows how to query and create database users using a Java SDK.

## 2. Flowchart
![Creating a database user](assets/databaseuser_en.png)

## 3. Prerequisites

1. You have created [a HUAWEI ID](https://id5.cloud.huawei.com/CAS/portal/userRegister/regbyphone.html?&lang=en-us) and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?locale=en-us#/accountindex/realNameAuth).

2. You have obtained a Huawei Cloud SDK. You can also install a Java SDK.

3. You have obtained the access key ID (AK) and secret access key (SK) of your HUAWEI ID. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html).

4. You have set up the development environment with Java JDK 1.8 or later.


## 4. Obtaining and Installing an SDK
You can obtain and install an SDK in Maven mode. Download and install Maven in your operating system (OS). After the installation is complete, add the required dependencies to the **pom.xml** file of the Java project.

For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter).
```xml
 <dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-gaussdb</artifactId>
    <version>3.1.1</version>
</dependency>
```

## 5. Key Code Snippet
Query and create database users using the SDK.

```java
public class DatabaseUserDemo {
    
    private static final Logger logger = LoggerFactory.getLogger(DatabaseUserDemo.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security purposes, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = "<YOUR INSTANCE_ID>";
        String regionId = "<YOUR REGION_ID>";

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        GaussDBClient client = GaussDBClient.newBuilder()
                .withCredential(auth)
                .withRegion(GaussDBRegion.valueOf(regionId))
                .build();

        // Queries database users.
        listDatabaseUser(client, instanceId);

        // Configures database username and password.
        String username = "<Your USER_NAME>";
        String password = "<YOUR PASSWORD>";
        // Creates a database user.
        createDatabaseUser(client, instanceId, username, password);

        // Queries database users.
        listDatabaseUser(client, instanceId);
    }

    private static void listDatabaseUser(GaussDBClient client, String instanceId) {
        ListGaussMySqlDatabaseUserRequest request = new ListGaussMySqlDatabaseUserRequest();
        request.withInstanceId(instanceId);
        try {
            ListGaussMySqlDatabaseUserResponse response = client.listGaussMySqlDatabaseUser(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void createDatabaseUser(GaussDBClient client, String instanceId, String username, String password) {
        CreateGaussMySqlDatabaseUserRequest request = new CreateGaussMySqlDatabaseUserRequest();
        CreateDatabaseUserRequest body = new CreateDatabaseUserRequest();
        CreateDatabaseUserList users = new CreateDatabaseUserList().
                withName(username).
                withPassword(password);
        body.withUsers(Collections.singletonList(users));
        request.withInstanceId(instanceId);
        request.withBody(body);
        try {
            CreateGaussMySqlDatabaseUserResponse response = client.createGaussMySqlDatabaseUser(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

}


```
## 6. Sample Return Values
- Return value of the API (ListGaussMySqlDatabaseUser) for querying database users
```
class ListGaussMySqlDatabaseUserResponse {
    users: [class ListGaussMySqlDatabaseUser {
        name: user_test_1
        host: 127.0.0.1
        databases: []
    }]
    totalCount: 1
}
```
- Return value from the API (CreateGaussMySqlDatabaseUser) for creating a database user
```
class CreateGaussMySqlDatabaseUserResponse {
    jobId: f725af33-af02-48af-a5e4-8197e8515372
}
```
- Return value of the API (ListGaussMySqlDatabaseUser) for querying database users
```
class ListGaussMySqlDatabaseUserResponse {
    users: [class ListGaussMySqlDatabaseUser {
        name: user_test_1
        host: 127.0.0.1
        databases: []
    }, class ListGaussMySqlDatabaseUser {
        name: user_test_2
        host: %
        databases: []
    }]
    totalCount: 2
}
```

## 7. References

For details, see [Querying Database Users](https://support.huaweicloud.com/intl/en-us/api-gaussdbformysql/ListGaussMySqlDatabaseUser.html).
You can directly run and debug the API in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/GaussDB/doc?api=ListGaussMySqlDatabaseUser).

For details, see [API for Creating a Database Account](https://support.huaweicloud.com/intl/en-us/api-gaussdbformysql/CreateGaussMySqlDatabaseUser.html).
You can directly run and debug the API in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/GaussDB/doc?api=CreateGaussMySqlDatabaseUser).

## Change History
| Released On | Issue|   Description  |
|:-----------:| :------: | :----------: |
| 2023-11-29  |   1.0    | First official release|
