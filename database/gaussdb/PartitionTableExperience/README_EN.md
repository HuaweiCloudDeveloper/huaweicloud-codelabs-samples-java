## [Feature Guide] Enhanced Partition Table Query

### Function Description

Enhanced Partitioned Table Query is suitable for tables that use generated columns as partition keys. If a query statement does not provide partition key conditions but provides equivalent conditions for all base columns of the generated column (which is used as the partition key), the optimizer can select the target partition for query without scanning the entire partitions, thereby optimizing the query performance. This function helps users partition a single large table and no SQL statements need to be modified, improving SQL query performance and flexibly expanding services. For details, see the following example.

Table query:

```sql
1Table 1:
2CREATE TABLE t1 (
3 a INT,
4 b INT,
5 c INT,
6 d INT GENERATED ALWAYS AS (a+b+c) STORED,
7 PRIMARY KEY(a,d)
8)
9PARTITION BY HASH (d)
10PARTITIONS 6;
11 
12Query 1: 
13SELECT * FROM t1 WHERE a = 4 AND b=10 AND c=11;
```

`Table 1` uses the generated column (`d`) as the partition key. `Query 1` does not provide the conditions of the partition key (`d`), but provides the equivalent condition for all base columns (`a`, `b`, and `c`) of the generated column (`d`).

After this function is introduced, the execution plan is as follows:

- Enabling the function:

```sql
1mysql> SET optimizer_switch='gen_col_partition_prune=on';
2mysql> explain SELECT * FROM t1 WHERE a = 4 AND b=10 AND c=11;
3+----+-------------+-------+------------+-------+---------------+---------+---------+-------------+------+----------+-------+
4| id | select_type | table | partitions | type  | possible_keys | key     | key_len | ref         | rows | filtered | Extra |
5+----+-------------+-------+------------+-------+---------------+---------+---------+-------------+------+----------+-------+
6|  1 | SIMPLE      | t1    | p1         | const | PRIMARY       | PRIMARY | 8       | const,const |    1 |   100.00 | NULL  |
7+----+-------------+-------+------------+-------+---------------+---------+---------+-------------+------+----------+-------+
81 row in set, 1 warning (0.00 sec)
```

- Disabling the function:

```sql
1mysql> SET optimizer_switch='gen_col_partition_prune=off';
2mysql> explain SELECT * FROM t1 WHERE a = 4 AND b=10 AND c=11;
3+----+-------------+-------+-------------------+------+---------------+---------+---------+-------+------+----------+-------------+
4| id | select_type | table | partitions        | type | possible_keys | key     | key_len | ref   | rows | filtered | Extra       |
5+----+-------------+-------+-------------------+------+---------------+---------+---------+-------+------+----------+-------------+
6|  1 | SIMPLE      | t1    | p0,p1,p2,p3,p4,p5 | ref  | PRIMARY       | PRIMARY | 4       | const |    1 |   100.00 | Using where |
7+----+-------------+-------+-------------------+------+---------------+---------+---------+-------+------+----------+-------------+
81 row in set, 1 warning (0.00 sec)
9 
10 
```

If this function is enabled, for queries such as `Query 1`, the optimizer can select a single target partition for query. There is no need to scan the entire table, so the overall query performance is improved.

### Function Switch

The `**optimizer_switch**` parameter is used to enable or disable this function.

- Enabling this function: `SET optimizer_switch='gen_col_partition_prune=on'`;
- Disabling this function: `SET optimizer_switch='gen_col_partition_prune=off'`;

This function is disabled by default.

### Function Description

Application scenarios

- Partition keys contain one or more generated columns, or contain the combinations of generated columns and normal columns.
- Sub-partition keys of partitioned tables contain generated columns.
- If a partition key contains multiple generated columns, the query conditions only need to contain base columns of the generated columns. For example, if the partition key is (`c`, `d`), the base column of `c` is `a`, and the base column of `d` is `b`, then the query conditions can be: `WHERE c=1 AND b=1` and `WHERE a=1 AND b=1`.

Constraints

- Only equivalent conditions of all base columns in a generated column can be used.
- There are no implicit conversions in equivalent conditions, for example, `WHERE a='1'` (`a` is a base column of the INT type) is not allowed.
- The generated columns used as partition keys must be in the `STORED` format, instead of the `VIRTUAL` format.
- If a generated column is used as the partition key, its base columns cannot be in the `VIRTUAL` format.
- If a generated column is used as the partition key, its base columns can only be in the `STRING` format when the COLLATION of the base columns is of the BINARY type.
- In these scenarios, the optimizer behavior is the same as that of the community edition `MySQL`.

### Examples

Users often encounter the following issues when using their databases:

- As service systems develop and workloads grow, the query performance for larger tables may deteriorate. If the system has been in use for a long time, re-planning the table structure has a great impact on the system. Users want to improve SQL query performance while ensuring that SQL statements are not modified, for example, horizontal splitting and partition reconstruction for a single large table.
- In some scenarios, users may want to delete or classify tables in batches based on a certain rule, for example, by time segment or factory type, to improve query and update efficiency. Partitioned tables can be used to speed up queries, but for a query that does not contain a partition key, the performance is not satisfactory.

In this case, Enhanced Partition Table Query is an ideal choice. For details, see the following example.

#### Partitioning Reconstruction for a Single Large Table, Zero SQL Modifications

The original table structure and query statements are as follows:

```sql
1Table 2:
2CREATE TABLE t1 (
3    id varchar(20) NOT NULL, //** id** contains date string.
4 plant varchar(6) NOT NULL, //Factory name
5    content varchar(20) NULL,
6    PRIMARY KEY (id)
7);
8 
9Query 2:
10select * from t1 where id='ABC20210109-abzxxxx' AND plant='H';
```

**Service requirements**: The data volume in`Table 2` increases greatly, and some query performance deteriorates. Users want to improve SQL query performance and delete expired data (by week or month based on the factory name) while ensuring no SQL statements are modified. In this way, table data can be managed more effectively.

With this function, the table can be reconstructed as follows:

- The generate column `plant_week` is added in the `STORED` format. The base columns of the generate column are `plant` (indicates factory) and `id` (indicates week). The `plant_week` column is used as the partition key.

```sql
1Table 3:
2CREATE TABLE t1 (
3    id varchar(20) NOT NULL,
4    plant varchar(6) NOT NULL,
5    plant_week varchar(20) GENERATED ALWAYS AS (concat(plant, week(substring(id, 4, 8)))) STORED,
6    content varchar(20) NULL,
7    PRIMARY KEY (id, plant_week)
8) PARTITION BY LIST COLUMNS(plant_week) (
9    PARTITION p1 VALUES in ('H1'),
10    PARTITION p2 VALUES in ('H2'),
11    PARTITION p3 VALUES in ('H3'),
12    ...
13    ...
14    PARTITION p5 VALUES in ('H52')
15);
```

After the table is reconstructed, the service requirements can be met.

1. The original `Query 2` does not need to be modified. Records can be queried in a specified partition of the `Table 3`. Records can only be queried in the corresponding factory and week. The amount of data searched by the `Query 2` is greatly reduced, and the query performance is improved.
2. Users can delete specified partitions, which means that expired data can be deleted (by week and factory). When a partition is deleted, the data update and query in other partitions are not affected.



## Change history

| Date       | Issue| Description    |
|------------| -------- | ------------ |
| 2023-12-20 | 1.0      | This issue is the first official release.|
