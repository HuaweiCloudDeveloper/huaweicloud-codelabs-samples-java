## 1. Introduction

TaurusDB is a MySQL-compatible, enterprise-grade distributed database. Data functions virtualization (DFV) is used to decouple storage from compute and can auto scale up to 128 TB per instance. With TaurusDB, there is no need for sharding, and no need to worry about data loss. It provides superior performance of commercial databases at the price of open-source databases.

This example shows how to create a TaurusDB instance using a Java SDK. SDK 3.1.56 is used in this example.
## 2. Prerequisites
- You have created [a HUAWEI ID](https://id5.cloud.huawei.com/CAS/portal/userRegister/regbyphone.html?&lang=en-us) and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?locale=en-us#/accountindex/realNameAuth).
- You have obtained a Huawei Cloud SDK. You can also install a Java SDK.
- You have obtained the access key ID (AK) and secret access key (SK) of the HUAWEI ID. You can create and view your AK/SK on the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html).
- You have set up the development environment with Java JDK 1.8 or later.

## 3. Installing an SDK
You can obtain and install an SDK in Maven mode. Download and install Maven in your operating system (OS). After the installation is complete, add the required dependencies to the **pom.xml** file of the Java project.

For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java).
``` java
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-gaussdb</artifactId>
    <version>3.1.56</version>
</dependency>
```
## 4. Sample Code
Creating a DB instance using the SDK.
``` java
package com.huawei.gaussdb;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.gaussdb.v3.GaussDBClient;
import com.huaweicloud.sdk.gaussdb.v3.model.CreateGaussMySqlInstanceRequest;
import com.huaweicloud.sdk.gaussdb.v3.model.CreateGaussMySqlInstanceResponse;
import com.huaweicloud.sdk.gaussdb.v3.model.ListGaussMySqlInstancesRequest;
import com.huaweicloud.sdk.gaussdb.v3.model.ListGaussMySqlInstancesResponse;
import com.huaweicloud.sdk.gaussdb.v3.model.MysqlDatastoreInReq;
import com.huaweicloud.sdk.gaussdb.v3.model.MysqlInstanceRequest;
import com.huaweicloud.sdk.gaussdb.v3.region.GaussDBRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateInstanceDemo {
    private static final Logger logger = LoggerFactory.getLogger(CreateInstanceDemo.class.getName());


    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security purposes, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK**.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        GaussDBClient client = GaussDBClient.newBuilder().withCredential(auth)
                .withRegion(GaussDBRegion.CN_NORTH_4)
                .build();
        //Creates a DB instance.
        createInstance(client);

        //Queries DB instances.
        listInstance(client);
    }

    private static void createInstance(GaussDBClient client) {
        CreateGaussMySqlInstanceRequest request = new CreateGaussMySqlInstanceRequest();
        MysqlInstanceRequest body = new MysqlInstanceRequest();

        // Region ID
        body.withRegion(GaussDBRegion.CN_NORTH_4.getId());
        // Instance name
        body.withName("<name>");
        //Database information
        MysqlDatastoreInReq dataStoreBody = new MysqlDatastoreInReq();
        dataStoreBody.withType("<type>").withVersion("<version>");
        body.withDatastore(dataStoreBody);
        //Instance type
        body.withMode("<mode>");
        // Specification code
        body.withFlavorRef("<flavor_ref>");
        // VPC ID
        body.withVpcId("<vpc_id>");
        // Subnet network ID
        body.withSubnetId("<subnet_id>");
        //Security group ID
        body.withSecurityGroupId("<security_group_id>");
        // Password
        body.withPassword("<password>");
        // AZ type
        body.withAvailabilityZoneMode("<availability_zone_mode>");
        // Number of read replicas
        body.withSlaveCount(1);
        request.withBody(body);
        try {
            CreateGaussMySqlInstanceResponse response = client.createGaussMySqlInstance(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }

    private static void listInstance(GaussDBClient client) {
        ListGaussMySqlInstancesRequest request = new ListGaussMySqlInstancesRequest();
        try {
            ListGaussMySqlInstancesResponse response = client.listGaussMySqlInstances(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}
```
You can directly run and debug the API in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/GaussDB/doc?api=CreateGaussMySqlInstance).

## 5. APIs and Parameters
1. Parameters for creating a DB instance.

*<vpc_id>*: VPC ID. To obtain the value, use either of the following methods:

- Log in to the VPC console and view the VPC ID in the VPC details.
- Call the API used to query VPCs. For details, see [Querying VPCs](https://support.huaweicloud.com/intl/en-us/api-vpc/vpc_api01_0003.html).

*<subnet_id>*: Network ID. To obtain the value, use either of the following methods:

- Log in to the VPC console and click the target subnet on the **Subnets** page. You can view the network ID on the displayed page.
- Call the API used to query subnets. For details, see [Querying Subnets](https://support.huaweicloud.com/intl/en-us/api-vpc/vpc_subnet01_0003.html).


*<security_group_id>*: Security group ID. To obtain the value, use either of the following methods:

- Log in to the VPC console and view the security group ID on the security group details page.
- Call the API used to query security group details. For details, see [Querying Security Groups](https://support.huaweicloud.com/intl/en-us/api-vpc/vpc_sg01_0003.html).

*<flavor_ref>*: Specification code. To obtain the value, see [Querying Database Specifications](https://support.huaweicloud.com/intl/en-us/api-gaussdbformysql/ShowGaussMySqlFlavors.html).

For details about other parameters, see [Creating a DB Instance](https://support.huaweicloud.com/intl/en-us/api-gaussdbformysql/CreateGaussMySqlInstance.html).

2. [Querying DB Instances](https://support.huaweicloud.com/intl/en-us/api-gaussdbformysql/ListGaussMySqlInstances.html).
## 6. Change History

| Released On | Issue| Description|
|:-----------:| :------: | :----------: |
| 2023-11-28  | 1.0 | Initial release|
