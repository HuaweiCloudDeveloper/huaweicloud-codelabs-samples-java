## 1. 简介

TaurusDB是华为自研的最新一代企业级高扩展海量存储分布式数据库，完全兼容MySQL。基于华为最新一代DFV存储，采用计算存储分离架构，128TB的海量存储，无需分库分表，数据0丢失，既拥有商业数据库的高可用和性能，又具备开源低成本效益。

本示例展示如何通过java版本的SDK方式创建TaurusDB实例，本示例配套的SDK版本为：3.1.56。
## 2. 前置条件
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。

## 3. 安装SDK
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

具体的SDK版本号请参见 [SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。
``` java
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-gaussdb</artifactId>
    <version>3.1.56</version>
</dependency>
```
## 4. 代码示例
以下代码展示如何使用SDK创建实例：
``` java
package com.huawei.gaussdb;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.gaussdb.v3.GaussDBClient;
import com.huaweicloud.sdk.gaussdb.v3.model.CreateGaussMySqlInstanceRequest;
import com.huaweicloud.sdk.gaussdb.v3.model.CreateGaussMySqlInstanceResponse;
import com.huaweicloud.sdk.gaussdb.v3.model.ListGaussMySqlInstancesRequest;
import com.huaweicloud.sdk.gaussdb.v3.model.ListGaussMySqlInstancesResponse;
import com.huaweicloud.sdk.gaussdb.v3.model.MysqlDatastoreInReq;
import com.huaweicloud.sdk.gaussdb.v3.model.MysqlInstanceRequest;
import com.huaweicloud.sdk.gaussdb.v3.region.GaussDBRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateInstanceDemo {
    private static final Logger logger = LoggerFactory.getLogger(CreateInstanceDemo.class.getName());


    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        GaussDBClient client = GaussDBClient.newBuilder().withCredential(auth)
                .withRegion(GaussDBRegion.CN_NORTH_4)
                .build();
        //创建数据库实例
        createInstance(client);

        //查询实例列表
        listInstance(client);
    }

    private static void createInstance(GaussDBClient client) {
        CreateGaussMySqlInstanceRequest request = new CreateGaussMySqlInstanceRequest();
        MysqlInstanceRequest body = new MysqlInstanceRequest();

        // 区域ID
        body.withRegion(GaussDBRegion.CN_NORTH_4.getId());
        // 实例名称
        body.withName("<name>");
        //数据库信息
        MysqlDatastoreInReq dataStoreBody = new MysqlDatastoreInReq();
        dataStoreBody.withType("<type>").withVersion("<version>");
        body.withDatastore(dataStoreBody);
        //实例类型
        body.withMode("<mode>");
        // 规格码
        body.withFlavorRef("<flavor_ref>");
        // 虚拟私有云ID
        body.withVpcId("<vpc_id>");
        // 子网的网络ID
        body.withSubnetId("<subnet_id>");
        //安全组ID
        body.withSecurityGroupId("<security_group_id>");
        // 密码
        body.withPassword("<password>");
        // 可用区类型
        body.withAvailabilityZoneMode("<availability_zone_mode>");
        // 只读节点个数
        body.withSlaveCount(1);
        request.withBody(body);
        try {
            CreateGaussMySqlInstanceResponse response = client.createGaussMySqlInstance(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }

    private static void listInstance(GaussDBClient client) {
        ListGaussMySqlInstancesRequest request = new ListGaussMySqlInstancesRequest();
        try {
            ListGaussMySqlInstancesResponse response = client.listGaussMySqlInstances(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}
```
您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/GaussDB/doc?api=CreateGaussMySqlInstance) 中直接运行调试该接口。

## 5. 接口及参数说明
1.创建数据库实例参数。

<vpc_id>：虚拟私有云ID，获取方法如下：

- 登录虚拟私有云服务的控制台界面，在虚拟私有云的详情页面查找VPC ID。
- 通过虚拟私有云服务的API接口查询，具体操作可参考[查询VPC列表](https://support.huaweicloud.com/api-vpc/vpc_api01_0003.html)。

<subnet_id>:子网的网络ID信息，获取方法如下：

- 登录虚拟私有云服务的控制台界面，单击VPC下的子网，进入子网详情页面，查找网络ID。
- 通过虚拟私有云服务的API接口查询，具体操作可参考[查询子网列表](https://support.huaweicloud.com/api-vpc/vpc_subnet01_0003.html)。


<security_group_id>:安全组ID信息，获取方法如下：

- 登录虚拟私有云服务的控制台界面，在安全组的详情页面查找安全组ID。
- 通过虚拟私有云服务的API接口查询，具体操作可参考[查询安全组列表](https://support.huaweicloud.com/api-vpc/vpc_sg01_0003.html)。

<flavor_ref>:规格码。参考[查询数据库规格](https://support.huaweicloud.com/api-gaussdbformysql/ShowGaussMySqlFlavors.html)中资源规格编码列获取。

其他参数请见 [创建数据库实例](https://support.huaweicloud.com/api-gaussdbformysql/CreateGaussMySqlInstance.html)。

2.请见 [查询实例列表](https://support.huaweicloud.com/api-gaussdbformysql/ListGaussMySqlInstances.html)。
## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-08-26 | 1.0 | 文档首次发布|