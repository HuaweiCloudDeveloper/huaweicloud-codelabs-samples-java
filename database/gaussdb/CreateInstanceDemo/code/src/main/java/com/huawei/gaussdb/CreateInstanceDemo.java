package com.huawei.gaussdb;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.gaussdb.v3.GaussDBClient;
import com.huaweicloud.sdk.gaussdb.v3.model.CreateGaussMySqlInstanceRequest;
import com.huaweicloud.sdk.gaussdb.v3.model.CreateGaussMySqlInstanceResponse;
import com.huaweicloud.sdk.gaussdb.v3.model.ListGaussMySqlInstancesRequest;
import com.huaweicloud.sdk.gaussdb.v3.model.ListGaussMySqlInstancesResponse;
import com.huaweicloud.sdk.gaussdb.v3.model.MysqlDatastoreInReq;
import com.huaweicloud.sdk.gaussdb.v3.model.MysqlInstanceRequest;
import com.huaweicloud.sdk.gaussdb.v3.region.GaussDBRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateInstanceDemo {
    private static final Logger logger = LoggerFactory.getLogger(CreateInstanceDemo.class.getName());


    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security purposes, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        GaussDBClient client = GaussDBClient.newBuilder().withCredential(auth)
                .withRegion(GaussDBRegion.CN_NORTH_4)
                .build();
        //Creates a DB instance.
        createInstance(client);

        //Queries DB instances.
        listInstance(client);
    }

    private static void createInstance(GaussDBClient client) {
        CreateGaussMySqlInstanceRequest request = new CreateGaussMySqlInstanceRequest();
        MysqlInstanceRequest body = new MysqlInstanceRequest();

        // Region ID
        body.withRegion(GaussDBRegion.CN_NORTH_4.getId());
        // Instance name
        body.withName("<name>");
        //Database information
        MysqlDatastoreInReq dataStoreBody = new MysqlDatastoreInReq();
        dataStoreBody.withType("<type>").withVersion("<version>");
        body.withDatastore(dataStoreBody);
        //Instance type
        body.withMode("<mode>");
        // Specification code
        body.withFlavorRef("<flavor_ref>");
        // VPC ID
        body.withVpcId("<vpc_id>");
        // Subnet network ID
        body.withSubnetId("<subnet_id>");
        //Security group ID
        body.withSecurityGroupId("<security_group_id>");
        //Password
        body.withPassword("<password>");
        // AZ type
        body.withAvailabilityZoneMode("<availability_zone_mode>");
        // Number of read replicas
        body.withSlaveCount(1);
        request.withBody(body);
        try {
            CreateGaussMySqlInstanceResponse response = client.createGaussMySqlInstance(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }

    private static void listInstance(GaussDBClient client) {
        ListGaussMySqlInstancesRequest request = new ListGaussMySqlInstancesRequest();
        try {
            ListGaussMySqlInstancesResponse response = client.listGaussMySqlInstances(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }
}
