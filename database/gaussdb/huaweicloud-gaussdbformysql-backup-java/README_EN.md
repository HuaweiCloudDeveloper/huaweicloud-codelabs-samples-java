## 1. Introduction
TaurusDB is a MySQL-compatible, enterprise-grade distributed database. Data functions virtualization (DFV) is used to decouple storage from compute and can auto scale up to 128 TB per instance. With TaurusDB, there is no need for sharding, and no need to worry about data loss. It provides superior performance of commercial databases at the price of open-source databases. This example shows how to modify the backup policy for a DB instance using a Java SDK.

## 2. Flowchart
![Modifying a backup policy](assets/backup_en.png)

## 3. Prerequisites

1. You have created [a HUAWEI ID](https://id5.cloud.huawei.com/CAS/portal/userRegister/regbyphone.html?&lang=en-us) and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?locale=en-us#/accountindex/realNameAuth).

2. You have obtained a Huawei Cloud SDK. You can also install a Java SDK.

3. You have obtained the access key ID (AK) and secret access key (SK) of your HUAWEI ID. You can create and view your AK/SK on the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html).

4. You have set up the development environment with the Java JDK 1.8 or later.


## 4. Obtaining and Installing an SDK
You can obtain and install an SDK in Maven mode. Download and install Maven in your operating system (OS). After the installation is complete, add the required dependencies to the **pom.xml** file of the Java project.

For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter).
```xml
 <dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-gaussdb</artifactId>
    <version>3.1.1</version>
</dependency>
```

## 5. Key Code Snippet
Modifying an automated backup policy using the SDK

```java

public class BackupDemo {
    private static final Logger logger = LoggerFactory.getLogger(BackupDemo.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security purposes, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = "<YOUR INSTANCE_ID>";
        String regionId = "<YOUR REGION_ID>";

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        GaussDBClient client = GaussDBClient.newBuilder()
                .withCredential(auth)
                .withRegion(GaussDBRegion.valueOf(regionId))
                .build();

        //Views the backup policy.
        showBackupPolicy(client, instanceId);

        //Modifies the backup retention period, time window, and backup cycle of the backup policy.
        String start_time = "<YOUR START_TIME>";
        Integer keep_days = 7; // Sets backup retention period to 7.
        String period = "<YOUR PERIOD>";
        MysqlUpdateBackupPolicyRequest backupPolicyBody = new MysqlUpdateBackupPolicyRequest();
        backupPolicyBody.setBackupPolicy(new MysqlBackupPolicy().
                withStartTime(start_time).
                withKeepDays(keep_days).
                withPeriod(period));
        //Modifies the backup policy.
        updateBackupPolicy(client, instanceId, backupPolicyBody);

        //Queries backups.
        showBackupList(client, instanceId);
    }

    private static void showBackupList(GaussDBClient client, String instanceId) {
        ShowGaussMySqlBackupListRequest request = new ShowGaussMySqlBackupListRequest();
        request.withInstanceId(instanceId);
        try {
            ShowGaussMySqlBackupListResponse response = client.showGaussMySqlBackupList(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void showBackupPolicy(GaussDBClient client, String instanceId) {
        ShowGaussMySqlBackupPolicyRequest request = new ShowGaussMySqlBackupPolicyRequest();
        request.setInstanceId(instanceId);
        try {
            ShowGaussMySqlBackupPolicyResponse response = client.showGaussMySqlBackupPolicy(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }


    private static void updateBackupPolicy(GaussDBClient client, String instanceId, MysqlUpdateBackupPolicyRequest backupPolicyBody) {
        UpdateGaussMySqlBackupPolicyRequest request = new UpdateGaussMySqlBackupPolicyRequest();
        request.setInstanceId(instanceId);
        request.withBody(backupPolicyBody);
        try {
            UpdateGaussMySqlBackupPolicyResponse response = client.updateGaussMySqlBackupPolicy(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }

    }

}



```
## 6. Sample Return Values
- Return value of the API (ShowGaussMySqlBackupPolicy) for querying an automated backup policy
```
class ShowGaussMySqlBackupPolicyResponse {
    backupPolicy: class BackupPolicy {
        keepDays: 7
        startTime: 19:00-20:00
        period: 1,2,3,4,5
        retentionNumBackupLevel1: null
    }
}
```
- Return value of the API (UpdateGaussMySqlBackupPolicy) for modifying a backup policy
```
class UpdateGaussMySqlBackupPolicyResponse {
    status: COMPLETED
    instanceId: 7e193cf036f148489657621c7bfff057in07
    instanceName: switchover-test
}
```
- Return value of the API (ShowGaussMySqlBackupList) for querying backups
```
class ShowGaussMySqlBackupListResponse {
    backups: [class Backups {
        id: be18935f22264ae0a6e04f6340188056br07
        name: GaussDBforMySQL-switchover-test-20220927190027369
        beginTime: 2022-09-28T03:00:27+0800
        endTime: 2022-09-28T03:03:03+0800
        status: COMPLETED
        takeUpTime: 2
        type: auto
        size: 76
        datastore: class MysqlDatastore {
            type: GaussDB(for MySQL)
            version: 8.0
        }
        instanceId: 7e193cf036f148489657621c7bfff057in07
        backupLevel: null
        description: null
    }, class Backups {
        id: cf7e7ccc539041f38e7b1cf4615c511cbr07
        name: GaussDBforMySQL-switchover-test-20220926190026957
        beginTime: 2022-09-27T03:00:26+0800
        endTime: 2022-09-27T03:02:56+0800
        status: COMPLETED
        takeUpTime: 2
        type: auto
        size: 76
        datastore: class MysqlDatastore {
            type: GaussDB(for MySQL)
            version: 8.0
        }
        instanceId: 7e193cf036f148489657621c7bfff057in07
        backupLevel: null
        description: null
    }]
    totalCount: 2
}
```

## 7. References

For details, see [Querying Backups] (https://support.huaweicloud.com/intl/en-us/api-gaussdbformysql/ShowGaussMySqlBackupList.html).
You can directly run and debug the API in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/GaussDB/doc?api=ShowGaussMySqlBackupList).

For details, see [Modifying an Automated Backup Policy] (https://support.huaweicloud.com/intl/en-us/api-gaussdbformysql/UpdateGaussMySqlBackupPolicy.html).
You can directly run and debug the API in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/GaussDB/doc?api=UpdateGaussMySqlBackupPolicy).

For details, see [Querying an Automated Backup Policy] (https://support.huaweicloud.com/intl/en-us/api-gaussdbformysql/ShowGaussMySqlBackupPolicy.html).
You can directly run and debug the API in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/GaussDB/doc?api=ShowGaussMySqlBackupPolicy).

## Change History
| Released On | Issue|   Description  |
|:-----------:| :------: | :----------: |
| 2023-11-29  |   1.0    | First official release|
