package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.gaussdb.v3.GaussDBClient;
import com.huaweicloud.sdk.gaussdb.v3.model.MysqlBackupPolicy;
import com.huaweicloud.sdk.gaussdb.v3.model.MysqlUpdateBackupPolicyRequest;
import com.huaweicloud.sdk.gaussdb.v3.model.ShowGaussMySqlBackupListRequest;
import com.huaweicloud.sdk.gaussdb.v3.model.ShowGaussMySqlBackupListResponse;
import com.huaweicloud.sdk.gaussdb.v3.model.ShowGaussMySqlBackupPolicyRequest;
import com.huaweicloud.sdk.gaussdb.v3.model.ShowGaussMySqlBackupPolicyResponse;
import com.huaweicloud.sdk.gaussdb.v3.model.UpdateGaussMySqlBackupPolicyRequest;
import com.huaweicloud.sdk.gaussdb.v3.model.UpdateGaussMySqlBackupPolicyResponse;
import com.huaweicloud.sdk.gaussdb.v3.region.GaussDBRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BackupDemo {
    private static final Logger logger = LoggerFactory.getLogger(BackupDemo.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security purposes, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = "<YOUR INSTANCE_ID>";
        String regionId = "<YOUR REGION_ID>";

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        GaussDBClient client = GaussDBClient.newBuilder()
                .withCredential(auth)
                .withRegion(GaussDBRegion.valueOf(regionId))
                .build();

        // Views the backup policy.
        showBackupPolicy(client, instanceId);

        // Modifies the backup retention period, time window, and backup cycle of the backup policy.
        String start_time = "<YOUR START_TIME>";
        Integer keep_days = 7; // 备份文件的保留天数，设为需要的保留天数，这里以7为例
        String period = "<YOUR PERIOD>";
        MysqlUpdateBackupPolicyRequest backupPolicyBody = new MysqlUpdateBackupPolicyRequest();
        backupPolicyBody.setBackupPolicy(new MysqlBackupPolicy().
                withStartTime(start_time).
                withKeepDays(keep_days).
                withPeriod(period));
        // Modifies the backup policy.
        updateBackupPolicy(client, instanceId, backupPolicyBody);

        // Queries backups.
        showBackupList(client, instanceId);
    }

    private static void showBackupList(GaussDBClient client, String instanceId) {
        ShowGaussMySqlBackupListRequest request = new ShowGaussMySqlBackupListRequest();
        request.withInstanceId(instanceId);
        try {
            ShowGaussMySqlBackupListResponse response = client.showGaussMySqlBackupList(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void showBackupPolicy(GaussDBClient client, String instanceId) {
        ShowGaussMySqlBackupPolicyRequest request = new ShowGaussMySqlBackupPolicyRequest();
        request.setInstanceId(instanceId);
        try {
            ShowGaussMySqlBackupPolicyResponse response = client.showGaussMySqlBackupPolicy(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }


    private static void updateBackupPolicy(GaussDBClient client, String instanceId, MysqlUpdateBackupPolicyRequest backupPolicyBody) {
        UpdateGaussMySqlBackupPolicyRequest request = new UpdateGaussMySqlBackupPolicyRequest();
        request.setInstanceId(instanceId);
        request.withBody(backupPolicyBody);
        try {
            UpdateGaussMySqlBackupPolicyResponse response = client.updateGaussMySqlBackupPolicy(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }

    }

}
