## 1. 介绍
TaurusDB是华为自研的最新一代企业级高扩展海量存储分布式数据库，完全兼容MySQL。基于华为最新一代DFV存储，采用计算存储分离架构，128TB的海量存储，无需分库分表，数据0丢失，既拥有商业数据库的高可用和性能，又具备开源低成本效益。本示例展示如何通过java版本的SDK方式修改数据库备份策略。

## 2. 流程图
![修改数据库备份策略的流程图](assets/backup.png)

## 3. 前置条件

1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。

2.获取华为云开发工具包（SDK），您也可1以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。


## 4. SDK获取和安装
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
 <dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-gaussdb</artifactId>
    <version>3.1.1</version>
</dependency>
```

## 5. 关键代码片段
以下代码展示如何使用SDK修改自动备份策略：

```java

public class BackupDemo {
    private static final Logger logger = LoggerFactory.getLogger(BackupDemo.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = "<YOUR INSTANCE_ID>";
        String regionId = "<YOUR REGION_ID>";

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        GaussDBClient client = GaussDBClient.newBuilder()
                .withCredential(auth)
                .withRegion(GaussDBRegion.valueOf(regionId))
                .build();

        // 查看备份策略
        showBackupPolicy(client, instanceId);

        // 修改的备份策略的备份时间段，备份文件的保留天数，备份周期配置
        String start_time = "<YOUR START_TIME>";
        Integer keep_days = 7; // 备份文件的保留天数，设为需要的保留天数，这里以7为例
        String period = "<YOUR PERIOD>";
        MysqlUpdateBackupPolicyRequest backupPolicyBody = new MysqlUpdateBackupPolicyRequest();
        backupPolicyBody.setBackupPolicy(new MysqlBackupPolicy().
                withStartTime(start_time).
                withKeepDays(keep_days).
                withPeriod(period));
        // 修改备份策略
        updateBackupPolicy(client, instanceId, backupPolicyBody);

        // 查询备份列表
        showBackupList(client, instanceId);
    }

    private static void showBackupList(GaussDBClient client, String instanceId) {
        ShowGaussMySqlBackupListRequest request = new ShowGaussMySqlBackupListRequest();
        request.withInstanceId(instanceId);
        try {
            ShowGaussMySqlBackupListResponse response = client.showGaussMySqlBackupList(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void showBackupPolicy(GaussDBClient client, String instanceId) {
        ShowGaussMySqlBackupPolicyRequest request = new ShowGaussMySqlBackupPolicyRequest();
        request.setInstanceId(instanceId);
        try {
            ShowGaussMySqlBackupPolicyResponse response = client.showGaussMySqlBackupPolicy(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }


    private static void updateBackupPolicy(GaussDBClient client, String instanceId, MysqlUpdateBackupPolicyRequest backupPolicyBody) {
        UpdateGaussMySqlBackupPolicyRequest request = new UpdateGaussMySqlBackupPolicyRequest();
        request.setInstanceId(instanceId);
        request.withBody(backupPolicyBody);
        try {
            UpdateGaussMySqlBackupPolicyResponse response = client.updateGaussMySqlBackupPolicy(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }

    }

}



```
## 6. 返回结果示例
- 查看自动备份策略（ShowGaussMySqlBackupPolicy）接口的返回值：
```
class ShowGaussMySqlBackupPolicyResponse {
    backupPolicy: class BackupPolicy {
        keepDays: 7
        startTime: 19:00-20:00
        period: 1,2,3,4,5
        retentionNumBackupLevel1: null
    }
}
```
- 修改备份策略（UpdateGaussMySqlBackupPolicy）接口的返回值：
```
class UpdateGaussMySqlBackupPolicyResponse {
    status: COMPLETED
    instanceId: 7e193cf036f148489657621c7bfff057in07
    instanceName: switchover-test
}
```
- 查询备份列表（ShowGaussMySqlBackupList）接口的返回值：
```
class ShowGaussMySqlBackupListResponse {
    backups: [class Backups {
        id: be18935f22264ae0a6e04f6340188056br07
        name: GaussDBforMySQL-switchover-test-20220927190027369
        beginTime: 2022-09-28T03:00:27+0800
        endTime: 2022-09-28T03:03:03+0800
        status: COMPLETED
        takeUpTime: 2
        type: auto
        size: 76
        datastore: class MysqlDatastore {
            type: GaussDB(for MySQL)
            version: 8.0
        }
        instanceId: 7e193cf036f148489657621c7bfff057in07
        backupLevel: null
        description: null
    }, class Backups {
        id: cf7e7ccc539041f38e7b1cf4615c511cbr07
        name: GaussDBforMySQL-switchover-test-20220926190026957
        beginTime: 2022-09-27T03:00:26+0800
        endTime: 2022-09-27T03:02:56+0800
        status: COMPLETED
        takeUpTime: 2
        type: auto
        size: 76
        datastore: class MysqlDatastore {
            type: GaussDB(for MySQL)
            version: 8.0
        }
        instanceId: 7e193cf036f148489657621c7bfff057in07
        backupLevel: null
        description: null
    }]
    totalCount: 2
}
```

## 7.参考链接

请见 [查询备份列表API](https://support.huaweicloud.com/api-gaussdbformysql/ShowGaussMySqlBackupList.html)
您可以在 [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=GaussDB&api=ShowGaussMySqlBackupList) 中直接运行调试该接口。

请见 [修改备份策略API](https://support.huaweicloud.com/api-gaussdbformysql/UpdateGaussMySqlBackupPolicy.html)
您可以在 [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=GaussDB&api=UpdateGaussMySqlBackupPolicy) 中直接运行调试该接口。

请见 [查询自动备份策略API](https://support.huaweicloud.com/api-gaussdbformysql/ShowGaussMySqlBackupPolicy.html)
您可以在 [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=GaussDB&api=ShowGaussMySqlBackupPolicy) 中直接运行调试该接口。

## 修订记录
|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2022-09-19 |   1.0    | 文档首次发布 |
