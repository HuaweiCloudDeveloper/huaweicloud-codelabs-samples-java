# TaurusDB HTAP Instances Analyzes Massive Data Quickly, Stably, and Accurately

## 1 Introduction

Hybrid Transactional/Analytical Processing (HTAP) is an emerging data architecture that joins online transactional processing (OLTP) and online analytical processing (OLAP) workloads. ClickHouse is a high-performance SQL database management system (DBMS) for OLAP. It can connect to MySQL using the MaterializeMySQL engine to enable data sharing between heterogeneous databases. To maximize performance, ClickHouse can be connected with many transactional processing databases such as MySQL. In actual application scenarios, ClickHouse still faces some challenges, which is why TaurusDB launched HTAP analysis, which not only delivers the same robust performance as ClickHouse, but also delivers better performance and stability, thanks to the MaterilizeMySQL engine. HTAP analysis provides faster, more accurate data analysis.

## 2. Background

In the big data era, data volumes have been increasing sharply and the user structure becomes more diversified. When processing data, users find that creating a visualized report requires data extraction, transformation, and loading. The entire period may take several days or even weeks. Extract, transform, and load (ETL) processing can process multi-source data based on data lakes, process massive data at a low cost, and have a complete ecosystem. However, traditional data warehouses and data lakes cannot support a large number of real-time concurrent updates, and the timeliness of data analysis in ETL mode is low. ETL processing does not handle changes well. For example, if an upstream data source changes (for example, the table structure changes), the entire data process needs to be modified accordingly, increasing the difficulty of data maintenance.

![](./assets/figure1.png)

To enable real-time analysis, HTAP is used. HTAP supports a large number of concurrent updates and ensures that data synchronization latency is usually within seconds or milliseconds. This eliminates complex steps such as the data extraction, transformation, and loading involved in traditional solutions and greatly improves the timeliness of data processing.

![](./assets/figure2.png)

## 3. Ultimate Performance of ClickHouse

- ClickHouse

ClickHouse is an open-source column-oriented distribute database developed by Yandex for OLAP. It supports real-time queries, comprehensive DBMS, efficient data compression, batch updates, and high availability. ClickHouse supports most of SQL statements and is easy to use. In the officially released benchmarks, ClickHouse is far ahead of the competition.

- Row-oriented storage and column-oriented storage

MySQL uses row-oriented storage. Data in tables is continuously stored in storage media and uses a row as a logical storage unit. This mode is suitable for random addition, deletion, modification, and query operations and is good for queries by row. However, if only a few attributes in a row need to be queried, row-oriented storage has to traverse all rows and then filter out the queried attributes. When there are many attributes in a table, the query is low. Although optimization solutions such as indexes can speed up queries in OLTP application scenarios, they still cannot process massive OLAP workloads.

ClickHouse uses column-oriented storage. Data in tables is continuously stored in storage media and uses a column as a logical storage unit. Column-oriented storage is suitable for concurrent data processing using Single Instruction Multiple Data (SIMD). It overcomes the shortcomings of row-oriented storage. Especially when many attributes are used, column-oriented storage greatly speeds up queries. Column-oriented storage ensures that adjacent data types are the same, which enables a great data compression ratio for compression.

![](./assets/figure3.png)

- Performance

The following table lists the performance test data (volume: 100 million) officially released by Yandex. The three data records from top to bottom indicate the query response time of Cold Cache, Second Round, and Third Round. ClickHouse was far ahead of that of major database engines. ClickHouse was more than 600 times better than MySQL.

Note: The data was obtained from a single node: 2 x Intel (R) Xeon (R) CPU E5-2650 v2 @ 2.60 GHz; 128 GiB RAM; mdRAID-5 on 8 6TB SATA HDD; ext4.

![](./assets/figure4.png)

## 4. TaurusDB HTAP Analysis

Although ClickHouse has such a robust performance, it still faces some difficulties in data type support and full replication performance. There are some performance issues related to the engine design, for example, query performance is low due to FINAL deduplication. User experience is poor.

- Full, parallel replication

The Materialize MySQL engine subscribes to MySQL data by consuming binlogs. The data synchronization process consists of three steps: Check whether the source MySQL parameters comply with required requirements; perform a full replication; perform an incremental replication. In ClickHouse, a full replication is performed based on a single thread. When there is a lot of data, the replication latency can be excessive. TaurusDB HTAP analysis performs a full replication based on multiple threads, improving by 8 to 10 times on average than ClickHouse.

![](./assets/figure5.png)

- Multi-Version Concurrency Control (MVCC) and snapshots

By default, the MaterializeMySQL engine adds two hidden fields during DDL conversion: **_sign** (value **-1**: DELETE; value **1**: INSERT/UPDATE) and **_version** (data version). The following figure shows the DDLs of the same table using MySQL and ClickHouse.
```text
1  Create Table: CREATE TABLE `runoob_tbl` (
2  `runoob_id` int unsigned NOT NULL AUTO_INCREMENT,
3  PRIMARY KEY (`runoob_id`)
4  ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8
5  ---------------------------------------------------------------
6  ATTACH TABLE _ UUID '14dbff59-930e-4aa8-9f20-ccfddaf78077'
7  (
8  `runoob_id` UInt32,
9 `_sign` Int8 MATERIALIZED 1, /// **_sign** field
10 `_version` UInt64 MATERIALIZED 1 /// **_version** field
```
The Materialize MySQL engine does not provide transaction consistency views of MySQL data. Data rows are synchronized to ClickHouse using batch insertion. ClickHouse uses ReplacingMergeTree as its underlying engine. If data is modified, FINAL (similar to GROUP BY) operations can remove the duplicates to obtain the latest data and deleted rows are hidden. However, when there is a lot of data, the performance of FINAL operations is often not satisfactory.

To ensure transaction consistency, TaurusDB HTAP analysis provides four isolation levels. Users can select an isolation level as needed. HTAP analysis provides snapshots to optimize the query performance caused by FINAL.

```text
1. **read_uncommitted**: MVCC is not supported. It may cause dirty reads.
2. **read_committed**: MVCC (including subquery) is supported. The latest committed data can be read.
3. **query_snapshot**: Snapshots are used for queries, which avoid the MERGE overhead in SELECT + FINAL queries.
4. **query _raw**: All raw data (including data from deleted and updated versions) is returned.
```
- FINAL performance optimization

ClickHouse's ReplacingMergeTree performs MERGE operations based on certain rules. To obtain the most current data, FINAL operations are required to remove deduplication. TaurusDB HTAP analysis optimizes FINAL operations of the ReplacingMergeTree engine in terms of indexes and filtering policies. Even if HTAP analysis does not depend on MVCC and snapshots, it can deliver powerful queries.

## 5. Compatibility and Stability of TaurusDB HTAP Analysis

- Data Types

There is a mapping between the basic data types of MySQL and ClickHouse (For details, see the following table). ClickHouse converts its unsupported MySQL data types to String types for storage. MySQL converts its unsupported ClickHouse data types to MYSQL_TYPE_STRING types for storage. However, there are some data types used in actual application scenarios are not supported by ClickHouse, but TaurusDB HTAP analysis supports common data types, such as BIT, TIME, and YEAR.

![](./assets/figure6.png)

- Synchronization of tables that contain unique keys

The MaterializeMySQL engine only supports the synchronization of tables that contain primary keys. In actual production environments, some tables may have unique keys but no primary keys. It is necessary to support the synchronization of such tables. TaurusDB HTAP analysis can process tables that only contain Unique Keys (NOT NULL), which are processed separately, and use Unique Keys for partitioning.

- Reconnection mechanism after replication interruption

A full data replication usually involves a lot of data and takes a long time, so there is more of a chance that the replication is interrupted (for example, if there are network faults or MySQL server breakdowns). In this case, ClickHouse stops the synchronization of the current database and returns an error. To improve the stability of data synchronization, TaurusDB HTAP analysis provides a reconnection mechanism for MaterializeMySQL. When a full replication is interrupted, residual data is cleared and the connection is re-established after a certain interval. When an incremental replication is interrupted, residual data is not cleared because the incremental replication is performed based on binlog events. Any data that has been successfully synchronized does not need to be synchronized again, after the connection is re-established, the latest synchronization point is found based on the global GTID and the synchronization resumes.

- A better way to handle exceptions

TaurusDB HTAP analysis not only delivers new features such as MVCC, snapshots and parallel replication, but also provides a better way to handle exceptions at the kernel layer. Take full parallel replication as an example. HTAP analysis maintains exception handling information and stacks for each parallel thread. This makes it easier to help users find the root causes of potential issues.

## 6. Unique Features of HTAP Analysis

- Show Slave Status

HTAP analysis provides the SHOW SLAVE STATUS command to help users intuitively obtain the database synchronization status of the Materialize MySQL engine. Users can view synchronization thread status, binlog location, GITD, and Second Behind Master for O&M.

- ALTER Database

ALTER DATABASE provides the following operations for MaterializeMySQL engine:
```text
ALTER DATABASE db MODIFY SETTING... //Modify database-level settings.
2. ALTER DATABASE db ADD TABLE OVERRIDE tbl...
ALTER DATABASE db MODIFY TABLE OVERRIDE tbl...
4. ALTER DATABASE db DROP TABLE ...
```
- Overwrite

TaurusDB HTAP analysis adds an overwrite function to the Materialize MySQL engine. Users can overwrite columns of a specified table and add new columns, add indexes, and overwrite PARTITION BY or SAMPLE BY fields. For details, see the following example.
```text
1 CREATE DATABASE test
2 ENGINE=MaterializeMySQL('host:port', 'db', 'user', 'pw')
3 TABLE OVERRIDE table1 (
4 _staged UInt8 MATERIALIZED 1 //Add the MATERIALIZED column. The type is UInt8.
5 )
6 PARTITION BY (...) //Overwrite the partition field.
```
- RANGE partitioning

Data partitioning is an important way to improve database performance. In ClickHouse, fields are first partitioned by date type. If there is no date type, fields are hashed and partitioned by the smaller type length. The partitioning policy of the ClickHouse is different from that of the MySQL. To support the partitioning policy of the MySQL, TaurusDB HTAP analysis supports RANGE partitioning. If a statement for table creation does not contain RANGE partitions, the ClickHouse policy is used by default.

- Trustlist/Blocklist filtering

The MaterializeMySQL engine supports database-level data synchronization, which means that all tables in the database are copied by default. However, in some scenarios, users do not have to copy all the tables, or some tables are not suitable for replication (for example, tables without primary keys or NOT NULL unique keys). TaurusDB HTAP analysis provides a trustlist or blocklist to allow users to select which tables are replicated.
```text
1 CREATE DATABASE test
2 ENGINE = MaterializeMySQL('host:port', 'db', 'user', 'pw')
3 SETTINGS black_list='T1,T2' //Add T1 and T2 to the blocklist.
```
## 7. Scenario Example

So far, we have taken a look at the advantages of TaurusDB HTAP analysis. Low lets learn a little about how to use HTAP analysis to resolve user issues.

![](./assets/figure7.png)

Take the MySQL engine and TaurusDB HTAP analysis as an example. Users can obtain complete MySQL transaction assurance and enjoy the amazing performance of TaurusDB HTAP analysis. Users obtain data from different channels and load the data to the MySQL engine. TaurusDB HTAP analysis is used as the standby database of MySQL to synchronize data in real time and provide efficient data analysis capabilities.

- High effectiveness

The combination of TaurusDB HTAP analysis and the MySQL engine lets you complete data synchronization within seconds.

- Data compression

TaurusDB HTAP analysis uses column-oriented storage, so it delivers an excellent data compression ratio and helps users greatly reduce storage costs.

- Historical backups

TaurusDB HTAP analysis has lower storage costs than MySQL and can be used to store historical backups in some scenarios.

- Tiered storage

To further reduce storage costs, TaurusDB HTAP analysis provides ESSD+EVS+OBS tiered storage. Hot data, warm data, and cold data are stored in different storage media.

## 8. Summary

Although HTAP is not a very new concept, it is becoming popular in recent years as the line between TP and AP has become increasingly blurred. The continuous iteration and upgrade of users requirements for data processing and consumption also creates more opportunities for HTAP development. TaurusDB HTAP analysis made a series of optimizations based on ClickHouse to solve problems encountered in production environments and improve user experience. HTAP competition will become increasingly fierce in the future, which is both a challenge and an opportunity for TaurusDB HTAP analysis. TaurusDB HTAP analysis will continue to provide efficient solutions to accelerate massive data processing and digital transformation.

**Note: This function is available for commercial use. If you want to experience this function, please leave a message in the comment area.**



## Change History

| Released   | Issue| Description    |
|------------| -------- | ------------ |
| 2023-12-20 | 1.0      | First official release|
