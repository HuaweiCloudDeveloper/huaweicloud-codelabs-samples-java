### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/GaussDBforopenGauss/doc?api=ListDbFlavors).

2.In this example, you can query database specifications.

3.Based on the returned result, you can learn about the database specifications supported in the current region, including the CPU, memory, array library engine version, and performance specifications.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. 
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. 
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-gaussdbforopengauss. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-gaussdbforopengauss</artifactId>
    <version>3.1.124</version>
</dependency>
```

### Code example
The following code shows how to query database specifications:
``` java
package com.huawei.gaussdb;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.gaussdbforopengauss.v3.GaussDBforopenGaussClient;
import com.huaweicloud.sdk.gaussdbforopengauss.v3.model.ListDbFlavorsRequest;
import com.huaweicloud.sdk.gaussdbforopengauss.v3.model.ListDbFlavorsResponse;
import com.huaweicloud.sdk.gaussdbforopengauss.v3.region.GaussDBforopenGaussRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListDbFlavors {
    private static final Logger logger = LoggerFactory.getLogger(ListDbFlavors.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential listDbFlavorsAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // Create a service client and set the corresponding region to CN_NORTH_4.
        GaussDBforopenGaussClient client = GaussDBforopenGaussClient.newBuilder()
            .withCredential(listDbFlavorsAuth)
            .withRegion(GaussDBforopenGaussRegion.CN_NORTH_4)
            .build();
        // Construction request
        ListDbFlavorsRequest request = new ListDbFlavorsRequest();
        try {
            // Obtaining Results
            ListDbFlavorsResponse response = client.listDbFlavors(request);
            // Print Results
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
#### ListDbFlavors
```
{
  "total": 10,
  "flavors": [
    {
      "vcpus": "1",
      "ram": 2,
      "spec_code": "gaussdb.opengauss.ee.dn.m6.4xlarge.8.in",
      "name": "GaussDB",
      "version": "1.0",
      "availability_zone": [
        "az1",
        "az2"
      ],
      "az_status": {
        "az1": "normal",
        "az2": "normal"
      },
      "group_type": "normal"
    }
  ]
}
```
### Change History

 Released On  |  Issue   | Description
 :----: |:---:| :------:  
 2024/12/04 | 1.0 | This document is released for the first time.