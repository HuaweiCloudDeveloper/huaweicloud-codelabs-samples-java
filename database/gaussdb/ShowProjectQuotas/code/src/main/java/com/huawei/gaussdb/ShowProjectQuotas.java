package com.huawei.gaussdb;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.gaussdbforopengauss.v3.GaussDBforopenGaussClient;
import com.huaweicloud.sdk.gaussdbforopengauss.v3.model.ShowProjectQuotasRequest;
import com.huaweicloud.sdk.gaussdbforopengauss.v3.model.ShowProjectQuotasResponse;
import com.huaweicloud.sdk.gaussdbforopengauss.v3.region.GaussDBforopenGaussRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowProjectQuotas {
    private static final Logger logger = LoggerFactory.getLogger(ShowProjectQuotas.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential showProjectQuotasAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // Create a service client and set the corresponding region to CN_NORTH_4.
        GaussDBforopenGaussClient client = GaussDBforopenGaussClient.newBuilder()
            .withCredential(showProjectQuotasAuth)
            .withRegion(GaussDBforopenGaussRegion.CN_NORTH_4)
            .build();
        // Construction request
        ShowProjectQuotasRequest request = new ShowProjectQuotasRequest();
        try {
            // Obtaining Results
            ShowProjectQuotasResponse response = client.showProjectQuotas(request);
            // Print Results
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}