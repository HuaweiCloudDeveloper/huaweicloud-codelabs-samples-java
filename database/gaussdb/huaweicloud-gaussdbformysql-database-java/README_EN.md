## 1. Introduction
TaurusDB is a MySQL-compatible, enterprise-grade distributed database. Data functions virtualization (DFV) is used to decouple storage from compute and can auto scale up to 128 TB per instance. With TaurusDB, there is no need for sharding, and no need to worry about data loss. It provides superior performance of commercial databases at the price of open-source databases. This example shows how to grant database permissions to a database user using a Java SDK.

## 2. Flowchart
![Granting permissions to a database user](assets/database_en.png)

## 3. Prerequisites

1. You have created [a HUAWEI ID](https://id5.cloud.huawei.com/CAS/portal/userRegister/regbyphone.html?&lang=en-us) and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?locale=en-us#/accountindex/realNameAuth).

2. You have obtained a Huawei Cloud SDK. You can also install a Java SDK.

3. You have obtained the access key ID (AK) and secret access key (SK) of your HUAWEI ID. You can create and view your AK/SK on the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html).

4. You have set up the development environment with Java JDK 1.8 or later.


## 4. Obtaining and Installing an SDK
You can obtain and install an SDK in Maven mode. Download and install Maven in your operating system (OS). After the installation is complete, add the required dependencies to the **pom.xml** file of the Java project.

For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter).
```xml
 <dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-gaussdb</artifactId>
    <version>3.1.1</version>
</dependency>
```

## 5. Key Code Snippet
Granting permissions to a database user using the SDK:

```java

public class DatabaseDemo {
    private static final Logger logger = LoggerFactory.getLogger(DatabaseDemo.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security purposes, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK**.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = "<YOUR INSTANCE_ID>";
        String regionId = "<YOUR REGION_ID>";

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        GaussDBClient client = GaussDBClient.newBuilder()
                .withCredential(auth)
                .withRegion(GaussDBRegion.valueOf(regionId))
                .build();

        // Queries databases.
        listDatabases(client, instanceId);

        // Configures the name and character set of a database.
        String dbName = "<YOUR DATABASE_NAME>";
        String dbCharacterSet = "<YOUR DATABASE_CHARACTERSET>";
        // Creates a database.
        createDatabase(client, instanceId, dbName, dbCharacterSet);

        // Queries database users.
        ListGaussMySqlDatabaseUserResponse listGaussMySqlDatabaseUserResponse = listDatabaseUsers(client, instanceId);

        if (listGaussMySqlDatabaseUserResponse != null) {
            // Obtains the first database user from the queried database users.
            String userName = listGaussMySqlDatabaseUserResponse.getUsers().get(0).getName();
            String host = listGaussMySqlDatabaseUserResponse.getUsers().get(0).getHost();
            //Authorizes the database user. You can set readonly to true or false. false is used in the example.
            boolean readonly = false;
            addDatabasePermission(client, instanceId, userName, host, dbName, readonly);
        }

    }

    private static void listDatabases(GaussDBClient client, String instanceId) {
        ListGaussMySqlDatabaseRequest request = new ListGaussMySqlDatabaseRequest();
        request.withInstanceId(instanceId);
        try {
            ListGaussMySqlDatabaseResponse response = client.listGaussMySqlDatabase(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

    private static void createDatabase(GaussDBClient client, String instanceId, String dbName, String dbCharacterSet) {
        CreateGaussMySqlDatabaseRequest request = new CreateGaussMySqlDatabaseRequest();
        CreateGaussMySqlDatabaseRequestBody body = new CreateGaussMySqlDatabaseRequestBody();
        body.withDatabases(Collections.singletonList(new CreateGaussMySqlDatabase()
                .withName(dbName)
                .withCharacterSet(dbCharacterSet)));
        request.withInstanceId(instanceId)
                .withBody(body);
        try {
            CreateGaussMySqlDatabaseResponse response = client.createGaussMySqlDatabase(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }

    }

    private static ListGaussMySqlDatabaseUserResponse listDatabaseUsers(GaussDBClient client, String instanceId) {
        ListGaussMySqlDatabaseUserRequest request = new ListGaussMySqlDatabaseUserRequest();
        request.withInstanceId(instanceId);
        try {
            ListGaussMySqlDatabaseUserResponse response = client.listGaussMySqlDatabaseUser(request);
            logger.info(response.toString());
            return response;
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return null;
    }

    private static void addDatabasePermission(GaussDBClient client, String instanceId, String userName, String host, String dbName, boolean readonly) {
        AddDatabasePermissionRequest request = new AddDatabasePermissionRequest();
        request.withInstanceId(instanceId);
        GrantDatabasePermissionRequestBody body = new GrantDatabasePermissionRequestBody();
        List<DatabasePermission> databases = new ArrayList<>();
        databases.add(new DatabasePermission()
                .withName(dbName)
                .withReadonly(false));
        GrantDatabasePermission grantDatabasePermission = new GrantDatabasePermission()
                .withName(userName)
                .withHost(host)
                .withDatabases(databases);
        List<GrantDatabasePermission> grantDatabasePermissonList = new ArrayList<>();
        grantDatabasePermissonList.add(grantDatabasePermission);
        body.withUsers(grantDatabasePermissonList);
        request.withBody(body);
        try {
            AddDatabasePermissionResponse response = client.addDatabasePermission(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
    }

}




```
## 6. Sample Return Values
- Return value from the API (ShowGaussMySqlBackupPolicy) for querying databases
```
class ListGaussMySqlDatabaseResponse {
    databases: [class ListGaussMysqlDatabaseInfo {
        name: database_final
        charset: gbk
        users: null
    }]
    totalCount: 1
}
```
- Return value from the API (CreateGaussMySqlDatabase) for creating a database
```
class CreateGaussMySqlDatabaseResponse {
    jobId: f1a44bd9-b0e6-45b6-9aab-2ddbf7241a82
}
```
- Return value of the API (ListGaussMySqlDatabaseUser) for querying database users
```
class ListGaussMySqlDatabaseUserResponse {
    users: [class ListGaussMySqlDatabaseUser {
        name: user_test_1
        host: 127.0.0.1
        databases: []
    }, class ListGaussMySqlDatabaseUser {
        name: user_test_2
        host: %
        databases: []
    }]
    totalCount: 2
}
```
- Return value from the API (AddDatabasePermission) for granting permissions to a database user
```
class AddDatabasePermissionResponse {
    jobId: 42cd2303-1d53-4ace-810c-126e89084a9e
}
```

## 7. References

For details, see [Querying Databases] (https://support.huaweicloud.com/intl/en-us/api-gaussdbformysql/ListGaussMySqlDatabase.html).
You can directly run and debug the API in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/GaussDB/doc?api=ListGaussMySqlDatabase).

For details, see [Creating a Database] (https://support.huaweicloud.com/intl/en-us/api-gaussdbformysql/CreateGaussMySqlDatabase.html).
You can directly run and debug the API in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/GaussDB/doc?api=CreateGaussMySqlDatabase).

For details, see [Querying Database Users] (https://support.huaweicloud.com/intl/en-us/api-gaussdbformysql/ListGaussMySqlDatabaseUser.html).
You can directly run and debug the API in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/GaussDB/doc?api=ListGaussMySqlDatabaseUser).

For details, see [Granting Permissions to a Database User] (https://support.huaweicloud.com/intl/en-us/api-gaussdbformysql/AddDatabasePermission.html).
You can directly run and debug the API in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/GaussDB/doc?api=AddDatabasePermission).

## Change History
| Released On | Issue|   Description  |
|:-----------:| :------: | :----------: |
| 2023-11-29  |   1.0    | First official release|
