package com.huaweicloud.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.*;
import com.huaweicloud.sdk.rds.v3.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.ArrayList;

public class BatchRestoreDatabase {
    private static final Logger logger = LoggerFactory.getLogger(BatchRestoreDatabase.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量CLOUD_SDK_AK和CLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.valueOf("cn-south-1"))
                .build();
        BatchRestoreDatabaseRequest request = new BatchRestoreDatabaseRequest();
        PostgreSQLRestoreDatabaseRequest body = new PostgreSQLRestoreDatabaseRequest();
        List<RestoreDatabaseInfo> listInstancesDatabases = new ArrayList<>();
        listInstancesDatabases.add(
                new RestoreDatabaseInfo()
                        .withOldName("oldName")
                        .withNewName("newName")
        );
        List<RestoreDatabaseInstance> listbodyInstances = new ArrayList<>();
        listbodyInstances.add(
                new RestoreDatabaseInstance()
                        .withRestoreTime(1699323939000L) // 恢复时间，采用毫秒时间戳
                        .withInstanceId("instanceId")
                        .withDatabases(listInstancesDatabases)
        );
        body.withInstances(listbodyInstances);
        request.withBody(body);
        try {
            BatchRestoreDatabaseResponse response = client.batchRestoreDatabase(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            logger.error(e.toString());
        }
    }
}