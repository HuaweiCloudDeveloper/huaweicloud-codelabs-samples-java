package com.huaweicloud.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.*;
import com.huaweicloud.sdk.rds.v3.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CreateInstance {
    private static final Logger logger = LoggerFactory.getLogger(CreateInstance.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量CLOUD_SDK_AK和CLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.CN_SOUTH_1)
                .build();

        CreateInstanceRequest request = new CreateInstanceRequest();
        InstanceRequest body = new InstanceRequest();
        Volume volumebody = new Volume();
        volumebody.withType(Volume.TypeEnum.fromValue("CLOUDSSD")) // 磁盘种类及大小
                .withSize(40);
        Datastore datastorebody = new Datastore();
        datastorebody.withType(Datastore.TypeEnum.fromValue("MySQL")) // 数据库引擎及版本
                .withVersion("8.0");
        body.withSecurityGroupId("e40955cf-2c78-4aec-8207-9e4a1d5ffdf2"); // 安全组Id
        body.withSubnetId("66b5fc1c-ddf2-49c0-a554-4703c1563d4c"); // 子网Id
        body.withVpcId("74f14b53-b5d8-43d9-8d96-a9c160f6b5a6"); // VpcId
        body.withAvailabilityZone("cn-south-1f"); // 可用区
        body.withRegion("cn-south-1");
        body.withVolume(volumebody);
        body.withFlavorRef("rds.mysql.x1.2xlarge.4"); // 规格码
        body.withDatastore(datastorebody);
        body.withName("instanceName"); // 实例名称
        request.withBody(body);

        try {
            CreateInstanceResponse response = client.createInstance(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            logger.error(e.toString());
        }
    }
}