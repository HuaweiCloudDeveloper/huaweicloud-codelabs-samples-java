package com.huaweicloud.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.*;
import com.huaweicloud.sdk.rds.v3.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SetBackupPolicy {
    private static final Logger logger = LoggerFactory.getLogger(SetBackupPolicy.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量CLOUD_SDK_AK和CLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.valueOf("cn-south-1"))
                .build();
        SetBackupPolicyRequest request = new SetBackupPolicyRequest();
        request.withInstanceId("instanceId");
        SetBackupPolicyRequestBody body = new SetBackupPolicyRequestBody();
        BackupPolicy backupPolicybody = new BackupPolicy();
        backupPolicybody.withKeepDays(10) // 取值范围:0~732。取0值,表示关闭自动备份策略
                .withStartTime("08:15-09:15") // 备份时间段。自动备份将在该时间段内触发。除关闭自动备份策略外必选。格式必须为hh:mm-HH:MM。HH取值必须比hh大1。mm和MM取值必须相同,且取值必须为00、15、30或45。
                .withPeriod("1,2"); // 备份周期配置。自动备份将在每星期指定的天进行。除关闭自动备份策略外必选。取值范围:格式为逗号隔开的数字,数字代表星期。
        body.withBackupPolicy(backupPolicybody);
        request.withBody(body);
        try {
            SetBackupPolicyResponse response = client.setBackupPolicy(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            logger.error(e.toString());
        }
    }
}