### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RDS/debug?api=ListStorageTypes) 中直接运行调试该接口。

2.在本样例中，您可以将数据库恢复到已有实例。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.调用接口前，您需要了解API [认证鉴权](https://support.huaweicloud.com/api-rds/rds_03_0001.html)。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-rds”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rds</artifactId>
    <version>3.1.121</version>
</dependency>
```

### 接口约束
1.源实例和目标实例的数据库类型必须一致，如都是MySQL。
2.恢复RDS for MySQL实例时，目标实例数据库版本必须高于或等于源实例的数据库版本，如MySQL 5.7.25->5.7.27。
3.RDS for MySQL数据库目标实例的存储空间大于或等于源实例的存储空间总大小。
4.不支持跨区域恢复操作。
5.恢复RDS for MySQL数据库到已有实例时，目标实例与原实例表名大小写设置不一致，可能会导致恢复失败。
6.恢复到当前实例时，该接口仅支持实例的数据库引擎MySQL和Microsoft SQL Server。

### 代码示例
以下代码展示如何将数据库恢复到已有实例：
``` java
package com.huaweicloud.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.*;
import com.huaweicloud.sdk.rds.v3.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RestoreExistInstance {
    private static final Logger logger = LoggerFactory.getLogger(RestoreExistInstance.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量CLOUD_SDK_AK和CLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.valueOf("cn-south-1"))
                .build();
        RestoreExistInstanceRequest request = new RestoreExistInstanceRequest();
        RestoreExistingInstanceRequestBody body = new RestoreExistingInstanceRequestBody();
        TargetInstanceRequest targetbody = new TargetInstanceRequest();
        targetbody.withInstanceId("instanceIdTarget");
        /*
        type表示恢复方式，枚举值：
        “backup”，表示使用备份文件恢复，按照此方式恢复时，“backup_id”必选。
        “timestamp”，表示按时间点恢复，按照此方式恢复时，“restore_time”必选，格式为UNIX时间戳，单位是毫秒，时区为UTC。
        */
        RestoreExistingInstanceRequestBodySource sourcebody = new RestoreExistingInstanceRequestBodySource();
        sourcebody.withInstanceId("instanceIdSource")
                .withType(RestoreExistingInstanceRequestBodySource.TypeEnum.fromValue("backup"))
                .withBackupId("backupId");
        body.withTarget(targetbody);
        body.withSource(sourcebody);
        request.withBody(body);
        try {
            RestoreExistInstanceResponse response = client.restoreExistInstance(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            logger.error(e.toString());
        }
    }
}
```

### 返回结果示例
```json
{
  "jobId": "fe76bbd2-bd51-47cf-bdcb-898a4ef69b4a"
}
```

### 修订记录

|    发布日期     | 文档版本 | 修订说明 |
|:-----------:| :------: | :----------: |
| 2024/11/14  | 1.0  | 文档首次发布| 