package com.huaweicloud.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.*;
import com.huaweicloud.sdk.rds.v3.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SetAutoEnlargePolicy {
    private static final Logger logger = LoggerFactory.getLogger(SetAutoEnlargePolicy.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量CLOUD_SDK_AK和CLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.valueOf("cn-south-1"))
                .build();

        SetAutoEnlargePolicyRequest request = new SetAutoEnlargePolicyRequest();
        request.withInstanceId("instanceId");
        CustomerModifyAutoEnlargePolicyReq body = new CustomerModifyAutoEnlargePolicyReq();
        body.withStepPercent(20); // 每次自动扩容当前存储空间的百分比。取值范围为5%~50%。
        body.withTriggerThreshold(CustomerModifyAutoEnlargePolicyReq.TriggerThresholdEnum.NUMBER_10); // 可用存储空间百分比，小于等于此值或者为10GB时触发扩容。
        body.withLimitSize(1000); // 扩容上限，单位GB。取值范围40GB~4000GB，需要大于等于实例当前存储空间总大小。
        body.withSwitchOption(true);
        request.withBody(body);

        try {
            SetAutoEnlargePolicyResponse response = client.setAutoEnlargePolicy(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            logger.error(e.toString());
        }
    }
}