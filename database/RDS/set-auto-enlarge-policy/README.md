### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RDS/debug?api=SetAutoEnlargePolicy) 中直接运行调试该接口。

2.在本样例中，您可以设置自动扩容策略。设置实例存储空间自动扩容策略，按扩容量扣除存储费用。

3.可用存储空间小于等于设置值或者为10GB时，自动扩容当前存储空间的20%（如果是非10倍数则向上取整）。如果自定义了扩容步长，则按自定义的步长扩容，如需自定义扩容步长，请联系客服申请。

4.设置只读实例自动扩容与主实例自动扩容互不影响，对只读实例设置自动扩容时，可选择大于或等于主实例的存储空间。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.调用接口前，您需要了解API [认证鉴权](https://support.huaweicloud.com/api-rds/rds_03_0001.html)。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-rds”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rds</artifactId>
    <version>3.1.121</version>
</dependency>
```

### 接口约束
1.账户余额必须大于等于0元，余额不足会导致自动扩容失败。
2.该接口仅支持RDS for MySQL和RDS for PostgreSQL磁盘大小为40GB及以上的云盘实例（即存储类型为SSD云盘或极速型SSD）。
3.实例在进行规格变更、内核小版本升级、备机迁移、重启时，不能进行自动扩容。
4.包周期实例存在未完成订单时，不会自动扩容。
5.实例状态为“正常”和“存储空间满”时可以进行扩容。最大可自动扩容至4000GB。

### 代码示例
以下代码展示如何设置自动扩容策略：
``` java
package com.huaweicloud.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.*;
import com.huaweicloud.sdk.rds.v3.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SetAutoEnlargePolicy {
    private static final Logger logger = LoggerFactory.getLogger(SetAutoEnlargePolicy.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量CLOUD_SDK_AK和CLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.valueOf("cn-south-1"))
                .build();

        SetAutoEnlargePolicyRequest request = new SetAutoEnlargePolicyRequest();
        request.withInstanceId("instanceId");
        CustomerModifyAutoEnlargePolicyReq body = new CustomerModifyAutoEnlargePolicyReq();
        body.withStepPercent(20); // 每次自动扩容当前存储空间的百分比。取值范围为5%~50%。
        body.withTriggerThreshold(CustomerModifyAutoEnlargePolicyReq.TriggerThresholdEnum.NUMBER_10); // 可用存储空间百分比，小于等于此值或者为10GB时触发扩容。
        body.withLimitSize(1000); // 扩容上限，单位GB。取值范围40GB~4000GB，需要大于等于实例当前存储空间总大小。
        body.withSwitchOption(true);
        request.withBody(body);

        try {
            SetAutoEnlargePolicyResponse response = client.setAutoEnlargePolicy(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            logger.error(e.toString());
        }
    }
}
```

### 返回结果示例
无

### 修订记录

|    发布日期     | 文档版本 | 修订说明 |
|:-----------:| :------: | :----------: |
| 2024/11/14  | 1.0  | 文档首次发布| 