package com.huaweicloud.demo;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.*;
import com.huaweicloud.sdk.rds.v3.model.*;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class CreateDbUser {
    private static final Logger logger = LoggerFactory.getLogger(CreateDbUser.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量CLOUD_SDK_AK和CLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.valueOf("cn-south-1"))
                .build();
        CreateDbUserRequest request = new CreateDbUserRequest();
        request.withInstanceId("instanceId");
        UserForCreation body = new UserForCreation();
        body.withPassword("password"); // 非空，长度8~32个字符，至少包含大写字母、小写字母、数字、特殊字符~!@#$%^*-_=+?,()&三种字符的组合，不能和账号名或账号名的逆序相同。
        body.withName("name"); // 数据库账号名称在1到32个字符之间，由字母、数字、中划线或下划线组成，不能包含其他特殊字符
        request.withBody(body);
        try {
            CreateDbUserResponse response = client.createDbUser(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            logger.error(e.toString());
        }
    }
}