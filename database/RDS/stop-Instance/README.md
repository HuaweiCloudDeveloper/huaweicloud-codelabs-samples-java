### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RDS/debug?api=StopInstance) 中直接运行调试该接口。

2.关闭数据库实例。停止实例以节省费用，在停止数据库实例后，支持手动重新开启实例。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.调用接口前，您需要了解API [认证鉴权](https://support.huaweicloud.com/api-rds/rds_03_0001.html)。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-rds”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rds</artifactId>
    <version>3.1.121</version>
</dependency>
```
### 接口约束
1.仅支持停止存储类型为超高IO、SSD云盘、极速型SSD的按需实例，专属云RDS不支持停止实例。
2.已停止的实例被删除后不会进入回收站。
3.停止主实例时，如果存在只读实例，会同时停止只读实例。不支持单独停止只读实例。对于华东-上海一区域，主实例和只读实例均默认停止十五天。对于除华东-上海一外的其他区域，主实例和只读实例均默认停止七天。
3.1.对于华东-上海一区域，实例默认停止十五天，如果您在十五天后未手动开启实例，数据库实例将于十五天后的下一个可维护时间段内自动启动。
3.2.对于除华东-上海一外的其他区域，实例默认停止七天，如果您在七天后未手动开启实例，数据库实例将于七天后的下一个可维护时间段内自动启动。
4.实例停止后，虚拟机（VM）停止收费，其余资源包括弹性公网IP（EIP）、存储资源、备份正常计费。
5.按需付费的数据库实例停止实例后，可能会出现由于资源不足引起开启失败，如遇到无法开启的情况请联系客服人员处理。
6.实例在以下状态不能执行停止实例： 创建、重启、扩容、变更规格、恢复、修改端口等不能进行此操作。

### 代码示例
以下代码展示如何关闭一个数据库实例：
``` java
package com.huaweicloud.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.*;
import com.huaweicloud.sdk.rds.v3.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class StopInstance {
    private static final Logger logger = LoggerFactory.getLogger(StopInstance.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量CLOUD_SDK_AK和CLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.CN_SOUTH_1)
                .build();

        StopInstanceRequest request = new StopInstanceRequest();
        request.withInstanceId("instanceId");

        try {
            StopInstanceResponse response = client.stopInstance(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            logger.error(e.toString());
        }
    }
}
```

### 返回结果示例
```json
{
  "job_id": "04efe8e2-9255-44ae-a98b-d87cae411890"
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024/11/18 | 1.0  | 文档首次发布| 