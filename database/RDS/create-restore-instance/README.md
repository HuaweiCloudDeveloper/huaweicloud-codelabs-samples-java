### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RDS/debug?api=CreateRestoreInstance) 中直接运行调试该接口。

2.恢复到新实例

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.调用接口前，您需要了解API [认证鉴权](https://support.huaweicloud.com/api-rds/rds_03_0001.html)。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-rds”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rds</artifactId>
    <version>3.1.121</version>
</dependency>
```

### 接口约束
1.原实例和目标实例的数据库类型必须一致，如都是MySQL。
2.原实例和目标实例的数据库版本约束为：原实例和目标实例的数据库版本必须一致。
3.数据库目标实例需大于或等于原实例的数据卷总大小。

### 代码示例
以下代码展示如何恢复到新实例：
``` java
package com.huaweicloud.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.*;
import com.huaweicloud.sdk.rds.v3.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CreateRestoreInstance {
    private static final Logger logger = LoggerFactory.getLogger(CreateRestoreInstance.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量CLOUD_SDK_AK和CLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.valueOf("cn-south-1"))
                .build();
        CreateRestoreInstanceRequest request = new CreateRestoreInstanceRequest();
        CreateRestoreInstanceRequestBody body = new CreateRestoreInstanceRequestBody();
        RestorePoint restorePointbody = new RestorePoint();
        Volume volumebody = new Volume();
        /*
        * 磁盘类型
        * COMMON,表示SATA。HIGH,表示SAS。ULTRAHIGH,表示SSD。
        * ULTRAHIGHPRO,表示SSD尊享版,仅支持超高性能型尊享版(需申请权限)。
        * CLOUDSSD,表示SSD云盘,仅支持通用型和独享型规格实例。
        * LOCALSSD,表示本地SSD。
        * ESSD,表示极速型SSD,仅支持独享型规格实例。
        */
        volumebody.withType(Volume.TypeEnum.fromValue("CLOUDSSD"))
                .withSize(40);
        body.withAvailabilityZone("cn-north-4a"); // 可用区ID。对于数据库实例类型不是单机的实例,需要分别为实例所有节点指定可用区,并用逗号隔开
        body.withVolume(volumebody);
        body.withFlavorRef("rds.mysql.s1.large"); // 使用查询数据库规格接口响应消息中的flavors字段中“spec_code”获取且对应az_status为“在售”状态。
        body.withName("instanceName");
        /*
        type表示恢复方式，枚举值：
        “backup”，表示使用备份文件恢复，按照此方式恢复时，“backup_id”必选。
        “timestamp”，表示按时间点恢复，按照此方式恢复时，“restore_time”必选，格式为UNIX时间戳，单位是毫秒，时区为UTC。
        */
        restorePointbody.withInstanceId("instanceId") // 原实例id
                .withType("backup")
                .withBackupId("backupId");
        body.withRestorePoint(restorePointbody);
        request.withBody(body);
        try {
            CreateRestoreInstanceResponse response = client.createRestoreInstance(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            logger.error(e.toString());
        }
    }
}
```

### 返回结果示例
```json
{
  "name": "targetInst",
  "availability_zone": "bbb,ccc",
  "ha": {
    "mode": "ha",
    "replication_mode": "async"
  },
  "flavor_ref": "rds.mysql.s1.large",
  "volume": {
    "type": "ULTRAHIGH",
    "size": 40
  },
  "region": "aaa",
  "disk_encryption_id": "2gfdsh-844a-4023-a776-fc5c5fb71fb4",
  "vpc_id": "490a4a08-ef4b-44c5-94be-3051ef9e4fce",
  "subnet_id": "0e2eda62-1d42-4d64-a9d1-4e9aa9cd994f",
  "data_vip": "192.168.0.147",
  "security_group_id": "2a1f7fc8-3307-42a7-aa6f-42c8b9b8f8c5",
  "backup_strategy": {
    "keep_days": 2,
    "start_time": "19:00-20:00"
  },
  "password": "Demo@12345678",
  "configuration_id": "52e86e87445847a79bf807ceda213165pr01",
  "enterprise_project_id": "ba1f7fc8-3307-42a7-aa6f-42c8b9b8f85c",
  "time_zone": "UTC+04:00",
  "restore_point": {
    "instance_id": "d8e6ca5a624745bcb546a227aa3ae1cfin01",
    "type": "backup",
    "backup_id": "2f4ddb93-b901-4b08-93d8-1d2e472f30fe"
  }
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024/12/28 | 1.0  | 文档首次发布| 