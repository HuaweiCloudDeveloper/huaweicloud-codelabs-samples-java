package com.huaweicloud.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.*;
import com.huaweicloud.sdk.rds.v3.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CreateRestoreInstance {
    private static final Logger logger = LoggerFactory.getLogger(CreateRestoreInstance.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量CLOUD_SDK_AK和CLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.valueOf("cn-south-1"))
                .build();
        CreateRestoreInstanceRequest request = new CreateRestoreInstanceRequest();
        CreateRestoreInstanceRequestBody body = new CreateRestoreInstanceRequestBody();
        RestorePoint restorePointbody = new RestorePoint();
        Volume volumebody = new Volume();
        /*
        * 磁盘类型
        * COMMON,表示SATA。HIGH,表示SAS。ULTRAHIGH,表示SSD。
        * ULTRAHIGHPRO,表示SSD尊享版,仅支持超高性能型尊享版(需申请权限)。
        * CLOUDSSD,表示SSD云盘,仅支持通用型和独享型规格实例。
        * LOCALSSD,表示本地SSD。
        * ESSD,表示极速型SSD,仅支持独享型规格实例。
        */
        volumebody.withType(Volume.TypeEnum.fromValue("CLOUDSSD"))
                .withSize(40);
        body.withAvailabilityZone("cn-north-4a"); // 可用区ID。对于数据库实例类型不是单机的实例,需要分别为实例所有节点指定可用区,并用逗号隔开
        body.withVolume(volumebody);
        body.withFlavorRef("rds.mysql.s1.large"); // 使用查询数据库规格接口响应消息中的flavors字段中“spec_code”获取且对应az_status为“在售”状态。
        body.withName("instanceName");
        /*
        type表示恢复方式，枚举值：
        “backup”，表示使用备份文件恢复，按照此方式恢复时，“backup_id”必选。
        “timestamp”，表示按时间点恢复，按照此方式恢复时，“restore_time”必选，格式为UNIX时间戳，单位是毫秒，时区为UTC。
        */
        restorePointbody.withInstanceId("instanceId") // 原实例id
                .withType("backup")
                .withBackupId("backupId");
        body.withRestorePoint(restorePointbody);
        request.withBody(body);
        try {
            CreateRestoreInstanceResponse response = client.createRestoreInstance(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            logger.error(e.toString());
        }
    }
}