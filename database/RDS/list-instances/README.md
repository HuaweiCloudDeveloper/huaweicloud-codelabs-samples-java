### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RDS/debug?api=ListInstances) 中直接运行调试该接口。

2.在本样例中，您可以查询数据库实例列表。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.调用接口前，您需要了解API [认证鉴权](https://support.huaweicloud.com/api-rds/rds_03_0001.html)。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-rds”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rds</artifactId>
    <version>3.1.121</version>
</dependency>
```

### 代码示例
以下代码展示如何查询数据库实例列表：
``` java
package com.huaweicloud.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.*;
import com.huaweicloud.sdk.rds.v3.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ListInstances {
    private static final Logger logger = LoggerFactory.getLogger(ListInstances.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量CLOUD_SDK_AK和CLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.valueOf("cn-south-1"))
                .build();

        ListInstancesRequest request = new ListInstancesRequest();

        try {
            ListInstancesResponse response = client.listInstances(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            logger.error(e.toString());
        }
    }
}
```

### 返回结果示例
```json
{
  "instances": [
    {
      "id": "ed7cc6166ec24360a5ed5c5c9c2ed726in01",
      "status": "ACTIVE",
      "name": "mysql-0820-022709-01",
      "port": 3306,
      "enable_ssl": true,
      "type": "Single",
      "region": "aaa",
      "datastore": {
        "type": "MySQL",
        "version": "5.7"
      },
      "created": "2018-08-20T02:33:49+0800",
      "updated": "2018-08-20T02:33:50+0800",
      "volume": {
        "type": "ULTRAHIGH",
        "size": 100
      },
      "nodes": [
        {
          "id": "06f1c2ad57604ae89e153e4d27f4e4b8no01",
          "name": "mysql-0820-022709-01_node0",
          "role": "master",
          "status": "ACTIVE",
          "availability_zone": "bbb"
        }
      ],
      "private_ips": [
        "192.168.0.142"
      ],
      "public_ips": [
        "10.154.219.187",
        "10.154.219.186"
      ],
      "public_dns_names": [
        "example3.com.",
        "example4.com."
      ],
      "db_user_name": "root",
      "vpc_id": "b21630c1-e7d3-450d-907d-39ef5f445ae7",
      "subnet_id": "45557a98-9e17-4600-8aec-999150bc4eef",
      "security_group_id": "38815c5c-482b-450a-80b6-0a301f2afd97",
      "flavor_ref": "rds.mysql.s1.large",
      "switch_strategy": "reliability",
      "read_only_by_user": false,
      "charge_info": {
        "charge_mode": "postPaid"
      },
      "backup_strategy": {
        "start_time": "19:00-20:00",
        "keep_days": 7
      },
      "maintenance_window": "02:00-06:00",
      "related_instance": [],
      "disk_encryption_id": "",
      "enterprise_project_id": "0",
      "time_zone": "UTC",
      "tags": [
        {
          "key": "rds001",
          "value": "rds001"
        }
      ]
    }
  ],
  "total_count": 1
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024/11/19 | 1.0  | 文档首次发布| 