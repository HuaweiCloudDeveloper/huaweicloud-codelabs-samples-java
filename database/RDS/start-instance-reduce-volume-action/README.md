### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RDS/debug?api=StartInstanceReduceVolumeAction) 中直接运行调试该接口。

2.数据库实例的磁盘空间缩容。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.调用接口前，您需要了解API [认证鉴权](https://support.huaweicloud.com/api-rds/rds_03_0001.html)。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-rds”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rds</artifactId>
    <version>3.1.121</version>
</dependency>
```
### 接口约束
1.该接口仅支持MySQL和PostgreSQL引擎。
2.如需开通磁盘缩容功能,请联系客服申请。
3.备实例磁盘容量和主实例磁盘容量一致,当主实例缩容时,会同时对备实例缩容。
4.支持缩容的磁盘类型包括:本地SSD盘(仅MySQL实例支持)、SSD云盘和超高IO。
5.实例状态为“正常”时可以进行缩容。
6.根据当前磁盘的使用量情况存在磁盘容量下限,当缩容后目标磁盘容量小于磁盘容量下限时,缩容会下发失败。
7.若实例开启了自动扩容,在缩容完成后,满足自动扩容条件会触发自动扩容。
8.缩容后,使用该实例之前的产生的备份恢复当前实例时,可能存在空间不足的问题。
9.使用缩容前的备份进行恢复时,请选择大于等于缩容前磁盘大小的实例。
10.磁盘缩容会造成短暂的业务中断,并且如果在业务高峰期缩容,可能会使磁盘空间被耗尽,导致磁盘缩容失败。请选择在业务低峰期进行磁盘缩容。
11.每次缩容至少缩小10GB;目标大小必须为10的整数倍。

### 代码示例
以下代码展示如何对一个数据库实例的磁盘进行缩容：
``` java
package com.huaweicloud.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.*;
import com.huaweicloud.sdk.rds.v3.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StartInstanceReduceVolumeAction {
    private static final Logger logger = LoggerFactory.getLogger(StartInstanceReduceVolumeAction.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量CLOUD_SDK_AK和CLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.CN_SOUTH_1)
                .build();

        StartInstanceReduceVolumeActionRequest request = new StartInstanceReduceVolumeActionRequest();
        request.withInstanceId("instanceId"); // 数据库实例ID
        ReduceVolumeRequestBody body = new ReduceVolumeRequestBody();
        ReduceVolumeObject reduceVolumebody = new ReduceVolumeObject();
        reduceVolumebody.withSize(40); // 磁盘缩容目标大小
        body.withReduceVolume(reduceVolumebody);
        request.withBody(body);

        try {
            StartInstanceReduceVolumeActionResponse response = client.startInstanceReduceVolumeAction(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            logger.error(e.toString());
        }
    }
}
```

### 返回结果示例
```json
{
  "job_id": "04efe8e2-9255-44ae-a98b-d87cae411890"
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024/11/18 | 1.0  | 文档首次发布| 