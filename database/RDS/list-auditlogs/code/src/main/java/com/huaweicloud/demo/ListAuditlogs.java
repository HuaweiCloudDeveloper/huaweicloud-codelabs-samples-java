package com.huaweicloud.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.*;
import com.huaweicloud.sdk.rds.v3.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ListAuditlogs {
    private static final Logger logger = LoggerFactory.getLogger(ListAuditlogs.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量CLOUD_SDK_AK和CLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.valueOf("cn-south-1"))
                .build();
        ListAuditlogsRequest request = new ListAuditlogsRequest();
        request.withInstanceId("instanceId");
        request.withStartTime("2018-08-06T10:41:14+0800"); // 查询开始时间,格式为“yyyy-mm-ddThh:mm:ssZ”。其中,T指某个时间的开始;Z指时区偏移量,例如北京时间偏移显示为+0800。
        request.withEndTime("2018-08-07T10:41:14+0800"); // 查询结束时间，时间跨度不超过30天。
        request.withOffset(0); // 索引位置,偏移量。从第一条数据偏移offset条数据后开始查询,默认为0(偏移0条数据,表示从第一条数据开始查询),必须为数字,不能为负数。
        request.withLimit(1); // 查询记录数。取值范围[1, 50]。
        try {
            ListAuditlogsResponse response = client.listAuditlogs(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            logger.error(e.toString());
        }
    }
}