### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RDS/debug?api=ListAuditlogs) 中直接运行调试该接口。

2.获取审计日志列表

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.调用接口前，您需要了解API [认证鉴权](https://support.huaweicloud.com/api-rds/rds_03_0001.html)。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-rds”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rds</artifactId>
    <version>3.1.121</version>
</dependency>
```

### 代码示例
以下代码展示获取审计日志列表：
``` java
package com.huaweicloud.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.*;
import com.huaweicloud.sdk.rds.v3.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ListAuditlogs {
    private static final Logger logger = LoggerFactory.getLogger(ListAuditlogs.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量CLOUD_SDK_AK和CLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.valueOf("cn-south-1"))
                .build();
        ListAuditlogsRequest request = new ListAuditlogsRequest();
        request.withInstanceId("instanceId");
        request.withStartTime("2018-08-06T10:41:14+0800"); // 查询开始时间,格式为“yyyy-mm-ddThh:mm:ssZ”。其中,T指某个时间的开始;Z指时区偏移量,例如北京时间偏移显示为+0800。
        request.withEndTime("2018-08-07T10:41:14+0800"); // 查询结束时间，时间跨度不超过30天。
        request.withOffset(0); // 索引位置,偏移量。从第一条数据偏移offset条数据后开始查询,默认为0(偏移0条数据,表示从第一条数据开始查询),必须为数字,不能为负数。
        request.withLimit(1); // 查询记录数。取值范围[1, 50]。
        try {
            ListAuditlogsResponse response = client.listAuditlogs(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            logger.error(e.toString());
        }
    }
}
```

### 返回结果示例
```json
{
  "auditlogs": [
    {
      "id": "fa163ea0e2bet11e9d832166a2cf894c5br01",
      "name": "317156_20190916032844_eb8fe5d181ec44a2850302691541f760in01_Audit_166a2cf8-d832-11e9-94c5-fa163ea0e2be",
      "size": 20481.835938,
      "begin_time": "2019-11-06T09:03:34+0800",
      "end_time": "2019-11-06T10:39:15+0800"
    },
    {
      "id": "fa163ea0e2bet11e9d832136a668094c5br01",
      "name": "317162_20190916032838_eb8fe5d181ec44a2850302691541f760in01_Audit_136a6680-d832-11e9-94c5-fa163ea0e2be",
      "size": 20481.835938,
      "begin_time": "2019-11-07T09:04:35+0800",
      "end_time": "2019-11-07T10:38:16+0800"
    }
  ],
  "total_record": 2
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024/12/27 | 1.0  | 文档首次发布| 