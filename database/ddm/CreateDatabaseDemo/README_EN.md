## 0. Version Description
In this sample, the SDK version is 3.1.2.

## 1. Introduction
DDM uses a decoupled compute and storage architecture and provides functions such as database and table sharding, read/write splitting, elastic scaling, and sustainable O&M. Management of instance nodes has no impacts on your workloads. You can perform O&M on your databases and read/write data from and to them on the DDM console, just like as operating a single-node MySQL database. This example shows how to create a sharded schema using the Java SDK.

## 2. Prerequisites

1. You have [registered](https://reg.huaweicloud.com/registerui/intl/register.html?locale=en-us) with Huawei Cloud, and completed [real-name authentication](https://auth.huaweicloud.com/authui/login.html?locale=en-us#/login).

2. You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3. You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and view your AK/SK on the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0001.html).

4. You have set up a development environment with Java JDK 1.8 or later.


## 3. Installing an SDK
You can obtain and install an SDK in Maven mode. Download and install Maven in your operating system (OS). After the installation is complete, add the required dependencies to the **pom.xml** file of the Java project.

For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter).
```xml
 <dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-ddm</artifactId>
    <version>3.1.2</version>
</dependency>
```

## 4. Sample Code
The following code shows how to create a sharded schema using the SDK.

```java
package com.huawei.ddm;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.ddm.v1.DdmClient;
import com.huaweicloud.sdk.ddm.v1.model.CreateDatabaseDetail;
import com.huaweicloud.sdk.ddm.v1.model.CreateDatabaseReq;
import com.huaweicloud.sdk.ddm.v1.model.CreateDatabaseRequest;
import com.huaweicloud.sdk.ddm.v1.model.CreateDatabaseResponse;
import com.huaweicloud.sdk.ddm.v1.model.DatabaseInstabcesParam;
import com.huaweicloud.sdk.ddm.v1.model.ListAvailableRdsListRequest;
import com.huaweicloud.sdk.ddm.v1.model.ListAvailableRdsListResponse;
import com.huaweicloud.sdk.ddm.v1.region.DdmRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

public class CreateDatabaseDemo {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateDatabaseDemo.class.getName());

    /**
     * Hard-coded or plaintext AK/SK and password are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     * args[0] = <<YOUR INSTANCE_ID>>
     * args[1] = <<YOUR REGION_ID>>
     * args[2] = <<database name>>
     * args[3] = <<number of shards>>
     * args[4] = <<username for accessing the data node>>
     * args[5] = <<password for accessing the data node>>
     **/
    public static void main(String[] args) {
        if (args.length != 6) {
            LOGGER.error("The expected number of parameters is 6");
            return;
        }

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = args[0];
        String regionId = args[1];
        String dbName = args[2];
        int shardNum = Integer.parseInt(args[3]);

        // Multiple DNs are required for creating a sharded schema. In the following example, the administrator passwords of DNs are consistent.
        String adminUserOfDN = args[4];
        String adminPwdOfDN = args[5];

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        DdmClient client = DdmClient.newBuilder()
                .withCredential(auth)
                .withRegion(DdmRegion.valueOf(regionId))
                .build();

        // Queries DB instances available for creating a schema.
        // Queries the obtained DNs for creating a schema.
        List<String> dnInstanceIds = listDataNodeInstances(client, instanceId);

        // Creates a sharded schema.
        createLogicDatabase(client, instanceId, dbName, shardNum, dnInstanceIds, adminUserOfDN, adminPwdOfDN);
    }

    private static List<String> listDataNodeInstances(DdmClient client, String instanceId) {
        ListAvailableRdsListRequest request = new ListAvailableRdsListRequest();
        request.withInstanceId(instanceId);

        List<String> dnInstanceIds = new ArrayList<>();

        Consumer<ListAvailableRdsListRequest> consumer = (req) -> {
            ListAvailableRdsListResponse response = client.listAvailableRdsList(req);
            LOGGER.info(response.toString());
            if (!response.getInstances().isEmpty()) {
                dnInstanceIds.add(response.getInstances().get(0).getId());
            }
        };

        executeRequest(consumer, request);

        return dnInstanceIds;
    }

    private static void createLogicDatabase(DdmClient client, String instanceId, String dbName, int shardNum,
                                            List<String> useDataNodeIds, String dnUser, String dnPwd) {
        if (useDataNodeIds.isEmpty()) {
            LOGGER.error("DataNodes must not be empty.");
            return;
        }

        CreateDatabaseRequest request = buildCreateDBRequest(instanceId, dbName, shardNum, useDataNodeIds, dnUser, dnPwd);

        Consumer<CreateDatabaseRequest> consumer = (req) -> {
            CreateDatabaseResponse response = client.createDatabase(req);
            LOGGER.info(response.toString());
        };

        executeRequest(consumer, request);
    }

    private static CreateDatabaseRequest buildCreateDBRequest(String instanceId, String dbName, int shardNum,
                                                              List<String> useDataNodeIds, String dnUser, String dnPwd) {
        List<DatabaseInstabcesParam> useDataNodes = new ArrayList<>();
        for (String dataNodeId : useDataNodeIds) {
            DatabaseInstabcesParam param = new DatabaseInstabcesParam();
            param.withId(dataNodeId)
                    .withAdminUser(dnUser)
                    .withAdminPassword(dnPwd);
            useDataNodes.add(param);
        }

        CreateDatabaseDetail detail = new CreateDatabaseDetail();
        detail.withName(dbName)
                .withShardMode(CreateDatabaseDetail.ShardModeEnum.CLUSTER)
                .withShardNumber(shardNum)
                .withUsedRds(useDataNodes);

        CreateDatabaseReq req = new CreateDatabaseReq().withDatabases(Collections.singletonList(detail));

        CreateDatabaseRequest request = new CreateDatabaseRequest();
        request.withInstanceId(instanceId)
                .withBody(req);

        return request;
    }

    private static <T> void executeRequest(Consumer<T> consumer, T request) {
        try {
            consumer.accept(request);
        } catch (ConnectionException e) {
            LOGGER.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            LOGGER.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            LOGGER.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}",
                    e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            LOGGER.error("Unexpected error occurred: ", e);
        }
    }

}

```


## 5. APIs and Parameters

For details, see [API for Querying DB Instances Available for Creating a Schema](https://support.huaweicloud.com/intl/en-us/api-ddm/ddm_api_01_0100.html).
You can directly run and debug the API in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DDM/doc?api=ListAvailableRdsList).

For details, see [Creating a Schema](https://support.huaweicloud.com/intl/en-us/api-ddm/ddm_16_0001.html).
You can directly run and debug the API in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DDM/doc?api=CreateDatabase).

## Change History
|    Released On   | Issue|   Description  |
|:----------:| :------: | :----------: |
| 2022-10-31 |   1.0    | First official release|
