package com.huawei.ddm;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.ddm.v1.DdmClient;
import com.huaweicloud.sdk.ddm.v1.model.CreateDatabaseDetail;
import com.huaweicloud.sdk.ddm.v1.model.CreateDatabaseReq;
import com.huaweicloud.sdk.ddm.v1.model.CreateDatabaseRequest;
import com.huaweicloud.sdk.ddm.v1.model.CreateDatabaseResponse;
import com.huaweicloud.sdk.ddm.v1.model.DatabaseInstabcesParam;
import com.huaweicloud.sdk.ddm.v1.model.ListAvailableRdsListRequest;
import com.huaweicloud.sdk.ddm.v1.model.ListAvailableRdsListResponse;
import com.huaweicloud.sdk.ddm.v1.region.DdmRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

public class CreateDatabaseDemo {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateDatabaseDemo.class.getName());

    /**
     * Hard-coded or plaintext AK/SK and password are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     * args[0] = <<YOUR INSTANCE_ID>>
     * args[1] = <<YOUR REGION_ID>>
     * args[2] = <<database name>>
     * args[3] = <<number of shards>>
     * args[4] = <<username for accessing the data node>>
     * args[5] = <<password for accessing the data node>>
     **/
    public static void main(String[] args) {
        if (args.length != 6) {
            LOGGER.error("The expected number of parameters is 6");
            return;
        }

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = args[0];
        String regionId = args[1];
        String dbName = args[2];
        int shardNum = Integer.parseInt(args[3]);

        // Multiple DNs are required for creating a sharded schema. In the following example, the administrator passwords of DNs are consistent.
        String adminUserOfDN = args[4];
        String adminPwdOfDN = args[5];

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        DdmClient client = DdmClient.newBuilder()
                .withCredential(auth)
                .withRegion(DdmRegion.valueOf(regionId))
                .build();

        // Queries DB instances available for creating a schema.
        // Queries the obtained DNs for creating a schema.
        List<String> dnInstanceIds = listDataNodeInstances(client, instanceId);

        // Creates a sharded schema.
        createLogicDatabase(client, instanceId, dbName, shardNum, dnInstanceIds, adminUserOfDN, adminPwdOfDN);
    }

    private static List<String> listDataNodeInstances(DdmClient client, String instanceId) {
        ListAvailableRdsListRequest request = new ListAvailableRdsListRequest();
        request.withInstanceId(instanceId);

        List<String> dnInstanceIds = new ArrayList<>();

        Consumer<ListAvailableRdsListRequest> consumer = (req) -> {
            ListAvailableRdsListResponse response = client.listAvailableRdsList(req);
            LOGGER.info(response.toString());
            if (!response.getInstances().isEmpty()) {
                dnInstanceIds.add(response.getInstances().get(0).getId());
            }
        };

        executeRequest(consumer, request);

        return dnInstanceIds;
    }

    private static void createLogicDatabase(DdmClient client, String instanceId, String dbName, int shardNum,
                                            List<String> useDataNodeIds, String dnUser, String dnPwd) {
        if (useDataNodeIds.isEmpty()) {
            LOGGER.error("DataNodes must not be empty.");
            return;
        }

        CreateDatabaseRequest request = buildCreateDBRequest(instanceId, dbName, shardNum, useDataNodeIds, dnUser, dnPwd);

        Consumer<CreateDatabaseRequest> consumer = (req) -> {
            CreateDatabaseResponse response = client.createDatabase(req);
            LOGGER.info(response.toString());
        };

        executeRequest(consumer, request);
    }

    private static CreateDatabaseRequest buildCreateDBRequest(String instanceId, String dbName, int shardNum,
                                                            List<String> useDataNodeIds, String dnUser, String dnPwd) {
        List<DatabaseInstabcesParam> useDataNodes = new ArrayList<>();
        for (String dataNodeId : useDataNodeIds) {
            DatabaseInstabcesParam param = new DatabaseInstabcesParam();
            param.withId(dataNodeId)
                    .withAdminUser(dnUser)
                    .withAdminPassword(dnPwd);
            useDataNodes.add(param);
        }

        CreateDatabaseDetail detail = new CreateDatabaseDetail();
        detail.withName(dbName)
                .withShardMode(CreateDatabaseDetail.ShardModeEnum.CLUSTER)
                .withShardNumber(shardNum)
                .withUsedRds(useDataNodes);

        CreateDatabaseReq req = new CreateDatabaseReq().withDatabases(Collections.singletonList(detail));

        CreateDatabaseRequest request = new CreateDatabaseRequest();
        request.withInstanceId(instanceId)
                .withBody(req);

        return request;
    }

    private static <T> void executeRequest(Consumer<T> consumer, T request) {
        try {
            consumer.accept(request);
        } catch (ConnectionException e) {
            LOGGER.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            LOGGER.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            LOGGER.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}",
                    e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            LOGGER.error("Unexpected error occurred: ", e);
        }
    }

}
