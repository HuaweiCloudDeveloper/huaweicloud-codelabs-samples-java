## 0. 版本说明
本示例配套的SDK版本为：3.1.2

## 1. 简介
DDM是由华为云自主研发的云原生分布式数据库中间件，采用存算分离架构，提供分库分表、读写分离、弹性扩容等能力，具有稳定可靠、高度可扩展、持续可运维的特点。服务器集群管理对用户完全透明，用户通过DDM管理控制台进行数据库运维与数据读写，提供类似传统单机数据库的使用体验。本示例展示如何通过java版本的SDK方式创建逻辑拆分库。

## 2. 前置条件

1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。


## 3. 安装SDK
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
 <dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-ddm</artifactId>
    <version>3.1.2</version>
</dependency>
```

## 4.代码示例
以下代码展示如何使用SDK创建逻辑拆分库：

```java
package com.huawei.ddm;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.ddm.v1.DdmClient;
import com.huaweicloud.sdk.ddm.v1.model.CreateDatabaseDetail;
import com.huaweicloud.sdk.ddm.v1.model.CreateDatabaseReq;
import com.huaweicloud.sdk.ddm.v1.model.CreateDatabaseRequest;
import com.huaweicloud.sdk.ddm.v1.model.CreateDatabaseResponse;
import com.huaweicloud.sdk.ddm.v1.model.DatabaseInstabcesParam;
import com.huaweicloud.sdk.ddm.v1.model.ListAvailableRdsListRequest;
import com.huaweicloud.sdk.ddm.v1.model.ListAvailableRdsListResponse;
import com.huaweicloud.sdk.ddm.v1.region.DdmRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

public class CreateDatabaseDemo {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateDatabaseDemo.class.getName());

    /**
     * 认证用的ak/sk和密码硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
     * 本示例以ak和sk保存在环境变量中为例，运行本实例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK
     * args[0] = <<YOUR INSTANCE_ID>>
     * args[1] = <<YOUR REGION_ID>>
     * args[2] = <<数据库名>>
     * args[3] = <<分片数量>>
     * args[4] = <<数据节点实例访问账号>>
     * args[5] = <<数据节点实例访问密码>>
     **/
    public static void main(String[] args) {
        if (args.length != 6) {
            LOGGER.error("The expected number of parameters is 6");
            return;
        }

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = args[0];
        String regionId = args[1];
        String dbName = args[2];
        int shardNum = Integer.parseInt(args[3]);

        // 创建逻辑拆分库会使用多个DN实例，此示例中预设该多个DN实例的管理员用户密码一致
        String adminUserOfDN = args[4];
        String adminPwdOfDN = args[5];

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        DdmClient client = DdmClient.newBuilder()
                .withCredential(auth)
                .withRegion(DdmRegion.valueOf(regionId))
                .build();

        // 查询创建逻辑库可选取的数据节点实例列表
        // 查询所得的DN实例用于创建逻辑拆分库
        List<String> dnInstanceIds = listDataNodeInstances(client, instanceId);

        // 创建逻辑拆分库
        createLogicDatabase(client, instanceId, dbName, shardNum, dnInstanceIds, adminUserOfDN, adminPwdOfDN);
    }

    private static List<String> listDataNodeInstances(DdmClient client, String instanceId) {
        ListAvailableRdsListRequest request = new ListAvailableRdsListRequest();
        request.withInstanceId(instanceId);

        List<String> dnInstanceIds = new ArrayList<>();

        Consumer<ListAvailableRdsListRequest> consumer = (req) -> {
            ListAvailableRdsListResponse response = client.listAvailableRdsList(req);
            LOGGER.info(response.toString());
            if (!response.getInstances().isEmpty()) {
                dnInstanceIds.add(response.getInstances().get(0).getId());
            }
        };

        executeRequest(consumer, request);

        return dnInstanceIds;
    }

    private static void createLogicDatabase(DdmClient client, String instanceId, String dbName, int shardNum,
                                            List<String> useDataNodeIds, String dnUser, String dnPwd) {
        if (useDataNodeIds.isEmpty()) {
            LOGGER.error("DataNodes must not be empty.");
            return;
        }

        CreateDatabaseRequest request = buildCreateDBRequest(instanceId, dbName, shardNum, useDataNodeIds, dnUser, dnPwd);

        Consumer<CreateDatabaseRequest> consumer = (req) -> {
            CreateDatabaseResponse response = client.createDatabase(req);
            LOGGER.info(response.toString());
        };

        executeRequest(consumer, request);
    }

    private static CreateDatabaseRequest buildCreateDBRequest(String instanceId, String dbName, int shardNum,
                                                              List<String> useDataNodeIds, String dnUser, String dnPwd) {
        List<DatabaseInstabcesParam> useDataNodes = new ArrayList<>();
        for (String dataNodeId : useDataNodeIds) {
            DatabaseInstabcesParam param = new DatabaseInstabcesParam();
            param.withId(dataNodeId)
                    .withAdminUser(dnUser)
                    .withAdminPassword(dnPwd);
            useDataNodes.add(param);
        }

        CreateDatabaseDetail detail = new CreateDatabaseDetail();
        detail.withName(dbName)
                .withShardMode(CreateDatabaseDetail.ShardModeEnum.CLUSTER)
                .withShardNumber(shardNum)
                .withUsedRds(useDataNodes);

        CreateDatabaseReq req = new CreateDatabaseReq().withDatabases(Collections.singletonList(detail));

        CreateDatabaseRequest request = new CreateDatabaseRequest();
        request.withInstanceId(instanceId)
                .withBody(req);

        return request;
    }

    private static <T> void executeRequest(Consumer<T> consumer, T request) {
        try {
            consumer.accept(request);
        } catch (ConnectionException e) {
            LOGGER.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            LOGGER.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            LOGGER.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}",
                    e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            LOGGER.error("Unexpected error occurred: ", e);
        }
    }

}

```


## 5.接口及参数说明

请见 [查询创建逻辑库可选取的数据库实例列表API](https://support.huaweicloud.com/api-ddm/ddm_api_01_0100.html)
您可以在 [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=DDM&api=ListAvailableRdsList) 中直接运行调试该接口。

请见 [创建DDM逻辑库API](https://support.huaweicloud.com/api-ddm/ddm_16_0001.html)
您可以在 [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=DDM&api=CreateDatabase) 中直接运行调试该接口。

## 修订记录
|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2022-10-31 |   1.0    | 文档首次发布 |
