package com.huawei.ddm;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.ddm.v1.DdmClient;
import com.huaweicloud.sdk.ddm.v1.model.ShowInstanceParamRequest;
import com.huaweicloud.sdk.ddm.v1.model.ShowInstanceParamResponse;
import com.huaweicloud.sdk.ddm.v1.model.UpdateInstanceParamRequest;
import com.huaweicloud.sdk.ddm.v1.model.UpdateInstanceParamResponse;
import com.huaweicloud.sdk.ddm.v1.model.UpdateParametersReq;
import com.huaweicloud.sdk.ddm.v1.model.UpdateParametersReqValues;
import com.huaweicloud.sdk.ddm.v1.region.DdmRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;

public class ModifyParamDemo {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModifyParamDemo.class.getName());

    /**
     * Hard-coded or plaintext AK and SK are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     * args[0] = <<YOUR INSTANCE_ID>>
     * args[1] = <<YOUR REGION_ID>>
     **/
    public static void main(String[] args) {
        if (args.length != 2) {
            LOGGER.error("The expected number of parameters is 2");
            return;
        }

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = args[0];
        String regionId = args[1];

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        DdmClient client = DdmClient.newBuilder()
                .withCredential(auth)
                .withRegion(DdmRegion.valueOf(regionId))
                .build();

        // Queries parameter details of a DDM instance.
        showInstanceParam(client, instanceId);

        // Modifies parameters of a DDM instance.
        // Changes the slow query duration threshold to 1s and the SQL execution timeout threshold to 30s.
        UpdateParametersReqValues values = new UpdateParametersReqValues()
                .withLongQueryTime("1")
                .withSqlExecuteTimeout("120");

        modifyInstanceParam(client, instanceId, values);

        // Queries parameter details of a DDM instance.
        showInstanceParam(client, instanceId);
    }

    private static void showInstanceParam(DdmClient client, String instanceId) {
        ShowInstanceParamRequest request = new ShowInstanceParamRequest().withInstanceId(instanceId);

        Consumer<ShowInstanceParamRequest> consumer = (req) -> {
            ShowInstanceParamResponse response = client.showInstanceParam(req);
            LOGGER.info(response.toString());
        };

        executeRequest(consumer, request);
    }

    private static void modifyInstanceParam(DdmClient client, String instanceId, UpdateParametersReqValues values) {
        UpdateInstanceParamRequest request = new UpdateInstanceParamRequest()
                .withInstanceId(instanceId)
                .withBody(new UpdateParametersReq().withValues(values));

        Consumer<UpdateInstanceParamRequest> consumer = (req) -> {
            UpdateInstanceParamResponse response = client.updateInstanceParam(req);
            LOGGER.info(response.toString());
        };

        executeRequest(consumer, request);
    }

    private static <T> void executeRequest(Consumer<T> consumer, T request) {
        try {
            consumer.accept(request);
        } catch (ConnectionException e) {
            LOGGER.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            LOGGER.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            LOGGER.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}",
                    e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            LOGGER.error("Unexpected error occurred: ", e);
        }
    }

}