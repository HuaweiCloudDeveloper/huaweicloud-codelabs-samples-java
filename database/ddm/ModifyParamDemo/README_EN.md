## 0. Version Description
In this sample, the SDK version is 3.1.2.

## 1. Introduction
DDM uses a decoupled compute and storage architecture and provides functions such as database and table sharding, read/write splitting, elastic scaling, and sustainable O&M. Management of instance nodes has no impacts on your workloads. You can perform O&M on your databases and read/write data from and to them on the DDM console, just like as operating a single-node MySQL database. This example shows how to create a sharded schema using the Java SDK.

## 2. Prerequisites

1. You have [registered](https://reg.huaweicloud.com/registerui/intl/register.html?locale=en-us) with Huawei Cloud, and completed [real-name authentication](https://auth.huaweicloud.com/authui/login.html?locale=en-us#/login).

2. You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3. You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and view your AK/SK on the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0001.html).

4. You have set up a development environment with Java JDK 1.8 or later.


## 3. Installing an SDK
You can obtain and install an SDK in Maven mode. Download and install Maven in your operating system (OS). After the installation is complete, add the required dependencies to the **pom.xml** file of the Java project.

For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter).
```xml
 <dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-ddm</artifactId>
    <version>3.1.2</version>
</dependency>
```

## 4. Sample Code
The following code shows how to modify parameters of a DDM instance using the SDK.

```java
package com.huawei.ddm;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.ddm.v1.DdmClient;
import com.huaweicloud.sdk.ddm.v1.model.ShowInstanceParamRequest;
import com.huaweicloud.sdk.ddm.v1.model.ShowInstanceParamResponse;
import com.huaweicloud.sdk.ddm.v1.model.UpdateInstanceParamRequest;
import com.huaweicloud.sdk.ddm.v1.model.UpdateInstanceParamResponse;
import com.huaweicloud.sdk.ddm.v1.model.UpdateParametersReq;
import com.huaweicloud.sdk.ddm.v1.model.UpdateParametersReqValues;
import com.huaweicloud.sdk.ddm.v1.region.DdmRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;

public class ModifyParamDemo {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModifyParamDemo.class.getName());

    /**
     * Hard-coded or plaintext AK and SK are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     * args[0] = <<YOUR INSTANCE_ID>>
     * args[1] = <<YOUR REGION_ID>>
     **/
    public static void main(String[] args) {
        if (args.length != 4) {
            LOGGER.error("The expected number of parameters is 4");
            return;
        }

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = args[0];
        String regionId = args[1];

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        DdmClient client = DdmClient.newBuilder()
                .withCredential(auth)
                .withRegion(DdmRegion.valueOf(regionId))
                .build();

        // Queries parameter details of a DDM instance.
        showInstanceParam(client, instanceId);

        // Modifies parameters of a DDM instance.
        // Changes the slow query duration threshold to 1s and the SQL execution timeout threshold to 30s.
        UpdateParametersReqValues values = new UpdateParametersReqValues()
                .withLongQueryTime("1")
                .withSqlExecuteTimeout("120");

        modifyInstanceParam(client, instanceId, values);

        // Queries parameter details of a DDM instance.
        showInstanceParam(client, instanceId);
    }

    private static void showInstanceParam(DdmClient client, String instanceId) {
        ShowInstanceParamRequest request = new ShowInstanceParamRequest().withInstanceId(instanceId);

        Consumer<ShowInstanceParamRequest> consumer = (req) -> {
            ShowInstanceParamResponse response = client.showInstanceParam(req);
            LOGGER.info(response.toString());
        };

        executeRequest(consumer, request);
    }

    private static void modifyInstanceParam(DdmClient client, String instanceId, UpdateParametersReqValues values) {
        UpdateInstanceParamRequest request = new UpdateInstanceParamRequest()
                .withInstanceId(instanceId)
                .withBody(new UpdateParametersReq().withValues(values));

        Consumer<UpdateInstanceParamRequest> consumer = (req) -> {
            UpdateInstanceParamResponse response = client.updateInstanceParam(req);
            LOGGER.info(response.toString());
        };

        executeRequest(consumer, request);
    }

    private static <T> void executeRequest(Consumer<T> consumer, T request) {
        try {
            consumer.accept(request);
        } catch (ConnectionException e) {
            LOGGER.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            LOGGER.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            LOGGER.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}",
                    e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            LOGGER.error("Unexpected error occurred: ", e);
        }
    }

}


```


## 5. APIs and Parameters

For details, see [Querying Parameters of a Specified DDM Instance](https://support.huaweicloud.com/intl/en-us/api-ddm/ddm_api_01_0084.html).
You can directly run and debug the API in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DDM/doc?api=ShowInstanceParam).

For details, see [Modifying Parameters of a DDM Instance](https://support.huaweicloud.com/intl/en-us/api-ddm/ddm_api_01_0085.html).
You can directly run and debug the API in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DDM/doc?api=UpdateInstanceParam).


## Change History
|    Released On   | Issue|   Description  |
|:----------:| :------: | :----------: |
| 2022-10-31 |   1.0    | First official release|
