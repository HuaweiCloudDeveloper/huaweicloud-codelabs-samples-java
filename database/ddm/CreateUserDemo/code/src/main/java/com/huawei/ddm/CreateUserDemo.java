package com.huawei.ddm;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.ddm.v1.DdmClient;
import com.huaweicloud.sdk.ddm.v1.model.CreateUsersDatabases;
import com.huaweicloud.sdk.ddm.v1.model.CreateUsersInfo;
import com.huaweicloud.sdk.ddm.v1.model.CreateUsersReq;
import com.huaweicloud.sdk.ddm.v1.model.CreateUsersRequest;
import com.huaweicloud.sdk.ddm.v1.model.CreateUsersResponse;
import com.huaweicloud.sdk.ddm.v1.model.ListUsersRequest;
import com.huaweicloud.sdk.ddm.v1.model.ListUsersResponse;
import com.huaweicloud.sdk.ddm.v1.region.DdmRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

public class CreateUserDemo {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateUserDemo.class.getName());

    /**
     * Hard-coded or plaintext AK and SK are risky. For security purposes, encrypt your AK and SK and store them in the configuration file or environment variables.
     * In this example, the AK and SK are stored in environment variables for identity authentication. Configure environment variables **HUAWEICLOUD_SDK_AK** and **HUAWEICLOUD_SDK_SK** in the local environment first.
     * args[0] = <<YOUR INSTANCE_ID>>
     * args[1] = <<YOUR REGION_ID>>
     * args[2] = <<name of the database to be associated with>>
     * args[3] = <<username>>
     * args[4] = <<password>>
     **/
    public static void main(String[] args) {
        if (args.length != 5) {
            LOGGER.error("The expected number of parameters is 5");
            return;
        }

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = args[0];
        String regionId = args[1];

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        DdmClient client = DdmClient.newBuilder()
                .withCredential(auth)
                .withRegion(DdmRegion.valueOf(regionId))
                .build();

        // Views the user list.
        listUsers(client, instanceId);

        // Creates a user that has all database permissions and associates it with the created schema.
        String dbName = args[2];
        String userName = args[3];
        String userPwd = args[4];

        createUser(client, instanceId, userName, userPwd, buildFullAuthorities(), dbName);

        // Views the user list.
        listUsers(client, instanceId);
    }

    private static void listUsers(DdmClient client, String instanceId) {
        ListUsersRequest request = new ListUsersRequest();
        request.withInstanceId(instanceId);

        Consumer<ListUsersRequest> consumer = (req) -> {
            ListUsersResponse response = client.listUsers(req);
            LOGGER.info(response.toString());
        };

        executeRequest(consumer, request);
    }

    private static void createUser(DdmClient client, String instanceId, String userName, String userPwd,
                                    List<CreateUsersInfo.BaseAuthorityEnum> authorities, String dbName) {
        CreateUsersDatabases databases = new CreateUsersDatabases().withName(dbName);
        CreateUsersInfo usersInfo = new CreateUsersInfo()
                .withName(userName)
                .withPassword(userPwd)
                .withDatabases(Collections.singletonList(databases))
                .withBaseAuthority(authorities);

        CreateUsersReq uses = new CreateUsersReq().withUsers(Collections.singletonList(usersInfo));
        CreateUsersRequest request = new CreateUsersRequest()
                .withInstanceId(instanceId)
                .withBody(uses);

        Consumer<CreateUsersRequest> consumer = (req) -> {
            CreateUsersResponse response = client.createUsers(req);
            LOGGER.info(response.toString());
        };

        executeRequest(consumer, request);
    }

    private static List<CreateUsersInfo.BaseAuthorityEnum> buildFullAuthorities() {
        return Arrays.asList(
            CreateUsersInfo.BaseAuthorityEnum.CREATE,
            CreateUsersInfo.BaseAuthorityEnum.DROP,
            CreateUsersInfo.BaseAuthorityEnum.ALTER,
            CreateUsersInfo.BaseAuthorityEnum.INDEX,
            CreateUsersInfo.BaseAuthorityEnum.INSERT,
            CreateUsersInfo.BaseAuthorityEnum.DELETE,
            CreateUsersInfo.BaseAuthorityEnum.UPDATE,
            CreateUsersInfo.BaseAuthorityEnum.SELECT
        );
    }

    private static <T> void executeRequest(Consumer<T> consumer, T request) {
        try {
            consumer.accept(request);
        } catch (ConnectionException e) {
            LOGGER.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            LOGGER.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            LOGGER.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}",
                    e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            LOGGER.error("Unexpected error occurred: ", e);
        }
    }

}
