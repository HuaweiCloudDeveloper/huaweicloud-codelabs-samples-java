package com.huawei.orgid.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class UserInfo {
    @JsonProperty("tenant_name")
    private String tenantName;

    private String role;

    @JsonProperty("login_type")
    private String loginType;

    @JsonProperty("user_name")
    private String userName;

    private String mobile;

    @JsonProperty("employeeCode")
    private String employee_code;

    @JsonProperty("domain_id")
    private String domainId;

    @JsonProperty("user_id")
    private String userId;

    private String name;

    private String tenant;
}