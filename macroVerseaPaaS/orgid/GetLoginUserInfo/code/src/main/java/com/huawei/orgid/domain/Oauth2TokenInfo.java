package com.huawei.orgid.domain;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Oauth2TokenInfo {
    @JsonProperty("access_token")
    private String accessToken;

    @JsonProperty("refresh_token")
    private String refreshToken;

    private String scope;

    @JsonProperty("token_type")
    private String tokenType;

    @JsonProperty("expires_in")
    private Long expiresIn;
}
