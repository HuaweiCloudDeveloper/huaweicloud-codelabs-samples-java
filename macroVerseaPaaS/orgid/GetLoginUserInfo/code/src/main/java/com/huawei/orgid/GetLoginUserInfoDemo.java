package com.huawei.orgid;

import com.huawei.orgid.domain.Oauth2TokenInfo;
import com.huawei.orgid.domain.UserInfo;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;

import javax.net.ssl.SSLContext;

public class GetLoginUserInfoDemo {
    private static final Logger logger = LoggerFactory.getLogger(GetLoginUserInfoDemo.class.getName());

    private static final String GRANT_TYPE = "authorization_code";

    private static final String ORGID_URL = "https://orgid.macroverse.huaweicloud.com";

    private static final String OAUTH_TOKEN_URL = "/oauth2/token";

    private static final String OAUTH_INFO_URL = "/oauth2/userinfo";

    public static void main(String[] args) {
        // 用户尝试登录当前应用时，在OrgID登录页输入登录信息在OrgID侧认证成功后，由OrgID颁发的授权码信息。只能使用一次
        String code
            = "UNE5P1aIIDgRaeR_v5aTmfudH2X_BKtRtVicpmjqDVXJHs-JLljN8zl3AUOuD7wY60TmBZ0bev0-5LGlsTpIsHTdKhCwKxwp5f9r_GdqD3xcjOuJdm5KF7fjrg02brcY";

        // 获取登录用户的token信息
        Oauth2TokenInfo oauth2UserToken = getOauth2UserToken(code);

        // 获取登录用户的详细用户信息
        getOauth2UserInfo(oauth2UserToken.getAccessToken());
    }

    private static Oauth2TokenInfo getOauth2UserToken(String code) {
        // 对接应用在OrgID侧的应用凭证，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        String clientId = System.getenv("CLIENT_ID");
        // 对接应用在OrgID侧的应用凭证密钥，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        String clientSecret = System.getenv("CLIENT_SECRET");
        // 在OrgID侧认证成功后需重定向的应用首页地址,建议在配置文件配置(不能时随意地址，在OrgID侧配置的，会进行校验)
        String redirectUrl = System.getenv("REDIRECT_URL");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        String formUrlEncodeRequest = MessageFormat.format(
            "grant_type={0}&code={1}&client_id={2}&client_secret={3}&redirect_uri={4}", GRANT_TYPE, code, clientId,
            clientSecret, redirectUrl);
        HttpEntity<?> httpEntity = new HttpEntity<>(formUrlEncodeRequest, headers);

        ResponseEntity<Oauth2TokenInfo> responseEntity = sendHttp(ORGID_URL + OAUTH_TOKEN_URL, HttpMethod.POST,
            httpEntity, Oauth2TokenInfo.class);
        return responseEntity.getBody();
    }

    private static void getOauth2UserInfo(String authorization) {
        HttpHeaders userHeaders = new HttpHeaders();
        userHeaders.setBearerAuth(authorization);
        HttpEntity<String> userEntity = new HttpEntity<>(userHeaders);
        ResponseEntity<UserInfo> infoEntity = sendHttp(ORGID_URL + OAUTH_INFO_URL, HttpMethod.GET, userEntity,
            UserInfo.class);
        logger.info("Login user info is {}", infoEntity.getBody());
    }

    private static <T> ResponseEntity<T> sendHttp(String url, HttpMethod method, HttpEntity<?> httpEntity, Class<T> c) {
        RestTemplate restTemplate = getRestTemplate();
        ResponseEntity<T> exchange = null;
        try {
            exchange = restTemplate.exchange(url, method, httpEntity, c);
        } catch (Exception e) {
            logger.error("invoke verify code interface failure,result = {}", e.getMessage());
            throw new RuntimeException();
        }
        if (exchange.getBody() == null) {
            logger.error("invoke verify code interface failure, responseBody is null");
            throw new RuntimeException();
        }
        return exchange;
    }

    public static RestTemplate getRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();

        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        httpRequestFactory.setConnectionRequestTimeout(10 * 2000);
        httpRequestFactory.setConnectTimeout(10 * 2000);

        CloseableHttpClient httpClient = getHttpsClient();
        httpRequestFactory.setHttpClient(httpClient);

        restTemplate.setRequestFactory(httpRequestFactory);
        return restTemplate;
    }

    public static CloseableHttpClient getHttpsClient() {

        CloseableHttpClient httpClient;
        SSLContext sslContext = null;
        try {
            sslContext = SSLContexts.custom().loadTrustMaterial(null, (x509Certificates, s) -> true).build();
        } catch (NoSuchAlgorithmException e) {
            logger.error("getHttpsClient faild:enccrypt faild.Msg is {}.", e.getMessage());
            throw new RuntimeException();
        } catch (KeyManagementException e) {
            logger.error("getHttpsClient faild:get key faild.Msg is {}.", e.getMessage());
            throw new RuntimeException();
        } catch (KeyStoreException e) {
            logger.error("getHttpsClient faild:get keyStore faild.Msg is {}.", e.getMessage());
            throw new RuntimeException();
        }
        SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslContext,
            new String[] {"TLSv1.2"}, null, NoopHostnameVerifier.INSTANCE);
        httpClient = HttpClients.custom().setSSLSocketFactory(sslConnectionSocketFactory).build();
        return httpClient;
    }
}
