## 1. 简介

组织成员帐号（OrgID）是面向企业提供组织管理、企业成员帐号管理以及SaaS应用授权管理能力的云服务，将Huawei ID帐号体系延伸到企业用户，统一华为云面向生态SaaS服务的组织、帐号，同时面向生态伙伴推出SaaS服务帐号集成规范。

本示例展示如何通过java版本的SDK方式获取从OrgID使用Oauth2标准协议侧登陆后的token及用户信息
## 2. 前置条件
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 在OrgId侧已申请应用凭证信息，或按照以下步骤进行了应用的创建：
（1）访问OrgID服务，登录OrgID服务的管理页面，https://orgid.macroverse.huaweicloud.com/orgid/console-web/index.html#/orgid-home。
（2）选择“应用管理”，单击右上角的“添加自建应用”，填写应用信息，上传图标，选择Oauth标准协议。
  首页URL：Oauth登录交互流程的回调地址，用户账号密码在OrgID侧登录成功后，回调应用本身的地址，会携带授权码信息（code）。
  管理员登录URL： 若Demo应用区分管理员角色，且登录后为管理后台，可自行配置。
  退出地址：OrgID退出登录时，通知应用的回调地址。
（3）在应用管理页面选择“授权管理”，单击“授权设置”，选择授权范围，允许账号登录该应用。
（4）将应用凭证信息在示例代码中进行替换，复制刚刚创建成功的应用的登录配置的首页URL地址，从OrgID侧登录应用，将OrgID颁发的授权码code进行替换。

## 3. 代码示例
以下代码展示如何使用SDK创建流：(仅展示主类代码和示例逻辑，具体代码请查看GetLoginUserInfoDemo.java)
``` java
package com.huawei.orgid;

import com.huawei.orgid.domain.Oauth2TokenInfo;
import com.huawei.orgid.domain.UserInfo;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;

import javax.net.ssl.SSLContext;

public class GetLoginUserInfoDemo {
    private static final Logger logger = LoggerFactory.getLogger(GetLoginUserInfoDemo.class.getName());

    // 认证类型，固定值 authorization_code或client_credentials
    private static final String GRANT_TYPE = "authorization_code";

    private static final String ORGID_URL = "https://orgid.macroverse.huaweicloud.com";

    private static final String OAUTH_TOKEN_URL = "/oauth2/token";

    private static final String OAUTH_INFO_URL = "/oauth2/userinfo";

    public static void main(String[] args) {
        // 用户尝试登录当前应用时，在OrgID登录页输入登录信息在OrgID侧认证成功后，由OrgID颁发的授权码信息。只能使用一次
        String code
            = "7QRd470p1vpM-R0yj93NH9-fkgRtvU7UxJP1x5HIFKNvpBEcJhLDt1zIKpszIWwbDqFJzUtzH8JbjfueY_IVmOTw2gT3AM9uZg7iHsdBOIFr9fs3pWzXV5dOrlttYpvC";

        // 获取登录用户的token信息
        Oauth2TokenInfo oauth2UserToken = getOauth2UserToken(code);

        // 获取登录用户的详细用户信息
        getOauth2UserInfo(oauth2UserToken.getAccessToken());
    }
    
    private static Oauth2TokenInfo getOauth2UserToken(String code) {
        // 对接应用在OrgID侧的应用凭证，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        String clientId = System.getenv("CLIENT_ID");
        // 对接应用在OrgID侧的应用凭证密钥，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        String clientSecret = System.getenv("CLIENT_SECRET");
        // 在OrgID侧认证成功后需重定向的应用首页地址,建议在配置文件配置(不能时随意地址，在OrgID侧配置的，会进行校验)
        String redirectUrl = System.getenv("REDIRECT_URL");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        String formUrlEncodeRequest = MessageFormat.format(
            "grant_type={0}&code={1}&client_id={2}&client_secret={3}&redirect_uri={4}", GRANT_TYPE, code, clientId,
            clientSecret, redirectUrl);
        HttpEntity<?> httpEntity = new HttpEntity<>(formUrlEncodeRequest, headers);

        ResponseEntity<Oauth2TokenInfo> responseEntity = sendHttp(ORGID_URL + OAUTH_TOKEN_URL, HttpMethod.POST,
            httpEntity, Oauth2TokenInfo.class);
        return responseEntity.getBody();
    }

    private static void getOauth2UserInfo(String authorization) {
        HttpHeaders userHeaders = new HttpHeaders();
        userHeaders.setBearerAuth(authorization);
        HttpEntity<String> userEntity = new HttpEntity<>(userHeaders);
        ResponseEntity<UserInfo> infoEntity = sendHttp(ORGID_URL + OAUTH_INFO_URL, HttpMethod.GET, userEntity,
            UserInfo.class);
        logger.info("Login user info is {}", infoEntity.getBody());
    }
}
```

## 5. 返回结果示例
``` java
{
    "access_token": "SetPuNNPf49RlT16Y0fvmqetHYmndhXLI7e8lq-4zOJAsKfPmD",
    "refresh_token": "PMBlkkSHDqFtxyNSQrHJfyxdJyNoqiq2_EdhMqRrlQKcbObFeWD",
    "scope": "phone profile email",
    "token_type": "Bearer",
    "expires_in": 7200
}
```
## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-03-01 | 1.0 | 文档首次发布|

## 7. 参考链接
组织成员账号官方文档链接：https://support.huaweicloud.com/orgid/index.html