/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ResourceObjectDto {
    /**
     * 文件资源ID
     */
    private String id;

    /**
     * 文件资源ID(作为请求时使用)
     */
    private String objectId;

    /**
     * 文件资源的长度
     */
    private Long length;

    /**
     * 文件的sha256(用于完整性校验)
     */
    private String sha256;

    /**
     * 文件的hash(用于完整性校验)
     */
    private String hash;

    /**
     * 数据的状态
     */
    private Integer state;

    /**
     * 资源类型
     */
    private Integer type;

    /**
     * 起始偏移量
     */
    private int start;

    /**
     * 文件上传信息
     */
    private EndPointURL uploadUrl;

    /**
     * 文件下载信息
     */
    private EndPointURL downloadUrl;

    /**
     * 分片明文上传Id
     */
    private String uploadId;

    /**
     * 缩略图信息
     */
    private List<ImagesDto> images;
}
