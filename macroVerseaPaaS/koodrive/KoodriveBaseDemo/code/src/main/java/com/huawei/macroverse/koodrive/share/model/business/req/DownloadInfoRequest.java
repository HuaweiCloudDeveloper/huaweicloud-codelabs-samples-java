/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.req;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 获取下载链接的请求参数
 */
@Getter
@Setter
public class DownloadInfoRequest extends KoodriveCommonResp {
    private String containerId;

    private List<String> fileIds;
}
