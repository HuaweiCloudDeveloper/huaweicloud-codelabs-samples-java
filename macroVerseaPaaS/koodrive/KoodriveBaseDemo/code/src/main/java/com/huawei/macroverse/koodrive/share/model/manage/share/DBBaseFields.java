/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.manage.share;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 公共字段
 */
@Getter
@Setter
public class DBBaseFields {
    private String tenantId;

    private String creator;

    private Date createTime;

    private String modifier;

    private Date updateTime;
}
