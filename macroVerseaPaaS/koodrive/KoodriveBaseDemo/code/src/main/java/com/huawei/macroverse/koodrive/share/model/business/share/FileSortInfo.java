/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class FileSortInfo {
    /**
     * 排序类型
     * 1:文件名排序，2：文件大小排序，3：创建时间排序，4：修改时间排序，5：回收时间（只针对于回收文件）.6:文件类型
     */
    private Integer sortType;

    /**
     * 顺序
     * 1：升序，2：降序
     */
    private Integer sortDirection;
}
