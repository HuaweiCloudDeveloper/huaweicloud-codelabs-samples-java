/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.service.impl;

import com.alibaba.fastjson.JSON;
import com.huawei.macroverse.koodrive.service.api.IKoodriveSessionService;
import com.huawei.macroverse.koodrive.share.config.KoodriveConfig;
import com.huawei.macroverse.koodrive.share.exception.KoodriveServerErrorCode;
import com.huawei.macroverse.koodrive.share.exception.KoodriveServerException;
import com.huawei.macroverse.koodrive.share.http.HttpClient;
import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import com.huawei.macroverse.koodrive.share.model.session.req.SessionAuthCodeURLReq;
import com.huawei.macroverse.koodrive.share.model.session.req.SessionCommonReq;
import com.huawei.macroverse.koodrive.share.model.session.req.SessionCreateReq;
import com.huawei.macroverse.koodrive.share.model.session.resp.SessionAuthCodeURLResp;
import com.huawei.macroverse.koodrive.share.model.session.resp.SessionAuthResp;
import com.huawei.macroverse.koodrive.share.model.session.resp.SessionCreateResp;
import com.huawei.macroverse.koodrive.share.model.session.resp.SessionRefreshResp;
import com.huawei.macroverse.koodrive.utils.CommonHeaderUtil;
import com.huawei.macroverse.koodrive.utils.StringUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@AllArgsConstructor
@Service("koodriveSessionService")
public class KoodriveSessionServiceImpl implements IKoodriveSessionService {
    private final KoodriveConfig koodriveConfig;

    @Override
    public SessionAuthCodeURLResp getAuthCodeURL(SessionAuthCodeURLReq req) throws KoodriveServerException {
        StringBuilder builder = new StringBuilder(koodriveConfig.getKoodriveHost());
        builder.append(koodriveConfig.getSessionConfig().getAuthCode());
        if (StringUtils.isBlank(req.getDomain())) {
            req.setDomain(koodriveConfig.getKoodriveDomain());
        }
        builder.append("?domain=");
        builder.append(req.getDomain());
        Map<String, String> header = null;
        if (StringUtils.isNoBlank(req.getLanguage())) {
            header = new HashMap<>();
            header.put("language", req.getLanguage());
        }

        Optional<SessionAuthCodeURLResp> sessionAuthCodeURLResp = HttpClient.doGet(builder.toString(), header, SessionAuthCodeURLResp.class);
        if (!sessionAuthCodeURLResp.isPresent()) {
            throw new KoodriveServerException(KoodriveServerErrorCode.AUTHCODEURL_ERROR.getCode(),
                    KoodriveServerErrorCode.AUTHCODEURL_ERROR.getMsg());
        }

        return sessionAuthCodeURLResp.get();
    }

    @Override
    public SessionCreateResp createSession(SessionCreateReq req) throws KoodriveServerException {
        StringBuilder builder = new StringBuilder(koodriveConfig.getKoodriveHost());
        builder.append(koodriveConfig.getSessionConfig().getCreate());
        Map<String, String> header = null;
        if (StringUtils.isNoBlank(req.getLanguage())) {
            header = new HashMap<>();
            header.put("language", req.getLanguage());
        }

        if (StringUtils.isBlank(req.getDomain())) {
            req.setDomain(koodriveConfig.getKoodriveDomain());
        }
        Optional<SessionCreateResp> resp = HttpClient.doPost(builder.toString(), header, JSON.toJSONString(req), SessionCreateResp.class);
        if (!resp.isPresent()) {
            throw new KoodriveServerException(KoodriveServerErrorCode.CREATESESSION_ERROR.getCode(),
                    KoodriveServerErrorCode.CREATESESSION_ERROR.getMsg());
        }

        CommonHeaderUtil.setAuthorizationCookie(resp.get().getData().getAccessToken(), resp.get().getData().getSessionId());
        return resp.get();
    }

    @Override
    public SessionAuthResp authSession(SessionCommonReq req) throws KoodriveServerException {
        StringBuilder builder = new StringBuilder(koodriveConfig.getKoodriveHost());
        builder.append(koodriveConfig.getSessionConfig().getAuth());

        Map<String, String> header = new HashMap<>();
        header.put("Authorization", req.getAccessToken());
        if (StringUtils.isNoBlank(req.getLanguage())) {
            header.put("language", req.getLanguage());
        }
        Optional<SessionAuthResp> sessionAuthResp = HttpClient.doGet(builder.toString(), header, SessionAuthResp.class);
        if (!sessionAuthResp.isPresent()) {
            throw new KoodriveServerException(KoodriveServerErrorCode.AUTH_ERROR.getCode(),
                    KoodriveServerErrorCode.AUTH_ERROR.getMsg());
        }
        return sessionAuthResp.get();
    }

    @Override
    public SessionRefreshResp refresh(SessionCommonReq req) throws KoodriveServerException {
        StringBuilder builder = new StringBuilder(koodriveConfig.getKoodriveHost());
        builder.append(koodriveConfig.getSessionConfig().getRefresh());

        Map<String, String> header = new HashMap<>();
        header.put("x-session-id", req.getSessionId());
        header.put("Authorization", req.getAccessToken());
        if (StringUtils.isNoBlank(req.getLanguage())) {
            header.put("language", req.getLanguage());
        }

        Optional<SessionRefreshResp> resp = HttpClient.doPost(builder.toString(), header, null, SessionRefreshResp.class);
        if (!resp.isPresent()) {
            throw new KoodriveServerException(KoodriveServerErrorCode.REFRESH_ERROR.getCode(),
                    KoodriveServerErrorCode.REFRESH_ERROR.getMsg());
        }

        CommonHeaderUtil.setAuthorizationCookie(resp.get().getAccessToken(), null);
        return resp.get();
    }

    @Override
    public KoodriveCommonResp logout(SessionCommonReq req) throws KoodriveServerException {
        StringBuilder builder = new StringBuilder(koodriveConfig.getKoodriveHost());
        builder.append(koodriveConfig.getSessionConfig().getLogout());

        Map<String, String> header = new HashMap<>();
        header.put("x-session-id", req.getSessionId());
        header.put("Authorization", req.getAccessToken());
        if (StringUtils.isNoBlank(req.getLanguage())) {
            header.put("language", req.getLanguage());
        }
        Optional<KoodriveCommonResp> koodriveCommonResp = HttpClient.doGet(builder.toString(), header, KoodriveCommonResp.class);
        if (!koodriveCommonResp.isPresent()) {
            throw new KoodriveServerException(KoodriveServerErrorCode.LOGOUT_ERROR.getCode(),
                    KoodriveServerErrorCode.LOGOUT_ERROR.getMsg());
        }
        return koodriveCommonResp.get();
    }
}
