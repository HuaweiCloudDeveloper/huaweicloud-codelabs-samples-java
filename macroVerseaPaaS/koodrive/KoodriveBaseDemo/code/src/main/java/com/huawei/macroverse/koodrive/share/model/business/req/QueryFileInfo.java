/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.req;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import com.huawei.macroverse.koodrive.share.model.business.share.FileSortInfo;
import com.huawei.macroverse.koodrive.share.model.business.share.PageInfo;
import lombok.Getter;
import lombok.Setter;

/**
 * 查询文件信息的请求参数
 */
@Setter
@Getter
public class QueryFileInfo extends KoodriveCommonResp {

    /**
     * 分页信息
     */
    private PageInfo pageInfo;

    /**
     * 排序信息
     */
    private FileSortInfo sortInfo;

    /**
     * 空间标识
     */
    private String containerId;

    /**
     * 文件父目录，
     * 普通文件系统根目录填root；
     * 保险箱文件系统根目录填sbox
     */
    private String parentFileId;

    /**
     * 文件类型
     * 10：文件夹
     * 15：图片
     * 20：视频
     * 26：word
     * 27：excel
     * 28：ppt
     * 30：音频
     * 35：压缩文件
     * 40：应用
     * 45：其他
     */
    private String fileType;

    /**
     * 文件名，搜索时使用
     */
    private String fileName;

}
