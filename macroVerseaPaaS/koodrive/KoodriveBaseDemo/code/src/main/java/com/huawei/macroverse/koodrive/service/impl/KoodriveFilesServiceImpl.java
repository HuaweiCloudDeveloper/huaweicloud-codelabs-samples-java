/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.service.impl;

import com.alibaba.fastjson.JSON;
import com.huawei.macroverse.koodrive.service.api.business.IKoodriveFilesService;
import com.huawei.macroverse.koodrive.share.config.KoodriveConfig;
import com.huawei.macroverse.koodrive.share.exception.KoodriveServerErrorCode;
import com.huawei.macroverse.koodrive.share.exception.KoodriveServerException;
import com.huawei.macroverse.koodrive.share.http.HttpClient;
import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import com.huawei.macroverse.koodrive.share.model.business.req.CompleteFileRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.ContainerRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.CreateFileRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.DirectoryCreateRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.DownloadInfoRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.FilePreviewAndEditLinkReq;
import com.huawei.macroverse.koodrive.share.model.business.req.GetFullFileRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.QueryFileInfo;
import com.huawei.macroverse.koodrive.share.model.business.req.SearchFileRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.UpdateFileInfo;
import com.huawei.macroverse.koodrive.share.model.business.resp.CreateFileResponse;
import com.huawei.macroverse.koodrive.share.model.business.resp.DownloadResponse;
import com.huawei.macroverse.koodrive.share.model.business.resp.FilePreviewAndEditLinkRsp;
import com.huawei.macroverse.koodrive.share.model.business.resp.GetFullPathResponse;
import com.huawei.macroverse.koodrive.share.model.business.resp.QueryFileListRsp;
import com.huawei.macroverse.koodrive.share.model.business.resp.QueryFileMetadataRsp;
import com.huawei.macroverse.koodrive.share.model.business.resp.SearchFileResponse;
import com.huawei.macroverse.koodrive.utils.CommonHeaderUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@AllArgsConstructor
@Service("koodriveBusinessService")
public class KoodriveFilesServiceImpl implements IKoodriveFilesService {
    private final KoodriveConfig koodriveConfig;

    @Override
    public DownloadResponse getDownLoadUrl(DownloadInfoRequest req) throws KoodriveServerException {
        Optional<DownloadResponse> downloadResponse = HttpClient.doPost(koodriveConfig.getKoodriveHost() +
                        koodriveConfig.getBusinessConfig().getFileDownload(),
                CommonHeaderUtil.getCommonAuthHeader(),
                JSON.toJSONString(req),
                DownloadResponse.class);
        if (!downloadResponse.isPresent()) {
            throw new KoodriveServerException(KoodriveServerErrorCode.GETDOWNLOADURL_ERROR.getCode(),
                    KoodriveServerErrorCode.GETDOWNLOADURL_ERROR.getMsg());
        }
        return downloadResponse.get();
    }

    @Override
    public CreateFileResponse createFile(CreateFileRequest req) throws KoodriveServerException {
        Optional<CreateFileResponse> createFileResponse = HttpClient.doPost(koodriveConfig.getKoodriveHost() +
                        koodriveConfig.getBusinessConfig().getFileCreate(),
                CommonHeaderUtil.getCommonAuthHeader(),
                JSON.toJSONString(req),
                CreateFileResponse.class);
        if (!createFileResponse.isPresent()) {
            throw new KoodriveServerException(KoodriveServerErrorCode.CREATEFILE_ERROR.getCode(),
                    KoodriveServerErrorCode.CREATEFILE_ERROR.getMsg());
        }
        return createFileResponse.get();
    }

    @Override
    public KoodriveCommonResp completeFile(CompleteFileRequest req) throws KoodriveServerException {
        Optional<KoodriveCommonResp> koodriveCommonResp = HttpClient.doPost(koodriveConfig.getKoodriveHost() +
                        koodriveConfig.getBusinessConfig().getFileComplete(),
                CommonHeaderUtil.getCommonAuthHeader(),
                JSON.toJSONString(req),
                KoodriveCommonResp.class);
        if (!koodriveCommonResp.isPresent()) {
            throw new KoodriveServerException(KoodriveServerErrorCode.COMPLETEFILE_ERROR.getCode(),
                    KoodriveServerErrorCode.COMPLETEFILE_ERROR.getMsg());
        }
        return koodriveCommonResp.get();
    }

    @Override
    public SearchFileResponse searchFile(SearchFileRequest req) throws KoodriveServerException {
        Optional<SearchFileResponse> searchFileResponse = HttpClient.doPost(koodriveConfig.getKoodriveHost() +
                        koodriveConfig.getBusinessConfig().getFileSearch(),
                CommonHeaderUtil.getCommonAuthHeader(),
                null,
                SearchFileResponse.class);
        if (!searchFileResponse.isPresent()) {
            throw new KoodriveServerException(KoodriveServerErrorCode.SEARCHFILE_ERROR.getCode(),
                    KoodriveServerErrorCode.SEARCHFILE_ERROR.getMsg());
        }
        return searchFileResponse.get();
    }

    @Override
    public GetFullPathResponse getFilePath(GetFullFileRequest req) throws KoodriveServerException {
        StringBuilder builder = new StringBuilder(koodriveConfig.getKoodriveHost());
        builder.append(koodriveConfig.getBusinessConfig().getFilePath());
        builder.append("?fileId=");
        builder.append(req.getFileId());
        builder.append("&containerId=");
        builder.append(req.getContainerId());
        Optional<GetFullPathResponse> getFullPathResponse = HttpClient.doPost(builder.toString(),
                CommonHeaderUtil.getCommonAuthHeader(), null, GetFullPathResponse.class);
        if (!getFullPathResponse.isPresent()) {
            throw new KoodriveServerException(KoodriveServerErrorCode.SEARCHFILE_ERROR.getCode(),
                    KoodriveServerErrorCode.SEARCHFILE_ERROR.getMsg());
        }
        return getFullPathResponse.get();
    }

    @Override
    public FilePreviewAndEditLinkRsp previewFiles(FilePreviewAndEditLinkReq req) {
        return null;
    }

    @Override
    public FilePreviewAndEditLinkRsp editFile(FilePreviewAndEditLinkReq req) {
        return null;
    }

    @Override
    public QueryFileListRsp getPersonalFiles(QueryFileInfo fileListReq, String type) throws KoodriveServerException {
        StringBuilder builder = new StringBuilder(koodriveConfig.getKoodriveHost());
        builder.append(koodriveConfig.getBusinessConfig().getFilePersonal());
        String url = builder.toString();
        url = url.replace("{type}", type);
        Optional<QueryFileListRsp> queryFileListRsp = HttpClient.doPost(url, CommonHeaderUtil.getCommonAuthHeader(),
                JSON.toJSONString(fileListReq), QueryFileListRsp.class);
        if (!queryFileListRsp.isPresent()) {
            throw new KoodriveServerException(KoodriveServerErrorCode.GETPERSONALFILES_ERROR.getCode(),
                    KoodriveServerErrorCode.GETPERSONALFILES_ERROR.getMsg());
        }
        return queryFileListRsp.get();
    }

    @Override
    public QueryFileListRsp getDepartmentFiles(QueryFileInfo fileListReq, String groupId, String type, Integer teamType) throws KoodriveServerException {
        StringBuilder builder = new StringBuilder(koodriveConfig.getKoodriveHost());
        builder.append(koodriveConfig.getBusinessConfig().getFileDepartment());
        if (teamType != null) {
            builder.append("?team_type=");
            builder.append(teamType);
        }

        String url = builder.toString();
        url = url.replace("{groupId}", groupId);
        url = url.replace("{type}", type);
        Optional<QueryFileListRsp> queryFileListRsp = HttpClient.doPost(url, CommonHeaderUtil.getCommonAuthHeader(),
                JSON.toJSONString(fileListReq), QueryFileListRsp.class);
        if (!queryFileListRsp.isPresent()) {
            throw new KoodriveServerException(KoodriveServerErrorCode.GETDEPARTMENTFILES_ERROR.getCode(),
                    KoodriveServerErrorCode.GETDEPARTMENTFILES_ERROR.getMsg());
        }
        return queryFileListRsp.get();
    }

    @Override
    public QueryFileMetadataRsp getFile(ContainerRequest containerRequest, String fileId) throws KoodriveServerException {
        StringBuilder builder = new StringBuilder(koodriveConfig.getKoodriveHost());
        builder.append(koodriveConfig.getBusinessConfig().getFileDepartment());
        String url = builder.toString();
        url = url.replace("{fileId}", fileId);
        Optional<QueryFileMetadataRsp> queryFileMetadataRsp = HttpClient.doPost(url, CommonHeaderUtil.getCommonAuthHeader(),
                JSON.toJSONString(containerRequest), QueryFileMetadataRsp.class);
        if (!queryFileMetadataRsp.isPresent()) {
            throw new KoodriveServerException(KoodriveServerErrorCode.GETFILE_ERROR.getCode(),
                    KoodriveServerErrorCode.GETFILE_ERROR.getMsg());
        }
        return queryFileMetadataRsp.get();
    }

    @Override
    public QueryFileMetadataRsp createDirectory(DirectoryCreateRequest directoryCreateReq) throws KoodriveServerException {
        StringBuilder builder = new StringBuilder(koodriveConfig.getKoodriveHost());
        builder.append(koodriveConfig.getBusinessConfig().getFileDirectory());
        Optional<QueryFileMetadataRsp> queryFileMetadataRsp = HttpClient.doPost(builder.toString(),
                CommonHeaderUtil.getCommonAuthHeader(), null, QueryFileMetadataRsp.class);
        if (!queryFileMetadataRsp.isPresent()) {
            throw new KoodriveServerException(KoodriveServerErrorCode.CREATEDIRECOTRY_ERROR.getCode(),
                    KoodriveServerErrorCode.CREATEDIRECOTRY_ERROR.getMsg());
        }
        return queryFileMetadataRsp.get();
    }

    @Override
    public KoodriveCommonResp renameFile(UpdateFileInfo updateFileInfo, String fileId) throws KoodriveServerException {
        StringBuilder builder = new StringBuilder(koodriveConfig.getKoodriveHost());
        builder.append(koodriveConfig.getBusinessConfig().getFileRename());
        String url = builder.toString();
        url = url.replace("{fileId}", fileId);
        Optional<KoodriveCommonResp> koodriveCommonResp = HttpClient.doPost(url, CommonHeaderUtil.getCommonAuthHeader(),
                JSON.toJSONString(updateFileInfo), KoodriveCommonResp.class);
        if (!koodriveCommonResp.isPresent()) {
            throw new KoodriveServerException(KoodriveServerErrorCode.RENAMEFILE_ERROR.getCode(),
                    KoodriveServerErrorCode.RENAMEFILE_ERROR.getMsg());
        }
        return koodriveCommonResp.get();
    }
}
