/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.session.resp;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 刷新token响应体
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SessionRefreshResp extends KoodriveCommonResp {

    /**
     * 响应数据
     */
    private String accessToken;

    /**
     * csrfToken
     */
    private String csrfToken;
}
