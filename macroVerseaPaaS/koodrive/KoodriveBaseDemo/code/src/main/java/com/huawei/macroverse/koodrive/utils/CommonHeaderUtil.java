/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.utils;

import com.huawei.macroverse.koodrive.share.exception.KoodriveServerErrorCode;
import com.huawei.macroverse.koodrive.share.exception.KoodriveServerException;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * 公共方法
 */
public class CommonHeaderUtil {

    public static Map<String, String> getCommonAuthHeader() throws KoodriveServerException {
        RequestAttributes reqAttributes = RequestContextHolder.getRequestAttributes();
        if (!(reqAttributes instanceof ServletRequestAttributes)) {
            throw new KoodriveServerException(KoodriveServerErrorCode.UNKOWN_ERROR.getCode(),
                    KoodriveServerErrorCode.UNKOWN_ERROR.getMsg());
        }

        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) reqAttributes;
        HttpServletRequest request = requestAttributes.getRequest();
        String authorization = request.getHeader("Authorization");

        if (StringUtils.isBlank(authorization)) {
            Cookie[] cookies = request.getCookies();
            for (Cookie c : cookies) {
                if ("Authorization".equals(c.getName())) {
                    authorization = c.getValue();
                }
            }
        }

        Map<String, String> header = new HashMap<>();
        header.put("Authorization", authorization);
        return header;
    }

    public static void setAuthorizationCookie(String authCookie, String xSessionCookie) throws KoodriveServerException {
        RequestAttributes reqAttributes = RequestContextHolder.getRequestAttributes();
        if (!(reqAttributes instanceof ServletRequestAttributes)) {
            throw new KoodriveServerException(KoodriveServerErrorCode.UNKOWN_ERROR.getCode(),
                    KoodriveServerErrorCode.UNKOWN_ERROR.getMsg());
        }

        try {
            ServletRequestAttributes requestAttributes = (ServletRequestAttributes) reqAttributes;
            HttpServletResponse response = requestAttributes.getResponse();
            if (response == null) {
                return;
            }

            Cookie authorization = new Cookie("Authorization",
                    URLEncoder.encode(authCookie, StandardCharsets.UTF_8.name()));
            authorization.setHttpOnly(true);
            response.addCookie(authorization);

            if (StringUtils.isNoBlank(xSessionCookie)) {
                Cookie xSessionId = new Cookie("x-session-id", xSessionCookie);
                xSessionId.setHttpOnly(true);
                response.addCookie(xSessionId);
            }
        } catch (UnsupportedEncodingException e) {
            throw new KoodriveServerException(KoodriveServerErrorCode.ENCODE_ERROR.getCode(),
                    KoodriveServerErrorCode.ENCODE_ERROR.getMsg());
        }
    }
}
