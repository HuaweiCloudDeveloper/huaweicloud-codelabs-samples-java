/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PermissionDto {
    /**
     * 是否允许被搜索(仅适用于anyone类型)
     */
    private Boolean canDiscoverFile;

    /**
     * 类型
     * 必填
     */
    private String category;

    /**
     * 是否已删除
     * 必填
     */
    private Boolean deleted;

    /**
     * 名称
     */
    private String displayName;

    /**
     * 失效时间
     */
    private String expirationTime;

    /**
     * 权限ID
     * 必填
     */
    private String id;

    /**
     * 角色
     * 必填
     */
    private String role;

    /**
     * 类型
     */
    private String type;
}
