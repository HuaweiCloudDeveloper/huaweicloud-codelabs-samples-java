/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.manage.share;

import lombok.Getter;
import lombok.Setter;

/**
 * 空间详情的基础字段
 */
@Getter
@Setter
public class SpaceBaseFields extends DBBaseFields {
    private String ownerId;

    private Integer type;

    private Integer status;

    private Long capacity;

    private Long spaceUsed;

    private String containerId;

    private String rootFileId;

    private String bizRootFileId;

    private String bizSboxFileId;
}
