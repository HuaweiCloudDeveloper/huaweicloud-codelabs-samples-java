/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CipherDto {
    /**
     * 加密算法
     */
    private String algorithm;

    /**
     * 数据类型
     */
    private Integer dataType;

    /**
     * 加密文件的fek的hash
     */
    private String hfek;

    /**
     * 加密文件的IV
     */
    private String iv;

    /**
     * 密钥串
     */
    private List<KeyChainDto> keychains;
}
