/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model;

import lombok.Getter;
import lombok.Setter;

/**
 * koodrive 响应体公共父类
 */
@Getter
@Setter
public class KoodriveCommonResp {
    Integer code;

    String msg;

    public KoodriveCommonResp() {
    }

    public KoodriveCommonResp(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
