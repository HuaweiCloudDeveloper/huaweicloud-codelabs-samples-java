/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Data;

@Data
public class DownLoadLinkInfo {
    private String usage;
    private String downloadLink;
}
