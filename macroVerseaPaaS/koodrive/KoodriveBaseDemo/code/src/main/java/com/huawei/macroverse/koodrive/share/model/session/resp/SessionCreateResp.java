/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.session.resp;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import lombok.Data;

/**
 * 创建Session信息响应体
 */
@Data
public class SessionCreateResp extends KoodriveCommonResp {

    private SessionCreateData data;
}
