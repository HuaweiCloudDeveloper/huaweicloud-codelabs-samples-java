/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.req;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import lombok.Getter;
import lombok.Setter;

/**
 * 空间相关接口的请求参数
 */
@Getter
@Setter
public class ContainerRequest extends KoodriveCommonResp {
    private String containerId;
}
