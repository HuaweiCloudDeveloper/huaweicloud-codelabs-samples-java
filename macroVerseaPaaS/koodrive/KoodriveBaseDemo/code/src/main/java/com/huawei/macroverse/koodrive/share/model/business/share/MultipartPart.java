/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MultipartPart {
    /**
     * 分片编号
     */
    private Integer number;

    /**
     * 分片对象ID
     */
    private String partId;

    /**
     * 文件上传URL结构
     */
    private EndPointURL uploadUrl;
}
