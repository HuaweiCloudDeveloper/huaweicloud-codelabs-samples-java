/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.service.api.manage;

/**
 * koodrive管理面接口: 空间管理、部门管理、用户管理
 */
public interface IKoodriveManageService extends IKoodriveSpaceManage,IKoodriveDepartmentManage,IKoodriveUserManage {

}
