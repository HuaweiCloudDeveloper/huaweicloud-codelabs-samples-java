/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.manage.req;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 创建空间请求参数
 */
@Getter
@Setter
public class SpaceMgmtReq extends KoodriveCommonResp {
    private Long ownerId;

    private Integer type;

    private Integer status;

    private String containerId;

    private String rootFileId;

    private Long capacity;

    private Long spaceUsed;

    private String name;

    private List<String> userIds;

    private String iconUrl;

    private Integer top;

    private String tenantId;

    private List<Long> ownerIds;
}
