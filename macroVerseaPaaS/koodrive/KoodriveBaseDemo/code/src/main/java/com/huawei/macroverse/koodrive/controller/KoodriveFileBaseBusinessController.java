/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.controller;

import com.huawei.macroverse.koodrive.service.api.business.IKoodriveFilesService;
import com.huawei.macroverse.koodrive.share.exception.KoodriveServerException;
import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import com.huawei.macroverse.koodrive.share.model.business.req.CompleteFileRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.ContainerRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.CreateFileRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.DirectoryCreateRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.DownloadInfoRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.GetFullFileRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.QueryFileInfo;
import com.huawei.macroverse.koodrive.share.model.business.req.SearchFileRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.UpdateFileInfo;
import com.huawei.macroverse.koodrive.share.model.business.resp.CreateFileResponse;
import com.huawei.macroverse.koodrive.share.model.business.resp.DownloadResponse;
import com.huawei.macroverse.koodrive.share.model.business.resp.GetFullPathResponse;
import com.huawei.macroverse.koodrive.share.model.business.resp.QueryFileListRsp;
import com.huawei.macroverse.koodrive.share.model.business.resp.QueryFileMetadataRsp;
import com.huawei.macroverse.koodrive.share.model.business.resp.SearchFileResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文件管理相关接口
 */
@RestController
@RequestMapping("/koodrive/cloudfilesaas/v1")
@AllArgsConstructor
public class KoodriveFileBaseBusinessController {

    private IKoodriveFilesService businessService;

    /**
     * 批量获取文件下载地址
     *
     * @param req 请求参数
     * @return 下载地址信息
     * @throws KoodriveServerException 异常
     */
    @RequestMapping(value = "/files/download", method = RequestMethod.POST)
    public DownloadResponse getDownLoadUrl(@RequestBody DownloadInfoRequest req)
            throws KoodriveServerException {
        return businessService.getDownLoadUrl(req);
    }

    /**
     * 新增文件,文件上传的第一阶段
     *
     * @param req 新增文件请求体
     * @return 新增文件响应
     */
    @RequestMapping(value = "/files/create", method = RequestMethod.POST)
    public CreateFileResponse createFile(@RequestBody CreateFileRequest req) throws KoodriveServerException {
        return businessService.createFile(req);
    }

    /**
     * 上传完成通知,文件上传的第三阶段
     *
     * @param req 上传完成通知请求体
     * @return 上传完成通知响应
     */
    @RequestMapping(value = "/files/complete", method = RequestMethod.POST)
    public KoodriveCommonResp completeFile(@RequestBody CompleteFileRequest req) throws KoodriveServerException {
        return businessService.completeFile(req);
    }

    /**
     * 文件搜索
     *
     * @param req 搜索文件的条件
     * @return SearchFileResponse 满足条件的文件列表
     */
    @RequestMapping(value = "/files/search", method = RequestMethod.POST)
    public SearchFileResponse searchFile(@RequestBody SearchFileRequest req) throws KoodriveServerException {
        return businessService.searchFile(req);
    }

    /**
     * 查询文件的路径
     *
     * @param fileId 文件id
     * @param containerId 空间id
     * @return GetFullPathResponse 文件路径结果
     */
    @RequestMapping(value = "/files/path", method = RequestMethod.GET)
    public GetFullPathResponse getFilePath(@RequestParam(name = "fileId") String fileId,
                                           @RequestParam(name = "containerId") String containerId) throws KoodriveServerException {
        return businessService.getFilePath(new GetFullFileRequest(fileId, containerId));
    }

    /**
     * 获取个人文件列表
     *
     * @param fileListReq 请求体
     * @param type        文件类型
     * @return 文件列表
     */
    @RequestMapping(value = "/files/{type}", method = RequestMethod.POST)
    @ResponseBody
    public QueryFileListRsp getPersonalFiles(@RequestBody QueryFileInfo fileListReq,
                                             @PathVariable("type") String type) throws KoodriveServerException {
        return businessService.getPersonalFiles(fileListReq, type);
    }

    /**
     * 获取团队文件列表
     *
     * @param fileListReq 请求参数
     * @param groupId     空间id
     * @param type        文件类型
     * @return 团队文件列表结果
     */
    @RequestMapping(value = "/files/{groupId}/{type}", method = RequestMethod.POST)
    public QueryFileListRsp getDepartmentFiles(@RequestBody QueryFileInfo fileListReq,
                                               @PathVariable("groupId") String groupId, @PathVariable("type") String type,
                                               @RequestParam(value = "team_type", required = false) Integer teamType) throws KoodriveServerException {
        return businessService.getDepartmentFiles(fileListReq, groupId, type, teamType);
    }

    /**
     * 获取文件详情
     *
     * @param containerRequest 请求参数
     * @param fileId           文件id
     * @return 文件详情
     */
    @RequestMapping(value = "/files/{fileId}/detail", method = RequestMethod.POST)
    @ResponseBody
    public QueryFileMetadataRsp getFile(@RequestBody ContainerRequest containerRequest,
                                        @PathVariable("fileId") String fileId) throws KoodriveServerException {
        return businessService.getFile(containerRequest, fileId);
    }

    /**
     * 创建目录
     *
     * @param directoryCreateReq 请求参数
     * @return 创建结果
     */
    @RequestMapping(value = "/files/directory", method = RequestMethod.POST)
    @ResponseBody
    public QueryFileMetadataRsp createDirectory(@RequestBody DirectoryCreateRequest directoryCreateReq) throws KoodriveServerException {
        return businessService.createDirectory(directoryCreateReq);
    }

    /**
     * 文件重命名
     *
     * @param updateFileInfo 请求参数
     * @param fileId         文件id
     * @return 重命名结果
     */
    @RequestMapping(value = "/files/rename/{fileId}", method = RequestMethod.POST)
    @ResponseBody
    public KoodriveCommonResp renameFile(@RequestBody UpdateFileInfo updateFileInfo,
                                         @PathVariable("fileId") String fileId) throws KoodriveServerException {
        return businessService.renameFile(updateFileInfo, fileId);
    }
}
