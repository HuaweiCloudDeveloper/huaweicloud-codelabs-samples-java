/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.req;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import com.huawei.macroverse.koodrive.share.model.business.share.MultiPartInfo;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.List;

/**
 * 文件上传第一段请求参数
 */
@Getter
@Setter
public class CreateFileRequest extends KoodriveCommonResp {
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String createdTime;

    private String description;

    private String mimeType;

    private String fileName;

    private String fileType;

    private List<String> parentFolder;

    private String sha256;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String editTime;

    private Long length;

    private String uploadMode;

    private Integer uploadType;

    private Integer autoRename;

    private List<MultiPartInfo> multiParts;

    private String containerId;
}
