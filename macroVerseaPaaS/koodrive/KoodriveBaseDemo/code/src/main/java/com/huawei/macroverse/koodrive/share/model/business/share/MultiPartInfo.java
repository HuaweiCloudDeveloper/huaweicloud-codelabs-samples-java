/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MultiPartInfo {
    private Integer partNumber;

    private Long partSize;

    private Long partOffset;

    private List<Long> hashCtx;

    private String uploadUrl;

    private String partId;
}
