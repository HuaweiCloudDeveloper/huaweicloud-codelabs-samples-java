/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.service.api.business;

import com.huawei.macroverse.koodrive.share.exception.KoodriveServerException;
import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import com.huawei.macroverse.koodrive.share.model.business.req.CompleteFileRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.ContainerRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.CreateFileRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.DirectoryCreateRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.DownloadInfoRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.FilePreviewAndEditLinkReq;
import com.huawei.macroverse.koodrive.share.model.business.req.GetFullFileRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.SearchFileRequest;
import com.huawei.macroverse.koodrive.share.model.business.req.QueryFileInfo;
import com.huawei.macroverse.koodrive.share.model.business.req.UpdateFileInfo;
import com.huawei.macroverse.koodrive.share.model.business.resp.CreateFileResponse;
import com.huawei.macroverse.koodrive.share.model.business.resp.DownloadResponse;
import com.huawei.macroverse.koodrive.share.model.business.resp.FilePreviewAndEditLinkRsp;
import com.huawei.macroverse.koodrive.share.model.business.resp.GetFullPathResponse;
import com.huawei.macroverse.koodrive.share.model.business.resp.QueryFileListRsp;
import com.huawei.macroverse.koodrive.share.model.business.resp.QueryFileMetadataRsp;
import com.huawei.macroverse.koodrive.share.model.business.resp.SearchFileResponse;

/**
 * 文件基础操作相关接口：文件查询、上传、下载、搜索、重命名等
 */
public interface IKoodriveBaseFiles {

    /**
     * 获取文件下载地址
     *
     * @param req 下载文件请求参数
     * @return 文件下载的url地址
     */
    DownloadResponse getDownLoadUrl(DownloadInfoRequest req) throws KoodriveServerException;

    /**
     * 第一段 文件上传
     *
     * @param req 文件描述信息
     * @return 文件上传结果
     */
    CreateFileResponse createFile(CreateFileRequest req) throws KoodriveServerException;

    /**
     * 上传完成通知,文件上传的第三阶段
     *
     * @param req 上传完成通知请求体
     * @return 上传完成通知响应
     */
    KoodriveCommonResp completeFile(CompleteFileRequest req) throws KoodriveServerException;

    /**
     * 文件搜索
     *
     * @param req 文件搜索请求体
     * @return 文件搜索响应
     */
    SearchFileResponse searchFile(SearchFileRequest req) throws KoodriveServerException;

    /**
     * 查询文件全路径
     *
     * @param req 查询文件全路径请求体
     * @return 查询文件全路径响应
     */
    GetFullPathResponse getFilePath(GetFullFileRequest req) throws KoodriveServerException;

    /**
     * 获取文件在线预览链接
     *
     * @param req 预览链接请求参数
     * @return FilePreviewAndEditLinkRsp
     */
    FilePreviewAndEditLinkRsp previewFiles(FilePreviewAndEditLinkReq req);

    /**
     * 获取文件在线编辑链接
     *
     * @param req 编辑链接请求参数
     * @return FilePreviewAndEditLinkRsp
     */
    FilePreviewAndEditLinkRsp editFile(FilePreviewAndEditLinkReq req);

    /**
     * 获取个人文件列表
     *
     * @param fileListReq 请求体
     * @param type 文件类型
     * @return 文件列表
     */
    QueryFileListRsp getPersonalFiles(QueryFileInfo fileListReq, String type) throws KoodriveServerException;

    /**
     * 获取团队文件列表
     *
     * @param fileListReq 请求参数
     * @param groupId 空间id
     * @param type 文件类型
     * @return 团队文件列表结果
     */
    QueryFileListRsp getDepartmentFiles(QueryFileInfo fileListReq, String groupId, String type, Integer teamType) throws KoodriveServerException;

    /**
     * 获取文件详情
     *
     * @param containerRequest 请求参数
     * @param fileId 文件id
     * @return 文件详情
     */
    QueryFileMetadataRsp getFile(ContainerRequest containerRequest, String fileId) throws KoodriveServerException;

    /**
     * 创建目录
     *
     * @param directoryCreateReq 请求参数
     * @return 创建结果
     */
    QueryFileMetadataRsp createDirectory( DirectoryCreateRequest directoryCreateReq) throws KoodriveServerException;

    /**
     * 文件重命名
     *
     * @param updateFileInfo 请求参数
     * @param fileId 文件id
     * @return 重命名结果
     */
    KoodriveCommonResp renameFile(UpdateFileInfo updateFileInfo, String fileId) throws KoodriveServerException;
}
