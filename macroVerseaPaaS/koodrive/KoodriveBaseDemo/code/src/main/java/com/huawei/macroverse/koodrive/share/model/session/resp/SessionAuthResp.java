/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.session.resp;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import lombok.Data;

/**
 * auth认证响应体
 */
@Data
public class SessionAuthResp extends KoodriveCommonResp {

    /**
     * 响应数据
     */
    private UserInfo data;

    public SessionAuthResp() {
    }

    public SessionAuthResp(UserInfo data) {
        this.data = data;
    }

    public SessionAuthResp(int retCode, String retMsg) {
        super(retCode, retMsg);
    }
}
