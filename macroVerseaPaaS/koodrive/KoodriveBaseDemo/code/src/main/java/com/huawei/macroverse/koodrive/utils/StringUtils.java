/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.utils;

/**
 * String工具类
 */
public final class StringUtils {

    /**
     * 字符串工具类
     *
     * @author w00344032
     * @version V1.0.0
     * @since 2022-10-31
     */

    /**
     * 空字符串
     */
    public static final String EMPTY = "";

    /**
     * 私有构造方法
     */
    private StringUtils() {
        super();
    }

    /**
     * 是否为空串(判断是否为null 或 "") isEmpty(null) = true; isEmpty("") = true; isEmpty(" ") = false;
     *
     * @param value 输入的字符串
     * @return 是否为空串
     */
    public static boolean isEmpty(String value) {
        return (null == value) || (0 == value.length());
    }

    /**
     * 是否为空格串(判断是否为null 、"" 或 " ") isBlank(null) = true; isBlank("") = true; isBlank(" ") = true;
     *
     * @param value 输入的字符串
     * @return 是否为空格串
     */
    public static boolean isBlank(String value) {
        // 是否为空串，如果为空串直接返回
        return (null == value) || (0 == value.length()) || EMPTY.equals(value.trim());
    }

    /**
     * 字符串是否为非空
     *
     * @param value 输入的字符串
     * @return 是否为非空格串
     */
    public static boolean isNoBlank(String value) {
        return !isBlank(value);
    }

}

