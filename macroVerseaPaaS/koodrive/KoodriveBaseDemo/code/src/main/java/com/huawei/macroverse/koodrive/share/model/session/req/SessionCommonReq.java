/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.session.req;

import lombok.Getter;
import lombok.Setter;

/**
 * Session通用请求入参
 */
@Getter
@Setter
public class SessionCommonReq {
    private String sessionId;

    private String accessToken;

    private String language;

    public SessionCommonReq(String accessToken) {
        this.accessToken = accessToken;
    }

    public SessionCommonReq(String sessionId, String accessToken,String language) {
        this.sessionId = sessionId;
        this.accessToken = accessToken;
        this.language = language;
    }

    public SessionCommonReq(String accessToken,String language) {
        this.accessToken = accessToken;
        this.language = language;
    }

}
