/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.resp;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * 文件信息
 */
@Setter
@Getter
public class SingleFileInfo {
    /**
     * 上传地址
     */
    private String url;

    /**
     * 上传请求头
     */
    private Map<String, String> headers;
}
