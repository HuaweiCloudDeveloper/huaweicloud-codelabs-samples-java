/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorDataDto {
    /**
     * 冲突的原文件标识
     */
    private String conflictFileId;
}
