/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FileInfo {

    /**
     * 类型,drive#file
     */
    private String category;

    /**
     * 文件id
     */
    private String id;

    /**
     * 文件名,最长160个字节
     */
    private String fileName;

    /**
     * 文件类型,外部输入
     */
    private String fileType;

    /**
     * 内容类型
     */
    private String mimeType;

}
