/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.resp;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import com.huawei.macroverse.koodrive.share.model.business.share.MultiPartInfo;
import com.huawei.macroverse.koodrive.share.model.business.share.UploadFormInfo;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 新增文件响应体
 */
@Getter
@Setter
public class CreateFileResponse extends KoodriveCommonResp {
    private String fileId;

    private Boolean rapidUpload;

    private String containerId;

    private String fileName;

    private String uploadId;

    private List<String> parentFolder;

    private Boolean exist;

    private List<MultiPartInfo> multiParts;

    private UploadFormInfo formInfo;

    private SingleFileInfo singleFileInfo;
}
