/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.req;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import com.huawei.macroverse.koodrive.share.model.business.share.PageInfo;
import com.huawei.macroverse.koodrive.share.model.business.share.SearchFilter;
import com.huawei.macroverse.koodrive.share.model.business.share.SortInfo;
import lombok.Data;

/**
 * 查询文件请求参数
 */
@Data
public class SearchFileRequest extends KoodriveCommonResp {
    private String searchType;

    private PageInfo pageInfo;

    private SearchFilter filter;

    private SortInfo sortInfo;

    /**
     * 是否递归查询
     */
    private Boolean recursived = false;

    /**
     * 回收站查询
     */
    private Boolean recycled = false;
}
