/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class EndPointURL {
    /**
     * 文件资源url
     */
    private String url;

    /**
     * 请求方式
     */
    private String method;

    /**
     * 请求头信息
     */
    private Map<String, String> headers;
}
