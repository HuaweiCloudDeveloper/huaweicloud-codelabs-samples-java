/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.http;

import com.alibaba.fastjson.JSON;
import com.huawei.macroverse.koodrive.share.exception.KoodriveServerException;
import com.huawei.macroverse.koodrive.share.exception.KoodriveServerErrorCode;
import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import com.huawei.macroverse.koodrive.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.http.MediaType;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.Optional;

/**
 * Http client 工具类
 *
 * @param <T> 返回的响应体
 */
@Slf4j
public class HttpClient<T extends KoodriveCommonResp> {

    private static final Integer MAX_TIMEOUT = 600000;

    private static final Integer HTTP_POOL_MAX = 2000;

    private static final String STR_HTTP = "http";

    private static final String STR_HTTPS = "https";

    private static final RequestConfig REQUEST_CONFIG;

    private static final CloseableHttpClient CLOSEABLE_HTTP_CLIENT;

    private static final String[] SUPPORTED_CIPHER_SUITES = {"TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384",
            "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256", "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256",
            "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384"};

    static {
        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register(STR_HTTP, PlainConnectionSocketFactory.INSTANCE)
                .register(STR_HTTPS, createSSLConnSocketFactory()).build();
        // 设置连接池
        final PoolingHttpClientConnectionManager connMgr = new PoolingHttpClientConnectionManager(
                socketFactoryRegistry);
        // 设置连接池大小
        connMgr.setMaxTotal(HTTP_POOL_MAX);
        connMgr.setDefaultMaxPerRoute(connMgr.getMaxTotal());
        RequestConfig.Builder configBuilder = RequestConfig.custom();
        // 设置连接超时
        configBuilder.setConnectTimeout(MAX_TIMEOUT);
        // 设置读取超时
        configBuilder.setSocketTimeout(MAX_TIMEOUT);
        // 设置从连接池获取连接实例的超时
        configBuilder.setConnectionRequestTimeout(MAX_TIMEOUT);
        REQUEST_CONFIG = configBuilder.build();

        SSLContext sslContext = null;
        try {
            sslContext = new SSLContextBuilder().loadTrustMaterial(null, (chain, authType) -> true).build();
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            log.error("create Client is error,error message is : ", e);
        }
        assert sslContext != null;
        SSLConnectionSocketFactory sslsf =
                new SSLConnectionSocketFactory(
                        sslContext,
                        new String[]{"TLSv1.2"},
                        SUPPORTED_CIPHER_SUITES,
                        SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

        CLOSEABLE_HTTP_CLIENT =
                HttpClients.custom().setConnectionManager(connMgr).setDefaultRequestConfig(REQUEST_CONFIG).setSSLSocketFactory(sslsf).build();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                CLOSEABLE_HTTP_CLIENT.close();
            } catch (IOException ignored) {
            }
        }));
    }


    private static String doGet(String url, Map<String, String> headers) throws KoodriveServerException {
        HttpGet httpGet = new HttpGet(url);
        // 设置请求头
        if (null != headers) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                httpGet.addHeader(entry.getKey(), entry.getValue());
            }
        }
        try {
            CloseableHttpResponse response = CLOSEABLE_HTTP_CLIENT.execute(httpGet);
            return analysisRequestResult2String(url, response);
        } catch (IOException e) {
            log.error("doGet have a IOException e = : ", e);
            throw new KoodriveServerException(KoodriveServerErrorCode.IO_ERROR.getCode(), KoodriveServerErrorCode.IO_ERROR.getMsg());
        } finally {
            httpGet.releaseConnection();
        }
    }

    public static <T> Optional<T> doGet(String url, Map<String, String> headers, Class<T> responseClazz) throws KoodriveServerException {
        try {
            String out = doGet(url, headers);
            if (StringUtils.isEmpty(out)) {
                log.debug("analysisRequestCodeArtsResult, out = {}", out);
                return Optional.empty();
            }
            return Optional.ofNullable(JSON.parseObject(out, responseClazz));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    /**
     * 发送 POST 请求
     *
     * @param url    API接口URL
     * @param params 参数map
     * @return
     */
    public static String doPost(String url, Map<String, String> headers, String params) throws KoodriveServerException {
        HttpPost httpPost = getHttpPost(url, headers);
        try {
            setEntity(params, httpPost);
            CloseableHttpResponse response = CLOSEABLE_HTTP_CLIENT.execute(httpPost);
            return analysisRequestResult2String(url, response);
        } catch (IOException e) {
            log.error("doPost have a IOException e = : ", e);
            throw new KoodriveServerException(KoodriveServerErrorCode.IO_ERROR.getCode(), KoodriveServerErrorCode.IO_ERROR.getMsg());
        } finally {
            httpPost.releaseConnection();
        }
    }

    public static String doPut(String url, Map<String, String> headers, String params) throws KoodriveServerException {
        HttpPut httpPut = new HttpPut(url);
        httpPut.setConfig(REQUEST_CONFIG);
        // 设置请求头
        if (null != headers) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                httpPut.addHeader(entry.getKey(), entry.getValue());
            }
        }
        StringEntity stringEntity = new StringEntity(params, "UTF-8");
        stringEntity.setContentEncoding("UTF-8");
        stringEntity.setContentType(MediaType.APPLICATION_JSON_VALUE);
        httpPut.setEntity(stringEntity);

        try {
            CloseableHttpResponse response = CLOSEABLE_HTTP_CLIENT.execute(httpPut);
            return analysisRequestResult2String(url, response);
        } catch (IOException e) {
            log.error("doPut have a IOException e = : ", e);
            throw new KoodriveServerException(KoodriveServerErrorCode.IO_ERROR.getCode(), KoodriveServerErrorCode.IO_ERROR.getMsg());
        } finally {
            httpPut.releaseConnection();
        }
    }

    public static <T> Optional<T> doPut(String url, Map<String, String> headers, String params,
                                        Class<T> responseClazz) throws KoodriveServerException {
        try {
            String out = doPut(url, headers, params);
            if (StringUtils.isEmpty(out)) {
                log.debug("analysisRequestCodeArtsResult, out = {}", out);
                return Optional.empty();
            }
            return Optional.ofNullable(JSON.parseObject(out, responseClazz));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public static String doDelete(String url, Map<String, String> headers) throws KoodriveServerException {
        HttpDelete httpDelete = new HttpDelete(url);
        httpDelete.setConfig(REQUEST_CONFIG);
        // 设置请求头
        if (null != headers) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                httpDelete.addHeader(entry.getKey(), entry.getValue());
            }
        }

        try {
            CloseableHttpResponse response = CLOSEABLE_HTTP_CLIENT.execute(httpDelete);
            return analysisRequestResult2String(url, response);
        } catch (IOException e) {
            log.error("doDeleteWithBody have a IOException e = : ", e);
            throw new KoodriveServerException(KoodriveServerErrorCode.IO_ERROR.getCode(), KoodriveServerErrorCode.IO_ERROR.getMsg());
        } finally {
            httpDelete.releaseConnection();
        }
    }

    public static <T> T doDelete(String url, Map<String, String> headers,
                                 Class<T> responseClazz) throws KoodriveServerException {
        String out = doDelete(url, headers);
        if (StringUtils.isEmpty(out)) {
            log.debug("analysisRequestCodeArtsResult, out = {}", out);
            return null;
        }
        return JSON.parseObject(out, responseClazz);
    }

    public static <T> Optional<T> doPost(String url, Map<String, String> headers, String params, Class<T> responseClazz) {
        try {
            String out = doPost(url, headers, params);
            if (StringUtils.isEmpty(out)) {
                log.debug("analysisRequestCodeArtsResult, out = {}", out);
                return Optional.empty();
            }
            return Optional.ofNullable(JSON.parseObject(out, responseClazz));
        } catch (Exception | KoodriveServerException e) {
            return Optional.empty();
        }
    }

    private static void setEntity(String params, HttpPost httpPost) {
        if (StringUtils.isEmpty(params)) {
            return;
        }
        StringEntity stringEntity = new StringEntity(params, StandardCharsets.UTF_8);
        stringEntity.setContentEncoding("UTF-8");
        stringEntity.setContentType("application/json");
        httpPost.setEntity(stringEntity);
    }

    private static HttpPost getHttpPost(String url, Map<String, String> headers) throws KoodriveServerException {
        try {
            URIBuilder uriBuilder = new URIBuilder(url);
            HttpPost httpPost = new HttpPost(uriBuilder.build().toString());
            httpPost.setConfig(REQUEST_CONFIG);
            // 设置请求头
            if (null != headers) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    httpPost.addHeader(entry.getKey(), entry.getValue());
                }
            }
            return httpPost;
        } catch (URISyntaxException e) {
            log.error("[HttpClientUtils]usr syntax error. url={},exception={}", url, e);
            throw new KoodriveServerException(e.getMessage());
        }
    }

    private static String analysisRequestResult2String(String url, CloseableHttpResponse response)
            throws IOException, KoodriveServerException {
        int statusCode = response.getStatusLine().getStatusCode();
        log.info("analysisRequestCodeArtsResult, statusCode = {}", statusCode);
        String out = response.getEntity() == null ? "" : EntityUtils.toString(response.getEntity(), "utf-8");
        if (statusCode < HttpStatus.SC_OK || statusCode >= HttpStatus.SC_MULTIPLE_CHOICES) {
            log.error("Failed to send request. url = {}, statusCode = {}", url, statusCode);
            log.error("Failed to send request. {}", out);
            throw new KoodriveServerException(statusCode, out);
        }
        return out;
    }

    /**
     * 创建SSL安全连接
     *
     * @return
     */
    private static SSLConnectionSocketFactory createSSLConnSocketFactory() {
        SSLConnectionSocketFactory sslsf = null;
        try {
            SSLContext ctx = SSLContext.getInstance("TLSv1.2");
            X509TrustManager tm =
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(X509Certificate[] chain, String authType)
                                throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(X509Certificate[] chain, String authType)
                                throws CertificateException {
                        }

                        @Override
                        public X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[0];
                        }
                    };
            ctx.init(null, new TrustManager[]{tm}, SecureRandom.getInstanceStrong());
            // 创建SSL安全连接
            sslsf = new SSLConnectionSocketFactory(ctx, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        } catch (GeneralSecurityException e) {
            log.error("Failed createSSLConnSocketFactory .", e);
        }
        return sslsf;
    }
}
