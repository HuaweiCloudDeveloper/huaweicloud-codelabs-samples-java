/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ImagesDto {
    /**
     * 文件资源名称
     */
    private String process;

    /**
     * 文件下载信息
     */
    private EndPointURL downloadUrl;
}
