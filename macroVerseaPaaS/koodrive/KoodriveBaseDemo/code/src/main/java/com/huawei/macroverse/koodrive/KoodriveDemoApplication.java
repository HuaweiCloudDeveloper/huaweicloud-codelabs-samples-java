/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * Spring boot入口函数
 */
@SpringBootApplication
@ServletComponentScan("com.huawei.macroverse.koodrive.filter")
public class KoodriveDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(KoodriveDemoApplication.class, args);
    }
}
