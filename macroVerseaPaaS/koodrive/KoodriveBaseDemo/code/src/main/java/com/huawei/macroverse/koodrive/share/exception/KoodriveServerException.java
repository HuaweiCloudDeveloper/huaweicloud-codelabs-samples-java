/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.exception;

import lombok.Getter;
import lombok.Setter;

import java.util.function.Supplier;

/**
 * koodrive 错误类型定义
 */
@Setter
@Getter
public class KoodriveServerException extends Throwable  {

    /**
     * 异常码
     */
    private Integer code;

    /**
     * 错误消息
     */
    private String message;

    /**
     * 构造方法
     *
     * @param message 错误消息
     */
    public KoodriveServerException( String message) {
        this.message = message;
    }

    /**
     * 构造方法
     *
     * @param code    异常码
     * @param message 错误消息
     */
    public KoodriveServerException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
