/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.req;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * 创建目录请求参数
 */
@Setter
@Getter
public class DirectoryCreateRequest extends KoodriveCommonResp {

    /**
     * 文件创建时间
     */
    private Date createdTime;

    /**
     * 文件描述。字符长度不超过512字符，不能包括emoji等非法字符。
     */
    private String description;

    /**
     * 资源类型
     */
    private String mimeType;

    /**
     * 文件修改时间
     */
    private Date editedTime;

    /**
     * 文件名，最长80个字符。不能包括'<>|:"*?/'，不能等于'..','.',''
     */
    private String fileName;

    /**
     * 文件类型，外部输入
     */
    private String fileType;

    /**
     * 父目录标识列表，只取第一个作为父目录
     */
    private List<String> parentFolder;

    /**
     * 空间标识ID，container和parentAlias和parentFolder互斥，优先使用parentFolder。
     */
    private String containerId;

    /**
     * 2：强制重命名，3：拒绝重命名
     */
    private String renameMode;
}
