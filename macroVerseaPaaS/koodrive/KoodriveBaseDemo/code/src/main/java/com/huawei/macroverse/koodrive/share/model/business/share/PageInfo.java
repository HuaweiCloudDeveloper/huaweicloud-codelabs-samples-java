/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PageInfo {

    /**
     * 每页数量，取值范围[1,100]，默认100
     */
    private Integer pageSize = 100;

    /**
     * 起始游标，为空时从第一页开始查询
     */
    private String pageCursor;
}
