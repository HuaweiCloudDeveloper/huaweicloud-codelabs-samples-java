/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.req;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import lombok.Getter;
import lombok.Setter;

/**
 * 文件预览请求参数
 */
@Getter
@Setter
public class FilePreviewAndEditLinkReq extends KoodriveCommonResp {

    /**
     * 文件格式，可选值： w :文字文件 s :表格文件 p :演示文件 f :PDF文件 x :图片文件和压缩包文件
     */
    private String type;

    /**
     * 预览模式，可选值：
     * high_definition :高清预览
     * ordinary :普通预览
     * cache :缓存预览
     * 不传默认high_definition
     * official :公文极速预览
     */
    private String previewMode;

    /**
     * 文件id
     */
    private String fileId;
}
