/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Data;

@Data
public class SortInfo {

    /**
     * 文件名 fileName
     * 文件大小 size
     * 文件类型 type
     * 更新时间 updateTime
     * 创建时间 createTime
     * 删除时间 deleteTime
     */

    /**
     * 排序类型
     * 1:文件名排序，2：文件大小排序，3：创建时间排序，4：修改时间排序，5：回收时间（只针对于回收文件）
     */
    private String sortType;

    /**
     * 顺序
     * 1：升序  asc，2：降序 desc
     */
    private String sortDirection;
}
