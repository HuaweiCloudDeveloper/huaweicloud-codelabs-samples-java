/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.resp;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import com.huawei.macroverse.koodrive.share.model.business.share.FileDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 查询文件列表响应体
 */
@Setter
@Getter
public class QueryFileListRsp extends KoodriveCommonResp {

    /**
     * 资源类型，固定值为drive#fileList
     */
    private String category = "drive#fileList";

    /**
     * 分页游标，如果不存在，代表已返回最后一个
     */
    private String nextCursor;

    /**
     * 是否是完整的搜索
     */
    private boolean searchCompleted;

    /**
     * 文件列表
     */
    private List<FileDto> files;
}
