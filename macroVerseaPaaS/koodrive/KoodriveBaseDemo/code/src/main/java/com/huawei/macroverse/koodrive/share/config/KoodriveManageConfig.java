/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 管理面接口配置
 */
@Setter
@Getter
@Configuration
@ConfigurationProperties(prefix = "koodrive.manage")
public class KoodriveManageConfig {
    private String spaceOwner;
    private String spaceCreate;
}
