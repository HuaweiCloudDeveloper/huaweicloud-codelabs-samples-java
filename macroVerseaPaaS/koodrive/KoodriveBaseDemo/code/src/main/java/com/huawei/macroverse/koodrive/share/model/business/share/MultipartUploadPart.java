/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MultipartUploadPart {
    /**
     * 分片编号
     */
    private Integer number;

    /**
     * 分片长度, 取值范围[5M, 5GB], 最后一个分片例外[0, 5GB]
     */
    private Long length;

    /**
     * 分片的sha256(可选)
     */
    private Long sha256;

    /**
     * 迭代计算Hash的上下文
     */
    private String iterationHashCtx;

    /**
     * 到上一个数据块为止的总长度
     */
    private Long partOffset;

    /**
     * 用于迭代计算hash值上下文，该字段仅在分片并行上
     */
    private List<Long> hashCtx;
}
