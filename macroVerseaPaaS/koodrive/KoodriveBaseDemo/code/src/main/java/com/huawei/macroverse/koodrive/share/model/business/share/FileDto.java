/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
public class FileDto {
    /**
     * 文件id
     */
    private String id;

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 文件类型
     */
    private String mimeType;

    /**
     * 后缀名
     */
    private String fileSuffix;

    /**
     * 是否已被回收
     */
    private Boolean recycled;

    /**
     * 后缀名
     */
    private String recycledTime;

    /**
     * 文件大小
     */
    private Long size;

    /**
     * 是否被直接回收
     */
    private Boolean directlyRecycled;

    /**
     * 父目录
     */
    private String[] parentFolder;

    /**
     * 容器
     */
    private String[] containers;

    /**
     * 属性
     */
    private UserProperties userProperties;

    /**
     * 属性
     */
    private Map<String, Object> properties;

    /**
     * 文件的sha256
     */
    private String sha256;

    /**
     * 附件列表
     */
    private List<AttachmentDto> attachments;

    /**
     * owners
     */
    private List<UserDto> owners;

    /**
     * 最后编辑用户
     */
    private UserDto lastEditor;

    /**
     * 文件类型
     */
    private String fileType;

    /**
     * 云空间文件全路劲
     */
    private String idPath;

    /**
     * 内容和缩略图下载地址列表，FileProxy方式地址
     */
    private List<DownloadLinks> downloadLinks;

    /**
     * 文件创建时间
     */
    private String createdTime;

    /**
     * 文件编辑时间
     */
    private String editedTime;

    /**
     * 收藏标识
     */
    private Boolean favorite;
}
