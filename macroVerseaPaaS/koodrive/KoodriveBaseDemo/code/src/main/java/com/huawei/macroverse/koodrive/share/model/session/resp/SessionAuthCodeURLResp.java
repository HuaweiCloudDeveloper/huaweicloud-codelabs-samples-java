/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.session.resp;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import lombok.Data;

/**
 * authCodURL接口响应
 */
@Data
public class SessionAuthCodeURLResp extends KoodriveCommonResp {

    /**
     * 响应数据
     */
    private String uri;

    private String orgIdHost;
}
