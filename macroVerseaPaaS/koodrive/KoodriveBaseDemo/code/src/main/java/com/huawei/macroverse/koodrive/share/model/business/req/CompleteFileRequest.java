/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.req;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import lombok.Getter;
import lombok.Setter;

/**
 * 文件上传完成请求体
 */
@Getter
@Setter
public class CompleteFileRequest extends KoodriveCommonResp {
    private String fileId;
}
