/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 业务面接口uri配置
 */
@Setter
@Getter
@Configuration
@ConfigurationProperties(prefix = "koodrive.business")
public class KoodriveBusinessConfig {
    private String fileDownload;
    private String fileCreate;
    private String fileComplete;
    private String fileSearch;
    private String filePath;
    private String filePreview;
    private String fileEdit;
    private String filePersonal;
    private String fileDepartment;
    private String fileDetail;
    private String fileDirectory;
    private String fileRename;
}
