/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.resp;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import lombok.Data;

/**
 * 获取文件全路径响应体
 */
@Data
public class GetFullPathResponse extends KoodriveCommonResp {
    private String fileIdPath;
    private String namePath;
}
