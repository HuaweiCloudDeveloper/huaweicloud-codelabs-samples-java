/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.manage.share;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 空间详情
 */
@Getter
@Setter
public class SpaceMgmt extends SpaceBaseFields {
    private String id;

    private List<String> ownerIds;

    private String administrators;

    private Integer userNum;
}
