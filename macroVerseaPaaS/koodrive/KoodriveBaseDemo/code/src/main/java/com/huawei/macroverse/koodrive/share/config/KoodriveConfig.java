/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * koodirve所有接口汇总
 */
@Setter
@Getter
@Configuration
public class KoodriveConfig {
    @Value("${koodrive.base.host}")
    private String koodriveHost;

    @Value("${koodrive.base.domain}")
    private String koodriveDomain;

    @Value("${koodrive.base.language}")
    private String koodriveLanguage;

    @Autowired
    private KoodriveSessionConfig sessionConfig;

    @Autowired
    private KoodriveBusinessConfig businessConfig;

    @Autowired
    private KoodriveManageConfig manageConfig;
}
