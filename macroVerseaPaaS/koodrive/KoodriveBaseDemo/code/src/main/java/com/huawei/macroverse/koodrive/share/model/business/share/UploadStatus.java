/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class UploadStatus {
    /**
     * 上传的HTTP消息的body
     */
    private String body;

    /**
     * 上传的HTTP响应信息
     */
    private Map<String, String> headers;

    /**
     * 上传的HTTP响应Code
     */
    private Integer status;
}
