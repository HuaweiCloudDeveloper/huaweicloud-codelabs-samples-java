/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.service.api;

import com.huawei.macroverse.koodrive.share.exception.KoodriveServerException;
import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import com.huawei.macroverse.koodrive.share.model.session.req.SessionAuthCodeURLReq;
import com.huawei.macroverse.koodrive.share.model.session.req.SessionCommonReq;
import com.huawei.macroverse.koodrive.share.model.session.req.SessionCreateReq;
import com.huawei.macroverse.koodrive.share.model.session.resp.SessionAuthCodeURLResp;
import com.huawei.macroverse.koodrive.share.model.session.resp.SessionAuthResp;
import com.huawei.macroverse.koodrive.share.model.session.resp.SessionCreateResp;
import com.huawei.macroverse.koodrive.share.model.session.resp.SessionRefreshResp;

/**
 * 登录相关接口
 */
public interface IKoodriveSessionService {

    /**
     * 获取认证服务器登录地址
     *
     * @param req 获取登录地址请求参数
     * @return SessionOrgIdLogUriResp 登录地址
     */
    SessionAuthCodeURLResp getAuthCodeURL(SessionAuthCodeURLReq req) throws KoodriveServerException;

    /**
     * 初始化创建会话
     *
     * @param req CreateSessionReq参数
     * @return 初始化创建会话结果
     */
    SessionCreateResp createSession(SessionCreateReq req) throws KoodriveServerException;

    /**
     * 用户认证
     *
     * @param req 用户认证参数
     * @return 用户信息
     */
    SessionAuthResp authSession(SessionCommonReq req) throws KoodriveServerException;

    /**
     * 刷新token
     *
     * @param req 刷新token请求入参
     * @return 返回新的认证token
     */
    SessionRefreshResp refresh(SessionCommonReq req) throws KoodriveServerException;

    /**
     *
     * 登出
     *
     * @param req 登出请求参数
     * @return 登出响应
     */
    KoodriveCommonResp logout(SessionCommonReq req) throws KoodriveServerException;
}
