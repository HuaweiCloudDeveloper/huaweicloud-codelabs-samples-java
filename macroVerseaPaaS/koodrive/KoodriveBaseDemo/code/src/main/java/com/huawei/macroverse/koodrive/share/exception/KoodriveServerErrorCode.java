/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.exception;

import lombok.Getter;

/**
 * koodrive 错误描述定义
 */
@Getter
public enum KoodriveServerErrorCode {
    IO_ERROR(100004, "IO Error!"),

    PARAM_ERROR(100005, "Param Error"),

    ENCODE_ERROR(100006, "URL Encoder Error"),

    GETSPACE_ERROR(100007, "Get Space Error"),

    CREATESPACE_ERROR(100008, "Create Space Error"),

    GETDOWNLOADURL_ERROR(100009, "Get Download Url Error"),

    CREATEFILE_ERROR(100010, "Create File Error"),

    COMPLETEFILE_ERROR(100011, "Complete File Error"),

    SEARCHFILE_ERROR(100012, "Search File Error"),

    GETFULLPATH_ERROR(100013, "Get Full Path Error"),

    GETPERSONALFILES_ERROR(100014, "Get Personal Files Error"),

    GETDEPARTMENTFILES_ERROR(100015, "Get Department Files Error"),

    GETFILE_ERROR(100016, "Get File Error"),

    CREATEDIRECOTRY_ERROR(100017, "Create Directory Error"),

    RENAMEFILE_ERROR(100018, "Rename File Error"),

    AUTHCODEURL_ERROR(100019, "Auth Code Url Error"),

    CREATESESSION_ERROR(100020, "Create Session Error"),

    AUTH_ERROR(100021, "Auth Error"),

    REFRESH_ERROR(100022, "Refresh Error"),

    LOGOUT_ERROR(100023, "Logout Error"),

    UNKOWN_ERROR(199999, "Param Error");

    private final Integer code;

    private final String msg;

    KoodriveServerErrorCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
