/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {
    /**
     * 策略
     */
    private String category;

    /**
     * me
     */
    private boolean me;

    /**
     * 权限id
     */
    private String permissionId;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 最终展示的用户名
     */
    private String displayName;
}
