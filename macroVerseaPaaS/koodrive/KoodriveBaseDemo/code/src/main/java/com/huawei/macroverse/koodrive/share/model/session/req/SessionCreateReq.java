/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.session.req;

import lombok.Getter;
import lombok.Setter;

/**
 * 创建Session请求
 */
@Getter
@Setter
public class SessionCreateReq  {
    private String code;

    private String domain;

    private String language;
}
