/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.req;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import lombok.Getter;
import lombok.Setter;

/**
 * 更新文件信息请求参数
 */
@Setter
@Getter
public class UpdateFileInfo extends KoodriveCommonResp {

    /**
     * 空间标识
     */
    private String containerId;

    /**
     * 新文件名
     */
    private String name;

    /**
     * 2:强制重命名，3：拒绝重命名
     */
    private Integer renameMode;
}
