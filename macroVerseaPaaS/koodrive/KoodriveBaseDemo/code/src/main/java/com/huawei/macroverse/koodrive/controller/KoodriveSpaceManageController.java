/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.controller;

import com.huawei.macroverse.koodrive.service.api.manage.IKoodriveManageService;
import com.huawei.macroverse.koodrive.share.exception.KoodriveServerException;
import com.huawei.macroverse.koodrive.share.model.manage.req.SpaceMgmtReq;
import com.huawei.macroverse.koodrive.share.model.manage.resp.SpaceMgmtResp;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 空间管理相关接口
 */
@RestController
@RequestMapping("/koodrive/cloudfilesaas/v1/space")
@AllArgsConstructor
public class KoodriveSpaceManageController {

    private IKoodriveManageService manageService;

    /**
     * 查询空间详情
     *
     * @param ownerId 空间所有者
     * @param type  空间类型
     * @return SpaceMgmtResp 空间详情
     */
    @GetMapping(value = "/{ownerId}")
    public SpaceMgmtResp getSpace(@NonNull @PathVariable Long ownerId,
                                  @RequestParam(value = "type", required = false) Integer type) throws KoodriveServerException {
        return manageService.getSpace(ownerId, type);
    }


    /**
     *  创建空间
     *
     * @param ownerId 空间所有者
     * @param spaceMgmtReq 空间入参
     * @return SpaceMgmtResp 空间详情
     */
    @PostMapping(value = "/{ownerId}")
    public SpaceMgmtResp createSpace(@NonNull @PathVariable Long ownerId, @NonNull @RequestBody SpaceMgmtReq spaceMgmtReq) throws KoodriveServerException {
        return manageService.createSpace(ownerId, spaceMgmtReq);
    }
}
