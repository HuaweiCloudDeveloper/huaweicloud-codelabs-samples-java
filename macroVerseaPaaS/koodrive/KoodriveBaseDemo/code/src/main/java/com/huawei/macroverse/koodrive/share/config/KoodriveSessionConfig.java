/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Session管理相关接口的uri配置
 */
@Setter
@Getter
@Configuration
@ConfigurationProperties(prefix = "koodrive.session")
public class KoodriveSessionConfig {
    private String authCode;

    private String auth;

    private String create;

    private String logout;

    private String refresh;
}
