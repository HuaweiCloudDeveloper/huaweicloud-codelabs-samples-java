/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.session.resp;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * 创建Session信息
 */
@Setter
@Getter
public class SessionCreateData {
    private String sessionId;

    @JsonIgnore
    private String sessionHash;

    @JsonIgnore
    private LocalDateTime sessionExpire;

    private String accessToken;

    private String userId;

    @JsonIgnore
    private LocalDateTime createTime;

    @JsonIgnore
    private LocalDateTime updateTime;

    private UserInfo userInfo;

    @Override
    public String toString() {
        return "SessionCreateData{" + "userId='" + userId + '\'' + ", userInfo=" + userInfo + '}';
    }
}