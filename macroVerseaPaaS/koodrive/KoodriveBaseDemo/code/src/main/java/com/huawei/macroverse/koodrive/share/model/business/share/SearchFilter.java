/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Data;

@Data
public class SearchFilter {
    private String keyWord;
    private String containerId;
    private String parentFolder;
    private String fileType;
    private String sizeRange;
    private String updateTimeRange;
    private String ownerId;
}

