/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.req;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import lombok.Data;

/**
 * 获取文件全路径请求参数
 */
@Data
public class GetFullFileRequest extends KoodriveCommonResp {
    private String fileId;

    private String containerId;

    public GetFullFileRequest() {

    }

    public GetFullFileRequest(String fileId, String containerId) {
        this.containerId = containerId;
        this.fileId = fileId;
    }
}
