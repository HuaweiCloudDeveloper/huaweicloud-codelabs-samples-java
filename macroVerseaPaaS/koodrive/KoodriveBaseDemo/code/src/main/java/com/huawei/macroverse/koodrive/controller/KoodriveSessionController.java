/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.controller;

import com.huawei.macroverse.koodrive.service.api.IKoodriveSessionService;
import com.huawei.macroverse.koodrive.share.exception.KoodriveServerException;
import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import com.huawei.macroverse.koodrive.share.model.session.req.SessionAuthCodeURLReq;
import com.huawei.macroverse.koodrive.share.model.session.req.SessionCommonReq;
import com.huawei.macroverse.koodrive.share.model.session.req.SessionCreateReq;
import com.huawei.macroverse.koodrive.share.model.session.resp.SessionAuthCodeURLResp;
import com.huawei.macroverse.koodrive.share.model.session.resp.SessionAuthResp;
import com.huawei.macroverse.koodrive.share.model.session.resp.SessionCreateResp;
import com.huawei.macroverse.koodrive.share.model.session.resp.SessionRefreshResp;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Session管理相关接口
 */
@RestController
@RequestMapping("/koodrive/cloudfilesaas/v1")
@AllArgsConstructor
public class KoodriveSessionController {

    private IKoodriveSessionService sessionService;

    /**
     *  oauth2认证服务器地址
     * @param domain 域名
     * @return SessionAuthCodeURLResp oauth2认证服务器信息
     */
    @RequestMapping(value = "/authCodeURL", method = RequestMethod.GET)
    public SessionAuthCodeURLResp authCodeURL(@RequestParam(value = "domain", required = false) String domain) throws KoodriveServerException {
        return sessionService.getAuthCodeURL(new SessionAuthCodeURLReq(domain));
    }

    /**
     * 登录
     *
     * @param req 登录授权码以及域名
     * @return SessionCreateResp 登录用户信息
     */
    @RequestMapping(value = "/session", method = RequestMethod.POST)
    public SessionCreateResp create(@RequestBody SessionCreateReq req) throws KoodriveServerException {
        return sessionService.createSession(req);
    }

    /**
     * 用户认证
     *
     * @param accessToken 认证token
     * @param language 语言
     * @return SessionAuthResp 用户认证信息
     */
    @RequestMapping(value = "/session/auth", method = RequestMethod.GET)
    public SessionAuthResp authSession(@CookieValue(value = "Authorization") String accessToken,
                                       @CookieValue(value = "language", required = false) String language) throws KoodriveServerException {
        return sessionService.authSession(new SessionCommonReq(accessToken, language));
    }

    /**
     * 刷新token
     *
     * @param sessionId refresh-token
     * @param accessToken 认证token
     * @param language 语言
     * @return SessionRefreshResp 返回新的token信息
     */
    @RequestMapping(value = "/session/refresh", method = RequestMethod.POST)
    public SessionRefreshResp refresh(@CookieValue(value = "x-session-id") String sessionId,
                                      @CookieValue(value = "Authorization") String accessToken,
                                      @CookieValue(value = "language", required = false) String language) throws KoodriveServerException {
        return sessionService.refresh(new SessionCommonReq(sessionId, accessToken, language));
    }


    /**
     *  登出
     *
     * @param accessToken 认证token
     * @param sessionId refresh-token
     * @param language 语言
     * @return KoodriveCommonResp 登出结果
     */
    @RequestMapping("/session/logoff")
    public KoodriveCommonResp logoff(@CookieValue(value = "Authorization", required = false) String accessToken,
                                     @CookieValue(value = "x-session-id", required = false) String sessionId,
                                     @CookieValue(value = "language", required = false) String language) throws KoodriveServerException {
        return sessionService.logout(new SessionCommonReq(sessionId, accessToken, language));
    }
}
