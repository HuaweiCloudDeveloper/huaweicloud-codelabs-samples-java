/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ResourceDto {
    /**
     * 文件资源的hash
     */
    private String hash;

    /**
     * 文件资源ID
     */
    private String id;

    /**
     * 文件资源的长度
     */
    private Long length;

    /**
     * 文件的sha256(用于完整性校验)
     */
    private String sha256;

    /**
     * 数据的状态
     */
    private Integer state;

    /**
     * 资源类型
     */
    private Integer type;

    /**
     * 分片对象, 适用于type={0,3}
     */
    private List<SliceObjectDto> objects;

    /**
     * 未定义对象，包含文件的下载链接
     */
    private ResourceObjectDto object;
}
