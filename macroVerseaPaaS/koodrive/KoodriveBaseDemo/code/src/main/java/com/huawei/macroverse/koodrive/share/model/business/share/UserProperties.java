/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class UserProperties {
    /**
     * 属性
     */
    private Map<String, String> attributes;

    /**
     * 属性
     */
    private Map<String, String> properties;
}
