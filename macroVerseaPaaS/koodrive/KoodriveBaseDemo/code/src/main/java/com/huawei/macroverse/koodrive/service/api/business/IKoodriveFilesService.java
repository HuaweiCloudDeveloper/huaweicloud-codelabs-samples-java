/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.service.api.business;


/**
 * 文件业务面接口清单:文件操作
 */
public interface IKoodriveFilesService extends IKoodriveBaseFiles {


}
