/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class AttachmentDto {
    /**
     * 资源ID
     */
    private String assetId;

    /**
     * 资源信息
     */
    private AssetDto asset;

    /**
     * 资源版本ID
     */
    private String versionId;

    /**
     * 类型属性
     */
    private Map<String, Object> attributes;

    /**
     * 资源实体的hash,默认hmac
     */
    private String hash;

    /**
     * 实体资源长度
     */
    private Long length;

    /**
     * MIME类型
     */
    private String mimeType;

    /**
     * 用途
     */
    private String usage;

    /**
     * 实体资源的sha256
     */
    private String sha256;

    /**
     * 文件创建时间
     */
    private String createdTime;

    /**
     * 文件修改时间
     */
    private String modifiedTime;

    /**
     * 文件公共自定义属性
     */
    private Map<String, Object> properties;
}
