/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class ComposePartDto {
    /**
     * 分片对象
     */
    private List<MultipartComposePart> objectParts;

    /**
     * 分片序号
     */
    private String uploadId;

    /**
     * 分片集合的headers
     */
    private String headers;
}
