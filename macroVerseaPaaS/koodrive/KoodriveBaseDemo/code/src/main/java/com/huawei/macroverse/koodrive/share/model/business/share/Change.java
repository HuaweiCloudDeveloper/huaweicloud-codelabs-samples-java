/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Change {
    /**
     * 资源类型，固定值为drive#change
     */
    private String category;

    /**
     * 变更的资源类型
     */
    private String type;

    /**
     * 变更类型
     */
    private String changeType;

    /**
     * 变更时间
     */
    private String time;

    /**
     * 资源是否被删除
     */
    private boolean deleted;

    /**
     * 文件ID
     */
    private String fileId;

    /**
     * 文件信息
     */
    private FileDto file;
}
