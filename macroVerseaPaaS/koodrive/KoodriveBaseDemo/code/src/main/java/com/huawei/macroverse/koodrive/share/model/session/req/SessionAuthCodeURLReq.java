/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.session.req;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * authCodeURL请求体
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SessionAuthCodeURLReq {
    private String domain;
    private String language;

    public SessionAuthCodeURLReq(String domain) {
        this.domain = domain;
    }
}
