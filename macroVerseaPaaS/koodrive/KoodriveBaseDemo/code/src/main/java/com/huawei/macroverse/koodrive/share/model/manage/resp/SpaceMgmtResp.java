/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.manage.resp;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import com.huawei.macroverse.koodrive.share.model.manage.share.SpaceMgmt;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 空间详情
 */
@Getter
@Setter
public class SpaceMgmtResp extends KoodriveCommonResp {
    private String id;

    private List<String> failedList;

    SpaceMgmt data;
}
