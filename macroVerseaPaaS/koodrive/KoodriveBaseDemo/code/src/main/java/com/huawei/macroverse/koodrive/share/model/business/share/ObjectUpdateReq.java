/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class ObjectUpdateReq {
    /**
     * 类型属性
     */
    private Map<String, Object> attributes;

    /**
     * 分片md5(如果是加密, 是指密文的md5)
     */
    private String md5;

    /**
     * 对象ID(关联项)
     */
    private String objectId;

    /**
     * 扩展属性
     */
    private Map<String, String> properties;

    /**
     * 分片sha256(如果是加密, 是指密文的sha256)，获取OBS上传地址的场景中，必传
     */
    private String sha256;

    /**
     * OBS上传状态对象
     */
    private UploadStatus uploadStatus;
}
