/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.resp;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import com.huawei.macroverse.koodrive.share.model.business.share.SearchFileInfo;
import lombok.Data;

import java.util.List;

/**
 * 搜索文件响应体
 */
@Data
public class SearchFileResponse extends KoodriveCommonResp {

    private List<SearchFileInfo> files;

    private Integer total;

    private String nextPageCursor;
}
