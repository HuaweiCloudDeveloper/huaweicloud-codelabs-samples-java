/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DownloadInfo {
    private String fileId;

    private String url;

    private String fileName;

    private String code;

    private String msg;
}
