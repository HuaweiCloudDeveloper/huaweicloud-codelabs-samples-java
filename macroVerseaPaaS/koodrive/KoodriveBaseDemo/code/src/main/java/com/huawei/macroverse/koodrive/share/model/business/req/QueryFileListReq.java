/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.req;

import lombok.Getter;
import lombok.Setter;

/**
 * 查询文件列表请求参数
 */
@Setter
@Getter
public class QueryFileListReq extends QueryFileInfo {

    /**
     * 团队id
     */
    private String groupId;

    /**
     * 类型
     */
    private String type;

    /**
     * 团队类型：部门，群组
     */
    private Integer teamType;

}
