/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.service.api.manage;

import com.huawei.macroverse.koodrive.share.exception.KoodriveServerException;
import com.huawei.macroverse.koodrive.share.model.manage.req.SpaceMgmtReq;
import com.huawei.macroverse.koodrive.share.model.manage.resp.SpaceMgmtResp;

/**
 * 空间管理相关接口
 */
public interface IKoodriveSpaceManage {

    SpaceMgmtResp getSpace(Long ownerId, Integer type) throws KoodriveServerException;

    SpaceMgmtResp createSpace( Long ownerId,  SpaceMgmtReq spaceMgmtReq) throws KoodriveServerException;
}
