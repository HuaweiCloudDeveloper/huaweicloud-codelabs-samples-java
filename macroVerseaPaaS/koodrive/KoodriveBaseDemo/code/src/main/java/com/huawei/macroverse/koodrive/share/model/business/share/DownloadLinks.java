/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DownloadLinks {
    /**
     * 用途，业务自定义
     */
    private String usage;

    /**
     * 附件下载地址
     */
    private String downloadLink;

    /**
     * 附版本
     */
    private String version;
}
