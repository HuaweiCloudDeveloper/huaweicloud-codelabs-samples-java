/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MultipartComposePart {
    /**
     * 分片序号
     */
    private Integer number;

    /**
     * x-google-generation
     */
    private String xggen;

    /**
     * 分片的id
     */
    private String partId;

    /**
     * 分片上传tag
     */
    private String etag;
}
