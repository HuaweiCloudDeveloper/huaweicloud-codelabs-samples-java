/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class AssetDto {
    /**
     * Asset ID
     * 必填
     */
    private String id;

    /**
     * Asset版本ID
     * 必填
     */
    private String versionId;

    /**
     * 版本号
     * 必填
     */
    private Long version;

    /**
     * MIME type
     */
    private String mimeType;

    /**
     * 历史版本的保存策略
     */
    private Integer keepPolicy;

    /**
     * 数据的状态
     */
    private Integer state;

    /**
     * 数据创建时间
     * 必填
     */
    private String createdTime;

    /**
     * 数据最后修改时间
     */
    private String modifiedTime;

    /**
     * 加密信息
     */
    private CipherDto cipher;

    /**
     * 最后一次修改的用户UPID
     */
    private String lastModifyingUser;

    /**
     * 文件资源
     */
    private ResourceDto resource;

    /**
     * 类型属性(Key部分使用字母开头的字母加数字组合, Value部分禁止使用emoji字符, 不限单个属性长度，限制总大小为16K以内)
     */
    private Map<String, Object> attributes;

    /**
     * 文件公共自定义属性。key-value个数不能超过30个；单个key-value长度不超过124字符
     */
    private Map<String, Object> properties;
}
