/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.resp;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import com.huawei.macroverse.koodrive.share.model.business.share.AttachmentDto;
import com.huawei.macroverse.koodrive.share.model.business.share.DownloadLinks;
import com.huawei.macroverse.koodrive.share.model.business.share.UserDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * 查询文件详情响应体
 */
@Setter
@Getter
public class QueryFileMetadataRsp extends KoodriveCommonResp {
    /**
     * 文件id
     */
    private String id;

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 文件类型
     */
    private String mimeType;

    /**
     * 后缀名
     */
    private String fileSuffix;

    /**
     * 是否已被回收
     */
    private Boolean recycled;

    /**
     * 文件大小
     */
    private Long size;

    /**
     * 是否被直接回收
     */
    private Boolean directlyRecycled;

    /**
     * 父目录
     */
    private String[] parentFolder;

    /**
     * 容器
     */
    private String[] containers;

    /**
     * 属性
     */
    private Map<String, Object> properties;

    /**
     * 文件的sha256
     */
    private String sha256;

    /**
     * 附件列表
     */
    private List<AttachmentDto> attachments;

    /**
     * owners
     */
    private List<UserDto> owners;

    /**
     * lastEditor
     */
    private UserDto lastEditor;

    /**
     * 文件类型
     */
    private String fileType;

    /**
     * 云空间文件全路劲
     */
    private String idPath;

    /**
     * 创建时间
     */
    private String createdTime;

    /**
     * 编辑时间
     */
    private String editedTime;

    /**
     * 是否收藏
     */
    private Boolean favorite;

    /**
     * 下载链接
     */
    private List<DownloadLinks> downloadLinks;

}
