/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.resp;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import com.huawei.macroverse.koodrive.share.model.business.share.DownloadInfo;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 下载文件响应体
 */
@Getter
@Setter
public class DownloadResponse extends KoodriveCommonResp {
    private List<DownloadInfo> downloads;

    private Integer batchCode;
}
