/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class SearchFileInfo {
    private String id;

    private String fileName;

    private String fileSuffix;

    private String fileType;

    private String size;

    private String updateTime;

    private String namePath;

    private String idPath;

    private List<DownLoadLinkInfo> downloadLinks;

    private String teamName;

    private String teamId;

    private String containerId;

    private String containerType;

    private Boolean favorite;

    private String owner;

    private String recycledTime;

    private Map<String, Object> properties;
}
