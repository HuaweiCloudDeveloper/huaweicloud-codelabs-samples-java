/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Data;

@Data
public class BatchFileResult {
    /**
     * 文件处理结果码
     */
    private String code;

    /**
     * 文件处理结果消息
     */
    private String msg;

    /**
     * 处理的文件对象
     */
    private FileIdInfo srcFile;
}
