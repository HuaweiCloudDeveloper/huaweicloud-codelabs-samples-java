/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.resp;

import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import lombok.Getter;
import lombok.Setter;

/**
 * 预览文件响应体
 */
@Getter
@Setter
public class FilePreviewAndEditLinkRsp extends KoodriveCommonResp {
    /**
     * 文件在线预览路径
     */
    private String link;
}
