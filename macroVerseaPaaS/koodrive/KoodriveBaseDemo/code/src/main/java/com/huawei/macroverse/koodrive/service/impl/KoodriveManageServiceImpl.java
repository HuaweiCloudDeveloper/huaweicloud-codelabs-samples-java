/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.service.impl;

import com.alibaba.fastjson.JSON;
import com.huawei.macroverse.koodrive.service.api.manage.IKoodriveManageService;
import com.huawei.macroverse.koodrive.share.config.KoodriveConfig;
import com.huawei.macroverse.koodrive.share.exception.KoodriveServerErrorCode;
import com.huawei.macroverse.koodrive.share.exception.KoodriveServerException;
import com.huawei.macroverse.koodrive.share.http.HttpClient;
import com.huawei.macroverse.koodrive.share.model.manage.req.SpaceMgmtReq;
import com.huawei.macroverse.koodrive.share.model.manage.resp.SpaceMgmtResp;
import com.huawei.macroverse.koodrive.utils.CommonHeaderUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@AllArgsConstructor
@Service("koodriveManageService")
public class KoodriveManageServiceImpl implements IKoodriveManageService {

    private final KoodriveConfig koodriveConfig;

    @Override
    public SpaceMgmtResp getSpace(Long ownerId, Integer type) throws KoodriveServerException {
        StringBuilder builder = new StringBuilder(koodriveConfig.getKoodriveHost());
        builder.append(koodriveConfig.getManageConfig().getSpaceOwner());
        if (type != null) {
            builder.append("?type=");
            builder.append(type);
        }
        String url = builder.toString();
        url = url.replace("{ownerId}", String.valueOf(ownerId));
        Optional<SpaceMgmtResp> spaceMgmtResp = HttpClient.doGet(url, CommonHeaderUtil.getCommonAuthHeader(), SpaceMgmtResp.class);
        if (!spaceMgmtResp.isPresent()) {
            throw new KoodriveServerException(KoodriveServerErrorCode.GETSPACE_ERROR.getCode(),
                    KoodriveServerErrorCode.GETSPACE_ERROR.getMsg());
        }
        return spaceMgmtResp.get();
    }

    public SpaceMgmtResp createSpace(Long ownerId, SpaceMgmtReq spaceMgmtReq) throws KoodriveServerException {
        StringBuilder builder = new StringBuilder(koodriveConfig.getKoodriveHost());
        builder.append(koodriveConfig.getManageConfig().getSpaceCreate());
        String url = builder.toString();
        url = url.replace("{ownerId}", String.valueOf(ownerId));
        spaceMgmtReq.setOwnerId(ownerId);
        Optional<SpaceMgmtResp> spaceMgmtResp = HttpClient.doPost(url, CommonHeaderUtil.getCommonAuthHeader(),
                JSON.toJSONString(spaceMgmtReq), SpaceMgmtResp.class);
        if (!spaceMgmtResp.isPresent()) {
            throw new KoodriveServerException(KoodriveServerErrorCode.CREATESPACE_ERROR.getCode(),
                    KoodriveServerErrorCode.CREATESPACE_ERROR.getMsg());
        }
        return spaceMgmtResp.get();
    }
}
