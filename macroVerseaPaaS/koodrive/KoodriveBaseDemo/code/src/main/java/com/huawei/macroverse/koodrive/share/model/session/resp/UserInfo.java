/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.session.resp;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

/**
 * 登录返回的用户信息
 */
@Getter
@Setter
public class UserInfo {
    private Integer role;

    private String userId;

    private String innerUserId;

    private String userName;

    private String mobile;

    private String orgId;

    private String orgName;

    private String tenantId;

    private Integer userCount;

    @JsonIgnore
    private String rootDeptCode;

    @JsonIgnore
    private String orgRefreshToken;

    @JsonIgnore
    private String orgToken;

    @Override
    public String toString() {
        return "UserInfo{" +  ", innerUserId='" + innerUserId + '\'' + '}';
    }
}