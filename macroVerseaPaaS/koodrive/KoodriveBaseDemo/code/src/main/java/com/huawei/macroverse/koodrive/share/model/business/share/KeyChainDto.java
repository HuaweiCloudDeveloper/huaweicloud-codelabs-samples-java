/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.macroverse.koodrive.share.model.business.share;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KeyChainDto {
    /**
     * 加密算法
     */
    private String algorithm;

    /**
     * 加密后的key
     */
    private String ekey;

    /**
     * key的UUID
     */
    private String id;

    /**
     * 密钥类型
     */
    private Integer type;
}
