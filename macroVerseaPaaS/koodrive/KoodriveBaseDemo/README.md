## 1. 简介

云盘服务（KooDrive）是华为云面向政企等客户推出的数据存储、访问、同步、管理和协作等功能的在线服务，是企业的一站式数字内容中枢，使能企业高效知识协作。

本示例展示如何对接Koodrive服务，包含登录、查询空间，查询空间文件等功能的对接
## 2. 前置条件
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已经订购Koodrive服务，并设置了自定义域名
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。


## 3. 代码示例
以下代码展示对接Koodrive登录能力：(仅展示登录相关接口的，具体对接细节参考demo)
``` java
package com.huawei.macroverse.koodrive.controller;

import com.huawei.macroverse.koodrive.service.api.IKoodriveSessionService;
import com.huawei.macroverse.koodrive.share.model.KoodriveCommonResp;
import com.huawei.macroverse.koodrive.share.model.session.req.SessionAuthCodeURLReq;
import com.huawei.macroverse.koodrive.share.model.session.req.SessionCommonReq;
import com.huawei.macroverse.koodrive.share.model.session.req.SessionCreateReq;
import com.huawei.macroverse.koodrive.share.model.session.resp.SessionAuthCodeURLResp;
import com.huawei.macroverse.koodrive.share.model.session.resp.SessionAuthResp;
import com.huawei.macroverse.koodrive.share.model.session.resp.SessionCreateResp;
import com.huawei.macroverse.koodrive.share.model.session.resp.SessionRefreshResp;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/koodrive/cloudfilesaas/v1")
@AllArgsConstructor
public class KoodriveSessionController {

    private IKoodriveSessionService sessionService;

    /**
     *  oauth2认证服务器地址
     * @param domain 域名
     * @return SessionAuthCodeURLResp oauth2认证服务器信息
     */
    @RequestMapping(value = "/authCodeURL", method = RequestMethod.GET)
    public SessionAuthCodeURLResp authCodeURL(@RequestParam(value = "domain", required = false) String domain) {
        return sessionService.getAuthCodeURL(new SessionAuthCodeURLReq(domain));
    }

    /**
     * 登录
     *
     * @param req 登录授权码以及域名
     * @return SessionCreateResp 登录用户信息
     */
    @RequestMapping(value = "/session", method = RequestMethod.POST)
    public SessionCreateResp create(@RequestBody SessionCreateReq req) {
        return sessionService.createSession(req);
    }

    /**
     * 用户认证
     *
     * @param accessToken 认证token
     * @param language 语言
     * @return SessionAuthResp 用户认证信息
     */
    @RequestMapping(value = "/session/auth", method = RequestMethod.GET)
    public SessionAuthResp authSession(@CookieValue(value = "Authorization") String accessToken,
                                       @CookieValue(value = "language", required = false) String language) {
        return sessionService.authSession(new SessionCommonReq(accessToken, language));
    }

    /**
     * 刷新token
     *
     * @param sessionId refresh-token
     * @param accessToken 认证token
     * @param language 语言
     * @return SessionRefreshResp 返回新的token信息
     */
    @RequestMapping(value = "/session/refresh", method = RequestMethod.POST)
    public SessionRefreshResp refresh(@CookieValue(value = "x-session-id") String sessionId,
                                      @CookieValue(value = "Authorization") String accessToken,
                                      @CookieValue(value = "language", required = false) String language) {
        return sessionService.refresh(new SessionCommonReq(sessionId, accessToken, language));
    }


    /**
     *  登出
     *
     * @param accessToken 认证token
     * @param sessionId refresh-token
     * @param language 语言
     * @return KoodriveCommonResp 登出结果
     */
    @RequestMapping("/session/logoff")
    public KoodriveCommonResp logoff(@CookieValue(value = "Authorization", required = false) String accessToken,
                                     @CookieValue(value = "x-session-id", required = false) String sessionId,
                                     @CookieValue(value = "language", required = false) String language) {
        return sessionService.logout(new SessionCommonReq(sessionId, accessToken, language));
    }
}
```
您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/MSSI/doc?api=CreateCustomConnectorFromOpenapi) 中直接运行调试该接口。

## 5. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-03-14 | 1.0 | 文档首次发布|

## 6. 参考链接
Koodrive官方文档链接：https://support.huaweicloud.com/koodrive/index.html