package com.huawei.mssi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.utils.JsonUtils;
import com.huaweicloud.sdk.mssi.v1.MssiClient;
import com.huaweicloud.sdk.mssi.v1.model.BaseConnectionInfo;
import com.huaweicloud.sdk.mssi.v1.model.CreateConnectionInfoRequest;
import com.huaweicloud.sdk.mssi.v1.model.CreateConnectionInfoResponse;
import com.huaweicloud.sdk.mssi.v1.model.CreateCustomConnectorFromOpenapiRequest;
import com.huaweicloud.sdk.mssi.v1.model.CreateCustomConnectorFromOpenapiResponse;
import com.huaweicloud.sdk.mssi.v1.model.CreateFlowRequest;
import com.huaweicloud.sdk.mssi.v1.model.CreateFlowResponse;
import com.huaweicloud.sdk.mssi.v1.model.CustomConnectorInfoV2;
import com.huaweicloud.sdk.mssi.v1.model.FlowMeta;
import com.huaweicloud.sdk.mssi.v1.model.ShowCustomConnectorRequest;
import com.huaweicloud.sdk.mssi.v1.model.ShowCustomConnectorResponse;
import com.huaweicloud.sdk.mssi.v1.region.MssiRegion;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CreateFlowDemo {
    private static final Logger logger = LoggerFactory.getLogger(CreateFlowDemo.class.getName());

    public static void main(String[] args) throws JsonProcessingException {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");


        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = new HttpConfig();
        MssiClient client = MssiClient.newBuilder()
            .withCredential(auth)
            .withHttpConfig(httpConfig)
            .withRegion(MssiRegion.CN_NORTH_4)
            .build();

        // 创建连接器
        String connectorId = CreateCustomConnectorFromOpenapi(client);
        // 发布连接器
        ReleaseCustomConnector(client, connectorId);
        // 创建连接
        CreateConnectionInfo(client, connectorId);
        // 创建流
        CreateFlow(client);
    }

    private static String CreateCustomConnectorFromOpenapi(MssiClient client) {
        CreateCustomConnectorFromOpenapiRequest request = new CreateCustomConnectorFromOpenapiRequest();
        CustomConnectorInfoV2 body = new CustomConnectorInfoV2();

        String connectorId = "";
        String swaggerInfo
            = "{\"swagger\":\"2.0\",\"info\":{\"description\":\"\",\"version\":\"1.0.0\",\"title\":\"菜谱_vs3m\",\"x-can-modify-host-address\":false,\"x-cdm-visibility\":false,\"x-hw-color\":\"#14a9ff\",\"x-hw-icon\":\"icon-huawei-default-5\"},\"host\":\"apis.tianapi.com\",\"basePath\":\"/\",\"schemes\":[\"https\"],\"security\":[{\"ApiKeyAuth\":[]}],\"paths\":{\"/caipu/index\":{\"get\":{\"summary\":\"查询菜谱\",\"operationId\":\"b296acfa-7eaa-4abc-b35d-3def2403069c\",\"consumes\":[\"application/json\"],\"produces\":[\"application/json\"],\"parameters\":[{\"name\":\"word\",\"in\":\"query\",\"description\":\"\",\"required\":true,\"type\":\"string\",\"format\":\"input\",\"x-hw-label\":\"\",\"x-hw-visibility\":\"none\",\"x-hw-format\":\"input\",\"x-hw-select-options\":[],\"x-hw-url-encode\":false}],\"responses\":{\"200\":{\"description\":\"请求成功\",\"headers\":{},\"schema\":{\"type\":\"object\",\"properties\":{\"code\":{\"type\":\"number\",\"format\":\"input\",\"description\":\"\",\"x-hw-visibility\":\"none\",\"x-hw-label\":\"\",\"x-hw-format\":\"input\",\"x-hw-select-options\":[],\"x-hw-default\":\"\"},\"msg\":{\"type\":\"string\",\"format\":\"input\",\"description\":\"\",\"default\":\"\",\"x-hw-visibility\":\"none\",\"x-hw-label\":\"\",\"x-hw-format\":\"input\",\"x-hw-select-options\":[],\"x-hw-default\":\"\"},\"result\":{\"type\":\"object\",\"description\":\"\",\"properties\":{\"curpage\":{\"type\":\"number\",\"format\":\"input\",\"description\":\"\",\"x-hw-visibility\":\"none\",\"x-hw-label\":\"\",\"x-hw-format\":\"input\",\"x-hw-select-options\":[],\"x-hw-default\":\"\"},\"allnum\":{\"type\":\"number\",\"format\":\"input\",\"description\":\"\",\"x-hw-visibility\":\"none\",\"x-hw-label\":\"\",\"x-hw-format\":\"input\",\"x-hw-select-options\":[],\"x-hw-default\":\"\"},\"list\":{\"type\":\"array\",\"description\":\"\",\"items\":{\"type\":\"object\",\"description\":\"\",\"properties\":{\"id\":{\"type\":\"number\",\"format\":\"input\",\"description\":\"\",\"x-hw-visibility\":\"none\",\"x-hw-label\":\"\",\"x-hw-format\":\"input\",\"x-hw-select-options\":[],\"x-hw-default\":\"\"},\"type_id\":{\"type\":\"number\",\"format\":\"input\",\"description\":\"\",\"x-hw-visibility\":\"none\",\"x-hw-label\":\"\",\"x-hw-format\":\"input\",\"x-hw-select-options\":[],\"x-hw-default\":\"\"},\"type_name\":{\"type\":\"string\",\"format\":\"input\",\"description\":\"\",\"default\":\"\",\"x-hw-visibility\":\"none\",\"x-hw-label\":\"\",\"x-hw-format\":\"input\",\"x-hw-select-options\":[],\"x-hw-default\":\"\"},\"cp_name\":{\"type\":\"string\",\"format\":\"input\",\"description\":\"\",\"default\":\"\",\"x-hw-visibility\":\"none\",\"x-hw-label\":\"\",\"x-hw-format\":\"input\",\"x-hw-select-options\":[],\"x-hw-default\":\"\"},\"zuofa\":{\"type\":\"string\",\"format\":\"input\",\"description\":\"\",\"default\":\"\",\"x-hw-visibility\":\"none\",\"x-hw-label\":\"\",\"x-hw-format\":\"input\",\"x-hw-select-options\":[],\"x-hw-default\":\"\"},\"texing\":{\"type\":\"string\",\"format\":\"input\",\"description\":\"\",\"default\":\"\",\"x-hw-visibility\":\"none\",\"x-hw-label\":\"\",\"x-hw-format\":\"input\",\"x-hw-select-options\":[],\"x-hw-default\":\"\"},\"tishi\":{\"type\":\"string\",\"format\":\"input\",\"description\":\"\",\"default\":\"\",\"x-hw-visibility\":\"none\",\"x-hw-label\":\"\",\"x-hw-format\":\"input\",\"x-hw-select-options\":[],\"x-hw-default\":\"\"},\"tiaoliao\":{\"type\":\"string\",\"format\":\"input\",\"description\":\"\",\"default\":\"\",\"x-hw-visibility\":\"none\",\"x-hw-label\":\"\",\"x-hw-format\":\"input\",\"x-hw-select-options\":[],\"x-hw-default\":\"\"},\"yuanliao\":{\"type\":\"string\",\"format\":\"input\",\"description\":\"\",\"default\":\"\",\"x-hw-visibility\":\"none\",\"x-hw-label\":\"\",\"x-hw-format\":\"input\",\"x-hw-select-options\":[],\"x-hw-default\":\"\"}},\"x-hw-visibility\":\"none\",\"x-hw-label\":\"\",\"x-hw-format\":\"input\",\"x-hw-select-options\":[],\"x-hw-default\":\"\"},\"x-hw-visibility\":\"none\",\"x-hw-label\":\"\",\"x-hw-format\":\"input\",\"x-hw-select-options\":[],\"x-hw-default\":\"\"}},\"x-hw-visibility\":\"none\",\"x-hw-label\":\"\",\"x-hw-format\":\"input\",\"x-hw-select-options\":[],\"x-hw-default\":\"\"}}}}},\"x-model\":{\"input-MappingConfig\":{\"rules\":[]},\"input-is-model-mapping\":false,\"input-model\":{},\"output-MappingConfig\":{\"rules\":[]},\"output-is-model-mapping\":false,\"output-model\":{}}}}},\"securityDefinitions\":{\"ApiKeyAuth\":{\"type\":\"apiKey\",\"name\":\"key\",\"in\":\"query\"}}}";
        Object object = JsonUtils.toObject(swaggerInfo, Object.class);
        body.withSwagger(object);
        request.withBody(body);
        try {
            CreateCustomConnectorFromOpenapiResponse response = client.createCustomConnectorFromOpenapi(request);
            connectorId = response.getId();
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            logger.error("error", e);

        } catch (ServiceResponseException e) {
            logger.error("error", e);
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return connectorId;
    }

    private static void ReleaseCustomConnector(MssiClient client, String connectorId) {
        ShowCustomConnectorRequest request = new ShowCustomConnectorRequest();
        request.withConnectorId(connectorId);
        try {
            ShowCustomConnectorResponse response = client.showCustomConnector(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            logger.error("error", e);

        } catch (ServiceResponseException e) {
            logger.error("error", e);
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    private static void CreateConnectionInfo(MssiClient client, String connectorId) {
        CreateConnectionInfoRequest request = new CreateConnectionInfoRequest();
        BaseConnectionInfo body = new BaseConnectionInfo();

        body.withConnectorId(connectorId);
        body.withConnectionName("菜谱_1");
        body.withAuthType("apiKey");
        HashMap<String, Object> map = new HashMap<>();
        map.put("key", "1234567");
        body.withAuthProp(map);
        body.withAuthConfigId("7c976ea2-be20-486d-bf40-730532790361");

        request.withBody(body);
        try {
            CreateConnectionInfoResponse response = client.createConnectionInfo(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            logger.error("error", e);

        } catch (ServiceResponseException e) {
            logger.error("error", e);
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    private static void CreateFlow(MssiClient client) throws JsonProcessingException {
        CreateFlowRequest request = new CreateFlowRequest();
        FlowMeta body = new FlowMeta();

        body.withId("66a4002c-bf2e-42fd-ba36-97ccb092ddb9");
        body.withName("流程test2");
        body.withIcon(
            "https://connector-icon.obs.cn-north-4.myhuaweicloud.com/icon/0b28948fba8025c72fbfc01702ee95b3/7b56cd53b02a43c2b8853d0b227ced84.png");
        body.withVersion("1.0");
        body.withSchemaVersion("1.0");
        // 构造connectors对象
        List<Object> objects = new ArrayList<Object>();
        String definitionRef = "{\"definitionRef\":\"ManualTrigger\"}";
        Object object = JsonUtils.toObject(definitionRef, Object.class);
        objects.add(object);
        body.withConnectors(objects);
        // 构造steps对象
        List<Map<String, Object>> maps = new ArrayList<Map<String, Object>>();
        String stepsInfo
            = "{\"icon\":\"\",\"id\":\"ManualTrigger\",\"stepId\":\"72cec68c-444b-47fe-b356-ef3bdcb07255\",\"call\":{\"connectorVersion\":\"1\",\"connectorRef\":\"ManualTrigger\",\"presentOptions\":{\"blockPosition\":{\"body\":{\"manualTrigger\":{\"contextRefPos\":[],\"functionRefPos\":[],\"customFunctionRefPos\":[],\"variableRefPos\":[],\"triggerRefPos\":[],\"globalVariableRefPos\":[]}}}},\"triggerRef\":{\"refName\":\"手动触发器触发事件\",\"refId\":\"ManualTrigger\",\"type\":\"instant\",\"input\":{\"body\":{\"manualTrigger\":[]}},\"authentication\":{\"connectionId\":\"\",\"schema\":{}}}},\"actionType\":\"instant\",\"actionName\":\"手动触发器触发事件\",\"actionId\":\"ManualTrigger\",\"name\":\"手动触发流\",\"transmit\":[],\"is_valid\":true,\"version\":\"1\",\"isContainArrayField\":false,\"intelligent_map\":{\"match\":false,\"enable\":false,\"stepId\":\"\"},\"global_variable\":[]}";

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.readValue(stepsInfo, new TypeReference<Map<String, Object>>() { });
        maps.add(map);
        body.withSteps(maps);
        body.withVersion("1.0");
        body.withIsValid(true);
        request.withBody(body);
        try {
            CreateFlowResponse response = client.createFlow(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            logger.error("error", e);

        } catch (ServiceResponseException e) {
            logger.error("error", e);
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

}
