## 1. 简介

开天集成工作台（MacroVerse SmartStage for Integrators，MSSI）是面向应用开发者和API开发者，提供基于元数据的可扩展的集成平台，打造开放、共生、智能、协同的技术标准体系，从而降低应用间的集成工作量，并沉淀多种集成资产，如连接器、领域信息模型资产等的平台。

本示例展示如何通过java版本的SDK方式创建连接器，发布连接器，创建连接，创建流
## 2. 前置条件
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。

## 3. 安装SDK
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

具体的SDK版本号请参见 [SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-mssi</artifactId>
    <version>3.1.60</version>
</dependency>
```
## 4. 代码示例
以下代码展示如何使用SDK创建流：(仅展示主类代码和示例逻辑，具体代码请查看CreateFlowDemo.java)
``` java
package com.huawei.mssi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.mssi.v1.MssiClient;
import com.huaweicloud.sdk.mssi.v1.model.BaseConnectionInfo;
import com.huaweicloud.sdk.mssi.v1.model.CreateConnectionInfoRequest;
import com.huaweicloud.sdk.mssi.v1.model.CreateConnectionInfoResponse;
import com.huaweicloud.sdk.mssi.v1.model.CreateCustomConnectorFromOpenapiRequest;
import com.huaweicloud.sdk.mssi.v1.model.CreateCustomConnectorFromOpenapiResponse;
import com.huaweicloud.sdk.mssi.v1.model.CreateFlowRequest;
import com.huaweicloud.sdk.mssi.v1.model.CreateFlowResponse;
import com.huaweicloud.sdk.mssi.v1.model.CustomConnectorInfoV2;
import com.huaweicloud.sdk.mssi.v1.model.FlowMeta;
import com.huaweicloud.sdk.mssi.v1.model.ShowCustomConnectorRequest;
import com.huaweicloud.sdk.mssi.v1.model.ShowCustomConnectorResponse;
import com.huaweicloud.sdk.mssi.v1.region.MssiRegion;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Map;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CreateFlowDemo {
    private static final Logger logger = LoggerFactory.getLogger(CreateFlowDemo.class.getName());

    public static void main(String[] args) throws ParseException, JsonProcessingException {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = new HttpConfig();
        
        MssiClient client = MssiClient.newBuilder()
            .withCredential(auth)
            .withHttpConfig(httpConfig)
            .withRegion(MssiRegion.CN_NORTH_4)
            .build();

        //创建连接器
        String connectorId = CreateCustomConnectorFromOpenapi(client);
        // 发布连接器
        ReleaseCustomConnector(client, connectorId);
        //创建连接
        CreateConnectionInfo(client, connectorId);
        //创建流
        CreateFlow(client);
    }
}
```
您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/MSSI/doc?api=CreateCustomConnectorFromOpenapi) 中直接运行调试该接口。

## 5. 返回结果示例
``` java
{
"flow_id": "8629ba24-e879-400a-bde9-63213219d567",
"webhook": null,
"apig_url": null,
"steps": null
}
```
## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-09-25 | 1.0 | 文档首次发布|

## 7. 参考链接
开天集成工作台官方文档链接：https://support.huaweicloud.com/mssi/index.html