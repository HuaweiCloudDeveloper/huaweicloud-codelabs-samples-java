## 1、介绍
**裸金属服务器元数据更新**

[更新裸金属服务器元数据](https://support.huaweicloud.com/api-bms/bms_api_0631.html)
更新裸金属服务器元数据。
  如果元数据中没有待更新字段，则自动添加该字段。
  如果元数据中已存在待更新字段，则直接更新字段值。
  如果元数据中的字段不再请求参数中，则保持不变。

**您将学到什么？**

如何通过java版SDK来体验对裸金属服务器元数据更新

## 2、前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云租户账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 3、endpoint 华为云各服务应用区域和各服务的终端节点，具体请参见[地区和终端节点](https://developer.huaweicloud.com/endpoint)。
- 4、华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。
- 5、用户需要在裸金属服务器（BMS）中购买裸金属服务器。

## 3、SDK获取和安装
您可以通过如下方式获取和安装 SDK： 通过 Maven 安装依赖（推荐） 通过 Maven 安装项目依赖是使用 Java SDK 的推荐方法，首先您需要在您的操作系统中下载并安装 Maven ，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
本示例使用BMS SDK：
```xml
<dependency>
  <groupId>com.huaweicloud.sdk</groupId>
  <artifactId>huaweicloud-sdk-bms</artifactId>
  <version>3.1.63</version>
</dependency>
<dependency>
  <groupId>com.huaweicloud.sdk</groupId>
  <artifactId>huaweicloud-sdk-core</artifactId>
  <version>3.1.58</version>
  <scope>compile</scope>
</dependency>
```


## 4、接口参数说明
关于接口参数的详细说明可参见：

[更新裸金属服务器元数据](https://support.huaweicloud.com/api-bms/bms_api_0631.html)


## 5、代码示例
以下代码展示如何使用更新裸金属服务器元数据相关SDK

```java
package com.huawei.bms;

import com.huaweicloud.sdk.bms.v1.BmsClient;
import com.huaweicloud.sdk.bms.v1.model.UpdateBaremetalServerMetadataReq;
import com.huaweicloud.sdk.bms.v1.model.UpdateBaremetalServerMetadataRequest;
import com.huaweicloud.sdk.bms.v1.model.UpdateBaremetalServerMetadataResponse;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;

import java.util.HashMap;
import java.util.Map;

public class BMSMetadataManagementDemo {

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // 配置客户端属性
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // 创建认证
        BasicCredentials authWithProjectId = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        // 创建BmsClient实例并初始化
        BmsClient bmsClient = BmsClient.newBuilder()
            .withHttpConfig(config)
            .withCredential(authWithProjectId)
            .withRegion(new Region("cn-north-4", new String[] {"https://bms.cn-north-4.myhuaweicloud.com"}))
            .build();

        // 裸金属服务器元数据更新
        updateBareMetalServerMetadata(bmsClient);
    }

    private static void updateBareMetalServerMetadata(BmsClient client) {
        // 裸金属服务器ID
        String serverId = "{your serverId string}";
        // 将您要新增或修改的裸金属元数据的key value 写入如下metadata对象中
        Map<String, String> metadata = new HashMap<>();
        metadata.put("{your key}", "{your value}");
        try {
            UpdateBaremetalServerMetadataResponse response = client.updateBaremetalServerMetadata(
                    new UpdateBaremetalServerMetadataRequest().withServerId(serverId)
                            .withBody(new UpdateBaremetalServerMetadataReq().withMetadata(metadata)));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        System.out.println("HttpStatusCode: " + e.getHttpStatusCode());
        System.out.println("RequestId: " + e.getRequestId());
        System.out.println("ErrorCode: " + e.getErrorCode());
        System.out.println("ErrorMsg: " + e.getErrorMsg());
    }
}
```

## 6、修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-11-21 | 1.0 | 文档首次发布 |