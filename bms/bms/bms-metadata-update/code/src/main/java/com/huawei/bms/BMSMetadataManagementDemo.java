package com.huawei.bms;

import com.huaweicloud.sdk.bms.v1.BmsClient;
import com.huaweicloud.sdk.bms.v1.model.UpdateBaremetalServerMetadataReq;
import com.huaweicloud.sdk.bms.v1.model.UpdateBaremetalServerMetadataRequest;
import com.huaweicloud.sdk.bms.v1.model.UpdateBaremetalServerMetadataResponse;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;

import java.util.HashMap;
import java.util.Map;

public class BMSMetadataManagementDemo {

    public static void main(String[] args) {
        // Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // Configure client properties
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // Create certificate
        BasicCredentials authWithProjectId = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        // Create an BmsClient and initialize it
        BmsClient bmsClient = BmsClient.newBuilder()
            .withHttpConfig(config)
            .withCredential(authWithProjectId)
            .withRegion(new Region("cn-north-4", new String[] {"https://bms.cn-north-4.myhuaweicloud.com"}))
            .build();

        // Update BMS Metadata
        updateBareMetalServerMetadata(bmsClient);
    }

    private static void updateBareMetalServerMetadata(BmsClient client) {
        // BMS Server ID
        String serverId = "{your serverId string}";
        // Metadata
        Map<String, String> metadata = new HashMap<>();
        metadata.put("{your key}", "{your value}");
        try {
            UpdateBaremetalServerMetadataResponse response = client.updateBaremetalServerMetadata(
                    new UpdateBaremetalServerMetadataRequest().withServerId(serverId)
                            .withBody(new UpdateBaremetalServerMetadataReq().withMetadata(metadata)));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        System.out.println("HttpStatusCode: " + e.getHttpStatusCode());
        System.out.println("RequestId: " + e.getRequestId());
        System.out.println("ErrorCode: " + e.getErrorCode());
        System.out.println("ErrorMsg: " + e.getErrorMsg());
    }
}