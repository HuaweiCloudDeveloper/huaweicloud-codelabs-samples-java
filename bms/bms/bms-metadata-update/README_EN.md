## 1.Introduction
**Updating BMS Metadata**

[Updating BMS Metadata](https://support.huaweicloud.com/intl/en-us/api-bms/bms_api_0631.html)
Updating BMS Metadata
- If the metadata does not contain the target field, the field is automatically added to the field.
- If the metadata contains the target field, the field value is automatically updated.
- If the field in the metadata is not requested, the field value remains unchanged.

**What will you learn?**

How to update BMS metadata.

## 2.Preconditions

- 1.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.
- 2.You have a Huawei Cloud account and the AK and SK of the account. (On the Huawei Cloud management console, hover the cursor over your account name and select **My Credentials**. In the navigation pane on the left, choose **Access Keys** and view your AK and SK. If you do not have the AK and SK, create them.) For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 3.Endpoint: Regions and endpoints of HUAWEI CLOUD services. For details, see [Regions and Endpoints](https://developer.huaweicloud.com/intl/en-us/endpoint).
- 4.The **Java JDK version is 1.8** or later.
- 5.You need to purchase the Bare cloud servers(BMS).

## 3.SDK Installation
You can obtain and install the SDK in the following ways: Installing project dependencies through Maven is the recommended method for using the Java SDK.
First, you need to download and install Maven in your operating system. After the installation is complete, you only need to add the corresponding dependencies to the pom.xml file of the Java project.
This example uses the BMS SDK:
``` xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-bms</artifactId>
    <version>3.1.63</version>
</dependency>
<dependency>
<groupId>com.huaweicloud.sdk</groupId>
<artifactId>huaweicloud-sdk-core</artifactId>
<version>3.1.58</version>
<scope>compile</scope>
</dependency>
```

## 4.Interface parameter description
For detailed descriptions of interface parameters:
[Updating BMS Metadata](https://support.huaweicloud.com/intl/en-us/api-bms/bms_api_0631.html)

## 5.Code Sample
```java
package com.huawei.bms;


import com.huaweicloud.sdk.bms.v1.BmsClient;
import com.huaweicloud.sdk.bms.v1.model.UpdateBaremetalServerMetadataReq;
import com.huaweicloud.sdk.bms.v1.model.UpdateBaremetalServerMetadataRequest;
import com.huaweicloud.sdk.bms.v1.model.UpdateBaremetalServerMetadataResponse;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class BMSMetadataManagementDemo {

    private static final Logger LOGGER = LoggerFactory.getLogger(BMSMetadataManagementDemo.class.getName());

    public static void main(String[] args) {
        // Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // Configure client properties
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // Create certificate
        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        // Create an BmsClient and initialize it
        BmsClient bmsClient = BmsClient.newBuilder()
            .withHttpConfig(config)
            .withCredential(auth)
            .withRegion(new Region("cn-north-4", new String[] {"https://bms.cn-north-4.myhuaweicloud.com"}))
            .build();

        // Update BMS Metadata
        updateBareMetalServerMetadata(bmsClient);
    }

    private static void updateBareMetalServerMetadata(BmsClient client) {
        // BMS Server ID
        String serverId = "{your serverId string}";
        // Metadata 
        Map<String, String> metadata = new HashMap<>();
        metadata.put("{your key}", "{your value}");
        try {
            UpdateBaremetalServerMetadataResponse response = client.updateBaremetalServerMetadata(
                    new UpdateBaremetalServerMetadataRequest().withServerId(serverId)
                            .withBody(new UpdateBaremetalServerMetadataReq().withMetadata(metadata)));
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
        LOGGER.error("RequestId: " + e.getRequestId());
        LOGGER.error("ErrorCode: " + e.getErrorCode());
        LOGGER.error("ErrorMsg: " + e.getErrorMsg());
    }
}
```

## 6. Change History
|  Release Date | Issue|   Description  |
| :--------: | :------: | :----------: |
| 2024-1-30|   1.0    | This issue is the first official release.|