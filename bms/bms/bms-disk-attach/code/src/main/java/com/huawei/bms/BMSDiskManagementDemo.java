package com.huawei.bms;

import com.huaweicloud.sdk.bms.v1.BmsClient;
import com.huaweicloud.sdk.bms.v1.model.AttachBaremetalServerVolumeRequest;
import com.huaweicloud.sdk.bms.v1.model.AttachBaremetalServerVolumeResponse;
import com.huaweicloud.sdk.bms.v1.model.AttachVolumeBody;
import com.huaweicloud.sdk.bms.v1.model.VolumeAttachment;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;

import java.util.UUID;

public class BMSDiskManagementDemo {

    public static void main(String[] args) {
        // Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // Configure client properties
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // Create certificate
        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        // Create an BmsClient and initialize it
        BmsClient bmsClient = BmsClient.newBuilder()
            .withHttpConfig(config)
            .withCredential(auth)
            .withRegion(new Region("cn-north-4", new String[] {"https://bms.cn-north-4.myhuaweicloud.com"}))
            .build();

        // Attach the Bare metal Server(BMS) cloud disk
        attachBaremetalServerVolume(bmsClient);
    }

    private static void attachBaremetalServerVolume(BmsClient client) {
        // Server ID
        String serverId = "{your serverId string}";
        // volume ID
        UUID volumeId = UUID.fromString("{your volumeId string}");
        try {
            AttachBaremetalServerVolumeResponse response = client.attachBaremetalServerVolume(
                    new AttachBaremetalServerVolumeRequest()
                            .withServerId(serverId)
                            .withBody(new AttachVolumeBody().withVolumeAttachment(
                                    new VolumeAttachment().withVolumeId(volumeId))));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            System.out.println("HttpStatusCode: " + e.getHttpStatusCode());
            System.out.println("RequestId: " + e.getRequestId());
            System.out.println("ErrorCode: " + e.getErrorCode());
            System.out.println("ErrorMsg: " + e.getErrorMsg());
        }
    }
}