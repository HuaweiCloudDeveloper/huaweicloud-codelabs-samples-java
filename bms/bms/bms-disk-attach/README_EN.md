## 1.Introduction
**Attaching the Bare metal Server(BMS) cloud disk**

[Attaching the Bare metal Server(BMS) cloud disk](https://support.huaweicloud.com/intl/en-us/api-bms/bms_api_0626.html)
After the bare metal server is created successfully, if you think that the disk is not enough or the current disk does not meet the requirements, you can attach the existing cloud disk to the bare metal server and use it as a data disk.

**What will you learn?**

How to attach the Bare metal Server(BMS) cloud disk by the Java SDK.

## 2.Preconditions

- 1.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.
- 2.You have a Huawei Cloud account and the AK and SK of the account. (On the Huawei Cloud management console, hover the cursor over your account name and select **My Credentials**. In the navigation pane on the left, choose **Access Keys** and view your AK and SK. If you do not have the AK and SK, create them.) For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 3.Endpoint: Regions and endpoints of HUAWEI CLOUD services. For details, see [Regions and Endpoints](https://developer.huaweicloud.com/intl/en-us/endpoint).
- 4.The **Java JDK version is 1.8** or later.
- 5.You need to purchase the Bare cloud servers(BMS).

## 3.SDK Installation
You can obtain and install the SDK in the following ways: Installing project dependencies through Maven is the recommended method for using the Java SDK.
First, you need to download and install Maven in your operating system. After the installation is complete, you only need to add the corresponding dependencies to the pom.xml file of the Java project.
This example uses the BMS SDK:
``` xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-bms</artifactId>
    <version>3.1.63</version>
</dependency>
<dependency>
<groupId>com.huaweicloud.sdk</groupId>
<artifactId>huaweicloud-sdk-core</artifactId>
<version>3.1.58</version>
<scope>compile</scope>
</dependency>
```

## 4.Interface parameter description
For detailed descriptions of interface parameters:

[Attaching the Bare metal Server(BMS) cloud disk](https://support.huaweicloud.com/intl/en-us/api-bms/bms_api_0626.html)

## 5.Code Sample
```java
package com.huawei.bms;


import com.huaweicloud.sdk.bms.v1.BmsClient;
import com.huaweicloud.sdk.bms.v1.model.AttachBaremetalServerVolumeRequest;
import com.huaweicloud.sdk.bms.v1.model.AttachBaremetalServerVolumeResponse;
import com.huaweicloud.sdk.bms.v1.model.AttachVolumeBody;
import com.huaweicloud.sdk.bms.v1.model.VolumeAttachment;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class BMSDiskManagementDemo {

    private static final Logger LOGGER = LoggerFactory.getLogger(BMSDiskAttachDemo.class.getName());

    public static void main(String[] args) {
        // Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // Configure client properties
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // Create certificate
        BasicCredentials auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        // Create an BmsClient and initialize it
        BmsClient bmsClient = BmsClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(new Region("cn-north-4", new String[] {"https://bms.cn-north-4.myhuaweicloud.com"}))
                .build();

        // Attach the Bare metal Server(BMS) cloud disk
        attachBaremetalServerVolume(bmsClient);
    }

    private static void attachBaremetalServerVolume(BmsClient client) {
        // server ID
        String serverId = "{your serverId string}";
        // volume ID
        UUID volumeId = UUID.fromString("{your volumeId string}");
        try {
            AttachBaremetalServerVolumeResponse response = client.attachBaremetalServerVolume(
                    new AttachBaremetalServerVolumeRequest()
                            .withServerId(serverId)
                            .withBody(new AttachVolumeBody().withVolumeAttachment(
                                    new VolumeAttachment().withVolumeId(volumeId))));
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
        LOGGER.error("RequestId: " + e.getRequestId());
        LOGGER.error("ErrorCode: " + e.getErrorCode());
        LOGGER.error("ErrorMsg: " + e.getErrorMsg());
    }
}
```

## 6. Change History
|  Release Date | Issue|   Description  |
| :--------: | :------: | :----------: |
| 2024-1-30|   1.0    | This issue is the first official release.|