## 1、介绍
**裸金属服务器云硬盘挂载**

[裸金属服务器挂载云硬盘](https://support.huaweicloud.com/api-bms/bms_api_0626.html)
裸金属服务器创建成功后，如果发现磁盘不够用或者当前磁盘不满足要求，可以将已有云硬盘挂载给裸金属服务器，作为数据盘使用。

**您将学到什么？**

如何通过java版SDK来体验对裸金属服务器云硬盘的挂载

## 2、前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云租户账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 3、endpoint 华为云各服务应用区域和各服务的终端节点，具体请参见[地区和终端节点](https://developer.huaweicloud.com/endpoint)。
- 4、华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。
- 5、用户需要在裸金属服务器（BMS）中购买裸金属服务器。

## 3、SDK获取和安装
您可以通过如下方式获取和安装 SDK： 通过 Maven 安装依赖（推荐） 通过 Maven 安装项目依赖是使用 Java SDK 的推荐方法，首先您需要在您的操作系统中下载并安装 Maven ，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
本示例使用BMS SDK：
```xml
<dependency>
  <groupId>com.huaweicloud.sdk</groupId>
  <artifactId>huaweicloud-sdk-bms</artifactId>
  <version>3.1.63</version>
</dependency>
<dependency>
  <groupId>com.huaweicloud.sdk</groupId>
  <artifactId>huaweicloud-sdk-core</artifactId>
  <version>3.1.58</version>
  <scope>compile</scope>
</dependency>
```


## 4、接口参数说明
关于接口参数的详细说明可参见：

[裸金属服务器挂载云硬盘](https://support.huaweicloud.com/api-bms/bms_api_0626.html)


## 5、代码示例
以下代码展示如何使用挂载裸金属服务器相关SDK

```java
package com.huawei.bms;


import com.huaweicloud.sdk.bms.v1.BmsClient;
import com.huaweicloud.sdk.bms.v1.model.AttachBaremetalServerVolumeRequest;
import com.huaweicloud.sdk.bms.v1.model.AttachBaremetalServerVolumeResponse;
import com.huaweicloud.sdk.bms.v1.model.AttachVolumeBody;
import com.huaweicloud.sdk.bms.v1.model.VolumeAttachment;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class BMSDiskManagementDemo {

    private static final Logger LOGGER = LoggerFactory.getLogger(BMSDiskAttachDemo.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // 配置客户端属性
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // 创建认证
        BasicCredentials auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        // 创建BmsClient实例并初始化
        BmsClient bmsClient = BmsClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(new Region("cn-north-4", new String[] {"https://bms.cn-north-4.myhuaweicloud.com"}))
                .build();

        // 裸金属服务器挂载云硬盘
        attachBaremetalServerVolume(bmsClient);
    }

    private static void attachBaremetalServerVolume(BmsClient client) {
        // 裸金属服务器ID
        String serverId = "{your serverId string}";
        // 要挂载的裸金属服务器的云磁盘ID
        UUID volumeId = UUID.fromString("{your volumeId string}");
        try {
            AttachBaremetalServerVolumeResponse response = client.attachBaremetalServerVolume(
                    new AttachBaremetalServerVolumeRequest()
                            .withServerId(serverId)
                            .withBody(new AttachVolumeBody().withVolumeAttachment(
                                    new VolumeAttachment().withVolumeId(volumeId))));
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
        LOGGER.error("RequestId: " + e.getRequestId());
        LOGGER.error("ErrorCode: " + e.getErrorCode());
        LOGGER.error("ErrorMsg: " + e.getErrorMsg());
    }
}
```

## 6、修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-11-21 | 1.0 | 文档首次发布 |