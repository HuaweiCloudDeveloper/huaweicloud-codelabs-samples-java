package com.huawei.bms;

import com.huaweicloud.sdk.bms.v1.BmsClient;
import com.huaweicloud.sdk.bms.v1.model.BatchRebootBaremetalServersRequest;
import com.huaweicloud.sdk.bms.v1.model.BatchRebootBaremetalServersResponse;
import com.huaweicloud.sdk.bms.v1.model.BatchStartBaremetalServersRequest;
import com.huaweicloud.sdk.bms.v1.model.BatchStartBaremetalServersResponse;
import com.huaweicloud.sdk.bms.v1.model.BatchStopBaremetalServersRequest;
import com.huaweicloud.sdk.bms.v1.model.BatchStopBaremetalServersResponse;
import com.huaweicloud.sdk.bms.v1.model.ChangeBaremetalNameBody;
import com.huaweicloud.sdk.bms.v1.model.ChangeBaremetalNameServer;
import com.huaweicloud.sdk.bms.v1.model.ChangeBaremetalServerNameRequest;
import com.huaweicloud.sdk.bms.v1.model.ChangeBaremetalServerNameResponse;
import com.huaweicloud.sdk.bms.v1.model.CreateBareMetalServersRequest;
import com.huaweicloud.sdk.bms.v1.model.CreateBareMetalServersResponse;
import com.huaweicloud.sdk.bms.v1.model.CreateBaremetalServersBody;
import com.huaweicloud.sdk.bms.v1.model.CreateServers;
import com.huaweicloud.sdk.bms.v1.model.ExtendParam;
import com.huaweicloud.sdk.bms.v1.model.ListBareMetalServerDetailsRequest;
import com.huaweicloud.sdk.bms.v1.model.ListBareMetalServerDetailsResponse;
import com.huaweicloud.sdk.bms.v1.model.ListBareMetalServersRequest;
import com.huaweicloud.sdk.bms.v1.model.ListBareMetalServersResponse;
import com.huaweicloud.sdk.bms.v1.model.MetaDataInfo;
import com.huaweicloud.sdk.bms.v1.model.Nics;
import com.huaweicloud.sdk.bms.v1.model.OsReinstall;
import com.huaweicloud.sdk.bms.v1.model.OsReinstallBody;
import com.huaweicloud.sdk.bms.v1.model.OsStartBody;
import com.huaweicloud.sdk.bms.v1.model.OsStopBody;
import com.huaweicloud.sdk.bms.v1.model.OsStopBodyType;
import com.huaweicloud.sdk.bms.v1.model.RebootBody;
import com.huaweicloud.sdk.bms.v1.model.ReinstallBaremetalServerOsRequest;
import com.huaweicloud.sdk.bms.v1.model.ReinstallBaremetalServerOsResponse;
import com.huaweicloud.sdk.bms.v1.model.RootVolume;
import com.huaweicloud.sdk.bms.v1.model.ServersInfoType;
import com.huaweicloud.sdk.bms.v1.model.ServersList;
import com.huaweicloud.sdk.bms.v1.model.StartServersInfo;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ManageBms {
    public static void main(String[] args) {
        // Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // Configure client properties
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // Create certificate
        BasicCredentials auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        BmsClient client = BmsClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(new Region("cn-east-3", "https://bms.cn-east-3.myhuaweicloud.com"))
                .build();

        // create bms
        createBms(client);

        // query bms details
        listBmsDetails(client);

        // query bms list
        listBms(client);

        // reboot bms
        batchRebootBms(client);

        // start bms
        batchStartBms(client);

        // stop bms
        batchStopBms(client);

        // change bms name
        changeBmsName(client);

        // reinstall bms
        reinstallBms(client);

    }

    private static void listBms(BmsClient client) {
        ListBareMetalServersRequest request = new ListBareMetalServersRequest();
        try {
            ListBareMetalServersResponse response = client.listBareMetalServers(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            logError(e);
        }

    }

    private static void listBmsDetails(BmsClient client) {
        ListBareMetalServerDetailsRequest request = new ListBareMetalServerDetailsRequest();
        // bms serverId
        request.withServerId("{ serverId }");
        try {
            ListBareMetalServerDetailsResponse response = client.listBareMetalServerDetails(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            logError(e);
        }
    }

    private static void createBms(BmsClient client) {
        CreateBareMetalServersRequest request = new CreateBareMetalServersRequest();
        CreateBaremetalServersBody body = new CreateBaremetalServersBody();
        CreateServers serverSetter = new CreateServers();

        // subnetId
        Nics nics = new Nics().withSubnetId(UUID.fromString("{ subnetId }"));
        List<Nics> nicsList = new ArrayList<Nics>();
        nicsList.add(nics);

        serverSetter
                // az id
                .withAvailabilityZone("{ az-id }")
                // bms name
                .withName("{ bms-name }")
                // imageId
                .withImageRef(UUID.fromString("{ image-id }"))
                // FlavorRef
                .withFlavorRef("{ flavor-id }")
                // Vpc id
                .withVpcid(UUID.fromString("{ vpc-id }"))
                .withNics(nicsList)
                // count
                .withCount(1)
                .withExtendparam(new ExtendParam().withEnterpriseProjectId("{ EnterpriseProjectId }"))
                // OpSvcUserid
                .withMetadata(new MetaDataInfo()
                        .withOpSvcUserid("{ OpSvcUserid }"))
                // volume type and size
                .withRootVolume(new RootVolume().withVolumetype(RootVolume.VolumetypeEnum.SATA).withSize(40))
                // KeyName
                .withKeyName("{ KeyName }");
        body.withServer(serverSetter);
        request.withBody(body);
        try {
            CreateBareMetalServersResponse response = client.createBareMetalServers(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            logError(e);
        }
    }

    private static void reinstallBms(BmsClient client) {
        ReinstallBaremetalServerOsRequest request = new ReinstallBaremetalServerOsRequest();
        request.withBody(new OsReinstallBody().withOsReinstall(new OsReinstall()
                        .withKeyname("{ Keyname }")))
                // bms id
                .withServerId("{ ServerId }");
        try {
            ReinstallBaremetalServerOsResponse response = client.reinstallBaremetalServerOs(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            logError(e);
        }
    }

    private static void changeBmsName(BmsClient client) {
        ChangeBaremetalServerNameRequest request = new ChangeBaremetalServerNameRequest();
        request.withBody(new ChangeBaremetalNameBody()
                        .withServer(new ChangeBaremetalNameServer()
                                .withName("{ name }")))
                .withServerId("{ ServerId }");
        try {
            ChangeBaremetalServerNameResponse response = client.changeBaremetalServerName(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            logError(e);
        }
    }

    private static void batchStopBms(BmsClient client) {
        BatchStopBaremetalServersRequest request = new BatchStopBaremetalServersRequest();
        List<ServersList> servers = new ArrayList<ServersList>();
        // bms id
        servers.add(new ServersList().withId(UUID.fromString("{ ServerId }")));
        request.withBody(new OsStopBody().withOsStop(new OsStopBodyType().withServers(servers)));
        try {
            BatchStopBaremetalServersResponse response = client.batchStopBaremetalServers(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            logError(e);
        }
    }

    private static void batchStartBms(BmsClient client) {
        BatchStartBaremetalServersRequest request = new BatchStartBaremetalServersRequest();
        List<ServersList> servers = new ArrayList<ServersList>();
        // bms id
        servers.add(new ServersList().withId(UUID.fromString("{ ServerId }")));
        request.withBody(new OsStartBody().withOsStart(new StartServersInfo().withServers(servers)));
        try {
            BatchStartBaremetalServersResponse response = client.batchStartBaremetalServers(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            logError(e);
        }
    }

    private static void batchRebootBms(BmsClient client) {
        BatchRebootBaremetalServersRequest request = new BatchRebootBaremetalServersRequest();
        List<ServersList> servers = new ArrayList<ServersList>();
        // bms id
        servers.add(new ServersList().withId(UUID.fromString("{ ServerId }")));
        request.withBody(new RebootBody().withReboot(new ServersInfoType()
                // Reboot Type
                .withServers(servers).withType(ServersInfoType.TypeEnum.SOFT)));
        try {
            BatchRebootBaremetalServersResponse response = client.batchRebootBaremetalServers(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            logError(e);
        }
    }

    private static void logError(ClientRequestException e) {
        System.out.println(e.getLocalizedMessage());
        System.out.println(e.getHttpStatusCode());
        System.out.println(e.getErrorCode());
        System.out.println(e.getErrorMsg());
    }
}
