## 1、介绍
**管理裸金属生命周期**

**您将学到什么？**

如何通过java版本SDK来管理裸金属生命周期

## 2、 前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云租户账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 3、endpoint 华为云各服务应用区域和各服务的终端节点，具体请参见[地区和终端节点](https://developer.huaweicloud.com/endpoint)。
- 4、华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。

## 3、SDK获取和安装
您可以通过如下方式获取和安装 SDK： 通过Maven安装项目依赖是使用Java SDK的推荐方法，首先您需要在您的操作系统中下载并安装Maven，安装完成后您只需在Java项目的pom.xml文件加入相应的依赖项即可。
本示例使用BMS SDK：

``` xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-bms</artifactId>
    <version>3.1.63</version>
</dependency>
```

## 4、接口参数说明
关于接口参数的详细说明可参见：

[创建裸金属服务器](https://support.huaweicloud.com/api-bms/bms_api_0606.html)

[查询裸金属服务器详情](https://support.huaweicloud.com/api-bms/bms_api_0608.html)

[查询裸金属服务器详情列表](https://support.huaweicloud.com/api-bms/bms_api_0609.html)

[修改裸金属服务器名称](https://support.huaweicloud.com/api-bms/bms_api_0611.html)

[重装裸金属服务器操作系统](https://support.huaweicloud.com/api-bms/bms_api_0612.html)

[启动裸金属服务器](https://support.huaweicloud.com/api-bms/bms_api_0613.html)

[重启裸金属服务器](https://support.huaweicloud.com/api-bms/bms_api_0614.html)

[关闭裸金属服务器](https://support.huaweicloud.com/api-bms/bms_api_0615.html)


## 5、代码示例

``` java
package com.huawei.bms;

import com.huaweicloud.sdk.bms.v1.BmsClient;
import com.huaweicloud.sdk.bms.v1.model.BatchRebootBaremetalServersRequest;
import com.huaweicloud.sdk.bms.v1.model.BatchRebootBaremetalServersResponse;
import com.huaweicloud.sdk.bms.v1.model.BatchStartBaremetalServersRequest;
import com.huaweicloud.sdk.bms.v1.model.BatchStartBaremetalServersResponse;
import com.huaweicloud.sdk.bms.v1.model.BatchStopBaremetalServersRequest;
import com.huaweicloud.sdk.bms.v1.model.BatchStopBaremetalServersResponse;
import com.huaweicloud.sdk.bms.v1.model.ChangeBaremetalNameBody;
import com.huaweicloud.sdk.bms.v1.model.ChangeBaremetalNameServer;
import com.huaweicloud.sdk.bms.v1.model.ChangeBaremetalServerNameRequest;
import com.huaweicloud.sdk.bms.v1.model.ChangeBaremetalServerNameResponse;
import com.huaweicloud.sdk.bms.v1.model.CreateBareMetalServersRequest;
import com.huaweicloud.sdk.bms.v1.model.CreateBareMetalServersResponse;
import com.huaweicloud.sdk.bms.v1.model.CreateBaremetalServersBody;
import com.huaweicloud.sdk.bms.v1.model.CreateServers;
import com.huaweicloud.sdk.bms.v1.model.ExtendParam;
import com.huaweicloud.sdk.bms.v1.model.ListBareMetalServerDetailsRequest;
import com.huaweicloud.sdk.bms.v1.model.ListBareMetalServerDetailsResponse;
import com.huaweicloud.sdk.bms.v1.model.ListBareMetalServersRequest;
import com.huaweicloud.sdk.bms.v1.model.ListBareMetalServersResponse;
import com.huaweicloud.sdk.bms.v1.model.MetaDataInfo;
import com.huaweicloud.sdk.bms.v1.model.Nics;
import com.huaweicloud.sdk.bms.v1.model.OsReinstall;
import com.huaweicloud.sdk.bms.v1.model.OsReinstallBody;
import com.huaweicloud.sdk.bms.v1.model.OsStartBody;
import com.huaweicloud.sdk.bms.v1.model.OsStopBody;
import com.huaweicloud.sdk.bms.v1.model.OsStopBodyType;
import com.huaweicloud.sdk.bms.v1.model.RebootBody;
import com.huaweicloud.sdk.bms.v1.model.ReinstallBaremetalServerOsRequest;
import com.huaweicloud.sdk.bms.v1.model.ReinstallBaremetalServerOsResponse;
import com.huaweicloud.sdk.bms.v1.model.RootVolume;
import com.huaweicloud.sdk.bms.v1.model.ServersInfoType;
import com.huaweicloud.sdk.bms.v1.model.ServersList;
import com.huaweicloud.sdk.bms.v1.model.StartServersInfo;
import com.huaweicloud.sdk.bms.v1.region.BmsRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ManageBms {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // 配置客户端属性
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // 创建认证
        BasicCredentials auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        BmsClient client = BmsClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(BmsRegion.valueOf("cn-east-3"))
                .build();

        // 创建bms
        createBms(client);

        // 查询bms
        listBmsDetails(client);

        // 查询列表
        listBms(client);

        // 重启裸金属服务器
        batchRebootBms(client);

        // 启动裸金属服务器
        batchStartBms(client);

        // 关闭裸金属服务器
        batchStopBms(client);

        // 修改裸金属服务器名称
        changeBmsName(client);

        // 重装裸金属服务器操作系统
        reinstallBms(client);

    }

    private static void listBms(BmsClient client) {
        ListBareMetalServersRequest request = new ListBareMetalServersRequest();
        try {
            ListBareMetalServersResponse response = client.listBareMetalServers(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            System.out.println(e.getLocalizedMessage());
        } catch (RequestTimeoutException e) {
            System.out.println(e.getLocalizedMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getLocalizedMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }

    }

    private static void listBmsDetails(BmsClient client) {
        ListBareMetalServerDetailsRequest request = new ListBareMetalServerDetailsRequest();
        // bms serverId
        request.withServerId("{ serverId }");
        try {
            ListBareMetalServerDetailsResponse response = client.listBareMetalServerDetails(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            System.out.println(e.getLocalizedMessage());
        } catch (RequestTimeoutException e) {
            System.out.println(e.getLocalizedMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getLocalizedMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    private static void createBms(BmsClient client) {
        CreateBareMetalServersRequest request = new CreateBareMetalServersRequest();
        CreateBaremetalServersBody body = new CreateBaremetalServersBody();
        CreateServers serverSetter = new CreateServers();

        // 创建参数subnetId
        Nics nics = new Nics().withSubnetId(UUID.fromString("{ subnetId }"));
        List<Nics> nicsList = new ArrayList<Nics>();
        nicsList.add(nics);

        serverSetter
                // az信息
                .withAvailabilityZone("{ az-id }")
                // bms名称
                .withName("{ bms-name }")
                // imageId
                .withImageRef(UUID.fromString("{ image-id }"))
                // FlavorRef
                .withFlavorRef("{ flavor-id }")
                // Vpcid
                .withVpcid(UUID.fromString("{ vpc-id }"))
                .withNics(nicsList)
                // 创建虚拟机数量
                .withCount(1)
                .withExtendparam(new ExtendParam().withEnterpriseProjectId("{ EnterpriseProjectId }"))
                // OpSvcUserid
                .withMetadata(new MetaDataInfo()
                        .withOpSvcUserid("{ OpSvcUserid }"))
                // 系统盘类型以及size
                .withRootVolume(new RootVolume().withVolumetype(RootVolume.VolumetypeEnum.SATA).withSize(40))
                // KeyName
                .withKeyName("{ KeyName }");
        body.withServer(serverSetter);
        request.withBody(body);
        try {
            CreateBareMetalServersResponse response = client.createBareMetalServers(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            System.out.println(e.getLocalizedMessage());
        } catch (RequestTimeoutException e) {
            System.out.println(e.getLocalizedMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getLocalizedMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    private static void reinstallBms(BmsClient client) {
        ReinstallBaremetalServerOsRequest request = new ReinstallBaremetalServerOsRequest();
        request.withBody(new OsReinstallBody().withOsReinstall(new OsReinstall()
                        .withKeyname("{ Keyname }")))
                // 重装的bms的id
                .withServerId("{ ServerId }");
        try {
            ReinstallBaremetalServerOsResponse response = client.reinstallBaremetalServerOs(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            System.out.println(e.getLocalizedMessage());
        } catch (RequestTimeoutException e) {
            System.out.println(e.getLocalizedMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getLocalizedMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }

    }

    private static void changeBmsName(BmsClient client) {
        ChangeBaremetalServerNameRequest request = new ChangeBaremetalServerNameRequest();
        request.withBody(new ChangeBaremetalNameBody()
                        .withServer(new ChangeBaremetalNameServer()
                                .withName("{ name }")))
                .withServerId("{ ServerId }");
        try {
            ChangeBaremetalServerNameResponse response = client.changeBaremetalServerName(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            System.out.println(e.getLocalizedMessage());
        } catch (RequestTimeoutException e) {
            System.out.println(e.getLocalizedMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getLocalizedMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    private static void batchStopBms(BmsClient client) {
        BatchStopBaremetalServersRequest request = new BatchStopBaremetalServersRequest();
        List<ServersList> servers = new ArrayList<ServersList>();
        // 要关机的bms的id
        servers.add(new ServersList().withId(UUID.fromString("{ ServerId }")));
        request.withBody(new OsStopBody().withOsStop(new OsStopBodyType().withServers(servers)));
        try {
            BatchStopBaremetalServersResponse response = client.batchStopBaremetalServers(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            System.out.println(e.getLocalizedMessage());
        } catch (RequestTimeoutException e) {
            System.out.println(e.getLocalizedMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getLocalizedMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    private static void batchStartBms(BmsClient client) {
        BatchStartBaremetalServersRequest request = new BatchStartBaremetalServersRequest();
        List<ServersList> servers = new ArrayList<ServersList>();
        // 要开机的bms的id
        servers.add(new ServersList().withId(UUID.fromString("{ ServerId }")));
        request.withBody(new OsStartBody().withOsStart(new StartServersInfo().withServers(servers)));
        try {
            BatchStartBaremetalServersResponse response = client.batchStartBaremetalServers(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            System.out.println(e.getLocalizedMessage());
        } catch (RequestTimeoutException e) {
            System.out.println(e.getLocalizedMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getLocalizedMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    private static void batchRebootBms(BmsClient client) {
        BatchRebootBaremetalServersRequest request = new BatchRebootBaremetalServersRequest();
        List<ServersList> servers = new ArrayList<ServersList>();
        // 要重启的bms的id
        servers.add(new ServersList().withId(UUID.fromString("{ ServerId }")));
        request.withBody(new RebootBody().withReboot(new ServersInfoType()
                // 重启的类型
                .withServers(servers).withType(ServersInfoType.TypeEnum.SOFT)));
        try {
            BatchRebootBaremetalServersResponse response = client.batchRebootBaremetalServers(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            System.out.println(e.getLocalizedMessage());
        } catch (RequestTimeoutException e) {
            System.out.println(e.getLocalizedMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getLocalizedMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}

```

## 6、修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-03-05 | 1.0 | 文档首次发布 |