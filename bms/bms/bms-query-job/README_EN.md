## 1.Introduction
**Querying Task Statuses**

[Querying Task Statuses](https://support.huaweicloud.com/intl/en-us/api-bms/bms_api_0640.html)

This API is used to query the execution status of a task.

After a task, such as creating a BMS or attaching disks, is delivered, job_id is returned, based on which you can query the execution status of the task.

**What will you learn?**

How to querying task statuses by the Java SDK.

## 2.Preconditions

- 1.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.
- 2.You have a Huawei Cloud account and the AK and SK of the account. (On the Huawei Cloud management console, hover the cursor over your account name and select **My Credentials**. In the navigation pane on the left, choose **Access Keys** and view your AK and SK. If you do not have the AK and SK, create them.) For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 3.Endpoint: Regions and endpoints of HUAWEI CLOUD services. For details, see [Regions and Endpoints](https://developer.huaweicloud.com/intl/en-us/endpoint).
- 4.The **Java JDK version is 1.8** or later.

## 3.SDK Installation
You can obtain and install the SDK in the following ways: Installing project dependencies through Maven is the recommended method for using the Java SDK.
First, you need to download and install Maven in your operating system. After the installation is complete, you only need to add the corresponding dependencies to the pom.xml file of the Java project.
This example uses the BMS SDK:
``` xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-bms</artifactId>
    <version>3.1.63</version>
</dependency>
```

## 4.Interface parameter description
For detailed descriptions of interface parameters:

[Querying Task Statuses](https://support.huaweicloud.com/intl/en-us/api-bms/bms_api_0640.html)

## 5.Code Sample

``` java
package com.huawei.bms;

import com.huaweicloud.sdk.bms.v1.BmsClient;
import com.huaweicloud.sdk.bms.v1.model.ShowJobInfosRequest;
import com.huaweicloud.sdk.bms.v1.model.ShowJobInfosResponse;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;

public class QueryJobDemo {
    public static void main(String[] args) {

        // Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        BmsClient client = BmsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(new Region("cn-north-4", "https://bms.cn-north-4.myhuaweicloud.com"))
                .build();

        ShowJobInfosRequest request = new ShowJobInfosRequest();
        // Task ID
        request.withJobId("{ jobId }");
        try {
            ShowJobInfosResponse response = client.showJobInfos(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            System.out.println("HttpStatusCode: " + e.getHttpStatusCode());
            System.out.println("RequestId: " + e.getRequestId());
            System.out.println("ErrorCode: " + e.getErrorCode());
            System.out.println("ErrorMsg: " + e.getErrorMsg());
        }
    }
}
```

## 6. Change History
| Release Date | Issue|   Description  |
|:------------:| :------: | :----------: |
|  2024-02-05  |   1.0    | This issue is the first official release.|