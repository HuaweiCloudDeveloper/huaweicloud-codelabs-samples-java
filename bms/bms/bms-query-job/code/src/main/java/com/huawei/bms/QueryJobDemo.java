package com.huawei.bms;

import com.huaweicloud.sdk.bms.v1.BmsClient;
import com.huaweicloud.sdk.bms.v1.model.ShowJobInfosRequest;
import com.huaweicloud.sdk.bms.v1.model.ShowJobInfosResponse;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;

public class QueryJobDemo {
    public static void main(String[] args) {

        // Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        BmsClient client = BmsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(new Region("cn-north-4", "https://bms.cn-north-4.myhuaweicloud.com"))
                .build();

        ShowJobInfosRequest request = new ShowJobInfosRequest();
        // Task ID
        request.withJobId("{ jobId }");
        try {
            ShowJobInfosResponse response = client.showJobInfos(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            System.out.println("HttpStatusCode: " + e.getHttpStatusCode());
            System.out.println("RequestId: " + e.getRequestId());
            System.out.println("ErrorCode: " + e.getErrorCode());
            System.out.println("ErrorMsg: " + e.getErrorMsg());
        }
    }
}
