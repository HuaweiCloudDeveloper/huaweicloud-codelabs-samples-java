package com.huawei.kafka;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.*;
import com.huaweicloud.sdk.kafka.v2.region.KafkaRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ListTopicPartitions {
    private static final Logger logger = LoggerFactory.getLogger(ListTopicPartitions.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String listTopicPartitionsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listTopicPartitionsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Note: For the following projectId, choose HUAWEI CLOUD Console > IAM My Credential > the project ID of the corresponding region.
        String projectId = "<YOUR PROJECT ID>";

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listTopicPartitionsAk)
                .withSk(listTopicPartitionsSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to Region.AP_SOUTHEAST_3.
        KafkaClient client = KafkaClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(KafkaRegion.AP_SOUTHEAST_3)
                .build();

        // Build Request
        ListTopicPartitionsRequest request = new ListTopicPartitionsRequest();
        request.withInstanceId("<YOUR INSTANCE ID>");
        request.withTopic("<YOUR TOPIC NAME>");

        try {
            // Send a request
            ListTopicPartitionsResponse response = client.listTopicPartitions(request);
            // Print the response result.
            logger.info(response.toString());
        }catch(ServiceResponseException  e){
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}