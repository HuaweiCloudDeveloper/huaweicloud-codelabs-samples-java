package com.huawei.dms.kafka.lifecycle;

import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.DeleteInstanceRequest;
import com.huaweicloud.sdk.kafka.v2.model.DeleteInstanceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteInstance {
    private static final Logger logger = LoggerFactory.getLogger(DeleteInstance.class);

    public static void main(String[] args) {
        KafkaClient client = General.getClient();
        try {
            DeleteInstanceRequest request = new DeleteInstanceRequest();
            request.withInstanceId("<YOUR InstanceId>");
            DeleteInstanceResponse response = client.deleteInstance(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
