package com.huawei.dms.kafka.lifecycle;

import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.ListInstancesRequest;
import com.huaweicloud.sdk.kafka.v2.model.ListInstancesResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListInstances {
    private static final Logger logger = LoggerFactory.getLogger(ListInstances.class);

    public static void main(String[] args) {
        KafkaClient client = General.getClient();
        try {
            ListInstancesRequest request = new ListInstancesRequest();
            request.withEngine(ListInstancesRequest.EngineEnum.fromValue("kafka"));
            ListInstancesResponse response = client.listInstances(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
