package com.huawei.dms.kafka.lifecycle;

import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.CreateInstanceByEngineRequest;
import com.huaweicloud.sdk.kafka.v2.model.CreateInstanceByEngineReq;
import com.huaweicloud.sdk.kafka.v2.model.CreateInstanceByEngineResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateInstance {
    private static final Logger logger = LoggerFactory.getLogger(CreateInstance.class);

    public static void main(String[] args) {
        KafkaClient client = General.getClient();
        try {
            CreateInstanceByEngineRequest request = new CreateInstanceByEngineRequest();
            request.withEngine(CreateInstanceByEngineRequest.EngineEnum.fromValue("kafka"));
            CreateInstanceByEngineReq body = new CreateInstanceByEngineReq();
            List<String> listbodyAvailableZones = new ArrayList<>();
            listbodyAvailableZones.add("<YOUR AvailableZones");
            body.withAvailableZones(listbodyAvailableZones);
            body.withStorageSpecCode(CreateInstanceByEngineReq.StorageSpecCodeEnum.fromValue("<YOUR StorageSpecCode>"));
            body.withProductId("<YOUR ProductId>");
            body.withSubnetId("<YOUR SubnetId>");
            body.withSecurityGroupId("<YOUR SecurityGroupId>");
            body.withVpcId("<YOUR VpcId>");
            body.withStorageSpace("<YOUR StorageSpace>");
            body.withBrokerNum("<YOUR BrokerNum>");
            body.withEngineVersion("<YOUR EngineVersion>");
            body.withEngine(CreateInstanceByEngineReq.EngineEnum.fromValue("kafka"));
            body.withDescription("<YOUR InstanceDescription>");
            body.withName("<YOUR InstanceName>");
            request.withBody(body);

            CreateInstanceByEngineResponse response = client.createInstanceByEngine(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
