## 1. 示例简介
基于华为云Java SDK对分布式消息服务Kafka实例进行管理的代码示例。其中包括：创建Kafka实例、查询Kafka实例和删除指定Kafka实例。

## 2. 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已获取对应区域的项目，请在华为云控制台“我的凭证 > API凭证 > 项目列表”页面上查看项目，例如：cn-north-4。具体请参见[API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html)

## 3. SDK获取和安装
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-kafka”，具体的SDK版本号请参见[SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/?language=java)

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>3.1.87</version>
</dependency>
<dependency>
<groupId>com.huaweicloud.sdk</groupId>
<artifactId>huaweicloud-sdk-kafka</artifactId>
<version>3.1.87</version>
</dependency>
```

## 4. 代码示例
以下代码展示如何使用创建Kafka实例、查询Kafka实例和删除指定Kafka实例相关SDK

### 4.1 初始化认证信息
```java
package com.huawei.dms.kafka.lifecycle;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.region.KafkaRegion;

/**
 * client工具类
 */
public class General {
    private static KafkaClient client;

    // 通过 ak, sk 获取初始化认证信息
    static {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        client = KafkaClient.newBuilder()
                .withCredential(auth)
                .withRegion(KafkaRegion.valueOf("<REGION ID>"))
                .build();
    }

    public static KafkaClient getClient() {
        return client;
    }

    public static void setClient(KafkaClient client) {
        General.client = client;
    }
}
```

### 4.2 创建Kafka实例
```java
package com.huawei.dms.kafka.lifecycle;

import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.CreateInstanceByEngineRequest;
import com.huaweicloud.sdk.kafka.v2.model.CreateInstanceByEngineReq;
import com.huaweicloud.sdk.kafka.v2.model.CreateInstanceByEngineResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateInstance {
    private static final Logger logger = LoggerFactory.getLogger(CreateInstance.class);

    public static void main(String[] args) {
        KafkaClient client = General.getClient();
        try {
            CreateInstanceByEngineRequest request = new CreateInstanceByEngineRequest();
            request.withEngine(CreateInstanceByEngineRequest.EngineEnum.fromValue("kafka"));
            CreateInstanceByEngineReq body = new CreateInstanceByEngineReq();
            List<String> listbodyAvailableZones = new ArrayList<>();
            listbodyAvailableZones.add("<YOUR AvailableZones");
            body.withAvailableZones(listbodyAvailableZones);
            body.withStorageSpecCode(CreateInstanceByEngineReq.StorageSpecCodeEnum.fromValue("<YOUR StorageSpecCode>"));
            body.withProductId("<YOUR ProductId>");
            body.withSubnetId("<YOUR SubnetId>");
            body.withSecurityGroupId("<YOUR SecurityGroupId>");
            body.withVpcId("<YOUR VpcId>");
            body.withStorageSpace("<YOUR StorageSpace>");
            body.withBrokerNum("<YOUR BrokerNum>");
            body.withEngineVersion("<YOUR EngineVersion>");
            body.withEngine(CreateInstanceByEngineReq.EngineEnum.fromValue("kafka"));
            body.withDescription("<YOUR InstanceDescription>");
            body.withName("<YOUR InstanceName>");
            request.withBody(body);

            CreateInstanceByEngineResponse response = client.createInstanceByEngine(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
```

### 4.3 查询Kafka实例
```java
package com.huawei.dms.kafka.lifecycle;

import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.ListInstancesRequest;
import com.huaweicloud.sdk.kafka.v2.model.ListInstancesResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListInstances {
    private static final Logger logger = LoggerFactory.getLogger(ListInstances.class);

    public static void main(String[] args) {
        KafkaClient client = General.getClient();
        try {
            ListInstancesRequest request = new ListInstancesRequest();
            request.withEngine(ListInstancesRequest.EngineEnum.fromValue("kafka"));
            ListInstancesResponse response = client.listInstances(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
```


### 4.4 删除Kafka实例
```java
package com.huawei.dms.kafka.lifecycle;

import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.DeleteInstanceRequest;
import com.huaweicloud.sdk.kafka.v2.model.DeleteInstanceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteInstance {
    private static final Logger logger = LoggerFactory.getLogger(DeleteInstance.class);

    public static void main(String[] args) {
        KafkaClient client = General.getClient();
        try {
            DeleteInstanceRequest request = new DeleteInstanceRequest();
            request.withInstanceId("<YOUR InstanceId>");
            DeleteInstanceResponse response = client.deleteInstance(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
```


## 5. 参考
更多信息请参考[分布式消息服务Kafka版文档](https://support.huaweicloud.com/Kafka/index.html)

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-03-28 | 1.0 | 文档首次发布|

