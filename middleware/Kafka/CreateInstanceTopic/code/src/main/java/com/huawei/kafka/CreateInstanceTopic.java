package com.huawei.kafka;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.*;
import com.huaweicloud.sdk.kafka.v2.region.KafkaRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;


public class CreateInstanceTopic {
    private static final Logger logger = LoggerFactory.getLogger(CreateInstanceTopic.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String createInstanceTopicAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String createInstanceTopicSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Note: For the following projectId, choose HUAWEI CLOUD Console > IAM My Credential > the project ID of the corresponding region.
        String projectId = "<YOUR PROJECT ID>";

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(createInstanceTopicAk)
                .withSk(createInstanceTopicSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to Region.AP_SOUTHEAST_3.
        KafkaClient client = KafkaClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(KafkaRegion.AP_SOUTHEAST_3)
                .build();

        // Build Request
        CreateInstanceTopicRequest request = new CreateInstanceTopicRequest();
        request.withInstanceId("<YOUR INSTANCE ID>");
        CreateInstanceTopicReq body = new CreateInstanceTopicReq();
        ArrayList<CreateInstanceTopicReqTopicOtherConfigs> listbodyTopicOtherConfigs = new ArrayList<>();
        //Configure a topic, for example, name:message.timestamp.type value:LogAppendTime.
        listbodyTopicOtherConfigs.add(
                new CreateInstanceTopicReqTopicOtherConfigs()
                        .withName("<YOUR TOPIC CONFIG NAME>")
                        .withValue("<YOUR TOPIC CONFIG VALUE>")
        );
        body.withTopicOtherConfigs(listbodyTopicOtherConfigs);
        // Message aging time. The default value is 72. The value ranges from 1 to 720, in hours.
        body.withRetentionTime(72);
        // Indicates whether to enable synchronous replication. After synchronous replication is enabled, acks must be set to -1 when the client generates messages.
        // Otherwise, synchronous replication does not take effect and is disabled by default.
        body.withSyncReplication(false);
        // Number of topic partitions, which is used to set the number of concurrent consumptions. Value range: 1-200.
        body.withPartition(3);
        // Indicates whether the Number of topic partitions, which is used to set the number of concurrent consumptions. Value range: 1-200. uses synchronous flushing. The default value is false.
        // Synchronous disk flushing deteriorates performance.
        body.withSyncMessageFlush(false);
        // Number of copies and reliability of configuration data. Value range: 1–3.
        body.withReplication(3);
        // Topic name. The value is a string of 3 to 200 characters and starts with a letter. Only uppercase letters, lowercase letters, hyphens (-), underscores (_), periods (.), and digits are supported.
        body.withId("test00003");
        request.withBody(body);

        try {
            // Send a request
            CreateInstanceTopicResponse response = client.createInstanceTopic(request);
            // Print the response result.
            logger.info(response.toString());
        }catch(ServiceResponseException  e){
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}