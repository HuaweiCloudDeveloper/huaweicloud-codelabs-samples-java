### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/Kafka/doc?api=CreateInstanceTopic).
2.This API is used to create a topic for a Kafka instance.

3.This API is used to create a Kafka instance topic.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The current account has the permission to use DMS Kafka.

6.An instance has been created in DMS Kafka.

7.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-kafka. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-kafka</artifactId>
    <version>3.1.87</version>
</dependency>

```

### Sample Code

``` java
package com.huawei.kafka;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.*;
import com.huaweicloud.sdk.kafka.v2.region.KafkaRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;


public class CreateInstanceTopic {
    private static final Logger logger = LoggerFactory.getLogger(CreateInstanceTopic.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String createInstanceTopicAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String createInstanceTopicSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Note: For the following projectId, choose HUAWEI CLOUD Console > IAM My Credential > the project ID of the corresponding region.
        String projectId = "<YOUR PROJECT ID>";

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(createInstanceTopicAk)
                .withSk(createInstanceTopicSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to Region.AP_SOUTHEAST_3.
        KafkaClient client = KafkaClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(KafkaRegion.AP_SOUTHEAST_3)
                .build();

        // Build Request
        CreateInstanceTopicRequest request = new CreateInstanceTopicRequest();
        request.withInstanceId("<YOUR INSTANCE ID>");
        CreateInstanceTopicReq body = new CreateInstanceTopicReq();
        ArrayList<CreateInstanceTopicReqTopicOtherConfigs> listbodyTopicOtherConfigs = new ArrayList<>();
        //Configure a topic, for example, name:message.timestamp.type value:LogAppendTime.
        listbodyTopicOtherConfigs.add(
                new CreateInstanceTopicReqTopicOtherConfigs()
                        .withName("<YOUR TOPIC CONFIG NAME>")
                        .withValue("<YOUR TOPIC CONFIG VALUE>")
        );
        body.withTopicOtherConfigs(listbodyTopicOtherConfigs);
        // Message aging time. The default value is 72. The value ranges from 1 to 720, in hours.
        body.withRetentionTime(72);
        // Indicates whether to enable synchronous replication. After synchronous replication is enabled, acks must be set to -1 when the client generates messages.
        // Otherwise, synchronous replication does not take effect and is disabled by default.
        body.withSyncReplication(false);
        // Number of topic partitions, which is used to set the number of concurrent consumptions. Value range: 1-200.
        body.withPartition(3);
        // Indicates whether the Number of topic partitions, which is used to set the number of concurrent consumptions. Value range: 1-200. uses synchronous flushing. The default value is false.
        // Synchronous disk flushing deteriorates performance.
        body.withSyncMessageFlush(false);
        // Number of copies and reliability of configuration data. Value range: 1–3.
        body.withReplication(3);
        // Topic name. The value is a string of 3 to 200 characters and starts with a letter. Only uppercase letters, lowercase letters, hyphens (-), underscores (_), periods (.), and digits are supported.
        body.withId("test00003");
        request.withBody(body);

        try {
            // Send a request
            CreateInstanceTopicResponse response = client.createInstanceTopic(request);
            // Print the response result.
            logger.info(response.toString());
        }catch(ServiceResponseException  e){
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}
```

### Example of the returned result
```
{
  "name" : "test01"
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/09/26 | 1.0 | This document is released for the first time.