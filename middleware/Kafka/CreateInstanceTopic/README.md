### 产品介绍

1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/Kafka/doc?api=CreateInstanceTopic)
中直接运行调试该接口。

2.该接口用于向Kafka实例创建Topic。

3.当需要创建Kafka实例Topic时，调用此接口可以达成预期。

### 前置条件

1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.当前登录账号拥有使用分布式消息服务 Kafka的权限。

6.已在分布式消息服务 Kafka创建实例。

7.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-kafka”，具体的SDK版本号请参见 [SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-kafka</artifactId>
    <version>3.1.87</version>
</dependency>
```

### 代码示例

``` java
package com.huawei.kafka;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.*;
import com.huaweicloud.sdk.kafka.v2.region.KafkaRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;


public class CreateInstanceTopic {
    private static final Logger logger = LoggerFactory.getLogger(CreateInstanceTopic.class.getName());

    public static void main(String[] args) {

        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String createInstanceTopicAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String createInstanceTopicSk = System.getenv("HUAWEICLOUD_SDK_SK");

       // 说明：如下ProjectId请在“华为云控制台> IAM我的凭证>对应region的project ID”中获取。
        String projectId = "<YOUR PROJECT ID>";

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(createInstanceTopicAk)
                .withSk(createInstanceTopicSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为Region.CN_NORTH_4
        KafkaClient client = KafkaClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(KafkaRegion.CN_NORTH_4)
                .build();

        // 构建请求
        CreateInstanceTopicRequest request = new CreateInstanceTopicRequest();
        request.withInstanceId("<YOUR INSTANCE ID>");
        CreateInstanceTopicReq body = new CreateInstanceTopicReq();
        ArrayList<CreateInstanceTopicReqTopicOtherConfigs> listbodyTopicOtherConfigs = new ArrayList<>();
        // topic配置 例如 name:message.timestamp.type value:LogAppendTime
        listbodyTopicOtherConfigs.add(
                new CreateInstanceTopicReqTopicOtherConfigs()
                        .withName("<YOUR TOPIC CONFIG NAME>")
                        .withValue("<YOUR TOPIC CONFIG VALUE>")
        );
        body.withTopicOtherConfigs(listbodyTopicOtherConfigs);
        // 消息老化时间。默认值为72。取值范围1-720，单位小时。
        body.withRetentionTime(72);
        // 是否开启同步复制，开启后，客户端生产消息时相应的也要设置acks=-1，否则不生效，默认关闭。
        body.withSyncReplication(false);
        // topic分区数，设置消费的并发数。取值范围：1-200。
        body.withPartition(3);
        // 是否使用同步落盘。默认值为false。同步落盘会导致性能降低。
        body.withSyncMessageFlush(false);
        // 副本数，配置数据的可靠性。取值范围：1-3。
        body.withReplication(3);
        // topic名称，长度为3-200，以字母开头且只支持大小写字母、中横线、下划线、点以及数字。
        body.withId("test00003");
        request.withBody(body);

        try {
            // 发送请求
            CreateInstanceTopicResponse response = client.createInstanceTopic(request);
            // 打印响应结果
            logger.info(response.toString());
        }catch(ServiceResponseException  e){
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}
```

### 返回结果示例

```
{
  "name" : "test01"
}
```

### 修订记录

    发布日期    | 文档版本 | 修订说明

:----------:|:----:| :------:  
2024/09/10 | 1.0 | 文档首次发布