### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/Kafka/sdk?api=ShowInstance).

2.In this example, you can query a specified instance.

3.This API is used to query the configuration parameters, topic list, partition information, and consumer group information of a specified instance.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The current account has the permission to use DMS Kafka.

6.An instance has been created in DMS Kafka.

7.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-kafka. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-kafka</artifactId>
    <version>3.1.121</version>
</dependency>

```

### Code example

``` java
package com.huawei.kafka;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.ShowInstanceRequest;
import com.huaweicloud.sdk.kafka.v2.model.ShowInstanceResponse;
import com.huaweicloud.sdk.kafka.v2.region.KafkaRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowInstance {

    private static final Logger logger = LoggerFactory.getLogger(ShowInstance.class.getName());

    public static void main(String[] args) {
    
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String showInstanceAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showInstanceSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Note: For the following projectId, choose HUAWEI CLOUD Console > IAM My Credential > the project ID of the corresponding region.
        String projectId = "<YOUR PROJECT ID>";

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(showInstanceAk)
                .withSk(showInstanceSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to KafkaRegion.AP_SOUTHEAST_3.
        KafkaClient client = KafkaClient.newBuilder()
                .withCredential(auth)
                .withRegion(KafkaRegion.AP_SOUTHEAST_3)
                .withHttpConfig(httpConfig)
                .build();

        // Build Request
        ShowInstanceRequest request = new ShowInstanceRequest();
        String instanceId = "<YOUR INSTANCE ID>";
        request.withInstanceId(instanceId);

        try {
            // Send a request
            ShowInstanceResponse response = client.showInstance(request);
            // Print the response result.
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
```
{
 "name": "kafka-demo",
 "engine": "kafka",
 "port": 9092,
 "status": "RUNNING",
 "description": "",
 "type": "single",
 "specification": "kafka.2u4g.single.small * 1 broker",
 "engine_version": "2.7",
 "connect_address": "192.168.*.*",
 "instance_id": "acad****",
 "resource_spec_code": "",
 "charging_mode": 1,
 "vpc_id": "9bc1****",
 "vpc_name": "vpc-demo",
 "created_at": "1731290379600",
 "product_id": "s6.2u4g.single.small",
 "security_group_id": "2a97****",
 "security_group_name": "sg-demo",
 "subnet_id": "5b5a****",
 "subnet_name": "subnet-demo",
 "subnet_cidr": "192.168.0.0/16",
 "available_zones": [
  "effd****"
 ],
 "available_zone_names": [
  "AZ1"
 ],
 "user_id": "dd73****",
 "user_name": "demouser",
 "kafka_manager_user": "",
 "maintain_begin": "02:00:00",
 "maintain_end": "06:00:00",
 "enable_log_collection": false,
 "new_auth_cert": false,
 "dns_enable": false,
 "storage_space": 62,
 "total_storage_space": 100,
 "used_storage_space": 0,
 "partition_num": "100",
 "replica_num": "-1",
 "enable_publicip": false,
 "ssl_enable": false,
 "broker_ssl_enable": false,
 "management_connect_domain_name": "",
 "public_management_connect_domain_name": "",
 "public_connect_domain_name": "",
 "cross_vpc_info": "{\"192.168.*.*\":{\"advertised_ip\":\"192.168.*.*\",\"port\":9011,\"port_id\":\"6011****\"}}",
 "public_cross_vpc_info": "{\"192.168.*.*\":{\"advertised_ip\":\"192.168.*.*\",\"port\":\"-\",\"port_id\":\"6011****\"}}",
 "storage_resource_id": "73b9****",
 "storage_spec_code": "dms.physical.storage.extreme",
 "service_type": "advanced",
 "storage_type": "hec",
 "enterprise_project_id": "ee9f****",
 "is_logical_volume": true,
 "extend_times": 0,
 "ipv6_enable": false,
 "ipv6_connect_addresses": [],
 "support_features": "kafka.crossvpc.domain.enable,auto.create.topics.enable,rabbitmq.plugin.management,auto_topic_switch,feature.physerver.kafka.user.manager,kafka.new.pod.port,message_trace_enable,features.pod.token.access,feature.physerver.kafka.sasl.wildcard,log.enable,features.log.collection,max.connections,rabbitmq.manage.support,replica_port_standalone,feature.physerver.kafka.topic.accesspolicy,smartConnect.keyspace.enable,enable.kafka.quota.monitor,rocketmq.acl,roma_app_enable,support.kafka.producer.ip,enable.new.authinfo,enable.kafka.quota,rabbitmq_run_log_enable,max.ssl.connections,route,message_trace_v2_enable,kafka.config.dynamic.modify.enable,feature.physerver.kafka.topic.modify,enable.topic.quota,roma.user.manage.no.support,auto.create.groups.enable,feature.physerver.kafka.pulbic.dynamic,kafka.config.static.modify.enable,amqp.overview",
 "agent_enable": false,
 "disk_encrypted": false,
 "ces_version": "linux,v1,v2,v3,v4",
 "public_access_enabled": "closed",
 "node_num": 1,
 "new_spec_billing_enable": true,
 "broker_num": 1,
 "dr_enable": false,
 "port_protocols": {
  "private_plain_enable": true,
  "private_plain_address": "192.168.*.*:9092",
  "private_plain_domain_name": "",
  "private_sasl_ssl_enable": false,
  "private_sasl_ssl_address": "",
  "private_sasl_ssl_domain_name": "",
  "private_sasl_plaintext_enable": false,
  "private_sasl_plaintext_address": "",
  "private_sasl_plaintext_domain_name": "",
  "public_plain_enable": false,
  "public_plain_address": "",
  "public_plain_domain_name": "",
  "public_sasl_ssl_enable": false,
  "public_sasl_ssl_address": "",
  "public_sasl_ssl_domain_name": "",
  "public_sasl_plaintext_enable": false,
  "public_sasl_plaintext_address": "",
  "public_sasl_plaintext_domain_name": ""
 },
 "config_ssl_need_restart_process": false,
 "support_scram_mechanism": true,
 "kafka_version": "2.7.2",
 "kafka_manager_enable": false,
 "kafka_private_connect_address": "192.168.*.*:9092",
 "kafka_private_connect_domain_name": "",
 "kafka_public_status": "closed",
 "pod_connect_address": "100.74.*.*:9080",
 "enable_auto_topic": false,
 "public_bandwidth": 0,
 "public_boundwidth": 0,
 "message_query_inst_enable": true,
 "vpc_client_plain": false,
 "rest_enable": false,
 "rest_connect_address": "",
 "connector_id": "",
 "trace_enable": false,
 "es_username": "",
 "es_mrs": false,
 "connector_enable": false,
 "connector_node_num": 0,
 "mqs_connector_enable": false,
 "retention_policy": "time_base",
 "sasl_enabled_mechanisms": [],
 "ssl_two_way_enable": false,
 "cert_replaced": false
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/11/11 | 1.0 | This document is released for the first time.