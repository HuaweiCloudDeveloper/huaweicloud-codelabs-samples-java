### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/Kafka/sdk?api=ShowInstance) 中直接运行调试该接口。

2.在本样例中，您可以查询指定实例。

3.查询指定实例，可以获取实例的配置参数、主题列表、分区信息、消费者组信息等。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.当前登录账号拥有使用分布式消息服务 Kafka的权限。

6.已在分布式消息服务 Kafka创建实例。

7.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-kafka”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-kafka</artifactId>
    <version>3.1.121</version>
</dependency>
```

### 代码示例
``` java
package com.huawei.kafka;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.ShowInstanceRequest;
import com.huaweicloud.sdk.kafka.v2.model.ShowInstanceResponse;
import com.huaweicloud.sdk.kafka.v2.region.KafkaRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowInstance {

    private static final Logger logger = LoggerFactory.getLogger(ShowInstance.class.getName());

    public static void main(String[] args) {
    
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String showInstanceAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showInstanceSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 说明：如下ProjectId请在“华为云控制台> IAM我的凭证>对应region的project ID”中获取。
        String projectId = "<YOUR PROJECT ID>";

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(showInstanceAk)
                .withSk(showInstanceSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为Region.CN_NORTH_4
        KafkaClient client = KafkaClient.newBuilder()
                .withCredential(auth)
                .withRegion(KafkaRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();

        // 构建请求
        ShowInstanceRequest request = new ShowInstanceRequest();
        String instanceId = "<YOUR INSTANCE ID>";
        request.withInstanceId(instanceId);

        try {
            // 发送请求
            ShowInstanceResponse response = client.showInstance(request);
            // 打印响应结果
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例
```
{
 "name": "kafka-demo",
 "engine": "kafka",
 "port": 9092,
 "status": "RUNNING",
 "description": "",
 "type": "single",
 "specification": "kafka.2u4g.single.small * 1 broker",
 "engine_version": "2.7",
 "connect_address": "192.168.*.*",
 "instance_id": "acad****",
 "resource_spec_code": "",
 "charging_mode": 1,
 "vpc_id": "9bc1****",
 "vpc_name": "vpc-demo",
 "created_at": "1731290379600",
 "product_id": "s6.2u4g.single.small",
 "security_group_id": "2a97****",
 "security_group_name": "sg-demo",
 "subnet_id": "5b5a****",
 "subnet_name": "subnet-demo",
 "subnet_cidr": "192.168.0.0/16",
 "available_zones": [
  "effd****"
 ],
 "available_zone_names": [
  "AZ1"
 ],
 "user_id": "dd73****",
 "user_name": "demouser",
 "kafka_manager_user": "",
 "maintain_begin": "02:00:00",
 "maintain_end": "06:00:00",
 "enable_log_collection": false,
 "new_auth_cert": false,
 "dns_enable": false,
 "storage_space": 62,
 "total_storage_space": 100,
 "used_storage_space": 0,
 "partition_num": "100",
 "replica_num": "-1",
 "enable_publicip": false,
 "ssl_enable": false,
 "broker_ssl_enable": false,
 "management_connect_domain_name": "",
 "public_management_connect_domain_name": "",
 "public_connect_domain_name": "",
 "cross_vpc_info": "{\"192.168.*.*\":{\"advertised_ip\":\"192.168.*.*\",\"port\":9011,\"port_id\":\"6011****\"}}",
 "public_cross_vpc_info": "{\"192.168.*.*\":{\"advertised_ip\":\"192.168.*.*\",\"port\":\"-\",\"port_id\":\"6011****\"}}",
 "storage_resource_id": "73b9****",
 "storage_spec_code": "dms.physical.storage.extreme",
 "service_type": "advanced",
 "storage_type": "hec",
 "enterprise_project_id": "ee9f****",
 "is_logical_volume": true,
 "extend_times": 0,
 "ipv6_enable": false,
 "ipv6_connect_addresses": [],
 "support_features": "kafka.crossvpc.domain.enable,auto.create.topics.enable,rabbitmq.plugin.management,auto_topic_switch,feature.physerver.kafka.user.manager,kafka.new.pod.port,message_trace_enable,features.pod.token.access,feature.physerver.kafka.sasl.wildcard,log.enable,features.log.collection,max.connections,rabbitmq.manage.support,replica_port_standalone,feature.physerver.kafka.topic.accesspolicy,smartConnect.keyspace.enable,enable.kafka.quota.monitor,rocketmq.acl,roma_app_enable,support.kafka.producer.ip,enable.new.authinfo,enable.kafka.quota,rabbitmq_run_log_enable,max.ssl.connections,route,message_trace_v2_enable,kafka.config.dynamic.modify.enable,feature.physerver.kafka.topic.modify,enable.topic.quota,roma.user.manage.no.support,auto.create.groups.enable,feature.physerver.kafka.pulbic.dynamic,kafka.config.static.modify.enable,amqp.overview",
 "agent_enable": false,
 "disk_encrypted": false,
 "ces_version": "linux,v1,v2,v3,v4",
 "public_access_enabled": "closed",
 "node_num": 1,
 "new_spec_billing_enable": true,
 "broker_num": 1,
 "dr_enable": false,
 "port_protocols": {
  "private_plain_enable": true,
  "private_plain_address": "192.168.*.*:9092",
  "private_plain_domain_name": "",
  "private_sasl_ssl_enable": false,
  "private_sasl_ssl_address": "",
  "private_sasl_ssl_domain_name": "",
  "private_sasl_plaintext_enable": false,
  "private_sasl_plaintext_address": "",
  "private_sasl_plaintext_domain_name": "",
  "public_plain_enable": false,
  "public_plain_address": "",
  "public_plain_domain_name": "",
  "public_sasl_ssl_enable": false,
  "public_sasl_ssl_address": "",
  "public_sasl_ssl_domain_name": "",
  "public_sasl_plaintext_enable": false,
  "public_sasl_plaintext_address": "",
  "public_sasl_plaintext_domain_name": ""
 },
 "config_ssl_need_restart_process": false,
 "support_scram_mechanism": true,
 "kafka_version": "2.7.2",
 "kafka_manager_enable": false,
 "kafka_private_connect_address": "192.168.*.*:9092",
 "kafka_private_connect_domain_name": "",
 "kafka_public_status": "closed",
 "pod_connect_address": "100.74.*.*:9080",
 "enable_auto_topic": false,
 "public_bandwidth": 0,
 "public_boundwidth": 0,
 "message_query_inst_enable": true,
 "vpc_client_plain": false,
 "rest_enable": false,
 "rest_connect_address": "",
 "connector_id": "",
 "trace_enable": false,
 "es_username": "",
 "es_mrs": false,
 "connector_enable": false,
 "connector_node_num": 0,
 "mqs_connector_enable": false,
 "retention_policy": "time_base",
 "sasl_enabled_mechanisms": [],
 "ssl_two_way_enable": false,
 "cert_replaced": false
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-11-11 | 1.0 | 文档首次发布 |