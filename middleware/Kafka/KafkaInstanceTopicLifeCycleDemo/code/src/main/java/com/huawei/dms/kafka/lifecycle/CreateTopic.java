package com.huawei.dms.kafka.lifecycle;

import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.CreateInstanceTopicReq;
import com.huaweicloud.sdk.kafka.v2.model.CreateInstanceTopicRequest;
import com.huaweicloud.sdk.kafka.v2.model.CreateInstanceTopicResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateTopic {
    private static final Logger logger = LoggerFactory.getLogger(CreateTopic.class);

    public static void main(String[] args) {
        KafkaClient client = General.getClient();
        try {
            CreateInstanceTopicRequest request = new CreateInstanceTopicRequest();
            CreateInstanceTopicReq body = new CreateInstanceTopicReq();
            body.setId("topic-test");
            body.setPartition(3);
            body.setReplication(3);
            request.withBody(body);
            request.withInstanceId("<YOUR InstanceId>");
            CreateInstanceTopicResponse response = client.createInstanceTopic(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
