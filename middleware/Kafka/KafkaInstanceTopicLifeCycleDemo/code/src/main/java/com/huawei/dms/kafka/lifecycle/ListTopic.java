package com.huawei.dms.kafka.lifecycle;

import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.ListInstanceTopicsRequest;
import com.huaweicloud.sdk.kafka.v2.model.ListInstanceTopicsResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListTopic {
    private static final Logger logger = LoggerFactory.getLogger(ListTopic.class);

    public static void main(String[] args) {
        KafkaClient client = General.getClient();
        try {
            ListInstanceTopicsRequest request = new ListInstanceTopicsRequest();
            request.withInstanceId("<YOUR InstanceId>");
            ListInstanceTopicsResponse response = client.listInstanceTopics(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
