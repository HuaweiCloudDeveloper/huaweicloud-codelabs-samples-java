package com.huawei.dms.kafka.lifecycle;

import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.BatchDeleteInstanceTopicReq;
import com.huaweicloud.sdk.kafka.v2.model.BatchDeleteInstanceTopicRequest;
import com.huaweicloud.sdk.kafka.v2.model.BatchDeleteInstanceTopicResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteTopic {
    private static final Logger logger = LoggerFactory.getLogger(DeleteTopic.class);

    public static void main(String[] args) {
        KafkaClient client = General.getClient();
        try {
            BatchDeleteInstanceTopicRequest request = new BatchDeleteInstanceTopicRequest();
            BatchDeleteInstanceTopicReq body = new BatchDeleteInstanceTopicReq();
            body.addTopicsItem("topic-test");
            request.withBody(body);
            request.withInstanceId("<YOUR InstanceId>");
            BatchDeleteInstanceTopicResponse response = client.batchDeleteInstanceTopic(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
