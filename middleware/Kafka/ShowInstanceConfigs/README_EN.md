### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/Kafka/doc?api=ShowInstanceConfigs).
2.Obtaining Instance Configurations by Kafka

3.This API is used to obtain Kafka instance configuration information..

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The current account has the permission to use DMS Kafka.

6.An instance has been created in DMS Kafka.

7.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-kafka. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-kafka</artifactId>
    <version>3.1.87</version>
</dependency>

```

### Sample Code

``` java
package com.huawei.kafka;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.*;
import com.huaweicloud.sdk.kafka.v2.region.KafkaRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ShowInstanceConfigs {
    private static final Logger logger = LoggerFactory.getLogger(ShowInstanceConfigs.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String showInstanceConfigsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showInstanceConfigsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Note: For the following projectId, choose HUAWEI CLOUD Console > IAM My Credential > the project ID of the corresponding region.
        String projectId = "<YOUR PROJECT ID>";

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(showInstanceConfigsAk)
                .withSk(showInstanceConfigsSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to Region.AP_SOUTHEAST_3.
        KafkaClient client = KafkaClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(KafkaRegion.AP_SOUTHEAST_3)
                .build();

        // Build Request
        ShowInstanceConfigsRequest request = new ShowInstanceConfigsRequest();
        request.withInstanceId("<YOUR INSTANCE ID>");

        try {
            // Send a request
            ShowInstanceConfigsResponse response = client.showInstanceConfigs(request);
            // Print the response result.
            logger.info(response.toString());
        }catch(ServiceResponseException  e){
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}
```

### Example of the returned result
```
{
  "kafka_configs" : [ {
    "name" : "min.insy*************",
    "valid_values" : "1~3",
    "default_value" : "1",
    "config_type" : "dynamic",
    "value" : "1",
    "value_type" : "integer"
  }, {
    "name" : "message.*************",
    "valid_values" : "0~10485760",
    "default_value" : "10485760",
    "config_type" : "dynamic",
    "value" : "10485760",
    "value_type" : "integer"
  }, {
    "name" : "auto.create*************",
    "valid_values" : "true,false",
    "default_value" : "true",
    "config_type" : "dynamic",
    "value" : "true",
    "value_type" : "enum"
  }, {
    "name" : "connections*************",
    "valid_values" : "5000~600000",
    "default_value" : "600000",
    "config_type" : "static",
    "value" : "600000",
    "value_type" : "integer"
  }, {
    "name" : "log.rete*************",
    "valid_values" : "1~168",
    "default_value" : "72",
    "config_type" : "static",
    "value" : "72",
    "value_type" : "integer"
  }, {
    "name" : "max.connec*************",
    "valid_values" : "100~20000",
    "default_value" : "1000",
    "config_type" : "dynamic",
    "value" : "1000",
    "value_type" : "integer"
  }, {
    "name" : "group.max.ses*************",
    "valid_values" : "6000~1800000",
    "default_value" : "1800000",
    "config_type" : "static",
    "value" : "1800000",
    "value_type" : "integer"
  }, {
    "name" : "unclean.leade*************",
    "valid_values" : "true,false",
    "default_value" : "false",
    "config_type" : "dynamic",
    "value" : "false",
    "value_type" : "enum"
  }, {
    "name" : "default.repl*************",
    "valid_values" : "1~3",
    "default_value" : "3",
    "config_type" : "static",
    "value" : "3",
    "value_type" : "integer"
  }, {
    "name" : "offsets.re*************",
    "valid_values" : "1440~30240",
    "default_value" : "20160",
    "config_type" : "dynamic",
    "value" : "20160",
    "value_type" : "integer"
  }, {
    "name" : "num.part*************",
    "valid_values" : "1~200",
    "default_value" : "3",
    "config_type" : "static",
    "value" : "3",
    "value_type" : "integer"
  }, {
    "name" : "group.min.ses*************",
    "valid_values" : "6000~300000",
    "default_value" : "6000",
    "config_type" : "static",
    "value" : "6000",
    "value_type" : "integer"
  }, {
    "name" : "allow.everyon*************",
    "valid_values" : "true,false",
    "default_value" : "true",
    "config_type" : "static",
    "value" : "true",
    "value_type" : "enum"
  } ]
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/09/26 | 1.0 | This document is released for the first time.