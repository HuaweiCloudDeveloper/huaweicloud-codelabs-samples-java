### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/Kafka/doc?api=BatchDeleteGroup).

2.In this example, you can delete consumer groups of Kafka instances in batches.

3.You can delete multiple consumer groups in batches. Deleting a consumer group can delete consumer groups that are no longer used, release resources, and avoid naming conflicts when a consumer group needs to be re-created.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The current account has the permission to use DMS Kafka.

6.An instance has been created in DMS Kafka.

7.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-kafka. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-kafka</artifactId>
    <version>3.1.121</version>
</dependency>

```

### Code example

``` java
package com.huawei.kafka;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.BatchDeleteGroupReq;
import com.huaweicloud.sdk.kafka.v2.model.BatchDeleteGroupRequest;
import com.huaweicloud.sdk.kafka.v2.model.BatchDeleteGroupResponse;
import com.huaweicloud.sdk.kafka.v2.region.KafkaRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.ArrayList;

public class BatchDeleteGroup {

    private static final Logger logger = LoggerFactory.getLogger(BatchDeleteGroup.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String batchDeleteGroupAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String batchDeleteGroupSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Note: For the following projectId, choose HUAWEI CLOUD Console > IAM My Credential > the project ID of the corresponding region.
        String projectId = "<YOUR PROJECT ID>";

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(batchDeleteGroupAk)
                .withSk(batchDeleteGroupSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to KafkaRegion.AP_SOUTHEAST_3.
        KafkaClient client = KafkaClient.newBuilder()
                .withCredential(auth)
                .withRegion(KafkaRegion.AP_SOUTHEAST_3)
                .withHttpConfig(httpConfig)
                .build();

        // Construct a request and set the request parameter instance ID and IDs of all consumer groups to be deleted.
        BatchDeleteGroupRequest request = new BatchDeleteGroupRequest();
        // Specifies the instance ID, which can be obtained from the Kafka page. The ID is displayed under the Kafka instance name. You can also obtain the instance ID from the API for querying ListInstances of all instances. The returned value contains the instance ID.
        String instanceId = "<YOUR INSTANCE ID>";
        request.withInstanceId(instanceId);
        BatchDeleteGroupReq body = new BatchDeleteGroupReq();
        List<String> listbodyGroupIds = new ArrayList<>();
        // IDs of all consumer groups to be deleted. The value can be obtained from the API for querying ListInstanceConsumerGroups of all consumer groups. The return value contains the consumer group ID.
        listbodyGroupIds.add("<YOUR GROUP ID>");
        body.withGroupIds(listbodyGroupIds);
        request.withBody(body);

        try {
            // Send a request
            BatchDeleteGroupResponse response = client.batchDeleteGroup(request);
            // Print the response result.
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
```
{
  "failed_groups" : [ {
    "group_id" : "test-1",
    "error_message" : "UNKNOW"
  }, {
    "group_id" : "test-2",
    "error_message" : "UNKNOW"
  } ],
  "total" : 2
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/11/11 | 1.0 | This document is released for the first time.