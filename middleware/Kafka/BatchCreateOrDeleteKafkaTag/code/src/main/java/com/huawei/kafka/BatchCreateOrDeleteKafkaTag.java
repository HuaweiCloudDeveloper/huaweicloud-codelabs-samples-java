package com.huawei.kafka;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.*;
import com.huaweicloud.sdk.kafka.v2.region.KafkaRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;


public class BatchCreateOrDeleteKafkaTag {
    private static final Logger logger = LoggerFactory.getLogger(BatchCreateOrDeleteKafkaTag.class.getName());

    public static void main(String[] args) {

        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String batchCreateOrDeleteKafkaTagAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String batchCreateOrDeleteKafkaTagSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 说明：如下ProjectId请在“华为云控制台> IAM我的凭证>对应region的project ID”中获取。
        String projectId = "<YOUR PROJECT ID>";

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(batchCreateOrDeleteKafkaTagAk)
                .withSk(batchCreateOrDeleteKafkaTagSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为Region.CN_NORTH_4
        KafkaClient client = KafkaClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(KafkaRegion.CN_NORTH_4)
                .build();

        // 构建请求
        BatchCreateOrDeleteKafkaTagRequest request = new BatchCreateOrDeleteKafkaTagRequest();
        request.withInstanceId("<YOUR INSTANCE ID>");
        BatchCreateOrDeleteTagReq body = new BatchCreateOrDeleteTagReq();
        ArrayList<TagEntity> listbodyTags = new ArrayList<>();
        listbodyTags.add(
                new TagEntity()
                        // 标签键。不能为空。对于同一个实例，Key值唯一。长度为1~128个字符（中文也可以输入128个字符）由任意语种字母、数字、空格和字符组成，字符仅支持_ . : = + - @不能以_sys_开头。首尾字符不能为空格。
                        .withKey("<YOUR KEY>")
                        // 长度为0~255个字符（中文也可以输入255个字符）。由任意语种字母、数字、空格和字符组成，字符仅支持_ . : = + - @
                        .withValue("<YOUR VALUE>")
        );
        body.withTags(listbodyTags);
        // 操作标识（仅支持小写）:create（创建）delete（删除）
        body.withAction(BatchCreateOrDeleteTagReq.ActionEnum.fromValue("create"));
        request.withBody(body);

        try {
            // 发送请求
            BatchCreateOrDeleteKafkaTagResponse response = client.batchCreateOrDeleteKafkaTag(request);
            // 打印响应结果
            logger.info(response.toString());
        }catch(ServiceResponseException  e){
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}