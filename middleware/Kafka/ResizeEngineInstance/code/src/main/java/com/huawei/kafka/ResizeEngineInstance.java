package com.huawei.kafka;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.*;
import com.huaweicloud.sdk.kafka.v2.region.KafkaRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class ResizeEngineInstance {
    private static final Logger logger = LoggerFactory.getLogger(ResizeEngineInstance.class.getName());

    public static void main(String[] args) {

        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 说明：如下ProjectId请在“华为云控制台> IAM我的凭证>对应region的project ID”中获取。
        String projectId = "<YOUR PROJECT ID>";

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为Region.CN_NORTH_4
        KafkaClient client = KafkaClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(KafkaRegion.CN_NORTH_4)
                .build();

        // 构建请求
        ResizeEngineInstanceRequest request = new ResizeEngineInstanceRequest();
        // 消息引擎
        request.withEngine(ResizeEngineInstanceRequest.EngineEnum.fromValue("kafka"));
        request.withInstanceId("<YOUR INSTANCE ID>");
        ResizeEngineInstanceReq body = new ResizeEngineInstanceReq();
        // 当oper_type参数为horizontal时，该参数有效。
        body.withNewBrokerNum(5);
        // 扩容后的存储空间。当oper_type类型是storage或horizontal时，该参数有效且必填。实例存储空间 = 代理数量 * 每个broker的存储空间。
        //当oper_type类型是storage时，代理数量不变，每个broker存储空间最少扩容100GB。当oper_type类型是horizontal时，每个broker的存储空间不变。
        body.withNewStorageSpace(1600);
        // 变更类型。取值范围：storage：存储空间扩容，代理数量不变。horizontal：代理数量扩容，每个broker的存储空间不变。vertical：垂直扩容，broker的底层虚机规格变更，代理数量和存储空间不变。
        body.withOperType("horizontal");
        request.withBody(body);

        try {
            // 发送请求
            ResizeEngineInstanceResponse response = client.resizeEngineInstance(request);
            // 打印响应结果
            logger.info(response.toString());
        }catch(ServiceResponseException  e){
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}