package com.huawei.dms.kafka.lifecycle;

import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.ShowKafkaTagsRequest;
import com.huaweicloud.sdk.kafka.v2.model.ShowKafkaTagsResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateInstanceTag {
    private static final Logger logger = LoggerFactory.getLogger(CreateInstanceTag.class);

    public static void main(String[] args) {
        KafkaClient client = General.getClient();
        try {
            ShowKafkaTagsRequest request = new ShowKafkaTagsRequest();
            request.withInstanceId("<YOUR InstanceId>");

            ShowKafkaTagsResponse response = client.showKafkaTags(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
