package com.huawei.dms.kafka.lifecycle;

import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.BatchCreateOrDeleteKafkaTagRequest;
import com.huaweicloud.sdk.kafka.v2.model.BatchCreateOrDeleteTagReq;
import com.huaweicloud.sdk.kafka.v2.model.BatchCreateOrDeleteKafkaTagResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateOrDeleteInstanceTag {
    private static final Logger logger = LoggerFactory.getLogger(CreateOrDeleteInstanceTag.class);

    public static void main(String[] args) {
        KafkaClient client = General.getClient();
        try {
            BatchCreateOrDeleteKafkaTagRequest request = new BatchCreateOrDeleteKafkaTagRequest();
            request.withInstanceId("<YOUR InstanceId>");
            BatchCreateOrDeleteTagReq body = new BatchCreateOrDeleteTagReq();
            List<TagEntity> listbodyTags = new ArrayList<>();
            listbodyTags.add(
                    new TagEntity()
                            .withKey("<YOUR TagKey>")
                            .withValue("<YOUR TagValue>")
            );
            body.withTags(listbodyTags);
            body.withAction(BatchCreateOrDeleteTagReq.ActionEnum.fromValue("<action>"));
            request.withBody(body);

            BatchCreateOrDeleteKafkaTagResponse response = client.batchCreateOrDeleteKafkaTag(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
