### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/Kafka/doc?api=ListInstanceConsumerGroups).
2.Querying All Consumer Groups in Kafka.

3.You can call this API to view consumer group information of a Kafka instance.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The current account has the permission to use DMS Kafka.

6.An instance has been created in DMS Kafka.

7.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-kafka. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-kafka</artifactId>
    <version>3.1.87</version>
</dependency>

```

### Sample Code

``` java
package com.huawei.kafka;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.*;
import com.huaweicloud.sdk.kafka.v2.region.KafkaRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ListInstanceConsumerGroups {
    private static final Logger logger = LoggerFactory.getLogger(ListInstanceConsumerGroups.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String listInstanceConsumerGroupsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listInstanceConsumerGroupsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Note: For the following projectId, choose HUAWEI CLOUD Console > IAM My Credential > the project ID of the corresponding region.
        String projectId = "<YOUR PROJECT ID>";

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listInstanceConsumerGroupsAk)
                .withSk(listInstanceConsumerGroupsSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to Region.AP_SOUTHEAST_3.
        KafkaClient client = KafkaClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(KafkaRegion.AP_SOUTHEAST_3)
                .build();

        // Build Request
        ListInstanceConsumerGroupsRequest request = new ListInstanceConsumerGroupsRequest();
        request.withInstanceId("<YOUR INSTANCE ID>");
        // Offset, indicating that the query starts from this offset. The offset is greater than or equal to 0.
        request.withOffset("<YOUR OFFSET>");
        // Maximum number of consumer group IDs returned in the current query. The default value is 10. The value ranges from 1 to 50.
        request.withLimit("<YOUR LIMIT>");
        // Consumer group name filtering query. The filtering mode is field containing filtering.
        request.withGroup("<YOUR GROUP NAME>");

        try {
            // Send a request
            ListInstanceConsumerGroupsResponse response = client.listInstanceConsumerGroups(request);
            // Print the response result
            logger.info(response.toString());
        }catch(ServiceResponseException  e){
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}
```

### Example of the returned result
```
{
  "groups" : [ {
    "createdAt" : 1691401194847,
    "group_id" : "cons********************",
    "state" : "EMPTY",
    "coordinator_id" : 1,
    "lag" : 0,
    "group_desc" : null
  }, {
    "createdAt" : 1691401194960,
    "group_id" : "cons********************",
    "state" : "STABLE",
    "coordinator_id" : 2,
    "lag" : 0,
    "group_desc" : null
  }, {
    "createdAt" : 1691401207309,
    "group_id" : "cons********************",
    "state" : "STABLE",
    "coordinator_id" : 3,
    "lag" : 0,
    "group_desc" : null
  } ],
  "total" : 3
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/09/26 | 1.0 | This document is released for the first time.