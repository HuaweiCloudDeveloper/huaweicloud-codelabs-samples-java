package com.huawei.kafka;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.CreateGroupReq;
import com.huaweicloud.sdk.kafka.v2.model.CreateKafkaConsumerGroupRequest;
import com.huaweicloud.sdk.kafka.v2.model.CreateKafkaConsumerGroupResponse;
import com.huaweicloud.sdk.kafka.v2.region.KafkaRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateKafkaConsumerGroup {

    private static final Logger logger = LoggerFactory.getLogger(CreateKafkaConsumerGroup.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String createKafkaConsumerGroupAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String createKafkaConsumerGroupSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Note: For the following projectId, choose HUAWEI CLOUD Console > IAM My Credential > the project ID of the corresponding region.
        String projectId = "<YOUR PROJECT ID>";

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(createKafkaConsumerGroupAk)
                .withSk(createKafkaConsumerGroupSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to KafkaRegion.AP_SOUTHEAST_3.
        KafkaClient client = KafkaClient.newBuilder()
                .withCredential(auth)
                .withRegion(KafkaRegion.AP_SOUTHEAST_3)
                .withHttpConfig(httpConfig)
                .build();

        // Construct a request and set the request parameters instance ID and consumer group name.
        CreateKafkaConsumerGroupRequest request = new CreateKafkaConsumerGroupRequest();
        // Specifies the instance ID, which can be obtained from the Kafka page. The ID is displayed under the Kafka instance name. You can also obtain the instance ID from the API for querying ListInstances of all instances. The returned value contains the instance ID.
        String instanceId = "<YOUR INSTANCE ID>";
        request.withInstanceId(instanceId);
        CreateGroupReq body = new CreateGroupReq();
        // Consumer group name
        String groupName = "<YOUR GROUP NAME>";
        body.withGroupName(groupName);
        request.withBody(body);

        try {
            // Send a request
            CreateKafkaConsumerGroupResponse response = client.createKafkaConsumerGroup(request);
            // Print the response result.
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}