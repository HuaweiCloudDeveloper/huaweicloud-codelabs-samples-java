### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/Kafka/debug?api=CreateKafkaConsumerGroup) 中直接运行调试该接口。

2.在本样例中，您可以创建消费组。

3.创建消费组，可以用于处理生产者产生的消息，通过创建新的消费组，可以更好地管理和分配分区。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.当前登录账号拥有使用分布式消息服务 Kafka的权限。

6.已在分布式消息服务 Kafka创建实例。

7.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-kafka”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-kafka</artifactId>
    <version>3.1.121</version>
</dependency>
```

### 代码示例
``` java
package com.huawei.kafka;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.kafka.v2.KafkaClient;
import com.huaweicloud.sdk.kafka.v2.model.CreateGroupReq;
import com.huaweicloud.sdk.kafka.v2.model.CreateKafkaConsumerGroupRequest;
import com.huaweicloud.sdk.kafka.v2.model.CreateKafkaConsumerGroupResponse;
import com.huaweicloud.sdk.kafka.v2.region.KafkaRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateKafkaConsumerGroup {

    private static final Logger logger = LoggerFactory.getLogger(CreateKafkaConsumerGroup.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String createKafkaConsumerGroupAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String createKafkaConsumerGroupSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 说明：如下ProjectId请在“华为云控制台> IAM我的凭证>对应region的project ID”中获取。
        String projectId = "<YOUR PROJECT ID>";

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(createKafkaConsumerGroupAk)
                .withSk(createKafkaConsumerGroupSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为Region.CN_NORTH_4
        KafkaClient client = KafkaClient.newBuilder()
                .withCredential(auth)
                .withRegion(KafkaRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();

        // 构建请求
        CreateKafkaConsumerGroupRequest request = new CreateKafkaConsumerGroupRequest();
        //实例ID，可从Kafka页面获取。ID显示在Kafka实例名称下。实例ID也可以通过查询所有实例的ListInstances接口获取。返回值中包含实例ID。
        String instanceId = "<YOUR INSTANCE ID>";
        request.withInstanceId(instanceId);
        CreateGroupReq body = new CreateGroupReq();
        //消费组名称
        String groupName = "<YOUR GROUP NAME>";
        body.withGroupName(groupName);
        request.withBody(body);

        try {
            // 发送请求
            CreateKafkaConsumerGroupResponse response = client.createKafkaConsumerGroup(request);
            // 打印响应结果
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例
```
{
    body: success
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-11-12 | 1.0 | 文档首次发布 |