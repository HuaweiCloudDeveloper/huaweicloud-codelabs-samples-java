package com.huawei.dcs;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.dcs.v2.region.DcsRegion;
import com.huaweicloud.sdk.dcs.v2.*;
import com.huaweicloud.sdk.dcs.v2.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ResetPassword {

    private static final Logger logger = LoggerFactory.getLogger(ResetPassword.class.getName());

    public static void main(String[] args) {

        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String resetPasswordAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String resetPasswordSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 注意，如下的projectId 请使用对应DCS项目中代码检查任务详情链接中的ID
        String projectId = "<YOUR PROJECT ID>";

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(resetPasswordAk)
                .withSk(resetPasswordSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为Region.CN_NORTH_4
        DcsClient client = DcsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(DcsRegion.CN_NORTH_4)
                .build();

        // 构建执行检查任务请求头并设置MigrationTasks参数
        ResetPasswordRequest request = new ResetPasswordRequest();
        request.withInstanceId("<YOUR INSTANCE ID>");
        ResetInstancePasswordBody body = new ResetInstancePasswordBody();
        body.withNoPasswordAccess(false);
        body.withNewPassword("<YOUR NEW PASSWORD>");
        request.withBody(body);

        try {
            // 发送请求批量启动迁移任务
            ResetPasswordResponse response = client.resetPassword(request);
            // 打印响应结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }
    }
}