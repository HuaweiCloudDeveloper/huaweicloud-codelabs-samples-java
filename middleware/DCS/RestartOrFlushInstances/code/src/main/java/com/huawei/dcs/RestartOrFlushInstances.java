package com.huawei.dcs;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.dcs.v2.region.DcsRegion;
import com.huaweicloud.sdk.dcs.v2.*;
import com.huaweicloud.sdk.dcs.v2.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;


public class RestartOrFlushInstances {

    private static final Logger logger = LoggerFactory.getLogger(RestartOrFlushInstances.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String restartOrFlushInstancesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String restartOrFlushInstancesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Note: For the following deletePipeProjectId, choose HUAWEI CLOUD Console > IAM My Credential > the project ID of the corresponding region.
        String projectId = "<YOUR PROJECT ID>";

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(restartOrFlushInstancesAk)
                .withSk(restartOrFlushInstancesSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to Region.CN_NORTH_4.
        DcsClient client = DcsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(DcsRegion.CN_NORTH_4)
                .build();

        // Build Request
        RestartOrFlushInstancesRequest request = new RestartOrFlushInstancesRequest();
        ChangeInstanceStatusBody body = new ChangeInstanceStatusBody();
        ArrayList<String> listbodyInstances = new ArrayList<>();
        listbodyInstances.add("<YOUR INSTANCE ID>");
        // Operation on the instance: restart: forcibly restart soft_restart: soft restart, restart only the process flush: clear data
        body.withAction("flush");
        body.withInstances(listbodyInstances);
        request.withBody(body);

        try {
            // Send a request
            RestartOrFlushInstancesResponse response = client.restartOrFlushInstances(request);
            // Print the response result.
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }
    }
}