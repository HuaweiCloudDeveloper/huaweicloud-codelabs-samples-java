package com.huawei.dcs;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.dcs.v2.region.DcsRegion;
import com.huaweicloud.sdk.dcs.v2.*;
import com.huaweicloud.sdk.dcs.v2.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ListNumberOfInstancesInDifferentStatus {

    private static final Logger logger = LoggerFactory.getLogger(ListNumberOfInstancesInDifferentStatus.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String listNumberOfInstancesInDifferentStatusAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listNumberOfInstancesInDifferentStatusSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Note: For the following deletePipeProjectId, choose HUAWEI CLOUD Console > IAM My Credential > the project ID of the corresponding region.
        String projectId = "<YOUR PROJECT ID>";

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listNumberOfInstancesInDifferentStatusAk)
                .withSk(listNumberOfInstancesInDifferentStatusSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to Region.CN_NORTH_4.
        DcsClient client = DcsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(DcsRegion.CN_NORTH_4)
                .build();

        // Build Request
        ListNumberOfInstancesInDifferentStatusRequest request = new ListNumberOfInstancesInDifferentStatusRequest();
        // Indicates whether to return the number of instances that fail to be created. If this parameter is set to true, the returned statistics include the number of instances that fail to be created.
        // If this parameter is set to false or other values, the returned statistics do not include the number of instances that fail to be created.
        request.withIncludeFailure("true");

        try {
            // Send a request
            ListNumberOfInstancesInDifferentStatusResponse response = client.listNumberOfInstancesInDifferentStatus(request);
            // Print the response result.
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }
    }
}