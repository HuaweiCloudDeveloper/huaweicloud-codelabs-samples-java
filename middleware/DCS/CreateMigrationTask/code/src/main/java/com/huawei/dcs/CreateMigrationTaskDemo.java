package com.huawei.dcs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.dcs.v2.DcsClient;
import com.huaweicloud.sdk.dcs.v2.model.BackupFilesBody;
import com.huaweicloud.sdk.dcs.v2.model.Files;
import com.huaweicloud.sdk.dcs.v2.model.SourceInstanceBody;
import com.huaweicloud.sdk.dcs.v2.model.TargetInstanceBody;
import com.huaweicloud.sdk.dcs.v2.model.CreateMigrationTaskBody;
import com.huaweicloud.sdk.dcs.v2.model.CreateMigrationTaskRequest;
import com.huaweicloud.sdk.dcs.v2.model.CreateMigrationTaskResponse;
import com.huaweicloud.sdk.dcs.v2.region.DcsRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CreateMigrationTaskDemo {

    private static final Logger logger = LoggerFactory.getLogger(CreateMigrationTaskDemo.class.getName());

    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String PROJECT_ID = "<YOUR PROJECTID>";

    public static void main(String[] args) {

        String task_name = "<YOUR BACKUP TASK NAME>";

        String description = "<YOUR TASK DESCRIPTION>";

        // backupfile_import：表示备份文件导入
        String migration_type = "backupfile_import";

        // full_amount_migration：表示全量迁移,incremental_migration：表示增量迁移。
        String migration_method = "full_amount_migration";

        BackupFilesBody backupFilesBody = BackupFilesBodyRequest();

        // 分为vpc和vpn两种模式
        String network_type = "vpc";

        SourceInstanceBody sourceInstanceBody = SourceInstanceBodyRequest();

        TargetInstanceBody targetInstanceBody = TargetInstanceBodyRequest();

        ICredential auth = new BasicCredentials()
                .withAk(AK)
                .withSk(SK)
                .withProjectId(PROJECT_ID);

        HttpConfig httpConfig = new HttpConfig();

        DcsClient dcsClient = DcsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(DcsRegion.CN_NORTH_4)
                .build();

        // 请求体中task_name，migration_type，migration_method，target_instance是必填项
        CreateMigrationTaskBody requestBody = new CreateMigrationTaskBody()
                .withTaskName(task_name)
                .withDescription(description)
                .withMigrationType(CreateMigrationTaskBody.MigrationTypeEnum.valueOf(migration_type))
                .withMigrationMethod(CreateMigrationTaskBody.MigrationMethodEnum.valueOf(migration_method))
                .withBackupFiles(backupFilesBody)
                .withNetworkType(CreateMigrationTaskBody.NetworkTypeEnum.valueOf(network_type))
                .withSourceInstance(sourceInstanceBody)
                .withTargetInstance(targetInstanceBody);

        CreateMigrationTaskRequest createMigrationTaskRequest = new CreateMigrationTaskRequest().withBody(requestBody);
        try {
            CreateMigrationTaskResponse createMigrationTaskResponse = dcsClient.createMigrationTask(createMigrationTaskRequest);
            logger.info(createMigrationTaskResponse.toString());
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static BackupFilesBody BackupFilesBodyRequest() {
        BackupFilesBody backupFilesBody = new BackupFilesBody();
        // self_build_obs:来源obs桶， backup_record：来源备份记录
        String file_source = "backup_record";
        String bucket_name = "<YOUR BUCKET NAME>";
        // 导入的文件列表,可以有多个file文件，必须要有filename
        List<Files> Files = new ArrayList<>();
        Files file = new Files();
        String file_name = "<YOUR FILE NAME>";
        String size = "<YOUR FILE SIZE>";
        String update_at = "<YOUR FILE LAST UPDATE TIME>";
        file.setFileName(file_name);
        file.setSize(size);
        file.setUpdateAt(update_at);
        Files.add(file);
        String backup_id = "<YOUR BACKUP ID>";
        // 其中Files和bucket_name为比填项
        backupFilesBody.setFileSource(BackupFilesBody.FileSourceEnum.valueOf(file_source));
        backupFilesBody.setBucketName(bucket_name);
        backupFilesBody.setFiles(Files);
        backupFilesBody.setBackupId(backup_id);
        return backupFilesBody;
    }

    private static SourceInstanceBody SourceInstanceBodyRequest() {
        SourceInstanceBody sourceInstanceBody = new SourceInstanceBody();
        String addrs = "<YOUR ADDRS>";
        String password = "<YOUR REDIS PASSWORD>";
        // addrs为必填项，若为桶数据导入，则不填
        sourceInstanceBody.setAddrs(addrs);
        sourceInstanceBody.setPassword(password);
        return  sourceInstanceBody;
    }

    private static TargetInstanceBody TargetInstanceBodyRequest() {
        TargetInstanceBody targetInstanceBody = new TargetInstanceBody();
        String id = "<REDIS INSTANCE ID>";
        String name = "<REDIS INSTANCE NAME>";
        String instancePassword = "<REDIS PASSWORD>";
        // 迁移和备份导入目标实例，其中id是必填项
        targetInstanceBody.setId(id);
        targetInstanceBody.setName(name);
        targetInstanceBody.setPassword(instancePassword);
        return targetInstanceBody;
    }

}
