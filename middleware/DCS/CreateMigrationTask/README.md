## 1. 简介

华为云提供了分布式缓存（DCS）的SDK，您可以直接集成SDK来调用DCS的相关API，从而实现对DCS的快速操作。
该示例展示了如何通过java版SDK来创建备份导入任务。
## 2. 前置条件
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。

## 3. 安装SDK
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

具体的SDK版本号请参见 [SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-dcs</artifactId>
    <version>3.1.60</version>
</dependency>
```
## 4. 代码示例
以下代码展示如何使用SDK备份指定实例
``` java
public class CreateMigrationTaskDemo {

    private static final Logger logger = LoggerFactory.getLogger(CreateMigrationTaskDemo.class.getName());
    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String PROJECT_ID = "<YOUR PROJECTID>";

    public static void main(String[] args) {

        String task_name = "<YOUR BACKUP TASK NAME>";

        String description = "<YOUR TASK DESCRIPTION>";

        // backupfile_import：表示备份文件导入
        String migration_type = "backupfile_import";

        // full_amount_migration：表示全量迁移,incremental_migration：表示增量迁移。
        String migration_method = "full_amount_migration";

        BackupFilesBody backupFilesBody = BackupFilesBodyRequest();

        // 分为vpc和vpn两种模式
        String network_type = "vpc";

        SourceInstanceBody sourceInstanceBody = SourceInstanceBodyRequest();

        TargetInstanceBody targetInstanceBody = TargetInstanceBodyRequest();

        ICredential auth = new BasicCredentials()
                .withAk(AK)
                .withSk(SK)
                .withProjectId(PROJECT_ID);

        HttpConfig httpConfig = new HttpConfig();

        DcsClient dcsClient = DcsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(DcsRegion.CN_NORTH_4)
                .build();

        // 请求体中task_name，migration_type，migration_method，target_instance是必填项
        CreateMigrationTaskBody requestBody = new CreateMigrationTaskBody()
                .withTaskName(task_name)
                .withDescription(description)
                .withMigrationType(CreateMigrationTaskBody.MigrationTypeEnum.valueOf(migration_type))
                .withMigrationMethod(CreateMigrationTaskBody.MigrationMethodEnum.valueOf(migration_method))
                .withBackupFiles(backupFilesBody)
                .withNetworkType(CreateMigrationTaskBody.NetworkTypeEnum.valueOf(network_type))
                .withSourceInstance(sourceInstanceBody)
                .withTargetInstance(targetInstanceBody);

        CreateMigrationTaskRequest createMigrationTaskRequest = new CreateMigrationTaskRequest().withBody(requestBody);
        try {
            CreateMigrationTaskResponse createMigrationTaskResponse = dcsClient.createMigrationTask(createMigrationTaskRequest);
            logger.info(createMigrationTaskResponse.toString());
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

}
```
您可以在 [API Explorer](https://console.ulanqab.huawei.com/apiexplorer/#/openapi/DCS/doc?api=CreateMigrationTask) 中直接运行调试该接口。

## 5. 返回结果示例
``` java
{
    "id": "156080818b6bc66a018b6bca9fdd00fb"
    "name": "dcs-migration-test"
    "status": "MIGRATING"
}
```
## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-10-30 | 1.0 | 文档首次发布|

## 7. 参考链接
分布式缓存服务DCS官方文档链接：https://support.huaweicloud.com/wtsnew-dcs/index.html