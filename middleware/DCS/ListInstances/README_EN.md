## Example Introduction

1.You can run the debugging interface directly in the link[API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/DCS/doc?api=ListInstances).

2.This interface is used to query all DCS instances.

3.This interface is used to obtain the DCS instance list and instance details.

## Preparations Before Development

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.You have created a project and created a check task on the DCS platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-dcs. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

``` xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-dcs</artifactId>
    <version>3.1.113</version>
</dependency>
```

## Sample Code

``` java
package com.huawei.dcs;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.dcs.v2.model.*;
import com.huaweicloud.sdk.dcs.v2.region.DcsRegion;
import com.huaweicloud.sdk.dcs.v2.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ListInstances {
    private static final Logger logger = LoggerFactory.getLogger(ListInstances.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String listAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Note: For the following deletePipeProjectId, choose HUAWEI CLOUD Console > IAM My Credential > the project ID of the corresponding region.
        String projectId = "<YOUR PROJECT ID>";

         // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listAk)
                .withSk(listSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to Region.CN_NORTH_4.
        DcsClient client = DcsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(DcsRegion.CN_NORTH_4)
                .build();

        // Build Request
        ListInstancesRequest request = new ListInstancesRequest();

        try {
            // Send a request
            ListInstancesResponse response = client.listInstances(request);
            // Print the response result.
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}
```
## Example of the returned result
```
{
 "instances": [],
 "instance_num": 0
}
```

## Change History

| Release Date | Document Version |             Revision Description              |
|:------------:|:----------------:|:---------------------------------------------:|
|  2024-10-28  |       1.0        | This document is released for the first time. |