### 产品介绍

1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/DCS/doc?api=ListMigrationTask)
中直接运行调试该接口。

2.DCS查询迁移任务列表。

3.调用接口查询迁移任务列表，显示迁移任务详细信息

### 前置条件

1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在DCS平台创建项目创建检查任务。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-dcs”，具体的SDK版本号请参见 [SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-dcs</artifactId>
    <version>3.1.113</version>
</dependency>
```

### 代码示例

``` java
package com.huawei.dcs;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.dcs.v2.model.*;
import com.huaweicloud.sdk.dcs.v2.region.DcsRegion;
import com.huaweicloud.sdk.dcs.v2.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ListMigrationTask {
    private static final Logger logger = LoggerFactory.getLogger(ListMigrationTask.class.getName());

    public static void main(String[] args) {

        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listMigrationAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listMigrationSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 注意，如下的projectId 请使用对应DCS项目中代码检查任务详情链接中的ID
        String projectId = "<YOUR PROJECT ID>";

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(listMigrationAk)
                .withSk(listMigrationSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为Region.CN_NORTH_4
        DcsClient client = DcsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(DcsRegion.CN_NORTH_4)
                .build();

        // 构建请求
        ListMigrationTaskRequest request = new ListMigrationTaskRequest();

        try {
            // 发送请求
            ListMigrationTaskResponse response = client.listMigrationTask(request);
            // 打印响应结果
            logger.info(response.toString());
        }catch(ServiceResponseException  e){
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}
```

### 返回结果示例
```
{
    "count": 1,
    "migration_tasks": [
        {
            "task_id": "2944a563-6f27-*************",
            "task_name": "dcs-migration*************",
            "status": "TERMINATED",
            "migration_type": "online_migration",
            "migration_method": "incremental_migration",
            "data_source": "192.168.*************",
            "source_instance_name": "migration-*************",
            "source_instance_id": "132e0017-715b-4556-*************",
            "target_instance_addrs": "192.168.*************",
            "target_instance_name": "migration-*************",
            "target_instance_id": "eb1a8d1c-9ea6-4fcd-*************",
            "created_at": "2021-06-16T06:12:55.508Z",
            "description": "dcs-test",
            "source_instance_status": "RUNNING",
            "target_instance_status": "RUNNING",
            "source_instance_subnet_id": "192.*************",
            "target_instance_subnet_id": "192.*************",
            "source_instance_spec_code": "redis.single.xu1.tiny.128",
            "target_instance_spec_code": "redis.single.xu1.tiny.128",
            "error_message": null,
            "released_at": "202404011057",
            "version": "6.0.21",
            "resume_mode": "auto",
            "supported_features": []
        }
    ]
}
```

### 修订记录

    发布日期    | 文档版本 | 修订说明

:----------:|:----:| :------:  
2024/09/10 | 1.0 | 文档首次发布