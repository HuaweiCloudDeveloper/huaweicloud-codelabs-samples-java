### 产品介绍

1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/DCS/doc?api=ShowMigrationTask)
中直接运行调试该接口。

2. DCS查询迁移任务详情。

3.redis迁移如果需要查询任务的详情信息，可以调用此接口达到预期。

### 前置条件

1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在DCS平台创建项目创建检查任务。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-dcs”，具体的SDK版本号请参见 [SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-dcs</artifactId>
    <version>3.1.113</version>
</dependency>
```

### 代码示例

``` java
package com.huawei.dcs;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.dcs.v2.model.*;
import com.huaweicloud.sdk.dcs.v2.region.DcsRegion;
import com.huaweicloud.sdk.dcs.v2.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ShowMigrationTask {
    private static final Logger logger = LoggerFactory.getLogger(ShowMigrationTask.class.getName());

    public static void main(String[] args) {

        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String showMigrationAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showMigrationSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 注意，如下的projectId 请使用对应DCS项目中代码检查任务详情链接中的ID
        String projectId = "<YOUR PROJECT ID>";

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(showMigrationAk)
                .withSk(showMigrationSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为Region.CN_NORTH_4
        DcsClient client = DcsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(DcsRegion.CN_NORTH_4)
                .build();

        // 构造请求
        ShowMigrationTaskRequest request = new ShowMigrationTaskRequest();
        request.withTaskId("<YOUR TASK ID>");

        try {
            // 发送请求
            ShowMigrationTaskResponse response = client.showMigrationTask(request);
            // 打印响应结果
            logger.info(response.toString());
        }catch(ServiceResponseException  e){
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}
```

### 返回结果示例
```
{
  "task_name" : "migr*******************",
  "task_id" : "a276c1ffa0*******************",
  "status" : "TERMINATING",
  "migration_type" : "incremental_migration",
  "network_type" : "vpc",
  "source_instance" : {
    "addrs" : "192.*******************",
    "id" : "86157b69-8a62*******************",
    "name" : "dcs*******************",
    "ip" : "192.1*******************",
    "port" : "6379",
    "proxy_multi_db" : false,
    "password" : "DCS*******************",
    "task_status" : "RUNNING",
    "db" : "0"
  },
  "target_instance" : {
    "addrs" : "192.1*******************",
    "id" : "86157b69-8a62-47*******************",
    "name" : "test-*******************",
    "ip" : "192.1*******************",
    "port" : "6379",
    "proxy_multi_db" : false,
    "password" : "DCS*******************",
    "task_status" : "RUNNING",
    "db" : "0"
  },
  "created_at" : "2019/10/22 14:44:30",
  "updated_at" : "2019/10/22 18:00:00"
}
```

### 修订记录

    发布日期    | 文档版本 | 修订说明

:----------:|:----:| :------:  
2024/09/10 | 1.0 | 文档首次发布