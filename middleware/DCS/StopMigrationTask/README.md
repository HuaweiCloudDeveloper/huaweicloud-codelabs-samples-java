## 1. 简介

华为云提供了分布式缓存（DCS）的SDK，您可以直接集成SDK来调用DCS的相关API，从而实现对DCS的快速操作。
该示例展示了如何通过java版SDK来停止数据迁移任务。
## 2. 前置条件
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。

## 3. 安装SDK
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

具体的SDK版本号请参见 [SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-dcs</artifactId>
    <version>3.1.62</version>
</dependency>
```
## 4. 代码示例
以下代码展示如何使用SDK备份指定实例
``` java
package com.huawei.dcs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.dcs.v2.DcsClient;
import com.huaweicloud.sdk.dcs.v2.model.StopMigrationTaskRequest;
import com.huaweicloud.sdk.dcs.v2.model.StopMigrationTaskResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StopMigrationTaskDemo {

    private static final Logger logger = LoggerFactory.getLogger(StopMigrationTaskDemo.class.getName());

    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String PROJECT_ID = "<YOUR PROJECTID>";

    private static final String TASK_ID = "<MIGRATION TASK ID>";

    public static void main(String[] args) {

        ICredential auth = new BasicCredentials()
                .withAk(AK)
                .withSk(SK)
                .withProjectId(PROJECT_ID);

        DcsClient dcsClient = DcsClient.newBuilder()
                .withCredential(auth)
                .withRegion(DcsRegion.CN_NORTH_1)
                .build();
        
        StopMigrationTaskRequest requestBody = new StopMigrationTaskRequest().withTaskId(TASK_ID);
        try {
            StopMigrationTaskResponse stopMigrationTaskResponse = dcsClient.stopMigrationTask(requestBody);
            logger.info(stopMigrationTaskResponse.toString());
        } catch ( ServiceResponseException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

}
```
您可以在 [API Explorer](https://console.ulanqab.huawei.com/apiexplorer/#/openapi/DCS/doc?api=StopMigrationTask) 中直接运行调试该接口。

## 5. 返回结果示例
``` java
{
    "task_name": "dcs-migration-7x9u",
    "task_id": "8abfa7aa8b7078fc018b708013d60175",
    "description": null,
    "status": "TERMINATING",
    "migration_type": "backupfile_import",
    "migration_method": "full_amount_migration",
    "tenant_vpc_id": null,
    "tenant_subnet_id": null,
    "ecs_tenant_private_ip": null,
    "released_at": null,
    "tenant_security_group_id": null,
    "version": null,
    "backup_files": {
        "file_source": "self_build_obs",
        "bucket_name": "123",
        "files": [
            {
                "file_name": "DCS_V2_VM_X86_Euler2.5_4.0.2   .tar.gz",
                "size": "616.14 MB",
                "update_at": "2023-03-09T06:28:38.455Z"
            }
        ],
        "backup_record": null,
        "proxy_multi_db": null,
        "db": null
    },
    "network_type": "vpc",
    "bandwidth_limit_mb": null,
    "source_instance": null,
    "target_instance": {
        "id": "46b19324-bf46-40b2-9f33-d6c71087be24",
        "ip": null,
        "port": null,
        "password": null,
        "addrs": null,
        "name": "dcs-SDKAPIClusterTest",
        "proxy_multi_db": null,
        "db": null
    },
    "created_at": "2023-10-27T09:38:43.822Z",
    "updated_at": "2023-10-27T09:38:50.974Z",
    "resume_mode": null,
    "supported_features": [],
    "task_status": null
}
```
## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-10-30 | 1.0 | 文档首次发布|

## 7. 参考链接
分布式缓存服务DCS官方文档链接：https://support.huaweicloud.com/wtsnew-dcs/index.html