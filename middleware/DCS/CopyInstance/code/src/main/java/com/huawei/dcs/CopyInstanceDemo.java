package com.huawei.dcs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.dcs.v2.DcsClient;
import com.huaweicloud.sdk.dcs.v2.model.BackupInstanceBody;
import com.huaweicloud.sdk.dcs.v2.model.CopyInstanceRequest;
import com.huaweicloud.sdk.dcs.v2.model.CopyInstanceResponse;
import com.huaweicloud.sdk.dcs.v2.region.DcsRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CopyInstanceDemo {

    private static final Logger logger = LoggerFactory.getLogger(CopyInstanceDemo.class.getName());

    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String PROJECT_ID = "<YOUR PROJECTID>";
    private static final String INSTANCE_ID = "<YOUR INSTANCEID>";

    public static void main(String[] args) {

        String remark = "<REMARK INFOMATION>";

        // 备份实例缓存的格式分为aof和rdb格式
        String backup_format = "rdb";

        ICredential auth = new BasicCredentials()
                .withAk(AK)
                .withSk(SK)
                .withProjectId(PROJECT_ID);

        HttpConfig httpConfig = new HttpConfig();

        DcsClient dcsClient = DcsClient.newBuilder()
            .withCredential(auth)
            .withHttpConfig(httpConfig)
            .withRegion(DcsRegion.CN_EAST_3)
            .build();

        BackupInstanceBody requestBody = new BackupInstanceBody()
                .withRemark(remark)
                .withBackupFormat(BackupInstanceBody.BackupFormatEnum.valueOf(backup_format));

        CopyInstanceRequest copyInstanceRequest = new CopyInstanceRequest()
                .withInstanceId(INSTANCE_ID)
                .withBody(requestBody);
        try {
            CopyInstanceResponse copyInstanceResponse = dcsClient.copyInstance(copyInstanceRequest);
            logger.info(copyInstanceResponse.toString());
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

}
