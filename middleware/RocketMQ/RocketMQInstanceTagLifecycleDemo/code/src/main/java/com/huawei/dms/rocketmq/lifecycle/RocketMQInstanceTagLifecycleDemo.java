package com.huawei.dms.rocketmq.lifecycle;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.rocketmq.v2.RocketMQClient;
import com.huaweicloud.sdk.rocketmq.v2.model.BatchCreateOrDeleteRocketmqTagRequest;
import com.huaweicloud.sdk.rocketmq.v2.model.BatchCreateOrDeleteRocketmqTagResponse;
import com.huaweicloud.sdk.rocketmq.v2.model.BatchCreateOrDeleteTagReq;
import com.huaweicloud.sdk.rocketmq.v2.model.ShowRocketmqTagsRequest;
import com.huaweicloud.sdk.rocketmq.v2.model.ShowRocketmqTagsResponse;
import com.huaweicloud.sdk.rocketmq.v2.model.TagEntity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class RocketMQInstanceTagLifecycleDemo {
    private static final Logger logger = LoggerFactory.getLogger(RocketMQInstanceTagLifecycleDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        ICredential auth = new BasicCredentials()
                .withAk(System.getenv("HUAWEICLOUD_SDK_AK"))
                .withSk(System.getenv("HUAWEICLOUD_SDK_SK"))
                .withProjectId("<YOUR Project Id>");
        RocketMQClient client = RocketMQClient.newBuilder()
                .withCredential(auth)
                .withEndpoint("<YOUR Endpoint String>")
                .build();
        // 创建标签
        createTag(client);
        // 查询标签
        queryTag(client);
        // 删除标签
        deleteTag(client);
    }

    private static void createTag(RocketMQClient client) {
        createOrDeleteTag(client, BatchCreateOrDeleteTagReq.ActionEnum.CREATE);
    }

    private static void createOrDeleteTag(RocketMQClient client, BatchCreateOrDeleteTagReq.ActionEnum create) {
        try {
            TagEntity tag = new TagEntity();
            tag.setKey("<YOUR TagKey>");
            tag.setValue("<YOUR TagVal>");

            List<TagEntity> tags = new ArrayList<>();
            tags.add(tag);

            BatchCreateOrDeleteTagReq body = new BatchCreateOrDeleteTagReq();
            body.setAction(create);
            body.setTags(tags);

            BatchCreateOrDeleteRocketmqTagRequest request = new BatchCreateOrDeleteRocketmqTagRequest();
            request.setInstanceId("<YOUR InstanceId>");
            request.setBody(body);

            BatchCreateOrDeleteRocketmqTagResponse response = client.batchCreateOrDeleteRocketmqTag(request);

            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private static void queryTag(RocketMQClient client) {
        try {
            ShowRocketmqTagsRequest request = new ShowRocketmqTagsRequest();
            request.setInstanceId("<YOUR InstanceId>");

            ShowRocketmqTagsResponse response = client.showRocketmqTags(request);

            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private static void deleteTag(RocketMQClient client) {
        createOrDeleteTag(client, BatchCreateOrDeleteTagReq.ActionEnum.DELETE);
    }
}