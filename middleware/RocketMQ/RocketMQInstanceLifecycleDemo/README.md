## 1. 示例简介
基于华为云Java SDK对分布式消息服务RocketMQ实例进行生命周期管理的代码示例。其中包括：创建RocketMQ实例、删除RocketMQ实例、查询RocketMQ实例、修改RocketMQ实例。

## 2. 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已获取对应区域的项目，请在华为云控制台“我的凭证 > API凭证 > 项目列表”页面上查看项目，例如：cn-north-4。具体请参见[API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html)

## 3. SDK获取和安装
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-rocketmq”，具体的SDK版本号请参见[SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/?language=java)

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>3.0.91</version>
</dependency>
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rocketmq</artifactId>
    <version>3.1.63</version>
</dependency>
```

## 4. 代码示例
以下代码展示如何使用RocketMQ实例创建、删除、查询、修改的相关SDK
```java
package com.huawei.dms.rocketmq.lifecycle;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.rocketmq.v2.RocketMQClient;
import com.huaweicloud.sdk.rocketmq.v2.model.CreatePostPaidInstanceReq;
import com.huaweicloud.sdk.rocketmq.v2.model.CreatePostPaidInstanceRequest;
import com.huaweicloud.sdk.rocketmq.v2.model.CreatePostPaidInstanceResponse;

import java.util.ArrayList;
import java.util.List;

import com.huaweicloud.sdk.rocketmq.v2.model.DeleteInstanceRequest;
import com.huaweicloud.sdk.rocketmq.v2.model.DeleteInstanceResponse;
import com.huaweicloud.sdk.rocketmq.v2.model.ListAvailableZonesRequest;
import com.huaweicloud.sdk.rocketmq.v2.model.ListAvailableZonesResponse;
import com.huaweicloud.sdk.rocketmq.v2.model.ShowInstanceRequest;
import com.huaweicloud.sdk.rocketmq.v2.model.ShowInstanceResponse;
import com.huaweicloud.sdk.rocketmq.v2.model.UpdateInstanceReq;
import com.huaweicloud.sdk.rocketmq.v2.model.UpdateInstanceRequest;
import com.huaweicloud.sdk.rocketmq.v2.model.UpdateInstanceResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RocketMQInstanceLifeCycleDemo {
    private static final Logger logger = LoggerFactory.getLogger(RocketMQInstanceLifeCycleDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String endpoint = "<YOUR Endpoint String>";
        String projectId = "<YOUR Project Id>";
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);
        RocketMQClient client = RocketMQClient.newBuilder()
                .withCredential(auth)
                .withEndpoint(endpoint)
                .build();
        // 查询可用区
        listAvailableZones(client);
        // 创建实例
        createPostPaidInstance(client);
        // 修改实例
        updateInstance(client);
        // 查询实例
        showInstance(client);
        // 删除实例
        deleteInstance(client);
    }

    private static void createPostPaidInstance(RocketMQClient client) {
        try {
            CreatePostPaidInstanceRequest request = new CreatePostPaidInstanceRequest();
            CreatePostPaidInstanceReq body = new CreatePostPaidInstanceReq();
            /* 必要 */
            body.withName("<YOUR InstanceName>");
            body.withEngine(CreatePostPaidInstanceReq.EngineEnum.fromValue("reliability"));
            body.withEngineVersion(CreatePostPaidInstanceReq.EngineVersionEnum.fromValue("4.8.0"));
            body.withStorageSpace(600);
            // vpc、安全组、子网
            body.withVpcId("<YOUR VpcId>");
            body.withSubnetId("<YOUR SubnetId>");
            body.withSecurityGroupId("<YOUR SecurityGroupId>");
            // 可用区的id，通过ListAvailableZones获得
            List<String> listbodyAvailableZones = new ArrayList<>();
            listbodyAvailableZones.add("<AvailableZones>");
            body.withAvailableZones(listbodyAvailableZones);
            // 产品标识的id
            // c6.4u8g.cluster.small: 单个代理最大Topic数2000，单个代理最大消费组数2000
            // c6.4u8g.cluster：单个代理最大Topic数4000，单个代理最大消费组数4000
            // c6.8u16g.cluster：单个代理最大Topic数8000，单个代理最大消费组数8000
            // c6.12u24g.cluster：单个代理最大Topic数12000，单个代理最大消费组数12000
            // c6.16u32g.cluster：单个代理最大Topic数16000，单个代理最大消费组数16000
            body.withProductId(CreatePostPaidInstanceReq.ProductIdEnum.fromValue("c6.4u8g.cluster"));
            // dms.physical.storage.high.v2: 高IO类型磁盘
            // dms.physical.storage.ultra.v2: 超高IO类型磁盘
            body.withStorageSpecCode(CreatePostPaidInstanceReq.StorageSpecCodeEnum.fromValue("dms.physical.storage.high.v2"));
            /* 非必要 */
            body.setDescription("Description Info");
            body.withSslEnable(false);
            body.withIpv6Enable(false);
            body.setEnablePublicip(false);
            // 实例绑定的弹性IP地址的ID,enablePublicip为true必填
            body.setPublicipId("<YOUR Specifies the ID of the elastic IP address>");
            body.withBrokerNum(1);
            request.withBody(body);
            CreatePostPaidInstanceResponse response = client.createPostPaidInstance(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private static void showInstance(RocketMQClient client) {
        try {
            ShowInstanceRequest request = new ShowInstanceRequest();
            request.withInstanceId("<YOUR InstanceId>");
            ShowInstanceResponse response = client.showInstance(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private static void deleteInstance(RocketMQClient client) {
        try {
            DeleteInstanceRequest request = new DeleteInstanceRequest();
            request.withInstanceId("<YOUR InstanceId>");
            DeleteInstanceResponse response = client.deleteInstance(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private static void updateInstance(RocketMQClient client) {
        try {
            UpdateInstanceRequest request = new UpdateInstanceRequest();
            request.withInstanceId("<YOUR InstanceId>");
            UpdateInstanceReq body = new UpdateInstanceReq();
            body.withName("<YOUR InstanceName>");
            body.withDescription("Description");
            body.withSecurityGroupId("<YOUR SecurityGroupId>");
            // ACL访问控制。
            body.withRetentionPolicy(false);
            request.withBody(body);
            UpdateInstanceResponse response = client.updateInstance(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private static void listAvailableZones(RocketMQClient client) {
        try {
            ListAvailableZonesRequest request = new ListAvailableZonesRequest();
            ListAvailableZonesResponse response = client.listAvailableZones(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
```
## 5. 参考
更多信息请参考[分布式消息服务RocketMQ版文档](https://support.huaweicloud.com/hrm/index.html)

### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: | :-----: | :------:  
 2023/10/18 |1.0 | 文档首次发布
