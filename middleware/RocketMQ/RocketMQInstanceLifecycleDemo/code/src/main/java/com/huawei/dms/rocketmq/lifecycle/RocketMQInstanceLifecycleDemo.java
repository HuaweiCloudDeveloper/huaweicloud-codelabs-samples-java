package com.huawei.dms.rocketmq.lifecycle;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.rocketmq.v2.RocketMQClient;
import com.huaweicloud.sdk.rocketmq.v2.model.BatchDeleteInstanceReq;
import com.huaweicloud.sdk.rocketmq.v2.model.BatchDeleteInstancesRequest;
import com.huaweicloud.sdk.rocketmq.v2.model.BatchDeleteInstancesResponse;
import com.huaweicloud.sdk.rocketmq.v2.model.CreateInstanceByEngineReq;
import com.huaweicloud.sdk.rocketmq.v2.model.CreateInstanceByEngineRequest;
import com.huaweicloud.sdk.rocketmq.v2.model.CreateInstanceByEngineResponse;
import com.huaweicloud.sdk.rocketmq.v2.model.ListAvailableZonesRequest;
import com.huaweicloud.sdk.rocketmq.v2.model.ListAvailableZonesResponse;
import com.huaweicloud.sdk.rocketmq.v2.model.ShowInstanceRequest;
import com.huaweicloud.sdk.rocketmq.v2.model.ShowInstanceResponse;
import com.huaweicloud.sdk.rocketmq.v2.model.UpdateInstanceReq;
import com.huaweicloud.sdk.rocketmq.v2.model.UpdateInstanceRequest;
import com.huaweicloud.sdk.rocketmq.v2.model.UpdateInstanceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class RocketMQInstanceLifecycleDemo {
    private static final Logger logger = LoggerFactory.getLogger(RocketMQInstanceLifecycleDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String projectId = "<YOUR Project Id>";
        String endpoint = "<YOUR Endpoint String>";
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);

        RocketMQClient client = RocketMQClient.newBuilder()
                .withCredential(auth)
                .withEndpoint(endpoint)
                .build();
        // 查询可用区
        listAvailableZones(client);
        // 创建实例
        createPostPaidInstance(client);
        // 修改实例
        updateInstance(client);
        // 查询实例
        showInstance(client);
        // 删除实例
        deleteInstance(client);
    }

    private static void createPostPaidInstance(RocketMQClient client) {
        try {
            List<String> availableZones = new ArrayList<>();
            availableZones.add("<AvailableZones>");

            CreateInstanceByEngineReq body = new CreateInstanceByEngineReq();
            body.withBrokerNum(1);
            // 实例绑定的弹性IP地址的ID,enablePublicip为true必填
            body.withPublicipId("<YOUR Specifies the ID of the elastic IP address>");
            body.withEnablePublicip(false);
            body.withIpv6Enable(false);
            body.withStorageSpecCode(CreateInstanceByEngineReq.StorageSpecCodeEnum.fromValue("dms.physical.storage.high.v2"));
            body.withSslEnable(false);
            body.withProductId(CreateInstanceByEngineReq.ProductIdEnum.fromValue("c6.4u8g.cluster"));
            body.withAvailableZones(availableZones);
            // vpc、安全组、子网
            body.withSecurityGroupId("<YOUR SecurityGroupId>");
            body.withSubnetId("<YOUR SubnetId>");
            body.withVpcId("<YOUR VpcId>");
            /* 必要 */
            body.withStorageSpace(600);
            body.withEngineVersion(CreateInstanceByEngineReq.EngineVersionEnum.fromValue("4.8.0"));
            body.withEngine(CreateInstanceByEngineReq.EngineEnum.fromValue("reliability"));
            body.withName("<YOUR InstanceName>");

            CreateInstanceByEngineRequest request = new CreateInstanceByEngineRequest();
            request.withEngine(CreateInstanceByEngineRequest.EngineEnum.fromValue("reliability"));
            request.withBody(body);

            CreateInstanceByEngineResponse response = client.createInstanceByEngine(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private static void showInstance(RocketMQClient client) {
        try {
            ShowInstanceRequest request = new ShowInstanceRequest();
            request.withInstanceId("<YOUR InstanceId>");

            ShowInstanceResponse response = client.showInstance(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private static void deleteInstance(RocketMQClient client) {
        try {
            List<String> instances = new ArrayList<>();
            instances.add("<YOUR InstanceId>");

            BatchDeleteInstanceReq body = new BatchDeleteInstanceReq();
            body.withAction(BatchDeleteInstanceReq.ActionEnum.fromValue("delete"));
            body.withInstances(instances);

            BatchDeleteInstancesRequest request = new BatchDeleteInstancesRequest();
            request.withBody(body);

            BatchDeleteInstancesResponse response = client.batchDeleteInstances(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private static void updateInstance(RocketMQClient client) {
        try {
            UpdateInstanceReq body = new UpdateInstanceReq();
            body.withName("<YOUR InstanceName>");
            body.withDescription("Description");
            body.withSecurityGroupId("<YOUR SecurityGroupId>");

            UpdateInstanceRequest request = new UpdateInstanceRequest();
            request.withInstanceId("<YOUR InstanceId>");
            request.withBody(body);

            UpdateInstanceResponse response = client.updateInstance(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private static void listAvailableZones(RocketMQClient client) {
        try {
            ListAvailableZonesRequest request = new ListAvailableZonesRequest();
            ListAvailableZonesResponse response = client.listAvailableZones(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}