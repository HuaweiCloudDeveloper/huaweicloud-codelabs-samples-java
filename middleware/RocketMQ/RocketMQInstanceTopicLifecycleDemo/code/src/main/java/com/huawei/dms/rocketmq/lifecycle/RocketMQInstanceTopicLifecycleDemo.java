package com.huawei.dms.rocketmq.lifecycle;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.rocketmq.v2.RocketMQClient;
import com.huaweicloud.sdk.rocketmq.v2.model.CreateTopicOrBatchDeleteTopicReq;
import com.huaweicloud.sdk.rocketmq.v2.model.CreateTopicOrBatchDeleteTopicRequest;
import com.huaweicloud.sdk.rocketmq.v2.model.CreateTopicOrBatchDeleteTopicResponse;
import com.huaweicloud.sdk.rocketmq.v2.model.DeleteTopicRequest;
import com.huaweicloud.sdk.rocketmq.v2.model.DeleteTopicResponse;
import com.huaweicloud.sdk.rocketmq.v2.model.ListRocketInstanceTopicsRequest;
import com.huaweicloud.sdk.rocketmq.v2.model.ListRocketInstanceTopicsResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RocketMQInstanceTopicLifecycleDemo {
    private static final Logger logger = LoggerFactory.getLogger(RocketMQInstanceTopicLifecycleDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String projectId = "<YOUR Project Id>";
        String endpoint = "<YOUR Endpoint String>";
        ICredential auth = new BasicCredentials()
                .withAk(System.getenv("HUAWEICLOUD_SDK_AK"))
                .withSk(System.getenv("HUAWEICLOUD_SDK_SK"))
                .withProjectId(projectId);
        RocketMQClient client = RocketMQClient.newBuilder()
                .withCredential(auth)
                .withEndpoint(endpoint)
                .build();
        // 创建Topic
        createTopic(client);
        // 查询Topic列表
        listTopics(client);
        // 删除Topic
        deleteTopic(client);
    }

    private static void createTopic(RocketMQClient client) {
        try {
            List<String> brokers = new ArrayList<>();
            brokers.add("<YOUR BrokerName");

            CreateTopicOrBatchDeleteTopicReq body = new CreateTopicOrBatchDeleteTopicReq();
            body.setBrokers(brokers);
            body.setName("<YOUR TopicName>");
            body.setQueueNum(new BigDecimal(Integer.valueOf("<YOUR QueueNum>")));
            body.setPermission(CreateTopicOrBatchDeleteTopicReq.PermissionEnum.ALL);
            body.setMessageType(CreateTopicOrBatchDeleteTopicReq.MessageTypeEnum.NORMAL);

            CreateTopicOrBatchDeleteTopicRequest request = new CreateTopicOrBatchDeleteTopicRequest();
            request.setInstanceId("<YOUR InstanceId>");
            request.setBody(body);

            CreateTopicOrBatchDeleteTopicResponse response = client.createTopicOrBatchDeleteTopic(request);

            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private static void listTopics(RocketMQClient client) {
        try {
            ListRocketInstanceTopicsRequest request = new ListRocketInstanceTopicsRequest();
            request.setInstanceId("<YOUR InstanceId>");
            request.setOffset(1);
            request.setLimit(1);

            ListRocketInstanceTopicsResponse response = client.listRocketInstanceTopics(request);

            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private static void deleteTopic(RocketMQClient client) {
        try {
            DeleteTopicRequest request = new DeleteTopicRequest();
            request.setInstanceId("<YOUR InstanceId>");
            request.setTopic("<YOUR TopicName>");

            DeleteTopicResponse response = client.deleteTopic(request);

            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}