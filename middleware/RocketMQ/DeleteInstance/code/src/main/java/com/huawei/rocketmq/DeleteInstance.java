package com.huawei.rocketmq;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rocketmq.v2.RocketMQClient;
import com.huaweicloud.sdk.rocketmq.v2.model.DeleteInstanceRequest;
import com.huaweicloud.sdk.rocketmq.v2.model.DeleteInstanceResponse;
import com.huaweicloud.sdk.rocketmq.v2.region.RocketMQRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteInstance {
    private static final Logger logger = LoggerFactory.getLogger(DeleteInstance.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // The AK and SK used for authentication are directly written to the code, which has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Note that the following projectId is the ID of the corresponding DCS project code check task details link.
        String projectId = "<YOUR PROJECT Id>";

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to Region.CN_NORTH_4.
        RocketMQClient client = RocketMQClient.newBuilder()
            .withCredential(auth)
            .withHttpConfig(httpConfig)
            .withRegion(RocketMQRegion.CN_NORTH_4)
            .build();
        // Build Request
        DeleteInstanceRequest request = new DeleteInstanceRequest();
        request.withInstanceId("<YOUR INSTANCE Id>");

        try {
            // Send a request
            DeleteInstanceResponse response = client.deleteInstance(request);
            // Print Results
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error("HttpStatusCode: " + e.getMessage());
        }

    }
}