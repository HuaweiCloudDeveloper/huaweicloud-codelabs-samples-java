### 产品介绍

1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RocketMQ/doc?api=ListInstances)
中直接运行调试该接口。

2.在本样例中，您可以查询所有实例列表。

3.我们可以使用接口查询所有实例列表，用来自定义页面的实例列表展示。

### 前置条件

1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.当前登录账号拥有使用分布式消息服务RocketMQ的权限。

6.已在ROCKETMQ平台创建实例。

7.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-rocketmq”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rocketmq</artifactId>
    <version>3.1.121</version>
</dependency>

```

### 代码示例

``` java
package com.huawei.rocketmq;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rocketmq.v2.RocketMQClient;
import com.huaweicloud.sdk.rocketmq.v2.model.ListInstancesRequest;
import com.huaweicloud.sdk.rocketmq.v2.model.ListInstancesResponse;
import com.huaweicloud.sdk.rocketmq.v2.region.RocketMQRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListInstances {
    private static final Logger logger = LoggerFactory.getLogger(ListInstances.class.getName());

    public static void main(String[] args) {

        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 注意，如下的projectId 请使用对应DCS项目中代码检查任务详情链接中的ID
        String projectId = "<YOUR Project Id>";

        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为Region.CN_NORTH_4
        RocketMQClient client = RocketMQClient.newBuilder()
            .withCredential(auth)
            .withHttpConfig(httpConfig)
            .withRegion(RocketMQRegion.CN_NORTH_4)
            .build();
        // 构建请求
        ListInstancesRequest request = new ListInstancesRequest();
        request.withEngine(ListInstancesRequest.EngineEnum.ROCKETMQ);

        try {
            // 发送请求
            ListInstancesResponse response = client.listInstances(request);
            // 打印结果
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error("HttpStatusCode: " + e.getMessage());
        }

    }
}

```

### 返回结果示例

####

```
{
 "instances": [
  {
   "name": "rocketmq-**",
   "engine": "reliability",
   "status": "CREATING",
   "description": "",
   "type": "single.basic",
   "specification": "rocketmq.b1.large.1, 500 tps",
   "engine_version": "5.x",
   "instance_id": "e5b96d11-dc2e-4bd3-908***********",
   "resource_spec_code": "",
   "charging_mode": 1,
   "vpc_id": "9bc19799-5cb9-4777-afc1***********",
   "vpc_name": "vpc-***********",
   "created_at": "1731569237555",
   "product_id": "rocketmq.b1.large.1",
   "security_group_id": "c123f709-b76a-47b9-8daa***********",
   "security_group_name": "sg-wtc",
   "subnet_id": "5b5a21b5-c2ab-4618-8819-***********",
   "subnet_cidr": "192.168.0.0/16",
   "available_zones": [
    "effdcbc7d4d64a02aa1fa26***********"
   ],
   "available_zone_names": [
    "AZ1"
   ],
   "user_id": "990327b3121d42f7bc74***********",
   "user_name": "***********",
   "maintain_begin": "02:00:00",
   "maintain_end": "06:00:00",
   "enable_log_collection": false,
   "dns_enable": false,
   "storage_space": 100,
   "total_storage_space": 100,
   "used_storage_space": 0,
   "enable_publicip": false,
   "ssl_enable": true,
   "management_connect_domain_name": "",
   "public_management_connect_domain_name": "",
   "public_connect_domain_name": "",
   "storage_resource_id": "e8a583be-d06e-4ce***********",
   "storage_spec_code": "dms.physical.storage.general",
   "service_type": "advanced",
   "storage_type": "hec",
   "enterprise_project_id": "0",
   "extend_times": 0,
   "ipv6_enable": false,
   "support_features": "auto.create.topics.enable,rabbitmq.plugin.management,auto_topic_switch,feature.physerver.kafka.user.manager,kafka.new.pod.port,message_trace_enable,features.pod.token.access,log.enable,features.log.collection,max.connections,rabbitmq.manage.support,replica_port_standalone,feature.physerver.kafka.topic.accesspolicy,enable.kafka.quota.monitor,rocketmq.acl,roma_app_enable,support.kafka.producer.ip,enable.new.authinfo,enable.kafka.quota,rabbitmq_run_log_enable,max.ssl.connections,route,message_trace_v2_enable,kafka.config.dynamic.modify.enable,feature.physerver.kafka.topic.modify,enable.topic.quota,roma.user.manage.no.support,auto.create.groups.enable,feature.physerver.kafka.pulbic.dynamic,kafka.config.static.modify.enable,amqp.overview",
   "disk_encrypted": false,
   "ces_version": "linux,v1,v2,v3,v4",
   "new_spec_billing_enable": true,
   "enable_acl": true,
   "enable_elastic_tps": false,
   "dr_enable": false,
   "quota_address": "",
   "config_ssl_need_restart_process": false,
   "grpc_address": "",
   "grpc_domain_name": "",
   "produce_portion": 1,
   "consume_portion": 1,
   "max_msg_process_tps": 0,
   "tls_mode": "SSL"
  }
 ],
 "instance_num": 1
}
```

### 修订记录

    发布日期    | 文档版本 | 修订说明

:----------:|:----:| :------:  
2024/11/14 | 1.0 | 文档首次发布