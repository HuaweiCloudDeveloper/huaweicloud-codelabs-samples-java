### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/RocketMQ/doc?api=ListInstances).

2.In this example, you can query the list of all instances.

3.You can use the API to query the list of all instances to customize the instance list display on the page.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The current account has the permission to use ROCKETMQ.

6.An instance has been created on ROCKETMQ.

7.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-rocketmq. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rocketmq</artifactId>
    <version>3.1.121</version>
</dependency>

```

### Code example

``` java
package com.huawei.rocketmq;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rocketmq.v2.RocketMQClient;
import com.huaweicloud.sdk.rocketmq.v2.model.ListInstancesRequest;
import com.huaweicloud.sdk.rocketmq.v2.model.ListInstancesResponse;
import com.huaweicloud.sdk.rocketmq.v2.region.RocketMQRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListInstances {
    private static final Logger logger = LoggerFactory.getLogger(ListInstances.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // The AK and SK used for authentication are directly written to the code, which has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to Region.CN_NORTH_4.
        RocketMQClient client = RocketMQClient.newBuilder()
            .withCredential(auth)
            .withHttpConfig(httpConfig)
            .withRegion(RocketMQRegion.CN_NORTH_4)
            .build();
        // Build Request
        ListInstancesRequest request = new ListInstancesRequest();
        request.withEngine(ListInstancesRequest.EngineEnum.ROCKETMQ);

        try {
            // Send a request
            ListInstancesResponse response = client.listInstances(request);
            // Print Results
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error("HttpStatusCode: " + e.getMessage());
        }

    }
}
```

### Example of the returned result

####

```
{
 "instances": [
  {
   "name": "rocketmq-**",
   "engine": "reliability",
   "status": "CREATING",
   "description": "",
   "type": "single.basic",
   "specification": "rocketmq.b1.large.1, 500 tps",
   "engine_version": "5.x",
   "instance_id": "e5b96d11-dc2e-4bd3-908***********",
   "resource_spec_code": "",
   "charging_mode": 1,
   "vpc_id": "9bc19799-5cb9-4777-afc1***********",
   "vpc_name": "vpc-***********",
   "created_at": "1731569237555",
   "product_id": "rocketmq.b1.large.1",
   "security_group_id": "c123f709-b76a-47b9-8daa***********",
   "security_group_name": "sg-wtc",
   "subnet_id": "5b5a21b5-c2ab-4618-8819-***********",
   "subnet_cidr": "192.168.0.0/16",
   "available_zones": [
    "effdcbc7d4d64a02aa1fa26***********"
   ],
   "available_zone_names": [
    "AZ1"
   ],
   "user_id": "990327b3121d42f7bc74***********",
   "user_name": "***********",
   "maintain_begin": "02:00:00",
   "maintain_end": "06:00:00",
   "enable_log_collection": false,
   "dns_enable": false,
   "storage_space": 100,
   "total_storage_space": 100,
   "used_storage_space": 0,
   "enable_publicip": false,
   "ssl_enable": true,
   "management_connect_domain_name": "",
   "public_management_connect_domain_name": "",
   "public_connect_domain_name": "",
   "storage_resource_id": "e8a583be-d06e-4ce***********",
   "storage_spec_code": "dms.physical.storage.general",
   "service_type": "advanced",
   "storage_type": "hec",
   "enterprise_project_id": "0",
   "extend_times": 0,
   "ipv6_enable": false,
   "support_features": "auto.create.topics.enable,rabbitmq.plugin.management,auto_topic_switch,feature.physerver.kafka.user.manager,kafka.new.pod.port,message_trace_enable,features.pod.token.access,log.enable,features.log.collection,max.connections,rabbitmq.manage.support,replica_port_standalone,feature.physerver.kafka.topic.accesspolicy,enable.kafka.quota.monitor,rocketmq.acl,roma_app_enable,support.kafka.producer.ip,enable.new.authinfo,enable.kafka.quota,rabbitmq_run_log_enable,max.ssl.connections,route,message_trace_v2_enable,kafka.config.dynamic.modify.enable,feature.physerver.kafka.topic.modify,enable.topic.quota,roma.user.manage.no.support,auto.create.groups.enable,feature.physerver.kafka.pulbic.dynamic,kafka.config.static.modify.enable,amqp.overview",
   "disk_encrypted": false,
   "ces_version": "linux,v1,v2,v3,v4",
   "new_spec_billing_enable": true,
   "enable_acl": true,
   "enable_elastic_tps": false,
   "dr_enable": false,
   "quota_address": "",
   "config_ssl_need_restart_process": false,
   "grpc_address": "",
   "grpc_domain_name": "",
   "produce_portion": 1,
   "consume_portion": 1,
   "max_msg_process_tps": 0,
   "tls_mode": "SSL"
  }
 ],
 "instance_num": 1
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/11/14 | 1.0 | This document is released for the first time.