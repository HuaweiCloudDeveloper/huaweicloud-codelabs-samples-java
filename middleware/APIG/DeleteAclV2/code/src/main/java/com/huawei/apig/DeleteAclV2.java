package com.huawei.apig;

import com.huaweicloud.sdk.apig.v2.ApigClient;
import com.huaweicloud.sdk.apig.v2.model.DeleteAclV2Request;
import com.huaweicloud.sdk.apig.v2.model.DeleteAclV2Response;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.apig.v2.region.ApigRegion;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteAclV2 {

    private static final Logger logger = LoggerFactory.getLogger(DeleteAclV2.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String deleteAclV2Ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String deleteAclV2Sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(deleteAclV2Ak)
                .withSk(deleteAclV2Sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to ApigRegion.CN_NORTH_4.
        ApigClient client = ApigClient.newBuilder()
                .withCredential(auth)
                .withRegion(ApigRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request and set the request parameter instance ID and ACL policy ID.
        DeleteAclV2Request request = new DeleteAclV2Request();
        // Specifies the instance ID, which can be obtained from the Instance Information page on the API Gateway console.
        String instanceId = "<YOUR INSTANCE ID>";
        request.withInstanceId(instanceId);
        // Indicates the ID of an ACL policy. You can use the ListAclStrategiesV2 interface to query the ACL policy list. The returned value contains the ACL policy ID.
        String aclId = "<YOUR ACL ID>";
        request.withAclId(aclId);
        try {
            DeleteAclV2Response response = client.deleteAclV2(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}