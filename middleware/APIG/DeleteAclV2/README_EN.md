### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/APIG/sdk?api=DeleteAclV2).

2.In this example, you can delete an ACL policy.

3.Deleting an ACL policy is to remove the configured ACL policy to cancel the access restriction on specific APIs. In this way, API access permissions can be easily adjusted to meet different business requirements. By deleting an ACL policy, you can dynamically manage and adjust API access permissions, improving system flexibility and security.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The current account has the permission to use APIG. You have created an APIG premium instance and created an ACL policy.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-apig. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-apig</artifactId>
    <version>3.1.121</version>
</dependency>

```

### Code example

``` java
package com.huawei.apig;

import com.huaweicloud.sdk.apig.v2.ApigClient;
import com.huaweicloud.sdk.apig.v2.model.DeleteAclV2Request;
import com.huaweicloud.sdk.apig.v2.model.DeleteAclV2Response;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.apig.v2.region.ApigRegion;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteAclV2 {

    private static final Logger logger = LoggerFactory.getLogger(DeleteAclV2.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String deleteAclV2Ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String deleteAclV2Sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(deleteAclV2Ak)
                .withSk(deleteAclV2Sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to ApigRegion.CN_NORTH_4.
        ApigClient client = ApigClient.newBuilder()
                .withCredential(auth)
                .withRegion(ApigRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request and set the request parameter instance ID and ACL policy ID.
        DeleteAclV2Request request = new DeleteAclV2Request();
        // Specifies the instance ID, which can be obtained from the Instance Information page on the API Gateway console.
        String instanceId = "<YOUR INSTANCE ID>";
        request.withInstanceId(instanceId);
        // Indicates the ID of an ACL policy. You can use the ListAclStrategiesV2 interface to query the ACL policy list. The returned value contains the ACL policy ID.
        String aclId = "<YOUR ACL ID>";
        request.withAclId(aclId);
        try {
            DeleteAclV2Response response = client.deleteAclV2(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result

#### DeleteAclV2

```
{
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/11/14 | 1.0 | This document is released for the first time.