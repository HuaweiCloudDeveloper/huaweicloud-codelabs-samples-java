## 1. 简介

华为云提供了API网关（APIG）的SDK，您可以直接集成SDK来调用APIG的相关API，从而实现对APIG的快速操作。
该示例展示了如何通过java版SDK来创建并发布一个API。

## 2. 前置条件
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。

## 3. 安装SDK
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

具体的SDK版本号请参见 [SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-apig</artifactId>
    <version>3.1.61</version>
</dependency>
```
## 4. 代码示例
以下代码展示如何使用SDK创建并发布一个API

### 4.1 初始化认证信息
``` java
// 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
// 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
String AK = System.getenv("HUAWEICLOUD_SDK_AK");
String SK = System.getenv("HUAWEICLOUD_SDK_SK");
ICredential auth = new BasicCredentials()
        .withAk(AK)
        .withSk(SK)
        .withProjectId(PROJECT_ID);
```

### 4.2 初始化Apig客户端
``` java
ApigClient client = ApigClient.newBuilder()
        .withHttpConfig(config)
        .withCredential(auth)
        .withRegion(ApigRegion.CN_NORTH_4)
        .build();
```

### 4.3 创建API
``` java
// 构造请求参数
CreateApiV2Request request = new CreateApiV2Request();
request.withInstanceId(INSTANCE_ID);
ApiCreate body = new ApiCreate();
body.withName("<YOUR API NAME>");
body.withGroupId("<YOUR API GROUP ID>");
body.withReqMethod(ApiCreate.ReqMethodEnum.GET);
body.withReqProtocol(ApiCreate.ReqProtocolEnum.HTTPS);
body.withAuthType(ApiCreate.AuthTypeEnum.NONE);
body.withReqUri("<YOUR REQUEST URI>");
body.withType(ApiCreate.TypeEnum.NUMBER_1);
body.withBackendType(ApiCreate.BackendTypeEnum.HTTP);
BackendApiCreate backendApibody = new BackendApiCreate();
backendApibody.withUrlDomain("<YOUR BACKEND URL DOMAIN>")
        .withReqProtocol(BackendApiCreate.ReqProtocolEnum.HTTPS)
        .withReqMethod(BackendApiCreate.ReqMethodEnum.GET)
        .withReqUri("<YOUR BACKEND REQUEST URI>")
        .withTimeout(30);
body.withBackendApi(backendApibody);
request.withBody(body);

// 调用接口，并接收响应参数
CreateApiV2Response response = client.createApiV2(request);
```

### 4.4 发布API
``` java
// 构造请求参数
BatchPublishOrOfflineApiV2Request request = new BatchPublishOrOfflineApiV2Request();
request.withInstanceId(INSTANCE_ID);
request.withAction(ApiActionInfo.ActionEnum.ONLINE.toString());
ApiBatchPublish body = new ApiBatchPublish();
List<String> listbodyApis = new ArrayList<>();
listbodyApis.add(apiId);
body.withEnvId("DEFAULT_ENVIRONMENT_RELEASE_ID");
body.withApis(listbodyApis);
request.withBody(body);

// 调用接口，并接收响应参数
BatchPublishOrOfflineApiV2Response response = client.batchPublishOrOfflineApiV2(request);
```

## 5. 返回结果示例

### 5.1 创建API返回结果示例
``` java
{
 "name": "demo_api",
 "type": 1,
 "version": "V0.0.1",
 "req_protocol": "HTTPS",
 "req_method": "GET",
 "req_uri": "/demo/uri",
 "auth_type": "NONE",
 "auth_opt": {
  "app_code_auth_type": "DISABLE"
 },
 "cors": false,
 "match_mode": "NORMAL",
 "backend_type": "HTTP",
 "remark": "",
 "group_id": "<your group id>",
 "body_remark": "",
 "result_normal_sample": "",
 "result_failure_sample": "",
 "authorizer_id": "",
 "tags": [],
 "tag": "",
 "roma_app_id": "",
 "domain_name": "",
 "id": "3262750cf3104fc695a6ffcf9cdaba0d",
 "status": 1,
 "arrange_necessary": 2,
 "register_time": "2023-11-03T09:58:57.012602342Z",
 "update_time": "2023-11-03T09:58:57.012602458Z",
 "group_name": "DEFAULT",
 "group_version": "V1",
 "run_env_name": "",
 "run_env_id": "",
 "publish_id": "",
 "publish_time": "",
 "roma_app_name": "",
 "ld_api_id": "",
 "api_group_info": {
  "id": "<your group id>",
  "name": "DEFAULT",
  "status": 1,
  "sl_domain": "<your group sl_domain>",
  "register_time": "2023-02-27T09:11:54Z",
  "update_time": "2023-02-27T09:11:54Z",
  "on_sell_status": 2,
  "url_domains": []
 },
 "mock_info": null,
 "func_info": null,
 "req_params": [],
 "backend_params": [],
 "policy_functions": null,
 "policy_mocks": null,
 "backend_api": {
  "url_domain": "demo.com",
  "version": "",
  "req_protocol": "HTTPS",
  "req_method": "GET",
  "req_uri": "/demo/backend/uri",
  "timeout": 30,
  "remark": "",
  "authorizer_id": "",
  "enable_client_ssl": false,
  "retry_count": "-1",
  "id": "8ab0d879741a428f914434c019b53398",
  "status": 1,
  "register_time": "2023-11-03T09:58:57.016091142Z",
  "update_time": "2023-11-03T09:58:57.016091249Z",
  "vpc_channel_status": 2,
  "vpc_channel_info": {
   "cascade_flag": false,
   "vpc_channel_id": "",
   "vpc_channel_port": 0
  }
 },
 "policy_https": []
}
```

### 5.2 发布API返回结果示例
``` java
{
 "success": [
  {
   "publish_id": "a8e012e3d03848e5b6e3c818a81f32ea",
   "api_id": "3262750cf3104fc695a6ffcf9cdaba0d",
   "api_name": "demo_api",
   "env_id": "DEFAULT_ENVIRONMENT_RELEASE_ID",
   "version_id": "faff6c4892d04785bfcc5730d461e443",
   "remark": "",
   "publish_time": "2023-11-03T10:00:16.892864085Z"
  }
 ],
 "failure": []
}
```

## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-11-03 | 1.0 | 文档首次发布|

## 7. 参考链接
API网关 APIG官方文档链接：https://support.huaweicloud.com/apig/index.html