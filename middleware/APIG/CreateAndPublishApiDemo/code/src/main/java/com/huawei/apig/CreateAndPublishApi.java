package com.huawei.apig;

import com.huaweicloud.sdk.apig.v2.ApigClient;
import com.huaweicloud.sdk.apig.v2.model.ApiActionInfo;
import com.huaweicloud.sdk.apig.v2.model.ApiBatchPublish;
import com.huaweicloud.sdk.apig.v2.model.ApiCreate;
import com.huaweicloud.sdk.apig.v2.model.BackendApiCreate;
import com.huaweicloud.sdk.apig.v2.model.BatchPublishOrOfflineApiV2Request;
import com.huaweicloud.sdk.apig.v2.model.BatchPublishOrOfflineApiV2Response;
import com.huaweicloud.sdk.apig.v2.model.CreateApiV2Request;
import com.huaweicloud.sdk.apig.v2.model.CreateApiV2Response;
import com.huaweicloud.sdk.apig.v2.region.ApigRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 创建并发布一个API的示例代码
 */
public class CreateAndPublishApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateAndPublishApi.class);

    /* 认证用的 ak 和 sk 硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    本示例以 ak 和 sk 保存在环境变量中为例，运行本示例前请先在本地环境中设置环境变量 HUAWEICLOUD_SDK_AK 和 HUAWEICLOUD_SDK_SK 。*/
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String PROJECT_ID = "<YOUR PROJECT ID>";
    private static final String INSTANCE_ID = "<YOUR INSTANCE ID>";

    public static void main(String[] args) {
        // 初始化认证信息
        ICredential auth = new BasicCredentials()
                .withAk(AK)
                .withSk(SK)
                .withProjectId(PROJECT_ID);

        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.setIgnoreSSLVerification(true);

        // 初始化Apig客户端
        ApigClient client = ApigClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(ApigRegion.CN_NORTH_4)
                .build();

        try {
            // 创建API，获取API ID
            String apiId = createApi(client);
            // 发布API
            publishApi(client, apiId);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    private static String createApi(ApigClient client) {
        // 构造请求参数
        CreateApiV2Request request = new CreateApiV2Request();
        request.withInstanceId(INSTANCE_ID);
        ApiCreate body = new ApiCreate();
        body.withName("<YOUR API NAME>");
        body.withGroupId("<YOUR API GROUP ID>");
        body.withReqMethod(ApiCreate.ReqMethodEnum.GET);
        body.withReqProtocol(ApiCreate.ReqProtocolEnum.HTTPS);
        body.withAuthType(ApiCreate.AuthTypeEnum.NONE);
        body.withReqUri("<YOUR REQUEST URI>");
        body.withType(ApiCreate.TypeEnum.NUMBER_1);
        body.withBackendType(ApiCreate.BackendTypeEnum.HTTP);
        BackendApiCreate backendApibody = new BackendApiCreate();
        backendApibody.withUrlDomain("<YOUR BACKEND URL DOMAIN>")
                .withReqProtocol(BackendApiCreate.ReqProtocolEnum.HTTPS)
                .withReqMethod(BackendApiCreate.ReqMethodEnum.GET)
                .withReqUri("<YOUR BACKEND REQUEST URI>")
                .withTimeout(30);
        body.withBackendApi(backendApibody);
        request.withBody(body);

        // 调用接口，并接收响应参数
        CreateApiV2Response response = client.createApiV2(request);
        LOGGER.info(response.toString());

        // 返回API ID
        return response.getId();
    }

    private static void publishApi(ApigClient client, String apiId) {
        // 构造请求参数
        BatchPublishOrOfflineApiV2Request request = new BatchPublishOrOfflineApiV2Request();
        request.withInstanceId(INSTANCE_ID);
        request.withAction(ApiActionInfo.ActionEnum.ONLINE.toString());
        ApiBatchPublish body = new ApiBatchPublish();
        List<String> listbodyApis = new ArrayList<>();
        listbodyApis.add(apiId);
        body.withEnvId("DEFAULT_ENVIRONMENT_RELEASE_ID");
        body.withApis(listbodyApis);
        request.withBody(body);

        // 调用接口，并接收响应参数
        BatchPublishOrOfflineApiV2Response response = client.batchPublishOrOfflineApiV2(request);
        LOGGER.info(response.toString());
    }
}
