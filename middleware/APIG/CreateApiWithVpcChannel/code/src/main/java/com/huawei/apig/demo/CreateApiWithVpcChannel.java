package com.huawei.apig.demo;

import com.huaweicloud.sdk.apig.v2.ApigClient;
import com.huaweicloud.sdk.apig.v2.model.ApiActionInfo;
import com.huaweicloud.sdk.apig.v2.model.ApiBackendVpcReq;
import com.huaweicloud.sdk.apig.v2.model.ApiCreate;
import com.huaweicloud.sdk.apig.v2.model.ApiGroupCreate;
import com.huaweicloud.sdk.apig.v2.model.AssociateDomainV2Request;
import com.huaweicloud.sdk.apig.v2.model.AssociateDomainV2Response;
import com.huaweicloud.sdk.apig.v2.model.BackendApiCreate;
import com.huaweicloud.sdk.apig.v2.model.CreateApiGroupV2Request;
import com.huaweicloud.sdk.apig.v2.model.CreateApiGroupV2Response;
import com.huaweicloud.sdk.apig.v2.model.CreateApiV2Request;
import com.huaweicloud.sdk.apig.v2.model.CreateApiV2Response;
import com.huaweicloud.sdk.apig.v2.model.CreateOrDeletePublishRecordForApiV2Request;
import com.huaweicloud.sdk.apig.v2.model.CreateOrDeletePublishRecordForApiV2Response;
import com.huaweicloud.sdk.apig.v2.model.CreateVpcChannelV2Request;
import com.huaweicloud.sdk.apig.v2.model.CreateVpcChannelV2Response;
import com.huaweicloud.sdk.apig.v2.model.UrlDomainCreate;
import com.huaweicloud.sdk.apig.v2.model.VpcCreate;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateApiWithVpcChannel {
    private static final Logger logger = LoggerFactory.getLogger(CreateApiWithVpcChannel.class.getName());
    private static final String INSTANCE_ID = "<YOUR INSTANCE ID>";
    private static final String RELEASE_ENV_ID = "DEFAULT_ENVIRONMENT_RELEASE_ID";

    public static void main(String[] args) {

        // Hardcoded or plaintext AK/SK is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "<YOUR PROJECT ID>";
        String apigEndpoint = "<APIG ENDPOINT>";
        BasicCredentials auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.setIgnoreSSLVerification(true);
        ApigClient apigClient = ApigClient.newBuilder()
                .withHttpConfig(config).withCredential(auth).withEndpoint(apigEndpoint).build();

        try {
            String groupId = createApiGroup(apigClient);

            String vpcChannelId = createVpc(apigClient);

            String apiId = createApi(apigClient, groupId, vpcChannelId);

            publishApi(apigClient, apiId);

            associateUrlDomain(apigClient, groupId);
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static String createApiGroup(ApigClient apigClient) {
        // Construct request parameters.
        CreateApiGroupV2Request request = new CreateApiGroupV2Request();
        request.setInstanceId(INSTANCE_ID);
        ApiGroupCreate body = new ApiGroupCreate();
        body.setName("<YOUR API GROUP NAME>");
        request.setBody(body);

        // Receive response parameters.
        CreateApiGroupV2Response response = apigClient.createApiGroupV2(request);
        String groupId = response.getId();
        logger.info(groupId);
        return groupId;
    }

    private static String createVpc(ApigClient apigClient) {
        // Construct request parameters.
        CreateVpcChannelV2Request request = new CreateVpcChannelV2Request();
        request.setInstanceId(INSTANCE_ID);
        VpcCreate body = new VpcCreate();
        body.setName("<YOUR VPC CHANNEL NAME>");
        body.setPort(8080);
        request.setBody(body);

        // Receive response parameters.
        CreateVpcChannelV2Response response = apigClient.createVpcChannelV2(request);
        String vpcChannelId = response.getId();
        logger.info(vpcChannelId);
        return vpcChannelId;
    }

    private static String createApi(ApigClient apigClient, String apiGroupId, String vpcChannelId) {
        // Construct request parameters.
        CreateApiV2Request request = new CreateApiV2Request();
        request.setInstanceId(INSTANCE_ID);
        ApiCreate body = new ApiCreate();
        body.setName("<YOUR API NAME>");
        body.setGroupId(apiGroupId);
        body.setType(ApiCreate.TypeEnum.NUMBER_1);
        body.setAuthType(ApiCreate.AuthTypeEnum.APP);
        body.setReqMethod(ApiCreate.ReqMethodEnum.GET);
        body.setReqProtocol(ApiCreate.ReqProtocolEnum.HTTPS);
        body.setReqUri("<YOUR REQUEST URI>");
        body.setBackendType(ApiCreate.BackendTypeEnum.HTTP);
        BackendApiCreate backendApiCreate = new BackendApiCreate();
        backendApiCreate.setReqMethod(BackendApiCreate.ReqMethodEnum.GET);
        backendApiCreate.setReqProtocol(BackendApiCreate.ReqProtocolEnum.HTTPS);
        backendApiCreate.setTimeout(30);
        backendApiCreate.setReqUri("<YOUR BACKEND REQUEST URI>");
        backendApiCreate.setVpcChannelStatus(BackendApiCreate.VpcChannelStatusEnum.NUMBER_1);
        ApiBackendVpcReq apiBackendVpcReq = new ApiBackendVpcReq();
        apiBackendVpcReq.setVpcChannelId(vpcChannelId);
        backendApiCreate.setVpcChannelInfo(apiBackendVpcReq);
        body.setBackendApi(backendApiCreate);
        request.setBody(body);

        // Receive response parameters.
        CreateApiV2Response response = apigClient.createApiV2(request);
        String apiId = response.getId();
        logger.info(apiId);
        return apiId;
    }

    private static void publishApi(ApigClient apigClient, String apiId) {
        // Construct request parameters.
        CreateOrDeletePublishRecordForApiV2Request request = new CreateOrDeletePublishRecordForApiV2Request();
        request.setInstanceId(INSTANCE_ID);
        ApiActionInfo body = new ApiActionInfo();
        body.setApiId(apiId);
        body.setAction(ApiActionInfo.ActionEnum.ONLINE);
        body.setEnvId(RELEASE_ENV_ID);
        request.setBody(body);

        // Receive response parameters.
        CreateOrDeletePublishRecordForApiV2Response response = apigClient.createOrDeletePublishRecordForApiV2(request);
        logger.info(response.toString());
    }

    private static void associateUrlDomain(ApigClient apigClient, String apiGroupId) {
        // Construct request parameters.
        AssociateDomainV2Request request = new AssociateDomainV2Request();
        request.setInstanceId(INSTANCE_ID);
        request.setGroupId(apiGroupId);
        UrlDomainCreate body = new UrlDomainCreate();
        body.setUrlDomain("<YOUR URL DOMAIN>");
        request.setBody(body);

        // Receive response parameters.
        AssociateDomainV2Response response = apigClient.associateDomainV2(request);
        logger.info(response.toString());
    }
}