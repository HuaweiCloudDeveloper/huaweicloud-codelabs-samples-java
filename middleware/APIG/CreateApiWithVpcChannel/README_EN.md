# Example of Opening a Backend Service with a Load Balance Channel in APIG (Java)
## 0. Version
This example is developed based on Huawei Cloud SDK V3.0.

## 1. Introduction
This is sample code of using Huawei Cloud Java SDK to open a backend service with a load balance channel in API Gateway (APIG). The sample code shows how to create an API group, create a load balance channel, bind an independent domain name, create and publish an API.

## 2. Preparations
- You have [signed up](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html?lang=en-us) to Huawei Cloud and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?locale=en-us#/accountindex/realNameAuth).
- You have set up the development environment with Java JDK 1.8 or later.
- You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. To create and view an AK/SK, go to the My Credentials > Access Keys page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0001.html).
- You have obtained the project ID of the target region. View the project ID on the My Credentials > API Credentials page of the Huawei Cloud console. For details, see [API Credentials](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0002.html).

## 3. Installing an SDK
You can obtain and install an SDK in Maven mode. Download and install Maven in your operating system (OS). After the installation is complete, add the corresponding dependencies to the pom.xml file of the Java project.

Before using the server SDK, install huaweicloud-sdk-core and huaweicloud-sdk-apig. For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>3.0.69</version>
</dependency>
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-apig</artifactId>
    <version>3.0.69</version>
</dependency>
```

## 4. Getting Started
### 4.1 Importing Dependencies
```java
// User authentication
import com.huaweicloud.sdk.core.auth.BasicCredentials;
// Request exception classes
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
// APIG client
import com.huaweicloud.sdk.apig.v2.ApigClient;
// Request and response classes for creating API groups
import com.huaweicloud.sdk.apig.v2.model.ApiGroupCreate;
import com.huaweicloud.sdk.apig.v2.model.CreateApiGroupV2Request;
import com.huaweicloud.sdk.apig.v2.model.CreateApiGroupV2Response;
// Request and response classes for creating APIs
import com.huaweicloud.sdk.apig.v2.model.ApiCreate;
import com.huaweicloud.sdk.apig.v2.model.BackendApiCreate;
import com.huaweicloud.sdk.apig.v2.model.CreateApiV2Request;
import com.huaweicloud.sdk.apig.v2.model.CreateApiV2Response;
// Request and response classes for publishing APIs
import com.huaweicloud.sdk.apig.v2.model.ApiActionInfo;
import com.huaweicloud.sdk.apig.v2.model.CreateOrDeletePublishRecordForApiV2Request;
import com.huaweicloud.sdk.apig.v2.model.CreateOrDeletePublishRecordForApiV2Response;
// Request and response classes for binding independent domain names
import com.huaweicloud.sdk.apig.v2.model.AssociateDomainV2Request;
import com.huaweicloud.sdk.apig.v2.model.AssociateDomainV2Response;
import com.huaweicloud.sdk.apig.v2.model.UrlDomainCreate;
// Log printing
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
```

### 4.2 Initializing Authentication Information
``` java
// Hardcoded or plaintext AK/SK is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
// In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
String ak = System.getenv("HUAWEICLOUD_SDK_AK");
String sk = System.getenv("HUAWEICLOUD_SDK_SK");
BasicCredentials auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);
```

### 4.3 Initializing ApigClient
```java
HttpConfig config = HttpConfig.getDefaultHttpConfig();
config.setIgnoreSSLVerification(true);
ApigClient apigClient = ApigClient.newBuilder()
        .withHttpConfig(config).withCredential(auth).withEndpoint(apigEndpoint).build();
```

### 4.4 Creating an API Group
#### 4.4.1 Sample Code
```java
try {
    // Construct request parameters.
    CreateApiGroupV2Request request = new CreateApiGroupV2Request();
    request.setInstanceId("<YOUR INSTANCE ID>");
    ApiGroupCreate body = new ApiGroupCreate();
    body.setName("<YOUR API GROUP NAME>");
    request.setBody(body);

    // Receive response parameters.
    CreateApiGroupV2Response response = apigClient.createApiGroupV2(request);
    String groupId = response.getId();
    logger.info(groupId);
} catch (ClientRequestException e) {
    logger.error(String.valueOf(e.getHttpStatusCode()));
    logger.error(e.toString());
} catch (ServerResponseException e) {
    logger.error(String.valueOf(e.getHttpStatusCode()));
    logger.error(e.getMessage());
}
```

### 4.5 Creating a Load Balance Channel
#### 4.5.1 Sample Code
```java
try {
    // Construct request parameters.
    CreateVpcChannelV2Request request = new CreateVpcChannelV2Request();
    request.setInstanceId("<YOUR INSTANCE ID>");
    VpcCreate body = new VpcCreate();
    body.setName("<YOUR VPC CHANNEL NAME>");
    body.setPort(8080);
    request.setBody(body);

    // Receive response parameters.
    CreateVpcChannelV2Response response = apigClient.createVpcChannelV2(request);
    String vpcChannelId = response.getId();
    logger.info(vpcChannelId);
} catch (ClientRequestException e) {
    logger.error(String.valueOf(e.getHttpStatusCode()));
    logger.error(e.toString());
} catch (ServerResponseException e) {
    logger.error(String.valueOf(e.getHttpStatusCode()));
    logger.error(e.getMessage());
}
```

### 4.6 Creating an API
#### 4.6.1 Sample Code
```java
try {
    // Construct request parameters.
    CreateApiV2Request request = new CreateApiV2Request();
    request.setInstanceId("<YOUR INSTANCE ID>");
    ApiCreate body = new ApiCreate();
    body.setName("<YOUR API NAME>");
    body.setGroupId("<YOUR GROUP ID>");
    body.setType(ApiCreate.TypeEnum.NUMBER_1);
    body.setAuthType(ApiCreate.AuthTypeEnum.APP);
    body.setReqMethod(ApiCreate.ReqMethodEnum.GET);
    body.setReqProtocol(ApiCreate.ReqProtocolEnum.HTTPS);
    body.setReqUri("<YOUR REQUEST URI>");
    body.setBackendType(ApiCreate.BackendTypeEnum.HTTP);
    BackendApiCreate backendApiCreate = new BackendApiCreate();
    backendApiCreate.setReqMethod(BackendApiCreate.ReqMethodEnum.GET);
    backendApiCreate.setReqProtocol(BackendApiCreate.ReqProtocolEnum.HTTPS);
    backendApiCreate.setTimeout(30);
    backendApiCreate.setReqUri("<YOUR BACKEND REQUEST URI>");
    backendApiCreate.setVpcChannelStatus(BackendApiCreate.VpcChannelStatusEnum.NUMBER_1);
    ApiBackendVpcReq apiBackendVpcReq = new ApiBackendVpcReq();
    apiBackendVpcReq.setVpcChannelId("<YOUR VPC CHANNEL ID>");
    backendApiCreate.setVpcChannelInfo(apiBackendVpcReq);
    body.setBackendApi(backendApiCreate);
    request.setBody(body);

    // Receive response parameters.
    CreateApiV2Response response = apigClient.createApiV2(request);
    String apiId = response.getId();
    logger.info(apiId);
} catch (ClientRequestException e) {
    logger.error(String.valueOf(e.getHttpStatusCode()));
    logger.error(e.toString());
} catch (ServerResponseException e) {
    logger.error(String.valueOf(e.getHttpStatusCode()));
    logger.error(e.getMessage());
}
```

### 4.7 Publishing an API
#### 4.7.1 Sample Code
```java
try {
    // Construct request parameters.
    CreateOrDeletePublishRecordForApiV2Request request = new CreateOrDeletePublishRecordForApiV2Request();
    request.setInstanceId("<YOUR INSTANCE ID>");
    ApiActionInfo body = new ApiActionInfo();
    body.setApiId("<YOUR API ID>");
    body.setAction(ApiActionInfo.ActionEnum.ONLINE);
    body.setEnvId("DEFAULT_ENVIRONMENT_RELEASE_ID");
    request.setBody(body);

    // Receive response parameters.
    CreateOrDeletePublishRecordForApiV2Response response = apigClient.createOrDeletePublishRecordForApiV2(request);
    logger.info(response.toString());
} catch (ClientRequestException e) {
    logger.error(String.valueOf(e.getHttpStatusCode()));
    logger.error(e.toString());
} catch (ServerResponseException e) {
    logger.error(String.valueOf(e.getHttpStatusCode()));
    logger.error(e.getMessage());
}
```

### 4.8 Binding an Independent Domain Name
#### 4.8.1 Sample Code
```java
try {
    // Construct request parameters.
    AssociateDomainV2Request request = new AssociateDomainV2Request();
    request.setInstanceId("<YOUR INSTANCE ID>");
    request.setGroupId("<YOUR GROUP ID>");
    UrlDomainCreate body = new UrlDomainCreate();
    body.setUrlDomain("<YOUR URL DOMAIN>");
    request.setBody(body);

    // Receive response parameters.
    AssociateDomainV2Response response = apigClient.associateDomainV2(request);
    logger.info(response.toString());
} catch (ClientRequestException e) {
    logger.error(String.valueOf(e.getHttpStatusCode()));
    logger.error(e.toString());
} catch (ServerResponseException e) {
    logger.error(String.valueOf(e.getHttpStatusCode()));
    logger.error(e.getMessage());
}
```

## 5. FAQ
None

## 6. Reference
For details, see the [APIG documentation](https://support.huaweicloud.com/intl/en-us/apig/index.html).

## 7. Change History

|    Date     | Version |   Description   |
|:-----------:| :------: | :----------: |
| Dec 2, 2021 |   1.0    | First release |
|  Mar, 2024  | 2.0 | English version |

