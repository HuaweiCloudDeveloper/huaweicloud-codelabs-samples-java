### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/APIG/debug?api=CreateAclStrategyV2) 中直接运行调试该接口。

2.在本样例中，您可以创建ACL策略。

3.创建ACL策略可以用来限制对API的访问，确保只有经过授权的用户或应用程序可以使用API。它可以帮助你保护API免受未经授权的访问、恶意攻击或滥用。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.当前登录账号拥有使用APIG服务的权限，已经创建了APIG专享版实例。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-apig”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-apig</artifactId>
    <version>3.1.121</version>
</dependency>
```

### 代码示例
``` java
public class CreateAclStrategyV2 {

    private static final Logger logger = LoggerFactory.getLogger(CreateAclStrategyV2.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String createAclStrategyV2Ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String createAclStrategyV2Sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(createAclStrategyV2Ak)
                .withSk(createAclStrategyV2Sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为ApigRegion.CN_NORTH_4
        ApigClient client = ApigClient.newBuilder()
                .withCredential(auth)
                .withRegion(ApigRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();

        // 构建请求，设置请求参数实例ID，ACL策略名称，类型，ACL策略值，对象类型
        CreateAclStrategyV2Request request = new CreateAclStrategyV2Request();
        // 实例ID，在API网关控制台的“实例信息”中获取。
        request.withInstanceId("<YOUR INSTANCE ID>");
        ApiAclCreate body = new ApiAclCreate();
        // 对象类型。IP：IP地址，DOMAIN：账号名，DOMAIN_ID：账号ID
        body.withEntityType(ApiAclCreate.EntityTypeEnum.fromValue("IP"));
        // ACL策略值，支持一个或多个值，使用英文半角逗号分隔。
        // entity_type为IP时，策略值需填写IP地址。
        // entity_type为DOMAIN时，策略值需填写账号名，账号支持除英文半角逗号以外的任意ASCII字符，账号名长度限制在1-64个字符，不支持纯数字。多账号名字符的总长度不超过1024。
        // entity_type为DOMAIN_ID时，策略值需填写账号ID，获取方式请参见API参考的“附录 > 获取账号ID”章节。
        body.withAclValue("<IP地址:例如：192.168.1.12>");
        // 类型。PERMIT (白名单类型)，DENY (黑名单类型)
        body.withAclType(ApiAclCreate.AclTypeEnum.fromValue("PERMIT"));
        // ACL策略名称。支持汉字，英文，数字，下划线，且只能以英文和汉字开头，3 ~ 64字符。
        // 中文字符必须为UTF-8或者unicode编码。
        body.withAclName("<YOUR ACL NAME>");
        request.withBody(body);
        try {
            CreateAclStrategyV2Response response = client.createAclStrategyV2(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例
```
{
 "id": "43ae****",
 "acl_name": "testacl",
 "entity_type": "IP",
 "acl_type": "PERMIT",
 "acl_value": "192.168.1.12",
 "update_time": "2024-11-13T03:38:34.595792485Z"
}
```
### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-11-13 | 1.0 | 文档首次发布 |