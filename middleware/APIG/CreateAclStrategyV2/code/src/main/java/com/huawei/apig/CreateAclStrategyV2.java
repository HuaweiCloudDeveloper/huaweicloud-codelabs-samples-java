package com.huawei.apig;

import com.huaweicloud.sdk.apig.v2.ApigClient;
import com.huaweicloud.sdk.apig.v2.model.ApiAclCreate;
import com.huaweicloud.sdk.apig.v2.model.CreateAclStrategyV2Request;
import com.huaweicloud.sdk.apig.v2.model.CreateAclStrategyV2Response;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.apig.v2.region.ApigRegion;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateAclStrategyV2 {

    private static final Logger logger = LoggerFactory.getLogger(CreateAclStrategyV2.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String createAclStrategyV2Ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String createAclStrategyV2Sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(createAclStrategyV2Ak)
                .withSk(createAclStrategyV2Sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to ApigRegion.CN_NORTH_4.
        ApigClient client = ApigClient.newBuilder()
                .withCredential(auth)
                .withRegion(ApigRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();

        // Construct a request and set the request parameters instance ID, ACL policy name, type, ACL policy value, and object type.
        CreateAclStrategyV2Request request = new CreateAclStrategyV2Request();
        // Specifies the instance ID, which can be obtained from the Instance Information page on the API Gateway console.
        request.withInstanceId("<YOUR INSTANCE ID>");
        ApiAclCreate body = new ApiAclCreate();
        // Indicates the object type. The options are as follows:IP:IP addressDomain:account nameDomain_ID:account ID
        body.withEntityType(ApiAclCreate.EntityTypeEnum.fromValue("IP"));
        // ACL policy value. One or more values are supported and separated by commas (,).
        // If entity_type is set to IP, enter the IP address in the policy value.
        // When entity_type is set to Domain, enter the account name in the policy value. The account name can contain 1 to 64 characters except commas (,). Only digits are supported. The total length of the multi-account name cannot exceed 1024 characters.
        // If entity_type is set to Domain_ID, enter the account ID in the policy value. For details about how to obtain the account ID, see Appendix > Obtaining the Account ID in the API Reference.
        body.withAclValue("<IP address: For example: 192.168.1.12>");
        // Indicates the type. The options are as follows: PERMIT (whitelist type) DENY (blacklist type)
        body.withAclType(ApiAclCreate.AclTypeEnum.fromValue("PERMIT"));
        // Specifies the name of an ACL policy. The value can contain 3 to 64 characters, including letters, digits, and underscores (_). The value must start with a letter or a Chinese character.
        // Chinese characters must be in UTF-8 or Unicode format.
        body.withAclName("<YOUR ACL NAME>");
        request.withBody(body);
        try {
            CreateAclStrategyV2Response response = client.createAclStrategyV2(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}