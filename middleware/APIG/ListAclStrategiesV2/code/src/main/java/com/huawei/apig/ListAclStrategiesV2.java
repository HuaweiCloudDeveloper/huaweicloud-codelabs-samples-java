package com.huawei.apig;

import com.huaweicloud.sdk.apig.v2.ApigClient;
import com.huaweicloud.sdk.apig.v2.model.ListAclStrategiesV2Request;
import com.huaweicloud.sdk.apig.v2.model.ListAclStrategiesV2Response;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.apig.v2.region.ApigRegion;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListAclStrategiesV2 {

    private static final Logger logger = LoggerFactory.getLogger(ListAclStrategiesV2.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listAclStrategiesV2Ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String listAclStrategiesV2Sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listAclStrategiesV2Ak)
                .withSk(listAclStrategiesV2Sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to ApigRegion.CN_NORTH_4.
        ApigClient client = ApigClient.newBuilder()
                .withCredential(auth)
                .withRegion(ApigRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request and set the request parameter instance ID.
        ListAclStrategiesV2Request request = new ListAclStrategiesV2Request();
        // Specifies the instance ID, which can be obtained from the Instance Information page on the API Gateway console.
        String instanceId = "<YOUR INSTANCE ID>";
        request.withInstanceId(instanceId);
        try {
            ListAclStrategiesV2Response response = client.listAclStrategiesV2(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}