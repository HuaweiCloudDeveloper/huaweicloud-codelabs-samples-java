package com.huawei.apig.demo;

import com.huaweicloud.sdk.apig.v2.ApigClient;
import com.huaweicloud.sdk.apig.v2.model.ApiAuthCreate;
import com.huaweicloud.sdk.apig.v2.model.AppCreate;
import com.huaweicloud.sdk.apig.v2.model.CreateAnAppV2Request;
import com.huaweicloud.sdk.apig.v2.model.CreateAnAppV2Response;
import com.huaweicloud.sdk.apig.v2.model.CreateAuthorizingAppsV2Request;
import com.huaweicloud.sdk.apig.v2.model.CreateAuthorizingAppsV2Response;
import com.huaweicloud.sdk.apig.v2.model.ListApisUnbindedToAppV2Request;
import com.huaweicloud.sdk.apig.v2.model.ListApisUnbindedToAppV2Response;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class AuthApi {
    private static final Logger logger = LoggerFactory.getLogger(AuthApi.class.getName());
    private static final String INSTANCE_ID = "<YOUR INSTANCE ID>";
    private static final String RELEASE_ENV_ID = "DEFAULT_ENVIRONMENT_RELEASE_ID";

    public static void main(String[] args) {
        // Hardcoded or plaintext AK/SK is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "<YOUR PROJECT ID>";
        String apigEndpoint = "<APIG ENDPOINT>";
        BasicCredentials auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.setIgnoreSSLVerification(true);
        ApigClient apigClient = ApigClient.newBuilder()
                .withHttpConfig(config).withCredential(auth).withEndpoint(apigEndpoint).build();

        try {
            String appId = createApp(apigClient);
            listUnboundApis(apigClient, appId);
            String apiId = "<YOUR API ID>";
            createAppAuth(apigClient, appId, apiId);
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static String createApp(ApigClient apigClient) {
        // Construct request parameters.
        CreateAnAppV2Request request = new CreateAnAppV2Request();
        request.setInstanceId(INSTANCE_ID);
        AppCreate body = new AppCreate();
        body.setName("<YOUR APP NAME>");
        request.setBody(body);

        // Receive response parameters.
        CreateAnAppV2Response response = apigClient.createAnAppV2(request);
        String appId = response.getId();
        logger.info(appId);
        return appId;
    }

    private static void listUnboundApis(ApigClient apigClient, String appId) {
        // Construct request parameters.
        ListApisUnbindedToAppV2Request request = new ListApisUnbindedToAppV2Request();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        request.setEnvId(RELEASE_ENV_ID);

        // Receive response parameters.
        ListApisUnbindedToAppV2Response response = apigClient.listApisUnbindedToAppV2(request);
        logger.info(response.toString());
    }

    private static void createAppAuth(ApigClient apigClient, String appId, String apiId) {
        // Construct request parameters.
        CreateAuthorizingAppsV2Request request = new CreateAuthorizingAppsV2Request();
        request.setInstanceId(INSTANCE_ID);

        ApiAuthCreate body = new ApiAuthCreate();
        List<String> appIds = new ArrayList<>();
        appIds.add(appId);
        body.setAppIds(appIds);
        List<String> apiIds = new ArrayList<>();
        apiIds.add(apiId);
        body.setApiIds(apiIds);
        body.setEnvId(RELEASE_ENV_ID);
        request.setBody(body);

        // Receive response parameters.
        CreateAuthorizingAppsV2Response response = apigClient.createAuthorizingAppsV2(request);
        logger.info(response.toString());
    }
}