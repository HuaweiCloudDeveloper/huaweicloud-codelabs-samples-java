# Example of API Access Authorization in APIG (Java)
## 0. Version
This example is developed based on Huawei Cloud SDK V3.0.

## 1. Introduction
This is sample code of using Huawei Cloud Java SDK to authorize API access in API Gateway (APIG). The sample code shows how to create an app, query available APIs, and authorize APIs.

## 2. Preparations
- You have [signed up](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html?lang=en-us) to Huawei Cloud and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?locale=en-us#/accountindex/realNameAuth).
- You have set up the development environment with Java JDK 1.8 or later.
- You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. To create and view an AK/SK, go to the My Credentials > Access Keys page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0001.html).
- You have obtained the project ID of the target region. View the project ID on the My Credentials > API Credentials page of the Huawei Cloud console. For details, see [API Credentials](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0002.html).

## 3. Installing an SDK
You can obtain and install an SDK in Maven mode. Download and install Maven in your operating system (OS). After the installation is complete, add the corresponding dependencies to the pom.xml file of the Java project.

Before using the server SDK, install huaweicloud-sdk-core and huaweicloud-sdk-apig. For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>3.0.69</version>
</dependency>
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-apig</artifactId>
    <version>3.0.69</version>
</dependency>
```

## 4. Getting Started
### 4.1 Importing Dependencies
```java
// User authentication
import com.huaweicloud.sdk.core.auth.BasicCredentials;
// Request exception classes
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
// APIG client
import com.huaweicloud.sdk.apig.v2.ApigClient;
// Request and response classes for creating API groups
import com.huaweicloud.sdk.apig.v2.model.ApiGroupCreate;
import com.huaweicloud.sdk.apig.v2.model.CreateApiGroupV2Request;
import com.huaweicloud.sdk.apig.v2.model.CreateApiGroupV2Response;
// Request and response classes for creating APIs
import com.huaweicloud.sdk.apig.v2.model.ApiCreate;
import com.huaweicloud.sdk.apig.v2.model.BackendApiCreate;
import com.huaweicloud.sdk.apig.v2.model.CreateApiV2Request;
import com.huaweicloud.sdk.apig.v2.model.CreateApiV2Response;
// Request and response classes for publishing APIs
import com.huaweicloud.sdk.apig.v2.model.ApiActionInfo;
import com.huaweicloud.sdk.apig.v2.model.CreateOrDeletePublishRecordForApiV2Request;
import com.huaweicloud.sdk.apig.v2.model.CreateOrDeletePublishRecordForApiV2Response;
// Request and response classes for binding independent domain names
import com.huaweicloud.sdk.apig.v2.model.AssociateDomainV2Request;
import com.huaweicloud.sdk.apig.v2.model.AssociateDomainV2Response;
import com.huaweicloud.sdk.apig.v2.model.UrlDomainCreate;
// Log printing
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
```

### 4.2 Initializing Authentication Information
``` java
// Hardcoded or plaintext AK/SK is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
// In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
String ak = System.getenv("HUAWEICLOUD_SDK_AK");
String sk = System.getenv("HUAWEICLOUD_SDK_SK");
BasicCredentials auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);
```

### 4.3 Initializing ApigClient
```java
HttpConfig config = HttpConfig.getDefaultHttpConfig();
config.setIgnoreSSLVerification(true);
ApigClient apigClient = ApigClient.newBuilder()
    .withHttpConfig(config).withCredential(auth).withEndpoint(apigEndpoint).build();
```

### 4.4 Creating an App
#### 4.4.1 Sample Code
```java
try{
    // Construct request parameters.
    CreateAnAppV2Request request=new CreateAnAppV2Request();
    request.setInstanceId("<YOUR INSTANCE ID>");
    AppCreate body=new AppCreate();
    body.setName("<YOUR APP NAME>");
    request.setBody(body);

    // Receive response parameters.
    CreateAnAppV2Response response=apigClient.createAnAppV2(request);
    String appId=response.getId();
    logger.info(appId);
} catch (ClientRequestException e) {
    logger.error(String.valueOf(e.getHttpStatusCode()));
    logger.error(e.toString());
} catch (ServerResponseException e) {
    logger.error(String.valueOf(e.getHttpStatusCode()));
    logger.error(e.getMessage());
}
```

### 4.5 Listing APIs Available for Access Authorization
#### 4.5.1 Sample Code
```java
try {
    // Construct request parameters.
    ListApisUnbindedToAppV2Request request = new ListApisUnbindedToAppV2Request();
    request.setInstanceId("<YOUR INSTANCE ID>");
    request.setAppId("<YOUR APP ID>");
    request.setEnvId("DEFAULT_ENVIRONMENT_RELEASE_ID");

    // Receive response parameters.
    ListApisUnbindedToAppV2Response response = apigClient.listApisUnbindedToAppV2(request);
    logger.info(response.toString());
} catch (ClientRequestException e) {
    logger.error(String.valueOf(e.getHttpStatusCode()));
    logger.error(e.toString());
} catch (ServerResponseException e) {
    logger.error(String.valueOf(e.getHttpStatusCode()));
    logger.error(e.getMessage());
}
```

### 4.6 Authorizing API Access
#### 4.6.1 Sample Code
```java
try {
    // Construct request parameters.
    CreateAuthorizingAppsV2Request request = new CreateAuthorizingAppsV2Request();
    request.setInstanceId("<YOUR INSTANCE ID>");

    ApiAuthCreate body = new ApiAuthCreate();
    List<String> appIds = new ArrayList<>();
    appIds.add("<YOUR APP ID>");
    body.setAppIds(appIds);
    List<String> apiIds = new ArrayList<>();
    apiIds.add("<YOUR API ID>");
    body.setApiIds(apiIds);
    body.setEnvId("DEFAULT_ENVIRONMENT_RELEASE_ID");
    request.setBody(body);

    // Receive response parameters.
    CreateAuthorizingAppsV2Response response = apigClient.createAuthorizingAppsV2(request);
    logger.info(response.toString());
} catch (ClientRequestException e) {
    logger.error(String.valueOf(e.getHttpStatusCode()));
    logger.error(e.toString());
} catch (ServerResponseException e) {
    logger.error(String.valueOf(e.getHttpStatusCode()));
    logger.error(e.getMessage());
}
```

## 5. FAQ
None

## 6. Reference
For details, see the [APIG documentation](https://support.huaweicloud.com/intl/en-us/apig/index.html).

## 7. Change History

|    Date     | Version |   Description   |
|:-----------:| :------: | :----------: |
| Dec 2, 2021 |   1.0    | First release |
|  Mar, 2024  | 2.0 | English version |