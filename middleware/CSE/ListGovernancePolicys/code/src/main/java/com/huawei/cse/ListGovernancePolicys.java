package com.huawei.cse;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.cse.v1.CseClient;
import com.huaweicloud.sdk.cse.v1.model.ListGovernancePolicysRequest;
import com.huaweicloud.sdk.cse.v1.model.ListGovernancePolicysResponse;
import com.huaweicloud.sdk.cse.v1.region.CseRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListGovernancePolicys {

    private static final Logger logger = LoggerFactory.getLogger(ListGovernancePolicys.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listGovernancePolicysAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listGovernancePolicysSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listGovernancePolicysAk)
                .withSk(listGovernancePolicysSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CseRegion.CN_NORTH_4.
        CseClient client = CseClient.newBuilder()
                .withCredential(auth)
                .withRegion(CseRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request and set the request parameters: microservice engine instance ID, enterprise project ID, and environment.
        ListGovernancePolicysRequest request = new ListGovernancePolicysRequest();
        // Specifies the ID of a CSE instance. For the following engine IDs, click the name of a ServiceComb exclusive edition on the CSE console, and obtain the engine ID in the link.
        // https://console.huaweicloud.com/cse2/?region=cn-north-4#/csm/engine/list?engineId={engineId}
        String engineId = "<ENGINE ID>";
        request.withXEngineId(engineId);
        // Enterprise project ID. Log in to the management console, choose Enterprise > Project Management, and view the enterprise project ID in the enterprise project details.
        String enterpriseProjectID = "<ENTERPRISE PROJECT ID>";
        request.withXEnterpriseProjectID(enterpriseProjectID);
        // Indicates the home environment. If this parameter is set to all, all environments are queried.
        String environment = "all";
        request.withEnvironment(environment);
        try {
            ListGovernancePolicysResponse response = client.listGovernancePolicys(request);
            logger.info("ListGovernancePolicys: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListGovernancePolicys connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListGovernancePolicys RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListGovernancePolicys ServiceResponseException", e);
        }
    }
}