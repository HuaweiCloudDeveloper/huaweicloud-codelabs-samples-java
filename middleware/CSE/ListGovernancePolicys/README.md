### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CSE/debug?api=ListGovernancePolicys) 中直接运行调试该接口。

2.在本样例中，您可以查询治理策略列表。

3.查询治理策略列表，可以获取当前微服务应用程序中的所有治理策略列表。通过查询治理策略列表，您可以了解当前应用程序中使用的所有治理策略，包括限流、熔断、降级、重试等策略，以及它们的配置和参数设置。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已创建ServiceComb引擎专享版，并且创建了治理策略。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-cse”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cse</artifactId>
    <version>3.1.119</version>
</dependency>
```

### 代码示例
``` java
public class ListGovernancePolicys {

    private static final Logger logger = LoggerFactory.getLogger(ListGovernancePolicys.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listGovernancePolicysAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listGovernancePolicysSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(listGovernancePolicysAk)
                .withSk(listGovernancePolicysSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CseRegion.CN_NORTH_4
        CseClient client = CseClient.newBuilder()
                .withCredential(auth)
                .withRegion(CseRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求，设置请求参数微服务引擎的实例ID，企业项目ID，所属环境
        ListGovernancePolicysRequest request = new ListGovernancePolicysRequest();
        // 微服务引擎的实例ID。如下的 engineId 请使用微服务引擎控制台页面，点击某个ServiceComb引擎专享版名称，链接中的engineId
        // https://console.huaweicloud.com/cse2/?region=cn-north-4#/csm/engine/list?engineId={engineId}
        String engineId = "<ENGINE ID>";
        request.withXEngineId(engineId);
        // 企业项目ID。登录管理控制台。在企业项目详情中查看“ID”即为企业项目ID。
        String enterpriseProjectID = "<ENTERPRISE PROJECT ID>";
        request.withXEnterpriseProjectID(enterpriseProjectID);
        // 所属环境。填写all时表示查询所有环境。
        String environment = "all";
        request.withEnvironment(environment);
        try {
            ListGovernancePolicysResponse response = client.listGovernancePolicys(request);
            logger.info("ListGovernancePolicys: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListGovernancePolicys connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListGovernancePolicys RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListGovernancePolicys ServiceResponseException", e);
        }
    }
}
```

### 返回结果示例
```
[
 {
  "policies": [
   {
    "name": "scene-3wam",
    "id": "f792****",
    "status": "enabled",
    "creatTime": 1730428572,
    "updateTime": 1730428572,
    "selector": {
     "app": "testprod",
     "environment": "production"
    },
    "kind": "rateLimiting",
    "spec": {
     "limitRefreshPeriod": "1S",
     "name": "生产限流策略",
     "rate": "1"
    }
   }
  ],
  "matchGroup": {
   "name": "scene-3wam",
   "id": "1f13****",
   "status": "enabled",
   "creatTime": 1730428550,
   "updateTime": 1730428550,
   "selector": {
    "app": "testprod",
    "environment": "production"
   },
   "kind": "match-group",
   "spec": {
    "alias": "生产环境限流场景",
    "matches": [
     {
      "apiPath": {
       "prefix": "/"
      },
      "headers": {},
      "method": [
       "GET",
       "PUT",
       "POST",
       "DELETE",
       "PATCH"
      ],
      "name": "rule1",
      "showAlert": false,
      "uniqIndex": "iiupm"
     }
    ]
   }
  }
 }
]
```
### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-11-01 | 1.0 | 文档首次发布 |