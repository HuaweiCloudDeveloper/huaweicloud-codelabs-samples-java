### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CSE/doc?api=ListGovernancePolicys).

2.In this example, you can query the governance policy list.

3.Query the governance policy list. You can obtain the list of all governance policies in the current microservice application. By querying the governance policy list, you can learn about all governance policies used in the current application, including traffic limiting, circuit breaker, degradation, and retry policies, as well as their configuration and parameter settings.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The ServiceComb exclusive edition has been created and governance policies have been created.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-cse. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cse</artifactId>
    <version>3.1.119</version>
</dependency>

```

### Code example

``` java
package com.huawei.cse;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.cse.v1.CseClient;
import com.huaweicloud.sdk.cse.v1.model.ListGovernancePolicysRequest;
import com.huaweicloud.sdk.cse.v1.model.ListGovernancePolicysResponse;
import com.huaweicloud.sdk.cse.v1.region.CseRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListGovernancePolicys {

    private static final Logger logger = LoggerFactory.getLogger(ListGovernancePolicys.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listGovernancePolicysAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listGovernancePolicysSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listGovernancePolicysAk)
                .withSk(listGovernancePolicysSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CseRegion.CN_NORTH_4.
        CseClient client = CseClient.newBuilder()
                .withCredential(auth)
                .withRegion(CseRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request and set the request parameters: microservice engine instance ID, enterprise project ID, and environment.
        ListGovernancePolicysRequest request = new ListGovernancePolicysRequest();
        // Specifies the ID of a CSE instance. For the following engine IDs, click the name of a ServiceComb exclusive edition on the CSE console, and obtain the engine ID in the link.
        // https://console.huaweicloud.com/cse2/?region=cn-north-4#/csm/engine/list?engineId={engineId}
        String engineId = "<ENGINE ID>";
        request.withXEngineId(engineId);
        // Enterprise project ID. Log in to the management console, choose Enterprise > Project Management, and view the enterprise project ID in the enterprise project details.
        String enterpriseProjectID = "<ENTERPRISE PROJECT ID>";
        request.withXEnterpriseProjectID(enterpriseProjectID);
        // Indicates the home environment. If this parameter is set to all, all environments are queried.
        String environment = "all";
        request.withEnvironment(environment);
        try {
            ListGovernancePolicysResponse response = client.listGovernancePolicys(request);
            logger.info("ListGovernancePolicys: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListGovernancePolicys connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListGovernancePolicys RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListGovernancePolicys ServiceResponseException", e);
        }
    }
}
```

### Example of the returned result

#### ListGovernancePolicys

```
[
 {
  "policies": [
   {
    "name": "scene-3wam",
    "id": "f792****",
    "status": "enabled",
    "creatTime": 1730428572,
    "updateTime": 1730428572,
    "selector": {
     "app": "testprod",
     "environment": "production"
    },
    "kind": "rateLimiting",
    "spec": {
     "limitRefreshPeriod": "1S",
     "name": "生产限流策略",
     "rate": "1"
    }
   }
  ],
  "matchGroup": {
   "name": "scene-3wam",
   "id": "1f13****",
   "status": "enabled",
   "creatTime": 1730428550,
   "updateTime": 1730428550,
   "selector": {
    "app": "testprod",
    "environment": "production"
   },
   "kind": "match-group",
   "spec": {
    "alias": "生产环境限流场景",
    "matches": [
     {
      "apiPath": {
       "prefix": "/"
      },
      "headers": {},
      "method": [
       "GET",
       "PUT",
       "POST",
       "DELETE",
       "PATCH"
      ],
      "name": "rule1",
      "showAlert": false,
      "uniqIndex": "iiupm"
     }
    ]
   }
  }
 }
]
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/11/01 | 1.0 | This document is released for the first time.