### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CSE/doc?api=ShowEngineQuotas).

2.In this example, you can query the CSE quota.

3.Queries CSE quotas to learn about the quota type, number of used quotas, and total number of engine quotas, helping you better manage and optimize CSE usage.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-cse. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cse</artifactId>
    <version>3.1.119</version>
</dependency>

```

### Code example

``` java
package com.huawei.cse;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.cse.v1.CseClient;
import com.huaweicloud.sdk.cse.v1.model.ShowEngineQuotasRequest;
import com.huaweicloud.sdk.cse.v1.model.ShowEngineQuotasResponse;
import com.huaweicloud.sdk.cse.v1.region.CseRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowEngineQuotas {

    private static final Logger logger = LoggerFactory.getLogger(ShowEngineQuotas.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String showEngineQuotasAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showEngineQuotasSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(showEngineQuotasAk)
                .withSk(showEngineQuotasSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CseRegion.CN_NORTH_4.
        CseClient client = CseClient.newBuilder()
                .withCredential(auth)
                .withRegion(CseRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request.
        ShowEngineQuotasRequest request = new ShowEngineQuotasRequest();
        try {
            ShowEngineQuotasResponse response = client.showEngineQuotas(request);
            logger.info("ShowEngineQuotas: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ShowEngineQuotas connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ShowEngineQuotas RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ShowEngineQuotas ServiceResponseException", e);
        }
    }
}
```

### Example of the returned result

#### ListFlavors

```
{
 "quotas": {
  "resources": [
   {
    "type": "Engine",
    "used": 1,
    "quota": 5
   }
  ]
 }
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/10/31 | 1.0 | This document is released for the first time.