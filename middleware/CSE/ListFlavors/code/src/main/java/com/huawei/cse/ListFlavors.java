package com.huawei.cse;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.cse.v1.CseClient;
import com.huaweicloud.sdk.cse.v1.model.ListFlavorsRequest;
import com.huaweicloud.sdk.cse.v1.model.ListFlavorsResponse;
import com.huaweicloud.sdk.cse.v1.region.CseRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListFlavors {

    private static final Logger logger = LoggerFactory.getLogger(ListFlavors.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listFlavorsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listFlavorsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listFlavorsAk)
                .withSk(listFlavorsSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CseRegion.CN_NORTH_4.
        CseClient client = CseClient.newBuilder()
                .withCredential(auth)
                .withRegion(CseRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request.
        ListFlavorsRequest request = new ListFlavorsRequest();
        try {
            ListFlavorsResponse response = client.listFlavors(request);
            logger.info("ListFlavors: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListFlavors connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListFlavors RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListFlavors ServiceResponseException", e);
        }
    }
}