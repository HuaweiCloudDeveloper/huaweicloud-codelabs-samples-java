package com.huawei.dms.rabbitmq.lifecycle;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rabbitmq.v2.RabbitMQClient;
import com.huaweicloud.sdk.rabbitmq.v2.model.ShowInstanceRequest;
import com.huaweicloud.sdk.rabbitmq.v2.model.ShowInstanceResponse;


import com.huaweicloud.sdk.rabbitmq.v2.region.RabbitMQRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RabbitMQShowInstanceDetailDemo {
    private static final Logger logger = LoggerFactory.getLogger(RabbitMQShowInstanceDetailDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId("<YOUR PROJECT ID>");
        RabbitMQClient client = RabbitMQClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(new HttpConfig())
                .withRegion(RabbitMQRegion.valueOf("<REGION ID>"))
                .build();

        ShowInstanceRequest request = new ShowInstanceRequest();
        String instance_id = "<YOUR INSTANCE ID>";
        request.setInstanceId(instance_id);
        try {
            ShowInstanceResponse response = client.showInstance(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}