### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RabbitMQ/doc?api=ListVhosts).
2.Query the Vhost list.

3.This interface is used to query the Vhost list.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A project and a check task have been created on the DMS platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-rabbitmq. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rabbitmq</artifactId>
    <version>3.1.117</version>
</dependency>

```

### Sample Code

``` java
package com.huawei.rabbitmq;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rabbitmq.v2.RabbitMQClient;
import com.huaweicloud.sdk.rabbitmq.v2.model.*;
import com.huaweicloud.sdk.rabbitmq.v2.region.RabbitMQRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ListVhosts {
    private static final Logger logger = LoggerFactory.getLogger(ListVhosts.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String listVhostsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listVhostsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Note: For the following deletePipeProjectId, choose HUAWEI CLOUD Console > IAM My Credential > the project ID of the corresponding region.
        String projectId = "<YOUR PROJECT ID>";

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listVhostsAk)
                .withSk(listVhostsSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to Region.CN_NORTH_4.
        RabbitMQClient client = RabbitMQClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(RabbitMQRegion.CN_NORTH_4)
                .build();

        // Build Request
        ListVhostsRequest request = new ListVhostsRequest();
        request.withInstanceId("<YOUR INSTANCE ID>");

        try {
            // Send a request
            ListVhostsResponse response = client.listVhosts(request);
            // Print the response result.
            logger.info(response.toString());
        }catch(ServiceResponseException  e){
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}
```

### Example of the returned result

#### DeleteHost

```
{
  "size" : 10,
  "total" : 13,
  "items" : [ {
    "name" : "/",
    "tracing" : false
  }, {
    "name" : "test-vhost1",
    "tracing" : false
  }, {
    "name" : "test-vhost10",
    "tracing" : false
  }, {
    "name" : "test-vhost2",
    "tracing" : false
  }, {
    "name" : "test-vhost3",
    "tracing" : false
  }, {
    "name" : "test-vhost4",
    "tracing" : false
  }, {
    "name" : "test-vhost5",
    "tracing" : false
  }, {
    "name" : "test-vhost6",
    "tracing" : false
  }, {
    "name" : "test-vhost7",
    "tracing" : false
  }, {
    "name" : "test-vhost8",
    "tracing" : false
  } ]
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/09/26 | 1.0 | This document is released for the first time.