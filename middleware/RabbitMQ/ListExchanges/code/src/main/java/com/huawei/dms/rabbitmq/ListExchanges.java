package com.huawei.dms.rabbitmq;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rabbitmq.v2.RabbitMQClient;
import com.huaweicloud.sdk.rabbitmq.v2.model.ListExchangesRequest;
import com.huaweicloud.sdk.rabbitmq.v2.model.ListExchangesResponse;
import com.huaweicloud.sdk.rabbitmq.v2.region.RabbitMQRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListExchanges {

    private static final Logger logger = LoggerFactory.getLogger(ListExchanges.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listExchangesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listExchangesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listExchangesAk)
                .withSk(listExchangesSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to RabbitMQRegion.CN_NORTH_4.
        RabbitMQClient client = RabbitMQClient.newBuilder()
                .withCredential(auth)
                .withRegion(RabbitMQRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request and set the request parameter instance ID and Vhost name.
        ListExchangesRequest request = new ListExchangesRequest();
        // Specifies the instance ID. You can obtain the instance ID on the RabbitMQ page. The instance ID is displayed under the RabbitMQ instance name. You can also obtain the instance ID from the API for querying ListInstancesDetails of all instances. The returned value contains the instance ID.
        String instanceId = "<YOUR INSTANCE ID>";
        request.withInstanceId(instanceId);
        // Name of the Vhost to which the Vhost belongs. You can click the RabbitMQ instance name and view the Vhost list on the page. Alternatively, you can use the API for querying the Vhost list ListVhosts to obtain the Vhost name. The returned value contains the Vhost name.
        String vHost = "<YOUR VHOST>";
        request.withVhost(vHost);
        try {
            ListExchangesResponse response = client.listExchanges(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}