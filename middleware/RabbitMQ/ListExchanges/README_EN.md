### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/RabbitMQ/doc?api=ListExchanges).

2.In this example, you can query the Exchange list.

3.Query the Exchange list to know the Exchange name, type, whether the Exchange is persistent, and Vhost to which the Exchange belongs.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The current account has the permission to use RabbitMQ and has created an instance.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-rabbitmq. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rabbitmq</artifactId>
    <version>3.1.120</version>
</dependency>

```

### Code example

``` java
package com.huawei.dms.rabbitmq;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rabbitmq.v2.RabbitMQClient;
import com.huaweicloud.sdk.rabbitmq.v2.model.ListExchangesRequest;
import com.huaweicloud.sdk.rabbitmq.v2.model.ListExchangesResponse;
import com.huaweicloud.sdk.rabbitmq.v2.region.RabbitMQRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListExchanges {

    private static final Logger logger = LoggerFactory.getLogger(ListExchanges.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listExchangesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listExchangesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listExchangesAk)
                .withSk(listExchangesSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to RabbitMQRegion.CN_NORTH_4.
        RabbitMQClient client = RabbitMQClient.newBuilder()
                .withCredential(auth)
                .withRegion(RabbitMQRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request and set the request parameter instance ID and Vhost name.
        ListExchangesRequest request = new ListExchangesRequest();
        // Specifies the instance ID. You can obtain the instance ID on the RabbitMQ page. The instance ID is displayed under the RabbitMQ instance name. You can also obtain the instance ID from the API for querying ListInstancesDetails of all instances. The returned value contains the instance ID.
        String instanceId = "<YOUR INSTANCE ID>";
        request.withInstanceId(instanceId);
        // Name of the Vhost to which the Vhost belongs. You can click the RabbitMQ instance name and view the Vhost list on the page. Alternatively, you can use the API for querying the Vhost list ListVhosts to obtain the Vhost name. The returned value contains the Vhost name.
        String vHost = "<YOUR VHOST>";
        request.withVhost(vHost);
        try {
            ListExchangesResponse response = client.listExchanges(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result

#### ListExchanges

```
{
 "total": 7,
 "size": 7,
 "items": [
  {
   "durable": true,
   "vhost": "Vhost-1",
   "default": true,
   "internal": false,
   "name": "(AMQP default)",
   "auto_delete": false,
   "arguments": {},
   "type": "direct"
  },
  {
   "durable": true,
   "vhost": "Vhost-1",
   "default": true,
   "internal": false,
   "name": "amq.direct",
   "auto_delete": false,
   "arguments": {},
   "type": "direct"
  },
  {
   "durable": true,
   "vhost": "Vhost-1",
   "default": true,
   "internal": false,
   "name": "amq.fanout",
   "auto_delete": false,
   "arguments": {},
   "type": "fanout"
  },
  {
   "durable": true,
   "vhost": "Vhost-1",
   "default": true,
   "internal": false,
   "name": "amq.headers",
   "auto_delete": false,
   "arguments": {},
   "type": "headers"
  },
  {
   "durable": true,
   "vhost": "Vhost-1",
   "default": true,
   "internal": false,
   "name": "amq.match",
   "auto_delete": false,
   "arguments": {},
   "type": "headers"
  },
  {
   "durable": true,
   "vhost": "Vhost-1",
   "default": true,
   "internal": true,
   "name": "amq.rabbitmq.trace",
   "auto_delete": false,
   "arguments": {},
   "type": "topic"
  },
  {
   "durable": true,
   "vhost": "Vhost-1",
   "default": true,
   "internal": false,
   "name": "amq.topic",
   "auto_delete": false,
   "arguments": {},
   "type": "topic"
  }
 ]
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/11/07 | 1.0 | This document is released for the first time.