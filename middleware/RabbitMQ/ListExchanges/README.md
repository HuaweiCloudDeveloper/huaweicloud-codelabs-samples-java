### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RabbitMQ/debug?api=ListExchanges) 中直接运行调试该接口。

2.在本样例中，您可以查询Exchange列表。

3.查询Exchange列表，可以了解Exchange名称，类型，是否持久化，所属Vhost等。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.当前登录账号拥有使用RabbitMQ的权限，已创建实例，已创建Vhost。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-rabbitmq”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rabbitmq</artifactId>
    <version>3.1.120</version>
</dependency>
```

### 代码示例
``` java
public class ListExchanges {

    private static final Logger logger = LoggerFactory.getLogger(ListExchanges.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listExchangesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listExchangesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(listExchangesAk)
                .withSk(listExchangesSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为RabbitMQRegion.CN_NORTH_4
        RabbitMQClient client = RabbitMQClient.newBuilder()
                .withCredential(auth)
                .withRegion(RabbitMQRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求，设置请求参数实例ID，所属Vhost名称
        ListExchangesRequest request = new ListExchangesRequest();
        // 实例ID。可以通过Rabbitmq页面获取，Rabbitmq实例名称下方有对应的ID或者通过查询所有实例列表ListInstancesDetails的API获取，返回值中有实例ID的信息
        String instanceId = "<YOUR INSTANCE ID>";
        request.withInstanceId(instanceId);
        // 所属Vhost名称。可以通过点击Rabbitmq实例名称，然后在页面查看Vhost列表或者通过查询Vhost列表ListVhosts的API获取，返回值中有Vhost名称的信息
        String vHost = "<YOUR VHOST>";
        request.withVhost(vHost);
        try {
            ListExchangesResponse response = client.listExchanges(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例
```
{
 "total": 7,
 "size": 7,
 "items": [
  {
   "durable": true,
   "vhost": "Vhost-1",
   "default": true,
   "internal": false,
   "name": "(AMQP default)",
   "auto_delete": false,
   "arguments": {},
   "type": "direct"
  },
  {
   "durable": true,
   "vhost": "Vhost-1",
   "default": true,
   "internal": false,
   "name": "amq.direct",
   "auto_delete": false,
   "arguments": {},
   "type": "direct"
  },
  {
   "durable": true,
   "vhost": "Vhost-1",
   "default": true,
   "internal": false,
   "name": "amq.fanout",
   "auto_delete": false,
   "arguments": {},
   "type": "fanout"
  },
  {
   "durable": true,
   "vhost": "Vhost-1",
   "default": true,
   "internal": false,
   "name": "amq.headers",
   "auto_delete": false,
   "arguments": {},
   "type": "headers"
  },
  {
   "durable": true,
   "vhost": "Vhost-1",
   "default": true,
   "internal": false,
   "name": "amq.match",
   "auto_delete": false,
   "arguments": {},
   "type": "headers"
  },
  {
   "durable": true,
   "vhost": "Vhost-1",
   "default": true,
   "internal": true,
   "name": "amq.rabbitmq.trace",
   "auto_delete": false,
   "arguments": {},
   "type": "topic"
  },
  {
   "durable": true,
   "vhost": "Vhost-1",
   "default": true,
   "internal": false,
   "name": "amq.topic",
   "auto_delete": false,
   "arguments": {},
   "type": "topic"
  }
 ]
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-11-07 | 1.0 | 文档首次发布 |