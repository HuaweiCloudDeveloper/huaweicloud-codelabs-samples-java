package com.huawei.dms.rabbitmq.lifecycle;

import com.huaweicloud.sdk.rabbitmq.v2.RabbitMQClient;
import com.huaweicloud.sdk.rabbitmq.v2.model.UpdateInstanceReq;
import com.huaweicloud.sdk.rabbitmq.v2.model.UpdateInstanceRequest;
import com.huaweicloud.sdk.rabbitmq.v2.model.UpdateInstanceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RabbitMQUpdateInstanceDemo {
    private static final Logger logger = LoggerFactory.getLogger(RabbitMQUpdateInstanceDemo.class);

    public static void main(String[] args) {
        RabbitMQClient client = General.getClient();
        try {
            UpdateInstanceRequest request = new UpdateInstanceRequest();
            request.withInstanceId("<YOUR InstanceId>");
            UpdateInstanceReq body = new UpdateInstanceReq();
            body.withName("<YOUR Name>");
            body.withEnterpriseProjectId("<YOUR EnterpriseProjectId>");
            body.withEnablePublicip(true);
            body.withPublicipId("<YOUR PublicipId>");
            body.withSecurityGroupId("<YOUR SecurityGroupId>");
            body.withMaintainBegin("06:00:00");
            body.withMaintainEnd("10:00:00");
            body.withDescription("<YOUR Description>");
            request.withBody(body);
            UpdateInstanceResponse response = client.updateInstance(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}