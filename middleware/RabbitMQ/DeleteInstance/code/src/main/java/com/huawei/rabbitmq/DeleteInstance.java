package com.huawei.rabbitmq;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rabbitmq.v2.RabbitMQClient;
import com.huaweicloud.sdk.rabbitmq.v2.model.*;
import com.huaweicloud.sdk.rabbitmq.v2.region.RabbitMQRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DeleteInstance {
    private static final Logger logger = LoggerFactory.getLogger(DeleteInstance.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String deleteInstanceAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String deleteInstanceSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Note: For the following deletePipeProjectId, choose HUAWEI CLOUD Console > IAM My Credential > the project ID of the corresponding region.
        String projectId = "<YOUR PROJECT ID>";

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(deleteInstanceAk)
                .withSk(deleteInstanceSk)
                .withProjectId(projectId);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to Region.CN_NORTH_4.
        RabbitMQClient client = RabbitMQClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(RabbitMQRegion.AP_SOUTHEAST_3)
                .build();

        // Build Request
        DeleteInstanceRequest request = new DeleteInstanceRequest();
        request.withInstanceId("<YOUR INSTANCE ID>");

        try {
            // Send a request
            DeleteInstanceResponse response = client.deleteInstance(request);
            // Print the response result.
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}