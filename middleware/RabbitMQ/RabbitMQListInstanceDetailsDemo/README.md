## 1. 示例简介
基于华为云Java SDK对分布式消息服务RabbitMQ实例进行生命周期管理的代码示例。其中包括：修改RabbitMQ实例信息。

## 2. 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已获取对应区域的项目，请在华为云控制台“我的凭证 > API凭证 > 项目列表”页面上查看项目，例如：cn-north-4。具体请参见[API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html)

## 3. SDK获取和安装
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-rabbitmq”，具体的SDK版本号请参见[SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/?language=java)

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>3.1.87</version>
</dependency>
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rabbitmq</artifactId>
    <version>3.1.87</version>
</dependency>
```

## 4. 代码示例
以下代码展示如何使用查询RabbitMQ实例列表（RabbitMQ ListInstanceDetails）相关SDK

```java
package com.huawei.dms.rabbitmq.lifecycle;

import com.huaweicloud.sdk.rabbitmq.v2.RabbitMQClient;
import com.huaweicloud.sdk.rabbitmq.v2.model.UpdateInstanceReq;
import com.huaweicloud.sdk.rabbitmq.v2.model.UpdateInstanceRequest;
import com.huaweicloud.sdk.rabbitmq.v2.model.UpdateInstanceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateInstance {
    private static final Logger logger = LoggerFactory.getLogger(UpdateInstance.class);

    public static void main(String[] args) {
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String PROJECT_ID = "<YOUR PROJECT ID>";
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(PROJECT_ID);
        RabbitMQClient client = RabbitMQClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(new HttpConfig())
                .withRegion(RabbitMQRegion.valueOf("<REGION ID>"))
                .build();
        
        ListInstancesDetailsRequest request = new ListInstancesDetailsRequest();
        try {
            ListInstancesDetailsResponse response = client.listInstancesDetails(request);
            logger.info(String.valueOf(response.toString()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
```


## 5. 参考
更多信息请参考[分布式消息服务RabbitMQ版文档](https://support.huaweicloud.com/RabbitMQ/index.html)

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-03-26 | 1.0 | 文档首次发布|
