package com.huawei.ims;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.ims.v2.ImsClient;
import com.huaweicloud.sdk.ims.v2.model.UpdateImageRequest;
import com.huaweicloud.sdk.ims.v2.model.UpdateImageRequestBody;
import com.huaweicloud.sdk.ims.v2.model.UpdateImageResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class IMSUpdateImageDemo {

    private static final Logger LOGGER = LoggerFactory.getLogger(IMSUpdateImageDemo.class.getName());

    public static void main(String[] args) {
        // There will be security risks if the AK and SK used for authentication is written into code. Encrypt the AK/SK and store them into the configuration file or environment variables.
        // In this example, the AK and SK are stored in environment variables. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // Configure client attributes.
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // Create a credential.
        BasicCredentials auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        // Create and initialize an imsClient instance.
        ImsClient imsClient = ImsClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(new Region("cn-north-4", new String[] {"https://ims.cn-north-4.myhuaweicloud.com"}))
                .build();

        // Update image information.
        updateImage(imsClient);
    }



    private static void updateImage(ImsClient client) {
        // Image ID.
        String imageId = "{your imageId string}";
        // Operation. The value can be add, replace, or remove.
        UpdateImageRequestBody.OpEnum op = UpdateImageRequestBody.OpEnum.REPLACE;
        // The name of the attribute to be modified. A slash (/) needs to be added in front of it.
        String path = "{your path string}";
        // The new value of the attribute.
        String value = "{your value string}";
        List<UpdateImageRequestBody> body = new ArrayList<>();
        body.add(new UpdateImageRequestBody()
                .withOp(op)
                .withPath(path)
                .withValue(value));
        try {
            UpdateImageResponse response = client.updateImage(
                    new UpdateImageRequest()
                            .withImageId(imageId)
                            .withBody(body));
            LOGGER.info(response.toString());
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
        LOGGER.error("RequestId: " + e.getRequestId());
        LOGGER.error("ErrorCode: " + e.getErrorCode());
        LOGGER.error("ErrorMsg: " + e.getErrorMsg());
    }
}