## 1. Introduction
Updating Image Information

[Updating image information] (https://support.huaweicloud.com/intl/en-us/api-ims/ims_03_0604.html)
This API is used to modify image attributes.


What You Will Learn

Use the Java SDK to update image information.

## 2. Prerequisites
- 1. You have obtained the Huawei Cloud SDK. You can also install the Java SDK.
- 2. You must have a Huawei Cloud account and a pair of AK/SK. You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details, see [Access Keys] (https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html).
- 3. endpoint: You have obtained the application region and endpoint of a Huawei Cloud service. For details, see [Regions and Endpoints]. (https://developer.huaweicloud.com/intl/en-us/endpoint).
- 4. The Java SDK version is 1.8 or later.
- 5. You have created images on the IMS console.

## 3. Obtaining and Installing the SDK
You use Maven to obtain and install the SDK. Download Maven and install it in your operating system (OS). After the installation is complete, add the required dependencies to the pom.xml file of the Java project.
The IMS SDK is used as an example.
```xml
    <dependencies>
        <dependency>
            <groupId>com.huaweicloud.sdk</groupId>
            <artifactId>huaweicloud-sdk-ims</artifactId>
            <version>3.1.63</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>2.0.5</version>
        </dependency>
        <dependency>
            <groupId>com.huaweicloud.sdk</groupId>
            <artifactId>huaweicloud-sdk-core</artifactId>
            <version>3.1.58</version>
            <scope>compile</scope>
        </dependency>
    </dependencies>
```


## 4. API Parameters
For details about API parameters, see:

[Updating image information] (https://support.huaweicloud.com/intl/en-us/api-ims/ims_03_0604.html)

## 5. Sample Code
The following code shows how to use the SDK to update image information:

```java
package com.huawei.ims;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.ims.v2.ImsClient;
import com.huaweicloud.sdk.ims.v2.model.UpdateImageRequest;
import com.huaweicloud.sdk.ims.v2.model.UpdateImageRequestBody;
import com.huaweicloud.sdk.ims.v2.model.UpdateImageResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class IMSUpdateImageDemo {

    private static final Logger LOGGER = LoggerFactory.getLogger(IMSUpdateImageDemo.class.getName());

    public static void main(String[] args) {
        // There will be security risks if the AK and SK used for authentication is written into code. Encrypt the AK/SK and store them into the configuration file or environment variables.
        // In this example, the AK and SK are stored in environment variables. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // Configure client attributes.
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // Create a credential.
        BasicCredentials auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        // Create and initialize an imsClient instance.
        ImsClient imsClient = ImsClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(new Region("cn-north-4", new String[] {"https://ims.cn-north-4.myhuaweicloud.com"}))
                .build();

        // Update image information.
        updateImage(imsClient);
    }



    private static void updateImage(ImsClient client) {
        // Image ID.
        String imageId = "{your imageId string}";
        // Operation. The value can be add, replace, or remove.
        UpdateImageRequestBody.OpEnum op = UpdateImageRequestBody.OpEnum.REPLACE;
        // The name of the attribute to be modified. A slash (/) needs to be added in front of it.
        String path = "{your path string}";
       // The new value of the attribute.
        String value = "{your value string}";
        List<UpdateImageRequestBody> body = new ArrayList<>();
        body.add(new UpdateImageRequestBody()
                .withOp(op)
                .withPath(path)
                .withValue(value));
        try {
            UpdateImageResponse response = client.updateImage(
                    new UpdateImageRequest()
                            .withImageId(imageId)
                            .withBody(body));
            LOGGER.info(response.toString());
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
        LOGGER.error("RequestId: " + e.getRequestId());
        LOGGER.error("ErrorCode: " + e.getErrorCode());
        LOGGER.error("ErrorMsg: " + e.getErrorMsg());
    }
}
```

## 6. Change History

|  Date  | Version |   Description   |
|:----------:| :------: | :----------: |
| Dec 30, 2023 |   1.0    | First release |
