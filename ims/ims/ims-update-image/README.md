## 1、介绍
**镜像服务更新镜像信息**

[更新镜像信息](https://support.huaweicloud.com/api-ims/ims_03_0604.html)
更新镜像信息接口，主要用于镜像属性的修改。


**您将学到什么？**

如何通过java版SDK来体验管理镜像服务更新镜像信息。

## 2、前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云租户账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 3、endpoint 华为云各服务应用区域和各服务的终端节点，具体请参见[地区和终端节点](https://developer.huaweicloud.com/endpoint)。
- 4、华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。
- 5、用户需要在镜像服务创建镜像。

## 3、SDK获取和安装
您可以通过如下方式获取和安装 SDK： 通过 Maven 安装依赖（推荐） 通过 Maven 安装项目依赖是使用 Java SDK 的推荐方法，首先您需要在您的操作系统中下载并安装 Maven ，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
本示例使用BMS SDK：
```xml
    <dependencies>
        <dependency>
            <groupId>com.huaweicloud.sdk</groupId>
            <artifactId>huaweicloud-sdk-ims</artifactId>
            <version>3.1.63</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>2.0.5</version>
        </dependency>
        <dependency>
            <groupId>com.huaweicloud.sdk</groupId>
            <artifactId>huaweicloud-sdk-core</artifactId>
            <version>3.1.58</version>
            <scope>compile</scope>
        </dependency>
    </dependencies>
```


## 4、接口参数说明
关于接口参数的详细说明可参见：

[镜像服务更新镜像信息](https://support.huaweicloud.com/api-ims/ims_03_0604.html)

## 5、代码示例
以下代码展示如何使用更新镜像信息相关SDK

```java
package com.huawei.ims;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.ims.v2.ImsClient;
import com.huaweicloud.sdk.ims.v2.model.UpdateImageRequest;
import com.huaweicloud.sdk.ims.v2.model.UpdateImageRequestBody;
import com.huaweicloud.sdk.ims.v2.model.UpdateImageResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class IMSUpdateImageDemo {

    private static final Logger LOGGER = LoggerFactory.getLogger(IMSUpdateImageDemo.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // 配置客户端属性
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // 创建认证
        BasicCredentials auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        // 创建imsClient实例并初始化
        ImsClient imsClient = ImsClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(new Region("cn-north-4", new String[] {"https://ims.cn-north-4.myhuaweicloud.com"}))
                .build();

        // 镜像服务更新镜像信息
        updateImage(imsClient);
    }



    private static void updateImage(ImsClient client) {
        // 镜像ID
        String imageId = "{your imageId string}";
        //操作类型 目前取值为add、replace和remove。
        UpdateImageRequestBody.OpEnum op = UpdateImageRequestBody.OpEnum.REPLACE;
        // 需要操作的属性名称，需要在属性名称前加“/”。
        String path = "{your path string}";
        // 需要操作的属性的值。
        String value = "{your value string}";
        List<UpdateImageRequestBody> body = new ArrayList<>();
        body.add(new UpdateImageRequestBody()
                .withOp(op)
                .withPath(path)
                .withValue(value));
        try {
            UpdateImageResponse response = client.updateImage(
                    new UpdateImageRequest()
                            .withImageId(imageId)
                            .withBody(body));
            LOGGER.info(response.toString());
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
        LOGGER.error("RequestId: " + e.getRequestId());
        LOGGER.error("ErrorCode: " + e.getErrorCode());
        LOGGER.error("ErrorMsg: " + e.getErrorMsg());
    }
}
```

## 6、修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-12-30 | 1.0 | 文档首次发布 |