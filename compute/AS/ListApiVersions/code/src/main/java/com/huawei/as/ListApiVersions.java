package com.huawei.as;

import com.huaweicloud.sdk.as.v1.model.ListApiVersionsRequest;
import com.huaweicloud.sdk.as.v1.model.ListApiVersionsResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.as.v1.region.AsRegion;
import com.huaweicloud.sdk.as.v1.AsClient;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListApiVersions {

    private static final Logger logger = LoggerFactory.getLogger(ListApiVersions.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listApiVersionsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listApiVersionsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listApiVersionsAk)
                .withSk(listApiVersionsSk);

        // Create a service client and set the corresponding region to AsRegion.CN_NORTH_4.
        AsClient client = AsClient.newBuilder()
                .withCredential(auth)
                .withRegion(AsRegion.CN_NORTH_4)
                .build();
        // Build Request
        ListApiVersionsRequest request = new ListApiVersionsRequest();
        try {
            ListApiVersionsResponse response = client.listApiVersions(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}