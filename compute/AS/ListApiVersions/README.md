### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/AS/debug?api=ListApiVersions) 中直接运行调试该接口。

2.在本样例中，您可以查询弹性伸缩API所有版本信息。

3.查询弹性伸缩API所有版本信息，可以用来获取当前支持的所有 API 版本以及每个版本的详细信息。通过调用这个接口，可以了解到每个版本API的URL地址、API版本ID等信息。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.当前登录账号拥有使用AS服务的权限。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-as”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-as</artifactId>
    <version>3.1.123</version>
</dependency>
```

### 代码示例
``` java
public class ListApiVersions {

    private static final Logger logger = LoggerFactory.getLogger(ListApiVersions.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listApiVersionsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listApiVersionsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(listApiVersionsAk)
                .withSk(listApiVersionsSk);

        // 创建服务客户端并配置对应region为AsRegion.CN_NORTH_4
        AsClient client = AsClient.newBuilder()
                .withCredential(auth)
                .withRegion(AsRegion.CN_NORTH_4)
                .build();
        // 构建请求
        ListApiVersionsRequest request = new ListApiVersionsRequest();
        try {
            ListApiVersionsResponse response = client.listApiVersions(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例
```
{
 "versions": [
  {
   "id": "v1",
   "links": [
    {
     "href": "https://as.cn-north-4.myhuaweicloud.com/autoscaling-api/v1/",
     "rel": "self"
    }
   ],
   "min_version": "",
   "status": "CURRENT",
   "updated": "2016-06-30T00:00:00Z",
   "version": ""
  },
  {
   "id": "v2",
   "links": [
    {
     "href": "https://as.cn-north-4.myhuaweicloud.com/autoscaling-api/v2/",
     "rel": "self"
    }
   ],
   "min_version": "",
   "status": "SUPPORTED",
   "updated": "2018-03-30T00:00:00Z",
   "version": ""
  }
 ]
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-12-04 | 1.0 | 文档首次发布 |