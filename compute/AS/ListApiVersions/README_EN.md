### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/AS/debug?api=ListApiVersions).

2.In this example, you can query all AS API versions.

3.This API is used to query all supported API versions and detailed information about each version. By calling this API, you can obtain information such as the URL and API version ID of each API version.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. 
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. 
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The current login account has the permission to use the AS service.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-as. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-as</artifactId>
    <version>3.1.123</version>
</dependency>
```

### Code example
``` java
package com.huawei.as;

import com.huaweicloud.sdk.as.v1.model.ListApiVersionsRequest;
import com.huaweicloud.sdk.as.v1.model.ListApiVersionsResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.as.v1.region.AsRegion;
import com.huaweicloud.sdk.as.v1.AsClient;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListApiVersions {

    private static final Logger logger = LoggerFactory.getLogger(ListApiVersions.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listApiVersionsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listApiVersionsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listApiVersionsAk)
                .withSk(listApiVersionsSk);

        // Create a service client and set the corresponding region to AsRegion.CN_NORTH_4.
        AsClient client = AsClient.newBuilder()
                .withCredential(auth)
                .withRegion(AsRegion.CN_NORTH_4)
                .build();
        // Build Request
        ListApiVersionsRequest request = new ListApiVersionsRequest();
        try {
            ListApiVersionsResponse response = client.listApiVersions(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
```
{
 "versions": [
  {
   "id": "v1",
   "links": [
    {
     "href": "https://as.cn-north-4.myhuaweicloud.com/autoscaling-api/v1/",
     "rel": "self"
    }
   ],
   "min_version": "",
   "status": "CURRENT",
   "updated": "2016-06-30T00:00:00Z",
   "version": ""
  },
  {
   "id": "v2",
   "links": [
    {
     "href": "https://as.cn-north-4.myhuaweicloud.com/autoscaling-api/v2/",
     "rel": "self"
    }
   ],
   "min_version": "",
   "status": "SUPPORTED",
   "updated": "2018-03-30T00:00:00Z",
   "version": ""
  }
 ]
}
```
### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/12/04 | 1.0 | This document is released for the first time.