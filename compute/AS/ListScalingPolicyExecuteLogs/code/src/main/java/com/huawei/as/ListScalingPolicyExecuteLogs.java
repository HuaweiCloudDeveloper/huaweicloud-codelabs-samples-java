package com.huawei.as;

import com.huaweicloud.sdk.as.v1.AsClient;
import com.huaweicloud.sdk.as.v1.model.ListScalingPolicyExecuteLogsRequest;
import com.huaweicloud.sdk.as.v1.model.ListScalingPolicyExecuteLogsResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.as.v1.region.AsRegion;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListScalingPolicyExecuteLogs {

    private static final Logger logger = LoggerFactory.getLogger(ListScalingPolicyExecuteLogs.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String scalingPolicyExecuteLogsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String scalingPolicyExecuteLogsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(scalingPolicyExecuteLogsAk)
                .withSk(scalingPolicyExecuteLogsSk);

        // Create a service client and set the corresponding region to AsRegion.CN_NORTH_4.
        AsClient client = AsClient.newBuilder()
                .withCredential(auth)
                .withRegion(AsRegion.CN_NORTH_4)
                .build();
        // Construct a request and set the request parameter AS policy ID.
        ListScalingPolicyExecuteLogsRequest request = new ListScalingPolicyExecuteLogsRequest();
        // Specifies the scaling policy ID.
        String scalingPolicyId = "<YOUR SCALING POLICY ID>";
        request.withScalingPolicyId(scalingPolicyId);
        try {
            ListScalingPolicyExecuteLogsResponse response = client.listScalingPolicyExecuteLogs(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}