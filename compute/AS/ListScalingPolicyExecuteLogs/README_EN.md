### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/AS/debug?api=ListScalingPolicyExecuteLogs).

2.In this example, you can query policy execution logs.

3.Query historical policy execution records based on the entered conditions. The query results are displayed on multiple pages. You can filter and query logs based on the log ID, scaling resource type, scaling resource ID, policy execution type, query start time, query end time, query start line number, and number of records. If no filter condition is specified, a maximum of 20 policy execution logs are queried by default.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. 
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. 
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The current login account has the permission to use the AS service.

6.You have created AS configurations, AS groups, and AS policies on AS.

7.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-as. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-as</artifactId>
    <version>3.1.123</version>
</dependency>
```

### Code example
``` java
package com.huawei.as;

import com.huaweicloud.sdk.as.v1.AsClient;
import com.huaweicloud.sdk.as.v1.model.ListScalingPolicyExecuteLogsRequest;
import com.huaweicloud.sdk.as.v1.model.ListScalingPolicyExecuteLogsResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.as.v1.region.AsRegion;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListScalingPolicyExecuteLogs {

    private static final Logger logger = LoggerFactory.getLogger(ListScalingPolicyExecuteLogs.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String scalingPolicyExecuteLogsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String scalingPolicyExecuteLogsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(scalingPolicyExecuteLogsAk)
                .withSk(scalingPolicyExecuteLogsSk);

        // Create a service client and set the corresponding region to AsRegion.CN_NORTH_4.
        AsClient client = AsClient.newBuilder()
                .withCredential(auth)
                .withRegion(AsRegion.CN_NORTH_4)
                .build();
        // Construct a request and set the request parameter AS policy ID.
        ListScalingPolicyExecuteLogsRequest request = new ListScalingPolicyExecuteLogsRequest();
        // Specifies the scaling policy ID.
        String scalingPolicyId = "<YOUR SCALING POLICY ID>";
        request.withScalingPolicyId(scalingPolicyId);
        try {
            ListScalingPolicyExecuteLogsResponse response = client.listScalingPolicyExecuteLogs(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
```
{
 "scaling_policy_execute_log": [
  {
   "id": "eab6****",
   "tenant_id": "16d5****",
   "scaling_resource_type": "SCALING_GROUP",
   "scaling_resource_id": "a34b****",
   "scaling_policy_id": "c1f3****",
   "old_value": "1",
   "desire_value": "1",
   "limit_value": "1",
   "execute_time": "2024-12-04T01:06:00Z",
   "status": "SUCCESS",
   "type": "ADD",
   "execute_type": "SCHEDULED",
   "job_records": [
    {
     "job_name": "ADJUST_VM_NUMBERS",
     "record_type": "MEG",
     "record_time": "2024-12-04T01:06:00Z",
     "message": "modify desire number of scaling group",
     "job_status": "SUCCESS"
    }
   ]
  }
 ],
 "total_number": 1,
 "limit": 20,
 "start_number": 0
}
```
### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/12/04 | 1.0 | This document is released for the first time.