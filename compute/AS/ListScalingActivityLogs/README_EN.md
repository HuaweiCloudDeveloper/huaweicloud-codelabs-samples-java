### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/AS/debug?api=ListScalingActivityLogs).

2.In this example, you can query scaling action logs.

3.Query scaling action logs based on the search criteria. The query results are displayed on multiple pages. You can filter and query records based on the start time, end time, start line number, and number of records. If no filter criteria are specified, a maximum of 20 scaling action logs can be queried by default.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. 
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. 
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The current login account has the permission to use the AS service.

6.An AS configuration and an AS group have been created in AS.

7.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-as. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-as</artifactId>
    <version>3.1.123</version>
</dependency>
```

### Code example
``` java
package com.huawei.as;

import com.huaweicloud.sdk.as.v1.AsClient;
import com.huaweicloud.sdk.as.v1.model.ListScalingActivityLogsRequest;
import com.huaweicloud.sdk.as.v1.model.ListScalingActivityLogsResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.as.v1.region.AsRegion;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListScalingActivityLogs {

    private static final Logger logger = LoggerFactory.getLogger(ListScalingActivityLogs.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listScalingActivityLogsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listScalingActivityLogsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listScalingActivityLogsAk)
                .withSk(listScalingActivityLogsSk);

        // Create a service client and set the corresponding region to AsRegion.CN_NORTH_4.
        AsClient client = AsClient.newBuilder()
                .withCredential(auth)
                .withRegion(AsRegion.CN_NORTH_4)
                .build();
        // Construct a request and set the request parameter AS group ID.
        ListScalingActivityLogsRequest request = new ListScalingActivityLogsRequest();
        // Specifies the AS group ID.
        String scalingGroupId = "<YOUR SCALING GROUP ID>";
        request.withScalingGroupId(scalingGroupId);
        try {
            ListScalingActivityLogsResponse response = client.listScalingActivityLogs(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
```
{
 "scaling_activity_log": [
  {
   "id": "fe95****",
   "instance_value": 0,
   "desire_value": 1,
   "scaling_value": 1,
   "start_time": "2024-11-20 19:30:33",
   "end_time": "2024-11-20 19:31:50",
   "instance_added_list": "as-config-4f7c-D2VITXE2",
   "instance_deleted_list": null,
   "instance_removed_list": null,
   "status": "SUCCESS",
   "description": "{\"reason\":[{\"change_reason\":\"DIFF\",\"old_value\":0,\"change_time\":\"2024-11-21T03:30:33Z\",\"new_value\":1}]}"
  }
 ],
 "total_number": 1,
 "limit": 20,
 "start_number": 0
}
```
### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/12/03 | 1.0 | This document is released for the first time.