package com.huawei.as;

import com.huaweicloud.sdk.as.v1.AsClient;
import com.huaweicloud.sdk.as.v1.model.ListScalingActivityLogsRequest;
import com.huaweicloud.sdk.as.v1.model.ListScalingActivityLogsResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.as.v1.region.AsRegion;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListScalingActivityLogs {

    private static final Logger logger = LoggerFactory.getLogger(ListScalingActivityLogs.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listScalingActivityLogsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listScalingActivityLogsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listScalingActivityLogsAk)
                .withSk(listScalingActivityLogsSk);

        // Create a service client and set the corresponding region to AsRegion.CN_NORTH_4.
        AsClient client = AsClient.newBuilder()
                .withCredential(auth)
                .withRegion(AsRegion.CN_NORTH_4)
                .build();
        // Construct a request and set the request parameter AS group ID.
        ListScalingActivityLogsRequest request = new ListScalingActivityLogsRequest();
        // Specifies the AS group ID.
        String scalingGroupId = "<YOUR SCALING GROUP ID>";
        request.withScalingGroupId(scalingGroupId);
        try {
            ListScalingActivityLogsResponse response = client.listScalingActivityLogs(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}