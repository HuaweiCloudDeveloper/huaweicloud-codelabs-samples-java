package com.huawei.functiongraph;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.functiongraph.v2.FunctionGraphClient;
import com.huaweicloud.sdk.functiongraph.v2.model.ListFunctionTemplateRequest;
import com.huaweicloud.sdk.functiongraph.v2.model.ListFunctionTemplateResponse;
import com.huaweicloud.sdk.functiongraph.v2.region.FunctionGraphRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListFunctionTemplate {

    private static final Logger logger = LoggerFactory.getLogger(ListFunctionTemplate.class.getName());

    public static void main(String[] args) throws InterruptedException {
        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential icredential = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // Create a service client and set the corresponding region to FunctionGraphRegion.CN_NORTH_4.
        FunctionGraphClient client = FunctionGraphClient.newBuilder()
            .withCredential(icredential)
            .withHttpConfig(httpConfig)
            .withRegion(FunctionGraphRegion.CN_NORTH_4)
            .build();
        // Create a query request header.
        ListFunctionTemplateRequest request = new ListFunctionTemplateRequest();
        // Start position of the query. Default value: 0 Default value: 0 Minimum length: 1 Maximum length: 64
        request.withMarker("1");
        // Maximum number of templates obtained in each query.
        request.withMaxitems("1");
        // Indicates whether the template is public.
        request.withIspublic("true");
        try {
            ListFunctionTemplateResponse response = client.listFunctionTemplate(request);
            // Print Results
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}