### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/FunctionGraph/doc?api=ShowFunctionTemplate).

2.In this example, you can obtain the specified function template.

3.You can obtain a specified function template for custom template management.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.FunctionGraph has been enabled.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-functiongraph. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-functiongraph</artifactId>
    <version>3.1.119</version>
</dependency>
```

### Code example

``` java
package com.huawei.functiongraph;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.functiongraph.v2.FunctionGraphClient;
import com.huaweicloud.sdk.functiongraph.v2.model.ShowFunctionTemplateRequest;
import com.huaweicloud.sdk.functiongraph.v2.model.ShowFunctionTemplateResponse;
import com.huaweicloud.sdk.functiongraph.v2.region.FunctionGraphRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowFunctionTemplate {

    private static final Logger logger = LoggerFactory.getLogger(ShowFunctionTemplate.class.getName());

    public static void main(String[] args) throws InterruptedException {
        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential icredential = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // Create a service client and set the corresponding region to FunctionGraphRegion.CN_NORTH_4.
        FunctionGraphClient client = FunctionGraphClient.newBuilder()
            .withCredential(icredential)
            .withHttpConfig(httpConfig)
            .withRegion(FunctionGraphRegion.CN_NORTH_4)
            .build();
        // Create a query request header.
        ShowFunctionTemplateRequest request = new ShowFunctionTemplateRequest();
        // Template ID. This parameter can be obtained from the ListFunctionTemplate API.
        // You can also use the template page link to obtain the value. The parameter location is as follows:
        // https://console.huaweicloud.com/functiongraph/?region=cn-north-4&locale=zh-cn#/serverless/functions/create?templateName={templateId}&from=template
        request.withTemplateId("templateId");
        try {
            ShowFunctionTemplateResponse response = client.showFunctionTemplate(request);
            // Print Results
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result

#### ShowFunctionTemplate

```
{
 "id": "f386ffb8-0e9f-419a-ade5************",
 "type": 1,
 "title": "title",
 "template_name": "template_name",
 "description": "description",
 "runtime": "PHP7.3",
 "handler": "index.handler",
 "code_type": "inline",
 "code": "************",
 "timeout": 10,
 "memory_size": 128,
 "trigger_metadata_list": null,
 "temp_detail": {
  "input": "无",
  "output": "output",
  "warning": "warning"
 },
 "user_data": "",
 "encrypted_user_data": "",
 "dependencies": null,
 "scene": "basic_function_usage",
 "service": "FunctionGraph"
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/10/30 | 1.0 | This document is released for the first time.