package com.huawei.functiongraph;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.functiongraph.v2.FunctionGraphClient;
import com.huaweicloud.sdk.functiongraph.v2.model.ShowFunctionTemplateRequest;
import com.huaweicloud.sdk.functiongraph.v2.model.ShowFunctionTemplateResponse;
import com.huaweicloud.sdk.functiongraph.v2.region.FunctionGraphRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowFunctionTemplate {

    private static final Logger logger = LoggerFactory.getLogger(ShowFunctionTemplate.class.getName());

    public static void main(String[] args) throws InterruptedException {
        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential icredential = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // Create a service client and set the corresponding region to FunctionGraphRegion.CN_NORTH_4.
        FunctionGraphClient client = FunctionGraphClient.newBuilder()
            .withCredential(icredential)
            .withHttpConfig(httpConfig)
            .withRegion(FunctionGraphRegion.CN_NORTH_4)
            .build();
        // Create a query request header.
        ShowFunctionTemplateRequest request = new ShowFunctionTemplateRequest();
        // Template ID. This parameter can be obtained from the ListFunctionTemplate API.
        // You can also use the template page link to obtain the value. The parameter location is as follows:
        // https://console.huaweicloud.com/functiongraph/?region=cn-north-4&locale=zh-cn#/serverless/functions/create?templateName={templateId}&from=template
        request.withTemplateId("80d2cf5f-6a1d-4442-b63a-f239e1bc5b82");
        try {
            ShowFunctionTemplateResponse response = client.showFunctionTemplate(request);
            // Print Results
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}