### 产品介绍

1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/FunctionGraph/doc?api=ListFunctions)
中直接运行调试该接口。

2.在本样例中，您可以获取函数列表。

3.我们可以获取函数列表，用于自定义页面函数管理。

### 前置条件

1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已开通FunctionGraph服务。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-functiongraph”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-functiongraph</artifactId>
    <version>3.1.119</version>
</dependency>
```

### 代码示例

``` java
package com.huawei.functiongraph;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.functiongraph.v2.FunctionGraphClient;
import com.huaweicloud.sdk.functiongraph.v2.model.ListFunctionsRequest;
import com.huaweicloud.sdk.functiongraph.v2.model.ListFunctionsResponse;
import com.huaweicloud.sdk.functiongraph.v2.region.FunctionGraphRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListFunctions {

    private static final Logger logger = LoggerFactory.getLogger(ListFunctions.class.getName());

    public static void main(String[] args) throws InterruptedException {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential icredential = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为FunctionGraphRegion.CN_NORTH_4
        FunctionGraphClient client = FunctionGraphClient.newBuilder()
            .withCredential(icredential)
            .withHttpConfig(httpConfig)
            .withRegion(FunctionGraphRegion.CN_NORTH_4)
            .build();
        // 创建查询请求头
        ListFunctionsRequest request = new ListFunctionsRequest();
        try {
            ListFunctionsResponse response = client.listFunctions(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例

#### ListFunctions

```
{
 "functions": [
  {
   "func_urn": "urn:fss:cn-north-4:08037e2e8a800fdf2ff4c0********:function:default:fss_examples_ima********",
   "func_name": "fss_examples_image_watermark",
   "domain_id": "05c7cfe66a8025e90f1dc********",
   "namespace": "08037e2e8a800fdf2ff4c********",
   "project_name": "cn-north-4",
   "package": "default",
   "runtime": "Python3.6",
   "timeout": 3,
   "handler": "index.handler",
   "memory_size": 128,
   "cpu": 300,
   "code_type": "inline",
   "code_filename": "index.zip",
   "code_size": 505,
   "digest": "55701b2b26fffb5b32454f480b0e82a05130866019df440450cc35912ad64****************",
   "version": "latest",
   "image_name": "latest-240927105********",
   "app_xrole": "",
   "last_modified": "2024-09-27T10:54:59+08:00",
   "func_code": {},
   "FuncCode": {},
   "strategy_config": {
    "concurrency": 400,
    "concurrent_num": 1
   },
   "enterprise_project_id": "0",
   "long_time": true,
   "log_config": {
    "group_name": "",
    "group_id": "",
    "stream_name": "",
    "stream_id": "",
    "switch_time": 0
   },
   "type": "v2",
   "enable_cloud_debug": "DISABLED",
   "enable_dynamic_memory": false,
   "custom_image": {},
   "is_stateful_function": false,
   "is_bridge_function": false,
   "is_return_stream": false,
   "stream_response": false,
   "is_websocket_function": false,
   "enable_auth_in_header": false,
   "reserved_instance_idle_mode": false,
   "enable_snapshot": true,
   "ephemeral_storage": 512,
   "network_controller": {
    "disable_public_network": false,
    "trigger_access_vpcs": null
   },
   "is_shared": false,
   "enable_class_isolation": false,
   "resource_id": "dac8714e-75ba-430b-8cc3-e622********.fss_examples_image_wa********",
   "custom_health_check_config": {
    "http_get_url": "",
    "initial_delay_seconds": 0,
    "period_seconds": 0,
    "timeout_seconds": 0,
    "failure_threshold": 0,
    "success_threshold": 0
   },
   "disable_insecure_ak_sk": false,
   "status": 0
  }
 ],
 "next_marker": 1,
 "count": 1
}
```

### 修订记录

    发布日期    | 文档版本 | 修订说明

:----------:|:----:| :------:  
2024/10/29 | 1.0 | 文档首次发布