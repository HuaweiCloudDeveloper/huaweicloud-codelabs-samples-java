### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/FunctionGraph/doc?api=ListAppTemplates).

2.In this example, you can get a list of functions.

3.You can obtain the function list for managing customized page functions.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.FunctionGraph has been enabled.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-functiongraph. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-functiongraph</artifactId>
    <version>3.1.119</version>
</dependency>
```

### Code example

``` java
package com.huawei.functiongraph;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.functiongraph.v2.FunctionGraphClient;
import com.huaweicloud.sdk.functiongraph.v2.model.ListFunctionsRequest;
import com.huaweicloud.sdk.functiongraph.v2.model.ListFunctionsResponse;
import com.huaweicloud.sdk.functiongraph.v2.region.FunctionGraphRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListFunctions {

    private static final Logger logger = LoggerFactory.getLogger(ListFunctions.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential icredential = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // Create a service client and set the corresponding region to FunctionGraphRegion.CN_NORTH_4.
        FunctionGraphClient client = FunctionGraphClient.newBuilder()
            .withCredential(icredential)
            .withHttpConfig(httpConfig)
            .withRegion(FunctionGraphRegion.CN_NORTH_4)
            .build();
        // Create a query request header.
        ListFunctionsRequest request = new ListFunctionsRequest();
        try {
            ListFunctionsResponse response = client.listFunctions(request);
            // Print Results
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result

#### ListFunctions

```
{
 "functions": [
  {
   "func_urn": "urn:fss:cn-north-4:08037e2e8a800fdf2ff4c0********:function:default:fss_examples_ima********",
   "func_name": "fss_examples_image_watermark",
   "domain_id": "05c7cfe66a8025e90f1dc********",
   "namespace": "08037e2e8a800fdf2ff4c********",
   "project_name": "cn-north-4",
   "package": "default",
   "runtime": "Python3.6",
   "timeout": 3,
   "handler": "index.handler",
   "memory_size": 128,
   "cpu": 300,
   "code_type": "inline",
   "code_filename": "index.zip",
   "code_size": 505,
   "digest": "55701b2b26fffb5b32454f480b0e82a05130866019df440450cc35912ad64****************",
   "version": "latest",
   "image_name": "latest-240927105********",
   "app_xrole": "",
   "last_modified": "2024-09-27T10:54:59+08:00",
   "func_code": {},
   "FuncCode": {},
   "strategy_config": {
    "concurrency": 400,
    "concurrent_num": 1
   },
   "enterprise_project_id": "0",
   "long_time": true,
   "log_config": {
    "group_name": "",
    "group_id": "",
    "stream_name": "",
    "stream_id": "",
    "switch_time": 0
   },
   "type": "v2",
   "enable_cloud_debug": "DISABLED",
   "enable_dynamic_memory": false,
   "custom_image": {},
   "is_stateful_function": false,
   "is_bridge_function": false,
   "is_return_stream": false,
   "stream_response": false,
   "is_websocket_function": false,
   "enable_auth_in_header": false,
   "reserved_instance_idle_mode": false,
   "enable_snapshot": true,
   "ephemeral_storage": 512,
   "network_controller": {
    "disable_public_network": false,
    "trigger_access_vpcs": null
   },
   "is_shared": false,
   "enable_class_isolation": false,
   "resource_id": "dac8714e-75ba-430b-8cc3-e622********.fss_examples_image_wa********",
   "custom_health_check_config": {
    "http_get_url": "",
    "initial_delay_seconds": 0,
    "period_seconds": 0,
    "timeout_seconds": 0,
    "failure_threshold": 0,
    "success_threshold": 0
   },
   "disable_insecure_ak_sk": false,
   "status": 0
  }
 ],
 "next_marker": 1,
 "count": 1
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/10/29 | 1.0 | This document is released for the first time.