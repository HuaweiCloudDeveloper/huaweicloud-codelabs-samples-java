## 0. 版本说明
本示例基于华为云SDK V3.0版本开发

## 1. 介绍
华为云提供了消息通知服务SDK，您可以通过集成服务端SDK来调用消息通知相关服务，通过消息发送功能来实现您的消息通知。消息发送包含多种发送方式，如邮件、短信、http消息等。

该示例展示了如何通过java版SDK发送消息。

## 2. 前置条件
### 2.1 获取AK/SK
开发者在使用前需先获取账号的ak、sk、endpoint、projectId。

您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证-访问密钥”页面上创建和查看您的 AK/SK。更多信息请查看[访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)。

endpoint 华为云各服务应用区域和各服务的终端节点，详情请查看 [地区和终端节点](https://developer.huaweicloud.com/endpoint)。

projectId 云服务所在项目 ID ，根据你想操作的项目所属区域选择对应的 [项目ID](https://support.huaweicloud.com/api-smn/smn_api_66000.html)。

### 2.2 运行环境
Java JDK 1.8 及其以上版本。

### 2.3 SDK获取和安装
开发者可以通过Maven方式获取和安装SDK，包括“huaweicloud-sdk-core”和“huaweicloud-sdk-smn”。SDK版本号可以参考： [SDK中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/SMN?lang=Java) 。

需要在Java项目的pom.xml文件中加入如下的依赖项。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-smn</artifactId>
    <version>3.1.70</version>
</dependency>
```

## 3. 示例代码
```java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.smn.v2.SmnClient;
import com.huaweicloud.sdk.smn.v2.model.PublishMessageRequest;
import com.huaweicloud.sdk.smn.v2.model.PublishMessageRequestBody;
import com.huaweicloud.sdk.smn.v2.model.PublishMessageResponse;
import com.huaweicloud.sdk.smn.v2.region.SmnRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PublishMessage {
    private static final Logger LOGGER = LoggerFactory.getLogger(PublishMessage.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 初始化认证
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        // 初始化消息通知服务（SMN）客户端
        SmnClient client = SmnClient.newBuilder()
                // 配置认证
                .withCredential(auth)
                // 配置region，也可以使用withEndPoint选择您需要的endpoint
                .withRegion(SmnRegion.CN_EAST_2)
                .build();

        // 构造发布消息的请求
        PublishMessageRequest request = new PublishMessageRequest()
                // 您的发布消息的主题urn
                .withTopicUrn("yourTopicUrn")
                // 发布消息的请求体
                .withBody(
                        new PublishMessageRequestBody()
                                // 发布消息的内容
                                .withMessage("yourMessage"));
        try {
            // 接收发布消息的响应
            PublishMessageResponse response = client.publishMessage(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException e) {
            LOGGER.error(e.getMessage());
        } catch (RequestTimeoutException e) {
            LOGGER.error(e.getMessage());
        } catch (ServiceResponseException e) {
            LOGGER.error("HttpStatusCode: {}", e.getHttpStatusCode());
            LOGGER.error("RequestId: {}", e.getRequestId());
            LOGGER.error("ErrorCode: {}", e.getErrorCode());
            LOGGER.error("ErrorMsg: {}", e.getErrorMsg());
        }
    }
}
```

## 4. 参考
更多信息请参考[消息通知服务-发送消息](https://support.huaweicloud.com/api-smn/PublishMessage.html)

## 5. 修订记录
|    发布日期    | 文档版本 |  修订说明  |
|:----------:|:----:|:------:|
| 2021-11-24 | 1.0  | 文档首次发布 |
| 2023-12-06 | 1.1  | 更新示例代码 |
