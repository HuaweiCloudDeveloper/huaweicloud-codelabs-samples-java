/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2023. All rights reserved.
 */

package com.huawei.smn.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.smn.v2.SmnClient;
import com.huaweicloud.sdk.smn.v2.model.PublishMessageRequest;
import com.huaweicloud.sdk.smn.v2.model.PublishMessageRequestBody;
import com.huaweicloud.sdk.smn.v2.model.PublishMessageResponse;
import com.huaweicloud.sdk.smn.v2.region.SmnRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PublishMessage {
    private static final Logger LOGGER = LoggerFactory.getLogger(PublishMessage.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Initialize authentication.
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        // Initializing the Simple Message Notification (SMN) Client.
        SmnClient client = SmnClient.newBuilder()
                // Configure authentication
                .withCredential(auth)
                // Configure a region. You can also use withEndPoint to select an endpoint.
                .withRegion(SmnRegion.CN_EAST_2)
                .build();

        // Construct the request for publishing a message.
        PublishMessageRequest request = new PublishMessageRequest()
                // Your topic urn for publishing a message.
                .withTopicUrn("yourTopicUrn")
                // Request body for publishing a message.
                .withBody(
                        new PublishMessageRequestBody()
                                // The content of your message
                                .withMessage("yourMessage"));
        try {
            // Receive the response to publishing a message.
            PublishMessageResponse response = client.publishMessage(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException e) {
            LOGGER.error(e.getMessage());
        } catch (RequestTimeoutException e) {
            LOGGER.error(e.getMessage());
        } catch (ServiceResponseException e) {
            LOGGER.error("HttpStatusCode: {}", e.getHttpStatusCode());
            LOGGER.error("RequestId: {}", e.getRequestId());
            LOGGER.error("ErrorCode: {}", e.getErrorCode());
            LOGGER.error("ErrorMsg: {}", e.getErrorMsg());
        }
    }
}