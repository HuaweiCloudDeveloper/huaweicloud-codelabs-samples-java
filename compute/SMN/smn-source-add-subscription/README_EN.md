## 0. Version Description
This example is developed based on Huawei Cloud SDK V3.0.

## 1. Introduction
HUAWEI CLOUD provides the SMN SDK. You can use the SDK to invoke SMN-related services. You can add subscriptions to a created topic. When you send messages in the topic, all confirmed subscriptions in the topic will be notified. Subscription involves multiple protocols, such as SMS, email, and HTTP. After you invoke the Add Subscription service, the added terminal will receive a subscription confirmation notification when the network and terminal services are normal. Click the link in the notification to confirm the subscription. Message notifications that you publish can only be sent to endpoints that have confirmed subscriptions.

This example shows how to add a subscription using the Java SDK.

## 2. Preparations
### 2.1 Obtaining the AK/SK
Before using the account, you need to obtain the AK, SK, endpoint, and projectId of the account.

You must have a HUAWEI CLOUD account and the corresponding Access Key (AK) and Secret Access Key (SK). On the HUAWEI CLOUD console, choose My Credential > Access Keys to create and view your AK/SK. For more information, see [the access key](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html).

Endpoint: Regions and endpoints of HUAWEI CLOUD services. For details, see [Regions and Endpoints](https://developer.huaweicloud.com/endpoint).

projectId: ID of the project to which the cloud service belongs. Select the [project ID](https://support.huaweicloud.com/api-smn/smn_api_66000.html) based on the region to which the project belongs.

### 2.2 Running environment
Java JDK 1.8 or later.

### 2.3 SDK Installation
You can use Maven to obtain and install SDKs, including huaweicloud-sdk-core and huaweicloud-sdk-smn. For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/SMN?lang=Java).

Add the following dependency items to the **pom.xml** file of the Java project:
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-smn</artifactId>
    <version>3.1.70</version>
</dependency>
```

## 3. Sample Code
```java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.smn.v2.SmnClient;
import com.huaweicloud.sdk.smn.v2.model.AddSubscriptionRequest;
import com.huaweicloud.sdk.smn.v2.model.AddSubscriptionRequestBody;
import com.huaweicloud.sdk.smn.v2.model.AddSubscriptionResponse;
import com.huaweicloud.sdk.smn.v2.region.SmnRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddSubscription {
    private static final Logger LOGGER = LoggerFactory.getLogger(AddSubscription.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Initialize authentication.
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        // Initializing the Simple Message Notification (SMN) Client.
        SmnClient client = SmnClient.newBuilder()
                // Configure authentication
                .withCredential(auth)
                // Configure a region. You can also use withEndPoint to select an endpoint.
                .withRegion(SmnRegion.CN_EAST_2)
                .build();

        // Construct the request for adding a subscription.
        AddSubscriptionRequest request = new AddSubscriptionRequest()
                // Your topic urn for adding a subscription.
                .withTopicUrn("yourTopicUrn")
                // Request body for adding a subscription.
                .withBody(
                        new AddSubscriptionRequestBody()
                                // Subscription protocol, for example, email, sms or http.
                                .withProtocol("email")
                                // In this example, the email protocol is used. Here, enter the email address of the terminal.
                                .withEndpoint("yourEmail@huawei.com"));
        try {
            // Receive the response to adding a subscription.
            AddSubscriptionResponse response = client.addSubscription(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException e) {
            LOGGER.error(e.getMessage());
        } catch (RequestTimeoutException e) {
            LOGGER.error(e.getMessage());
        } catch (ServiceResponseException e) {
            LOGGER.error("HttpStatusCode: {}", e.getHttpStatusCode());
            LOGGER.error("RequestId: {}", e.getRequestId());
            LOGGER.error("ErrorCode: {}", e.getErrorCode());
            LOGGER.error("ErrorMsg: {}", e.getErrorMsg());
        }
    }
}
```

## 4. References
For more information, see [Simple Message Notification (SMN) - Subscription](https://support.huaweicloud.com/api-smn/AddSubscription.html)

## 5. Change History
| Released On | Version |       Description       |
|:-----------:|:-------:|:-----------------------:|
| 2021-11-24  |   1.0   |     Initial release     |
| 2023-12-06  |   1.1   | Updated the Sample Code |
