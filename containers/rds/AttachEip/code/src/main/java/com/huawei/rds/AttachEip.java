/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.rds;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.RdsClient;
import com.huaweicloud.sdk.rds.v3.model.AttachEipRequest;
import com.huaweicloud.sdk.rds.v3.model.BindEipRequest;
import com.huaweicloud.sdk.rds.v3.model.AttachEipResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AttachEip {
    private static final Logger logger = LoggerFactory.getLogger(AttachEip.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = "<YOUR INSTANCE ID>"; // RDS实例ID
        // true,绑定弹性公网IP。
        // false,解绑弹性公网IP。
        boolean isBind = false;
        // 弹性公网IP对应的ID,仅允许使用标准的UUID格式。isBind为true时必选
        String publicIpId = "<YOUR PUBLIC_IP ID>";
        // 待绑定的弹性公网IP地址,仅允许使用标准的IP地址格式。isBind为true时必选
        String publicIp = "<YOUR PUBLIC IP>";

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头,实例ID
        AttachEipRequest request = new AttachEipRequest();
        request.withInstanceId(instanceId);
        BindEipRequest body = new BindEipRequest();
        body.withIsBind(isBind);
        body.withPublicIpId(publicIpId);
        body.withPublicIp(publicIp);
        request.withBody(body);
        try {
            AttachEipResponse response = client.attachEip(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
