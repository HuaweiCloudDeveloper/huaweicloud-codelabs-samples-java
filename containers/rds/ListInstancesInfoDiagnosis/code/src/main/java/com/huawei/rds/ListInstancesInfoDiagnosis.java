/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.rds;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.RdsClient;
import com.huaweicloud.sdk.rds.v3.model.ListInstancesInfoDiagnosisRequest;
import com.huaweicloud.sdk.rds.v3.model.ListInstancesInfoDiagnosisResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListInstancesInfoDiagnosis {
    private static final Logger logger = LoggerFactory.getLogger(ListInstancesInfoDiagnosis.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String engine = "<YOUR ENGINE>"; // 引擎类型。枚举值：sqlserver/mysql/postgresql
        String diagnosis = "<YOUR DIAGNOSIS>"; // 诊断项。枚举值：high_pressure/lock_wait/insufficient_capacity/slow_sql_frequency/disk_performance_cap/mem_overrun/age_exceed/connections_exceed
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头
        ListInstancesInfoDiagnosisRequest request = new ListInstancesInfoDiagnosisRequest();
        request.withEngine(ListInstancesInfoDiagnosisRequest.EngineEnum.fromValue(engine));
        request.withDiagnosis(ListInstancesInfoDiagnosisRequest.DiagnosisEnum.fromValue(diagnosis));
        try {
            ListInstancesInfoDiagnosisResponse response = client.listInstancesInfoDiagnosis(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}