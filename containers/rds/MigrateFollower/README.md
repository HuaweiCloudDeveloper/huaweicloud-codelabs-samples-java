### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RDS/sdk?api=MigrateFollower) 中直接运行调试该接口。

2.在本样例中，您可以迁移RDS主备实例的备机。

3.RDS数据库可变更规格后，您可根据业务需要，进行备机可用区的迁移。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已购买RDS实例。

6.调用接口前，您需要了解API [认证鉴权](https://support.huaweicloud.com/api-rds/rds_03_0001.html)。

7.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-rds”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rds</artifactId>
    <version>3.1.112</version>
</dependency>
```
### 接口约束
1. 该接口仅支持MySQL和PostgreSQL引擎。
2. RDS for MySQL 5.6、5.7、8.0版本的主备实例支持备机可用区迁移功能。其中，RDS for MySQL 5.6和5.7版本的本地盘实例备机可用区迁移，以及8.0版本的备机可用区迁移功能需要联系客服人员申请权限。
3. 仅支持HA实例。
4. 实例在创建、重启、数据库升级、变更规格、修改端口、创建用户、删除用户状态下，不能进行此操作。
### 代码示例
以下代码展示如何迁移RDS主备实例的备机：
``` java
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.rds;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.RdsClient;
import com.huaweicloud.sdk.rds.v3.model.MigrateFollowerRequest;
import com.huaweicloud.sdk.rds.v3.model.FollowerMigrateRequest;
import com.huaweicloud.sdk.rds.v3.model.MigrateFollowerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MigrateFollower {
    private static final Logger logger = LoggerFactory.getLogger(MigrateFollower.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = "<YOUR INSTANCE ID>"; // RDS实例ID
        String azCode = "<YOUR AZ CODE>"; // 要迁入的可用区code,如：cn-north-4b
        String nodeId = "<YOUR NODE ID>"; // 备机节点Id
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        RdsClient client = RdsClient.newBuilder()
                .withRegion(RdsRegion.CN_NORTH_4)
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头,实例ID
        MigrateFollowerRequest request = new MigrateFollowerRequest();
        request.withInstanceId(instanceId);
        FollowerMigrateRequest body = new FollowerMigrateRequest();
        body.withAzCode(azCode);
        body.withNodeId(nodeId);
        request.withBody(body);
        try {
            MigrateFollowerResponse response = client.migrateFollower(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
 
```

### 返回结果示例
#### MigrateFollower
```
{
 "workflowId": "532a3a2d-xxxx-xxxx-xxxx-xxxxb451e943"
}
```
### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: |:----:| :------:  
 2024/09/20 | 1.0  | 文档首次发布
