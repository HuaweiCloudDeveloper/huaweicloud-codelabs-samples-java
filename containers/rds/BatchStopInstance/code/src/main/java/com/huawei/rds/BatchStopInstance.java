/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.rds;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.RdsClient;
import com.huaweicloud.sdk.rds.v3.model.BatchStopInstanceRequest;
import com.huaweicloud.sdk.rds.v3.model.BatchShutdownInsReq;
import com.huaweicloud.sdk.rds.v3.model.BatchStopInstanceResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class BatchStopInstance {
    private static final Logger logger = LoggerFactory.getLogger(BatchStopInstance.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = "<YOUR INSTANCE ID>"; // RDS实例ID
        List<String> listbodyInstanceIds = new ArrayList<>(); // 待停止的RDS实例ID列表
        listbodyInstanceIds.add(instanceId);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        ICredential auth = new BasicCredentials().withAk(ak)
                .withSk(sk);
        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头
        BatchStopInstanceRequest request = new BatchStopInstanceRequest();
        BatchShutdownInsReq body = new BatchShutdownInsReq();
        body.withInstanceIds(listbodyInstanceIds);
        request.withBody(body);
        try {
            BatchStopInstanceResponse response = client.batchStopInstance(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}