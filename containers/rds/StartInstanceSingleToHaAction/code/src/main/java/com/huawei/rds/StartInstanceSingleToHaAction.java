/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.rds;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rds.v3.model.StartInstanceSingleToHaActionRequest;
import com.huaweicloud.sdk.rds.v3.model.Single2Ha;
import com.huaweicloud.sdk.rds.v3.model.ADDomainInfo;
import com.huaweicloud.sdk.rds.v3.model.Single2HaObject;
import com.huaweicloud.sdk.rds.v3.model.StartInstanceSingleToHaActionResponse;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.RdsClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StartInstanceSingleToHaAction {
    private static final Logger logger = LoggerFactory.getLogger(StartInstanceSingleToHaAction.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = "<YOUR INSTANCE ID>"; // RDS实例ID
        String adminAccountName = "<YOUR ADMIN ACCOUNT NAME>"; // 备实例管理员账号
        String adminPwd = "<YOUR ADMIN PASSWORD>"; // 备实例管理员密码
        String azCodeNewNode = "<YOUR AZ CODE NEW NODE>"; // 备实例可用区，如：cn-north-4g
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        RdsClient client = RdsClient.newBuilder()
                .withRegion(RdsRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .withCredential(auth)
                .build();
        // 构建请求头
        StartInstanceSingleToHaActionRequest request = new StartInstanceSingleToHaActionRequest();
        request.withInstanceId(instanceId);
        Single2Ha body = new Single2Ha();
        ADDomainInfo adDomainInfoSingleToHa = new ADDomainInfo();
        adDomainInfoSingleToHa.withDomainAdminAccountName(adminAccountName)
                .withDomainAdminPwd(adminPwd);
        Single2HaObject singleToHabody = new Single2HaObject();
        singleToHabody.withAzCodeNewNode(azCodeNewNode)
                .withAdDomainInfo(adDomainInfoSingleToHa);
        body.withSingleToHa(singleToHabody);
        request.withBody(body);
        try {
            StartInstanceSingleToHaActionResponse response = client.startInstanceSingleToHaAction(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}