/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.rds;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.RdsClient;
import com.huaweicloud.sdk.rds.v3.model.StartInstanceEnlargeVolumeActionRequest;
import com.huaweicloud.sdk.rds.v3.model.EnlargeVolumeRequestBody;
import com.huaweicloud.sdk.rds.v3.model.EnlargeVolumeObject;
import com.huaweicloud.sdk.rds.v3.model.StartInstanceEnlargeVolumeActionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StartInstanceEnlargeVolumeAction {
    private static final Logger logger = LoggerFactory.getLogger(StartInstanceEnlargeVolumeAction.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String securityToken = System.getenv("HWCLOUD_SECURITY_TOKEN"); // Use in temporary aksk scene
        String instanceId = "YOUR InstanceId"; // RDS实例ID
        // 扩容后实例磁盘的目标大小。
        // 每次扩容最小容量为10GB,实例所选容量大小必须为10的整数倍,取值范围:40GB~4000GB。
        int size = 60;
        ICredential auth = new BasicCredentials()
                .withSecurityToken(securityToken) // Use in temporary aksk scene
                .withAk(ak)
                .withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头
        StartInstanceEnlargeVolumeActionRequest request = new StartInstanceEnlargeVolumeActionRequest();
        request.withInstanceId(instanceId);
        EnlargeVolumeRequestBody body = new EnlargeVolumeRequestBody();
        EnlargeVolumeObject enlargeVolumebody = new EnlargeVolumeObject();
        // 扩容后实例磁盘的目标大小。
        enlargeVolumebody.withSize(size);
        body.withEnlargeVolume(enlargeVolumebody);
        request.withBody(body);
        try {
            StartInstanceEnlargeVolumeActionResponse response = client.startInstanceEnlargeVolumeAction(request);
            logger.info(response.toString());
        }  catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
