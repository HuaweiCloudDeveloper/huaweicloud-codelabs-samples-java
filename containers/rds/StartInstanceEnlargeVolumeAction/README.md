### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RDS/doc?api=StartInstanceEnlargeVolumeAction) 中直接运行调试该接口。

2.在本样例中，您可以为指定RDS实例扩容磁盘空间。

3.通过本样例，您可以根据RDS实例磁盘实际使用情况，为实例磁盘进行扩容，避免数据存储空间不足。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已购买RDS实例。

6.调用接口前，您需要了解API [认证鉴权](https://support.huaweicloud.com/api-rds/rds_03_0001.html)。

7.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-rds”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

### 接口约束
1.备实例大小和主实例大小一致，当主实例扩容时，会同时扩容备实例。

2.实例状态为“正常”和“存储空间满”时可以进行扩容。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rds</artifactId>
    <version>3.1.112</version>
</dependency>
```

### 代码示例
以下代码展示如何扩容数据库实例的磁盘空间：
``` java
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.rds;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.RdsClient;
import com.huaweicloud.sdk.rds.v3.model.StartInstanceEnlargeVolumeActionRequest;
import com.huaweicloud.sdk.rds.v3.model.EnlargeVolumeRequestBody;
import com.huaweicloud.sdk.rds.v3.model.EnlargeVolumeObject;
import com.huaweicloud.sdk.rds.v3.model.StartInstanceEnlargeVolumeActionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StartInstanceEnlargeVolumeAction {
    private static final Logger logger = LoggerFactory.getLogger(StartInstanceEnlargeVolumeAction.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String securityToken = System.getenv("HWCLOUD_SECURITY_TOKEN"); // Use in temporary aksk scene
        String instanceId = "YOUR InstanceId"; // RDS实例ID
        // 扩容后实例磁盘的目标大小。
        // 每次扩容最小容量为10GB,实例所选容量大小必须为10的整数倍,取值范围:40GB~4000GB。
        int size = 60;
        ICredential auth = new BasicCredentials()
                .withSecurityToken(securityToken) // Use in temporary aksk scene
                .withAk(ak)
                .withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头
        StartInstanceEnlargeVolumeActionRequest request = new StartInstanceEnlargeVolumeActionRequest();
        request.withInstanceId(instanceId);
        EnlargeVolumeRequestBody body = new EnlargeVolumeRequestBody();
        EnlargeVolumeObject enlargeVolumebody = new EnlargeVolumeObject();
        // 扩容后实例磁盘的目标大小。
        enlargeVolumebody.withSize(size);
        body.withEnlargeVolume(enlargeVolumebody);
        request.withBody(body);
        try {
            StartInstanceEnlargeVolumeActionResponse response = client.startInstanceEnlargeVolumeAction(request);
            logger.info(response.toString());
        }  catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
 
 
```

### 返回结果示例
#### StartInstanceEnlargeVolumeAction
```
{
    jobId: 737a38de-d4af-4784-abda-927b8db56418
    orderId: null
}
```
### 运行结果验证
运行前RDS实例磁盘大小为50G.
![img.png](assets/before.png)
运行后RDS实例磁盘大小为60G.
![img.png](assets/after.png)
### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: |:----:| :------:  
 2024/09/02 | 1.0  | 文档首次发布