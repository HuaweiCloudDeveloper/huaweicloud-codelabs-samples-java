### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RDS/doc?api=ModifyPostgresqlHbaConf) 中直接运行调试该接口。

2.在本样例中，您可以修改pg_hba.conf文件的单个或多个配置。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已购买RDS实例。

6.调用接口前，您需要了解API [认证鉴权](https://support.huaweicloud.com/api-rds/rds_03_0001.html)。

7.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-rds”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rds</artifactId>
    <version>3.1.112</version>
</dependency>
```
### 代码示例
以下代码展示如何修改pg_hba.conf文件的单个或多个配置：
``` java
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.rds;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.RdsClient;
import com.huaweicloud.sdk.rds.v3.model.ModifyPostgresqlHbaConfRequest;
import com.huaweicloud.sdk.rds.v3.model.PostgresqlHbaConf;
import com.huaweicloud.sdk.rds.v3.model.ModifyPostgresqlHbaConfResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ModifyPostgresqlHbaConf {
    private static final Logger logger = LoggerFactory.getLogger(ModifyPostgresqlHbaConf.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = "<YOUR INSTANCE ID>"; // RDS实例ID
        String type = "host"; // 连接类型。枚举值：host、hostssl、hostnossl
        String database = "all"; // 数据库名。除template0，template1的数据库名，多个以逗号隔开。
        String user = "all"; // 除内置用户（rdsAdmin, rdsMetric, rdsBackup, rdsRepl, rdsProxy）以外的用户名。
        String address = "0.0.0.0/0"; // 客户端IP地址。0.0.0.0/0表示允许用户从任意IP地址访问数据库。
        String method = "reject"; // 认证方式。枚举值：reject、md5、scram-sha-256
        Integer priority = 0; // 优先级，表示配置的先后。 修改或新增pg_hba.conf文件配置，以priority作为唯一标识。
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        RdsClient client = RdsClient.newBuilder()
                .withRegion(RdsRegion.CN_NORTH_4)
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头,实例ID
        ModifyPostgresqlHbaConfRequest request = new ModifyPostgresqlHbaConfRequest();
        request.withInstanceId(instanceId);
        List<PostgresqlHbaConf> listbodyBody = new ArrayList<>();
        listbodyBody.add(
                new PostgresqlHbaConf()
                        .withType(type)
                        .withDatabase(database)
                        .withUser(user)
                        .withAddress(address)
                        .withMethod(method)
                        .withPriority(priority)
        );
        request.withBody(listbodyBody);
        try {
            ModifyPostgresqlHbaConfResponse response = client.modifyPostgresqlHbaConf(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
 
```

### 返回结果示例
#### ModifyPostgresqlHbaConf
```
{ 
  "code" : 0, 
  "message" : "" 
}
```
### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: |:----:| :------:  
 2024/11/27 | 1.0  | 文档首次发布
