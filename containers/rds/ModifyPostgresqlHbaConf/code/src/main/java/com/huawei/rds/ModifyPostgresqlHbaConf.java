/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.rds;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.RdsClient;
import com.huaweicloud.sdk.rds.v3.model.ModifyPostgresqlHbaConfRequest;
import com.huaweicloud.sdk.rds.v3.model.PostgresqlHbaConf;
import com.huaweicloud.sdk.rds.v3.model.ModifyPostgresqlHbaConfResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ModifyPostgresqlHbaConf {
    private static final Logger logger = LoggerFactory.getLogger(ModifyPostgresqlHbaConf.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = "<YOUR INSTANCE ID>"; // RDS实例ID
        String type = "host"; // 连接类型。枚举值：host、hostssl、hostnossl
        String database = "all"; // 数据库名。除template0，template1的数据库名，多个以逗号隔开。
        String user = "all"; // 除内置用户（rdsAdmin, rdsMetric, rdsBackup, rdsRepl, rdsProxy）以外的用户名。
        String address = "0.0.0.0/0"; // 客户端IP地址。0.0.0.0/0表示允许用户从任意IP地址访问数据库。
        String method = "reject"; // 认证方式。枚举值：reject、md5、scram-sha-256
        Integer priority = 0; // 优先级，表示配置的先后。 修改或新增pg_hba.conf文件配置，以priority作为唯一标识。
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        RdsClient client = RdsClient.newBuilder()
                .withRegion(RdsRegion.CN_NORTH_4)
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头,实例ID
        ModifyPostgresqlHbaConfRequest request = new ModifyPostgresqlHbaConfRequest();
        request.withInstanceId(instanceId);
        List<PostgresqlHbaConf> listbodyBody = new ArrayList<>();
        listbodyBody.add(
                new PostgresqlHbaConf()
                        .withType(type)
                        .withDatabase(database)
                        .withUser(user)
                        .withAddress(address)
                        .withMethod(method)
                        .withPriority(priority)
        );
        request.withBody(listbodyBody);
        try {
            ModifyPostgresqlHbaConfResponse response = client.modifyPostgresqlHbaConf(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}