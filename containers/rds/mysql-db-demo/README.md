## 1. 示例简介
华为云关系型数据库（Relational Database Service，简称RDS）是一种基于云计算平台的即开即用、稳定可靠、弹性伸缩、便捷管理的在线关系型数据库服务。 本示例展示如何通过接口调用方式，获取审计日志下载链接

## 2. 开发前准备

1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。


## 3. 安装SDK
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rds</artifactId>
    <version>3.1.5</version>
</dependency>
```

## 4. 代码示例
以下代码展示如何使用SDK获取审计日志下载链接：

```java
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huaweicloud.rds;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.rds.v3.RdsClient;
import com.huaweicloud.sdk.rds.v3.model.GenerateAuditlogDownloadLinkRequest;
import com.huaweicloud.sdk.rds.v3.model.ListAuditlogsRequest;
import com.huaweicloud.sdk.rds.v3.model.ListAuditlogsResponse;
import com.huaweicloud.sdk.rds.v3.model.ShowAuditlogDownloadLinkRequest;
import com.huaweicloud.sdk.rds.v3.model.ShowAuditlogDownloadLinkResponse;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;

/**
 * rds获取审计日志下载链接
 *
 * @since 2023-08-26
 */
public class ShowAuditlogDownloadLink {
    private static final Logger logger = LoggerFactory.getLogger(MysqlDbDemo.class.getName());

    private static final Region REGION = RdsRegion.CN_EAST_3; // 实例所在的Region

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = "<YOUR INSTANCE_ID>";
        String startTime = "<YOUR START_TIME>"; // ex: 2023-08-07T00:00:00+0800
        String endTime = "<YOUR END_TIME>"; // ex: 2023-08-08T18:09:38+0800;
        int offset = 0; // <offset value>
        int limit = 10; // <YOUR LIMIT>

        RdsClient client = createClient(ak, sk);
        // 获取审计日志id列表，根据审计日志列表，查询审计日志下载链接
        List<String> ids = queryAuditLogIds(client, instanceId, startTime, endTime, offset, limit);
        // 获取审计日志下载链接
        showAuditlogDownloadLink(client,instanceId,ids);
    }

    private static List<String> queryAuditLogIds(RdsClient client, String instanceId, String startTime,
        String endTime, int offset, int limit) {
        ListAuditlogsRequest request = new ListAuditlogsRequest();
        //设置实例id，起始时间，结束时间
        request.setInstanceId(instanceId);
        request.setStartTime(startTime);
        request.setEndTime(endTime);
        request.setOffset(offset);
        request.setLimit(limit);
        ListAuditlogsResponse response;
        List<String> ids = new ArrayList<>();
        //获取审计日志id列表
        try {
            response = client.listAuditlogs(request);
            logger.info(response.toString());
            for (int i = 0; i < response.getAuditlogs().size(); i++) {
                ids.add(response.getAuditlogs().get(i).getId());
            }
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return ids;
    }
    private static RdsClient createClient(String ak, String sk) {
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        return RdsClient.newBuilder().withCredential(auth).withRegion(REGION).build();
    }

    public static void showAuditlogDownloadLink(RdsClient client, String instanceId,  List<String> ids) {
        ShowAuditlogDownloadLinkRequest request = new ShowAuditlogDownloadLinkRequest()
            .withInstanceId(instanceId)
            .withBody(
                new GenerateAuditlogDownloadLinkRequest().withIds(ids)
            );
        invokeClient(() -> {
            ShowAuditlogDownloadLinkResponse response = client.showAuditlogDownloadLink(request);
            logger.info(response.toString());
        });
    }

    private static <T> void invokeClient(Runnable runnable) {
        try {
            runnable.run();
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(),
                e.getErrorMsg());
        }
    }
}


```
## 5.返回结果示例
- 获取审计日志下载链接接口的返回值：
```
{
	"links": ["https://obs.domainname.com/rdsbucket.username.1/xxxxxx", "https://obs.domainname.com/rdsbucket.username.2/xxxxxx"]
}
```

## 6.参考链接

请见 [获取审计日志下载链接](https://support.huaweicloud.com/api-rds/rds_05_0004.html)
您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RDS/doc?api=ShowAuditlogDownloadLink) 中直接运行调试该接口。

## 修订记录
|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2022-10-30 |   1.0    | 文档首次发布 |
 