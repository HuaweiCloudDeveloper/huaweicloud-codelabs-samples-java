/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huaweicloud.rds;


import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.rds.v3.RdsClient;
import com.huaweicloud.sdk.rds.v3.model.GenerateAuditlogDownloadLinkRequest;
import com.huaweicloud.sdk.rds.v3.model.ListAuditlogsRequest;
import com.huaweicloud.sdk.rds.v3.model.ListAuditlogsResponse;
import com.huaweicloud.sdk.rds.v3.model.ShowAuditlogDownloadLinkRequest;
import com.huaweicloud.sdk.rds.v3.model.ShowAuditlogDownloadLinkResponse;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * RDS obtains the links for downloading audit logs.
 *
 * @since 2023-08-26
 */
public class ShowAuditlogDownloadLink {
    private static final Logger logger = LoggerFactory.getLogger(ShowAuditlogDownloadLink.class.getName());

    private static final Region REGION = RdsRegion.CN_EAST_3; // Region where the instance is located

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security purposes, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = "<YOUR INSTANCE_ID>";
        String startTime = "<YOUR START_TIME>"; // ex: 2023-08-07T00:00:00+0800
        String endTime = "<YOUR END_TIME>"; // ex: 2023-08-08T18:09:38+0800;
        int offset = 0; // <offset value>
        int limit = 10; // <YOUR LIMIT>

        RdsClient client = createClient(ak, sk);
        // Obtain the audit log ID list, which is used for querying the audit log download links.
        List<String> ids = queryAuditLogIds(client, instanceId, startTime, endTime, offset, limit);
        // Obtain the links for downloading audit logs.
        showAuditlogDownloadLink(client,instanceId,ids);
    }

    private static List<String> queryAuditLogIds(RdsClient client, String instanceId, String startTime,
        String endTime, int offset, int limit) {
        ListAuditlogsRequest request = new ListAuditlogsRequest();
        // Set the instance ID, start time, and end time.
        request.setInstanceId(instanceId);
        request.setStartTime(startTime);
        request.setEndTime(endTime);
        request.setOffset(offset);
        request.setLimit(limit);
        ListAuditlogsResponse response;
        List<String> ids = new ArrayList<>();
        // Obtain the audit log ID list.
        try {
            response = client.listAuditlogs(request);
            logger.info(response.toString());
            for (int i = 0; i < response.getAuditlogs().size(); i++) {
                ids.add(response.getAuditlogs().get(i).getId());
            }
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(), e.getErrorMsg());
        }
        return ids;
    }
    private static RdsClient createClient(String ak, String sk) {
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        return RdsClient.newBuilder().withCredential(auth).withRegion(REGION).build();
    }

    public static void showAuditlogDownloadLink(RdsClient client, String instanceId,  List<String> ids) {
        ShowAuditlogDownloadLinkRequest request = new ShowAuditlogDownloadLinkRequest()
            .withInstanceId(instanceId)
            .withBody(
                new GenerateAuditlogDownloadLinkRequest().withIds(ids)
            );
        invokeClient(() -> {
            ShowAuditlogDownloadLinkResponse response = client.showAuditlogDownloadLink(request);
            logger.info(response.toString());
        });
    }

    private static <T> void invokeClient(Runnable runnable) {
        try {
            runnable.run();
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(),
                e.getErrorMsg());
        }
    }
}

