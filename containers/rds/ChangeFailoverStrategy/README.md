### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RDS/sdk?api=ChangeFailoverStrategy) 中直接运行调试该接口。

2.在本样例中，您可以切换主备实例的倒换策略。

3.云数据库 RDS服务支持切换主备实例的可用性策略，以满足不同业务需求。可选择 “可靠性优先”或者“可用性优先”两种策略。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已购买至少一个主备类型的RDS实例。

6.调用接口前，您需要了解API [认证鉴权](https://support.huaweicloud.com/api-rds/rds_03_0001.html)。

7.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-rds”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-rds</artifactId>
    <version>3.1.112</version>
</dependency>
```
### 接口约束
1. 该接口仅支持MySQL引擎。
2. 仅支持主备实例，即HA实例。
3. 实例在创建、数据库升级、创建用户、删除用户状态下不能进行此操作。
### 代码示例
以下代码展示如何切换主备实例的倒换策略：
``` java
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.rds;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rds.v3.model.ChangeFailoverStrategyRequest;
import com.huaweicloud.sdk.rds.v3.model.FailoverStrategyRequest;
import com.huaweicloud.sdk.rds.v3.model.ChangeFailoverStrategyResponse;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.RdsClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChangeFailoverStrategy {
    private static final Logger logger = LoggerFactory.getLogger(ChangeFailoverStrategy.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = "<YOUR INSTANCE ID>"; // RDS实例ID
        //可用性策略,可选择如下方式:
        //reliability:可靠性优先,数据库应该尽可能保障数据的可靠性,即数据丢失量最少。对于数据一致性要求较高的业务,建议选择该策略。
        //availability:可用性优先,数据库应该可快恢复服务,即可用时间最长。对于数据库在线时间要求较高的业务,建议选择该策略。
        String repairStrategy = "<YOUR REPAIR STRATEGY>";
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        ICredential auth = new BasicCredentials().withAk(ak)
                .withSk(sk);
        RdsClient client = RdsClient.newBuilder()
                .withHttpConfig(httpConfig)
                .withCredential(auth)
                .withRegion(RdsRegion.CN_NORTH_4)
                .build();
        // 构建请求头
        ChangeFailoverStrategyRequest request = new ChangeFailoverStrategyRequest();
        request.withInstanceId(instanceId);
        FailoverStrategyRequest body = new FailoverStrategyRequest();
        body.withRepairStrategy(repairStrategy);
        request.withBody(body);
        try {
            ChangeFailoverStrategyResponse response = client.changeFailoverStrategy(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
 
```

### 返回结果示例
#### ChangeFailoverStrategy
```
{
}
```
### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: |:----:| :------:  
 2024/09/20 | 1.0  | 文档首次发布