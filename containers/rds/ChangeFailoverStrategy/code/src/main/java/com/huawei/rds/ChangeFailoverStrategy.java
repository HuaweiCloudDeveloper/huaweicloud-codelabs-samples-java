/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.rds;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.rds.v3.model.ChangeFailoverStrategyRequest;
import com.huaweicloud.sdk.rds.v3.model.FailoverStrategyRequest;
import com.huaweicloud.sdk.rds.v3.model.ChangeFailoverStrategyResponse;
import com.huaweicloud.sdk.rds.v3.region.RdsRegion;
import com.huaweicloud.sdk.rds.v3.RdsClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChangeFailoverStrategy {
    private static final Logger logger = LoggerFactory.getLogger(ChangeFailoverStrategy.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String instanceId = "<YOUR INSTANCE ID>"; // RDS实例ID
        // 可用性策略,可选择如下方式:
        // reliability:可靠性优先,数据库应该尽可能保障数据的可靠性,即数据丢失量最少。对于数据一致性要求较高的业务,建议选择该策略。
        // availability:可用性优先,数据库应该可快恢复服务,即可用时间最长。对于数据库在线时间要求较高的业务,建议选择该策略。
        String repairStrategy = "<YOUR REPAIR STRATEGY>";
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        ICredential auth = new BasicCredentials().withAk(ak)
                .withSk(sk);
        RdsClient client = RdsClient.newBuilder()
                .withCredential(auth)
                .withRegion(RdsRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头
        ChangeFailoverStrategyRequest request = new ChangeFailoverStrategyRequest();
        request.withInstanceId(instanceId);
        FailoverStrategyRequest body = new FailoverStrategyRequest();
        body.withRepairStrategy(repairStrategy);
        request.withBody(body);
        try {
            ChangeFailoverStrategyResponse response = client.changeFailoverStrategy(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}