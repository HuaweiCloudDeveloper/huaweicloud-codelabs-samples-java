## 1. 示例简介

华为云提供了[云容器引擎 CCE SDK](https://sdkcenter.developer.huaweicloud.com/?product=CCE)，您可以直接集成SDK来调用相关API，从而实现对云容器引擎服务的快速操作。

该示例展示了使用云容器引擎服务时，如何删除一个已经开启了集群删除保护的集群。

## 2. 开发前准备

- 已注册华为云，并完成实名认证。
- 已在华为云控制台授权使用CCE服务。
- 具备开发环境 ，支持Java JDK 1.8及以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见访问密钥。
- 已获取CCE服务对应区域的项目ID，请在华为云控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见API凭证。
- 已创建可用的虚拟私有云和子网，请在华为云控制台“网络控制台 >虚拟私有云”页面上查看虚拟私有云ID和子网ID。

## 3. 安装SDK

[云容器引擎 CCE SDK](https://sdkcenter.developer.huaweicloud.com/?product=CCE)支持Java JDK 1.8及其以上版本。

通过Maven配置所依赖的云容器引擎服务SDK，具体的SDK版本号请参见[SDK开发中心](https://sdkcenter.developer.huaweicloud.com/?language=java)。

``` xml
# 配置CCE服务依赖
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cce</artifactId>
    <version>${version}</version>
</dependency>
```

## 4. 代码示例
以下代码展示如何创建一个集群同时开启集群删除保护，再通过集群更新接口来关闭集群保护，最后删除这个集群。
``` java

public class DeleteClusterEnabledDeletionProtection {

    private final static String apiVersion = "v3";

    private final static String clusterApiKind = "Cluster";

    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    static String accessKey = System.getenv("HUAWEICLOUD_SDK_AK");  // 华为云账号Access Key
    static String secretKey = System.getenv("HUAWEICLOUD_SDK_SK");  // 华为云账号Secret Access Key
    /* 以下部分请替换成用户实际参数 */
    static String projectId = "<YOUR PROJECT ID>";     // 华为云账号项目ID
    static String userRegion = "<YOUR REGION>";        // 服务所在区域，eg cn-north-4

    // 创建集群相关参数
    static String clusterName = "<YOUR ClUSTER NAME>";      // 新建集群的名称, eg: test-cluster
    static String vpc = "<YOUR VPC ID>";                    // 新建集群使用的虚拟私有云ID
    static String subnet = "<YOUR SUBNET ID>";              // 新建集群使用的子网ID
    static String flavor = "cce.s1.small";                  // 集群规格
    static String version = "v1.30";                        // 集群版本
    static String eniSubnetId = "<YOUR ipv4 SUBNET ID>";    // turbo集群使用的ipv4子网ID
    static boolean deletionProtection = true;               // 集群删除保护

    public static void main(String[] args) {
        // 初始化客户端
        CceClient client = initClient();

        // 创建集群(开启集群删除保护)
        String clusterId = createClusterAndWaitReady(client);

        // 更新集群,关闭集群删除保护
        UpdateClusterResponse updateResponse = client.updateCluster(getUpdateDeletionProtectionRequest(clusterId, false));
        System.out.println("update cluster response:" + updateResponse);

        // 删除集群
        deleteClusterAndWaitReady(clusterId, client);
    }

    // 初始化客户端
    static CceClient initClient() {
        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(accessKey)
                .withSk(secretKey)
                .withProjectId(projectId);

        // 创建服务客户端
        return CceClient.newBuilder()
                .withCredential(auth)
                .withRegion(CceRegion.valueOf(userRegion))
                .build();

    }

    // 创建集群并等待完成,返回集群UID
    static String createClusterAndWaitReady(CceClient client) {
        try {
            // 发起创建集群请求
            CreateClusterResponse response = client.createCluster(getCreateClusterRequest());
            System.out.println(response.toString());
            // 等待集群创建完成
            waitJobReady(response.getStatus().getJobID(), client, "create cluster");
            return response.getMetadata().getUid();
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
            throw e;
        }
    }

    // 创建集群请求
    static CreateClusterRequest getCreateClusterRequest() {
        // 设置集群配置
        ClusterSpec spec = new ClusterSpec()
                .withType(ClusterSpec.TypeEnum.VIRTUALMACHINE)
                .withFlavor(flavor)
                .withVersion(version)
                .withHostNetwork(new HostNetwork()
                        .withVpc(vpc)
                        .withSubnet(subnet))
                .withContainerNetwork(new ContainerNetwork()
                        .withMode(ContainerNetwork.ModeEnum.ENI))
                .withEniNetwork(new EniNetwork()
                        .withEniSubnetId(eniSubnetId))
                .withDeletionProtection(deletionProtection);

        Cluster cluster = new Cluster()
                .withKind(clusterApiKind)
                .withApiVersion(apiVersion)
                .withMetadata(new ClusterMetadata()
                        .withName(clusterName))
                .withSpec(spec);

        // 创建集群请求
        return new CreateClusterRequest()
                .withBody(cluster);
    }

    static UpdateClusterRequest getUpdateDeletionProtectionRequest(String clusterId, boolean deletionProtection) {
        return new UpdateClusterRequest().withClusterId(clusterId)
                .withBody(new ClusterInformation().withSpec(
                        new ClusterInformationSpec().withDeletionProtection(deletionProtection)));
    }

    // 删除集群并等待集群删除完成
    static void deleteClusterAndWaitReady(String clusterId, CceClient client) {
        try {
            // 发起删除集群请求
            DeleteClusterResponse response = client.deleteCluster(getDeleteClusterRequest(clusterId));
            System.out.println("delete cluster response:" + response.toString());
            // 等待集群删除完成
            waitJobReady(response.getStatus().getJobID(), client, "delete cluster");
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
            throw e;
        }
    }

    static DeleteClusterRequest getDeleteClusterRequest(String clusterId) {
        return new DeleteClusterRequest().withClusterId(clusterId)
                .withDeleteEfs(DeleteClusterRequest.DeleteEfsEnum.TRUE)
                .withDeleteEni(DeleteClusterRequest.DeleteEniEnum.TRUE)
                .withDeleteEvs(DeleteClusterRequest.DeleteEvsEnum.TRUE)
                .withDeleteNet(DeleteClusterRequest.DeleteNetEnum.TRUE)
                .withDeleteObs(DeleteClusterRequest.DeleteObsEnum.TRUE)
                .withDeleteSfs(DeleteClusterRequest.DeleteSfsEnum.TRUE)
                .withDeleteSfs30(DeleteClusterRequest.DeleteSfs30Enum.TRUE)
                .withOndemandNodePolicy(DeleteClusterRequest.OndemandNodePolicyEnum.DELETE)
                .withPeriodicNodePolicy(DeleteClusterRequest.PeriodicNodePolicyEnum.RETAIN);
    }

    // 查询任务执行状态，等待任务成功
    static void waitJobReady(String jobId, CceClient client, String jobDesc) throws RuntimeException {
        int tryTimes = 180;
        while (tryTimes-- > 0) {
            ShowJobRequest request = new ShowJobRequest();
            request.setJobId(jobId);
            ShowJobResponse response = client.showJob(request);
            if (response != null && response.getStatus() != null && response.getStatus().getPhase() != null) {
                String phase = response.getStatus().getPhase();
                System.out.printf("Wait %s ready, job is %s\n", jobDesc, phase);
                if ("Success".equals(phase)) {
                    System.out.printf("%s successfully!\n", jobDesc);
                    return;
                } else if ("Failed".equals(phase)) {
                    System.out.printf("%s failed!", jobDesc);
                    throw new RuntimeException(jobDesc + " failed");
                }
            }
            try {
                Thread.sleep(10 * 1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

```

## 5. 参考

更多信息请参考：[云容器引擎 CCE 文档](https://support.huaweicloud.com/cce/)

## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-08-18 | 1.0 | 文档首次发布 |