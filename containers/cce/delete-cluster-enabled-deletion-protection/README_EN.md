## 1. Sample Overview
 
Huawei Cloud offers [CCE SDK] (https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?product=CCE). You can use SDK to call APIs to perform operations on CCE clusters.
 
This sample shows how to delete a cluster enabled deletion protection using CCE.

## 2. Preparations
 
- You have registered with Huawei Cloud and completed real-name authentication.
- You have been authorized to use CCE on the Huawei Cloud console.
- The development environment (Java JDK 1.8 and later) is available.
- You have obtained the Access Key (AK) and Secret Access Key (SK) of the Huawei Cloud account. You can create and view your AK/SK on the My Credentials > Access Keys page on the Huawei Cloud console. For details, see Access Keys.
- You have obtained the project ID of the corresponding region of CCE. You can view the project ID on the My Credentials > API Credentials page on the Huawei Cloud console. For details, see API Credentials.
- An available VPC and subnet have been created. You can view the VPC ID and subnet ID on the Virtual Private Cloud page on the network console.
 
## 3. Installing SDK

[CCE SDK] (https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?product=CCE) supports Java JDK 1.8 and later.

Configuring the Dependent CCE SDK Using Maven，For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java)。

``` xml
# Configuring CCE Service Dependency
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cce</artifactId>
    <version>${version}</version>
</dependency>
```

## 4. Sample Code
The following code shows how to create a CCE cluster and to enable deletion protection, then disable deletion protection with cluster update API, finally delete the cluster.
``` java

public class DeleteClusterEnabledDeletionProtection {

    private final static String apiVersion = "v3";

    private final static String clusterApiKind  = "Cluster";

    /* Replace the following parameters with actual ones. */
    // Client-related parameters
    // There will be security risks if the AK/SK used for authentication is directly written into code. To minimize the risk, store the AK and SK in ciphertext in the configuration file or environment variables and decrypt them for using.
    // In this example, the AK and SK are stored in environment variables. Before running this example, configure environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    static String ak = System.getenv("HUAWEICLOUD_SDK_AK");   // Access key of the Huawei Cloud account
    static String sk = System.getenv("HUAWEICLOUD_SDK_SK");   // Secret access key of the Huawei Cloud account
    static String projectId = "<YOUR PROJECT ID>";     //Project ID of the Huawei Cloud account
    static String userRegion = "<YOUR REGION>";        // Region where the service is located, for example, cn-north-4

    // Parameters for creating a cluster
    static String clusterName = "<YOUR ClUSTER NAME>";      // Name of a cluster, for example, test-cluster
    static String vpc = "<YOUR VPC ID>";                    // ID of the VPC used by a cluster
    static String subnet = "<YOUR SUBNET ID>";              // ID of the subnet used by a cluster
    static String flavor = "cce.s1.small";                  // Flavor of a cluster
    static String version = "v1.30";                        // Version of a cluster
    static String eniSubnetId = "<YOUR ipv4 SUBNET ID>";    // ID of the IPv4 subnet used by a CCE Turbo cluster
    static boolean deletionProtection = true;               // Cluster deletion protection

    public static void main(String[] args) {
        // initialize the client.
        CceClient client = initClient();

        // create a cluster.(enable cluster deletion protection)
        String clusterId = createClusterAndWaitReady(client);

        // update the cluster to disable cluster deletion protection
        UpdateClusterResponse updateResponse = client.updateCluster(getUpdateDeletionProtectionRequest(clusterId, false));
        System.out.println("update cluster response:" + updateResponse);

        // delete cluster
        deleteClusterAndWaitReady(clusterId, client);
    }

    static CceClient initClient() {

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        return CceClient.newBuilder()
                .withCredential(auth)
                .withRegion(CceRegion.valueOf(userRegion))
                .build();

    }

    // Create a cluster and wait until the creation is complete. The cluster UID is returned.
    static String createClusterAndWaitReady(CceClient client) {
        try {
            // Create a cluster.
            CreateClusterResponse response = client.createCluster(getCreateClusterRequest());
            System.out.println(response.toString());
            waitJobReady(response.getStatus().getJobID(), client, "create cluster");
            return response.getMetadata().getUid();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    // Request for creating a cluster
    static CreateClusterRequest getCreateClusterRequest() {
        // Configure cluster settings.
        ClusterSpec spec = new ClusterSpec()
                .withType(ClusterSpec.TypeEnum.VIRTUALMACHINE)
                .withFlavor(flavor)
                .withVersion(version)
                .withHostNetwork(new HostNetwork()
                        .withVpc(vpc)
                        .withSubnet(subnet))
                .withContainerNetwork(new ContainerNetwork()
                        .withMode(ContainerNetwork.ModeEnum.ENI))
                .withEniNetwork(new EniNetwork()
                        .withEniSubnetId(eniSubnetId))
                .withDeletionProtection(deletionProtection);

        Cluster cluster = new Cluster()
                .withKind(clusterApiKind)
                .withApiVersion(apiVersion)
                .withMetadata(new ClusterMetadata()
                        .withName(clusterName))
                .withSpec(spec);

        // Create a cluster creation request.
        return new CreateClusterRequest()
                .withBody(cluster);
    }

    static UpdateClusterRequest getUpdateDeletionProtectionRequest(String clusterId, boolean deletionProtection) {
        return new UpdateClusterRequest().withClusterId(clusterId)
                .withBody(new ClusterInformation().withSpec(
                        new ClusterInformationSpec().withDeletionProtection(deletionProtection)));
    }

    // Delete a cluster and wait until the deletion is complete.
    static void deleteClusterAndWaitReady(String clusterId, CceClient client) {
        try {
            // request for deleting a cluster
            DeleteClusterResponse response = client.deleteCluster(getDeleteClusterRequest(clusterId));
            System.out.println("delete cluster response:" + response.toString());
            // wait until the deletion is complete.
            waitJobReady(response.getStatus().getJobID(), client, "delete cluster");
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
            throw e;
        }
    }

    static DeleteClusterRequest getDeleteClusterRequest(String clusterId) {
        return new DeleteClusterRequest().withClusterId(clusterId)
                .withDeleteEfs(DeleteClusterRequest.DeleteEfsEnum.TRUE)
                .withDeleteEni(DeleteClusterRequest.DeleteEniEnum.TRUE)
                .withDeleteEvs(DeleteClusterRequest.DeleteEvsEnum.TRUE)
                .withDeleteNet(DeleteClusterRequest.DeleteNetEnum.TRUE)
                .withDeleteObs(DeleteClusterRequest.DeleteObsEnum.TRUE)
                .withDeleteSfs(DeleteClusterRequest.DeleteSfsEnum.TRUE)
                .withDeleteSfs30(DeleteClusterRequest.DeleteSfs30Enum.TRUE)
                .withOndemandNodePolicy(DeleteClusterRequest.OndemandNodePolicyEnum.DELETE)
                .withPeriodicNodePolicy(DeleteClusterRequest.PeriodicNodePolicyEnum.RETAIN);
    }

    // Obtain the execution status of a job and wait until the job is complete.
    static void waitJobReady(String jobId, CceClient client, String jobDesc) throws RuntimeException {
        int tryTimes = 180;
        while (tryTimes-- > 0) {
            ShowJobRequest request = new ShowJobRequest();
            request.setJobId(jobId);
            ShowJobResponse response = client.showJob(request);
            if (response != null && response.getStatus() != null && response.getStatus().getPhase() != null) {
                String phase = response.getStatus().getPhase();
                System.out.printf("Wait %s ready, job is %s\n", jobDesc, phase);
                if ("Success".equals(phase)) {
                    System.out.printf("%s successfully!\n", jobDesc);
                    return;
                } else if ("Failed".equals(phase)) {
                    System.out.printf("%s failed!", jobDesc);
                    throw new RuntimeException(jobDesc + " failed");
                }
            }
            try {
                Thread.sleep(10 * 1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

```

## 5. Reference
 
For more information, see [Cloud Container Engine Documentation] (https://support.huaweicloud.com/intl/en-us/function-cce/index.html).
 
## 6. Change History
 
|    Release Date    | Version | Description |
|:----------:| :------: | :----------: |
| 2023-08-18 | 1.0 | This issue is first release. |