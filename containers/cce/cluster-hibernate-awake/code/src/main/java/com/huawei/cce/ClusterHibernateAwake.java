package com.huawei.cce;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.cce.v3.region.CceRegion;
import com.huaweicloud.sdk.cce.v3.CceClient;
import com.huaweicloud.sdk.cce.v3.model.HibernateClusterRequest;
import com.huaweicloud.sdk.cce.v3.model.HibernateClusterResponse;
import com.huaweicloud.sdk.cce.v3.model.AwakeClusterRequest;
import com.huaweicloud.sdk.cce.v3.model.AwakeClusterResponse;

public class ClusterHibernateAwake {

    /* 以下部分请替换成用户实际参数 */
    // 客户端相关参数
    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    static String accessKey = System.getenv("HUAWEICLOUD_SDK_AK");             // 华为云账号Access Key
    static String secretKey = System.getenv("HUAWEICLOUD_SDK_SK");             // 华为云账号Secret Access Key
    static String userRegion = "<YOUR REGION>";                                // 服务所在区域，eg cn-north-4
    static String clusterId = "<YOUR CLUSTER ID>";                             // 集群ID
    static String projectId = "<YOUR PROJECT ID>";                             // 项目ID

    static CceClient initClient() {
        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(accessKey)
                .withSk(secretKey)
                .withProjectId(projectId);

        // 创建服务客户端
        return CceClient.newBuilder()
                .withCredential(auth)
                .withRegion(CceRegion.valueOf(userRegion))
                .build();
    }

    // 集群休眠
    static void hibernateCluster(String clusterId, CceClient client) {
        HibernateClusterRequest hibernateRequest = new HibernateClusterRequest();
        hibernateRequest.withClusterId(clusterId);
        try {
            HibernateClusterResponse hibernateResponse = client.hibernateCluster(hibernateRequest);
            System.out.println(hibernateResponse.toString());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
            throw e;
        }
    }

    // 集群唤醒
    static void awakeCluster(String clusterId, CceClient client) {
        AwakeClusterRequest awakeRequest = new AwakeClusterRequest();
        awakeRequest.withClusterId(clusterId);
        try {
            AwakeClusterResponse awakeResponse = client.awakeCluster(awakeRequest);
            System.out.println(awakeResponse.toString());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
            throw e;
        }
    }

    public static void main(String[] args) {

        ICredential auth = new BasicCredentials()
                .withAk(accessKey)
                .withSk(secretKey)
                .withProjectId(projectId);;

        CceClient client = initClient();
        hibernateCluster(clusterId, client);
        awakeCluster(clusterId, client);
    }
}
