## 1. 示例简介

华为云提供了[云容器引擎 CCE SDK](https://sdkcenter.developer.huaweicloud.com/?product=CCE)，您可以直接集成SDK来调用相关API，从而实现对云容器引擎服务的快速操作。

该示例展示了如何使用云容器引擎服务休眠和唤醒集群。

**注意：集群唤醒后，将继续收取集群管理费用。**

## 2. 开发前准备

- 已注册华为云，并完成实名认证。
- 已在华为云控制台授权使用CCE服务。
- 具备开发环境 ，支持Java JDK 1.8及以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见访问密钥。
- 已获取CCE服务对应区域的项目ID，请在华为云控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见API凭证。
- 已创建可用的虚拟私有云和子网，请在华为云控制台“网络控制台 >虚拟私有云”页面上查看虚拟私有云ID和子网ID。

## 3. 安装SDK

[云容器引擎 CCE SDK](https://sdkcenter.developer.huaweicloud.com/?product=CCE)支持Java JDK 1.8及其以上版本。

通过Maven配置所依赖的云容器引擎服务SDK，具体的SDK版本号请参见[SDK开发中心](https://sdkcenter.developer.huaweicloud.com/?language=java)。

``` xml
# 配置CCE服务依赖
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cce</artifactId>
    <version>${version}</version>
</dependency>
```

## 4. 代码示例
以下代码展示如何休眠和唤醒集群。
``` java

package com.huawei.cce;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.cce.v3.region.CceRegion;
import com.huaweicloud.sdk.cce.v3.CceClient;
import com.huaweicloud.sdk.cce.v3.model.HibernateClusterRequest;
import com.huaweicloud.sdk.cce.v3.model.HibernateClusterResponse;
import com.huaweicloud.sdk.cce.v3.model.AwakeClusterRequest;
import com.huaweicloud.sdk.cce.v3.model.AwakeClusterResponse;

public class ClusterHibernateAwake {

    /* 以下部分请替换成用户实际参数 */
    // 客户端相关参数
    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    static String accessKey = System.getenv("HUAWEICLOUD_SDK_AK");             // 华为云账号Access Key
    static String secretKey = System.getenv("HUAWEICLOUD_SDK_SK");             // 华为云账号Secret Access Key
    static String userRegion = "<YOUR REGION>";                                // 服务所在区域，eg cn-north-4
    static String clusterId = "<YOUR CLUSTER ID>";                             // 集群ID
    static String projectId = "<YOUR PROJECT ID>";                             // 项目ID

    static CceClient initClient() {
        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(accessKey)
                .withSk(secretKey)
                .withProjectId(projectId);

        // 创建服务客户端
        return CceClient.newBuilder()
                .withCredential(auth)
                .withRegion(CceRegion.valueOf(userRegion))
                .build();
    }

    // 集群休眠
    static void hibernateCluster(String clusterId, CceClient client) {
        HibernateClusterRequest hibernateRequest = new HibernateClusterRequest();
        hibernateRequest.withClusterId(clusterId);
        try {
            HibernateClusterResponse hibernateResponse = client.hibernateCluster(hibernateRequest);
            System.out.println(hibernateResponse.toString());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
            throw e;
        }
    }

    // 集群唤醒
    static void awakeCluster(String clusterId, CceClient client) {
        AwakeClusterRequest awakeRequest = new AwakeClusterRequest();
        awakeRequest.withClusterId(clusterId);
        try {
            AwakeClusterResponse awakeResponse = client.awakeCluster(awakeRequest);
            System.out.println(awakeResponse.toString());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
            throw e;
        }
    }

    public static void main(String[] args) {

        ICredential auth = new BasicCredentials()
                .withAk(accessKey)
                .withSk(secretKey)
                .withProjectId(projectId);;

        CceClient client = initClient();
        hibernateCluster(clusterId, client);
        awakeCluster(clusterId, client);
    }
}


```

## 5. 参考

更多信息请参考：[云容器引擎 CCE 文档](https://support.huaweicloud.com/cce/)

## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-11-16 | 1.0 | 文档首次发布 |