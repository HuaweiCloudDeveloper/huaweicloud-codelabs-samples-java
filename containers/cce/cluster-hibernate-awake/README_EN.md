## 1. Sample Overview
 
Huawei Cloud offers [CCE SDK] (https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?product=CCE). You can use SDK to call APIs to perform operations on CCE clusters.
 
This sample shows how to hibernate and wake up a CCE cluster.
 
**Note: After a cluster is woken up, it will continue to be charged.**
 
## 2. Preparations
 
- You have registered with Huawei Cloud and completed real-name authentication.
- You have been authorized to use CCE on the Huawei Cloud console.
- The development environment (Java JDK 1.8 and later) is available.
- You have obtained the Access Key (AK) and Secret Access Key (SK) of the Huawei Cloud account. You can create and view your AK/SK on the My Credentials > Access Keys page on the Huawei Cloud console. For details, see Access Keys.
- You have obtained the project ID of the corresponding region of CCE. You can view the project ID on the My Credentials > API Credentials page on the Huawei Cloud console. For details, see API Credentials.
- An available VPC and subnet have been created. You can view the VPC ID and subnet ID on the Virtual Private Cloud page on the network console.

## 3. Installing SDK

[CCE SDK] (https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?product=CCE) supports Java JDK 1.8 and later.

Configuring the Dependent CCE SDK Using Maven，For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java)。

``` xml
# Configuring CCE Service Dependency
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cce</artifactId>
    <version>${version}</version>
</dependency>
```

## 4. Sample Code
The following code shows how to hibernate and wake up a cluster.
``` java

package com.huawei.cce;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.cce.v3.region.CceRegion;
import com.huaweicloud.sdk.cce.v3.CceClient;
import com.huaweicloud.sdk.cce.v3.model.HibernateClusterRequest;
import com.huaweicloud.sdk.cce.v3.model.HibernateClusterResponse;
import com.huaweicloud.sdk.cce.v3.model.AwakeClusterRequest;
import com.huaweicloud.sdk.cce.v3.model.AwakeClusterResponse;

public class ClusterHibernateAwake {

    /* Replace the following parameters with actual ones. */
    // Client-related parameters
    // There will be security risks if the AK/SK used for authentication is directly written into code. To minimize the risk, store the AK and SK in ciphertext in the configuration file or environment variables and decrypt them for using.
    // In this example, the AK and SK are stored in environment variables. Before running this example, configure environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    static String accessKey = System.getenv("HUAWEICLOUD_SDK_AK");             // Access key of the Huawei Cloud account
    static String secretKey = System.getenv("HUAWEICLOUD_SDK_SK");             // Secret access key of the Huawei Cloud account
    static String userRegion = "<YOUR REGION>";                                // Region where the service is located, for example, cn-north-4
    static String clusterId = "<YOUR CLUSTER ID>";                             // Cluster ID
    static String projectId = "<YOUR PROJECT ID>";                             // Project ID

    static CceClient initClient() {

        ICredential auth = new BasicCredentials()
                .withAk(accessKey)
                .withSk(secretKey)
                .withProjectId(projectId);


        return CceClient.newBuilder()
                .withCredential(auth)
                .withRegion(CceRegion.valueOf(userRegion))
                .build();
    }

    // Hibernate a cluster.
    static void hibernateCluster(String clusterId, CceClient client) {
        HibernateClusterRequest hibernateRequest = new HibernateClusterRequest();
        hibernateRequest.withClusterId(clusterId);
        try {
            HibernateClusterResponse hibernateResponse = client.hibernateCluster(hibernateRequest);
            System.out.println(hibernateResponse.toString());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
            throw e;
        }
    }

    // Wake up a cluster.
    static void awakeCluster(String clusterId, CceClient client) {
        AwakeClusterRequest awakeRequest = new AwakeClusterRequest();
        awakeRequest.withClusterId(clusterId);
        try {
            AwakeClusterResponse awakeResponse = client.awakeCluster(awakeRequest);
            System.out.println(awakeResponse.toString());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
            throw e;
        }
    }

    public static void main(String[] args) {

        ICredential auth = new BasicCredentials()
                .withAk(accessKey)
                .withSk(secretKey)
                .withProjectId(projectId);;

        CceClient client = initClient();
        hibernateCluster(clusterId, client);
        awakeCluster(clusterId, client);
    }
}


```

## 5. Reference
 
For more information, see [Cloud Container Engine Documentation] (https://support.huaweicloud.com/intl/en-us/function-cce/index.html).
 
## 6. Change History
 
|    Release Date    | Version | Description |
|:----------:| :------: | :----------: |
| 2023-11-16 | 1.0 | This issue is first release. 