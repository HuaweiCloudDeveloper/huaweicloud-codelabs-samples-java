package com.huawei.cce;

import java.util.ArrayList;

import com.huaweicloud.sdk.cce.v3.CceClient;
import com.huaweicloud.sdk.cce.v3.model.CertDuration;
import com.huaweicloud.sdk.cce.v3.model.Cluster;
import com.huaweicloud.sdk.cce.v3.model.ClusterSpec;
import com.huaweicloud.sdk.cce.v3.model.ContainerNetwork;
import com.huaweicloud.sdk.cce.v3.model.CreateKubernetesClusterCertRequest;
import com.huaweicloud.sdk.cce.v3.model.CreateKubernetesClusterCertResponse;
import com.huaweicloud.sdk.cce.v3.model.CreateNodePoolRequest;
import com.huaweicloud.sdk.cce.v3.model.CreateNodePoolResponse;
import com.huaweicloud.sdk.cce.v3.model.CreateNodeRequest;
import com.huaweicloud.sdk.cce.v3.model.CreateNodeResponse;
import com.huaweicloud.sdk.cce.v3.model.EniNetwork;
import com.huaweicloud.sdk.cce.v3.model.HostNetwork;
import com.huaweicloud.sdk.cce.v3.model.ClusterMetadata;
import com.huaweicloud.sdk.cce.v3.model.CreateClusterRequest;
import com.huaweicloud.sdk.cce.v3.model.CreateClusterResponse;
import com.huaweicloud.sdk.cce.v3.model.Login;
import com.huaweicloud.sdk.cce.v3.model.NodeCreateRequest;
import com.huaweicloud.sdk.cce.v3.model.NodeMetadata;
import com.huaweicloud.sdk.cce.v3.model.NodePool;
import com.huaweicloud.sdk.cce.v3.model.NodePoolMetadata;
import com.huaweicloud.sdk.cce.v3.model.NodePoolSpec;
import com.huaweicloud.sdk.cce.v3.model.NodeSpec;
import com.huaweicloud.sdk.cce.v3.model.ShowJobRequest;
import com.huaweicloud.sdk.cce.v3.model.ShowJobResponse;
import com.huaweicloud.sdk.cce.v3.model.Volume;
import com.huaweicloud.sdk.cce.v3.region.CceRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

public class ClusterUse {

    private final static String apiVersion = "v3";

    private final static String clusterApiKind = "Cluster";

    private final static String nodeApiKind = "Node";

    private final static String nodePoolApiKind = "NodePool";

    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    static String accessKey = System.getenv("HUAWEICLOUD_SDK_AK");  // 华为云账号Access Key
    static String secretKey = System.getenv("HUAWEICLOUD_SDK_SK");  // 华为云账号Secret Access Key
    /* 以下部分请替换成用户实际参数 */
    static String projectId = "<YOUR PROJECT ID>";     // 华为云账号项目ID
    static String userRegion = "<YOUR REGION>";        // 服务所在区域，eg cn-north-4

    // 创建集群相关参数
    static String clusterName = "<YOUR ClUSTER NAME>";      // 新建集群的名称, eg: test-cluster
    static String vpc = "<YOUR VPC ID>";                    // 新建集群使用的虚拟私有云ID
    static String subnet = "<YOUR SUBNET ID>";              // 新建集群使用的子网ID
    static String flavor = "cce.s1.small";                  // 集群规格
    static String version = "v1.25";                        // 集群版本
    static String eniSubnetId = "<YOUR ipv4 SUBNET ID>";    // turbo集群使用的ipv4子网ID

    // 创建节点相关参数
    static String nodeName = "<YOUR NODEPOOL NAME>";            // 节点名称
    static int count = 1;                                       //节点个数
    static String sshKey = "<YOUR SSH KEY>";                    // 节点登录使用的密钥对名称
    static String operatingSystem = "<OS of NODE in NODEPOOL>"; // 节点的操作系统
    static String nodeFlavor = "<FLAVOR>";                      // 节点的规格
    static String availableZone = "<AVAILABLE ZONE>";           // 节点所在的可用区
    static String rootVolumeType = "<YOUR ROOT VOLUME TYPE>";   // 系统盘存储卷类型
    static int rootVolumeSize = 50;                             // 系统盘存储卷大小, 单位GB
    static String dataVolumeType = "<YOUR DATA VOLUME TYPE>";   // 数据盘存储卷类型
    static int dataVolumeSize = 100;                            // 数据盘存储卷大小, 单位GB

    // 创建节点池相关参数
    static String nodePoolName = "<YOUR NODEPOOL NAME>";             // 节点池名称
    static String sshKeyInPool = "<YOUR SSH KEY>";                   // 节点池中节点登录使用的密钥对名称
    static String osInPool = "<OS of NODE in NODEPOOL>";             // 节点池中节点的操作系统
    static int initNodeCount = 1;                                    // 节点池初始大小
    static String nodeFlavorInPool = "<FLAVOR>";                     // 节点池中节点的规格
    static String azInPool = "<AVAILABLE ZONE>";                     // 节点所在的可用区
    static String rootVolumeTypeInPool = "<YOUR ROOT VOLUME TYPE>";  // 系统盘存储卷类型
    static int rootVolumeSizeInPool = 50;                            // 系统盘存储卷大小, 单位GB
    static String dataVolumeTypeInPool = "<YOUR DATA VOLUME TYPE>";  // 数据盘存储卷类型
    static int dataVolumeSizeInPool = 100;                           // 数据盘存储卷大小, 单位GB

    // 创建证书相关参数
    static int duration = 1;                           // 集群证书有效时间，单位为天

    public static void main(String[] args) {
        // 初始化客户端
        CceClient client = initClient();

        // 创建集群
        String clusterId = createClusterAndWaitReady(client);

        // 在已创建集群的默认节点池上创建一个节点
        createNodeAndWaitReady(clusterId, client);

        // 在已创建集群上新建一个节点池
        createNodePool(clusterId, client);

        // 获取集群证书
        CreateKubernetesClusterCertResponse response = createClusterCert(clusterId, client);
        System.out.println("cluster cert info:" + response);
    }

    // 初始化客户端
    static CceClient initClient() {
        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(accessKey)
                .withSk(secretKey)
                .withProjectId(projectId);

        // 创建服务客户端
        return CceClient.newBuilder()
                .withCredential(auth)
                .withRegion(CceRegion.valueOf(userRegion))
                .build();

    }

    // 创建集群并等待完成,返回集群UID
    static String createClusterAndWaitReady(CceClient client) {
        try {
            // 发起创建集群请求
            CreateClusterResponse response = client.createCluster(getCreateClusterRequest());
            System.out.println(response.toString());
            // 等待集群创建完成
            waitJobReady(response.getStatus().getJobID(), client, "create cluster");
            return response.getMetadata().getUid();
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
            throw e;
        }
    }

    // 创建节点并等待创建完成
    static void createNodeAndWaitReady(String clusterId, CceClient client) {
        // 发起创建节点池请求
        CreateNodeResponse response = client.createNode(getCreateNodeRequest(clusterId));
        System.out.println(response.toString());
        // 等待节点创建完成
        waitJobReady(response.getStatus().getJobID(), client, "create node");
    }

    // 创建节点池
    static void createNodePool(String clusterId, CceClient client) {
        // 发起创建节点池请求
        CreateNodePoolResponse response = client.createNodePool(getCreateNodePoolRequest(clusterId));
        System.out.println(response.toString());
    }

    // 获取集群证书
    static CreateKubernetesClusterCertResponse createClusterCert(String clusterId, CceClient client) {
        // 构造请求
        CreateKubernetesClusterCertRequest request = new CreateKubernetesClusterCertRequest()
                .withClusterId(clusterId)
                .withBody(new CertDuration()
                        .withDuration(duration));
        // 发起创建节点池请求
        return client.createKubernetesClusterCert(request);
    }

    // 创建集群请求
    static CreateClusterRequest getCreateClusterRequest() {
        // 设置集群配置
        ClusterSpec spec = new ClusterSpec()
                .withType(ClusterSpec.TypeEnum.VIRTUALMACHINE)
                .withFlavor(flavor)
                .withVersion(version)
                .withHostNetwork(new HostNetwork()
                        .withVpc(vpc)
                        .withSubnet(subnet))
                .withContainerNetwork(new ContainerNetwork()
                        .withMode(ContainerNetwork.ModeEnum.ENI))
                .withEniNetwork(new EniNetwork()
                        .withEniSubnetId(eniSubnetId));

        Cluster cluster = new Cluster()
                .withKind(clusterApiKind)
                .withApiVersion(apiVersion)
                .withMetadata(new ClusterMetadata()
                        .withName(clusterName))
                .withSpec(spec);

        // 创建集群请求
        return new CreateClusterRequest()
                .withBody(cluster);
    }

    // 创建节点请求
    static CreateNodeRequest getCreateNodeRequest(String clusterId) {
        // 节点配置
        NodeSpec nodeSpec = new NodeSpec()
                .withCount(count)
                .withOs(operatingSystem)
                .withFlavor(nodeFlavor)
                .withAz(availableZone)
                .withLogin(new Login()
                        .withSshKey(sshKey))
                .withRootVolume(new Volume()
                        .withSize(rootVolumeSize)
                        .withVolumetype(rootVolumeType))
                .withDataVolumes(new ArrayList<Volume>(1) {{
                    add(new Volume().withSize(dataVolumeSize)
                            .withVolumetype(dataVolumeType));
                }});
        // 设置节点参数
        NodeCreateRequest node = new NodeCreateRequest()
                .withApiVersion(apiVersion)
                .withKind(nodeApiKind)
                .withMetadata(new NodeMetadata()
                        .withName(nodeName))
                .withSpec(nodeSpec);

        // 构造请求
        return new CreateNodeRequest()
                .withClusterId(clusterId)
                .withBody(node);
    }

    // 创建节点池请求
    static CreateNodePoolRequest getCreateNodePoolRequest(String clusterId) {
        // 设置节点池中节点参数
        NodeSpec nodeSpec = new NodeSpec()
                .withOs(osInPool)
                .withFlavor(nodeFlavorInPool)
                .withAz(azInPool)
                .withLogin(new Login()
                        .withSshKey(sshKeyInPool))
                .withRootVolume(new Volume()
                        .withSize(rootVolumeSizeInPool)
                        .withVolumetype(rootVolumeTypeInPool))
                .withDataVolumes(new ArrayList<Volume>(1) {{
                    add(new Volume().withSize(dataVolumeSizeInPool)
                            .withVolumetype(dataVolumeTypeInPool));
                }});
        // 设置节点池参数
        NodePool nodePool = new NodePool()
                .withApiVersion(apiVersion)
                .withKind(nodePoolApiKind)
                .withMetadata(new NodePoolMetadata()
                        .withName(nodePoolName))
                .withSpec(new NodePoolSpec()
                        .withInitialNodeCount(initNodeCount)
                        .withNodeTemplate(nodeSpec));

        // 构造请求
        return new CreateNodePoolRequest()
                .withClusterId(clusterId)
                .withBody(nodePool);
    }

    // 查询任务执行状态，等待任务成功
    static void waitJobReady(String jobId, CceClient client, String jobDesc) throws RuntimeException {
        int tryTimes = 180;
        while (tryTimes-- > 0) {
            ShowJobRequest request = new ShowJobRequest();
            request.setJobId(jobId);
            ShowJobResponse response = client.showJob(request);
            if (response != null && response.getStatus() != null && response.getStatus().getPhase() != null) {
                String phase = response.getStatus().getPhase();
                System.out.printf("Wait %s ready, job is %s\n", jobDesc, phase);
                if ("Success".equals(phase)) {
                    System.out.printf("%s successfully!\n", jobDesc);
                    return;
                } else if ("Failed".equals(phase)) {
                    System.out.printf("%s failed!", jobDesc);
                    throw new RuntimeException(jobDesc + " failed");
                }
            }
            try {
                Thread.sleep(10 * 1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
