## 1. Sample Overview
 
Huawei Cloud offers [CCE SDK] (https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?product=CCE). You can use SDK to call APIs to perform operations on CCE clusters.
 
This sample shows how to create a cluster and a node in the cluster using CCE.
 
**Note: The cluster in this example is billed on a pay-per-use basis. If you do not use the cluster, delete it in time.**
 
## 2. Preparations
 
- You have registered with Huawei Cloud and completed real-name authentication.
- You have been authorized to use CCE on the Huawei Cloud console.
- The development environment (Java JDK 1.8 and later) is available.
- You have obtained the Access Key (AK) and Secret Access Key (SK) of the Huawei Cloud account. You can create and view your AK/SK on the My Credentials > Access Keys page on the Huawei Cloud console. For details, see Access Keys.
- You have obtained the project ID of the corresponding region of CCE. You can view the project ID on the My Credentials > API Credentials page on the Huawei Cloud console. For details, see API Credentials.
- An available VPC and subnet have been created. You can view the VPC ID and subnet ID on the Virtual Private Cloud page on the network console.
 
## 3. Installing SDK

[CCE SDK] (https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?product=CCE) supports Java JDK 1.8 and later.

Configuring the Dependent CCE SDK Using Maven，For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java)。

``` xml
# Configuring CCE Service Dependency
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cce</artifactId>
    <version>${version}</version>
</dependency>
```

## 4. Sample Code
The following code shows how to create a CCE cluster and a node in the cluster, how to create a node pool when scaling is needed, and how to obtain the cluster certificate that can be used to configure kubectl to access the cluster.
``` java

public class ClusterUse {

    private final static String apiVersion = "v3";

    private final static String clusterApiKind  = "Cluster";

    private final static String nodeApiKind     = "Node";

    private final static String nodePoolApiKind = "NodePool";

    /* Replace the following parameters with actual ones. */
    // Client-related parameters
    // There will be security risks if the AK/SK used for authentication is directly written into code. To minimize the risk, store the AK and SK in ciphertext in the configuration file or environment variables and decrypt them for using.
    // In this example, the AK and SK are stored in environment variables. Before running this example, configure environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    static String ak = System.getenv("HUAWEICLOUD_SDK_AK");   // Access key of the Huawei Cloud account
    static String sk = System.getenv("HUAWEICLOUD_SDK_SK");   // Secret access key of the Huawei Cloud account
    static String projectId = "<YOUR PROJECT ID>";     //Project ID of the Huawei Cloud account
    static String userRegion = "<YOUR REGION>";        // Region where the service is located, for example, cn-north-4

    // Parameters for creating a cluster
    static String clusterName = "<YOUR ClUSTER NAME>";      // Name of a cluster, for example, test-cluster
    static String vpc = "<YOUR VPC ID>";                    // ID of the VPC used by a cluster
    static String subnet = "<YOUR SUBNET ID>";              // ID of the subnet used by a cluster
    static String flavor = "cce.s1.small";                  // Flavor of a cluster
    static String version = "v1.25";                        // Version of a cluster
    static String eniSubnetId = "<YOUR ipv4 SUBNET ID>";    // ID of the IPv4 subnet used by a CCE Turbo cluster

    // Parameters for creating a node
    static String nodeName = "<YOUR NODEPOOL NAME>";           // Name of a node
    static int count = 1;                                      // Number of nodes
    static String sshKey = "<YOUR SSH KEY>";                   // Name of the key pair used for logging in to a node
    static String os = "<OS of NODE in NODEPOOL>";             // OS of a node
    static String nodeFlavor = "<FLAVOR>";                     // Flavor of a node
    static String az = "<AVAILABLE ZONE>";                     // AZ where a node is in
    static String rootVolumeType = "<YOUR ROOT VOLUME TYPE>";  // Type of a system disk storage volume
    static int rootVolumeSize = 50;                            // Size of a system disk storage volume, in GiB
    static String dataVolumeType = "<YOUR DATA VOLUME TYPE>";  // Type of a data disk storage volume
    static int dataVolumeSize = 100;                           // Size of a data disk storage volume, in GiB

    // Parameters for creating a node pool
    static String nodePoolName = "<YOUR NODEPOOL NAME>";             // Name of a node pool
    static String sshKeyInPool = "<YOUR SSH KEY>";                   // Name of the key pair used for logging in to a node in a node pool
    static String osInPool = "<OS of NODE in NODEPOOL>";             // OS of a node in a node pool
    static int initNodeCount = 1;                                    // Initial number of nodes in a node pool
    static String nodeFlavorInPool = "<FLAVOR>";                     // Flavor of a node in a node pool
    static String azInPool = "<AVAILABLE ZONE>";                     // AZ where a node in a node pool is in
    static String rootVolumeTypeInPool = "<YOUR ROOT VOLUME TYPE>";  // Type of a system disk storage volume
    static int rootVolumeSizeInPool = 50;                            // Size of system disk storage volume, in GiB
    static String dataVolumeTypeInPool = "<YOUR DATA VOLUME TYPE>";  // Type of a data disk storage volume
    static int dataVolumeSizeInPool = 100;                           // Size of a data disk storage volume, in GiB

    // Parameters for deleting a cluster
    // Whether to delete an SFS Turbo file system volume. Options:- true or block (The deletion is executed. If the deletion fails, the subsequent process will be blocked.)- try (The deletion is executed. If the deletion fails, the failure will be ignored and the subsequent process will proceed.)- false or skip (The deletion is skipped. This is the default option.)
    static DeleteClusterRequest.DeleteEfsEnum deleteEfs = DeleteClusterRequest.DeleteEfsEnum.TRUE;
    // Whether to delete a port with an ENI attached (native ENI). Options:- true or block (The deletion is executed. If the deletion fails, the subsequent process will be blocked. This is the default option.)- try (The deletion is executed. If the deletion fails, the failure will be ignored and the subsequent process will proceed.)- false or skip (The deletion is skipped.)
    static DeleteClusterRequest.DeleteEniEnum deleteEni = DeleteClusterRequest.DeleteEniEnum.TRUE;
    // Whether to delete an EVS disk. Options: - true or block (The deletion is executed. If the deletion fails, the subsequent process will be blocked.)- try (The deletion is executed. If the deletion fails, the failure will be ignored and the subsequent process will proceed.)- false or skip (The deletion is skipped. This is the default option.)
    static DeleteClusterRequest.DeleteEvsEnum deleteEvs = DeleteClusterRequest.DeleteEvsEnum.TRUE;
    // Whether to delete cluster Service or ingress resources, such as a load balancer. Options:- true or block (The deletion is executed. If the deletion fails, the subsequent process will be blocked. This is the default option.)- try (The deletion is executed. If the deletion fails, the failure will be ignored and the subsequent process will proceed.)- false or skip (The deletion is skipped.)
    static DeleteClusterRequest.DeleteNetEnum deleteNet = DeleteClusterRequest.DeleteNetEnum.TRUE;
    // Whether to delete an OBS volume. Options:- true or block (The deletion is executed. If the deletion fails, the subsequent process will be blocked.)- try (The deletion is executed. If the deletion fails, the failure will be ignored and the subsequent process will proceed.)- false or skip (The deletion is skipped. This is the default option.)
    static DeleteClusterRequest.DeleteObsEnum deleteObs = DeleteClusterRequest.DeleteObsEnum.TRUE;
    // Whether to delete an SFS file system volume. Options:- true or block (The deletion is executed. If the deletion fails, the subsequent process will be blocked.)- try (The deletion is executed. If the deletion fails, the failure will be ignored and the subsequent process will proceed.)- false or skip (The deletion is skipped. This is the default option.)
    static DeleteClusterRequest.DeleteSfsEnum deleteSfs = DeleteClusterRequest.DeleteSfsEnum.TRUE;
    // Whether to delete an SFS 3.0 file system volume. Options:- true or block (The deletion is executed. If the deletion fails, the subsequent process will be blocked.)- try (The deletion is executed. If the deletion fails, the failure will be ignored and the subsequent process will proceed.)- false or skip (The deletion is skipped. This is the default option.)
    static DeleteClusterRequest.DeleteSfs30Enum deleteSfs30 = DeleteClusterRequest.DeleteSfs30Enum.TRUE;
    // Policies for processing all pay-per-use nodes in a cluster. Options:- delete (The nodes will be deleted.)- reset (The nodes will be reset and retained. The data stored in the nodes will not be retained.)- retain (The nodes and the data stored in the nodes will be retained. The nodes will not be reset.)
    static DeleteClusterRequest.OndemandNodePolicyEnum ondemandNodePolicy = DeleteClusterRequest.OndemandNodePolicyEnum.DELETE;
    // Policies for processing all yearly/monthly nodes in a cluster. Options:- reset (The nodes will be reset and retained. The data stored in the nodes will not be retained.)- retain (The nodes and the data stored in the nodes will be retained. The nodes will not be reset.)
    static DeleteClusterRequest.PeriodicNodePolicyEnum periodicNodePolicy = DeleteClusterRequest.PeriodicNodePolicyEnum.RESET;

    // Parameters for creating a certificate
    static int duration = 1;                           // Validity period of the certificate of a cluster, in days

    public static void main(String[] args) {
        // Initialize the client.
        CceClient client = initClient();

        // Create a cluster.
        String clusterId = createClusterAndWaitReady(client);

        // Create a node in the default node pool of the created cluster.
        createNodeAndWaitReady(clusterId, client);

        // Create a node pool in the created cluster.
        createNodePool(clusterId, client);

        // Obtain the certificate of a cluster.
        CreateKubernetesClusterCertResponse response = createClusterCert(clusterId, client);
        System.out.println("cluster cert info:" + response);

        // Delete a cluster.
        ClusterUse.deleteClusterAndWaitReady(clusterId, client);
    }

    static CceClient initClient() {

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        return CceClient.newBuilder()
                .withCredential(auth)
                .withRegion(CceRegion.valueOf(userRegion))
                .build();

    }

    // Create a cluster and wait until the creation is complete. The cluster UID is returned.
    static String createClusterAndWaitReady(CceClient client) {
        try {
            // Create a cluster.
            CreateClusterResponse response = client.createCluster(getCreateClusterRequest());
            System.out.println(response.toString());
            waitJobReady(response.getStatus().getJobID(), client, "create cluster");
            return response.getMetadata().getUid();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    // Create a node and wait until the creation is complete.
    static void createNodeAndWaitReady(String clusterId, CceClient client) {
        CreateNodeResponse response = client.createNode(getCreateNodeRequest(clusterId));
        System.out.println(response.toString());
        waitJobReady(response.getStatus().getJobID(), client, "create node");
    }

    // Create a node pool.
    static void createNodePool(String clusterId, CceClient client) {
        CreateNodePoolResponse response = client.createNodePool(getCreateNodePoolRequest(clusterId));
        System.out.println(response.toString());
    }

    // Obtain the cluster certificate.
    static CreateKubernetesClusterCertResponse createClusterCert(String clusterId, CceClient client) {
        CreateKubernetesClusterCertRequest request = new CreateKubernetesClusterCertRequest()
                .withClusterId(clusterId)
                .withBody(new CertDuration()
                        .withDuration(duration));
        return client.createKubernetesClusterCert(request);
    }

    // Request for creating a cluster
    static CreateClusterRequest getCreateClusterRequest() {
        // Configure cluster settings.
        ClusterSpec spec = new ClusterSpec()
                .withType(ClusterSpec.TypeEnum.VIRTUALMACHINE)
                .withFlavor(flavor)
                .withVersion(version)
                .withHostNetwork(new HostNetwork()
                        .withVpc(vpc)
                        .withSubnet(subnet))
                .withContainerNetwork(new ContainerNetwork()
                        .withMode(ContainerNetwork.ModeEnum.ENI))
                .withEniNetwork(new EniNetwork()
                        .withEniSubnetId(eniSubnetId));

        Cluster cluster = new Cluster()
                .withKind(clusterApiKind)
                .withApiVersion(apiVersion)
                .withMetadata(new ClusterMetadata()
                        .withName(clusterName))
                .withSpec(spec);

        // Create a cluster creation request.
        return new CreateClusterRequest()
                .withBody(cluster);
    }

    // Request for creating a node
    static CreateNodeRequest getCreateNodeRequest(String clusterId) {
        NodeSpec nodeSpec = new NodeSpec()
                .withCount(count)
                .withOs(os)
                .withFlavor(nodeFlavor)
                .withAz(az)
                .withLogin(new Login()
                        .withSshKey(sshKey))
                .withRootVolume(new Volume()
                        .withSize(rootVolumeSize)
                        .withVolumetype(rootVolumeType))
                .withDataVolumes(new ArrayList<Volume>(1) {{
                    add(new Volume().withSize(dataVolumeSize)
                            .withVolumetype(dataVolumeType));
                }});
        NodeCreateRequest node = new NodeCreateRequest()
                .withApiVersion(apiVersion)
                .withKind(nodeApiKind)
                .withMetadata(new NodeMetadata()
                        .withName(nodeName))
                .withSpec(nodeSpec);

        return new CreateNodeRequest()
                .withClusterId(clusterId)
                .withBody(node);
    }

    // Request for creating a node pool
    static CreateNodePoolRequest getCreateNodePoolRequest(String clusterId) {
        NodeSpec nodeSpec = new NodeSpec()
                .withOs(osInPool)
                .withFlavor(nodeFlavorInPool)
                .withAz(azInPool)
                .withLogin(new Login()
                        .withSshKey(sshKeyInPool))
                .withRootVolume(new Volume()
                        .withSize(rootVolumeSizeInPool)
                        .withVolumetype(rootVolumeTypeInPool))
                .withDataVolumes(new ArrayList<Volume>(1) {{
                    add(new Volume().withSize(dataVolumeSizeInPool)
                            .withVolumetype(dataVolumeTypeInPool));
                }});
        NodePool nodePool = new NodePool()
                .withApiVersion(apiVersion)
                .withKind(nodePoolApiKind)
                .withMetadata(new NodePoolMetadata()
                        .withName(nodePoolName))
                .withSpec(new NodePoolSpec()
                        .withInitialNodeCount(initNodeCount)
                        .withNodeTemplate(nodeSpec));

        return new CreateNodePoolRequest()
                .withClusterId(clusterId)
                .withBody(nodePool);
    }

    // Delete a cluster and wait until the deletion is complete.
    static void deleteClusterAndWaitReady(String clusterId, CceClient client) {
        DeleteClusterResponse response = client.deleteCluster(getDeleteClusterRequest(clusterId));
        System.out.println(response.toString());
        waitJobReady(response.getStatus().getJobID(), client, "delete cluster");
    }

    // Create a request for deleting a cluster.
    static DeleteClusterRequest getDeleteClusterRequest(String clusterId){
        return new DeleteClusterRequest()
                .withClusterId(clusterId)
                .withDeleteEfs(deleteEfs)
                .withDeleteEni(deleteEni)
                .withDeleteEvs(deleteEvs)
                .withDeleteNet(deleteNet)
                .withDeleteObs(deleteObs)
                .withDeleteSfs(deleteSfs)
                .withDeleteSfs30(deleteSfs30)
                .withOndemandNodePolicy(ondemandNodePolicy)
                .withPeriodicNodePolicy(periodicNodePolicy);
    }

    // Obtain the execution status of a job and wait until the job is complete.
    static void waitJobReady(String jobId, CceClient client, String jobDesc) throws RuntimeException {
        int tryTimes = 180;
        while (tryTimes-- > 0) {
            ShowJobRequest request = new ShowJobRequest();
            request.setJobId(jobId);
            ShowJobResponse response = client.showJob(request);
            if (response != null && response.getStatus() != null && response.getStatus().getPhase() != null) {
                String phase = response.getStatus().getPhase();
                System.out.printf("Wait %s ready, job is %s\n", jobDesc, phase);
                if ("Success".equals(phase)) {
                    System.out.printf("%s successfully!\n", jobDesc);
                    return;
                } else if ("Failed".equals(phase)) {
                    System.out.printf("%s failed!", jobDesc);
                    throw new RuntimeException(jobDesc + " failed");
                }
            }
            try {
                Thread.sleep(10 * 1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}


```

## 5. Reference
 
For more information, see [Cloud Container Engine Documentation] (https://support.huaweicloud.com/intl/en-us/function-cce/index.html).
 
## 6. Change History
 
|    Release Date    | Version | Description |
|:----------:| :------: | :----------: |
| 2023-08-18 | 1.0 | This issue is first release. |