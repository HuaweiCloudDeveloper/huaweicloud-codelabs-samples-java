## 1. 示例简介

华为云提供了[云容器引擎 CCE SDK](https://sdkcenter.developer.huaweicloud.com/?product=CCE)，您可以直接集成SDK来调用相关API，从而实现对云容器引擎服务的快速操作。

该示例展示了如何使用云容器引擎服务创建集群和节点。

**注意：示例中创建的CCE集群为按需计费，如不再使用请及时删除集群。**

## 2. 开发前准备

- 已注册华为云，并完成实名认证。
- 已在华为云控制台授权使用CCE服务。
- 具备开发环境 ，支持Java JDK 1.8及以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见访问密钥。
- 已获取CCE服务对应区域的项目ID，请在华为云控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见API凭证。
- 已创建可用的虚拟私有云和子网，请在华为云控制台“网络控制台 >虚拟私有云”页面上查看虚拟私有云ID和子网ID。

## 3. 安装SDK

[云容器引擎 CCE SDK](https://sdkcenter.developer.huaweicloud.com/?product=CCE)支持Java JDK 1.8及其以上版本。

通过Maven配置所依赖的云容器引擎服务SDK，具体的SDK版本号请参见[SDK开发中心](https://sdkcenter.developer.huaweicloud.com/?language=java)。

``` xml
# 配置CCE服务依赖
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cce</artifactId>
    <version>${version}</version>
</dependency>
```

## 4. 代码示例
以下代码展示如何创建一个CCE集群和节点，以及在需要扩缩容情况下如何创建一个节点池，最后展示如何获取集群的证书，可用于配置kubectl来访问集群。
``` java

public class ClusterUse {

    private final static String apiVersion = "v3";

    private final static String clusterApiKind  = "Cluster";

    private final static String nodeApiKind     = "Node";

    private final static String nodePoolApiKind = "NodePool";

    /* 以下部分请替换成用户实际参数 */
    // 客户端相关参数
    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    static String ak = System.getenv("HUAWEICLOUD_SDK_AK");   // 华为云账号Access Key
    static String sk = System.getenv("HUAWEICLOUD_SDK_SK");   // 华为云账号Secret Access Key
    static String projectId = "<YOUR PROJECT ID>";     // 华为云账号项目ID
    static String userRegion = "<YOUR REGION>";        // 服务所在区域，eg cn-north-4

    // 创建集群相关参数
    static String clusterName = "<YOUR ClUSTER NAME>";      // 新建集群的名称, eg: test-cluster
    static String vpc = "<YOUR VPC ID>";                    // 新建集群使用的虚拟私有云ID
    static String subnet = "<YOUR SUBNET ID>";              // 新建集群使用的子网ID
    static String flavor = "cce.s1.small";                  // 集群规格
    static String version = "v1.25";                        // 集群版本
    static String eniSubnetId = "<YOUR ipv4 SUBNET ID>";    // turbo集群使用的ipv4子网ID

    // 创建节点相关参数
    static String nodeName = "<YOUR NODEPOOL NAME>";           // 节点名称
    static int count = 1;                                      //节点个数
    static String sshKey = "<YOUR SSH KEY>";                   // 节点登录使用的密钥对名称
    static String os = "<OS of NODE in NODEPOOL>";             // 节点的操作系统
    static String nodeFlavor = "<FLAVOR>";                     // 节点的规格
    static String az = "<AVAILABLE ZONE>";                     // 节点所在的可用区
    static String rootVolumeType = "<YOUR ROOT VOLUME TYPE>";  // 系统盘存储卷类型
    static int rootVolumeSize = 50;                            // 系统盘存储卷大小, 单位GB
    static String dataVolumeType = "<YOUR DATA VOLUME TYPE>";  // 数据盘存储卷类型
    static int dataVolumeSize = 100;                           // 数据盘存储卷大小, 单位GB

    // 创建节点池相关参数
    static String nodePoolName = "<YOUR NODEPOOL NAME>";             // 节点池名称
    static String sshKeyInPool = "<YOUR SSH KEY>";                   // 节点池中节点登录使用的密钥对名称
    static String osInPool = "<OS of NODE in NODEPOOL>";             // 节点池中节点的操作系统
    static int initNodeCount = 1;                                    // 节点池初始大小
    static String nodeFlavorInPool = "<FLAVOR>";                     // 节点池中节点的规格
    static String azInPool = "<AVAILABLE ZONE>";                     // 节点所在的可用区
    static String rootVolumeTypeInPool = "<YOUR ROOT VOLUME TYPE>";  // 系统盘存储卷类型
    static int rootVolumeSizeInPool = 50;                            // 系统盘存储卷大小, 单位GB
    static String dataVolumeTypeInPool = "<YOUR DATA VOLUME TYPE>";  // 数据盘存储卷类型
    static int dataVolumeSizeInPool = 100;                           // 数据盘存储卷大小, 单位GB

    // 删除集群相关参数
    // 是否删除SFS Turbo（极速文件存储卷）， 枚举取值： - true或block (执行删除流程，失败则阻塞后续流程) - try (执行删除流程，失败则忽略，并继续执行后续流程) - false或skip (跳过删除流程，默认选项)
    static DeleteClusterRequest.DeleteEfsEnum deleteEfs = DeleteClusterRequest.DeleteEfsEnum.TRUE;
    // 是否删除eni ports（原生弹性网卡）， 枚举取值： - true或block (执行删除流程，失败则阻塞后续流程，默认选项) - try (执行删除流程，失败则忽略，并继续执行后续流程) - false或skip (跳过删除流程)
    static DeleteClusterRequest.DeleteEniEnum deleteEni = DeleteClusterRequest.DeleteEniEnum.TRUE;
    // 是否删除evs（云硬盘）， 枚举取值： - true或block (执行删除流程，失败则阻塞后续流程) - try (执行删除流程，失败则忽略，并继续执行后续流程) - false或skip (跳过删除流程，默认选项)
    static DeleteClusterRequest.DeleteEvsEnum deleteEvs = DeleteClusterRequest.DeleteEvsEnum.TRUE;
    // 是否删除elb（弹性负载均衡）等集群Service/Ingress相关资源。 枚举取值： - true或block (执行删除流程，失败则阻塞后续流程，默认选项) - try (执行删除流程，失败则忽略，并继续执行后续流程) - false或skip (跳过删除流程)
    static DeleteClusterRequest.DeleteNetEnum deleteNet = DeleteClusterRequest.DeleteNetEnum.TRUE;
    // 是否删除obs（对象存储卷）， 枚举取值： - true或block (执行删除流程，失败则阻塞后续流程) - try (执行删除流程，失败则忽略，并继续执行后续流程) - false或skip (跳过删除流程，默认选项)
    static DeleteClusterRequest.DeleteObsEnum deleteObs = DeleteClusterRequest.DeleteObsEnum.TRUE;
    // 是否删除sfs（文件存储卷）， 枚举取值： - true或block (执行删除流程，失败则阻塞后续流程) - try (执行删除流程，失败则忽略，并继续执行后续流程) - false或skip (跳过删除流程，默认选项)
    static DeleteClusterRequest.DeleteSfsEnum deleteSfs = DeleteClusterRequest.DeleteSfsEnum.TRUE;
    // 是否删除sfs3.0（文件存储卷3.0）， 枚举取值： - true或block (执行删除流程，失败则阻塞后续流程) - try (执行删除流程，失败则忽略，并继续执行后续流程) - false或skip (跳过删除流程，默认选项)
    static DeleteClusterRequest.DeleteSfs30Enum deleteSfs30 = DeleteClusterRequest.DeleteSfs30Enum.TRUE;
    // 集群下所有按需节点处理策略， 枚举取值： - delete (删除服务器) - reset (保留服务器并重置服务器，数据不保留) - retain （保留服务器不重置服务器，数据保留）
    static DeleteClusterRequest.OndemandNodePolicyEnum ondemandNodePolicy = DeleteClusterRequest.OndemandNodePolicyEnum.DELETE;
    // 集群下所有包周期节点处理策略， 枚举取值： - reset (保留服务器并重置服务器，数据不保留) - retain （保留服务器不重置服务器，数据保留）
    static DeleteClusterRequest.PeriodicNodePolicyEnum periodicNodePolicy = DeleteClusterRequest.PeriodicNodePolicyEnum.RESET;

    // 创建证书相关参数
    static int duration = 1;                           // 集群证书有效时间，单位为天

    public static void main(String[] args) {
        // 初始化客户端
        CceClient client = initClient();

        // 创建集群
        String clusterId = createClusterAndWaitReady(client);

        // 在已创建集群的默认节点池上创建一个节点
        createNodeAndWaitReady(clusterId, client);

        // 在已创建集群上新建一个节点池
        createNodePool(clusterId, client);

        // 获取集群证书
        CreateKubernetesClusterCertResponse response = createClusterCert(clusterId, client);
        System.out.println("cluster cert info:" + response);

        // 删除集群
        ClusterUse.deleteClusterAndWaitReady(clusterId, client);
    }

    // 初始化客户端
    static CceClient initClient() {
        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        // 创建服务客户端
        return CceClient.newBuilder()
                .withCredential(auth)
                .withRegion(CceRegion.valueOf(userRegion))
                .build();

    }

    // 创建集群并等待完成,返回集群UID
    static String createClusterAndWaitReady(CceClient client) {
        try {
            // 发起创建集群请求
            CreateClusterResponse response = client.createCluster(getCreateClusterRequest());
            System.out.println(response.toString());
            // 等待集群创建完成
            waitJobReady(response.getStatus().getJobID(), client, "create cluster");
            return response.getMetadata().getUid();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    // 创建节点并等待创建完成
    static void createNodeAndWaitReady(String clusterId, CceClient client) {
        // 发起创建节点池请求
        CreateNodeResponse response = client.createNode(getCreateNodeRequest(clusterId));
        System.out.println(response.toString());
        // 等待节点创建完成
        waitJobReady(response.getStatus().getJobID(), client, "create node");
    }

    // 创建节点池
    static void createNodePool(String clusterId, CceClient client) {
        // 发起创建节点池请求
        CreateNodePoolResponse response = client.createNodePool(getCreateNodePoolRequest(clusterId));
        System.out.println(response.toString());
    }

    // 获取集群证书
    static CreateKubernetesClusterCertResponse createClusterCert(String clusterId, CceClient client) {
        // 构造请求
        CreateKubernetesClusterCertRequest request = new CreateKubernetesClusterCertRequest()
                .withClusterId(clusterId)
                .withBody(new CertDuration()
                        .withDuration(duration));
        // 发起创建节点池请求
        return client.createKubernetesClusterCert(request);
    }

    // 创建集群请求
    static CreateClusterRequest getCreateClusterRequest() {
        // 设置集群配置
        ClusterSpec spec = new ClusterSpec()
                .withType(ClusterSpec.TypeEnum.VIRTUALMACHINE)
                .withFlavor(flavor)
                .withVersion(version)
                .withHostNetwork(new HostNetwork()
                        .withVpc(vpc)
                        .withSubnet(subnet))
                .withContainerNetwork(new ContainerNetwork()
                        .withMode(ContainerNetwork.ModeEnum.ENI))
                .withEniNetwork(new EniNetwork()
                        .withEniSubnetId(eniSubnetId));

        Cluster cluster = new Cluster()
                .withKind(clusterApiKind)
                .withApiVersion(apiVersion)
                .withMetadata(new ClusterMetadata()
                        .withName(clusterName))
                .withSpec(spec);

        // 创建集群请求
        return new CreateClusterRequest()
                .withBody(cluster);
    }

    // 创建节点请求
    static CreateNodeRequest getCreateNodeRequest(String clusterId) {
        // 节点配置
        NodeSpec nodeSpec = new NodeSpec()
                .withCount(count)
                .withOs(os)
                .withFlavor(nodeFlavor)
                .withAz(az)
                .withLogin(new Login()
                        .withSshKey(sshKey))
                .withRootVolume(new Volume()
                        .withSize(rootVolumeSize)
                        .withVolumetype(rootVolumeType))
                .withDataVolumes(new ArrayList<Volume>(1) {{
                    add(new Volume().withSize(dataVolumeSize)
                            .withVolumetype(dataVolumeType));
                }});
        // 设置节点参数
        NodeCreateRequest node = new NodeCreateRequest()
                .withApiVersion(apiVersion)
                .withKind(nodeApiKind)
                .withMetadata(new NodeMetadata()
                        .withName(nodeName))
                .withSpec(nodeSpec);

        // 构造请求
        return new CreateNodeRequest()
                .withClusterId(clusterId)
                .withBody(node);
    }

    // 创建节点池请求
    static CreateNodePoolRequest getCreateNodePoolRequest(String clusterId) {
        // 设置节点池中节点参数
        NodeSpec nodeSpec = new NodeSpec()
                .withOs(osInPool)
                .withFlavor(nodeFlavorInPool)
                .withAz(azInPool)
                .withLogin(new Login()
                        .withSshKey(sshKeyInPool))
                .withRootVolume(new Volume()
                        .withSize(rootVolumeSizeInPool)
                        .withVolumetype(rootVolumeTypeInPool))
                .withDataVolumes(new ArrayList<Volume>(1) {{
                    add(new Volume().withSize(dataVolumeSizeInPool)
                            .withVolumetype(dataVolumeTypeInPool));
                }});
        // 设置节点池参数
        NodePool nodePool = new NodePool()
                .withApiVersion(apiVersion)
                .withKind(nodePoolApiKind)
                .withMetadata(new NodePoolMetadata()
                        .withName(nodePoolName))
                .withSpec(new NodePoolSpec()
                        .withInitialNodeCount(initNodeCount)
                        .withNodeTemplate(nodeSpec));

        // 构造请求
        return new CreateNodePoolRequest()
                .withClusterId(clusterId)
                .withBody(nodePool);
    }

    // 删除集群并等待删除完成
    static void deleteClusterAndWaitReady(String clusterId, CceClient client) {
        // 发起创建节点池请求
        DeleteClusterResponse response = client.deleteCluster(getDeleteClusterRequest(clusterId));
        System.out.println(response.toString());
        // 等待节点创建完成
        waitJobReady(response.getStatus().getJobID(), client, "delete cluster");
    }

    // 创建删除集群请求
    static DeleteClusterRequest getDeleteClusterRequest(String clusterId){
        return new DeleteClusterRequest()
                .withClusterId(clusterId)
                .withDeleteEfs(deleteEfs)
                .withDeleteEni(deleteEni)
                .withDeleteEvs(deleteEvs)
                .withDeleteNet(deleteNet)
                .withDeleteObs(deleteObs)
                .withDeleteSfs(deleteSfs)
                .withDeleteSfs30(deleteSfs30)
                .withOndemandNodePolicy(ondemandNodePolicy)
                .withPeriodicNodePolicy(periodicNodePolicy);
    }

    // 查询任务执行状态，等待任务成功
    static void waitJobReady(String jobId, CceClient client, String jobDesc) throws RuntimeException {
        int tryTimes = 180;
        while (tryTimes-- > 0) {
            ShowJobRequest request = new ShowJobRequest();
            request.setJobId(jobId);
            ShowJobResponse response = client.showJob(request);
            if (response != null && response.getStatus() != null && response.getStatus().getPhase() != null) {
                String phase = response.getStatus().getPhase();
                System.out.printf("Wait %s ready, job is %s\n", jobDesc, phase);
                if ("Success".equals(phase)) {
                    System.out.printf("%s successfully!\n", jobDesc);
                    return;
                } else if ("Failed".equals(phase)) {
                    System.out.printf("%s failed!", jobDesc);
                    throw new RuntimeException(jobDesc + " failed");
                }
            }
            try {
                Thread.sleep(10 * 1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}


```

## 5. 参考

更多信息请参考：[云容器引擎 CCE 文档](https://support.huaweicloud.com/cce/)

## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-08-18 | 1.0 | 文档首次发布 |