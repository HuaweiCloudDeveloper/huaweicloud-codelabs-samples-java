package com.huawei.swr;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.swr.v2.region.SwrRegion;
import com.huaweicloud.sdk.swr.v2.SwrClient;
import com.huaweicloud.sdk.swr.v2.model.CreateSecretRequest;
import com.huaweicloud.sdk.swr.v2.model.CreateSecretResponse;

public class CreateSecret {

    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    static String accessKey = System.getenv("HUAWEICLOUD_SDK_AK");  // 华为云账号Access Key
    static String secretKey = System.getenv("HUAWEICLOUD_SDK_SK");  // 华为云账号Secret Access Key
    /* 以下部分请替换成用户实际参数 */
    static String projectId = "<YOUR PROJECT ID>";     // 华为云账号项目ID
    static String userRegion = "<YOUR REGION>";        // 服务所在区域，eg cn-north-4

    public static void main(String[] args) {
        System.out.println("This is a demo to create secret.");
        // 初始化客户端
        SwrClient client = initClient();
        // 创建登录指令
        CreateSecretRequest request = new CreateSecretRequest();
        try {
            CreateSecretResponse response = client.createSecret(request);
            System.out.println("secret info: " + response.getAuths().toString());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    // 初始化客户端
    static SwrClient initClient() {
        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(accessKey)
                .withSk(secretKey)
                .withProjectId(projectId);

        // 创建服务客户端
        return SwrClient.newBuilder()
                .withCredential(auth)
                .withRegion(SwrRegion.valueOf(userRegion))
                .build();

    }
}
