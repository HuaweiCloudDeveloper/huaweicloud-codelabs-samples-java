## 1. 示例简介

华为云提供了[容器镜像服务 SWR SDK](https://sdkcenter.developer.huaweicloud.com/?product=SWR) ，您可以直接集成SDK来调用相关API，从而实现对容器镜像服务的快速操作。

该示例展示了如何使用容器镜像服务生成临时登录指令。

## 2. 开发前准备

- 已注册华为云，并完成实名认证。
- 已在华为云控制台授权使用CCE服务。
- 具备开发环境 ，支持Java JDK 1.8及以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见访问密钥。
- 已获取SWR服务对应区域的项目ID，请在华为云控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见API凭证。

## 3. 安装SDK

[容器镜像服务 SWR SDK](https://sdkcenter.developer.huaweicloud.com/?product=SWR) 支持Java JDK 1.8及其以上版本。

通过Maven配置所依赖的云容器引擎服务SDK，具体的SDK版本号请参见[SDK开发中心](https://sdkcenter.developer.huaweicloud.com/?language=java) 。

```
# 配置SWR服务依赖
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-swr</artifactId>
    <version>${version}</version>
</dependency>
```

## 4. 代码示例
以下代码展示如何生成一条临时登录指令。
``` java
package com.huawei.swr;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.swr.v2.region.SwrRegion;
import com.huaweicloud.sdk.swr.v2.SwrClient;
import com.huaweicloud.sdk.swr.v2.model.CreateSecretRequest;
import com.huaweicloud.sdk.swr.v2.model.CreateSecretResponse;

public class CreateSecret {

    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    static String accessKey = System.getenv("HUAWEICLOUD_SDK_AK");  // 华为云账号Access Key
    static String secretKey = System.getenv("HUAWEICLOUD_SDK_SK");  // 华为云账号Secret Access Key
    /* 以下部分请替换成用户实际参数 */
    static String projectId = "<YOUR PROJECT ID>";     // 华为云账号项目ID
    static String userRegion = "<YOUR REGION>";        // 服务所在区域，eg cn-north-4

    public static void main(String[] args) {
        System.out.println("This is a demo to create secret.");
        // 初始化客户端
        SwrClient client = initClient();
        // 创建登录指令
        CreateSecretRequest request = new CreateSecretRequest();
        try {
            CreateSecretResponse response = client.createSecret(request);
            System.out.println("secret info: " + response.getAuths().toString());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    // 初始化客户端
    static SwrClient initClient() {
        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(accessKey)
                .withSk(secretKey)
                .withProjectId(projectId);

        // 创建服务客户端
        return SwrClient.newBuilder()
                .withCredential(auth)
                .withRegion(SwrRegion.valueOf(userRegion))
                .build();

    }
}

```

## 5. 参考

更多信息请参考：[容器镜像服务 SWR 文档](https://support.huaweicloud.com/swr/)

## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-11-18 | 1.0 | 文档首次发布 |