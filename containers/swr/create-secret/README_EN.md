## 1. Overview

Huawei Cloud provides [SWR SDK]( https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?product=SWR). You can use SDK to call APIs to perform operations on SWR.

This example shows how to use SWR to generate a temporary login command.

## 2. Preparations

- You have registered with Huawei Cloud and completed real-name authentication.
- You have been authorized to use SWR on the Huawei Cloud console.
- The development environment (Java JDK 1.8 or later) is available.
- You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. To create or view an AK/SK, choose My Credentials > Access Keys on the Huawei Cloud console. For details, see "Access Keys".
- You have obtained the project ID of the target region. To obtain a project ID, choose My Credentials > API Credentials on the Huawei Cloud console. For details, see "API Credentials".

## 3. Installing SDK

[SWR SDK]( https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?product=SWR) supports Java JDK 1.8 and later versions. 

Configuring the Dependent SWR SDK Using Maven，For details about the SDK version, see [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java)。

```
# Configuring SWR Service Dependency
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-swr</artifactId>
    <version>${version}</version>
</dependency>
```

## 4. Sample Code
The following code shows how to generate a temporary login command:
``` java
package com.huawei.swr;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.swr.v2.region.SwrRegion;
import com.huaweicloud.sdk.swr.v2.SwrClient;
import com.huaweicloud.sdk.swr.v2.model.CreateSecretRequest;
import com.huaweicloud.sdk.swr.v2.model.CreateSecretResponse;

public class CreateSecret {

   /* Replace the following parameters with actual ones. */
   // There will be security risks if the AK/SK used for authentication is directly written into code. Encrypt the AK/SK in the configuration file or environment variables for storage.
   // In this example, the AK/SK are stored in environment variables. Before running this example, configure environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    static String accessKey = System.getenv("HUAWEICLOUD_SDK_AK");  // AK of the Huawei Cloud account
    static String secretKey = System.getenv("HUAWEICLOUD_SDK_SK");  // SK of the Huawei Cloud account
    static String projectId = "<YOUR PROJECT ID>";                  // Project ID of the Huawei Cloud account
    static String userRegion = "<YOUR REGION>";                     // Region where the service is located, for example, cn-north-4

    public static void main(String[] args) {
        /* Main process */
        System.out.println("This is a demo to create secret.");
        // Initialize the client.
        SwrClient client = initClient();
        // Generate a temporary login command.
        CreateSecretRequest request = new CreateSecretRequest();
        try {
            CreateSecretResponse response = client.createSecret(request);
            System.out.println("secret info: " + response.getAuths().toString());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    static SwrClient initClient() {
        ICredential auth = new BasicCredentials()
                .withAk(accessKey)
                .withSk(secretKey)
                .withProjectId(projectId);

        return SwrClient.newBuilder()
                .withCredential(auth)
                .withRegion(SwrRegion.valueOf(userRegion))
                .build();

    }
}

```

## 5. Reference

For more details, see [SWR Documentation](https://support.huaweicloud.com/intl/en-us/swr/).

## 6. Change History

|    Date    | Version | Description |
|:----------:| :------: | :----------: |
| 2023-11-18 | 1.0 | This is first release. |
