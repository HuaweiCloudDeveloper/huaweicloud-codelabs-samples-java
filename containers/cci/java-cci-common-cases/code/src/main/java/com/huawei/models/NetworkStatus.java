package com.huawei.models;

import com.google.gson.annotations.SerializedName;

public class NetworkStatus {

    @SerializedName("state")
    private String state;

    @SerializedName("message")
    private String message;

    public NetworkStatus state(String state) {
        this.state = state;
        return this;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public NetworkStatus message(String message) {
        this.message = message;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
