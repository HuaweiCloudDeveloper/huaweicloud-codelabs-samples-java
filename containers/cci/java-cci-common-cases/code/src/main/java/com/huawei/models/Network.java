package com.huawei.models;

import com.google.gson.annotations.SerializedName;
import io.kubernetes.client.common.KubernetesObject;
import io.kubernetes.client.openapi.models.V1ObjectMeta;

import java.util.Objects;

public class Network implements KubernetesObject {

    @SerializedName("apiVersion")
    private String apiVersion;

    @SerializedName("kind")
    private String kind;

    @SerializedName("metadata")
    private V1ObjectMeta metadata;

    @SerializedName("spec")
    private NetworkSpec spec;

    @SerializedName("status")
    private NetworkStatus status;

    public Network apiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
        return this;
    }

    @Override
    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public Network kind(String kind) {
        this.kind = kind;
        return this;
    }

    @Override
    public String getKind() {
        return null;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Network metadata(V1ObjectMeta metadata) {
        this.metadata = metadata;
        return this;
    }

    @Override
    public V1ObjectMeta getMetadata() {
        return metadata;
    }

    public void setMetadata(V1ObjectMeta metadata) {
        this.metadata = metadata;
    }

    public Network spec(NetworkSpec spec) {
        this.spec = spec;
        return this;
    }

    public NetworkSpec getSpec() {
        return spec;
    }

    public void setSpec(NetworkSpec spec) {
        this.spec = spec;
    }

    public Network status(NetworkStatus status) {
        this.status = status;
        return this;
    }

    public NetworkStatus getStatus() {
        return status;
    }

    public void setStatus(NetworkStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Network network = (Network) o;
        boolean firstJudgments = Objects.equals(apiVersion, network.apiVersion) && Objects.equals(kind, network.kind) && Objects.equals(metadata, network.metadata);
        boolean secondJudgments = Objects.equals(spec, network.spec) && Objects.equals(status, network.status);
        return firstJudgments && secondJudgments;
    }

    @Override
    public int hashCode() {
        return Objects.hash(apiVersion, kind, metadata, spec, status);
    }
}
