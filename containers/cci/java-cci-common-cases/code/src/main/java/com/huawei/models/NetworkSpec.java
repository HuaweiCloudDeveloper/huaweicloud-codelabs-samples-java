package com.huawei.models;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class NetworkSpec {

    @SerializedName("cidr")
    private String cidr;

    @SerializedName("networkType")
    private String networkType;

    @SerializedName("attachedVPC")
    private String attachedVPC;

    @SerializedName("networkID")
    private String networkID;

    @SerializedName("subnetID")
    private String subnetID;

    @SerializedName("availableZone")
    private String availableZone;

    public NetworkSpec cidr(String cidr) {
        this.cidr = cidr;
        return this;
    }

    public String getCidr() {
        return cidr;
    }

    public void setCidr(String cidr) {
        this.cidr = cidr;
    }

    public NetworkSpec networkType(String networkType) {
        this.networkType = networkType;
        return this;
    }

    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public NetworkSpec attachedVPC(String attachedVPC) {
        this.attachedVPC = attachedVPC;
        return this;
    }

    public String getAttachedVPC() {
        return attachedVPC;
    }

    public void setAttachedVPC(String attachedVPC) {
        this.attachedVPC = attachedVPC;
    }

    public NetworkSpec networkID(String networkID) {
        this.networkID = networkID;
        return this;
    }

    public String getNetworkID() {
        return networkID;
    }

    public void setNetworkID(String networkID) {
        this.networkID = networkID;
    }

    public NetworkSpec subnetID(String subnetID) {
        this.subnetID = subnetID;
        return this;
    }

    public String getSubnetID() {
        return subnetID;
    }

    public void setSubnetID(String subnetID) {
        this.subnetID = subnetID;
    }

    public NetworkSpec availableZone(String availableZone) {
        this.availableZone = availableZone;
        return this;
    }

    public String getAvailableZone() {
        return availableZone;
    }

    public void setAvailableZone(String availableZone) {
        this.availableZone = availableZone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NetworkSpec that = (NetworkSpec) o;
        boolean firstJudgments = Objects.equals(cidr, that.cidr) && Objects.equals(networkType, that.networkType) && Objects.equals(attachedVPC, that.attachedVPC);
        boolean secondJudgments = Objects.equals(networkID, that.networkID) && Objects.equals(subnetID, that.subnetID) && Objects.equals(availableZone, that.availableZone);
        return firstJudgments && secondJudgments;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cidr, networkType, attachedVPC, networkID, subnetID, availableZone);
    }
}
