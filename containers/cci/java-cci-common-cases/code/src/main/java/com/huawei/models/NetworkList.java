package com.huawei.models;

import com.google.gson.annotations.SerializedName;
import io.kubernetes.client.common.KubernetesListObject;
import io.kubernetes.client.common.KubernetesObject;
import io.kubernetes.client.openapi.models.V1ListMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class NetworkList implements KubernetesListObject {

    @SerializedName("apiVersion")
    private String apiVersion;

    @SerializedName("items")
    private List<Network> items = new ArrayList<>();

    @SerializedName("kind")
    private String kind;

    @SerializedName("metadata")
    private V1ListMeta metadata;

    public NetworkList metadata(V1ListMeta metadata) {
        this.metadata = metadata;
        return this;
    }

    @Override
    public V1ListMeta getMetadata() {
        return metadata;
    }

    public void setMetadata(V1ListMeta metadata) {
        this.metadata = metadata;
    }

    public NetworkList apiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
        return this;
    }

    @Override
    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public NetworkList kind(String kind) {
        this.kind = kind;
        return this;
    }

    @Override
    public String getKind() {
        return null;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public NetworkList items(List<Network> items) {
        this.items = items;
        return this;
    }

    public NetworkList addItemsItem(Network itemsItem) {
        this.items.add(itemsItem);
        return this;
    }

    @Override
    public List<? extends KubernetesObject> getItems() {
        return items;
    }

    public void setItems(List<Network> items) {
        this.items = items;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NetworkList that = (NetworkList) o;
        return Objects.equals(apiVersion, that.apiVersion) && Objects.equals(items, that.items) && Objects.equals(kind, that.kind) && Objects.equals(metadata, that.metadata);
    }

    @Override
    public int hashCode() {
        return Objects.hash(apiVersion, items, kind, metadata);
    }
}
