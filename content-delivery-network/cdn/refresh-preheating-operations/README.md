## 1. 示例简介

CDN（Content Delivery Network，内容分发网络）。CDN是构建在现有互联网基础之上的一层智能虚拟网络，通过在网络各处部署节点服务器，实现将源站内容分发至所有CDN节点，使用户可以就近获得所需的内容。CDN服务缩短了用户查看内容的访问延迟，提高了用户访问网站的响应速度与网站的可用性，解决了网络带宽小、用户访问量大、网点分布不均等问题。

域名接入CDN加速后，您可以通过刷新和预热操作管理缓存在CDN节点上的资源。缓存刷新会将CDN节点的缓存内容删除，当用户向CDN节点请求资源时，CDN会直接回源站请求对应的资源。缓存预热是模拟用户请求行为，将源站的资源缓存到CDN节点，后续用户访问相应的资源时，无需回源，由离用户最近的CDN节点返回给用户，加快访问速度。

## 2. 开发前准备
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已具备开发环境 ，支持java8及以上版本。
- CDN服务软件开发工具包（CDN SDK，Content Delivery Network Software Development Kit）是对CDN服务提供的REST API进行的封装，获取[华为云CDN开发工具包（SDK）](https://github.com/huaweicloud/huaweicloud-sdk-java-v3) ，SDK的使用方法请参见[CDN SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK），[AK/SK参考](https://support.huaweicloud.com/iam_faq/iam_01_0022.html) 。
### 2.1 认证鉴权
华为云JAVA SDK支持两种认证方式：token认证和AK/SK认证，可以选择其中一种进行认证鉴权。
- Token认证：通过Token认证通用请求。[参考](https://support.huaweicloud.com/api-iam/iam_30_0000.html)
- AK/SK认证：通过AK（Access Key ID）/SK（Secret Access Key）加密调用请求。推荐使用AK/SK认证，其安全性比Token认证要高。[参考](https://support.huaweicloud.com/iam_faq/iam_01_0022.html)
## 3. 安装SDK
内容分发网络服务端SDK支持java8及以上版本。执行“ java -version” 检查当前java的版本信息。
使用服务端SDK前，您需要安装“huaweicloudsdkcore ”和 “huaweicloudsdkcdn”，具体的SDK版本号请参见 [SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。



## 4. 代码示例
### 4.1 导入pom依赖
``` java
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cdn</artifactId>
    <version>${cdn.version}</version>
</dependency>
```
### 4.2 导入依赖模块
``` java
import com.huaweicloud.sdk.cdn.v2.CdnClient;
import com.huaweicloud.sdk.cdn.v2.model.CreatePreheatingTasksRequest;
import com.huaweicloud.sdk.cdn.v2.model.CreatePreheatingTasksResponse;
import com.huaweicloud.sdk.cdn.v2.model.CreateRefreshTasksRequest;
import com.huaweicloud.sdk.cdn.v2.model.CreateRefreshTasksResponse;
import com.huaweicloud.sdk.cdn.v2.model.PreheatingTaskRequest;
import com.huaweicloud.sdk.cdn.v2.model.PreheatingTaskRequestBody;
import com.huaweicloud.sdk.cdn.v2.model.RefreshTaskRequest;
import com.huaweicloud.sdk.cdn.v2.model.RefreshTaskRequestBody;
import com.huaweicloud.sdk.cdn.v2.model.ShowHistoryTaskDetailsRequest;
import com.huaweicloud.sdk.cdn.v2.model.ShowHistoryTaskDetailsResponse;
import com.huaweicloud.sdk.cdn.v2.model.ShowHistoryTasksRequest;
import com.huaweicloud.sdk.cdn.v2.model.ShowHistoryTasksResponse;
import com.huaweicloud.sdk.cdn.v2.model.ShowUrlTaskInfoRequest;
import com.huaweicloud.sdk.cdn.v2.model.ShowUrlTaskInfoResponse;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;

import java.util.ArrayList;
import java.util.List;
```
### 4.3 初始化认证信息, 及客户端
示例相关参数说明如下所示：
- ak：华为云账号Access Key。
- sk：华为云账号Secret Access Key。
- iamEndpoint：iam身份认证域名。
- endpoint：接口调用域名。
- domainName：需要查询的域名。
- accountId：账号id
``` java
    public static void main(String[] args) {
            // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
            // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
            String ak = System.getenv("HUAWEICLOUD_SDK_AK");
            String sk = System.getenv("HUAWEICLOUD_SDK_SK");
            String iamEndpoint = "<iam endpoint>";
            String endpoint = "<endpoint>";
            String region = "<region>";
            String accountId = "<account_id>";
    
            ICredential auth = new GlobalCredentials().withIamEndpoint(iamEndpoint).withAk(ak).withSk(sk).withDomainId(accountId);
            Region regionObj = new Region(region, endpoint);
            CdnClient client = CdnClient.newBuilder().withCredential(auth).withRegion(regionObj).build();
    
            // 创建刷新任务
            createRefreshTasks(client);
    
            // 创建预热任务
            createPreheatingTasks(client);
    
            // 查询刷新预热任务
            showHistoryTasks(client);
    
            // 查询刷新预热任务详情
            showHistoryTaskDetails(client);
    
            // 查询刷新预热URL记录
            showUrlTaskInfo(client);
        }
```
#### 4.3.1 创建刷新任务
示例相关参数说明如下所示，详情[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/sdk?api=CreateRefreshTasks&version=v2)：
- refresh_url：刷新url。
- zhUrlEncode：是否中文编码。
- type：刷新类型。
- mode：刷新方式。
``` java
    public static void createRefreshTasks(CdnClient client) {
            CreateRefreshTasksRequest request = new CreateRefreshTasksRequest();
            RefreshTaskRequest body = new RefreshTaskRequest();
    
            String refreshUrl = "<refresh_url>";
            List<String> listRefreshTaskUrls = new ArrayList<>();
            listRefreshTaskUrls.add(refreshUrl);
    
            Boolean zhUrlEncode = Boolean.getBoolean("<zh_url_encode>");
            String type = "<refresh_type>";
            String mode = "<refresh_mode>";
    
            RefreshTaskRequestBody refreshTaskbody = new RefreshTaskRequestBody();
            refreshTaskbody.withType(RefreshTaskRequestBody.TypeEnum.fromValue(type))
                    .withMode(RefreshTaskRequestBody.ModeEnum.fromValue(mode))
                    .withZhUrlEncode(zhUrlEncode)
                    .withUrls(listRefreshTaskUrls);
            body.withRefreshTask(refreshTaskbody);
            request.withBody(body);
    
            try {
                CreateRefreshTasksResponse response = client.createRefreshTasks(request);
                System.out.println(response.toString());
            } catch (ConnectionException e) {
                e.printStackTrace();
            } catch (RequestTimeoutException e) {
                e.printStackTrace();
            } catch (ServiceResponseException e) {
                e.printStackTrace();
                System.out.println(e.getHttpStatusCode());
                System.out.println(e.getRequestId());
                System.out.println(e.getErrorCode());
                System.out.println(e.getErrorMsg());
            }
        }
```
#### 4.3.2 创建预热任务
示例相关参数说明如下所示，详情[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/sdk?api=CreatePreheatingTasks&version=v2)：
- preheating_url：预热url。
- zhUrlEncode：是否中文编码。
``` java
    public static void createPreheatingTasks(CdnClient client) {
            CreatePreheatingTasksRequest request = new CreatePreheatingTasksRequest();
            PreheatingTaskRequest body = new PreheatingTaskRequest();
    
            String preheatingUrl = "<preheating_url>";
            List<String> listPreheatingTaskUrls = new ArrayList<>();
            listPreheatingTaskUrls.add(preheatingUrl);
    
            Boolean zhUrlEncode = Boolean.getBoolean("<zh_url_encode>");
    
            PreheatingTaskRequestBody preheatingTaskbody = new PreheatingTaskRequestBody();
            preheatingTaskbody.withZhUrlEncode(zhUrlEncode).withUrls(listPreheatingTaskUrls);
            body.withPreheatingTask(preheatingTaskbody);
            request.withBody(body);
    
            try {
                CreatePreheatingTasksResponse response = client.createPreheatingTasks(request);
                System.out.println(response.toString());
            } catch (ConnectionException e) {
                e.printStackTrace();
            } catch (RequestTimeoutException e) {
                e.printStackTrace();
            } catch (ServiceResponseException e) {
                e.printStackTrace();
                System.out.println(e.getHttpStatusCode());
                System.out.println(e.getRequestId());
                System.out.println(e.getErrorCode());
                System.out.println(e.getErrorMsg());
            }
        }
```
#### 4.3.3 查询刷新预热任务
示例相关参数说明如下所示，详情[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/sdk?api=ShowHistoryTasks&version=v2)：
- page_size：单页最大数量。
- page_number：查询页数。
- start_date：查询起始时间。
- end_date：查询结束时间。
- order_field：用于排序字段。
- order_type：排序方式。
- task_status：任务状态。
- file_type：文件类型。
- task_type：任务类型。
``` java
    private static void showHistoryTasks(CdnClient client) {
            ShowHistoryTasksRequest request = new ShowHistoryTasksRequest();
            int pageSize = Integer.getInteger("<page_size>");
            int pageNumber = Integer.getInteger("<page_number>");
            long startDate = Long.getLong("<start_date>");
            long endDate = Long.getLong("<end_date>");
            String orderField = "<order_field>";
            String orderType = "<order_type>";
            String status = "<task_status>";
            String fileType = "<file_type>";
            String taskType = "<task_type>";
    
            request.withPageSize(pageSize);
            request.withPageNumber(pageNumber);
            request.withStartDate(startDate);
            request.withEndDate(endDate);
            request.withOrderField(orderField);
            request.withOrderType(orderType);
            request.withStatus(ShowHistoryTasksRequest.StatusEnum.fromValue(status));
            request.withFileType(ShowHistoryTasksRequest.FileTypeEnum.fromValue(fileType));
            request.withTaskType(ShowHistoryTasksRequest.TaskTypeEnum.fromValue(taskType));
    
            try {
                ShowHistoryTasksResponse response = client.showHistoryTasks(request);
                System.out.println(response.toString());
            } catch (ConnectionException e) {
                e.printStackTrace();
            } catch (RequestTimeoutException e) {
                e.printStackTrace();
            } catch (ServiceResponseException e) {
                e.printStackTrace();
                System.out.println(e.getHttpStatusCode());
                System.out.println(e.getRequestId());
                System.out.println(e.getErrorCode());
                System.out.println(e.getErrorMsg());
            }
        }
```
#### 4.3.4 查询刷新预热任务详情
示例相关参数说明如下所示，详情[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/sdk?api=ShowHistoryTaskDetails&version=v2)：
- history_tasks_id：任务id。
- page_size：单页最大数量。
- page_number：查询页数。
- task_status：任务状态。
- create_time：创建时间。
- url：刷新预热url。
``` java
    private static void showHistoryTaskDetails(CdnClient client) {
            ShowHistoryTaskDetailsRequest request = new ShowHistoryTaskDetailsRequest();
            String historyTasksId = "<history_tasks_id>";
            int pageSize = Integer.getInteger("<page_size>");
            int pageNumber = Integer.getInteger("<page_number>");
            String status = "<task_status>";
            long createTime = Long.getLong("<create_time>");
            String url = "<url>";
    
            request.withHistoryTasksId(historyTasksId);
            request.withPageSize(pageSize);
            request.withPageNumber(pageNumber);
            request.withStatus(status);
            request.withUrl(url);
            request.withCreateTime(createTime);
    
            try {
                ShowHistoryTaskDetailsResponse response = client.showHistoryTaskDetails(request);
                System.out.println(response.toString());
            } catch (ConnectionException e) {
                e.printStackTrace();
            } catch (RequestTimeoutException e) {
                e.printStackTrace();
            } catch (ServiceResponseException e) {
                e.printStackTrace();
                System.out.println(e.getHttpStatusCode());
                System.out.println(e.getRequestId());
                System.out.println(e.getErrorCode());
                System.out.println(e.getErrorMsg());
            }
        }
```
#### 4.3.5 查询刷新预热URL记录
示例相关参数说明如下所示，详情[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/sdk?api=ShowUrlTaskInfo&version=v2)：
- start_time：开始时间。
- end_time：结束时间。
- offset：偏移量。
- limit：单次查询条数。
- url：刷新预热url。
- task_status：任务状态。
- file_type：文件类型。
- task_type：任务类型。
``` java
    private static void showUrlTaskInfo(CdnClient client) {
            ShowUrlTaskInfoRequest request = new ShowUrlTaskInfoRequest();
            long startTime = Long.getLong("<start_time>");
            long endTime = Long.getLong("<end_time>");
            int offset = Integer.getInteger("<offset>");
            int limit = Integer.getInteger("<limit>");
            String url = "<url>";
            String status = "<task_status>";
            String fileType = "<file_type>";
            String taskType = "<task_type>";
    
            request.withStartTime(startTime);
            request.withEndTime(endTime);
            request.withOffset(offset);
            request.withLimit(limit);
            request.withUrl(url);
            request.withTaskType(taskType);
            request.withStatus(status);
            request.withFileType(fileType);
    
            try {
                ShowUrlTaskInfoResponse response = client.showUrlTaskInfo(request);
                System.out.println(response.toString());
            } catch (ConnectionException e) {
                e.printStackTrace();
            } catch (RequestTimeoutException e) {
                e.printStackTrace();
            } catch (ServiceResponseException e) {
                e.printStackTrace();
                System.out.println(e.getHttpStatusCode());
                System.out.println(e.getRequestId());
                System.out.println(e.getErrorCode());
                System.out.println(e.getErrorMsg());
            }
        }
```
## 5. 参考
用户可以根据以上示例了解刷新预热相关操作，更多信息请参考 [内容分发网络CDN文档](https://support.huaweicloud.com/cdn/index.html)

## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-12-25 | 1.0 | 文档首次发布 |