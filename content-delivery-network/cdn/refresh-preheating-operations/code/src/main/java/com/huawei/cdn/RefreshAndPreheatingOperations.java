package com.huawei.cdn;

import com.huaweicloud.sdk.cdn.v2.CdnClient;
import com.huaweicloud.sdk.cdn.v2.model.CreatePreheatingTasksRequest;
import com.huaweicloud.sdk.cdn.v2.model.CreatePreheatingTasksResponse;
import com.huaweicloud.sdk.cdn.v2.model.CreateRefreshTasksRequest;
import com.huaweicloud.sdk.cdn.v2.model.CreateRefreshTasksResponse;
import com.huaweicloud.sdk.cdn.v2.model.PreheatingTaskRequest;
import com.huaweicloud.sdk.cdn.v2.model.PreheatingTaskRequestBody;
import com.huaweicloud.sdk.cdn.v2.model.RefreshTaskRequest;
import com.huaweicloud.sdk.cdn.v2.model.RefreshTaskRequestBody;
import com.huaweicloud.sdk.cdn.v2.model.ShowHistoryTaskDetailsRequest;
import com.huaweicloud.sdk.cdn.v2.model.ShowHistoryTaskDetailsResponse;
import com.huaweicloud.sdk.cdn.v2.model.ShowHistoryTasksRequest;
import com.huaweicloud.sdk.cdn.v2.model.ShowHistoryTasksResponse;
import com.huaweicloud.sdk.cdn.v2.model.ShowUrlTaskInfoRequest;
import com.huaweicloud.sdk.cdn.v2.model.ShowUrlTaskInfoResponse;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;

import java.util.ArrayList;
import java.util.List;

public class RefreshAndPreheatingOperations {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String iamEndpoint = "<iam endpoint>";
        String endpoint = "<endpoint>";
        String region = "<region>";
        String accountId = "<account_id>";

        ICredential auth = new GlobalCredentials().withIamEndpoint(iamEndpoint).withAk(ak).withSk(sk).withDomainId(accountId);
        Region regionObj = new Region(region, endpoint);
        CdnClient client = CdnClient.newBuilder().withCredential(auth).withRegion(regionObj).build();

        // 创建刷新任务
        createRefreshTasks(client);

        // 创建预热任务
        createPreheatingTasks(client);

        // 查询刷新预热任务
        showHistoryTasks(client);

        // 查询刷新预热任务详情
        showHistoryTaskDetails(client);

        // 查询刷新预热URL记录
        showUrlTaskInfo(client);
    }

    public static void createRefreshTasks(CdnClient client) {
        CreateRefreshTasksRequest request = new CreateRefreshTasksRequest();
        RefreshTaskRequest body = new RefreshTaskRequest();

        String refreshUrl = "<refresh_url>";
        List<String> listRefreshTaskUrls = new ArrayList<>();
        listRefreshTaskUrls.add(refreshUrl);

        Boolean zhUrlEncode = Boolean.getBoolean("<zh_url_encode>");
        String type = "<refresh_type>";
        String mode = "<refresh_mode>";

        RefreshTaskRequestBody refreshTaskbody = new RefreshTaskRequestBody();
        refreshTaskbody.withType(RefreshTaskRequestBody.TypeEnum.fromValue(type))
                .withMode(RefreshTaskRequestBody.ModeEnum.fromValue(mode))
                .withZhUrlEncode(zhUrlEncode)
                .withUrls(listRefreshTaskUrls);
        body.withRefreshTask(refreshTaskbody);
        request.withBody(body);

        try {
            CreateRefreshTasksResponse response = client.createRefreshTasks(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            printExceptionMsg(e);
        }
    }

    public static void createPreheatingTasks(CdnClient client) {
        CreatePreheatingTasksRequest request = new CreatePreheatingTasksRequest();
        PreheatingTaskRequest body = new PreheatingTaskRequest();

        String preheatingUrl = "<preheating_url>";
        List<String> listPreheatingTaskUrls = new ArrayList<>();
        listPreheatingTaskUrls.add(preheatingUrl);

        Boolean zhUrlEncode = Boolean.getBoolean("<zh_url_encode>");

        PreheatingTaskRequestBody preheatingTaskbody = new PreheatingTaskRequestBody();
        preheatingTaskbody.withZhUrlEncode(zhUrlEncode).withUrls(listPreheatingTaskUrls);
        body.withPreheatingTask(preheatingTaskbody);
        request.withBody(body);

        try {
            CreatePreheatingTasksResponse response = client.createPreheatingTasks(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            printExceptionMsg(e);
        }
    }

    private static void showHistoryTasks(CdnClient client) {
        ShowHistoryTasksRequest request = new ShowHistoryTasksRequest();

        int pageSize = Integer.getInteger("<page_size>");
        int pageNumber = Integer.getInteger("<page_number>");
        long startDate = Long.getLong("<start_date>");
        long endDate = Long.getLong("<end_date>");
        String orderField = "<order_field>";
        String orderType = "<order_type>";
        String status = "<task_status>";
        String fileType = "<file_type>";
        String taskType = "<task_type>";

        request.withPageSize(pageSize);
        request.withPageNumber(pageNumber);
        request.withStartDate(startDate);
        request.withEndDate(endDate);
        request.withOrderField(orderField);
        request.withOrderType(orderType);
        request.withStatus(ShowHistoryTasksRequest.StatusEnum.fromValue(status));
        request.withFileType(ShowHistoryTasksRequest.FileTypeEnum.fromValue(fileType));
        request.withTaskType(ShowHistoryTasksRequest.TaskTypeEnum.fromValue(taskType));

        try {
            ShowHistoryTasksResponse response = client.showHistoryTasks(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            printExceptionMsg(e);
        }
    }

    private static void showHistoryTaskDetails(CdnClient client) {
        ShowHistoryTaskDetailsRequest request = new ShowHistoryTaskDetailsRequest();

        String historyTasksId = "<history_tasks_id>";
        int pageSize = Integer.getInteger("<page_size>");
        int pageNumber = Integer.getInteger("<page_number>");
        String status = "<task_status>";
        long createTime = Long.getLong("<create_time>");
        String url = "<url>";

        request.withHistoryTasksId(historyTasksId);
        request.withPageSize(pageSize);
        request.withPageNumber(pageNumber);
        request.withStatus(status);
        request.withUrl(url);
        request.withCreateTime(createTime);

        try {
            ShowHistoryTaskDetailsResponse response = client.showHistoryTaskDetails(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            printExceptionMsg(e);
        }
    }

    private static void showUrlTaskInfo(CdnClient client) {
        ShowUrlTaskInfoRequest request = new ShowUrlTaskInfoRequest();

        long startTime = Long.getLong("<start_time>");
        long endTime = Long.getLong("<end_time>");
        int offset = Integer.getInteger("<offset>");
        int limit = Integer.getInteger("<limit>");
        String url = "<url>";
        String status = "<task_status>";
        String fileType = "<file_type>";
        String taskType = "<task_type>";

        request.withStartTime(startTime);
        request.withEndTime(endTime);
        request.withOffset(offset);
        request.withLimit(limit);
        request.withUrl(url);
        request.withTaskType(taskType);
        request.withStatus(status);
        request.withFileType(fileType);

        try {
            ShowUrlTaskInfoResponse response = client.showUrlTaskInfo(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            printExceptionMsg(e);
        }
    }

    private static void printExceptionMsg(ServiceResponseException e) {
        System.out.println(e.getHttpStatusCode());
        System.out.println(e.getRequestId());
        System.out.println(e.getErrorCode());
        System.out.println(e.getErrorMsg());
    }
}
