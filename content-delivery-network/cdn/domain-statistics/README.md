## 1. 示例简介

CDN（Content Delivery Network，内容分发网络）。CDN是构建在现有互联网基础之上的一层智能虚拟网络，通过在网络各处部署节点服务器，实现将源站内容分发至所有CDN节点，使用户可以就近获得所需的内容。CDN服务缩短了用户查看内容的访问延迟，提高了用户访问网站的响应速度与网站的可用性，解决了网络带宽小、用户访问量大、网点分布不均等问题。
本示例指导用户进行对域名指标的查询操作，包括按区域运营商查询域名统计数据，查询统计域名数据，查询TOP100 URL明细等。

## 2. 开发前准备
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已具备开发环境 ，支持java8及以上版本。
- CDN服务软件开发工具包（CDN SDK，Content Delivery Network Software Development Kit）是对CDN服务提供的REST API进行的封装，获取[华为云CDN开发工具包(SDK)](https://github.com/huaweicloud/huaweicloud-sdk-java-v3) ，SDK的使用方法请参见[CDN SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK），[AK/SK参考](https://support.huaweicloud.com/iam_faq/iam_01_0022.html) 。

### 2.1 认证鉴权
华为云JAVA SDK支持两种认证方式：token认证和AK/SK认证，可以选择其中一种进行认证鉴权。
- Token认证：通过Token认证通用请求。[参考](https://support.huaweicloud.com/api-iam/iam_30_0000.html)
- AK/SK认证：通过AK(Access Key ID)/SK(Secret Access Key)加密调用请求。推荐使用AK/SK认证，其安全性比Token认证要高。[参考](https://support.huaweicloud.com/iam_faq/iam_01_0022.html)

## 3. 安装SDK
内容分发网络服务端SDK支持java8及以上版本。执行`java -version`检查当前java的版本信息。
使用服务端SDK前，您需要安装“huaweicloudsdkcore”和 “huaweicloudsdkcdn”，具体的SDK版本号请参见 [SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。

## 4. 代码示例

### 4.1 导入pom依赖

``` xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cdn</artifactId>
    <version>3.1.54</version>
</dependency>
```

### 4.2 导入依赖模块

``` java
import java.util.Arrays;
import com.huaweicloud.sdk.cdn.v2.CdnClient;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainLocationStatsRequest;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainLocationStatsResponse;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainStatsRequest;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainStatsResponse;
import com.huaweicloud.sdk.cdn.v2.model.ShowTopUrlRequest;
import com.huaweicloud.sdk.cdn.v2.model.ShowTopUrlResponse;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.utils.JsonUtils;
```

### 4.3 初始化认证信息, 及客户端

示例相关参数说明如下所示：
- ak：华为云账号Access Key。
- sk：华为云账号Secret Access Key。
- iamEndpoint：iam身份认证域名。
- endpoint：接口调用域名。
- region: 服务所在区域。

``` java
public static void main(String[] args) {
    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    String iamEndpoint = "<iam endpoint>";
    String endpoint = "<endpoint>";
    
    ICredential auth = new GlobalCredentials().withIamEndpoint(iamEndpoint).withAk(ak).withSk(sk);
    CdnClient client = CdnClient.newBuilder().withCredential(auth).withEndpoints(Arrays.asList(endpoint)).build();
    
    // 按区域运营商查询域名统计数据
    showDomainLocationStats(client);
    
    // 查询域名统计数据
    showDomainStats(client);
    
    // 查询TOP100 URL明细
    showTopUrl(client);
}
```

## 4.4 按区域运营商查询域名统计数据

示例相关参数说明如下所示，详情[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/doc?api=ShowDomainLocationStats&version=v2)：
- client：客户端。
- domainName：域名。
- action：操作名称。
- startTime: 开始时间。
- endTime: 结束时间。
- statType：查询指标类型。
- interval：查询时间粒度。
- country: 国家&地区编码。
- province: 省份编码。
- isp：运营商编码。
- groupBy：数据分组方式。
- enterpriseProjectId: 企业项目id。

``` java
public static void showDomainLocationStats(CdnClient client) {
    String domainName = "<domain_name>";
    String action = "<action>";
    String startTime = "<start_time>";
    String endTime = "<end_time>";
    String statType = "<stat_type>";
    String interval = "<interval>";
    String country = "<country>";
    String province = "<province>";
    String isp = "<isp>";
    String groupBy = "<group_by>";
    String enterpriseProjectId = "<enterprise_project_id>";
    
    ShowDomainLocationStatsRequest request = new ShowDomainLocationStatsRequest();
    request.setDomainName(domainName);
    request.setAction(action);
    request.setCountry(country);
    request.setStatType(statType);
    request.setStartTime(Long.valueOf(startTime));
    request.setEndTime(Long.valueOf(endTime));
    request.setInterval(Long.valueOf(interval));
    request.setProvince(province);
    request.setIsp(isp);
    request.setGroupBy(groupBy);
    request.setEnterpriseProjectId(enterpriseProjectId);
    
    try {
        ShowDomainLocationStatsResponse response = client.showDomainLocationStats(request);
        int resultCode = response.getHttpStatusCode();
        if (resultCode == 200) {
            System.out.println("show domain location stats success");
        }
        System.out.println(JsonUtils.toJSON(response));
    } catch (ConnectionException | RequestTimeoutException e) {
        System.out.println(e.getMessage());
    } catch (ServiceResponseException e) {
        printExceptionMsg(e);
    }
}
```

## 4.5 查询域名统计数据

示例相关参数说明如下所示，详情[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/doc?version=v2&api=ShowDomainStats)：
- client：客户端。
- domainName：域名。
- action：操作名称。
- startTime: 开始时间。
- endTime: 结束时间。
- statType：查询指标类型。
- interval：查询时间粒度。
- groupBy：数据分组方式。
- serviceArea：服务区域。
- enterpriseProjectId: 企业项目id。

``` java
public static void showDomainStats(CdnClient client) {
    String domainName = "<domain_name>";
    String action = "<action>";
    String startTime = "<start_time>";
    String endTime = "<end_time>";
    String statType = "<stat_type>";
    String interval = "<interval>";
    String groupBy = "<group_by>";
    String serviceArea = "<service_area>";
    String enterpriseProjectId = "<enterprise_project_id>";
    
    ShowDomainStatsRequest request = new ShowDomainStatsRequest();
    request.setDomainName(domainName);
    request.setAction(action);
    request.setGroupBy(groupBy);
    request.setStatType(statType);
    request.setStartTime(Long.valueOf(startTime));
    request.setEndTime(Long.valueOf(endTime));
    request.setInterval(Long.valueOf(interval));
    request.setServiceArea(serviceArea);
    request.setEnterpriseProjectId(enterpriseProjectId);
    
    try {
        ShowDomainStatsResponse response = client.showDomainStats(request);
        int resultCode = response.getHttpStatusCode();
        if (resultCode == 200) {
            System.out.println("show domain stats success");
        }
        System.out.println(JsonUtils.toJSON(response));
    } catch (ConnectionException | RequestTimeoutException e) {
        System.out.println(e.getMessage());
    } catch (ServiceResponseException e) {
        printExceptionMsg(e);
    }
}
```

## 4.6 查询TOP100 URL明细

示例相关参数说明如下所示，详情[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/doc?version=v2&api=ShowTopUrl)：
- client：客户端。 
- domainName：域名。
- startTime: 开始时间。
- endTime: 结束时间。
- statType：查询指标类型。
- serviceArea：服务区域。
- enterpriseProjectId: 企业项目id。

``` java
public static void showTopUrl(CdnClient client) {
    String domainName = "<domain_name>";
    String startTime = "<start_time>";
    String endTime = "<end_time>";
    String statType = "<stat_type>";
    String serviceArea = "<service_area>";
    String enterpriseProjectId = "<enterprise_project_id>";
    
    ShowTopUrlRequest request = new ShowTopUrlRequest();
    request.setDomainName(domainName);
    request.setStatType(statType);
    request.setStartTime(Long.valueOf(startTime));
    request.setEndTime(Long.valueOf(endTime));
    request.setServiceArea(serviceArea);
    request.setEnterpriseProjectId(enterpriseProjectId);
    
    try {
        ShowTopUrlResponse response = client.showTopUrl(request);
        int resultCode = response.getHttpStatusCode();
        if (resultCode == 200) {
            System.out.println("show top url success");
        }
        System.out.println(JsonUtils.toJSON(response));
    } catch (ConnectionException | RequestTimeoutException e) {
        System.out.println(e.getMessage());
    } catch (ServiceResponseException e) {
        printExceptionMsg(e);
    }
}
```

## 5. 参考

用户可以根据以上示例了解域名相关操作，更多信息请参考 [内容分发网络CDN文档](https://support.huaweicloud.com/cdn/index.html)

## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-12-06 | 1.0 | 文档首次发布 |