package com.huawei.cdn;

import java.util.Arrays;

import com.huaweicloud.sdk.cdn.v2.CdnClient;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainLocationStatsRequest;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainLocationStatsResponse;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainStatsRequest;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainStatsResponse;
import com.huaweicloud.sdk.cdn.v2.model.ShowTopUrlRequest;
import com.huaweicloud.sdk.cdn.v2.model.ShowTopUrlResponse;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.utils.JsonUtils;

public class DomainStatistics {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String iamEndpoint = "<iam endpoint>";
        String endpoint = "<endpoint>";
        
        ICredential auth = new GlobalCredentials().withIamEndpoint(iamEndpoint).withAk(ak).withSk(sk);
        CdnClient client = CdnClient.newBuilder().withCredential(auth).withEndpoints(Arrays.asList(endpoint)).build();
        
        // 按区域运营商查询域名统计数据
        showDomainLocationStats(client);
        
        // 查询域名统计数据
        showDomainStats(client);
        
        // 查询TOP100 URL明细
        showTopUrl(client);
    }
    
    public static void showDomainLocationStats(CdnClient client) {
        String domainName = "<domain_name>";
        String action = "<action>";
        String startTime = "<start_time>";
        String endTime = "<end_time>";
        String statType = "<stat_type>";
        String interval = "<interval>";
        String country = "<country>";
        String province = "<province>";
        String isp = "<isp>";
        String groupBy = "<group_by>";
        String enterpriseProjectId = "<enterprise_project_id>";
        
        ShowDomainLocationStatsRequest request = new ShowDomainLocationStatsRequest();
        request.setDomainName(domainName);
        request.setAction(action);
        request.setCountry(country);
        request.setStatType(statType);
        request.setStartTime(Long.valueOf(startTime));
        request.setEndTime(Long.valueOf(endTime));
        request.setInterval(Long.valueOf(interval));
        request.setProvince(province);
        request.setIsp(isp);
        request.setGroupBy(groupBy);
        request.setEnterpriseProjectId(enterpriseProjectId);
        
        try {
            ShowDomainLocationStatsResponse response = client.showDomainLocationStats(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode == 200) {
                System.out.println("show domain location stats success");
            }
            System.out.println(JsonUtils.toJSON(response));
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            printExceptionMsg(e);
        }
    }
    
    public static void showDomainStats(CdnClient client) {
        String domainName = "<domain_name>";
        String action = "<action>";
        String startTime = "<start_time>";
        String endTime = "<end_time>";
        String statType = "<stat_type>";
        String interval = "<interval>";
        String groupBy = "<group_by>";
        String serviceArea = "<service_area>";
        String enterpriseProjectId = "<enterprise_project_id>";
        
        ShowDomainStatsRequest request = new ShowDomainStatsRequest();
        request.setDomainName(domainName);
        request.setAction(action);
        request.setGroupBy(groupBy);
        request.setStatType(statType);
        request.setStartTime(Long.valueOf(startTime));
        request.setEndTime(Long.valueOf(endTime));
        request.setInterval(Long.valueOf(interval));
        request.setServiceArea(serviceArea);
        request.setEnterpriseProjectId(enterpriseProjectId);
        
        try {
            ShowDomainStatsResponse response = client.showDomainStats(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode == 200) {
                System.out.println("show domain stats success");
            }
            System.out.println(JsonUtils.toJSON(response));
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            printExceptionMsg(e);
        }
    }
    
    public static void showTopUrl(CdnClient client) {
        String domainName = "<domain_name>";
        String startTime = "<start_time>";
        String endTime = "<end_time>";
        String statType = "<stat_type>";
        String serviceArea = "<service_area>";
        String enterpriseProjectId = "<enterprise_project_id>";
        
        ShowTopUrlRequest request = new ShowTopUrlRequest();
        request.setDomainName(domainName);
        request.setStatType(statType);
        request.setStartTime(Long.valueOf(startTime));
        request.setEndTime(Long.valueOf(endTime));
        request.setServiceArea(serviceArea);
        request.setEnterpriseProjectId(enterpriseProjectId);
        
        try {
            ShowTopUrlResponse response = client.showTopUrl(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode == 200) {
                System.out.println("show top url success");
            }
            System.out.println(JsonUtils.toJSON(response));
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            printExceptionMsg(e);
        }
    }
    
    private static void printExceptionMsg(ServiceResponseException e) {
        System.out.println(e.getHttpStatusCode());
        System.out.println(e.getRequestId());
        System.out.println(e.getErrorCode());
        System.out.println(e.getErrorMsg());
    }
}
