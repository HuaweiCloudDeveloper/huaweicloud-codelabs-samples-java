package com.huawei.cdn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.huaweicloud.sdk.cdn.v2.CdnClient;
import com.huaweicloud.sdk.cdn.v2.model.Configs;
import com.huaweicloud.sdk.cdn.v2.model.IpFilter;
import com.huaweicloud.sdk.cdn.v2.model.ModifyDomainConfigRequestBody;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainFullConfigRequest;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainFullConfigResponse;
import com.huaweicloud.sdk.cdn.v2.model.UpdateDomainFullConfigRequest;
import com.huaweicloud.sdk.cdn.v2.model.UpdateDomainFullConfigResponse;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.JsonUtils;

public class DomainFullConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(DomainFullConfig.class);
    
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String iamEndpoint = "<iam endpoint>";
        String endpoint = "<endpoint>";
        String domainName = "your query name";
        String region = "cn-north-1";
        String ipType = "black";
        String ipValue = "you ip";
        String businessType = "you type";
        Integer ipv6Accelerate = 0;
        ICredential auth = new GlobalCredentials().withIamEndpoint(iamEndpoint).withAk(ak).withSk(sk);
        CdnClient client = CdnClient.newBuilder().withCredential(auth).withRegion(new Region(region, endpoint)).build();
        IpFilter setIpFilter = new IpFilter();
        setIpFilter.setType(ipType);
        setIpFilter.setValue(ipValue);
        String setIpFilterResponse = setIpFilterConfig(client, domainName, setIpFilter);
        System.out.println(setIpFilterResponse);
        String getIpFilterResponse = getIpFilterConfig(client, domainName);
        System.out.println(getIpFilterResponse);
        IpFilter deleteIpFilter = new IpFilter();
        deleteIpFilter.setType(ipType);
        String deleteIpFilterResponse = deleteIpFilterConfig(client, domainName, deleteIpFilter);
        System.out.println(deleteIpFilterResponse);
        String setBusinessTypeResponse = setBusinessTypeConfig(client, domainName, businessType);
        System.out.println(setBusinessTypeResponse);
        String getBusinessTypeResponse = getBusinessTypeConfig(client, domainName);
        System.out.println(getBusinessTypeResponse);
        String setIpv6Response = setIpv6Config(client, domainName, ipv6Accelerate);
        System.out.println(setIpv6Response);
        String getIpv6ConfigResponse = getIpv6Config(client, domainName);
        System.out.println(getIpv6ConfigResponse);
    }
    
    public static String setIpFilterConfig(CdnClient client, String domainName, IpFilter ipFilter) {
        UpdateDomainFullConfigRequest request = new UpdateDomainFullConfigRequest();
        ModifyDomainConfigRequestBody body = new ModifyDomainConfigRequestBody();
        Configs configs = new Configs();
        configs.setIpFilter(ipFilter);
        body.setConfigs(configs);
        request.withBody(body);
        request.setDomainName(domainName);
        String result = "";
        try {
            UpdateDomainFullConfigResponse response = client.updateDomainFullConfig(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode == 204) {
                return "set ip filter success";
            }
        } catch (ConnectionException e) {
            LOGGER.error("setIpFilterConfig ConnectionException", e);
        } catch (RequestTimeoutException e) {
            LOGGER.error("setIpFilterConfig RequestTimeoutException", e);
            
        } catch (ServiceResponseException e) {
            LOGGER.error("ServiceResponseException RequestTimeoutException", e);
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return result;
    }
    
    public static String getIpFilterConfig(CdnClient client, String domainName) {
        ShowDomainFullConfigRequest request = new ShowDomainFullConfigRequest();
        request.setDomainName(domainName);
        String result = "";
        try {
            ShowDomainFullConfigResponse response = client.showDomainFullConfig(request);
            result = JsonUtils.toJSON(response.getConfigs().getIpFilter());
        } catch (ConnectionException e) {
            LOGGER.error("getIpFilterConfig ConnectionException", e);
        } catch (RequestTimeoutException e) {
            LOGGER.error("getIpFilterConfig RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            LOGGER.error("getIpFilterConfig ServiceResponseException", e);
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return result;
    }
    
    public static String deleteIpFilterConfig(CdnClient client, String domainName, IpFilter ipFilter) {
        UpdateDomainFullConfigRequest request;
        request = new UpdateDomainFullConfigRequest();
        ModifyDomainConfigRequestBody body = new ModifyDomainConfigRequestBody();
        Configs configs = new Configs();
        configs.setIpFilter(ipFilter);
        body.setConfigs(configs);
        request.withBody(body);
        request.setDomainName(domainName);
        String result = "";
        try {
            UpdateDomainFullConfigResponse response = client.updateDomainFullConfig(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode == 204) {
                return "delete ip filter success";
            }
        } catch (ConnectionException e) {
            LOGGER.error("deleteIpFilterConfig ConnectionException", e);
        } catch (RequestTimeoutException e) {
            LOGGER.error("deleteIpFilterConfig RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            LOGGER.error("deleteIpFilterConfig ServiceResponseException", e);
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return result;
    }
    
    public static String setBusinessTypeConfig(CdnClient client, String domainName, String businessType) {
        UpdateDomainFullConfigRequest request = new UpdateDomainFullConfigRequest();
        ModifyDomainConfigRequestBody body = new ModifyDomainConfigRequestBody();
        Configs configs = new Configs();
        configs.setBusinessType(businessType);
        request.setDomainName(domainName);
        body.setConfigs(configs);
        request.withBody(body);
        String result = "";
        try {
            UpdateDomainFullConfigResponse response = client.updateDomainFullConfig(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode == 204) {
                return "set business type success";
            }
        } catch (ConnectionException e) {
            LOGGER.error("setBusinessTypeConfig ConnectionException", e);
        } catch (RequestTimeoutException e) {
            LOGGER.error("setBusinessTypeConfig RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            LOGGER.error("setBusinessTypeConfig ServiceResponseException", e);
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return result;
    }
    
    public static String getBusinessTypeConfig(CdnClient client, String domainName) {
        ShowDomainFullConfigRequest request = new ShowDomainFullConfigRequest();
        request.setDomainName(domainName);
        String result = "";
        try {
            ShowDomainFullConfigResponse response = client.showDomainFullConfig(request);
            result = response.getConfigs().getBusinessType();
        } catch (ConnectionException e) {
            LOGGER.error("getBusinessTypeConfig ConnectionException", e);
        } catch (RequestTimeoutException e) {
            LOGGER.error("getBusinessTypeConfig RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            LOGGER.error("getBusinessTypeConfig ServiceResponseException", e);
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return result;
    }
    
    public static String setIpv6Config(CdnClient client, String domainName, Integer ipv6Accelerate) {
        UpdateDomainFullConfigRequest request = new UpdateDomainFullConfigRequest();
        ModifyDomainConfigRequestBody body = new ModifyDomainConfigRequestBody();
        Configs configs = new Configs();
        configs.setIpv6Accelerate(ipv6Accelerate);
        request.setDomainName(domainName);
        body.setConfigs(configs);
        request.withBody(body);
        String result = "";
        try {
            UpdateDomainFullConfigResponse response = client.updateDomainFullConfig(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode == 204) {
                return "set ipv6 success";
            }
        } catch (ConnectionException e) {
            LOGGER.error("setIpv6Config ConnectionException", e);
        } catch (RequestTimeoutException e) {
            LOGGER.error("setIpv6Config RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            LOGGER.error("setIpv6Config ServiceResponseException", e);
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return result;
    }
    
    public static String getIpv6Config(CdnClient client, String domainName) {
        ShowDomainFullConfigRequest request = new ShowDomainFullConfigRequest();
        request.setDomainName(domainName);
        String result = "";
        try {
            ShowDomainFullConfigResponse response = client.showDomainFullConfig(request);
            Integer status = response.getConfigs().getIpv6Accelerate();
            if (status != null) {
                result = status == 1 ? "开启ipv6" : "关闭ipv6";
            }
        } catch (ConnectionException e) {
            LOGGER.error("getIpv6Config ConnectionException", e);
        } catch (RequestTimeoutException e) {
            LOGGER.error("getIpv6Config RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            LOGGER.error("getIpv6Config ServiceResponseException", e);
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return result;
    }
    
}
