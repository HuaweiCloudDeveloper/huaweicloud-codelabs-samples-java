## 1. 示例简介

CDN（Content Delivery Network，内容分发网络）。CDN是构建在现有互联网基础之上的一层智能虚拟网络，通过在网络各处部署节点服务器，实现将源站内容分发至所有CDN节点，使用户可以就近获得所需的内容。CDN服务缩短了用户查看内容的访问延迟，提高了用户访问网站的响应速度与网站的可用性，解决了网络带宽小、用户访问量大、网点分布不均等问题。
本示例指导用户设置、查询和删除域名下的IP黑白名单信息以及设置、查询业务类型和设置、查询IPV6信息。

## 2. 开发前准备
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已具备开发环境 ，支持java8及以上版本。
- CDN服务软件开发工具包（CDN SDK，Content Delivery Network Software Development Kit）是对CDN服务提供的REST API进行的封装，获取[华为云CDN开发工具包（SDK）](https://github.com/huaweicloud/huaweicloud-sdk-java-v3) ，SDK的使用方法请参见[CDN SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK），[AK/SK参考](https://support.huaweicloud.com/iam_faq/iam_01_0022.html) 。
### 2.1 认证鉴权
华为云JAVA SDK支持两种认证方式：token认证和AK/SK认证，可以选择其中一种进行认证鉴权。
- Token认证：通过Token认证通用请求。[参考](https://support.huaweicloud.com/api-iam/iam_30_0000.html)
- AK/SK认证：通过AK（Access Key ID）/SK（Secret Access Key）加密调用请求。推荐使用AK/SK认证，其安全性比Token认证要高。[参考](https://support.huaweicloud.com/iam_faq/iam_01_0022.html)
## 3. 安装SDK
内容分发网络服务端SDK支持java8及以上版本。执行“ java -version” 检查当前java的版本信息。
使用服务端SDK前，您需要安装“huaweicloudsdkcore ”和 “huaweicloudsdkcdn”，具体的SDK版本号请参见 [SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。



## 4. 代码示例
### 4.1 导入pom依赖
``` java
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cdn</artifactId>
    <version>${cdn.version}</version>
</dependency>
```
### 4.2 导入依赖模块
``` java
import com.huaweicloud.sdk.cdn.v2.CdnClient;
import com.huaweicloud.sdk.cdn.v2.model.Configs;
import com.huaweicloud.sdk.cdn.v2.model.IpFilter;
import com.huaweicloud.sdk.cdn.v2.model.ModifyDomainConfigRequestBody;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainFullConfigRequest;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainFullConfigResponse;
import com.huaweicloud.sdk.cdn.v2.model.UpdateDomainFullConfigRequest;
import com.huaweicloud.sdk.cdn.v2.model.UpdateDomainFullConfigResponse;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.JsonUtils;
```
### 4.3 初始化认证信息, 及客户端
示例相关参数说明如下所示：
- ak：华为云账号Access Key。
- sk：华为云账号Secret Access Key。
- iamEndpoint：iam身份认证域名。
- endpoint：接口调用域名。
- domainName：需要查询的域名。
- region: 服务所在区域。
- ipType: IP黑白名单类型，off：关闭IP黑白名单，black：IP黑名单，white：IP白名单。
- ipValue: 配置IP黑白名单，当type=off时，非必传， 支持IPv6,支持配置IP地址和IP&掩码格式的网段, 多条规则用“,”分割,最多支持配置150个, 多个完全重复的IP/IP段将合并为一个,不支持带通配符的地址，如192.168.0.*[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/doc?version=v2&api=UpdateDomainFullConfig)。
- businessType: 业务类型: web:网站加速，download:文件下载加速，video:点播加速。
- ipv6Accelerate: ipv6设置,1:打开;0:关闭。
``` java
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String iamEndpoint = "<iam endpoint>";
        String endpoint = "<endpoint>";
        String domainName = "your query name";
        String region = "cn-north-1";
        String ipType = "black";
        String ipValue = "you ip";
        String businessType = "you type";
        Integer ipv6Accelerate = 0;
        ICredential auth = new GlobalCredentials().withIamEndpoint(iamEndpoint).withAk(ak).withSk(sk);
        CdnClient client = CdnClient.newBuilder().withCredential(auth).withRegion(new Region(region, endpoint)).build();
        IpFilter setIpFilter = new IpFilter();
        setIpFilter.setType(ipType);
        setIpFilter.setValue(ipValue);
        String setIpFilterResponse = setIpFilterConfig(client, domainName, setIpFilter);
        System.out.println(setIpFilterResponse);
        String getIpFilterResponse = getIpFilterConfig(client, domainName);
        System.out.println(getIpFilterResponse);
        IpFilter deleteIpFilter = new IpFilter();
        deleteIpFilter.setType(ipType);
        String deleteIpFilterResponse = deleteIpFilterConfig(client, domainName, deleteIpFilter);
        System.out.println(deleteIpFilterResponse);
        String setBusinessTypeResponse = setBusinessTypeConfig(client, domainName, businessType);
        System.out.println(setBusinessTypeResponse);
        String getBusinessTypeResponse = getBusinessTypeConfig(client, domainName);
        System.out.println(getBusinessTypeResponse);
        String setIpv6Response = setIpv6Config(client, domainName, ipv6Accelerate);
        System.out.println(setIpv6Response);
        String getIpv6ConfigResponse = getIpv6Config(client, domainName);
        System.out.println(getIpv6ConfigResponse);
    }
```
#### 4.3.1 设置IP黑白名单方法
示例相关参数说明如下所示：
- client：客户端。
- domainName：操作域名。
- ipFilter：ip黑白名单，详情[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/doc?version=v2&api=UpdateDomainFullConfig)IpFilter。
``` java
    public static String setIpFilterConfig(CdnClient client, String domainName, IpFilter ipFilter) {
        UpdateDomainFullConfigRequest request = new UpdateDomainFullConfigRequest();
        ModifyDomainConfigRequestBody body = new ModifyDomainConfigRequestBody();
        Configs configs = new Configs();
        configs.setIpFilter(ipFilter);
        body.setConfigs(configs);
        request.withBody(body);
        request.setDomainName(domainName);
        String result = "";
        try {
            UpdateDomainFullConfigResponse response = client.updateDomainFullConfig(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode == 204) {
                return "set ip filter success";
            }
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return result;
    }
```
#### 4.3.2 查询P黑白名单设置结果方法
``` java
    public static String getIpFilterConfig(CdnClient client, String domainName) {
        ShowDomainFullConfigRequest request = new ShowDomainFullConfigRequest();
        request.setDomainName(domainName);
        String result = "";
        try {
            ShowDomainFullConfigResponse response = client.showDomainFullConfig(request);
            result = JsonUtils.toJSON(response.getConfigs().getIpFilter());
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return result;
    }
```
#### 4.3.3 删除IP黑白名单设置结果
示例相关参数说明如下所示：
- client：客户端。
- domainName：操作域名。
- ipFilter：ip黑白名单，详情[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/doc?version=v2&api=UpdateDomainFullConfig)IpFilter。
``` java
    public static String deleteIpFilterConfig(CdnClient client, String domainName, IpFilter ipFilter) {
        UpdateDomainFullConfigRequest request = new UpdateDomainFullConfigRequest();
        ModifyDomainConfigRequestBody body = new ModifyDomainConfigRequestBody();
        Configs configs = new Configs();
        configs.setIpFilter(ipFilter);
        body.setConfigs(configs);
        request.withBody(body);
        request.setDomainName(domainName);
        String result = "";
        try {
            UpdateDomainFullConfigResponse response = client.updateDomainFullConfig(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode == 204) {
                return "delete ip filter success";
            }
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return result;
    }
```
#### 4.4.1 设置业务类型方法
示例相关参数说明如下所示：
- client：客户端。
- domainName：操作域名。
- businessType：业务类型，详情[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/doc?version=v2&api=UpdateDomainFullConfig)business_type。
``` java
    public static String setBusinessTypeConfig(CdnClient client, String domainName, String businessType) {
        UpdateDomainFullConfigRequest request = new UpdateDomainFullConfigRequest();
        ModifyDomainConfigRequestBody body = new ModifyDomainConfigRequestBody();
        Configs configs = new Configs();
        configs.setBusinessType(businessType);
        request.setDomainName(domainName);
        body.setConfigs(configs);
        request.withBody(body);
        String result = "";
        try {
            UpdateDomainFullConfigResponse response = client.updateDomainFullConfig(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode == 204) {
                return "set business type success";
            }
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return result;
    }
```
#### 4.4.2 查询业务类型方法
``` java
    public static String getBusinessTypeConfig(CdnClient client, String domainName) {
        ShowDomainFullConfigRequest request = new ShowDomainFullConfigRequest();
        request.setDomainName(domainName);
        String result = "";
        try {
            ShowDomainFullConfigResponse response = client.showDomainFullConfig(request);
            result = response.getConfigs().getBusinessType();
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return result;
    }
```
#### 4.5.1 设置IPV6开关方法
示例相关参数说明如下所示：
- client：客户端。
- domainName：操作域名。
- ipv6Accelerate：ipv6设置，详情[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/doc?version=v2&api=UpdateDomainFullConfig)ipv6_accelerate。
``` java
    public static String setIpv6Config(CdnClient client, String domainName, Integer ipv6Accelerate) {
        UpdateDomainFullConfigRequest request = new UpdateDomainFullConfigRequest();
        ModifyDomainConfigRequestBody body = new ModifyDomainConfigRequestBody();
        Configs configs = new Configs();
        configs.setIpv6Accelerate(ipv6Accelerate);
        request.setDomainName(domainName);
        body.setConfigs(configs);
        request.withBody(body);
        String result = "";
        try {
            UpdateDomainFullConfigResponse response = client.updateDomainFullConfig(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode == 204) {
                return "set ipv6 success";
            }
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return result;
    }
```
#### 4.5.2 查询IPV6开关方法
``` java
    public static String getIpv6Config(CdnClient client, String domainName) {
        ShowDomainFullConfigRequest request = new ShowDomainFullConfigRequest();
        request.setDomainName(domainName);
        String result = "";
        try {
            ShowDomainFullConfigResponse response = client.showDomainFullConfig(request);
            Integer status = response.getConfigs().getIpv6Accelerate();
            if (status != null) {
                result = status == 1 ? "开启ipv6" : "关闭ipv6";
            }
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return result;
    }
```
## 5. 参考
用户可以根据IP黑白名单、IPV6设置和业务类型设置的传参模型，举一反三设置其他配置信息，域名配置文档[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/doc?version=v2&api=UpdateDomainFullConfig)。
更多信息请参考 [内容分发网络CDN文档](https://support.huaweicloud.com/cdn/index.html)

## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-08-24 | 1.0 | 文档首次发布 |