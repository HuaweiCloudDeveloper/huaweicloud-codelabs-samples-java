## 1. 示例简介

CDN（Content Delivery Network，内容分发网络）。CDN是构建在现有互联网基础之上的一层智能虚拟网络，通过在网络各处部署节点服务器，实现将源站内容分发至所有CDN节点，使用户可以就近获得所需的内容。CDN服务缩短了用户查看内容的访问延迟，提高了用户访问网站的响应速度与网站的可用性，解决了网络带宽小、用户访问量大、网点分布不均等问题。
本示例指导用户进行域名的基本操作，包括创建、删除、查询、启用/停用加速域名等。

## 2. 开发前准备
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已具备开发环境 ，支持java8及以上版本。
- CDN服务软件开发工具包（CDN SDK，Content Delivery Network Software Development Kit）是对CDN服务提供的REST API进行的封装，获取[华为云CDN开发工具包(SDK)](https://github.com/huaweicloud/huaweicloud-sdk-java-v3) ，SDK的使用方法请参见[CDN SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK），[AK/SK参考](https://support.huaweicloud.com/iam_faq/iam_01_0022.html) 。

### 2.1 认证鉴权
华为云JAVA SDK支持两种认证方式：token认证和AK/SK认证，可以选择其中一种进行认证鉴权。
- Token认证：通过Token认证通用请求。[参考](https://support.huaweicloud.com/api-iam/iam_30_0000.html)
- AK/SK认证：通过AK(Access Key ID)/SK(Secret Access Key)加密调用请求。推荐使用AK/SK认证，其安全性比Token认证要高。[参考](https://support.huaweicloud.com/iam_faq/iam_01_0022.html)

## 3. 安装SDK
内容分发网络服务端SDK支持java8及以上版本。执行`java -version`检查当前java的版本信息。
使用服务端SDK前，您需要安装“huaweicloudsdkcore”和 “huaweicloudsdkcdn”，具体的SDK版本号请参见 [SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。

## 4. 代码示例

### 4.1 导入pom依赖

``` java
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cdn</artifactId>
    <version>${cdn.version}</version>
</dependency>
```

### 4.2 导入依赖模块

``` java
import com.huaweicloud.sdk.cdn.v1.CdnClient;
import com.huaweicloud.sdk.cdn.v1.model.CreateDomainRequest;
import com.huaweicloud.sdk.cdn.v1.model.CreateDomainRequestBody;
import com.huaweicloud.sdk.cdn.v1.model.CreateDomainResponse;
import com.huaweicloud.sdk.cdn.v1.model.DeleteDomainRequest;
import com.huaweicloud.sdk.cdn.v1.model.DeleteDomainResponse;
import com.huaweicloud.sdk.cdn.v1.model.DisableDomainRequest;
import com.huaweicloud.sdk.cdn.v1.model.DisableDomainResponse;
import com.huaweicloud.sdk.cdn.v1.model.DomainBody;
import com.huaweicloud.sdk.cdn.v1.model.EnableDomainRequest;
import com.huaweicloud.sdk.cdn.v1.model.EnableDomainResponse;
import com.huaweicloud.sdk.cdn.v1.model.ListDomainsRequest;
import com.huaweicloud.sdk.cdn.v1.model.ListDomainsResponse;
import com.huaweicloud.sdk.cdn.v1.model.Sources;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainDetailByNameRequest;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainDetailByNameResponse;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import java.util.ArrayList;
```

### 4.3 初始化认证信息, 及客户端

示例相关参数说明如下所示：
- ak：华为云账号Access Key。
- sk：华为云账号Secret Access Key。
- iamEndpoint：iam身份认证域名。
- endpoint：接口调用域名。
- region: 服务所在区域。

``` java
public static void main(String[] args) {
    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    String iamEndpoint = "<iam endpoint>";
    String endpoint = "<endpoint>";
    String region = "<region>";
    
    ICredential auth = new GlobalCredentials().withIamEndpoint(iamEndpoint).withAk(ak).withSk(sk);
    Region regionObj = new Region(region, endpoint);
    CdnClient client = CdnClient.newBuilder().withCredential(auth).withRegion(regionObj).build();
    
    // 创建加速域名
    createDomainExample(client);
    
    // 删除加速域名
    deleteDomainExample(client);
    
    // 启用加速域名
    enableDomainExample(client);
    
    // 停用加速域名
    disableDomainExample(client);
    
    // 查询加速域名
    listDomainExample(client);
    
    // 查询加速域名详情
    ShowDomainDetailByNameExample(auth, regionObj);
}
```

## 4.4 创建加速域名

示例相关参数说明如下所示，详情[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/doc?version=v1&api=CreateDomain)：
- client：客户端。
- domainName：加速域名。
- sources：源站配置。
- businessType: 域名业务类型。
- serviceArea: 域名服务范围。

``` java
public static void createDomainExample(CdnClient client) {
    CreateDomainRequest request = new CreateDomainRequest();
    CreateDomainRequestBody body = new CreateDomainRequestBody();
    
    String domainName = "<domain_name>";
    Sources sources = new Sources();
    sources.setOriginType(Sources.OriginTypeEnum.DOMAIN);
    sources.setIpOrDomain("www.example.com");
    sources.setActiveStandby(1);
    ArrayList<Sources> sourceList = new ArrayList<>();
    sourceList.add(sources);
    DomainBody.BusinessTypeEnum businessType = DomainBody.BusinessTypeEnum.WEB;
    DomainBody.ServiceAreaEnum serviceArea = DomainBody.ServiceAreaEnum.MAINLAND_CHINA;
    
    DomainBody domainBody = new DomainBody();
    domainBody.setDomainName(domainName);
    domainBody.setBusinessType(businessType);
    domainBody.setSources(sourceList);
    domainBody.setServiceArea(serviceArea);
    
    body.setDomain(domainBody);
    request.withBody(body);
    
    try {
        CreateDomainResponse response = client.createDomain(request);
        int resultCode = response.getHttpStatusCode();
        if (resultCode == 200) {
            System.out.println("create domain success");
        }
    } catch (ConnectionException | RequestTimeoutException e) {
        System.out.println(e.getMessage());
    } catch (ServiceResponseException e) {
        printExceptionMsg(e);
    }
}
```

## 4.6 删除加速域名

示例相关参数说明如下所示，详情[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/doc?version=v1&api=DeleteDomain)：
- client：客户端。
- domainId：加速域名ID。

``` java
public static void deleteDomainExample(CdnClient client) {
    DeleteDomainRequest request = new DeleteDomainRequest();
    String domainId = "<domain_id>";
    request.setDomainId(domainId);
    try {
        DeleteDomainResponse response = client.deleteDomain(request);
        int resultCode = response.getHttpStatusCode();
        if (resultCode == 200) {
            System.out.println("delete domain success");
        }
    } catch (ConnectionException | RequestTimeoutException e) {
        System.out.println(e.getMessage());
    } catch (ServiceResponseException e) {
        printExceptionMsg(e);
    }
}
```

## 4.7 启用加速域名

示例相关参数说明如下所示，详情[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/doc?version=v1&api=EnableDomain)：
- client：客户端。
- domainId：加速域名ID。

``` java
private static void enableDomainExample(CdnClient client) {
    EnableDomainRequest request = new EnableDomainRequest();
    String domainId = "<domain_id>";
    request.setDomainId(domainId);
    
    try {
        EnableDomainResponse response = client.enableDomain(request);
        int resultCode = response.getHttpStatusCode();
        if (resultCode == 200) {
            System.out.println("enable domain success");
        }
    } catch (ConnectionException | RequestTimeoutException e) {
        System.out.println(e.getMessage());
    } catch (ServiceResponseException e) {
        printExceptionMsg(e);
    }
}
```

## 4.8 停用加速域名

示例相关参数说明如下所示，详情[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/doc?version=v1&api=DisableDomain)：
- client：客户端。
- domainId：加速域名ID。

``` java
private static void disableDomainExample(CdnClient client) {
    DisableDomainRequest request = new DisableDomainRequest();
    String domainId = "<domain_id>";
    request.setDomainId(domainId);
    
    try {
        DisableDomainResponse response = client.disableDomain(request);
        int resultCode = response.getHttpStatusCode();
        if (resultCode == 200) {
            System.out.println("disable domain success");
        }
    } catch (ConnectionException | RequestTimeoutException e) {
        System.out.println(e.getMessage());
    } catch (ServiceResponseException e) {
        printExceptionMsg(e);
    }
}
```

## 4.9 查询加速域名

示例相关参数说明如下所示，详情[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/doc?version=v1&api=ListDomains)：
- client：客户端。
- domainStatus：加速域名状态。

``` java
private static void listDomainExample(CdnClient client) {
    ListDomainsRequest request = new ListDomainsRequest();
    ListDomainsRequest.DomainStatusEnum domainStatus = ListDomainsRequest.DomainStatusEnum.ONLINE;
    
    request.setPageSize(2);
    request.setDomainStatus(domainStatus);
    try {
        ListDomainsResponse response = client.listDomains(request);
        int resultCode = response.getHttpStatusCode();
        if (resultCode == 200) {
            System.out.println("query domains success");
            System.out.println("total domains: " + response.getTotal());
        }
    } catch (ConnectionException | RequestTimeoutException e) {
        System.out.println(e.getMessage());
    } catch (ServiceResponseException e) {
        printExceptionMsg(e);
    }
}
```

## 4.10 查询加速域名详情

示例相关参数说明如下所示，详情[参考](https://console.huaweicloud.com/apiexplorer/#/openapi/CDN/doc?version=v2&api=ShowDomainDetailByName)：
- client：客户端。
- domainName：加速域名。

``` java
private static void ShowDomainDetailByNameExample(ICredential auth, Region region) {
    com.huaweicloud.sdk.cdn.v2.CdnClient client = com.huaweicloud.sdk.cdn.v2.CdnClient.newBuilder()
        .withCredential(auth).withRegion(region).build();
    
    ShowDomainDetailByNameRequest request = new ShowDomainDetailByNameRequest();
    String domainName = "<domain_name>";
    request.setDomainName(domainName);
    
    try {
        ShowDomainDetailByNameResponse response = client.showDomainDetailByName(request);
        int resultCode = response.getHttpStatusCode();
        if (resultCode == 200) {
            System.out.println("query domain detail success");
        }
    } catch (ConnectionException | RequestTimeoutException e) {
        System.out.println(e.getMessage());
    } catch (ServiceResponseException e) {
        printExceptionMsg(e);
    }
}
```

## 5. 参考

用户可以根据以上示例了解域名相关操作，更多信息请参考 [内容分发网络CDN文档](https://support.huaweicloud.com/cdn/index.html)

## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-11-24 | 1.0 | 文档首次发布 |