package com.huawei.cdn;

import com.huaweicloud.sdk.cdn.v1.CdnClient;
import com.huaweicloud.sdk.cdn.v1.model.CreateDomainRequest;
import com.huaweicloud.sdk.cdn.v1.model.CreateDomainRequestBody;
import com.huaweicloud.sdk.cdn.v1.model.CreateDomainResponse;
import com.huaweicloud.sdk.cdn.v1.model.DeleteDomainRequest;
import com.huaweicloud.sdk.cdn.v1.model.DeleteDomainResponse;
import com.huaweicloud.sdk.cdn.v1.model.DisableDomainRequest;
import com.huaweicloud.sdk.cdn.v1.model.DisableDomainResponse;
import com.huaweicloud.sdk.cdn.v1.model.DomainBody;
import com.huaweicloud.sdk.cdn.v1.model.EnableDomainRequest;
import com.huaweicloud.sdk.cdn.v1.model.EnableDomainResponse;
import com.huaweicloud.sdk.cdn.v1.model.ListDomainsRequest;
import com.huaweicloud.sdk.cdn.v1.model.ListDomainsResponse;
import com.huaweicloud.sdk.cdn.v1.model.Sources;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainDetailByNameRequest;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainDetailByNameResponse;

import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;

import java.util.ArrayList;

public class DomainOperations {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String iamEndpoint = "<iam endpoint>";
        String endpoint = "<endpoint>";
        String region = "<region>";
        
        ICredential auth = new GlobalCredentials().withIamEndpoint(iamEndpoint).withAk(ak).withSk(sk);
        Region regionObj = new Region(region, endpoint);
        CdnClient client = CdnClient.newBuilder().withCredential(auth).withRegion(regionObj).build();
        
        // 创建加速域名
        createDomainExample(client);
        
        // 删除加速域名
        deleteDomainExample(client);
        
        // 启用加速域名
        enableDomainExample(client);
        
        // 停用加速域名
        disableDomainExample(client);
        
        // 查询加速域名
        listDomainExample(client);
        
        // 查询加速域名详情
        ShowDomainDetailByNameExample(auth, regionObj);
    }
    
    public static void createDomainExample(CdnClient client) {
        CreateDomainRequest request = new CreateDomainRequest();
        CreateDomainRequestBody body = new CreateDomainRequestBody();
        
        String domainName = "<domain_name>";
        Sources sources = new Sources();
        sources.setOriginType(Sources.OriginTypeEnum.DOMAIN);
        sources.setIpOrDomain("www.example.com");
        sources.setActiveStandby(1);
        ArrayList<Sources> sourceList = new ArrayList<>();
        sourceList.add(sources);
        DomainBody.BusinessTypeEnum businessType = DomainBody.BusinessTypeEnum.WEB;
        DomainBody.ServiceAreaEnum serviceArea = DomainBody.ServiceAreaEnum.MAINLAND_CHINA;
        
        DomainBody domainBody = new DomainBody();
        domainBody.setDomainName(domainName);
        domainBody.setBusinessType(businessType);
        domainBody.setSources(sourceList);
        domainBody.setServiceArea(serviceArea);
        
        body.setDomain(domainBody);
        request.withBody(body);
        
        try {
            CreateDomainResponse response = client.createDomain(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode == 200) {
                System.out.println("create domain success");
            }
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            printExceptionMsg(e);
        }
    }
    
    public static void deleteDomainExample(CdnClient client) {
        DeleteDomainRequest request = new DeleteDomainRequest();
        String domainId = "<domain_id>";
        request.setDomainId(domainId);
        try {
            DeleteDomainResponse response = client.deleteDomain(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode == 200) {
                System.out.println("delete domain success");
            }
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            printExceptionMsg(e);
        }
    }
    
    private static void enableDomainExample(CdnClient client) {
        EnableDomainRequest request = new EnableDomainRequest();
        String domainId = "<domain_id>";
        request.setDomainId(domainId);
        
        try {
            EnableDomainResponse response = client.enableDomain(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode == 200) {
                System.out.println("enable domain success");
            }
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            printExceptionMsg(e);
        }
    }
    
    private static void disableDomainExample(CdnClient client) {
        DisableDomainRequest request = new DisableDomainRequest();
        String domainId = "<domain_id>";
        request.setDomainId(domainId);
        
        try {
            DisableDomainResponse response = client.disableDomain(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode == 200) {
                System.out.println("disable domain success");
            }
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            printExceptionMsg(e);
        }
    }
    
    private static void listDomainExample(CdnClient client) {
        ListDomainsRequest request = new ListDomainsRequest();
        ListDomainsRequest.DomainStatusEnum domainStatus = ListDomainsRequest.DomainStatusEnum.ONLINE;
        
        request.setPageSize(2);
        request.setDomainStatus(domainStatus);
        try {
            ListDomainsResponse response = client.listDomains(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode == 200) {
                System.out.println("query domains success");
                System.out.println("total domains: " + response.getTotal());
            }
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            printExceptionMsg(e);
        }
    }
    
    private static void ShowDomainDetailByNameExample(ICredential auth, Region region) {
        com.huaweicloud.sdk.cdn.v2.CdnClient client = com.huaweicloud.sdk.cdn.v2.CdnClient.newBuilder()
            .withCredential(auth).withRegion(region).build();
        
        ShowDomainDetailByNameRequest request = new ShowDomainDetailByNameRequest();
        String domainName = "<domain_name>";
        request.setDomainName(domainName);
        
        try {
            ShowDomainDetailByNameResponse response = client.showDomainDetailByName(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode == 200) {
                System.out.println("query domain detail success");
            }
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            printExceptionMsg(e);
        }
    }
    
    private static void printExceptionMsg(ServiceResponseException e) {
        System.out.println(e.getHttpStatusCode());
        System.out.println(e.getRequestId());
        System.out.println(e.getErrorCode());
        System.out.println(e.getErrorMsg());
    }
}
