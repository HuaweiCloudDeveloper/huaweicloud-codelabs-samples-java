/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.cloudpond.demo;

import com.huaweicloud.sdk.cloudpond.v1.CloudPondClient;
import com.huaweicloud.sdk.cloudpond.v1.model.Condition;
import com.huaweicloud.sdk.cloudpond.v1.model.CreateEdgeSite;
import com.huaweicloud.sdk.cloudpond.v1.model.CreateEdgeSiteRequest;
import com.huaweicloud.sdk.cloudpond.v1.model.CreateEdgeSiteRequestBody;
import com.huaweicloud.sdk.cloudpond.v1.model.CreateEdgeSiteResponse;
import com.huaweicloud.sdk.cloudpond.v1.model.CreateLocation;
import com.huaweicloud.sdk.cloudpond.v1.region.CloudPondRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CloudPondCreateEdgeSiteDemo {
    private static final Logger logger = LoggerFactory.getLogger(CloudPondCreateEdgeSiteDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        GlobalCredentials globalCredentials = new GlobalCredentials().withAk(ak).withSk(sk);

        CloudPondClient cloudPondClient = CloudPondClient.newBuilder().withCredential(globalCredentials)
            .withRegion(CloudPondRegion.valueOf("cn-north-4"))
            .build();

        // 创建边缘小站请求
        CreateEdgeSiteRequest request = new CreateEdgeSiteRequest().withBody(
            new CreateEdgeSiteRequestBody().withEdgeSite(new CreateEdgeSite().withName("{edge site name}")
                .withDescription("{edge site description}")
                .withRegionId("{region id}")
                .withLocation(new CreateLocation().withCountry(CreateLocation.CountryEnum.CN)
                    .withProvince("{province}")
                    .withCity("{city}")
                    .withDistrict("{district}")
                    .withDescription("{location description}")
                    .withCondition(new Condition().withEnvironment(0).withSpace(0).withTransport(0)))));
        try {
            CreateEdgeSiteResponse response = cloudPondClient.createEdgeSite(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()), e);
            logger.error(e.toString());
        }
    }
}
