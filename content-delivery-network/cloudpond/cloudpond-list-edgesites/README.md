## 版本说明
本示例配套的SDK版本为：v3.0

## 示例简介
华为云提供了智能边缘小站服务端SDK，您可以直接集成服务端SDK来调用智能边缘小站的相关API，从而实现对智能边缘小站服务的快速操作。

该示例展示了如何通过java版SDK查询边缘小站列表。

## 前置条件
### 获取AK/SK
开发者在使用前需先获取账号的ak、sk。

您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。
请在华为云控制台“我的凭证-访问密钥”页面上创建和查看您的 AK/SK。更多信息请查看访问密钥。

### 运行环境
Java JDK 1.8 及其以上版本

### SDK获取和安装
您可以通过Maven配置所依赖的智能边缘小站服务SDK

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) (产品类别：CDN与智能边缘)

```xml
<dependencies>
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-cloudpond</artifactId>
        <version>3.1.57</version>
    </dependency>
</dependencies>
```

### 接口及参数说明
[接口及参数说明](https://support.huaweicloud.com/api-ies/ListEdgeSites.html)

## 示例代码
```java
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.cloudpond.v1.CloudPondClient;
import com.huaweicloud.sdk.cloudpond.v1.model.ListEdgeSitesRequest;
import com.huaweicloud.sdk.cloudpond.v1.model.ListEdgeSitesResponse;
import com.huaweicloud.sdk.cloudpond.v1.region.CloudPondRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CloudPondListEdgeSitesDemo {
    private static final Logger logger = LoggerFactory.getLogger(CloudPondListEdgeSitesDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        GlobalCredentials globalCredentials = new GlobalCredentials().withAk(ak).withSk(sk);

        CloudPondClient cloudPondClient = CloudPondClient.newBuilder()
            .withCredential(globalCredentials)
            .withRegion(CloudPondRegion.valueOf("cn-north-4"))
            .build();

        // 查询边缘小站列表请求
        ListEdgeSitesRequest request = new ListEdgeSitesRequest().withLimit(10);
        try {
            ListEdgeSitesResponse response = cloudPondClient.listEdgeSites(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()), e);
            logger.error(e.toString());
        }
    }
}
```

## 参考
更多信息请参考：https://support.huaweicloud.com/productdesc-ies/ies_01_0100.html

## 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-09-18 | 1.0 | 文档首次发布|  