/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huaweicloud.cloudpond.demo;

import com.huaweicloud.sdk.cloudpond.v1.CloudPondClient;
import com.huaweicloud.sdk.cloudpond.v1.model.ListEdgeSitesRequest;
import com.huaweicloud.sdk.cloudpond.v1.model.ListEdgeSitesResponse;
import com.huaweicloud.sdk.cloudpond.v1.region.CloudPondRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CloudPondListEdgeSitesDemo {
    private static final Logger logger = LoggerFactory.getLogger(CloudPondListEdgeSitesDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        GlobalCredentials globalCredentials = new GlobalCredentials().withAk(ak).withSk(sk);

        CloudPondClient cloudPondClient = CloudPondClient.newBuilder().withCredential(globalCredentials)
                .withRegion(CloudPondRegion.valueOf("cn-north-4")).build();

        // 查询边缘小站列表请求
        ListEdgeSitesRequest request = new ListEdgeSitesRequest().withLimit(10);
        try {
            ListEdgeSitesResponse response = cloudPondClient.listEdgeSites(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()), e);
            logger.error(e.toString());
        }
    }
}