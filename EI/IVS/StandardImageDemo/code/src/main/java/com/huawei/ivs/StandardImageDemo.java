package com.huawei.ivs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.ivs.v2.IvsClient;
import com.huaweicloud.sdk.ivs.v2.model.DetectStandardByIdCardImageRequest;
import com.huaweicloud.sdk.ivs.v2.model.DetectStandardByIdCardImageResponse;
import com.huaweicloud.sdk.ivs.v2.model.IvsStandardByIdCardImageRequestBody;
import com.huaweicloud.sdk.ivs.v2.model.IvsStandardByIdCardImageRequestBodyData;
import com.huaweicloud.sdk.ivs.v2.model.Meta;
import com.huaweicloud.sdk.ivs.v2.model.ReqDataByIdCardImage;
import com.huaweicloud.sdk.ivs.v2.region.IvsRegion;

import org.apache.commons.codec.binary.Base64;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class StandardImageDemo {

    public static String getImgStr(String imgPath) {
        byte[] data = null;
        try {
            data = Files.readAllBytes(Paths.get(imgPath));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return Base64.encodeBase64String(data);
    }

    public static String getResourceImagePath(String imgPath) {
        try {
            URL url = StandardImageDemo.class.getClassLoader().getResource(imgPath);
            assert url != null;
            Path path = Paths.get(url.toURI());
            return path.toString();
        } catch (URISyntaxException e) {
            System.out.println(e.getMessage());
            return "";
        }
    }

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 将图片转为base64
        String faceImage = getImgStr(getResourceImagePath("data/face-demo.jpg"));
        String idcardImage1 = getImgStr(getResourceImagePath("data/idcard1-demo.jpg"));
        String idcardImage2 = getImgStr(getResourceImagePath("data/idcard2-demo.jpg"));

        // Init Auth Info
        ICredential credential = getCredential(ak, sk);

        // create IvsClient
        IvsClient client = getClient(IvsRegion.CN_NORTH_4, credential);

        // 三要素人脸图片认证
        detectStandardByIdCardImage(client, idcardImage1, idcardImage2, faceImage);

    }

    /**
     * 三要素人脸图片认证
     *
     * @param client 初始化的ivs客户端
     * @param idcardImage1 身份证人像面的base64编码
     * @param idcardImage2 身份证国徽面的base64编码
     * @param faceImage 认证人的正脸图片
     */
    private static void detectStandardByIdCardImage(IvsClient client, String idcardImage1, String idcardImage2,
        String faceImage) {
        DetectStandardByIdCardImageRequest request = new DetectStandardByIdCardImageRequest();
        IvsStandardByIdCardImageRequestBody body = new IvsStandardByIdCardImageRequestBody();

        Meta metaBody = new Meta();
        metaBody.setUuid("uuid");
        body.withMeta(metaBody);

        IvsStandardByIdCardImageRequestBodyData dataBody = new IvsStandardByIdCardImageRequestBodyData();
        List<ReqDataByIdCardImage> listDataReqData = new ArrayList<>();
        listDataReqData.add(
                new ReqDataByIdCardImage()
                        .withIdcardImage1(idcardImage1)
                        .withIdcardImage2(idcardImage2)
                        .withFaceImage(faceImage)
        );
        dataBody.withReqData(listDataReqData);
        body.withData(dataBody);

        request.withBody(body);
        try {
            DetectStandardByIdCardImageResponse response = client.detectStandardByIdCardImage(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    /**
     * 获取鉴权
     *
     * @param ak 开通人证核身服务账号的AK
     * @param sk 开通人证核身服务账号的SK
     * @return 认证信息
     */
    public static ICredential getCredential(String ak, String sk) {
        return new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
    }

    /**
     * 初始化客户端
     *
     * @param region 开通服务的RegionID
     * @param auth 认证信息
     * @return ivs客户端
     */
    public static IvsClient getClient(Region region, ICredential auth) {
        return IvsClient.newBuilder()
                .withCredential(auth)
                .withRegion(region)
                .build();
    }
}
