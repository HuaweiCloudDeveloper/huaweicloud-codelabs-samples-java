package com.huawei.ges;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.ges.v2.GesClient;
import com.huaweicloud.sdk.ges.v2.model.ListGraphs2Request;
import com.huaweicloud.sdk.ges.v2.model.ListGraphs2Response;
import com.huaweicloud.sdk.ges.v2.region.GesRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListGraphsDemo {
    private static final Logger logger = LoggerFactory.getLogger(ListGraphsDemo.class.getName());

    public static void main(String[] args) {
        // The AK and SK used for authentication are hard-coded or stored in plaintext, which has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, AK and SK are stored in environment variables for authentication. Before running this example, set environment variables CLOUD_SDK_AK and CLOUD_SDK_SK in the local environment
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        GesClient client = GesClient.newBuilder().withCredential(auth).withRegion(GesRegion.valueOf("ap-southeast-1")).build();

        ListGraphs2Request request = new ListGraphs2Request();

        try {
            ListGraphs2Response response = client.listGraphs2(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }
}