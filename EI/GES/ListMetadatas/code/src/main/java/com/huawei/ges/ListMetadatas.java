package com.huawei.ges;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.ges.v1.GesClient;
import com.huaweicloud.sdk.ges.v1.model.ListMetadatasRequest;
import com.huaweicloud.sdk.ges.v1.model.ListMetadatasResponse;
import com.huaweicloud.sdk.ges.v1.region.GesRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ListMetadatas {
    private static final Logger logger = LoggerFactory.getLogger(ListMetadatas.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to GesRegion.AP_SOUTHEAST_3.
        GesClient client = GesClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(GesRegion.AP_SOUTHEAST_3)
                .build();

        // Build Request
        ListMetadatasRequest request = new ListMetadatasRequest();

        try {
            // Send a request
            ListMetadatasResponse response = client.listMetadatas(request);
            // Print the response result.
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}