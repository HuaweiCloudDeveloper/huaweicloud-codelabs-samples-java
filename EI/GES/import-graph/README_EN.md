## Version Description
In this example, the SDK version is 3.1.75.

## Incrementally Importing Data to a Graph
This example shows how to import data to graphs incrementally using the Java SDK.

## Preparations
- HUAWEI CLOUD Java SDK supports Java JDK 1.8 and above.

- You must have a HUAWEI CLOUD account and the corresponding Access Key (AK) and Secret Access Key (SK). On the HUAWEI CLOUD console, choose My Credential > Access Keys to create and view your AK/SK. For more information, see [the access key](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0001.html).

## Obtain and Install the SDK
You can obtain and install the SDK through Maven. You only need to add the corresponding dependencies to the pom.xml file of the Java project. For the specific SDK version number, please see [the SDK Development Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter).

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-ges</artifactId>
    <version>3.1.75</version>
</dependency>
```
## Sample Code
```java
package com.huawei.ges;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.ges.v2.GesClient;
import com.huaweicloud.sdk.ges.v2.model.ImportGraphReq;
import com.huaweicloud.sdk.ges.v2.model.ImportGraph2Request;
import com.huaweicloud.sdk.ges.v2.model.ImportGraph2Response;
import com.huaweicloud.sdk.ges.v2.region.GesRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ImportGraphDemo {
    private static final Logger logger = LoggerFactory.getLogger(ImportGraphDemo.class.getName());

    public static void main(String[] args) {
        // The AK and SK used for authentication are hard-coded or stored in plaintext, which has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, AK and SK are stored in environment variables for authentication. Before running this example, set environment variables CLOUD_SDK_AK and CLOUD_SDK_SK in the local environment
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        GesClient client = GesClient.newBuilder().withCredential(auth).withRegion(GesRegion.valueOf("ap-southeast-1")).build();

        ImportGraphReq importGraphReq = new ImportGraphReq();
        importGraphReq.setSchemaPath("{schemaPath}");
        importGraphReq.setEdgesetPath("{edgesetPath}");
        importGraphReq.setVertexsetPath("{vertexsetPath}");

        ImportGraph2Request request = new ImportGraph2Request();
        request.setGraphId("{graph_id}");
        request.setBody(importGraphReq);

        try {
            ImportGraph2Response response = client.importGraph2(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }
}
```
## Change History
| Released On | Version | Description |
| :--------: | :------: | :----------: |
| 2023/12/29 | 1.0 | Initial release |    