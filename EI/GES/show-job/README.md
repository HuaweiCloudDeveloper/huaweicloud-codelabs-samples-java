## 版本说明
本示例配套的SDK版本为：3.1.75

## 增量导入图
该示例展示如何通过java版SDK查询Job的执行状态。对创建图、关闭图、启动图、删除图、导入图等异步API命令下发后，会返回jobId，通过jobId查询任务的执行状态

## 前置条件
- 华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。

- 您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/intl/zh-cn/usermanual-ca/ca_01_0001.html) 。

## SDK 获取和安装
您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。具体的SDK版本号请参见 [SDK开发中心](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter) 。

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-ges</artifactId>
    <version>3.1.75</version>
</dependency>
```
## 示例代码
```java
package com.huawei.ges;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.ges.v2.GesClient;
import com.huaweicloud.sdk.ges.v2.model.ShowJob2Request;
import com.huaweicloud.sdk.ges.v2.model.ShowJob2Response;
import com.huaweicloud.sdk.ges.v2.region.GesRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ShowJobDemo {
   private static final Logger logger = LoggerFactory.getLogger(ShowJobDemo.class.getName());

   public static void main(String[] args) {
      // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
      // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量CLOUD_SDK_AK和CLOUD_SDK_SK。
      String ak = System.getenv("CLOUD_SDK_AK");
      String sk = System.getenv("CLOUD_SDK_SK");
      ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
      GesClient client = GesClient.newBuilder().withCredential(auth).withRegion(GesRegion.valueOf("cn-north-4")).build();

      ShowJob2Request request = new ShowJob2Request();
      request.setGraphId("{graphId}");
      request.setJobId("{jobId}");

      try {
         ShowJob2Response response = client.showJob2(request);
         logger.info(response.toString());
      } catch (ClientRequestException e) {
         logger.error(String.valueOf(e.getHttpStatusCode()));
         logger.error(e.toString());
      } catch (ServerResponseException e) {
         logger.error(String.valueOf(e.getHttpStatusCode()));
         logger.error(e.toString());
      }
   }
}
```
## 修订记录
| 发布日期 | 文档版本 | 修订说明 |
| :--------: | :------: | :----------: |
| 2023/12/29 | 1.0 | 文档首次发布 |    