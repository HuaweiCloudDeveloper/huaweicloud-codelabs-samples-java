### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/GES/doc?version=v1&api=ListBackups).
2.View list of all backups (1.0. 0)

3.This API is used to view list of all backups.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The current account has the permission to use GES.

6.An instance has been created in GES.

7.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-dns. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-ges</artifactId>
    <version>3.1.124</version>
</dependency>
```

### Sample Code

``` java
package com.huawei.ges;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.ges.v1.GesClient;
import com.huaweicloud.sdk.ges.v1.model.ListBackupsRequest;
import com.huaweicloud.sdk.ges.v1.model.ListBackupsResponse;
import com.huaweicloud.sdk.ges.v1.region.GesRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ListBackups {
    private static final Logger logger = LoggerFactory.getLogger(ListBackups.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to GesRegion.AP_SOUTHEAST_3.
        GesClient client = GesClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(GesRegion.AP_SOUTHEAST_3)
                .build();

        // Build Request
        ListBackupsRequest request = new ListBackupsRequest();

        try {
            // Send a request
            ListBackupsResponse response = client.listBackups(request);
            // Print the response result.
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}
```

### Example of the returned result
```
{
    "backupCount": 3,
    "backupList": [
        {
            "id": "ada3e720-ab87-********************",
            "name": "ges060803_nodele********************",
            "backupMethod": "manual",
            "graphId": "4c5f882d-a813-4d********************",
            "graph_name": "ges06080********************",
            "graphStatus": "200",
            "graphSizeTypeIndex": "1",
            "dataStoreVersion": "2.2.21",
            "arch": "x86_64",
            "status": "success",
            "startTimestamp": 1623160513000,
            "startTime": "2021-06-08T13:55:13",
            "endTimestamp": 1623160568000,
            "endTime": "2021-06-08T13:56:08",
            "size": 1,
            "duration": 54,
            "encrypted": false
        },
        {
            "id": "7ed3f51d-816d-46********************",
            "name": "ges060803_nodel********************",
            "backupMethod": "auto",
            "graphId": "4c5f882d-a813-********************",
            "graph_name": "ges06080********************",
            "graphStatus": "200",
            "graphSizeTypeIndex": "1",
            "dataStoreVersion": "2.2.21",
            "arch": "x86_64",
            "status": "success",
            "startTimestamp": 1623242004000,
            "startTime": "2021-06-09T12:33:24",
            "endTimestamp": 1623242004000,
            "endTime": "2021-06-09T12:33:24",
            "size": 1,
            "duration": 0,
            "encrypted": false
        },
        {
            "id": "604bfb46-04dd-45fc********************",
            "name": "ges060802_nodelete********************",
            "backupMethod": "manual",
            "graphId": "9b9a05c2-0cdb-41ac********************",
            "graph_name": "ges060802********************",
            "graphStatus": "400",
            "graphSizeTypeIndex": "0",
            "dataStoreVersion": "2.2.23",
            "arch": "x86_64",
            "status": "success",
            "startTimestamp": 1623160524000,
            "startTime": "2021-06-08T13:55:24",
            "endTimestamp": 1623160577000,
            "endTime": "2021-06-08T13:56:17",
            "size": 1,
            "duration": 53,
            "encrypted": false
        }
    ]
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/09/26 | 1.0 | This document is released for the first time.