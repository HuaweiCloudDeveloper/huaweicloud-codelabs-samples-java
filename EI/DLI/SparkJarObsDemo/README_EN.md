## 1. Example Introduction

DLI is fully compatible with the open-source Apache Spark and allows users to develop application code to import, query,
and analyze job data. HUAWEI CLOUD Object Storage Service (OBS) is an object-based storage service that provides
customers with massive, secure, reliable, and cost-effective data storage capabilities.  
This example describes how to compile Spark code to read and query OBS data, compile and package a Spark JAR job, and
submit a Spark JAR job.

## 2. Preparations for Development

- You have registered with HUAWEI CLOUD and completed real-name authentication.
- You have been authorized to use OBS on the HUAWEI CLOUD console.
- The development environment is available and Java JDK 1.8 or later is supported.
- Create a DLI general queue. For details about how to create a queue, see [Creating a
  Queue](https://support.huaweicloud.com/intl/en-us/usermanual-dli/dli_01_0363.html)。
- Install Maven and configure the basic development environment. Used for project management throughout the software
  development lifecycle. The version number used in this example is 3.8.1.
- You have obtained the Access Key (AK) and Secret Access Key (SK) of the HUAWEI CLOUD account. For details,
  see [How Do I Obtain an AK/SK?](https://support.huaweicloud.com/intl/en-us/dli_faq/dli_03_0033.html) 。

## 3. Upload data to the OBS bucket.

3.1 Create the people.json file based on the following data:

```
{"name":"Michael"}
{"name":"Andy", "age":30}
{"name":"Justin", "age":19}
```

3.2 Log in to OBS Console. In Bucket List, click the name of the created OBS bucket. In this example, the bucket name is
dli-test-obs01. The Overview page is displayed.  
3.3 Click Object in the navigation tree on the left and choose Upload Object to upload the people.json file to the root
directory of the OBS bucket.  
3.4 In the root directory of the OBS bucket, click Create Folder to create a folder named result.  
3.5 Click the result folder and click New Folder under result to create a folder named parquet.

## 4. Create a Maven project and configure the POM dependency.

**The following is a demonstration of how to use the IntelliJ IDEA 2020.2 tool.**  
4.1 Open IntelliJ IDEA and choose File > New > Project.
![New project](assets/img_1.png)  
Select Maven for 4.2, select 1.8 for Project SDK, and click Next.    
![New project](assets/img_2.png)  
4.3 Define the sample project name and save path, and click Finish.  
![Create Project](assets/img_3.png)  
As shown in the preceding figure, the Maven project name is SparkJarObs, and the Maven project path is D:
\DLITest\SparkJarObs.  
4.4 Add the following configuration to the pom.xml file:

```xml

<dependencies>
    <dependency>
        <groupId>org.apache.spark</groupId>
        <artifactId>spark-sql_2.11</artifactId>
        <version>2.3.2</version>
    </dependency>
</dependencies>
```

![New project](https://support.huaweicloud.com/devg-dli/zh-cn_image_0000001252053711.png)  
The final pom file is as follows:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.huawei.dli</groupId>
    <artifactId>dli-spark-demo</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <licenses>
        <license>
            <name>Apache 2.0 License</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.html</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <properties>
        <scala.binary.version>2.11</scala.binary.version>
        <spark_2.11.version>2.3.2</spark_2.11.version>

        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.apache.spark</groupId>
            <artifactId>spark-sql_2.11</artifactId>
            <version>2.3.2</version>
        </dependency>
    </dependencies>


    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <version>3.1.1</version>
                <executions>
                    <execution>
                        <id>copy</id>
                        <phase>package</phase>
                        <goals>
                            <goal>copy-dependencies</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <includeScope>runtime</includeScope>
                </configuration>
            </plugin>
            <plugin>
                <groupId>net.alchim31.maven</groupId>
                <artifactId>scala-maven-plugin</artifactId>
                <version>3.2.2</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>compile</goal>
                            <goal>testCompile</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <scalaVersion>${scala.binary.version}</scalaVersion>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>

```

4.5 Right-click the src > main > java folder in the project path and choose New > Package from the shortcut menu to
create a package and class file.  
![img.png](assets/img_4.png)  
Define the package as required. In this example, define the package as com.huawei.dli.demo. Press Enter.
Create a Java Class file in the package path. In this example, the file is defined as SparkDemoObs.  
![img_1.png](assets/img_5.png)  

## 5. Code Example

The following code shows how to read and write OBS data:  
**Note: You need to modify the AK/SK and related paths.**

``` java

package com.huawei.dli.demo;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.col;

public class SparkDemoObs {
    public static void main(String[] args) {
        SparkSession spark = SparkSession
                .builder()
                .config("spark.hadoop.fs.obs.access.key", "xxx")
                .config("spark.hadoop.fs.obs.secret.key", "yyy")
                .appName("java_spark_demo")
                .getOrCreate();
        // can also be used --conf to set the ak sk when submit the app

        // test json data:
        // {"name":"Michael"}
        // {"name":"Andy", "age":30}
        // {"name":"Justin", "age":19}
        Dataset<Row> df = spark.read().json("obs://dli-test-obs01/people.json");
        df.printSchema();
        // root
        // |-- age: long (nullable = true)
        // |-- name: string (nullable = true)

        // Displays the content of the DataFrame to stdout
        df.show();
        // +----+-------+
        // | age|   name|
        // +----+-------+
        // |null|Michael|
        // |  30|   Andy|
        // |  19| Justin|
        // +----+-------+

        // Select only the "name" column
        df.select("name").show();
        // +-------+
        // |   name|
        // +-------+
        // |Michael|
        // |   Andy|
        // | Justin|
        // +-------+

        // Select people older than 21
        df.filter(col("age").gt(21)).show();
        // +---+----+
        // |age|name|
        // +---+----+
        // | 30|Andy|
        // +---+----+

        // Count people by age
        df.groupBy("age").count().show();
        // +----+-----+
        // | age|count|
        // +----+-----+
        // |  19|    1|
        // |null|    1|
        // |  30|    1|
        // +----+-----+

        // Register the DataFrame as a SQL temporary view
        df.createOrReplaceTempView("people");

        Dataset<Row> sqlDF = spark.sql("SELECT * FROM people");
        sqlDF.show();
        // +----+-------+
        // | age|   name|
        // +----+-------+
        // |null|Michael|
        // |  30|   Andy|
        // |  19| Justin|
        // +----+-------+

        sqlDF.write().mode(SaveMode.Overwrite).parquet("obs://dli-test-obs01/result/parquet");
        spark.read().parquet("obs://dli-test-obs01/result/parquet").show();

        spark.stop();
    }
}
```

## 6. Debug and compile code and export JAR packages.

6.1 Double-click Maven on the right of the IntelliJ IDEA tool. Double-click clean and compile to compile the code, as
shown in the following figure.  
After the compilation is successful, double-click package to package the code.  
![img_2.png](assets/img_6.png)  
6.2 After the package is successfully packed, the generated JAR package is stored in the target directory for future
use. In this example, the file name is SparkJarObs-1.0-SNAPSHOT.jar under D:\DLITest\SparkJarObs\target.  
![img_3.png](assets/img_7.png)  

## 7. Upload the JAR package to OBS.

7.1 Log in to the OBS console and upload the SparkJarObs-1.0-SNAPSHOT.jar JAR package to the OBS directory.  
**Note: Remember the OBS path name, which is required when you select an application in step 8.3.**  
7.2 Upload the JAR package to the program package management page of DLI for unified management.  
a. Log in to the DLI management console and choose Data Management > Package Management.  
b. On the Package Management page, click Create in the upper right corner to create a package.  
c. In the Create Package dialog box, configure the following parameters.

- Package Type: Select JAR.
- OBS path: OBS path where the program package is stored.
- Grouping settings and group names can be selected as required to facilitate subsequent package identification and
  management.

d. Click OK to complete the creation of the package.  
![Create Package](assets/img_8.png)  

## 8. Creating a Spark Jar Job

8.1 Log in to the DLI console and choose Job Management > Spark Jobs.  
8.2 On the Spark Jobs page, click Create Job.  
8.3 On the job creation page, configure job running parameters. The details are as follows:

- Queue: Select the created DLI general queue.
- Select a supported Spark version from the drop-down list. The latest version is recommended.
- Job name (--name): indicates the name of a customized Spark Jar job. Currently, the value is SparkTestObs.
- Application: Select the JAR package uploaded in step 7 to the OBS path, for example, SparkJarObs-1.0-SNAPSHOT.jar.
- Main class: The format is package name + class name. For example, the current value is
  com.huawei.dli.demo.SparkDemoObs.  
  **Note: Do not set this parameter to SparkDemoObs**  
  ![Create Spark Jar Job](assets/img_9.png)  

8.4 Click Execute to submit the Spark Jar job. The running status of the submitted jobs is displayed on the Spark Job
  Management page.  
  ![Job Running Status](assets/img_10.png)  

## 9. Checking the Job Running Result

The running status of the submitted jobs is displayed on the Spark Job Management page. The initial status is
Starting.  
If the job is successfully executed, the job status is Succeeded. Click Driver Log under More in the Operation column to
view the running logs of the job.  
![Diver Log](assets/img_11.png)  
The job execution log result is as follows:  
![img_4.png](assets/img_12.png)  

If the job is successfully executed, go to the result/parquet directory in the OBS bucket to view the generated parquet
file.  
![OBS bucket file](assets/img_13.png)  

## 10. Reference

For more information,
see [Spark Jar Development Guide](https://support.huaweicloud.com/intl/en-us/devg-dli/dli_09_0205.html)

## 11. Change History

|  Release Date  | Document Version |  Revision Description  |
|:--------------:|:----------------:|:----------------------:|
| 2023 - 08 - 25 |       1.0        | Document First Release |