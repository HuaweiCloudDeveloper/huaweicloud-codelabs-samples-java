## 1. 示例简介
DLI完全兼容开源的Apache Spark，支持用户开发应用程序代码来进行作业数据的导入、查询以及分析处理。华为云对象存储服务（Object Storage Service，OBS）是一个基于对象的存储服务，为客户提供海量、安全、高可靠、低成本的数据存储能力。  
本示例从编写Spark程序代码读取和查询OBS数据、 编译打包到提交Spark Jar作业等完整操作步骤来帮助您在DLI上进行开发。


## 2. 开发前准备

- 已注册华为云，并完成实名认证。
- 已在华为云控制台授权使用OBS服务。
- 具备开发环境 ，支持Java JDK 1.8及以上版本。
- 创建DLI通用队列，创建队列详细介绍请参考[创建队列](https://support.huaweicloud.com/usermanual-dli/dli_01_0363.html)。
- 安装maven，开发环境的基本配置。用于项目管理，贯穿软件开发生命周期。本示例使用的版本号为3.8.1。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请参见[如何获取AK/SK？](https://support.huaweicloud.com/dli_faq/dli_03_0033.html) 。


## 3. 上传数据到OBS桶
3.1 根据如下数据，创建people.json文件。
```
{"name":"Michael"}
{"name":"Andy", "age":30}
{"name":"Justin", "age":19}
```
3.2 进入OBS管理控制台，在“桶列表”下，单击已创建的OBS桶名称，本示例桶名为“dli-test-obs01”，进入“概览”页面。  
3.3 单击左侧列表中的“对象”，选择“上传对象”，将people.json文件上传到OBS桶根目录下。  
3.4 在OBS桶根目录下，单击“新建文件夹”，创建名为“result”的文件夹。  
3.5 单击“result”的文件夹，在“result”下单击“新建文件夹”，创建名为“parquet”的文件夹。


## 4. 新建Maven工程，配置pom依赖
**以下通过IntelliJ IDEA 2020.2工具操作演示。**  
4.1 打开IntelliJ IDEA，选择“File > New > Project”。
![新建project](https://support.huaweicloud.com/devg-dli/zh-cn_image_0000001252187705.png)
4.2 选择Maven，Project SDK选择1.8，单击“Next”。
![新建project](https://support.huaweicloud.com/devg-dli/zh-cn_image_0000001637557382.png)
4.3 定义样例工程名和配置样例工程存储路径，单击“Finish”完成工程创建。
![创建工程](https://support.huaweicloud.com/devg-dli/zh-cn_image_0000001637398494.png)
如上图所示，本示例创建Maven工程名为：SparkJarObs，Maven工程路径为：“D:\DLITest\SparkJarObs”。  
4.4 在pom.xml文件中添加如下配置。
```xml
<dependencies>
    <dependency>
        <groupId>org.apache.spark</groupId>
        <artifactId>spark-sql_2.11</artifactId>
        <version>2.3.2</version>
    </dependency>
</dependencies>
```
![新建project](https://support.huaweicloud.com/devg-dli/zh-cn_image_0000001252053711.png)
最终pom文件如下：
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.huawei.dli</groupId>
    <artifactId>dli-spark-demo</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <licenses>
        <license>
            <name>Apache 2.0 License</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.html</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <properties>
        <scala.binary.version>2.11</scala.binary.version>
        <spark_2.11.version>2.3.2</spark_2.11.version>

        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.apache.spark</groupId>
            <artifactId>spark-sql_2.11</artifactId>
            <version>2.3.2</version>
        </dependency>
    </dependencies>


    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <version>3.1.1</version>
                <executions>
                    <execution>
                        <id>copy</id>
                        <phase>package</phase>
                        <goals>
                            <goal>copy-dependencies</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <includeScope>runtime</includeScope>
                </configuration>
            </plugin>
            <plugin>
                <groupId>net.alchim31.maven</groupId>
                <artifactId>scala-maven-plugin</artifactId>
                <version>3.2.2</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>compile</goal>
                            <goal>testCompile</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <scalaVersion>${scala.binary.version}</scalaVersion>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>

```
4.5 在工程路径的“src > main > java”文件夹上鼠标右键，选择“New > Package”，新建Package和类文件。
![新建Package](https://support.huaweicloud.com/devg-dli/zh-cn_image_0000001637399398.png)
Package根据需要定义，本示例定义为：“com.huawei.dli.demo”，完成后回车。
在包路径下新建Java Class文件，本示例定义为：SparkDemoObs。
![新建Java Class文件](https://support.huaweicloud.com/devg-dli/zh-cn_image_0000001637559882.png)


## 5. 代码示例
以下代码展示如何读取和写入OBS数据：  
**注意：使用时请您自行修改ak/sk以及相关路径**
``` java

package com.huawei.dli.demo;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.col;

public class SparkDemoObs {
    public static void main(String[] args) {
        SparkSession spark = SparkSession
                .builder()
                .config("spark.hadoop.fs.obs.access.key", "xxx")
                .config("spark.hadoop.fs.obs.secret.key", "yyy")
                .appName("java_spark_demo")
                .getOrCreate();
        // can also be used --conf to set the ak sk when submit the app

        // test json data:
        // {"name":"Michael"}
        // {"name":"Andy", "age":30}
        // {"name":"Justin", "age":19}
        Dataset<Row> df = spark.read().json("obs://dli-test-obs01/people.json");
        df.printSchema();
        // root
        // |-- age: long (nullable = true)
        // |-- name: string (nullable = true)

        // Displays the content of the DataFrame to stdout
        df.show();
        // +----+-------+
        // | age|   name|
        // +----+-------+
        // |null|Michael|
        // |  30|   Andy|
        // |  19| Justin|
        // +----+-------+

        // Select only the "name" column
        df.select("name").show();
        // +-------+
        // |   name|
        // +-------+
        // |Michael|
        // |   Andy|
        // | Justin|
        // +-------+

        // Select people older than 21
        df.filter(col("age").gt(21)).show();
        // +---+----+
        // |age|name|
        // +---+----+
        // | 30|Andy|
        // +---+----+

        // Count people by age
        df.groupBy("age").count().show();
        // +----+-----+
        // | age|count|
        // +----+-----+
        // |  19|    1|
        // |null|    1|
        // |  30|    1|
        // +----+-----+

        // Register the DataFrame as a SQL temporary view
        df.createOrReplaceTempView("people");

        Dataset<Row> sqlDF = spark.sql("SELECT * FROM people");
        sqlDF.show();
        // +----+-------+
        // | age|   name|
        // +----+-------+
        // |null|Michael|
        // |  30|   Andy|
        // |  19| Justin|
        // +----+-------+

        sqlDF.write().mode(SaveMode.Overwrite).parquet("obs://dli-test-obs01/result/parquet");
        spark.read().parquet("obs://dli-test-obs01/result/parquet").show();

        spark.stop();
    }
}
```


## 6. 调试、编译代码并导出Jar包
6.1 双击IntelliJ IDEA工具右侧的“Maven”，参考下图分别双击“clean”、“compile”对代码进行编译。  
编译成功后，双击“package”对代码进行打包。
![编译打包](https://support.huaweicloud.com/devg-dli/zh-cn_image_0000001637454354.png)
6.2 打包成功后，生成的Jar包会放到target目录下，以备后用。本示例将会生成到：“D:\DLITest\SparkJarObs\target”下名为“SparkJarObs-1.0-SNAPSHOT.jar”。
![导出jar包](https://support.huaweicloud.com/devg-dli/zh-cn_image_0000001685853973.png)

## 7. 上传Jar包到OBS下
7.1 登录OBS控制台，将生成的“SparkJarObs-1.0-SNAPSHOT.jar”Jar包文件上传到OBS路径下。  
**注意：请您记住OBS路径名，步骤8.3选择应用程序时需要。**  
7.2 将Jar包文件上传到DLI的程序包管理中，方便后续统一管理。  
a. 登录DLI管理控制台，单击“数据管理 > 程序包管理”。  
b. 在“程序包管理”页面，单击右上角的“创建”创建程序包。  
c. 在“创建程序包”对话框，配置以下参数。  
- 包类型：选择“JAR”。  
- OBS路径：程序包所在的OBS路径。  
- 分组设置和组名称根据情况选择设置，方便后续识别和管理程序包。

d. 单击“确定”，完成创建程序包。
![创建程序包](https://support.huaweicloud.com/devg-dli/zh-cn_image_0000001633300054.png)


## 8. 创建Spark Jar作业
8.1 登录DLI控制台，单击“作业管理 > Spark作业”。  
8.2 在“Spark作业”管理界面，单击“创建作业”。  
8.3 在作业创建界面，配置对应作业运行参数。具体说明如下：   
- 所属队列：选择已创建的DLI通用队列。
- 在下拉列表中选择支持的Spark版本，推荐使用最新版本。
- 作业名称（--name）：自定义Spark Jar作业运行的名称。当前定义为：SparkTestObs。
- 应用程序：选择步骤7上传到OBS路径中的Jar包，例如当前选择为：“SparkJarObs-1.0-SNAPSHOT.jar”。
- 主类：格式为：程序包名+类名。例如当前为：com.huawei.dli.demo.SparkDemoObs。  
**注意：不要填成SparkDemoObs**
![创建Spark Jar作业](https://support.huaweicloud.com/devg-dli/zh-cn_image_0000001207665912.png)  

8.4 单击“执行”，提交该Spark Jar作业。在Spark作业管理界面显示已提交的作业运行状态。
![作业运行状态](https://support.huaweicloud.com/devg-dli/zh-cn_image_0000001638204318.png)


## 9. 查看作业运行结果
在Spark作业管理界面显示已提交的作业运行状态。初始状态显示为“启动中”。  
如果作业运行成功则作业状态显示为“已成功”，单击“操作”列“更多”下的“Driver日志”，显示当前作业运行的日志。
![diver日志](https://support.huaweicloud.com/devg-dli/zh-cn_image_0000001686404953.png)
作业执行日志结果如下：
![ “Driver日志”中的作业执行日志](https://support.huaweicloud.com/devg-dli/zh-cn_image_0000001251907299.png)

如果作业运行成功，本示例进入OBS桶下的“result/parquet”目录，可查看已生成预期的parquet文件。
![obs桶文件](https://support.huaweicloud.com/devg-dli/zh-cn_image_0000001638205778.png)


## 10. 参考
更多信息请参考：[spark Jar开发指南文档](https://support.huaweicloud.com/devg-dli/dli_09_0205.html)


## 11. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-08-25 | 1.0 | 文档首次发布 |