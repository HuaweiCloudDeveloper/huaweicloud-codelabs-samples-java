package com.huawei.dli.demo;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.col;

public class SparkDemoObs {
  public static void main(String[] args) {
    SparkSession spark = SparkSession
        .builder()
        .config("spark.hadoop.fs.obs.access.key", "xxxx")
        .config("spark.hadoop.fs.obs.secret.key", "xxxx")
        .appName("java_spark_demo")
        .getOrCreate();
    // can also be used --conf to set the ak sk when submit the app

    // test json data:
    // {"name":"Michael"}
    // {"name":"Andy", "age":30}
    // {"name":"Justin", "age":19}
    Dataset<Row> df = spark.read().json("obs://a-sdk-test-cn4/resources/people.json");
    df.printSchema();
    // root
    // |-- age: long (nullable = true)
    // |-- name: string (nullable = true)

    // Displays the content of the DataFrame to stdout
    df.show();
    // +----+-------+
    // | age|   name|
    // +----+-------+
    // |null|Michael|
    // |  30|   Andy|
    // |  19| Justin|
    // +----+-------+

    // Select only the "name" column
    df.select("name").show();
    // +-------+
    // |   name|
    // +-------+
    // |Michael|
    // |   Andy|
    // | Justin|
    // +-------+

    // Select people older than 21
    df.filter(col("age").gt(21)).show();
    // +---+----+
    // |age|name|
    // +---+----+
    // | 30|Andy|
    // +---+----+

    // Count people by age
    df.groupBy("age").count().show();
    // +----+-----+
    // | age|count|
    // +----+-----+
    // |  19|    1|
    // |null|    1|
    // |  30|    1|
    // +----+-----+

    // Register the DataFrame as a SQL temporary view
    df.createOrReplaceTempView("people");

    Dataset<Row> sqlDF = spark.sql("SELECT * FROM people");
    sqlDF.show();
    // +----+-------+
    // | age|   name|
    // +----+-------+
    // |null|Michael|
    // |  30|   Andy|
    // |  19| Justin|
    // +----+-------+

    sqlDF.write().mode(SaveMode.Overwrite).parquet("obs://a-sdk-test-cn4/result/parquet");
    spark.read().parquet("obs://a-sdk-test-cn4/result/parquet").show();

    spark.stop();
  }
}
