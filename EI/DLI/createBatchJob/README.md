## 示例简介
华为云提供了DLI服务端SDK，您可以直接集成服务端SDK来调用DLI服务的相关API，从而实现对DLI服务的快速操作。

数据湖探索（Data Lake Insight，简称DLI）是完全兼容Apache Spark、Apache Flink、Trino生态，提供一站式的流处理、批处理、交互式分析的Serverless融合处理分析服务。用户不需要管理任何服务器，即开即用。支持标准SQL/Spark SQL/Flink SQL，支持多种接入方式，并兼容主流数据格式。数据无需复杂的抽取、转换、加载，使用SQL或程序就可以对云上CloudTable、RDS、DWS、CSS、OBS、ECS自建数据库以及线下数据库的异构数据进行探索。

该示例展示了如何通过java版SDK以jar包的方式提交Spark作业，并查询作业详情和作业状态

## 前置条件
1. 使用DLI前需要先注册公有云帐户，再开通DLI。
2. 开通DLI通道 （详情参考：https://support.huaweicloud.com/dli/index.html ）
3. 您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
4. 获取 projectId，projectId 用于资源隔离。获取方式请参考 [获取项目ID](https://support.huaweicloud.com/api-dli/dli_02_0183.html)。
5. 获取华为云开发工具包（SDK）。华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。

### SDK获取和安装

您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。 具体的SDK版本号请参见 SDK开发中心 。

```xml
<dependency>
   <groupId>com.huaweicloud.sdk</groupId>
   <artifactId>huaweicloud-sdk-dli</artifactId>
   <version>3.1.63</version>
</dependency>
```

## 示例代码
1. 示例展示了如何使用DLI SDK以jar包的方式提交Spark作业，并查询作业详情和作业状态
```java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.dli.v1.DliClient;
import com.huaweicloud.sdk.dli.v1.model.BatchJobInfo;
import com.huaweicloud.sdk.dli.v1.model.CreateBatchJobRequest;
import com.huaweicloud.sdk.dli.v1.model.CreateBatchJobResponse;
import com.huaweicloud.sdk.dli.v1.model.ShowBatchInfoRequest;
import com.huaweicloud.sdk.dli.v1.model.ShowBatchInfoResponse;
import com.huaweicloud.sdk.dli.v1.model.ShowBatchStateRequest;
import com.huaweicloud.sdk.dli.v1.model.ShowBatchStateResponse;

import java.util.ArrayList;
import java.util.List;

public class CreateBatchJobExample {
    public static void main(String[] args) {
        // 基础认证信息：
        // ak: 华为云账号Access Key
        // sk: 华为云账号Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // projectId: 项目ID
        String projectId = "{******your project id******}";

        // 初始化sdk
        List<String> endpoints = new ArrayList<>();
        endpoints.add("dli.cn-north-4.myhuaweicloud.com");

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        // 创建DliClient实例
        DliClient client = DliClient.newBuilder()
            .withCredential(auth)
            .withEndpoints(endpoints)
            .build();

        // 创建请求
        CreateBatchJobRequest request = makeRequest();
        ShowBatchInfoRequest requestShowBatchInfo = new ShowBatchInfoRequest();
        ShowBatchStateRequest requestShowBatchState = new ShowBatchStateRequest();

        try {
            // 提交批处理作业
            CreateBatchJobResponse response = client.createBatchJob(request);
            System.out.println(response.toString());

            // 根据批处理作业的id查询作业详情
            requestShowBatchInfo.setBatchId(response.getId());
            ShowBatchInfoResponse responseShowBatchInfo = client.showBatchInfo(requestShowBatchInfo);
            System.out.println(responseShowBatchInfo.toString());

            // 根据批处理作业的id查询批处理作业的状态
            requestShowBatchState.setBatchId(response.getId());
            ShowBatchStateResponse responseShowBatchState = client.showBatchState(requestShowBatchState);
            System.out.println(responseShowBatchState.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    private static CreateBatchJobRequest makeRequest() {
        CreateBatchJobRequest request = new CreateBatchJobRequest();
        BatchJobInfo body = new BatchJobInfo();

        // 指定用户已上传到DLI资源管理系统的类型为jar或pyFile的程序包名。也支持指定OBS路径，例如：obs://桶名/包名。（必选）
        // 本案例的配套代码见第2节
        body.setFile("obs://your/file/address/demo.jar");

        // 批处理作业的Java/Spark主类的 reference name （必选）
        body.setClassName("WordCount");

        // 指定作业特性。表示用户作业使用的Spark镜像类型。（必选）
        // basic：表示使用DLI提供的基础Spark镜像。
        // custom：表示使用用户自定义的Spark镜像。
        // ai：表示使用DLI提供的AI镜像。
        body.setFeature(BatchJobInfo.FeatureEnum.BASIC);

        // 指定队列名字（可选）
        body.setQueue("your-queue-name");

        // 设置任务的别名。（可选）
        body.setName("your-task-name");

        // 设置Spark计算引擎的版本（可选）
        body.setSparkVersion("3.3.1");

        // 放入body体
        request.withBody(body);
        return request;
    }
}
```
2. 在本案例中运行的WordCount程序如下所示，您可根据需要将其编译打包后上传至OBS，然后在body体中设置jar包路径。
```java
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

import scala.Tuple2;

public class WordCount {
    public static void main(String[] args) {
        // 创建SparkConf对象并设置应用配置信息
        SparkConf conf = new SparkConf()
            .setMaster("local")
            .setAppName("Word Count");
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 读取字符串，并拆分为单词
        List<String> data = Arrays.asList("hadoop mapreduce",
            "Spark yarn hdfs hadoop mapreduce mapreduce yarn Doris mapreduce Doris mapreduce");
        JavaRDD<String> rdd = sc.parallelize(data);
        JavaRDD<String> rddWords = rdd.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public Iterator<String> call(String s) throws Exception {
                String[] split = s.split(" ");
                return Arrays.asList(split).iterator();
            }
        });

        // 将单词转换为键值对，以单词为键，1为值
        JavaPairRDD<String, Integer> pairs = rddWords.mapToPair(new PairFunction<String, String, Integer>() {
            @Override
            public Tuple2<String, Integer> call(String s) {
                return new Tuple2<>(s, 1);
            }
        });

        // 对键值对进行合并，将具有相同键的值相加
        List<Tuple2<String, Integer>> res = pairs.reduceByKey(new Function2<Integer, Integer, Integer>() {
            @Override
            public Integer call(Integer i1, Integer i2) {
                return i1 + i2;
            }
        }).collect();

        // 输出结果
        res.forEach(new Consumer<Tuple2<String, Integer>>() {
            @Override
            public void accept(Tuple2<String, Integer> stringIntegerTuple2) {
                System.out.println(stringIntegerTuple2.toString());
            }
        });

        sc.close();
    }
}
```


## 返回结果示例
创建批处理作业的响应示例：
```
{
  "id": "07a3e4e6-9a28-4e92-8d3f-9c538621a166",
  "appId": "",
  "name": "",
  "owner": "test1",
  "proxyUser": "",
  "state": "starting",
  "kind": "",
  "log": [],
  "sc_type": "CUSTOMIZED",
  "cluster_name": "aaa",
  "queue": "aaa",
  "image": "",
  "create_time": 1607589874156,
  "update_time": 1607589874156
}
```
查询批处理作业详情的响应示例：
```
{
    "id": "0a324461-d9d9-45da-a52a-3b3c7a3d809e",
    "appId": "",
    "name": "",
    "owner": "",
    "proxyUser": "",
    "state": "starting",
    "kind":"",
    "log": [
           "stdout: ",
            "stderr: ",
            "YARN Diagnostics: "
    ],
    "sc_type": "A",
    "cluster_name": "test",
    "queue": "test",
    "create_time": 1531906043036,
    "update_time": 1531906043036
}
```
查询批处理作业状态的响应示例：
```
{"id":"0a324461-d9d9-45da-a52a-3b3c7a3d809e","state":"Success"}
```

## 状态码

| 状态码 | 描述 |
|:---:|:--:|
| 200 |  提交成功。  |
| 400 | 请求错误。   |
| 500 |  内部服务器错误。  |


## 参考
更多信息请参考[DLI帮助文档](https://support.huaweicloud.com/api-dli/dli_02_0124.html)

## 修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: |:-----------:|
| 2023-10-29 |   1.0    | 文档首次发布 |