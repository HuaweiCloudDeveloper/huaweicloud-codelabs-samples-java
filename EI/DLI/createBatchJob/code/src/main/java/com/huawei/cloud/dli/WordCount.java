package com.huawei.cloud.dli;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

import scala.Tuple2;

public class WordCount {
    public static void main(String[] args) {
        // 创建SparkConf对象并设置应用配置信息
        SparkConf conf = new SparkConf()
            .setMaster("local")
            .setAppName("Word Count");
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 读取字符串，并拆分为单词
        List<String> data = Arrays.asList("hadoop mapreduce",
            "Spark yarn hdfs hadoop mapreduce mapreduce yarn Doris mapreduce Doris mapreduce");
        JavaRDD<String> rdd = sc.parallelize(data);
        JavaRDD<String> rddWords = rdd.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public Iterator<String> call(String s) throws Exception {
                String[] split = s.split(" ");
                return Arrays.asList(split).iterator();
            }
        });

        // 将单词转换为键值对，以单词为键，1为值
        JavaPairRDD<String, Integer> pairs = rddWords.mapToPair(new PairFunction<String, String, Integer>() {
            @Override
            public Tuple2<String, Integer> call(String s) {
                return new Tuple2<>(s, 1);
            }
        });

        // 对键值对进行合并，将具有相同键的值相加
        List<Tuple2<String, Integer>> res = pairs.reduceByKey(new Function2<Integer, Integer, Integer>() {
            @Override
            public Integer call(Integer i1, Integer i2) {
                return i1 + i2;
            }
        }).collect();

        // 输出结果
        res.forEach(new Consumer<Tuple2<String, Integer>>() {
            @Override
            public void accept(Tuple2<String, Integer> stringIntegerTuple2) {
                System.out.println(stringIntegerTuple2.toString());
            }
        });

        sc.close();
    }
}
