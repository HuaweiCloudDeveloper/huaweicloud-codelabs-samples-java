package com.huawei.cloud.dli;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.dli.v1.DliClient;
import com.huaweicloud.sdk.dli.v1.model.BatchJobInfo;
import com.huaweicloud.sdk.dli.v1.model.CreateBatchJobRequest;
import com.huaweicloud.sdk.dli.v1.model.CreateBatchJobResponse;
import com.huaweicloud.sdk.dli.v1.model.ShowBatchInfoRequest;
import com.huaweicloud.sdk.dli.v1.model.ShowBatchInfoResponse;
import com.huaweicloud.sdk.dli.v1.model.ShowBatchStateRequest;
import com.huaweicloud.sdk.dli.v1.model.ShowBatchStateResponse;

import java.util.ArrayList;
import java.util.List;

public class CreateBatchJobExample {
    public static void main(String[] args) {
        // 基础认证信息：
        // ak: 华为云账号Access Key
        // sk: 华为云账号Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // projectId: 项目ID
        String projectId = "{******your project id******}";

        // 初始化sdk
        List<String> endpoints = new ArrayList<>();
        endpoints.add("dli.cn-north-4.myhuaweicloud.com");

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        // 创建DliClient实例
        DliClient client = DliClient.newBuilder()
            .withCredential(auth)
            .withEndpoints(endpoints)
            .build();

        // 创建请求
        CreateBatchJobRequest request = makeRequest();
        ShowBatchInfoRequest requestShowBatchInfo = new ShowBatchInfoRequest();
        ShowBatchStateRequest requestShowBatchState = new ShowBatchStateRequest();

        try {
            // 提交批处理作业
            CreateBatchJobResponse response = client.createBatchJob(request);
            System.out.println(response.toString());

            // 根据批处理作业的id查询作业详情
            requestShowBatchInfo.setBatchId(response.getId());
            ShowBatchInfoResponse responseShowBatchInfo = client.showBatchInfo(requestShowBatchInfo);
            System.out.println(responseShowBatchInfo.toString());

            // 根据批处理作业的id查询批处理作业的状态
            requestShowBatchState.setBatchId(response.getId());
            ShowBatchStateResponse responseShowBatchState = client.showBatchState(requestShowBatchState);
            System.out.println(responseShowBatchState.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    private static CreateBatchJobRequest makeRequest() {
        CreateBatchJobRequest request = new CreateBatchJobRequest();
        BatchJobInfo body = new BatchJobInfo();

        // 指定用户已上传到DLI资源管理系统的类型为jar或pyFile的程序包名。也支持指定OBS路径，例如：obs://桶名/包名。（必选）
        body.setFile("obs://your/file/address/demo.jar");

        // 批处理作业的Java/Spark主类的 reference name （必选）
        body.setClassName("WordCount");

        // 指定作业特性。表示用户作业使用的Spark镜像类型。（必选）
        // basic：表示使用DLI提供的基础Spark镜像。
        // custom：表示使用用户自定义的Spark镜像。
        // ai：表示使用DLI提供的AI镜像。
        body.setFeature(BatchJobInfo.FeatureEnum.BASIC);

        // 指定队列名字（可选）
        body.setQueue("your-queue-name");

        // 设置任务的别名。（可选）
        body.setName("your-task-name");

        // 设置Spark计算引擎的版本（可选）
        body.setSparkVersion("3.3.1");

        // 放入body体
        request.withBody(body);
        return request;
    }
}