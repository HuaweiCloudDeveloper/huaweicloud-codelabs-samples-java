package com.huawei.cloud.dli;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.dli.v1.DliClient;
import com.huaweicloud.sdk.dli.v1.model.CreateDatabaseRequest;
import com.huaweicloud.sdk.dli.v1.model.CreateDatabaseRequestBody;
import com.huaweicloud.sdk.dli.v1.model.CreateDatabaseResponse;

import java.util.ArrayList;
import java.util.List;

public class CreateDatabases {
    public static void main(String[] args) {
        // 基础认证信息：
        // ak: 华为云账号Access Key
        // sk: 华为云账号Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // projectId: 项目ID
        String projectId = "{******your project id******}";

        // 1.初始化sdk
        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        // 2.创建DliClient实例
        List<String> endpoints = new ArrayList<>();
        endpoints.add("dli.cn-north-4.myhuaweicloud.com");
        DliClient client = DliClient.newBuilder()
            .withCredential(auth)
            .withEndpoints(endpoints)
            .build();

        // 3.创建请求，添加参数
        CreateDatabaseRequest request = new CreateDatabaseRequest();
        CreateDatabaseRequestBody body = new CreateDatabaseRequestBody();
        body.withDatabaseName("lxhtest1020");
        request.withBody(body);

        // 4.提交创建请求
        try {
            CreateDatabaseResponse response = client.createDatabase(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        }
    }
}