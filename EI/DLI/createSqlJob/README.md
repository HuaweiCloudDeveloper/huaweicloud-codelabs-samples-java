## 示例简介
华为云提供了DLI服务端SDK，您可以直接集成服务端SDK来调用DLI服务的相关API，从而实现对DLI服务的快速操作。

数据湖探索（Data Lake Insight，简称DLI）是完全兼容Apache Spark、Apache Flink、Trino生态，提供一站式的流处理、批处理、交互式分析的Serverless融合处理分析服务。用户不需要管理任何服务器，即开即用。支持标准SQL/Spark SQL/Flink SQL，支持多种接入方式，并兼容主流数据格式。数据无需复杂的抽取、转换、加载，使用SQL或程序就可以对云上CloudTable、RDS、DWS、CSS、OBS、ECS自建数据库以及线下数据库的异构数据进行探索。

该示例展示了如何通过java版SDK提交SQL作业。作业包含以下类型：DDL、DCL、IMPORT、QUERY和INSERT。其中，IMPORT与导入数据的功能一致，区别仅在于实现方式不同。

## 前置条件
1. 使用DLI前需要先注册公有云帐户，再开通DLI。
2. 开通DLI通道 （详情参考：https://support.huaweicloud.com/dli/index.html ）
3. 您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
4. 获取 projectId，projectId 用于资源隔离。获取方式请参考 [获取项目ID](https://support.huaweicloud.com/api-dli/dli_02_0183.html)。
5. 获取华为云开发工具包（SDK）。华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。

### SDK获取和安装

您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。 具体的SDK版本号请参见 SDK开发中心 。

```xml
<dependency>
   <groupId>com.huaweicloud.sdk</groupId>
   <artifactId>huaweicloud-sdk-dli</artifactId>
   <version>3.1.63</version>
</dependency>
```

## 示例代码
提交SQL作业
```java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.dli.v1.DliClient;
import com.huaweicloud.sdk.dli.v1.model.CreateSqlJobRequest;
import com.huaweicloud.sdk.dli.v1.model.CreateSqlJobRequestBody;
import com.huaweicloud.sdk.dli.v1.model.CreateSqlJobResponse;
import com.huaweicloud.sdk.dli.v1.model.TmsTagEntity;

import java.util.ArrayList;
import java.util.List;

public class CreateSqlJobExample {
    public static void main(String[] args) {
        // 基础认证信息：
        // ak: 华为云账号Access Key
        // sk: 华为云账号Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // projectId: 项目ID
        String projectId = "{******your project id******}";

        // 1.初始化sdk
        List<String> endpoints = new ArrayList<>();
        endpoints.add("dli.cn-north-4.myhuaweicloud.com");

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        // 2.创建DliClient实例
        DliClient client = DliClient.newBuilder()
            .withCredential(auth)
            .withEndpoints(endpoints)
            .build();

        // 3.创建请求，添加参数
        CreateSqlJobRequest request = makeRequest();

        // 4.提交作业
        try {
            CreateSqlJobResponse response = client.createSqlJob(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    private static CreateSqlJobRequest makeRequest() {
        CreateSqlJobRequest request = new CreateSqlJobRequest();
        CreateSqlJobRequestBody body = new CreateSqlJobRequestBody();

        // 3.1 设置SQL语句
        body.setSql("desc table1");

        // 3.2 设置数据库（可选）
        body.setCurrentdb("db1");

        // 3.3 设置队列（可选）
        body.setQueueName("default");

        // 3.4 设置自定义conf （可选）
        ArrayList<String> conf = new ArrayList<>();
        conf.add("dli.sql.shuffle.partitions = 200");
        body.setConf(conf);

        // 3.5 设置tags
        ArrayList<TmsTagEntity> tags = new ArrayList<>();
        TmsTagEntity tag1 = new TmsTagEntity();
        tag1.setKey("workspace");
        tag1.setValue("space1");
        TmsTagEntity tag2 = new TmsTagEntity();
        tag2.setKey("jobName");
        tag2.setValue("name1");
        tags.add(tag1);
        tags.add(tag2);
        body.setTags(tags);

        // 3.6 放入body体
        request.withBody(body);
        return request;
    }
}
```

## 返回结果示例
```
{
  "is_success": true,
  "message": "",
  "job_id": "8ecb0777-9c70-4529-9935-29ea0946039c",
  "job_type": "DDL",
  "job_mode":"sync",
  "schema": [
    {
      "col_name": "string"
    },
    {
      "data_type": "string"
    },
    {
      "comment": "string"
    }
  ],
  "rows": [
    [
      "c1",
      "int",
      null
    ],
    [
      "c2",
      "string",
      null
    ]
  ]
}
```

## 状态码

| 状态码 | 描述 |
|:---:|:--:|
| 200 |  提交成功。  |
| 400 | 请求错误。   |
| 500 |  内部服务器错误。  |


## 参考
更多信息请参考[DLI帮助文档](https://support.huaweicloud.com/api-dli/dli_02_0102.html)

## 修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: |:-----------:|
| 2023-10-27 |   1.0    | 文档首次发布 |