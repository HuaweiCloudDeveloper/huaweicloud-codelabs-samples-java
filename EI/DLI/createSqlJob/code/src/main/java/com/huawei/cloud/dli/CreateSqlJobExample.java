package com.huawei.cloud.dli;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.dli.v1.DliClient;
import com.huaweicloud.sdk.dli.v1.model.CreateSqlJobRequest;
import com.huaweicloud.sdk.dli.v1.model.CreateSqlJobRequestBody;
import com.huaweicloud.sdk.dli.v1.model.CreateSqlJobResponse;
import com.huaweicloud.sdk.dli.v1.model.TmsTagEntity;

import java.util.ArrayList;
import java.util.List;

public class CreateSqlJobExample {
    public static void main(String[] args) {
        // 基础认证信息：
        // ak: 华为云账号Access Key
        // sk: 华为云账号Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // projectId: 项目ID
        String projectId = "{******your project id******}";

        // 1.初始化sdk
        List<String> endpoints = new ArrayList<>();
        endpoints.add("dli.cn-north-4.myhuaweicloud.com");

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        // 2.创建DliClient实例
        DliClient client = DliClient.newBuilder()
            .withCredential(auth)
            .withEndpoints(endpoints)
            .build();

        // 3.创建请求，添加参数
        CreateSqlJobRequest request = makeRequest();

        // 4.提交作业
        try {
            CreateSqlJobResponse response = client.createSqlJob(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    private static CreateSqlJobRequest makeRequest() {
        CreateSqlJobRequest request = new CreateSqlJobRequest();
        CreateSqlJobRequestBody body = new CreateSqlJobRequestBody();

        // 3.1 设置SQL语句
        body.setSql("desc table1");

        // 3.2 设置数据库（可选）
        body.setCurrentdb("db1");

        // 3.3 设置队列（可选）
        body.setQueueName("default");

        // 3.4 设置自定义conf （可选）
        ArrayList<String> conf = new ArrayList<>();
        conf.add("dli.sql.shuffle.partitions = 200");
        body.setConf(conf);

        // 3.5 设置tags
        ArrayList<TmsTagEntity> tags = new ArrayList<>();
        TmsTagEntity tag1 = new TmsTagEntity();
        tag1.setKey("workspace");
        tag1.setValue("space1");
        TmsTagEntity tag2 = new TmsTagEntity();
        tag2.setKey("jobName");
        tag2.setValue("name1");
        tags.add(tag1);
        tags.add(tag2);
        body.setTags(tags);

        // 3.6 放入body体
        request.withBody(body);
        return request;
    }
}