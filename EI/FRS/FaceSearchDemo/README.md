# 人脸识别服务人脸搜索示例（Java版本）

## 0.版本说明
本示例基于华为云SDK V3.0版本开发

## 1.简介
华为云提供了人脸识别服务端SDK，您可以直接集成服务端SDK来调用人脸识别服务的相关API，从而实现对人脸识别服务的快速操作。

该示例展示了如何通过Java版SDK实现人脸搜索。

## 2.开发前准备
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已订阅人脸识别服务。
- 已具备开发环境，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 已获取直播服务对应区域的项目ID，请在华为云控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见 [API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html) 。

## 3.安装SDK
您可以通过Maven配置所依赖的人脸识别服务SDK
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-frs</artifactId>
    <version>3.1.69</version>
</dependency>
```

## 4. 开始使用
### 4.1 导入依赖模块
```java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;

//v2版本sdk
import com.huaweicloud.sdk.frs.v2.FrsClient;
import com.huaweicloud.sdk.frs.v2.model.AddFacesBase64Req;
import com.huaweicloud.sdk.frs.v2.model.AddFacesByBase64Request;
import com.huaweicloud.sdk.frs.v2.model.AddFacesByBase64Response;
import com.huaweicloud.sdk.frs.v2.model.CreateFaceSetReq;
import com.huaweicloud.sdk.frs.v2.model.CreateFaceSetRequest;
import com.huaweicloud.sdk.frs.v2.model.CreateFaceSetResponse;
import com.huaweicloud.sdk.frs.v2.model.DeleteFaceSetRequest;
import com.huaweicloud.sdk.frs.v2.model.DeleteFaceSetResponse;
import com.huaweicloud.sdk.frs.v2.model.FaceSearchBase64Req;
import com.huaweicloud.sdk.frs.v2.model.FaceSetFace;
import com.huaweicloud.sdk.frs.v2.model.SearchFaceByBase64Request;
import com.huaweicloud.sdk.frs.v2.model.SearchFaceByBase64Response;
import com.huaweicloud.sdk.frs.v2.model.TypeInfo;
import com.huaweicloud.sdk.frs.v2.model.UpdateFaceReq;
import com.huaweicloud.sdk.frs.v2.model.UpdateFaceRequest;
import com.huaweicloud.sdk.frs.v2.model.UpdateFaceResponse;
import com.huaweicloud.sdk.frs.v2.region.FrsRegion;
```
### 4.2 初始化认证信息
``` java
public static ICredential getCredential(String ak, String sk) {
    return new BasicCredentials()
            .withAk(ak)
            .withSk(sk);
}
```
相关参数说明如下所示：
- ak：华为云账号Access Key。
- sk：华为云账号Secret Access Key 。
### 4.3 初始化人脸识别服务的客户端
``` java
public static FrsClient getClient(Region region, ICredential auth) {
    // 初始化人脸识别服务的客户端
    return FrsClient.newBuilder()
            .withCredential(auth)
            .withRegion(region) // 选择服务所在区域 FrsRegion.CN_NORTH_4
            .build();
}
```
相关参数说明如下所示：

service region: 服务所在区域，例如：
- CN_NORTH_1 北京一
- CN_NORTH_4 北京四
## 5. SDK demo代码解析
### 5.1 人脸搜索
``` java
SearchFaceByBase64Request searchRequest = new SearchFaceByBase64Request();
searchRequest.withFaceSetName(faceSetName);
FaceSearchBase64Req faceSearchBase64Req = new FaceSearchBase64Req();
List<Map<String, String>> listBodySort = new ArrayList<>();
Map<String, String> map = new HashMap<>();
map.put("timestamp", "asc");
listBodySort.add(map);
List<String> listBodyReturnFields = new ArrayList<>();
listBodyReturnFields.add("timestamp");
faceSearchBase64Req.withSort(listBodySort);
faceSearchBase64Req.withReturnFields(listBodyReturnFields);
faceSearchBase64Req.withImageBase64(imageBase64);
searchRequest.withBody(faceSearchBase64Req);
try {
    SearchFaceByBase64Response searchResponse = client.searchFaceByBase64(searchRequest);
    System.out.println(searchResponse.toString());
} catch (ConnectionException | RequestTimeoutException e) {
    System.out.println(e.getMessage());
} catch (ServiceResponseException e) {
    System.out.println(e.getMessage());
    System.out.println(e.getHttpStatusCode());
    System.out.println(e.getRequestId());
    System.out.println(e.getErrorCode());
    System.out.println(e.getErrorMsg());
}
```
### 5.2 创建人脸库
``` java
CreateFaceSetRequest createFaceSetRequest = new CreateFaceSetRequest();
CreateFaceSetReq createFaceSetReq = new CreateFaceSetReq();
createFaceSetReq.withFaceSetName(faceSetName);
Map<String, TypeInfo> stringTypeInfoMap = new HashMap<>();
TypeInfo typeInfo = new TypeInfo();
typeInfo.withType("long");
stringTypeInfoMap.put("timestamp", typeInfo);
createFaceSetReq.withExternalFields(stringTypeInfoMap);
createFaceSetRequest.withBody(createFaceSetReq);
try {
    CreateFaceSetResponse createFaceSetResponse = client.createFaceSet(createFaceSetRequest);
    System.out.println(createFaceSetResponse.toString());
} catch (ConnectionException | RequestTimeoutException e) {
    System.out.println(e.getMessage());
} catch (ServiceResponseException e) {
    System.out.println(e.getMessage());
    System.out.println(e.getHttpStatusCode());
    System.out.println(e.getRequestId());
    System.out.println(e.getErrorCode());
    System.out.println(e.getErrorMsg());
}
```
### 5.3 删除人脸库
``` java
DeleteFaceSetRequest deleteFaceSetRequest = new DeleteFaceSetRequest();
deleteFaceSetRequest.withFaceSetName(faceSetName);
try {
    DeleteFaceSetResponse deleteFaceSetResponse = client.deleteFaceSet(deleteFaceSetRequest);
    System.out.println(deleteFaceSetResponse.toString());
} catch (ConnectionException | RequestTimeoutException e) {
    System.out.println(e.getMessage());
} catch (ServiceResponseException e) {
    System.out.println(e.getMessage());
    System.out.println(e.getHttpStatusCode());
    System.out.println(e.getRequestId());
    System.out.println(e.getErrorCode());
    System.out.println(e.getErrorMsg());
}
```
### 5.4 添加人脸
``` java
AddFacesByBase64Request addFacesByBase64Request = new AddFacesByBase64Request();
addFacesByBase64Request.withFaceSetName(faceSetName);
AddFacesBase64Req addFacesBase64Req = new AddFacesBase64Req();
HashMap<String, Long> externalFields = new HashMap<>();
externalFields.put("timestamp", System.currentTimeMillis());
addFacesBase64Req.withExternalFields(externalFields);
addFacesBase64Req.withImageBase64(imageBase64);
addFacesByBase64Request.withBody(addFacesBase64Req);
try {
    AddFacesByBase64Response addFacesByBase64Response = client.addFacesByBase64(addFacesByBase64Request);
    System.out.println(addFacesByBase64Response.toString());
    return addFacesByBase64Response.getFaces();
} catch (ConnectionException | RequestTimeoutException e) {
    System.out.println(e.getMessage());
} catch (ServiceResponseException e) {
    System.out.println(e.getMessage());
    System.out.println(e.getHttpStatusCode());
    System.out.println(e.getRequestId());
    System.out.println(e.getErrorCode());
    System.out.println(e.getErrorMsg());
}
return new ArrayList<>();
```
### 5.5 更新人脸
``` java
UpdateFaceRequest updateFaceRequest = new UpdateFaceRequest();
updateFaceRequest.withFaceSetName(faceSetName);
UpdateFaceReq updateFaceReq = new UpdateFaceReq();
updateFaceReq.withFaceId(faceId).withExternalImageId(externalImageId);
updateFaceRequest.withBody(updateFaceReq);
try {
    UpdateFaceResponse response = client.updateFace(updateFaceRequest);
    System.out.println(response.toString());
} catch (ConnectionException | RequestTimeoutException e) {
    System.out.println(e.getMessage());
} catch (ServiceResponseException e) {
    System.out.println(e.getMessage());
    System.out.println(e.getHttpStatusCode());
    System.out.println(e.getRequestId());
    System.out.println(e.getErrorCode());
    System.out.println(e.getErrorMsg());
}
```
## 6.参考
更多信息请参考[人脸识别服务](https://support.huaweicloud.com/face/index.html)

## 7.修订记录
|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2023-03-08 |   1.0    | 文档首次发布 |