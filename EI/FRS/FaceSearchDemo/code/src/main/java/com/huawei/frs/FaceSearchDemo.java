package com.huawei.frs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.frs.v2.FrsClient;
import com.huaweicloud.sdk.frs.v2.model.AddFacesBase64Req;
import com.huaweicloud.sdk.frs.v2.model.AddFacesByBase64Request;
import com.huaweicloud.sdk.frs.v2.model.AddFacesByBase64Response;
import com.huaweicloud.sdk.frs.v2.model.CreateFaceSetReq;
import com.huaweicloud.sdk.frs.v2.model.CreateFaceSetRequest;
import com.huaweicloud.sdk.frs.v2.model.CreateFaceSetResponse;
import com.huaweicloud.sdk.frs.v2.model.DeleteFaceSetRequest;
import com.huaweicloud.sdk.frs.v2.model.DeleteFaceSetResponse;
import com.huaweicloud.sdk.frs.v2.model.FaceSearchBase64Req;
import com.huaweicloud.sdk.frs.v2.model.FaceSetFace;
import com.huaweicloud.sdk.frs.v2.model.SearchFaceByBase64Request;
import com.huaweicloud.sdk.frs.v2.model.SearchFaceByBase64Response;
import com.huaweicloud.sdk.frs.v2.model.TypeInfo;
import com.huaweicloud.sdk.frs.v2.model.UpdateFaceReq;
import com.huaweicloud.sdk.frs.v2.model.UpdateFaceRequest;
import com.huaweicloud.sdk.frs.v2.model.UpdateFaceResponse;
import com.huaweicloud.sdk.frs.v2.region.FrsRegion;

import org.apache.commons.codec.binary.Base64;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FaceSearchDemo {

    public static String getImgStr(String imgPath) {
        byte[] data = null;
        try {
            data = Files.readAllBytes(Paths.get(imgPath));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return Base64.encodeBase64String(data);
    }

    public static String getResourceImagePath(String imgPath) {
        try {
            URL url = FaceSearchDemo.class.getClassLoader().getResource(imgPath);
            assert url != null;
            Path path = Paths.get(url.toURI());
            return path.toString();
        } catch (URISyntaxException e) {
            System.out.println(e.getMessage());
            return "";
        }
    }

    public static void main(String[] args) throws InterruptedException {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 将图片转为base64
        String imageBase64 = getImgStr(getResourceImagePath("data/face-demo.jpg"));

        // Init Auth Info
        ICredential credential = getCredential(ak, sk);

        // create frsClient
        FrsClient client = getClient(FrsRegion.CN_NORTH_4, credential);

        // 创建人脸集
        String faceSetName = "face_set_name";
        createFaceSet(client, faceSetName);

        // 添加人脸Base64方式
        List<FaceSetFace> faceSetFaces = addFacesByBase64(client, faceSetName, imageBase64);
        assert faceSetFaces != null;
        FaceSetFace faceSetFace = faceSetFaces.get(0);

        // 获取face_id 和 external_image_id
        String faceId = faceSetFace.getFaceId();
        String externalImageId = faceSetFace.getExternalImageId();

        Thread.sleep(1000);

        // 人脸搜索Base64方式
        searchFaceByBase64(client, faceSetName, imageBase64);

        // 根据人脸ID更新人脸
        updateFace(client, faceSetName, faceId, externalImageId + "_new");

        // 删除人脸集
        deleteFaceSet(client, faceSetName);
    }

    /**
     * 获取鉴权
     *
     * @param ak 开通人脸识别服务账号的AK
     * @param sk 开通人脸识别服务账号的SK
     * @return 认证信息
     */
    public static ICredential getCredential(String ak, String sk) {
        return new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
    }

    /**
     * 初始化客户端
     *
     * @param region 开通服务的RegionID
     * @param auth 认证信息
     * @return frs客户端
     */
    public static FrsClient getClient(Region region, ICredential auth) {
        return FrsClient.newBuilder()
                .withCredential(auth)
                .withRegion(region)
                .build();
    }

    private static void searchFaceByBase64(FrsClient client, String faceSetName, String imageBase64) {
        SearchFaceByBase64Request searchRequest = new SearchFaceByBase64Request();
        searchRequest.withFaceSetName(faceSetName);
        FaceSearchBase64Req faceSearchBase64Req = new FaceSearchBase64Req();
        List<Map<String, String>> listBodySort = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        map.put("timestamp", "asc");
        listBodySort.add(map);
        List<String> listBodyReturnFields = new ArrayList<>();
        listBodyReturnFields.add("timestamp");
        faceSearchBase64Req.withSort(listBodySort);
        faceSearchBase64Req.withReturnFields(listBodyReturnFields);
        faceSearchBase64Req.withImageBase64(imageBase64);
        searchRequest.withBody(faceSearchBase64Req);
        try {
            SearchFaceByBase64Response searchResponse = client.searchFaceByBase64(searchRequest);
            System.out.println(searchResponse.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            handleServiceResponseException(e);
        }
    }

    private static void createFaceSet(FrsClient client, String faceSetName) {
        CreateFaceSetRequest createFaceSetRequest = new CreateFaceSetRequest();
        CreateFaceSetReq createFaceSetReq = new CreateFaceSetReq();
        createFaceSetReq.withFaceSetName(faceSetName);
        Map<String, TypeInfo> stringTypeInfoMap = new HashMap<>();
        TypeInfo typeInfo = new TypeInfo();
        typeInfo.withType("long");
        stringTypeInfoMap.put("timestamp", typeInfo);
        createFaceSetReq.withExternalFields(stringTypeInfoMap);
        createFaceSetRequest.withBody(createFaceSetReq);
        try {
            CreateFaceSetResponse createFaceSetResponse = client.createFaceSet(createFaceSetRequest);
            System.out.println(createFaceSetResponse.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            handleServiceResponseException(e);
        }
    }

    private static void deleteFaceSet(FrsClient client, String faceSetName) {
        DeleteFaceSetRequest deleteFaceSetRequest = new DeleteFaceSetRequest();
        deleteFaceSetRequest.withFaceSetName(faceSetName);
        try {
            DeleteFaceSetResponse deleteFaceSetResponse = client.deleteFaceSet(deleteFaceSetRequest);
            System.out.println(deleteFaceSetResponse.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            handleServiceResponseException(e);
        }
    }

    private static List<FaceSetFace> addFacesByBase64(FrsClient client, String faceSetName, String imageBase64) {
        AddFacesByBase64Request addFacesByBase64Request = new AddFacesByBase64Request();
        addFacesByBase64Request.withFaceSetName(faceSetName);
        AddFacesBase64Req addFacesBase64Req = new AddFacesBase64Req();
        HashMap<String, Long> externalFields = new HashMap<>();
        externalFields.put("timestamp", System.currentTimeMillis());
        addFacesBase64Req.withExternalFields(externalFields);
        addFacesBase64Req.withImageBase64(imageBase64);
        addFacesByBase64Request.withBody(addFacesBase64Req);
        try {
            AddFacesByBase64Response addFacesByBase64Response = client.addFacesByBase64(addFacesByBase64Request);
            System.out.println(addFacesByBase64Response.toString());
            return addFacesByBase64Response.getFaces();
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            handleServiceResponseException(e);
        }
        return new ArrayList<>();
    }

    private static void updateFace(FrsClient client, String faceSetName, String faceId, String externalImageId) {
        UpdateFaceRequest updateFaceRequest = new UpdateFaceRequest();
        updateFaceRequest.withFaceSetName(faceSetName);
        UpdateFaceReq updateFaceReq = new UpdateFaceReq();
        updateFaceReq.withFaceId(faceId).withExternalImageId(externalImageId);
        updateFaceRequest.withBody(updateFaceReq);
        try {
            UpdateFaceResponse response = client.updateFace(updateFaceRequest);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            handleServiceResponseException(e);
        }
    }

    private static void handleServiceResponseException(ServiceResponseException e) {
        System.out.println(e.getMessage());
        System.out.println(e.getHttpStatusCode());
        System.out.println(e.getRequestId());
        System.out.println(e.getErrorCode());
        System.out.println(e.getErrorMsg());
    }
}