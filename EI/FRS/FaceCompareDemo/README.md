# 人脸识别服务人脸比对示例（Java版本）

## 0.版本说明
本示例基于华为云SDK V3.0版本开发

## 1.简介
华为云提供了人脸识别服务端SDK，您可以直接集成服务端SDK来调用人脸识别服务的相关API，从而实现对人脸识别服务的快速操作。

该示例展示了如何通过Java版SDK实现人脸比对。

## 2.开发前准备
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已订阅人脸识别服务。
- 已具备开发环境，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 已获取直播服务对应区域的项目ID，请在华为云控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见 [API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html) 。

## 3.安装SDK
您可以通过Maven配置所依赖的人脸识别服务SDK
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-frs</artifactId>
    <version>3.1.69</version>
</dependency>
```

## 4. 开始使用
### 4.1 导入依赖模块
```java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;

//v2版本sdk
import com.huaweicloud.sdk.frs.v2.FrsClient;
import com.huaweicloud.sdk.frs.v2.model.CompareFaceByBase64Request;
import com.huaweicloud.sdk.frs.v2.model.CompareFaceByBase64Response;
import com.huaweicloud.sdk.frs.v2.model.FaceCompareBase64Req;
import com.huaweicloud.sdk.frs.v2.region.FrsRegion;

```
### 4.2 初始化认证信息
``` java
public static ICredential getCredential(String ak, String sk) {
    return new BasicCredentials()
            .withAk(ak)
            .withSk(sk);
}
```
相关参数说明如下所示：
- ak：华为云账号Access Key。
- sk：华为云账号Secret Access Key 。
### 4.3 初始化人脸识别服务的客户端
``` java
public static FrsClient getClient(Region region, ICredential auth) {
    // 初始化人脸识别服务的客户端
    return FrsClient.newBuilder()
            .withCredential(auth)
            .withRegion(region) // 选择服务所在区域 FrsRegion.CN_NORTH_4
            .build();
}
```
相关参数说明如下所示：

service region: 服务所在区域，例如：
- CN_NORTH_1 北京一
- CN_NORTH_4 北京四
## 5. SDK demo代码解析
### 5.1 人脸比对
``` java
CompareFaceByBase64Request compareRequest = new CompareFaceByBase64Request();
FaceCompareBase64Req faceCompareBase64Req = new FaceCompareBase64Req();
faceCompareBase64Req.withImage1Base64("/9j/4AAQSkZJRgABAQAAAQABAAD...");
faceCompareBase64Req.withImage2Base64("/9j/4AAQSkZJRgABAQAAAQABAAD...");
compareRequest.withBody(faceCompareBase64Req);
try {
    CompareFaceByBase64Response compareResponse = client.compareFaceByBase64(compareRequest);
    System.out.println(compareResponse.toString());
} catch (ConnectionException | RequestTimeoutException e) {
    System.out.println(e.getCause().toString());
} catch (ServiceResponseException e) {
    System.out.println(e.getCause().toString());
    System.out.println(e.getHttpStatusCode());
    System.out.println(e.getRequestId());
    System.out.println(e.getErrorCode());
    System.out.println(e.getErrorMsg());
}
```
## 6.参考
更多信息请参考[人脸识别服务](https://support.huaweicloud.com/face/index.html)

## 7.修订记录
|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2023-03-08 |   1.0    | 文档首次发布 |