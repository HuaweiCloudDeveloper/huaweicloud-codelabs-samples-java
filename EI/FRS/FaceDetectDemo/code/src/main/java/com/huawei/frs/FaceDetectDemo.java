package com.huawei.frs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.frs.v2.FrsClient;
import com.huaweicloud.sdk.frs.v2.model.DetectFaceByBase64Request;
import com.huaweicloud.sdk.frs.v2.model.DetectFaceByBase64Response;
import com.huaweicloud.sdk.frs.v2.model.FaceDetectBase64Req;
import com.huaweicloud.sdk.frs.v2.region.FrsRegion;

import org.apache.commons.codec.binary.Base64;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FaceDetectDemo {

    public static String getImgStr(String imgPath) {
        byte[] data = null;
        try {
            data = Files.readAllBytes(Paths.get(imgPath));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return Base64.encodeBase64String(data);
    }

    public static String getResourceImagePath(String imgPath) {
        try {
            URL url = FaceDetectDemo.class.getClassLoader().getResource(imgPath);
            assert url != null;
            Path path = Paths.get(url.toURI());
            return path.toString();
        } catch (URISyntaxException e) {
            System.out.println(e.getMessage());
            return "";
        }
    }

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 将图片转为base64
        String imageBase64 = getImgStr(getResourceImagePath("data/face-demo.jpg"));

        // Init Auth Info
        ICredential credential = getCredential(ak, sk);

        // create frsClient
        FrsClient client = getClient(FrsRegion.CN_NORTH_4, credential);

        // 人脸检测Base64方式
        String attributes = "2,4";
        detectFaceByBase64(client, imageBase64, attributes);

    }

    /**
     * 获取鉴权
     *
     * @param ak 开通人脸识别服务账号的AK
     * @param sk 开通人脸识别服务账号的SK
     * @return 认证信息
     */
    public static ICredential getCredential(String ak, String sk) {
        return new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
    }

    /**
     * 初始化客户端
     *
     * @param region 开通服务的RegionID
     * @param auth 认证信息
     * @return frs客户端
     */
    public static FrsClient getClient(Region region, ICredential auth) {
        return FrsClient.newBuilder()
                .withCredential(auth)
                .withRegion(region)
                .build();
    }

    /**
     * 人脸检测
     *
     * @param client 初始化的frs客户端
     * @param imageBase64 人脸图片的base64编码
     * @param attributes 检测的人脸属性
     */
    private static void detectFaceByBase64(FrsClient client, String imageBase64, String attributes) {
        DetectFaceByBase64Request detectRequest = new DetectFaceByBase64Request();
        FaceDetectBase64Req faceDetectBase64Req = new FaceDetectBase64Req();
        faceDetectBase64Req.withImageBase64(imageBase64);
        faceDetectBase64Req.withAttributes(attributes);
        detectRequest.setBody(faceDetectBase64Req);
        try {
            DetectFaceByBase64Response detectResponse = client.detectFaceByBase64(detectRequest);
            System.out.println(detectResponse.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}