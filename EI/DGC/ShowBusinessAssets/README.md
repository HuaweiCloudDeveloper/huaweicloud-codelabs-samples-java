### 示例简介

华为云提供了数据资产服务（DataArtsStudios）的SDK，您可以直接集成SDK来调用DataArtsStudios的相关API，从而实现对DataArtsStudios的快速操作。 该示例展示了如何通过java版SDK查询业务资产。

### 前置条件

1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。  
2、您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。  
3、获取对应地区的region，可在[API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/dataartsstudio/doc?api=ShowBusinessAssets) 中Region下拉框中选择不同的地区进行查询。  
4、获取空间workspace和项目poriectId，可在数据服务空间列表信息中获取  
5、数据来源，可通过[API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/DataArtsStudio/doc?api=BatchPublish)接口发布业务资产或者在console页面通过"数据架构-主题设计-发布"、"数据架构-关系建模-逻辑模型-发布"等方式人工同步数据
6、华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。  
7、安装SDK
您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。 具体的SDK版本号请参见 SDK开发中心 。

```xml
   <dependencies>
      <dependency>
         <groupId>com.huaweicloud.sdk</groupId>
         <artifactId>huaweicloud-sdk-dataartsstudio</artifactId>
         <version>3.1.45</version>
      </dependency>
   </dependencies>
 ```
### 开始使用

查询业务资产示例代码
```java
package com.huawei.clouds.dataarts.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.dataartsstudio.v1.DataArtsStudioClient;
import com.huaweicloud.sdk.dataartsstudio.v1.model.BusinessAssetRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.ShowBusinessAssetsRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.ShowBusinessAssetsResponse;
import com.huaweicloud.sdk.dataartsstudio.v1.region.DataArtsStudioRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowBusinessAssetsDemo {
    private static final Logger logger = LoggerFactory.getLogger(ShowBusinessAssetsDemo.class);

    private static final int DEFAULT_LIMIT = 10;

    private static final int DEFAULT_OFFSET = 0;

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionid = "{your regionid string}";
        String endpoint = "{your endpoint string}";
        String workspace = "{your workspace string}";
        String keyWord = "{your query keyWord string}";
        String projectId = "{your projectId string}";
        // 查询业务对象时type枚举值使用BUSINESS，逻辑实体使用LOGICENTITY
        String typeName = "{your typeName string}";
        
        BasicCredentials auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);
        
        Region region = new Region(regionid, endpoint);
        DataArtsStudioClient dataArtsStudioClient = DataArtsStudioClient.newBuilder()
                .withCredential(auth)
                .withRegion(region)
                .build();

        ShowBusinessAssetsRequest showBusinessAssetsRequest = getShowBusinessAssetsRequest(workspace, keyWord, typeName);
        try {
            ShowBusinessAssetsResponse response = dataArtsStudioClient.showBusinessAssets(showBusinessAssetsRequest);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("RequestId: " + e.getRequestId());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static ShowBusinessAssetsRequest getShowBusinessAssetsRequest(String workspace, String keyWord, String typeName) {

        BusinessAssetRequest requestBody = new BusinessAssetRequest()
                .withSearchAllAttributes(false)
                .withLimit(DEFAULT_LIMIT)
                .withOffset(DEFAULT_OFFSET)
                .withQuery(keyWord)
                .withType(BusinessAssetRequest.TypeEnum.fromValue(typeName));
        return new ShowBusinessAssetsRequest().withBody(requestBody).withWorkspace(workspace);
    }
}
 ```

### 返回示例

业务资产信息

```
{
    "assets": [
        {
            "attributes": {
                "owner": "userName",
                "code": "code",
                "create_time": 1680780320570,
                "data_classify": "BASE_DATA",
                "description": "无",
                "workspace_id": "workspaceId",
                "update_time": 1681355584556,
                "tables": [
                    {
                        "type_name": "dli_table_managed",
                        "name": "dim_test005",
                        "guid": "guid",
                        "unique_attributes": {
                            "qualified_name": "qualifiedName"
                        }
                    }
                ],
                "name_eng": "dim_test005",
                "qualified_name": "qualifiedName",
                "name": "test005",
                "alias": "",
                "fields": [
                    {
                        "type_name": "BusinessLogicEntityColumn",
                        "guid": "32d91e29-3ecc-4515-a813-81db2d93a4d2"
                    }
                ]
            },
            "classification_names": null,
            "connection": null,
            "create_time": 1680779684163,
            "created_by": "userName",
            "display_text": "test005",
            "guid": "57424b9b-a7cf-4a27-8fae-54c6e59754be",
            "relationship_attributes": {
                "tables": [
                    {
                        "type_name": "dli_table_managed",
                        "relationship_attributes": {
                            "type_name": "business_logicentity_table",
                            "attributes": {}
                        },
                        "relationship_status": "ACTIVE",
                        "guid": "51f290f7-ba4e-458e-892f-f5a7827923ab",
                        "display_text": "dim_test005",
                        "relationship_guid": "a0521d71-ad1a-4ac1-87fc-b83fc3565118"
                    }
                ],
                "catelog": {
                    "type_name": "BusinessCatalog",
                    "relationship_attributes": {
                        "type_name": "business_catelog_logicentity"
                    },
                    "relationship_status": "ACTIVE",
                    "guid": "98893dd0-4a50-4d62-88c9-dcb694a87350",
                    "detail_attributes": {
                        "related_conn_num": 0,
                        "related_table_num": 0,
                        "attributes": {
                            "path": "大大大222222",
                            "data_owner_list": [
                                "userName"
                            ]
                        }
                    },
                    "display_text": "大大大222222",
                    "relationship_guid": "7d5bcf7d-a84e-405c-9a38-a0559d0b128a"
                },
                "fields": [
                    {
                        "type_name": "BusinessLogicEntityColumn",
                        "relationship_attributes": {
                            "type_name": "business_logicentity_fields"
                        },
                        "relationship_status": "ACTIVE",
                        "guid": "32d91e29-3ecc-4515-a813-81db2d93a4d2",
                        "display_text": "test0059999999",
                        "relationship_guid": "e04e177c-0b49-47c6-9ecf-cf30ef4dcaa3"
                    }
                ],
                "tags": []
            },
            "tags": null,
            "type_name": "BusinessLogicEntity",
            "update_time": 1681354940613,
            "updated_by": "userName"
        }
    ],
    "count": 10
}
```

### 接口及参数说明

参见：
[查询业务资产](https://support.huaweicloud.com/api-dataartsstudio/ShowBusinessAssets.html)

### 修订记录
   
| 发布日期        | 文档版本    | 修订说明    |
|-------------|-----|-----|
| 2023-08-01  |   1.0  |  文档首次发布   |
