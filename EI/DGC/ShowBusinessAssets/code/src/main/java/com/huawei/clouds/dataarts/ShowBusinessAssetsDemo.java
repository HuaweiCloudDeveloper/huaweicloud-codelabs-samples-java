package com.huawei.clouds.dataarts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.dataartsstudio.v1.DataArtsStudioClient;
import com.huaweicloud.sdk.dataartsstudio.v1.model.BusinessAssetRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.ShowBusinessAssetsRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.ShowBusinessAssetsResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowBusinessAssetsDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShowBusinessAssetsDemo.class);

    private static final int DEFAULT_LIMIT = 10;

    private static final int DEFAULT_OFFSET = 0;

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionid = "{your regionid string}";
        String endpoint = "{your endpoint string}";
        String workspace = "{your workspace string}";
        String keyWord = "{your query keyWord string}";
        String projectId = "{your projectId string}";
        // 查询业务对象时type枚举值使用BUSINESS，逻辑实体使用LOGICENTITY
        String typeName = "{your typeName string}";

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        Region region = new Region(regionid, endpoint);
        DataArtsStudioClient dataArtsStudioClient = DataArtsStudioClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        ShowBusinessAssetsRequest showBusinessAssetsRequest = getShowBusinessAssetsRequest(workspace, keyWord, typeName);
        try {
            ShowBusinessAssetsResponse response = dataArtsStudioClient.showBusinessAssets(showBusinessAssetsRequest);
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
            LOGGER.error("RequestId: " + e.getRequestId());
            LOGGER.error("ErrorCode: " + e.getErrorCode());
            LOGGER.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static ShowBusinessAssetsRequest getShowBusinessAssetsRequest(String workspace, String keyWord, String typeName) {
        BusinessAssetRequest requestBody = new BusinessAssetRequest()
            .withSearchAllAttributes(false)
            .withLimit(DEFAULT_LIMIT)
            .withOffset(DEFAULT_OFFSET)
            .withQuery(keyWord)
            .withType(BusinessAssetRequest.TypeEnum.fromValue(typeName));
        return new ShowBusinessAssetsRequest()
            .withBody(requestBody).withWorkspace(workspace);
    }
}
