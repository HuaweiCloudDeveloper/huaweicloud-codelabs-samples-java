### Introduction
This example shows how to update directory using the Java SDK.

### Preparations
1. Obtain the Huawei Cloud Development Kit (SDK). You can also view and install the JAVA SDK. 
2. You must have a HUAWEI CLOUD account and the corresponding Access Key (AK) and Secret Access Key (SK). On the HUAWEI CLOUD console, choose My Credential > Access Keys to create and view your AK/SK. For more information, see [the access key](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0001.html).
3. Obtain the region of the corresponding region,You can select different regions in the Region drop-down box in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DataArtsStudio/doc?api=UpdateDirectory) to query.  
4. Obtain the space workspace and projectId, which can be obtained in the data architecture space list information. 
5. HUAWEI CLOUD Java SDK supports Java JDK 1.8 and above.  
6. Install SDK. 
   You can obtain and install the SDK through Maven. You only need to add the corresponding dependencies to the pom.xml file of the Java project. For the specific SDK version number, please see the SDK Development Center.

```xml
   <dependencies>
      <dependency>
         <groupId>com.huaweicloud.sdk</groupId>
         <artifactId>huaweicloud-sdk-dataartsstudio</artifactId>
         <version>3.1.45</version>
      </dependency>
   </dependencies>
 ```
### Start To Operate

Update Directory Sample Code
```java
package com.huawei.clouds.dataarts;


import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.StringUtils;
import com.huaweicloud.sdk.dataartsstudio.v1.DataArtsStudioClient;
import com.huaweicloud.sdk.dataartsstudio.v1.model.DirectoryVO;

import com.huaweicloud.sdk.dataartsstudio.v1.model.UpdateDirectoryRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.UpdateDirectoryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class UpdateDirectoryDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateDirectoryDemo.class);

    public static void main(String[] args) {
        // The AK and SK used for authentication are hard-coded or stored in plaintext, which has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, AK and SK are stored in environment variables for authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String projectId = "{your projectId string}";
        String workspace = "{your workspace string}";
        String directoryName = "{your directory name string}";
        // STANDARD_ELEMENT or CODE
        String directoryType = "{your directory type string}";
        // when update directory, the id is necessary
        String directoryId = "{your directoryId string}";
        String parentId = "{your parent directory id string}";
        String prevId = "{your last directory id string}";

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        Region region = new Region(regionId, endpoint);
        DataArtsStudioClient dataArtsStudioClient = DataArtsStudioClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        DirectoryVO directoryVO = getDirectoryVO(directoryId, directoryName, parentId, prevId, directoryType);
        UpdateDirectoryRequest request = getUpdateDirectoryRequest(directoryVO, workspace);
        try {
            UpdateDirectoryResponse response = dataArtsStudioClient.updateDirectory(request);
            LOGGER.info("response = " + response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
            LOGGER.error("RequestId: " + e.getRequestId());
            LOGGER.error("ErrorCode: " + e.getErrorCode());
            LOGGER.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static DirectoryVO getDirectoryVO(String directoryId, String directoryName, String parentId,
                                              String prevId, String directoryType) {
        if (StringUtils.isEmpty(directoryId)) {
            LOGGER.error("directoryId is empty and can not be updated.");
            return null;
        }
        DirectoryVO directoryVO = new DirectoryVO();
        directoryVO.withId(Long.parseLong(directoryId));
        directoryVO.withName(directoryName);
        if (!StringUtils.isEmpty(parentId)) {
            directoryVO.withParentId(Long.parseLong(parentId));
        } else {
            LOGGER.warn("parentId is null");
            directoryVO.withParentId(null);
        }
        if (!StringUtils.isEmpty(prevId)) {
            directoryVO.withPrevId(Long.parseLong(prevId));
        } else {
            LOGGER.warn("prevId is null");
            directoryVO.withPrevId(null);
        }
        directoryVO.withType(DirectoryVO.TypeEnum.fromValue(directoryType));
        return directoryVO;
    }

    private static UpdateDirectoryRequest getUpdateDirectoryRequest(DirectoryVO body, String workspace) {
        UpdateDirectoryRequest updateDirectoryRequest = new UpdateDirectoryRequest();
        updateDirectoryRequest.withBody(body);
        updateDirectoryRequest.withWorkspace(workspace);
        return updateDirectoryRequest;
    }
}
 ```

### Request Sample
```
{
  "name" : "test",
  "parent_id" : null,
  "prev_id" : "1036594387779555328",
  "type" : "CODE",
  "id" : "1036594387779555328"
}
```

### Response Sample
```
{
  "data" : {
    "value" : {
      "name" : "test",
      "description" : null,
      "type" : "CODE",
      "id" : "1036594387779555328",
      "parent_id" : null,
      "prev_id" : "1036594387779555328",
      "root_id" : "1036594387779555328",
      "qualified_name" : null,
      "create_time" : null,
      "update_time" : null,
      "create_by" : null,
      "update_by" : "7b71e498e75d44048c9a22dd3c54f978",
      "children" : null
    }
  }
}
```

### Change History
| Released On | Version |       Description       |
|:-----------:|:-------:|:-----------------------:|
| 2023-12-13  |   1.0   | Initial release |

