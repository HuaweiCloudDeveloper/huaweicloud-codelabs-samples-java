### 示例简介

该示例展示了如何用java版SDK修改目录。

### 前置条件

1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。  
2、您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。  
3、获取对应地区的region，可在[API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/DataArtsStudio/doc?api=UpdateDirectory) 中Region下拉框中选择不同的地区进行查询。  
4、获取空间workspace和项目projectId，可在数据架构空间列表信息中获取  
5、华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。  
6、安装SDK
您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。具体的SDK版本号请参见SDK开发中心。

```xml
   <dependencies>
      <dependency>
         <groupId>com.huaweicloud.sdk</groupId>
         <artifactId>huaweicloud-sdk-dataartsstudio</artifactId>
         <version>3.1.45</version>
      </dependency>
   </dependencies>
 ```
### 开始使用

修改目录示例代码
```java
package com.huawei.clouds.dataarts;


import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.StringUtils;
import com.huaweicloud.sdk.dataartsstudio.v1.DataArtsStudioClient;
import com.huaweicloud.sdk.dataartsstudio.v1.model.DirectoryVO;

import com.huaweicloud.sdk.dataartsstudio.v1.model.UpdateDirectoryRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.UpdateDirectoryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class UpdateDirectoryDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateDirectoryDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String projectId = "{your projectId string}";
        String workspace = "{your workspace string}";
        String directoryName = "{your directory name string}";
        // STANDARD_ELEMENT or CODE
        String directoryType = "{your directory type string}";
        // when update directory, the id is necessary
        String directoryId = "{your directoryId string}";
        String parentId = "{your parent directory id string}";
        String prevId = "{your last directory id string}";

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        Region region = new Region(regionId, endpoint);
        DataArtsStudioClient dataArtsStudioClient = DataArtsStudioClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        DirectoryVO directoryVO = getDirectoryVO(directoryId, directoryName, parentId, prevId, directoryType);
        UpdateDirectoryRequest request = getUpdateDirectoryRequest(directoryVO, workspace);
        try {
            UpdateDirectoryResponse response = dataArtsStudioClient.updateDirectory(request);
            LOGGER.info("response = " + response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
            LOGGER.error("RequestId: " + e.getRequestId());
            LOGGER.error("ErrorCode: " + e.getErrorCode());
            LOGGER.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static DirectoryVO getDirectoryVO(String directoryId, String directoryName, String parentId,
                                              String prevId, String directoryType) {
        if (StringUtils.isEmpty(directoryId)) {
            LOGGER.error("directoryId is empty and can not be updated.");
            return null;
        }
        DirectoryVO directoryVO = new DirectoryVO();
        directoryVO.withId(Long.parseLong(directoryId));
        directoryVO.withName(directoryName);
        if (!StringUtils.isEmpty(parentId)) {
            directoryVO.withParentId(Long.parseLong(parentId));
        } else {
            LOGGER.warn("parentId is null");
            directoryVO.withParentId(null);
        }
        if (!StringUtils.isEmpty(prevId)) {
            directoryVO.withPrevId(Long.parseLong(prevId));
        } else {
            LOGGER.warn("prevId is null");
            directoryVO.withPrevId(null);
        }
        directoryVO.withType(DirectoryVO.TypeEnum.fromValue(directoryType));
        return directoryVO;
    }

    private static UpdateDirectoryRequest getUpdateDirectoryRequest(DirectoryVO body, String workspace) {
        UpdateDirectoryRequest updateDirectoryRequest = new UpdateDirectoryRequest();
        updateDirectoryRequest.withBody(body);
        updateDirectoryRequest.withWorkspace(workspace);
        return updateDirectoryRequest;
    }
}
 ```
### 请求示例
 ```
{
  "name" : "test",
  "parent_id" : null,
  "prev_id" : "1036594387779555328",
  "type" : "CODE",
  "id" : "1036594387779555328"
}
 ```

### 返回示例

```
{
  "data" : {
    "value" : {
      "name" : "test",
      "description" : null,
      "type" : "CODE",
      "id" : "1036594387779555328",
      "parent_id" : null,
      "prev_id" : "1036594387779555328",
      "root_id" : "1036594387779555328",
      "qualified_name" : null,
      "create_time" : null,
      "update_time" : null,
      "create_by" : null,
      "update_by" : "7b71e498e75d44048c9a22dd3c54f978",
      "children" : null
    }
  }
}
```

### 接口及参数说明

参见：
[修改目录](https://support.huaweicloud.com/api-dataartsstudio/UpdateDirectory.html)

### 修订记录
   
| 发布日期        | 文档版本    | 修订说明    |
|-------------|-----|-----|
| 2023-12-13  |   1.0  |  文档首次发布   |
