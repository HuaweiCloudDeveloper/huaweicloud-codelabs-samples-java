package com.huawei.clouds.dataarts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.StringUtils;
import com.huaweicloud.sdk.dataartsstudio.v1.DataArtsStudioClient;
import com.huaweicloud.sdk.dataartsstudio.v1.model.DeleteDirectoryRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.DeleteDirectoryResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class DeleteDirectoryDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteDirectoryDemo.class);

    public static void main(String[] args) {
        // The AK and SK used for authentication are hard-coded or stored in plaintext, which has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, AK and SK are stored in environment variables for authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String projectId = "{your projectId string}";
        String workspace = "{your workspace string}";
        // separate values with commas pls, (id1,id2)
        String directoryIdList = "{your directory id list string}";

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        Region region = new Region(regionId, endpoint);
        DataArtsStudioClient dataArtsStudioClient = DataArtsStudioClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        List<Long> idsItem = getIdsParam(directoryIdList);
        DeleteDirectoryRequest request = getDeleteDirectoryRequest(idsItem, workspace);
        try {
            DeleteDirectoryResponse response = dataArtsStudioClient.deleteDirectory(request);
            LOGGER.info("response = " + response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
            LOGGER.error("RequestId: " + e.getRequestId());
            LOGGER.error("ErrorCode: " + e.getErrorCode());
            LOGGER.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static List<Long> getIdsParam(String directoryIdList) {
        if (StringUtils.isEmpty(directoryIdList)) {
            LOGGER.error("directoryIdList is empty, check pls");
            return null;
        }
        String[] directoryIdArr = directoryIdList.split(",");
        List<Long> directoryIds = new ArrayList<>();
        for (String directoryId : directoryIdArr) {
            directoryIds.add(Long.parseLong(directoryId));
        }
        return directoryIds;
    }

    private static DeleteDirectoryRequest getDeleteDirectoryRequest(List<Long> ids, String workspace) {
        DeleteDirectoryRequest deleteDirectoryRequest = new DeleteDirectoryRequest();
        deleteDirectoryRequest.withIds(ids);
        deleteDirectoryRequest.withWorkspace(workspace);
        return deleteDirectoryRequest;
    }
}