package com.huawei.clouds.dataarts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.StringUtils;
import com.huaweicloud.sdk.dataartsstudio.v1.DataArtsStudioClient;
import com.huaweicloud.sdk.dataartsstudio.v1.model.CreateDirectoryRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.CreateDirectoryResponse;
import com.huaweicloud.sdk.dataartsstudio.v1.model.DirectoryVO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateDirectoryDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateDirectoryDemo.class);

    public static void main(String[] args) {
        // The AK and SK used for authentication are hard-coded or stored in plaintext, which has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, AK and SK are stored in environment variables for authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String projectId = "{your projectId string}";
        String workspace = "{your workspace string}";
        String directoryName = "{your directory name string}";
        // STANDARD_ELEMENT or CODE
        String directoryType = "{your directory type string}";
        String parentId = "{your parent directory id string}";
        String prevId = "{your last directory id string}";
        String description = "{your directory description string}";

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        Region region = new Region(regionId, endpoint);
        DataArtsStudioClient dataArtsStudioClient = DataArtsStudioClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        DirectoryVO directoryVO = getDirectoryVO(description, directoryName, parentId, prevId, directoryType);
        CreateDirectoryRequest request = getCreateDirectoryRequest(directoryVO, workspace);
        try {
            CreateDirectoryResponse response = dataArtsStudioClient.createDirectory(request);
            LOGGER.info("response = " + response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
            LOGGER.error("RequestId: " + e.getRequestId());
            LOGGER.error("ErrorCode: " + e.getErrorCode());
            LOGGER.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static DirectoryVO getDirectoryVO(String description, String directoryName, String parentId,
                                              String prevId, String directoryType) {
        DirectoryVO directoryVO = new DirectoryVO();
        directoryVO.withDescription(description);
        directoryVO.withName(directoryName);
        if (!StringUtils.isEmpty(parentId)) {
            directoryVO.withParentId(Long.parseLong(parentId));
        } else {
            LOGGER.warn("parentId is null");
            directoryVO.withParentId(null);
        }
        if (!StringUtils.isEmpty(prevId)) {
            directoryVO.withPrevId(Long.parseLong(prevId));
        } else {
            LOGGER.warn("prevId is null");
            directoryVO.withPrevId(null);
        }
        directoryVO.withType(DirectoryVO.TypeEnum.fromValue(directoryType));
        return directoryVO;
    }

    private static CreateDirectoryRequest getCreateDirectoryRequest(DirectoryVO body, String workspace) {
        CreateDirectoryRequest createDirectoryRequest = new CreateDirectoryRequest();
        createDirectoryRequest.withBody(body);
        createDirectoryRequest.withWorkspace(workspace);
        return createDirectoryRequest;
    }
}