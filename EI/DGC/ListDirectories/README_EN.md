### Introduction
This example shows how to list all directories using the Java SDK.

### Preparations
1. Obtain the Huawei Cloud Development Kit (SDK). You can also view and install the JAVA SDK. 
2. You must have a HUAWEI CLOUD account and the corresponding Access Key (AK) and Secret Access Key (SK). On the HUAWEI CLOUD console, choose My Credential > Access Keys to create and view your AK/SK. For more information, see [the access key](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0001.html).
3. Obtain the region of the corresponding region,You can select different regions in the Region drop-down box in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DataArtsStudio/doc?api=ListDirectories) to query.  
4. Obtain the space workspace and projectId, which can be obtained in the data architecture space list information. 
5. HUAWEI CLOUD Java SDK supports Java JDK 1.8 and above.  
6. Install SDK. 
   You can obtain and install the SDK through Maven. You only need to add the corresponding dependencies to the pom.xml file of the Java project. For the specific SDK version number, please see the SDK Development Center.

```xml
   <dependencies>
      <dependency>
         <groupId>com.huaweicloud.sdk</groupId>
         <artifactId>huaweicloud-sdk-dataartsstudio</artifactId>
         <version>3.1.45</version>
      </dependency>
   </dependencies>
 ```
### Start To Operate

Sample Code
```java
package com.huawei.clouds.dataarts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.dataartsstudio.v1.DataArtsStudioClient;
import com.huaweicloud.sdk.dataartsstudio.v1.model.ListDirectoriesRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.ListDirectoriesResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ListDirectoriesDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(ListDirectoriesDemo.class);

    private static final int DEFAULT_LIMIT = 10;

    private static final int DEFAULT_OFFSET = 0;

    public static void main(String[] args) {
        // The AK and SK used for authentication are hard-coded or stored in plaintext, which has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, AK and SK are stored in environment variables for authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String projectId = "{your projectId string}";
        String workspace = "{your workspace string}";
        // Enum value: CODE or STANDARD_ELEMENT
        String typeName = "{your directory type name string}";

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        Region region = new Region(regionId, endpoint);
        DataArtsStudioClient dataArtsStudioClient = DataArtsStudioClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        ListDirectoriesRequest request = getListDirectoriesRequest(typeName, workspace);
        try {
            ListDirectoriesResponse response = dataArtsStudioClient.listDirectories(request);
            LOGGER.info("response = " + response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
            LOGGER.error("RequestId: " + e.getRequestId());
            LOGGER.error("ErrorCode: " + e.getErrorCode());
            LOGGER.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static ListDirectoriesRequest getListDirectoriesRequest(String typeName, String workspace) {
        ListDirectoriesRequest listDirectoriesRequest = new ListDirectoriesRequest();
        listDirectoriesRequest.withLimit(DEFAULT_LIMIT);
        listDirectoriesRequest.withOffset(DEFAULT_OFFSET);
        listDirectoriesRequest.withType(ListDirectoriesRequest.TypeEnum.fromValue(typeName));
        listDirectoriesRequest.withWorkspace(workspace);
        return listDirectoriesRequest;
    }
}
 ```

### Response Sample
```
{
  "data" : {
    "value" : [ {
      "name" : "db",
      "description" : "db",
      "type" : "db",
      "id" : "db",
      "parent_id" : "db",
      "prev_id" : "db",
      "root_id" : "db",
      "qualified_name" : "qname",
      "tenant_id" : "tid",
      "create_time" : 1111,
      "update_time" : 2222,
      "create_by" : "test",
      "update_by" : "test",
      "children" : [ {
        "name" : "child",
        "description" : "db",
        "xxxx" : "其他属性",
        "children" : [ {
          "name" : "child_of_child",
          "xxxx" : "其他属性"
        } ]
      } ]
    } ]
  }
}
```

### Change History
| Released On | Version |       Description       |
|:-----------:|:-------:|:-----------------------:|
| 2023-12-13  |   1.0   | Initial release |

