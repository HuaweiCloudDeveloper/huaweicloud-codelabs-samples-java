package com.huawei.clouds.dataarts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.dataartsstudio.v1.DataArtsStudioClient;
import com.huaweicloud.sdk.dataartsstudio.v1.model.ListDirectoriesRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.ListDirectoriesResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListDirectoriesDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(ListDirectoriesDemo.class);

    private static final int DEFAULT_LIMIT = 10;

    private static final int DEFAULT_OFFSET = 0;

    public static void main(String[] args) {
        // The AK and SK used for authentication are hard-coded or stored in plaintext, which has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, AK and SK are stored in environment variables for authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String projectId = "{your projectId string}";
        String workspace = "{your workspace string}";
        // Enum value: CODE or STANDARD_ELEMENT
        String typeName = "{your directory type name string}";

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        Region region = new Region(regionId, endpoint);
        DataArtsStudioClient dataArtsStudioClient = DataArtsStudioClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        ListDirectoriesRequest request = getListDirectoriesRequest(typeName, workspace);
        try {
            ListDirectoriesResponse response = dataArtsStudioClient.listDirectories(request);
            LOGGER.info("response = " + response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
            LOGGER.error("RequestId: " + e.getRequestId());
            LOGGER.error("ErrorCode: " + e.getErrorCode());
            LOGGER.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static ListDirectoriesRequest getListDirectoriesRequest(String typeName, String workspace) {
        ListDirectoriesRequest listDirectoriesRequest = new ListDirectoriesRequest();
        listDirectoriesRequest.withLimit(DEFAULT_LIMIT);
        listDirectoriesRequest.withOffset(DEFAULT_OFFSET);
        listDirectoriesRequest.withType(ListDirectoriesRequest.TypeEnum.fromValue(typeName));
        listDirectoriesRequest.withWorkspace(workspace);
        return listDirectoriesRequest;
    }
}