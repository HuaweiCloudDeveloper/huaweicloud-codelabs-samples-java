### 示例简介

该示例展示了如何用java版SDK获取所有目录。

### 前置条件

1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。  
2、您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。  
3、获取对应地区的region，可在[API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/DataArtsStudio/doc?api=ListDirectories) 中Region下拉框中选择不同的地区进行查询。  
4、获取空间workspace和项目projectId，可在数据架构空间列表信息中获取  
5、华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。  
6、安装SDK
您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。具体的SDK版本号请参见SDK开发中心。

```xml
   <dependencies>
      <dependency>
         <groupId>com.huaweicloud.sdk</groupId>
         <artifactId>huaweicloud-sdk-dataartsstudio</artifactId>
         <version>3.1.45</version>
      </dependency>
   </dependencies>
 ```
### 开始使用

获取所有目录
```java
package com.huawei.clouds.dataarts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.dataartsstudio.v1.DataArtsStudioClient;
import com.huaweicloud.sdk.dataartsstudio.v1.model.ListDirectoriesRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.ListDirectoriesResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ListDirectoriesDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(ListDirectoriesDemo.class);

    private static final int DEFAULT_LIMIT = 10;

    private static final int DEFAULT_OFFSET = 0;

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String projectId = "{your projectId string}";
        String workspace = "{your workspace string}";
        // Enum value: CODE or STANDARD_ELEMENT
        String typeName = "{your directory type name string}";

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        Region region = new Region(regionId, endpoint);
        DataArtsStudioClient dataArtsStudioClient = DataArtsStudioClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        ListDirectoriesRequest request = getListDirectoriesRequest(typeName, workspace);
        try {
            ListDirectoriesResponse response = dataArtsStudioClient.listDirectories(request);
            LOGGER.info("response = " + response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
            LOGGER.error("RequestId: " + e.getRequestId());
            LOGGER.error("ErrorCode: " + e.getErrorCode());
            LOGGER.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static ListDirectoriesRequest getListDirectoriesRequest(String typeName, String workspace) {
        ListDirectoriesRequest listDirectoriesRequest = new ListDirectoriesRequest();
        listDirectoriesRequest.withLimit(DEFAULT_LIMIT);
        listDirectoriesRequest.withOffset(DEFAULT_OFFSET);
        listDirectoriesRequest.withType(ListDirectoriesRequest.TypeEnum.fromValue(typeName));
        listDirectoriesRequest.withWorkspace(workspace);
        return listDirectoriesRequest;
    }
}
 ```

### 返回示例

```
{
  "data" : {
    "value" : [ {
      "name" : "db",
      "description" : "db",
      "type" : "db",
      "id" : "db",
      "parent_id" : "db",
      "prev_id" : "db",
      "root_id" : "db",
      "qualified_name" : "qname",
      "tenant_id" : "tid",
      "create_time" : 1111,
      "update_time" : 2222,
      "create_by" : "test",
      "update_by" : "test",
      "children" : [ {
        "name" : "child",
        "description" : "db",
        "xxxx" : "其他属性",
        "children" : [ {
          "name" : "child_of_child",
          "xxxx" : "其他属性"
        } ]
      } ]
    } ]
  }
}
```

### 接口及参数说明

参见：
[获取所有目录](https://support.huaweicloud.com/api-dataartsstudio/GetAllDirectorys.html)

### 修订记录
   
| 发布日期        | 文档版本    | 修订说明    |
|-------------|-----|-----|
| 2023-12-13  |   1.0  |  文档首次发布   |
