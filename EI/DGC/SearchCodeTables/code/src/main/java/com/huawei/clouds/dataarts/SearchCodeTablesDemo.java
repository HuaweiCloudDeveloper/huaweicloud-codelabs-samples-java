package com.huawei.clouds.dataarts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.dataartsstudio.v1.DataArtsStudioClient;
import com.huaweicloud.sdk.dataartsstudio.v1.model.SearchCodeTablesResponse;
import com.huaweicloud.sdk.dataartsstudio.v1.model.SearchCodeTablesRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchCodeTablesDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(SearchCodeTablesDemo.class);

    private static final int DEFAULT_LIMIT = 10;

    private static final int DEFAULT_OFFSET = 0;

    public static void main(String[] args) {
        // The AK and SK used for authentication are hard-coded or stored in plaintext, which has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, AK and SK are stored in environment variables for authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String projectId = "{your projectId string}";
        String workspace = "{your workspace string}";
        String codeTableName = "{your codeTable name string}";
        String createdBy = "{your codeTable creator string}";
        String status = "{your codeTable status string}";

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        Region region = new Region(regionId, endpoint);
        DataArtsStudioClient dataArtsStudioClient = DataArtsStudioClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        SearchCodeTablesRequest request = getSearchCodeTablesRequest(codeTableName, createdBy, status, workspace);
        try {
            SearchCodeTablesResponse response = dataArtsStudioClient.searchCodeTables(request);
            System.out.println("response = " + response.toString());
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
            LOGGER.error("RequestId: " + e.getRequestId());
            LOGGER.error("ErrorCode: " + e.getErrorCode());
            LOGGER.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static SearchCodeTablesRequest getSearchCodeTablesRequest(String codeTableName,
                   String createdBy, String status, String workspace) {
        SearchCodeTablesRequest searchCodeTablesRequest = new SearchCodeTablesRequest();
        searchCodeTablesRequest.withName(codeTableName);
        searchCodeTablesRequest.withCreateBy(createdBy);
        searchCodeTablesRequest.withStatus(SearchCodeTablesRequest.StatusEnum.fromValue(status));
        searchCodeTablesRequest.withLimit(DEFAULT_LIMIT);
        searchCodeTablesRequest.withOffset(DEFAULT_OFFSET);
        searchCodeTablesRequest.withWorkspace(workspace);
        return searchCodeTablesRequest;
    }
}