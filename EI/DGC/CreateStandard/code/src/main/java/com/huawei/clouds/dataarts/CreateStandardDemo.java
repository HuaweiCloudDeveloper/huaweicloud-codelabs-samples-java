package com.huawei.clouds.dataarts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.StringUtils;
import com.huaweicloud.sdk.dataartsstudio.v1.DataArtsStudioClient;
import com.huaweicloud.sdk.dataartsstudio.v1.model.CreateStandardRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.CreateStandardResponse;
import com.huaweicloud.sdk.dataartsstudio.v1.model.StandElementValueVO;
import com.huaweicloud.sdk.dataartsstudio.v1.model.StandElementValueVOList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CreateStandardDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateStandardDemo.class);

    private static final String NAMECH = "nameCh";

    public static void main(String[] args) {
        // The AK and SK used for authentication are hard-coded or stored in plaintext, which has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, AK and SK are stored in environment variables for authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String projectId = "{your projectId string}";
        String workspace = "{your workspace string}";
        String directoryId = "{your directory id string}";
        String fdName = "{your fdName string}";

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        Region region = new Region(regionId, endpoint);
        DataArtsStudioClient dataArtsStudioClient = DataArtsStudioClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        List<StandElementValueVO> values = getStandElementValues(fdName, directoryId);
        StandElementValueVOList body = getStandElementValueVOList(directoryId, values);
        CreateStandardRequest request = getCreateStandardRequest(body, workspace);
        try {
            CreateStandardResponse response = dataArtsStudioClient.createStandard(request);
            LOGGER.info("response = " + response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
            LOGGER.error("RequestId: " + e.getRequestId());
            LOGGER.error("ErrorCode: " + e.getErrorCode());
            LOGGER.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    /*
     * Creates a single standard under the specified directory.
     * If you want to create multiple standards at the same time, you can extend StandElementValueVO
     */
    private static List<StandElementValueVO> getStandElementValues(String fdValue, String directoryId) {
        List<StandElementValueVO> standElementValueVOList = new ArrayList<>();
        StandElementValueVO standElementValueVOOne = new StandElementValueVO();
        standElementValueVOOne.withFdName(NAMECH);
        standElementValueVOOne.withFdValue(fdValue);
        if (StringUtils.isEmpty(directoryId)) {
            LOGGER.error("directoryId is empty,check pls");
            return null;
        }
        standElementValueVOOne.withDirectoryId(Long.parseLong(directoryId));
        standElementValueVOList.add(standElementValueVOOne);
        return standElementValueVOList;

    }

    private static StandElementValueVOList getStandElementValueVOList( String directoryId, List<StandElementValueVO> values) {
        if (StringUtils.isEmpty(directoryId)) {
            LOGGER.error("directoryId is empty,check pls");
            return null;
        }
        if (values == null || values.size() == 0) {
            LOGGER.error("StandElementValueVO list values is empty,check pls");
            return null;
        }
        StandElementValueVOList standElementValueVOList = new StandElementValueVOList();
        standElementValueVOList.withId(Long.parseLong(directoryId));
        standElementValueVOList.withDirectoryId(Long.parseLong(directoryId));
        standElementValueVOList.withValues(values);

        return standElementValueVOList;
    }

    private static CreateStandardRequest getCreateStandardRequest(StandElementValueVOList body, String workspace) {
        CreateStandardRequest createStandardRequest = new CreateStandardRequest();
        createStandardRequest.withBody(body);
        createStandardRequest.withWorkspace(workspace);
        return createStandardRequest;
    }
}