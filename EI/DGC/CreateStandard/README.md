### 示例简介

该示例展示了如何用java版SDK创建数据标准。

### 前置条件

1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。  
2、您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。  
3、获取对应地区的region，可在[API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/DataArtsStudio/doc?api=CreateStandard) 中Region下拉框中选择不同的地区进行查询。  
4、获取空间workspace和项目projectId，可在数据架构空间列表信息中获取  
5、华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。  
6、安装SDK
您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。具体的SDK版本号请参见SDK开发中心。

```xml
   <dependencies>
      <dependency>
         <groupId>com.huaweicloud.sdk</groupId>
         <artifactId>huaweicloud-sdk-dataartsstudio</artifactId>
         <version>3.1.45</version>
      </dependency>
   </dependencies>
 ```
### 开始使用

创建数据标准
```java
package com.huawei.clouds.dataarts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.StringUtils;
import com.huaweicloud.sdk.dataartsstudio.v1.DataArtsStudioClient;
import com.huaweicloud.sdk.dataartsstudio.v1.model.CreateStandardRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.CreateStandardResponse;
import com.huaweicloud.sdk.dataartsstudio.v1.model.StandElementValueVO;
import com.huaweicloud.sdk.dataartsstudio.v1.model.StandElementValueVOList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CreateStandardDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(ListAllStandardsDemo.class);

    private static final String NAMECH = "nameCh";

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String projectId = "{your projectId string}";
        String workspace = "{your workspace string}";
        String directoryId = "{your directory id string}";
        String fdName = "{your fdName string}";

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        Region region = new Region(regionId, endpoint);
        DataArtsStudioClient dataArtsStudioClient = DataArtsStudioClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        List<StandElementValueVO> values = getStandElementValues(fdName, directoryId);
        StandElementValueVOList body = getStandElementValueVOList(directoryId, values);
        CreateStandardRequest request = getCreateStandardRequest(body, workspace);
        try {
            CreateStandardResponse response = dataArtsStudioClient.createStandard(request);
            LOGGER.info("response = " + response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
            LOGGER.error("RequestId: " + e.getRequestId());
            LOGGER.error("ErrorCode: " + e.getErrorCode());
            LOGGER.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    /*
     * Creates a single standard under the specified directory.
     * If you want to create multiple standards at the same time, you can extend StandElementValueVO
     */
    private static List<StandElementValueVO> getStandElementValues(String fdValue, String directoryId) {
        List<StandElementValueVO> standElementValueVOList = new ArrayList<>();
        StandElementValueVO standElementValueVOOne = new StandElementValueVO();
        standElementValueVOOne.withFdName(NAMECH);
        standElementValueVOOne.withFdValue(fdValue);
        if (StringUtils.isEmpty(directoryId)) {
            LOGGER.error("directoryId is empty,check pls");
            return null;
        }
        standElementValueVOOne.withDirectoryId(Long.parseLong(directoryId));
        standElementValueVOList.add(standElementValueVOOne);
        return standElementValueVOList;

    }

    private static StandElementValueVOList getStandElementValueVOList( String directoryId, List<StandElementValueVO> values) {
        if (StringUtils.isEmpty(directoryId)) {
            LOGGER.error("directoryId is empty,check pls");
            return null;
        }
        if (values == null || values.size() == 0) {
            LOGGER.error("StandElementValueVO list values is empty,check pls");
            return null;
        }
        StandElementValueVOList standElementValueVOList = new StandElementValueVOList();
        standElementValueVOList.withId(Long.parseLong(directoryId));
        standElementValueVOList.withDirectoryId(Long.parseLong(directoryId));
        standElementValueVOList.withValues(values);

        return standElementValueVOList;
    }

    private static CreateStandardRequest getCreateStandardRequest(StandElementValueVOList body, String workspace) {
        CreateStandardRequest createStandardRequest = new CreateStandardRequest();
        createStandardRequest.withBody(body);
        createStandardRequest.withWorkspace(workspace);
        return createStandardRequest;
    }
}
 ```
### 请求示例

 ```
{
  "id" : 0,
  "directory_id" : "793889791589650432",
  "values" : [ {
    "fd_name" : "nameEn",
    "fd_value" : "demo"
  }, {
    "fd_name" : "dataType",
    "fd_value" : "STRING"
  }, {
    "fd_name" : "dataLength",
    "fd_value" : "128"
  }, {
    "fd_name" : "hasAllowValueList",
    "fd_value" : false
  }, {
    "fd_name" : "allowList",
    "fd_value" : ""
  }, {
    "fd_name" : "referCodeTable",
    "fd_value" : "885123958788317184"
  }, {
    "fd_name" : "codeStandColumn",
    "fd_value" : "52470"
  }, {
    "fd_name" : "dqcRule",
    "fd_value" : ""
  }, {
    "fd_name" : "ruleOwner",
    "fd_value" : "liuxu"
  }, {
    "fd_name" : "dataMonitorOwner",
    "fd_value" : "liuxu"
  }, {
    "fd_name" : "standardLevel",
    "fd_value" : "domain"
  }, {
    "fd_name" : "description",
    "fd_value" : "这是一个demo"
  } ]
}
 ```

### 接口及参数说明

参见：
[创建数据标准](https://support.huaweicloud.com/api-dataartsstudio/CreateStandard_0.html)

### 修订记录
   
| 发布日期        | 文档版本    | 修订说明    |
|-------------|-----|-----|
| 2023-12-14  |   1.0  |  文档首次发布   |
