### Introduction
This example shows how to delete the data standards using the Java SDK.

### Preparations
1. Obtain the Huawei Cloud Development Kit (SDK). You can also view and install the JAVA SDK. 
2. You must have a HUAWEI CLOUD account and the corresponding Access Key (AK) and Secret Access Key (SK). On the HUAWEI CLOUD console, choose My Credential > Access Keys to create and view your AK/SK. For more information, see [the access key](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0001.html).
3. Obtain the region of the corresponding region,You can select different regions in the Region drop-down box in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DataArtsStudio/doc?api=DeleteStandard) to query.  
4. Obtain the space workspace and projectId, which can be obtained in the data architecture space list information. 
5. HUAWEI CLOUD Java SDK supports Java JDK 1.8 and above.  
6. Install SDK. 
   You can obtain and install the SDK through Maven. You only need to add the corresponding dependencies to the pom.xml file of the Java project. For the specific SDK version number, please see the SDK Development Center.

```xml
   <dependencies>
      <dependency>
         <groupId>com.huaweicloud.sdk</groupId>
         <artifactId>huaweicloud-sdk-dataartsstudio</artifactId>
         <version>3.1.45</version>
      </dependency>
   </dependencies>
 ```
### Start To Operate

Sample Code
```java
package com.huawei.clouds.dataarts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.StringUtils;
import com.huaweicloud.sdk.dataartsstudio.v1.DataArtsStudioClient;
import com.huaweicloud.sdk.dataartsstudio.v1.model.DeleteStandardRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.DeleteStandardResponse;
import com.huaweicloud.sdk.dataartsstudio.v1.model.IdsParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class DeleteStandardDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteStandardDemo.class);

    public static void main(String[] args) {
        // The AK and SK used for authentication are hard-coded or stored in plaintext, which has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, AK and SK are stored in environment variables for authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String projectId = "{your projectId string}";
        String workspace = "{your workspace string}";
        // separate values with commas pls, (id1,id2)
        String standardIdList = "{your data standard id list string}";

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        Region region = new Region(regionId, endpoint);
        DataArtsStudioClient dataArtsStudioClient = DataArtsStudioClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        IdsParam body = getIdsParam(standardIdList);
        DeleteStandardRequest request = getDeleteStandardRequest(body, workspace);
        try {
            DeleteStandardResponse response = dataArtsStudioClient.deleteStandard(request);
            LOGGER.info("response = " + response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
            LOGGER.error("RequestId: " + e.getRequestId());
            LOGGER.error("ErrorCode: " + e.getErrorCode());
            LOGGER.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static IdsParam getIdsParam(String standardIdList) {
        if (StringUtils.isEmpty(standardIdList)) {
            LOGGER.error("standardIdList is empty, check pls");
            return null;
        }
        String[] standardIdArr = standardIdList.split(",");
        List<Long> standardIds = new ArrayList<>();
        for (String standardId : standardIdArr) {
            standardIds.add(Long.parseLong(standardId));
        }
        IdsParam idsParam = new IdsParam();
        idsParam.withIds(standardIds);
        return idsParam;
    }

    private static DeleteStandardRequest getDeleteStandardRequest(IdsParam body, String workspace) {
        DeleteStandardRequest deleteStandardRequest = new DeleteStandardRequest();
        deleteStandardRequest.withBody(body);
        deleteStandardRequest.withWorkspace(workspace);
        return deleteStandardRequest;
    }
}
 ```

### Change History
| Released On | Version |       Description       |
|:-----------:|:-------:|:-----------------------:|
| 2023-12-14  |   1.0   | Initial release |

