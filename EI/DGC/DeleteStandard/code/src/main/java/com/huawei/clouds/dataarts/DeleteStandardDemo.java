package com.huawei.clouds.dataarts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.StringUtils;
import com.huaweicloud.sdk.dataartsstudio.v1.DataArtsStudioClient;
import com.huaweicloud.sdk.dataartsstudio.v1.model.DeleteStandardRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.DeleteStandardResponse;
import com.huaweicloud.sdk.dataartsstudio.v1.model.IdsParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class DeleteStandardDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteStandardDemo.class);

    public static void main(String[] args) {
        // The AK and SK used for authentication are hard-coded or stored in plaintext, which has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, AK and SK are stored in environment variables for authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String projectId = "{your projectId string}";
        String workspace = "{your workspace string}";
        // separate values with commas pls, (id1,id2)
        String standardIdList = "{your data standard id list string}";

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        Region region = new Region(regionId, endpoint);
        DataArtsStudioClient dataArtsStudioClient = DataArtsStudioClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        IdsParam body = getIdsParam(standardIdList);
        DeleteStandardRequest request = getDeleteStandardRequest(body, workspace);
        try {
            DeleteStandardResponse response = dataArtsStudioClient.deleteStandard(request);
            LOGGER.info("response = " + response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
            LOGGER.error("RequestId: " + e.getRequestId());
            LOGGER.error("ErrorCode: " + e.getErrorCode());
            LOGGER.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static IdsParam getIdsParam(String standardIdList) {
        if (StringUtils.isEmpty(standardIdList)) {
            LOGGER.error("standardIdList is empty, check pls");
            return null;
        }
        String[] standardIdArr = standardIdList.split(",");
        List<Long> standardIds = new ArrayList<>();
        for (String standardId : standardIdArr) {
            standardIds.add(Long.parseLong(standardId));
        }
        IdsParam idsParam = new IdsParam();
        idsParam.withIds(standardIds);
        return idsParam;
    }

    private static DeleteStandardRequest getDeleteStandardRequest(IdsParam body, String workspace) {
        DeleteStandardRequest deleteStandardRequest = new DeleteStandardRequest();
        deleteStandardRequest.withBody(body);
        deleteStandardRequest.withWorkspace(workspace);
        return deleteStandardRequest;
    }
}