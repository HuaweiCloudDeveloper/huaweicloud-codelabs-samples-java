### Introduction
Huawei Cloud provides the SDK of DataArtsStudios. You can directly integrate the SDK to call the relevant APIs of DataArtsStudios to achieve quick operations on DataArtsStudios. This example shows how to create a code table using the Java SDK.

### Preparations
1. Obtain the Huawei Cloud Development Kit (SDK). You can also view and install the JAVA SDK. 
2. You must have a HUAWEI CLOUD account and the corresponding Access Key (AK) and Secret Access Key (SK). On the HUAWEI CLOUD console, choose My Credential > Access Keys to create and view your AK/SK. For more information, see [the access key](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0001.html).
3. Obtain the region of the corresponding region,You can select different regions in the Region drop-down box in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DataArtsStudio/doc?api=CreateCodeTable) to query.  
4. Obtain the space workspace and projectId, which can be obtained in the data architecture space list information. 
5. HUAWEI CLOUD Java SDK supports Java JDK 1.8 and above.  
6. Install SDK. 
   You can obtain and install the SDK through Maven. You only need to add the corresponding dependencies to the pom.xml file of the Java project. For the specific SDK version number, please see the SDK Development Center.

```xml
   <dependencies>
      <dependency>
         <groupId>com.huaweicloud.sdk</groupId>
         <artifactId>huaweicloud-sdk-dataartsstudio</artifactId>
         <version>3.1.45</version>
      </dependency>
   </dependencies>
 ```
### Start To Operate

Sample Code
```java
package com.huawei.clouds.dataarts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.dataartsstudio.v1.DataArtsStudioClient;
import com.huaweicloud.sdk.dataartsstudio.v1.model.CodeTableVO;
import com.huaweicloud.sdk.dataartsstudio.v1.model.CodeTableFieldVO;
import com.huaweicloud.sdk.dataartsstudio.v1.model.CreateCodeTableRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.CreateCodeTableResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CreateCodeTableDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateCodeTableDemo.class);

    public static void main(String[] args) {
       // The AK and SK used for authentication are hard-coded or stored in plaintext, which has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
       // In this example, AK and SK are stored in environment variables for authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String projectId = "{your projectId string}";
        String workspace = "{your workspace string}";
        String chName = "{your codeTable Chinese name string}";
        String enName = "{your codeTable English name string}";
        String description = "{your codeTable description string}";
        String tableVersion = "{your codeTable version string}";
        String directoryId = "{your directoryId string}";
        // the field sample of code table
        List<CodeTableFieldVO> codeTableFields = getCodeTableFieldVOSampleList();

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        Region region = new Region(regionId, endpoint);
        DataArtsStudioClient dataArtsStudioClient = DataArtsStudioClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        CreateCodeTableRequest request = new CreateCodeTableRequest();
        request.withBody(getCodeTableVO(chName, enName, description, codeTableFields, tableVersion, directoryId));
        request.withWorkspace(workspace);
        try {
            CreateCodeTableResponse response = dataArtsStudioClient.createCodeTable(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException e) {
            LOGGER.error("ConnectionException: " + e.getMessage());
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
            LOGGER.error("RequestId: " + e.getRequestId());
            LOGGER.error("ErrorCode: " + e.getErrorCode());
            LOGGER.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static CodeTableVO getCodeTableVO(String chName, String enName, String desc, List<CodeTableFieldVO> codeTableFields,
                                              String tableVersion, String directoryId) {
        CodeTableVO codeTableVO = new CodeTableVO()
                .withNameEn(chName)
                .withNameCh(enName)
                .withDescription(desc).withCodeTableFields(codeTableFields)
                .withTbVersion(Integer.valueOf(tableVersion))
                .withDirectoryId(Long.valueOf(directoryId));
        return codeTableVO;
    }

    private static List<CodeTableFieldVO> getCodeTableFieldVOSampleList() {
        List<CodeTableFieldVO> codeTableFields = new ArrayList<>();
        CodeTableFieldVO codeTableFieldVO1 = new CodeTableFieldVO()
                .withOrdinal(1)
                .withNameCh("编码")
                .withNameEn("code")
                .withDataType("STRING")
                .withDescription("code field");
        CodeTableFieldVO codeTableFieldVO2 = new CodeTableFieldVO()
                .withOrdinal(2)
                .withNameCh("值")
                .withNameEn("value")
                .withDataType("STRING")
                .withDescription("value field");
        codeTableFields.add(codeTableFieldVO1);
        codeTableFields.add(codeTableFieldVO2);
        return codeTableFields;
    }
}
 ```

### Request Sample
```
{
  "name_en" : "demo",
  "name_ch" : "demo",
  "description" : "",
  "code_table_fields" : [ {
    "ordinal" : 1,
    "name_ch" : "编码",
    "name_en" : "code",
    "data_type" : "STRING",
    "description" : "",
    "code_table_field_values" : [ ]
  }, {
    "ordinal" : 2,
    "name_ch" : "值",
    "name_en" : "value",
    "data_type" : "STRING",
    "description" : "",
    "code_table_field_values" : [ ]
  } ],
  "tb_version" : 0,
  "directory_id" : "793889572823142400"
}
```

### Response Sample
```
{
  "data" : {
    "value" : {
      "id" : "1014131824240230400",
      "name_en" : "test",
      "name_ch" : "test",
      "tb_version" : 0,
      "directory_id" : "1012307270173851648",
      "directory_path" : null,
      "description" : "",
      "create_by" : "abc",
      "status" : "DRAFT",
      "create_time" : "2022-08-30T11:17:48.557+08:00",
      "update_time" : "2022-08-30T11:17:48.557+08:00",
      "approval_info" : null,
      "new_biz" : null,
      "code_table_fields" : [ {
        "id" : null,
        "code_table_id" : null,
        "ordinal" : 1,
        "name_en" : "code",
        "name_ch" : "编码",
        "description" : "",
        "data_type" : "STRING",
        "domain_type" : null,
        "data_type_extend" : null,
        "is_unique_key" : false,
        "code_table_field_values" : [ ],
        "count_field_values" : null
      }, {
        "id" : null,
        "code_table_id" : null,
        "ordinal" : 2,
        "name_en" : "value",
        "name_ch" : "值",
        "description" : "",
        "data_type" : "STRING",
        "domain_type" : null,
        "data_type_extend" : null,
        "is_unique_key" : false,
        "code_table_field_values" : [ ],
        "count_field_values" : null
      } ]
    }
  }
}
```

### Change History
| Released On | Version |       Description       |
|:-----------:|:-------:|:-----------------------:|
| 2023-12-12  |   1.0   | Initial release |

