package com.huawei.clouds.dataarts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.dataartsstudio.v1.DataArtsStudioClient;
import com.huaweicloud.sdk.dataartsstudio.v1.model.CodeTableVO;
import com.huaweicloud.sdk.dataartsstudio.v1.model.CodeTableFieldVO;
import com.huaweicloud.sdk.dataartsstudio.v1.model.CreateCodeTableRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.CreateCodeTableResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CreateCodeTableDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateCodeTableDemo.class);

    public static void main(String[] args) {
        // The AK and SK used for authentication are hard-coded or stored in plaintext, which has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, AK and SK are stored in environment variables for authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String projectId = "{your projectId string}";
        String workspace = "{your workspace string}";
        String chName = "{your codeTable Chinese name string}";
        String enName = "{your codeTable English name string}";
        String description = "{your codeTable description string}";
        String tableVersion = "{your codeTable version string}";
        String directoryId = "{your directoryId string}";
        // the field sample of code table
        List<CodeTableFieldVO> codeTableFields = getCodeTableFieldVOSampleList();

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        Region region = new Region(regionId, endpoint);
        DataArtsStudioClient dataArtsStudioClient = DataArtsStudioClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        CreateCodeTableRequest request = new CreateCodeTableRequest();
        request.withBody(getCodeTableVO(chName, enName, description, codeTableFields, tableVersion, directoryId));
        request.withWorkspace(workspace);
        try {
            CreateCodeTableResponse response = dataArtsStudioClient.createCodeTable(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException e) {
            LOGGER.error("ConnectionException: " + e.getMessage());
        } catch (ServiceResponseException e) {
            LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
            LOGGER.error("RequestId: " + e.getRequestId());
            LOGGER.error("ErrorCode: " + e.getErrorCode());
            LOGGER.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static CodeTableVO getCodeTableVO(String chName, String enName, String desc, List<CodeTableFieldVO> codeTableFields,
                                              String tableVersion, String directoryId) {
        CodeTableVO codeTableVO = new CodeTableVO()
                .withNameEn(chName)
                .withNameCh(enName)
                .withDescription(desc).withCodeTableFields(codeTableFields)
                .withTbVersion(Integer.valueOf(tableVersion))
                .withDirectoryId(Long.valueOf(directoryId));
        return codeTableVO;
    }

    private static List<CodeTableFieldVO> getCodeTableFieldVOSampleList() {
        List<CodeTableFieldVO> codeTableFields = new ArrayList<>();
        CodeTableFieldVO codeTableFieldVO1 = new CodeTableFieldVO()
                .withOrdinal(1)
                .withNameCh("编码")
                .withNameEn("code")
                .withDataType("STRING")
                .withDescription("code field");
        CodeTableFieldVO codeTableFieldVO2 = new CodeTableFieldVO()
                .withOrdinal(2)
                .withNameCh("值")
                .withNameEn("value")
                .withDataType("STRING")
                .withDescription("value field");
        codeTableFields.add(codeTableFieldVO1);
        codeTableFields.add(codeTableFieldVO2);
        return codeTableFields;
    }
}
