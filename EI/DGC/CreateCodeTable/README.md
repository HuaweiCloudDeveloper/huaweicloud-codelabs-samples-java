### 示例简介
华为云提供了DataArtsStudios的SDK，您可以直接集成SDK来调用DataArtsStudios的相关API，从而实现对DataArtsStudios的快速操作。 该示例展示了如何通过java版SDK创建码表。

### 前置条件
1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。  
2、您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。  
3、获取对应地区的region，可在[API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/DataArtsStudio/doc?api=CreateCodeTable) 中Region下拉框中选择不同的地区进行查询。  
4、获取空间workspace和项目poriectId，可在数据架构空间列表信息中获取。  
5、华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。  
6、安装SDK
您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。 具体的SDK版本号请参见 SDK开发中心。

```xml
   <dependencies>
      <dependency>
         <groupId>com.huaweicloud.sdk</groupId>
         <artifactId>huaweicloud-sdk-dataartsstudio</artifactId>
         <version>3.1.45</version>
      </dependency>
   </dependencies>
 ```
### 开始使用

创建码表示例代码
```java
package com.huawei.clouds.dataarts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.dataartsstudio.v1.DataArtsStudioClient;
import com.huaweicloud.sdk.dataartsstudio.v1.model.CodeTableVO;
import com.huaweicloud.sdk.dataartsstudio.v1.model.CodeTableFieldVO;
import com.huaweicloud.sdk.dataartsstudio.v1.model.CreateCodeTableRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.CreateCodeTableResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CreateCodeTableDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateCodeTableDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String projectId = "{your projectId string}";
        String workspace = "{your workspace string}";
        String chName = "{your codeTable Chinese name string}";
        String enName = "{your codeTable English name string}";
        String description = "{your codeTable description string}";
        String tableVersion = "{your codeTable version string}";
        String directoryId = "{your directoryId string}";
        // the field sample of code table
        List<CodeTableFieldVO> codeTableFields = getCodeTableFieldVOSampleList();

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        Region region = new Region(regionId, endpoint);
        DataArtsStudioClient dataArtsStudioClient = DataArtsStudioClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        CreateCodeTableRequest request = new CreateCodeTableRequest();
        request.withBody(getCodeTableVO(chName, enName, description, codeTableFields, tableVersion, directoryId));
        request.withWorkspace(workspace);
        try {
            CreateCodeTableResponse response = dataArtsStudioClient.createCodeTable(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException e) {
            LOGGER.error("ConnectionException: " + e.getMessage());
        } catch (ServiceResponseException e) {
            LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
            LOGGER.error("RequestId: " + e.getRequestId());
            LOGGER.error("ErrorCode: " + e.getErrorCode());
            LOGGER.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static CodeTableVO getCodeTableVO(String chName, String enName, String desc, List<CodeTableFieldVO> codeTableFields,
                                              String tableVersion, String directoryId) {
        CodeTableVO codeTableVO = new CodeTableVO()
                .withNameEn(chName)
                .withNameCh(enName)
                .withDescription(desc).withCodeTableFields(codeTableFields)
                .withTbVersion(Integer.valueOf(tableVersion))
                .withDirectoryId(Long.valueOf(directoryId));
        return codeTableVO;
    }

    private static List<CodeTableFieldVO> getCodeTableFieldVOSampleList() {
        List<CodeTableFieldVO> codeTableFields = new ArrayList<>();
        CodeTableFieldVO codeTableFieldVO1 = new CodeTableFieldVO()
                .withOrdinal(1)
                .withNameCh("编码")
                .withNameEn("code")
                .withDataType("STRING")
                .withDescription("code field");
        CodeTableFieldVO codeTableFieldVO2 = new CodeTableFieldVO()
                .withOrdinal(2)
                .withNameCh("值")
                .withNameEn("value")
                .withDataType("STRING")
                .withDescription("value field");
        codeTableFields.add(codeTableFieldVO1);
        codeTableFields.add(codeTableFieldVO2);
        return codeTableFields;
    }
}
 ```

### 请求示例
```
{
  "name_en" : "demo",
  "name_ch" : "demo",
  "description" : "",
  "code_table_fields" : [ {
    "ordinal" : 1,
    "name_ch" : "编码",
    "name_en" : "code",
    "data_type" : "STRING",
    "description" : "",
    "code_table_field_values" : [ ]
  }, {
    "ordinal" : 2,
    "name_ch" : "值",
    "name_en" : "value",
    "data_type" : "STRING",
    "description" : "",
    "code_table_field_values" : [ ]
  } ],
  "tb_version" : 0,
  "directory_id" : "793889572823142400"
}
```


### 返回示例

返回data数据是 CodeTableVO

```
{
  "data" : {
    "value" : {
      "id" : "1014131824240230400",
      "name_en" : "test",
      "name_ch" : "test",
      "tb_version" : 0,
      "directory_id" : "1012307270173851648",
      "directory_path" : null,
      "description" : "",
      "create_by" : "abc",
      "status" : "DRAFT",
      "create_time" : "2022-08-30T11:17:48.557+08:00",
      "update_time" : "2022-08-30T11:17:48.557+08:00",
      "approval_info" : null,
      "new_biz" : null,
      "code_table_fields" : [ {
        "id" : null,
        "code_table_id" : null,
        "ordinal" : 1,
        "name_en" : "code",
        "name_ch" : "编码",
        "description" : "",
        "data_type" : "STRING",
        "domain_type" : null,
        "data_type_extend" : null,
        "is_unique_key" : false,
        "code_table_field_values" : [ ],
        "count_field_values" : null
      }, {
        "id" : null,
        "code_table_id" : null,
        "ordinal" : 2,
        "name_en" : "value",
        "name_ch" : "值",
        "description" : "",
        "data_type" : "STRING",
        "domain_type" : null,
        "data_type_extend" : null,
        "is_unique_key" : false,
        "code_table_field_values" : [ ],
        "count_field_values" : null
      } ]
    }
  }
}
```

### 接口及参数说明

参见：
[创建码表](https://support.huaweicloud.com/api-dataartsstudio/CreateCodeTable_0.html)

### 修订记录
   
| 发布日期        | 文档版本    | 修订说明    |
|-------------|-----|-----|
| 2023-12-12  |   1.0  |  文档首次发布   |
