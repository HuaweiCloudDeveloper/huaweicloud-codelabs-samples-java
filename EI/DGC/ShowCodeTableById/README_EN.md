### Introduction
This example shows how to search code table information based on code table id using the Java SDK.

### Preparations
1. Obtain the Huawei Cloud Development Kit (SDK). You can also view and install the JAVA SDK. 
2. You must have a HUAWEI CLOUD account and the corresponding Access Key (AK) and Secret Access Key (SK). On the HUAWEI CLOUD console, choose My Credential > Access Keys to create and view your AK/SK. For more information, see [the access key](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0001.html).
3. Obtain the region of the corresponding region,You can select different regions in the Region drop-down box in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DataArtsStudio/doc?api=ShowCodeTableById) to query.  
4. Obtain the space workspace and projectId, which can be obtained in the data architecture space list information. 
5. Data sources can be created through the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DataArtsStudio/doc?api=CreateCodeTable) interface.
6. HUAWEI CLOUD Java SDK supports Java JDK 1.8 and above.  
7. Install SDK. 
   You can obtain and install the SDK through Maven. You only need to add the corresponding dependencies to the pom.xml file of the Java project. For the specific SDK version number, please see the SDK Development Center.

```xml
   <dependencies>
      <dependency>
         <groupId>com.huaweicloud.sdk</groupId>
         <artifactId>huaweicloud-sdk-dataartsstudio</artifactId>
         <version>3.1.45</version>
      </dependency>
   </dependencies>
 ```
### Start To Operate

Sample Code
```java
package com.huawei.clouds.dataarts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.dataartsstudio.v1.DataArtsStudioClient;
import com.huaweicloud.sdk.dataartsstudio.v1.model.ShowCodeTableByIdRequest;
import com.huaweicloud.sdk.dataartsstudio.v1.model.ShowCodeTableByIdResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowCodeTableByIdDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShowCodeTableByIdDemo.class);

    public static void main(String[] args) {
        // The AK and SK used for authentication are hard-coded or stored in plaintext, which has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, AK and SK are stored in environment variables for authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String projectId = "{your projectId string}";
        String workspace = "{your workspace string}";
        String codeTableId = "{your codeTableId string}";

        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        Region region = new Region(regionId, endpoint);
        DataArtsStudioClient dataArtsStudioClient = DataArtsStudioClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        ShowCodeTableByIdRequest request = getShowCodeTableByIdRequest(codeTableId, workspace);
        try {
            ShowCodeTableByIdResponse response = dataArtsStudioClient.showCodeTableById(request);
            LOGGER.info("response = " + response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
            LOGGER.error("RequestId: " + e.getRequestId());
            LOGGER.error("ErrorCode: " + e.getErrorCode());
            LOGGER.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static ShowCodeTableByIdRequest getShowCodeTableByIdRequest(String codeTableId, String workspace) {
        ShowCodeTableByIdRequest showCodeTableByIdRequest = new ShowCodeTableByIdRequest();
        showCodeTableByIdRequest.withId(codeTableId);
        showCodeTableByIdRequest.withWorkspace(workspace);
        return showCodeTableByIdRequest;
    }
}
 ```

### Response Sample
```
{
  "data" : {
    "value" : {
      "id" : "1012307352952635392",
      "name_en" : "RY_000001",
      "name_ch" : "性别",
      "tb_version" : 0,
      "directory_id" : "1012307270173851648",
      "directory_path" : null,
      "description" : "",
      "create_by" : "abc",
      "status" : "PUBLISHED",
      "create_time" : "2022-08-25T10:28:01+08:00",
      "update_time" : "2022-08-25T10:31:08+08:00",
      "approval_info" : null,
      "new_biz" : null,
      "code_table_fields" : [ {
        "id" : "66929",
        "code_table_id" : "1012307352952635392",
        "ordinal" : 1,
        "name_en" : "code",
        "name_ch" : "编码",
        "description" : "",
        "data_type" : "STRING",
        "domain_type" : null,
        "data_type_extend" : null,
        "is_unique_key" : false,
        "code_table_field_values" : [ ],
        "count_field_values" : null
      }, {
        "id" : "66930",
        "code_table_id" : "1012307352952635392",
        "ordinal" : 2,
        "name_en" : "value",
        "name_ch" : "值",
        "description" : "",
        "data_type" : "STRING",
        "domain_type" : null,
        "data_type_extend" : null,
        "is_unique_key" : false,
        "code_table_field_values" : [ ],
        "count_field_values" : null
      } ]
    }
  }
}
```

### Change History
| Released On | Version |       Description       |
|:-----------:|:-------:|:-----------------------:|
| 2023-12-13  |   1.0   | Initial release |

