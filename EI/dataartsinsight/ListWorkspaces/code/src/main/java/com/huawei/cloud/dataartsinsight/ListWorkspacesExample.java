package com.huawei.cloud.dataartsinsight;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.dataartsinsight.v1.DataArtsInsightClient;
import com.huaweicloud.sdk.dataartsinsight.v1.model.ListWorkspacesRequest;
import com.huaweicloud.sdk.dataartsinsight.v1.model.ListWorkspacesResponse;
import com.huaweicloud.sdk.dataartsinsight.v1.region.DataArtsInsightRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ListWorkspacesExample {
    private static final Logger LOGGER = LoggerFactory.getLogger(ListWorkspacesExample.class);

    public static void main(String[] args) {
        // 基础认证信息：
        // ak: 华为云账号Access Key
        // sk: 华为云账号Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // projectId: 项目ID
        String projectId = "{******your project id******}";

        // 1.初始化sdk
        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        // 2.创建DataArtsInsightClient实例
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        List<String> endpoints = DataArtsInsightRegion.CN_NORTH_4.getEndpoints();
        DataArtsInsightClient client = DataArtsInsightClient.newBuilder()
            .withCredential(auth)
            .withHttpConfig(config)
            .withEndpoints(endpoints)
            .build();

        // 3.创建请求，添加参数
        ListWorkspacesRequest request = new ListWorkspacesRequest();
        request.setInstanceId("{******your instance id******}");
        request.setLimit(10);
        request.setOffset(0);

        // 4.发起请求，获取工作空间列表
        try {
            ListWorkspacesResponse response = client.listWorkspaces(request);
            LOGGER.info("list workspace success : {}", response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            LOGGER.error("list workspace fail.", e);
        } catch (ServiceResponseException e) {
            LOGGER.error(e.getErrorMsg(), e);
        }
    }
}