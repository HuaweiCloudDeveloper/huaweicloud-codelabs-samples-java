## 示例简介
智能数据洞察服务（DataArts Insight,简称DataArtsInsight）是新一代BI服务，提供可视、实时、易用、安全的企业智能分析数据服务，以最自然高效的方式获取业务见解，支撑业务实时高效决策。适配云上云下多种数据源，提供丰富多样的可视化组件，采用拖拽式自由布局，轻松实现数据分析和报表搭建，快速定制专属数据大屏。

华为云提供了DataArtsInsight服务端SDK，您可以直接集成服务端SDK来调用DataArtsInsight服务的相关API，从而实现对DataArtsInsight服务的快速操作。

工作空间是DataArtsInsight服务一个重要的概念，是租户创建的逻辑空间，用以数据源、数据集、仪表板、大屏等资源逻辑隔离。

该示例展示了如何通过java版SDK查询工作空间列表。

## 前置条件
1. 使用DataArtsInsight前需要先注册公有云帐户（详情参考：https://support.huaweicloud.com/qs-dataartsinsight/dataartsinsight_02_0003.html）。
2. 开通DataArtsInsight服务 （详情参考：https://support.huaweicloud.com/qs-dataartsinsight/dataartsinsight_02_0008.html）
3. 您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
4. 获取 projectId，projectId 用于资源隔离。获取方式请参考 [获取项目ID](https://support.huaweicloud.com/api-iam/iam_06_0001.html)。
5. 获取华为云开发工具包（SDK）。华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。

### SDK获取和安装
您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。 具体的SDK版本号请参见 SDK开发中心 。

```xml
<dependency>
   <groupId>com.huaweicloud.sdk</groupId>
   <artifactId>huaweicloud-sdk-dataartsinsight</artifactId>
   <version>3.1.72</version>
</dependency>
```

## 示例代码
```java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.dataartsinsight.v1.DataArtsInsightClient;
import com.huaweicloud.sdk.dataartsinsight.v1.model.ListWorkspacesRequest;
import com.huaweicloud.sdk.dataartsinsight.v1.model.ListWorkspacesResponse;
import com.huaweicloud.sdk.dataartsinsight.v1.region.DataArtsInsightRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

public class ListWorkspacesExample {
   private static final Logger LOGGER = LoggerFactory.getLogger(ListWorkspacesExample.class);

   public static void main(String[] args) {
      // 基础认证信息：
      // ak: 华为云账号Access Key
      // sk: 华为云账号Secret Access Key
      // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
      // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
      String ak = System.getenv("HUAWEICLOUD_SDK_AK");
      String sk = System.getenv("HUAWEICLOUD_SDK_SK");

      // projectId: 项目ID
      String projectId = "{******your project id******}";

      // 1.初始化sdk
      BasicCredentials auth = new BasicCredentials()
          .withAk(ak)
          .withSk(sk)
          .withProjectId(projectId);

      // 2.创建DataArtsInsightClient实例
      HttpConfig config = HttpConfig.getDefaultHttpConfig();
      config.withIgnoreSSLVerification(true);
      List<String> endpoints = DataArtsInsightRegion.CN_NORTH_4.getEndpoints();
      DataArtsInsightClient client = DataArtsInsightClient.newBuilder()
          .withCredential(auth)
          .withHttpConfig(config)
          .withEndpoints(endpoints)
          .build();

      // 3.创建请求，添加参数
      ListWorkspacesRequest request = new ListWorkspacesRequest();
      request.setInstanceId("{******your instance id******}");
      request.setLimit(10);
      request.setOffset(0);

      // 4.发起请求，获取工作空间列表
      try {
         ListWorkspacesResponse response = client.listWorkspaces(request);
         LOGGER.info("list workspace success : {}", response.toString());
      } catch (ConnectionException | RequestTimeoutException e) {
         LOGGER.error("list workspace fail.", e);
      } catch (ServiceResponseException e) {
         LOGGER.error(e.getErrorMsg(), e);
      }
   }
}
```

## 返回结果示例
```
{
    "count": 17,
    "page_data": [
        {
            "configs": {
                "mode": "0",
                "worksAuthorized": "0",
                "worksPublic": "1",
                "onlyAdminCreateDatasource": "0",
                "isPin": "1",
                "fieldShowType": "0",
                "worksView": "0"
            },
            "create_time": 1676984980510,
            "create_user": "xxxxxx",
            "description": "Description workspace information",
            "domain_id": "xxxxxxx",
            "eps_id": "xxxxxxxxxx",
            "id": "xxxxxxxxxxxxx",
            "instance_id": "xxxx7d17c41c414dabaa08f47c7dxxxx",
            "is_default": 1,
            "name": "Example workspace name",
            "owner_name": "xxxxxx",
            "project_id": "xxxxxxxb4dac4055888643b3xxxxxx",
            "update_time": 1687167926377,
            "update_user": "xxxxxxxx"
        }
    ]
}
```

## 状态码
| 状态码 | 描述 |
|:---:|:--:|
| 200 |  提交成功。  |
| 400 | 请求错误。   |
| 500 |  内部服务器错误。  |

## 参考
更多信息请参考[API Explorer](https://support.huaweicloud.com/api-dataartsinsight/ListWorkspaces.html)

## 修订记录
|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: |:-----------:|
| 2023-12-18 |   1.0    | 文档首次发布 |
