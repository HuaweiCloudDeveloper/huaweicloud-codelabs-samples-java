## 示例简介
数智融合计算服务(DataArtsFabric,简称Fabric)是华为云提供的数据+AI一站式开发平台，提供从数据处理、分析到模型微调、推理、部署上线的全生命周期管理能力，让数据工程师、数据科学家、AI应用开发工程师等多角色使用自己最熟悉的工具，在同一个工作台上工作，实现从开发到生产的高效协同。
华为云提供了DataArtsFabric服务端SDK，您可以直接集成服务端SDK来调用DataArtsFabric服务的相关API，从而实现对DataArtsFabric服务的快速操作。

工作空间(WorkSpace)是DataArtsFabric服务一个重要的概念，是租户创建的逻辑空间，用以数据源、数据集、仪表板、大屏等资源逻辑隔离。

该示例展示了如何通过java版SDK创建一个工作空间。

## 前置条件
1. 使用DataArtsFabric前需要先注册公有云帐户（详情参考：https://support.huaweicloud.com/qs-dataartsinsight/dataartsinsight_02_0003.html）。
2. 开通DataArtsFabric服务 （详情参考：https://support.huaweicloud.com/qs-dataartsinsight/dataartsinsight_02_0008.html）
3. 您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
4. 获取 projectId，projectId 用于资源隔离。获取方式请参考 [获取项目ID](https://support.huaweicloud.com/api-iam/iam_06_0001.html)。
5. 获取华为云开发工具包（SDK）。华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。

### SDK获取和安装
您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。 具体的SDK版本号请参见 SDK开发中心 。

```xml
<dependency>
   <groupId>com.huaweicloud.sdk</groupId>
   <artifactId>huaweicloud-sdk-dataartsfabric</artifactId>
   <version>3.1.119</version>
</dependency>
```

## 示例代码
```java
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.dataartsfabric.v1.*;
import com.huaweicloud.sdk.dataartsfabric.v1.model.*;


public class CreateWorkspaceSolution {

    public static void main(String[] args) {
        // The AK and SK used for authentication are hard-coded or stored in plaintext, which has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, AK and SK are stored in environment variables for authentication. Before running this example, set environment variables CLOUD_SDK_AK and CLOUD_SDK_SK in the local environment
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");
        String iamEndpoint = "https://iam.cn-north-7.myhuaweicloud.com";
        String endpoint = "https://fabric.cn-north-7.myhuaweicloud.com";

        ICredential auth = new BasicCredentials()
                .withIamEndpoint(iamEndpoint)
                .withAk(ak)
                .withSk(sk);

        DataArtsFabricClient client = DataArtsFabricClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region("cn-north-7", endpoint))
                .build();
        CreateWorkspaceRequest request = new CreateWorkspaceRequest();
        CreateWorkspaceRequestInput body = new CreateWorkspaceRequestInput();
        body.withName("your workspace name");
        request.withBody(body);
        try {
            CreateWorkspaceResponse response = client.createWorkspace(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}
```

## 返回结果示例
```
{
  "id": "e935d0ef-f4eb-4b95-aff1-9d33ae9f57a6",
  "name": "fabric",
  "description": "fabric",
  "create_time": "2023-05-30T12:24:30.401Z",
  "create_domain_name": "admin",
  "create_user_name": "user",
  "metastore_id": "2180518f-42b8-4947-b20b-adfc53981a25",
  "access_url": "https://:test.fabric.com/",
  "enterprise_project_id": "01049549-82cd-4b2b-9733-ddb94350c125"
}
```

## 状态码
| 状态码 | 描述 |
|:---:|:--:|
| 200 |  提交成功。  |
| 400 | 请求错误。   |
| 500 |  内部服务器错误。  |

## 参考
更多信息请参考[API Explorer](https://support.huaweicloud.com/api-dataartsinsight/ListWorkspaces.html)

## 修订记录
|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: |:-----------:|
| 2024-10-21 |   1.0    | 文档首次发布 |
