
## 功能介绍
数据接入服务（Data Ingestion Service,简称DIS）面向IoT、互联网等实时数据，提供高效采集、传输、分发能力，支持多种IoT协议，提供丰富的接口，帮助您快速构建实时数据应用。

华为云提供了DIS服务端SDK，您可以直接集成服务端SDK来调用DIS服务的相关API，从而实现对DIS服务的快速操作。

为了完善DIS服务通道的可视化,DIS推出了查询通道列表的接口

该示例展示了如何通过java版SDK查询通道列表。

## 前置条件
1. 获取华为云开发工具包（SDK）。
2. 要实现此示例，您需要拥有华为云账号以及该账号对应的 Access Key（AK）， Secret Access Key（SK），projectId，
   region和endpoint（详细信息请参考：https://support.huaweicloud.com/usermanual-dis/dis_01_0043.html ）。

### SDK获取和安装
本示例基于华为云SDK V3.0版本开发
您可以通过Maven配置所依赖的主机迁移服务SDK
```xml
<dependency>
    <groupId>com.huaweicloud.dis</groupId>
    <artifactId>huaweicloud-sdk-java-dis</artifactId>
    <version>1.3.5</version>
</dependency>
```

## 示例代码

```java
public static void main(String[] args) {
    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    // 创建DIS客户端实例
    DIS dic = DISClientBuilder.standard()
        .withEndpoint("your endpoint")
        .withAk(ak)
        .withSk(sk)
        .withProjectId("your projectId")
        .withRegion("your region")
        .withDefaultClientCertAuthEnabled(true)
        .build();
    // 创建请求
    ListStreamsRequest listStreamsRequest = new ListStreamsRequest();
    listStreamsRequest.setLimit(100);
    String startStreamName = null;
    ListStreamsResult listStreamsResult = null;
    List<String> streams = new ArrayList<>();
    try {
        do {
            listStreamsRequest.setExclusiveStartStreamName(startStreamName);
            // 发起请求
            listStreamsResult = dic.listStreams(listStreamsRequest);
            streams.addAll(listStreamsResult.getStreamNames());
            if (streams.size() > 0) {
                startStreamName = streams.get(streams.size() - 1);
            }
        } while (listStreamsResult.getHasMoreStreams());
        LOGGER.info("Success to list streams");
        for (int i = 1; i <= streams.size(); i++) {
            LOGGER.info("{}\t\t{}", i, streams.get(i - 1));
        }
    } catch (Exception e) {
        LOGGER.error("Failed to list streams", e);
    }
}
```

## 返回结果示例
```
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - Success to list streams
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 1		dis-1178
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 2		dis-13dt
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 3		dis-14dt
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 4		dis-0811
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 5		dis-081s
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 6		dis-hcjc
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 7		dis-cdsd
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 8		dis-scyd
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 9		dis-3ed5
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 10		dis-cds3
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 11		dis-1w3d
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 12		dis-9j8y
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 13		dis-5f4g
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 14		dis-4ds6
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 15		dis-9ik8
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 16		dis-ccms

```

## 参考
更多信息请参考[API Explorer](https://support.huaweicloud.com/api-dis/dis_02_0024.html)

## 修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: |:-----------:|
| 2023-08-17 |   1.0    | 文档首次发布 |
