
## Function
Data Ingestion Service (DIS) provides efficient collection, transmission, and distribution capabilities for real-time IoT and Internet data, supports multiple IoT protocols, and provides various types of APIs to help you quickly build real-time data applications.

You can integrate the DIS server software development kit (SDK) provided by Huawei Cloud to call DIS APIs and use DIS smoothly.

To improve the visualization of streams, DIS provides an API for querying the stream list.

This example describes how to query the stream list using a Java SDK.

## Prerequisites
1. You have obtained the Huawei Cloud SDK.
2. You have a Huawei Cloud account, its Access Key ID/Secret Access Key (AK/SK), project ID, region, and endpoint. (See https://support.huaweicloud.com/intl/en-us/qs-dis/dis_01_0043.html for details.)


### Obtaining and Installing an SDK
This example is developed based on Huawei Cloud SDK V3.0.
You can use Maven to configure the dependent DIS SDK.
```xml
<dependency>
    <groupId>com.huaweicloud.dis</groupId>
    <artifactId>huaweicloud-sdk-java-dis</artifactId>
    <version>1.3.5</version>
</dependency>
```

## Example Code

```java
public static void main(String[] args) {
    // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
    // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    // Create a DIS client instance.
    DIS dic = DISClientBuilder.standard()
        .withEndpoint("your endpoint")
        .withAk(ak)
        .withSk(sk)
        .withProjectId("your projectId")
        .withRegion("your region")
        .withDefaultClientCertAuthEnabled(true)
        .build();
    // Create a request.
    ListStreamsRequest listStreamsRequest = new ListStreamsRequest();
    listStreamsRequest.setLimit(100);
    String startStreamName = null;
    ListStreamsResult listStreamsResult = null;
    List<String> streams = new ArrayList<>();
    try {
        do {
            listStreamsRequest.setExclusiveStartStreamName(startStreamName);
            // Send the request.
            listStreamsResult = dic.listStreams(listStreamsRequest);
            streams.addAll(listStreamsResult.getStreamNames());
            if (streams.size() > 0) {
                startStreamName = streams.get(streams.size() - 1);
            }
        } while (listStreamsResult.getHasMoreStreams());
        LOGGER.info("Success to list streams");
        for (int i = 1; i <= streams.size(); i++) {
            LOGGER.info("{}\t\t{}", i, streams.get(i - 1));
        }
    } catch (Exception e) {
        LOGGER.error("Failed to list streams", e);
    }
}
```

## Example Returned Result
```
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - Success to list streams
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 1		dis-1178
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 2		dis-13dt
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 3		dis-14dt
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 4		dis-0811
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 5		dis-081s
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 6		dis-hcjc
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 7		dis-cdsd
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 8		dis-scyd
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 9		dis-3ed5
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 10		dis-cds3
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 11		dis-1w3d
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 12		dis-9j8y
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 13		dis-5f4g
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 14		dis-4ds6
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 15		dis-9ik8
15:15:58.789 [main] INFO com.bigdata.dis.sdk.demo.example.ListStreams - 16		dis-ccms

```

## Reference
For more information, see [Querying Streams](https://support.huaweicloud.com/intl/en-us/api-dis/dis_02_0024.html).

## Change History

|    Date    | Issue |   Description   |
|:----------:| :------: |:-----------:|
| 2023-12-11 |   1.0    | This issue is the first official release. |
