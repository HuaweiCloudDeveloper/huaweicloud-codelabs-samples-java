package com.bigdata.dis.sdk.demo.example;

import java.util.ArrayList;
import java.util.List;

import com.huaweicloud.dis.DISClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.huaweicloud.dis.DIS;
import com.huaweicloud.dis.iface.stream.request.ListStreamsRequest;
import com.huaweicloud.dis.iface.stream.response.ListStreamsResult;

/**
 * List Stream Example
 */
public class ListStreams {
    private static final Logger LOGGER = LoggerFactory.getLogger(ListStreams.class);

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Create a DIS client instance.
        DIS dic = DISClientBuilder.standard()
                .withEndpoint("your endpoint")
                .withAk(ak)
                .withSk(sk)
                .withProjectId("your projectId")
                .withRegion("your region")
                .withDefaultClientCertAuthEnabled(true)
                .build();
        // Create a request.
        ListStreamsRequest listStreamsRequest = new ListStreamsRequest();
        listStreamsRequest.setLimit(100);
        String startStreamName = null;
        ListStreamsResult listStreamsResult = null;
        List<String> streams = new ArrayList<>();

        try {
            do {
                listStreamsRequest.setExclusiveStartStreamName(startStreamName);
                // Send the request.
                listStreamsResult = dic.listStreams(listStreamsRequest);
                streams.addAll(listStreamsResult.getStreamNames());
                if (streams.size() > 0) {
                    startStreamName = streams.get(streams.size() - 1);
                }
            } while (listStreamsResult.getHasMoreStreams());

            LOGGER.info("Success to list streams");
            for (int i = 1; i <= streams.size(); i++) {
                LOGGER.info("{}\t\t{}", i, streams.get(i - 1));
            }
        } catch (Exception e) {
            LOGGER.error("Failed to list streams", e);
        }
    }
}
