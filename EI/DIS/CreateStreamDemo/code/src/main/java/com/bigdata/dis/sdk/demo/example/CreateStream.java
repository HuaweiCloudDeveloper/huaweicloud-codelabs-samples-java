package com.bigdata.dis.sdk.demo.example;

import com.huaweicloud.dis.DISClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.huaweicloud.dis.DIS;
import com.huaweicloud.dis.iface.data.request.StreamType;
import com.huaweicloud.dis.iface.stream.request.CreateStreamRequest;
import com.huaweicloud.dis.util.DataTypeEnum;

/**
 * Create Stream Example
 */
public class CreateStream {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateStream.class);
    
    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Create a DIS client instance.
        DIS dic = DISClientBuilder.standard()
                .withEndpoint("your endpoint")
                .withAk(ak)
                .withSk(sk)
                .withProjectId("your projectId")
                .withRegion("your region")
                .withDefaultClientCertAuthEnabled(true)
                .build();

        String streamName = "dis-2345";
        // Create a request.
        CreateStreamRequest createStreamRequest = new CreateStreamRequest();
        createStreamRequest.setStreamName(streamName);
        // COMMON indicates a common stream, and ADVANCED indicates an advanced stream.
        createStreamRequest.setStreamType(StreamType.COMMON.name());
        createStreamRequest.setDataType(DataTypeEnum.BLOB.name());
        // Set the number of partitions.
        createStreamRequest.setPartitionCount(1);
        // Set the data lifecycle.
        createStreamRequest.setDataDuration(24);
        try {
            dic.createStream(createStreamRequest);
            LOGGER.info("Success to create stream {}", streamName);
        } catch (Exception e) {
            LOGGER.error("Failed to create stream {}", streamName, e);
        }
    }
}
