
## 功能介绍
数据接入服务（Data Ingestion Service,简称DIS）面向IoT、互联网等实时数据，提供高效采集、传输、分发能力，支持多种IoT协议，提供丰富的接口，帮助您快速构建实时数据应用。

华为云提供了DIS服务端SDK，您可以直接集成服务端SDK来调用DIS服务的相关API，从而实现对DIS服务的快速操作。

通道是DIS服务一个重要的概念，是租户创建的逻辑单位，用以区分不同租户实时数据的集合。 在用户发送或者接收实时数据时，需要指定通道。

该示例展示了如何通过java版SDK创建通道。

## 前置条件
1. 获取华为云开发工具包（SDK）。
2. 要实现此示例，您需要拥有华为云账号以及该账号对应的 Access Key（AK）， Secret Access Key（SK），projectId，
   region和endpoint（详细信息请参考：https://support.huaweicloud.com/usermanual-dis/dis_01_0043.html ）。

### SDK获取和安装
本示例基于华为云SDK V3.0版本开发
您可以通过Maven配置所依赖的主机迁移服务SDK
```xml
<dependency>
    <groupId>com.huaweicloud.dis</groupId>
    <artifactId>huaweicloud-sdk-java-dis</artifactId>
    <version>1.3.5</version>
</dependency>
```

## 示例代码

```java
public static void main(String[] args) {
    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    // 创建DIS客户端实例
    DIS dic = DISClientBuilder.standard()
        .withEndpoint("your endpoint")
        .withAk(ak)
        .withSk(sk)
        .withProjectId("your projectId")
        .withRegion("your region")
        .withDefaultClientCertAuthEnabled(true)
        .build();

    String streamName = "dis-2345";
    // 创建请求
    CreateStreamRequest createStreamRequest = new CreateStreamRequest();
    createStreamRequest.setStreamName(streamName);
    // COMMON 普通通道; ADVANCED 高级通道
    createStreamRequest.setStreamType(StreamType.COMMON.name());
    createStreamRequest.setDataType(DataTypeEnum.BLOB.name());
    // 设置分区
    createStreamRequest.setPartitionCount(1);
    // 设置数据的生命周期
    createStreamRequest.setDataDuration(24);
    try {
        dic.createStream(createStreamRequest);
        LOGGER.info("Success to create stream {}", streamName);
    } catch (Exception e) {
        LOGGER.error("Failed to create stream {}", streamName, e);
    }
}
```

## 返回结果示例
```
19:55:13.044 [main] INFO com.bigdata.dis.sdk.demo.example.CreateStream - Success to create stream dis-2345

```

## 参考
更多信息请参考[API Explorer](https://support.huaweicloud.com/api-dis/dis_02_0016_01.html)

## 修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: |:-----------:|
| 2023-08-17 |   1.0    | 文档首次发布 |
