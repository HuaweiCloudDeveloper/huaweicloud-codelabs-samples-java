## Function
You can integrate the Data Ingestion Service (DIS) server software development kit (SDK) provided by Huawei Cloud to call DIS APIs and use DIS smoothly.

With DIS, you can create data streams for custom applications capable of processing or analyzing streaming data to transmit data from outside the cloud to the cloud.
An application of DIS is as follows: It collects vehicle traffic data in real time and caches the data in streams. The analytics platform periodically reads data from the streams and applies data analysis results to the dispatch system, which helps plan the opening hours of parking lots and allocate traffic resources.

This example describes how to use a Java SDK to download data from DIS.

## Prerequisites
1. You have registered an account with Huawei Cloud and enabled DIS.
2. You have created a DIS stream. (See https://support.huaweicloud.com/intl/en-us/qs-dis/dis_01_0601.html for details.)
3. You have obtained the Huawei Cloud SDK.
4. You have a Huawei Cloud account, its Access Key ID/Secret Access Key (AK/SK), project ID, region, and endpoint. (See https://support.huaweicloud.com/intl/en-us/usermanual-dis/dis_01_0043.html for details.)

### Obtaining and Installing an SDK
This example is developed based on Huawei Cloud SDK V3.0.
You can use Maven to configure the dependent DIS SDK.
```xml
<dependency>
    <groupId>com.huaweicloud.dis</groupId>
    <artifactId>huaweicloud-sdk-java-dis</artifactId>
    <version>1.3.5</version>
</dependency>
```

## Example Code


```java
private static void runConsumerDemo() {
    // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
    // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    // Create a DIS client instance.
    DIS dic = DISClientBuilder.standard()
        .withEndpoint("your endpoint")
        .withAk(ak)
        .withSk(sk)
        .withProjectId("your projectId")
        .withRegion("your region")
        .withDefaultClientCertAuthEnabled(true)
        .build();
    // Set the stream name.
    String streamName = "Practice";
    // Set the ID of the data download partition.
    String partitionId = "0";
    // Set the starting sequence number for data download.
    // String startingSequenceNumber = "0";
    // Set the data download mode.
    // AT_SEQUENCE_NUMBER: The download starts from the position specified by sequenceNumber, which is defined by GetPartitionCursorRequest.setStartingSequenceNumber.
    // AFTER_SEQUENCE_NUMBER: The download starts from the position after the position specified by sequenceNumber, which is defined by GetPartitionCursorRequest.setStartingSequenceNumber.
    // TRIM_HORIZON: The download starts from the earliest record.
    // LATEST: The download starts from the latest record.
    // AT_TIMESTAMP: The download starts from the 13-digit timestamp defined by GetPartitionCursorRequest.setTimestamp.
    String cursorType = PartitionCursorTypeEnum.LATEST.name();
    try {
        // Obtain the data cursor.
        GetPartitionCursorRequest request = new GetPartitionCursorRequest();
        request.setStreamName(streamName);
        request.setPartitionId(partitionId);
        request.setCursorType(cursorType);
        // request.setStartingSequenceNumber(startingSequenceNumber);
        GetPartitionCursorResult response = dic.getPartitionCursor(request);
        String cursor = response.getPartitionCursor();
        LOGGER.info("Get stream {}[partitionId={}] cursor success : {}", streamName, partitionId, cursor);
        GetRecordsRequest recordsRequest = new GetRecordsRequest();
        GetRecordsResult recordResponse = null;
        int count = 0;
        while (count < 10) {
            recordsRequest.setPartitionCursor(cursor);
            recordResponse = dic.getRecords(recordsRequest);
            // Obtain the next batch of data cursors.
            cursor = recordResponse.getNextPartitionCursor();
            for (Record record : recordResponse.getRecords()) {
                LOGGER.info("Get Record [{}], partitionKey [{}], sequenceNumber [{}].",
                    new String(record.getData().array()),
                    record.getPartitionKey(),
                    record.getSequenceNumber());
            }
            count ++ ;
        }
    } catch (DISClientException e) {
        LOGGER.error("Failed to get a normal response, please check params and retry. Error message [{}]",
            e.getMessage(),
            e);
    } catch (Exception e) {
        LOGGER.error(e.getMessage(), e);
    }
}
```

## Example Returned Result
```
09:09:21.211 [main] INFO example.ConsumerDemo - Get Record [hello world.], partitionKey [118125], sequenceNumber [30].
09:09:21.211 [main] INFO example.ConsumerDemo - Get Record [hello world.], partitionKey [997728], sequenceNumber [31].
09:09:21.211 [main] INFO example.ConsumerDemo - Get Record [hello world.], partitionKey [375762], sequenceNumber [32].
```

## Reference
For more information, see [Step 4: Obtaining Data from DIS](https://support.huaweicloud.com/intl/en-us/qs-dis/dis_01_0604.html).

## Change History

|    Date    | Issue |   Description   |
|:----------:| :------: |:-----------:|
| 2023-12-11 |   1.0    | This issue is the first official release. |
