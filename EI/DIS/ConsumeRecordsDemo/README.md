
## 功能介绍
华为云提供了DIS服务端SDK，您可以直接集成服务端SDK来调用DIS服务的相关API，从而实现对DIS服务的快速操作。

数据接入服务（DIS）为处理或分析流数据的自定义应用程序构建数据流管道，主要解决云服务外的数据实时传输到云服务内的问题
一个应用场景就是实时收集城市各交通枢纽的车辆通行数据，缓存在通道中，分析平台周期读取通道中的数据分析后将结果应用到调度系统，实现对停车场开放时长和交通资源的调配

该示例展示了如何通过java版SDK从DIS服务中下载数据。

## 前置条件
1. 使用DIS前需要注册公有云帐户，再开通DIS
2. 开通DIS通道 （详情参考：https://support.huaweicloud.com/qs-dis/dis_01_0601.html ）
3. 获取华为云开发工具包（SDK）。
4. 要实现此示例，您需要拥有华为云账号以及该账号对应的 Access Key（AK）， Secret Access Key（SK），projectId，
     region和endpoint（详细信息请参考：https://support.huaweicloud.com/usermanual-dis/dis_01_0043.html ）

### SDK获取和安装
本示例基于华为云SDK V3.0版本开发
您可以通过Maven配置所依赖的主机迁移服务SDK
```xml
<dependency>
    <groupId>com.huaweicloud.dis</groupId>
    <artifactId>huaweicloud-sdk-java-dis</artifactId>
    <version>1.3.5</version>
</dependency>
```

## 示例代码


```java
private static void runConsumerDemo() {
    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    // 创建DIS客户端实例
    DIS dic = DISClientBuilder.standard()
        .withEndpoint("your endpoint")
        .withAk(ak)
        .withSk(sk)
        .withProjectId("your projectId")
        .withRegion("your region")
        .withDefaultClientCertAuthEnabled(true)
        .build();
    // 配置流名称
    String streamName = "Practice";
    // 配置数据下载分区ID
    String partitionId = "0";
    // 配置下载数据序列号
    // String startingSequenceNumber = "0";
    // 配置下载数据方式
    // AT_SEQUENCE_NUMBER: 从指定的sequenceNumber开始获取，需要设置GetPartitionCursorRequest.setStartingSequenceNumber
    // AFTER_SEQUENCE_NUMBER: 从指定的sequenceNumber之后开始获取，需要设置GetPartitionCursorRequest.setStartingSequenceNumber
    // TRIM_HORIZON: 从最旧的记录开始获取
    // LATEST: 从最新的记录开始获取
    // AT_TIMESTAMP: 从指定的时间戳(13位)开始获取，需要设置GetPartitionCursorRequest.setTimestamp
    String cursorType = PartitionCursorTypeEnum.LATEST.name();
    try {
        // 获取数据游标
        GetPartitionCursorRequest request = new GetPartitionCursorRequest();
        request.setStreamName(streamName);
        request.setPartitionId(partitionId);
        request.setCursorType(cursorType);
        // request.setStartingSequenceNumber(startingSequenceNumber);
        GetPartitionCursorResult response = dic.getPartitionCursor(request);
        String cursor = response.getPartitionCursor();
        LOGGER.info("Get stream {}[partitionId={}] cursor success : {}", streamName, partitionId, cursor);
        GetRecordsRequest recordsRequest = new GetRecordsRequest();
        GetRecordsResult recordResponse = null;
        int count = 0;
        while (count < 10) {
            recordsRequest.setPartitionCursor(cursor);
            recordResponse = dic.getRecords(recordsRequest);
            // 下一批数据游标
            cursor = recordResponse.getNextPartitionCursor();
            for (Record record : recordResponse.getRecords()) {
                LOGGER.info("Get Record [{}], partitionKey [{}], sequenceNumber [{}].",
                    new String(record.getData().array()),
                    record.getPartitionKey(),
                    record.getSequenceNumber());
            }
            count ++ ;
        }
    } catch (DISClientException e) {
        LOGGER.error("Failed to get a normal response, please check params and retry. Error message [{}]",
            e.getMessage(),
            e);
    } catch (Exception e) {
        LOGGER.error(e.getMessage(), e);
    }
}
```

## 返回结果示例
```
09:09:21.211 [main] INFO example.ConsumerDemo - Get Record [hello world.], partitionKey [118125], sequenceNumber [30].
09:09:21.211 [main] INFO example.ConsumerDemo - Get Record [hello world.], partitionKey [997728], sequenceNumber [31].
09:09:21.211 [main] INFO example.ConsumerDemo - Get Record [hello world.], partitionKey [375762], sequenceNumber [32].
```

## 参考
更多信息请参考[API Explorer](https://support.huaweicloud.com/qs-dis/dis_01_0604.html)

## 修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: |:-----------:|
| 2023-08-17 |   1.0    | 文档首次发布 |
