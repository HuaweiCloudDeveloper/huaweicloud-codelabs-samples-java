package example;

import com.huaweicloud.dis.DIS;
import com.huaweicloud.dis.DISClientBuilder;
import com.huaweicloud.dis.exception.DISClientException;
import com.huaweicloud.dis.iface.data.request.GetPartitionCursorRequest;
import com.huaweicloud.dis.iface.data.request.GetRecordsRequest;
import com.huaweicloud.dis.iface.data.response.GetPartitionCursorResult;
import com.huaweicloud.dis.iface.data.response.GetRecordsResult;
import com.huaweicloud.dis.iface.data.response.Record;
import com.huaweicloud.dis.iface.stream.request.DeleteStreamRequest;
import com.huaweicloud.dis.util.PartitionCursorTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsumerDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerDemo.class);
    
    public static void main(String[] args) {
        runConsumerDemo();
    }
    
    private static void runConsumerDemo() {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Create a DIS client instance.
        DIS dic = DISClientBuilder.standard()
                .withEndpoint("your endpoint")
                .withAk(ak)
                .withSk(sk)
                .withProjectId("your projectId")
                .withRegion("your region")
                .withDefaultClientCertAuthEnabled(true)
                .build();

        // Set the stream name.
        String streamName = "Practice";

        // Set the ID of the data download partition.
        String partitionId = "0";

        // Set the starting sequence number for data download.
        // String startingSequenceNumber = "0";
        // Set the data download mode.
        // AT_SEQUENCE_NUMBER: The download starts from the position specified by sequenceNumber, which is defined by GetPartitionCursorRequest.setStartingSequenceNumber.
        // AFTER_SEQUENCE_NUMBER: The download starts from the position after the position specified by sequenceNumber, which is defined by GetPartitionCursorRequest.setStartingSequenceNumber.
        // TRIM_HORIZON: The download starts from the earliest record.
        // LATEST: The download starts from the latest record.
        // AT_TIMESTAMP: The download starts from the 13-digit timestamp defined by GetPartitionCursorRequest.setTimestamp.
        String cursorType = PartitionCursorTypeEnum.LATEST.name();

        try {
            // Obtain the data cursor.
            GetPartitionCursorRequest request = new GetPartitionCursorRequest();
            request.setStreamName(streamName);
            request.setPartitionId(partitionId);
            request.setCursorType(cursorType);
            // request.setStartingSequenceNumber(startingSequenceNumber);
            GetPartitionCursorResult response = dic.getPartitionCursor(request);
            String cursor = response.getPartitionCursor();

            LOGGER.info("Get stream {}[partitionId={}] cursor success : {}", streamName, partitionId, cursor);

            GetRecordsRequest recordsRequest = new GetRecordsRequest();
            GetRecordsResult recordResponse = null;
            int count = 0;
            while (count < 10) {
                recordsRequest.setPartitionCursor(cursor);
                recordResponse = dic.getRecords(recordsRequest);
                // Obtain the next batch of data cursors.
                cursor = recordResponse.getNextPartitionCursor();

                for (Record record : recordResponse.getRecords()) {
                    LOGGER.info("Get Record [{}], partitionKey [{}], sequenceNumber [{}].",
                            new String(record.getData().array()),
                            record.getPartitionKey(),
                            record.getSequenceNumber());
                }
                count ++ ;
            }
        } catch (DISClientException e) {
            LOGGER.error("Failed to get a normal response, please check params and retry. Error message [{}]",
                e.getMessage(),
                e);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
