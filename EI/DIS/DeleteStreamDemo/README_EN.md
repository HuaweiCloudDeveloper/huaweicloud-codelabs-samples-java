
## Function
Data Ingestion Service (DIS) provides efficient collection, transmission, and distribution capabilities for real-time IoT and Internet data, supports multiple IoT protocols, and provides various types of APIs to help you quickly build real-time data applications.

You can integrate the DIS server software development kit (SDK) provided by Huawei Cloud to call DIS APIs and use DIS smoothly.

A stream is a logical unit you can create in DIS to distinguish your real-time data from that of other tenants. To send or receive real-time data, you must specify a stream.

This example describes how to delete a stream using a Java SDK.

## Prerequisites
1. You have obtained the Huawei Cloud SDK.
2. You have a Huawei Cloud account, its Access Key ID/Secret Access Key (AK/SK), project ID, region, and endpoint. (See https://support.huaweicloud.com/intl/en-us/qs-dis/dis_01_0043.html for details.)


### Obtaining and Installing an SDK
This example is developed based on Huawei Cloud SDK V3.0.
You can use Maven to configure the dependent DIS SDK.
```xml
<dependency>
    <groupId>com.huaweicloud.dis</groupId>
    <artifactId>huaweicloud-sdk-java-dis</artifactId>
    <version>1.3.5</version>
</dependency>
```

## Example Code

```java
public static void main(String[] args) {
    // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
    // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    // Create a DIS client instance.
    DIS dic = DISClientBuilder.standard()
        .withEndpoint("your endpoint")
        .withAk(ak)
        .withSk(sk)
        .withProjectId("your projectId")
        .withRegion("your region")
        .withDefaultClientCertAuthEnabled(true)
        .build();
    // Specify the stream name.
    String streamName = "dis-2345";
    // Create a request.
    DeleteStreamRequest deleteStreamRequest = new DeleteStreamRequest();
    deleteStreamRequest.setStreamName(streamName);
    try {
        // Send the request.
        dic.deleteStream(deleteStreamRequest);
        LOGGER.info("Success to delete stream {}", streamName);
    } catch (Exception e) {
        LOGGER.error("Failed to delete stream {}", streamName, e);
    }
}
```

## Example Returned Result
```
20:02:43.104 [main] INFO com.bigdata.dis.sdk.demo.example.DeleteStream - Success to delete stream dis-2345

```

## Reference
For more information, see [Deleting Specified Streams](https://support.huaweicloud.com/intl/en-us/api-dis/DeleteStream.html).

## Change History

|    Date    | Issue |   Description   |
|:----------:| :------: |:-----------:|
| 2023-12-11 |   1.0    | This issue is the first official release. |
