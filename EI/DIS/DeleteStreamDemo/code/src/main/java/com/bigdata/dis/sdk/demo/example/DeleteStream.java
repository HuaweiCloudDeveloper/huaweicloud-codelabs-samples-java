package com.bigdata.dis.sdk.demo.example;

import com.huaweicloud.dis.DISClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.huaweicloud.dis.DIS;
import com.huaweicloud.dis.iface.stream.request.DeleteStreamRequest;

/**
 * Delete Stream Example
 */
public class DeleteStream {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteStream.class);
    
    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Create a DIS client instance.
        DIS dic = DISClientBuilder.standard()
                .withEndpoint("your endpoint")
                .withAk(ak)
                .withSk(sk)
                .withProjectId("your projectId")
                .withRegion("your region")
                .withDefaultClientCertAuthEnabled(true)
                .build();
        // Specify the stream name.
        String streamName = "dis-2345";
        // Create a request.
        DeleteStreamRequest deleteStreamRequest = new DeleteStreamRequest();
        deleteStreamRequest.setStreamName(streamName);
        try {
            // Send the request.
            dic.deleteStream(deleteStreamRequest);
            LOGGER.info("Success to delete stream {}", streamName);
        } catch (Exception e) {
            LOGGER.error("Failed to delete stream {}", streamName, e);
        }
    }
}
