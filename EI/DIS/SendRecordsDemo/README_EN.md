
## Function
You can integrate the Data Ingestion Service (DIS) server software development kit (SDK) provided by Huawei Cloud to call DIS APIs and use DIS smoothly.

With DIS, you can create data streams for custom applications capable of processing or analyzing streaming data to transmit data from outside the cloud to the cloud.
An application of DIS is as follows: It collects vehicle traffic data in real time and caches the data in streams. The analytics platform periodically reads data from the streams and applies data analysis results to the dispatch system, which helps plan the opening hours of parking lots and allocate traffic resources.

This example describes how to use a Java SDK to continuously upload local data to DIS through a DIS stream.

## Prerequisites
1. You have registered an account with Huawei Cloud and enabled DIS.
2. You have created a DIS stream. (See https://support.huaweicloud.com/intl/en-us/qs-dis/dis_01_0601.html for details.)
3. You have obtained the Huawei Cloud SDK.
4. You have a Huawei Cloud account, its Access Key ID/Secret Access Key (AK/SK), project ID, region, and endpoint. (See https://support.huaweicloud.com/intl/en-us/qs-dis/dis_01_0043.html for details.)


### Obtaining and Installing an SDK
This example is developed based on Huawei Cloud SDK V3.0.
You can use Maven to configure the dependent DIS SDK.
```xml
<dependency>
    <groupId>com.huaweicloud.dis</groupId>
    <artifactId>huaweicloud-sdk-java-dis</artifactId>
    <version>1.3.5</version>
</dependency>
```

## Example Code

```java
private static void runProduceDemo() {
    // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
    // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    // Create a DIS client instance.
    DIS dic = DISClientBuilder.standard()
        .withEndpoint("your endpoint")
        .withAk(ak)
        .withSk(sk)
        .withProjectId("your projectId")
        .withRegion("your region")
        .withDefaultClientCertAuthEnabled(true)
        .build();
    // Set the stream name.
    String streamName = "Practice";
    // Set the data to be uploaded.
    String message = "hello world.";
    PutRecordsRequest putRecordsRequest = new PutRecordsRequest();
    putRecordsRequest.setStreamName(streamName);
    // Create a request list.
    List<PutRecordsRequestEntry> putRecordsRequestEntryList = new ArrayList<PutRecordsRequestEntry>();
    ByteBuffer buffer = ByteBuffer.wrap(message.getBytes(StandardCharsets.UTF_8));
    // Add three pieces of data.
    for (int i = 0; i < 3; i++) {
        PutRecordsRequestEntry putRecordsRequestEntry = new PutRecordsRequestEntry();
        putRecordsRequestEntry.setData(buffer);
        putRecordsRequestEntry.setPartitionKey(String.valueOf(ThreadLocalRandom.current().nextInt(1000000)));
        putRecordsRequestEntryList.add(putRecordsRequestEntry);
    }
    putRecordsRequest.setRecords(putRecordsRequestEntryList);
    LOGGER.info("========== BEGIN PUT ============");
    PutRecordsResult putRecordsResult = null;
    try {
        // Send a request.
        putRecordsResult = dic.putRecords(putRecordsRequest);
    } catch (DISClientException e) {
        LOGGER.error("Failed to get a normal response, please check params and retry. Error message [{}]", e.getMessage(), e);
    } catch (Exception e) {
        LOGGER.error(e.getMessage(), e);
    }
    if (putRecordsResult != null) {
        LOGGER.info("Put {} records[{} successful / {} failed].",
            putRecordsResult.getRecords().size(),
            putRecordsResult.getRecords().size() - putRecordsResult.getFailedRecordCount().get(),
            putRecordsResult.getFailedRecordCount());
        for (int j = 0; j < putRecordsResult.getRecords().size(); j++) {
            PutRecordsResultEntry putRecordsRequestEntry = putRecordsResult.getRecords().get(j);
            if (!StringUtils.isNullOrEmpty(putRecordsRequestEntry.getErrorCode())) {
                // The data fails to be uploaded.
                LOGGER.error("[{}] put failed, errorCode [{}], errorMessage [{}]",
                    new String(putRecordsRequestEntryList.get(j).getData().array()),
                    putRecordsRequestEntry.getErrorCode(),
                    putRecordsRequestEntry.getErrorMessage());
            } else {
                // The data is uploaded successfully.
                LOGGER.info("[{}] put success, partitionId [{}], partitionKey [{}], sequenceNumber [{}]",
                    new String(putRecordsRequestEntryList.get(j).getData().array()),
                    putRecordsRequestEntry.getPartitionId(),
                    putRecordsRequestEntryList.get(j).getPartitionKey(),
                    putRecordsRequestEntry.getSequenceNumber());
            }
        }
    }
    LOGGER.info("========== END PUT ============");
}
```


## Example Returned Result
```
20:28:19.979 [main] INFO com.bigdata.dis.sdk.demo.example.ProducerDemo - Put 3 records[3 successful / 0 failed].
20:28:19.979 [main] INFO com.bigdata.dis.sdk.demo.example.ProducerDemo - [hello world.] put success, partitionId [shardId-0000000000], partitionKey [636365], sequenceNumber [6]
20:28:19.979 [main] INFO com.bigdata.dis.sdk.demo.example.ProducerDemo - [hello world.] put success, partitionId [shardId-0000000000], partitionKey [166462], sequenceNumber [7]
20:28:19.979 [main] INFO com.bigdata.dis.sdk.demo.example.ProducerDemo - [hello world.] put success, partitionId [shardId-0000000000], partitionKey [32440], sequenceNumber [8]
20:28:19.979 [main] INFO com.bigdata.dis.sdk.demo.example.ProducerDemo - ========== END PUT ============

```

## Reference
For more information, see [Step 3: Sending Data to DIS](https://support.huaweicloud.com/intl/en-us/qs-dis/dis_01_0603.html).

## Change History

|    Date    | Issue |   Description   |
|:----------:| :------: |:-----------:|
| 2023-12-11 |   1.0    | This issue is the first official release. |
