package com.bigdata.dis.sdk.demo.example;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.huaweicloud.dis.DIS;
import com.huaweicloud.dis.DISClientBuilder;
import com.huaweicloud.dis.core.util.StringUtils;
import com.huaweicloud.dis.exception.DISClientException;
import com.huaweicloud.dis.iface.data.request.PutRecordsRequest;
import com.huaweicloud.dis.iface.data.request.PutRecordsRequestEntry;
import com.huaweicloud.dis.iface.data.response.PutRecordsResult;
import com.huaweicloud.dis.iface.data.response.PutRecordsResultEntry;

public class ProducerDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProducerDemo.class);
    
    public static void main(String[] args) {
        runProduceDemo();
    }
    
    private static void runProduceDemo() {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Create a DIS client instance.
        DIS dic = DISClientBuilder.standard()
                .withEndpoint("your endpoint")
                .withAk(ak)
                .withSk(sk)
                .withProjectId("your projectId")
                .withRegion("your region")
                .withDefaultClientCertAuthEnabled(true)
                .build();

        // Set the stream name.
        String streamName = "Practice";

        // Set the data to be uploaded.
        String message = "hello world.";


        PutRecordsRequest putRecordsRequest = new PutRecordsRequest();
        putRecordsRequest.setStreamName(streamName);
        // Create a request list.
        List<PutRecordsRequestEntry> putRecordsRequestEntryList = new ArrayList<PutRecordsRequestEntry>();
        ByteBuffer buffer = ByteBuffer.wrap(message.getBytes(StandardCharsets.UTF_8));
        // Add three pieces of data.
        for (int i = 0; i < 3; i++) {
            PutRecordsRequestEntry putRecordsRequestEntry = new PutRecordsRequestEntry();
            putRecordsRequestEntry.setData(buffer);
            putRecordsRequestEntry.setPartitionKey(String.valueOf(ThreadLocalRandom.current().nextInt(1000000)));
            putRecordsRequestEntryList.add(putRecordsRequestEntry);
        }
        putRecordsRequest.setRecords(putRecordsRequestEntryList);

        LOGGER.info("========== BEGIN PUT ============");

        PutRecordsResult putRecordsResult = null;
        try {
            // Send a request.
            putRecordsResult = dic.putRecords(putRecordsRequest);
        } catch (DISClientException e) {
            LOGGER.error("Failed to get a normal response, please check params and retry. Error message [{}]", e.getMessage(), e);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        if (putRecordsResult != null) {
            LOGGER.info("Put {} records[{} successful / {} failed].",
                    putRecordsResult.getRecords().size(),
                    putRecordsResult.getRecords().size() - putRecordsResult.getFailedRecordCount().get(),
                    putRecordsResult.getFailedRecordCount());

            for (int j = 0; j < putRecordsResult.getRecords().size(); j++) {
                PutRecordsResultEntry putRecordsRequestEntry = putRecordsResult.getRecords().get(j);
                if (!StringUtils.isNullOrEmpty(putRecordsRequestEntry.getErrorCode())) {
                    // The data fails to be uploaded.
                    LOGGER.error("[{}] put failed, errorCode [{}], errorMessage [{}]",
                            new String(putRecordsRequestEntryList.get(j).getData().array()),
                            putRecordsRequestEntry.getErrorCode(),
                            putRecordsRequestEntry.getErrorMessage());
                } else {
                    // The data is uploaded successfully.
                    LOGGER.info("[{}] put success, partitionId [{}], partitionKey [{}], sequenceNumber [{}]",
                            new String(putRecordsRequestEntryList.get(j).getData().array()),
                            putRecordsRequestEntry.getPartitionId(),
                            putRecordsRequestEntryList.get(j).getPartitionKey(),
                            putRecordsRequestEntry.getSequenceNumber());
                }
            }
        }
        LOGGER.info("========== END PUT ============");
    }
    
}
