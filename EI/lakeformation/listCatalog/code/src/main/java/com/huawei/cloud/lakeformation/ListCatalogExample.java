package com.huawei.cloud.lakeformation;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.lakeformation.v1.LakeFormationClient;
import com.huaweicloud.sdk.lakeformation.v1.model.CatalogInput;
import com.huaweicloud.sdk.lakeformation.v1.model.CreateCatalogRequest;
import com.huaweicloud.sdk.lakeformation.v1.model.ListCatalogsRequest;
import com.huaweicloud.sdk.lakeformation.v1.model.ListCatalogsResponse;

import java.util.ArrayList;
import java.util.List;

public class ListCatalogExample {
    public static void main(String[] args) {
        // 基础认证信息：
        // ak: 华为云账号Access Key
        // sk: 华为云账号Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // projectId: 项目ID
        String projectId = "{******your project id******}";

        // 1.初始化sdk
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        List<String> endpoints = new ArrayList<>();
        endpoints.add("lakeformation.lakecat.com");
        BasicCredentials basicCredentials = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);

        // 2.创建LakeFormationClient实例
        LakeFormationClient client = LakeFormationClient.newBuilder()
            .withHttpConfig(config)
            .withCredential(basicCredentials)
            .withEndpoints(endpoints)
            .build();

        // 3.创建请求，添加参数
        CatalogInput catalogInput = new CatalogInput();
        catalogInput.setCatalogName("{******catalog name******}");
        catalogInput.setDescription("{******catalog description******}");
        catalogInput.setLocation("{******catalog location******}");
        CreateCatalogRequest createCatalogRequest =
            new CreateCatalogRequest().withInstanceId("{******your instance id******}").withBody(catalogInput);

        // 4.创建catalog
        client.createCatalog(createCatalogRequest);

        ListCatalogsRequest listCatalogsRequest =
            new ListCatalogsRequest().withInstanceId("{******your instance id******}");

        // 5.查询catalogs列表
        try {
            ListCatalogsResponse response = client.listCatalogs(listCatalogsRequest);
            System.out.println(response.getHttpStatusCode());
            System.out.println(response);
        } catch (ClientRequestException | ServerResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getMessage());
        }
    }
}
