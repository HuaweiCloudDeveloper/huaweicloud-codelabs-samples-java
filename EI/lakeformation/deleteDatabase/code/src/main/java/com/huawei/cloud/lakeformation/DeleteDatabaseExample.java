package com.huawei.cloud.lakeformation;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.lakeformation.v1.LakeFormationClient;
import com.huaweicloud.sdk.lakeformation.v1.model.CreateDatabaseRequest;
import com.huaweicloud.sdk.lakeformation.v1.model.DatabaseInput;
import com.huaweicloud.sdk.lakeformation.v1.model.DeleteDatabaseRequest;
import com.huaweicloud.sdk.lakeformation.v1.model.DeleteDatabaseResponse;

import java.util.ArrayList;
import java.util.List;

public class DeleteDatabaseExample {
    public static void main(String[] args) {
        // 基础认证信息：
        // ak: 华为云账号Access Key
        // sk: 华为云账号Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // projectId: 项目ID
        String projectId = "{******your project id******}";

        // 1.初始化sdk
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        List<String> endpoints = new ArrayList<>();
        endpoints.add("lakeformation.lakecat.com");
        BasicCredentials basicCredentials = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);

        // 2.创建LakeFormationClient实例
        LakeFormationClient client = LakeFormationClient.newBuilder()
            .withHttpConfig(config)
            .withCredential(basicCredentials)
            .withEndpoints(endpoints)
            .build();

        // 3. 创建数据库
        DatabaseInput databaseInput = new DatabaseInput();
        databaseInput.setDatabaseName("{******database name******}");
        databaseInput.setDescription("{******database desc******}");
        databaseInput.setOwner("{******database owner******}");
        databaseInput.setLocation("{******database location******}");
        CreateDatabaseRequest createDatabaseRequest = new CreateDatabaseRequest()
            .withInstanceId("{******your instance id******}")
            .withCatalogName("{******catalog name******}")
            .withBody(databaseInput);

        client.createDatabase(createDatabaseRequest);

        // 4.创建请求，添加参数
        DeleteDatabaseRequest deleteDatabaseRequest =
            new DeleteDatabaseRequest().withCatalogName("{******catalog name******}")
                .withDatabaseName("{******database name******}").withInstanceId("{******your instance id******}");

        // 5.删除database
        try {
            DeleteDatabaseResponse response = client.deleteDatabase(deleteDatabaseRequest);
            System.out.println(response.getHttpStatusCode());
            System.out.println(response);
        } catch (ClientRequestException | ServerResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getMessage());
        }
    }
}
