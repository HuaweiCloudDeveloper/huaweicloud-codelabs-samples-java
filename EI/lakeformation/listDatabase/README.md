## 示例简介
华为云提供了LakeFormation服务端SDK，您可以直接集成服务端SDK来调用LakeFormation服务的相关API，从而实现对LakeFormation服务的快速操作。

LakeFormation是企业级数据湖一站式构建服务，提供数据湖元数据统一管理的可视化界面及API，兼容Hive元数据模型以及Ranger权限模型，
支持对接MapReduce服务（MRS）、数据仓库服务 GaussDB（DWS）、数据治理中心 DataArts Studio、数据湖探索（DLI）等多种计算引擎及大数据云服务，
使用户可以便捷高效地构建数据湖和运营相关业务，加速释放业务数据价值。

该示例展示了如何通过java版SDK从LakeFormation服务中列举Database元数据。

## 前置条件
1. 使用LakeFormation前需要注册公有云帐户，再开通LakeFormation
2. 开通LakeFormation通道 （详情参考：https://support.huaweicloud.com/qs-lakeformation/lakeformation_02_0002.html ）
3. 获取华为云开发工具包（SDK）。
4. 要实现此示例，您需要拥有华为云账号以及该账号对应的
   1) Access Key（AK）， Secret Access Key（SK）（详情参考：https://support.huaweicloud.com/devg-apisign/api-sign-provide-aksk.html）
   2) projectId（详情参考：https://support.huaweicloud.com/api-lakeformation/lakeformation_04_0026.html）
   3) instanceId（详情参考：https://support.huaweicloud.com/lakeformation_faq/lakeformation_05_0007.html）

### SDK获取和安装
本示例基于华为云SDK V3.0版本开发
您可以通过Maven配置所依赖的主机迁移服务SDK
```xml
<dependency>
   <groupId>com.huaweicloud.sdk</groupId>
   <artifactId>huaweicloud-sdk-lakeformation</artifactId>
   <version>3.1.45</version>
</dependency>
```

## 示例代码
```java
    public static void listDatabases() {
        // 基础认证信息：
        // ak: 华为云账号Access Key
        // sk: 华为云账号Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // projectId: 项目ID
        String projectId = "{******your project id******}";

        // 1.初始化sdk
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        List<String> endpoints = new ArrayList<>();
        endpoints.add("lakeformation.lakecat.com");
        BasicCredentials basicCredentials = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);

        // 2.创建LakeFormationClient实例
        LakeFormationClient client = LakeFormationClient.newBuilder()
            .withHttpConfig(config)
            .withCredential(basicCredentials)
            .withEndpoints(endpoints)
            .build();

        // 3. 创建数据库
        DatabaseInput databaseBody = new DatabaseInput();
        databaseBody.setDatabaseName("{******database name******}");
        databaseBody.setDescription("{******database desc******}");
        databaseBody.setOwner("{******database owner******}");
        databaseBody.setLocation("{******database location******}");
        CreateDatabaseRequest createDatabaseRequest = new CreateDatabaseRequest()
            .withInstanceId("{******your instance id******}")
            .withCatalogName("{******catalog name******}")
            .withBody(databaseBody);

        client.createDatabase(createDatabaseRequest);

        // 4.创建请求，添加参数
        ListDatabasesRequest listDatabasesRequest =
            new ListDatabasesRequest().withCatalogName("{******catalog name******}").withInstanceId("{******your instance id******}");

        // 5.查询databases列表
        try {
            ListDatabasesResponse response = client.listDatabases(listDatabasesRequest);
            System.out.println(response.getHttpStatusCode());
            System.out.println(response);
        } catch (ClientRequestException | ServerResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getMessage());
        }
    }
```

## 返回结果示例
```
200
class ListDatabasesResponse {
    databases: [class Database {
        catalogName: catalog_demo
        databaseName: database_demo
        owner: public
        ownerType: USER
        description: 
        location: obs://bukect/demo_1/
        parameters: {}
        tableLocationList: null
        functionLocationList: null
    }]
    pageInfo: class PagedInfo {
        currentCount: 3
        nextMarker: null
        previousMarker: null
    }
}
```

## 参考
更多信息请参考[API Explorer](https://support.huaweicloud.com/api-lakeformation/ListDatabases.html)

## 修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: |:-----------:|
| 2023-09-01 |   1.0    | 文档首次发布 |