## 1. 示例简介
华为云提供了盘古大模型（PanguLargeModels）的SDK，您可以直接集成SDK来调用PanguLargeModels的相关API，从而实现对PanguLargeModels的快速操作。该示例展示了如何通过java版SDK进行通用文本对话。
## 2. 前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 访问密钥 。
- 3、获取对应地区的region，可在[API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/PanguLargeModels/doc?api=ExecuteTextCompletion) 中Region下拉框中选择不同的地区进行查询。
- 4、华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。
## 3.安装SDK
您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。 具体的SDK版本号请参见 SDK开发中心 。
```xml
<dependencies>
   <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-pangulargemodels</artifactId>
        <version>3.1.60</version>
   </dependency>
</dependencies>
```
## 4.开始使用
- 通用文本PanguText提供单轮文本能力，常用于文本生成、文本摘要、闭卷问答等任务。

```java
package com.huawei.cloud.pangulargemodels;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.pangulargemodels.v1.PanguLargeModelsClient;
import com.huaweicloud.sdk.pangulargemodels.v1.model.ExecuteTextCompletionRequest;
import com.huaweicloud.sdk.pangulargemodels.v1.model.ExecuteTextCompletionResponse;
import com.huaweicloud.sdk.pangulargemodels.v1.model.TextCompletionReq;

public class ExecuteTextCompletionExample {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionid = "<YOUR REGION_ID>";
        String endpoint = "<PANGU ENDPOINT>";
        String projectId = "<YOUR PROJECT_ID>";
        String deploymentId = "<YOUR DEPLOYMENT_ID>";

        // 1.构造认证对象，初始化配置
        BasicCredentials auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
        HttpConfig httpConfig = new HttpConfig();
        httpConfig.setIgnoreSSLVerification(true);

        // 2.创建PanguLargeModelsClient
        PanguLargeModelsClient client = PanguLargeModelsClient.newBuilder()
            .withCredential(auth)
            .withHttpConfig(httpConfig)
            .withRegion(new Region(regionid, endpoint))
            .build();

        // 3.构造请求体
        ExecuteTextCompletionRequest request = buildTextCompletionRequest(deploymentId);

        // 4.请求获取响应
        try {
            ExecuteTextCompletionResponse response = client.executeTextCompletion(request);
            System.out.println(response);
        } catch (ConnectionException e) {
            System.out.println(e.getMessage());
        } catch (RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getMessage());
        }
    }

    private static ExecuteTextCompletionRequest buildTextCompletionRequest(String deploymentId) {
        TextCompletionReq textCompletionReq = new TextCompletionReq();
        textCompletionReq.withMaxTokens(50).withN(1).withPrompt("hello");
        ExecuteTextCompletionRequest request = new ExecuteTextCompletionRequest();
        request.setDeploymentId(deploymentId);
        request.withBody(textCompletionReq);
        return request;
    }
}
```

## 5.返回示例
```json
{
  "id": "bf470a6d-70f2-44e5-9da1-23f66064e4f2",
  "created": 1696759072,
  "choices": [
    {
      "index": 0,
      "text": "Hello! How can I assist you today?"
    }
  ],
  "usage": {
    "completion_tokens": 9,
    "prompt_tokens": 3,
    "total_tokens": 12
  }
}
```
## 6.接口及参数说明
参见：[通用文本](https://support.huaweicloud.com/api-pangulm/pangulm_06_0011.html)
## 7. 参考
更多示例信息请参考[PanguLargeModels](https://support.huaweicloud.com/api-pangulm/pangulm_06_0002.html)
## 8. 修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2023-10-12 |   1.0    | 文档首次发布 |