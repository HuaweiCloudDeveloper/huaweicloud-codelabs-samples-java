package com.huawei.cloud.pangulargemodels;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.pangulargemodels.v1.PanguLargeModelsClient;
import com.huaweicloud.sdk.pangulargemodels.v1.model.ChatCompletionReq;
import com.huaweicloud.sdk.pangulargemodels.v1.model.ExecuteChatCompletionRequest;
import com.huaweicloud.sdk.pangulargemodels.v1.model.ExecuteChatCompletionResponse;
import com.huaweicloud.sdk.pangulargemodels.v1.model.Message;

import java.util.ArrayList;
import java.util.List;

public class ExecuteChatCompletionExample {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionid = "<YOUR REGION_ID>";
        String endpoint = "<PANGU ENDPOINT>";
        String projectId = "<YOUR PROJECT_ID>";
        String deploymentId = "<YOUR DEPLOYMENT_ID>";

        // 1.构造认证对象，初始化配置
        BasicCredentials auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
        HttpConfig httpConfig = new HttpConfig();
        httpConfig.setIgnoreSSLVerification(true);

        // 2.创建PanguLargeModelsClient
        PanguLargeModelsClient client = PanguLargeModelsClient.newBuilder()
            .withCredential(auth)
            .withHttpConfig(httpConfig)
            .withRegion(new Region(regionid, endpoint))
            .build();

        // 3.构造请求体
        ExecuteChatCompletionRequest request = buildChatCompletionRequest(deploymentId);

        // 4.请求获取响应
        try {
            ExecuteChatCompletionResponse response = client.executeChatCompletion(request);
            System.out.println(response);
        } catch (ConnectionException e) {
            System.out.println(e.getMessage());
        } catch (RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getMessage());
        }
    }

    private static ExecuteChatCompletionRequest buildChatCompletionRequest(String deploymentId) {
        ChatCompletionReq completionReq = new ChatCompletionReq();
        List<Message> messages = new ArrayList<>();
        Message msg1 = new Message();
        msg1.withContent("hello");
        messages.add(msg1);
        Message msg2 = new Message();
        msg2.withContent("Hello! How can I assist you today?");
        messages.add(msg2);
        Message msg3 = new Message();
        msg3.withContent("what's your name?");
        messages.add(msg3);
        completionReq.setMessages(messages);
        completionReq.setMaxTokens(50);
        completionReq.setN(1);
        ExecuteChatCompletionRequest request = new ExecuteChatCompletionRequest();
        request.setDeploymentId(deploymentId);
        request.withBody(completionReq);
        return request;
    }
}