## 1. 示例简介
华为云提供了盘古大模型（PanguLargeModels）的SDK，您可以直接集成SDK来调用PanguLargeModels的相关API，从而实现对PanguLargeModels的快速操作。该示例展示了如何通过java版SDK进行对话问答。
## 2. 前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 访问密钥 。
- 3、获取对应地区的region，可在[API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/PanguLargeModels/doc?api=ExecuteChatCompletion) 中Region下拉框中选择不同的地区进行查询。
- 4、华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。
-
## 3.安装SDK
您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。 具体的SDK版本号请参见 SDK开发中心 。
```xml
<dependencies>
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-pangulargemodels</artifactId>
        <version>3.1.60</version>
    </dependency>
</dependencies>
```
## 4.开始使用
- 对话问答PanguChat提供多轮文本能力，常用于对话问答、聊天任务。

```java
package com.huawei.cloud.pangulargemodels;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.pangulargemodels.v1.PanguLargeModelsClient;
import com.huaweicloud.sdk.pangulargemodels.v1.model.ChatCompletionReq;
import com.huaweicloud.sdk.pangulargemodels.v1.model.ExecuteChatCompletionRequest;
import com.huaweicloud.sdk.pangulargemodels.v1.model.ExecuteChatCompletionResponse;
import com.huaweicloud.sdk.pangulargemodels.v1.model.Message;

import java.util.ArrayList;
import java.util.List;

public class ExecuteChatCompletionExample {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionid = "<YOUR REGION_ID>";
        String endpoint = "<PANGU ENDPOINT>";
        String projectId = "<YOUR PROJECT_ID>";
        String deploymentId = "<YOUR DEPLOYMENT_ID>";

        // 1.构造认证对象，初始化配置
        BasicCredentials auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
        HttpConfig httpConfig = new HttpConfig();
        httpConfig.setIgnoreSSLVerification(true);

        // 2.创建PanguLargeModelsClient
        PanguLargeModelsClient client = PanguLargeModelsClient.newBuilder()
            .withCredential(auth)
            .withHttpConfig(httpConfig)
            .withRegion(new Region(regionid, endpoint))
            .build();

        // 3.构造请求体
        ExecuteChatCompletionRequest request = buildChatCompletionRequest(deploymentId);

        // 4.请求获取响应
        try {
            ExecuteChatCompletionResponse response = client.executeChatCompletion(request);
            System.out.println(response);
        } catch (ConnectionException e) {
            System.out.println(e.getMessage());
        } catch (RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getMessage());
        }
    }

    private static ExecuteChatCompletionRequest buildChatCompletionRequest(String deploymentId) {
        ChatCompletionReq completionReq = new ChatCompletionReq();
        List<Message> messages = new ArrayList<>();
        Message msg1 = new Message();
        msg1.withContent("hello");
        messages.add(msg1);
        Message msg2 = new Message();
        msg2.withContent("Hello! How can I assist you today?");
        messages.add(msg2);
        Message msg3 = new Message();
        msg3.withContent("what's your name?");
        messages.add(msg3);
        completionReq.setMessages(messages);
        completionReq.setMaxTokens(50);
        completionReq.setN(1);
        ExecuteChatCompletionRequest request = new ExecuteChatCompletionRequest();
        request.setDeploymentId(deploymentId);
        request.withBody(completionReq);
        return request;
    }
}
```

## 5.返回示例
```json
{
  "id": "8efcd203-4cd9-4452-923f-00439205c6cb",
  "created": 1696758816,
  "choices": [
    {
      "index": 0,
      "message": {
        "role": null,
        "content": "My name is AI Assistant. How can I help you today?"
      }
    }
  ],
  "usage": {
    "completion_tokens": 9,
    "prompt_tokens": 3,
    "total_tokens": 12
  }
}
```
## 6.接口及参数说明
参见：[对话问答](https://support.huaweicloud.com/api-pangulm/pangulm_06_0012.html)
## 7. 参考
更多示例信息请参考[PanguLargeModels](https://support.huaweicloud.com/api-pangulm/pangulm_06_0002.html)
## 8. 修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2023-10-12 |   1.0    | 文档首次发布 |