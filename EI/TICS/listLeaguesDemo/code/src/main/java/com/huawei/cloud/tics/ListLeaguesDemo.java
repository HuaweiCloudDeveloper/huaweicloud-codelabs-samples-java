package com.huawei.cloud.tics;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.tics.v1.TicsClient;
import com.huaweicloud.sdk.tics.v1.model.ListLeaguesRequest;
import com.huaweicloud.sdk.tics.v1.model.ListLeaguesResponse;
import com.huaweicloud.sdk.tics.v1.region.TicsRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListLeaguesDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(ListLeaguesDemo.class);

    public static void main(String[] args) {
        // 基础认证信息：
        // ak: 华为云账号Access Key
        // sk: 华为云账号Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{******your project id******}";

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        TicsClient client = TicsClient.newBuilder()
                .withCredential(auth)
                .withRegion(TicsRegion.CN_NORTH_4)
                .build();

        // 获取联盟列表
        listLeagues(client);
    }

    private static void listLeagues(TicsClient client) {
        ListLeaguesRequest request = new ListLeaguesRequest();
        // 每页记录数
        request.withLimit(10);
        // 记录数偏移量
        request.withOffset(5);
        // 查询的联盟类型，owned:创建者，participativenormal:参与者
        request.withType(ListLeaguesRequest.TypeEnum.OWNED);

        try {
            ListLeaguesResponse response = client.listLeagues(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(e.toString());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}
