## 1. 简介
可信智能计算服务TICS（ Trusted Intelligence Computing Service ）打破数据孤岛，在数据隐私保护的前提下，实现行业内部、各行业间的多方数据联合分析和联邦计算。TICS基于安全多方计算MPC、区块链等技术，实现了数据在存储、流通、计算过程中端到端的安全和可审计，推动了跨行业的可信数据融合和协同。

本示例展示如何通过java版本的SDK方式查询联邦学习作业列表，本示例配套的SDK版本为：3.1.69。
## 2. 前置条件
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。

## 3. 安装SDK
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

具体的SDK版本号请参见 [SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。
``` java
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-tics</artifactId>
    <version>3.1.69</version>
</dependency>
```
## 4. 代码示例
以下代码展示如何使用SDK查询联邦学习作业列表：
``` java
package com.huawei.cloud.tics;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.tics.v1.TicsClient;
import com.huaweicloud.sdk.tics.v1.model.ListFlJobRequest;
import com.huaweicloud.sdk.tics.v1.model.ListFlJobResponse;
import com.huaweicloud.sdk.tics.v1.region.TicsRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListFlJobDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(ListFlJobDemo.class);

    public static void main(String[] args) {
        // 基础认证信息：
        // ak: 华为云账号Access Key
        // sk: 华为云账号Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        //运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。

        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{******your project id******}";

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        TicsClient client = TicsClient.newBuilder()
                .withCredential(auth)
                .withRegion(TicsRegion.CN_NORTH_4)
                .build();

        // 查询联邦学习作业列表
        listFlJob(client);
    }

    private static void listFlJob(TicsClient client) {
        ListFlJobRequest request = new ListFlJobRequest();
        // 联盟id
        request.withLeagueId("<league_id>");
        // 每页记录数
        request.withLimit(10);
        // 记录数偏移量
        request.withOffset(5);

        try {
            ListFlJobResponse response = client.listFlJob(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(e.toString());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}

```
您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/TICS/sdk?api=ListFlJob) 中直接运行调试该接口。

## 5. 接口及参数说明
接口及参数请见 [查询联邦学习作业列表](https://support.huaweicloud.com/api-tics/ListFlJob.html)。

## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-11-27 | 1.0 | 文档首次发布|