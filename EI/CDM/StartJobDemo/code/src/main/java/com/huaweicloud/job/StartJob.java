package com.huaweicloud.job;

import com.huaweicloud.sdk.cdm.v1.CdmClient;
import com.huaweicloud.sdk.cdm.v1.model.StartJobRequest;
import com.huaweicloud.sdk.cdm.v1.model.StartJobResponse;
import com.huaweicloud.sdk.cdm.v1.region.CdmRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class StartJob {
    private static final Logger logger = LoggerFactory.getLogger(StartJob.class);

    public static void main(String[] args) {

        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";
        String clusterId = "{your clusterId string}";
        String jobName = "{your jobName string}";
        String regionId = "{your regionId string}";

        ICredential auth = new BasicCredentials()
                                .withAk(ak)
                                .withSk(sk)
                                .withProjectId(projectId);
        CdmClient client = CdmClient.newBuilder()
                                    .withCredential(auth)
                                    .withRegion(CdmRegion.valueOf(regionId))
                                    .build();
        StartJobRequest request = new StartJobRequest();
        request.withClusterId(clusterId).withJobName(jobName);

        try {
            StartJobResponse response = client.startJob(request);
            logger.info("success in start job:{}", jobName);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException var10) {
            logger.debug("error : {}",var10.getMessage(),var10);
        } catch (ServiceResponseException var12) {
            logger.debug("error : {}",var12.getMessage(),var12);
            System.out.println(var12.getHttpStatusCode());
            System.out.println(var12.getRequestId());
            System.out.println(var12.getErrorCode());
            System.out.println(var12.getErrorMsg());
        }
    }
}


