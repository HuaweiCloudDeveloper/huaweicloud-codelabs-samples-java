# 1、介绍
**什么是华为云CDM服务**

云数据迁移（Cloud Data Migration, 简称CDM），是一种高效、易用的数据集成服务。 CDM围绕大数据迁移上云和智能数据湖解决方案，提供了简单易用的迁移能力和多种数据源到数据湖的集成能力，降低了客户数据源迁移和集成的复杂性，有效的提高您数据迁移和集成的效率。

本示例展示了如何通过SDK来开启CDM集群已经存在的作业

# 2、前置条件
1：要使用华为云 Java SDK ，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台 “我的凭证-访问密钥” 页面上创建和查看您的 AK&SK。

2：要使用华为云 Java SDK 访问指定服务的 API，您需要确认已在华为云控制台开通当前服务。[购买cdm服务](https://console.huaweicloud.com/cdm/?agencyId=c88df0a42b1140d689620e800ef21c4f&locale=zh-cn&region=cn-south-1#/cdm/createInstance)

3：要执行作业，用户需要获取到对应地区的region，本服务的终端节点您可以在[地区和终端节点](https://support.huaweicloud.com/api-dataartsstudio/dataartsstudio_02_0004.html)中获取

4：要执行作业，用户需要获取到projectId，可在控制台->我的凭证->api凭证->项目列表中查看项目ID,具体获取方式可依据[项目和账号ID](https://support.huaweicloud.com/api-cdm/projectid_accountid.html)进行获取

5：要执行作业，用户需要[创建集群](https://support.huaweicloud.com/api-cdm/CreateCluster.html)，创建好集群后，在集群管理页面即可查看对应集群的clusterId

6：要执行作业，用户需要集群中存在作业，如果集群不存在作业，可以通过[创建作业](https://support.huaweicloud.com/api-cdm/CreateJob.html)来创建新的作业，若已经存在作业，则进入集群的作业管理页面，获取已经创建成功的作业名称。

7：华为云 Java SDK 支持 Java JDK 1.8 及其以上版本，您可以通过Maven方式获取和安装SDK，您只需要在java项目的pom.xml文件中加入相应的依赖即可
```xml
<!--       安装华为云SDK-->
    <dependencys>
        <dependency>
            <groupId>com.huaweicloud.sdk</groupId>
            <artifactId>huaweicloud-sdk-cdm</artifactId>
            <version>3.1.49</version>
        </dependency>
    </dependencys>
```



# 4、接口参数说明
- ak ：Access Key ID  
- sk ：Access Key ID   [AK/SK获取方法](https://support.huaweicloud.com/qs-obs/obs_qs_0005.html)
- projectId ：项目ID,获取方法参见[项目ID和账号ID](https://support.huaweicloud.com/api-cdm/projectid_accountid.html)
- clusterId ： 集群ID
- jobName ： 作业名称


关于接口的详细说明可参见：

[启动作业接口](https://support.huaweicloud.com/api-cdm/StartJob.html)


 
# 5、代码示例
以下代码展示如何使用SDK运行集群中已经存在的作业
```java
import com.huaweicloud.sdk.cdm.v1.CdmClient;
import com.huaweicloud.sdk.cdm.v1.model.StartJobRequest;
import com.huaweicloud.sdk.cdm.v1.model.StartJobResponse;
import com.huaweicloud.sdk.cdm.v1.region.CdmRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory
public class demo {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";
        String clusterId = "{your clusterId string}";
        String jobName = "{your jobName string}";
        String regionId = "{your regionId string}";

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);
        //构建客户端
        CdmClient client = CdmClient.newBuilder()
                .withCredential(auth)
                .withRegion(CdmRegion.valueOf(regionId))
                .build();
        StartJobRequest request = new StartJobRequest();
        //封装请求
        request.withClusterId(clusterId).withJobName(jobName);
        try {
            StartJobResponse response = client.startJob(request);
            logger.info("success in start job:{}", jobName);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException var10) {
            logger.debug("error : {}",var10.getMessage(),var10);
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
} 
```
# 6、运行结果
```json
{
        "submissions" : [ {
        "job-name" : "jdbc2hive",
        "creation-user" : "cdm",
        "creation-date" : "1536905778725",
        "progress" : 1,
        "status" : "BOOTING"
        } ]
        }
```

# 7、修订记录

| 发布日期       | 文档版本 | 修订说明   |  
|------------|------|--------|
| 2023-08-04 | 1.0  | 文档首次发布 |
 