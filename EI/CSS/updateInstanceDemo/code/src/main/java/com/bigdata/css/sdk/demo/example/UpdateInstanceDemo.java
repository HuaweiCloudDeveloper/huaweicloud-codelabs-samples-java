package com.bigdata.css.sdk.demo.example;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.css.v1.CssClient;
import com.huaweicloud.sdk.css.v1.model.UpdateInstanceRequest;
import com.huaweicloud.sdk.css.v1.model.UpdateInstanceResponse;
import com.huaweicloud.sdk.css.v1.region.CssRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateInstanceDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateInstanceDemo.class);

    public static void main(String[] args) {
        // If the AK and SK used for authentication are directly written into the code, there are high security risks. You are advised to store the AK and SK in ciphertext in the configuration file or environment variables and decrypt the AK and SK when using them to ensure security.
        // In this example, the AK and SK are stored in environment variables for identity authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        CssClient client = CssClient.newBuilder()
                .withCredential(auth)
                .withRegion(CssRegion.valueOf("cn-north-4"))
                .build();
        UpdateInstanceRequest request = new UpdateInstanceRequest();
        request.setClusterId("<target_cluster_id>");
        request.setInstanceId("<target_instance_id>");
        try {
            UpdateInstanceResponse response = client.updateInstance(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException e) {
            LOGGER.error(e.toString());
        } catch (RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(e.toString());
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(String.valueOf(e.getErrorCode()));
            LOGGER.error(String.valueOf(e.getErrorMsg()));
        }
    }
}