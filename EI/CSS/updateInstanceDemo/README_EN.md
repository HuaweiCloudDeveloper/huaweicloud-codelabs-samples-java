## 1. Example 
Huawei Cloud provides the Cloud Search Service (CSS) SDK. You can directly integrate the SDK to call CSS APIs to quickly operate CSS. A node has to be brought offline due to certain reasons (such as performance problems and faults). If such a capability is provided, the ID, name, trafficip, internalip, and resource of the node will not be changed after the node is replaced. This example shows how to replace the node using the Java SDK.
## 2. Prerequisites 
- 1、Obtain the Huawei Cloud SDK. You can also install the Java SDK by referring to Installing the Java SDK.
- 2、You must have a Huawei Cloud account and the access key (AK) and secret access key (SK) corresponding to the account. You can create and view your AK/SK on the My Credentials page of the Huawei Cloud console. For details, see Access Keys.
- 3、Huawei Cloud Java SDK supports Java JDK 1.8 or later.
## 3.Installing the SDK
To obtain and install the SDK using Maven, you only need to add dependencies to the pom.xml file of the Java project. For details about the SDK version, see SDK Development Center.
```xml
<dependencies>
   <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-css</artifactId>
        <version>3.1.54</version>
   </dependency>
</dependencies>
```
## 4.Start to use
- Because the underlying ECS is faulty, a node cannot provide services and must be brought offline. However, the IP address of the node has been configured on the customer's application side. If the node is directly brought offline, the customer's application side also needs to be modified.
- The performance deteriorates due to immature solutions. For example, the performance deteriorates after disk capacity expansion. In this case, you need to replace the node to initialize the new node.
- In the scenario where multiple AZs are replaced with a single AZ, if there are three master nodes in a single AZ, two of the master nodes can be replaced with other AZs.
- In the flavor replacement scenario, the ECS flavor modification API cannot be used to replace flavors of different series. For example, the specifications of a cloud disk can be replaced with those of a local model by replacing nodes.
- In the AZ migration scenario, the user cluster is in AZ1, but all resources in AZ1 have been sold out. All resources in AZ1 can be migrated to AZ2 by replacing the node.
```java
public class UpdateInstanceDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateInstanceDemo.class);

    public static void main(String[] args) {
        // Writing the AK and SK used for authentication into the code has high security risks. It is recommended that the AK and SK be stored in ciphertext in the configuration file or environment variables and decrypted to ensure security.
        // In this example, the AK and SK are stored in environment variables for identity authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        CssClient client = CssClient.newBuilder()
                .withCredential(auth)
                .withRegion(CssRegion.valueOf("cn-north-4"))
                .build();
        UpdateInstanceRequest request = new UpdateInstanceRequest();
        request.setClusterId("<target_cluster_id>");
        request.setInstanceId("<target_instance_id>");
        try {
            UpdateInstanceResponse response = client.updateInstance(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException e) {
            LOGGER.error(e.toString());
        } catch (RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(e.toString());
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(String.valueOf(e.getErrorCode()));
            LOGGER.error(String.valueOf(e.getErrorMsg()));
        }
    }
}
```

## 5.Response Example
Cluster Information
```json
{}
```
## 6.Interface and Parameter Description
refer：[Node Replacement](https://support.huaweicloud.com/intl/en-us/api-css/UpdateInstance.html)
## 7. Refer
For more example information, see[CSS](https://support.huaweicloud.com/intl/en-us/api-css/css_03_0057.html)
## 8. Change History

|  Release Date  | Document Version |   Description   |
| :--------: | :------: | :----------: |
| 2023-08-30 |   1.0    | Released the first version |