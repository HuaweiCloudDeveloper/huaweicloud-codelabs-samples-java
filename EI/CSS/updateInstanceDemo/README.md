## 1. 示例简介
华为云提供了云搜索（CSS）的SDK，您可以直接集成SDK来调用CSS的相关API，从而实现对CSS的快速操作。当由于某些原因（性能、故障等），迫不得已需要下线一个节点时。如果有这样一种能力，能够保证节点被换掉后，不会改变节点的id、名称、trafficip、internalip、resource信息等，该示例展示了如何通过java版SDK节点替换。
## 2. 前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 访问密钥 。
- 3、华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。
## 3.安装SDK
您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。 具体的SDK版本号请参见 SDK开发中心 。
```xml
<dependencies>
   <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-css</artifactId>
        <version>3.1.54</version>
   </dependency>
</dependencies>
```
## 4.开始使用
- 由于底层ecs故障，某个节点无法再提供服务，不得不下线，但是这个节点的ip已经被配置到客户的应用侧了，如果直接下线节点，客户的应用侧也需要改动
- 由于方案不成熟导致性能下降，比如磁盘扩容后性能下降，通过节点替换初始化新节点
- 单AZ替换多AZ场景，如果单AZ中有三个master节点，可以通过节点替换，将其中的两个master形变到另外额的AZ
- 规格替换场景，跨系列规格替换，不能用ECS变更规格接口。比如云盘规格，可以通过节点替换将云盘规格换成本地盘系列机型
- AZ迁移场景，用户集群在AZ1，但此时AZ1资源已经全部售罄，通过节点替换可以将AZ1资源全部迁移到AZ2
```java
public class UpdateInstanceDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateInstanceDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        CssClient client = CssClient.newBuilder()
                .withCredential(auth)
                .withRegion(CssRegion.valueOf("cn-north-4"))
                .build();
        UpdateInstanceRequest request = new UpdateInstanceRequest();
        request.setClusterId("<target_cluster_id>");
        request.setInstanceId("<target_instance_id>");
        try {
            UpdateInstanceResponse response = client.updateInstance(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException e) {
            LOGGER.error(e.toString());
        } catch (RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(e.toString());
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(String.valueOf(e.getErrorCode()));
            LOGGER.error(String.valueOf(e.getErrorMsg()));
        }
    }
}
```

## 5.返回示例
集群信息
```json
{}
```
## 6.接口及参数说明
参见：[节点替换](https://support.huaweicloud.com/api-css/UpdateInstance.html)
## 7. 参考
更多示例信息请参考[CSS](https://support.huaweicloud.com/api-css/css_03_0057.html)
## 8. 修订记录

|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2023-08-30 |   1.0    | 文档首次发布 |