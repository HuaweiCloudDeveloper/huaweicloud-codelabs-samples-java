## 1. 示例简介
华为云提供了云搜索（CSS）的SDK，您可以直接集成SDK来调用CSS的相关API，从而实现对CSS的快速操作。 该示例展示了如何通过java版SDK滚动重启。
## 2. 前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 访问密钥 。
- 3、华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。
## 3.安装SDK
您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。 具体的SDK版本号请参见 SDK开发中心 。
```xml
<dependencies>
   <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-css</artifactId>
        <version>3.1.5</version>
   </dependency>
</dependencies>
```
## 4.开始使用
- 滚动重启示例代码
```java
public class RollingRestartServiceDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(RollingRestartServiceDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份认证为例，运行示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        CssClient client = CssClient.newBuilder()
                .withCredential(auth)
                .withRegion(CssRegion.valueOf("cn-north-4"))
                .build();
        RollingRestartRequest request = new RollingRestartRequest();
        RollingRestartReq body = new RollingRestartReq();
        body.setType("role");
        body.setValue("ess");
        request.setClusterId("{cluster_id}");
        request.withBody(body);
        try {
            RollingRestartResponse response = client.rollingRestart(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException e) {
            LOGGER.error(e.toString());
        } catch (RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(e.toString());
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(String.valueOf(e.getErrorCode()));
            LOGGER.error(String.valueOf(e.getErrorMsg()));
        }
    }
}
```

## 5.返回示例
滚动重启响应示例
```json
无响应体
```
## 6.接口及参数说明
参见：[滚动重启](https://support.huaweicloud.com/intl/en-us/api-css/RollingRestart.html)
## 7. 参考
更多示例信息请参考[CSS](https://support.huaweicloud.com/api-css/css_03_0057.html)
## 8. 修订记录

|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2023-08-30 |   1.0    | 文档首次发布 |