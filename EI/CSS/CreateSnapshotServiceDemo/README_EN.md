## 1. Example 
Huawei Cloud provides the Cloud Search Service (CSS) SDK. You can directly integrate the SDK to call CSS APIs to quickly operate CSS. This example shows how to create a snapshot using the Java SDK.
## 2. Prerequisites 
- 1、Obtain the Huawei Cloud SDK. You can also install the Java SDK by referring to Installing the Java SDK.
- 2、You must have a Huawei Cloud account and the access key (AK) and secret access key (SK) corresponding to the account. You can create and view your AK/SK on the My Credentials page of the Huawei Cloud console. For details, see Access Keys.
- 3、Huawei Cloud Java SDK supports Java JDK 1.8 or later.
## 3.Installing the SDK
To obtain and install the SDK using Maven, you only need to add dependencies to the pom.xml file of the Java project. For details about the SDK version, see SDK Development Center.
```xml
<dependencies>
   <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-css</artifactId>
        <version>3.1.5</version>
   </dependency>
</dependencies>
```
## 4.Start to use
- Sample code for creating a snapshot
```java
public class CreateSnapshotServiceDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateSnapshotServiceDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份认证为例，运行示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        CssClient client = CssClient.newBuilder()
                .withCredential(auth)
                .withRegion(CssRegion.valueOf("cn-north-4"))
                .build();
        CreateSnapshotRequest request = new CreateSnapshotRequest();
        CreateSnapshotReq body = new CreateSnapshotReq();
        body.setName("css-snapshot");
        body.setIndices("{index}");
        body.setDescription("{description}");
        request.setClusterId("{clsuter_id}");
        request.withBody(body);
        try {
            CreateSnapshotResponse response = client.createSnapshot(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException e) {
            LOGGER.error(e.toString());
        } catch (RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(e.toString());
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(String.valueOf(e.getErrorCode()));
            LOGGER.error(String.valueOf(e.getErrorMsg()));
        }
    }
}
```

## 5.Response Example
Response example for creating a snapshot
```json
{
  "backup" : {
    "id" : "9dc4f5c9-33c0-45c7-9378-ae35ae350682",
    "name" : "snapshot_101"
  }
}
```
## 6.Interface and Parameter Description
refer：[Create a snapshot](https://support.huaweicloud.com/intl/en-us/api-css/CreateSnapshot.html)
## 7. Refer
For more example information, see[CSS](https://support.huaweicloud.com/intl/en-us/api-css/css_03_0057.html)
## 8. Change History

|  Release Date  | Document Version |   Description   |
| :--------: | :------: | :----------: |
| 2023-08-30 |   1.0    | Released the first version |