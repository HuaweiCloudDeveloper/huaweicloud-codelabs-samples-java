package com.bigdata.css.sdk.demo.example;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.css.v1.CssClient;
import com.huaweicloud.sdk.css.v1.model.CreateSnapshotReq;
import com.huaweicloud.sdk.css.v1.model.CreateSnapshotRequest;
import com.huaweicloud.sdk.css.v1.model.CreateSnapshotResponse;
import com.huaweicloud.sdk.css.v1.region.CssRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateSnapshotServiceDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateSnapshotServiceDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份认证为例，运行示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        CssClient client = CssClient.newBuilder()
                .withCredential(auth)
                .withRegion(CssRegion.valueOf("cn-north-4"))
                .build();
        CreateSnapshotRequest request = new CreateSnapshotRequest();
        CreateSnapshotReq body = new CreateSnapshotReq();
        body.setName("css-snapshot");
        body.setIndices("{index}");
        body.setDescription("{description}");
        request.setClusterId("{clsuter_id}");
        request.withBody(body);
        try {
            CreateSnapshotResponse response = client.createSnapshot(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException e) {
            LOGGER.error(e.toString());
        } catch (RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(e.toString());
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(String.valueOf(e.getErrorCode()));
            LOGGER.error(String.valueOf(e.getErrorMsg()));
        }
    }
}