package com.bigdata.css.sdk.demo.example;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.css.v1.CssClient;
import com.huaweicloud.sdk.css.v1.model.CreateClusterBody;
import com.huaweicloud.sdk.css.v1.model.CreateClusterDatastoreBody;
import com.huaweicloud.sdk.css.v1.model.CreateClusterReq;
import com.huaweicloud.sdk.css.v1.model.CreateClusterRequest;
import com.huaweicloud.sdk.css.v1.model.CreateClusterInstanceBody;
import com.huaweicloud.sdk.css.v1.model.CreateClusterInstanceNicsBody;
import com.huaweicloud.sdk.css.v1.model.CreateClusterInstanceVolumeBody;
import com.huaweicloud.sdk.css.v1.model.CreateClusterResponse;
import com.huaweicloud.sdk.css.v1.region.CssRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateClusterServiceDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateClusterServiceDemo.class);

    public static void main(String[] args) {
        // If the AK and SK used for authentication are directly written into the code, there are high security risks. You are advised to store the AK and SK in ciphertext in the configuration file or environment variables and decrypt the AK and SK when using them to ensure security.
        // In this example, the AK and SK are stored in environment variables for identity authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        CssClient client = CssClient.newBuilder()
                .withCredential(auth)
                .withRegion(CssRegion.valueOf("cn-north-4"))
                .build();
        CreateClusterRequest request = new CreateClusterRequest();
        CreateClusterReq body = new CreateClusterReq();

        CreateClusterBody cluster = initCLusterBody();

        body.setCluster(cluster);
        request.withBody(body);
        try {
            CreateClusterResponse response = client.createCluster(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException e) {
            LOGGER.error(e.toString());
        } catch (RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(e.toString());
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(String.valueOf(e.getErrorCode()));
            LOGGER.error(String.valueOf(e.getErrorMsg()));
        }
    }

    private static CreateClusterBody initCLusterBody() {
        CreateClusterBody cluster = new CreateClusterBody();
        cluster.setName("css-create-cluster");
        cluster.setInstanceNum(1);

        CreateClusterDatastoreBody datastore = new CreateClusterDatastoreBody();
        datastore.setType("elasticsearch");
        datastore.setVersion("{elasticsearch_version}");
        cluster.setDatastore(datastore);

        CreateClusterInstanceBody instance = new CreateClusterInstanceBody();
        instance.setFlavorRef("{flavor_name}");

        CreateClusterInstanceNicsBody nics = new CreateClusterInstanceNicsBody();
        nics.setNetId("{your_net_id}");
        nics.setVpcId("{YOUR VPC ID}");
        nics.setSecurityGroupId("{your_security_group}");
        instance.setNics(nics);

        CreateClusterInstanceVolumeBody volume = new CreateClusterInstanceVolumeBody();
        volume.setVolumeType("COMMON");
        volume.setSize(40);
        instance.setVolume(volume);
        cluster.setInstance(instance);
        return cluster;
    }
}