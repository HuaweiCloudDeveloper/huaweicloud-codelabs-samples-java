

## 1、功能介绍

华为云提供了媒体处理服务端SDK，您可以直接集成服务端SDK来调用媒体处理的相关API，从而实现对媒体处理的快速操作。 该示例展示了如何通过java版SDK创建、取消和查询解析任务。

**您将学到什么？**

如何通过java版SDK来体验媒体处理(MPC)创建、取消的查询解析任务功能

## 2、开发时序图
### 创建解析任务
![image](assets/create.jpg)

### 取消解析任务
![image](assets/cancel.jpg)

### 查询解析任务
![image](assets/query.jpg)

## 3、前置条件
- 1、已[注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=40c99ba3cea14417a2428c756a626599&lang=zh-cn)华为帐号并开通华为云，已进行[实名认证](https://support.huaweicloud.com/usermanual-account/zh-cn_topic_0077914254.html)。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、已获取账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 4、已获取转码服务对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见[API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html)。 
- 5、已将需要处理的媒资文件上传至MPC同[区域](https://developer.huaweicloud.com/endpoint?MPC)的OBS桶中，并将OBS桶进行授权，允许MPC访问。具体请参见[上传音视频文件](https://support.huaweicloud.com/usermanual-mpc/mpc010002.html)和[获取云资源授权](https://support.huaweicloud.com/usermanual-mpc/mpc010003.html)。

## 4、SDK获取和安装
您可以通过Maven配置所依赖的[媒体处理服务SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/MPC?lang=Java)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-mpc</artifactId>
    <version>3.1.53</version>
</dependency>

```

## 5、接口参数说明
关于接口参数的详细说明可参见：

[a.新建视频解析任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MPC/doc?api=CreateExtractTask)

[b.取消视频解析任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MPC/doc?api=DeleteExtractTask)

[c.查询视频解析任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MPC/doc?api=ListExtractTask)

## 6、关键代码片段

### 6.1、创建解析任务

```java
    /**
     * 创建视频解析任务
     *
     * @return taskId 提交的解析任务的任务ID
     */
    public static String createExtractTask() {
        // 设置解析输入视频地址和输出路径
        ObsObjInfo input = new ObsObjInfo().withBucket("<example-bucket>").withLocation("<Region ID>").withObject("<example-path/input.mp4>");
        ObsObjInfo output = new ObsObjInfo().withBucket("<example-bucket>").withLocation("<Region ID>").withObject("<example-path/output>");
    
    
        // 创建视频解析任务
        CreateExtractTaskRequest request = new CreateExtractTaskRequest();
    
        // 创建请求body参数
        CreateExtractTaskReq body = new CreateExtractTaskReq();
        // 设置排队处理
        body.withSync(0);
        // 设置源文件信息
        body.withInput(input);
        // 设置输出文件信息
        body.withOutput(output);
        request.withBody(body);
    
        // 发送创建任务请求
        try {
            CreateExtractTaskResponse response = initMpcClient().createExtractTask(request);
            logger.info(response.toString());
            return response.getTaskId();
        } catch (ConnectionException e) {
            logger.error("The authentication is rejected. Check whether the AK/SK is correct: ", e);
        } catch (RequestTimeoutException e) {
            logger.error("The server processing times out and does not return a response: ", e);
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode:{}, RequestId:{}, ErrorCode:{}, ErrorMsg:{}",e.getHttpStatusCode(),e.getRequestId(),e.getErrorCode(),e.getErrorMsg());
        }
            return "";
        }
```

### 6.2、取消解析任务

```java
    /**
     * 取消视频解析任务
     * @param taskId 任务ID
     */
    public static void deleteExtractTask(String taskId) {
        // 查询视频解析任务
        DeleteExtractTaskRequest request = new DeleteExtractTaskRequest();
        request.withTaskId(taskId);
    
        // 发送查询任务请求
        try {
            DeleteExtractTaskResponse response = initMpcClient().deleteExtractTask(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("The authentication is rejected. Check whether the AK/SK is correct: ", e);
        } catch (RequestTimeoutException e) {
            logger.error("The server processing times out and does not return a response: ", e);
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode:{}, RequestId:{}, ErrorCode:{}, ErrorMsg:{}",e.getHttpStatusCode(),e.getRequestId(),e.getErrorCode(),e.getErrorMsg());
        }
        }
```

### 6.3、查询解析任务

```java
    /**
     * 查询视频解析任务
     * @param taskId 任务ID
     */
    public static void listExtractTask(String taskId) {
        // 查询视频解析任务
        ListExtractTaskRequest request = new ListExtractTaskRequest();
        List<String> listRequestTaskId = new ArrayList<>();
        listRequestTaskId.add(taskId);
        request.withTaskId(listRequestTaskId);
    
        // 发送查询任务请求
        try {
            ListExtractTaskResponse response = initMpcClient().listExtractTask(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("The authentication is rejected. Check whether the AK/SK is correct: ", e);
        } catch (RequestTimeoutException e) {
            logger.error("The server processing times out and does not return a response: ", e);
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode:{}, RequestId:{}, ErrorCode:{}, ErrorMsg:{}",e.getHttpStatusCode(),e.getRequestId(),e.getErrorCode(),e.getErrorMsg());
        }
        }
```


## 7、运行结果
**创建视频解析任务，返回任务ID**
```json
{
  "task_id": "1024"
}
```
**查询解析任务的状态和结果**
```json
        {
  "total": 1,
  "tasks": [
    {
      "task_id": "471367",
      "status": "SUCCEED",
      "create_time": "20200608101432",
      "start_time": "20200608101432",
      "end_time": "20200608101437",
      "description": "Task done successfully",
      "input": {
        "bucket": "example-bucket",
        "location": "region01",
        "object": "example-path/input.mp4",
        "file_name": null
      },
      "output": {
        "bucket": "example-bucket",
        "location": "region01",
        "object": "example-path/output",
        "file_name": "40cf104dbbf45181fcd7bb56f54058ba.txt"
      },
      "metadata": {
        "size": 821407522,
        "duration": 1428,
        "format": "MP4",
        "bitrate": 4590726,
        "video": [
          {
            "width": 1280,
            "height": 720,
            "bitrate": 4460614,
            "frame_rate": 30,
            "codec": "H.264"
          }
        ],
        "audio": [
          {
            "codec": "AAC",
            "sample": 44100,
            "channels": 2,
            "bitrate": 130112
          }
        ]
      }
    }
  ]
}
```
**取消已下发的视频解析任务**

取消视频解析任务成功时，状态码：204，响应体为空。

取消视频解析任务失败时，状态码：400，响应体如下：
```json
{
  "error_code": "MPC.363011005",
  "error_msg": "Can not find the taskID"
}
```

## 8、参考
本示例的代码工程仅用于简单演示，实际开发过程中应严格遵循开发指南。访问以下链接可以获取详细信息：[开发指南](https://support.huaweicloud.com/sdkreference-mpc/mpc_05_0123.html)