package com.huawei.hwclouds.mpc.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.mpc.v1.MpcClient;
import com.huaweicloud.sdk.mpc.v1.model.CreateExtractTaskReq;
import com.huaweicloud.sdk.mpc.v1.model.CreateExtractTaskRequest;
import com.huaweicloud.sdk.mpc.v1.model.CreateExtractTaskResponse;
import com.huaweicloud.sdk.mpc.v1.model.DeleteExtractTaskRequest;
import com.huaweicloud.sdk.mpc.v1.model.DeleteExtractTaskResponse;
import com.huaweicloud.sdk.mpc.v1.model.ListExtractTaskRequest;
import com.huaweicloud.sdk.mpc.v1.model.ListExtractTaskResponse;
import com.huaweicloud.sdk.mpc.v1.model.ObsObjInfo;
import com.huaweicloud.sdk.mpc.v1.region.MpcRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ExtractTaskSolution {
    private static final Logger logger = LoggerFactory.getLogger(ExtractTaskSolution.class.getName());

    /**
     * Initialization MpcClient
     * @return MpcClient
     */
    public static MpcClient initMpcClient() {
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true).withTimeout(3);
        /*
           Set the HTTP proxy according to the actual situation. You can delete this part of the code when you do not need to use a proxy.
           example: httpConfig.withProxyHost("proxy.huawei.com").withProxyPort(8080)
                or: httpConfig.withProxyHost("<Proxy Host>").withProxyPort(8080).withProxyUsername("<Proxy Username>").withProxyPassword("<Proxy Password>");
        */
        httpConfig.withProxyHost("<Proxy Host>").withProxyPort(8080).withProxyUsername("<Proxy Username>").withProxyPassword("<Proxy Password>");

        /**
         * You have obtained the access key (AK) and secret access key (SK) of the Huawei Cloud account. To create and view an AK/SK,
         * go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console.
         * Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
         * In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
         */
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        BasicCredentials auth = new BasicCredentials().withAk(ak).withSk(sk);
        // Region ID is obtained from Regions and Endpoints. https://developer.huaweicloud.com/endpoint?MPC
        return MpcClient.newBuilder()
            .withHttpConfig(httpConfig)
            .withCredential(auth)
            .withRegion(MpcRegion.valueOf("<Region ID>"))
            .build();
    }

    /**
     * Creating a Parsing Task
     *
     * @return taskId: ID of the submitted snapshot capturing task.
     */
    public static String createExtractTask() {
        // Set the input file path.
        ObsObjInfo input = new ObsObjInfo().withBucket("<example-bucket>").withLocation("<Region ID>").withObject("<example-path/input.mp4>");
        ObsObjInfo output = new ObsObjInfo().withBucket("<example-bucket>").withLocation("<Region ID>").withObject("<example-path/output>");


        // Create a Parsing request.
        CreateExtractTaskRequest request = new CreateExtractTaskRequest();

        CreateExtractTaskReq body = new CreateExtractTaskReq();
        // Setting Queuing Processing
        body.withSync(0);
        // Setting Source File Information
        body.withInput(input);
        // Setting Output File Information
        body.withOutput(output);
        request.withBody(body);

        // Sending a Task Creation Request
        try {
            CreateExtractTaskResponse response = initMpcClient().createExtractTask(request);
            logger.info(response.toString());
            return response.getTaskId();
        } catch (ConnectionException e) {
            logger.error("The authentication is rejected. Check whether the AK/SK is correct: ", e);
        } catch (RequestTimeoutException e) {
            logger.error("The server processing times out and does not return a response: ", e);
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode:{}, RequestId:{}, ErrorCode:{}, ErrorMsg:{}",e.getHttpStatusCode(),e.getRequestId(),e.getErrorCode(),e.getErrorMsg());
        }
        return "";
    }

    /**
     * Query a Parsing tasks.
     * @param taskId: Task ID
     */
    public static void listExtractTask(String taskId) {
        ListExtractTaskRequest request = new ListExtractTaskRequest();
        List<String> listRequestTaskId = new ArrayList<>();
        listRequestTaskId.add(taskId);
        request.withTaskId(listRequestTaskId);

        try {
            ListExtractTaskResponse response = initMpcClient().listExtractTask(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("The authentication is rejected. Check whether the AK/SK is correct: ", e);
        } catch (RequestTimeoutException e) {
            logger.error("The server processing times out and does not return a response: ", e);
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode:{}, RequestId:{}, ErrorCode:{}, ErrorMsg:{}",e.getHttpStatusCode(),e.getRequestId(),e.getErrorCode(),e.getErrorMsg());
        }
    }

    /**
     * Delete a Parsing task.
     * @param taskId: Task ID
     */
    public static void deleteExtractTask(String taskId) {
        DeleteExtractTaskRequest request = new DeleteExtractTaskRequest();
        request.withTaskId(taskId);

        try {
            DeleteExtractTaskResponse response = initMpcClient().deleteExtractTask(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("The authentication is rejected. Check whether the AK/SK is correct: ", e);
        } catch (RequestTimeoutException e) {
            logger.error("The server processing times out and does not return a response: ", e);
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode:{}, RequestId:{}, ErrorCode:{}, ErrorMsg:{}",e.getHttpStatusCode(),e.getRequestId(),e.getErrorCode(),e.getErrorMsg());
        }
    }

    public static void main(String[] args) {
        String taskId = createExtractTask();
        deleteExtractTask(taskId);
        listExtractTask(taskId);
    }

}
