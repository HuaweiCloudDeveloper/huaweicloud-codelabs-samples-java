

## 1. Functions

You can integrate Media Processing Center (MPC) server SDKs provided by Huawei Cloud to call MPC APIs, making it easier for you to use the service. 
This sample shows how to use the Java SDK to create, cancel, and query a parsing task.

**What You Will Learn**

Using the Java SDK to create, cancel, and query a parsing task on MPC

## 2. Development Sequence Diagram
### Creating a Parsing Task
![image](assets/create_en.png)

### Deleting a Parsing Task
![image](assets/cancel_en.png)

### Querying a Parsing Task
![image](assets/query_en.png)

## 3. Prerequisites
- 1. You have[registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=5089ec193d9a4bedaa5bef6aa4d998fc&lang=zh-cn)with Huawei Cloud and passed[real-name authentication](https://support.huaweicloud.com/intl/zh-cn/usermanual-account/zh-cn_topic_0119621532.html).
- 2. The development environment (Java JDK 1.8 or later) is available.
- 3. You have obtained the access key (AK) and secret access key (SK) of the Huawei Cloud account. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/zh-cn/usermanual-ca/zh-cn_topic_0046606340.html).
- 4. You have obtained the project ID of the corresponding region of MPC. You can view the project ID on the **My Credentials** > **API Credentials** page of the Huawei Cloud console. For details, see[API Credentials](https://support.huaweicloud.com/intl/zh-cn/usermanual-ca/ca_01_0002.html)。
- 5. You have uploaded the media files to an OBS bucket in the [region](https://developer.huaweicloud.com/intl/zh-cn/endpoint)of MPC, and authorized MPC to access the OBS bucket. For details, see [Uploading Media Files](https://support.huaweicloud.com/intl/zh-cn/usermanual-mpc/mpc010002.html) and [Authorizing Access to Cloud Resources](https://support.huaweicloud.com/intl/zh-cn/usermanual-mpc/mpc010003.html).
## 4. Obtaining and Installing the SDK
You can use Maven to configure the dependent[MPC SDK](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/MPC?lang=Java)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-mpc</artifactId>
    <version>3.1.53</version>
</dependency>

```

## 5. API Parameter Description
For details about API parameters, see:

[a.Creating a Parsing Task](https://console.huaweicloud.com/apiexplorer/#/openapi/MPC/doc?api=CreateExtractTask)

[b.Deleting a Parsing Task](https://console.huaweicloud.com/apiexplorer/#/openapi/MPC/doc?api=DeleteExtractTask)

[c.Querying a Parsing Task](https://console.huaweicloud.com/apiexplorer/#/openapi/MPC/doc?api=ListExtractTask)

## 6. Key Code Snippets

### 6.1. Creating a Parsing Task

```java
    /**
     * Creating a Parsing Task
     *
     * @return taskId: ID of the submitted snapshot capturing task.
     */
    public static String createExtractTask() {
        // Set the input file path.
        ObsObjInfo input = new ObsObjInfo().withBucket("<example-bucket>").withLocation("<Region ID>").withObject("<example-path/input.mp4>");
        ObsObjInfo output = new ObsObjInfo().withBucket("<example-bucket>").withLocation("<Region ID>").withObject("<example-path/output>");
    
    
        // Create a Parsing request.
        CreateExtractTaskRequest request = new CreateExtractTaskRequest();
    
        CreateExtractTaskReq body = new CreateExtractTaskReq();
        // Setting Queuing Processing
        body.withSync(0);
        // Setting Source File Information
        body.withInput(input);
        // Setting Output File Information
        body.withOutput(output);
        request.withBody(body);
    
        // Sending a Task Creation Request
        try {
        CreateExtractTaskResponse response = initMpcClient().createExtractTask(request);
        logger.info(response.toString());
        return response.getTaskId();
        } catch (ConnectionException e) {
        logger.error("The authentication is rejected. Check whether the AK/SK is correct: ", e);
        } catch (RequestTimeoutException e) {
        logger.error("The server processing times out and does not return a response: ", e);
        } catch (ServiceResponseException e) {
        logger.error("HttpStatusCode:{}, RequestId:{}, ErrorCode:{}, ErrorMsg:{}",e.getHttpStatusCode(),e.getRequestId(),e.getErrorCode(),e.getErrorMsg());
        }
        return "";
        }
```

### 6.2. Canceling a Parsing Task

```java
    /**
     * Delete a Parsing task.
     * @param taskId: Task ID
     */
    public static void deleteExtractTask(String taskId) {
        DeleteExtractTaskRequest request = new DeleteExtractTaskRequest();
        request.withTaskId(taskId);
    
        try {
        DeleteExtractTaskResponse response = initMpcClient().deleteExtractTask(request);
        logger.info(response.toString());
        } catch (ConnectionException e) {
        logger.error("The authentication is rejected. Check whether the AK/SK is correct: ", e);
        } catch (RequestTimeoutException e) {
        logger.error("The server processing times out and does not return a response: ", e);
        } catch (ServiceResponseException e) {
        logger.error("HttpStatusCode:{}, RequestId:{}, ErrorCode:{}, ErrorMsg:{}",e.getHttpStatusCode(),e.getRequestId(),e.getErrorCode(),e.getErrorMsg());
        }
        }
```

### 6.3. Querying Parsing Tasks

```java
    /**
     * Query a Parsing tasks.
     * @param taskId: Task ID
     */
    public static void listExtractTask(String taskId) {
        ListExtractTaskRequest request = new ListExtractTaskRequest();
        List<String> listRequestTaskId = new ArrayList<>();
        listRequestTaskId.add(taskId);
        request.withTaskId(listRequestTaskId);
    
        try {
        ListExtractTaskResponse response = initMpcClient().listExtractTask(request);
        logger.info(response.toString());
        } catch (ConnectionException e) {
        logger.error("The authentication is rejected. Check whether the AK/SK is correct: ", e);
        } catch (RequestTimeoutException e) {
        logger.error("The server processing times out and does not return a response: ", e);
        } catch (ServiceResponseException e) {
        logger.error("HttpStatusCode:{}, RequestId:{}, ErrorCode:{}, ErrorMsg:{}",e.getHttpStatusCode(),e.getRequestId(),e.getErrorCode(),e.getErrorMsg());
        }
        }
```


## 7. Execution Results
**The ID of a created Parsing task is returned.**
```json
{
  "task_id": "1024"
}
```
**Query the status and result of a Parsing task.**
```json
        {
  "total": 1,
  "tasks": [
    {
      "task_id": "471367",
      "status": "SUCCEED",
      "create_time": "20200608101432",
      "start_time": "20200608101432",
      "end_time": "20200608101437",
      "description": "Task done successfully",
      "input": {
        "bucket": "example-bucket",
        "location": "region01",
        "object": "example-path/input.mp4",
        "file_name": null
      },
      "output": {
        "bucket": "example-bucket",
        "location": "region01",
        "object": "example-path/output",
        "file_name": "40cf104dbbf45181fcd7bb56f54058ba.txt"
      },
      "metadata": {
        "size": 821407522,
        "duration": 1428,
        "format": "MP4",
        "bitrate": 4590726,
        "video": [
          {
            "width": 1280,
            "height": 720,
            "bitrate": 4460614,
            "frame_rate": 30,
            "codec": "H.264"
          }
        ],
        "audio": [
          {
            "codec": "AAC",
            "sample": 44100,
            "channels": 2,
            "bitrate": 130112
          }
        ]
      }
    }
  ]
}
```
**Cancel a delivered Parsing task.**

When a Parsing task is canceled, the status code is 204 and the response body is empty.

When a Parsing task fails to be canceled, the status code is 400 and the response body is as follows:
```json
{
  "error_code": "MPC.363011005",
  "error_msg": "Can not find the taskID"
}
```

## 8. Reference
The code project in this sample is used only for demonstration. During actual development, strictly follow the development guide. 
Learn more details from [Development Guide](https://support.huaweicloud.com/intl/zh-cn/api-mpc/mpc_04_0061.html).