package com.huawei.hwclouds.mpc.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.mpc.v1.MpcClient;
import com.huaweicloud.sdk.mpc.v1.model.CreateThumbReq;
import com.huaweicloud.sdk.mpc.v1.model.CreateThumbnailsTaskRequest;
import com.huaweicloud.sdk.mpc.v1.model.CreateThumbnailsTaskResponse;
import com.huaweicloud.sdk.mpc.v1.model.DeleteThumbnailsTaskRequest;
import com.huaweicloud.sdk.mpc.v1.model.DeleteThumbnailsTaskResponse;
import com.huaweicloud.sdk.mpc.v1.model.ListThumbnailsTaskRequest;
import com.huaweicloud.sdk.mpc.v1.model.ListThumbnailsTaskResponse;
import com.huaweicloud.sdk.mpc.v1.model.ObsObjInfo;
import com.huaweicloud.sdk.mpc.v1.model.ThumbnailPara;
import com.huaweicloud.sdk.mpc.v1.region.MpcRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ThumbnailsTaskSolution {
    private static final Logger logger = LoggerFactory.getLogger(ThumbnailsTaskSolution.class.getName());

    /**
     *  Initialization MpcClient
     * @return MpcClient
     */
    public static MpcClient initMpcClient() {
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true).withTimeout(3);
        /*
           Set the HTTP proxy according to the actual situation. You can delete this part of the code when you do not need to use a proxy.
           example: httpConfig.withProxyHost("proxy.huawei.com").withProxyPort(8080)
                or: httpConfig.withProxyHost("<Proxy Host>").withProxyPort(8080).withProxyUsername("<Proxy Username>").withProxyPassword("<Proxy Password>");
        */
        httpConfig.withProxyHost("<Proxy Host>").withProxyPort(8080).withProxyUsername("<Proxy Username>").withProxyPassword("<Proxy Password>");

        /* You have obtained the access key (AK) and secret access key (SK) of the Huawei Cloud account. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console
           Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
           In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        */
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        BasicCredentials auth = new BasicCredentials().withAk(ak).withSk(sk);
        // Region ID is obtained from Regions and Endpoints. https://developer.huaweicloud.com/endpoint?MPC
        return MpcClient.newBuilder()
            .withHttpConfig(httpConfig)
            .withCredential(auth)
            .withRegion(MpcRegion.valueOf("<Region ID>"))
            .build();
    }

    /**
     * Create a snapshot capturing task.
     *
     * @return taskId: ID of the submitted snapshot capturing task.
     */
    public static String createThumbnailsTask() {
        // Set the input file path.
        ObsObjInfo input = new ObsObjInfo().withBucket("<example-bucket>").withLocation("<Region ID>").withObject("<example-path/input.mp4>");
        ObsObjInfo output = new ObsObjInfo().withBucket("<example-bucket>").withLocation("<Region ID>").withObject("<example-path/output>");


        // Create a snapshot capturing request.
        CreateThumbnailsTaskRequest request = new CreateThumbnailsTaskRequest();
        CreateThumbReq body = new CreateThumbReq();
        List<Integer> listThumbnailParaDots = new ArrayList<>();
        listThumbnailParaDots.add(50000);
        // Set the snapshot capturing type and capture a snapshot at a specified time point.
        ThumbnailPara thumbnailParabody = new ThumbnailPara();
        // Set the sampling type and capture a snapshot at a specified time point.
        thumbnailParabody.withType(ThumbnailPara.TypeEnum.fromValue("DOTS"))
            // Set the snapshot file name.
            .withOutputFilename("photo")
            // Set the interval for sampling snapshots.
            .withTime(10)
            // Set the start time for the sampling type to "TIME". This parameter is used together with "time".
            .withStartTime(100)
            // Capturing duration when the sampling type is set to "TIME". This parameter is used together with "time" and “start_time”, indicating that the first snapshot is captured at the time specified by “start_time” and snapshots
            // are captured at the interval specified by “time” until the duration specified by this parameter elapses. The unit is second.
            .withDuration(1)
            // Array of time points when a snapshot is captured.
            .withDots(listThumbnailParaDots)
            // Set the snapshot format.
            .withFormat(1)
            // Set the width of a snapshot.
            .withWidth(96)
            // Set the height of a snapshot.
            .withHeight(96);

        body.withThumbnailPara(thumbnailParabody);
        body.withOutput(output);
        body.withInput(input);
        request.withBody(body);

        // Send a snapshot capturing request.
        try {
            CreateThumbnailsTaskResponse response = initMpcClient().createThumbnailsTask(request);
            logger.info(response.toString());
            return response.getTaskId();
        } catch (ConnectionException e) {
            logger.error("The authentication is rejected. Check whether the AK/SK is correct: ", e);
        } catch (RequestTimeoutException e) {
            logger.error("The server processing times out and does not return a response: ", e);
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode:{}, RequestId:{}, ErrorCode:{}, ErrorMsg:{}",e.getHttpStatusCode(),e.getRequestId(),e.getErrorCode(),e.getErrorMsg());
        }
        return "";
    }

    /**
     * Query snapshot capturing tasks.
     * @param taskId: Task ID
     */
    public static void listThumbnailsTask(String taskId) {
        ListThumbnailsTaskRequest request = new ListThumbnailsTaskRequest();
        List<String> listRequestTaskId = new ArrayList<>();
        listRequestTaskId.add(taskId);
        request.withTaskId(listRequestTaskId);
        try {
            ListThumbnailsTaskResponse response = initMpcClient().listThumbnailsTask(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("The authentication is rejected. Check whether the AK/SK is correct: ", e);
        } catch (RequestTimeoutException e) {
            logger.error("The server processing times out and does not return a response: ", e);
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode:{}, RequestId:{}, ErrorCode:{}, ErrorMsg:{}",e.getHttpStatusCode(),e.getRequestId(),e.getErrorCode(),e.getErrorMsg());
        }
    }

    /**
     * Delete a snapshot capturing task.
     * @param taskId: Task ID
     */
    public static void deleteThumbnailsTask(String taskId) {
        DeleteThumbnailsTaskRequest request = new DeleteThumbnailsTaskRequest();
        request.withTaskId(taskId);
        try {
            DeleteThumbnailsTaskResponse response = initMpcClient().deleteThumbnailsTask(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("The authentication is rejected. Check whether the AK/SK is correct: ", e);
        } catch (RequestTimeoutException e) {
            logger.error("The server processing times out and does not return a response: ", e);
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode:{}, RequestId:{}, ErrorCode:{}, ErrorMsg:{}",e.getHttpStatusCode(),e.getRequestId(),e.getErrorCode(),e.getErrorMsg());
        }
    }

    public static void main(String[] args) {
        String taskId = createThumbnailsTask();
        deleteThumbnailsTask(taskId);
        listThumbnailsTask(taskId);
    }

}
