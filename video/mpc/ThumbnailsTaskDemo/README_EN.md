

## 1. Functions

You can integrate Media Processing Center (MPC) server SDKs provided by Huawei Cloud to call MPC APIs, making it easier for you to use the service. 
This sample shows how to use the Java SDK to create, cancel, and query a snapshot capturing task.

**What You Will Learn**

Using the Java SDK to create, cancel, and query a snapshot capturing task on MPC

## 2. Development Sequence Diagram
### Creating a Snapshot Capturing Task
![image](assets/create_en.png)

### Deleting a Snapshot Capturing Task
![image](assets/cancel_en.png)

### Querying Snapshot Capturing Tasks
![image](assets/query_en.png)

## 3. Prerequisites
- 1. You have[registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=5089ec193d9a4bedaa5bef6aa4d998fc&lang=zh-cn)with Huawei Cloud and passed[real-name authentication](https://support.huaweicloud.com/intl/zh-cn/usermanual-account/zh-cn_topic_0119621532.html).
- 2. The development environment (Java JDK 1.8 or later) is available.
- 3. You have obtained the access key (AK) and secret access key (SK) of the Huawei Cloud account. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/zh-cn/usermanual-ca/zh-cn_topic_0046606340.html).
- 4. You have obtained the project ID of the corresponding region of MPC. You can view the project ID on the **My Credentials** > **API Credentials** page of the Huawei Cloud console. For details, see[API Credentials](https://support.huaweicloud.com/intl/zh-cn/usermanual-ca/ca_01_0002.html)。
- 5. You have uploaded the media files to an OBS bucket in the [region](https://developer.huaweicloud.com/intl/zh-cn/endpoint)of MPC, and authorized MPC to access the OBS bucket. For details, see [Uploading Media Files](https://support.huaweicloud.com/intl/zh-cn/usermanual-mpc/mpc010002.html) and [Authorizing Access to Cloud Resources](https://support.huaweicloud.com/intl/zh-cn/usermanual-mpc/mpc010003.html).
## 4. Obtaining and Installing the SDK
You can use Maven to configure the dependent[MPC SDK](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/MPC?lang=Java)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-mpc</artifactId>
    <version>3.1.53</version>
</dependency>
```

## 5. API Parameter Description
For details about API parameters, see:

[a.Creating a Snapshot Capturing Task](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/MPC/doc?api=CreateThumbnailsTask)

[b.Canceling a Snapshot Capturing Task](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/MPC/doc?api=DeleteThumbnailsTask)

[c.Querying Snapshot Capturing Tasks](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/MPC/doc?api=ListThumbnailsTask)

## 6. Key Code Snippets
    Deliver a snapshot capturing task and obtain the task details.

### 6.1. Creating a Snapshot Capturing Task

```java
    /**
     * Create a snapshot capturing task.
     *
     * @return taskId: ID of the submitted snapshot capturing task.
     */
    public static String createThumbnailsTask() {
        // Set the input file path.
        ObsObjInfo input = new ObsObjInfo().withBucket("<example-bucket>").withLocation("<Region ID>").withObject("<example-path/input.mp4>");
        ObsObjInfo output = new ObsObjInfo().withBucket("<example-bucket>").withLocation("<Region ID>").withObject("<example-path/output>");
    
    
        // Create a snapshot capturing request.
        CreateThumbnailsTaskRequest request = new CreateThumbnailsTaskRequest();
        CreateThumbReq body = new CreateThumbReq();
        List<Integer> listThumbnailParaDots = new ArrayList<>();
        listThumbnailParaDots.add(50000);
        // Set the snapshot capturing type and capture a snapshot at a specified time point.
        ThumbnailPara thumbnailParabody = new ThumbnailPara();
        // Set the sampling type and capture a snapshot at a specified time point.
        thumbnailParabody.withType(ThumbnailPara.TypeEnum.fromValue("DOTS"))
        // Set the snapshot file name.
        .withOutputFilename("photo")
        // Set the interval for sampling snapshots.
        .withTime(10)
        // Set the start time for the sampling type to "TIME". This parameter is used together with "time".
        .withStartTime(100)
        // Capturing duration when the sampling type is set to "TIME". This parameter is used together with "time" and “start_time”, indicating that the first snapshot is captured at the time specified by “start_time” and snapshots
        // are captured at the interval specified by “time” until the duration specified by this parameter elapses. The unit is second.
        .withDuration(1)
        // Array of time points when a snapshot is captured.
        .withDots(listThumbnailParaDots)
        // Set the snapshot format.
        .withFormat(1)
        // Set the width of a snapshot.
        .withWidth(96)
        // Set the height of a snapshot.
        .withHeight(96);
    
        body.withThumbnailPara(thumbnailParabody);
        body.withOutput(output);
        body.withInput(input);
        request.withBody(body);
    
        // Send a snapshot capturing request.
        try {
        CreateThumbnailsTaskResponse response = initMpcClient().createThumbnailsTask(request);
        logger.info(response.toString());
        return response.getTaskId();
        } catch (ConnectionException e) {
        logger.error("The authentication is rejected. Check whether the AK/SK is correct: ", e);
        } catch (RequestTimeoutException e) {
        logger.error("The server processing times out and does not return a response: ", e);
        } catch (ServiceResponseException e) {
        logger.error("HttpStatusCode:{}, RequestId:{}, ErrorCode:{}, ErrorMsg:{}",e.getHttpStatusCode(),e.getRequestId(),e.getErrorCode(),e.getErrorMsg());
        }
        return "";
        }
```

### 6.2. Canceling a Snapshot Capturing Task

```java
    /**
     * Delete a snapshot capturing task.
     * @param taskId: Task ID
     */
    public static void deleteThumbnailsTask(String taskId) {
        DeleteThumbnailsTaskRequest request = new DeleteThumbnailsTaskRequest();
        request.withTaskId(taskId);
        try {
        DeleteThumbnailsTaskResponse response = initMpcClient().deleteThumbnailsTask(request);
        logger.info(response.toString());
        } catch (ConnectionException e) {
        logger.error("The authentication is rejected. Check whether the AK/SK is correct: ", e);
        } catch (RequestTimeoutException e) {
        logger.error("The server processing times out and does not return a response: ", e);
        } catch (ServiceResponseException e) {
        logger.error("HttpStatusCode:{}, RequestId:{}, ErrorCode:{}, ErrorMsg:{}",e.getHttpStatusCode(),e.getRequestId(),e.getErrorCode(),e.getErrorMsg());
        }
        }
```


### 6.3. Querying Snapshot Capturing Tasks

```java
    /**
     * Query snapshot capturing tasks.
     * @param taskId: Task ID
     */
    public static void listThumbnailsTask(String taskId) {
        ListThumbnailsTaskRequest request = new ListThumbnailsTaskRequest();
        List<String> listRequestTaskId = new ArrayList<>();
        listRequestTaskId.add(taskId);
        request.withTaskId(listRequestTaskId);
        try {
        ListThumbnailsTaskResponse response = initMpcClient().listThumbnailsTask(request);
        logger.info(response.toString());
        } catch (ConnectionException e) {
        logger.error("The authentication is rejected. Check whether the AK/SK is correct: ", e);
        } catch (RequestTimeoutException e) {
        logger.error("The server processing times out and does not return a response: ", e);
        } catch (ServiceResponseException e) {
        logger.error("HttpStatusCode:{}, RequestId:{}, ErrorCode:{}, ErrorMsg:{}",e.getHttpStatusCode(),e.getRequestId(),e.getErrorCode(),e.getErrorMsg());
        }
        }
```

## 7. Execution Results
**The ID of a created snapshot capturing task is returned.**
```json
{
  "task_id": "1024"
}
```
**Query the status and result of a snapshot capturing task.**
```json

    {
      "task_array" : [
        {
        "task_id" : 2528,
        "status" : "SUCCEEDED",
        "create_time" : 20201118121333,
        "end_time" : 20201118121336,
        "input" : {
          "bucket" : "example-bucket",
          "location" : "region01",
          "object" : "example-input.ts"
        },
        "output" : {
          "bucket" : "example-bucket",
          "location" : "region01",
          "object" : "example-output/example-path"
        },
        "thumbnail_info" : [ {
          "pic_name" : "9.jpg"
        }, {
          "pic_name" : "5.jpg"
        } ]
      } 
      ],
      "is_truncated" : 0,
      "total" : 1
    }
```
**Cancel a delivered snapshot capturing task.**

When a snapshot capturing task is canceled, the status code is 204 and the response body is empty.

When a snapshot capturing task fails to be canceled, the status code is 400 and the response body is as follows:
```json
        {
           "error_code": "MPC.363011005",
           "error_msg": "Can not find the taskID"
        }
```

## 8. Reference
The code project in this sample is used only for demonstration. During actual development, strictly follow the development guide.
Learn more details from [Development Guide](https://support.huaweicloud.com/intl/zh-cn/api-mpc/mpc_04_0034.html).