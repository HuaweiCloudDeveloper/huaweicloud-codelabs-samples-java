

## 1、功能介绍

华为云提供了媒体处理服务端SDK，您可以直接集成服务端SDK来调用媒体处理的相关API，从而实现对媒体处理的快速操作。 该示例展示了如何通过java版SDK创建、取消和查询截图任务。

**您将学到什么？**

如何通过java版SDK来体验媒体处理(MPC)创建、取消和查询截图任务功能

## 2、开发时序图
### 创建截图任务
![image](assets/create.jpg)

### 取消截图任务
![image](assets/cancel.jpg)

### 查询截图任务
![image](assets/query.jpg)

## 3、前置条件
- 1、已[注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=40c99ba3cea14417a2428c756a626599&lang=zh-cn)华为帐号并开通华为云，已进行[实名认证](https://support.huaweicloud.com/usermanual-account/zh-cn_topic_0077914254.html)。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、已获取账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 4、已获取转码服务对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见[API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html)。
- 5、已将需要处理的媒资文件上传至MPC同[区域](https://developer.huaweicloud.com/endpoint?MPC)的OBS桶中，并将OBS桶进行授权，允许MPC访问。具体请参见[上传音视频文件](https://support.huaweicloud.com/usermanual-mpc/mpc010002.html)和[获取云资源授权](https://support.huaweicloud.com/usermanual-mpc/mpc010003.html)。

## 4、SDK获取和安装
您可以通过Maven配置所依赖的[媒体处理服务SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/MPC?lang=Java)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-mpc</artifactId>
    <version>3.1.53</version>
</dependency>
```

## 5、接口参数说明
关于接口参数的详细说明可参见：

[a.新建截图任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MPC/doc?api=CreateThumbnailsTask)

[b.取消截图任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MPC/doc?api=DeleteThumbnailsTask)

[c.查询截图任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MPC/doc?api=ListThumbnailsTask)

## 6、关键代码片段

### 6.1、创建截图任务

```java
    /**
     * 创建截图任务
     *
     * @return taskId 提交的截图任务的任务ID
     */
    public static String createThumbnailsTask() {
        // 设置截图输入视频地址
        ObsObjInfo input = new ObsObjInfo().withBucket("<example-bucket>").withLocation("<Region ID>").withObject("<example-path/input.mp4>");
        ObsObjInfo output = new ObsObjInfo().withBucket("<example-bucket>").withLocation("<Region ID>").withObject("<example-path/output>");
    
    
        // 创建截图请求
        CreateThumbnailsTaskRequest request = new CreateThumbnailsTaskRequest();
        CreateThumbReq body = new CreateThumbReq();
        List<Integer> listThumbnailParaDots = new ArrayList<>();
        listThumbnailParaDots.add(50000);
        // 设置截图类型,此处理按时间点截图
        ThumbnailPara thumbnailParabody = new ThumbnailPara();
        // 设置采样类型，指定时间点截图
        thumbnailParabody.withType(ThumbnailPara.TypeEnum.fromValue("DOTS"))
            // 设置截图输出文件名称
            .withOutputFilename("photo")
            // 设置采样截图的时间间隔值
            .withTime(10)
            // 设置采样类型为“TIME”模式的开始时间，和“time”配合使用
            .withStartTime(100)
            // 采样类型为“TIME”模式的持续时间，和“time”、“start_time”配合使用，表示从视频文件的第“start_time”开始，持续时间为“duration”，每间隔“time”生成一张截图，单位：秒。
            .withDuration(1)
            // 指定时间截图的时间点数组
            .withDots(listThumbnailParaDots)
            // 设置截图文件格式
            .withFormat(1)
            // 设置截图的宽
            .withWidth(96)
            // 设置截图的高
            .withHeight(96);
    
        body.withThumbnailPara(thumbnailParabody);
        body.withOutput(output);
        body.withInput(input);
        request.withBody(body);
    
        // 发送截图请求
        try {
            CreateThumbnailsTaskResponse response = initMpcClient().createThumbnailsTask(request);
            logger.info(response.toString());
            return response.getTaskId();
        } catch (ConnectionException e) {
            logger.error("The authentication is rejected. Check whether the AK/SK is correct: ", e);
        } catch (RequestTimeoutException e) {
            logger.error("The server processing times out and does not return a response: ", e);
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode:{}, RequestId:{}, ErrorCode:{}, ErrorMsg:{}",e.getHttpStatusCode(),e.getRequestId(),e.getErrorCode(),e.getErrorMsg());
        }
            return "";
        }
```

### 6.2、取消截图任务

```java
    /**
     * 取消截图任务
     * @param taskId 任务ID
     */
    public static void deleteThumbnailsTask(String taskId) {
        DeleteThumbnailsTaskRequest request = new DeleteThumbnailsTaskRequest();
        request.withTaskId(taskId);
        try {
            DeleteThumbnailsTaskResponse response = initMpcClient().deleteThumbnailsTask(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("The authentication is rejected. Check whether the AK/SK is correct: ", e);
        } catch (RequestTimeoutException e) {
            logger.error("The server processing times out and does not return a response: ", e);
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode:{}, RequestId:{}, ErrorCode:{}, ErrorMsg:{}",e.getHttpStatusCode(),e.getRequestId(),e.getErrorCode(),e.getErrorMsg());
        }
        }
```


### 6.3、查询截图任务

```java
    /**
     * 查询截图任务
     * @param taskId 任务ID
     */
    public static void listThumbnailsTask(String taskId) {
        ListThumbnailsTaskRequest request = new ListThumbnailsTaskRequest();
        List<String> listRequestTaskId = new ArrayList<>();
        listRequestTaskId.add(taskId);
        request.withTaskId(listRequestTaskId);
        try {
            ListThumbnailsTaskResponse response = initMpcClient().listThumbnailsTask(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("The authentication is rejected. Check whether the AK/SK is correct: ", e);
        } catch (RequestTimeoutException e) {
            logger.error("The server processing times out and does not return a response: ", e);
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode:{}, RequestId:{}, ErrorCode:{}, ErrorMsg:{}",e.getHttpStatusCode(),e.getRequestId(),e.getErrorCode(),e.getErrorMsg());
        }
        }
```

## 7、运行结果
**创建截图任务，返回截图任务ID**
```json
{
  "task_id": "1024"
}
```
**查询解截图任务的状态和结果**
```json

    {
      "task_array" : [
        {
        "task_id" : 2528,
        "status" : "SUCCEEDED",
        "create_time" : 20201118121333,
        "end_time" : 20201118121336,
        "input" : {
          "bucket" : "example-bucket",
          "location" : "region01",
          "object" : "example-input.ts"
        },
        "output" : {
          "bucket" : "example-bucket",
          "location" : "region01",
          "object" : "example-output/example-path"
        },
        "thumbnail_info" : [ {
          "pic_name" : "9.jpg"
        }, {
          "pic_name" : "5.jpg"
        } ]
      } 
      ],
      "is_truncated" : 0,
      "total" : 1
    }
```
**取消已下发的截图任务**

取消截图任务成功时，状态码：204，响应体为空。

取消截图任务失败时，状态码：400，响应体如下：
```json
        {
           "error_code": "MPC.363011005",
           "error_msg": "Can not find the taskID"
        }
```

## 8、参考
本示例的代码工程仅用于简单演示，实际开发过程中应严格遵循开发指南。访问以下链接可以获取详细信息：[开发指南](https://support.huaweicloud.com/sdkreference-mpc/mpc_05_0111.html)