package com.huawei.hwclouds.live;

import com.huaweicloud.sdk.live.v1.LiveClient;
import com.huaweicloud.sdk.live.v1.model.RecordIndexRequestBody;
import com.huaweicloud.sdk.live.v1.model.CreateRecordIndexRequest;
import com.huaweicloud.sdk.live.v1.model.CreateRecordIndexResponse;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.OffsetDateTime;

public class RecordIndexes {
    private static final Logger LOGGER = LoggerFactory.getLogger(RecordIndexes.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String iamEndpoint = "<iam endpoint>";
        String region = "<region>";
        String endpoint = "<endpoint>";
        String pushDomain = "your publish domain name";
        String appName = "your app name";
        String streamName = "your stream name";
        String startTime = "file start time";
        String endTime = "file end time";
        OffsetDateTime fileStartTime = OffsetDateTime.parse(startTime);
        OffsetDateTime fileEndTime = OffsetDateTime.parse(endTime);
        String object = "{publish_domain}/{app}/{stream}-{start_time}-{end_time}";

        BasicCredentials auth = new BasicCredentials().withIamEndpoint(iamEndpoint).withAk(ak).withSk(sk);
        LiveClient client = LiveClient.newBuilder().withCredential(auth).withRegion(new Region(region, endpoint)).build();
        RecordIndexRequestBody recordIndex = new RecordIndexRequestBody();
        recordIndex.setPublishDomain(pushDomain);
        recordIndex.setApp(appName);
        recordIndex.setStream(streamName);
        recordIndex.setStartTime(fileStartTime);
        recordIndex.setEndTime(fileEndTime);
        recordIndex.setObject(object);
        String result = createRecordIndexes(client, recordIndex);
        System.out.println(result);
    }

    public static String createRecordIndexes(LiveClient client, RecordIndexRequestBody recordIndex) {
        CreateRecordIndexRequest request = new CreateRecordIndexRequest();
        request.withBody(recordIndex);
        try {
            CreateRecordIndexResponse response = client.createRecordIndex(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode != 201) {
                return "create record indexes failed";
            }
        } catch (ConnectionException e) {
            LOGGER.error("createRecordIndexes ConnectionException", e);
        } catch (RequestTimeoutException e) {
            LOGGER.error("createRecordIndexes RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            LOGGER.error("createRecordIndexes ServiceResponseException", e);
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return "create record indexes success";
    }
}