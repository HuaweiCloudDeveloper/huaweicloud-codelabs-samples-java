## 1. 示例简介

视频直播服务（Live）是在华为云提供的可信，开放，全球服务直播内容接入与分发网络的基础上，将华为公司在视频领域的多年技术积累和电信级运营和运维能力开放出来，构建了便捷接入、高清流畅、低延迟、高并发的一站式视频直播解决方案。
本示例指导用户创建录制视频索引文件。

## 2. 开发前准备

- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register)
  华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已具备开发环境 ，支持java8及以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见[访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)。
- 已获取直播服务对应区域的项目ID，请在华为云控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见[API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html)。

## 3. 安装SDK

视频直播服务端Java SDK支持java8及以上版本。执行“ java -version” 检查当前java的版本信息。

使用服务端SDK前，您需要安装“huaweicloud-sdk-live”，具体的SDK版本号请参见 [SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。

## 4. 代码示例
### 4.1 导入pom依赖
``` java
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-live</artifactId>
    <version>${sdk-version}</version>
</dependency>
```
### 4.2 导入依赖模块
``` java
import com.huaweicloud.sdk.live.v1.LiveClient;
import com.huaweicloud.sdk.live.v1.model.RecordIndexRequestBody;
import com.huaweicloud.sdk.live.v1.model.CreateRecordIndexRequest;
import com.huaweicloud.sdk.live.v1.model.CreateRecordIndexResponse;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import java.time.OffsetDateTime;
```

### 4.3 初始化认证信息, 及客户端

示例相关参数说明如下所示：

- ak：华为云账号Access Key。
- sk：华为云账号Secret Access Key。
- iamEndpoint：iam身份认证域名。
- region: 服务所在区域。
- endpoint：接口调用域名。
- pushDomain：推流域名。
- appName：应用名。
- streamName：流名。
- startTime：开始时间。格式为：YYYY-MM-DDTHH:mm:ssZ（UTC时间），开始时间与结束时间的间隔最大为12小时。开始时间不允许晚于当前时间。
- endTime：结束时间。格式为：YYYY-MM-DDTHH:mm:ssZ（UTC时间），开始时间与结束时间的间隔最大为12小时。结束时间必须晚于开始时间。
- object：m3u8文件在OBS中的储存路径。支持下列字符串的转义
  {publish_domain}
  {app}
  {stream}
  {start_time}
  {end_time}
  {start_time_unix}
  {end_time_unix}
  其中{start_time},{end_time},{start_time_unix},{end_time_unix}为返回结果的实际时间。 默认值为Index/{publish_domain}/{app}/{stream}/{stream}-{start_time}-{end_time}

``` java
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String iamEndpoint = "<iam endpoint>";
        String region = "<region>";
        String endpoint = "<endpoint>";
        String pushDomain = "your publish domain name";
        String appName = "your app name";
        String streamName = "your stream name";
        String startTime = "file start time";
        String endTime = "file end time";
        OffsetDateTime fileStartTime = OffsetDateTime.parse(startTime);
        OffsetDateTime fileEndTime = OffsetDateTime.parse(endTime);
        String object = "{publish_domain}/{app}/{stream}-{start_time}-{end_time}";

        BasicCredentials auth = new BasicCredentials().withIamEndpoint(iamEndpoint).withAk(ak).withSk(sk);
        LiveClient client = LiveClient.newBuilder().withCredential(auth).withRegion(new Region(region, endpoint)).build();
        RecordIndexRequestBody recordIndex = new RecordIndexRequestBody();
        recordIndex.setPublishDomain(pushDomain);
        recordIndex.setApp(appName);
        recordIndex.setStream(streamName);
        recordIndex.setStartTime(fileStartTime);
        recordIndex.setEndTime(fileEndTime);
        recordIndex.setObject(object);
        String result = createRecordIndexes(client, recordIndex);
        System.out.println(result);
    }
```

#### 4.3.1 创建录制视频索引文件

示例相关参数说明如下所示：

- client：客户端。
- recordIndex：录制索引文件参数。

``` java
    public static String createRecordIndexes(LiveClient client, RecordIndexRequestBody recordIndex) {
        CreateRecordIndexRequest request = new CreateRecordIndexRequest();
        request.withBody(recordIndex);
        try {
            CreateRecordIndexResponse response = client.createRecordIndex(request);
            int resultCode = response.getHttpStatusCode();
            if (resultCode != 201) {
                return "create record indexes failed";
            }
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return "create record indexes success";
    }
```

## 5. 参考

更多信息请参考 [视频直播Live文档](https://support.huaweicloud.com/live/index.html)

## 6. 修订记录

|    发布日期    | 文档版本 |  修订说明  |
|:----------:|:----:|:------:|
| 2023-10-18 | 1.0  | 文档首次发布 |