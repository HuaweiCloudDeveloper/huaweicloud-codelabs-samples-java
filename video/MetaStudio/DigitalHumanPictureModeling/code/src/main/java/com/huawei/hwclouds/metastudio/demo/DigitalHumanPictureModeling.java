package com.huawei.hwclouds.metastudio.demo;

import com.huaweicloud.sdk.metastudio.v1.region.MetaStudioRegion;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.FormDataFilePart;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.metastudio.v1.MetaStudioClient;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreatePictureModelingJobResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CreatePictureModelingJobRequestBody;
import com.huaweicloud.sdk.metastudio.v1.model.CreatePictureModelingJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ShowPictureModelingJobResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ShowPictureModelingJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreatePictureModelingByUrlJobResponse;
import com.huaweicloud.sdk.metastudio.v1.model.PictureModelingByUrlReq;
import com.huaweicloud.sdk.metastudio.v1.model.CreatePictureModelingByUrlJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListPictureModelingJobsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListPictureModelingJobsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ListStylesRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListStylesResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class DigitalHumanPictureModeling {

    private static final Logger logger = LoggerFactory.getLogger(DigitalHumanPictureModeling.class);

    // 获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID和您的AK/SK。
    // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK、HUAWEICLOUD_SDK_SK。

    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");

    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    public static void main(String[] args) throws IOException {
        ICredential auth = new BasicCredentials()
                .withAk(AK)
                .withSk(SK);

        // 使用默认配置
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        // 创建MetaStudio实例
        MetaStudioClient mClient = MetaStudioClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(MetaStudioRegion.CN_NORTH_4) // region为上海一需写为：withEndpoint("cn-east-3")
                .build();

        // 1,照片建模任务列表查询
        listPictureModelingJobs(mClient);
        // 2,查询数字人风格列表
        String style_id = listStyles(mClient);
        // 3,基于图片URL创建照片建模任务
        String url = getAssetFileInfo(getAsset(mClient, AssetTypeEnum.IMAGE, AssetSourceEnum.SYSTEM, null)).getDownloadUrl();
        String job_id = createPictureModelingByUrlJob(mClient, style_id, url);
        // 4,照片建模任务详情查询
        showPictureModelingJob(mClient, job_id);
        // 5,创建照片建模任务
        String path = "female.jpg"; // 此处设置您的图片路径
        createPictureModelingJob(mClient, path);

    }

    /**
     * 照片建模任务列表查询
     *
     * @param client
     */
    private static void listPictureModelingJobs(MetaStudioClient client) {
        logger.info("listPictureModelingJobs start");
        ListPictureModelingJobsRequest request = new ListPictureModelingJobsRequest();
        try {
            ListPictureModelingJobsResponse response = client.listPictureModelingJobs(request);
            logger.info("listPictureModelingJobs" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listPictureModelingJobs ClientRequestException" + e.getHttpStatusCode());
            logger.error("listPictureModelingJobs ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("listPictureModelingJobs ServerResponseException" + e.getHttpStatusCode());
            logger.error("listPictureModelingJobs ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 查询数字人风格列表
     *
     * @param client
     */
    private static String listStyles(MetaStudioClient client) {
        logger.info("listStyles start");
        ListStylesRequest request = new ListStylesRequest();
        String style_id = "";
        try {
            ListStylesResponse response = client.listStyles(request);
            logger.info("listStyles " + response.toString());
            style_id = response.getStyles().get(0).getStyleId();
        } catch (ClientRequestException ex) {
            logger.error("listStyles ClientRequestException " + ex.getHttpStatusCode());
            logger.error("listStyles ClientRequestException " + ex.getMessage());
        } catch (ServerResponseException e) {
            logger.error("listStyles ServerResponseException " + e.getHttpStatusCode());
            logger.error("listStyles ServerResponseException " + e.getMessage());
        }
        return style_id;
    }

    /**
     * 基于图片URL创建照片建模任务
     *
     */
    private static String createPictureModelingByUrlJob(MetaStudioClient client, String style_id, String url) {
        logger.info("createPictureModelingByUrlJob start");
        CreatePictureModelingByUrlJobRequest request = new CreatePictureModelingByUrlJobRequest()
                .withBody(new PictureModelingByUrlReq()
                        .withPictureUrl(url)
                        .withStyleId(style_id)
                        .withName("<your text>"));
        String job_id = "";
        try {
            CreatePictureModelingByUrlJobResponse response = client.createPictureModelingByUrlJob(request);
            logger.info("createPictureModelingByUrlJob" + response.toString());
            job_id = response.getJobId();
        } catch (ClientRequestException e) {
            logger.error("createPictureModelingByUrlJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("createPictureModelingByUrlJob ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("createPictureModelingByUrlJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("createPictureModelingByUrlJob ServerResponseException" + e.getMessage());
        }
        return job_id;
    }

    /**
     * 照片建模任务详情查询
     *
     */
    private static void showPictureModelingJob(MetaStudioClient client, String job_id) {
        logger.info("showPictureModelingJob start");
        ShowPictureModelingJobRequest request = new ShowPictureModelingJobRequest()
                .withJobId(job_id);
        try {
            ShowPictureModelingJobResponse response = client.showPictureModelingJob(request);
            logger.info("showPictureModelingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showPictureModelingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("showPictureModelingJob ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("showPictureModelingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("showPictureModelingJob ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 创建照片建模任务
     *
     */
    private static void createPictureModelingJob(MetaStudioClient client, String path) throws FileNotFoundException {
        logger.info("createPictureModelingJob start");
        try (FileInputStream inputStream =  new FileInputStream(new File(path))) {
            CreatePictureModelingJobRequest request = new CreatePictureModelingJobRequest()
                    .withBody(new CreatePictureModelingJobRequestBody()
                            .withStyleId("system_female_001")
                            .withName("<your text>")
                            .withFile(new FormDataFilePart(inputStream, path)));
            try {
                CreatePictureModelingJobResponse response = client.createPictureModelingJob(request);
                logger.info("createPictureModelingJob" + response.toString());
            } catch (ClientRequestException e) {
                logger.error("createPictureModelingJob ClientRequestException" + e.getHttpStatusCode());
                logger.error("createPictureModelingJob ClientRequestException" + e.getMessage());
            } catch (ServerResponseException e) {
                logger.error("createPictureModelingJob ServerResponseException" + e.getHttpStatusCode());
                logger.error("createPictureModelingJob ServerResponseException" + e.getMessage());
            }
        } catch (IOException ex) {
            logger.error("file input stream open failed");
        }
    }

    // 从资产中获取背景图
    public static AssetFileInfo getAssetFileInfo(DigitalAssetInfo assetInfo) {
        for (AssetFileInfo assetFileInfo : assetInfo.getFiles()) {
            if (assetFileInfo.getAssetFileCategory().equals("MAIN")) {
                return assetFileInfo;
            }
        }
        return null;
    }

    // 获取资产
    private static DigitalAssetInfo getAsset(MetaStudioClient client, DigitalAssetInfo.AssetTypeEnum assetType, ListAssetsRequest.AssetSourceEnum assetSource, String systemProperty) {
        DigitalAssetInfo digitalAssetInfo = null;
        try {
            ListAssetsRequest listAssetsRequest = new ListAssetsRequest()
                    .withAssetType(String.valueOf(assetType)) // 设置资产类型
                    .withSystemProperty(systemProperty) // 设置系统属性
                    .withAssetSource(assetSource) // 设置资产来源：系统资产
                    .withAssetState(String.valueOf(DigitalAssetInfo.AssetStateEnum.ACTIVED) // 主文件上传成功，资产激活。
                    );
            logger.info("ListAssetsRequest  " + listAssetsRequest.toString());
            ListAssetsResponse response = client.listAssets(listAssetsRequest);
            digitalAssetInfo = response.getAssets().get(0);
        } catch (ClientRequestException e) {
            logger.error("getAsset ClientRequestException" + e.getHttpStatusCode());
            logger.error("getAsset ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("getAsset ServerResponseException" + e.getHttpStatusCode());
            logger.error("getAsset ServerResponseException" + e.getMessage());
        }
        return digitalAssetInfo;
    }


}
