

## 1、功能介绍

华为云提供了MetaStudio服务端SDK，您可以直接集成服务端SDK来调用MetaStudio的相关API，从而实现对MetaStudio的快速操作。

**您将学到什么？**

该场景示例代码以数字人风格化照片建模为例，介绍如何使用MetaStudio Java SDK创建一个风格化照片建模任务。 

建模生成的3D数字人模型可以用于后续视频制作和视频直播。

## 2、开发时序图

![image](assets/DigitalHumanPictureModeling.jpg)


## 3、前置条件
- 1、已[注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=40c99ba3cea14417a2428c756a626599&lang=zh-cn)华为帐号并开通华为云，已进行[实名认证](https://support.huaweicloud.com/usermanual-account/zh-cn_topic_0077914254.html)。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、已获取账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 4、已获取MetaStudio服务对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见[API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html)。 

## 4、SDK获取和安装
您可以通过Maven配置所依赖的[数字内容生产线SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/MetaStudio?lang=Java)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-metastudio</artifactId>
    <version>3.1.78</version>
</dependency>

```

## 5、接口参数说明
关于接口参数的详细说明可参见：

[a.查询数字人风格列表](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ListStyles)

[b.创建照片建模任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreatePictureModelingJob)

[c.照片建模任务列表查询](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ListPictureModelingJobs)

[d.照片建模任务详情查询](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowPictureModelingJob)

[e.基于图片URL创建照片建模任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreatePictureModelingByUrlJob)


## 6、关键代码片段

### 6.1、查询数字人风格列表
```java
/**
 * 查询数字人风格列表
 *
 * @param client
 */
private static String listStyles(MetaStudioClient client) {
    logger.info("listStyles start");
    ListStylesRequest request = new ListStylesRequest();
    String style_id = "";
    try {
        ListStylesResponse response = client.listStyles(request);
        logger.info("listStyles" + response.toString());
        style_id = response.getStyles().get(0).getStyleId();
    } catch (ClientRequestException e) {
        logger.error("listStyles ClientRequestException" + e.getHttpStatusCode());
        logger.error("listStyles ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("listStyles ServerResponseException" + e.getHttpStatusCode());
        logger.error("listStyles ServerResponseException" + e.getMessage());
    }
    return style_id;
}
```

### 6.2、创建照片建模任务
```java
/**
 * 创建照片建模任务
 *
 */
private static void createPictureModelingJob(MetaStudioClient client, String path) throws FileNotFoundException {
    logger.info("createPictureModelingJob start");
    File file = new File(path);
    FileInputStream inputStream =  new FileInputStream(file);
    CreatePictureModelingJobRequest request = new CreatePictureModelingJobRequest()
            .withBody(new CreatePictureModelingJobRequestBody()
                    .withStyleId("system_female_001")
                    .withName("<your text>")
                    .withFile(new FormDataFilePart(inputStream, path)));
    try {
        CreatePictureModelingJobResponse response = client.createPictureModelingJob(request);
        logger.info("createPictureModelingJob" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("createPictureModelingJob ClientRequestException" + e.getHttpStatusCode());
        logger.error("createPictureModelingJob ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("createPictureModelingJob ServerResponseException" + e.getHttpStatusCode());
        logger.error("createPictureModelingJob ServerResponseException" + e.getMessage());
    }
}
```

### 6.3、照片建模任务列表查询
```java
/**
 * 照片建模任务列表查询
 *
 * @param client
 */
private static void listPictureModelingJobs(MetaStudioClient client) {
    logger.info("listPictureModelingJobs start");
    ListPictureModelingJobsRequest request = new ListPictureModelingJobsRequest();
    try {
        ListPictureModelingJobsResponse response = client.listPictureModelingJobs(request);
        logger.info("listPictureModelingJobs" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("listPictureModelingJobs ClientRequestException" + e.getHttpStatusCode());
        logger.error("listPictureModelingJobs ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("listPictureModelingJobs ServerResponseException" + e.getHttpStatusCode());
        logger.error("listPictureModelingJobs ServerResponseException" + e.getMessage());
    }
}
```

### 6.4、照片建模任务详情查询
```java
/**
 * 照片建模任务详情查询
 *
 */
private static void showPictureModelingJob(MetaStudioClient client, String job_id) {
    logger.info("showPictureModelingJob start");
    ShowPictureModelingJobRequest request = new ShowPictureModelingJobRequest()
            .withJobId(job_id);
    try {
        ShowPictureModelingJobResponse response = client.showPictureModelingJob(request);
        logger.info("showPictureModelingJob" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("showPictureModelingJob ClientRequestException" + e.getHttpStatusCode());
        logger.error("showPictureModelingJob ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("showPictureModelingJob ServerResponseException" + e.getHttpStatusCode());
        logger.error("showPictureModelingJob ServerResponseException" + e.getMessage());
    }
}
```

### 6.5、基于图片URL创建照片建模任务
```java
 /**
 * 基于图片URL创建照片建模任务
 *
 */
private static String createPictureModelingByUrlJob(MetaStudioClient client, String style_id, String url) {
    logger.info("createPictureModelingByUrlJob start");
    CreatePictureModelingByUrlJobRequest request = new CreatePictureModelingByUrlJobRequest()
            .withBody(new PictureModelingByUrlReq()
                    .withPictureUrl(url)
                    .withStyleId(style_id)
                    .withName("<your text>"));
    String job_id = "";
    try {
        CreatePictureModelingByUrlJobResponse response = client.createPictureModelingByUrlJob(request);
        logger.info("createPictureModelingByUrlJob" + response.toString());
        job_id = response.getJobId();
    } catch (ClientRequestException e) {
        logger.error("createPictureModelingByUrlJob ClientRequestException" + e.getHttpStatusCode());
        logger.error("createPictureModelingByUrlJob ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("createPictureModelingByUrlJob ServerResponseException" + e.getHttpStatusCode());
        logger.error("createPictureModelingByUrlJob ServerResponseException" + e.getMessage());
    }
    return job_id;
}
```

## 7、运行结果
**照片建模任务列表查询**
```json
{
  "X-Request-Id":"bcd0fe***",
  "count":10,"picture_modeling_jobs":[
    {
      "error_info":{

      },"job_id":"bc8ebc***",
      "name":"aera4ra34",
      "state":"SUCCEED",
      "model_cover_url":"",
      "style_id":"system_female_001"
    },{
      "error_info":{},"job_id":"a9f264***",
      "name":"<your name>",
      "state":{
        "$ref":"$.picture_modeling_jobs[0].error_info.state"
      },"model_cover_url":"",
      "style_id":"system_female_001"
    }
  ]
}
```

**查询数字人风格列表**
```json
{
	"X-Request-Id":"65af5b***",
	"count":9,"styles":[
		{
			"update_time":"20230424081130",
			"create_time":"20230314030232",
			"extra_meta":{
				"edit_enable":true,
                "picture_modeling_enable":true,
                "model_id":"001"
			},"project_id":"SYSTEM",
			"sex":"MALE",
			"name":"system_male_001",
			"description":"男性风格",
			"state":"PUBLISHED",
			"style_assets":[
				{
					"cover_url":"https://***",
					"asset_type":"MATERIAL",
					"asset_id":"4d4b44***"
				}
			],
			"style_id":"system_male_001",
			"tags":[
				"男性"
			]
		}
	]
}
```

**基于图片URL创建照片建模任务**
```json
{
	"model_asset_id":"853721***",
	"X-Request-Id":"1b1126***",
	"job_id":"e97e6f***"
}
```

**照片建模任务详情查询**
```json
{
	"model_asset_id":"853721***",
	"error_info":{
		"error_msg":"",
		"error_code":""
	},"X-Request-Id":"5f5d97***",
	"job_id":"e97e6f***",
	"name":"name",
	"state":"WAITING",
	"model_cover_url":"",
	"style_id":"system_male_001"
}
```

**创建照片建模任务**
```json
{
	"model_asset_id":"6fb15e***",
	"X-Request-Id":"03e258***",
	"job_id":"f076ca***"
}
```

## 8、参考
本示例的代码工程仅用于简单演示，实际开发过程中应严格遵循开发指南。访问以下链接可以获取详细信息：[开发指南](https://support.huaweicloud.com/ssdk-metastudio/metastudio_06_0000.html)