package com.huawei.hwclouds.metastudio.demo;

import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsRequest.AssetSourceEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.metastudio.v1.MetaStudioClient;
import com.huaweicloud.sdk.metastudio.v1.model.AssetFileInfo;
import com.huaweicloud.sdk.metastudio.v1.model.BackgroundConfigInfo;
import com.huaweicloud.sdk.metastudio.v1.model.CreateVideoScriptsReq;
import com.huaweicloud.sdk.metastudio.v1.model.CreateVideoScriptsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreateVideoScriptsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteVideoScriptRequest;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteVideoScriptResponse;
import com.huaweicloud.sdk.metastudio.v1.model.DigitalAssetInfo;
import com.huaweicloud.sdk.metastudio.v1.model.ImageLayerConfig;
import com.huaweicloud.sdk.metastudio.v1.model.LayerConfig;
import com.huaweicloud.sdk.metastudio.v1.model.LayerPositionConfig;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ListVideoScriptsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListVideoScriptsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ShootScript;
import com.huaweicloud.sdk.metastudio.v1.model.ShootScriptItem;
import com.huaweicloud.sdk.metastudio.v1.model.ShowVideoScriptRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ShowVideoScriptResponse;
import com.huaweicloud.sdk.metastudio.v1.model.TextConfig;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateVideoScriptRequest;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateVideoScriptResponse;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateVideoScriptsReq;
import com.huaweicloud.sdk.metastudio.v1.model.VideoConfig;
import com.huaweicloud.sdk.metastudio.v1.model.VoiceConfig;
import com.huaweicloud.sdk.metastudio.v1.model.CopyVideoScriptsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CopyVideoScriptsResponse;
import com.huaweicloud.sdk.metastudio.v1.region.MetaStudioRegion;

public class VideoScriptDemo {
    private static final Logger logger = LoggerFactory.getLogger(VideoScriptDemo.class);

    // 获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID和您的AK/SK。
    // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK、HUAWEICLOUD_SDK_SK。
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");

    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    public static void main(String[] args) throws IOException {
        ICredential auth = new BasicCredentials()
                .withAk(AK)
                .withSk(SK);

        // 使用默认配置
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        // 创建MetaStudio实例
        MetaStudioClient mClient = MetaStudioClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(MetaStudioRegion.CN_NORTH_4) // region为上海一需写为：withEndpoint("cn-east-3")
                .build();

        // 1,创建视频制作剧本
        String scriptId = createVideoScripts(mClient);

        // 2,查询视频制作剧本列表
        listVideoScripts(mClient);

        // 3,查询视频制作剧本详情
        showVideoScript(mClient, scriptId);

        // 4,更新视频制作剧本
        updateVideoScript(mClient, scriptId);

        // 5,复制视频制作剧本
        copyVideoScripts(mClient, scriptId);

        // 6,删除视频制作剧本
        deleteVideoScript(mClient, scriptId);
    }

    /**
     * 创建视频制作剧本
     *
     */
    public static String createVideoScripts(MetaStudioClient client) {
        logger.info("createVideoScripts start");
        String jobId = null;
        try {
            CreateVideoScriptsRequest request = new CreateVideoScriptsRequest()
                .withBody(new CreateVideoScriptsReq()
                    .withVideoConfig(buildVideoConfig())
                    .withVoiceConfig(buildVoiceConfig(client))
                    .withShootScripts(buildLiveShootScriptItems(client))
                    .withScriptName("<your script name>"));
            CreateVideoScriptsResponse response = client.createVideoScripts(request);
            jobId = response.getScriptId();
            logger.info("createVideoScripts " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createVideoScripts ClientRequestException " + e.getHttpStatusCode());
            logger.error("createVideoScripts ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("createVideoScripts ServerResponseException " + e.getHttpStatusCode());
            logger.error("createVideoScripts ServerResponseException " + e.getMessage());
        }
        return jobId;
    }

    /**
     * 查询视频制作剧本列表
     *
     */
    private static void listVideoScripts(MetaStudioClient client) {
        logger.info("listVideoScripts start");
        ListVideoScriptsRequest request = new ListVideoScriptsRequest();
        try {
            ListVideoScriptsResponse response = client.listVideoScripts(request);
            logger.info("listVideoScripts " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listVideoScripts ClientRequestException " + e.getHttpStatusCode());
            logger.error("listVideoScripts ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("listVideoScripts ServerResponseException " + e.getHttpStatusCode());
            logger.error("listVideoScripts ServerResponseException " + e.getMessage());
        }
    }

    /**
     * 查询视频制作剧本详情
     *
     */
    private static void showVideoScript(MetaStudioClient client, String scriptId) {
        logger.info("showVideoScript start");
        ShowVideoScriptRequest request = new ShowVideoScriptRequest().withScriptId(scriptId);
        try {
            ShowVideoScriptResponse response = client.showVideoScript(request);
            logger.info("showVideoScript " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showVideoScript ClientRequestException " + e.getHttpStatusCode());
            logger.error("showVideoScript ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("showVideoScript ServerResponseException " + e.getHttpStatusCode());
            logger.error("showVideoScript ServerResponseException " + e.getMessage());
        }
    }

    /**
     * 删除视频制作剧本
     *
     */
    private static void deleteVideoScript(MetaStudioClient client, String scriptId) {
        logger.info("deleteVideoScript start");
        DeleteVideoScriptRequest request = new DeleteVideoScriptRequest().withScriptId(scriptId);
        try {
            DeleteVideoScriptResponse response = client.deleteVideoScript(request);
            logger.info("deleteVideoScript" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("deleteVideoScript ClientRequestException" + e.getHttpStatusCode());
            logger.error("deleteVideoScript ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("deleteVideoScript ServerResponseException" + e.getHttpStatusCode());
            logger.error("deleteVideoScript ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 更新视频制作剧本
     *
     */
    public static void updateVideoScript(MetaStudioClient client, String scriptId) {
        logger.info("updateVideoScript start");
        try {
            UpdateVideoScriptsReq req = new UpdateVideoScriptsReq()
                .withVideoConfig(buildVideoConfig())
                .withVoiceConfig(buildVoiceConfig(client))
                .withShootScripts(buildLiveShootScriptItems(client))
                .withScriptName("name");
            UpdateVideoScriptRequest request = new UpdateVideoScriptRequest()
                .withScriptId(scriptId)
                .withBody(req);
            UpdateVideoScriptResponse response = client.updateVideoScript(request);
            logger.info("updateVideoScript " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("updateVideoScript ClientRequestException " + e.getHttpStatusCode());
            logger.error("updateVideoScript ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("updateVideoScript ServerResponseException " + e.getHttpStatusCode());
            logger.error("updateVideoScript ServerResponseException " + e.getMessage());
        }
    }

    /**
     * 复制视频制作剧本
     *
     */
    private static void copyVideoScripts(MetaStudioClient client, String scriptId) {
        logger.info("copyVideoScripts start");
        CopyVideoScriptsRequest request = new CopyVideoScriptsRequest()
                .withScriptId(scriptId);
        try {
            CopyVideoScriptsResponse response = client.copyVideoScripts(request);
            logger.info("copyVideoScripts" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("copyVideoScripts ClientRequestException" + e.getHttpStatusCode());
            logger.error("copyVideoScripts ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("copyVideoScripts ServerResponseException" + e.getHttpStatusCode());
            logger.error("copyVideoScripts ServerResponseException" + e.getMessage());
        }
    }

    private static VideoConfig buildVideoConfig() {
        return new VideoConfig()
            .withBitrate(3000)
            .withCodec(VideoConfig.CodecEnum.H264)
            .withWidth(547)
            .withHeight(976);
    }

    private static VoiceConfig buildVoiceConfig(MetaStudioClient client) {
        // 获取音色资产
        DigitalAssetInfo voiceAssetInfo = getAsset(client, DigitalAssetInfo.AssetTypeEnum.VOICE_MODEL, AssetSourceEnum.SYSTEM, null);
        return new VoiceConfig().withVoiceAssetId(voiceAssetInfo.getAssetId());
    }

    private static List<ShootScriptItem> buildLiveShootScriptItems(MetaStudioClient client) {
        // 获取图片资产
        DigitalAssetInfo imgAssetInfo = getAsset(client, DigitalAssetInfo.AssetTypeEnum.IMAGE, AssetSourceEnum.SYSTEM, "BACKGROUND_IMG:Yes");
        // 获取图片文件
        AssetFileInfo assetFileInfo = getAssetFileInfo(imgAssetInfo);
        LayerPositionConfig layerPositionConfig = new LayerPositionConfig().withDx(0).withDy(0).withLayerIndex(1);
        LayerConfig layerConfig = new LayerConfig()
            .withLayerType(LayerConfig.LayerTypeEnum.HUMAN)
            .withPosition(layerPositionConfig)
            .withImageConfig(new ImageLayerConfig().withImageUrl(assetFileInfo.getDownloadUrl()));
        BackgroundConfigInfo backgroundConfigInfo = new BackgroundConfigInfo()
            .withBackgroundTitle(assetFileInfo.getFileName())
            .withBackgroundType(BackgroundConfigInfo.BackgroundTypeEnum.IMAGE)
            .withBackgroundConfig(assetFileInfo.getDownloadUrl());

        List<LayerConfig> layerConfigs = new ArrayList<>();
        layerConfigs.add(layerConfig);
        List<BackgroundConfigInfo> backgroundConfigInfos = new ArrayList<>();
        backgroundConfigInfos.add(backgroundConfigInfo);
        ShootScript shootScript = new ShootScript()
            .withLayerConfig(layerConfigs)
            .withTextConfig(new TextConfig().withText("<your script text>"))
            .withBackgroundConfig(backgroundConfigInfos);
        ShootScriptItem shootScriptItem = new ShootScriptItem()
            .withShootScript(shootScript);
        List<ShootScriptItem> liveShootScriptItems = new ArrayList<>();
        liveShootScriptItems.add(shootScriptItem);
        return liveShootScriptItems;
    }

    // 获取资产
    private static DigitalAssetInfo getAsset(MetaStudioClient client, DigitalAssetInfo.AssetTypeEnum assetType, ListAssetsRequest.AssetSourceEnum assetSource, String systemProperty) {
        DigitalAssetInfo digitalAssetInfo = null;
        try {
            ListAssetsRequest listAssetsRequest = new ListAssetsRequest()
                    .withSystemProperty(systemProperty) // 系统属性
                    .withAssetType(String.valueOf(assetType)) // 资产类型
                    .withAssetSource(assetSource) // 资产来源设置为：系统资产
                    .withAssetState(String.valueOf(DigitalAssetInfo.AssetStateEnum.ACTIVED) // 主文件上传成功，资产激活。
                    );
            logger.info("ListAssetsRequest: " + listAssetsRequest.toString());
            ListAssetsResponse response = client.listAssets(listAssetsRequest);
            digitalAssetInfo = response.getAssets().get(0);

        } catch (ClientRequestException e) {
            logger.error("getAsset ClientRequestException" + e.getHttpStatusCode());
            logger.error("getAsset ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("getAsset ServerResponseException" + e.getHttpStatusCode());
            logger.error("getAsset ServerResponseException" + e.getMessage());
        }
        return digitalAssetInfo;
    }

    // 从资产中获取背景图
    public static AssetFileInfo getAssetFileInfo(DigitalAssetInfo digitalAssetInfo) {
        for (AssetFileInfo assetFileInfo : digitalAssetInfo.getFiles()) {
            if (assetFileInfo.getAssetFileCategory().equals("MAIN")) {
                return assetFileInfo;
            }
        }
        return null;
    }
}