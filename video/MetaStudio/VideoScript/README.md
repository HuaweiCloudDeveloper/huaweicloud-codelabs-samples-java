

## 1、功能介绍

华为云提供了MetaStudio服务端SDK，您可以直接集成服务端SDK来调用MetaStudio的相关API，从而实现对MetaStudio的快速操作。

**您将学到什么？**

如何通过java版SDK来体验MetaStudio服务的数字人视频制作剧本管理功能。

## 2、开发时序图
![image](assets/VideoScript.jpg)


## 3、前置条件
- 1、已[注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=40c99ba3cea14417a2428c756a626599&lang=zh-cn)华为帐号并开通华为云，已进行[实名认证](https://support.huaweicloud.com/usermanual-account/zh-cn_topic_0077914254.html)。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、已获取账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 4、已获取MetaStudio服务对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见[API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html)。 

## 4、SDK获取和安装
您可以通过Maven配置所依赖的[数字内容生产线SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/MetaStudio?lang=Java)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-metastudio</artifactId>
    <version>3.1.60</version>
</dependency>

```

## 5、接口参数说明
关于接口参数的详细说明可参见：

[a.创建视频制作剧本](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=Create2DDigitalHumanVideo)

[b.查询视频制作剧本列表](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=Show2DDigitalHumanVideo)

[c.查询视频制作剧本详情](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=Cancel2DDigitalHumanVideo)

[d.更新视频制作剧本](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreatePhotoDigitalHumanVideo)

[e.删除视频制作剧本](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowPhotoDigitalHumanVideo)

[f.复制视频制作剧本](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CopyVideoScripts)

## 6、关键代码片段

### 6.1、创建视频制作剧本

```java
/**
 * 创建视频制作剧本
 *
 */
public static String createVideoScripts(MetaStudioClient client) {
    logger.info("createVideoScripts start");
    String jobId = null;
    try {
        CreateVideoScriptsRequest request = new CreateVideoScriptsRequest()
        .withBody(new CreateVideoScriptsReq()
        .withVideoConfig(buildVideoConfig())
        .withVoiceConfig(buildVoiceConfig(client))
        .withShootScripts(buildLiveShootScriptItems(client))
        .withScriptName("name"));
        CreateVideoScriptsResponse response = client.createVideoScripts(request);
        jobId = response.getScriptId();
        logger.info("createVideoScripts " + response.toString());
    } catch (ClientRequestException e) {
        logger.error("createVideoScripts ClientRequestException " + e.getHttpStatusCode());
        logger.error("createVideoScripts ClientRequestException " + e);
    } catch (ServerResponseException e) {
        logger.error("createVideoScripts ServerResponseException " + e.getHttpStatusCode());
        logger.error("createVideoScripts ServerResponseException " + e.getMessage());
    }
    return jobId;
}
```

### 6.2、查询视频制作剧本列表

```java
/**
 * 查询视频制作剧本列表
 *
 */
private static void listVideoScripts(MetaStudioClient client) {
    logger.info("listVideoScripts start");
    ListVideoScriptsRequest request = new ListVideoScriptsRequest();
    try {
        ListVideoScriptsResponse response = client.listVideoScripts(request);
        logger.info("listVideoScripts " + response.toString());
    } catch (ClientRequestException e) {
        logger.error("listVideoScripts ClientRequestException " + e.getHttpStatusCode());
        logger.error("listVideoScripts ClientRequestException " + e);
    } catch (ServerResponseException e) {
        logger.error("listVideoScripts ServerResponseException " + e.getHttpStatusCode());
        logger.error("listVideoScripts ServerResponseException " + e.getMessage());
    }
}
```

### 6.3、查询视频制作剧本详情

```java
/**
 * 查询视频制作剧本详情
 *
 */
private static void showVideoScript(MetaStudioClient client, String scriptId) {
    logger.info("showVideoScript start");
    ShowVideoScriptRequest request = new ShowVideoScriptRequest().withScriptId(scriptId);
    try {
        ShowVideoScriptResponse response = client.showVideoScript(request);
        logger.info("showVideoScript " + response.toString());
    } catch (ClientRequestException e) {
        logger.error("showVideoScript ClientRequestException " + e.getHttpStatusCode());
        logger.error("showVideoScript ClientRequestException " + e);
    } catch (ServerResponseException e) {
        logger.error("showVideoScript ServerResponseException " + e.getHttpStatusCode());
        logger.error("showVideoScript ServerResponseException " + e.getMessage());
    }
}

```

### 6.4、更新视频制作剧本
```java
/**
 * 更新视频制作剧本
 *
 */
public static void updateVideoScript(MetaStudioClient client, String scriptId) {
    logger.info("updateVideoScript start");
    try {
        UpdateVideoScriptsReq req = new UpdateVideoScriptsReq()
        .withVideoConfig(buildVideoConfig())
        .withVoiceConfig(buildVoiceConfig(client))
        .withShootScripts(buildLiveShootScriptItems(client))
        .withScriptName("name");
        UpdateVideoScriptRequest request = new UpdateVideoScriptRequest()
        .withScriptId(scriptId)
        .withBody(req);
        UpdateVideoScriptResponse response = client.updateVideoScript(request);
        logger.info("updateVideoScript " + response.toString());
    } catch (ClientRequestException e) {
        logger.error("updateVideoScript ClientRequestException " + e.getHttpStatusCode());
        logger.error("updateVideoScript ClientRequestException " + e);
    } catch (ServerResponseException e) {
        logger.error("updateVideoScript ServerResponseException " + e.getHttpStatusCode());
        logger.error("updateVideoScript ServerResponseException " + e.getMessage());
    }
}

```

### 6.5、删除视频制作剧本
```java
/**
 * 删除视频制作剧本
 *
 */
private static void deleteVideoScript(MetaStudioClient client, String scriptId) {
    logger.info("deleteVideoScript start");
    DeleteVideoScriptRequest request = new DeleteVideoScriptRequest().withScriptId(scriptId);
    try {
        DeleteVideoScriptResponse response = client.deleteVideoScript(request);
        logger.info("deleteVideoScript" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("deleteVideoScript ClientRequestException" + e.getHttpStatusCode());
        logger.error("deleteVideoScript ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("deleteVideoScript ServerResponseException" + e.getHttpStatusCode());
        logger.error("deleteVideoScript ServerResponseException" + e.getMessage());
    }
}
```

### 6.6、复制视频制作剧本
```java
/**
 * 复制视频制作剧本
 *
 */
private static void copyVideoScripts(MetaStudioClient client, String scriptId) {
    logger.info("copyVideoScripts start");
    CopyVideoScriptsRequest request = new CopyVideoScriptsRequest()
            .withScriptId(scriptId);
    try {
        CopyVideoScriptsResponse response = client.copyVideoScripts(request);
        logger.info("copyVideoScripts" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("copyVideoScripts ClientRequestException" + e.getHttpStatusCode());
        logger.error("copyVideoScripts ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("copyVideoScripts ServerResponseException" + e.getHttpStatusCode());
        logger.error("copyVideoScripts ServerResponseException" + e.getMessage());
    }
}
```

## 7、运行结果
**创建视频制作剧本，返回任务ID**
```json
{
  "audio_files":{
    "audio_file_url":[]
  },
  "X-Request-Id":"429f79***",
  "script_id":"97fec1***"
}
```
**查询视频制作剧本列表**
```json
{
  "X-Request-Id":"02ac74***",
  "video_scripts":[
    {
      "update_time":"2024-01-26T03:33:27Z",
      "create_time":"2024-01-26T03:33:27Z",
      "script_id":"97fec1***",
      "script_type":"TEXT",
      "text":"<your script text>",
      "script_name":"name"
    },{
      "model_asset_id":"9d97e9***",
      "script_cover_url":"https://***",
      "update_time":"2024-01-26T03:30:41Z",
      "create_time":"2024-01-26T03:06:30Z",
      "script_id":"50b4ba***",
      "script_type":"TEXT",
      "text":"<speak></speak>",
      "script_name":"未命名1706238390018"
    }
  ],
  "count":7312
}
```
**查询视频制作剧本详情**
```json
{
  "audio_files":{
    "audio_file_url":[]
  },
  "update_time":"2024-01-26T03:33:27Z",
  "create_time":"2024-01-26T03:33:27Z",
  "X-Request-Id":"97c83b***",
  "shoot_scripts":[
    {
      "shoot_script":{
        "background_config":[
          {
            "background_config":"https://***",
            "background_title":"人工智能.png",
            "background_type":"IMAGE"
          }
        ],
        "text_config":{
          "text":"<your script text>"
        },"layer_config":[
          {
            "image_config":{
              "image_url":"https://***"
            },"position":{
              "dx":0,
              "dy":0,
              "layer_index":1
            },
            "layer_type":"HUMAN"
          }
        ],
        "animation_config":[],
        "emotion_config":[],
        "script_type":"TEXT"
      }
    }
  ],
  "video_config":{
    "codec":"H264",
    "clip_mode":"RESIZE",
    "width":547,
    "bitrate":3000,
    "frame_rate":"25",
    "height":976,
    "is_subtitle_enable":false
  },
  "voice_config":{
    "volume":140,
    "pitch":100,
    "speed":100,
    "voice_asset_id":"36c956***"
  },
  "script_id":"97fec1***",
  "script_name":"name"
}
```
**更新视频制作剧本**
```json
{
  "audio_files":{
    "audio_file_url":[]
  },
  "X-Request-Id":"ee39d6***",
  "script_id":"97fec1***"
}
```
**删除视频制作剧本**
```json
{
  "X-Request-Id":"b82afe***"
}
```
**复制视频制作剧本**
```json
{
    "scriptId": "853401***",
    "xRequestId": "3d24dc***"
}
```
## 8、参考
本示例的代码工程仅用于简单演示，实际开发过程中应严格遵循开发指南。访问以下链接可以获取详细信息：[开发指南](https://support.huaweicloud.com/ssdk-metastudio/metastudio_06_0000.html)