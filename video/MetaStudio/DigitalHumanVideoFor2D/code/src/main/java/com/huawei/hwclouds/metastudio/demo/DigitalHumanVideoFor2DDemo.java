package com.huawei.hwclouds.metastudio.demo;

import java.awt.image.BufferedImage;

import java.io.File;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Base64;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.metastudio.v1.MetaStudioClient;
import com.huaweicloud.sdk.metastudio.v1.model.AssetFileInfo;
import com.huaweicloud.sdk.metastudio.v1.model.BackgroundConfigInfo;
import com.huaweicloud.sdk.metastudio.v1.model.Cancel2DDigitalHumanVideoRequest;
import com.huaweicloud.sdk.metastudio.v1.model.Cancel2DDigitalHumanVideoResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CancelPhotoDigitalHumanVideoRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CancelPhotoDigitalHumanVideoResponse;
import com.huaweicloud.sdk.metastudio.v1.model.Create2DDigitalHumanVideoReq;
import com.huaweicloud.sdk.metastudio.v1.model.Create2DDigitalHumanVideoRequest;
import com.huaweicloud.sdk.metastudio.v1.model.Create2DDigitalHumanVideoResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CreatePhotoDetectionReq;
import com.huaweicloud.sdk.metastudio.v1.model.CreatePhotoDetectionRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreatePhotoDetectionResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CreatePhotoDigitalHumanVideoReq;
import com.huaweicloud.sdk.metastudio.v1.model.CreatePhotoDigitalHumanVideoRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreatePhotoDigitalHumanVideoResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CreateVideoScriptsReq;
import com.huaweicloud.sdk.metastudio.v1.model.CreateVideoScriptsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreateVideoScriptsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.DigitalAssetInfo;
import com.huaweicloud.sdk.metastudio.v1.model.LayerConfig;
import com.huaweicloud.sdk.metastudio.v1.model.LayerPositionConfig;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsRequest.AssetSourceEnum;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ListDigitalHumanVideoRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListDigitalHumanVideoResponse;
import com.huaweicloud.sdk.metastudio.v1.model.OutputAssetConfig;
import com.huaweicloud.sdk.metastudio.v1.model.ShootScript;
import com.huaweicloud.sdk.metastudio.v1.model.ShootScriptItem;
import com.huaweicloud.sdk.metastudio.v1.model.Show2DDigitalHumanVideoRequest;
import com.huaweicloud.sdk.metastudio.v1.model.Show2DDigitalHumanVideoResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ShowPhotoDetectionRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ShowPhotoDetectionResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ShowPhotoDigitalHumanVideoRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ShowPhotoDigitalHumanVideoResponse;
import com.huaweicloud.sdk.metastudio.v1.model.SystemProperty;
import com.huaweicloud.sdk.metastudio.v1.model.TextConfig;
import com.huaweicloud.sdk.metastudio.v1.model.VideoConfig;
import com.huaweicloud.sdk.metastudio.v1.model.VoiceConfig;
import com.huaweicloud.sdk.metastudio.v1.region.MetaStudioRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.nio.charset.StandardCharsets;

public class DigitalHumanVideoFor2DDemo {

    private static final Logger logger = LoggerFactory.getLogger(DigitalHumanVideoFor2DDemo.class);
    // 获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID和您的AK/SK。
    // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK、HUAWEICLOUD_SDK_SK。
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");

    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    public static void main(String[] args) throws IOException {
        ICredential auth = new BasicCredentials()
                .withAk(AK)
                .withSk(SK);
        // 使用默认配置
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // 创建MetaStudio实例
        MetaStudioClient mClient = MetaStudioClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(MetaStudioRegion.CN_NORTH_4) // region为上海一需写为：withEndpoint("cn-east-3")
                .build();

        // 1,创建分身数字人视频制作任务(文本驱动)
        String jobId = create2DDigitalHumanVideoByText(mClient);

        // 2,查询分身数字人视频制作任务详情
        show2DDigitalHumanVideo(mClient, jobId);

        // 3,取消等待中的分身数字人视频制作任务
        cancel2DDigitalHumanVideo(mClient, jobId);

        // 4,创建照片分身数字人视频制作任务(文本驱动)
        String imagePath = "XXX.jpg"; // 此处设置您的图片路径
        String photoJobId = createPhotoDigitalHumanVideo(mClient, imagePath);

        // 5,创建照片分身数字人视频制作任务(语音驱动)
        createPhotoDigitalHumanVideoByAudio(mClient, imagePath);

        // 6,查询照片分身数字人视频制作任务详情
        showPhotoDigitalHumanVideo(mClient, photoJobId);

        // 7,取消等待中的照片分身数字人视频制作任务
        cancelPhotoDigitalHumanVideo(mClient, photoJobId);

        // 8,创建照片检测任务
        jobId = createPhotoDetection(mClient, imagePath);

        // 9,查询照片检测任务详情
        showPhotoDetection(mClient, jobId);

        // 10,查询视频制作任务列表
        listDigitalHumanVideo(mClient);

        // 创建分身数字人视频制作任务(语音驱动)
        // 11,创建视频制作剧本（语音驱动）
        CreateVideoScriptsResponse response = createVideoScripts(mClient);
        String script_id = response.getScriptId();
        String upload_url = response.getAudioFiles().getAudioFileUrl().get(0).getAudioFileUploadUrl();

        // 12,上传音频资产到obs
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type","application/octet-stream");
        String path = "XXX.wav"; // 输入您的音频文件
        uploadFileToObs(upload_url, path, headers);

        // 13,创建分身数字人视频制作任务(语音驱动)
        create2DDigitalHumanVideoByAudio(mClient, script_id);
    }


    /**
     * 创建分身数字人视频制作任务（文本驱动）
     *
     */
    public static String create2DDigitalHumanVideoByText(MetaStudioClient client) {
        logger.info("create2DDigitalHumanVideo start");
        String jobId = null;
        try {
            VideoConfig videoConfig = new VideoConfig()
                .withBitrate(3000)
                .withCodec(VideoConfig.CodecEnum.H264)
                .withWidth(1920)
                .withHeight(1080)
                .withIsSubtitleEnable(false);
            OutputAssetConfig outputAssetConfig = new OutputAssetConfig().withAssetName("<your asset name>");
            DigitalAssetInfo modelAssetInfo = getAsset(client, DigitalAssetInfo.AssetTypeEnum.HUMAN_MODEL_2D, AssetSourceEnum.SYSTEM, null);
            Create2DDigitalHumanVideoRequest request = new Create2DDigitalHumanVideoRequest()
                .withBody(new Create2DDigitalHumanVideoReq()
                    .withVoiceConfig(buildVoiceConfig(client))
                    .withVideoConfig(videoConfig)
                    .withShootScripts(buildShootScriptItems(client))
                    .withOutputAssetConfig(outputAssetConfig)
                    .withModelAssetId(modelAssetInfo.getAssetId()));
            Create2DDigitalHumanVideoResponse response = client.create2DDigitalHumanVideo(request);
            jobId = response.getJobId();
            logger.info("create2DDigitalHumanVideo " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("create2DDigitalHumanVideo ClientRequestException " + e.getHttpStatusCode());
            logger.error("create2DDigitalHumanVideo ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("create2DDigitalHumanVideo ServerResponseException " + e.getHttpStatusCode());
            logger.error("create2DDigitalHumanVideo ServerResponseException " + e.getMessage());
        }
        return jobId;
    }

    /**
     * 创建分身数字人视频制作任务(语音驱动)
     *
     */
    public static String create2DDigitalHumanVideoByAudio(MetaStudioClient client, String script_id) {
        logger.info("create2DDigitalHumanVideo start");
        String jobId = null;
        try {
            DigitalAssetInfo modelAssetInfo = getAsset(client, DigitalAssetInfo.AssetTypeEnum.HUMAN_MODEL_2D, AssetSourceEnum.SYSTEM, null);
            Create2DDigitalHumanVideoRequest request = new Create2DDigitalHumanVideoRequest()
                    .withBody(new Create2DDigitalHumanVideoReq()
                            .withScriptId(script_id)
                            .withModelAssetId(modelAssetInfo.getAssetId())
                            .withOutputAssetConfig(new OutputAssetConfig().withAssetName("name"))
                    );

            Create2DDigitalHumanVideoResponse response = client.create2DDigitalHumanVideo(request);
            logger.info("create2DDigitalHumanVideoByAudio " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("create2DDigitalHumanVideo ClientRequestException " + e.getHttpStatusCode());
            logger.error("create2DDigitalHumanVideo ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("create2DDigitalHumanVideo ServerResponseException " + e.getHttpStatusCode());
            logger.error("create2DDigitalHumanVideo ServerResponseException " + e.getMessage());
        }
        return jobId;
    }

    /**
     * 创建视频制作剧本（语音驱动）
     *
     */
    private static CreateVideoScriptsResponse createVideoScripts(MetaStudioClient client) {
        logger.info("createVideoScripts start");
        List<ShootScriptItem> shootScripts = new ArrayList<>();
        shootScripts.add(new ShootScriptItem().withSequenceNo(0)
                .withShootScript(new ShootScript().withScriptType(ShootScript.ScriptTypeEnum.AUDIO)));
        CreateVideoScriptsRequest request = new CreateVideoScriptsRequest()
                .withBody(new CreateVideoScriptsReq()
                        .withScriptName("<your name>")
                        .withVoiceConfig(new VoiceConfig().withVoiceAssetId("<your AssetId>"))
                        .withShootScripts(shootScripts));
        CreateVideoScriptsResponse response = new CreateVideoScriptsResponse();
        try {
            response = client.createVideoScripts(request);
            logger.info("createVideoScripts" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createVideoScripts ClientRequestException" + e.getHttpStatusCode());
            logger.error("createVideoScripts ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("createVideoScripts ServerResponseException" + e.getHttpStatusCode());
            logger.error("createVideoScripts ServerResponseException" + e.getMessage());
        }
        return response;
    }


    /**
     * 上传音频资产到obs
     *
     */
    private static void uploadFileToObs(String upload_url, String path, Map<String, String> headers)
        throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(upload_url).openConnection();
        connection.setRequestMethod("PUT");
        connection.setDoOutput(true);
        Iterator entries = headers.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry entry = (Map.Entry) entries.next();
            connection.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
        }
        connection.connect();
        // 写入文件数据
        OutputStream outputStream = connection.getOutputStream();
        try (FileInputStream inputStream = new FileInputStream(new File(path))) {
            byte[] bys = new byte[1024];
            int length;
            while ((length = inputStream.read(bys)) != -1) {
                outputStream.write(bys, 0, length);
            }
            String nextLine = "\r\n";
            outputStream.write(nextLine.getBytes(StandardCharsets.UTF_8));

            try (OutputStream os = connection.getOutputStream()) {
                os.write("{\"key\": \"value\"}".getBytes(StandardCharsets.UTF_8));
            }
            int responseCode = connection.getResponseCode();
            logger.info("Response Code: " + responseCode);

            // 获取返回值
            InputStream response = connection.getInputStream();
            try (InputStreamReader reader = new InputStreamReader(response, StandardCharsets.UTF_8)) {
                while (reader.read() != -1) {
                    logger.info(new String(bys, "UTF-8"));
                }
                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    logger.info(connection.getResponseMessage());
                    logger.info("upload success");
                } else {
                    logger.error("upload failed");
                }
            } catch (IOException ex) {
                logger.error("response stream open failed");
            } finally {
                response.close();
            }
        } catch (IOException e) {
            logger.error("file input stream open failed");
        } finally {
            outputStream.close();
        }
    }

    /**
     * 查询分身数字人视频制作任务详情
     *
     */
    private static void show2DDigitalHumanVideo(MetaStudioClient client, String jobId) {
        logger.info("showDigitalHumanBusinessCard start");
        Show2DDigitalHumanVideoRequest request = new Show2DDigitalHumanVideoRequest().withJobId(jobId);
        try {
            Show2DDigitalHumanVideoResponse response = client.show2DDigitalHumanVideo(request);
            logger.info("show2DDigitalHumanVideo " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("show2DDigitalHumanVideo ClientRequestException " + e.getHttpStatusCode());
            logger.error("show2DDigitalHumanVideo ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("show2DDigitalHumanVideo ServerResponseException " + e.getHttpStatusCode());
            logger.error("show2DDigitalHumanVideo ServerResponseException " + e.getMessage());
        }
    }

    /**
     * 取消等待中的分身数字人视频制作任务
     *
     */
    private static void cancel2DDigitalHumanVideo(MetaStudioClient client, String jobId) {
        logger.info("cancel2DDigitalHumanVideo start");
        Cancel2DDigitalHumanVideoRequest request = new Cancel2DDigitalHumanVideoRequest().withJobId(jobId);
        try {
            Cancel2DDigitalHumanVideoResponse response = client.cancel2DDigitalHumanVideo(request);
            logger.info("cancel2DDigitalHumanVideo" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("cancel2DDigitalHumanVideo ClientRequestException" + e.getHttpStatusCode());
            logger.error("cancel2DDigitalHumanVideo ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("cancel2DDigitalHumanVideo ServerResponseException" + e.getHttpStatusCode());
            logger.error("cancel2DDigitalHumanVideo ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 创建照片分身数字人视频制作任务（文本驱动）
     *
     */
    public static String createPhotoDigitalHumanVideo(MetaStudioClient client, String path) {
        logger.info("createPhotoDigitalHumanVideo start");
        String jobId = null;
        try {
            ShootScript shootScript = new ShootScript()
                    .withScriptType(ShootScript.ScriptTypeEnum.TEXT)
                    .withTextConfig(new TextConfig().withText("<speak>一二三四五</speak>"));
            ShootScriptItem shootScriptItem = new ShootScriptItem()
                    .withShootScript(shootScript)
                    .withSequenceNo(21);
            List<ShootScriptItem> liveShootScriptItems = new ArrayList<>();
            liveShootScriptItems.add(shootScriptItem);

            OutputAssetConfig photoOutputAssetConfig = new OutputAssetConfig().withAssetName("<your asset name>");

            CreatePhotoDigitalHumanVideoRequest request = new CreatePhotoDigitalHumanVideoRequest()
                .withBody(new CreatePhotoDigitalHumanVideoReq()
                    .withVoiceConfig(buildVoiceConfig(client))
                    .withShootScripts(liveShootScriptItems)
                    .withOutputAssetConfig(photoOutputAssetConfig)
                    .withHumanImage(encodeImageToBase64(path)));
            CreatePhotoDigitalHumanVideoResponse response = client.createPhotoDigitalHumanVideo(request);
            jobId = response.getJobId();
            logger.info("createPhotoDigitalHumanVideo " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createPhotoDigitalHumanVideo ClientRequestException " + e.getHttpStatusCode());
            logger.error("createPhotoDigitalHumanVideo ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("createPhotoDigitalHumanVideo ServerResponseException " + e.getHttpStatusCode());
            logger.error("createPhotoDigitalHumanVideo ServerResponseException " + e.getMessage());
        }
        return jobId;
    }

    /**
     * 创建照片分身数字人视频制作任务（语音驱动）
     *
     */
    public static String createPhotoDigitalHumanVideoByAudio(MetaStudioClient client, String path) {
        logger.info("createPhotoDigitalHumanVideo start");
        String jobId = null;
        try {
            ShootScript shootScript = new ShootScript()
                    .withScriptType(ShootScript.ScriptTypeEnum.AUDIO);
            ShootScriptItem shootScriptItem = new ShootScriptItem()
                    .withShootScript(shootScript)
                    .withSequenceNo(1);
            List<ShootScriptItem> liveShootScriptItems = new ArrayList<>();
            liveShootScriptItems.add(shootScriptItem);

            OutputAssetConfig photoOutputAssetConfig = new OutputAssetConfig().withAssetName("<your asset name>");

            CreatePhotoDigitalHumanVideoRequest request = new CreatePhotoDigitalHumanVideoRequest()
                    .withBody(new CreatePhotoDigitalHumanVideoReq()
                            .withVoiceConfig(buildVoiceConfig(client))
                            .withShootScripts(liveShootScriptItems)
                            .withOutputAssetConfig(photoOutputAssetConfig)
                            .withHumanImage(encodeImageToBase64(path)));
            CreatePhotoDigitalHumanVideoResponse response = client.createPhotoDigitalHumanVideo(request);
            jobId = response.getJobId();
            logger.info("createPhotoDigitalHumanVideo " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createPhotoDigitalHumanVideo ClientRequestException " + e.getHttpStatusCode());
            logger.error("createPhotoDigitalHumanVideo ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("createPhotoDigitalHumanVideo ServerResponseException " + e.getHttpStatusCode());
            logger.error("createPhotoDigitalHumanVideo ServerResponseException " + e.getMessage());
        }
        return jobId;
    }

    private static VoiceConfig buildVoiceConfig(MetaStudioClient client) {
        // 获取音色资产
        DigitalAssetInfo voiceAssetInfo = getAsset(client, DigitalAssetInfo.AssetTypeEnum.VOICE_MODEL, AssetSourceEnum.SYSTEM, null);
        VoiceConfig config = new VoiceConfig()
                .withVoiceAssetId(voiceAssetInfo.getAssetId())
                .withSpeed(100)
                .withPitch(100)
                .withVolume(140);
        return config;
    }

    private static List<ShootScriptItem> buildShootScriptItems(MetaStudioClient client) {
        // 获取图片资产
        DigitalAssetInfo imgAssetInfo = getAsset(client, DigitalAssetInfo.AssetTypeEnum.IMAGE, AssetSourceEnum.SYSTEM, "BACKGROUND_IMG:Yes");

        // 获取图片文件
        AssetFileInfo assetFileInfo = getAssetFileInfo(imgAssetInfo);
        LayerPositionConfig layerPositionConfig = new LayerPositionConfig()
            .withDx(0)
            .withDy(0)
            .withLayerIndex(1);
        BackgroundConfigInfo backgroundConfigInfo = new BackgroundConfigInfo()
            .withBackgroundTitle(assetFileInfo.getFileName())
            .withBackgroundType(BackgroundConfigInfo.BackgroundTypeEnum.IMAGE)
            .withBackgroundConfig(assetFileInfo.getDownloadUrl());
        LayerConfig layerConfig = new LayerConfig()
                .withLayerType(LayerConfig.LayerTypeEnum.HUMAN)
                .withPosition(layerPositionConfig);

        List<LayerConfig> layerConfigs = new ArrayList<>();
        layerConfigs.add(layerConfig);
        List<BackgroundConfigInfo> backgroundConfigInfos = new ArrayList<>();
        backgroundConfigInfos.add(backgroundConfigInfo);
        ShootScript shootScript = new ShootScript()
            .withLayerConfig(layerConfigs)
            .withTextConfig(new TextConfig().withText("<speak>一二三四五</speak>"))
            .withBackgroundConfig(backgroundConfigInfos);
        ShootScriptItem shootScriptItem = new ShootScriptItem()
            .withShootScript(shootScript);
        List<ShootScriptItem> liveShootScriptItems = new ArrayList<>();
        liveShootScriptItems.add(shootScriptItem);
        return liveShootScriptItems;
    }

    /**
     * 查询照片分身数字人视频制作任务详情
     *
     */
    private static void showPhotoDigitalHumanVideo(MetaStudioClient client, String jobId) {
        logger.info("showPhotoDigitalHumanVideo start");
        ShowPhotoDigitalHumanVideoRequest request = new ShowPhotoDigitalHumanVideoRequest().withJobId(jobId);
        try {
            ShowPhotoDigitalHumanVideoResponse response = client.showPhotoDigitalHumanVideo(request);
            logger.info("showPhotoDigitalHumanVideo " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showPhotoDigitalHumanVideo ClientRequestException " + e.getHttpStatusCode());
            logger.error("showPhotoDigitalHumanVideo ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("showPhotoDigitalHumanVideo ServerResponseException " + e.getHttpStatusCode());
            logger.error("showPhotoDigitalHumanVideo ServerResponseException " + e.getMessage());
        }
    }

    /**
     * 取消等待中的照片分身数字人视频制作任务
     *
     */
    private static void cancelPhotoDigitalHumanVideo(MetaStudioClient client, String jobId) {
        logger.info("cancelPhotoDigitalHumanVideo start");
        CancelPhotoDigitalHumanVideoRequest request = new CancelPhotoDigitalHumanVideoRequest().withJobId(jobId);
        try {
            CancelPhotoDigitalHumanVideoResponse response = client.cancelPhotoDigitalHumanVideo(request);
            logger.info("cancelPhotoDigitalHumanVideo" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("cancelPhotoDigitalHumanVideo ClientRequestException" + e.getHttpStatusCode());
            logger.error("cancelPhotoDigitalHumanVideo ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("cancelPhotoDigitalHumanVideo ServerResponseException" + e.getHttpStatusCode());
            logger.error("cancelPhotoDigitalHumanVideo ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 查询视频制作任务列表
     *
     */
    private static void listDigitalHumanVideo(MetaStudioClient client) {
        logger.info("listDigitalHumanVideo start");
        ListDigitalHumanVideoRequest request = new ListDigitalHumanVideoRequest();
        try {
            ListDigitalHumanVideoResponse response = client.listDigitalHumanVideo(request);
            logger.info("listDigitalHumanVideo" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listDigitalHumanVideo ClientRequestException" + e.getHttpStatusCode());
            logger.error("listDigitalHumanVideo ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("listDigitalHumanVideo ServerResponseException" + e.getHttpStatusCode());
            logger.error("listDigitalHumanVideo ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 查询照片检测任务详情
     *
     */
    private static void showPhotoDetection(MetaStudioClient client, String job_id) {
        logger.info("showPhotoDetection start");
        ShowPhotoDetectionRequest request = new ShowPhotoDetectionRequest()
                .withJobId(job_id);
        try {
            ShowPhotoDetectionResponse response = client.showPhotoDetection(request);
            logger.info("showPhotoDetection" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showPhotoDetection ClientRequestException" + e.getHttpStatusCode());
            logger.error("showPhotoDetection ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("showPhotoDetection ServerResponseException" + e.getHttpStatusCode());
            logger.error("showPhotoDetection ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 创建照片检测任务
     *
     */
    private static String createPhotoDetection(MetaStudioClient client, String path) {
        logger.info("createPhotoDetection start");
        String job_id = null;
        CreatePhotoDetectionRequest request = new CreatePhotoDetectionRequest()
                .withBody(new CreatePhotoDetectionReq().withHumanImage(encodeImageToBase64(path)));
        try {
            CreatePhotoDetectionResponse response = client.createPhotoDetection(request);
            logger.info("createPhotoDetection" + response.toString());
            job_id = response.getJobId();
        } catch (ClientRequestException e) {
            logger.error("createPhotoDetection ClientRequestException" + e.getHttpStatusCode());
            logger.error("createPhotoDetection ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("createPhotoDetection ServerResponseException" + e.getHttpStatusCode());
            logger.error("createPhotoDetection ServerResponseException" + e.getMessage());
        }
        return job_id;
    }

    // 从资产中获取主图
    public static AssetFileInfo getAssetFileInfo(DigitalAssetInfo digitalAssetInfo) {
        for (AssetFileInfo assetFileInfo : digitalAssetInfo.getFiles()) {
            if (assetFileInfo.getAssetFileCategory().equals("MAIN")) {
                return assetFileInfo;
            }
        }
        return null;
    }

    private static DigitalAssetInfo getAsset(MetaStudioClient client, DigitalAssetInfo.AssetTypeEnum assetType, ListAssetsRequest.AssetSourceEnum assetSource, String systemProperty) {
        DigitalAssetInfo digitalAssetInfo = null;
        try {
            ListAssetsRequest listAssetsRequest = new ListAssetsRequest()
                    .withAssetType(String.valueOf(assetType)) // 资产类型
                    .withAssetSource(assetSource) // 资产来源：系统资产
                    .withSystemProperty(systemProperty) // 系统属性
                    .withAssetState(String.valueOf(DigitalAssetInfo.AssetStateEnum.ACTIVED) // 主文件上传成功，资产激活。
                    );
            logger.info(" ListAssetsRequest " + listAssetsRequest.toString());
            ListAssetsResponse response = client.listAssets(listAssetsRequest);
            digitalAssetInfo = response.getAssets().get(0);
        } catch (ClientRequestException e) {
            logger.error("getAsset ClientRequestException" + e.getHttpStatusCode());
            logger.error("getAsset ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("getAsset ServerResponseException" + e.getHttpStatusCode());
            logger.error("getAsset ServerResponseException" + e.getMessage());
        }
        return digitalAssetInfo;
    }

    // 对图片进行base64编码
    public static String encodeImageToBase64(String imgFile) { // 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        File imageFile = new File(imgFile);
        String retcode = "";
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            BufferedImage bufferedImage = ImageIO.read(imageFile);
            ImageIO.write(bufferedImage, "jpg", outputStream);

            // 对字节数组Base64编码
            Base64.Encoder encoder = Base64.getEncoder();
            retcode = encoder.encodeToString(outputStream.toByteArray());
        } catch (MalformedURLException e1) {
            logger.error("encode image file failed: " + e1.getMessage());
        } catch (IOException e) {
            logger.error("encode image file failed: " + e.getMessage());
        }

        logger.info("BASE64: " + retcode);
        return retcode; // 返回Base64编码过的字节数组字符串
    }
}