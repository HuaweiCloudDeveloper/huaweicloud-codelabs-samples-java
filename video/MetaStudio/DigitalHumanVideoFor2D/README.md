

## 1、功能介绍

华为云提供了MetaStudio服务端SDK，您可以直接集成服务端SDK来调用MetaStudio的相关API，从而实现对MetaStudio的快速操作。

**您将学到什么？**

如何通过java版SDK来体验MetaStudio服务的分身数字人视频制作视频制作功能。
此处展示了文本驱动和语音驱动两种方式。

## 2、开发时序图

![image](assets/DigitalHumanVideoFor2D.jpg)


## 3、前置条件
- 1、已[注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=40c99ba3cea14417a2428c756a626599&lang=zh-cn)华为帐号并开通华为云，已进行[实名认证](https://support.huaweicloud.com/usermanual-account/zh-cn_topic_0077914254.html)。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、已获取账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 4、已获取MetaStudio服务对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见[API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html)。 

## 4、SDK获取和安装
您可以通过Maven配置所依赖的[数字内容生产线SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/MetaStudio?lang=Java)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-metastudio</artifactId>
    <version>3.1.78</version>
</dependency>

```

## 5、接口参数说明
关于接口参数的详细说明可参见：

[a.创建分身数字人视频制作任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=Create2DDigitalHumanVideo)

[b.查询分身数字人视频制作任务详情](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=Show2DDigitalHumanVideo)

[c.取消等待中的分身数字人视频制作任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=Cancel2DDigitalHumanVideo)

[d.创建照片分身数字人视频制作任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreatePhotoDigitalHumanVideo)

[e.查询照片分身数字人视频制作任务详情](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowPhotoDigitalHumanVideo)

[f.取消等待中的照片分身数字人视频制作任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CancelPhotoDigitalHumanVideo)

[g.创建照片检测任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreatePhotoDetection)

[h.查询照片检测任务详情](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowPhotoDetection)

[i.查询视频制作任务列表](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ListDigitalHumanVideo)

[j.创建视频制作剧本](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateVideoScripts)


## 6、关键代码片段

### 6.1、创建分身数字人视频制作任务
```java
/**
 * 创建分身数字人视频制作任务（文本驱动）
 *
 */
public static String create2DDigitalHumanVideoByText(MetaStudioClient client) {
    logger.info("create2DDigitalHumanVideo start");
    String jobId = null;
    try {
        VideoConfig videoConfig = new VideoConfig()
            .withBitrate(3000)
            .withCodec(VideoConfig.CodecEnum.H264)
            .withWidth(1920)
            .withHeight(1080)
            .withIsSubtitleEnable(false);
        OutputAssetConfig outputAssetConfig = new OutputAssetConfig().withAssetName("<your asset name>");
        DigitalAssetInfo modelAssetInfo = getAsset(client, DigitalAssetInfo.AssetTypeEnum.HUMAN_MODEL_2D, null);
        Create2DDigitalHumanVideoRequest request = new Create2DDigitalHumanVideoRequest()
        .withBody(new Create2DDigitalHumanVideoReq()
            .withVoiceConfig(buildVoiceConfig(client))
            .withVideoConfig(videoConfig)
            .withShootScripts(buildShootScriptItems(client))
            .withOutputAssetConfig(outputAssetConfig)
            .withModelAssetId(modelAssetInfo.getAssetId())
         );
        Create2DDigitalHumanVideoResponse response = client.create2DDigitalHumanVideo(request);
        jobId = response.getJobId();
        logger.info("create2DDigitalHumanVideo " + response.toString());
    } catch (ClientRequestException e) {
        logger.error("create2DDigitalHumanVideo ClientRequestException " + e.getHttpStatusCode());
        logger.error("create2DDigitalHumanVideo ClientRequestException " + e);
    } catch (ServerResponseException e) {
        logger.error("create2DDigitalHumanVideo ServerResponseException " + e.getHttpStatusCode());
        logger.error("create2DDigitalHumanVideo ServerResponseException " + e.getMessage());
    }
    return jobId;
}
```

### 6.2、查询分身数字人视频制作任务详情
```java
/**
 * 查询分身数字人视频制作任务详情
 *
 */
private static void show2DDigitalHumanVideo(MetaStudioClient client, String jobId) {
        logger.info("showDigitalHumanBusinessCard start");
        Show2DDigitalHumanVideoRequest request = new Show2DDigitalHumanVideoRequest().withJobId(jobId);
    try {
        Show2DDigitalHumanVideoResponse response = client.show2DDigitalHumanVideo(request);
        logger.info("show2DDigitalHumanVideo " + response.toString());
    } catch (ClientRequestException e) {
        logger.error("show2DDigitalHumanVideo ClientRequestException " + e.getHttpStatusCode());
        logger.error("show2DDigitalHumanVideo ClientRequestException " + e);
    } catch (ServerResponseException e) {
        logger.error("show2DDigitalHumanVideo ServerResponseException " + e.getHttpStatusCode());
        logger.error("show2DDigitalHumanVideo ServerResponseException " + e.getMessage());
    }
}
```

### 6.3、取消等待中的分身数字人视频制作任务
```java
/**
 * 取消等待中的分身数字人视频制作任务
 *
 */
private static void cancel2DDigitalHumanVideo(MetaStudioClient client, String jobId) {
    logger.info("cancel2DDigitalHumanVideo start");
    Cancel2DDigitalHumanVideoRequest request = new Cancel2DDigitalHumanVideoRequest().withJobId(jobId);
    try {
        Cancel2DDigitalHumanVideoResponse response = client.cancel2DDigitalHumanVideo(request);
        logger.info("cancel2DDigitalHumanVideo" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("cancel2DDigitalHumanVideo ClientRequestException" + e.getHttpStatusCode());
        logger.error("cancel2DDigitalHumanVideo ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("cancel2DDigitalHumanVideo ServerResponseException" + e.getHttpStatusCode());
        logger.error("cancel2DDigitalHumanVideo ServerResponseException" + e.getMessage());
    }
}
```

### 6.4、创建照片分身数字人视频制作任务（文本驱动）
```java
/**
 * 创建照片分身数字人视频制作任务（文本驱动）
 *
 */
public static String createPhotoDigitalHumanVideo(MetaStudioClient client, String path) {
    logger.info("createPhotoDigitalHumanVideo start");
    String jobId = null;
    try {
        ShootScript shootScript = new ShootScript()
            .withScriptType(ShootScript.ScriptTypeEnum.TEXT)
            .withTextConfig(new TextConfig().withText("<speak>一二三四五</speak>"));
        ShootScriptItem shootScriptItem = new ShootScriptItem()
            .withShootScript(shootScript)
            .withSequenceNo(21);
        List<ShootScriptItem> liveShootScriptItems = new ArrayList<>();
        liveShootScriptItems.add(shootScriptItem);
    
        PhotoVideoConfig photoVideoConfig = new PhotoVideoConfig()
            .withCodec(PhotoVideoConfig.CodecEnum.H264)
            .withBitrate(8000);
        OutputAssetConfig photoOutputAssetConfig = new OutputAssetConfig().withAssetName("name");
    
        CreatePhotoDigitalHumanVideoRequest request = new CreatePhotoDigitalHumanVideoRequest()
            .withBody(new CreatePhotoDigitalHumanVideoReq()
            .withVoiceConfig(buildVoiceConfig(client))
            .withShootScripts(liveShootScriptItems)
            .withOutputAssetConfig(photoOutputAssetConfig)
            .withHumanImage(encodeImageToBase64(path)));
        CreatePhotoDigitalHumanVideoResponse response = client.createPhotoDigitalHumanVideo(request);
        jobId = response.getJobId();
        logger.info("createPhotoDigitalHumanVideo " + response.toString());
    } catch (ClientRequestException e) {
        logger.error("createPhotoDigitalHumanVideo ClientRequestException " + e.getHttpStatusCode());
        logger.error("createPhotoDigitalHumanVideo ClientRequestException " + e);
    } catch (ServerResponseException e) {
        logger.error("createPhotoDigitalHumanVideo ServerResponseException " + e.getHttpStatusCode());
        logger.error("createPhotoDigitalHumanVideo ServerResponseException " + e.getMessage());
    }
    return jobId;
}

```

### 6.5、创建照片分身数字人视频制作任务（语音驱动）
```java
/**
 * 创建照片分身数字人视频制作任务（语音驱动）
 *
 */
public static String createPhotoDigitalHumanVideoByAudio(MetaStudioClient client, String path) {
    logger.info("createPhotoDigitalHumanVideo start");
    String jobId = null;
    try {
        ShootScript shootScript = new ShootScript()
        .withScriptType(ShootScript.ScriptTypeEnum.AUDIO);
        ShootScriptItem shootScriptItem = new ShootScriptItem()
        .withShootScript(shootScript)
        .withSequenceNo(1);
        List<ShootScriptItem> liveShootScriptItems = new ArrayList<>();
        liveShootScriptItems.add(shootScriptItem);
    
        OutputAssetConfig photoOutputAssetConfig = new OutputAssetConfig().withAssetName("<your asset name>");
    
        CreatePhotoDigitalHumanVideoRequest request = new CreatePhotoDigitalHumanVideoRequest()
        .withBody(new CreatePhotoDigitalHumanVideoReq()
        .withVoiceConfig(buildVoiceConfig(client))
        .withShootScripts(liveShootScriptItems)
        .withOutputAssetConfig(photoOutputAssetConfig)
        .withHumanImage(encodeImageToBase64(path)));
        CreatePhotoDigitalHumanVideoResponse response = client.createPhotoDigitalHumanVideo(request);
        jobId = response.getJobId();
        logger.info("createPhotoDigitalHumanVideo " + response.toString());
    } catch (ClientRequestException e) {
        logger.error("createPhotoDigitalHumanVideo ClientRequestException " + e.getHttpStatusCode());
        logger.error("createPhotoDigitalHumanVideo ClientRequestException " + e.getMessage());
    } catch (ServerResponseException e) {
        logger.error("createPhotoDigitalHumanVideo ServerResponseException " + e.getHttpStatusCode());
        logger.error("createPhotoDigitalHumanVideo ServerResponseException " + e.getMessage());
    }
    return jobId;
}

```

### 6.6、查询照片分身数字人视频制作任务详情
```java
/**
 * 查询照片分身数字人视频制作任务详情
 *
 */
private static void showPhotoDigitalHumanVideo(MetaStudioClient client, String jobId) {
    logger.info("showPhotoDigitalHumanVideo start");
    ShowPhotoDigitalHumanVideoRequest request = new ShowPhotoDigitalHumanVideoRequest().withJobId(jobId);
    try {
        ShowPhotoDigitalHumanVideoResponse response = client.showPhotoDigitalHumanVideo(request);
        logger.info("showPhotoDigitalHumanVideo " + response.toString());
    } catch (ClientRequestException e) {
        logger.error("showPhotoDigitalHumanVideo ClientRequestException " + e.getHttpStatusCode());
        logger.error("showPhotoDigitalHumanVideo ClientRequestException " + e);
    } catch (ServerResponseException e) {
        logger.error("showPhotoDigitalHumanVideo ServerResponseException " + e.getHttpStatusCode());
        logger.error("showPhotoDigitalHumanVideo ServerResponseException " + e.getMessage());
    }
}
```

### 6.7、取消等待中的照片分身数字人视频制作任务
```java
/**
 * 取消等待中的照片分身数字人视频制作任务
 *
 */
private static void cancelPhotoDigitalHumanVideo(MetaStudioClient client, String jobId) {
    logger.info("cancelPhotoDigitalHumanVideo start");
    CancelPhotoDigitalHumanVideoRequest request = new CancelPhotoDigitalHumanVideoRequest().withJobId(jobId);
    try {
        CancelPhotoDigitalHumanVideoResponse response = client.cancelPhotoDigitalHumanVideo(request);
        logger.info("cancelPhotoDigitalHumanVideo" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("cancelPhotoDigitalHumanVideo ClientRequestException" + e.getHttpStatusCode());
        logger.error("cancelPhotoDigitalHumanVideo ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("cancelPhotoDigitalHumanVideo ServerResponseException" + e.getHttpStatusCode());
        logger.error("cancelPhotoDigitalHumanVideo ServerResponseException" + e.getMessage());
    }
}
```

### 6.8、创建照片检测任务
```java
/**
 * 创建照片检测任务
 *
 */
private static String createPhotoDetection(MetaStudioClient client, String path) {
    logger.info("createPhotoDetection start");
    String job_id = null;
    CreatePhotoDetectionRequest request = new CreatePhotoDetectionRequest()
        .withBody(new CreatePhotoDetectionReq().withHumanImage(encodeImageToBase64(path)));
    try {
        CreatePhotoDetectionResponse response = client.createPhotoDetection(request);
        logger.info("createPhotoDetection" + response.toString());
        job_id = response.getJobId();
    } catch (ClientRequestException e) {
        logger.error("createPhotoDetection ClientRequestException" + e.getHttpStatusCode());
        logger.error("createPhotoDetection ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("createPhotoDetection ServerResponseException" + e.getHttpStatusCode());
        logger.error("createPhotoDetection ServerResponseException" + e.getMessage());
    }
    return job_id;
}
```

### 6.9、查询照片检测任务详情
```java
/**
 * 查询照片检测任务详情
 *
 */
private static void showPhotoDetection(MetaStudioClient client, String job_id) {
    logger.info("showPhotoDetection start");
    ShowPhotoDetectionRequest request = new ShowPhotoDetectionRequest()
            .withJobId(job_id);
    try {
        ShowPhotoDetectionResponse response = client.showPhotoDetection(request);
        logger.info("showPhotoDetection" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("showPhotoDetection ClientRequestException" + e.getHttpStatusCode());
        logger.error("showPhotoDetection ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("showPhotoDetection ServerResponseException" + e.getHttpStatusCode());
        logger.error("showPhotoDetection ServerResponseException" + e.getMessage());
    }
}
```

### 6.10、查询视频制作任务列表
```java
/**
 * 查询视频制作任务列表
 *
 */
private static void listDigitalHumanVideo(MetaStudioClient client) {
    logger.info("listDigitalHumanVideo start");
    ListDigitalHumanVideoRequest request = new ListDigitalHumanVideoRequest();
    try {
        ListDigitalHumanVideoResponse response = client.listDigitalHumanVideo(request);
        logger.info("listDigitalHumanVideo" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("listDigitalHumanVideo ClientRequestException" + e.getHttpStatusCode());
        logger.error("listDigitalHumanVideo ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("listDigitalHumanVideo ServerResponseException" + e.getHttpStatusCode());
        logger.error("listDigitalHumanVideo ServerResponseException" + e.getMessage());
    }
}
```

### 6.11、创建视频制作剧本（语音驱动）
```java
/**
 * 创建视频制作剧本（语音驱动）
 *
 */
private static CreateVideoScriptsResponse createVideoScripts(MetaStudioClient client) {
    logger.info("createVideoScripts start");
    List<ShootScriptItem> shootScripts = new ArrayList<>();
    shootScripts.add(new ShootScriptItem().withSequenceNo(0)
            .withShootScript(new ShootScript().withScriptType(ShootScript.ScriptTypeEnum.AUDIO)));
    CreateVideoScriptsRequest request = new CreateVideoScriptsRequest()
            .withBody(new CreateVideoScriptsReq()
                    .withScriptName("<your name>")
                    .withVoiceConfig(new VoiceConfig().withVoiceAssetId("<your AssetId>"))
                    .withShootScripts(shootScripts));
    CreateVideoScriptsResponse response = new CreateVideoScriptsResponse();
    try {
        response = client.createVideoScripts(request);
        logger.info("createVideoScripts" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("createVideoScripts ClientRequestException" + e.getHttpStatusCode());
        logger.error("createVideoScripts ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("createVideoScripts ServerResponseException" + e.getHttpStatusCode());
        logger.error("createVideoScripts ServerResponseException" + e.getMessage());
    }
    return response;
}
```

### 6.12、上传音频资产到obs
```java
/**
 * 上传音频资产到obs
 *
 */
private static void uploadFileToObs(String upload_url, String path, Map<String, String> headers) throws IOException {
    HttpURLConnection connection = (HttpURLConnection) new URL(upload_url).openConnection();
    connection.setRequestMethod("PUT");
    connection.setDoOutput(true);
    Iterator entries = headers.entrySet().iterator();
    while(entries.hasNext()){
        Map.Entry entry = (Map.Entry) entries.next();
        connection.setRequestProperty((String) entry.getKey(),(String) entry.getValue());
    }
    connection.connect();
    // 写入文件数据
    OutputStream outputStream  = connection.getOutputStream();
    File file = new File(path);
    FileInputStream inputStream =  new FileInputStream(file);
    byte[] bytes = new byte[1024];
    int length;
    while ((length = inputStream.read(bytes))!= -1){
        outputStream.write(bytes, 0, length);
    }
    String nextLine = "\r\n";
    outputStream.write(nextLine.getBytes());

    try (OutputStream os = connection.getOutputStream()) {
        os.write("{\"key\":\"value\"}".getBytes());
    }
    int responseCode = connection.getResponseCode();
    System.out.println("Response Code : " + responseCode);

    // 获取返回值
    InputStream response = connection.getInputStream();
    InputStreamReader reader = new InputStreamReader(response);
    while (reader.read() != -1){
        System.out.println(new String(bytes, "UTF-8"));
    }
    if (connection.getResponseCode() == HttpURLConnection.HTTP_OK){
        System.out.println(connection.getResponseMessage());
        System.err.println("上传成功");
    }else {
        System.err.println("上传失败");
    }
}
```

### 6.13、创建分身数字人视频制作任务(语音驱动)
```java
/**
 * 创建分身数字人视频制作任务(语音驱动)
 *
 */
public static String create2DDigitalHumanVideoByAudio(MetaStudioClient client, String script_id) {
    logger.info("create2DDigitalHumanVideo start");
    String jobId = null;
    try {
        DigitalAssetInfo modelAssetInfo = getAsset(client, DigitalAssetInfo.AssetTypeEnum.HUMAN_MODEL_2D, null);
        Create2DDigitalHumanVideoRequest request = new Create2DDigitalHumanVideoRequest()
                .withBody(new Create2DDigitalHumanVideoReq()
                        .withScriptId(script_id)
                        .withModelAssetId(modelAssetInfo.getAssetId())
                        .withOutputAssetConfig(new OutputAssetConfig().withAssetName("name"))
                );

        Create2DDigitalHumanVideoResponse response = client.create2DDigitalHumanVideo(request);
        logger.info("create2DDigitalHumanVideoByAudio " + response.toString());
    } catch (ClientRequestException e) {
        logger.error("create2DDigitalHumanVideo ClientRequestException " + e.getHttpStatusCode());
        logger.error("create2DDigitalHumanVideo ClientRequestException " + e);
    } catch (ServerResponseException e) {
        logger.error("create2DDigitalHumanVideo ServerResponseException " + e.getHttpStatusCode());
        logger.error("create2DDigitalHumanVideo ServerResponseException " + e.getMessage());
    }
    return jobId;
}
```

## 7、运行结果
**创建分身数字人视频制作任务**
```json
{
  "X-Request-Id":"c15b8f***",
  "job_id":"73f034***"
}
```
**查询分身数字人视频制作任务详情**
```json
{
  "model_asset_id":"9d97e9***",
  "output_asset_config":{
    "asset_name":"<your name>",
    "asset_id":"a24386***"
  },
  "create_time":"2024-01-26T07:18:04Z",
  "X-Request-Id":"b8973b***",
  "job_id":"73f034***",
  "video_config":{
    "codec":"H264",
    "clip_mode":"RESIZE",
    "width":547,
    "bitrate":3000,
    "frame_rate":"25",
    "height":976,
    "is_subtitle_enable":false
  },
  "voice_config":{
    "volume":140,
    "pitch":100,
    "speed":100,
    "voice_asset_id":"36c956***"
  },
  "lastupdate_time":"2024-01-26T07:18:04Z",
  "state":"WAITING"
}
```
**取消等待中的分身数字人视频制作任务**
```json
{
	"X-Request-Id":"67f72b***"
}
```
**创建照片分身数字人视频制作任务（文本驱动）**
```json
{
  "X-Request-Id":"f39d1d***",
  "job_id":"51178e***"
}
```
**创建照片分身数字人视频制作任务（语音驱动）**
```json
{
  "X-Request-Id":"b7301d***",
  "job_id":"4f1134***"
}
```
**查询照片分身数字人视频制作任务详情**
```json
{
	"X-Request-Id":"f39d1d***",
	"job_id":"51178e***"
}
```
**取消等待中的照片分身数字人视频制作任务**
```json
{
	"X-Request-Id":"bd17b5***"
}
```

**创建照片检测任务**
```json
{
	"X-Request-Id":"bcb6e3***",
	"job_id":"f21212***"
}
```

**查询照片检测任务详情**
```json
{
	"create_time":"2024-01-26T07:18:05Z",
	"X-Request-Id":"a8a358***",
	"job_id":"f21212***",
	"state":"WAITING"
}
```

**查询视频制作任务列表**
```json
{
	"X-Request-Id":"ddfff6***",
	"jobs":[
		{
			"output_asset_config":{
				"asset_name":"name",
				"asset_id":"84b2a0***"
			},"create_time":"2024-01-26T07:18:04Z",
			"job_id":"51178e***",
			"lastupdate_time":"2024-01-26T07:18:05Z",
			"state":"CANCELED"
		},{
			"output_asset_config":{
				"asset_name":"name",
				"asset_id":"a24386***"
			},"create_time":"2024-01-26T07:18:04Z",
			"job_id":"73f034***",
			"lastupdate_time":"2024-01-26T07:18:04Z",
			"state":{
				"$ref":"$.jobs[0].output_asset_config.state"
			}
		},{
			"duration":211.56,
            "output_asset_config":{
				"cover_url":"https://***",
                "asset_name":"1000字视频",
				"asset_id":"d48708***"
			},"start_time":"2024-01-26T07:05:59Z",
			"create_time":"2024-01-26T07:05:55Z",
			"job_id":"efabba***",
			"lastupdate_time":"2024-01-26T07:15:10Z",
			"state":"SUCCEED"
		}
	],
	"count":15373
}
```

**创建视频制作剧本（语音驱动）**
```json
{
	"audio_files":{
		"audio_file_url":[
			{
				"audio_file_upload_url":"https://***",
                "sequence_no":0
			}
		]
	},"X-Request-Id":"bab480***",
	"script_id":"82cee2***"
}
```

**创建分身数字人视频制作任务(语音驱动)**
```json
{
	"X-Request-Id":"5f9119***",
	"job_id":"39d363***"
}
```
## 8、参考
本示例的代码工程仅用于简单演示，实际开发过程中应严格遵循开发指南。访问以下链接可以获取详细信息：[开发指南](https://support.huaweicloud.com/ssdk-metastudio/metastudio_06_0000.html)