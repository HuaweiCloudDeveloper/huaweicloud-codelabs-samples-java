## 1、功能介绍

华为云提供了MetaStudio服务端SDK，您可以直接集成服务端SDK来调用MetaStudio的相关API，从而实现对MetaStudio的快速操作。

**您将学到什么？**

如何通过java版SDK来体验MetaStudio服务的数字人语音合成管理功能。

## 2、开发时序图
![image](assets/TTSC.jpg)

## 3、前置条件
- 1、已[注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=40c99ba3cea14417a2428c756a626599&lang=zh-cn)华为帐号并开通华为云，已进行[实名认证](https://support.huaweicloud.com/usermanual-account/zh-cn_topic_0077914254.html)。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、已获取账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 4、已获取MetaStudio服务对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见[API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html)。

## 4、SDK获取和安装
您可以通过Maven配置所依赖的[数字内容生产线SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/MetaStudio?lang=Java)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-metastudio</artifactId>
    <version>3.1.78</version>
</dependency>
```

## 5、接口参数说明
关于接口参数的详细说明可参见：
[a.创建TTS试听任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateTtsAudition)

[b.获取TTS试听文件](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowTtsAuditionFile)

## 6、关键代码片段

### 6.1、创建TTS试听任务
```java
    /**
     * 创建TTS试听任务
     *
     */
    private static String createTtsAudition(MetaStudioClient client, String emotion) {
        logger.info("createTtsAudition start");
        CreateTtsAuditionRequest request = new CreateTtsAuditionRequest()
                .withBody(new CreateTtsAuditionRequestBody()
                        .withText("<your text>")
                        .withEmotion(emotion));
        String jobId = "";
        try {
            CreateTtsAuditionResponse response = client.createTtsAudition(request);
            jobId = response.getJobId();
            logger.info("createTtsAudition" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createTtsAudition ClientRequestException" + e.getHttpStatusCode());
            logger.error("createTtsAudition ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("createTtsAudition ServerResponseException" + e.getHttpStatusCode());
            logger.error("createTtsAudition ServerResponseException" + e.getMessage());
        }
        return jobId;
    }
```

### 6.2、获取TTS试听文件
```java
    /**
     * 获取TTS试听文件
     *
     */
    private static void showTtsAuditionFile(MetaStudioClient client, String jobId) {
        logger.info("showTtsAuditionFile start");
        ShowTtsAuditionFileRequest request = new ShowTtsAuditionFileRequest()
                .withJobId(jobId);
        try {
            ShowTtsAuditionFileResponse response = client.showTtsAuditionFile(request);
            logger.info("showTtsAuditionFile" + response.toString());
        }  catch (ClientRequestException e) {
            logger.error("showTtsAuditionFile ClientRequestException" + e.getHttpStatusCode());
            logger.error("showTtsAuditionFile ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showTtsAuditionFile ServerResponseException" + e.getHttpStatusCode());
            logger.error("showTtsAuditionFile ServerResponseException" + e.getMessage());
        }
    }
```

## 7、运行结果
**创建TTS试听任务**
```json
{
    "job_id": "0ce83e***"
}
```

**获取TTS试听文件**
```json
{
    "is-file-complete": false,
    "message": null,
    "files": []
}
```

## 8、参考
本示例的代码工程仅用于简单演示，实际开发过程中应严格遵循开发指南。访问以下链接可以获取详细信息：[开发指南](https://support.huaweicloud.com/ssdk-metastudio/metastudio_06_0000.html)