package com.huawei.hwclouds.metastudio.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.metastudio.v1.MetaStudioClient;
import com.huaweicloud.sdk.metastudio.v1.model.CreateTtsAuditionRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreateTtsAuditionRequestBody;
import com.huaweicloud.sdk.metastudio.v1.model.CreateTtsAuditionResponse;
import com.huaweicloud.sdk.metastudio.v1.model.DigitalAssetInfo;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsRequest.AssetSourceEnum;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ShowTtsAuditionFileRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ShowTtsAuditionFileResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class TTSC {

    private static final Logger logger = LoggerFactory.getLogger(TTSC.class);

    // 获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID和您的AK/SK。
    // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK、HUAWEICLOUD_SDK_SK。
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");

    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    private static final String REGION_ID = "CN_NORTH_5";
    private static final String ENDPOINT = "metastudio.cn-north-5.cloudbu.huawei.com";
    public static void main(String[] args) throws IOException {
        ICredential auth = new BasicCredentials()
                .withAk(AK)
                .withSk(SK);

        // 使用默认配置
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        // 创建MetaStudio实例
        MetaStudioClient mClient = MetaStudioClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(new Region(REGION_ID, ENDPOINT))
                .build();


        // 1,创建TTS试听任务
        String emotion = listAssets(mClient, DigitalAssetInfo.AssetTypeEnum.VOICE_MODEL);
        String jobId = createTtsAudition(mClient, emotion);

        // 2,获取TTS试听文件
        showTtsAuditionFile(mClient, jobId);
    }

    /**
     * 创建TTS试听任务
     *
     */
    private static String createTtsAudition(MetaStudioClient client, String emotion) {
        logger.info("createTtsAudition start");
        CreateTtsAuditionRequest request = new CreateTtsAuditionRequest()
                .withBody(new CreateTtsAuditionRequestBody()
                        .withText("<your text>")
                        .withEmotion(emotion));
        String jobId = "";
        try {
            CreateTtsAuditionResponse response = client.createTtsAudition(request);
            jobId = response.getJobId();
            logger.info("createTtsAudition" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createTtsAudition ClientRequestException" + e.getHttpStatusCode());
            logger.error("createTtsAudition ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("createTtsAudition ServerResponseException" + e.getHttpStatusCode());
            logger.error("createTtsAudition ServerResponseException" + e.getMessage());
        }
        return jobId;
    }


    /**
     * 获取TTS试听文件
     *
     */
    private static void showTtsAuditionFile(MetaStudioClient client, String jobId) {
        logger.info("showTtsAuditionFile start");
        ShowTtsAuditionFileRequest request = new ShowTtsAuditionFileRequest()
                .withJobId(jobId);
        try {
            ShowTtsAuditionFileResponse response = client.showTtsAuditionFile(request);
            logger.info("showTtsAuditionFile" + response.toString());
        }  catch (ClientRequestException e) {
            logger.error("showTtsAuditionFile ClientRequestException" + e.getHttpStatusCode());
            logger.error("showTtsAuditionFile ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showTtsAuditionFile ServerResponseException" + e.getHttpStatusCode());
            logger.error("showTtsAuditionFile ServerResponseException" + e.getMessage());
        }
    }

    public static String listAssets(MetaStudioClient client, DigitalAssetInfo.AssetTypeEnum assetType) {
        String assertId = "";
        try {
            ListAssetsRequest listAssetsRequest = new ListAssetsRequest()
                    .withAssetType(String.valueOf(assetType))
                    .withAssetSource(AssetSourceEnum.CUSTOMIZATION)
                    .withAssetState(String.valueOf(DigitalAssetInfo.AssetStateEnum.ACTIVED));
            ListAssetsResponse response = client.listAssets(listAssetsRequest);
            logger.info("listAssets : " + response.toString());
            assertId = response.getAssets().get(0).getAssetId();
        } catch (ClientRequestException e) {
            logger.error("getAsset ClientRequestException" + e.getHttpStatusCode());
            logger.error("getAsset ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("getAsset ServerResponseException" + e.getHttpStatusCode());
            logger.error("getAsset ServerResponseException" + e.getMessage());
        }
        return assertId;
    }
}