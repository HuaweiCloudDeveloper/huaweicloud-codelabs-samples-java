package com.huawei.hwclouds.metastudio.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.metastudio.v1.MetaStudioClient;
import com.huaweicloud.sdk.metastudio.v1.model.CreateAssetbyReplicationInfoRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreateAssetbyReplicationInfoResponse;
import com.huaweicloud.sdk.metastudio.v1.model.DigitalAssetInfo.AssetTypeEnum;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsRequest.AssetSourceEnum;
import com.huaweicloud.sdk.metastudio.v1.model.ReplicationAssetInfo;
import com.huaweicloud.sdk.metastudio.v1.model.ReplicationEncInfo;
import com.huaweicloud.sdk.metastudio.v1.model.ShowAssetReplicationInfoRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ShowAssetReplicationInfoResponse;
import com.huaweicloud.sdk.metastudio.v1.region.MetaStudioRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.huaweicloud.sdk.metastudio.v1.model.CreateFileResponse;
import com.huaweicloud.sdk.metastudio.v1.model.DigitalAssetInfo;
import com.huaweicloud.sdk.metastudio.v1.model.CreateDigitalAssetRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreateDigitalAssetResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CreateFileRequest;
import com.huaweicloud.sdk.metastudio.v1.model.FilesCreateReq;
import com.huaweicloud.sdk.metastudio.v1.model.ConfirmFileUploadRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ConfirmFileUploadResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetSummaryRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetSummaryResponse;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateDigitalAssetRequest;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateDigitalAssetResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ShowAssetRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ShowAssetResponse;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteAssetRequest;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteAssetResponse;
import com.huaweicloud.sdk.metastudio.v1.model.RestoreAssetRequest;
import com.huaweicloud.sdk.metastudio.v1.model.RestoreAssetResponse;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteFileRequest;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteFileResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CreateDigitalAssetRequestBody;
import com.huaweicloud.sdk.metastudio.v1.model.ConfirmFileUploadRequestBody;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetSummarysReq;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateDigitalAssetRequestBody;
import java.io.IOException;
import java.io.File;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import cn.hutool.crypto.digest.MD5;
import java.nio.charset.StandardCharsets;

public class AssetCenterDemo {

    private static final Logger logger = LoggerFactory.getLogger(AssetCenterDemo.class);

    // 获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID和您的AK/SK。
    // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK、HUAWEICLOUD_SDK_SK。
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");

    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    public static void main(String[] args) throws IOException {
        ICredential auth = new BasicCredentials()
                .withAk(AK)
                .withSk(SK);

        // 使用默认配置
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        // 创建MetaStudio实例
        MetaStudioClient mClient = MetaStudioClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(MetaStudioRegion.CN_NORTH_4) // region为上海一需写为：withEndpoint("cn-east-3")
                .build();

        // 1, 创建资产
        String asset_id = createDigitalAsset(mClient);
        // 2, 创建文件
        String path = "female.jpg"; // 文件路径,此处以jpg文件为例
        File file = new File(path);
        byte[] digest = MD5.create().digest(file);
        String md5 = Base64.getEncoder().encodeToString(digest);
        CreateFileResponse response = createFile(mClient, asset_id, md5, file.length());
        String upload_url = response.getUploadUrl();
        String file_id = response.getFileId();
        // 3, 确认文件已上传
        Map<String, String> headers = new HashMap<String, String>(); // obs上传头部
        headers.put("Content-MD5",md5);
        headers.put("Content-Type","image/jpeg"); // 此处Content-Type需和path指向的文件匹配
        uploadFileToObs(upload_url, path, headers); // 先将path对应的文件上传到obs
        confirmFileUpload(mClient, file_id); // 确认文件已上传
        // 4, 查询资产列表，指定类型为图片
        listAssets(mClient, AssetTypeEnum.IMAGE);
        // 5, 已知资产id，查询资产概要
        listAssetSummary(mClient, asset_id);
        // 6, 已知资产id，查询资产详情
        showAsset(mClient, asset_id);
        // 7, 已知资产id，更新资产
        updateDigitalAsset(mClient, asset_id);
        // 8, 已知资产id，删除资产
        deleteAsset(mClient, asset_id);
        // 9, 已知资产id，恢复被删除的资产
        restoreAsset(mClient, asset_id);
        // 10, 已知file_id，删除文件
        deleteFile(mClient, file_id);
        // 11， 查询资产复制信息
        String voiceId = getListAssets(mClient);
        ShowAssetReplicationInfoResponse replicationInfo = showAssetReplicationInfo(mClient, voiceId);
        // 12, 复制资产（需要切换到另一个region执行以下API，此处只是参数示例）
        createAssetbyReplicationInfo(mClient, replicationInfo);
    }

    /**
     * 创建资产
     *
     */
    private static String createDigitalAsset(MetaStudioClient client) {
        logger.info("createDigitalAsset start");
        CreateDigitalAssetRequest request = new CreateDigitalAssetRequest().
                withBody(new CreateDigitalAssetRequestBody()
                        .withAssetName("test-image")
                        .withAssetType(CreateDigitalAssetRequestBody.AssetTypeEnum.IMAGE));
        String asset_id = "";
        try {
            CreateDigitalAssetResponse response = client.createDigitalAsset(request);
            logger.info("createDigitalAsset" + response.toString());
            asset_id = response.getAssetId();
        } catch (ClientRequestException e) {
            logger.error("createDigitalAsset ClientRequestException" + e.getHttpStatusCode());
            logger.error("createDigitalAsset ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("createDigitalAsset ServerResponseException" + e.getHttpStatusCode());
            logger.error("createDigitalAsset ServerResponseException" + e.getMessage());
        }
        return asset_id;
    }

    /**
     * 创建文件
     *
     */
    private static CreateFileResponse createFile(MetaStudioClient client, String asset_id, String md5, long fileLength) {
        logger.info("createFile start");
        CreateFileRequest request = new CreateFileRequest()
                .withBody(new FilesCreateReq()
                        .withAssetId(asset_id)
                        .withFileName("female.jpg")
                        .withFileType("jpg")
                        .withAssetFileCategory("MAIN")
                        .withFileSize(fileLength)
                        .withFileMd5(md5));
        CreateFileResponse response = null;

        try {
            response = client.createFile(request);
            logger.info("createFile" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createFile ClientRequestException" + e.getHttpStatusCode());
            logger.error("createFile ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("createFile ServerResponseException" + e.getHttpStatusCode());
            logger.error("createFile ServerResponseException" + e.getMessage());
        }
        return response;
    }

    /**
     * 文件上传到OBS
     *
     */
    private static void uploadFileToObs(String upload_url, String path, Map<String, String> headers) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(upload_url).openConnection();
        connection.setRequestMethod("PUT");
        connection.setDoOutput(true);
        Iterator entries = headers.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry entry = (Map.Entry) entries.next();
            connection.setRequestProperty((String) entry.getKey(),(String) entry.getValue());
        }
        logger.info(connection.getRequestProperties().toString());
        connection.connect();
        // 写入文件数据
        OutputStream outputStream = connection.getOutputStream();
        try (FileInputStream inputStream = new FileInputStream(new File(path))) {
            byte[] bytes = new byte[1024];
            int length;
            while ((length = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, length);
            }

            int responseCode = connection.getResponseCode();
            logger.info("Response Code : " + responseCode);

            InputStream response = connection.getInputStream();
            try (InputStreamReader reader = new InputStreamReader(response, StandardCharsets.UTF_8)) {
                while (reader.read() != -1) {
                    logger.info(new String(bytes, "UTF-8"));
                }
                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    logger.info(connection.getResponseMessage());
                    logger.info("upload success");
                } else {
                    logger.error("upload failed");
                }
            } catch (IOException e1) {
                logger.error("response stream open failed");
            } finally {
                response.close();
            }
        } catch (IOException e2) {
            logger.error("file input stream open failed");
        } finally {
            outputStream.close();
        }
    }


    /**
     * 确认文件已上传
     *
     */
    private static void confirmFileUpload(MetaStudioClient client, String file_id) {
        logger.info("confirmFileUpload start");
        ConfirmFileUploadRequest request = new ConfirmFileUploadRequest()
                .withFileId(file_id)
                .withBody(new ConfirmFileUploadRequestBody().withState(ConfirmFileUploadRequestBody.StateEnum.CREATED));
        try {
            ConfirmFileUploadResponse response = client.confirmFileUpload(request);
            logger.info("confirmFileUpload" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("confirmFileUpload ClientRequestException" + e.getHttpStatusCode());
            logger.error("confirmFileUpload ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("confirmFileUpload ServerResponseException" + e.getHttpStatusCode());
            logger.error("confirmFileUpload ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 查询资产列表
     *
     */
    public static void listAssets(MetaStudioClient client, DigitalAssetInfo.AssetTypeEnum assetType) {
        logger.info("listAssets start");
        try {
            ListAssetsRequest listAssetsRequest = new ListAssetsRequest()
                    .withAssetType(String.valueOf(assetType)).withAssetSource(
                            ListAssetsRequest.AssetSourceEnum.ALL).withAssetState(
                            String.valueOf(DigitalAssetInfo.AssetStateEnum.ACTIVED));
            ListAssetsResponse response = client.listAssets(listAssetsRequest);
            logger.info("listAssets" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("getAsset ClientRequestException" + e.getHttpStatusCode());
            logger.error("getAsset ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("getAsset ServerResponseException" + e.getHttpStatusCode());
            logger.error("getAsset ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 查询资产概要
     *
     */
    private static void listAssetSummary(MetaStudioClient client, String asset_id) {
        logger.info("listAssetSummary start");
        List<String>asset_ids = new ArrayList<>();
        asset_ids.add(asset_id);
        ListAssetSummaryRequest request = new ListAssetSummaryRequest()
                .withBody(new ListAssetSummarysReq()
                        .withAssetIds(asset_ids));
        try {
            ListAssetSummaryResponse response = client.listAssetSummary(request);
            logger.info("listAssetSummary" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listAssetSummary ClientRequestException" + e.getHttpStatusCode());
            logger.error("listAssetSummary ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("listAssetSummary ServerResponseException" + e.getHttpStatusCode());
            logger.error("listAssetSummary ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 更新资产
     *
     */
    private static void updateDigitalAsset(MetaStudioClient client, String asset_id) {
        logger.info("updateDigitalAsset start");
        UpdateDigitalAssetRequest request = new UpdateDigitalAssetRequest()
                .withAssetId(asset_id)
                .withBody(new UpdateDigitalAssetRequestBody());
        try {
            UpdateDigitalAssetResponse response = client.updateDigitalAsset(request);
            logger.info("updateDigitalAsset" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("updateDigitalAsset ClientRequestException" + e.getHttpStatusCode());
            logger.error("updateDigitalAsset ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("updateDigitalAsset ServerResponseException" + e.getHttpStatusCode());
            logger.error("updateDigitalAsset ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 查询资产详情
     *
     */
    private static void showAsset(MetaStudioClient client, String asset_id) {
        logger.info("showAsset start");
        ShowAssetRequest request = new ShowAssetRequest().withAssetId(asset_id);

        try {
            ShowAssetResponse response = client.showAsset(request);
            logger.info("showAsset" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showAsset ClientRequestException" + e.getHttpStatusCode());
            logger.error("showAsset ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("showAsset ServerResponseException" + e.getHttpStatusCode());
            logger.error("showAsset ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 删除资产
     *
     */
    private static void deleteAsset(MetaStudioClient client, String asset_id) {
        logger.info("deleteAsset start");
        DeleteAssetRequest request = new DeleteAssetRequest()
                .withAssetId(asset_id);
        try {
            DeleteAssetResponse response = client.deleteAsset(request);
            logger.info("deleteAsset" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("deleteAsset ClientRequestException" + e.getHttpStatusCode());
            logger.error("deleteAsset ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("deleteAsset ServerResponseException" + e.getHttpStatusCode());
            logger.error("deleteAsset ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 恢复被删除的资产
     *
     */
    private static void restoreAsset(MetaStudioClient client, String asset_id) {
        logger.info("restoreAsset start");
        RestoreAssetRequest request = new RestoreAssetRequest()
                .withAssetId(asset_id);
        try {
            RestoreAssetResponse response = client.restoreAsset(request);
            logger.info("restoreAsset" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("restoreAsset ClientRequestException" + e.getHttpStatusCode());
            logger.error("restoreAsset ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("restoreAsset ServerResponseException" + e.getHttpStatusCode());
            logger.error("restoreAsset ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 删除文件
     *
     */
    private static void deleteFile(MetaStudioClient client, String file_id) {
        logger.info("deleteFile start");
        DeleteFileRequest request = new DeleteFileRequest()
                .withFileId(file_id);
        try {
            DeleteFileResponse response = client.deleteFile(request);
            logger.info("deleteFile" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("deleteFile ClientRequestException" + e.getHttpStatusCode());
            logger.error("deleteFile ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("deleteFile ServerResponseException" + e.getHttpStatusCode());
            logger.error("deleteFile ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 查询资产复制信息
     *
     */
    private static ShowAssetReplicationInfoResponse showAssetReplicationInfo(MetaStudioClient client, String assetId) {
        logger.info("showAssetReplicationInfo start");
        ShowAssetReplicationInfoRequest request = new ShowAssetReplicationInfoRequest()
                .withAssetId(assetId);
        ShowAssetReplicationInfoResponse response = null;
        try {
            response = client.showAssetReplicationInfo(request);
            logger.info("showAssetReplicationInfo" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showAssetReplicationInfo ClientRequestException" + e.getHttpStatusCode());
            logger.error("showAssetReplicationInfo ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showAssetReplicationInfo ServerResponseException" + e.getHttpStatusCode());
            logger.error("showAssetReplicationInfo ServerResponseException" + e.getMessage());
        }
        return response;
    }


    /**
     * 复制资产
     *
     */
    private static void createAssetbyReplicationInfo(MetaStudioClient client, ShowAssetReplicationInfoResponse replicationInfo) {
        logger.info("createAssetbyReplicationInfo start");
        String replicationAssetId = replicationInfo.getAssetId();
        String replicationAssetInfo = replicationInfo.getAssetInfo();
        String replicationAlgorithm = replicationInfo.getEncryptionInfo().getAlgorithm();
        String replicationKeyId = replicationInfo.getEncryptionInfo().getKeyId();
        String replicationIv = replicationInfo.getEncryptionInfo().getIv();
        CreateAssetbyReplicationInfoRequest request = new CreateAssetbyReplicationInfoRequest().withBody(new ReplicationAssetInfo()
                .withAssetId(replicationAssetId)
                .withAssetInfo(replicationAssetInfo)
                .withEncryptionInfo(new ReplicationEncInfo()
                        .withAlgorithm(replicationAlgorithm)
                        .withKeyId(replicationKeyId)
                        .withIv(replicationIv)));

        try {
            CreateAssetbyReplicationInfoResponse response = client.createAssetbyReplicationInfo(request);
            logger.info("createAssetbyReplicationInfo" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createAssetbyReplicationInfo ClientRequestException" + e.getHttpStatusCode());
            logger.error("createAssetbyReplicationInfo ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("createAssetbyReplicationInfo ServerResponseException" + e.getHttpStatusCode());
            logger.error("createAssetbyReplicationInfo ServerResponseException" + e.getMessage());
        }
    }

    // 获取一个类型为VOICE_MODEL的资产id
    public static String getListAssets(MetaStudioClient client) {
        ListAssetsRequest listAssetsRequest = new ListAssetsRequest()
                .withAssetSource(AssetSourceEnum.CUSTOMIZATION)
                .withAssetType(AssetTypeEnum.VOICE_MODEL.toString());
        ListAssetsResponse response = client.listAssets(listAssetsRequest);
        return response.getAssets().get(0).getAssetId();
    }



}