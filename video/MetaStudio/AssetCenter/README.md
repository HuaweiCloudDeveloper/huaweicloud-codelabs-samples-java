

## 1、功能介绍

华为云提供了MetaStudio服务端SDK，您可以直接集成服务端SDK来调用MetaStudio的相关API，从而实现对MetaStudio的快速操作。 

**您将学到什么？**

如何通过java版SDK来体验MetaStudio服务的资产管理功能。

## 2、开发时序图

![image](assets/AssetCenter.png)


## 3、前置条件
- 1、已[注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=40c99ba3cea14417a2428c756a626599&lang=zh-cn)华为帐号并开通华为云，已进行[实名认证](https://support.huaweicloud.com/usermanual-account/zh-cn_topic_0077914254.html)。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、已获取账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 4、已获取MetaStudio服务对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见[API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html)。

## 4、SDK获取和安装
您可以通过Maven配置所依赖的[数字内容生产线SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/MetaStudio?lang=Java)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-metastudio</artifactId>
    <version>3.1.78</version>
</dependency>

```

## 5、接口参数说明
关于接口参数的详细说明可参见：

[a.创建资产](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateDigitalAsset)

[b.创建文件](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateFile)

[c.确认文件已上传](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ConfirmFileUpload)

[d.查询资产列表](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ListAssets)

[e.查询资产概要](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ListAssetSummary)

[f.查询资产详情](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowAsset)

[g.更新资产](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=UpdateDigitalAsset)

[h.删除资产](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=DeleteAsset)

[i.恢复被删除的资产](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=RestoreAsset)

[j.删除文件](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=DeleteFile)

[k.查询资产复制信息](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowAssetReplicationInfo)

[l.复制资产](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateAssetbyReplicationInfo)


## 6、关键代码片段

### 6.1、创建资产
```java
/**
 * 创建资产
 *
 */
private static String createDigitalAsset(MetaStudioClient client) {
    logger.info("createDigitalAsset start");
    CreateDigitalAssetRequest request = new CreateDigitalAssetRequest().
    withBody(new CreateDigitalAssetRequestBody()
        .withAssetName("test-image")
        .withAssetType(CreateDigitalAssetRequestBody.AssetTypeEnum.IMAGE));
    String asset_id = "";
    try {
        CreateDigitalAssetResponse response = client.createDigitalAsset(request);
        logger.info("createDigitalAsset" + response.toString());
        asset_id = response.getAssetId();
    } catch (ClientRequestException e) {
        logger.error("createDigitalAsset ClientRequestException" + e.getHttpStatusCode());
        logger.error("createDigitalAsset ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("createDigitalAsset ServerResponseException" + e.getHttpStatusCode());
        logger.error("createDigitalAsset ServerResponseException" + e.getMessage());
    }
    return asset_id;
    }
```

### 6.2、创建文件
```java
/**
 * 生成md5
 *
 */
private static String digest(File file) {
    try (FileInputStream fileInputStream = new FileInputStream(file);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream)) {
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        final byte[] buffer = new byte[1024];
        int read;
        while ((read = bufferedInputStream.read(buffer, 0, 1024)) > -1) {
            messageDigest.update(buffer, 0, read);
        }
        byte[] digest = messageDigest.digest();
        return Base64.getEncoder().encodeToString(digest);
    } catch (NoSuchAlgorithmException e) {
        logger.error("message digest get instance failed");
        throw new RuntimeException(e);
    } catch (IOException e) {
        logger.error("read file failed");
        throw new RuntimeException(e);
    }
}

/**
 * 创建文件
 *
 */
private static CreateFileResponse createFile(MetaStudioClient client, String asset_id, String md5, long fileLength) {
    logger.info("createFile start");
    logger.info("asset_id = " + asset_id);
    CreateFileRequest request = new CreateFileRequest()
        .withBody(new FilesCreateReq()
        .withAssetId(asset_id)
        .withFileName("female.jpg")
        .withFileType("jpg")
        .withAssetFileCategory("MAIN")
        .withFileSize(fileLength)
        .withFileMd5(md5));
    CreateFileResponse response = null;

    try {
        response = client.createFile(request);
        logger.info("createFile" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("createFile ClientRequestException" + e.getHttpStatusCode());
        logger.error("createFile ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("createFile ServerResponseException" + e.getHttpStatusCode());
        logger.error("createFile ServerResponseException" + e.getMessage());
    }
    return response;
}
```

### 6.3、确认文件已上传
```java
/**
 * 获取文件上传头域
 *
 */
private static Map<String, String> getUploadFileHeaders(String fileType, String md5) {
    Map<String, String> headers = new HashMap<>();
    headers.put("Content-MD5", md5);
    if (StringUtils.isEmpty(fileType)) {
        headers.put("Content-Type", "application/octet-stream");
        return headers;
    }
    String lowerFileType = StringUtils.lowerCase(fileType);
    switch (lowerFileType) {
        case "png":
            headers.put("Content-Type", "image/png");
            break;
        case "jpg":
        case "jpeg":
            headers.put("Content-Type", "image/jpeg");
            break;
        case "gif":
            headers.put("Content-Type", "image/gif");
            break;
        case "mp3":
            headers.put("Content-Type", "audio/mp3");
            break;
        case "wav":
            headers.put("Content-Type", "audio/wav");
            break;
        case "mp4":
            headers.put("Content-Type", "video/mp4");
            break;
        default:
            headers.put("Content-Type", "application/octet-stream");
            break;
    }
    return headers;
}

/**
 * 文件上传到OBS
 *
 */
private static void uploadFileToObs(String upload_url, String path, Map<String, String> headers) throws IOException {
    HttpURLConnection connection = (HttpURLConnection) new URL(upload_url).openConnection();
    connection.setRequestMethod("PUT");
    connection.setDoOutput(true);
    Iterator entries = headers.entrySet().iterator();
    while (entries.hasNext()) {
        Map.Entry entry = (Map.Entry) entries.next();
        connection.setRequestProperty((String) entry.getKey(),(String) entry.getValue());
    }
    logger.info(connection.getRequestProperties().toString());
    connection.connect();
    // 写入文件数据
    OutputStream outputStream = connection.getOutputStream();
    try (FileInputStream inputStream = new FileInputStream(new File(path))) {
        byte[] bytes = new byte[1024];
        int length;
        while ((length = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, length);
        }

        int responseCode = connection.getResponseCode();
        logger.info("Response Code : " + responseCode);
    
        InputStream response = connection.getInputStream();
        try (InputStreamReader reader = new InputStreamReader(response, StandardCharsets.UTF_8)) {
            while (reader.read() != -1) {
                logger.info(new String(bytes, "UTF-8"));
            }
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                logger.info(connection.getResponseMessage());
                logger.info("upload success");
            } else {
                logger.error("upload failed");
            }
        } catch (IOException e1) {
            logger.error("response stream open failed");
        } finally {
            response.close();
        }
    } catch (IOException e2) {
        logger.error("file input stream open failed");
    } finally {
        outputStream.close();
    }
}

/**
 * 确认文件已上传
 *
 */
private static void confirmFileUpload(MetaStudioClient client, String file_id) {
    logger.info("confirmFileUpload start");
    ConfirmFileUploadRequest request = new ConfirmFileUploadRequest()
            .withFileId(file_id)
            .withBody(new ConfirmFileUploadRequestBody().withState(ConfirmFileUploadRequestBody.StateEnum.CREATED));
    try {
        ConfirmFileUploadResponse response = client.confirmFileUpload(request);
        logger.info("confirmFileUpload" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("confirmFileUpload ClientRequestException" + e.getHttpStatusCode());
        logger.error("confirmFileUpload ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("confirmFileUpload ServerResponseException" + e.getHttpStatusCode());
        logger.error("confirmFileUpload ServerResponseException" + e.getMessage());
    }
}
```

### 6.4、查询资产列表
```java
/**
 * 查询资产列表
 *
 */
public static void listAssets(MetaStudioClient client, DigitalAssetInfo.AssetTypeEnum assetType) {
    logger.info("listAssets start");
    DigitalAssetInfo digitalAssetInfo = null;
    try {
        ListAssetsRequest listAssetsRequest = new ListAssetsRequest()
                .withAssetType(String.valueOf(assetType)).withAssetSource(
                        ListAssetsRequest.AssetSourceEnum.ALL).withAssetState(
                        String.valueOf(DigitalAssetInfo.AssetStateEnum.ACTIVED));
        ListAssetsResponse response = client.listAssets(listAssetsRequest);
        logger.info("listAssets" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("getAsset ClientRequestException" + e.getHttpStatusCode());
        logger.error("getAsset ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("getAsset ServerResponseException" + e.getHttpStatusCode());
        logger.error("getAsset ServerResponseException" + e.getMessage());
    }
}
```

### 6.5、查询资产概要
```java
/**
 * 查询资产概要
 *
 */
private static void listAssetSummary(MetaStudioClient client, String asset_id) {
    logger.info("listAssetSummary start");
    List<String>asset_ids = new ArrayList<>();
    asset_ids.add(asset_id);
    ListAssetSummaryRequest request = new ListAssetSummaryRequest()
            .withBody(new ListAssetSummarysReq()
                    .withAssetIds(asset_ids));
    try {
        ListAssetSummaryResponse response = client.listAssetSummary(request);
        logger.info("listAssetSummary" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("listAssetSummary ClientRequestException" + e.getHttpStatusCode());
        logger.error("listAssetSummary ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("listAssetSummary ServerResponseException" + e.getHttpStatusCode());
        logger.error("listAssetSummary ServerResponseException" + e.getMessage());
    }
}
```

### 6.6、查询资产详情
```java
/**
 * 查询资产详情
 *
 */
private static void showAsset(MetaStudioClient client, String asset_id) {
    logger.info("showAsset start");
    ShowAssetRequest request = new ShowAssetRequest().withAssetId(asset_id);

    try {
        ShowAssetResponse response = client.showAsset(request);
        logger.info("showAsset" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("showAsset ClientRequestException" + e.getHttpStatusCode());
        logger.error("showAsset ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("showAsset ServerResponseException" + e.getHttpStatusCode());
        logger.error("showAsset ServerResponseException" + e.getMessage());
    }
}
```

### 6.7、更新资产
```java
/**
 * 更新资产
 *
 */
private static void updateDigitalAsset(MetaStudioClient client, String asset_id) {
    logger.info("updateDigitalAsset start");
    UpdateDigitalAssetRequest request = new UpdateDigitalAssetRequest()
            .withAssetId(asset_id)
            .withBody(new UpdateDigitalAssetRequestBody());
    try {
        UpdateDigitalAssetResponse response = client.updateDigitalAsset(request);
        logger.info("updateDigitalAsset" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("updateDigitalAsset ClientRequestException" + e.getHttpStatusCode());
        logger.error("updateDigitalAsset ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("updateDigitalAsset ServerResponseException" + e.getHttpStatusCode());
        logger.error("updateDigitalAsset ServerResponseException" + e.getMessage());
    }
}
```

### 6.8、删除资产
```java
/**
 * 删除资产
 *
 */
private static void deleteAsset(MetaStudioClient client, String asset_id) {
    logger.info("deleteAsset start");
    DeleteAssetRequest request = new DeleteAssetRequest()
            .withAssetId(asset_id);
    try {
        DeleteAssetResponse response = client.deleteAsset(request);
        logger.info("deleteAsset" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("deleteAsset ClientRequestException" + e.getHttpStatusCode());
        logger.error("deleteAsset ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("deleteAsset ServerResponseException" + e.getHttpStatusCode());
        logger.error("deleteAsset ServerResponseException" + e.getMessage());
    }
}
```

### 6.9、恢复被删除的资产
```java
/**
 * 恢复被删除的资产
 *
 */
private static void restoreAsset(MetaStudioClient client, String asset_id) {
    logger.info("restoreAsset start");
    RestoreAssetRequest request = new RestoreAssetRequest()
            .withAssetId(asset_id);
    try {
        RestoreAssetResponse response = client.restoreAsset(request);
        logger.info("restoreAsset" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("restoreAsset ClientRequestException" + e.getHttpStatusCode());
        logger.error("restoreAsset ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("restoreAsset ServerResponseException" + e.getHttpStatusCode());
        logger.error("restoreAsset ServerResponseException" + e.getMessage());
    }
}
```

### 6.10、删除文件
```java
/**
 * 删除文件
 *
 */
private static void deleteFile(MetaStudioClient client, String file_id) {
    logger.info("deleteFile start");
    DeleteFileRequest request = new DeleteFileRequest()
            .withFileId(file_id);
    try {
        DeleteFileResponse response = client.deleteFile(request);
        logger.info("deleteFile" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("deleteFile ClientRequestException" + e.getHttpStatusCode());
        logger.error("deleteFile ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("deleteFile ServerResponseException" + e.getHttpStatusCode());
        logger.error("deleteFile ServerResponseException" + e.getMessage());
    }
}
```

### 6.11、查询资产复制信息
```java
/**
 * 查询资产复制信息
 *
 */
private static ShowAssetReplicationInfoResponse showAssetReplicationInfo(MetaStudioClient client, String assetId) {
    logger.info("showAssetReplicationInfo start");
    ShowAssetReplicationInfoRequest request = new ShowAssetReplicationInfoRequest()
            .withAssetId(assetId);
    ShowAssetReplicationInfoResponse response = null;
    try {
        response = client.showAssetReplicationInfo(request);
        logger.info("showAssetReplicationInfo" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("showAssetReplicationInfo ClientRequestException" + e.getHttpStatusCode());
        logger.error("showAssetReplicationInfo ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("showAssetReplicationInfo ServerResponseException" + e.getHttpStatusCode());
        logger.error("showAssetReplicationInfo ServerResponseException" + e.getMessage());
    }
    return response;
}
```

### 6.12、复制资产
```java
/**
 * 复制资产
 *
 */
private static void createAssetbyReplicationInfo(MetaStudioClient client, ShowAssetReplicationInfoResponse replicationInfo) {
    logger.info("createAssetbyReplicationInfo start");
    String replicationAssetId = replicationInfo.getAssetId();
    String replicationAssetInfo = replicationInfo.getAssetInfo();
    String replicationAlgorithm = replicationInfo.getEncryptionInfo().getAlgorithm();
    String replicationKeyId = replicationInfo.getEncryptionInfo().getKeyId();
    String replicationIv = replicationInfo.getEncryptionInfo().getIv();
    CreateAssetbyReplicationInfoRequest request = new CreateAssetbyReplicationInfoRequest().withBody(new ReplicationAssetInfo()
            .withAssetId(replicationAssetId)
            .withAssetInfo(replicationAssetInfo)
            .withEncryptionInfo(new ReplicationEncInfo()
                    .withAlgorithm(replicationAlgorithm)
                    .withKeyId(replicationKeyId)
                    .withIv(replicationIv)));

    try {
        CreateAssetbyReplicationInfoResponse response = client.createAssetbyReplicationInfo(request);
        logger.info("createAssetbyReplicationInfo" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("createAssetbyReplicationInfo ClientRequestException" + e.getHttpStatusCode());
        logger.error("createAssetbyReplicationInfo ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("createAssetbyReplicationInfo ServerResponseException" + e.getHttpStatusCode());
        logger.error("createAssetbyReplicationInfo ServerResponseException" + e.getMessage());
    }
}
```


## 7、运行结果
**创建资产**
```json
{
  "X-Request-Id":"c99fde***",
  "asset_id":"2dbf21***"
}
```

**创建文件**
```json
{
	"X-Request-Id":"69f084***",
	"file_id":"877102***",
	"upload_url":"https://***"
}
```

**确认文件已上传**
```json
{
	"X-Request-Id":"dbf0f9***"
}
```

**查询资产列表**
```json
{
  "assets": [
    {
      "system_properties": [],
      "update_time": "20240129092915",
      "asset_name": "test-image",
      "create_time": "20240129092914",
      "asset_state": "ACTIVED",
      "asset_type": "IMAGE",
      "files": [
        {
          "file_md5": "zuy3lI***",
          "file_name": "female.jpg",
          "file_type": "jpg",
          "file_id": "027950***",
          "download_url": "https://***",
          "asset_file_category": "MAIN",
          "state": "CREATED",
          "file_size": 1163098
        }
      ],
      "asset_id": "62d5dd***",
      "tags": []
    }
  ]
}
```

**查询资产概要**
```json
{
	"asset_list":[
		{
			"asset_name":"test-image",
			"asset_type":"IMAGE",
			"asset_id":"62d5dd***"
		}
	],
	"X-Request-Id":"dea950***"
}
```

**查询资产详情**
```json
{
	"system_properties":[],
	"update_time":"20240129092915",
	"asset_name":"test-image",
	"create_time":"20240129092914",
	"X-Request-Id":"fbdc8d***",
	"asset_state":"ACTIVED",
	"asset_type":"IMAGE",
	"files":[
		{
			"file_md5":"zuy3lI***",
			"file_name":"female.jpg",
			"file_type":"jpg",
			"file_id":"027950***",
            "download_url":"https://***",
			"asset_file_category":"MAIN",
			"state":"CREATED",
			"file_size":1163098
		}
	],
	"asset_id":"62d5dd***",
	"tags":[]
}
```

**更新资产**
```json
{
	"system_properties":[],
	"update_time":"20240129092915",
	"asset_name":"test-image",
	"create_time":"20240129092914",
	"X-Request-Id":"79b6e6***",
	"asset_state":"ACTIVED",
	"asset_type":"IMAGE",
	"files":[
		{
			"file_md5":"zuy3lI***",
			"file_name":"female.jpg",
			"file_type":"jpg",
			"file_id":"027950***",
            "download_url":"https://***",
			"asset_file_category":"MAIN",
			"state":"CREATED",
			"file_size":1163098
		}
	],
	"asset_id":"62d5dd***",
	"tags":[]
}
```

**删除资产**
```json
{
	"X-Request-Id":"400132***"
}
```

**恢复被删除的资产**
```json
{
	"X-Request-Id":"2fc6bf***"
}
```

**删除文件**
```json
{
	"X-Request-Id":"6d1f3b***"
}
```

**查询资产复制信息**
```json
{
    "assetId": "bd9807***",
    "assetInfo": "5FiDdl***",
    "encryptionInfo": {
        "algorithm": "AES128_GCM",
        "keyId": 1,
        "iv": "5telJR***"
    },
    "xRequestId": "806cc2***"
}
```

**复制资产**
```json
{
   "xRequestId": "806cc2***"
}
```

## 8、参考
本示例的代码工程仅用于简单演示，实际开发过程中应严格遵循开发指南。访问以下链接可以获取详细信息：[开发指南](https://support.huaweicloud.com/sdkreference-mpc/mpc_05_0123.html)