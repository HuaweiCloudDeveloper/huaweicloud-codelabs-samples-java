

## 1、功能介绍

华为云提供了MetaStudio服务端SDK，您可以直接集成服务端SDK来调用MetaStudio的相关API，从而实现对MetaStudio的快速操作。

**您将学到什么？**

如何通过java版SDK来体验MetaStudio服务的数字人名片制作功能。

## 2、开发时序图

![image](assets/BusinessCard.jpg)


## 3、前置条件
- 1、已[注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=40c99ba3cea14417a2428c756a626599&lang=zh-cn)华为帐号并开通华为云，已进行[实名认证](https://support.huaweicloud.com/usermanual-account/zh-cn_topic_0077914254.html)。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、已获取账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 4、已获取MetaStudio服务对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见[API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html)。 

## 4、SDK获取和安装
您可以通过Maven配置所依赖的[数字内容生产线SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/MetaStudio?lang=Java)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-metastudio</artifactId>
    <version>3.1.78</version>
</dependency>

```

## 5、接口参数说明
关于接口参数的详细说明可参见：

[a.创建数字人名片制作](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateDigitalHumanBusinessCard)

[b.查询数字人名片制作任务列表](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ListDigitalHumanBusinessCard)

[c.查询数字人名片制作任务详情](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowDigitalHumanBusinessCard)

[d.更新数字人名片制作](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=UpdateDigitalHumanBusinessCard)

[e.删除数字人名片制作任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=DeleteDigitalHumanBusinessCard)

## 6、关键代码片段

### 6.1、创建数字人名片制作

```java
/**
 * 创建数字人名片制作
 *
 */
public static String createDigitalHumanBusinessCard(MetaStudioClient client, String imagePath) {
    logger.info("createDigitalHumanBusinessCard start");
    String jobId = null;
    try {
        CreateDigitalHumanBusinessCardReq req = buildCreateDigitalHumanBusinessCardReq(client, imagePath);
        CreateDigitalHumanBusinessCardRequest request = new CreateDigitalHumanBusinessCardRequest()
        .withBody(req);
        // 发起请求
        CreateDigitalHumanBusinessCardResponse response = client.createDigitalHumanBusinessCard(request);
        jobId = response.getJobId();
        logger.info("createDigitalHumanBusinessCard " + response.toString());
    } catch (ClientRequestException e) {
        logger.error("createDigitalHumanBusinessCard ClientRequestException " + e.getHttpStatusCode());
        logger.error("createDigitalHumanBusinessCard ClientRequestException " + e);
    } catch (ServerResponseException e) {
        logger.error("createDigitalHumanBusinessCard ServerResponseException " + e.getHttpStatusCode());
        logger.error("createDigitalHumanBusinessCard ServerResponseException " + e.getMessage());
    }
    return jobId;
}
```

### 6.2、查询数字人名片制作任务列表

```java
/**
 * 查询数字人名片制作任务列表
 *
 */
private static void listDigitalHumanBusinessCard(MetaStudioClient client) {
    logger.info("listDigitalHumanBusinessCard start");
    ListDigitalHumanBusinessCardRequest request = new ListDigitalHumanBusinessCardRequest();
    try {
        ListDigitalHumanBusinessCardResponse response = client.listDigitalHumanBusinessCard(request);
        logger.info("listDigitalHumanBusinessCard" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("listDigitalHumanBusinessCard ClientRequestException" + e.getHttpStatusCode());
        logger.error("listDigitalHumanBusinessCard ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("listDigitalHumanBusinessCard ServerResponseException" + e.getHttpStatusCode());
        logger.error("listDigitalHumanBusinessCard ServerResponseException" + e.getMessage());
    }
}
```

### 6.3、查询数字人名片制作任务详情 

```java
/**
 * 查询数字人名片制作任务详情
 *
 */
private static void showDigitalHumanBusinessCard(MetaStudioClient client, String jobId) {
    logger.info("showDigitalHumanBusinessCard start");
    ShowDigitalHumanBusinessCardRequest request = new ShowDigitalHumanBusinessCardRequest().withJobId(jobId);
    try {
        ShowDigitalHumanBusinessCardResponse response = client.showDigitalHumanBusinessCard(request);
        logger.info("showDigitalHumanBusinessCard " + response.toString());
    } catch (ClientRequestException e) {
       logger.error("showDigitalHumanBusinessCard ClientRequestException " + e.getHttpStatusCode());
       logger.error("showDigitalHumanBusinessCard ClientRequestException " + e);
    } catch (ServerResponseException e) {
        logger.error("showDigitalHumanBusinessCard ServerResponseException " + e.getHttpStatusCode());
        logger.error("showDigitalHumanBusinessCard ServerResponseException " + e.getMessage());
    }
}
```
### 6.4、更新数字人名片制作
```java
/**
 * 更新数字人名片制作
 *
 */
private static void updateDigitalHumanBusinessCard(MetaStudioClient client, String jobId, String imagePath) {
    logger.info("updateDigitalHumanBusinessCard start");
    try {
        CreateDigitalHumanBusinessCardReq req = buildCreateDigitalHumanBusinessCardReq(client, imagePath).withVideoAssetName("<your video asset name>");
        UpdateDigitalHumanBusinessCardRequest request = new UpdateDigitalHumanBusinessCardRequest()
        .withJobId(jobId)
        .withBody(req);
        UpdateDigitalHumanBusinessCardResponse response = client.updateDigitalHumanBusinessCard(request);
        logger.info("updateDigitalHumanBusinessCard " + response.toString());
    } catch (ClientRequestException e) {
        logger.error("updateDigitalHumanBusinessCard ClientRequestException " + e.getHttpStatusCode());
        logger.error("updateDigitalHumanBusinessCard ClientRequestException " + e);
    } catch (ServerResponseException e) {
        logger.error("updateDigitalHumanBusinessCard ServerResponseException " + e.getHttpStatusCode());
        logger.error("updateDigitalHumanBusinessCard ServerResponseException " + e.getMessage());
    }
}
```
### 6.5、删除数字人名片制作任务
```java
/**
 * 删除数字人名片制作任务
 *
 */
private static void deleteDigitalHumanBusinessCard(MetaStudioClient client, String jobId) {
    logger.info("deleteDigitalHumanBusinessCard start");
    DeleteDigitalHumanBusinessCardRequest request = new DeleteDigitalHumanBusinessCardRequest().withJobId(jobId);
    try {
        DeleteDigitalHumanBusinessCardResponse response = client.deleteDigitalHumanBusinessCard(request);
        logger.info("deleteDigitalHumanBusinessCard" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("deleteDigitalHumanBusinessCard ClientRequestException" + e.getHttpStatusCode());
        logger.error("deleteDigitalHumanBusinessCard ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("deleteDigitalHumanBusinessCard ServerResponseException" + e.getHttpStatusCode());
        logger.error("deleteDigitalHumanBusinessCard ServerResponseException" + e.getMessage());
    }
}

```

## 7、运行结果
**创建数字人名片制作**
```json
{
  "X-Request-Id":"d923e9***",
  "job_id":"99c556***"
}
```
**查询数字人名片制作任务列表**
```json
{
  "X-Request-Id":"63202a***",
  "jobs":[
    {
      "output_asset_config":{
        "asset_name":"name",
        "asset_id":"8894bd***"
      },"business_card_type":"2D_DIGITAL_HUMAN_CARD",
      "create_time":"2024-01-26T02:47:47Z",
      "job_id":"b37e93***",
      "lastupdate_time":"2024-01-26T02:47:47Z",
      "state":"WAITING"
    }
  ],
  "count":1752
}

```
**查询数字人名片制作任务详情**
```json
{
  "card_templet_asset_id":"cf1cb0***",
  "introduction_text":"<your introduction text>",
  "card_text_config":{
    "address":"address",
    "mobile_phone":"123456789",
    "name":"name",
    "company":"company",
    "title":"title"
  },"X-Request-Id":"01374d***",
  "card_image_url":{
    "human_image_url":"https://***"
  },"job_info":{
    "output_asset_config":{
      "asset_name":"name",
      "asset_id":"8894bd***"
    },"business_card_type":"2D_DIGITAL_HUMAN_CARD",
    "create_time":"2024-01-26T02:47:47Z",
    "job_id":"b37e93***",
    "lastupdate_time":"2024-01-26T02:47:47Z",
    "state":"WAITING"
  },"introduction_type":"TEXT",
  "voice_asset_id":"36c956***"
}
```
**更新数字人名片制作**
```json
{
  "X-Request-Id":"75f546***",
  "job_id":"b37e93***"
}
```
**删除数字人名片制作任务**
```json
{
  "X-Request-Id":"9155f6***"
}
```
## 8、参考
本示例的代码工程仅用于简单演示，实际开发过程中应严格遵循开发指南。访问以下链接可以获取详细信息：[开发指南](https://support.huaweicloud.com/sdkreference-mpc/mpc_05_0123.html)