package com.huawei.hwclouds.metastudio.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.metastudio.v1.MetaStudioClient;
import com.huaweicloud.sdk.metastudio.v1.model.BusinessCardImageConfig;
import com.huaweicloud.sdk.metastudio.v1.model.BusinessCardTextConfig;
import com.huaweicloud.sdk.metastudio.v1.model.CreateDigitalHumanBusinessCardReq;
import com.huaweicloud.sdk.metastudio.v1.model.CreateDigitalHumanBusinessCardRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreateDigitalHumanBusinessCardResponse;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteDigitalHumanBusinessCardRequest;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteDigitalHumanBusinessCardResponse;
import com.huaweicloud.sdk.metastudio.v1.model.DigitalAssetInfo;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsRequest.AssetSourceEnum;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ListDigitalHumanBusinessCardRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListDigitalHumanBusinessCardResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ShowDigitalHumanBusinessCardRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ShowDigitalHumanBusinessCardResponse;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateDigitalHumanBusinessCardRequest;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateDigitalHumanBusinessCardResponse;
import com.huaweicloud.sdk.metastudio.v1.region.MetaStudioRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Base64;

public class BusinessCardDemo {

    private static final Logger logger = LoggerFactory.getLogger(BusinessCardDemo.class);

    // 获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID和您的AK/SK。
    // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK、HUAWEICLOUD_SDK_SK。

    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");

    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");


    public static void main(String[] args) {
        ICredential auth = new BasicCredentials()
            .withAk(AK)
            .withSk(SK);
        // 使用默认配置
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        // 创建MetaStudio实例
        MetaStudioClient mClient = MetaStudioClient.newBuilder()
            .withCredential(auth)
            .withHttpConfig(config)
            .withRegion(MetaStudioRegion.CN_NORTH_4) // region为上海一需写为：withEndpoint("cn-east-3")
            .build();

        // 1,创建数字人名片制作
        String imagePath = "XXX.jpg"; // 输入您的图片路径
        String jobId = createDigitalHumanBusinessCard(mClient, imagePath);

        // 2,查询数字人名片制作任务列表
        listDigitalHumanBusinessCard(mClient);

        // 3,查询数字人名片制作任务详情
        showDigitalHumanBusinessCard(mClient, jobId);

        // 4,更新数字人名片制作
        updateDigitalHumanBusinessCard(mClient, jobId, imagePath);

        // 5,删除数字人名片制作任务
        deleteDigitalHumanBusinessCard(mClient, jobId);

    }

    /**
     * 创建数字人名片制作
     *
     */
    public static String createDigitalHumanBusinessCard(MetaStudioClient client, String imagePath) {
        logger.info("createDigitalHumanBusinessCard start");
        String jobId = null;
        try {
            CreateDigitalHumanBusinessCardReq req = buildCreateDigitalHumanBusinessCardReq(client, imagePath);
            CreateDigitalHumanBusinessCardRequest request = new CreateDigitalHumanBusinessCardRequest()
                .withBody(req);
            // 发起请求
            CreateDigitalHumanBusinessCardResponse response = client.createDigitalHumanBusinessCard(request);
            jobId = response.getJobId();
            logger.info("createDigitalHumanBusinessCard " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createDigitalHumanBusinessCard ClientRequestException " + e.getHttpStatusCode());
            logger.error("createDigitalHumanBusinessCard ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("createDigitalHumanBusinessCard ServerResponseException " + e.getHttpStatusCode());
            logger.error("createDigitalHumanBusinessCard ServerResponseException " + e.getMessage());
        }
        return jobId;
    }

    /**
     * 构建创建数字人名片制作任务请求体
     *
     */
    private static CreateDigitalHumanBusinessCardReq buildCreateDigitalHumanBusinessCardReq(MetaStudioClient client, String imagePath) {
        // 名片信息
        BusinessCardTextConfig cardTextConfig = new BusinessCardTextConfig()
            .withName("<your card name>")
            .withTitle("<your title>")
            .withAddress("<your address>")
            .withCompany("<your company>")
            .withMobilePhone("<your MobilePhone>");
        // 图片信息
        BusinessCardImageConfig cardImageConfig = new BusinessCardImageConfig().withHumanImage(encodeImageToBase64(imagePath));

        // 请求体
        return new CreateDigitalHumanBusinessCardReq().withBusinessCardType(
                CreateDigitalHumanBusinessCardReq.BusinessCardTypeEnum._2D_DIGITAL_HUMAN_CARD)
                    .withVoiceAssetId(getAsset(client, DigitalAssetInfo.AssetTypeEnum.VOICE_MODEL, AssetSourceEnum.SYSTEM, null).getAssetId())
                    .withCardTempletAssetId(getAsset(client, DigitalAssetInfo.AssetTypeEnum.BUSINESS_CARD_TEMPLET, AssetSourceEnum.SYSTEM, null).getAssetId())
                    .withCardTextConfig(cardTextConfig)
                    .withCardImageConfig(cardImageConfig)
                    .withIntroductionText("<your introduction text>");
    }

    /**
     * 查询数字人名片制作任务列表
     *
     */
    private static void listDigitalHumanBusinessCard(MetaStudioClient client) {
        logger.info("listDigitalHumanBusinessCard start");
        ListDigitalHumanBusinessCardRequest request = new ListDigitalHumanBusinessCardRequest();
        try {
            ListDigitalHumanBusinessCardResponse response = client.listDigitalHumanBusinessCard(request);
            logger.info("listDigitalHumanBusinessCard" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listDigitalHumanBusinessCard ClientRequestException" + e.getHttpStatusCode());
            logger.error("listDigitalHumanBusinessCard ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("listDigitalHumanBusinessCard ServerResponseException" + e.getHttpStatusCode());
            logger.error("listDigitalHumanBusinessCard ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 查询数字人名片制作任务详情
     *
     */
    private static void showDigitalHumanBusinessCard(MetaStudioClient client, String jobId) {
        logger.info("showDigitalHumanBusinessCard start");
        ShowDigitalHumanBusinessCardRequest request = new ShowDigitalHumanBusinessCardRequest().withJobId(jobId);
        try {
            ShowDigitalHumanBusinessCardResponse response = client.showDigitalHumanBusinessCard(request);
            logger.info("showDigitalHumanBusinessCard " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showDigitalHumanBusinessCard ClientRequestException " + e.getHttpStatusCode());
            logger.error("showDigitalHumanBusinessCard ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("showDigitalHumanBusinessCard ServerResponseException " + e.getHttpStatusCode());
            logger.error("showDigitalHumanBusinessCard ServerResponseException " + e.getMessage());
        }
    }

    /**
     * 更新数字人名片制作
     *
     */
    private static void updateDigitalHumanBusinessCard(MetaStudioClient client, String jobId, String imagePath) {
        logger.info("updateDigitalHumanBusinessCard start");
        try {
            CreateDigitalHumanBusinessCardReq req = buildCreateDigitalHumanBusinessCardReq(client, imagePath).withVideoAssetName("<your video asset name>");
            UpdateDigitalHumanBusinessCardRequest request = new UpdateDigitalHumanBusinessCardRequest()
                .withJobId(jobId)
                .withBody(req);
            UpdateDigitalHumanBusinessCardResponse response = client.updateDigitalHumanBusinessCard(request);
            logger.info("updateDigitalHumanBusinessCard " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("updateDigitalHumanBusinessCard ClientRequestException " + e.getHttpStatusCode());
            logger.error("updateDigitalHumanBusinessCard ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("updateDigitalHumanBusinessCard ServerResponseException " + e.getHttpStatusCode());
            logger.error("updateDigitalHumanBusinessCard ServerResponseException " + e.getMessage());
        }
    }

    /**
     * 删除数字人名片制作任务
     *
     */
    private static void deleteDigitalHumanBusinessCard(MetaStudioClient client, String jobId) {
        logger.info("deleteDigitalHumanBusinessCard start");
        DeleteDigitalHumanBusinessCardRequest request = new DeleteDigitalHumanBusinessCardRequest().withJobId(jobId);
        try {
            DeleteDigitalHumanBusinessCardResponse response = client.deleteDigitalHumanBusinessCard(request);
            logger.info("deleteDigitalHumanBusinessCard" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("deleteDigitalHumanBusinessCard ClientRequestException" + e.getHttpStatusCode());
            logger.error("deleteDigitalHumanBusinessCard ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("deleteDigitalHumanBusinessCard ServerResponseException" + e.getHttpStatusCode());
            logger.error("deleteDigitalHumanBusinessCard ServerResponseException" + e.getMessage());
        }
    }


    private static DigitalAssetInfo getAsset(MetaStudioClient client, DigitalAssetInfo.AssetTypeEnum assetType, ListAssetsRequest.AssetSourceEnum assetSource, String systemProperty) {
        DigitalAssetInfo digitalAssetInfo = null;
        try {
            ListAssetsRequest listAssetsRequest = new ListAssetsRequest().withSystemProperty(systemProperty) // 系统属性
                    .withAssetType(String.valueOf(assetType)) // 资产类型
                    .withAssetSource(assetSource) // 资产来源：系统资产
                    .withAssetState(String.valueOf(DigitalAssetInfo.AssetStateEnum.ACTIVED)); // 主文件上传成功，资产激活。
            logger.info("ListAssetsRequest " + listAssetsRequest.toString());
            ListAssetsResponse response = client.listAssets(listAssetsRequest);
            digitalAssetInfo = response.getAssets().get(0);
        } catch (ClientRequestException e) {
            logger.error("getAsset ClientRequestException " + e.getHttpStatusCode());
            logger.error("getAsset ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("getAsset ServerResponseException " + e.getHttpStatusCode());
            logger.error("getAsset ServerResponseException" + e.getMessage());
        }
        return digitalAssetInfo;
    }

    // 对图片进行 base64 编码
    public static String encodeImageToBase64(String imgFile) { // 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        File imageFile = new File(imgFile);
        String ret = "";
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            BufferedImage bufferedImage = ImageIO.read(imageFile);
            ImageIO.write(bufferedImage, "jpg", outputStream);
            // 对字节数组进行Base64编码
            Base64.Encoder encoder = Base64.getEncoder();
            ret = encoder.encodeToString(outputStream.toByteArray());
        } catch (MalformedURLException e1) {
            logger.error("encode image file failed: " + e1.getMessage());
        } catch (IOException e) {
            logger.error("encode image file failed: " + e.getMessage());
        }
        logger.info("BASE64: ");
        logger.info(ret);    
        return ret;
    }
}