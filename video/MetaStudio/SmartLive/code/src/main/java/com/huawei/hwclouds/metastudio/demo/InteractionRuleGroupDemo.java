package com.huawei.hwclouds.metastudio.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.metastudio.v1.MetaStudioClient;
import com.huaweicloud.sdk.metastudio.v1.model.CreateInteractionRuleGroupRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreateInteractionRuleGroupResponse;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteInteractionRuleGroupRequest;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteInteractionRuleGroupResponse;
import com.huaweicloud.sdk.metastudio.v1.model.InteractionRuleGroup;
import com.huaweicloud.sdk.metastudio.v1.model.ListInteractionRuleGroupsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListInteractionRuleGroupsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ListSmartLiveJobsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListSmartLiveJobsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateInteractionRuleGroupRequest;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateInteractionRuleGroupResponse;

import com.huaweicloud.sdk.metastudio.v1.region.MetaStudioRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class InteractionRuleGroupDemo {

    private static final Logger logger = LoggerFactory.getLogger(SmartLiveDemo.class);

    // 获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID和您的AK/SK。
    // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK、HUAWEICLOUD_SDK_SK。

    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    public static void main(String[] args) throws IOException {
        ICredential auth = new BasicCredentials()
                .withAk(AK)
                .withSk(SK);

        // 使用默认配置
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        // 创建MetaStudio实例
        MetaStudioClient mClient = MetaStudioClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(MetaStudioRegion.CN_NORTH_4)
                .build();

        // 12,创建智能直播间互动规则库
        String group_id = createInteractionRuleGroup(mClient);

        // 13,更新智能直播间互动规则库
        updateInteractionRuleGroup(mClient, group_id);

        // 14,查询智能直播间互动规则库列表
        listInteractionRuleGroups(mClient);

        // 15,删除智能直播间互动规则库
        deleteInteractionRuleGroup(mClient, group_id);

        // 16,查询数字人智能直播任务列表
        listSmartLiveJobs(mClient);
    }

    /**
     * 创建智能直播间互动规则库
     */
    private static String createInteractionRuleGroup(MetaStudioClient client) {
        logger.info("createInteractionRuleGroup start ");
        InteractionRuleGroup body = new InteractionRuleGroup().withGroupName("<your group name>");
        CreateInteractionRuleGroupRequest request = new CreateInteractionRuleGroupRequest()
                .withBody(body);
        String group_id = null;
        try {
            CreateInteractionRuleGroupResponse response = client.createInteractionRuleGroup(request);
            group_id = response.getGroupId();
            logger.info("createInteractionRuleGroup " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createInteractionRuleGroup ClientRequestException " + e.getHttpStatusCode());
            logger.error("createInteractionRuleGroup ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("createInteractionRuleGroup ServerResponseException " + e.getHttpStatusCode());
            logger.error("createInteractionRuleGroup ServerResponseException " + e.getMessage());
        }
        return group_id;
    }

    /**
     * 更新智能直播间互动规则库
     */
    private static void updateInteractionRuleGroup(MetaStudioClient client, String group_id) {
        logger.info("updateInteractionRuleGroup start ");
        InteractionRuleGroup body = new InteractionRuleGroup().withGroupName("<your group name>");
        UpdateInteractionRuleGroupRequest request = new UpdateInteractionRuleGroupRequest()
                .withGroupId(group_id)
                .withBody(body);
        try {
            UpdateInteractionRuleGroupResponse response = client.updateInteractionRuleGroup(request);
            logger.info("updateInteractionRuleGroup " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("updateInteractionRuleGroup ClientRequestException " + e.getHttpStatusCode());
            logger.error("updateInteractionRuleGroup ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("updateInteractionRuleGroup ServerResponseException " + e.getHttpStatusCode());
            logger.error("updateInteractionRuleGroup ServerResponseException " + e.getMessage());
        }
    }

    /**
     * 查询智能直播间互动规则库列表
     */
    private static void listInteractionRuleGroups(MetaStudioClient client) {
        logger.info("listInteractionRuleGroups start ");
        ListInteractionRuleGroupsRequest request = new ListInteractionRuleGroupsRequest()
                .withGroupName("<your group name>");
        try {
            ListInteractionRuleGroupsResponse response = client.listInteractionRuleGroups(request);
            logger.info("listInteractionRuleGroups" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listInteractionRuleGroups ClientRequestException " + e.getHttpStatusCode());
            logger.error("listInteractionRuleGroups ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("listInteractionRuleGroups ServerResponseException " + e.getHttpStatusCode());
            logger.error("listInteractionRuleGroups ServerResponseException " + e.getMessage());
        }
    }

    /**
     * 删除智能直播间互动规则库
     */
    private static void deleteInteractionRuleGroup(MetaStudioClient client, String group_id) {
        logger.info("deleteInteractionRuleGroup start ");
        DeleteInteractionRuleGroupRequest request = new DeleteInteractionRuleGroupRequest()
                .withGroupId(group_id);
        try {
            DeleteInteractionRuleGroupResponse response = client.deleteInteractionRuleGroup(request);
            logger.info("deleteInteractionRuleGroup " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("deleteInteractionRuleGroup ClientRequestException " + e.getHttpStatusCode());
            logger.error("deleteInteractionRuleGroup ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("deleteInteractionRuleGroup ServerResponseException " + e.getHttpStatusCode());
            logger.error("deleteInteractionRuleGroup ServerResponseException " + e.getMessage());
        }
    }

    /**
     * 查询数字人智能直播任务列表
     */
    private static void listSmartLiveJobs(MetaStudioClient client) {
        logger.info("listSmartLiveJobs start");
        ListSmartLiveJobsRequest request = new ListSmartLiveJobsRequest();
        try {
            ListSmartLiveJobsResponse response = client.listSmartLiveJobs(request);
            logger.info("listSmartLiveJobs" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listSmartLiveJobs ClientRequestException" + e.getHttpStatusCode());
            logger.error("listSmartLiveJobs ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("listSmartLiveJobs ServerResponseException" + e.getHttpStatusCode());
            logger.error("listSmartLiveJobs ServerResponseException" + e.getMessage());
        }
    }
}
