package com.huawei.hwclouds.metastudio.demo;

import com.huaweicloud.sdk.metastudio.v1.MetaStudioClient;
import com.huaweicloud.sdk.metastudio.v1.model.AssetFileInfo;
import com.huaweicloud.sdk.metastudio.v1.model.BackgroundConfigInfo;
import com.huaweicloud.sdk.metastudio.v1.model.ControlSmartLiveReq;
import com.huaweicloud.sdk.metastudio.v1.model.CreateSmartLiveRoomReq;
import com.huaweicloud.sdk.metastudio.v1.model.CreateSmartLiveRoomRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreateSmartLiveRoomResponse;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteSmartLiveRoomRequest;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteSmartLiveRoomResponse;
import com.huaweicloud.sdk.metastudio.v1.model.DigitalAssetInfo;
import com.huaweicloud.sdk.metastudio.v1.model.ExecuteSmartLiveCommandRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ExecuteSmartLiveCommandResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ImageLayerConfig;
import com.huaweicloud.sdk.metastudio.v1.model.LayerConfig;
import com.huaweicloud.sdk.metastudio.v1.model.LayerPositionConfig;
import com.huaweicloud.sdk.metastudio.v1.model.LayerSizeConfig;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsRequest.AssetSourceEnum;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ListSmartLiveRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListSmartLiveResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ListSmartLiveRoomsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListSmartLiveRoomsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.LiveEvent;
import com.huaweicloud.sdk.metastudio.v1.model.LiveEventReportRequest;
import com.huaweicloud.sdk.metastudio.v1.model.LiveEventReportResponse;
import com.huaweicloud.sdk.metastudio.v1.model.LiveShootScriptItem;
import com.huaweicloud.sdk.metastudio.v1.model.LiveVideoScriptInfo;
import com.huaweicloud.sdk.metastudio.v1.model.PlayPolicy;
import com.huaweicloud.sdk.metastudio.v1.model.PlayPolicy.PlayModeEnum;
import com.huaweicloud.sdk.metastudio.v1.model.PlayPolicy.RandomPlayModeEnum;
import com.huaweicloud.sdk.metastudio.v1.model.ReportLiveEventReq;
import com.huaweicloud.sdk.metastudio.v1.model.ShowSmartLiveRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ShowSmartLiveResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ShowSmartLiveRoomRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ShowSmartLiveRoomResponse;
import com.huaweicloud.sdk.metastudio.v1.model.SmartLiveRoomBaseInfo;
import com.huaweicloud.sdk.metastudio.v1.model.StartSmartLiveReq;
import com.huaweicloud.sdk.metastudio.v1.model.StartSmartLiveRequest;
import com.huaweicloud.sdk.metastudio.v1.model.StartSmartLiveResponse;
import com.huaweicloud.sdk.metastudio.v1.model.StopSmartLiveRequest;
import com.huaweicloud.sdk.metastudio.v1.model.StopSmartLiveResponse;
import com.huaweicloud.sdk.metastudio.v1.model.TextConfig;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateSmartLiveRoomRequest;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateSmartLiveRoomResponse;
import com.huaweicloud.sdk.metastudio.v1.model.VideoConfig;
import com.huaweicloud.sdk.metastudio.v1.model.VoiceConfig;
import com.huaweicloud.sdk.metastudio.v1.region.MetaStudioRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SmartLiveDemo {

    private static final Logger logger = LoggerFactory.getLogger(SmartLiveDemo.class);

    // 获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID和您的AK/SK。
    // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK、HUAWEICLOUD_SDK_SK。

    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    public static void main(String[] args) throws IOException {
        ICredential auth = new BasicCredentials()
                .withAk(AK)
                .withSk(SK);

        // 使用默认配置
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        // 创建MetaStudio实例
        MetaStudioClient mClient = MetaStudioClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(MetaStudioRegion.CN_NORTH_4)
                .build();

        // 1,创建智能直播间
        String roomId = createSmartLiveRoom(mClient);

        // 2,查询智能直播间列表
        listSmartLiveRoom(mClient);

        // 3,查询智能直播剧本详情
        showSmartLiveRoom(mClient, roomId);

        // 4,更新智能直播间信息
        updateSmartLiveRoom(mClient, roomId);

        // 5,启动数字人智能直播任务
        String jobId = startSmartLive(mClient, roomId);

        // 6,查询数字人智能直播任务列表
        listSmartLive(mClient, jobId);

        // 7,查询数字人智能直播任务详情
        showSmartLive(mClient, roomId, jobId);

        // 8,控制数字人直播过程
        executeSmartLiveCommand(mClient, roomId, jobId);

        // 9,上报直播间事件
        liveEventReport(mClient, roomId, jobId);

        // 10,结束数字人智能直播任务
        stopSmartLive(mClient, roomId, jobId);

        // 11,删除智能直播间
        deleteSmartLiveRoom(mClient, roomId, jobId);


    }

    /**
     * 创建智能直播间
     *
     */
    public static String createSmartLiveRoom(MetaStudioClient client) {
        String roomId = null;
        try {
            CreateSmartLiveRoomReq req = buidlCreateSmartLiveRoomReq(client);
            CreateSmartLiveRoomRequest createSmartLiveRoomRequest = new CreateSmartLiveRoomRequest()
                .withBody(req);
            CreateSmartLiveRoomResponse response = client.createSmartLiveRoom(createSmartLiveRoomRequest);
            roomId = response.getRoomId();
            logger.info("createSmartLiveRoomId " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createSmartLiveRoom ClientRequestException " + e.getHttpStatusCode());
            logger.error("createSmartLiveRoom ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("createSmartLiveRoom ServerResponseException " + e.getHttpStatusCode());
            logger.error("createSmartLiveRoom ServerResponseException " + e.getMessage());
        }
        return roomId;
    }

    /**
     * 查询智能直播间列表
     *
     */
    private static String listSmartLiveRoom(MetaStudioClient client) {
        logger.info("listSmartLiveRoom start");
        ListSmartLiveRoomsRequest request = new ListSmartLiveRoomsRequest();
        String roomId = null;
        try {
            ListSmartLiveRoomsResponse response = client.listSmartLiveRooms(request);
            List<SmartLiveRoomBaseInfo> roomInfoList = response.getSmartLiveRooms();
            roomId = roomInfoList.get(0).getRoomId();
            logger.info("listSmartLiveRoom " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listSmartLiveRoom ClientRequestException " + e.getHttpStatusCode());
            logger.error("listSmartLiveRoom ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("listSmartLiveRoom ServerResponseException " + e.getHttpStatusCode());
            logger.error("listSmartLiveRoom ServerResponseException " + e.getMessage());
        }
        return roomId;
    }

    /**
     * 查询智能直播剧本详情
     *
     */
    private static void showSmartLiveRoom(MetaStudioClient client, String roomId) {
        logger.info("showSmartLiveRoom start ");
        ShowSmartLiveRoomRequest request = new ShowSmartLiveRoomRequest().withRoomId(roomId);
        try {
            ShowSmartLiveRoomResponse response = client.showSmartLiveRoom(request);
            logger.info("showSmartLiveRoom " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showSmartLiveRoom ClientRequestException " + e.getHttpStatusCode());
            logger.error("showSmartLiveRoom ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("showSmartLiveRoom ServerResponseException " + e.getHttpStatusCode());
            logger.error("showSmartLiveRoom ServerResponseException " + e.getMessage());
        }
    }

    /**
     * 更新智能直播间信息
     *
     */
    private static void updateSmartLiveRoom(MetaStudioClient client, String roomId) {
        logger.info("updateSmartLiveRoom start ");
        try {
            CreateSmartLiveRoomReq req = buidlCreateSmartLiveRoomReq(client);
            UpdateSmartLiveRoomRequest request = new UpdateSmartLiveRoomRequest()
                .withRoomId(roomId)
                .withBody(req);
            UpdateSmartLiveRoomResponse response = client.updateSmartLiveRoom(request);
            logger.info("updateSmartLiveRoom " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("updateSmartLiveRoom ClientRequestException " + e.getHttpStatusCode());
            logger.error("updateSmartLiveRoom ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("updateSmartLiveRoom ServerResponseException " + e.getHttpStatusCode());
            logger.error("updateSmartLiveRoom ServerResponseException " + e.getMessage());
        }
    }

    private static CreateSmartLiveRoomReq buidlCreateSmartLiveRoomReq(MetaStudioClient client) {
        // 获取模型资产
        DigitalAssetInfo modelAssetInfo = getAsset(client, DigitalAssetInfo.AssetTypeEnum.HUMAN_MODEL_2D, AssetSourceEnum.SYSTEM, null);
        List<LayerConfig> layerConfigs = new ArrayList<>();
        layerConfigs.add(getLayerConfigHuman(modelAssetInfo));

        // 获取音色资产
        DigitalAssetInfo voiceAssetInfo = getAsset(client, DigitalAssetInfo.AssetTypeEnum.VOICE_MODEL, AssetSourceEnum.SYSTEM, null);
        VoiceConfig voiceConfig = new VoiceConfig().withVoiceAssetId(voiceAssetInfo.getAssetId());

        // 获取图片资产
        DigitalAssetInfo imgAssetInfo = getAsset(client, DigitalAssetInfo.AssetTypeEnum.IMAGE, AssetSourceEnum.SYSTEM, "BACKGROUND_IMG:Yes");
        List<BackgroundConfigInfo> backgroundConfigInfos = new ArrayList<>();
        backgroundConfigInfos.add(getBackgroundConfig(imgAssetInfo));

        // 获取拍摄脚本
        LiveShootScriptItem shootScriptItem = new LiveShootScriptItem()
                .withTitle("<your title>")
                .withTextConfig(new TextConfig().withText("<your text, quantity is at least 200>"));
        List<LiveShootScriptItem> liveShootScriptItems = new ArrayList<>();
        liveShootScriptItems.add(shootScriptItem);

        // 获取剧本播放策略
        PlayPolicy playPolicy = new PlayPolicy().withRepeatCount(1)
                .withAutoPlayScript(true)
                .withPlayMode(PlayModeEnum.TEXT)
                .withRandomPlayMode(RandomPlayModeEnum.NONE);

        // 获取视频输出配置
        VideoConfig videoConfig = new VideoConfig()
                .withBitrate(3000)
                .withCodec(VideoConfig.CodecEnum.H264)
                .withWidth(1080)
                .withHeight(1920);

        // 获取直播剧本
        LiveVideoScriptInfo liveVideoScriptInfo = new LiveVideoScriptInfo()
                .withScriptName("<your script name>")
                .withModelAssetId(modelAssetInfo.getAssetId())
                .withLayerConfig(layerConfigs)
                .withVoiceConfig(voiceConfig)
                .withBackgroundConfig(backgroundConfigInfos)
                .withShootScripts(liveShootScriptItems);
        List<LiveVideoScriptInfo> liveVideoScriptInfos = new ArrayList<>();
        liveVideoScriptInfos.add(liveVideoScriptInfo);

        return new CreateSmartLiveRoomReq()
                .withPlayPolicy(playPolicy)
                .withSceneScripts(liveVideoScriptInfos)
                .withVideoConfig(videoConfig)
                .withRoomName("<your room name>"); // 不能重名
    }

    /**
     * 删除智能直播间
     *
     */
    private static void deleteSmartLiveRoom(MetaStudioClient client, String roomId, String jobId) {
        // 直播完成后才能删除直播房间，这里轮询直播间状态
        for (int i = 0; i < 12; i++) {
            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                logger.error("查询直播状态等待异常");
            }
            ShowSmartLiveResponse response = showSmartLive(client, roomId, jobId);
            ShowSmartLiveResponse.StateEnum state = response.getState();
            if (state != ShowSmartLiveResponse.StateEnum.PROCESSING
                && state != ShowSmartLiveResponse.StateEnum.WAITING) {
                break;
            }
        }
        logger.info("deleteSmartLiveRoom start ");
        DeleteSmartLiveRoomRequest request = new DeleteSmartLiveRoomRequest().withRoomId(roomId);
        try {
            DeleteSmartLiveRoomResponse response = client.deleteSmartLiveRoom(request);
            logger.info("deleteSmartLiveRoom " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("deleteSmartLiveRoom ClientRequestException " + e.getHttpStatusCode());
            logger.error("deleteSmartLiveRoom ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("deleteSmartLiveRoom ServerResponseException " + e.getHttpStatusCode());
            logger.error("deleteSmartLiveRoom ServerResponseException " + e.getMessage());
        }
    }

    /**
     * 启动数字人智能直播任务
     *
     */
    private static String startSmartLive(MetaStudioClient client, String roomId) {
        logger.info("startSmartLive start ");
        String jobId = null;
        VideoConfig videoConfig = new VideoConfig()
            .withBitrate(3000)
            .withCodec(VideoConfig.CodecEnum.H264)
            .withWidth(547)
            .withHeight(976);
        StartSmartLiveRequest request = new StartSmartLiveRequest()
            .withRoomId(roomId)
            .withBody(new StartSmartLiveReq().withVideoConfig(videoConfig));
        try {
            StartSmartLiveResponse response = client.startSmartLive(request);
            logger.info("startSmartLive " + response.toString());
            jobId = response.getJobId();
        } catch (ClientRequestException e) {
            logger.error("startSmartLive ClientRequestException " + e.getHttpStatusCode());
            logger.error("startSmartLive ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("startSmartLive ServerResponseException " + e.getHttpStatusCode());
            logger.error("startSmartLive ServerResponseException " + e.getMessage());
        }
        return jobId;
    }

    /**
     * 查询数字人智能直播任务列表
     *
     */
    private static void listSmartLive(MetaStudioClient client, String roomId) {
        logger.info("listSmartLive start ");
        ListSmartLiveRequest request = new ListSmartLiveRequest()
            .withRoomId(roomId)
            .withLimit(10)
            .withOffset(0);
        try {
            ListSmartLiveResponse response = client.listSmartLive(request);
            logger.info("listSmartLive " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listSmartLive ClientRequestException " + e.getHttpStatusCode());
            logger.error("listSmartLive ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("listSmartLive ServerResponseException " + e.getHttpStatusCode());
            logger.error("listSmartLive ServerResponseException " + e.getMessage());
        }
    }

    /**
     * 查询数字人智能直播任务详情
     *
     */
    private static ShowSmartLiveResponse showSmartLive(MetaStudioClient client, String roomId, String jobId) {
        logger.info("showSmartLive start ");
        ShowSmartLiveResponse response = null;
        ShowSmartLiveRequest request = new ShowSmartLiveRequest().withRoomId(roomId).withJobId(jobId);
        try {
            response = client.showSmartLive(request);
            logger.info("showSmartLive " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showSmartLive ClientRequestException " + e.getHttpStatusCode());
            logger.error("showSmartLive ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("showSmartLive ServerResponseException " + e.getHttpStatusCode());
            logger.error("showSmartLive ServerResponseException " + e.getMessage());
        }
        return response;
    }

    /**
     * 控制数字人直播过程
     *
     */
    private static void executeSmartLiveCommand(MetaStudioClient client, String roomId, String jobId) {
        logger.info("executeSmartLiveCommand start ");
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("play_type", "PLAY_NOW");
        Map<String, String> textMap = new HashMap<>();
        textMap.put("text", "<your text>");
        paramMap.put("text_config", textMap);
        List<Map<String, Object>> param = new ArrayList<>();
        param.add(paramMap);

        ExecuteSmartLiveCommandRequest request = new ExecuteSmartLiveCommandRequest()
            .withRoomId(roomId)
            .withJobId(jobId)
            .withBody(new ControlSmartLiveReq()
                    .withCommand(ControlSmartLiveReq.CommandEnum.INSERT_PLAY_SCRIPT)
                    .withParams(param));
        try {
            ExecuteSmartLiveCommandResponse response = client.executeSmartLiveCommand(request);
            logger.info("executeSmartLiveCommand " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("executeSmartLiveCommand ClientRequestException " + e.getHttpStatusCode());
            logger.error("executeSmartLiveCommand ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("executeSmartLiveCommand ServerResponseException " + e.getHttpStatusCode());
            logger.error("executeSmartLiveCommand ServerResponseException " + e.getMessage());
        }
    }

    /**
     * 上报直播间事件
     *
     */
    private static void liveEventReport(MetaStudioClient client, String roomId, String jobId) {
        // 进行中的直播状态为PROCESSING才能上报事件
        for (int i = 0; i < 12; i++) {
            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                logger.error("查询直播状态等待异常");
            }
            ShowSmartLiveResponse response = showSmartLive(client, roomId, jobId);
            ShowSmartLiveResponse.StateEnum state = response.getState();
            if (state == ShowSmartLiveResponse.StateEnum.PROCESSING) {
                break;
            }
        }

        logger.info("liveEventReport start ");
        LiveEvent liveEvent = new LiveEvent()
            .withType(99)
            .withTimestamp(new Timestamp(System.currentTimeMillis()).getTime())
            .withContent("<your report event >");
        List<LiveEvent> liveEvents = new ArrayList<>();
        liveEvents.add(liveEvent);
        LiveEventReportRequest request = new LiveEventReportRequest()
            .withRoomId(roomId)
            .withJobId(jobId)
            .withBody(new ReportLiveEventReq()
                .withTotal(1)
                .withEvents(liveEvents));
        try {
            LiveEventReportResponse response = client.liveEventReport(request);
            logger.info("executeSmartLiveCommand " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("executeSmartLiveCommand ClientRequestException " + e.getHttpStatusCode());
            logger.error("executeSmartLiveCommand ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("executeSmartLiveCommand ServerResponseException " + e.getHttpStatusCode());
            logger.error("executeSmartLiveCommand ServerResponseException " + e.getMessage());
        }
    }

    /**
     * 结束数字人智能直播任务
     *
     */
    private static void stopSmartLive(MetaStudioClient client, String roomId, String jobId) {
        logger.info("stopSmartLive start ");
        StopSmartLiveRequest request = new StopSmartLiveRequest().withRoomId(roomId).withJobId(jobId);
        try {
            StopSmartLiveResponse response = client.stopSmartLive(request);
            logger.info("stopSmartLive " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("stopSmartLive ClientRequestException " + e.getHttpStatusCode());
            logger.error("stopSmartLive ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("stopSmartLive ServerResponseException " + e.getHttpStatusCode());
            logger.error("stopSmartLive ServerResponseException " + e.getMessage());
        }
    }


    // 获取模型资产信息
    private static LayerConfig getLayerConfigHuman(DigitalAssetInfo modelAssetInfo) {
        AssetFileInfo modelInfo = null;
        for (AssetFileInfo assetFileInfo : modelAssetInfo.getFiles()) {
            if (assetFileInfo.getAssetFileCategory().equals("COVER")) {
                modelInfo = assetFileInfo;
            }
        }

        LayerPositionConfig layerPositionConfig = new LayerPositionConfig()
                .withDx(0)
                .withDy(0)
                .withLayerIndex(1);
        LayerConfig layerConfigHuman = new LayerConfig() // 为人物模型配置图层
                .withLayerType(LayerConfig.LayerTypeEnum.HUMAN)
                .withGroupId(modelAssetInfo.getAssetId() + "-0") // 配置层数
                .withPosition(layerPositionConfig)
                .withSize(new LayerSizeConfig().withHeight(1920).withWidth(1080))
                .withImageConfig(new ImageLayerConfig().withImageUrl(modelInfo.getDownloadUrl()));
        return layerConfigHuman;
    }

    // 从资产中获取背景图信息
    private static BackgroundConfigInfo getBackgroundConfig(DigitalAssetInfo imgAssetInfo) {
        AssetFileInfo assetFileInfo = null; // 获取图片文件
        for (AssetFileInfo assetFileInfoIt : imgAssetInfo.getFiles()) {
            if (assetFileInfoIt.getAssetFileCategory().equals("MAIN")) {
                assetFileInfo = assetFileInfoIt;
                break;
            }
        }

        if(assetFileInfo == null) {
            logger.error("getAssetFileInfo ClientRequestException");
            return null;
        }

        BackgroundConfigInfo backgroundConfigInfo = new BackgroundConfigInfo()
                .withBackgroundTitle(assetFileInfo.getFileName())
                .withBackgroundType(BackgroundConfigInfo.BackgroundTypeEnum.IMAGE)
                .withBackgroundAssetId(imgAssetInfo.getAssetId())
                .withBackgroundConfig(assetFileInfo.getDownloadUrl())
                .withBackgroundCoverUrl(assetFileInfo.getDownloadUrl());
        return backgroundConfigInfo;
    }

    // 获取资产
    private static DigitalAssetInfo getAsset(MetaStudioClient client, DigitalAssetInfo.AssetTypeEnum assetType, ListAssetsRequest.AssetSourceEnum assetSource, String systemProperty) {
        DigitalAssetInfo digitalAssetInfo = null;
        try {
            ListAssetsRequest listAssetsRequest = new ListAssetsRequest()
                    .withSystemProperty(systemProperty) // 系统属性
                    .withAssetType(String.valueOf(assetType)) // 资产类型
                    .withAssetSource(assetSource) // 资产来源：系统资产
                    .withAssetState(String.valueOf(DigitalAssetInfo.AssetStateEnum.ACTIVED)); // 主文件上传成功，资产激活。
            logger.info("ListAssetsRequest:  " + listAssetsRequest.toString());
            ListAssetsResponse response = client.listAssets(listAssetsRequest);
            digitalAssetInfo = response.getAssets().get(0);
        } catch (ClientRequestException e) {
            logger.error("getAsset ClientRequestException " + e.getHttpStatusCode());
            logger.error("getAsset ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("getAsset ServerResponseException" + e.getHttpStatusCode());
            logger.error("getAsset ServerResponseException" + e.getMessage());
        }
        return digitalAssetInfo;
    }

}