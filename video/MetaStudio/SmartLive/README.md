

## 1、功能介绍

华为云提供了MetaStudio服务端SDK，您可以直接集成服务端SDK来调用MetaStudio的相关API，从而实现对MetaStudio的快速操作。

**您将学到什么？**

如何通过java版SDK来体验MetaStudio服务的数字人直播功能。

## 2、开发时序图
![image](assets/SmartLive.jpg)


## 3、前置条件
- 1、已[注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=40c99ba3cea14417a2428c756a626599&lang=zh-cn)华为帐号并开通华为云，已进行[实名认证](https://support.huaweicloud.com/usermanual-account/zh-cn_topic_0077914254.html)。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、已获取账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 4、已获取MetaStudio服务对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见[API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html)。 

## 4、SDK获取和安装
您可以通过Maven配置所依赖的[数字内容生产线SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/MetaStudio?lang=Java)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-metastudio</artifactId>
    <version>3.1.78</version>
</dependency>
```

## 5、接口参数说明
关于接口参数的详细说明可参见：

[a.创建智能直播间](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateSmartLiveRoom)

[b.查询智能直播间列表](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ListSmartLiveRooms)

[c.查询智能直播剧本详情](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowSmartLiveRoom)

[d.更新智能直播间信息](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=UpdateSmartLiveRoom)

[e.删除智能直播间](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=DeleteSmartLiveRoom)

[f.启动数字人智能直播任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=StartSmartLive)

[g.查询数字人智能直播任务列表](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ListSmartLive)

[h.查询数字人智能直播任务详情](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowSmartLive)

[i.控制数字人直播过程](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ExecuteSmartLiveCommand)

[j.上报直播间事件](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=LiveEventReport)

[k.结束数字人智能直播任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=StopSmartLive)

[l.创建智能直播间互动规则库](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateInteractionRuleGroup)

[m.查询智能直播间互动规则库列表](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ListInteractionRuleGroups)

[n.更新智能直播间互动规则库](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=UpdateInteractionRuleGroup)

[o.删除智能直播间互动规则库](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=DeleteInteractionRuleGroup)

[p.查询数字人智能直播任务列表](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ListSmartLiveJobs)

## 6、关键代码片段

### 6.1、创建智能直播间
```java
/**
 * 创建智能直播间
 *
 */
public static String createSmartLiveRoom(MetaStudioClient client) {
    String roomId = null;
    try {
        CreateSmartLiveRoomReq req = buidlCreateSmartLiveRoomReq(client);
        CreateSmartLiveRoomRequest createSmartLiveRoomRequest = new CreateSmartLiveRoomRequest()
            .withBody(req);
        CreateSmartLiveRoomResponse response = client.createSmartLiveRoom(createSmartLiveRoomRequest);
        roomId = response.getRoomId();
        logger.info("createSmartLiveRoomId" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("createSmartLiveRoom ClientRequestException" + e.getHttpStatusCode());
        logger.error("createSmartLiveRoom ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("createSmartLiveRoom ServerResponseException" + e.getHttpStatusCode());
        logger.error("createSmartLiveRoom ServerResponseException" + e.getMessage());
    }
    return roomId;
}
```

### 6.2、查询智能直播间列表
```java
/**
 * 查询智能直播间列表
 *
 */
private static String listSmartLiveRoom(MetaStudioClient client) {
    logger.info("listSmartLiveRoom start***");
    ListSmartLiveRoomsRequest request = new ListSmartLiveRoomsRequest();
    String roomId = null;
    try {
        ListSmartLiveRoomsResponse response = client.listSmartLiveRooms(request);
        List<SmartLiveRoomBaseInfo> roomInfoList = response.getSmartLiveRooms();
        roomId = roomInfoList.get(0).getRoomId();
        logger.info("listSmartLiveRoom" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("listSmartLiveRoom ClientRequestException" + e.getHttpStatusCode());
        logger.error("listSmartLiveRoom ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("listSmartLiveRoom ServerResponseException" + e.getHttpStatusCode());
        logger.error("listSmartLiveRoom ServerResponseException" + e.getMessage());
    }
    return roomId;
}
```

### 6.3、查询智能直播剧本详情
```java
/**
 * 查询智能直播剧本详情
 *
 */
private static void showSmartLiveRoom(MetaStudioClient client, String roomId) {
    logger.info("showSmartLiveRoom start");
    ShowSmartLiveRoomRequest request = new ShowSmartLiveRoomRequest().withRoomId(roomId);
    try {
        ShowSmartLiveRoomResponse response = client.showSmartLiveRoom(request);
        logger.info("showSmartLiveRoom" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("showSmartLiveRoom ClientRequestException" + e.getHttpStatusCode());
        logger.error("showSmartLiveRoom ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("showSmartLiveRoom ServerResponseException" + e.getHttpStatusCode());
        logger.error("showSmartLiveRoom ServerResponseException" + e.getMessage());
    }
}
```
### 6.4、更新智能直播间信息
```java
/**
 * 更新智能直播间信息
 *
 */
private static void updateSmartLiveRoom(MetaStudioClient client, String roomId) {
    logger.info("updateSmartLiveRoom start");
    try {
        CreateSmartLiveRoomReq req = buidlCreateSmartLiveRoomReq(client);
        UpdateSmartLiveRoomRequest request = new UpdateSmartLiveRoomRequest()
        .withRoomId(roomId)
        .withBody(req);
        UpdateSmartLiveRoomResponse response = client.updateSmartLiveRoom(request);
        logger.info("updateSmartLiveRoom :" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("updateSmartLiveRoom ClientRequestException" + e.getHttpStatusCode());
        logger.error("updateSmartLiveRoom ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("updateSmartLiveRoom ServerResponseException" + e.getHttpStatusCode());
        logger.error("updateSmartLiveRoom ServerResponseException" + e.getMessage());
    }
}
```
### 6.5、删除智能直播间
```java
/**
 * 删除智能直播间
 *
 */
private static void deleteSmartLiveRoom(MetaStudioClient client, String roomId, String jobId) {
    // 直播完成后才能删除直播房间
    for (int i = 0; i < 12; i++) {
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            logger.error("查询直播状态等待异常");
        }
        ShowSmartLiveResponse response = showSmartLive(client, roomId, jobId);
        ShowSmartLiveResponse.StateEnum state = response.getState();
        if (state != ShowSmartLiveResponse.StateEnum.PROCESSING
        && state != ShowSmartLiveResponse.StateEnum.WAITING) {
        break;
        }
    }
    logger.info("deleteSmartLiveRoom start");
    DeleteSmartLiveRoomRequest request = new DeleteSmartLiveRoomRequest().withRoomId(roomId);
    try {
        DeleteSmartLiveRoomResponse response = client.deleteSmartLiveRoom(request);
        logger.info("deleteSmartLiveRoom" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("deleteSmartLiveRoom ClientRequestException" + e.getHttpStatusCode());
        logger.error("deleteSmartLiveRoom ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("deleteSmartLiveRoom ServerResponseException" + e.getHttpStatusCode());
        logger.error("deleteSmartLiveRoom ServerResponseException" + e.getMessage());
    }
    }
```

### 6.6、启动数字人智能直播任务
```java
/**
 * 启动数字人智能直播任务
 *
 */
private static String startSmartLive(MetaStudioClient client, String roomId) {
    logger.info("startSmartLive start");
    String jobId = null;
    VideoConfig videoConfig = new VideoConfig()
        .withBitrate(3000)
        .withCodec(VideoConfig.CodecEnum.H264)
        .withWidth(547)
        .withHeight(976);
    StartSmartLiveRequest request = new StartSmartLiveRequest()
        .withRoomId(roomId)
        .withBody(new StartSmartLiveReq().withVideoConfig(videoConfig));
    try {
        StartSmartLiveResponse response = client.startSmartLive(request);
        logger.info("startSmartLive" + response.toString());
        jobId = response.getJobId();
    } catch (ClientRequestException e) {
        logger.error("startSmartLive ClientRequestException" + e.getHttpStatusCode());
        logger.error("startSmartLive ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("startSmartLive ServerResponseException" + e.getHttpStatusCode());
        logger.error("startSmartLive ServerResponseException" + e.getMessage());
    }
    return jobId;
}
```

### 6.7、查询数字人智能直播任务列表
```java
/**
 * 查询数字人智能直播任务列表
 *
 */
private static void listSmartLive(MetaStudioClient client, String roomId) {
    logger.info("listSmartLive start");
    ListSmartLiveRequest request = new ListSmartLiveRequest()
        .withRoomId(roomId)
        .withLimit(10)
        .withOffset(0);
    try {
        ListSmartLiveResponse response = client.listSmartLive(request);
        logger.info("listSmartLive" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("listSmartLive ClientRequestException" + e.getHttpStatusCode());
        logger.error("listSmartLive ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("listSmartLive ServerResponseException" + e.getHttpStatusCode());
        logger.error("listSmartLive ServerResponseException" + e.getMessage());
    }
}
```

### 6.8、查询数字人智能直播任务详情
```java
/**
 * 查询数字人智能直播任务详情
 *
 */
private static ShowSmartLiveResponse showSmartLive(MetaStudioClient client, String roomId, String jobId) {
    logger.info("showSmartLive start");
    ShowSmartLiveResponse response = null;
    ShowSmartLiveRequest request = new ShowSmartLiveRequest().withRoomId(roomId).withJobId(jobId);
    try {
        response = client.showSmartLive(request);
        logger.info("showSmartLive" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("showSmartLive ClientRequestException" + e.getHttpStatusCode());
        logger.error("showSmartLive ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("showSmartLive ServerResponseException" + e.getHttpStatusCode());
        logger.error("showSmartLive ServerResponseException" + e.getMessage());
    }
    return response;
}
```

### 6.9、控制数字人直播过程
```java
/**
 * 控制数字人直播过程
 *
 */
private static void executeSmartLiveCommand(MetaStudioClient client, String roomId, String jobId) {
    logger.info("executeSmartLiveCommand start");
    Map<String, Object> paramMap = new HashMap<>();
    paramMap.put("script_type", "TEXT");
    paramMap.put("text_config", "互动");
    Map<String, String> textMap = new HashMap<>();
    textMap.put("text", "看到评论区有观众问问题，我来响应下。");
    paramMap.put("text_config", textMap);
    ExecuteSmartLiveCommandRequest request = new ExecuteSmartLiveCommandRequest()
        .withRoomId(roomId)
        .withJobId(jobId)
        .withBody(new ControlSmartLiveReq()
            .withCommand(ControlSmartLiveReq.CommandEnum.INSERT_PLAY_SCRIPT));
    try {
        ExecuteSmartLiveCommandResponse response = client.executeSmartLiveCommand(request);
        logger.info("executeSmartLiveCommand" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("executeSmartLiveCommand ClientRequestException" + e.getHttpStatusCode());
        logger.error("executeSmartLiveCommand ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("executeSmartLiveCommand ServerResponseException" + e.getHttpStatusCode());
        logger.error("executeSmartLiveCommand ServerResponseException" + e.getMessage());
    }
}
```

### 6.10、上报直播间事件
```java
/**
 * 上报直播间事件
 *
 */
private static void liveEventReport(MetaStudioClient client, String roomId, String jobId) {
    // 进行中的直播状态为PROCESSING才能上报事件
    for (int i = 0; i < 12; i++) {
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            logger.error("查询直播状态等待异常");
        }
        ShowSmartLiveResponse response = showSmartLive(client, roomId, jobId);
        ShowSmartLiveResponse.StateEnum state = response.getState();
        if (state == ShowSmartLiveResponse.StateEnum.PROCESSING) {
            break;
        }
    }

    logger.info("liveEventReport start");
    LiveEvent liveEvent = new LiveEvent()
        .withType(99)
        .withTimestamp(new Timestamp(System.currentTimeMillis()).getTime())
        .withContent("<your report event >");
    List<LiveEvent> liveEvents = new ArrayList<>();
    liveEvents.add(liveEvent);
    LiveEventReportRequest request = new LiveEventReportRequest()
        .withRoomId(roomId)
        .withJobId(jobId)
        .withBody(new ReportLiveEventReq()
            .withTotal(1)
            .withEvents(liveEvents));
    try {
        LiveEventReportResponse response = client.liveEventReport(request);
        logger.info("executeSmartLiveCommand" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("executeSmartLiveCommand ClientRequestException" + e.getHttpStatusCode());
        logger.error("executeSmartLiveCommand ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("executeSmartLiveCommand ServerResponseException" + e.getHttpStatusCode());
        logger.error("executeSmartLiveCommand ServerResponseException" + e.getMessage());
    }
}
```

### 6.11、结束数字人智能直播任务
```java
/**
 * 结束数字人智能直播任务
 *
 */
private static void stopSmartLive(MetaStudioClient client, String roomId, String jobId) {
    logger.info("stopSmartLive start");
    StopSmartLiveRequest request = new StopSmartLiveRequest().withRoomId(roomId).withJobId(jobId);
    try {
        StopSmartLiveResponse response = client.stopSmartLive(request);
        logger.info("stopSmartLive" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("stopSmartLive ClientRequestException" + e.getHttpStatusCode());
        logger.error("stopSmartLive ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("stopSmartLive ServerResponseException" + e.getHttpStatusCode());
        logger.error("stopSmartLive ServerResponseException" + e.getMessage());
    }
}
```

### 6.12、创建智能直播间互动规则库
```java
 /**
 * 创建智能直播间互动规则库
 *
 */
private static String createInteractionRuleGroup(MetaStudioClient client) {
    logger.info("createInteractionRuleGroup start");
    InteractionRuleGroup body = new InteractionRuleGroup().withGroupName("name1111111");
    CreateInteractionRuleGroupRequest request = new CreateInteractionRuleGroupRequest()
            .withBody(body);
    String group_id = null;
    try {
        CreateInteractionRuleGroupResponse response = client.createInteractionRuleGroup(request);
        group_id = response.getGroupId();
        logger.info("group_id");
        logger.info("createInteractionRuleGroup" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("createInteractionRuleGroup ClientRequestException" + e.getHttpStatusCode());
        logger.error("createInteractionRuleGroup ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("createInteractionRuleGroup ServerResponseException" + e.getHttpStatusCode());
        logger.error("createInteractionRuleGroup ServerResponseException" + e.getMessage());
    }
    return group_id;
}
```

### 6.13、更新智能直播间互动规则库
```java
/**
 * 更新智能直播间互动规则库
 *
 */
private static void updateInteractionRuleGroup(MetaStudioClient client, String group_id) {
    logger.info("updateInteractionRuleGroup start");
    InteractionRuleGroup body = new InteractionRuleGroup().withGroupName("name211111");
    UpdateInteractionRuleGroupRequest request = new UpdateInteractionRuleGroupRequest()
            .withGroupId(group_id)
            .withBody(body);
    try {
        UpdateInteractionRuleGroupResponse response = client.updateInteractionRuleGroup(request);
        logger.info("updateInteractionRuleGroup" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("updateInteractionRuleGroup ClientRequestException" + e.getHttpStatusCode());
        logger.error("updateInteractionRuleGroup ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("updateInteractionRuleGroup ServerResponseException" + e.getHttpStatusCode());
        logger.error("updateInteractionRuleGroup ServerResponseException" + e.getMessage());
    }
}
```

### 6.14、查询智能直播间互动规则库列表
```java
/**
 * 查询智能直播间互动规则库列表
 *
 */
private static void listInteractionRuleGroups(MetaStudioClient client) {
    logger.info("listInteractionRuleGroups start");
    ListInteractionRuleGroupsRequest request = new ListInteractionRuleGroupsRequest()
            .withGroupName("name3");
    try {
        ListInteractionRuleGroupsResponse response = client.listInteractionRuleGroups(request);
        logger.info("listInteractionRuleGroups" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("listInteractionRuleGroups ClientRequestException" + e.getHttpStatusCode());
        logger.error("listInteractionRuleGroups ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("listInteractionRuleGroups ServerResponseException" + e.getHttpStatusCode());
        logger.error("listInteractionRuleGroups ServerResponseException" + e.getMessage());
    }
}
```

### 6.15、删除智能直播间互动规则库
```java
/**
 * 删除智能直播间互动规则库
 *
 */
private static void deleteInteractionRuleGroup(MetaStudioClient client, String group_id) {
    logger.info("deleteInteractionRuleGroup start");
    DeleteInteractionRuleGroupRequest request = new DeleteInteractionRuleGroupRequest()
            .withGroupId(group_id);
    try {
        DeleteInteractionRuleGroupResponse response = client.deleteInteractionRuleGroup(request);
        logger.info("deleteInteractionRuleGroup" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("deleteInteractionRuleGroup ClientRequestException" + e.getHttpStatusCode());
        logger.error("deleteInteractionRuleGroup ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("deleteInteractionRuleGroup ServerResponseException" + e.getHttpStatusCode());
        logger.error("deleteInteractionRuleGroup ServerResponseException" + e.getMessage());
    }
}
```

### 6.16、查询数字人智能直播任务列表
```java
/**
 * 查询数字人智能直播任务列表
 *
 */
private static void listSmartLiveJobs(MetaStudioClient client) {
    logger.info("listSmartLiveJobs start");
    ListSmartLiveJobsRequest request = new ListSmartLiveJobsRequest();
    try {
        ListSmartLiveJobsResponse response = client.listSmartLiveJobs(request);
        logger.info("listSmartLiveJobs" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("listSmartLiveJobs ClientRequestException" + e.getHttpStatusCode());
        logger.error("listSmartLiveJobs ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("listSmartLiveJobs ServerResponseException" + e.getHttpStatusCode());
        logger.error("listSmartLiveJobs ServerResponseException" + e.getMessage());
    }
}
```



## 7、运行结果
**创建智能直播间**
```json
{
  "room_id":"3d4c76***",
  "X-Request-Id":"527e60***"
}
```

**查询智能直播间列表**
```json
{
  "X-Request-Id":"62a11f***",
  "count":3738,"smart_live_rooms":[
    {
      "room_id":"3d4c76***",
      "room_name":"name",
      "cover_url":"https://***",
      "update_time":"2024-01-26T08:46:58Z",
      "create_time":"2024-01-26T08:46:58Z",
      "error_info":{},
      "room_state":"ENABLE",
      "model_infos":[
        {
          "model_asset_id":"7927d1***",
          "asset_name":"test059229"
        }
      ],
      "room_type":"NORMAL"
    }
  ]
}
```

**查询智能直播剧本详情**
```json
{
	"room_id":"3d4c76***",
	"room_name":"name",
	"backup_model_asset_ids":[],
	"cover_url":"https://***",
    "update_time":"2024-01-26T08:46:58Z",
	"create_time":"2024-01-26T08:46:58Z",
	"shared_config":{
		
	},"X-Request-Id":"134b53***",
	"video_config":{
		"codec":"H264",
		"clip_mode":"RESIZE",
		"width":547,
        "bitrate":3000,
        "frame_rate":"25",
		"height":976,
        "is_subtitle_enable":false
	},"scene_scripts":[
		{
			"background_config":[
				{
					"background_config":"https://***",
                    "background_title":"20.jpg",
					"background_cover_url":"https://***",
                    "background_asset_id":"10f59f***",
					"background_type":"IMAGE"
				}
			],
			"model_asset_id":"7927d1***",
			"layer_config":[
				{
					"image_config":{
						"image_url":"https://***"
                    },"position":{
						"dx":0,
                        "dy":0,
                        "layer_index":1
					},"layer_type":"HUMAN"
				}
			],
			"shoot_scripts":[
				{
					"text_config":{
						"text":"b111"
					},"title":"a111"
				}
			],
			"voice_config":{
				"volume":140,
                "pitch":100,
                "speed":100,
                "voice_asset_id":"4eda76***"
			},"script_name":"c111"
		}
	],
	"room_type":"NORMAL"
}
```

**更新智能直播间信息**
```json
{
	"room_id":"226a3d***",
	"room_name":"name_updata",
	"backup_model_asset_ids":[
		
	],
	"cover_url":"",
	"update_time":"2024-01-26T08:54:20Z",
	"create_time":"2024-01-26T08:54:18Z",
	"shared_config":{
		
	},"X-Request-Id":"fe3177***",
	"video_config":{
		"codec":"H264",
		"clip_mode":"RESIZE",
		"width":547,"bitrate":3000,"frame_rate":"25",
		"height":976,"is_subtitle_enable":false
	},"scene_scripts":[
		{
			"background_config":[
				{
					"background_config":"https://***",
                    "background_title":"20.jpg",
					"background_cover_url":"https://***",
                    "background_asset_id":"10f59f***",
					"background_type":"IMAGE"
				}
			],
			"model_asset_id":"7927d1***",
			"layer_config":[
				{
					"image_config":{
						"image_url":"https://***"
                    },"position":{
						"dx":0,
                        "dy":0,
                        "layer_index":1
					},"layer_type":"HUMAN"
				}
			],
			"shoot_scripts":[
				{
					"text_config":{
						"text":"b111"
					},"title":"a111"
				}
			],
			"voice_config":{
				"volume":140,
                "pitch":100,
                "speed":100,
                "voice_asset_id":"4eda76***"
			},"script_name":"c111"
		}
	],
	"room_type":"NORMAL"
}
```

**启动数字人智能直播任务**
```json
{
	"rtc_room_info":{
		"room_id":"2ccf44***",
		"app_id":"c312ab***",
		"users":[
			{
				"user_type":"PLAYER",
				"signature":"d21558***",
				"user_id":"2ccf44***",
				"ctime":1706266460
			}
		]
	},"X-Request-Id":"f51058****",
	"job_id":"2ccf44***",
	"live_event_report_url":"https://***"
}
```

**查询数字人智能直播任务列表**
```json
{
  "smart_live_jobs":[],
  "X-Request-Id":"65ec22***",
  "count":0
}
```

**查询数字人智能直播任务详情**
```json
{
	"duration":0.0,"rtc_room_info":{
		"room_id":"2ccf44***",
		"app_id":"c312ab***",
		"users":[
			{
				"user_type":"PLAYER",
				"signature":"d21558***",
				"user_id":"2ccf44***",
				"ctime":1706266460
			}
		]
	},"create_time":"2024-01-26T08:54:20Z",
	"X-Request-Id":"6bae32***",
	"job_id":"2ccf44***",
	"stream_duration":0.0,"live_event_report_url":"https://***",
	"lastupdate_time":"2024-01-26T08:54:20Z",
	"state":"WAITING"
}
```

**控制数字人直播过程**
```json
{
  "X-Request-Id":"e0bac6***",
  "command":"INSERT_PLAY_SCRIPT"
}
```

**上报直播间事件**
```json
{
  "X-Request-Id":"416bf6***"
}
```
**结束数字人智能直播任务**
```json
{
	"X-Request-Id":"11917f***"
}
```

**删除智能直播间**
```json
{
	"X-Request-Id":"d12444***"
}
```

**创建智能直播间互动规则库**
```json
{
	"X-Request-Id":"d1e389***",
	"group_id":"3b35ee***"
}
```

**更新智能直播间互动规则库**
```json
{
	"update_time":"2024-01-26T08:55:41Z",
	"create_time":"2024-01-26T08:55:40Z",
	"X-Request-Id":"38f715***",
	"group_id":"3b35ee***",
	"group_name":"name211111",
	"interaction_rules":[]
}
```

**查询智能直播间互动规则库列表**
```json
{
	"X-Request-Id":"12286d***",
	"count":0,
    "interaction_rule_groups":[]
}
```

**删除智能直播间互动规则库**
```json
{
	"X-Request-Id":"3b1daf***"
}
```

**查询数字人智能直播任务列表**
```json
{
  "count": 7628,
  "smartLiveJobs": [
    {
      "jobId": "9ec760***",
      "roomId": "9fae57***",
      "roomName": "未命名1711939621742",
      "state": "SUCCEED",
      "duration": 799.0,
      "startTime": "2024-04-01T07:06:08Z",
      "endTime": "2024-04-01T07:19:27Z",
      "errorInfo": null,
      "createTime": "2024-04-01T07:06:07Z",
      "lastupdateTime": "2024-04-01T07:19:27Z",
      "rtcRoomInfo": null,
      "liveEventReportUrl": "https://***",
      "liveEventCallbackConfig": {
        "liveEventTypeCallbackUrl": null,
        "authType": "NONE",
        "key": null,
        "callbackEventType": [
          "SHOOT_SCRIPT_SWITCH"
        ]
      },
      "streamDuration": 772.0,
      "blockReason": null,
      "coverUrl": "https://***",
      "coStreamerConfig": {
        "voiceConfig": {
          "voiceAssetId": "80f3ae***",
          "speed": 100,
          "pitch": 100,
          "volume": 140
        },
        "streamerAction": "SILENCE"
      },
      "liveJobLog": {
        "interactionRecordsUrl": "https://***"
      }
    }
  ]
}
```

## 8、参考
本示例的代码工程仅用于简单演示，实际开发过程中应严格遵循开发指南。访问以下链接可以获取详细信息：[开发指南](https://support.huaweicloud.com/ssdk-metastudio/metastudio_06_0000.html)