package com.huawei.hwclouds.metastudio.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.metastudio.v1.MetaStudioClient;
import com.huaweicloud.sdk.metastudio.v1.model.Create2dModelTrainingJobReq;
import com.huaweicloud.sdk.metastudio.v1.model.Create2dModelTrainingJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.Create2dModelTrainingJobResponse;
import com.huaweicloud.sdk.metastudio.v1.model.Delete2dModelTrainingJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.Delete2dModelTrainingJobResponse;
import com.huaweicloud.sdk.metastudio.v1.model.Execute2dModelTrainingCommandByUserReq;
import com.huaweicloud.sdk.metastudio.v1.model.Execute2dModelTrainingCommandByUserReq.CommandEnum;
import com.huaweicloud.sdk.metastudio.v1.model.Execute2dModelTrainingCommandByUserRequest;
import com.huaweicloud.sdk.metastudio.v1.model.Execute2dModelTrainingCommandByUserResponse;
import com.huaweicloud.sdk.metastudio.v1.model.List2dModelTrainingJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.List2dModelTrainingJobResponse;
import com.huaweicloud.sdk.metastudio.v1.model.Show2dModelTrainingJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.Show2dModelTrainingJobResponse;
import com.huaweicloud.sdk.metastudio.v1.model.Update2dModelTrainingJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.Update2dModelTrainingJobResponse;
import com.huaweicloud.sdk.metastudio.v1.region.MetaStudioRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TraningJobManage {

    private static final Logger logger = LoggerFactory.getLogger(TraningJobManage.class);

    // 获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID和您的AK/SK。
    // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK、HUAWEICLOUD_SDK_SK。
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");

    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    public static void main(String[] args) {
        ICredential auth = new BasicCredentials()
                .withAk(AK)
                .withSk(SK);

        // 使用默认配置
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        // 创建MetaStudio实例
        MetaStudioClient mClient = MetaStudioClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(MetaStudioRegion.CN_NORTH_4)
                .build();


        // 1,查询分身数字人模型训练任务详情
        list2dModelTrainingJob(mClient);

        // 2,创建分身数字人模型训练任务
        String jobId = create2dModelTrainingJob(mClient);

        // 3,查询分身数字人模型训练任务列表
        show2dModelTrainingJob(mClient, jobId);

        // 4,更新分身数字人模型训练任务
        update2dModelTrainingJob(mClient, jobId);

        // 5,租户执行分身数字人模型训练任务命令
        execute2dModelTrainingCommandByUser(mClient, jobId);

        // 6,删除分身数字人模型训练任务
        delete2dModelTrainingJob(mClient, jobId);

    }

    /**
     * 创建分身数字人模型训练任务
     */
    private static String create2dModelTrainingJob(MetaStudioClient client) {
        logger.info("create2dModelTrainingJob start");
        Create2dModelTrainingJobRequest request = new Create2dModelTrainingJobRequest();
        request.withBody(new Create2dModelTrainingJobReq().withName("<your name>"));
        String jobId = null;
        try {
            Create2dModelTrainingJobResponse response = client.create2dModelTrainingJob(request);
            jobId = response.getJobId();
            logger.info("create2dModelTrainingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("create2dModelTrainingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("create2dModelTrainingJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("create2dModelTrainingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("create2dModelTrainingJob ServerResponseException" + e.getMessage());
        }
        return jobId;
    }


    /**
     * 查询分身数字人模型训练任务列表
     */
    private static void list2dModelTrainingJob(MetaStudioClient client) {
        logger.info("list2dModelTrainingJob start");
        List2dModelTrainingJobRequest request = new List2dModelTrainingJobRequest();
        try {
            List2dModelTrainingJobResponse response = client.list2dModelTrainingJob(request);
            logger.info("list2dModelTrainingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("list2dModelTrainingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("list2dModelTrainingJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("list2dModelTrainingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("list2dModelTrainingJob ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 查询分身数字人模型训练任务详情
     */
    private static void show2dModelTrainingJob(MetaStudioClient client, String jobId) {
        logger.info("show2dModelTrainingJob start");
        Show2dModelTrainingJobRequest request = new Show2dModelTrainingJobRequest();
        request.withJobId(jobId);
        try {
            Show2dModelTrainingJobResponse response = client.show2dModelTrainingJob(request);
            logger.info("show2dModelTrainingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("show2dModelTrainingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("show2dModelTrainingJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("show2dModelTrainingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("show2dModelTrainingJob ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 删除分身数字人模型训练任务
     */
    private static void delete2dModelTrainingJob(MetaStudioClient client, String jobId) {
        logger.info("delete2dModelTrainingJob start");
        Delete2dModelTrainingJobRequest request = new Delete2dModelTrainingJobRequest();
        request.withJobId(jobId);
        try {
            Delete2dModelTrainingJobResponse response = client.delete2dModelTrainingJob(request);
            logger.info("delete2dModelTrainingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("delete2dModelTrainingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("delete2dModelTrainingJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("delete2dModelTrainingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("delete2dModelTrainingJob ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 更新分身数字人模型训练任务
     */
    private static void update2dModelTrainingJob(MetaStudioClient client, String jobId) {
        logger.info("update2dModelTrainingJob start");
        Update2dModelTrainingJobRequest request = new Update2dModelTrainingJobRequest();
        request.withJobId(jobId);
        request.withBody(new Create2dModelTrainingJobReq().withName("<your new name>"));
        try {
            Update2dModelTrainingJobResponse response = client.update2dModelTrainingJob(request);
            logger.info("update2dModelTrainingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("update2dModelTrainingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("update2dModelTrainingJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("update2dModelTrainingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("update2dModelTrainingJob ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 租户执行分身数字人模型训练任务命令
     */
    private static void execute2dModelTrainingCommandByUser(MetaStudioClient client, String jobId) {
        logger.info("execute2dModelTrainingCommandByUser start");
        Execute2dModelTrainingCommandByUserRequest request = new Execute2dModelTrainingCommandByUserRequest();
        request.withJobId(jobId);
        request.withBody(new Execute2dModelTrainingCommandByUserReq().withCommand(CommandEnum.GET_MULTIPART_UPLOADED));
        try {
            Execute2dModelTrainingCommandByUserResponse response = client.execute2dModelTrainingCommandByUser(request);
            logger.info("execute2dModelTrainingCommandByUser" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("execute2dModelTrainingCommandByUser ClientRequestException" + e.getHttpStatusCode());
            logger.error("execute2dModelTrainingCommandByUser ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("execute2dModelTrainingCommandByUser ServerResponseException" + e.getHttpStatusCode());
            logger.error("execute2dModelTrainingCommandByUser ServerResponseException" + e.getMessage());
        }
    }

}