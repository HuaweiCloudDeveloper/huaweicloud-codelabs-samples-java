## 1、功能介绍

华为云提供了MetaStudio服务端SDK，您可以直接集成服务端SDK来调用MetaStudio的相关API，从而实现对MetaStudio的快速操作。

**您将学到什么？**

如何通过java版SDK来体验MetaStudio服务的数字人分身形象制作管理。

## 2、开发时序图
![image](assets/TraningJobManage.jpg)

## 3、前置条件
- 1、已[注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=40c99ba3cea14417a2428c756a626599&lang=zh-cn)华为帐号并开通华为云，已进行[实名认证](https://support.huaweicloud.com/usermanual-account/zh-cn_topic_0077914254.html)。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、已获取账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 4、已获取MetaStudio服务对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见[API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html)。

## 4、SDK获取和安装
您可以通过Maven配置所依赖的[数字内容生产线SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/MetaStudio?lang=Java)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-metastudio</artifactId>
    <version>3.1.78</version>
</dependency>
```

## 5、接口参数说明
关于接口参数的详细说明可参见：
[a.创建分身数字人模型训练任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=Create2dModelTrainingJob)

[b.查询分身数字人模型训练任务列表](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=List2dModelTrainingJob)

[c.查询分身数字人模型训练任务详情](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=Show2dModelTrainingJob)

[d.删除分身数字人模型训练任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=Delete2dModelTrainingJob)

[e.更新分身数字人模型训练任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=Update2dModelTrainingJob)

[f.租户执行分身数字人模型训练任务命令](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=Execute2dModelTrainingCommandByUser)

## 6、关键代码片段

### 6.1、创建分身数字人模型训练任务
```java
    /**
     * 创建分身数字人模型训练任务
     */
    private static String create2dModelTrainingJob(MetaStudioClient client) {
        logger.info("create2dModelTrainingJob start");
        Create2dModelTrainingJobRequest request = new Create2dModelTrainingJobRequest();
        request.withBody(new Create2dModelTrainingJobReq().withName("<your name>"));
        String jobId = null;
        try {
            Create2dModelTrainingJobResponse response = client.create2dModelTrainingJob(request);
            jobId = response.getJobId();
            logger.info("create2dModelTrainingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("create2dModelTrainingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("create2dModelTrainingJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("create2dModelTrainingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("create2dModelTrainingJob ServerResponseException" + e.getMessage());
        }
        return jobId;
    }
```

### 6.2、查询分身数字人模型训练任务列表
```java
    /**
     * 查询分身数字人模型训练任务列表
     */
    private static void list2dModelTrainingJob(MetaStudioClient client) {
        logger.info("list2dModelTrainingJob start");
        List2dModelTrainingJobRequest request = new List2dModelTrainingJobRequest();
        try {
            List2dModelTrainingJobResponse response = client.list2dModelTrainingJob(request);
            logger.info("list2dModelTrainingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("list2dModelTrainingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("list2dModelTrainingJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("list2dModelTrainingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("list2dModelTrainingJob ServerResponseException" + e.getMessage());
        }
    }
```

### 6.3、查询分身数字人模型训练任务详情
```java
    /**
     * 查询分身数字人模型训练任务详情
     */
    private static void show2dModelTrainingJob(MetaStudioClient client, String jobId) {
        logger.info("show2dModelTrainingJob start");
        Show2dModelTrainingJobRequest request = new Show2dModelTrainingJobRequest();
        request.withJobId(jobId);
        try {
            Show2dModelTrainingJobResponse response = client.show2dModelTrainingJob(request);
            logger.info("show2dModelTrainingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("show2dModelTrainingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("show2dModelTrainingJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("show2dModelTrainingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("show2dModelTrainingJob ServerResponseException" + e.getMessage());
        }
    }
```

### 6.4、删除分身数字人模型训练任务
```java
    /**
     * 删除分身数字人模型训练任务
     */
    private static void delete2dModelTrainingJob(MetaStudioClient client, String jobId) {
        logger.info("delete2dModelTrainingJob start");
        Delete2dModelTrainingJobRequest request = new Delete2dModelTrainingJobRequest();
        request.withJobId(jobId);
        try {
            Delete2dModelTrainingJobResponse response = client.delete2dModelTrainingJob(request);
            logger.info("delete2dModelTrainingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("delete2dModelTrainingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("delete2dModelTrainingJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("delete2dModelTrainingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("delete2dModelTrainingJob ServerResponseException" + e.getMessage());
        }
    }
```

### 6.5、更新分身数字人模型训练任务
```java
    /**
     * 更新分身数字人模型训练任务
     */
    private static void update2dModelTrainingJob(MetaStudioClient client, String jobId) {
        logger.info("update2dModelTrainingJob start");
        Update2dModelTrainingJobRequest request = new Update2dModelTrainingJobRequest();
        request.withJobId(jobId);
        request.withBody(new Create2dModelTrainingJobReq().withName("<your new name>"));
        try {
            Update2dModelTrainingJobResponse response = client.update2dModelTrainingJob(request);
            logger.info("update2dModelTrainingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("update2dModelTrainingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("update2dModelTrainingJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("update2dModelTrainingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("update2dModelTrainingJob ServerResponseException" + e.getMessage());
        }
    }
```

### 6.6、租户执行分身数字人模型训练任务命令
```java
    /**
     * 租户执行分身数字人模型训练任务命令
     */
    private static void execute2dModelTrainingCommandByUser(MetaStudioClient client, String jobId) {
        logger.info("execute2dModelTrainingCommandByUser start");
        Execute2dModelTrainingCommandByUserRequest request = new Execute2dModelTrainingCommandByUserRequest();
        request.withJobId(jobId);
        request.withBody(new Execute2dModelTrainingCommandByUserReq().withCommand(CommandEnum.GET_MULTIPART_UPLOADED));
        try {
            Execute2dModelTrainingCommandByUserResponse response = client.execute2dModelTrainingCommandByUser(request);
            logger.info("execute2dModelTrainingCommandByUser" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("execute2dModelTrainingCommandByUser ClientRequestException" + e.getHttpStatusCode());
            logger.error("execute2dModelTrainingCommandByUser ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("execute2dModelTrainingCommandByUser ServerResponseException" + e.getHttpStatusCode());
            logger.error("execute2dModelTrainingCommandByUser ServerResponseException" + e.getMessage());
        }
    }
```

## 7、运行结果
**创建分身数字人模型训练任务**
```json
{
  "job_id": "e3d947***",
  "training_video_upload_url": ["https://***"],
    "cover_upload_url": "https://***",
    "id_card_image1_upload_url": "https://***",
    "id_card_image2_upload_url": "https://***",
    "grant_file_upload_url": "https://***"
    }
```

**查询分身数字人模型训练任务列表**
```json
{
  "job_id": "efc530***",
  "name": "***",
  "state": "WAIT_FILE_UPLOAD",
  "assetId": null,
  "project_id": "fe66c7***",
  "cover_download_url": null,
  "last-update-time": "2024-03-26T07:28:09Z",
  "create_time": "2024-03-26T07:28:09Z",
  "contact": null,
  "batch-name": null,
  "tags": null,
  "model_version": "V3.2",
  "matting_type": "AI",
  "training_video_download_url": null,
  "id_card_image1_download_url": null,
  "id_card_image2_download_url": null,
  "grant_file_download_url": null,
  "operation_logs": [
    {
      "operate_time": "2024-03-26T07:28:09Z",
      "log_type": "USER_CREATE_JOD",
      "log-description": null,
      "operate_user": "USER"
    }
  ],
  "comment_logs": [],
  "is_mask_file_uploaded": false,
  "mask-fileDownload-url": null,
  "verify-videoDownload-url": null,
  "markable-videoDownload-url": null,
  "inference-data-process-video-mark-info": null,
  "inference-data-process-action-mark-info": null,
  "is-background-replacement": true,
  "model-resolution": "1080P"
}
```

**查询分身数字人模型训练任务详情**
```json
{
  "count": 101,
  "jobs": [
    {
      "job_id": "646682***",
      "name": "***",
      "state": "INFERENCE_DATA_PREPROCESSING",
      "assetId": "7ec703***",
      "project_id": "fe66c7***",
      "cover_download_url": "https://***",
      "last_update_time": "2024-03-26T07:27:54Z",
      "create_time": "2024-03-25T13:27:16Z",
      "contact": "15088518279",
      "batchName": null,
      "tags": null,
      "model_version": "V3.2",
      "matting_type": "AI"
    }
  ]
}
```

**删除分身数字人模型训练任务**
```json
```

**更新分身数字人模型训练任务**
```json
{
  "job_id": "efc530***",
  "training_video_upload_url": null,
  "cover_upload_url": "https://***",
  "id_card_image1_upload_url": "https://***",
  "id_card_image2_upload_url": "https://***",
  "grant_file_upload_url": "https://***"
}
```

**租户执行分身数字人模型训练任务命令**
```json
{
  "commond_result": "EXCUTE_FAILED",
  "attachment_upload_url": null,
  "multipart_data": null
}
```

## 8、参考
本示例的代码工程仅用于简单演示，实际开发过程中应严格遵循开发指南。访问以下链接可以获取详细信息：[开发指南](https://support.huaweicloud.com/ssdk-metastudio/metastudio_06_0000.html)