

## 1、功能介绍

华为云提供了MetaStudio服务端SDK，您可以直接集成服务端SDK来调用MetaStudio的相关API，从而实现对MetaStudio的快速操作。 

**您将学到什么？**

如何通过java版SDK来体验MetaStudio服务的视频驱动。视频任务启动成功后，将返回RTC房间信息，通过集成RTC客户将摄像头视频传入房间后即可驱动数字人面部表情和肢体动作。

视频驱动可用于中之人驱动的数字人直播等场景。

## 2、开发时序图

![image](assets/DigitalHumanVideoMotion.jpg)


## 3、前置条件
- 1、已[注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=40c99ba3cea14417a2428c756a626599&lang=zh-cn)华为帐号并开通华为云，已进行[实名认证](https://support.huaweicloud.com/usermanual-account/zh-cn_topic_0077914254.html)。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、已获取账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 4、已获取MetaStudio服务对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见[API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html)。 

## 4、SDK获取和安装
您可以通过Maven配置所依赖的[数字内容生产线SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/MetaStudio?lang=Java)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-metastudio</artifactId>
    <version>3.1.78</version>
</dependency>

```

## 5、接口参数说明
关于接口参数的详细说明可参见：

[a.创建视频驱动任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateVideoMotionCaptureJob)

[b.查询视频驱动任务列表](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ListVideoMotionCaptureJobs)

[c.查询视频驱动任务详情](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowVideoMotionCaptureJob)

[d.停止视频驱动任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=StopVideoMotionCaptureJob)

[e.控制数字人驱动](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ExecuteVideoMotionCaptureCommand)


## 6、关键代码片段

### 6.1、创建视频驱动任务
```java
/**
 * 创建视频驱动任务
 *
 */
private static String createVideoMotionCaptureJob(MetaStudioClient client) {
    logger.info("createVideoMotionCaptureJob start");
    CreateVideoMotionCaptureJobRequest request = new CreateVideoMotionCaptureJobRequest()
        .withBody(new VideoMotionCaptureJobReq()
        .withMotionCaptureMode(VideoMotionCaptureJobReq.MotionCaptureModeEnum.FULL_BODY)
        .withOutputInfo(new OutputInfo()
        .withFaceAddr("<ip>:<port>")
        .withBodyAddr("<ip>:<port>")
        .withAudioAddr("<ip>:<port>")
        .withSessionId(<"your session_id">)));
    String job_id = "";
    try {
        CreateVideoMotionCaptureJobResponse response = client.createVideoMotionCaptureJob(request);
        logger.info("createVideoMotionCaptureJob" + response.toString());
        job_id = response.getJobId();
    } catch (ClientRequestException e) {
        logger.error("createVideoMotionCaptureJob ClientRequestException" + e.getHttpStatusCode());
        logger.error("createVideoMotionCaptureJob ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("createVideoMotionCaptureJob ServerResponseException" + e.getHttpStatusCode());
        logger.error("createVideoMotionCaptureJob ServerResponseException" + e.getMessage());
    }
    return job_id;
    }
```

### 6.2、查询视频驱动任务列表
```java
/**
 * 查询视频驱动任务列表
 *
 */
private static void listVideoMotionCaptureJobs(MetaStudioClient client) {
    logger.info("listVideoMotionCaptureJobs start");
    ListVideoMotionCaptureJobsRequest request = new ListVideoMotionCaptureJobsRequest()
            .withOffset(0)
            .withLimit(10);
    try {
        ListVideoMotionCaptureJobsResponse response = client.listVideoMotionCaptureJobs(request);
        logger.info("listVideoMotionCaptureJobs" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("listVideoMotionCaptureJobs ClientRequestException" + e.getHttpStatusCode());
        logger.error("listVideoMotionCaptureJobs ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("listVideoMotionCaptureJobs ServerResponseException" + e.getHttpStatusCode());
        logger.error("listVideoMotionCaptureJobs ServerResponseException" + e.getMessage());
    }
}
```

### 6.3、查询视频驱动任务详情
```java
/**
 * 查询视频驱动任务详情
 *
 */
private static ShowVideoMotionCaptureJobResponse.StateEnum showVideoMotionCaptureJob(MetaStudioClient client, String job_id) {
    logger.info("showVideoMotionCaptureJob start");
    ShowVideoMotionCaptureJobRequest request = new ShowVideoMotionCaptureJobRequest()
            .withJobId(job_id);
    ShowVideoMotionCaptureJobResponse.StateEnum state = null;
    try {
        ShowVideoMotionCaptureJobResponse response = client.showVideoMotionCaptureJob(request);
        logger.info("showVideoMotionCaptureJob" + response.toString());
        state = response.getState();
    } catch (ClientRequestException e) {
        logger.error("showVideoMotionCaptureJob ClientRequestException" + e.getHttpStatusCode());
        logger.error("showVideoMotionCaptureJob ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("showVideoMotionCaptureJob ServerResponseException" + e.getHttpStatusCode());
        logger.error("showVideoMotionCaptureJob ServerResponseException" + e.getMessage());
    }
    return state;
}
```

### 6.4、停止视频驱动任务
```java
/**
 * 停止视频驱动任务
 *
 */
private static void stopVideoMotionCaptureJob(MetaStudioClient client, String job_id) {
    logger.info("stopVideoMotionCaptureJob start");
    StopVideoMotionCaptureJobRequest request = new StopVideoMotionCaptureJobRequest()
            .withJobId(job_id);
    try {
        StopVideoMotionCaptureJobResponse response = client.stopVideoMotionCaptureJob(request);
        logger.info("stopVideoMotionCaptureJob" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("stopVideoMotionCaptureJob ClientRequestException" + e.getHttpStatusCode());
        logger.error("stopVideoMotionCaptureJob ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("stopVideoMotionCaptureJob ServerResponseException" + e.getHttpStatusCode());
        logger.error("stopVideoMotionCaptureJob ServerResponseException" + e.getMessage());
    }
}
```

### 6.5、控制数字人驱动
```java
/**
 * 控制数字人驱动
 *
 */
private static void executeVideoMotionCaptureCommand(MetaStudioClient client, String job_id) {
    logger.info("executeVideoMotionCaptureCommand start");
    ExecuteVideoMotionCaptureCommandRequest request = new ExecuteVideoMotionCaptureCommandRequest()
            .withJobId(job_id)
            .withBody(new ControlDigitalHumanLiveReq()
                    .withCommand(ControlDigitalHumanLiveReq.CommandEnum.BODY_POS_RESET));
    try {
        ExecuteVideoMotionCaptureCommandResponse response = client.executeVideoMotionCaptureCommand(request);
        logger.info("executeVideoMotionCaptureCommand" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("executeVideoMotionCaptureCommand ClientRequestException" + e.getHttpStatusCode());
        logger.error("executeVideoMotionCaptureCommand ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("executeVideoMotionCaptureCommand ServerResponseException" + e.getHttpStatusCode());
        logger.error("executeVideoMotionCaptureCommand ServerResponseException" + e.getMessage());
    }
}
```


## 7、运行结果
**创建视频驱动任务**
```json
{
	"rtc_room_info":{
		"room_id":"166b89***",
		"app_id":"c312ab***",
		"users":[
			{
				"user_type":"CAPTURE",
				"signature":"b4b44b***",
				"user_id":"166b89***",
				"ctime":1706332858
			},{
				"user_type":"ANIMATION",
				"signature":"e8eabb***",
				"user_id":"166b80***",
				"ctime":1706332858
			},{
				"user_type":"RENDER",
				"signature":"c6ac94***",
				"user_id":"166b89***",
				"ctime":1706332858
			},{
				"user_type":"PLAYER",
				"signature":"90c0b5***",
				"user_id":"166b89***",
				"ctime":1706332858
			}
		]
	},"X-Request-Id":"9c82c8***",
	"job_id":"166b89***"
}
```

**查询视频驱动任务列表**
```json
{
	"total":4343,
    "video_motion_capture_jobs":[
		{
			"input_info":{
				"rtc_room_info":{
					"room_id":"19ed6a***",
					"app_id":"c312ab***",
					"users":[
						{
							"user_type":"CAPTURE",
							"signature":"1b4d2f***",
							"user_id":"19ed6a***",
							"ctime":1706258968
						},{
							"user_type":"ANIMATION",
							"signature":"da21e1***",
							"user_id":"19ed6a***",
							"ctime":1706258968
						},{
							"user_type":"RENDER",
							"signature":"46fbee***",
							"user_id":"19ed6a***",
							"ctime":1706258968
						},{
							"user_type":"PLAYER",
							"signature":"3e7512***",
							"user_id":"19ed6a***",
							"ctime":1706258968
						}
					]
				}
			},
            "start_time":"2024-01-26T06:49:29Z",
			"job_id":"19ed6a***",
			"motion_capture_mode":"FULL_BODY",
			"output_info":{
				"face_addr":"<ip>:<port>",
				"body_addr":"<ip>:<port>",
				"session_id":"c312ab***",
                "audio_addr":"<ip>:<port>"
			},"state":"SUCCEED"
		}
	],
	"X-Request-Id":"276e1a***"
}
```

**查询视频驱动任务详情**
```json
{
	"input_info":{
		"rtc_room_info":{
			"room_id":"166b89***",
			"app_id":"c312ab***",
			"users":[
				{
					"user_type":"CAPTURE",
					"signature":"b4b44b***",
					"user_id":"166b89***",
					"ctime":1706332858
				},{
					"user_type":"ANIMATION",
					"signature":"e8eabb***",
					"user_id":"166b8a***",
					"ctime":1706332858
				},{
					"user_type":"RENDER",
					"signature":"c6ac94***",
					"user_id":"166b8a***",
					"ctime":1706332858
				},{
					"user_type":"PLAYER",
					"signature":"90c0b5***",
					"user_id":"166b89***",
					"ctime":1706332858
				}
			]
		}
	},"start_time":"2024-01-27T03:20:59Z",
	"X-Request-Id":"3da599***",
	"job_id":"166b89***",
	"motion_capture_mode":"FULL_BODY",
	"output_info":{
		"face_addr":"<ip>:<port>",
		"body_addr":"<ip>:<port>",
		"session_id":"919a11***",
        "audio_addr":"<ip>:<port>"
	},"state":"PROCESSING"
}
```

**停止视频驱动任务**
```json
{
	"X-Request-Id":"674140***"
}
```

**控制数字人驱动**
```json
{
	"X-Request-Id":"ee6f06***"
}
```

## 8、参考
本示例的代码工程仅用于简单演示，实际开发过程中应严格遵循开发指南。访问以下链接可以获取详细信息：[开发指南](https://support.huaweicloud.com/ssdk-metastudio/metastudio_06_0000.html)