package com.huawei.hwclouds.metastudio.demo;

import com.huaweicloud.sdk.metastudio.v1.region.MetaStudioRegion;
import java.util.concurrent.TimeUnit;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.metastudio.v1.MetaStudioClient;
import com.huaweicloud.sdk.metastudio.v1.model.CreateVideoMotionCaptureJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListVideoMotionCaptureJobsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListVideoMotionCaptureJobsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ShowVideoMotionCaptureJobResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ShowVideoMotionCaptureJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ExecuteVideoMotionCaptureCommandRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ControlDigitalHumanLiveReq;
import com.huaweicloud.sdk.metastudio.v1.model.StopVideoMotionCaptureJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ExecuteVideoMotionCaptureCommandResponse;
import com.huaweicloud.sdk.metastudio.v1.model.StopVideoMotionCaptureJobResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class DigitalHumanVideoMotion {

    private static final Logger logger = LoggerFactory.getLogger(DigitalHumanVideoMotion.class);

    // 获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID和您的AK/SK。
    // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK、HUAWEICLOUD_SDK_SK。
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");

    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    public static void main(String[] args) throws IOException, InterruptedException {
        ICredential auth = new BasicCredentials()
                .withAk(AK)
                .withSk(SK);
        // 使用默认配置
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        // 创建MetaStudio实例
        MetaStudioClient mClient = MetaStudioClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(MetaStudioRegion.CN_NORTH_4) // region为上海一需写为：withEndpoint("cn-east-3")
                .build();

        // 1. 查询视频驱动任务列表
        listVideoMotionCaptureJobs(mClient);
        // 2. 创建视频驱动任务
        String job_id = createVideoMotionCaptureJob(mClient);
        // 3. 查询视频驱动任务详情
        TimeUnit.SECONDS.sleep(5);
        ShowVideoMotionCaptureJobResponse.StateEnum state = showVideoMotionCaptureJob(mClient, job_id);
        if (state == ShowVideoMotionCaptureJobResponse.StateEnum.PROCESSING) {
            TimeUnit.SECONDS.sleep(3);
        }
        // 4. 控制数字人驱动
        executeVideoMotionCaptureCommand(mClient, job_id);
        TimeUnit.SECONDS.sleep(5);
        // 5. 停止视频驱动任务
        stopVideoMotionCaptureJob(mClient, job_id);
    }

    /**
     * 查询视频驱动任务列表
     *
     */
    private static void listVideoMotionCaptureJobs(MetaStudioClient client) {
        logger.info("listVideoMotionCaptureJobs start");
        ListVideoMotionCaptureJobsRequest request = new ListVideoMotionCaptureJobsRequest()
                .withOffset(0)
                .withLimit(10);
        try {
            ListVideoMotionCaptureJobsResponse response = client.listVideoMotionCaptureJobs(request);
            logger.info("listVideoMotionCaptureJobs" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listVideoMotionCaptureJobs ClientRequestException" + e.getHttpStatusCode());
            logger.error("listVideoMotionCaptureJobs ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("listVideoMotionCaptureJobs ServerResponseException" + e.getHttpStatusCode());
            logger.error("listVideoMotionCaptureJobs ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 创建视频驱动任务
     *
     */
    private static String createVideoMotionCaptureJob(MetaStudioClient client) {
        logger.info("createVideoMotionCaptureJob start");
        CreateVideoMotionCaptureJobRequest request = new CreateVideoMotionCaptureJobRequest()
                .withBody(new VideoMotionCaptureJobReq()
                        .withMotionCaptureMode(VideoMotionCaptureJobReq.MotionCaptureModeEnum.FULL_BODY)
                        .withOutputInfo(new OutputInfo()
                                .withFaceAddr("X.X.X.X:X")
                                .withBodyAddr("X.X.X.X:X")
                                .withAudioAddr("X.X.X.X:X")
                                .withSessionId(<your session_id>)));
        String job_id = "";
        try {
            CreateVideoMotionCaptureJobResponse response = client.createVideoMotionCaptureJob(request);
            logger.info("createVideoMotionCaptureJob" + response.toString());
            job_id = response.getJobId();
        } catch (ClientRequestException e) {
            logger.error("createVideoMotionCaptureJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("createVideoMotionCaptureJob ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("createVideoMotionCaptureJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("createVideoMotionCaptureJob ServerResponseException" + e.getMessage());
        }
        return job_id;
    }

    /**
     * 查询视频驱动任务详情
     *
     */
    private static ShowVideoMotionCaptureJobResponse.StateEnum showVideoMotionCaptureJob(MetaStudioClient client, String job_id) {
        logger.info("showVideoMotionCaptureJob start");
        ShowVideoMotionCaptureJobRequest request = new ShowVideoMotionCaptureJobRequest()
                .withJobId(job_id);
        ShowVideoMotionCaptureJobResponse.StateEnum state = null;
        try {
            ShowVideoMotionCaptureJobResponse response = client.showVideoMotionCaptureJob(request);
            logger.info("showVideoMotionCaptureJob" + response.toString());
            state = response.getState();
        } catch (ClientRequestException e) {
            logger.error("showVideoMotionCaptureJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("showVideoMotionCaptureJob ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("showVideoMotionCaptureJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("showVideoMotionCaptureJob ServerResponseException" + e.getMessage());
        }
        return state;
    }

    /**
     * 控制数字人驱动
     *
     */
    private static void executeVideoMotionCaptureCommand(MetaStudioClient client, String job_id) {
        logger.info("executeVideoMotionCaptureCommand start");
        ExecuteVideoMotionCaptureCommandRequest request = new ExecuteVideoMotionCaptureCommandRequest()
                .withJobId(job_id)
                .withBody(new ControlDigitalHumanLiveReq()
                        .withCommand(ControlDigitalHumanLiveReq.CommandEnum.BODY_POS_RESET));
        try {
            ExecuteVideoMotionCaptureCommandResponse response = client.executeVideoMotionCaptureCommand(request);
            logger.info("executeVideoMotionCaptureCommand" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("executeVideoMotionCaptureCommand ClientRequestException" + e.getHttpStatusCode());
            logger.error("executeVideoMotionCaptureCommand ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("executeVideoMotionCaptureCommand ServerResponseException" + e.getHttpStatusCode());
            logger.error("executeVideoMotionCaptureCommand ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 停止视频驱动任务
     *
     */
    private static void stopVideoMotionCaptureJob(MetaStudioClient client, String job_id) {
        logger.info("stopVideoMotionCaptureJob start");
        StopVideoMotionCaptureJobRequest request = new StopVideoMotionCaptureJobRequest()
                .withJobId(job_id);
        try {
            StopVideoMotionCaptureJobResponse response = client.stopVideoMotionCaptureJob(request);
            logger.info("stopVideoMotionCaptureJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("stopVideoMotionCaptureJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("stopVideoMotionCaptureJob ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("stopVideoMotionCaptureJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("stopVideoMotionCaptureJob ServerResponseException" + e.getMessage());
        }
    }
}
