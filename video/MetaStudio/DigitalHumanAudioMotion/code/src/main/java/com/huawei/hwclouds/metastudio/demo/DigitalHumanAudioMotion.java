package com.huawei.hwclouds.metastudio.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.metastudio.v1.MetaStudioClient;
import com.huaweicloud.sdk.metastudio.v1.model.DigitalAssetInfo;
import com.huaweicloud.sdk.metastudio.v1.model.ListTtsaJobsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListTtsaJobsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ListFacialAnimationsDataRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListFacialAnimationsDataResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CreateFacialAnimationsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreateFacialAnimationsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ListTtsaDataRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListTtsaDataResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CreateTtsaRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreateTtsaResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ListStylesRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListStylesResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CreateFASReq;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CreateTTSAReq;

import java.io.IOException;

public class DigitalHumanAudioMotion {
    private static final Logger logger = LoggerFactory.getLogger(DigitalHumanAudioMotion.class);

    // 获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID和您的AK/SK。
    // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK、HUAWEICLOUD_SDK_SK。
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");

    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");


    public static void main(String[] args) throws IOException {
        ICredential auth = new BasicCredentials()
                .withAk(AK)
                .withSk(SK);

        // 使用默认配置
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        // 创建MetaStudio实例
        MetaStudioClient mClient = MetaStudioClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(MetaStudioRegion.CN_NORTH_4) // region为上海一需写为：withEndpoint("cn-east-3")
                .build();

        // 1,获取语音驱动任务列表
        listTtsaJobs(mClient);
        // 2,创建语音驱动任务
        String style_id = listStyles(mClient);
        String voice_asset_id = getAsset(mClient, DigitalAssetInfo.AssetTypeEnum.VIDEO, AssetSourceEnum.SYSTEM, null).getAssetId();
        String job_id = createTtsa(mClient, voice_asset_id, style_id);
        // 3,获取语音驱动数据
        listTtsaData(mClient, job_id);
        // 4,创建语音驱动表情动画任务
        String facial_animations_job_id = createFacialAnimations(mClient);
        // 5,获取语音驱动表情数据
        listFacialAnimationsData(mClient, facial_animations_job_id);
    }

    /**
     * 获取语音驱动任务列表
     *
     */
    private static void listTtsaJobs(MetaStudioClient client) {
        logger.info("listTtsaJobs start");
        ListTtsaJobsRequest request = new ListTtsaJobsRequest();
        try {
            ListTtsaJobsResponse response = client.listTtsaJobs(request);
            logger.info("listTtsaJobs" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listTtsaJobs ClientRequestException" + e.getHttpStatusCode());
            logger.error("listTtsaJobs ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("listTtsaJobs ServerResponseException" + e.getHttpStatusCode());
            logger.error("listTtsaJobs ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 获取语音驱动表情数据
     *
     */
    private static void listFacialAnimationsData(MetaStudioClient client, String job_id) {
        logger.info("listFacialAnimationsData start");
        ListFacialAnimationsDataRequest request = new ListFacialAnimationsDataRequest().withJobId(job_id);
        try {
            ListFacialAnimationsDataResponse response = client.listFacialAnimationsData(request);
            logger.info("listFacialAnimationsData" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listFacialAnimationsData ClientRequestException" + e.getHttpStatusCode());
            logger.error("listFacialAnimationsData ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("listFacialAnimationsData ServerResponseException" + e.getHttpStatusCode());
            logger.error("listFacialAnimationsData ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 创建语音驱动表情动画任务
     *
     */
    private static String createFacialAnimations(MetaStudioClient client) {
        logger.info("createFacialAnimations start");
        CreateFacialAnimationsRequest request = new CreateFacialAnimationsRequest()
                .withBody(new CreateFASReq()
                        .withAudioFileDownloadUrl("<your audio url>")
                        .withFrameRate(60));
        String job_id = "";
        try {
            CreateFacialAnimationsResponse response = client.createFacialAnimations(request);
            job_id = response.getJobId();
            logger.info("createFacialAnimations" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createFacialAnimations ClientRequestException" + e.getHttpStatusCode());
            logger.error("createFacialAnimations ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("createFacialAnimations ServerResponseException" + e.getHttpStatusCode());
            logger.error("createFacialAnimations ServerResponseException" + e.getMessage());
        }
        return job_id;
    }


    /**
     * 获取语音驱动数据
     *
     */
    private static void listTtsaData(MetaStudioClient client, String job_id) {
        logger.info("listTtsaData start");
        ListTtsaDataRequest request = new ListTtsaDataRequest().withJobId(job_id);
        try {
            ListTtsaDataResponse response = client.listTtsaData(request);
            logger.info("listTtsaData" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listTtsaData ClientRequestException" + e.getHttpStatusCode());
            logger.error("listTtsaData ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("listTtsaData ServerResponseException" + e.getHttpStatusCode());
            logger.error("listTtsaData ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 创建语音驱动任务
     *
     */
    private static String createTtsa(MetaStudioClient client, String voice_asset_id, String style_id) {
        logger.info("createTtsaRequest start");
        CreateTtsaRequest request = new CreateTtsaRequest()
                .withBody(new CreateTTSAReq()
                        .withText("<your text>")
                        .withEmotion("HAPPY")
                        .withSpeed(100)
                        .withPitch(100)
                        .withVolume(140)
                        .withVoiceAssetId(voice_asset_id)
                        .withStyleId(style_id));
        String job_id = "";
        try {
            CreateTtsaResponse response = client.createTtsa(request);
            logger.info("createTtsaRequest" + response.toString());
            job_id = response.getJobId();
        } catch (ClientRequestException e) {
            logger.error("createTtsaRequest ClientRequestException" + e.getHttpStatusCode());
            logger.error("createTtsaRequest ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("createTtsaRequest ServerResponseException" + e.getHttpStatusCode());
            logger.error("createTtsaRequest ServerResponseException" + e.getMessage());
        }
        return job_id;
    }

    // 查询数字人风格列表
    private static String listStyles(MetaStudioClient client) {
        logger.info("listStyles start");
        ListStylesRequest request = new ListStylesRequest();
        String style_id = "";
        try {
            ListStylesResponse response = client.listStyles(request);
            logger.info("listStyles: " + response.toString());
            style_id = response.getStyles().get(0).getStyleId();
        } catch (ClientRequestException e) {
            logger.error("listStyles ClientRequestException " + e.getHttpStatusCode());
            logger.error("listStyles ClientRequestException " + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("listStyles ServerResponseException " + e.getHttpStatusCode());
            logger.error("listStyles ServerResponseException " + e.getMessage());
        }
        return style_id;
    }


    // 获取资产
    private static DigitalAssetInfo getAsset(MetaStudioClient client, DigitalAssetInfo.AssetTypeEnum assetType, ListAssetsRequest.AssetSourceEnum assetSource, String systemProperty) {
        DigitalAssetInfo digitalAssetInfo = null;
        try {
            ListAssetsRequest listAssetsRequest = new ListAssetsRequest().withSystemProperty(systemProperty) // 系统属性
                    .withAssetType(String.valueOf(assetType)) // 资产类型
                    .withAssetSource(assetSource) // 资产来源：系统资产
                    .withAssetState(String.valueOf(DigitalAssetInfo.AssetStateEnum.ACTIVED) // 主文件上传成功，资产激活。
                    );
            logger.info("ListAssetsRequest" + listAssetsRequest.toString());
            ListAssetsResponse response = client.listAssets(listAssetsRequest);
            digitalAssetInfo = response.getAssets().get(0);
        } catch (ClientRequestException e) {
            logger.error(" getAsset ClientRequestException" + e.getHttpStatusCode());
            logger.error(" getAsset ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error(" getAsset ServerResponseException" + e.getHttpStatusCode());
            logger.error(" getAsset ServerResponseException" + e.getMessage());
        }

        return digitalAssetInfo;
    }
}
