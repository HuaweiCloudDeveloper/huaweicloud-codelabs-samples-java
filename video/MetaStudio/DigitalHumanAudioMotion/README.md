

## 1、功能介绍

华为云提供了MetaStudio服务端SDK，您可以直接集成服务端SDK来调用MetaStudio的相关API，从而实现对MetaStudio的快速操作。 

**您将学到什么？**

该场景示例代码以数字人语音驱动为例，介绍如何使用MetaStudio Java SDK将输入的语音数据转换为驱动数字的表情基系数和肢体动作数据。

语音驱动可用于数字人的视频制作等场景。

## 2、开发时序图

![image](assets/DigitalHumanAudioMotion.jpg)


## 3、前置条件
- 1、已[注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=40c99ba3cea14417a2428c756a626599&lang=zh-cn)华为帐号并开通华为云，已进行[实名认证](https://support.huaweicloud.com/usermanual-account/zh-cn_topic_0077914254.html)。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、已获取账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 4、已获取MetaStudio服务对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见[API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html)。 

## 4、SDK获取和安装
您可以通过Maven配置所依赖的[数字内容生产线SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/MetaStudio?lang=Java)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-metastudio</artifactId>
    <version>3.1.78</version>
</dependency>

```

## 5、接口参数说明
关于接口参数的详细说明可参见：

[a.创建语音驱动任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateTtsa)

[b.查询视频驱动任务列表](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ListTtsaJobs)

[c.创建语音驱动表情动画任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateFacialAnimations)

[d.获取语音驱动表情数据](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ListFacialAnimationsData)

[e.获取语音驱动数据](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ListTtsaData)


## 6、关键代码片段

### 6.1、创建语音驱动任务
```java
/**
 * 创建语音驱动任务
 *
 */
private static String createTtsa(MetaStudioClient client, String voice_asset_id, String style_id) {
    logger.info("createTtsaRequest start");
    CreateTtsaRequest request = new CreateTtsaRequest()
        .withBody(new CreateTTSAReq()
        .withText("<your text>")
        .withEmotion("HAPPY")
        .withSpeed(100)
        .withPitch(100)
        .withVolume(140)
        .withVoiceAssetId(voice_asset_id)
        .withStyleId(style_id));
        String job_id = "";
    try {
        CreateTtsaResponse response = client.createTtsa(request);
        logger.info("createTtsaRequest" + response.toString());
        job_id = response.getJobId();
    } catch (ClientRequestException e) {
        logger.error("createTtsaRequest ClientRequestException" + e.getHttpStatusCode());
        logger.error("createTtsaRequest ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("createTtsaRequest ServerResponseException" + e.getHttpStatusCode());
        logger.error("createTtsaRequest ServerResponseException" + e.getMessage());
    }
    return job_id;
}
```

### 6.2、获取语音驱动任务列表
```java
/**
 * 获取语音驱动任务列表
 *
 */
private static void listTtsaJobs(MetaStudioClient client) {
    logger.info("listTtsaJobs start");
    ListTtsaJobsRequest request = new ListTtsaJobsRequest();
    try {
        ListTtsaJobsResponse response = client.listTtsaJobs(request);
        logger.info("listTtsaJobs" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("listTtsaJobs ClientRequestException" + e.getHttpStatusCode());
        logger.error("listTtsaJobs ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("listTtsaJobs ServerResponseException" + e.getHttpStatusCode());
        logger.error("listTtsaJobs ServerResponseException" + e.getMessage());
    }
    }
```

### 6.3、创建语音驱动表情动画任务
```java
/**
 * 创建语音驱动表情动画任务
 *
 */
private static String createFacialAnimations(MetaStudioClient client) {
    logger.info("createFacialAnimations start");
    CreateFacialAnimationsRequest request = new CreateFacialAnimationsRequest()
            .withBody(new CreateFASReq()
                    .withAudioFileDownloadUrl("<your audio url>")
                    .withFrameRate(60));
    String job_id = "";
    try {
        CreateFacialAnimationsResponse response = client.createFacialAnimations(request);
        job_id = response.getJobId();
        logger.info("createFacialAnimations" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("createFacialAnimations ClientRequestException" + e.getHttpStatusCode());
        logger.error("createFacialAnimations ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("createFacialAnimations ServerResponseException" + e.getHttpStatusCode());
        logger.error("createFacialAnimations ServerResponseException" + e.getMessage());
    }
    return job_id;
}
```

### 6.4、获取语音驱动表情数据
```java
/**
 * 获取语音驱动表情数据
 *
 */
private static void listFacialAnimationsData(MetaStudioClient client, String job_id) {
    logger.info("listFacialAnimationsData start");
    ListFacialAnimationsDataRequest request = new ListFacialAnimationsDataRequest().withJobId(job_id);
    try {
        ListFacialAnimationsDataResponse response = client.listFacialAnimationsData(request);
        logger.info("listFacialAnimationsData" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("listFacialAnimationsData ClientRequestException" + e.getHttpStatusCode());
        logger.error("listFacialAnimationsData ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("listFacialAnimationsData ServerResponseException" + e.getHttpStatusCode());
        logger.error("listFacialAnimationsData ServerResponseException" + e.getMessage());
    }
}
```

### 6.5、获取语音驱动数据
```java
/**
 * 获取语音驱动数据
 *
 */
private static void listTtsaData(MetaStudioClient client, String job_id) {
    logger.info("listTtsaData start");
    ListTtsaDataRequest request = new ListTtsaDataRequest().withJobId(job_id);
    try {
        ListTtsaDataResponse response = client.listTtsaData(request);
        logger.info("listTtsaData" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("listTtsaData ClientRequestException" + e.getHttpStatusCode());
        logger.error("listTtsaData ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("listTtsaData ServerResponseException" + e.getHttpStatusCode());
        logger.error("listTtsaData ServerResponseException" + e.getMessage());
    }
}
```

## 7、运行结果
**创建语音驱动任务**
```json
{
  "X-Request-Id":"d2e25f***",
  "job_id":"ceddc9***"
}
```

**获取语音驱动任务列表**
```json
{
	"total":18085,
    "X-Request-Id":"c129cd***",
	"ttsa_jobs":[
		{
			"start_time":"2024-01-26T16:42:18Z",
			"job_id":"d09a2c***",
			"end_time":"2024-01-26T16:43:21Z",
			"content_duration":3.05,
            "state":"FAILED"
		},{
			"start_time":"2024-01-26T16:42:16Z",
			"job_id":"4787d9***",
			"end_time":"2024-01-26T16:43:18Z",
			"content_duration":3.05,
            "state":{
				"$ref":"$.ttsa_jobs[0].state"
			}
		}
	]
}
```

**创建语音驱动表情动画任务**
```json
{
	"X-Request-Id":"f32446***",
	"job_id":"12e9c4***"
}
```

**获取语音驱动表情数据**
```json
{
	"state":"PROCESSING"
}
```

**获取语音驱动数据**
```json
{
  "X-Request-Id":"1b8281***"
}
```



## 8、参考
本示例的代码工程仅用于简单演示，实际开发过程中应严格遵循开发指南。访问以下链接可以获取详细信息：[开发指南](https://support.huaweicloud.com/ssdk-metastudio/metastudio_06_0000.html)