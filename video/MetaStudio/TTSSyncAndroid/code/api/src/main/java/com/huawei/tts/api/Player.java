package com.huawei.tts.api;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;

import androidx.annotation.NonNull;

import com.huawei.tts.consts.Constants;
import com.huawei.tts.util.ThreadUtils;

import java.util.Arrays;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicBoolean;

final class Player {

    private static final String TAG = Player.class.getSimpleName();

    private static final String SPEAKER_PLAYER_THREAD_NAME = "SPEAKER_PLAYER_THREAD";

    private static final int BUFFER_SIZE = AudioTrack.getMinBufferSize(Constants.VOICE.SAMPLE_RATE, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);

    // 主线程读写
    private PlayStateChangeListener playStateChangeListener;

    // 主线程读写
    private int playState = AudioTrack.PLAYSTATE_STOPPED;

    private ProcessVoiceDataThread processVoiceDataThread;

    private AudioTrack audioTrack;

    private Player() {
    }

    private @NonNull AudioTrack createAudioTrace() {
        Logger.i(TAG, " createAudioTrace ");
        return new AudioTrack(
                AudioManager.STREAM_MUSIC,
                Constants.VOICE.SAMPLE_RATE,
                AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                BUFFER_SIZE * 2,
                AudioTrack.MODE_STREAM
        );
    }

    void write(byte[] voiceData) {
        if (TTSConfig.isDebug()) {
            Logger.i(TAG, " write voiceData : " + (voiceData == null ? "0" : voiceData.length));
        }
        try {
            if (voiceData == null || voiceData.length == 0) {
                return;
            }
            if (audioTrack == null) {
                audioTrack = createAudioTrace();
            }
            if (processVoiceDataThread == null) {
                processVoiceDataThread = new ProcessVoiceDataThread(SPEAKER_PLAYER_THREAD_NAME);
            }
            processVoiceDataThread.writeVoiceData(voiceData);
        } catch (Exception e) {
            if (TTSConfig.isDebug()) {
                Logger.e(TAG, " write invalid state : " + e);
            }
        }
    }

    void play() {
        Logger.i(TAG, " play ");
        try {
            if (audioTrack == null) {
                audioTrack = createAudioTrace();
            }
            if (processVoiceDataThread != null) {
                processVoiceDataThread.wake();
            }
            audioTrack.play();
            dispatchPlayStateChangeEvent(AudioTrack.PLAYSTATE_PLAYING);
        } catch (Exception e) {
            Logger.e(TAG, " resume invalid state : " + e);
        }
    }

    void pause() {
        Logger.i(TAG, " pause ");
        try {
            if (audioTrack != null) {
                audioTrack.pause();
            }
            if (processVoiceDataThread != null) {
                processVoiceDataThread.sleep();
            }
            dispatchPlayStateChangeEvent(AudioTrack.PLAYSTATE_PAUSED);
        } catch (Exception e) {
            Logger.e(TAG, " pause invalid state : " + e);
        }
    }

    void stop() {
        Logger.i(TAG, " stop ");
        try {
            if (processVoiceDataThread != null) {
                processVoiceDataThread.release();
                processVoiceDataThread = null;
            }
            if (audioTrack != null) {
                audioTrack.flush();
                audioTrack.stop();
                audioTrack.release();
                audioTrack = null;
            }
            dispatchPlayStateChangeEvent(AudioTrack.PLAYSTATE_STOPPED);
        } catch (Exception e) {
            Logger.e(TAG, " stop invalid state : " + e);
        }
    }

    public void setPlayStateChangeListener(@NonNull PlayStateChangeListener playStateChangeListener) {
        ThreadUtils.getInstance().runOnMainThread(() -> Player.this.playStateChangeListener = playStateChangeListener);
    }

    static Player getInstance() {
        return Player.SingletonHolder.INSTANCE;
    }

    private void dispatchPlayStateChangeEvent(int playState) {
        ThreadUtils.getInstance().runOnMainThread(() -> {
            if (Player.this.playState == playState) {
                return;
            }
            Player.this.playState = playState;
            if (playStateChangeListener != null) {
                playStateChangeListener.onPlayStateChanged(playState);
            }
        });
    }

    private static class SingletonHolder {
        private static final Player INSTANCE = new Player();
    }

    private static class ProcessVoiceDataThread extends Thread {
        private final ConcurrentLinkedDeque<byte[]> voiceDataQueue = new ConcurrentLinkedDeque<>();

        private final AtomicBoolean isReleased = new AtomicBoolean(false);

        private final AtomicBoolean isSleeping = new AtomicBoolean(false);

        private static final Object PROCESS_VOICE_LOCK = new Object();

        ProcessVoiceDataThread(String name) {
            super(name);
            start();
        }

        void writeVoiceData(@NonNull byte[] voiceData) {
            try {
                voiceDataQueue.offer(voiceData);
                if (TTSConfig.isDebug()) {
                    Logger.i(TAG, " writeVoiceData queue size : " + voiceDataQueue.size() + " , voice data size : " + voiceData.length);
                }
            } catch (Exception e) {
                if (TTSConfig.isDebug()) {
                    Logger.e(TAG, " writeVoiceData error : " + e);
                }
            }
        }

        void sleep() {
            Logger.i(TAG, " sleep ");
            isSleeping.set(true);
        }

        void wake() {
            if (isSleeping.get()) {
                isSleeping.set(false);
                synchronized (PROCESS_VOICE_LOCK) {
                    try {
                        PROCESS_VOICE_LOCK.notifyAll();
                        Logger.i(TAG, " wake ");
                    } catch (Exception e) {
                        // ignored
                    }
                }
            }
        }

        void release() {
            Logger.i(TAG, " release ");
            isReleased.set(true);
            voiceDataQueue.clear();
        }

        @Override
        public void run() {
            while (true) {
                if (isReleased.get()) {
                    return;
                }
                checkSleeping();
                byte[] voiceData = voiceDataQueue.peek();
                if (isVoiceDataValid(voiceData) && Player.getInstance().audioTrack != null) {
                    executeWriting(voiceData);
                    notifyVoiceDataQueueEmpty();
                }
            }
        }

        private void notifyVoiceDataQueueEmpty() {
            ThreadUtils.getInstance().runOnMainThread(() -> {
                if (voiceDataQueue.isEmpty() && Player.getInstance().playState == AudioTrack.PLAYSTATE_PLAYING) {
                    Player.getInstance().stop();
                }
            });
        }

        private void checkSleeping() {
            if (isSleeping.get()) {
                synchronized (PROCESS_VOICE_LOCK) {
                    try {
                        PROCESS_VOICE_LOCK.wait();
                        Logger.i(TAG, " checkSleeping start sleep ");
                    } catch (Exception e) {
                        // ignored
                    }
                }
            }
        }

        private void executeWriting(byte[] voiceData) {
            if (isReleased.get()) {
                return;
            }
            try {
                int writtenSize = Player.getInstance().audioTrack.write(voiceData, 0, voiceData.length);
                if (writtenSize == 0 && voiceDataQueue.size() > 1) {// 缓冲区满了，完全写入不了
                    if (TTSConfig.isDebug()) {
                        Logger.i(TAG, " executeWriting buffer is full , queue size : " + voiceDataQueue.size() + " , voice data size : " + voiceData.length);
                    }
                } else if (writtenSize < voiceData.length && voiceDataQueue.size() > 1) {// 缓存区就要满了，写了一部分
                    byte[] remainVoiceData = Arrays.copyOfRange(voiceData, writtenSize, voiceData.length);
                    voiceDataQueue.poll();
                    if (isVoiceDataValid(remainVoiceData)) {
                        voiceDataQueue.addFirst(remainVoiceData);
                    }
                    if (TTSConfig.isDebug()) {
                        Logger.i(TAG, " executeWriting buffer is nearly full , queue size : " + voiceDataQueue.size() + " , voice data size : " + voiceData.length + " , written size : " + writtenSize + " , remain size : " + remainVoiceData.length);
                    }
                } else {
                    voiceDataQueue.poll();
                    if (TTSConfig.isDebug()) {
                        Logger.i(TAG, " executeWriting voice data written done , queue size : " + voiceDataQueue.size() + " , voice data size : " + voiceData.length);
                    }
                }
            } catch (Exception e) {
                if (TTSConfig.isDebug()) {
                    Logger.e(TAG, " executeWriting error : " + e);
                }
            }
        }

        private static boolean isVoiceDataValid(byte[] voiceData) {
            if (voiceData == null) {
                return false;
            }
            return voiceData.length > 0;
        }
    }

    interface PlayStateChangeListener {
        void onPlayStateChanged(int playState);
    }
}
