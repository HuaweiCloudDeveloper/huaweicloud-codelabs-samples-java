package com.huawei.tts.api.model.param;

/**
 * 朗读模式。
 */
public enum SpeakMode {
    /**
     * 仅返回音频流数据
     */
    ONLY_VOICE_DATA,
    /**
     * 返回音频数据，同时朗读
     */
    PLAY_AUDIO,
    /**
     * 返回音频数据，同时写入文件
     */
    WRITE_FILE,
    /**
     * 返回音频数据，朗读并写入文件
     */
    PLAY_AUDIO_AND_WRITE_FILE,
}
