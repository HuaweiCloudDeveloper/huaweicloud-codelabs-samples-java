package com.huawei.tts.api.model.config;

import android.text.TextUtils;

/**
 * 朗读声音参数。
 */
public final class VoiceConfig {

    /**
     * 声音资产Id
     */
    private String voiceAssetId;

    /**
     * 语速
     */
    private int speed;

    /**
     * 声高
     */
    private int pitch;

    /**
     * 音量
     */
    private int volume;

    public static VoiceConfig newInstance(VoiceConfig voiceConfig) {
        VoiceConfig newVoiceConfig = new VoiceConfig();
        if (voiceConfig != null) {
            newVoiceConfig.setVoiceAssetId(voiceConfig.getVoiceAssetId());
            newVoiceConfig.setSpeed(voiceConfig.getSpeed());
            newVoiceConfig.setPitch(voiceConfig.getPitch());
            newVoiceConfig.setVolume(voiceConfig.getVolume());
        }
        return newVoiceConfig;
    }

    public String getVoiceAssetId() {
        return voiceAssetId;
    }

    public VoiceConfig setVoiceAssetId(String voiceAssetId) {
        this.voiceAssetId = voiceAssetId;
        return this;
    }

    public int getSpeed() {
        return speed;
    }

    public VoiceConfig setSpeed(int speed) {
        this.speed = speed;
        return this;
    }

    public int getPitch() {
        return pitch;
    }

    public VoiceConfig setPitch(int pitch) {
        this.pitch = pitch;
        return this;
    }

    public int getVolume() {
        return volume;
    }

    public VoiceConfig setVolume(int volume) {
        this.volume = volume;
        return this;
    }

    @Override
    public String toString() {
        return "VoiceConfig{" +
                "voiceAssetId='" + voiceAssetId + '\'' +
                ", speed=" + speed +
                ", pitch=" + pitch +
                ", volume=" + volume +
                '}';
    }

    public String toSafeString() {
        return "VoiceConfig{" +
                "voiceAssetId='" + (TextUtils.isEmpty(voiceAssetId) ? "0" : voiceAssetId.length()) + '\'' +
                ", speed=" + speed +
                ", pitch=" + pitch +
                ", volume=" + volume +
                '}';
    }
}
