package com.huawei.tts.api.model.request;

import java.util.List;

public class AuthRequestBodyModel {

    private AuthModel auth;

    public AuthModel getAuth() {
        return auth;
    }

    public void setAuth(AuthModel auth) {
        this.auth = auth;
    }

    public static class AuthModel {
        private IdentityModel identity;
        private ScopeModel scope;

        public IdentityModel getIdentity() {
            return identity;
        }

        public void setIdentity(IdentityModel identity) {
            this.identity = identity;
        }

        public ScopeModel getScope() {
            return scope;
        }

        public void setScope(ScopeModel scope) {
            this.scope = scope;
        }

        public static class IdentityModel {
            private List<String> methods;
            private PasswordModel password;

            public List<String> getMethods() {
                return methods;
            }

            public void setMethods(List<String> methods) {
                this.methods = methods;
            }

            public PasswordModel getPassword() {
                return password;
            }

            public void setPassword(PasswordModel password) {
                this.password = password;
            }

            public static class PasswordModel {
                private UserModel user;

                public UserModel getUser() {
                    return user;
                }

                public void setUser(UserModel user) {
                    this.user = user;
                }

                public static class UserModel {
                    private String name;
                    private String password;
                    private DomainModel domain;

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public String getPassword() {
                        return password;
                    }

                    public void setPassword(String password) {
                        this.password = password;
                    }

                    public DomainModel getDomain() {
                        return domain;
                    }

                    public void setDomain(DomainModel domain) {
                        this.domain = domain;
                    }

                    public static class DomainModel {
                        private String name;

                        public String getName() {
                            return name;
                        }

                        public void setName(String name) {
                            this.name = name;
                        }
                    }
                }
            }
        }

        public static class ScopeModel {
            private ProjectModel project;

            public ProjectModel getProject() {
                return project;
            }

            public void setProject(ProjectModel project) {
                this.project = project;
            }

            public static class ProjectModel {
                private String name;

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }
            }
        }
    }
}
