package com.huawei.tts.util;

import android.media.AudioTrack;

import com.huawei.tts.api.model.param.SpeakState;

public class Mappers {

    public static SpeakState transPlayStateToSpeakState(int playState) {
        if (playState == AudioTrack.PLAYSTATE_PAUSED) {
            return SpeakState.PAUSED;
        } else if (playState == AudioTrack.PLAYSTATE_PLAYING) {
            return SpeakState.SPEAKING;
        } else {
            return SpeakState.STOPPED;
        }
    }

}
