package com.huawei.tts.api.model.param;

/**
 * 朗读选项。
 */
public class SpeakOption {

    /**
     * 朗读模式，必选
     */
    private SpeakMode speakMode = SpeakMode.PLAY_AUDIO;
    /**
     * 写入文件路径。如果朗读模式需要写入本地文件，必选
     */
    private String filePath = "";

    public SpeakMode getSpeakMode() {
        return speakMode;
    }

    public void setSpeakMode(SpeakMode speakMode) {
        this.speakMode = speakMode;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public String toString() {
        return "SpeakOption{" +
                "speakMode=" + speakMode +
                ", filePath='" + filePath + '\'' +
                '}';
    }

    public String toSafeString() {
        return "SpeakOption{" +
                "speakMode=" + speakMode +
                ", filePath='" + (filePath == null ? "0" : filePath.length()) + '\'' +
                '}';
    }
}
