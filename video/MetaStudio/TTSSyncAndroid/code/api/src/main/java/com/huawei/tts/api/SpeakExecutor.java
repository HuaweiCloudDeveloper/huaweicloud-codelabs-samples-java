package com.huawei.tts.api;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

final class SpeakExecutor {

    private static final String TAG = SpeakExecutor.class.getSimpleName();

    private static final Lock SPEAKER_LOCK = new ReentrantLock();

    static void executeSynchronously(SpeakerRunnable task) {
        try {
            SPEAKER_LOCK.lockInterruptibly();
            try {
                task.run();
            } finally {
                SPEAKER_LOCK.unlock();
            }
        } catch (InterruptedException | IllegalMonitorStateException e) {
            Logger.e(TAG, " executeSynchronously error : " + e + " , name : " + task.getTaskName() + " , task : " + task);
        }
    }

    interface SpeakerRunnable extends Runnable {
        String getTaskName();
    }
}
