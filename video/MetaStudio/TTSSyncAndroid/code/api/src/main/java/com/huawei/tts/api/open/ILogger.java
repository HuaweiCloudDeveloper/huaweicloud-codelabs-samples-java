package com.huawei.tts.api.open;

public interface ILogger {
    void v(String tag, String msg);

    void i(String tag, String msg);

    void w(String tag, String msg);

    void e(String tag, String msg);
}
