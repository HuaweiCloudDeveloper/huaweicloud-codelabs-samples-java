package com.huawei.tts.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.huawei.tts.api.Logger;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GsonUtil {
    private static final String TAG = GsonUtil.class.getSimpleName();
    private static final Gson GSON = createGson(true);

    private static final Gson GSON_NO_NULLS = createGson(false);

    private GsonUtil() {
    }

    public static Gson getGson() {
        return getGson(true);
    }

    public static Gson getGson(final boolean serializeNulls) {
        return serializeNulls ? GSON_NO_NULLS : GSON;
    }

    public static String toJson(final Object object) {
        return toJson(object, true);
    }

    public static String toJson(final Object object, final boolean includeNulls) {
        return includeNulls ? GSON.toJson(object) : GSON_NO_NULLS.toJson(object);
    }

    public static String toJson(final Object src, final Type typeOfSrc) {
        return toJson(src, typeOfSrc, true);
    }

    public static String toJson(final Object src, final Type typeOfSrc, final boolean includeNulls) {
        return includeNulls ? GSON.toJson(src, typeOfSrc) : GSON_NO_NULLS.toJson(src, typeOfSrc);
    }


    public static <T> T fromJson(final String json, final Class<T> type) {
        try {
            return GSON.fromJson(json, type);
        } catch (JsonSyntaxException jsonSyntaxException) {
            Logger.e(TAG, " fromJson jsonSyntaxException : " + jsonSyntaxException);
            return null;
        }
    }

    public static <T> T fromJson(final String json, final Type type) {
        try {
            return GSON.fromJson(json, type);
        } catch (JsonSyntaxException jsonSyntaxException) {
            Logger.e(TAG, " fromJson jsonSyntaxException : " + jsonSyntaxException);
            return null;
        }
    }

    public static <T> T fromJson(final Reader reader, final Class<T> type) {
        try {
            return GSON.fromJson(reader, type);
        } catch (JsonSyntaxException jsonSyntaxException) {
            Logger.e(TAG, " fromJson jsonSyntaxException : " + jsonSyntaxException);
            return null;
        } catch (JsonIOException jsonIOException) {
            Logger.e(TAG, " fromJson jsonIOException : " + jsonIOException);
            return null;
        }
    }

    public static <T> T fromJson(final Reader reader, final Type type) {
        try {
            return GSON.fromJson(reader, type);
        } catch (JsonSyntaxException jsonSyntaxException) {
            Logger.e(TAG, " fromJson jsonSyntaxException : " + jsonSyntaxException);
            return null;
        } catch (JsonIOException jsonIOException) {
            Logger.e(TAG, " fromJson jsonIOException : " + jsonIOException);
            return null;
        }
    }

    public static Type getListType(final Type type) {
        return TypeToken.getParameterized(List.class, type).getType();
    }

    public static Type getSetType(final Type type) {
        return TypeToken.getParameterized(Set.class, type).getType();
    }

    public static Type getMapType(final Type keyType, final Type valueType) {
        return TypeToken.getParameterized(Map.class, keyType, valueType).getType();
    }

    public static Type getArrayType(final Type type) {
        return TypeToken.getArray(type).getType();
    }

    public static Type getType(final Type rawType, final Type... typeArguments) {
        return TypeToken.getParameterized(rawType, typeArguments).getType();
    }

    private static Gson createGson(final boolean serializeNulls) {
        final GsonBuilder builder = new GsonBuilder();
        if (serializeNulls) {
            builder.serializeNulls();
        }
        builder.registerTypeAdapterFactory(EnumTypeAdapter.ENUM_FACTORY);
        return builder.create();
    }

    public static class EnumTypeAdapter<T extends Enum<T>> extends TypeAdapter<T> {
        private static final String TAG = EnumTypeAdapter.class.getSimpleName();
        public static final TypeAdapterFactory ENUM_FACTORY = new TypeAdapterFactory() {
            @SuppressWarnings({"rawtypes", "unchecked"})
            @Override
            public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
                Class<? super T> rawType = typeToken.getRawType();
                if (!Enum.class.isAssignableFrom(rawType) || rawType == Enum.class) {
                    return null;
                }
                if (!rawType.isEnum()) {
                    rawType = rawType.getSuperclass(); // handle anonymous subclasses
                }
                return (TypeAdapter<T>) new EnumTypeAdapter(rawType);
            }
        };

        private final Map<Integer, T> nameToConstant = new HashMap<>();

        private final Map<T, Integer> constantToName = new HashMap<>();

        public EnumTypeAdapter(Class<T> classOfT) {
            if (classOfT == null) {
                Logger.e(TAG, " EnumTypeAdapter classOfT is null ");
                return;
            }

            Field field = null;
            try {
                field = classOfT.getDeclaredField("value");
                field.setAccessible(true);
            } catch (NoSuchFieldException e) {
                Logger.e(TAG, " EnumTypeAdapter NoSuchFieldException : " + e);
            }

            if (classOfT.getEnumConstants() == null || classOfT.getEnumConstants().length == 0) {
                return;
            }
            for (T constant : classOfT.getEnumConstants()) {
                Integer name = constant.ordinal();
                try {
                    if (field != null) {
                        Object object = field.get(constant);
                        name = (object instanceof Integer) ? (Integer) object : name;
                    }
                } catch (IllegalAccessException e) {
                    Logger.e(TAG, " EnumTypeAdapter IllegalAccessException : " + e);
                }
                nameToConstant.put(name, constant);
                constantToName.put(constant, name);
            }
        }

        @Override
        public T read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            return nameToConstant.get(in.nextInt());
        }

        @Override
        public void write(JsonWriter out, T value) throws IOException {
            if (out != null) {
                out.value(value == null ? null : constantToName.get(value));
            }
        }
    }
}
