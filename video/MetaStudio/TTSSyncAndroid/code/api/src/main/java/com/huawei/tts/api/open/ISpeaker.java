package com.huawei.tts.api.open;

import com.huawei.tts.api.Speaker;
import com.huawei.tts.api.model.param.SpeakOption;
import com.huawei.tts.api.model.param.SpeakState;
import com.huawei.tts.api.open.callback.ISpeakCallback;

public interface ISpeaker {

    /**
     * 开始朗读，发声，不写入本地文件。注意：此接口不支持并行调用，只支持串行调用，必须在上一次调用结束（onFinish或onError通知）后再调用下一次。
     *
     * @param text     朗读的文字
     * @param callback 执行朗读的结果通知
     */
    void startSpeaking(String text, ISpeakCallback callback);

    /**
     * 开始朗读，可以自定义朗读选项：是否发声、是否写入本地文件。具体参考SpeakOption枚举。注意：此接口不支持并行调用，只支持串行调用，必须在上一次调用结束（onFinish或onError通知）后再调用下一次。
     *
     * @param text        朗读的文字
     * @param speakOption 朗读的选项
     * @param callback    执行朗读的结果通知
     */
    void startSpeaking(String text, SpeakOption speakOption, ISpeakCallback callback);

    /**
     * 停止朗读。
     */
    void stopSpeaking();

    /**
     * 暂停朗读。此时只暂停播放声音，不会停止接收服务端WebSocket的音频数据流。
     */
    void pauseSpeaking();

    /**
     * 恢复朗读。
     */
    void resumeSpeaking();

    /**
     * 获取当前朗读状态，具体参考SpeakState枚举。
     *
     * @return 朗读状态
     */
    SpeakState getSpeakState();

    /**
     * 添加朗读状态监听，当朗读状态发声变更时会会上报。
     *
     * @param listener 朗读状态监听
     */
    void addOnSpeakStateChangeListener(Speaker.OnSpeakStateChangeListener listener);

    /**
     * 移除朗读状态监听。
     *
     * @param listener 朗读状态监听
     */
    void removeOnSpeakStateChangeListener(Speaker.OnSpeakStateChangeListener listener);
}
