package com.huawei.tts.api;

import android.text.TextUtils;

import com.huawei.tts.api.model.config.AuthConfig;
import com.huawei.tts.api.model.config.VoiceConfig;
import com.huawei.tts.api.model.param.Region;
import com.huawei.tts.api.open.IConfigurator;
import com.huawei.tts.api.open.ILogger;
import com.huawei.tts.net.HttpClientManager;

public final class Configurator implements IConfigurator {

    private static final String TAG = Configurator.class.getSimpleName();

    @Override
    public IConfigurator setDebug(boolean isDebug) {
        Logger.i(TAG, " isDebug : " + isDebug);
        HttpClientManager.getInstance().setCaCheckOn(!isDebug);
        TTSConfig.setDebugMode(isDebug);
        return this;
    }

    @Override
    public IConfigurator setLogPrintingEnabled(boolean isLogPrintingEnabled) {
        Logger.i(TAG, " setLogPrintingEnabled : " + isLogPrintingEnabled);
        TTSConfig.setLogPrintingEnabled(isLogPrintingEnabled);
        return this;
    }

    @Override
    public IConfigurator setLogger(ILogger logger) {
        Logger.i(TAG, " setLogger : " + logger);
        TTSConfig.setLogger(logger);
        return this;
    }

    @Override
    public IConfigurator setRegion(Region region) {
        Logger.i(TAG, " setRegion : " + region);
        TTSConfig.setRegion(region);
        return this;
    }

    @Override
    public IConfigurator setProjectId(String projectId) {
        if (TTSConfig.isDebug()) {
            Logger.i(TAG, " setProjectId : " + projectId);
        } else {
            Logger.i(TAG, " setProjectId : " + (TextUtils.isEmpty(projectId) ? "0" : projectId.length()));
        }
        TTSConfig.setProjectId(projectId);
        return this;
    }

    @Override
    public IConfigurator setAuthConfig(AuthConfig authConfig) {
        if (TTSConfig.isDebug()) {
            Logger.i(TAG, " setAuthConfig : " + authConfig);
        } else {
            Logger.i(TAG, " setAuthConfig : " + (authConfig == null ? "null" : authConfig.toSafeString()));
        }
        TTSConfig.setAuthConfig(AuthConfig.newInstance(authConfig));
        return this;
    }

    @Override
    public IConfigurator setVoiceConfig(VoiceConfig voiceConfig) {
        if (TTSConfig.isDebug()) {
            Logger.i(TAG, " setVoiceConfig : " + voiceConfig);
        } else {
            Logger.i(TAG, " setVoiceConfig : " + (voiceConfig == null ? "null" : voiceConfig.toSafeString()));
        }
        TTSConfig.setVoiceConfig(VoiceConfig.newInstance(voiceConfig));
        return this;
    }
}
