package com.huawei.tts.util;

import android.os.Handler;
import android.os.Looper;

public class ThreadUtils {

    private final Handler handler = new Handler(Looper.getMainLooper());

    private ThreadUtils() {
    }

    public void runOnMainThread(Runnable runnable) {
        this.handler.post(runnable);
    }

    public void runOnMainThread(Runnable runnable, long delay) {
        this.handler.postDelayed(runnable, delay);
    }

    public static ThreadUtils getInstance() {
        return ThreadUtils.SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        private static final ThreadUtils INSTANCE = new ThreadUtils();
    }

}
