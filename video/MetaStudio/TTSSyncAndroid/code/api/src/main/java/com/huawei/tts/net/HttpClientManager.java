package com.huawei.tts.net;

import androidx.annotation.NonNull;

import com.huawei.tts.api.Logger;
import com.huawei.tts.consts.Constants;

import java.security.SecureRandom;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;

public class HttpClientManager {

    private static final String TAG = HttpClientManager.class.getSimpleName();

    private OkHttpClient client;

    private boolean isCaCheckOn = true;

    private HttpClientManager() {
        client = getOkHttpClient();
    }

    public OkHttpClient getClient() {
        return client;
    }

    public void setCaCheckOn(boolean isCaCheckOn) {
        if (this.isCaCheckOn != isCaCheckOn) {
            Logger.i(TAG, " setCaCheckOn isCaCheckOn : " + isCaCheckOn);
            this.isCaCheckOn = isCaCheckOn;
            client = getOkHttpClient();
        }
    }

    private OkHttpClient getOkHttpClient() {
        Logger.i(TAG, " getOkHttpClient isCaCheckOn : " + isCaCheckOn);
        if (isCaCheckOn) {
            return getDefaultOkHttpClient();
        } else {
            return getUnsafeOkHttpClient();
        }
    }

    private static @NonNull OkHttpClient getDefaultOkHttpClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(Constants.HTTP.TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(Constants.HTTP.TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(Constants.HTTP.TIME_OUT, TimeUnit.SECONDS)
                .build();
    }

    private static @NonNull OkHttpClient getUnsafeOkHttpClient() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };
            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            SecureRandom secureRandom = SecureRandom.getInstanceStrong();
            sslContext.init(null, trustAllCerts, secureRandom);
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            return new OkHttpClient.Builder()
                    .sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0])
                    .hostnameVerifier((hostName, sslSession) -> true)
                    .connectTimeout(Constants.HTTP.TIME_OUT, TimeUnit.SECONDS)
                    .readTimeout(Constants.HTTP.TIME_OUT, TimeUnit.SECONDS)
                    .writeTimeout(Constants.HTTP.TIME_OUT, TimeUnit.SECONDS)
                    .build();
        } catch (Exception e) {
            Logger.e(TAG, " getUnsafeOkHttpClient error : " + e + " , use default client ");
            return getDefaultOkHttpClient();
        }
    }

    public static HttpClientManager getInstance() {
        return HttpClientManager.SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        private static final HttpClientManager INSTANCE = new HttpClientManager();
    }

}
