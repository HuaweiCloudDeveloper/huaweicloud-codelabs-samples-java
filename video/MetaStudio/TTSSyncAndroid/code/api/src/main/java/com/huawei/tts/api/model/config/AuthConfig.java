package com.huawei.tts.api.model.config;

import android.text.TextUtils;

/**
 * 鉴权参数。警告：正式产品禁止将鉴权参数编码到端侧代码中，应当通过服务端下发鉴权参数，端侧再使用鉴权参数。
 * 方式一：使用token
 * 方式二：使用username + password + domain
 * 两种方式二选一，如果使用token，请保证token未超出有效期
 */
public class AuthConfig {

    private String token;
    private String username;
    private String password;
    private String domain;

    public static AuthConfig newInstance(AuthConfig authConfig) {
        AuthConfig newAuthConfig = new AuthConfig();
        if (authConfig != null) {
            newAuthConfig.setUsername(authConfig.getUsername())
                    .setPassword(authConfig.getPassword())
                    .setDomain(authConfig.getDomain())
                    .setToken(authConfig.getToken());
        }
        return newAuthConfig;
    }

    public String getToken() {
        return token;
    }

    public AuthConfig setToken(String token) {
        this.token = token;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public AuthConfig setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public AuthConfig setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getDomain() {
        return domain;
    }

    public AuthConfig setDomain(String domain) {
        this.domain = domain;
        return this;
    }

    @Override
    public String toString() {
        return "AuthConfig{" +
                "token='" + token + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", domain='" + domain + '\'' +
                '}';
    }

    public String toSafeString() {
        return "AuthConfig{" +
                "token='" + (TextUtils.isEmpty(token) ? "0" : token.length()) + '\'' +
                ", username='" + (TextUtils.isEmpty(username) ? "0" : username.length()) + '\'' +
                ", password='" + (TextUtils.isEmpty(password) ? "0" : password.length()) + '\'' +
                ", domain='" + (TextUtils.isEmpty(domain) ? "0" : domain.length()) + '\'' +
                '}';
    }
}
