package com.huawei.tts.net;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class SpeakWebSocketListener extends WebSocketListener {

    @Override
    public void onClosed(@NonNull WebSocket webSocket, int code, @NonNull String reason) {
    }

    @Override
    public void onClosing(@NonNull WebSocket webSocket, int code, @NonNull String reason) {
    }

    @Override
    public void onFailure(@NonNull WebSocket webSocket, @NonNull Throwable t, @Nullable Response response) {
    }

    @Override
    public void onMessage(@NonNull WebSocket webSocket, @NonNull String text) {
    }

    @Override
    public void onMessage(@NonNull WebSocket webSocket, @NonNull ByteString bytes) {
    }

    @Override
    public void onOpen(@NonNull WebSocket webSocket, @NonNull Response response) {
    }
}
