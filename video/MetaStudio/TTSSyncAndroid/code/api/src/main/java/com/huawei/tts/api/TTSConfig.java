package com.huawei.tts.api;

import com.huawei.tts.api.model.config.AuthConfig;
import com.huawei.tts.api.model.config.VoiceConfig;
import com.huawei.tts.api.model.param.Region;
import com.huawei.tts.api.open.ILogger;

class TTSConfig {

    private static boolean sisDebug;

    private static boolean sLogPrintingEnabled;

    private static ILogger sLogger;

    private static Region sRegion;

    private static String sProjectId;

    private static AuthConfig sAuthConfig;

    private static VoiceConfig sVoiceConfig;

    public static boolean isDebug() {
        return sisDebug;
    }

    public static void setDebugMode(boolean isDebug) {
        TTSConfig.sisDebug = isDebug;
    }

    public static boolean isLogPrintingEnabled() {
        return sLogPrintingEnabled;
    }

    public static void setLogPrintingEnabled(boolean logPrintingEnabled) {
        sLogPrintingEnabled = logPrintingEnabled;
    }

    static void setLogger(ILogger logger) {
        sLogger = logger;
    }

    static ILogger getLogger() {
        return sLogger;
    }

    static void setRegion(Region region) {
        sRegion = region;
    }

    static Region getRegion() {
        if (sRegion == null) {
            return Region.SHANGHAI;
        }
        return sRegion;
    }

    static void setProjectId(String projectId) {
        sProjectId = projectId;
    }

    static String getProjectId() {
        return sProjectId;
    }

    static void setAuthConfig(AuthConfig authConfig) {
        sAuthConfig = authConfig;
    }

    static AuthConfig getAuthConfig() {
        return AuthConfig.newInstance(sAuthConfig);
    }

    static void setVoiceConfig(VoiceConfig voiceConfig) {
        sVoiceConfig = voiceConfig;
    }

    static VoiceConfig getVoiceConfig() {
        return VoiceConfig.newInstance(sVoiceConfig);
    }
}
