package com.huawei.tts.api;

import android.util.Log;

public final class Logger {
    public static void v(String tag, String msg) {
        if (TTSConfig.isLogPrintingEnabled()) {
            if (TTSConfig.getLogger() != null) {
                TTSConfig.getLogger().v(tag, msg);
            } else {
                Log.v(tag, getThreadName() + msg);
            }
        }
    }

    public static void i(String tag, String msg) {
        if (TTSConfig.isLogPrintingEnabled()) {
            if (TTSConfig.getLogger() != null) {
                TTSConfig.getLogger().i(tag, msg);
            } else {
                Log.i(tag, getThreadName() + msg);
            }
        }
    }

    public static void w(String tag, String msg) {
        if (TTSConfig.isLogPrintingEnabled()) {
            if (TTSConfig.getLogger() != null) {
                TTSConfig.getLogger().w(tag, msg);
            } else {
                Log.w(tag, getThreadName() + msg);
            }
        }
    }

    public static void e(String tag, String msg) {
        if (TTSConfig.isLogPrintingEnabled()) {
            if (TTSConfig.getLogger() != null) {
                TTSConfig.getLogger().e(tag, msg);
            } else {
                Log.e(tag, getThreadName() + msg);
            }
        }
    }

    private static String getThreadName() {
        return " [" + Thread.currentThread().getName() + "] ";
    }
}
