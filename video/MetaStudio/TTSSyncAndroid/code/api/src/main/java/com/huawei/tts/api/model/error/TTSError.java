package com.huawei.tts.api.model.error;

/**
 * 错误码。警告：仅供调试参考，正式产品请重新封装错误码。
 */
public enum TTSError {
    SUCCESS("0", ""),
    UNKNOWN_AUTH_ERROR("100", "请参阅IAM状态码（https://support.huaweicloud.com/api-iam/iam_02_0005.html）"),
    AUTH_ERROR_WRONG_USERNAME_OR_PASSWORD("401", "认证信息非法（建议检查用户名和密码）"),
    UNKNOWN_SPEAK_ERROR("20000000", "未知异常"),
    SPEAK_ERROR_EMPTY_SOCKET_MSG("20050015", "WebSocket消息为空（建议跳过该次任务）"),
    SPEAK_ERROR_ASSERT_NOT_FOUND("20050004", "获取资产信息失败（如音色ID错误）"),
    SPEAK_ERROR_AUDIO_EXCEPTION("20050016", "音频流处理失败（建议跳过该次任务）"),
    SPEAK_ERROR_ASSET_EXCEPTION("20050024", "信息资产处理失败（如音色ID错误）"),
    SPEAK_ERROR_ALGORITHM_EXCEPTION("20050028", "算法侧返回失败（如文本中包含非法字符、英文音色搭配中文文本）"),
    SPEAK_ERROR_TTS_EXCEPTION("20050032", "TTS侧返回失败（服务内部会重试，建议跳过该次任务）");


    private final String code;
    private final String msg;

    TTSError(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
