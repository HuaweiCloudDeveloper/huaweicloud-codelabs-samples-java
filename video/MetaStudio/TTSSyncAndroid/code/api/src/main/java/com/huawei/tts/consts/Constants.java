package com.huawei.tts.consts;

public class Constants {

    public static class URL {
        // {region}
        public static final String AUTH = "https://iam.%s.myhuaweicloud.com/v3/auth/tokens";

        // {region} {project_id}
        public static final String SPEAK = "wss://metastudio-api.%s.myhuaweicloud.com:443/v1/%s/ttsc/jobs";
    }

    public static class HTTP {
        public static final int TIME_OUT = 10;
        public static final String RESPONSE_TOKEN_KEY = "X-Subject-Token";
        public static final String CONTENT_TYPE_KEY = "Content-Type";
        public static final String CONTENT_TYPE_JSON = "application/json;charset=utf-8";
        public static final int JOB_REQUEST_RANDOM_STRING_LENGTH = 10;
    }

    public static class VOICE {
        public static final int FMT_SIZE = 16;
        public static final int FORMAT_TAG = 0x0001;
        public static final int SAMPLE_RATE = 16000;
        public static final int CHANNELS = 1;
        public static final int BITS_PER_SAMPLE = 16;
    }
}
