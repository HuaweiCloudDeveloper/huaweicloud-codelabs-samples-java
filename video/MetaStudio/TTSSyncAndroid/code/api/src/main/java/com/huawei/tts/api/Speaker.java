package com.huawei.tts.api;

import android.text.TextUtils;
import android.util.ArrayMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.huawei.tts.api.model.error.TTSError;
import com.huawei.tts.api.model.param.Command;
import com.huawei.tts.api.model.param.SpeakMode;
import com.huawei.tts.api.model.param.SpeakOption;
import com.huawei.tts.api.model.param.SpeakState;
import com.huawei.tts.api.model.request.SpeakRequestBodyModel;
import com.huawei.tts.api.model.response.VoiceMessageModel;
import com.huawei.tts.api.open.ISpeaker;
import com.huawei.tts.api.open.callback.IActionCallback;
import com.huawei.tts.api.open.callback.ISpeakCallback;
import com.huawei.tts.consts.Constants;
import com.huawei.tts.net.HttpClientManager;
import com.huawei.tts.net.SpeakWebSocketListener;
import com.huawei.tts.util.DecodeUtils;
import com.huawei.tts.util.GsonUtil;
import com.huawei.tts.util.Mappers;
import com.huawei.tts.util.StringUtil;
import com.huawei.tts.util.ThreadUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

public final class Speaker implements ISpeaker {

    private static final String TAG = Speaker.class.getSimpleName();

    private final Map<String, List<byte[]>> voiceDataMap = new ArrayMap<>();

    private final List<OnSpeakStateChangeListener> onSpeakStateChangeListeners = new CopyOnWriteArrayList<>();

    private SpeakState speakState = SpeakState.STOPPED;

    private final AtomicBoolean isSpeakerWebSocketCanceled = new AtomicBoolean(false);

    private final SpeakingParam speakingParam = new SpeakingParam();

    private WebSocket speakerWebSocket;

    Speaker() {
        Player.getInstance().setPlayStateChangeListener(playState -> {
            speakState = Mappers.transPlayStateToSpeakState(playState);
            Logger.i(TAG, " onPlayStateChanged : " + playState);
            for (OnSpeakStateChangeListener onSpeakStateChangeListener : onSpeakStateChangeListeners) {
                onSpeakStateChangeListener.onSpeakStateChanged(speakState);
            }
        });
    }

    @Override
    public void startSpeaking(String text, ISpeakCallback callback) {
        setStartSpeakingParam(null, callback);
        startSpeaking(text);
    }

    @Override
    public void startSpeaking(String text, SpeakOption speakOption, ISpeakCallback callback) {
        setStartSpeakingParam(speakOption, callback);
        startSpeaking(text);
    }

    private void startSpeaking(String text) {
        Logger.i(TAG, " startSpeaking speakState : " + speakState + " , speakOption : " + (TTSConfig.isDebug() ? speakingParam.speakOption.toString() : speakingParam.speakOption.toSafeString()));
        Authenticator.getInstance().getAuthToken(new IActionCallback<String>() {
            @Override
            public void onSuccess(String token) {
                SpeakExecutor.executeSynchronously(new SpeakExecutor.SpeakerRunnable() {
                    @Override
                    public String getTaskName() {
                        return "IActionCallback-onSuccess";
                    }

                    @Override
                    public void run() {
                        onAuthTokenGot(token, text);
                    }
                });
            }

            @Override
            public void onFailed(String errorCode, String errorMsg) {
                ThreadUtils.getInstance().runOnMainThread(() -> {
                    if (speakingParam.speakCallback != null) {
                        speakingParam.speakCallback.onError(errorCode, errorMsg);
                    }
                });
            }
        });
    }

    @Override
    public void stopSpeaking() {
        SpeakExecutor.executeSynchronously(new SpeakExecutor.SpeakerRunnable() {
            @Override
            public String getTaskName() {
                return "stopSpeaking";
            }

            @Override
            public void run() {
                Logger.i(TAG, " stopSpeaking speakState : " + speakState);
                isSpeakerWebSocketCanceled.set(true);
                if (speakerWebSocket != null) {
                    speakerWebSocket.close(1000, "Normal close");
                    speakerWebSocket.cancel();
                    speakerWebSocket = null;
                }
                Player.getInstance().stop();
            }
        });
    }

    @Override
    public void pauseSpeaking() {
        SpeakExecutor.executeSynchronously(new SpeakExecutor.SpeakerRunnable() {
            @Override
            public String getTaskName() {
                return "pauseSpeaking";
            }

            @Override
            public void run() {
                Logger.i(TAG, " pauseSpeaking speakState : " + speakState);
                if (speakState == SpeakState.SPEAKING) {
                    Player.getInstance().pause();
                }
            }
        });
    }

    @Override
    public void resumeSpeaking() {
        SpeakExecutor.executeSynchronously(new SpeakExecutor.SpeakerRunnable() {
            @Override
            public String getTaskName() {
                return "resumeSpeaking";
            }

            @Override
            public void run() {
                Logger.i(TAG, " resumeSpeaking speakState : " + speakState);
                if (speakState == SpeakState.PAUSED) {
                    Player.getInstance().play();
                }
            }
        });
    }

    @Override
    public SpeakState getSpeakState() {
        return speakState;
    }

    @Override
    public void addOnSpeakStateChangeListener(OnSpeakStateChangeListener listener) {
        onSpeakStateChangeListeners.add(listener);
    }

    @Override
    public void removeOnSpeakStateChangeListener(OnSpeakStateChangeListener listener) {
        onSpeakStateChangeListeners.remove(listener);
    }

    private void onAuthTokenGot(String token, String text) {
        String jsonParams = genJsonParams(text);
        if (speakerWebSocket == null) {
            createWebSocket(token, new SpeakWebSocketListener() {
                @Override
                public void onClosing(@NonNull WebSocket webSocket, int code, @NonNull String reason) {
                    onWebSocketClosing(webSocket, code, reason);
                }

                private void onWebSocketClosing(@NonNull WebSocket webSocket, int code, @NonNull String reason) {
                    SpeakExecutor.executeSynchronously(new SpeakExecutor.SpeakerRunnable() {
                        @Override
                        public String getTaskName() {
                            return "SpeakWebSocketListener-onClosing";
                        }

                        @Override
                        public void run() {
                            if (isSpeakerWebSocketCanceled.get()) {
                                return;
                            }
                            if (TTSConfig.isDebug()) {
                                Logger.i(TAG, " startSpeaking SpeakWebSocketListener onClosing code : " + code + " , reason : " + reason + " , webSocket : " + webSocket);
                            } else {
                                Logger.i(TAG, " startSpeaking SpeakWebSocketListener onClosing code : " + code + " , reason : " + reason);
                            }
                            Speaker.this.speakerWebSocket = null;
                        }
                    });
                }

                @Override
                public void onFailure(@NonNull WebSocket webSocket, @NonNull Throwable throwable, @Nullable Response response) {
                    onWebSocketFailure(webSocket, throwable, response);
                }

                private void onWebSocketFailure(@NonNull WebSocket webSocket, @NonNull Throwable throwable, @Nullable Response response) {
                    SpeakExecutor.executeSynchronously(new SpeakExecutor.SpeakerRunnable() {
                        @Override
                        public String getTaskName() {
                            return "SpeakWebSocketListener-onFailure";
                        }

                        @Override
                        public void run() {
                            if (isSpeakerWebSocketCanceled.get()) {
                                return;
                            }
                            if (TTSConfig.isDebug()) {
                                Logger.e(TAG, " startSpeaking SpeakWebSocketListener onFailure throwable : " + throwable + " , response : " + response + " , webSocket : " + webSocket);
                            } else {
                                Logger.e(TAG, " startSpeaking SpeakWebSocketListener onFailure throwable : " + throwable);
                            }
                            ThreadUtils.getInstance().runOnMainThread(() -> {
                                if (speakingParam.speakCallback != null) {
                                    speakingParam.speakCallback.onError(TTSError.UNKNOWN_SPEAK_ERROR.getCode(), throwable.toString());
                                }
                            });
                        }
                    });
                }

                @Override
                public void onMessage(@NonNull WebSocket webSocket, @NonNull String text) {
                    onWebSocketMessage(webSocket, text);
                }

                private void onWebSocketMessage(@NonNull WebSocket webSocket, @NonNull String text) {
                    SpeakExecutor.executeSynchronously(new SpeakExecutor.SpeakerRunnable() {
                        @Override
                        public String getTaskName() {
                            return "SpeakWebSocketListener-onMessage";
                        }

                        @Override
                        public void run() {
                            if (isSpeakerWebSocketCanceled.get()) {
                                return;
                            }
                            if (TTSConfig.isDebug()) {
                                Logger.i(TAG, " startSpeaking SpeakWebSocketListener onMessage text : " + text.length() + " , webSocket : " + webSocket);
                            }
                            handleVoiceMessage(GsonUtil.fromJson(text, VoiceMessageModel.class));
                        }
                    });
                }

                @Override
                public void onOpen(@NonNull WebSocket webSocket, @NonNull Response response) {
                    onWebSocketOpen(webSocket, response);
                }

                private void onWebSocketOpen(@NonNull WebSocket webSocket, @NonNull Response response) {
                    SpeakExecutor.executeSynchronously(new SpeakExecutor.SpeakerRunnable() {
                        @Override
                        public String getTaskName() {
                            return "SpeakWebSocketListener-onOpen";
                        }

                        @Override
                        public void run() {
                            isSpeakerWebSocketCanceled.set(false);
                            if (TTSConfig.isDebug()) {
                                Logger.i(TAG, " startSpeaking SpeakWebSocketListener onOpen response : " + response + " , webSocket : " + webSocket + " , jsonParams : " + jsonParams);
                            } else {
                                Logger.i(TAG, " startSpeaking SpeakWebSocketListener onOpen ");
                            }
                            Speaker.this.speakerWebSocket = webSocket;
                            Speaker.this.speakerWebSocket.send(jsonParams);
                        }
                    });
                }
            });
        } else {
            SpeakExecutor.executeSynchronously(new SpeakExecutor.SpeakerRunnable() {
                @Override
                public String getTaskName() {
                    return "startSpeaking";
                }

                @Override
                public void run() {
                    if (isSpeakerWebSocketCanceled.get()) {
                        return;
                    }
                    if (TTSConfig.isDebug()) {
                        Logger.i(TAG, " startSpeaking jsonParams : " + jsonParams);
                    }
                    speakerWebSocket.send(jsonParams);
                }
            });
        }
    }

    private void handleVoiceMessage(VoiceMessageModel voiceMessage) {
        if (voiceMessage == null) {
            return;
        }
        String traceId = voiceMessage.getTraceId();
        if (!TextUtils.isEmpty(traceId)) {
            handleVoiceExceptionMessage(voiceMessage);
            return;
        }
        String jobId = voiceMessage.getJobId();
        if (TextUtils.isEmpty(jobId)) {
            return;
        }
        byte[] voiceData = DecodeUtils.decodeVoiceData(voiceMessage.getVoiceData());
        writeAndPlayVoiceData(voiceData);
        boolean isEnd = voiceMessage.getIsEnd();
        handleVoiceDataMap(jobId, voiceData, isEnd);
    }

    private void handleVoiceExceptionMessage(@NonNull VoiceMessageModel voiceMessage) {
        if (TextUtils.isEmpty(voiceMessage.getErrorCode())) {
            return;
        }
        TTSError targetError = TTSError.UNKNOWN_SPEAK_ERROR;
        for (TTSError error : TTSError.values()) {
            if (voiceMessage.getErrorCode().endsWith(error.getCode())) {
                targetError = error;
                break;
            }
        }
        TTSError finalTargetError = targetError;
        ThreadUtils.getInstance().runOnMainThread(() -> {
            if (speakingParam.speakCallback != null) {
                if (finalTargetError == TTSError.UNKNOWN_SPEAK_ERROR) {
                    speakingParam.speakCallback.onError(voiceMessage.getErrorCode(), voiceMessage.getErrorMsg());
                } else {
                    speakingParam.speakCallback.onError(finalTargetError.getCode(), finalTargetError.getMsg());
                }
            }
        });
    }

    private void writeAndPlayVoiceData(byte[] voiceData) {
        if (speakingParam.speakOption.getSpeakMode() == SpeakMode.PLAY_AUDIO
                || speakingParam.speakOption.getSpeakMode() == SpeakMode.PLAY_AUDIO_AND_WRITE_FILE) {
            if (TTSConfig.isDebug()) {
                Logger.i(TAG, " writeAndPlaySpeaking speakState : " + speakState);
            }
            Player.getInstance().write(voiceData);
            if (speakState == SpeakState.STOPPED) {
                Player.getInstance().play();
            }
        }
    }

    private void handleVoiceDataMap(String jobId, byte[] voiceData, boolean isEnd) {
        List<byte[]> voiceDataList = voiceDataMap.get(jobId);
        if (voiceDataList == null) {
            voiceDataList = new ArrayList<>();
            voiceDataList.add(voiceData);
            voiceDataMap.put(jobId, voiceDataList);
            ThreadUtils.getInstance().runOnMainThread(() -> {
                if (speakingParam.speakCallback != null) {
                    speakingParam.speakCallback.onBegin(jobId);
                }
            });
            ThreadUtils.getInstance().runOnMainThread(() -> {
                if (speakingParam.speakCallback != null) {
                    speakingParam.speakCallback.onSpeaking(Arrays.copyOf(voiceData, voiceData.length), jobId);
                }
            });
        } else {
            voiceDataList.add(voiceData);
            ThreadUtils.getInstance().runOnMainThread(() -> {
                if (speakingParam.speakCallback != null) {
                    speakingParam.speakCallback.onSpeaking(Arrays.copyOf(voiceData, voiceData.length), jobId);
                }
            });
            if (isEnd) {
                final byte[] allVoiceData = getAllVoiceDataByJobId(jobId);
                transVoiceDataToWavFile(allVoiceData);
                ThreadUtils.getInstance().runOnMainThread(() -> {
                    if (speakingParam.speakCallback != null) {
                        speakingParam.speakCallback.onFinish(allVoiceData, jobId);
                    }
                });
            }
        }
    }

    private void transVoiceDataToWavFile(byte[] allVoiceData) {
        if (speakingParam.speakOption.getSpeakMode() == SpeakMode.WRITE_FILE
                || speakingParam.speakOption.getSpeakMode() == SpeakMode.PLAY_AUDIO_AND_WRITE_FILE) {
            if (TextUtils.isEmpty(speakingParam.speakOption.getFilePath())) {
                Logger.e(TAG, " transVoiceDataToWavFile empty file name or file path , write to file stopped ");
            } else {
                DecodeUtils.toWavFile(speakingParam.speakOption.getFilePath(), allVoiceData);
            }
        }
    }

    private byte[] getAllVoiceDataByJobId(String jobId) {
        if (TextUtils.isEmpty(jobId)) {
            return new byte[0];
        }
        List<byte[]> voiceDataList = voiceDataMap.get(jobId);
        if (voiceDataList == null || voiceDataList.isEmpty()) {
            return new byte[0];
        }
        int totalLength = 0;
        for (byte[] bytes : voiceDataList) {
            totalLength += bytes.length;
        }

        byte[] allVoiceData = new byte[totalLength];
        int offset = 0;
        for (byte[] bytes : voiceDataList) {
            System.arraycopy(bytes, 0, allVoiceData, offset, bytes.length);
            offset += bytes.length;
        }
        return allVoiceData;
    }

    private void createWebSocket(String token, WebSocketListener webSocketListener) {
        String projectId = TTSConfig.getProjectId();
        String region = TTSConfig.getRegion().getValue();
        String url = String.format(Constants.URL.SPEAK, region, projectId);
        Request request = new Request.Builder()
                .url(url)
                .addHeader("X-Auth-Token", token)
                .get()
                .build();
        if (TTSConfig.isDebug()) {
            Logger.i(TAG, " createWebSocket url : " + url);
        } else {
            Logger.i(TAG, " createWebSocket url : " + (TextUtils.isEmpty(url) ? "0" : url.length()));
        }
        HttpClientManager.getInstance()
                .getClient()
                .newWebSocket(request, webSocketListener);
    }

    private String genJsonParams(String text) {
        SpeakRequestBodyModel speakRequestBodyModel = new SpeakRequestBodyModel();
        speakRequestBodyModel.setJobId(generateJobId());
        SpeakRequestBodyModel.ConfigModel configModel = new SpeakRequestBodyModel.ConfigModel();
        configModel.setSpeed(TTSConfig.getVoiceConfig().getSpeed());
        configModel.setPitch(TTSConfig.getVoiceConfig().getPitch());
        configModel.setVolume(TTSConfig.getVoiceConfig().getVolume());
        configModel.setVoiceAssetId(TTSConfig.getVoiceConfig().getVoiceAssetId());
        configModel.setSampleRate(String.valueOf(Constants.VOICE.SAMPLE_RATE));
        speakRequestBodyModel.setConfig(configModel);
        speakRequestBodyModel.setText(text);
        speakRequestBodyModel.setCommand(Command.START.getValue());
        return GsonUtil.toJson(speakRequestBodyModel);
    }

    private String generateJobId() {
        String randomString = StringUtil.generateRandomString(Constants.HTTP.JOB_REQUEST_RANDOM_STRING_LENGTH);
        return randomString + (new Date().getTime());
    }

    private void setStartSpeakingParam(@Nullable SpeakOption speakOption, @Nullable ISpeakCallback speakCallback) {
        if (speakOption == null) {
            speakingParam.speakOption = new SpeakOption();
        } else {
            speakingParam.speakOption = speakOption;
        }
        speakingParam.speakCallback = speakCallback;
    }

    static class SpeakingParam {
        SpeakOption speakOption = new SpeakOption();
        ISpeakCallback speakCallback = null;
    }

    public interface OnSpeakStateChangeListener {
        void onSpeakStateChanged(SpeakState speakState);
    }
}
