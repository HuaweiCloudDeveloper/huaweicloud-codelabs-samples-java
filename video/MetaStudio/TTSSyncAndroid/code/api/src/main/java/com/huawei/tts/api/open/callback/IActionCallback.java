package com.huawei.tts.api.open.callback;

public interface IActionCallback<T> {
    void onSuccess(T t);

    void onFailed(String errorCode, String errorMsg);
}
