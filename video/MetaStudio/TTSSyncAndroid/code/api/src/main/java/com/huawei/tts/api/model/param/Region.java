package com.huawei.tts.api.model.param;

/**
 * 站点信息。
 */
public enum Region {

    BEIJING("cn-north-4"),
    SHANGHAI("cn-east-3");

    private final String value;

    Region(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
