package com.huawei.tts.api.open.callback;

public interface ISpeakCallback {
    /**
     * 开始朗读的通知。
     *
     * @param speechGroupId 音频流的组Id。同一段文字的音频流组Id一致
     */
    void onBegin(String speechGroupId);

    /**
     * 正在接收朗读音频流的通知。注意：这个通知不是实际播放状态，仅代表音频流正在接收中。当收到音频流时，开始自动播放（如果配置了需要播放）。
     *
     * @param voiceData     音频流数据
     * @param speechGroupId 音频流的组Id。同一段文字的音频流组Id一致
     */
    void onSpeaking(byte[] voiceData, String speechGroupId);

    /**
     * 朗读音频流接收完毕的通知。注意：这个通知不是实际播放状态，仅代表音频流传输完毕。此时很可能依然在播放中（音频流尚未播放完毕）。
     *
     * @param allVoiceData  本段文字所有音频流数据
     * @param speechGroupId 音频流的组Id。同一段文字的音频流组Id一致
     */
    void onFinish(byte[] allVoiceData, String speechGroupId);

    /**
     * 朗读错误通知。警告：仅供调试参考，正式产品请重新封装错误码。
     *
     * @param errorCode 错误码
     * @param errorMsg  错误信息
     */
    void onError(String errorCode, String errorMsg);
}
