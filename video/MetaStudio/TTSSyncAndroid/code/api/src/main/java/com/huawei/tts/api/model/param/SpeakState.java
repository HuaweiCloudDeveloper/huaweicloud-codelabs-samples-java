package com.huawei.tts.api.model.param;

/**
 * 朗读状态
 */
public enum SpeakState {
    /**
     * 已停止
     */
    STOPPED(0),
    /**
     * 正在朗读
     */
    SPEAKING(1),
    /**
     * 已暂停
     */
    PAUSED(2);

    private final int value;

    SpeakState(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
