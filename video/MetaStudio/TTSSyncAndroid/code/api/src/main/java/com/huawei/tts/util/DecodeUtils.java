package com.huawei.tts.util;

import androidx.annotation.NonNull;

import com.huawei.tts.api.Logger;
import com.huawei.tts.consts.Constants;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;

public class DecodeUtils {

    private static final String TAG = DecodeUtils.class.getSimpleName();

    @NonNull
    public static byte[] decodeVoiceData(String voiceData) {
        try {
            return Base64.getDecoder().decode(voiceData);
        } catch (Exception e) {
            Logger.e(TAG, " decode error : " + e);
            return new byte[0];
        }
    }

    public static void toWavFile(String filePath, byte[] voiceData) {
        try (FileOutputStream fos = new FileOutputStream(filePath)) {
            fos.write(pcmToWav(voiceData));
        } catch (Exception e) {
            Logger.e(TAG, " toWavFile error : " + e);
        }
    }

    private static byte[] pcmToWav(byte[] pcmBytes) {
        return addHeader(pcmBytes, buildHeader(pcmBytes.length));
    }

    private static byte[] addHeader(byte[] pcmBytes, byte[] headerBytes) {
        byte[] result = new byte[44 + pcmBytes.length];
        if (headerBytes != null && headerBytes.length > 0) {
            System.arraycopy(headerBytes, 0, result, 0, 44);
        }
        System.arraycopy(pcmBytes, 0, result, 44, pcmBytes.length);
        return result;
    }

    private static byte[] buildHeader(Integer dataLength) {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            writeChar(bos, new char[]{'R', 'I', 'F', 'F'});
            writeInt(bos, dataLength + (44 - 8));
            writeChar(bos, new char[]{'W', 'A', 'V', 'E'});
            writeChar(bos, new char[]{'f', 'm', 't', ' '});
            writeInt(bos, Constants.VOICE.FMT_SIZE);
            writeShort(bos, Constants.VOICE.FORMAT_TAG);
            writeShort(bos, Constants.VOICE.CHANNELS);
            writeInt(bos, Constants.VOICE.SAMPLE_RATE);
            writeInt(bos, (short) (Constants.VOICE.CHANNELS * 2) * Constants.VOICE.SAMPLE_RATE);
            writeShort(bos, (short) (Constants.VOICE.CHANNELS * 2));
            writeShort(bos, Constants.VOICE.BITS_PER_SAMPLE);
            writeChar(bos, new char[]{'d', 'a', 't', 'a'});
            writeInt(bos, dataLength);
            return bos.toByteArray();
        } catch (IOException e) {
            Logger.e(TAG, " buildHeader error : " + e);
            return null;
        }
    }

    private static void writeShort(ByteArrayOutputStream bos, int s) throws IOException {
        byte[] arr = new byte[2];
        arr[1] = (byte) ((s << 16) >> 24);
        arr[0] = (byte) ((s << 24) >> 24);
        bos.write(arr);
    }

    private static void writeInt(ByteArrayOutputStream bos, int n) throws IOException {
        byte[] buf = new byte[4];
        buf[3] = (byte) (n >> 24);
        buf[2] = (byte) ((n << 8) >> 24);
        buf[1] = (byte) ((n << 16) >> 24);
        buf[0] = (byte) ((n << 24) >> 24);
        bos.write(buf);
    }

    private static void writeChar(ByteArrayOutputStream bos, char[] id) {
        for (char c : id) {
            bos.write(c);
        }
    }
}
