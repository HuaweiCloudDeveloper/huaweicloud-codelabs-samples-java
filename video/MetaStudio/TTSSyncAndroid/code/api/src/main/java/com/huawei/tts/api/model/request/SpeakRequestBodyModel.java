package com.huawei.tts.api.model.request;

import com.google.gson.annotations.SerializedName;

public class SpeakRequestBodyModel {

    @SerializedName("text")
    private String text;
    @SerializedName("command")
    private String command;
    @SerializedName("job_id")
    private String jobId;
    @SerializedName("config")
    private ConfigModel config;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public ConfigModel getConfig() {
        return config;
    }

    public void setConfig(ConfigModel config) {
        this.config = config;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public static class ConfigModel {
        @SerializedName("speed")
        private Integer speed;
        @SerializedName("pitch")
        private Integer pitch;
        @SerializedName("volume")
        private Integer volume;
        @SerializedName("sample_rate")
        private String sampleRate;
        @SerializedName("voice_asset_id")
        private String voiceAssetId;

        public Integer getSpeed() {
            return speed;
        }

        public void setSpeed(Integer speed) {
            this.speed = speed;
        }

        public Integer getPitch() {
            return pitch;
        }

        public void setPitch(Integer pitch) {
            this.pitch = pitch;
        }

        public Integer getVolume() {
            return volume;
        }

        public void setVolume(Integer volume) {
            this.volume = volume;
        }

        public String getSampleRate() {
            return sampleRate;
        }

        public void setSampleRate(String sampleRate) {
            this.sampleRate = sampleRate;
        }

        public String getVoiceAssetId() {
            return voiceAssetId;
        }

        public void setVoiceAssetId(String voiceAssetId) {
            this.voiceAssetId = voiceAssetId;
        }
    }
}
