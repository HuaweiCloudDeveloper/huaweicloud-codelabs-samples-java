package com.huawei.tts.api;

import android.annotation.SuppressLint;
import android.os.SystemClock;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.huawei.tts.api.model.config.AuthConfig;
import com.huawei.tts.api.model.error.TTSError;
import com.huawei.tts.api.model.request.AuthRequestBodyModel;
import com.huawei.tts.api.open.callback.IActionCallback;
import com.huawei.tts.consts.Constants;
import com.huawei.tts.net.HttpClientManager;
import com.huawei.tts.util.GsonUtil;

import java.io.IOException;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

final class Authenticator {

    private static final String TAG = Authenticator.class.getSimpleName();

    private String token;
    // token有效期（毫秒）
    private static final long TOKEN_VALIDITY_PERIOD = 24 * 3600 * 1000L;
    // token刷新间隔（毫秒）
    private static final long TOKEN_REFRESH_INTERVAL = (long) (TOKEN_VALIDITY_PERIOD * 0.7F);
    // token获取时间，取系统开机时间
    private long tokenObtainedTime;
    // token定时刷新器
    private Timer tokenRefreshTimer;

    private Authenticator() {
    }

    void getAuthToken(IActionCallback<String> callback) {
        // 外部注入了token，直接使用外部注入的token
        if (!TextUtils.isEmpty(TTSConfig.getAuthConfig().getToken())) {
            Logger.i(TAG, " getAuthToken use injected token ");
            callback.onSuccess(TTSConfig.getAuthConfig().getToken());
            return;
        }
        if (isTokenInvalid()) {
            Logger.i(TAG, " getAuthToken get new token ");
            getAuthTokenInter(callback);
        } else {
            Logger.i(TAG, " getAuthToken use previous token ");
            if (callback != null) {
                callback.onSuccess(token);
            }
        }
    }

    /**
     * token是否失效
     */
    private boolean isTokenInvalid() {
        // token为空
        if (TextUtils.isEmpty(token)) {
            return true;
        }
        // token过期
        long duration = SystemClock.elapsedRealtime() - tokenObtainedTime;
        if (duration <= 0) {
            token = null;
            return true;
        } else {
            return duration >= TOKEN_REFRESH_INTERVAL;
        }
    }

    @SuppressLint("CheckResult")
    private void getAuthTokenInter(@Nullable IActionCallback<String> callback) {
        String url = String.format(Constants.URL.AUTH, TTSConfig.getRegion().getValue());
        String jsonParams = genJsonParams();
        RequestBody requestBody = RequestBody.create(jsonParams, MediaType.parse(Constants.HTTP.CONTENT_TYPE_JSON));
        Request request = new Request.Builder()
                .addHeader(Constants.HTTP.CONTENT_TYPE_KEY, Constants.HTTP.CONTENT_TYPE_JSON)
                .url(url)
                .post(requestBody)
                .build();
        if (TTSConfig.isDebug()) {
            Logger.i(TAG, " getAuthTokenInter url : " + url + " , jsonParams : " + jsonParams);
        } else {
            Logger.i(TAG, " getAuthTokenInter url : " + (TextUtils.isEmpty(url) ? "0" : url.length()));
        }
        HttpClientManager.getInstance()
                .getClient()
                .newCall(request)
                .enqueue(new Callback() {
                    @Override
                    public void onFailure(@NonNull Call call, @NonNull IOException e) {
                        if (TTSConfig.isDebug()) {
                            Logger.i(TAG, " getAuthTokenInter onFailure : " + e);
                        } else {
                            Logger.i(TAG, " getAuthTokenInter onFailure ");
                        }
                        if (callback != null) {
                            callback.onFailed(TTSError.UNKNOWN_AUTH_ERROR.getCode(), e.toString());
                        }
                    }

                    @Override
                    public void onResponse(@NonNull Call call, @NonNull Response response) {
                        if (TTSConfig.isDebug()) {
                            Logger.i(TAG, " getAuthTokenInter onResponse : " + response);
                        } else {
                            Logger.i(TAG, " getAuthTokenInter onResponse : " + response.code());
                        }
                        // 状态码201表示token获取成功，从header中获取token
                        if (response.code() == 201 && response.headers().get(Constants.HTTP.RESPONSE_TOKEN_KEY) != null) {
                            token = response.headers().get(Constants.HTTP.RESPONSE_TOKEN_KEY);
                            tokenObtainedTime = SystemClock.elapsedRealtime();
                            // 添加token刷新定时器
                            startTokeRefreshTimer();
                            if (callback != null) {
                                callback.onSuccess(token);
                            }
                        } else {
                            if (callback != null) {
                                if (TextUtils.equals(TTSError.AUTH_ERROR_WRONG_USERNAME_OR_PASSWORD.getCode(), String.valueOf(response.code()))) {
                                    callback.onFailed(TTSError.AUTH_ERROR_WRONG_USERNAME_OR_PASSWORD.getCode(), TTSError.AUTH_ERROR_WRONG_USERNAME_OR_PASSWORD.getMsg());
                                } else {
                                    callback.onFailed(String.valueOf(response.code()), TTSError.UNKNOWN_AUTH_ERROR.getMsg());
                                }
                            }
                        }
                        response.close();
                    }
                });
    }

    private void startTokeRefreshTimer() {
        if (tokenRefreshTimer != null) {
            tokenRefreshTimer.cancel();
            tokenRefreshTimer = null;
        }
        Logger.i(TAG, " startTokeRefreshTimer ");
        tokenRefreshTimer = new Timer("AUTH_TOKEN_TIMER");
        tokenRefreshTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Logger.i(TAG, " refresh token ");
                getAuthTokenInter(null);
            }
        }, TOKEN_REFRESH_INTERVAL);
    }

    private static String genJsonParams() {
        AuthRequestBodyModel authTokenRequestModel = new AuthRequestBodyModel();
        AuthRequestBodyModel.AuthModel authModel = new AuthRequestBodyModel.AuthModel();
        authTokenRequestModel.setAuth(authModel);
        AuthRequestBodyModel.AuthModel.IdentityModel identityModel = new AuthRequestBodyModel.AuthModel.IdentityModel();
        identityModel.setMethods(Collections.singletonList("password"));
        AuthRequestBodyModel.AuthModel.IdentityModel.PasswordModel passwordModel = new AuthRequestBodyModel.AuthModel.IdentityModel.PasswordModel();
        AuthRequestBodyModel.AuthModel.IdentityModel.PasswordModel.UserModel userModel = new AuthRequestBodyModel.AuthModel.IdentityModel.PasswordModel.UserModel();
        AuthRequestBodyModel.AuthModel.IdentityModel.PasswordModel.UserModel.DomainModel domainModel = new AuthRequestBodyModel.AuthModel.IdentityModel.PasswordModel.UserModel.DomainModel();
        AuthConfig authConfig = TTSConfig.getAuthConfig();
        domainModel.setName(authConfig.getDomain());
        userModel.setName(authConfig.getUsername());
        userModel.setPassword(authConfig.getPassword());
        userModel.setDomain(domainModel);
        passwordModel.setUser(userModel);
        identityModel.setPassword(passwordModel);
        authModel.setIdentity(identityModel);
        AuthRequestBodyModel.AuthModel.ScopeModel scopeModel = new AuthRequestBodyModel.AuthModel.ScopeModel();
        AuthRequestBodyModel.AuthModel.ScopeModel.ProjectModel projectModel = new AuthRequestBodyModel.AuthModel.ScopeModel.ProjectModel();
        projectModel.setName(TTSConfig.getRegion().getValue());
        scopeModel.setProject(projectModel);
        authModel.setScope(scopeModel);
        return GsonUtil.toJson(authTokenRequestModel);
    }

    static Authenticator getInstance() {
        return Authenticator.SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        private static final Authenticator INSTANCE = new Authenticator();
    }
}
