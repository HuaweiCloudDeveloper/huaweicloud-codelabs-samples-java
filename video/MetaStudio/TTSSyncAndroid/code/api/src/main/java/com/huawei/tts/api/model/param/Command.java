package com.huawei.tts.api.model.param;

/**
 * WebSocket执行命令。
 */
public enum Command {

    START("START"),
    PRELOAD("PRELOAD"),
    HEARTBEAT("HEARTBEAT");

    private final String value;

    Command(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
