package com.huawei.tts.api.open;

import com.huawei.tts.api.model.config.AuthConfig;
import com.huawei.tts.api.model.config.VoiceConfig;
import com.huawei.tts.api.model.param.Region;

public interface IConfigurator {

    /**
     * 设置是否为调试模式，调试模式下会额外打印调试日志（开启日志打印的前提下），并且会关闭CA证书校验。警告：正式产品禁止设置为调试模式。
     *
     * @param isDebug 是否为调试模式
     */
    IConfigurator setDebug(boolean isDebug);

    /**
     * 设置是否开启日志打印。警告：如果是调试模式，会直接打印未脱敏的信息。
     *
     * @param isLogPrintingEnabled 是否开启日志打印
     */
    IConfigurator setLogPrintingEnabled(boolean isLogPrintingEnabled);

    /**
     * 注入日志打印器，如果未注入，则使用默认日志打印器（Android控制台logcat打印）。
     *
     * @param logger 注入的日志打印器
     */
    IConfigurator setLogger(ILogger logger);

    /**
     * 设置站点。
     *
     * @param region 站点
     */
    IConfigurator setRegion(Region region);

    /**
     * 设置项目Id。
     *
     * @param projectId 项目Id
     */
    IConfigurator setProjectId(String projectId);

    /**
     * 设置鉴权参数。
     *
     * @param authConfig 鉴权参数。警告：正式产品禁止将鉴权参数编码到端侧代码中，应当通过服务端下发鉴权参数，端侧再使用鉴权参数
     */
    IConfigurator setAuthConfig(AuthConfig authConfig);

    /**
     * 设置语音参数。
     *
     * @param config 语音参数。建议使用默认值
     */
    IConfigurator setVoiceConfig(VoiceConfig config);
}
