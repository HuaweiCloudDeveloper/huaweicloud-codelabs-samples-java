package com.huawei.tts.api;

import com.huawei.tts.api.open.IConfigurator;
import com.huawei.tts.api.open.ISpeaker;

public class TTSSdk {
    private static IConfigurator iConfigurator;

    private static ISpeaker iSpeaker;

    public synchronized static IConfigurator getConfigurator() {
        if (iConfigurator == null) {
            iConfigurator = new Configurator();
        }
        return iConfigurator;
    }

    public synchronized static ISpeaker getSpeaker() {
        if (iSpeaker == null) {
            iSpeaker = new Speaker();
        }
        return iSpeaker;
    }
}
