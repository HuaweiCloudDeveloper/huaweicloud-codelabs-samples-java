package com.huawei.tts;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.huawei.config.SdkConfig;
import com.huawei.tts.api.Logger;
import com.huawei.tts.api.Speaker;
import com.huawei.tts.api.TTSSdk;
import com.huawei.tts.api.model.param.SpeakMode;
import com.huawei.tts.api.model.param.SpeakOption;
import com.huawei.tts.api.model.param.SpeakState;
import com.huawei.tts.api.open.callback.ISpeakCallback;
import com.huawei.tts.demo.R;

import java.io.File;
import java.util.Date;


public class MainActivity extends FragmentActivity implements Speaker.OnSpeakStateChangeListener {

    private static final int REQUEST_WRITE_STORAGE_CODE = 1000;

    private static final String TAG = MainActivity.class.getSimpleName();
    private boolean isLoading;
    private SpeakState speakState = SpeakState.STOPPED;
    private View btnSetting;
    private TextView tvArticle;
    private View btnChangeArticle;
    private TextView tvChangeVoice;
    private View btnChangeVoice;
    private View ivStartSpeaking;
    private View pbStartSpeaking;
    private View btnStartSpeaking;
    private View btnStopSpeaking;
    private ImageView ivPauseSpeaking;
    private TextView tvPauseSpeaking;
    private View btnPauseSpeaking;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        requestPermission();
    }

    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE_CODE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initChangeVoiceText();
        initArticleText();
    }

    private void initViews() {
        TTSSdk.getSpeaker().addOnSpeakStateChangeListener(this);
        initSettingIv();
        initArticleTv();
        initChangeArticleBtn();
        initChangeVoiceBtn();
        initStartSpeakingBtn();
        initPauseSpeakingBtn();
        initStopSpeakingBtn();
    }

    private void initSettingIv() {
        btnSetting = findViewById(R.id.iv_setting);
        btnSetting.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, SettingActivity.class);
            startActivity(intent);
        });
    }

    private void initArticleTv() {
        tvArticle = findViewById(R.id.tv_article);
        initArticleText();
    }

    private void initArticleText() {
        if (tvArticle != null) {
            tvArticle.setText(SdkConfig.getInstance().getArticle());
        }
    }

    private void initChangeArticleBtn() {
        btnChangeArticle = findViewById(R.id.btn_change_article);
        btnChangeArticle.setOnClickListener(view -> handleChangeArticleBtnClicked());
    }

    private void initChangeVoiceBtn() {
        tvChangeVoice = findViewById(R.id.tv_change_voice);
        btnChangeVoice = findViewById(R.id.btn_change_voice);
        initChangeVoiceText();
        btnChangeVoice.setOnClickListener(view -> handleVoiceBtnClicked());
    }

    private void initChangeVoiceText() {
        if (tvChangeVoice != null) {
            tvChangeVoice.setText(SdkConfig.getInstance().getVoiceAssetName());
        }
    }

    private void initPauseSpeakingBtn() {
        ivPauseSpeaking = findViewById(R.id.iv_pause_speaking);
        tvPauseSpeaking = findViewById(R.id.tv_pause_speaking);
        btnPauseSpeaking = findViewById(R.id.btn_pause_speaking);
        btnPauseSpeaking.setOnClickListener(v -> {
            SpeakState speakState = TTSSdk.getSpeaker().getSpeakState();
            if (speakState == SpeakState.SPEAKING) {
                TTSSdk.getSpeaker().pauseSpeaking();
            } else if (speakState == SpeakState.PAUSED) {
                TTSSdk.getSpeaker().resumeSpeaking();
            }
        });
    }

    private void initStopSpeakingBtn() {
        btnStopSpeaking = findViewById(R.id.btn_stop_speaking);
        btnStopSpeaking.setOnClickListener(v -> {
            TTSSdk.getSpeaker().stopSpeaking();
        });
    }

    private void initStartSpeakingBtn() {
        ivStartSpeaking = findViewById(R.id.iv_start_speaking);
        pbStartSpeaking = findViewById(R.id.pb_start_speaking);
        btnStartSpeaking = findViewById(R.id.btn_start_speaking);
        btnStartSpeaking.setOnClickListener(view -> handleStartSpeakingBtnClicked());
    }

    private void handleStartSpeakingBtnClicked() {
        isLoading = true;
        refreshBtnStatus();
        SpeakOption speakOption;
        if (SdkConfig.getInstance().isWriteAudioFile()) {
            String filePath = Environment.getExternalStorageDirectory().getPath() + File.separator + new Date().getTime() + ".wav";
            speakOption = new SpeakOption();
            speakOption.setSpeakMode(SpeakMode.PLAY_AUDIO_AND_WRITE_FILE);
            speakOption.setFilePath(filePath);
        } else {
            speakOption = new SpeakOption();
        }
        TTSSdk.getSpeaker().startSpeaking(SdkConfig.getInstance().getArticle(), speakOption, new ISpeakCallback() {
            @Override
            public void onBegin(String speechGroupId) {
                Logger.i(TAG, " onBegin speechGroupId : " + speechGroupId);
                isLoading = false;
                refreshBtnStatus();
            }

            @Override
            public void onSpeaking(byte[] voiceData, String speechGroupId) {
                Logger.i(TAG, " onSpeaking speechGroupId : " + speechGroupId + " , voiceData : " + voiceData.length);
            }

            @Override
            public void onFinish(byte[] allVoiceData, String speechGroupId) {
                Logger.i(TAG, " onFinish speechGroupId : " + speechGroupId + " , allVoiceData : " + allVoiceData.length);
                isLoading = false;
                refreshBtnStatus();
                Toast.makeText(MainActivity.this, "音频文件写入完成 ", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(String errorCode, String errorMsg) {
                Logger.i(TAG, " onError errorCode : " + errorCode + " , errorMsg : " + errorMsg);
                isLoading = false;
                refreshBtnStatus();
                Toast.makeText(MainActivity.this, ("ERROR [ " + errorCode + " : " + errorMsg + " ] "), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void handleChangeArticleBtnClicked() {
        SdkConfig.getInstance().articleIndexIncrement();
        initArticleText();
    }

    private void handleVoiceBtnClicked() {
        SdkConfig.getInstance().voiceAssetIdIndexIncrement();
        SdkConfig.getInstance().takeEffect();
        initChangeVoiceText();
    }

    @Override
    public void onSpeakStateChanged(SpeakState speakState) {
        this.speakState = speakState;
        refreshBtnStatus();
    }


    private void refreshBtnStatus() {
        if (speakState == SpeakState.SPEAKING) {
            btnStartSpeaking.setVisibility(View.GONE);
            btnStopSpeaking.setVisibility(View.VISIBLE);
            btnPauseSpeaking.setVisibility(View.VISIBLE);
            ivPauseSpeaking.setImageResource(R.mipmap.ic_pause);
            tvPauseSpeaking.setText("暂停朗读");
            btnChangeArticle.setEnabled(false);
            btnChangeVoice.setEnabled(false);
            btnSetting.setEnabled(false);
        } else if (speakState == SpeakState.PAUSED) {
            btnStartSpeaking.setVisibility(View.GONE);
            btnStopSpeaking.setVisibility(View.VISIBLE);
            btnPauseSpeaking.setVisibility(View.VISIBLE);
            ivPauseSpeaking.setImageResource(R.mipmap.ic_resume);
            tvPauseSpeaking.setText("恢复朗读");
            btnChangeArticle.setEnabled(false);
            btnChangeVoice.setEnabled(false);
            btnSetting.setEnabled(false);
        } else {
            btnStartSpeaking.setVisibility(View.VISIBLE);
            pbStartSpeaking.setVisibility(isLoading ? View.VISIBLE : View.GONE);
            ivStartSpeaking.setVisibility(isLoading ? View.GONE : View.VISIBLE);
            btnStartSpeaking.setEnabled(!isLoading);
            btnStopSpeaking.setVisibility(View.GONE);
            btnPauseSpeaking.setVisibility(View.GONE);
            btnChangeArticle.setEnabled(!isLoading);
            btnChangeVoice.setEnabled(!isLoading);
            btnSetting.setEnabled(!isLoading);
        }
    }
}
