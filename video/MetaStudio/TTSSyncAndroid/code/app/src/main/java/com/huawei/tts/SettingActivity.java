package com.huawei.tts;

import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.huawei.adapter.SeekBarAdapter;
import com.huawei.config.SdkConfig;
import com.huawei.tts.demo.R;

public class SettingActivity extends FragmentActivity {

    private Switch swWriteAudioFile;
    private Switch swIsDebug;
    private Switch swIsLogPrintingEnabled;
    private SeekBar sbVoiceVolume;
    private TextView tvVoiceVolume;
    private SeekBar sbVoicePitch;
    private TextView tvVoicePitch;
    private SeekBar sbVoiceSpeed;
    private TextView tvVoiceSpeed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        initViews();
    }

    @Override
    protected void onStop() {
        SdkConfig.getInstance().takeEffect();
        super.onStop();
    }

    private void initViews() {
        initWriteToFileSw();
        initDebugSw();
        initLogSw();
        initVolumeSb();
        initPitchSb();
        initSpeedSb();
    }

    private void initWriteToFileSw() {
        swWriteAudioFile = findViewById(R.id.sw_write_audio_file);
        swWriteAudioFile.setChecked(SdkConfig.getInstance().isWriteAudioFile());
        swWriteAudioFile.setOnCheckedChangeListener((buttonView, isChecked) -> SdkConfig.getInstance().setWriteAudioFile(isChecked));
    }

    private void initSpeedSb() {
        sbVoiceSpeed = findViewById(R.id.sb_voice_speed);
        tvVoiceSpeed = findViewById(R.id.tv_voice_speed);
        sbVoiceSpeed.setMin(50);
        sbVoiceSpeed.setMax(200);
        sbVoiceSpeed.setProgress(SdkConfig.getInstance().getSpeed());
        tvVoiceSpeed.setText(String.valueOf(SdkConfig.getInstance().getSpeed()));
        sbVoiceSpeed.setOnSeekBarChangeListener(new SeekBarAdapter() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                SdkConfig.getInstance().setSpeed(progress);
                tvVoiceSpeed.setText(String.valueOf(SdkConfig.getInstance().getSpeed()));
            }
        });
    }

    private void initPitchSb() {
        sbVoicePitch = findViewById(R.id.sb_voice_pitch);
        tvVoicePitch = findViewById(R.id.tv_voice_pitch);
        sbVoicePitch.setMin(50);
        sbVoicePitch.setMax(200);
        sbVoicePitch.setProgress(SdkConfig.getInstance().getPitch());
        tvVoicePitch.setText(String.valueOf(SdkConfig.getInstance().getPitch()));
        sbVoicePitch.setOnSeekBarChangeListener(new SeekBarAdapter() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                SdkConfig.getInstance().setPitch(progress);
                tvVoicePitch.setText(String.valueOf(SdkConfig.getInstance().getPitch()));
            }
        });
    }

    private void initVolumeSb() {
        sbVoiceVolume = findViewById(R.id.sb_voice_volume);
        tvVoiceVolume = findViewById(R.id.tv_voice_volume);
        sbVoiceVolume.setMin(90);
        sbVoiceVolume.setMax(240);
        sbVoiceVolume.setProgress(SdkConfig.getInstance().getVolume());
        tvVoiceVolume.setText(String.valueOf(SdkConfig.getInstance().getVolume()));
        sbVoiceVolume.setOnSeekBarChangeListener(new SeekBarAdapter() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                SdkConfig.getInstance().setVolume(progress);
                tvVoiceVolume.setText(String.valueOf(SdkConfig.getInstance().getVolume()));
            }
        });
    }

    private void initLogSw() {
        swIsLogPrintingEnabled = findViewById(R.id.sw_log);
        swIsLogPrintingEnabled.setChecked(SdkConfig.getInstance().isLogPrintingEnabled());
        swIsLogPrintingEnabled.setOnCheckedChangeListener((buttonView, isChecked) -> SdkConfig.getInstance().setLogPrintingEnabled(isChecked));
    }

    private void initDebugSw() {
        swIsDebug = findViewById(R.id.sw_debug);
        swIsDebug.setChecked(SdkConfig.getInstance().isDebug());
        swIsDebug.setOnCheckedChangeListener((buttonView, isChecked) -> SdkConfig.getInstance().setDebug(isChecked));
    }
}