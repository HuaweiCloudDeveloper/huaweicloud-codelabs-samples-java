package com.huawei.config;

import com.huawei.data.TestData;
import com.huawei.tts.api.TTSSdk;
import com.huawei.tts.api.model.config.AuthConfig;
import com.huawei.tts.api.model.config.VoiceConfig;
import com.huawei.tts.api.model.param.Region;

public class SdkConfig {

    private boolean writeAudioFile = true;

    private boolean isDebug = true;

    private boolean isLogPrintingEnabled = true;

    private int speed = 100;

    private int pitch = 100;

    private int volume = 140;

    /**
     * 内部字段，文章索引
     */
    private int articleIndex = 0;
    /**
     * 内部字段，音色资产索引
     */
    private int voiceAssetIdIndex = 0;

    public boolean isWriteAudioFile() {
        return writeAudioFile;
    }

    public void setWriteAudioFile(boolean writeAudioFile) {
        this.writeAudioFile = writeAudioFile;
    }

    public boolean isDebug() {
        return isDebug;
    }

    public void setDebug(boolean debug) {
        isDebug = debug;
    }

    public boolean isLogPrintingEnabled() {
        return isLogPrintingEnabled;
    }

    public void setLogPrintingEnabled(boolean logPrintingEnabled) {
        isLogPrintingEnabled = logPrintingEnabled;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getPitch() {
        return pitch;
    }

    public void setPitch(int pitch) {
        this.pitch = pitch;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public String getVoiceAssetName() {
        return TestData.UTIL.getVoiceAsset(TestData.USER.VOICE_ASSETS, voiceAssetIdIndex).getKey();
    }

    public String getVoiceAssetId() {
        return TestData.UTIL.getVoiceAsset(TestData.USER.VOICE_ASSETS, voiceAssetIdIndex).getValue();
    }

    public void voiceAssetIdIndexIncrement() {
        voiceAssetIdIndex++;
        int totalVoiceAssetCount = TestData.USER.VOICE_ASSETS.size();
        if (voiceAssetIdIndex >= totalVoiceAssetCount) {
            voiceAssetIdIndex = 0;
        }
    }

    public String getArticle() {
        return TestData.ARTICLES.get(articleIndex);
    }

    public void articleIndexIncrement() {
        articleIndex++;
        int totalVoiceAssetCount = TestData.ARTICLES.size();
        if (articleIndex >= totalVoiceAssetCount) {
            articleIndex = 0;
        }
    }

    public void takeEffect() {
        AuthConfig authConfig = new AuthConfig()
                .setUsername(TestData.USER.USERNAME)
                .setPassword(TestData.USER.PASSWORD)
                .setDomain(TestData.USER.DOMAIN)
                .setToken("");// 根据需要填入token

        VoiceConfig voiceConfig = new VoiceConfig()
                .setSpeed(speed)
                .setPitch(pitch)
                .setVolume(volume)
                .setVoiceAssetId(getVoiceAssetId());

        TTSSdk.getConfigurator()
                .setDebug(isDebug)
                .setLogPrintingEnabled(isLogPrintingEnabled)
                .setRegion(Region.SHANGHAI)
                .setProjectId(TestData.USER.PROJECT_ID)
                .setAuthConfig(authConfig)
                .setVoiceConfig(voiceConfig);
    }

    private SdkConfig() {
    }

    public static SdkConfig getInstance() {
        return SdkConfig.SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        private static final SdkConfig INSTANCE = new SdkConfig();
    }

}
