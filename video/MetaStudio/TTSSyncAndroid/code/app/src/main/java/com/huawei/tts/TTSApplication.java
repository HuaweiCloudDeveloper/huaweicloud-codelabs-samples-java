package com.huawei.tts;

import android.annotation.SuppressLint;
import android.app.Application;

import com.huawei.config.SdkConfig;

public class TTSApplication extends Application {

    @SuppressLint("IfLackElseCheck")
    @Override
    public void onCreate() {
        super.onCreate();
        SdkConfig.getInstance().takeEffect();
    }
}
