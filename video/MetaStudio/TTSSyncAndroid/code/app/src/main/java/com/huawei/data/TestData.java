package com.huawei.data;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class TestData {
    /**
     * 示例文本
     */
    public static final List<String> ARTICLES = Collections.unmodifiableList(
            Arrays.asList(
                    "黄山位于安徽省黄山市境内，是中国最著名的山脉之一。黄山以其四季常青的树木、壮丽的山水风光和独特的地貌特征而闻名。黄山的最高峰——云顶峰海拔1,864.8米，是黄山的核心部分。黄山的山势险峻，峰岭交错，四季常青的树木覆盖着山岭，给人一种神秘而壮丽的感觉。在黄山，你可以欣赏到壮丽的日出和日落，以及四季的美景。",
                    "三亚位于海南省三亚市，是中国最美的海滨城市之一。三亚以其美丽的海滩、清澈的海水和丰富的岛屿资源而闻名。三亚的著名景点有西沙岛、东沙岛、七星海滩、天涯海角等。三亚的海滩沙质细腻，水质清澈，阳光充足，是度假和游泳的理想之地。此外，三亚的岛屿还拥有丰富的海洋生物资源，是观海洋生物的天堂。",
                    "九寨沟位于四川省阿坝藏族羌族自治州，是中国最美的谷地之一。九寨沟以其清澈见底的湖泊、壮丽的山水风光和多样的生物资源而闻名。九寨沟的湖泊中有众多的鱼类和水生生物，湖泊周围的山岭上生长着四季常青的树木。九寨沟的山水风光独特，给人一种神秘而壮丽的感觉。在这里，你可以欣赏到壮丽的日出和日落，以及四季的美景。",
                    "张家界位于湖南省张家界市，是中国最著名的山脉之一。张家界以其壮丽的山水风光和独特的地貌特征而闻名。张家界的天生三桥是世界自然遗产之一，是张家界的标志性景点。张家界的山势险峻，峰岭交错，四季常青的树木覆盖着山岭，给人一种神秘而壮丽的感觉。在张家界，你可以欣赏到壮丽的日出和日落，以及四季的美景。",
                    "桂林位于广西省桂林市，是中国最美的山水风光之一。桂林以其独特的地貌特征和壮丽的山水风光而闻名。桂林的山岭上生长着四季常青的树木，山间河流蜿蜒曲折，形成了众多的山水间景。桂林的山水风光独特，给人一种神秘而壮丽的感觉。在这里，你可以欣赏到壮丽的日出和日落，以及四季的美景。",
                    "大理位于云南省大理白族自治州，是中国最美的古城之一。大理以其独特的地貌特征、丰富的历史文化和壮丽的山水风光而闻名。大理的古城墙、古建筑和古街巷是大理的特色。大理的山势险峻，峰岭交错，四季常青的树木覆盖着山岭，给人一种神秘而壮丽的感觉。在大理，你可以欣赏到壮丽的日出和日落，以及四季的美景。",
                    "西双版纳位于云南省西双版纳傣族自治州，是中国最美的热带雨林之一。西双版纳以其丰富的热带雨林资源和独特的地貌特征而闻名。西双版纳的热带雨林中有众多的植物和动物，雨林中还有一些原始森林。西双版纳的雨林风光独特，给人一种神秘而壮丽的感觉。在这里，你可以欣赏到壮丽的日出和日落，以及四季的美景。",
                    "海南琼崖岛位于海南省琼海市，是中国最美的岛屿之一。琼崖岛以其美丽的海滩、清澈的海水和丰富的岛屿资源而闻名。琼崖岛的著名景点有文昌海滩、琼海湾等。琼崖岛的海滩沙质细腻，水质清澈，阳光充足，是度假和游泳的理想之地。此外，琼崖岛的岛屿还拥有丰富的海洋生物资源，是观海洋生物的天堂。",
                    "天台山位于江西省宜春市，是中国最美的山脉之一。天台山以其独特的地貌特征和壮丽的山水风光而闻名。天台山的山势险峻，峰岭交错，四季常青的树木覆盖着山岭，给人一种神秘而壮丽的感觉。在天台山，你可以欣赏到壮丽的日出和日落，以及四季的美景。此外，天台山还拥有丰富的生物资源，是观鸟的天堂。",
                    "江西庐山位于江西省庐山市，是中国最著名的山脉之一。庐山以其独特的地貌特征和壮丽的山水风光而闻名。庐山的山势险峻，峰岭交错，四季常青的树木覆盖着山岭，给人一种神秘而壮丽的感觉。在庐山，你可以欣赏到壮丽的日出和日落，以及四季的美景。此外，庐山还拥有丰富的生物资源，是观鸟的天堂。"
            )
    );

    /**
     * 调试的鉴权参数。警告：正式产品禁止将鉴权参数编码到端侧代码中，应当通过服务端下发鉴权参数，端侧再使用鉴权参数。
     */
    public static final class USER {

        public static final String USERNAME = "username";
        public static final String PASSWORD = "password";
        public static final String DOMAIN = "domian";

        public static final Map<String, String> VOICE_ASSETS = Collections.unmodifiableMap(
                new TreeMap<String, String>() {
                    {
                        put("亲切女声", "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                        put("闲聊女声", "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                        put("悬疑女声", "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                        put("幽默女声", "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                        put("自然女声", "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                        put("阳光男声", "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                        put("悬疑男声", "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                    }
                }

        );

        public static final String PROJECT_ID = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
    }

    public static class UTIL {
        public static Map.Entry<String, String> getVoiceAsset(Map<String, String> map, int index) {
            Set<Map.Entry<String, String>> entries = map.entrySet();
            int currentIndex = 0;
            Map.Entry<String, String> targetEntry = new Map.Entry<String, String>() {
                @Override
                public String getKey() {
                    return "";
                }

                @Override
                public String getValue() {
                    return "";
                }

                @Override
                public String setValue(String value) {
                    return "";
                }
            };
            for (Map.Entry<String, String> entry : entries) {
                if (currentIndex == index) {
                    targetEntry = entry;
                    break;
                }
                currentIndex++;
            }
            return targetEntry;
        }
    }
}
