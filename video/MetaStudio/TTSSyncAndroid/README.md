

## 1、功能介绍

华为云提供了语音同步合成WebSocket接口，可以将文本转化为语音，并实时返回合成的音频数据。本工程将WebSocket接口封装为安卓接口，方便安卓客户端调用（直接将源码中api模块导入工程）。

**您将学到什么？**

安卓应用如何调用语音同步合成WebSocket接口实现语音同步合成功能。



## 3、前置条件
- 1、已[注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=40c99ba3cea14417a2428c756a626599&lang=zh-cn)华为帐号并开通华为云，已进行[实名认证](https://support.huaweicloud.com/usermanual-account/zh-cn_topic_0077914254.html)。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、已获取账号对应的 Access Key（AK）和 Secret Access Key（SK）或IAM帐号及密码。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 4、已获取MetaStudio服务对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见[API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html)。 
- 5、本接口目前仅支持“华北-北京四”和“华东-上海一”区域使用，且必须[提交工单](https://console.huaweicloud.com/ticket/?region=cn-north-4#/ticketindex/createIndex)申请开通后才能使用。
- 6、使用接口前，需要在MetaStudio控制台服务概览页面，开通“声音合成”的按需计费。
  详细操作为：单击“声音合成”卡片中的“去开通”，在弹出的“开通按需计费服务提示”对话框中，勾选同意协议。单击“确定”，开通按需计费。



## 4、接口参数说明
主要接口参数的详细说明，具体可以参考源码：
```java
public interface ISpeaker {

    /**
     * 开始朗读，发声，不写入本地文件。注意：此接口不支持并行调用，只支持串行调用，必须在上一次调用结束（onFinish或onError通知）后再调用下一次。
     *
     * @param text     朗读的文字
     * @param callback 执行朗读的结果通知
     */
    void startSpeaking(String text, ISpeakCallback callback);
    
    /**
     * 开始朗读，可以自定义朗读选项：是否发声、是否写入本地文件。具体参考SpeakOption枚举。注意：此接口不支持并行调用，只支持串行调用，必须在上一次调用结束（onFinish或onError通知）后再调用下一次。
     *
     * @param text        朗读的文字
     * @param speakOption 朗读的选项
     * @param callback    执行朗读的结果通知
     */
    void startSpeaking(String text, SpeakOption speakOption, ISpeakCallback callback);
    
    /**
     * 停止朗读。
     */
    void stopSpeaking();
    
    /**
     * 暂停朗读。此时只暂停播放声音，不会停止接收服务端WebSocket的音频数据流。
     */
    void pauseSpeaking();
    
    /**
     * 恢复朗读。
     */
    void resumeSpeaking();
    
    /**
     * 获取当前朗读状态，具体参考SpeakState枚举。
     *
     * @return 朗读状态
     */
    SpeakState getSpeakState();
    
    /**
     * 添加朗读状态监听，当朗读状态发声变更时会会上报。
     *
     * @param listener 朗读状态监听
     */
    void addOnSpeakStateChangeListener(Speaker.OnSpeakStateChangeListener listener);
    
    /**
     * 移除朗读状态监听。
     *
     * @param listener 朗读状态监听
     */
    void removeOnSpeakStateChangeListener(Speaker.OnSpeakStateChangeListener listener);
}

public interface IConfigurator {

    /**
     * 设置是否为调试模式，调试模式下会额外打印调试日志（开启日志打印的前提下），并且会关闭CA证书校验。警告：正式产品禁止设置为调试模式。
     *
     * @param isDebug 是否为调试模式
     */
    IConfigurator setDebug(boolean isDebug);
    
    /**
     * 设置是否开启日志打印。警告：如果是调试模式，会直接打印未脱敏的信息。
     *
     * @param isLogPrintingEnabled 是否开启日志打印
     */
    IConfigurator setLogPrintingEnabled(boolean isLogPrintingEnabled);
    
    /**
     * 注入日志打印器，如果未注入，则使用默认日志打印器（Android控制台logcat打印）。
     *
     * @param logger 注入的日志打印器
     */
    IConfigurator setLogger(ILogger logger);
    
    /**
     * 设置站点。
     *
     * @param region 站点
     */
    IConfigurator setRegion(Region region);
    
    /**
     * 设置项目Id。
     *
     * @param projectId 项目Id
     */
    IConfigurator setProjectId(String projectId);
    
    /**
     * 设置鉴权参数。
     *
     * @param authConfig 鉴权参数。警告：正式产品禁止将鉴权参数编码到端侧代码中，应当通过服务端下发鉴权参数，端侧再使用鉴权参数
     */
    IConfigurator setAuthConfig(AuthConfig authConfig);
    
    /**
     * 设置语音参数。
     *
     * @param config 语音参数。建议使用默认值
     */
    IConfigurator setVoiceConfig(VoiceConfig config);
}
```

## 5、关键代码片段

### 5.1、初始化配置
```java
AuthConfig authConfig = new AuthConfig()
        .setUsername(TestData.USER.USERNAME)
        .setPassword(TestData.USER.PASSWORD)
        .setDomain(TestData.USER.DOMAIN)
        .setToken("");// 根据需要填入token

VoiceConfig voiceConfig = new VoiceConfig()
        .setSpeed(speed)
        .setPitch(pitch)
        .setVolume(volume)
        .setVoiceAssetId(getVoiceAssetId());

TTSSdk.getConfigurator()
        .setDebug(isDebug)
        .setLogPrintingEnabled(isLogPrintingEnabled)
        .setRegion(Region.SHANGHAI)
        .setProjectId(TestData.USER.PROJECT_ID)
        .setAuthConfig(authConfig)
        .setVoiceConfig(voiceConfig);
```

### 5.2、执行语音合成
```java
SpeakOption speakOption;
if (SdkConfig.getInstance().isWriteAudioFile()) {// 将音频写入本地文件
    String filePath = Environment.getExternalStorageDirectory().getPath() + File.separator + new Date().getTime() + ".wav";
    speakOption = new SpeakOption();
    speakOption.setSpeakMode(SpeakMode.PLAY_AUDIO_AND_WRITE_FILE);
    speakOption.setFilePath(filePath);
} else { // 仅执行语音合成
    speakOption = new SpeakOption();
}
TTSSdk.getSpeaker().startSpeaking(SdkConfig.getInstance().getArticle(), speakOption, new ISpeakCallback() {
    @Override
    public void onBegin(String speechGroupId) {
        Logger.i(TAG, " onBegin speechGroupId : " + speechGroupId);
    }

    @Override
    public void onSpeaking(byte[] voiceData, String speechGroupId) {
        Logger.i(TAG, " onSpeaking speechGroupId : " + speechGroupId + " , voiceData : " + voiceData.length);
    }

    @Override
    public void onFinish(byte[] allVoiceData, String speechGroupId) {
        Logger.i(TAG, " onFinish speechGroupId : " + speechGroupId + " , allVoiceData : " + allVoiceData.length);
    }

    @Override
    public void onError(String errorCode, String errorMsg) {
        Logger.i(TAG, " onError errorCode : " + errorCode + " , errorMsg : " + errorMsg);
    }
});
```

## 6、参考
本示例的代码工程仅用于简单演示，实际开发过程中应严格遵循开发指南。访问以下链接可以获取详细信息：[语音同步合成WebSocket接口](https://support.huaweicloud.com/api-metastudio/metastudio_02_0053.html)