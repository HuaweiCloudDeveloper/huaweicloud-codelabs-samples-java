package com.huawei.hwclouds.metastudio.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.utils.JsonUtils;
import com.huaweicloud.sdk.metastudio.v1.MetaStudioClient;
import com.huaweicloud.sdk.metastudio.v1.model.BackgroundConfigInfo;
import com.huaweicloud.sdk.metastudio.v1.model.BackgroundConfigInfo.BackgroundTypeEnum;
import com.huaweicloud.sdk.metastudio.v1.model.ChatCommandEnum;
import com.huaweicloud.sdk.metastudio.v1.model.ChatReqDataInfo;
import com.huaweicloud.sdk.metastudio.v1.model.ChatRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ChatResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ChatSubtitleConfig;
import com.huaweicloud.sdk.metastudio.v1.model.CreateActiveCodeReq;
import com.huaweicloud.sdk.metastudio.v1.model.CreateActiveCodeRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreateActiveCodeResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CreateDialogUrlReq;
import com.huaweicloud.sdk.metastudio.v1.model.CreateDialogUrlRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreateDialogUrlResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CreateOnceCodeRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreateOnceCodeResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CreateRobotReq;
import com.huaweicloud.sdk.metastudio.v1.model.CreateRobotRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreateRobotResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CreateSmartChatRoomReq;
import com.huaweicloud.sdk.metastudio.v1.model.CreateSmartChatRoomRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreateSmartChatRoomResponse;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteActiveCodeRequest;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteActiveCodeResponse;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteRobotRequest;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteRobotResponse;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteSmartChatRoomRequest;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteSmartChatRoomResponse;
import com.huaweicloud.sdk.metastudio.v1.model.DigitalAssetInfo;
import com.huaweicloud.sdk.metastudio.v1.model.DigitalAssetInfo.AssetTypeEnum;
import com.huaweicloud.sdk.metastudio.v1.model.LayerConfig;
import com.huaweicloud.sdk.metastudio.v1.model.LayerConfig.LayerTypeEnum;
import com.huaweicloud.sdk.metastudio.v1.model.LayerPositionConfig;
import com.huaweicloud.sdk.metastudio.v1.model.LayerSizeConfig;
import com.huaweicloud.sdk.metastudio.v1.model.ListActiveCodeRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListActiveCodeResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListAssetsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ListRobotRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListRobotResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ListSmartChatRoomsRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListSmartChatRoomsResponse;
import com.huaweicloud.sdk.metastudio.v1.model.RequestPayloadInfo;
import com.huaweicloud.sdk.metastudio.v1.model.ResetActiveCodeRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ResetActiveCodeResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ResponsePayloadInfo;
import com.huaweicloud.sdk.metastudio.v1.model.ShowActiveCodeRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ShowActiveCodeResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ShowRobotRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ShowRobotResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ShowSmartChatJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ShowSmartChatJobResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ShowSmartChatRoomRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ShowSmartChatRoomResponse;
import com.huaweicloud.sdk.metastudio.v1.model.StartSmartChatJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.StartSmartChatJobResponse;
import com.huaweicloud.sdk.metastudio.v1.model.StopSmartChatJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.StopSmartChatJobResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ThirdPartyModelConfig;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateActiveCodeReq;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateActiveCodeRequest;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateActiveCodeResponse;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateRobotReq;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateRobotRequest;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateRobotResponse;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateSmartChatRoomRequest;
import com.huaweicloud.sdk.metastudio.v1.model.UpdateSmartChatRoomResponse;
import com.huaweicloud.sdk.metastudio.v1.model.VideoConfig;
import com.huaweicloud.sdk.metastudio.v1.model.VideoConfig.CodecEnum;
import com.huaweicloud.sdk.metastudio.v1.model.VideoConfig.FrameRateEnum;
import com.huaweicloud.sdk.metastudio.v1.model.VoiceConfig;
import com.huaweicloud.sdk.metastudio.v1.region.MetaStudioRegion;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.Socket;
import java.net.URI;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509ExtendedTrustManager;

public class IntelligentInteractionDemo {

    private static final Logger logger = LoggerFactory.getLogger(IntelligentInteractionDemo.class);

    // 获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID和您的AK/SK。
    // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");

    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    private static final String PROJECT_ID = System.getenv("HUAWEICLOUD_SDK_PROJECT_ID");

    public static void main(String[] args) throws IOException, InterruptedException {
        ICredential auth = new BasicCredentials()
                .withAk(AK)
                .withSk(SK);

        // 使用默认配置
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        // 创建MetaStudio实例
        MetaStudioClient mClient = MetaStudioClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(MetaStudioRegion.CN_NORTH_4)
                .build();



        // 智能交互管理
        // 1. 创建应用
        String robotId = createRobot(mClient);
        // 2. 创建智能交互对话
        String roomId = createSmartChatRoom(mClient, robotId);
        // 3. 创建对话链接
        String url = createDialogUrl(mClient, roomId, robotId);
        // 4. 创建一次性鉴权码
        String onceCode = createOnceCode(mClient);

        // 方式2：基于LLM回调接口，支持自定义数字人大脑（替换创建应用步骤）
        createThirdPartyRobot(mClient, "http://xxx/llmUrl");

        // 方式3：基于Websocket接口，文本实时驱动数字人
        IntelligentInteractionWebSocketClient wsClient = null;
        try {
            // 3.1 请求建立WebSocket连接，获取chatId
            wsClient = initWebsocketClient("xxx", onceCode);

            // 3.2 发送文本驱动消息
            createTextDrive(wsClient);

            // 3.3 发送中断对话的消息
            createInterruptChat(wsClient);

            // 3.4 发送结束对话的消息
            createStopChat(wsClient);
        } finally {
            if (wsClient != null && wsClient.isOpen()) {
                wsClient.close();
            }
        }

        // 5. 查询应用列表
        listRobot(mClient);
        // 6. 查询智能交互对话列表
        listSmartChatRooms(mClient);
        // 7. 查询激活码列表
        String activeCodeId = listActiveCode(mClient, robotId);
        // 8. 删除激活码
        // 创建激活码只能创建一个，在create之前要先检查是否已经存在，。否则会创建失败
        if(activeCodeId.length() != 0) {
            deleteActiveCode(mClient, activeCodeId);
        }
        // 9. 创建激活码
        activeCodeId = createActiveCode(mClient, robotId, roomId);
        // 10. 查询激活码详情
        showActiveCode(mClient, activeCodeId);
        // 11. 修改激活码
        updateActiveCode(mClient, activeCodeId);
        // 12. 重置激活码
        resetActiveCode(mClient, activeCodeId);


        // 13. 启动数字人智能交互任务
        String jobId = startSmartChatJob(mClient, roomId, robotId);
        // 14. 启动数字人智能交互任务
        showSmartChatJob(mClient, jobId, roomId);
        // 15. 结束数字人智能交互任务
        stopSmartChatJob(mClient, jobId, roomId);
        // 16. 查询智能交互对话详情
        showSmartChatRoom(mClient, roomId);
        // 17. 更新智能交互对话信息
        updateSmartChatRoom(mClient, roomId, robotId);
        // 18. 删除智能交互对话
        // 删除之前要先停止任务，停止任务需要时间，停止结束才可以删除
        Thread.sleep(20000);
        deleteSmartChatRoom(mClient, roomId); // 这里要加延时，10s不够

        // 19. 查询应用详情
        showRobot(mClient, robotId);
        // 20. 修改应用
        updateRobot(mClient, robotId);
        // 21. 删除应用
        deleteRobot(mClient, robotId);
    }

    /**
     * 查询激活码列表
     *
     */
    private static String listActiveCode(MetaStudioClient client, String robotId) {
        logger.info("listActiveCode start");
        ListActiveCodeRequest request = new ListActiveCodeRequest()
                .withRobotId(robotId);
        String activeCodeId = "";
        try {
            ListActiveCodeResponse response = client.listActiveCode(request);
            logger.info("listActiveCode" + response.toString());
            activeCodeId = response.getData().get(0).getActiveCodeId();
        } catch (ClientRequestException e) {
            logger.error("listActiveCode ClientRequestException" + e.getHttpStatusCode());
            logger.error("listActiveCode ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("listActiveCode ServerResponseException" + e.getHttpStatusCode());
            logger.error("listActiveCode ServerResponseException" + e.getMessage());
        }
        return activeCodeId;
    }


    /**
     * 创建激活码
     *
     */
    private static String createActiveCode(MetaStudioClient client, String robotId, String roomId) {
        logger.info("createActiveCode start");
        CreateActiveCodeRequest request = new CreateActiveCodeRequest()
                .withBody(new CreateActiveCodeReq()
                        .withRobotId(robotId)
                        .withRoomId(roomId)
                        .withValidPeriod(3));
        String codeId = "";
        try {
            CreateActiveCodeResponse response = client.createActiveCode(request);
            logger.info("createActiveCode" + response.toString());
            codeId = response.getActiveCodeId();
        } catch (ClientRequestException e) {
            logger.error("createActiveCode ClientRequestException" + e.getHttpStatusCode());
            logger.error("createActiveCode ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("createActiveCode ServerResponseException" + e.getHttpStatusCode());
            logger.error("createActiveCode ServerResponseException" + e.getMessage());
        }
        return codeId;
    }


    /**
     * 查询激活码详情
     *
     */
    private static void showActiveCode(MetaStudioClient client, String activeCodeId) {
        logger.info("showActiveCode start");
        ShowActiveCodeRequest request = new ShowActiveCodeRequest()
                .withActiveCodeId(activeCodeId);
        try {
            ShowActiveCodeResponse response = client.showActiveCode(request);
            logger.info("showActiveCode" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showActiveCode ClientRequestException" + e.getHttpStatusCode());
            logger.error("showActiveCode ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showActiveCode ServerResponseException" + e.getHttpStatusCode());
            logger.error("showActiveCode ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 修改激活码
     *
     */
    private static void updateActiveCode(MetaStudioClient client, String activeCodeId) {
        logger.info("updateActiveCode start");
        UpdateActiveCodeRequest request = new UpdateActiveCodeRequest()
                .withActiveCodeId(activeCodeId)
                .withBody(new UpdateActiveCodeReq().withValidPeriod(10));
        try {
            UpdateActiveCodeResponse response = client.updateActiveCode(request);
            logger.info("updateActiveCode" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("updateActiveCode ClientRequestException" + e.getHttpStatusCode());
            logger.error("updateActiveCode ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("updateActiveCode ServerResponseException" + e.getHttpStatusCode());
            logger.error("updateActiveCode ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 重置激活码
     *
     */
    private static void resetActiveCode(MetaStudioClient client, String activeCodeId) {
        logger.info("resetActiveCode start");
        ResetActiveCodeRequest request = new ResetActiveCodeRequest()
                .withActiveCodeId(activeCodeId);

        try {
            ResetActiveCodeResponse response = client.resetActiveCode(request);
            logger.info("resetActiveCode" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("resetActiveCode ClientRequestException" + e.getHttpStatusCode());
            logger.error("resetActiveCode ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("resetActiveCode ServerResponseException" + e.getHttpStatusCode());
            logger.error("resetActiveCode ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 删除激活码
     *
     */
    private static void deleteActiveCode(MetaStudioClient client, String activeCodeId) {
        logger.info("deleteActiveCode start");
        List<String> list = new ArrayList<>();
        list.add(activeCodeId);
        DeleteActiveCodeRequest request = new DeleteActiveCodeRequest().withBody(list);
        try {
            DeleteActiveCodeResponse response = client.deleteActiveCode(request);
            logger.info("deleteActiveCode" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("deleteActiveCode ClientRequestException" + e.getHttpStatusCode());
            logger.error("deleteActiveCode ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("deleteActiveCode ServerResponseException" + e.getHttpStatusCode());
            logger.error("deleteActiveCode ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 创建对话链接
     *
     */
    public static String createDialogUrl(MetaStudioClient mClient, String roomId, String robotId) {
        logger.info("createDialogUrl start");
        String url = null;
        try {
            CreateDialogUrlRequest request = new CreateDialogUrlRequest();
            CreateDialogUrlReq body = new CreateDialogUrlReq();
            body.withRobotId(robotId);
            body.withRoomId(roomId);
            request.withBody(body);
            CreateDialogUrlResponse response = mClient.createDialogUrl(request);
            logger.info("createDialogUrl " + response.toString());
            url = response.getUrl();
        } catch (ClientRequestException e) {
            logger.error("createDialogUrl ClientRequestException " + e.getHttpStatusCode());
            logger.error("createDialogUrl ClientRequestException " + e);
        } catch (ServerResponseException e) {
            logger.error("createDialogUrl ServerResponseException " + e.getHttpStatusCode());
            logger.error("createDialogUrl ServerResponseException " + e.getMessage());
        }
        return url;
    }


    /**
     * 启动数字人智能交互任务
     *
     */
    private static String startSmartChatJob(MetaStudioClient client, String roomId, String robotId) {
        logger.info("startSmartChatJob start");
        StartSmartChatJobRequest request = new StartSmartChatJobRequest()
                .withRobotId(robotId)
                .withRoomId(roomId);
        String jobId = "";
        try {
            StartSmartChatJobResponse response = client.startSmartChatJob(request);
            logger.info("startSmartChatJob" + response.toString());
            jobId = response.getJobId();
        } catch (ClientRequestException e) {
            logger.error("startSmartChatJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("startSmartChatJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("startSmartChatJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("startSmartChatJob ServerResponseException" + e.getMessage());
        }
        return jobId;
    }


    /**
     * 结束数字人智能交互任务
     *
     */
    private static void stopSmartChatJob(MetaStudioClient client, String jobId, String roomId) {
        logger.info("stopSmartChatJob start");
        StopSmartChatJobRequest request = new StopSmartChatJobRequest()
                .withJobId(jobId)
                .withRoomId(roomId);
        try {
            StopSmartChatJobResponse response = client.stopSmartChatJob(request);
            logger.info("stopSmartChatJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("stopSmartChatJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("stopSmartChatJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("stopSmartChatJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("stopSmartChatJob ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 启动数字人智能交互任务
     *
     */
    private static void showSmartChatJob(MetaStudioClient client, String jobId, String roomId) {
        logger.info("showSmartChatJob start");
        ShowSmartChatJobRequest request = new ShowSmartChatJobRequest()
                .withJobId(jobId)
                .withRoomId(roomId);
        try {
            ShowSmartChatJobResponse response = client.showSmartChatJob(request);
            logger.info("showSmartChatJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showSmartChatJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("showSmartChatJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showSmartChatJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("showSmartChatJob ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 查询应用列表
     *
     */
    private static String listRobot(MetaStudioClient client) {
        logger.info("listRobot start");
        ListRobotRequest request = new ListRobotRequest();
        String robotId = "";
        try {
            ListRobotResponse response = client.listRobot(request);
            robotId = response.getData().get(0).getRobotId();
            logger.info("listRobot" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listRobot ClientRequestException" + e.getHttpStatusCode());
            logger.error("listRobot ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("listRobot ServerResponseException" + e.getHttpStatusCode());
            logger.error("listRobot ServerResponseException" + e.getMessage());
        }
        return robotId;
    }


    /**
     * 查询应用详情
     *
     */
    private static void showRobot(MetaStudioClient client, String robotId) {
        logger.info("showRobot start");
        ShowRobotRequest request = new ShowRobotRequest()
                .withRobotId(robotId);
        try {
            ShowRobotResponse response = client.showRobot(request);
            logger.info("showRobot" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showRobot ClientRequestException" + e.getHttpStatusCode());
            logger.error("showRobot ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showRobot ServerResponseException" + e.getHttpStatusCode());
            logger.error("showRobot ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 修改应用
     *
     */
    private static void updateRobot(MetaStudioClient client, String robotId) {
        logger.info("updateRobot start");
        UpdateRobotRequest request = new UpdateRobotRequest()
                .withRobotId(robotId)
                .withBody(new UpdateRobotReq());
        try {
            UpdateRobotResponse response = client.updateRobot(request);
            logger.info("updateRobot" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("updateRobot ClientRequestException" + e.getHttpStatusCode());
            logger.error("updateRobot ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("updateRobot ServerResponseException" + e.getHttpStatusCode());
            logger.error("updateRobot ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 删除应用
     *
     */
    private static void deleteRobot(MetaStudioClient client, String robotId) {
        logger.info("deleteRobot start");
        List<String> list = new ArrayList<>();
        list.add(robotId);
        DeleteRobotRequest request = new DeleteRobotRequest()
                .withBody(list);
        try {
            DeleteRobotResponse response = client.deleteRobot(request);
            logger.info("deleteRobot" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("deleteRobot ClientRequestException" + e.getHttpStatusCode());
            logger.error("deleteRobot ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("deleteRobot ServerResponseException" + e.getHttpStatusCode());
            logger.error("deleteRobot ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 查询智能交互对话列表
     *
     */
    private static String listSmartChatRooms(MetaStudioClient client) {
        logger.info("listSmartChatRooms start");
        ListSmartChatRoomsRequest request = new ListSmartChatRoomsRequest();
        String roomId = "";
        try {
            ListSmartChatRoomsResponse response = client.listSmartChatRooms(request);
            logger.info("listSmartChatRooms" + response.toString());
            roomId = response.getSmartChatRooms().get(0).getRoomId();
        } catch (ClientRequestException e) {
            logger.error("listSmartChatRooms ClientRequestException" + e.getHttpStatusCode());
            logger.error("listSmartChatRooms ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("listSmartChatRooms ServerResponseException" + e.getHttpStatusCode());
            logger.error("listSmartChatRooms ServerResponseException" + e.getMessage());
        }
        return roomId;
    }


    /**
     * 创建智能交互对话
     *
     */
    public static String createSmartChatRoom(MetaStudioClient mClient, String robotId) {
        logger.info("createSmartChatRoom start");
        String roomId = null;
        try {
            String modelAssetId = getAssetsId(mClient, AssetTypeEnum.HUMAN_MODEL);
            String videoConfigId = getAssetsId(mClient, AssetTypeEnum.VOICE_MODEL);
            DigitalAssetInfo digitalAssetInfo = getBackgroundAssets(mClient, AssetTypeEnum.IMAGE);
            List<LayerConfig> layerConfig = new ArrayList<>();
            layerConfig.add(new LayerConfig()
                    .withLayerType(LayerTypeEnum.HUMAN)
                    .withPosition(new LayerPositionConfig()
                            .withLayerIndex(1)
                            .withDx(260)
                            .withDy(10))
                    .withSize(new LayerSizeConfig()
                            .withWidth(600)
                            .withHeight(100)));
            CreateSmartChatRoomRequest request = new CreateSmartChatRoomRequest()
                    .withBody(new CreateSmartChatRoomReq()
                            .withRoomName("<your name>")
                            .withRoomDescription("")
                            .withVideoConfig(new VideoConfig()
                                    .withCodec(CodecEnum.H264)
                                    .withBitrate(3000)
                                    .withWidth(1920)
                                    .withHeight(1080)
                                    .withFrameRate(FrameRateEnum._25))
                            .withModelAssetId(modelAssetId)
                            .withVoiceConfig(new VoiceConfig()
                                    .withVoiceAssetId(videoConfigId)
                                    .withSpeed(100)
                                    .withPitch(100)
                                    .withVolume(140))
                            .withRobotId(robotId)
                            .withConcurrency(2)
                            .withBackgroundConfig(new BackgroundConfigInfo()
                                    .withBackgroundAssetId(digitalAssetInfo.getAssetId())
                                    .withBackgroundConfig(digitalAssetInfo.getFiles().get(0).getDownloadUrl())
                                    .withBackgroundType(BackgroundTypeEnum.IMAGE_2D))
                            .withLayerConfig(layerConfig)
                            .withChatSubtitleConfig(new ChatSubtitleConfig()
                                    .withDx(100)
                                    .withDy(96)
                                    .withWidth(800)
                                    .withHeight(887)));

            CreateSmartChatRoomResponse response = mClient.createSmartChatRoom(request);
            logger.info("createSmartChatRoom " + response.toString());
            roomId = response.getRoomId();
        } catch (ClientRequestException e) {
            logger.error("createSmartChatRoom ClientRequestException " + e.getHttpStatusCode());
            logger.error("createSmartChatRoom ClientRequestException " + e);
        } catch (ServerResponseException e) {
            logger.error("createSmartChatRoom ServerResponseException " + e.getHttpStatusCode());
            logger.error("createSmartChatRoom ServerResponseException " + e.getMessage());
        }
        return roomId;
    }


    /**
     * 查询智能交互对话详情
     *
     */
    private static void showSmartChatRoom(MetaStudioClient client, String roomId) {
        logger.info("showSmartChatRoom start");
        ShowSmartChatRoomRequest request = new ShowSmartChatRoomRequest()
                .withRoomId(roomId);
        try {
            ShowSmartChatRoomResponse response = client.showSmartChatRoom(request);
            logger.info("showSmartChatRoom" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showSmartChatRoom ClientRequestException" + e.getHttpStatusCode());
            logger.error("showSmartChatRoom ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showSmartChatRoom ServerResponseException" + e.getHttpStatusCode());
            logger.error("showSmartChatRoom ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 更新智能交互对话信息
     *
     */
    private static void updateSmartChatRoom(MetaStudioClient client, String roomId, String robotId) {
        logger.info("updateSmartChatRoom start");
        UpdateSmartChatRoomRequest request = new UpdateSmartChatRoomRequest()
                .withRoomId(roomId)
                .withBody(new CreateSmartChatRoomReq()
                        .withRoomName("<your name>")
                        .withRobotId(robotId));
        try {
            UpdateSmartChatRoomResponse response = client.updateSmartChatRoom(request);
            logger.info("updateSmartChatRoom" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("updateSmartChatRoom ClientRequestException" + e.getHttpStatusCode());
            logger.error("updateSmartChatRoom ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("updateSmartChatRoom ServerResponseException" + e.getHttpStatusCode());
            logger.error("updateSmartChatRoom ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 删除智能交互对话
     *
     */
    private static void deleteSmartChatRoom(MetaStudioClient client, String roomId) {
        logger.info("deleteSmartChatRoom start");
        DeleteSmartChatRoomRequest request = new DeleteSmartChatRoomRequest()
                .withRoomId(roomId);
        try {
            DeleteSmartChatRoomResponse response = client.deleteSmartChatRoom(request);
            logger.info("deleteSmartChatRoom" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("deleteSmartChatRoom ClientRequestException" + e.getHttpStatusCode());
            logger.error("deleteSmartChatRoom ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("deleteSmartChatRoom ServerResponseException" + e.getHttpStatusCode());
            logger.error("deleteSmartChatRoom ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 创建应用
     */
    public static String createRobot(MetaStudioClient mClient) {
        logger.info("createRobot start");
        String robotId = null;
        try {
            CreateRobotRequest request = new CreateRobotRequest();
            CreateRobotReq body = new CreateRobotReq();
            // 对接第三方应用厂商类型。
            // 0：科大讯飞AIUI；1：华为云CBS；2：科大讯飞星火交互认知大模型；5：第三方驱动（websocket接口驱动）；6：第三方语言模型
            body.withAppType(5);
            body.withName("<your name>");
            request.withBody(body);
            CreateRobotResponse response = mClient.createRobot(request);
            logger.info("createRobot " + response.toString());
            robotId = response.getRobotId();
        } catch (ClientRequestException e) {
            logger.error("createRobot ClientRequestException " + e.getHttpStatusCode());
            logger.error("createRobot ClientRequestException " + e);
        } catch (ServerResponseException e) {
            logger.error("createRobot ServerResponseException " + e.getHttpStatusCode());
            logger.error("createRobot ServerResponseException " + e.getMessage());
        }
        return robotId;
    }

    /**
     * 创建智能交互应用(配置自定义数字人大脑)
     */
    public static void createThirdPartyRobot(MetaStudioClient client, String llmUrl) {
        logger.info("createThirdPartyRobot start");
        try {
            CreateRobotRequest request = new CreateRobotRequest();
            CreateRobotReq body = new CreateRobotReq();
            ThirdPartyModelConfig thirdPartyModelConfigbody = new ThirdPartyModelConfig();
            thirdPartyModelConfigbody.withAppId("xxx")
                .withAppKey("xxx")
                .withLlmUrl(llmUrl)
                .withIsStream(true)
                .withChatRounds(1);
            body.withThirdPartyModelConfig(thirdPartyModelConfigbody);
            body.withAppType(6);
            body.withName("<your name>");
            request.withBody(body);
            CreateRobotResponse response = client.createRobot(request);
            logger.info("createThirdPartyRobot " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createThirdPartyRobot ClientRequestException " + e.getHttpStatusCode());
            logger.error("createThirdPartyRobot ClientRequestException " + e);
        } catch (ServerResponseException e) {
            logger.error("createThirdPartyRobot ServerResponseException " + e.getHttpStatusCode());
            logger.error("createThirdPartyRobot ServerResponseException " + e.getMessage());
        }
    }

    /**
     * 创建一次性鉴权码
     */
    public static String createOnceCode(MetaStudioClient mClient) {
        logger.info("createOnceCode start");
        CreateOnceCodeRequest request = new CreateOnceCodeRequest();
        String onceCode = null;
        try {
            CreateOnceCodeResponse response = mClient.createOnceCode(request);
            logger.info("createOnceCode " + response.toString());
            onceCode = response.getOnceCode();
        } catch (ClientRequestException e) {
            logger.error("createOnceCode ClientRequestException" + e.getHttpStatusCode());
            logger.error("createOnceCode ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("createOnceCode ServerResponseException" + e.getHttpStatusCode());
            logger.error("createOnceCode ServerResponseException" + e.getMessage());
        }
        return onceCode;
    }

    /**
     * 请求建立WebSocket连接，获取chatId
     */
    private static IntelligentInteractionWebSocketClient initWebsocketClient(String jobId, String onceCode) {
        logger.info("initWebsocketClient start");
        IntelligentInteractionWebSocketClient wsClient = null;
        try {
            // 创建MetaStudio websocket 实例
            String endpoint =
                "metastudio-api.cn-north-4.myhuaweicloud.com"; // region为上海一需写为：metastudio-api.cn-east-3.myhuaweicloud.com
            String path = String.format("/v1/%s/digital-human-chat/chat-command/%s", PROJECT_ID, jobId);
            String query = "token=" + onceCode;
            URI uri = new URI("wss", null, endpoint, 443, path, query, null);
            javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> true);
            wsClient = new IntelligentInteractionWebSocketClient(uri, jobId);
            wsClient.connectBlocking(5000, TimeUnit.MILLISECONDS);
            // 等候chatId返回
            String chatId =
                waitRespCommand(wsClient.getResponses(), ChatCommandEnum.START_CHAT, "showChatId").getChatId();
            wsClient.setChatId(chatId);
            logger.info("initWebsocketClient " + chatId);
        } catch (Throwable e) {
            logger.error("initWebsocketClient exception " + e.getMessage());
            if (wsClient != null && wsClient.isOpen()) {
                wsClient.close();
            }
        }
        return wsClient;
    }

    /**
     * 发送文本驱动消息
     */
    private static void createTextDrive(IntelligentInteractionWebSocketClient wsClient) {
        logger.info("createTextDrive start");
        try {
            ChatReqDataInfo data = new ChatReqDataInfo();
            data.setSeq(1);
            data.setText("您好，我是您的分身数字人");
            data.setIsLast(true);

            sendMessage(wsClient, ChatCommandEnum.TEXT_DRIVE, data);

            waitRespCommand(wsClient.getResponses(), ChatCommandEnum.TEXT_DRIVE_RSP, "createTextDrive");
            waitRespCommand(wsClient.getResponses(), ChatCommandEnum.START_SPEAKING, "createTextDrive");
            logger.info("createTextDrive end");
        } catch (Exception e) {
            logger.error("createTextDrive exception " + e.getMessage());
        }
    }

    /**
     * 发送中断对话的消息
     */
    private static void createInterruptChat(IntelligentInteractionWebSocketClient wsClient) {
        logger.info("createInterruptChat start");
        try {
            sendMessage(wsClient, ChatCommandEnum.INTERRUPT_CHAT, null);

            waitRespCommand(wsClient.getResponses(), ChatCommandEnum.INTERRUPT_CHAT_RSP, "createInterruptChat");
            waitRespCommand(wsClient.getResponses(), ChatCommandEnum.START_CHAT, "createInterruptChat");
            logger.info("createInterruptChat end");
        } catch (Exception e) {
            logger.error("createInterruptChat exception " + e.getMessage());
        }
    }

    /**
     * 发送结束对话的消息
     */
    private static void createStopChat(IntelligentInteractionWebSocketClient wsClient) {
        logger.info("createStopChat start");
        try {
            sendMessage(wsClient, ChatCommandEnum.STOP_CHAT, null);

            waitRespCommand(wsClient.getResponses(), ChatCommandEnum.STOP_CHAT_RSP, "createStopChat");
            logger.info("createStopChat end");
        } catch (Exception e) {
            logger.error("createStopChat exception " + e.getMessage());
        }
    }

    // 获取资产ID
    public static String getAssetsId(MetaStudioClient client, DigitalAssetInfo.AssetTypeEnum assetType) {
        logger.info("listAssets start");
        String assetsId = "";
        try {
            ListAssetsRequest listAssetsRequest = new ListAssetsRequest()
                    .withAssetType(String.valueOf(assetType));
            ListAssetsResponse response = client.listAssets(listAssetsRequest);
            logger.info("listAssets" + response.toString());
            assetsId = response.getAssets().get(0).getAssetId();
        } catch (ClientRequestException e) {
            logger.error("getAsset ClientRequestException" + e.getHttpStatusCode());
            logger.error("getAsset ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("getAsset ServerResponseException" + e.getHttpStatusCode());
            logger.error("getAsset ServerResponseException" + e.getMessage());
        }
        return assetsId;
    }

    // 获取背景图资产
    public static DigitalAssetInfo getBackgroundAssets(MetaStudioClient client, DigitalAssetInfo.AssetTypeEnum assetType) {
        logger.info("listAssets start");
        DigitalAssetInfo digitalAssetInfo = null;
        try {
            ListAssetsRequest listAssetsRequest = new ListAssetsRequest()
                    .withAssetType(String.valueOf(assetType))
                    .withSystemProperty("BACKGROUND_IMG");
            ListAssetsResponse response = client.listAssets(listAssetsRequest);
            logger.info("listAssets" + response.toString());
            digitalAssetInfo = response.getAssets().get(0);
        } catch (ClientRequestException e) {
            logger.error("getAsset ClientRequestException" + e.getHttpStatusCode());
            logger.error("getAsset ClientRequestException" + e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("getAsset ServerResponseException" + e.getHttpStatusCode());
            logger.error("getAsset ServerResponseException" + e.getMessage());
        }
        return digitalAssetInfo;
    }

    private static void sendMessage(IntelligentInteractionWebSocketClient wsClient, ChatCommandEnum command,
        ChatReqDataInfo data) {
        RequestPayloadInfo requestPayloadInfo = new RequestPayloadInfo();
        requestPayloadInfo.setJobId(wsClient.getJobId());
        requestPayloadInfo.setChatId(wsClient.getChatId());
        requestPayloadInfo.setCommand(command);
        requestPayloadInfo.setData(data);

        ChatRequest chatRequest = new ChatRequest();
        chatRequest.setPayload(requestPayloadInfo);

        wsClient.send(JsonUtils.toJSON(chatRequest));
    }

    private static ResponsePayloadInfo waitRespCommand(LinkedList<ChatResponse> respQueue, ChatCommandEnum command,
        String logPrefix) throws InterruptedException {
        String successCode = "MSS.00000000";
        while (true) {
            if (respQueue.peek() != null) {
                ChatResponse response = respQueue.pop();
                if (response.getErrorCode() == null || successCode.equals(response.getErrorCode())) {
                    if (response.getPayload() != null && response.getPayload().getCommand() == command) {
                        logger.info(logPrefix + " response " + JsonUtils.toJSON(response.getPayload()));
                        return response.getPayload();
                    }
                } else {
                    logger.error("response exception " + response.getErrorCode());
                    logger.error("response exception " + response.getErrorMsg());
                    return null;
                }
            } else {
                Thread.sleep(200);
            }
        }
    }

    public static class IntelligentInteractionWebSocketClient extends WebSocketClient {

        private static final Logger logger = LoggerFactory.getLogger(IntelligentInteractionWebSocketClient.class);

        private final String jobId;

        private final LinkedList<ChatResponse> responses;

        private String chatId;

        public IntelligentInteractionWebSocketClient(URI serverUri, String jobId) {
            super(serverUri);
            this.jobId = jobId;
            this.responses = new LinkedList<>();
            if (serverUri.toString().startsWith("wss")) {
                trustAllHosts(this);
            }
        }

        /**
         * 忽略证书
         *
         * @paramclient
         */
        static void trustAllHosts(IntelligentInteractionWebSocketClient client) {
            TrustManager[] trustAllCerts = new TrustManager[] {
                new X509ExtendedTrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] x509Certificates, String s, Socket socket) throws
                        CertificateException {

                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] x509Certificates, String s, Socket socket)
                        throws CertificateException {

                    }

                    @Override
                    public void checkClientTrusted(X509Certificate[] x509Certificates, String s, SSLEngine sslEngine)
                        throws CertificateException {

                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] x509Certificates, String s, SSLEngine sslEngine)
                        throws CertificateException {

                    }

                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    @Override
                    public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                        System.out.println("checkClientTrusted");
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                        System.out.println("checkServerTrusted");
                    }
                }
            };

            try {

                SSLContext ssl = SSLContext.getInstance("SSL");
                ssl.init(null, trustAllCerts, new java.security.SecureRandom());

                SSLSocketFactory socketFactory = ssl.getSocketFactory();
                client.setSocketFactory(socketFactory);
            } catch (Exception e) {
                logger.error("WebSocketClient trustAllHosts, exception: ", e);
            }
        }

        @Override
        public void onOpen(ServerHandshake handshake) {
            logger.info("WebSocketClient open, code {}", handshake.getHttpStatusMessage());
        }

        @Override
        public void onMessage(String msg) {
            logger.info("WebSocketClient receive resp. jobId {}, msg {}", jobId, msg);
            // 待进行转化
            responses.add(JsonUtils.toObject(msg, ChatResponse.class));
        }

        @Override
        public void onClose(int code, String reason, boolean remote) {
            String closeParam = MessageFormat.format("code {0}, reason {1}, remote {2}", code, reason, remote);
            logger.info("WebSocketClient onClose, jobId {}, {}", jobId, closeParam);
        }

        @Override
        public void onError(Exception e) {
            logger.error("WebSocketClient error, jobId {} exception: ", jobId, e);

        }

        public String getJobId() {
            return jobId;
        }

        public String getChatId() {
            return chatId;
        }

        public void setChatId(String chatId) {
            this.chatId = chatId;
        }

        public LinkedList<ChatResponse> getResponses() {
            return responses;
        }
    }

}
