package com.huaweicloud.sdk.metastudio.v1.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * 智能交互驱动返回结果
 **/
public class ChatResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty("error_code")
    private String errorCode = "MSS.00000000";

    @JsonProperty("error_msg")
    private String errorMsg = "success";

    @JsonProperty("request_id")
    private String requestId = null;

    @JsonProperty("payload")
    private ResponsePayloadInfo payload = null;

    /**
     * 返回码
     * minLength: 1
     * maxLength: 32
     **/
    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * 错误信息
     * minLength: 1
     * maxLength: 1024
     **/
    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    /**
     * 请求ID
     * minLength: 1
     * maxLength: 64
     **/
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * 对话响应payload数据信息
     **/
    public ResponsePayloadInfo getPayload() {
        return payload;
    }

    public void setPayload(ResponsePayloadInfo payload) {
        this.payload = payload;
    }

}
