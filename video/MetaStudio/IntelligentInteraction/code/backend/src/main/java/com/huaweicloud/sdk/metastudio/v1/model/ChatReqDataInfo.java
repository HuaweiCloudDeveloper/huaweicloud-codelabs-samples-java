package com.huaweicloud.sdk.metastudio.v1.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * 对话请求数据信息
 **/

public class ChatReqDataInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty("text")
    private String text = null;

    @JsonProperty("seq")
    private Integer seq = null;

    @JsonProperty("is_last")
    private Boolean isLast = null;

    /**
     * 文本
     * minLength: 0
     * maxLength: 2000
     **/
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    /**
     * 数据包序号
     * minimum: 1.0
     * maximum: 128.0
     **/
    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    /**
     * 是否为最后一个文本
     **/
    public Boolean getIsLast() {
        return isLast;
    }

    public void setIsLast(Boolean isLast) {
        this.isLast = isLast;
    }

}
