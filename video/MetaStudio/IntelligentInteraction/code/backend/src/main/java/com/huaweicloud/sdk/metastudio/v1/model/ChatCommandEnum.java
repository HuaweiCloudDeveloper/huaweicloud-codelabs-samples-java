package com.huaweicloud.sdk.metastudio.v1.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ChatCommandEnum {
    @JsonProperty("START_CHAT") START_CHAT("START_CHAT"),
    @JsonProperty("TEXT_DRIVE") TEXT_DRIVE("TEXT_DRIVE"),
    @JsonProperty("TEXT_DRIVE_RSP") TEXT_DRIVE_RSP("TEXT_DRIVE_RSP"),
    @JsonProperty("INTERRUPT_CHAT") INTERRUPT_CHAT("INTERRUPT_CHAT"),
    @JsonProperty("INTERRUPT_CHAT_RSP") INTERRUPT_CHAT_RSP("INTERRUPT_CHAT_RSP"),
    @JsonProperty("STOP_CHAT") STOP_CHAT("STOP_CHAT"),
    @JsonProperty("STOP_CHAT_RSP") STOP_CHAT_RSP("STOP_CHAT_RSP"),
    @JsonProperty("START_SPEAKING") START_SPEAKING("START_SPEAKING"),
    @JsonProperty("STOP_SPEAKING") STOP_SPEAKING("STOP_SPEAKING"),
    @JsonProperty("JOB_FINISHED") JOB_FINISHED("JOB_FINISHED"),
    ;

    private ChatCommandEnum(String value) {
        this.value = value;
    }

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}