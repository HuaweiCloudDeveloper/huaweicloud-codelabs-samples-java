package com.huaweicloud.sdk.metastudio.v1.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * 请求负载信息
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestPayloadInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty("job_id")
    private String jobId = null;

    @JsonProperty("robot_id")
    private String robotId = null;

    @JsonProperty("chat_id")
    private String chatId = null;

    @JsonProperty("command")
    private ChatCommandEnum command = null;

    @JsonProperty("data")
    private ChatReqDataInfo data = null;

    /**
     * 本次对话的任务id，用于标识一个对话任务。
     * minLength: 1
     * maxLength: 32
     **/
    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    /**
     * 应用ID。
     * minLength: 1
     * maxLength: 64
     **/
    public String getRobotId() {
        return robotId;
    }

    public void setRobotId(String robotId) {
        this.robotId = robotId;
    }

    /**
     * 对话ID。
     * minLength: 1
     * maxLength: 64
     **/
    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    /**
     *
     **/
    public ChatCommandEnum getCommand() {
        return command;
    }

    public void setCommand(ChatCommandEnum command) {
        this.command = command;
    }

    /**
     * 对话请求数据信息
     **/
    public ChatReqDataInfo getData() {
        return data;
    }

    public void setData(ChatReqDataInfo data) {
        this.data = data;
    }

}
