package com.huaweicloud.sdk.metastudio.v1.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * 智能交互驱动请求数据
 **/
public class ChatRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty("request_id")
    private String requestId = null;

    @JsonProperty("payload")
    private RequestPayloadInfo payload = null;

    /**
     * 请求ID
     * minLength: 1
     * maxLength: 64
     **/
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * 对话请求payload数据信息
     **/
    public RequestPayloadInfo getPayload() {
        return payload;
    }

    public void setPayload(RequestPayloadInfo payload) {
        this.payload = payload;
    }

}
