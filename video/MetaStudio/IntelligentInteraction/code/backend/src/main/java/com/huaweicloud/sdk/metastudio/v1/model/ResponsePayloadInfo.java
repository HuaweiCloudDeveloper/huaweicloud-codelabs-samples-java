package com.huaweicloud.sdk.metastudio.v1.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * 响应负载信息
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponsePayloadInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty("job_id")
    private String jobId = null;

    @JsonProperty("chat_id")
    private String chatId = null;

    @JsonProperty("command")
    private ChatCommandEnum command = null;

    /**
     * 本次对话的任务id，用于标识一个对话任务。
     * minLength: 1
     * maxLength: 32
     **/
    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    /**
     * 对话ID。
     * minLength: 1
     * maxLength: 64
     **/
    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    /**
     *
     **/
    public ChatCommandEnum getCommand() {
        return command;
    }

    public void setCommand(ChatCommandEnum command) {
        this.command = command;
    }

}
