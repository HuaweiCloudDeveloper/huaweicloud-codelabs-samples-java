## 版本说明 
本示例基于华为云SDK V3.0版本开发

## 功能介绍
**华为云MetaStudio智能交互提供哪些开放能力？**

华为云MetaStudio智能交互服务，运行态提供如下三种类型的开放能力，供不同类型的开发者集成：
* 方式1：开发者直接使用MetaStudio预置的数字人大脑（ASR及LLM），仅需要定制Web端侧交互界面。
  ![方式1](assets/mode1.png)
* 方式2：开发者需要定制数字人大脑（LLM），其他模块直接使用MetaStudio。
  ![方式2](assets/mode2.png)
  > 说明：方式2也可以基于Web SDK定制开发客户端。
* 方式3：MetaStudio仅提供数字人文本实时驱动能力，其他能力完全由开发者实现。
  ![方式3](assets/mode3.png)

智能交互的配置管理既可以使用MetaStudio的配置管理页面，也可以基于[智能交互管理API](https://support.huaweicloud.com/api-metastudio/CreateSmartChatRoom.html) 定制开发。
 ![配置](assets/configuration.png)
**您将学到什么？**

- 1、华为云提供了MetaStudio智能交互客户端Web SDK，您可以通过本Demo学习如何基于Web SDK开发自己的智能交互客户端。

- 2、华为云提供MetaStudio智能交互LLM回调接口，您可以通过本Demo学习如果将自己的LLM语言大模型接入智能交互中。

- 3、华为云提供了MetaStudio智能交互Websocket接口，您可以通过本Demo学习如何驱动数字人。

- 4、华为云提供了MetaStudio服务端SDK，您可以通过本Demo学习如何通过调用SDK接口对智能交互进行配置管理。

## 方式1：基于Web SDK定制客户端
### 前置条件
- 1、已开通华为云MetaStudio智能交互业务。
- 2、已配置华为云MetaStudio智能交互。

具体操作步骤请参考：用户指南>[分身数字人智能交互](https://support.huaweicloud.com/usermanual-metastudio/metastudio_05_0081.html) 。

### Web SDK获取和安装
Web SDK下载参考[“SDK下载”](https://support.huaweicloud.com/api-metastudio/metastudio_08_0005.html) 章节。

Web SDK集成参考[“快速入门”](https://support.huaweicloud.com/api-metastudio/metastudio_08_0008.html) 章节。

### 接口参数说明
关于Web SDK接口的详细说明可参见：

MetaStudio API参考>智能交互>智能交互客户端SDK>Web SDK>[接口参考](https://support.huaweicloud.com/api-metastudio/metastudio_08_0010.html) 章节。

MetaStudio API参考>智能交互>智能交互管理>智能交互数字人对话任务管理>[创建对话链接](https://support.huaweicloud.com/api-metastudio/CreateDialogUrl.html) 章节。

MetaStudio API参考>智能交互>智能交互管理>智能交互数字人鉴权码管理>[创建一次性鉴权码](https://support.huaweicloud.com/api-metastudio/CreateOnceCode.html) 章节。

### 关键代码片段
1、集成智能交互客户端SDK
```javascript
<html lang="en" style="font-size: 100px">
<head>
  <meta charset="UTF-8">
  <title>智能交互</title>
  <link rel="stylesheet" href="./HwICSUiSdk.css">
  <style>
    body{
      font-size: 16px;
    }
    #ics-root{
      width: 80vw; height: 80vh
    }
    .entry-container {
      display: flex;
      flex-direction: column;
      justify-content: space-around;
    }
  </style>
</head>
<body>
<div class="entry-container">
  <label>
    请输入serverAddress: <input id="serverAddress" placeholder="请输入服务器地址" style="display: inline-block;width: 800px" value="">
  </label>
  <label>
    请输入robotId: <input id="robotId" placeholder="请输入robotId" style="display: inline-block;width: 800px" value="">
  </label>
  <label>
    请输入一次性token: <input id="token" placeholder="请输入一次性token" style="width: 600px">
  </label>
  <button style="width: 200px;height: 50px" onclick="doInit()">开始智能交互</button>
</div>
<div id="ics-root"></div>
<script src="./HwICSUiSdk.js"></script>

<script>
  const doInit = async () => {
    const serverAddress = document.querySelector('#serverAddress').value;
    const robotId = document.querySelector('#robotId').value;
    const token = document.querySelector('#token').value;
    const result = await HwICSUiSdk.checkBrowserSupport();
    console.log('checkBrowserSupport', result)
    if (!result) {
      return;
    }
    if (serverAddress && robotId && token) {
      await HwICSUiSdk.create({
        onceCode: token.trim(),
        serverAddress,
        robotId,
        containerId: 'ics-root',
        config: {
          enableCaption: true,
          enableChatBtn: true
        },
        eventListeners: {
          error: (error) => {
            console.error('sdk error', {
              message: error.message,
              code: error.code,
              requestId: error.requestId
            }, error);
          }
        }
      });
    }
  }
</script>
</body>
</html>
```

### 返回结果示例
无返回

## 方式2：基于LLM回调接口，支持自定义数字人大脑
### 前置条件
- 1、已开通华为云MetaStudio智能交互业务。
- 2、已配置华为云MetaStudio智能交互。

具体操作步骤请参考：用户指南>[分身数字人智能交互](https://support.huaweicloud.com/usermanual-metastudio/metastudio_05_0081.html) 。
> 智能交互>应用>第三方应用需要选择“第三方语言模型”

### 接口参数说明
LLM回调接口的详细说明可参见：

MetaStudio API参考>智能交互>智能交互管理>智能交互数字人应用管理>[创建应用](https://support.huaweicloud.com/api-metastudio/CreateRobot.html) 章节。

### 关键代码片段
1、集成智能交互客户端SDK
```javascript
<html lang="en" style="font-size: 100px">
<head>
  <meta charset="UTF-8">
  <title>智能交互</title>
  <link rel="stylesheet" href="./HwICSUiSdk.css">
  <style>
    body{
        font-size: 16px;
    }
    #ics-root{
        width: 80vw; height: 80vh
    }
    .entry-container {
        display: flex;
        flex-direction: column;
        justify-content: space-around;
    }
  </style>
</head>
<body>
<div class="entry-container">
  <label>
     请输入serverAddress: <input id="serverAddress" placeholder="请输入服务器地址" style="display: inline-block;width: 800px" value="">
  </label>
  <label>
     请输入robotId: <input id="robotId" placeholder="请输入robotId" style="display: inline-block;width: 800px" value="">
  </label>
  <label>
     请输入一次性token: <input id="token" placeholder="请输入一次性token" style="width: 600px">
  </label>
  <button style="width: 200px;height: 50px" onclick="doInit()">开始智能交互</button>
</div>
<div id="ics-root"></div>
<script src="./HwICSUiSdk.js"></script>

<script>
  const doInit = async () => {
    const serverAddress = document.querySelector('#serverAddress').value;
    const robotId = document.querySelector('#robotId').value;
    const token = document.querySelector('#token').value;
    const result = await HwICSUiSdk.checkBrowserSupport();
    console.log('checkBrowserSupport', result)
    if (!result) {
      return;
    }
    if (serverAddress && robotId && token) {
      await HwICSUiSdk.create({
        onceCode: token.trim(),
        serverAddress,
        robotId,
        containerId: 'ics-root',
        config: {
          enableCaption: true,
          enableChatBtn: true
        },
        eventListeners: {
          error: (error) => {
            console.error('sdk error', {
              message: error.message,
              code: error.code,
              requestId: error.requestId
            }, error);
          }
        }
      });
    }
  }
</script>
</body>
</html>
```

2、配置LLM回调接口
```java
    /**
     * 创建智能交互应用(支持配置自定义数字人大脑)
     *
     */
    public static void createRobot(MetaStudioClient client, String llmUrl) {
        try {
            CreateRobotRequest request = new CreateRobotRequest();
            CreateRobotReq body = new CreateRobotReq();
            ThirdPartyModelConfig thirdPartyModelConfigbody = new ThirdPartyModelConfig();
            thirdPartyModelConfigbody.withAppId("xxx")
                .withAppKey("xxx")
                .withLlmUrl(llmUrl)
                .withIsStream(true)
                .withChatRounds(1);
            body.withThirdPartyModelConfig(thirdPartyModelConfigbody);
            request.withBody(body);
            CreateRobotResponse response = client.createRobot(request);
            System.out.println(response.toString());
            logger.info("createRobot " + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createRobot ClientRequestException " + e.getHttpStatusCode());
            logger.error("createRobot ClientRequestException " + e);
        } catch (ServerResponseException e) {
            logger.error("createRobot ServerResponseException " + e.getHttpStatusCode());
            logger.error("createRobot ServerResponseException " + e.getMessage());
        }
    }
```

### 返回结果示例
**配置LLM回调接口**
```json
{
  "robot_id" : "b3e2b1db10b34f45bff241b133142627"
}
```


## 方式3：基于Websocket接口，文本实时驱动数字人
### 前置条件
- 1、已开通华为云MetaStudio智能交互业务。
- 2、已配置华为云MetaStudio智能交互。

具体操作步骤请参考：用户指南>[分身数字人智能交互](https://support.huaweicloud.com/usermanual-metastudio/metastudio_05_0081.html) 。
> 智能交互>应用>第三方应用需要选择“第三方驱动”

### 交互时序图
文本实时驱动的交互流程请参见：

MetaStudio API参考>智能交互>[智能交互驱动流程](https://support.huaweicloud.com/api-metastudio/metastudio_02_0028.html) 章节。

### 接口参数说明
文本驱动的详细说明可参见：

MetaStudio API参考>智能交互>[智能交互驱动WebSocket接口](https://support.huaweicloud.com/api-metastudio/metastudio_02_0029.html) 章节。

### 关键代码片段
1、集成智能交互客户端SDK，获取jobId
```javascript
<html lang="en" style="font-size: 100px">
<head>
  <meta charset="UTF-8">
  <title>智能交互</title>
  <link rel="stylesheet" href="./HwICSUiSdk.css">
  <style>
    body{
      font-size: 16px;
    }
    #ics-root{
      width: 80vw; height: 80vh
    }
    .entry-container {
      display: flex;
      flex-direction: column;
      justify-content: space-around;
    }
  </style>
</head>
<body>
<div class="entry-container">
  <h2>文本交互模式demo</h2>
  <label>
    请输入serverAddress: <input id="serverAddress" placeholder="请输入服务器地址" style="display: inline-block;width: 800px" value="">
  </label>
  <label>
    请输入robotId: <input id="robotId" placeholder="请输入robotId" style="display: inline-block;width: 800px" value="">
  </label>
  <label>
    请输入一次性token: <input id="token" placeholder="请输入一次性token" style="width: 600px">
  </label>
  <button style="width: 200px;height: 50px" onclick="doInit()">开始智能交互</button>
</div>
<div id="ics-root"></div>
<script src="./HwICSUiSdk.js"></script>

<script>
  const doInit = async () => {
    const serverAddress = document.querySelector('#serverAddress').value;
    const robotId = document.querySelector('#robotId').value;
    const token = document.querySelector('#token').value;
    const result = await HwICSUiSdk.checkBrowserSupport();
    console.log('checkBrowserSupport', result)
    if (!result) {
      return;
    }
    if (serverAddress && robotId && token) {
      await HwICSUiSdk.create({
        onceCode: token.trim(),
        serverAddress,
        robotId,
        containerId: 'ics-root',
        eventListeners: {
          error: (error) => {
            console.error('sdk error', {
              message: error.message,
              code: error.code,
              requestId: error.requestId
            }, error);
          },
          jobInfoChange: (jobInfo) => {
            alert(`jobInfoChange: ${JSON.stringify(jobInfo)}`)
          }
        }
      });
    }
  }
</script>
</body>
</html>
```

2、请求建立WebSocket链接，获取chatId
```java
    /**
     * 请求建立WebSocket连接，获取chatId
     */
    private static IntelligentInteractionWebSocketClient initWebsocketClient(String jobId, String onceCode) {
        logger.info("initWebsocketClient start");
        IntelligentInteractionWebSocketClient wsClient = null;
        try {
            // 创建MetaStudio websocket 实例
            String endpoint =
                "metastudio-api.cn-east-3.myhuaweicloud.com"; // region为上海一需写为：metastudio-api.cn-east-3.myhuaweicloud.com
            String path = String.format("/v1/%s/digital-human-chat/chat-command/%s", PROJECT_ID, jobId);
            String query = "token=" + onceCode;
            URI uri = new URI("wss", null, endpoint, 443, path, query, null);
            javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> true);
            wsClient = new IntelligentInteractionWebSocketClient(uri, jobId);
            wsClient.connectBlocking(5000, TimeUnit.MILLISECONDS);
            // 等候chatId返回
            String chatId = waitRespCommand(wsClient.getResponses(), ChatCommandEnum.START_CHAT, "showChatId")
                .getChatId();
            wsClient.setChatId(chatId);
            logger.info("initWebsocketClient " + chatId);
        } catch (Throwable e) {
            logger.error("initWebsocketClient exception " + e.getMessage());
            if (wsClient != null && wsClient.isOpen()) {
                wsClient.close();
            }
        }
        return wsClient;
    }
```

3、发送文本驱动消息
```java
    /**
     * 发送文本驱动消息
     *
     */
    private static void createTextDrive(IntelligentInteractionWebSocketClient wsClient) {
        logger.info("createTextDrive start");
        try {
            ChatReqDataInfo data = new ChatReqDataInfo();
            data.setSeq(1);
            data.setText("您好，我是您的分身数字人");
            data.setIsLast(true);
            
            sendMessage(wsClient, ChatCommandEnum.TEXT_DRIVE, data);
            // 等待websocket返回消息
            waitRespCommand(wsClient.getResponses(), ChatCommandEnum.TEXT_DRIVE_RSP, "createTextDrive");
            waitRespCommand(wsClient.getResponses(), ChatCommandEnum.START_SPEAKING, "createTextDrive");
            logger.info("createTextDrive end");
        } catch (Exception e) {
            logger.error("createTextDrive exception " + e.getMessage());
        }
    }
```

4、发送中断对话的消息
```java
    /**
     * 发送中断对话的消息
     *
     */
    private static void createInterruptChat(IntelligentInteractionWebSocketClient wsClient) {
        logger.info("createInterruptChat start");
        try {
            sendMessage(wsClient, ChatCommandEnum.INTERRUPT_CHAT, null);
            // 等待websocket返回消息
            waitRespCommand(wsClient.getResponses(), ChatCommandEnum.INTERRUPT_CHAT_RSP, "createInterruptChat");
            waitRespCommand(wsClient.getResponses(), ChatCommandEnum.START_CHAT, "createInterruptChat");
            logger.info("createInterruptChat end");
        } catch (Exception e) {
            logger.error("createInterruptChat exception " + e.getMessage());
        }
    }
```

5、发送结束对话的消息
```java
    /**
     * 发送结束对话的消息
     *
     */
    private static void createStopChat(IntelligentInteractionWebSocketClient wsClient) {
        logger.info("createStopChat start");
        try {
            sendMessage(wsClient, ChatCommandEnum.STOP_CHAT, null);
            // 等待websocket返回消息
            waitRespCommand(wsClient.getResponses(), ChatCommandEnum.STOP_CHAT_RSP, "createStopChat");
            logger.info("createStopChat end");
        } catch (Exception e) {
            logger.error("createStopChat exception " + e.getMessage());
        }
    }
```

### 返回结果示例
**请求建立链接**
```json
{
  "error_code": "MSS.00000000",
  "error_msg": "success",
  "request_id": "d7aa08da33dd4a662ad***"
}
```

**发送启动对话的消息**
```json
{
  "request_id": "d7aa08da33dd4a662ad***",
  "payload": {
    "command": "START_CHAT",
    "job_id": "e37a28485f684769aa5***",
    "chat_id": "ac71c539395b444686***"
  }
}
```

**返回文本驱动的响应消息**
```json
{
  "error_code": "MSS.00000000",
  "error_msg": "success",
  "request_id": "d7aa08da33dd4a662ad5b***",
  "payload": {
    "command": "TEXT_DRIVE_RSP",
    "job_id": "e37a28485f684769aa537***",
    "chat_id": "ac71c539395b444686507***"
  }
}
```

## 配置管理
### 交互时序图
![交互时序](assets/intelligent-interaction-management.png)

### 前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。具体请参见[服务端SDK参考](https://support.huaweicloud.com/ssdk-metastudio/metastudio_06_0000.html) 。
- 2、您需要拥有华为云租户账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 3、华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。
- 4、已开通华为云MetaStudio智能交互业务。

### 接口参数说明
关于智能交互配置管理接口的详细说明可参见：

[a.查询激活码列表](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ListActiveCode)

[b.创建激活码](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateActiveCode)

[c.查询激活码详情](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowActiveCode)

[d.修改激活码](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=UpdateActiveCode)

[e.重置激活码](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ResetActiveCode)

[f.删除激活码](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=DeleteActiveCode)

[g.创建对话链接](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateDialogUrl)

[h.启动数字人智能交互任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=StartSmartChatJob)

[i.结束数字人智能交互任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=StopSmartChatJob)

[j.查询数字人智能交互任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowSmartChatJob)

[k.创建一次性鉴权码](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateOnceCode)

[l.查询应用列表](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ListRobot)

[m.创建应用](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateRobot)

[n.查询应用详情](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowRobot)

[o.修改应用](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=UpdateRobot)

[p.删除应用](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=DeleteRobot)

[q.查询智能交互对话列表](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ListSmartChatRooms)

[r.创建智能交互对话](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateSmartChatRoom)

[s.查询智能交互对话详情](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowSmartChatRoom)

[t.更新智能交互对话信息](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=UpdateSmartChatRoom)

[u.删除智能交互对话](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=DeleteSmartChatRoom)

### 关键代码片段
1、查询激活码列表
```java
    /**
     * 查询激活码列表
     *
     */
private static String listActiveCode(MetaStudioClient client, String robotId) {
    logger.info("listActiveCode start");
    ListActiveCodeRequest request = new ListActiveCodeRequest()
            .withRobotId(robotId);
    String activeCodeId = "";
    try {
        ListActiveCodeResponse response = client.listActiveCode(request);
        logger.info("listActiveCode" + response.toString());
        activeCodeId = response.getData().get(0).getActiveCodeId();
    } catch (ClientRequestException e) {
        logger.error("listActiveCode ClientRequestException" + e.getHttpStatusCode());
        logger.error("listActiveCode ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("listActiveCode ServerResponseException" + e.getHttpStatusCode());
        logger.error("listActiveCode ServerResponseException" + e.getMessage());
    }
    return activeCodeId;
}
```

2、创建激活码
```java
    /**
     * 创建激活码
     *
     */
private static String createActiveCode(MetaStudioClient client, String robotId, String roomId) {
    logger.info("createActiveCode start");
    CreateActiveCodeRequest request = new CreateActiveCodeRequest()
            .withBody(new CreateActiveCodeReq()
                    .withRobotId(robotId)
                    .withRoomId(roomId)
                    .withValidPeriod(3));
    String codeId = "";
    try {
        CreateActiveCodeResponse response = client.createActiveCode(request);
        logger.info("createActiveCode" + response.toString());
        codeId = response.getActiveCodeId();
    } catch (ClientRequestException e) {
        logger.error("createActiveCode ClientRequestException" + e.getHttpStatusCode());
        logger.error("createActiveCode ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("createActiveCode ServerResponseException" + e.getHttpStatusCode());
        logger.error("createActiveCode ServerResponseException" + e.getMessage());
    }
    return codeId;
}
```

3、查询激活码详情
```java
    /**
     * 查询激活码详情
     *
     */
private static void showActiveCode(MetaStudioClient client, String activeCodeId) {
    logger.info("showActiveCode start");
    ShowActiveCodeRequest request = new ShowActiveCodeRequest()
            .withActiveCodeId(activeCodeId);
    try {
        ShowActiveCodeResponse response = client.showActiveCode(request);
        logger.info("showActiveCode" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("showActiveCode ClientRequestException" + e.getHttpStatusCode());
        logger.error("showActiveCode ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("showActiveCode ServerResponseException" + e.getHttpStatusCode());
        logger.error("showActiveCode ServerResponseException" + e.getMessage());
    }
}
```

4、修改激活码
```java
    /**
     * 修改激活码
     *
     */
private static void updateActiveCode(MetaStudioClient client, String activeCodeId) {
    logger.info("updateActiveCode start");
    UpdateActiveCodeRequest request = new UpdateActiveCodeRequest()
            .withActiveCodeId(activeCodeId);
    try {
        UpdateActiveCodeResponse response = client.updateActiveCode(request);
        logger.info("updateActiveCode" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("updateActiveCode ClientRequestException" + e.getHttpStatusCode());
        logger.error("updateActiveCode ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("updateActiveCode ServerResponseException" + e.getHttpStatusCode());
        logger.error("updateActiveCode ServerResponseException" + e.getMessage());
    }
}
```

5、重置激活码
```java
    /**
     * 重置激活码
     *
     */
private static void resetActiveCode(MetaStudioClient client, String activeCodeId) {
    logger.info("resetActiveCode start");
    ResetActiveCodeRequest request = new ResetActiveCodeRequest()
            .withActiveCodeId(activeCodeId);

    try {
        ResetActiveCodeResponse response = client.resetActiveCode(request);
        logger.info("resetActiveCode" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("resetActiveCode ClientRequestException" + e.getHttpStatusCode());
        logger.error("resetActiveCode ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("resetActiveCode ServerResponseException" + e.getHttpStatusCode());
        logger.error("resetActiveCode ServerResponseException" + e.getMessage());
    }
}
```

6、删除激活码
```java
    /**
     * 删除激活码
     *
     */
private static void deleteActiveCode(MetaStudioClient client, String activeCodeId) {
    logger.info("deleteActiveCode start");
    List<String> list = new ArrayList<>();
    list.add(activeCodeId);
    DeleteActiveCodeRequest request = new DeleteActiveCodeRequest().withBody(list);
    try {
        DeleteActiveCodeResponse response = client.deleteActiveCode(request);
        logger.info("deleteActiveCode" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("deleteActiveCode ClientRequestException" + e.getHttpStatusCode());
        logger.error("deleteActiveCode ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("deleteActiveCode ServerResponseException" + e.getHttpStatusCode());
        logger.error("deleteActiveCode ServerResponseException" + e.getMessage());
    }
}
```

7、创建对话链接
```java
    /**
     * 创建对话链接
     *
     */
public static String createDialogUrl(MetaStudioClient mClient, String roomId, String robotId) {
    logger.info("createDialogUrl start");
    String url = null;
    try {
        CreateDialogUrlRequest request = new CreateDialogUrlRequest();
        CreateDialogUrlReq body = new CreateDialogUrlReq();
        body.withRobotId(robotId);
        body.withRoomId(roomId);
        request.withBody(body);
        CreateDialogUrlResponse response = mClient.createDialogUrl(request);
        logger.info("createDialogUrl " + response.toString());
        url = response.getUrl();
    } catch (ClientRequestException e) {
        logger.error("createDialogUrl ClientRequestException " + e.getHttpStatusCode());
        logger.error("createDialogUrl ClientRequestException " + e);
    } catch (ServerResponseException e) {
        logger.error("createDialogUrl ServerResponseException " + e.getHttpStatusCode());
        logger.error("createDialogUrl ServerResponseException " + e.getMessage());
    }
    return url;
}
```

8、启动数字人智能交互任务
```java
    /**
     * 启动数字人智能交互任务
     *
     */
private static String startSmartChatJob(MetaStudioClient client, String roomId, String robotId) {
    logger.info("startSmartChatJob start");
    StartSmartChatJobRequest request = new StartSmartChatJobRequest()
            .withRobotId(robotId)
            .withRoomId(roomId);
    String jobId = "";
    try {
        StartSmartChatJobResponse response = client.startSmartChatJob(request);
        logger.info("startSmartChatJob" + response.toString());
        jobId = response.getJobId();
    } catch (ClientRequestException e) {
        logger.error("startSmartChatJob ClientRequestException" + e.getHttpStatusCode());
        logger.error("startSmartChatJob ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("startSmartChatJob ServerResponseException" + e.getHttpStatusCode());
        logger.error("startSmartChatJob ServerResponseException" + e.getMessage());
    }
    return jobId;
}
```

9、结束数字人智能交互任务
```java
    /**
     * 结束数字人智能交互任务
     *
     */
private static void stopSmartChatJob(MetaStudioClient client, String jobId, String roomId) {
    logger.info("stopSmartChatJob start");
    StopSmartChatJobRequest request = new StopSmartChatJobRequest()
            .withJobId(jobId)
            .withRoomId(roomId);
    try {
        StopSmartChatJobResponse response = client.stopSmartChatJob(request);
        logger.info("stopSmartChatJob" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("stopSmartChatJob ClientRequestException" + e.getHttpStatusCode());
        logger.error("stopSmartChatJob ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("stopSmartChatJob ServerResponseException" + e.getHttpStatusCode());
        logger.error("stopSmartChatJob ServerResponseException" + e.getMessage());
    }
}
```

10、查询数字人智能交互任务
```java
    /**
     * 查询数字人智能交互任务
     *
     */
private static void showSmartChatJob(MetaStudioClient client, String jobId, String roomId) {
    logger.info("showSmartChatJob start");
    ShowSmartChatJobRequest request = new ShowSmartChatJobRequest()
            .withJobId(jobId)
            .withRoomId(roomId);
    try {
        ShowSmartChatJobResponse response = client.showSmartChatJob(request);
        logger.info("showSmartChatJob" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("showSmartChatJob ClientRequestException" + e.getHttpStatusCode());
        logger.error("showSmartChatJob ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("showSmartChatJob ServerResponseException" + e.getHttpStatusCode());
        logger.error("showSmartChatJob ServerResponseException" + e.getMessage());
    }
}
```

11、创建一次性鉴权码
```java
    /**
     * 创建一次性鉴权码
     */
public static String createOnceCode(MetaStudioClient mClient) {
    logger.info("createOnceCode start");
    CreateOnceCodeRequest request = new CreateOnceCodeRequest();
    String onceCode = null;
    try {
        CreateOnceCodeResponse response = mClient.createOnceCode(request);
        logger.info("createOnceCode " + response.toString());
        onceCode = response.getOnceCode();
    } catch (ClientRequestException e) {
        logger.error("createOnceCode ClientRequestException" + e.getHttpStatusCode());
        logger.error("createOnceCode ClientRequestException" + e.getMessage());
    } catch (ServerResponseException e) {
        logger.error("createOnceCode ServerResponseException" + e.getHttpStatusCode());
        logger.error("createOnceCode ServerResponseException" + e.getMessage());
    }
    return onceCode;
}
```

12、查询应用列表
```java
    /**
     * 查询应用列表
     *
     */
private static String listRobot(MetaStudioClient client) {
    logger.info("listRobot start");
    ListRobotRequest request = new ListRobotRequest();
    String robotId = "";
    try {
        ListRobotResponse response = client.listRobot(request);
        robotId = response.getData().get(0).getRobotId();
        logger.info("listRobot" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("listRobot ClientRequestException" + e.getHttpStatusCode());
        logger.error("listRobot ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("listRobot ServerResponseException" + e.getHttpStatusCode());
        logger.error("listRobot ServerResponseException" + e.getMessage());
    }
    return robotId;
}
```

13、创建应用
```java
    /**
     * 创建应用
     */
public static String createRobot(MetaStudioClient mClient) {
    logger.info("createRobot start");
    String robotId = null;
    try {
        CreateRobotRequest request = new CreateRobotRequest();
        CreateRobotReq body = new CreateRobotReq();
        // 对接第三方应用厂商类型。
        // 0：科大讯飞AIUI；1：华为云CBS；2：科大讯飞星火交互认知大模型；5：第三方驱动（websocket接口驱动）；6：第三方语言模型
        body.withAppType(5);
        body.withName("<your name>");
        request.withBody(body);
        CreateRobotResponse response = mClient.createRobot(request);
        logger.info("createRobot " + response.toString());
        robotId = response.getRobotId();
    } catch (ClientRequestException e) {
        logger.error("createRobot ClientRequestException " + e.getHttpStatusCode());
        logger.error("createRobot ClientRequestException " + e);
    } catch (ServerResponseException e) {
        logger.error("createRobot ServerResponseException " + e.getHttpStatusCode());
        logger.error("createRobot ServerResponseException " + e.getMessage());
    }
    return robotId;
}
```

14、查询应用详情
```java
    /**
     * 查询应用详情
     *
     */
private static void showRobot(MetaStudioClient client, String robotId) {
    logger.info("showRobot start");
    ShowRobotRequest request = new ShowRobotRequest()
            .withRobotId(robotId);
    try {
        ShowRobotResponse response = client.showRobot(request);
        logger.info("showRobot" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("showRobot ClientRequestException" + e.getHttpStatusCode());
        logger.error("showRobot ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("showRobot ServerResponseException" + e.getHttpStatusCode());
        logger.error("showRobot ServerResponseException" + e.getMessage());
    }
}
```

15、修改应用
```java
    /**
     * 修改应用
     *
     */
private static void updateRobot(MetaStudioClient client, String robotId) {
    logger.info("updateRobot start");
    UpdateRobotRequest request = new UpdateRobotRequest()
            .withRobotId(robotId)
            .withBody(new UpdateRobotReq());
    try {
        UpdateRobotResponse response = client.updateRobot(request);
        logger.info("updateRobot" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("updateRobot ClientRequestException" + e.getHttpStatusCode());
        logger.error("updateRobot ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("updateRobot ServerResponseException" + e.getHttpStatusCode());
        logger.error("updateRobot ServerResponseException" + e.getMessage());
    }
}
```

16、删除应用
```java
    /**
     * 删除应用
     *
     */
private static void deleteRobot(MetaStudioClient client, String robotId) {
    logger.info("deleteRobot start");
    List<String> list = new ArrayList<>();
    list.add(robotId);
    DeleteRobotRequest request = new DeleteRobotRequest()
            .withBody(list);
    try {
        DeleteRobotResponse response = client.deleteRobot(request);
        logger.info("deleteRobot" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("deleteRobot ClientRequestException" + e.getHttpStatusCode());
        logger.error("deleteRobot ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("deleteRobot ServerResponseException" + e.getHttpStatusCode());
        logger.error("deleteRobot ServerResponseException" + e.getMessage());
    }
}
```

17、查询智能交互对话列表
```java
    /**
     * 查询智能交互对话列表
     *
     */
private static String listSmartChatRooms(MetaStudioClient client) {
    logger.info("listSmartChatRooms start");
    ListSmartChatRoomsRequest request = new ListSmartChatRoomsRequest();
    String roomId = "";
    try {
        ListSmartChatRoomsResponse response = client.listSmartChatRooms(request);
        logger.info("listSmartChatRooms" + response.toString());
        roomId = response.getSmartChatRooms().get(0).getRoomId();
    } catch (ClientRequestException e) {
        logger.error("listSmartChatRooms ClientRequestException" + e.getHttpStatusCode());
        logger.error("listSmartChatRooms ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("listSmartChatRooms ServerResponseException" + e.getHttpStatusCode());
        logger.error("listSmartChatRooms ServerResponseException" + e.getMessage());
    }
    return roomId;
}
```

18、创建智能交互对话
```java
    /**
     * 创建智能交互对话
     *
     */
public static String createSmartChatRoom(MetaStudioClient mClient, String robotId) {
    logger.info("createSmartChatRoom start");
    String roomId = null;
    try {
        String modelAssetId = getAssetsId(mClient, AssetTypeEnum.HUMAN_MODEL);
        String videoConfigId = getAssetsId(mClient, AssetTypeEnum.VOICE_MODEL);
        DigitalAssetInfo digitalAssetInfo = getBackgroundAssets(mClient, AssetTypeEnum.IMAGE);
        List<LayerConfig> layerConfig = new ArrayList<>();
        layerConfig.add(new LayerConfig()
            .withLayerType(LayerTypeEnum.HUMAN)
            .withPosition(new LayerPositionConfig()
            .withLayerIndex(1)
            .withDx(260)
            .withDy(10))
            .withSize(new LayerSizeConfig()
            .withWidth(600)
            .withHeight(100)));
        CreateSmartChatRoomRequest request = new CreateSmartChatRoomRequest()
            .withBody(new CreateSmartChatRoomReq()
            .withRoomName("<your name>")
            .withRoomDescription("")
            .withVideoConfig(new VideoConfig()
                .withCodec(CodecEnum.H264)
                .withBitrate(3000)
                .withWidth(1920)
                .withHeight(1080)
                .withFrameRate(FrameRateEnum._25))
            .withModelAssetId(modelAssetId)
            .withVoiceConfig(new VoiceConfig()
                .withVoiceAssetId(videoConfigId)
                .withSpeed(100)
                .withPitch(100)
                .withVolume(140))
            .withRobotId(robotId)
            .withConcurrency(2)
            .withBackgroundConfig(new BackgroundConfigInfo()
                .withBackgroundAssetId(digitalAssetInfo.getAssetId())
                .withBackgroundConfig(digitalAssetInfo.getFiles().get(0).getDownloadUrl())
                .withBackgroundType(BackgroundTypeEnum.IMAGE_2D))
            .withLayerConfig(layerConfig)
            .withChatSubtitleConfig(new ChatSubtitleConfig()
                .withDx(100)
                .withDy(96)
                .withWidth(800)
                .withHeight(887)));
        CreateSmartChatRoomResponse response = mClient.createSmartChatRoom(request);
        logger.info("createSmartChatRoom " + response.toString());
        roomId = response.getRoomId();
    } catch (ClientRequestException e) {
        logger.error("createSmartChatRoom ClientRequestException " + e.getHttpStatusCode());
        logger.error("createSmartChatRoom ClientRequestException " + e);
    } catch (ServerResponseException e) {
        logger.error("createSmartChatRoom ServerResponseException " + e.getHttpStatusCode());
        logger.error("createSmartChatRoom ServerResponseException " + e.getMessage());
    }
    return roomId;
}
```

19、查询智能交互对话详情
```java
    /**
     * 查询智能交互对话详情
     *
     */
private static void showSmartChatRoom(MetaStudioClient client, String roomId) {
    logger.info("showSmartChatRoom start");
    ShowSmartChatRoomRequest request = new ShowSmartChatRoomRequest()
            .withRoomId(roomId);
    try {
        ShowSmartChatRoomResponse response = client.showSmartChatRoom(request);
        logger.info("showSmartChatRoom" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("showSmartChatRoom ClientRequestException" + e.getHttpStatusCode());
        logger.error("showSmartChatRoom ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("showSmartChatRoom ServerResponseException" + e.getHttpStatusCode());
        logger.error("showSmartChatRoom ServerResponseException" + e.getMessage());
    }
}
```

20、更新智能交互对话信息
```java
    /**
     * 更新智能交互对话信息
     *
     */
private static void updateSmartChatRoom(MetaStudioClient client, String roomId, String robotId) {
    logger.info("updateSmartChatRoom start");
    UpdateSmartChatRoomRequest request = new UpdateSmartChatRoomRequest()
            .withRoomId(roomId)
            .withBody(new CreateSmartChatRoomReq()
                    .withRoomName("<your name>")
                    .withRobotId(robotId));
    try {
        UpdateSmartChatRoomResponse response = client.updateSmartChatRoom(request);
        logger.info("updateSmartChatRoom" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("updateSmartChatRoom ClientRequestException" + e.getHttpStatusCode());
        logger.error("updateSmartChatRoom ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("updateSmartChatRoom ServerResponseException" + e.getHttpStatusCode());
        logger.error("updateSmartChatRoom ServerResponseException" + e.getMessage());
    }
}
```

21、删除智能交互对话
```java
    /**
     * 删除智能交互对话
     *
     */
private static void deleteSmartChatRoom(MetaStudioClient client, String roomId) {
    logger.info("deleteSmartChatRoom start");
    DeleteSmartChatRoomRequest request = new DeleteSmartChatRoomRequest()
            .withRoomId(roomId);
    try {
        DeleteSmartChatRoomResponse response = client.deleteSmartChatRoom(request);
        logger.info("deleteSmartChatRoom" + response.toString());
    } catch (ClientRequestException e) {
        logger.error("deleteSmartChatRoom ClientRequestException" + e.getHttpStatusCode());
        logger.error("deleteSmartChatRoom ClientRequestException" + e);
    } catch (ServerResponseException e) {
        logger.error("deleteSmartChatRoom ServerResponseException" + e.getHttpStatusCode());
        logger.error("deleteSmartChatRoom ServerResponseException" + e.getMessage());
    }
}
```

### 返回结果示例
**创建应用**
```json
{
  "robot_id" : "b3e2b1***"
}
```

**创建智能交互对话**
```json
{
  "room_id" : "26f065***"
}
```

**创建对话链接**
```json
{
  "url" : "https://***"
}
```

**创建一次性鉴权码**
```json
{
  "once_code" : "O1TA3W***"
}
```

**查询激活码列表**
```json
{
    "offset": "0",
    "limit": "10",
    "count": 1,
    "data": [{
        "activeCodeId": "2c9081***",
        "activeCode": "Z96PFL***",
        "robotId": "2c9081***",
        "roomId": "f8f166***",
        "validPeriod": 3,
        "expireTime": "2024-04-04T14:51:38Z",
        "createTime": "2024-04-01T14:51:38Z",
        "updateTime": "2024-04-01T14:51:38Z"
    }],
    "xRequestId": "c46cb1***"
}
```

**创建激活码**
```json
{
    "activeCodeId": "2c9081***",
    "activeCode": "8ER1ZY***",
    "robotId": "2c9081***",
    "roomId": "f8f166***",
    "validPeriod": 3,
    "expireTime": "2024-04-04T14:52:07Z",
    "createTime": "2024-04-01T14:52:07Z",
    "updateTime": "2024-04-01T14:52:07Z",
    "xRequestId": "cbccff***"
}
```

**查询激活码详情**
```json
{
    "activeCodeId": "2c9081***",
    "activeCode": "8ERAZY***",
    "robotId": "2c9081***",
    "roomId": "f8f166***",
    "validPeriod": 3,
    "expireTime": "2024-04-04T14:52:07Z",
    "createTime": "2024-04-01T14:52:07Z",
    "updateTime": "2024-04-01T14:52:07Z",
    "xRequestId": "3aebdd***"
}
```

**修改激活码**
```json
{
    "activeCodeId": "2c9081***",
    "activeCode": "8ERAZY***",
    "robotId": "2c9081***",
    "roomId": "f8f166***",
    "validPeriod": 10,
    "expireTime": "2024-04-11T15:01:05Z",
    "createTime": "2024-04-01T14:52:07Z",
    "updateTime": "2024-04-01T14:52:07Z",
    "xRequestId": "f0bd3d***"
}
```

**重置激活码**
```json
{
    "activeCodeId": "2c9081***",
    "activeCode": "8SF3KL***",
    "robotId": "2c9081***",
    "roomId": "f8f166***",
    "validPeriod": 10,
    "expireTime": "2024-04-11T15:01:05Z",
    "createTime": "2024-04-01T14:52:07Z",
    "updateTime": "2024-04-01T15:01:05Z",
    "xRequestId": "d1fe94***"
}
```

**删除激活码**
```json
{
    "xRequestId": "fe0022***"
}
```

**启动数字人智能交互任务**
```json
{
    "jobId": "5e18d8***",
    "rtcRoomInfo": {
        "appId": "cb23ac***",
        "roomId": "5e18d8***",
        "users": [{
            "userType": "PLAYER",
            "userId": "5e18d8***",
            "signature": "9f06a4***",
            "ctime": 1712028498
        }]
    },
    "chatSubtitleConfig": {
        "dx": 0,
        "dy": 763,
        "width": 1080,
        "height": 1156
    },
    "videoConfig": {
        "width": 1080,
        "height": 1920
    },
    "xRequestId": "a2c117***"
}
```

**结束数字人智能交互任务**
```json
{
    "xRequestId": "2e3f6e***"
}
```

**查询数字人智能交互任务**
```json
{
    "jobId": "5e18d8****",
    "state": "WAITING",
    "duration": 0.0,
    "startTime": "2024-04-02T09:28:18Z",
    "endTime": null,
    "errorInfo": null,
    "createTime": "2024-04-02T01:28:18Z",
    "lastupdateTime": "2024-04-02T09:28:18Z",
    "rtcRoomInfo": {
        "appId": "c312ab***",
        "roomId": "5e18d8***",
        "users": [{
            "userType": "PLAYER",
            "userId": "5e18d8***",
            "signature": "9f06a4***",
            "ctime": 1712028498
        }]
    },
    "chatSubtitleConfig": {
        "dx": 0,
        "dy": 763,
        "width": 1080,
        "height": 1156
    },
    "videoConfig": {
        "width": 1080,
        "height": 1920
    },
    "xRequestId": "a46d1f***"
}
```

**查询应用列表**
```json
{
  "offset": 0,
  "limit": 10,
  "count": 428,
  "data": [{
      "robotId": "2c9081***",
      "name": "yourName",
      "appId": "60e640***",
      "appType": 0,
      "concurrency": null,
      "language": "zh_CN",
      "createTime": "2023-10-19T03 42: 10Z",
      "updateTime": "2023-10-19T03 42: 10Z",
      "region": null,
      "cbsProjectId": null,
      "llmUrl": null,
      "isStream": null,
      "chatRounds": null
    }
  ]
}
```

**查询应用详情**
```json
{
    "robotId": "2c9081***",
    "name": "yourName",
    "appId": "e5f149***",
    "appType": 0,
    "concurrency": null,
    "language": "zh_CN",
    "createTime": "2023-10-24T08:06:09Z",
    "updateTime": "2023-10-26T06:01:55Z",
    "region": null,
    "cbsProjectId": null,
    "llmUrl": null,
    "isStream": null,
    "chatRounds": null,
    "xRequestId": "9b430c***"
}
```

**修改应用**
```json
{
    "xRequestId": "dacebe***"
}
```

**删除应用**
```json
{
    "xRequestId": "c028e8***"
}
```

**查询智能交互对话列表**
```json
{
    "count": 211,
    "countConcurrency": 1448,
    "smartChatRooms": [{
        "roomId": "f8f166***",
        "roomName": "***",
        "roomDescription": null,
        "robotId": "2c9081***",
        "coverUrl": "https://***",
        "modelInfos": {
            "modelAssetId": "4151b2***",
            "assetName": "***",
            "backupModelAssetIds": null
        },
        "voiceConfig": {
            "voiceAssetId": "6d5286***",
            "speed": 100,
            "pitch": 100,
            "volume": 140
        },
        "concurrency": 5,
        "createTime": "2024-01-31T03:39:30Z",
        "updateTime": "2024-04-01T13:13:07Z"
    }]
}
```

**查询智能交互对话详情**
```json
{
    "roomName": "***",
    "roomDescription": "***",
    "videoConfig": {
        "clipMode": "RESIZE",
        "codec": "H264",
        "bitrate": 3000,
        "width": 1080,
        "height": 1920,
        "frameRate": 25,
        "isSubtitleEnable": false,
        "subtitleConfig": null,
        "dx": null,
        "dy": null
    },
    "modelAssetId": "897e44***",
    "voiceConfig": {
        "voiceAssetId": "b96f9c***",
        "speed": 100,
        "pitch": 100,
        "volume": 140
    },
    "robotId": "2c9081***",
    "concurrency": 3,
    "backgroundConfig": {
        "backgroundType": "IMAGE_2D",
        "backgroundTitle": null,
        "humanPosition2d": null,
        "humanSize2d": null,
        "backgroundCoverUrl": null,
        "backgroundConfig": "https://***",
        "backgroundAssetId": "6842da***"
    },
    "layerConfig": [{
        "layerType": "HUMAN",
        "assetId": "c312ab***",
        "groupId": null,
        "position": {
            "dx": 0,
            "dy": 0,
            "layerIndex": 2
        },
        "size": {
            "width": 1080,
            "height": 1920
        },
        "imageConfig": {
            "imageUrl": "https://***"
        },
        "videoConfig": null,
        "textConfig": null
    }],
    "reviewConfig": null,
    "chatSubtitleConfig": {
        "dx": 0,
        "dy": 763,
        "width": 1080,
        "height": 1156
    },
    "roomId": "389eb3***",
    "createTime": "2024-03-26T03:33:08Z",
    "updateTime": "2024-04-02T01:14:47Z",
    "coverUrl": "https://***",
    "xRequestId": "ccfbee***"
}
```

**更新智能交互对话信息**
```json
{
    "roomName": "***",
    "roomDescription": "***",
    "videoConfig": {
        "clipMode": "RESIZE",
        "codec": "H264",
        "bitrate": 3000,
        "width": 1080,
        "height": 1920,
        "frameRate": 25,
        "isSubtitleEnable": false,
        "subtitleConfig": null,
        "dx": null,
        "dy": null
    },
    "modelAssetId": "fb364f***",
    "voiceConfig": {
        "voiceAssetId": "6d5286***",
        "speed": 100,
        "pitch": 100,
        "volume": 140
    },
    "robotId": "2c9081***",
    "concurrency": 3,
    "backgroundConfig": {
        "backgroundType": "IMAGE_2D",
        "backgroundTitle": null,
        "humanPosition2d": null,
        "humanSize2d": null,
        "backgroundCoverUrl": null,
        "backgroundConfig": "https://***",
        "backgroundAssetId": "3d6a24***"
    },
    "layerConfig": [{
        "layerType": "HUMAN",
        "assetId": null,
        "groupId": null,
        "position": {
            "dx": -147,
            "dy": 7,
            "layerIndex": 3
        },
        "size": {
            "width": 1080,
            "height": 1920
        },
        "imageConfig": {
            "imageUrl": "https://***"
        },
        "videoConfig": null,
        "textConfig": null
    }],
    "reviewConfig": null,
    "chatSubtitleConfig": {
        "dx": 570,
        "dy": 620,
        "width": 486,
        "height": 305
    },
    "roomId": "e2c04a***",
    "createTime": "2024-04-01T01:47:53Z",
    "updateTime": "2024-04-02T03:25:54Z",
    "coverUrl": "https://***",
    "xRequestId": "a2160a***"
}
```

**删除智能交互对话**
```json
{
    "xRequestId": "c71c70***"
}
```



## 参考
更多信息请参考:

[API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/doc)

[《华为云MetaStudio开发指南》](https://support.huaweicloud.com/api-metastudio/metastudio_02_0000.html) 

## 修订记录

|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2024-02-18 |   1.0    | 文档首次发布 |