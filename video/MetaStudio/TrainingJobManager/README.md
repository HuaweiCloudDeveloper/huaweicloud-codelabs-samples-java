## 1、功能介绍

华为云提供了MetaStudio服务端SDK，您可以直接集成服务端SDK来调用MetaStudio的相关API，从而实现对MetaStudio的快速操作。

**您将学到什么？**

如何通过java版SDK来体验MetaStudio服务的数字人声音制作任务管理功能。

## 2、开发时序图
![image](assets/TrainingJobManager.jpg)

## 3、前置条件
- 1、已[注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=40c99ba3cea14417a2428c756a626599&lang=zh-cn)华为帐号并开通华为云，已进行[实名认证](https://support.huaweicloud.com/usermanual-account/zh-cn_topic_0077914254.html)。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、已获取账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 4、已获取MetaStudio服务对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见[API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html)。

## 4、SDK获取和安装
您可以通过Maven配置所依赖的[数字内容生产线SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/MetaStudio?lang=Java)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-metastudio</artifactId>
    <version>3.1.86</version>
</dependency>
```

## 5、接口参数说明
关于接口参数的详细说明可参见：
[a.创建基础版语音训练任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateTrainingBasicJob)

[b.创建进阶版语音训练任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateTrainingMiddleJob)

[c.创建高级版语音训练任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CreateTrainingAdvanceJob)

[d.查询语音训练任务列表](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ListVoiceTrainingJob)

[e.提交语音训练任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=CommitVoiceTrainingJob)

[f.查询语音训练任务详情](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowVoiceTrainingJob)

[g.删除语音训练任务](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=DeleteVoiceTrainingJob)

[h.获取语音文件上传地址](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowJobUploadingAddress)

[i.获取语音训练任务审核结果](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowJobAuditResult)

[j.确认在线录音结果](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ConfirmTrainingSegment)

[k.获取在线录音确认结果](https://console.huaweicloud.com/apiexplorer/#/openapi/MetaStudio/sdk?api=ShowTrainingSegmentInfo)

## 6、关键代码片段

### 6.1、创建基础版语音训练任务
```java
    /**
     * 创建基础版语音训练任务
     *
     */
    private static CreateTrainingBasicJobResponse createTrainingBasicJob(MetaStudioClient client) {
        logger.info("createTrainingBasicJob start");
        CreateTrainingBasicJobRequest request = new CreateTrainingBasicJobRequest();
        request.withBody(new CreateTrainingJobReq().withTag(JobTag.ECOMMERCE)
                .withDescription("<your description>")
                .withSex(SexEnum.FEMALE)
                .withVoiceName("温柔女生")
                .withCreateType(CreateType.SEGMENT));
        CreateTrainingBasicJobResponse response = null;
        try {
            response = client.createTrainingBasicJob(request);
            logger.info("createTrainingBasicJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createTrainingBasicJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("createTrainingBasicJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("createTrainingBasicJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("createTrainingBasicJob ServerResponseException" + e.getMessage());
        }
        return response;
    }
```

### 6.2、创建进阶版语音训练任务
```java
    /**
     * 创建进阶版语音训练任务
     *
     */
    private static String createTrainingMiddleJob(MetaStudioClient client) {
        logger.info("createTrainingMiddleJob start");
        CreateTrainingMiddleJobRequest request = new CreateTrainingMiddleJobRequest();
        request.withBody(new CreateTrainingJobReq()
                .withTag(JobTag.ECOMMERCE)
                .withDescription("<your description>")
                .withSex(SexEnum.FEMALE)
                .withVoiceName("温柔女生")
                .withCreateType(CreateType.PACKAGE));
        String jobId = "";
        try {
            CreateTrainingMiddleJobResponse response = client.createTrainingMiddleJob(request);
            logger.info("createTrainingMiddleJob" + response.toString());
            jobId = response.getJobId();
        } catch (ClientRequestException e) {
            logger.error("createTrainingMiddleJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("createTrainingMiddleJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("createTrainingMiddleJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("createTrainingMiddleJob ServerResponseException" + e.getMessage());
        }
        return jobId;
    }
```

### 6.3、创建高级版语音训练任务
```java
    /**
     * 创建高级版语音训练任务
     *
     */
    private static String createTrainingAdvanceJob(MetaStudioClient client) {
        logger.info("createTrainingAdvanceJob start");
        CreateTrainingAdvanceJobRequest request = new CreateTrainingAdvanceJobRequest();
        request.withBody(new CreateTrainingJobReq()
                .withTag(JobTag.ECOMMERCE)
                .withDescription("<your description>")
                .withSex(SexEnum.FEMALE)
                .withVoiceName("温柔女生")
                .withCreateType(CreateType.PACKAGE));
        String jobId = "";
        try {
            CreateTrainingAdvanceJobResponse response = client.createTrainingAdvanceJob(request);
            logger.info("createTrainingAdvanceJob" + response.toString());
            jobId = response.getJobId();
        } catch (ClientRequestException e) {
            logger.error("createTrainingAdvanceJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("createTrainingAdvanceJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("createTrainingAdvanceJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("createTrainingAdvanceJob ServerResponseException" + e.getMessage());
        }
        return jobId;
    }
```

### 6.4、查询语音训练任务列表
```java
    /**
     * 查询语音训练任务列表
     *
     */
    private static void listVoiceTrainingJob(MetaStudioClient client, String jobId) {
        logger.info("listVoiceTrainingJob start");
        ListVoiceTrainingJobRequest request = new ListVoiceTrainingJobRequest();
        request.withJobId(jobId);
        try {
            ListVoiceTrainingJobResponse response = client.listVoiceTrainingJob(request);
            logger.info("listVoiceTrainingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listVoiceTrainingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("listVoiceTrainingJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("listVoiceTrainingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("listVoiceTrainingJob ServerResponseException" + e.getMessage());
        }
    }
```

### 6.5、提交语音训练任务
```java
    /**
     * 提交语音训练任务
     *
     */
    private static void commitVoiceTrainingJob(MetaStudioClient client, String jobId) {
        logger.info("commitVoiceTrainingJob start");
        CommitVoiceTrainingJobRequest request = new CommitVoiceTrainingJobRequest()
                .withJobId(jobId);
        try {
            CommitVoiceTrainingJobResponse response = client.commitVoiceTrainingJob(request);
            logger.info("commitVoiceTrainingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("commitVoiceTrainingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("commitVoiceTrainingJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("commitVoiceTrainingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("commitVoiceTrainingJob ServerResponseException" + e.getMessage());
        }
    }
```

### 6.6、查询语音训练任务详情
```java
    /**
     * 查询语音训练任务详情
     *
     */
    private static void showVoiceTrainingJob(MetaStudioClient client, String jobId) {
        logger.info("showVoiceTrainingJob start");
        ShowVoiceTrainingJobRequest request = new ShowVoiceTrainingJobRequest();
        request.withJobId(jobId);
        try {
            ShowVoiceTrainingJobResponse response = client.showVoiceTrainingJob(request);
            logger.info("showVoiceTrainingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showVoiceTrainingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("showVoiceTrainingJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showVoiceTrainingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("showVoiceTrainingJob ServerResponseException" + e.getMessage());
        }
    }
```

### 6.7、删除语音训练任务
```java
    /**
     * 删除语音训练任务
     *
     */
    private static void deleteVoiceTrainingJob(MetaStudioClient client, String jobId) {
        logger.info("deleteVoiceTrainingJob start");
        DeleteVoiceTrainingJobRequest request = new DeleteVoiceTrainingJobRequest()
                .withJobId(jobId);
        try {
            DeleteVoiceTrainingJobResponse response = client.deleteVoiceTrainingJob(request);
            logger.info("deleteVoiceTrainingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("deleteVoiceTrainingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("deleteVoiceTrainingJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("deleteVoiceTrainingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("deleteVoiceTrainingJob ServerResponseException" + e.getMessage());
        }
    }
```

### 6.8、获取语音文件上传地址
```java
    /**
     * 获取语音文件上传地址
     *
     */
    private static void showJobUploadingAddress(MetaStudioClient client, String jobId) {
        logger.info("showJobUploadingAddress start");
        ShowJobUploadingAddressRequest request = new ShowJobUploadingAddressRequest();
        request.withJobId(jobId);
        try {
            ShowJobUploadingAddressResponse response = client.showJobUploadingAddress(request);
            logger.info("showJobUploadingAddress" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showJobUploadingAddress ClientRequestException" + e.getHttpStatusCode());
            logger.error("showJobUploadingAddress ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showJobUploadingAddress ServerResponseException" + e.getHttpStatusCode());
            logger.error("showJobUploadingAddress ServerResponseException" + e.getMessage());
        }
    }
```

### 6.9、获取语音训练任务审核结果
```java
    /**
     * 获取语音训练任务审核结果
     *
     */
    private static void showJobAuditResult(MetaStudioClient client, String jobId) {
        logger.info("showJobAuditResult start");
        ShowJobAuditResultRequest request = new ShowJobAuditResultRequest()
                .withJobId(jobId);
        try {
            ShowJobAuditResultResponse response = client.showJobAuditResult(request);
            logger.info("showJobAuditResult" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showJobAuditResult ClientRequestException" + e.getHttpStatusCode());
            logger.error("showJobAuditResult ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showJobAuditResult ServerResponseException" + e.getHttpStatusCode());
            logger.error("showJobAuditResult ServerResponseException" + e.getMessage());
        }
    }
```

### 6.10、确认在线录音结果
```java
    /**
     * 确认在线录音结果
     *
     */
    private static void confirmTrainingSegment(MetaStudioClient client, String jobId) {
        logger.info("confirmTrainingSegment start");
        ConfirmTrainingSegmentRequest request = new ConfirmTrainingSegmentRequest()
                .withJobId(jobId)
                .withIndex(0);
        try {
            ConfirmTrainingSegmentResponse response = client.confirmTrainingSegment(request);
            logger.info("confirmTrainingSegment" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("confirmTrainingSegment ClientRequestException" + e.getHttpStatusCode());
            logger.error("confirmTrainingSegment ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("confirmTrainingSegment ServerResponseException" + e.getHttpStatusCode());
            logger.error("confirmTrainingSegment ServerResponseException" + e.getMessage());
        }
    }
```

### 6.11、获取在线录音确认结果
```java
    /**
     * 获取在线录音确认结果
     *
     */
    private static void showTrainingSegmentInfo(MetaStudioClient client, String jobId) {
        logger.info("showTrainingSegmentInfo start");
        ShowTrainingSegmentInfoRequest request = new ShowTrainingSegmentInfoRequest()
                .withJobId(jobId);
        try {
            ShowTrainingSegmentInfoResponse response = client.showTrainingSegmentInfo(request);
            logger.info("showTrainingSegmentInfo" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showTrainingSegmentInfo ClientRequestException" + e.getHttpStatusCode());
            logger.error("showTrainingSegmentInfo ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showTrainingSegmentInfo ServerResponseException" + e.getHttpStatusCode());
            logger.error("showTrainingSegmentInfo ServerResponseException" + e.getMessage());
        }
    }
```

## 7、运行结果
**创建基础版语音训练任务**
```json
{
  "job_id": "26f065***",
  "training_data_uploading_url": "https://***",
  "segment_uploading_url": {
    "audio_uploading_url": [
      "https://***"
    ],
    "txt_uploading_url": [
      "https://***"
    ]
  },
  "authorization_letter_uploading_url": "https://***"
}
```

**创建进阶版语音训练任务**
```json
{
  "job_id": "ae2bfe***",
  "training_data_uploading_url": "https://***",
  "segment_uploading_url": {
    "audio_uploading_url": [
      "https://***"
    ],
    "txt_uploading_url": [
      "https://***"
    ]
  },
  "authorization_letter_uploading_url": "https://***"
}
```

**创建高级版语音训练任务**
```json
{
  "job_id": "bcd64003xxx",
  "training_data_uploading_url": "https://***",
  "segment_uploading_url": {
    "audio_uploading_url": [
      "https://***"
    ],
    "txt_uploading_url": [
      "https://***"
    ]
  },
  "authorization_letter_uploading_url": "https://***"
}
```

**查询语音训练任务列表**
```json
{
  "count": 2147483647,
  "jobs": [
    {
      "job_type": "BASIC",
      "job_id": "502567***",
      "app_user_id": null,
      "voice_name": "温柔女生",
      "sex": "FEMALE",
      "language": "CN",
      "state": "CREATING",
      "asset_id": null,
      "job_failed_code": null,
      "job_failed_reason": null,
      "create_time": 2147483647,
      "lastupdate_time": 2147483647,
      "voice_authorization_url": null,
      "create_type": "SEGMENT",
      "tag": "ECOMMERCE"
    }
  ]
}
```

**提交语音训练任务**
```json
{}
```

**查询语音训练任务详情**
```json
{
  "job_type": "BASIC",
  "job_id": "502567***",
  "app_user_id": null,
  "voice_name": "温柔女生",
  "sex": "FEMALE",
  "language": "CN",
  "state": "CREATING",
  "asset_id": null,
  "job_failed_code": null,
  "job_failed_reason": null,
  "create_time": 2147483647,
  "lastupdate_time": 2147483647,
  "voice_authorization_url": null,
  "create_type": "SEGMENT",
  "tag": "ECOMMERCE"
}
```

**删除语音训练任务**
```json
{}
```

**获取语音文件上传地址**
```json
{
  "segment_url": {
    "audio_uploading_url": [
      "https://***"
    ],
    "txt_uploading_url": [
      "https://***"
    ]
  },
  "package_url": null,
  "authorization_letter_uploading_url": "https://***"
}
```

**获取语音训练任务审核结果**
```json
{
    "systemAuditResult": null,
    "adminAuditResult": null
}
```

**确认在线录音结果**
```json
{
    "confirmResult": true,
    "unmatchedIndexHit": null
}
```

**获取在线录音确认结果**
```json
{
    "confirmedIndex": [0]
}
```

## 8、参考
本示例的代码工程仅用于简单演示，实际开发过程中应严格遵循开发指南。访问以下链接可以获取详细信息：[开发指南](https://support.huaweicloud.com/ssdk-metastudio/metastudio_06_0000.html)