package com.huawei.hwclouds.metastudio.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.metastudio.v1.MetaStudioClient;
import com.huaweicloud.sdk.metastudio.v1.model.CommitVoiceTrainingJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CommitVoiceTrainingJobResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ConfirmTrainingSegmentRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ConfirmTrainingSegmentResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CreateTrainingAdvanceJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreateTrainingAdvanceJobResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CreateTrainingBasicJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreateTrainingBasicJobResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CreateTrainingJobReq;
import com.huaweicloud.sdk.metastudio.v1.model.CreateTrainingJobReq.SexEnum;
import com.huaweicloud.sdk.metastudio.v1.model.CreateTrainingMiddleJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.CreateTrainingMiddleJobResponse;
import com.huaweicloud.sdk.metastudio.v1.model.CreateType;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteVoiceTrainingJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.DeleteVoiceTrainingJobResponse;
import com.huaweicloud.sdk.metastudio.v1.model.JobTag;
import com.huaweicloud.sdk.metastudio.v1.model.ListVoiceTrainingJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ListVoiceTrainingJobResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ShowJobAuditResultRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ShowJobAuditResultResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ShowJobUploadingAddressRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ShowJobUploadingAddressResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ShowTrainingSegmentInfoRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ShowTrainingSegmentInfoResponse;
import com.huaweicloud.sdk.metastudio.v1.model.ShowVoiceTrainingJobRequest;
import com.huaweicloud.sdk.metastudio.v1.model.ShowVoiceTrainingJobResponse;
import com.huaweicloud.sdk.metastudio.v1.region.MetaStudioRegion;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;


public class TrainingJobManager {

    private static final Logger logger = LoggerFactory.getLogger(TrainingJobManager.class);

    // 获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID和您的AK/SK。
    // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK、HUAWEICLOUD_SDK_SK。
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");

    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    public static void main(String[] args) throws IOException {
        ICredential auth = new BasicCredentials()
                .withAk(AK)
                .withSk(SK);

        // 使用默认配置
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        // 创建MetaStudio实例
        MetaStudioClient mClient = MetaStudioClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(MetaStudioRegion.CN_NORTH_4)
                .build();

        // 1,创建基础版语音训练任务
        CreateTrainingBasicJobResponse createTrainingBasicJobResponse = createTrainingBasicJob(mClient);
        String basicJobId = createTrainingBasicJobResponse.getJobId();
        String txtUploadingUrl = createTrainingBasicJobResponse.getSegmentUploadingUrl().getTxtUploadingUrl().get(0);
        String audioUploadingUrl = createTrainingBasicJobResponse.getSegmentUploadingUrl().getAudioUploadingUrl().get(0);
        String authorizationLetterUploadingUrl = createTrainingBasicJobResponse.getAuthorizationLetterUploadingUrl();
        // 2,创建进阶版语音训练任务
        String middleJobId = createTrainingMiddleJob(mClient);

        // 3,创建高级版语音训练任务
        String advanceJobId = createTrainingAdvanceJob(mClient);

        // 4,查询语音训练任务列表
        listVoiceTrainingJob(mClient, basicJobId);

        // 5,查询语音训练任务详情
        showVoiceTrainingJob(mClient, basicJobId);

        // 6,获取语音文件上传地址
        showJobUploadingAddress(mClient, basicJobId);

        // 7,确认在线录音结果
        // 上传文本文件到obs
        Map<String, String> txtHeaders = new HashMap<String, String>();
        txtHeaders.put("Content-Type", "text/plain");
        uploadFileToObs(txtUploadingUrl, "XXX.txt", txtHeaders);
        // 上传音频文件到obs
        Map<String, String> audioHeaders = new HashMap<String, String>();
        audioHeaders.put("Content-Type", "audio/wav");
        uploadFileToObs(audioUploadingUrl, "XXX/24k_wsh_0.wav", audioHeaders);
        // 上传授权书到obs
        Map<String, String> authorHeaders = new HashMap<String, String>();
        authorHeaders.put("Content-Type", "application/octet-stream");
        uploadFileToObs(authorizationLetterUploadingUrl, "XXX.pdf", authorHeaders);
        confirmTrainingSegment(mClient, basicJobId);

        // 8,获取在线录音确认结果
        showTrainingSegmentInfo(mClient, basicJobId);

        // 9,提交语音训练任务
        commitVoiceTrainingJob(mClient, basicJobId);

        // 10,获取语音训练任务审核结果
        showJobAuditResult(mClient, basicJobId);

        // 11,删除语音训练任务
        deleteVoiceTrainingJob(mClient, basicJobId);
        deleteVoiceTrainingJob(mClient, middleJobId);
        deleteVoiceTrainingJob(mClient, advanceJobId);
    }

    /**
     * 创建基础版语音训练任务
     *
     */
    private static CreateTrainingBasicJobResponse createTrainingBasicJob(MetaStudioClient client) {
        logger.info("createTrainingBasicJob start");
        CreateTrainingBasicJobRequest request = new CreateTrainingBasicJobRequest();
        request.withBody(new CreateTrainingJobReq().withTag(JobTag.ECOMMERCE)
                .withDescription("<your description>")
                .withSex(SexEnum.FEMALE)
                .withVoiceName("温柔女生")
                .withCreateType(CreateType.SEGMENT));
        CreateTrainingBasicJobResponse response = null;
        try {
            response = client.createTrainingBasicJob(request);
            logger.info("createTrainingBasicJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createTrainingBasicJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("createTrainingBasicJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("createTrainingBasicJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("createTrainingBasicJob ServerResponseException" + e.getMessage());
        }
        return response;
    }


    /**
     * 创建进阶版语音训练任务
     *
     */
    private static String createTrainingMiddleJob(MetaStudioClient client) {
        logger.info("createTrainingMiddleJob start");
        CreateTrainingMiddleJobRequest request = new CreateTrainingMiddleJobRequest();
        request.withBody(new CreateTrainingJobReq()
                .withTag(JobTag.ECOMMERCE)
                .withDescription("<your description>")
                .withSex(SexEnum.FEMALE)
                .withVoiceName("温柔女生")
                .withCreateType(CreateType.PACKAGE));
        String jobId = "";
        try {
            CreateTrainingMiddleJobResponse response = client.createTrainingMiddleJob(request);
            logger.info("createTrainingMiddleJob" + response.toString());
            jobId = response.getJobId();
        } catch (ClientRequestException e) {
            logger.error("createTrainingMiddleJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("createTrainingMiddleJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("createTrainingMiddleJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("createTrainingMiddleJob ServerResponseException" + e.getMessage());
        }
        return jobId;
    }


    /**
     * 创建高级版语音训练任务
     *
     */
    private static String createTrainingAdvanceJob(MetaStudioClient client) {
        logger.info("createTrainingAdvanceJob start");
        CreateTrainingAdvanceJobRequest request = new CreateTrainingAdvanceJobRequest();
        request.withBody(new CreateTrainingJobReq()
                .withTag(JobTag.ECOMMERCE)
                .withDescription("<your description>")
                .withSex(SexEnum.FEMALE)
                .withVoiceName("温柔女生")
                .withCreateType(CreateType.PACKAGE));
        String jobId = "";
        try {
            CreateTrainingAdvanceJobResponse response = client.createTrainingAdvanceJob(request);
            logger.info("createTrainingAdvanceJob" + response.toString());
            jobId = response.getJobId();
        } catch (ClientRequestException e) {
            logger.error("createTrainingAdvanceJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("createTrainingAdvanceJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("createTrainingAdvanceJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("createTrainingAdvanceJob ServerResponseException" + e.getMessage());
        }
        return jobId;
    }


    /**
     * 查询语音训练任务列表
     *
     */
    private static void listVoiceTrainingJob(MetaStudioClient client, String jobId) {
        logger.info("listVoiceTrainingJob start");
        ListVoiceTrainingJobRequest request = new ListVoiceTrainingJobRequest();
        request.withJobId(jobId);
        try {
            ListVoiceTrainingJobResponse response = client.listVoiceTrainingJob(request);
            logger.info("listVoiceTrainingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listVoiceTrainingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("listVoiceTrainingJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("listVoiceTrainingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("listVoiceTrainingJob ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 提交语音训练任务
     *
     */
    private static void commitVoiceTrainingJob(MetaStudioClient client, String jobId) {
        logger.info("commitVoiceTrainingJob start");
        CommitVoiceTrainingJobRequest request = new CommitVoiceTrainingJobRequest()
                .withJobId(jobId);
        try {
            CommitVoiceTrainingJobResponse response = client.commitVoiceTrainingJob(request);
            logger.info("commitVoiceTrainingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("commitVoiceTrainingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("commitVoiceTrainingJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("commitVoiceTrainingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("commitVoiceTrainingJob ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 查询语音训练任务详情
     *
     */
    private static void showVoiceTrainingJob(MetaStudioClient client, String jobId) {
        logger.info("showVoiceTrainingJob start");
        ShowVoiceTrainingJobRequest request = new ShowVoiceTrainingJobRequest();
        request.withJobId(jobId);
        try {
            ShowVoiceTrainingJobResponse response = client.showVoiceTrainingJob(request);
            logger.info("showVoiceTrainingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showVoiceTrainingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("showVoiceTrainingJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showVoiceTrainingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("showVoiceTrainingJob ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 删除语音训练任务
     *
     */
    private static void deleteVoiceTrainingJob(MetaStudioClient client, String jobId) {
        logger.info("deleteVoiceTrainingJob start");
        DeleteVoiceTrainingJobRequest request = new DeleteVoiceTrainingJobRequest()
                .withJobId(jobId);
        try {
            DeleteVoiceTrainingJobResponse response = client.deleteVoiceTrainingJob(request);
            logger.info("deleteVoiceTrainingJob" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("deleteVoiceTrainingJob ClientRequestException" + e.getHttpStatusCode());
            logger.error("deleteVoiceTrainingJob ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("deleteVoiceTrainingJob ServerResponseException" + e.getHttpStatusCode());
            logger.error("deleteVoiceTrainingJob ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 获取语音文件上传地址
     *
     */
    private static void showJobUploadingAddress(MetaStudioClient client, String jobId) {
        logger.info("showJobUploadingAddress start");
        ShowJobUploadingAddressRequest request = new ShowJobUploadingAddressRequest();
        request.withJobId(jobId);
        try {
            ShowJobUploadingAddressResponse response = client.showJobUploadingAddress(request);
            logger.info("showJobUploadingAddress" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showJobUploadingAddress ClientRequestException" + e.getHttpStatusCode());
            logger.error("showJobUploadingAddress ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showJobUploadingAddress ServerResponseException" + e.getHttpStatusCode());
            logger.error("showJobUploadingAddress ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 获取语音训练任务审核结果
     *
     */
    private static void showJobAuditResult(MetaStudioClient client, String jobId) {
        logger.info("showJobAuditResult start");
        ShowJobAuditResultRequest request = new ShowJobAuditResultRequest()
                .withJobId(jobId);
        try {
            ShowJobAuditResultResponse response = client.showJobAuditResult(request);
            logger.info("showJobAuditResult" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showJobAuditResult ClientRequestException" + e.getHttpStatusCode());
            logger.error("showJobAuditResult ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showJobAuditResult ServerResponseException" + e.getHttpStatusCode());
            logger.error("showJobAuditResult ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 确认在线录音结果
     *
     */
    private static void confirmTrainingSegment(MetaStudioClient client, String jobId) {
        logger.info("confirmTrainingSegment start");
        ConfirmTrainingSegmentRequest request = new ConfirmTrainingSegmentRequest()
                .withJobId(jobId)
                .withIndex(0);
        try {
            ConfirmTrainingSegmentResponse response = client.confirmTrainingSegment(request);
            logger.info("confirmTrainingSegment" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("confirmTrainingSegment ClientRequestException" + e.getHttpStatusCode());
            logger.error("confirmTrainingSegment ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("confirmTrainingSegment ServerResponseException" + e.getHttpStatusCode());
            logger.error("confirmTrainingSegment ServerResponseException" + e.getMessage());
        }
    }


    /**
     * 获取在线录音确认结果
     *
     */
    private static void showTrainingSegmentInfo(MetaStudioClient client, String jobId) {
        logger.info("showTrainingSegmentInfo start");
        ShowTrainingSegmentInfoRequest request = new ShowTrainingSegmentInfoRequest()
                .withJobId(jobId);
        try {
            ShowTrainingSegmentInfoResponse response = client.showTrainingSegmentInfo(request);
            logger.info("showTrainingSegmentInfo" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showTrainingSegmentInfo ClientRequestException" + e.getHttpStatusCode());
            logger.error("showTrainingSegmentInfo ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showTrainingSegmentInfo ServerResponseException" + e.getHttpStatusCode());
            logger.error("showTrainingSegmentInfo ServerResponseException" + e.getMessage());
        }
    }

    /**
     * 上传音频资产到obs
     *
     */
    private static void uploadFileToObs(String upload_url, String filePath, Map<String, String> headers)
            throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(upload_url).openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("PUT");
        Iterator entries = headers.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry entry = (Map.Entry) entries.next();
            connection.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
        }
        connection.connect();
        // 写入文件数据
        OutputStream outputStream = connection.getOutputStream();
        try (FileInputStream inputStream = new FileInputStream(new File(filePath))) {
            int length;
            byte[] bys = new byte[1024];
            while ((length = inputStream.read(bys)) != -1) {
                outputStream.write(bys, 0, length);
            }
            String nextLine = "\r\n";
            outputStream.write(nextLine.getBytes(StandardCharsets.UTF_8));

            try (OutputStream os = connection.getOutputStream()) {
                os.write("{\"key\": \"value\"}".getBytes(StandardCharsets.UTF_8));
            }
            int responseCode = connection.getResponseCode();
            logger.info("Response Code: " + responseCode);

            // 获取返回值
            InputStream response = connection.getInputStream();
            try (InputStreamReader reader = new InputStreamReader(response, StandardCharsets.UTF_8)) {
                while (reader.read() != -1) {
                    logger.info(new String(bys, "UTF-8"));
                }
                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    logger.info(connection.getResponseMessage());
                    logger.info("upload success ");
                } else {
                    logger.error("upload failed");
                }
            } catch (IOException ex) {
                logger.error("response stream open failed ");
            } finally {
                response.close();
            }
        } catch (IOException e) {
            logger.error("file input stream open failed ");
        } finally {
            outputStream.close();
        }
    }
}