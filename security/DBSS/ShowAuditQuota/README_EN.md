### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/DBSS/doc?api=ShowAuditQuota).
2.Querying Account Quota Information

3.This API is used to querying Account Quota Information.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The current account has the permission to use DBSS.

6.An instance has been created in DBSS.

7.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-dbss. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-dbss</artifactId>
    <version>3.1.124</version>
</dependency>
```

### Sample Code

``` java
package com.huawei.dbss;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.dbss.v1.DbssClient;
import com.huaweicloud.sdk.dbss.v1.model.ShowAuditQuotaRequest;
import com.huaweicloud.sdk.dbss.v1.model.ShowAuditQuotaResponse;
import com.huaweicloud.sdk.dbss.v1.region.DbssRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ShowAuditQuota {
    private static final Logger logger = LoggerFactory.getLogger(ShowAuditQuota.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to DbssRegion.AP_SOUTHEAST_3.
        DbssClient client = DbssClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(DbssRegion.AP_SOUTHEAST_3)
                .build();

        // Build Request
        ShowAuditQuotaRequest request = new ShowAuditQuotaRequest();

        try {
            // Send a request
            ShowAuditQuotaResponse response = client.showAuditQuota(request);
            // Print the response result.
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}
```

### Example of the returned result
```
{
  "project_id" : "0250cb8a80c24c***********************",
  "cpu" : 796,
  "ram" : 1622016,
  "audit_quota" : 1
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/09/26 | 1.0 | This document is released for the first time.