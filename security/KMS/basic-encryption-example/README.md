## 1、介绍
**什么是密钥管理服务？**

[密钥管理服务](https://support.huaweicloud.com/productdesc-dew/dew_01_0001.html)，密钥管理，即密钥管理服务（Key Management Service，KMS），是一种安全、可靠、简单易用的密钥托管服务，帮助您轻松创建和管理密钥，保护密钥的安全。KMS通过使用硬件安全模块（Hardware Security Module，HSM）保护密钥安全，帮助用户轻松创建和管理密钥，所有的用户密钥都由HSM中的根密钥保护，避免密钥泄露。 当有大量数据（例如：照片、视频或者数据库文件等）需要加解密时，用户可采用信封加密方式加解密数据，无需通过网络传输大量数据即可完成数据加解密。

**您将学到什么？**

如何通过java版本的KMS SDK方式使用用户主密钥（CMK）进行字符串加解密

## 2、前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云租户账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 3、华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。
- 4、用户需要在密钥管理控制台创建用户主密钥，算法为AES_256。

## 3、SDK获取和安装
您可以通过Maven配置所依赖的密钥管理服务SDK
具体的SDK版本号请参见[SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/KMS?lang=Java)

## 4、接口参数说明
关于接口参数的详细说明可参见：

[a.加密数据](https://console.huaweicloud.com/apiexplorer/#/openapi/KMS/doc?api=EncryptData)

[b.解密数据](https://console.huaweicloud.com/apiexplorer/#/openapi/KMS/doc?api=DecryptData)


## 5、代码示例
如何通过java版本的KMS SDK方式使用用户主密钥（CMK）进行字符串加解密
```java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.kms.v2.KmsClient;
import com.huaweicloud.sdk.kms.v2.model.DecryptDataRequest;
import com.huaweicloud.sdk.kms.v2.model.DecryptDataRequestBody;
import com.huaweicloud.sdk.kms.v2.model.DecryptDataResponse;
import com.huaweicloud.sdk.kms.v2.model.EncryptDataRequest;
import com.huaweicloud.sdk.kms.v2.model.EncryptDataRequestBody;
import com.huaweicloud.sdk.kms.v2.model.EncryptDataResponse;


/**
 * 小数据加解密：使用用户主密钥（CMK）进行字符串加解密
 * 激活assert语法，请在VM_OPTIONS中添加-ea
 */
public class BasicEncryptionExample {

    /**
     * 基础认证信息：
     * 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
     * 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
     * - ACCESS_KEY: 华为云账号Access Key
     * - SECRET_ACCESS_KEY: 华为云账号Secret Access Key, 敏感信息，建议密文存储
     * - IAM_ENDPOINT: 华为云IAM服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?IAM
     * - KMS_REGION_ID: 华为云KMS支持的地域,详情见https://developer.huaweicloud.com/endpoint?DEW
     * - KMS_ENDPOINT: 华为云KMS服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?DEW
     */
    private static final String ACCESS_KEY = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SECRET_ACCESS_KEY = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String IAM_ENDPOINT = "https://<IamEndpoint>";
    private static final String KMS_REGION_ID = "<RegionId>";
    private static final String KMS_ENDPOINT = "https://<KmsEndpoint>";
    private static final String PLAIN_TEXT = "Hello World!";


    public static void main(final String[] args) {
        // 用户主密钥ID
        final String keyId = args[0];

        encryptAndDecrypt(keyId);
    }

    /**
     * 加解密数据实例
     *
     * @param keyId 用户主密钥ID
     */
    static void encryptAndDecrypt(String keyId) {

        // 1.准备访问华为云的认证信息
        final BasicCredentials auth = new BasicCredentials()
                .withIamEndpoint(IAM_ENDPOINT).withAk(ACCESS_KEY).withSk(SECRET_ACCESS_KEY);

        // 2.初始化SDK，传入认证信息及KMS访问终端地址
        final KmsClient kmsClient = KmsClient.newBuilder()
                .withRegion(new Region(KMS_REGION_ID, KMS_ENDPOINT)).withCredential(auth).build();

        // 3.组装加密请求信息
        final EncryptDataRequest encryptDataRequest = new EncryptDataRequest()
                .withBody(new EncryptDataRequestBody().withKeyId(keyId).withPlainText(PLAIN_TEXT));

        // 4.加密数据
        final EncryptDataResponse encryptDataResponse = kmsClient.encryptData(encryptDataRequest);

        // 5.组装解密请求信息
        final DecryptDataRequest decryptDataRequest = new DecryptDataRequest()
                .withBody(new DecryptDataRequestBody().withCipherText(encryptDataResponse.getCipherText()));

        // 6.解密数据
        final DecryptDataResponse decryptDataResponse = kmsClient.decryptData(decryptDataRequest);

        assert PLAIN_TEXT.equals(decryptDataResponse.getPlainText());
    }

}

```

## 6、修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2021-11-22 | 1.0 | 文档首次发布 |