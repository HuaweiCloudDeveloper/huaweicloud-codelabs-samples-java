package com.huawei.kms;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.kms.v2.KmsClient;
import com.huaweicloud.sdk.kms.v2.model.DecryptDataRequest;
import com.huaweicloud.sdk.kms.v2.model.DecryptDataRequestBody;
import com.huaweicloud.sdk.kms.v2.model.DecryptDataResponse;
import com.huaweicloud.sdk.kms.v2.model.EncryptDataRequest;
import com.huaweicloud.sdk.kms.v2.model.EncryptDataRequestBody;
import com.huaweicloud.sdk.kms.v2.model.EncryptDataResponse;


/**
 * 小数据加解密：使用用户主密钥（CMK）进行字符串加解密
 * 激活assert语法，请在VM_OPTIONS中添加-ea
 */
public class BasicEncryptionExample {

    /**
     * 基础认证信息：
     * 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
     * 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
     * - ACCESS_KEY: 华为云账号Access Key
     * - SECRET_ACCESS_KEY: 华为云账号Secret Access Key, 敏感信息，建议密文存储
     * - IAM_ENDPOINT: 华为云IAM服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?IAM
     * - KMS_REGION_ID: 华为云KMS支持的地域,详情见https://developer.huaweicloud.com/endpoint?DEW
     * - KMS_ENDPOINT: 华为云KMS服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?DEW
     */

    private static final String ACCESS_KEY = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SECRET_ACCESS_KEY = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String IAM_ENDPOINT = "https://<IamEndpoint>";
    private static final String KMS_REGION_ID = "<RegionId>";
    private static final String KMS_ENDPOINT = "https://<KmsEndpoint>";
    private static final String PLAIN_TEXT = "Hello World!";


    public static void main(final String[] args) {
        // 用户主密钥ID
        final String keyId = args[0];

        encryptAndDecrypt(keyId);
    }

    /**
     * 加解密数据实例
     *
     * @param keyId 用户主密钥ID
     */
    static void encryptAndDecrypt(String keyId) {

        // 1.准备访问华为云的认证信息
        final BasicCredentials auth = new BasicCredentials()
                .withIamEndpoint(IAM_ENDPOINT).withAk(ACCESS_KEY).withSk(SECRET_ACCESS_KEY);

        // 2.初始化SDK，传入认证信息及KMS访问终端地址
        final KmsClient kmsClient = KmsClient.newBuilder()
                .withRegion(new Region(KMS_REGION_ID, KMS_ENDPOINT)).withCredential(auth).build();

        // 3.组装加密请求信息
        final EncryptDataRequest encryptDataRequest = new EncryptDataRequest()
                .withBody(new EncryptDataRequestBody().withKeyId(keyId).withPlainText(PLAIN_TEXT));

        // 4.加密数据
        final EncryptDataResponse encryptDataResponse = kmsClient.encryptData(encryptDataRequest);

        // 5.组装解密请求信息
        final DecryptDataRequest decryptDataRequest = new DecryptDataRequest()
                .withBody(new DecryptDataRequestBody().withCipherText(encryptDataResponse.getCipherText()));

        // 6.解密数据
        final DecryptDataResponse decryptDataResponse = kmsClient.decryptData(decryptDataRequest);

        assert PLAIN_TEXT.equals(decryptDataResponse.getPlainText());
    }

}
