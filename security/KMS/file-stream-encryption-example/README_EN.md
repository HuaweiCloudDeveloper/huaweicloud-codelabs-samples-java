## 1. Introduction
**What Is Key Management Service?**

[Key Management Service](https://support.huaweicloud.com/intl/en-us/productdesc-dew/dew_01_0121.html), Key Management Service (KMS) is a secure, reliable, and easy-to-use key hosting service. It uses hardware security modules (HSMs) to keep your keys secure and help you easily create and manage keys. All keys are protected by the root keys in HSMs to avoid key leakage. If you want to encrypt or decrypt large volumes of data, such as pictures, videos, and database files, you can use the envelope encryption method, where the data does not need to be transferred over the network.

**What You Will Learn**

How to use a data encryption key (DEK) to encrypt and decrypt files using KMS JAVA SDK

## 2. Preparations
- You have obtained the Huawei Cloud SDK. You can also install the JAVA SDK.
- You have obtained a pair of access key ID (AK) and secret access key (SK) for your Huawei Cloud account. To view your AKs/SKs, go to the Huawei Cloud console, hover the cursor over the username in the upper right corner, and choose My Credentials from the drop-down list. Then, in the navigation pane on the left, choose Access Keys. For details, see [Access Keys](https://support.huaweicloud.com/en-us/usermanual-ca/en-us_topic_0046606340.html).
- You have set up the development environment with Java JDK 1.8 or later.
- You have created a CMK using the AES_256 algorithm on the KMS console.
- You have prepared the FirstPlainFile.jpg file.

## 3. Install SDKs
You can obtain and install an SDK in Maven mode. You need to download and install Maven in your operating system (OS). After the installation is complete, you only need to add the corresponding dependencies to the pom.xml file of the Java project.

Before using the service SDK, you need to install huaweicloud-sdk-kms. For details about the SDK version, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/KMS?lang=Java).

```xml
<dependency>
	<groupId>com.huaweicloud.sdk</groupId>
	<artifactId>huaweicloud-sdk-kms</artifactId>
	<version>3.1.116</version>
</dependency>
```

## 4. Interface parameter description
Detailed descriptions of interface parameters can be found at:

[a.Create a DEK](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/KMS/doc?api=CreateDatakey)

[b.Decrypt a DEK](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/KMS/doc?api=DecryptDatakey)


## 5. Code Sample
How to use a data encryption key (DEK) to encrypt and decrypt files using KMS JAVA SDK
```java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.kms.v2.KmsClient;
import com.huaweicloud.sdk.kms.v2.model.CreateDatakeyRequest;
import com.huaweicloud.sdk.kms.v2.model.CreateDatakeyRequestBody;
import com.huaweicloud.sdk.kms.v2.model.CreateDatakeyResponse;
import com.huaweicloud.sdk.kms.v2.model.DecryptDatakeyRequest;
import com.huaweicloud.sdk.kms.v2.model.DecryptDatakeyRequestBody;

import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Use data key (DEK) for file encryption and decryption
 * To activate assert syntax, add -ea in VM_OPTIONS
 * You have prepared the FirstPlainFile.jpg file.
 */
public class FileStreamEncryptionExample {

    /**
     * Basic certification information:
     * Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
     * In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
     * - ACCESS_KEY: AK of the Huawei Cloud account
     * - SECRET_ACCESS_KEY: SK of the Huawei Cloud account. This is sensitive information. Store this in ciphertext.
     * - IAM_ENDPOINT: endpoint for accessing IAM. For details, see https://developer.huaweicloud.com/intl/en-us/endpoint?IAM
     * - KMS_REGION_ID: regions supported by KMS. For details, see https://developer.huaweicloud.com/intl/en-us/endpoint?DEW
     * - KMS_ENDPOINT: endpoint for accessing KMS. For details, see https://developer.huaweicloud.com/intl/en-us/endpoint?DEW
     */
    private static final String ACCESS_KEY = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SECRET_ACCESS_KEY = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String IAM_ENDPOINT = "https://<IamEndpoint>";
    private static final String KMS_REGION_ID = "<RegionId>";
    private static final String KMS_ENDPOINT = "https://<KmsEndpoint>";


    /**
     * AES Algorithm related identifiers：
     * - AES_KEY_BIT_LENGTH: AES256 key bit length
     * - AES_KEY_BYTE_LENGTH: AES256 key byte length
     * - AES_ALG: AES256 algorithm, this example uses GCM for grouping mode and NoPadding for filling.
     * - AES_FLAG: AES algorithm ID
     * - GCM_TAG_LENGTH: GCM TAG length
     * - GCM_IV_LENGTH: GCM IV length
     */
    private static final String AES_KEY_BIT_LENGTH = "256";
    private static final String AES_KEY_BYTE_LENGTH = "32";
    private static final String AES_ALG = "AES/GCM/NoPadding";
    private static final String AES_FLAG = "AES";
    private static final int GCM_TAG_LENGTH = 16;
    private static final int GCM_IV_LENGTH = 12;

    public static void main(final String[] args) {
        // CMK ID
        final String keyId = args[0];

        encryptAndDecryptFile(keyId);
    }

    /**
     * Use data key (DEK) for file encryption and decryption
     *
     * @param keyId customer master key ID
     */
    static void encryptAndDecryptFile(String keyId) {
        // 1.Prepare the authentication information.
        final BasicCredentials auth = new BasicCredentials()
                .withIamEndpoint(IAM_ENDPOINT).withAk(ACCESS_KEY).withSk(SECRET_ACCESS_KEY);

        // 2.Initialize the SDK and import the authentication and KMS endpoint information.
        final KmsClient kmsClient = KmsClient.newBuilder()
                .withRegion(new Region(KMS_REGION_ID, KMS_ENDPOINT)).withCredential(auth).build();

        // 3.Unified create data key request information
        final CreateDatakeyRequest createDatakeyRequest = new CreateDatakeyRequest()
                .withBody(new CreateDatakeyRequestBody().withKeyId(keyId).withDatakeyLength(AES_KEY_BIT_LENGTH));

        // 4.Send request
        final CreateDatakeyResponse createDatakeyResponse = kmsClient.createDatakey(createDatakeyRequest);

        // 5.Receive created data key information
        // It is recommended to store the ciphertext key and KeyId locally to facilitate obtaining the plaintext key when decrypting data.
        // The plaintext key is used immediately after creation. The hexadecimal plaintext key needs to be converted into a byte array before use.
        final String cipherText = createDatakeyResponse.getCipherText();
        final byte[] plainKey = hexToBytes(createDatakeyResponse.getPlainText());

        // 6.Prepare files to be encrypted
        // inFile Original file to be encrypted
        // outEncryptFile Encrypted file
        // outDecryptFile Encrypted and then decrypted file
        final File inFile = new File("FirstPlainFile.jpg");
        final File outEncryptFile = new File("SecondEncryptFile.jpg");
        final File outDecryptFile = new File("ThirdDecryptFile.jpg");

        // 7.Generate nonce.
        final byte[] iv = new byte[GCM_IV_LENGTH];
        final SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(iv);

        // 8.Encrypt file and store encrypted file
        doFileFinal(Cipher.ENCRYPT_MODE, inFile, outEncryptFile, plainKey, iv);

        // 9.Unified decrypt data key request information
        final DecryptDatakeyRequest decryptDatakeyRequest = new DecryptDatakeyRequest()
                .withBody(new DecryptDatakeyRequestBody()
                        .withKeyId(keyId).withCipherText(cipherText).withDatakeyCipherLength(AES_KEY_BYTE_LENGTH));

        // 10. Decrypt the data key and replace the returned hexadecimal plaintext key with a byte array
        final byte[] decryptDataKey = hexToBytes(kmsClient.decryptDatakey(decryptDatakeyRequest).getDataKey());

        // 12. Decrypt the file and store the decrypted file
        doFileFinal(Cipher.DECRYPT_MODE, outEncryptFile, outDecryptFile, decryptDataKey, iv);

        // 13. Compare the original file with the encrypted and then decrypted file
        assert getFileSha256Sum(inFile).equals(getFileSha256Sum(outDecryptFile));

    }

    /**
     * Encrypt and decrypt files
     *
     * @param cipherMode Encryption mode, optional value is Cipher.ENCRYPT_MODE or Cipher.DECRYPT_MODE
     * @param infile     Original file to be encrypted
     * @param outFile    Encrypted file
     * @param keyPlain   plain key
     * @param iv         iv
     */
    static void doFileFinal(int cipherMode, File infile, File outFile, byte[] keyPlain, byte[] iv) {

        try (BufferedInputStream bis = new BufferedInputStream(Files.newInputStream(infile.toPath()));
             BufferedOutputStream bos = new BufferedOutputStream(Files.newOutputStream(outFile.toPath()))) {
            final byte[] bytIn = new byte[(int) infile.length()];
            final int fileLength = bis.read(bytIn);

            assert fileLength > 0;

            final SecretKeySpec secretKeySpec = new SecretKeySpec(keyPlain, AES_FLAG);
            final Cipher cipher = Cipher.getInstance(AES_ALG);
            final GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * Byte.SIZE, iv);
            cipher.init(cipherMode, secretKeySpec, gcmParameterSpec);
            final byte[] bytOut = cipher.doFinal(bytIn);
            bos.write(bytOut);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Convert hexadecimal string to byte array
     *
     * @param hexString hexadecimal string
     * @return byte array
     */
    static byte[] hexToBytes(String hexString) {
        final int stringLength = hexString.length();
        assert stringLength > 0;
        final byte[] result = new byte[stringLength / 2];
        int j = 0;
        for (int i = 0; i < stringLength; i += 2) {
            result[j++] = (byte) Integer.parseInt(hexString.substring(i, i + 2), 16);
        }
        return result;
    }

    /**
     * Calculate file SHA256 digest
     *
     * @param file file
     * @return SHA256 digest
     */
    static String getFileSha256Sum(File file) {
        int length;
        MessageDigest sha256;
        byte[] buffer = new byte[1024];
        try {
            sha256 = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }
        try (FileInputStream inputStream = new FileInputStream(file)) {
            while ((length = inputStream.read(buffer)) != -1) {
                sha256.update(buffer, 0, length);
            }
            return new BigInteger(1, sha256.digest()).toString(16);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

}

```

## 6. Change History

| Release Date | Issue|   Description  |
|:------------:| :------: | :----------: |
|  2024-9-30   |   1.0    | This issue is the first official release.|