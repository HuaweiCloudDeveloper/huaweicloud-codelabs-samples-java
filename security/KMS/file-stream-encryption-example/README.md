## 1、介绍
**什么是密钥管理服务？**

[密钥管理服务](https://support.huaweicloud.com/productdesc-dew/dew_01_0001.html)，密钥管理，即密钥管理服务（Key Management Service，KMS），是一种安全、可靠、简单易用的密钥托管服务，帮助您轻松创建和管理密钥，保护密钥的安全。KMS通过使用硬件安全模块（Hardware Security Module，HSM）保护密钥安全，帮助用户轻松创建和管理密钥，所有的用户密钥都由HSM中的根密钥保护，避免密钥泄露。 当有大量数据（例如：照片、视频或者数据库文件等）需要加解密时，用户可采用信封加密方式加解密数据，无需通过网络传输大量数据即可完成数据加解密。

**您将学到什么？**

如何通过java版本的KMS SDK方式使用数据密钥（DEK）进行文件加解密

## 2、前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云租户账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 3、华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。
- 4、用户需要在密钥管理控制台创建用户主密钥，算法为AES_256。
- 5、用户需要准备一个名为FirstPlainFile.jpg的文件

## 3、SDK获取和安装
您可以通过Maven配置所依赖的密钥管理服务SDK
具体的SDK版本号请参见[SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/KMS?lang=Java)

## 4、接口参数说明
关于接口参数的详细说明可参见：

[a.创建数据密钥](https://console.huaweicloud.com/apiexplorer/#/openapi/KMS/doc?api=CreateDatakey)

[b.解密数据密钥](https://console.huaweicloud.com/apiexplorer/#/openapi/KMS/doc?api=DecryptDatakey)


## 5、代码示例
如何通过java版本的KMS SDK方式使用用户主密钥（CMK）进行字符串加解密
```java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.kms.v2.KmsClient;
import com.huaweicloud.sdk.kms.v2.model.CreateDatakeyRequest;
import com.huaweicloud.sdk.kms.v2.model.CreateDatakeyRequestBody;
import com.huaweicloud.sdk.kms.v2.model.CreateDatakeyResponse;
import com.huaweicloud.sdk.kms.v2.model.DecryptDatakeyRequest;
import com.huaweicloud.sdk.kms.v2.model.DecryptDatakeyRequestBody;

import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * 使用数据密钥（DEK）进行文件加解密
 * 激活assert语法，请在VM_OPTIONS中添加-ea
 * 需要准备名为FirstPlainFile.jpg的文件
 */
public class FileStreamEncryptionExample {

    /**
     * 基础认证信息：
     * 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
     * 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
     * - ACCESS_KEY: 华为云账号Access Key
     * - SECRET_ACCESS_KEY: 华为云账号Secret Access Key, 敏感信息，建议密文存储
     * - IAM_ENDPOINT: 华为云IAM服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?IAM
     * - KMS_REGION_ID: 华为云KMS支持的地域,详情见https://developer.huaweicloud.com/endpoint?DEW
     * - KMS_ENDPOINT: 华为云KMS服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?DEW
     */
    private static final String ACCESS_KEY = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SECRET_ACCESS_KEY = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String IAM_ENDPOINT = "https://<IamEndpoint>";
    private static final String KMS_REGION_ID = "<RegionId>";
    private static final String KMS_ENDPOINT = "https://<KmsEndpoint>";


    /**
     * AES算法相关标识：
     * - AES_KEY_BIT_LENGTH: AES256密钥比特长度
     * - AES_KEY_BYTE_LENGTH: AES256密钥字节长度
     * - AES_ALG: AES256算法，本例分组模式使用GCM，填充使用PKCS5Padding
     * - AES_FLAG: AES算法标识
     * - GCM_TAG_LENGTH: GCM TAG长度
     * - GCM_IV_LENGTH: GCM 初始向量长度
     */
    private static final String AES_KEY_BIT_LENGTH = "256";
    private static final String AES_KEY_BYTE_LENGTH = "32";
    private static final String AES_ALG = "AES/GCM/NoPadding";
    private static final String AES_FLAG = "AES";
    private static final int GCM_TAG_LENGTH = 16;
    private static final int GCM_IV_LENGTH = 12;

    public static void main(final String[] args) {
        // 用户主密钥ID
        final String keyId = args[0];

        encryptAndDecryptFile(keyId);
    }

    /**
     * 使用数据密钥加解密文件实例
     *
     * @param keyId 用户主密钥ID
     */
    static void encryptAndDecryptFile(String keyId) {
        // 1.准备访问华为云的认证信息
        final BasicCredentials auth = new BasicCredentials()
                .withIamEndpoint(IAM_ENDPOINT).withAk(ACCESS_KEY).withSk(SECRET_ACCESS_KEY);

        // 2.初始化SDK，传入认证信息及KMS访问终端地址
        final KmsClient kmsClient = KmsClient.newBuilder()
                .withRegion(new Region(KMS_REGION_ID, KMS_ENDPOINT)).withCredential(auth).build();

        // 3.组装创建数据密钥请求信息
        final CreateDatakeyRequest createDatakeyRequest = new CreateDatakeyRequest()
                .withBody(new CreateDatakeyRequestBody().withKeyId(keyId).withDatakeyLength(AES_KEY_BIT_LENGTH));

        // 4.创建数据密钥
        final CreateDatakeyResponse createDatakeyResponse = kmsClient.createDatakey(createDatakeyRequest);

        // 5.接收创建的数据密钥信息
        // 密文密钥与KeyId建议保存在本地，方便解密数据时获取明文密钥
        // 明文密钥在创建后立即使用，使用前需要将16进制明文密钥转换成byte数组
        final String cipherText = createDatakeyResponse.getCipherText();
        final byte[] plainKey = hexToBytes(createDatakeyResponse.getPlainText());

        // 6.准备待加密的文件
        // inFile 待加密的原文件
        // outEncryptFile 加密后的文件
        // outDecryptFile 加密后再解密的文件
        final File inFile = new File("FirstPlainFile.jpg");
        final File outEncryptFile = new File("SecondEncryptFile.jpg");
        final File outDecryptFile = new File("ThirdDecryptFile.jpg");

        // 7.使用AES算法进行加密时，可以创建初始向量
        final byte[] iv = new byte[GCM_IV_LENGTH];
        final SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(iv);

        // 8.对文件进行加密，并存储加密后的文件
        doFileFinal(Cipher.ENCRYPT_MODE, inFile, outEncryptFile, plainKey, iv);

        // 9.组装解密数据密钥的请求
        final DecryptDatakeyRequest decryptDatakeyRequest = new DecryptDatakeyRequest()
                .withBody(new DecryptDatakeyRequestBody()
                        .withKeyId(keyId).withCipherText(cipherText).withDatakeyCipherLength(AES_KEY_BYTE_LENGTH));

        // 10.解密数据密钥，并对返回的16进制明文密钥换成byte数组
        final byte[] decryptDataKey = hexToBytes(kmsClient.decryptDatakey(decryptDatakeyRequest).getDataKey());

        // 12.对文件进行解密，并存储解密后的文件
        doFileFinal(Cipher.DECRYPT_MODE, outEncryptFile, outDecryptFile, decryptDataKey, iv);

        // 13.比对原文件和加密后再解密的文件
        assert getFileSha256Sum(inFile).equals(getFileSha256Sum(outDecryptFile));

    }

    /**
     * 对文件进行加解密
     *
     * @param cipherMode 加密模式，可选值为Cipher.ENCRYPT_MODE或者Cipher.DECRYPT_MODE
     * @param infile     待加解密的文件
     * @param outFile    加解密后的文件
     * @param keyPlain   明文密钥
     * @param iv         初始化向量
     */
    static void doFileFinal(int cipherMode, File infile, File outFile, byte[] keyPlain, byte[] iv) {

        try (BufferedInputStream bis = new BufferedInputStream(Files.newInputStream(infile.toPath()));
             BufferedOutputStream bos = new BufferedOutputStream(Files.newOutputStream(outFile.toPath()))) {
            final byte[] bytIn = new byte[(int) infile.length()];
            final int fileLength = bis.read(bytIn);

            assert fileLength > 0;

            final SecretKeySpec secretKeySpec = new SecretKeySpec(keyPlain, AES_FLAG);
            final Cipher cipher = Cipher.getInstance(AES_ALG);
            final GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * Byte.SIZE, iv);
            cipher.init(cipherMode, secretKeySpec, gcmParameterSpec);
            final byte[] bytOut = cipher.doFinal(bytIn);
            bos.write(bytOut);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 十六进制字符串转byte数组
     *
     * @param hexString 十六进制字符串
     * @return byte数组
     */
    static byte[] hexToBytes(String hexString) {
        final int stringLength = hexString.length();
        assert stringLength > 0;
        final byte[] result = new byte[stringLength / 2];
        int j = 0;
        for (int i = 0; i < stringLength; i += 2) {
            result[j++] = (byte) Integer.parseInt(hexString.substring(i, i + 2), 16);
        }
        return result;
    }

    /**
     * 计算文件SHA256摘要
     *
     * @param file 文件
     * @return SHA256摘要
     */
    static String getFileSha256Sum(File file) {
        int length;
        MessageDigest sha256;
        byte[] buffer = new byte[1024];
        try {
            sha256 = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }
        try (FileInputStream inputStream = new FileInputStream(file)) {
            while ((length = inputStream.read(buffer)) != -1) {
                sha256.update(buffer, 0, length);
            }
            return new BigInteger(1, sha256.digest()).toString(16);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

}

```

## 6、修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2021-11-22 | 1.0 | 文档首次发布 |