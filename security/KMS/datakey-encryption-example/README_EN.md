## 1. Introduction
**What Is Key Management Service?**

[Key Management Service](https://support.huaweicloud.com/intl/en-us/productdesc-dew/dew_01_0121.html), Key Management Service (KMS) is a secure, reliable, and easy-to-use key hosting service. It uses hardware security modules (HSMs) to keep your keys secure and help you easily create and manage keys. All keys are protected by the root keys in HSMs to avoid key leakage. If you want to encrypt or decrypt large volumes of data, such as pictures, videos, and database files, you can use the envelope encryption method, where the data does not need to be transferred over the network.

**What You Will Learn**

How to use a custom master key (CMK) to encrypt data encryption keys (DEKs) using KMS JAVA SDK

## 2. Preparations
- You have obtained the Huawei Cloud SDK. You can also install the JAVA SDK.
- You have obtained a pair of access key ID (AK) and secret access key (SK) for your Huawei Cloud account. To view your AKs/SKs, go to the Huawei Cloud console, hover the cursor over the username in the upper right corner, and choose My Credentials from the drop-down list. Then, in the navigation pane on the left, choose Access Keys. For details, see [Access Keys](https://support.huaweicloud.com/en-us/usermanual-ca/en-us_topic_0046606340.html).
- You have set up the development environment with Java JDK 1.8 or later.
- You have created a CMK using the AES_256 algorithm on the KMS console.

## 3. Install SDKs
You can obtain and install an SDK in Maven mode. You need to download and install Maven in your operating system (OS). After the installation is complete, you only need to add the corresponding dependencies to the pom.xml file of the Java project.

Before using the service SDK, you need to install huaweicloud-sdk-kms. For details about the SDK version, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/KMS?lang=Java).

```xml
<dependency>
	<groupId>com.huaweicloud.sdk</groupId>
	<artifactId>huaweicloud-sdk-kms</artifactId>
	<version>3.1.116</version>
</dependency>
```

## 4. Interface parameter description
Detailed descriptions of interface parameters can be found at:

[a.Encrypt a DEK](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/KMS/doc?api=EncryptDatakey)

[b.Decrypt a DEK](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/KMS/doc?api=DecryptDatakey)


## 5. Code Sample
How to use a custom master key (CMK) to encrypt data encryption keys (DEKs) using KMS JAVA SDK
```java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.kms.v2.KmsClient;
import com.huaweicloud.sdk.kms.v2.model.DecryptDatakeyRequest;
import com.huaweicloud.sdk.kms.v2.model.DecryptDatakeyRequestBody;
import com.huaweicloud.sdk.kms.v2.model.EncryptDatakeyRequest;
import com.huaweicloud.sdk.kms.v2.model.EncryptDatakeyRequestBody;
import com.huaweicloud.sdk.kms.v2.model.EncryptDatakeyResponse;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

/**
 * Encrypt data key using user master key (CMK)
 * To activate assert syntax, add -ea in VM_OPTIONS
 */
public class DataKeyEncryptExample {

    /**
     * Basic certification information:
     * Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
     * In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
     * - ACCESS_KEY: AK of the Huawei Cloud account
     * - SECRET_ACCESS_KEY: SK of the Huawei Cloud account. This is sensitive information. Store this in ciphertext.
     * - IAM_ENDPOINT: endpoint for accessing IAM. For details, see https://developer.huaweicloud.com/intl/en-us/endpoint?IAM
     * - KMS_REGION_ID: regions supported by KMS. For details, see https://developer.huaweicloud.com/intl/en-us/endpoint?DEW
     * - KMS_ENDPOINT: endpoint for accessing KMS. For details, see https://developer.huaweicloud.com/intl/en-us/endpoint?DEW
     */
    private static final String ACCESS_KEY = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SECRET_ACCESS_KEY = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String IAM_ENDPOINT = "https://<IamEndpoint>";
    private static final String KMS_REGION_ID = "<RegionId>";
    private static final String KMS_ENDPOINT = "https://<KmsEndpoint>";

    /* AES Algorithm related identifiers：
     * - AES_KEY_BYTE_LENGTH: AES256 key byte length
     */
    private static final String AES_KEY_BYTE_LENGTH = "32";


    public static void main(final String[] args) {
        // CMK ID
        final String keyId = args[0];

        encryptAndDecryptDataKey(keyId);
    }

    /**
     * Encrypt data key using user master key (CMK)
     *
     * @param keyId customer master key ID
     */
    static void encryptAndDecryptDataKey(String keyId) {
        // 1.Prepare the authentication information.
        final BasicCredentials auth = new BasicCredentials()
                .withIamEndpoint(IAM_ENDPOINT).withAk(ACCESS_KEY).withSk(SECRET_ACCESS_KEY);

        // 2.Initialize the SDK and import the authentication and KMS endpoint information.
        final KmsClient kmsClient = KmsClient.newBuilder()
                .withRegion(new Region(KMS_REGION_ID, KMS_ENDPOINT)).withCredential(auth).build();

        // Simulate data key generation
        final SecureRandom random = new SecureRandom();
        final byte[] plainKey = new byte[Integer.parseInt(AES_KEY_BYTE_LENGTH)];
        random.nextBytes(plainKey);

        // 3.Calculate the hash value of the data key
        final String plainKeyDigest = dataKeyDigest(plainKey);

        // 4.Unified encrypt data key information
        // PlainText format: The hexadecimal string of the key + the hexadecimal string of the key digest
        final EncryptDatakeyRequest encryptDatakeyRequest = new EncryptDatakeyRequest()
                .withBody(new EncryptDatakeyRequestBody().withKeyId(keyId)
                        .withPlainText(bytesToHexString(plainKey) + plainKeyDigest).withDatakeyPlainLength(AES_KEY_BYTE_LENGTH));

        // 5.Send request
        final EncryptDatakeyResponse encryptDatakeyResponse = kmsClient.encryptDatakey(encryptDatakeyRequest);

        // 6.Decrypt data key
        final DecryptDatakeyRequest decryptDatakeyRequest = new DecryptDatakeyRequest()
                .withBody(new DecryptDatakeyRequestBody()
                        .withKeyId(keyId).withCipherText(encryptDatakeyResponse.getCipherText())
                        .withDatakeyCipherLength(AES_KEY_BYTE_LENGTH));

        // 7.Decrypt the data key and replace the returned hexadecimal plaintext key with a byte array
        final byte[] decryptDataKey = hexToBytes(kmsClient.decryptDatakey(decryptDatakeyRequest).getDataKey());

        // 8.Check data consistency
        assert Arrays.equals(plainKey, decryptDataKey);
    }

    /**
     * Convert hexadecimal string to byte array
     *
     * @param hexString hexadecimal string
     * @return byte array
     */
    static byte[] hexToBytes(String hexString) {
        assert null != hexString;
        final int stringLength = hexString.length();
        assert stringLength > 0;
        final byte[] result = new byte[stringLength / 2];
        int j = 0;
        for (int i = 0; i < stringLength; i += 2) {
            result[j++] = (byte) Integer.parseInt(hexString.substring(i, i + 2), 16);
        }
        return result;
    }

    /**
     * Convert byte number to hexadecimal string
     *
     * @param bytes byte array
     * @return hexadecimal string
     */
    static String bytesToHexString(byte[] bytes) {
        final StringBuilder stringBuilder = new StringBuilder();
        for (byte b : bytes) {
            stringBuilder.append(String.format("%02x", b));
        }
        return stringBuilder.toString();
    }

    /**
     * Compute the digest of a data key
     *
     * @param dataKey plaintext data key
     * @return Hexadecimal representation of digest
     */
    static String dataKeyDigest(byte[] dataKey) {
        final MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }

        return bytesToHexString(messageDigest.digest(dataKey));
    }

}

```

## 6. Change History

| Release Date | Issue|   Description  |
|:------------:| :------: | :----------: |
|  2024-9-30   |   1.0    | This issue is the first official release.|