package com.huawei.kms;


import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.kms.v2.KmsClient;
import com.huaweicloud.sdk.kms.v2.model.DecryptDatakeyRequest;
import com.huaweicloud.sdk.kms.v2.model.DecryptDatakeyRequestBody;
import com.huaweicloud.sdk.kms.v2.model.EncryptDatakeyRequest;
import com.huaweicloud.sdk.kms.v2.model.EncryptDatakeyRequestBody;
import com.huaweicloud.sdk.kms.v2.model.EncryptDatakeyResponse;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

/**
 * 使用用户主密钥（CMK）加密数据密钥
 * 激活assert语法，请在VM_OPTIONS中添加-ea
 */
public class DataKeyEncryptExample {

    /**
     * 基础认证信息：
     * 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
     * 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
     * - ACCESS_KEY: 华为云账号Access Key
     * - SECRET_ACCESS_KEY: 华为云账号Secret Access Key, 敏感信息，建议密文存储
     * - IAM_ENDPOINT: 华为云IAM服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?IAM
     * - KMS_REGION_ID: 华为云KMS支持的地域,详情见https://developer.huaweicloud.com/endpoint?DEW
     * - KMS_ENDPOINT: 华为云KMS服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?DEW
     */
    private static final String ACCESS_KEY = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SECRET_ACCESS_KEY = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String IAM_ENDPOINT = "https://<IamEndpoint>";
    private static final String KMS_REGION_ID = "<RegionId>";
    private static final String KMS_ENDPOINT = "https://<KmsEndpoint>";

    /* AES算法相关标识：
     * - AES_KEY_BYTE_LENGTH: AES256密钥字节长度
     */
    private static final String AES_KEY_BYTE_LENGTH = "32";


    public static void main(final String[] args) {
        // 用户主密钥ID
        final String keyId = args[0];

        encryptAndDecryptDataKey(keyId);
    }

    /**
     * 加解密数据密钥示例
     *
     * @param keyId 用户主密钥ID
     */
    static void encryptAndDecryptDataKey(String keyId) {
        // 1.准备访问华为云的认证信息
        final BasicCredentials auth = new BasicCredentials()
                .withIamEndpoint(IAM_ENDPOINT).withAk(ACCESS_KEY).withSk(SECRET_ACCESS_KEY);

        // 2.初始化SDK，传入认证信息及KMS访问终端地址
        final KmsClient kmsClient = KmsClient.newBuilder()
                .withRegion(new Region(KMS_REGION_ID, KMS_ENDPOINT)).withCredential(auth).build();

        // 模拟生成数据密钥
        final SecureRandom random = new SecureRandom();
        final byte[] plainKey = new byte[Integer.parseInt(AES_KEY_BYTE_LENGTH)];
        random.nextBytes(plainKey);

        // 3.计算数据密钥的哈希值
        final String plainKeyDigest = dataKeyDigest(plainKey);

        // 4.组装加密数据密钥的请求
        // PlainText格式: 密钥的16进制字符串+密钥摘要的16进制字符串
        final EncryptDatakeyRequest encryptDatakeyRequest = new EncryptDatakeyRequest()
                .withBody(new EncryptDatakeyRequestBody().withKeyId(keyId)
                        .withPlainText(bytesToHexString(plainKey) + plainKeyDigest).withDatakeyPlainLength(AES_KEY_BYTE_LENGTH));

        // 5.加密数据密钥
        final EncryptDatakeyResponse encryptDatakeyResponse = kmsClient.encryptDatakey(encryptDatakeyRequest);

        // 6.解密数据密钥
        final DecryptDatakeyRequest decryptDatakeyRequest = new DecryptDatakeyRequest()
                .withBody(new DecryptDatakeyRequestBody()
                        .withKeyId(keyId).withCipherText(encryptDatakeyResponse.getCipherText())
                        .withDatakeyCipherLength(AES_KEY_BYTE_LENGTH));

        // 7.解密数据密钥，并对返回的16进制明文密钥换成byte数组
        final byte[] decryptDataKey = hexToBytes(kmsClient.decryptDatakey(decryptDatakeyRequest).getDataKey());

        // 8.校验数据一致性
        assert Arrays.equals(plainKey, decryptDataKey);
    }

    /**
     * 十六进制字符串转byte数组
     *
     * @param hexString 十六进制字符串
     * @return byte数组
     */
    static byte[] hexToBytes(String hexString) {
        assert null != hexString;
        final int stringLength = hexString.length();
        assert stringLength > 0;
        final byte[] result = new byte[stringLength / 2];
        int j = 0;
        for (int i = 0; i < stringLength; i += 2) {
            result[j++] = (byte) Integer.parseInt(hexString.substring(i, i + 2), 16);
        }
        return result;
    }

    /**
     * byte数转16进制字符串
     *
     * @param bytes byte数组
     * @return 16进制字符串
     */
    static String bytesToHexString(byte[] bytes) {
        final StringBuilder stringBuilder = new StringBuilder();
        for (byte b : bytes) {
            stringBuilder.append(String.format("%02x", b));
        }
        return stringBuilder.toString();
    }

    /**
     * 计算数据密钥的摘要
     *
     * @param dataKey 明文数据密钥
     * @return 摘要的16进制表示
     */
    static String dataKeyDigest(byte[] dataKey) {
        final MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }

        return bytesToHexString(messageDigest.digest(dataKey));
    }

}