/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.edgesec.v1.EdgeSecClient;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeWafDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeWafDomainsRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeWafDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.DeleteEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.DeleteEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ListCdnDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ListCdnDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeWafDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeWafDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ShowCdnDomainResponseBody;
import com.huaweicloud.sdk.edgesec.v1.model.ShowEdgeWafDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ShowEdgeWafDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ShowWafDomainResponseBody;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequestBody.ProtectedSwitchEnum;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.region.EdgeSecRegion;

import java.util.List;

public class WafDomainManagementDemo {
    public static void main(String[] args) {
        System.out.println("Start HUAWEI CLOUD Edge Security Waf Domain Management Java Demo...");
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "<YOUR PROJECT ID>";
        Region yourRegion = EdgeSecRegion.AP_SOUTHEAST_1;
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
        EdgeSecClient client = getClient(yourRegion, auth);

        try {
            /* 4.4.1 */
            List<ShowCdnDomainResponseBody> cdnDomains = listCdnDomain(client);
            if (cdnDomains.size() != 0) {
                ShowCdnDomainResponseBody showCdnDomainResponseBody = cdnDomains.get(0);

                /* 4.4.2 */
                createWafDomain(client, showCdnDomainResponseBody.getDomainName(), showCdnDomainResponseBody.getServiceArea().getValue());

                /* 4.4.3 */
                ShowWafDomainResponseBody wafDomainByDomain = queryWafDomainByDomain(client, showCdnDomainResponseBody.getDomainName());

                if (wafDomainByDomain.getId() != null) {
                    /* 4.4.4 */
                    queryWafDomainById(client, wafDomainByDomain.getId());

                    /* 4.4.5 */
                    updateWafDomain(client, wafDomainByDomain.getId(), ProtectedSwitchEnum.NUMBER_0);

                    /* 4.4.6 */
                    updateWafDomain(client, wafDomainByDomain.getId(), ProtectedSwitchEnum.NUMBER_1);

                    /* 4.4.7 */
                    deleteWafDomain(client, wafDomainByDomain.getId());
                }
            }
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        System.out.println("HUAWEI CLOUD Edge Security Waf Domain Management Java Demo End");
    }

    public static EdgeSecClient getClient(Region region, ICredential auth) {
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        return EdgeSecClient.newBuilder().withHttpConfig(config).withCredential(auth).withRegion(region).build();
    }

    private static List<ShowCdnDomainResponseBody> listCdnDomain(EdgeSecClient client) {
        ListCdnDomainsRequest request = new ListCdnDomainsRequest();
        ListCdnDomainsResponse response = client.listCdnDomains(request);
        return response.getDomains();
    }

    private static void createWafDomain(EdgeSecClient client, String domain, String serviceAreaType) {
        CreateEdgeWafDomainsRequest request = new CreateEdgeWafDomainsRequest();
        CreateEdgeWafDomainsRequestBody body = new CreateEdgeWafDomainsRequestBody();
        body.setDomainName(domain);
        body.setAreaType(CreateEdgeWafDomainsRequestBody.AreaTypeEnum.valueOf(serviceAreaType));
        request.setBody(body);
        CreateEdgeWafDomainsResponse response = client.createEdgeWafDomains(request);
        System.out.println(response.toString());
    }

    private static ShowWafDomainResponseBody queryWafDomainByDomain(EdgeSecClient client, String domain) {
        ListEdgeWafDomainsRequest request = new ListEdgeWafDomainsRequest();
        request.setDomainName(domain);
        ListEdgeWafDomainsResponse response = client.listEdgeWafDomains(request);
        System.out.println(response.toString());
        return response.getDomainList().get(0);
    }

    private static void queryWafDomainById(EdgeSecClient client, String domainId) {
        ShowEdgeWafDomainsRequest request = new ShowEdgeWafDomainsRequest();
        request.setDomainid(domainId);
        ShowEdgeWafDomainsResponse response = client.showEdgeWafDomains(request);
        System.out.println(response.toString());
    }

    private static boolean updateWafDomain(EdgeSecClient client, String domainId, ProtectedSwitchEnum protectedSwitchEnum) {
        UpdateEdgeDDoSDomainsRequest request = new UpdateEdgeDDoSDomainsRequest();
        UpdateEdgeDDoSDomainsRequestBody body = new UpdateEdgeDDoSDomainsRequestBody();
        body.setProtectedSwitch(protectedSwitchEnum);
        request.setDomainid(domainId);
        request.setBody(body);
        UpdateEdgeDDoSDomainsResponse response = client.updateEdgeDDoSDomains(request);
        return response.getHttpStatusCode() == 200;
    }

    private static boolean deleteWafDomain(EdgeSecClient client, String domainId) {
        DeleteEdgeDDoSDomainsRequest request = new DeleteEdgeDDoSDomainsRequest();
        request.setDomainid(domainId);
        DeleteEdgeDDoSDomainsResponse response = client.deleteEdgeDDoSDomains(request);
        return response.getHttpStatusCode() == 200;
    }
}
