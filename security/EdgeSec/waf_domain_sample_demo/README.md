## 1.简介
华为云提供了EdgeSec服务端SDK，您可以直接集成服务端SDK来调用EdgeSec的相关API，从而实现对EdgeSec的快速操作。该示例展示如何通过EdgeSec服务对已使用CDN服务的域名执行添加、查询、修改以及删除边缘WAF防护的操作。

## 2.开发前准备
- 已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=94fc0f9f861b4f30a85ec1f463d35609&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问秘钥](https://support.huaweicloud.com/devg-apisign/api-sign-provide-aksk.html)
- 已获取对应区域的项目，请在华为云控制台“我的凭证 > API凭证 > 项目列表”页面上查看项目——中国大陆境内的资源使用cn-north-4的项目，中国大陆境外的资源使用ap-southeast-1的项目。具体请参见 [API凭证](https://support.huaweicloud.com/devg-apisign/api-sign-provide-proid.html) 。
- 已经购买[CDN](https://www.huaweicloud.com/zh-cn/product/cdn.html)服务。
- 需要购买[边缘安全](https://www.huaweicloud.com/zh-cn/product/edgesec.html)服务。
- 已经在CDN服务上添加加速域名。

## 3.安装sdk

您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-edgesec”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com/?language=Java) 。

````xml
<dependency>
	<groupId>com.huaweicloud.sdk</groupId>
	<artifactId>huaweicloud-sdk-edgesec</artifactId>
	<version>3.1.55</version>
</dependency>
````
## 4.开始使用
### 4.1 导入依赖模块
````java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.edgesec.v1.EdgeSecClient;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeWafDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeWafDomainsRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeWafDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.DeleteEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.DeleteEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ListCdnDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ListCdnDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeWafDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeWafDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ShowCdnDomainResponseBody;
import com.huaweicloud.sdk.edgesec.v1.model.ShowEdgeWafDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ShowEdgeWafDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ShowWafDomainResponseBody;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequestBody.ProtectedSwitchEnum;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.region.EdgeSecRegion;
import java.util.List;
````

### 4.2 初始化认证信息

````java
ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
````

### 4.3 初始化边缘安全客户端

````java
HttpConfig config = HttpConfig.getDefaultHttpConfig();
config.withIgnoreSSLVerification(true);
EdgeSecClient.newBuilder().withHttpConfig(config).withCredential(auth).withRegion(region).build();
````

### 4.4 添加、查询，修改并删除WAF防护域名
此节4.4.1-4.4.6示范了在console界面上如何操作，4.4.8示范了代码如何实现上述操作。

#### 4.4.1 在边缘安全控制台，点击Web应用防火墙、网站设置的【添加防护网站】，在弹窗可以看到已经使用CDN服务的域名
![waf_domain_1](assets/waf_domain_1.png)

#### 4.4.2 选中需要添加WAF防护的域名，点击确定
![waf_domain_2](assets/waf_domain_2.png)

#### 4.4.3 搜索添加的域名
![waf_domain_3](assets/waf_domain_3.png)

#### 4.4.4 点击域名查看域名详情
![waf_domain_4](assets/waf_domain_4.png)

#### 4.4.5 如需关闭域名的WAF防护，如图操作
![waf_domain_5](assets/waf_domain_5.png)

#### 4.4.6 如需开启域名的WAF防护，如图操作
![waf_domain_6](assets/waf_domain_6.png)

#### 4.4.7 如需删除WAF防护域名，如图操作
![waf_domain_7](assets/waf_domain_7.png)

#### 4.4.8 示例代码
````java
public static void main(String[] args) {
    /* 边缘安全WAF防护域名操作 */
    System.out.println("Start HUAWEI CLOUD Edge Security Waf Domain Management Java Demo...");
    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    String projectId = "<YOUR PROJECT ID>";
    /* 中国大陆境内使用EdgeSecRegion.CN_NORTH_4，中国大陆境外使用EdgeSecRegion.AP_SOUTHEAST_1*/
    Region yourRegion = EdgeSecRegion.AP_SOUTHEAST_1;
    /* 创建EdgeSecClient实例 */
    ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);

    /* 创建EdgeSecClient实例 */
    EdgeSecClient client = getClient(yourRegion, auth);

    try {
        /* 4.4.1 查询已经使用CDN服务的域名列表*/
        List<ShowCdnDomainResponseBody> cdnDomains = listCdnDomain(client);
        if (cdnDomains.size() != 0) {
            ShowCdnDomainResponseBody showCdnDomainResponseBody = cdnDomains.get(0);

            /* 4.4.2 添加WAF防护域名*/
            createWafDomain(client, showCdnDomainResponseBody.getDomainName(), showCdnDomainResponseBody.getServiceArea().getValue());

            /* 4.4.3 查询WAF域名*/
            ShowWafDomainResponseBody wafDomainByDomain = queryWafDomainByDomain(client, showCdnDomainResponseBody.getDomainName());

            if (wafDomainByDomain.getId() != null) {
                /* 4.4.4 查询WAF域名详情*/
                queryWafDomainById(client, wafDomainByDomain.getId());

                /* 4.4.5 停用WAF域名防护*/
                updateWafDomain(client, wafDomainByDomain.getId(), ProtectedSwitchEnum.NUMBER_0);

                /* 4.4.6 开启WAF域名防护*/
                updateWafDomain(client, wafDomainByDomain.getId(), ProtectedSwitchEnum.NUMBER_1);

                /* 4.4.7 删除WAF域名*/
                deleteWafDomain(client, wafDomainByDomain.getId());
            }
        }
    } catch (ConnectionException | RequestTimeoutException e) {
        System.out.println(e.getMessage());
    } catch (ServiceResponseException e) {
        System.out.println(e.getHttpStatusCode());
        System.out.println(e.getErrorCode());
        System.out.println(e.getErrorMsg());
    }
    System.out.println("HUAWEI CLOUD Edge Security Waf Domain Management Java Demo End");
}

/**
 * 创建 EdgeSecClient
 *
 * @param region region信息
 * @param auth   鉴权凭证
 * @return EdgeSecClient实例
 */
public static EdgeSecClient getClient(Region region, ICredential auth) {
    // 使用默认配置
    HttpConfig config = HttpConfig.getDefaultHttpConfig();
    config.withIgnoreSSLVerification(true);
    // 初始化EdgeSec服务的客户端
    return EdgeSecClient.newBuilder().withHttpConfig(config).withCredential(auth).withRegion(region).build();
}

/**
 * 查询 CDN 域名
 *
 * @param client EdgeSecClient 实例
 * @return CDN 域名
 */
private static List<ShowCdnDomainResponseBody> listCdnDomain(EdgeSecClient client) {
    ListCdnDomainsRequest request = new ListCdnDomainsRequest();
    ListCdnDomainsResponse response = client.listCdnDomains(request);
    return response.getDomains();
}

/**
 * 创建 WAF 域名
 *
 * @param client          EdgeSecClient 实例
 * @param domain          域名
 * @param serviceAreaType 服务地区
 */
private static void createWafDomain(EdgeSecClient client, String domain, String serviceAreaType) {
    CreateEdgeWafDomainsRequest request = new CreateEdgeWafDomainsRequest();
    CreateEdgeWafDomainsRequestBody body = new CreateEdgeWafDomainsRequestBody();
    body.setDomainName(domain);
    body.setAreaType(CreateEdgeWafDomainsRequestBody.AreaTypeEnum.valueOf(serviceAreaType));
    request.setBody(body);
    CreateEdgeWafDomainsResponse response = client.createEdgeWafDomains(request);
    System.out.println(response.toString());
}

/**
 * 查询 WAF 域名详情
 *
 * @param client EdgeSecClient 实例
 * @param domain 域名
 * @return 域名ID
 */
private static ShowWafDomainResponseBody queryWafDomainByDomain(EdgeSecClient client, String domain) {
    ListEdgeWafDomainsRequest request = new ListEdgeWafDomainsRequest();
    request.setDomainName(domain);
    ListEdgeWafDomainsResponse response = client.listEdgeWafDomains(request);
    System.out.println(response.toString());
    return response.getDomainList().get(0);
}

/**
 * 查询 WAF 域名详情
 *
 * @param client   EdgeSecClient 实例
 * @param domainId 域名id
 * @return 域名ID
 */
private static void queryWafDomainById(EdgeSecClient client, String domainId) {
    ShowEdgeWafDomainsRequest request = new ShowEdgeWafDomainsRequest();
    request.setDomainid(domainId);
    ShowEdgeWafDomainsResponse response = client.showEdgeWafDomains(request);
    System.out.println(response.toString());
}

/**
 * 更新 WAF 域名防护状态
 *
 * @param client              EdgeSecClient 实例
 * @param domainId            域名id
 * @param protectedSwitchEnum 域名防护状态
 * @return 更新结果
 */
private static boolean updateWafDomain(EdgeSecClient client, String domainId, ProtectedSwitchEnum protectedSwitchEnum) {
    UpdateEdgeDDoSDomainsRequest request = new UpdateEdgeDDoSDomainsRequest();
    UpdateEdgeDDoSDomainsRequestBody body = new UpdateEdgeDDoSDomainsRequestBody();
    body.setProtectedSwitch(protectedSwitchEnum);
    request.setDomainid(domainId);
    request.setBody(body);
    UpdateEdgeDDoSDomainsResponse response = client.updateEdgeDDoSDomains(request);
    return response.getHttpStatusCode() == 200;
}

/**
 * 删除 WAF 域名
 *
 * @param client   EdgeSecClient 实例
 * @param domainId 域名id
 * @return 更新结果
 */
private static boolean deleteWafDomain(EdgeSecClient client, String domainId) {
    DeleteEdgeDDoSDomainsRequest request = new DeleteEdgeDDoSDomainsRequest();
    request.setDomainid(domainId);
    DeleteEdgeDDoSDomainsResponse response = client.deleteEdgeDDoSDomains(request);
    return response.getHttpStatusCode() == 200;
}
````

## 5.FAQ
### 5.1 为什么项目和Region不是客户服务所在的区域，而是根据中国大陆境内外区分？
边缘安全是全局级服务，客户购买的边缘安全服务资源只区分中国大陆境内和境外。

## 6.参考
更多信息请参考API Explorer

## 7.修订记录
|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2023-08-23 |   1.0    | 文档首次发布 |