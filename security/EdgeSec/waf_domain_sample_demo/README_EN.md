## 1. Introduction
You can integrate EdgeSec SDKs provided by Huawei Cloud to call EdgeSec APIs, making it easier for you to use the service. This example shows how to use EdgeSec to add domain names that have used CDN to edge WAF and how to query, modify, and remove them from edge WAF.

## 2. Preparations
- You have [registered](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=94fc0f9f861b4f30a85ec1f463d35609&lang=en-us) with Huawei Cloud and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth).
- You have set up the development environment with Java JDK 1.8 or later.
- You have obtained the AK and SK of your Huawei Cloud account. You can create and view an AK/SK on the My Credentials > Access Keys page on the Huawei Cloud management console. For details, see [Obtaining an AK/SK](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).
- You have obtained the project ID of the corresponding region. To obtain it, log in to the Huawei Cloud management console, choose My Credentials > API Credentials > Projects, and locate the project. For resources in the Chinese mainland, use the ID for project cn-north-4. For resources outside the Chinese mainland, use the ID for project ap-southeast-1. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-proid.html).
- You have purchased [CDN](https://www.huaweicloud.com/intl/en-us/product/cdn.html).
- You have purchased [Edge Security](https://www.huaweicloud.com/intl/en-us/product/edgesec.html).
- You have added an acceleration domain name to CDN.

## 3. Install SDKs

You can obtain and install an SDK in Maven mode. You need to download and install Maven in your operating system (OS). After the installation is complete, you only need to add the corresponding dependencies to the pom.xml file of the Java project.

Before using the service SDK, you need to install huaweicloud-sdk-edgesec. For details about the SDK version, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/EdgeSec?lang=Java).

````xml
<dependency>
	<groupId>com.huaweicloud.sdk</groupId>
	<artifactId>huaweicloud-sdk-edgesec</artifactId>
	<version>3.1.55</version>
</dependency>
````
## 4. Begin to Use
### 4.1 Import Dependent Modules
````java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.edgesec.v1.EdgeSecClient;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeWafDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeWafDomainsRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeWafDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.DeleteEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.DeleteEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ListCdnDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ListCdnDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeWafDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeWafDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ShowCdnDomainResponseBody;
import com.huaweicloud.sdk.edgesec.v1.model.ShowEdgeWafDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ShowEdgeWafDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ShowWafDomainResponseBody;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequestBody.ProtectedSwitchEnum;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.region.EdgeSecRegion;
import java.util.List;
````

### 4.2 Initialize Authentication Information

````java
ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
````

### 4.3 Initialize the EdgeSec Client

````java
HttpConfig config = HttpConfig.getDefaultHttpConfig();
config.withIgnoreSSLVerification(true);
EdgeSecClient.newBuilder().withHttpConfig(config).withCredential(auth).withRegion(region).build();
````

### 4.4 Add, Query, Modify, and Delete Domain Names in Edge WAF
In this section, 4.4.1 to 4.4.7 describe how to use EdgeSec through the console, and 4.4.8 describes how to use the same functions with code.

#### 4.4.1 On the Edge Security console, choose Web Application Firewall > Website Settings > Add Website. In the dialog box displayed, you can view the domain names that have used CDN.
![waf_domain_1](assets/waf_domain_1_en.png)

#### 4.4.2 Select the domain name you want to add to edge WAF and click OK.
![waf_domain_2](assets/waf_domain_2_en.png)

#### 4.4.3 Search for the added domain name.
![waf_domain_3](assets/waf_domain_3_en.png)

#### 4.4.4 Click the domain name to view its details.
![waf_domain_4](assets/waf_domain_4_en.png)

#### 4.4.5 Disable edge WAF protection for a domain name.
![waf_domain_5](assets/waf_domain_5_en.png)

#### 4.4.6 Enable edge WAF protection for a domain name.
![waf_domain_6](assets/waf_domain_6_en.png)

#### 4.4.7 Delete a domain name from edge WAF.
![waf_domain_7](assets/waf_domain_7_en.png)

#### 4.4.8 Sample Code
````java
public static void main(String[] args) {
    /*Operations on domain names added to edge WAF*/
    System.out.println("Start HUAWEI CLOUD Edge Security Waf Domain Management Java Demo...");
    /* 
        Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    */
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    String projectId = "<YOUR PROJECT ID>";
    /* EdgeSecRegion.CN_NORTH_4 is used for resources in the Chinese Mainland, and EdgeSecRegion.AP_SOUTHEAST_1 is used for resources outside the Chinese Mainland.*/
    Region yourRegion = EdgeSecRegion.AP_SOUTHEAST_1;
    // Create an EdgeSecClient instance.
    ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);

    // Create an EdgeSecClient instance.
    EdgeSecClient client = getClient(yourRegion, auth);

    try {
        /* 4.4.1 Query the domain names that have used the CDN service.*/
        List<ShowCdnDomainResponseBody> cdnDomains = listCdnDomain(client);
        if (cdnDomains.size() != 0) {
            ShowCdnDomainResponseBody showCdnDomainResponseBody = cdnDomains.get(0);

            /* 4.4.2 Add a domain name to edge WAF.*/
            createWafDomain(client, showCdnDomainResponseBody.getDomainName(), showCdnDomainResponseBody.getServiceArea().getValue());

            /* 4.4.3 Query the domain name added to edge WAF.*/
            ShowWafDomainResponseBody wafDomainByDomain = queryWafDomainByDomain(client, showCdnDomainResponseBody.getDomainName());

            if (wafDomainByDomain.getId() != null) {
                /* 4.4.4 Query details of the domain name added to edge WAF*/
                queryWafDomainById(client, wafDomainByDomain.getId());

                /* 4.4.5 Disable edge WAF protection for a domain name.*/
                updateWafDomain(client, wafDomainByDomain.getId(), ProtectedSwitchEnum.NUMBER_0);

                /* 4.4.6 Enable edge WAF protection for a domain name.*/
                updateWafDomain(client, wafDomainByDomain.getId(), ProtectedSwitchEnum.NUMBER_1);

                /* 4.4.7 Delete a domain name from edge WAF.*/
                deleteWafDomain(client, wafDomainByDomain.getId());
            }
        }
    } catch (ConnectionException | RequestTimeoutException e) {
        System.out.println(e.getMessage());
    } catch (ServiceResponseException e) {
        System.out.println(e.getHttpStatusCode());
        System.out.println(e.getErrorCode());
        System.out.println(e.getErrorMsg());
    }
    System.out.println("Huawei Cloud Edge Security WAF Domain Management Java Demo End");
}

/**
 * Create EdgeSecClient.
 *
 * @param region Region information
 * @param auth Credentials
 * @return EdgeSecClient instance
 */
public static EdgeSecClient getClient(Region region, ICredential auth) {
    // Use the default settings.
    HttpConfig config = HttpConfig.getDefaultHttpConfig();
    config.withIgnoreSSLVerification(true);
    // Initialize the EdgeSec client.
    return EdgeSecClient.newBuilder().withHttpConfig(config).withCredential(auth).withRegion(region).build();
}

/**
 * Query the domain names that have used CDN.
 *
 * @param client EdgeSecClient instance
 * @return CDN domain names
 */
private static List<ShowCdnDomainResponseBody> listCdnDomain(EdgeSecClient client) {
    ListCdnDomainsRequest request = new ListCdnDomainsRequest();
    ListCdnDomainsResponse response = client.listCdnDomains(request);
    return response.getDomains();
}

/**
 * Add a domain name to edge WAF.
 *
 * @param client         EdgeSecClient instance
 * @param domain         Domain name
 * @param serviceAreaType Service region
 */
private static void createWafDomain(EdgeSecClient client, String domain, String serviceAreaType) {
    CreateEdgeWafDomainsRequest request = new CreateEdgeWafDomainsRequest();
    CreateEdgeWafDomainsRequestBody body = new CreateEdgeWafDomainsRequestBody();
    body.setDomainName(domain);
    body.setAreaType(CreateEdgeWafDomainsRequestBody.AreaTypeEnum.valueOf(serviceAreaType));
    request.setBody(body);
    CreateEdgeWafDomainsResponse response = client.createEdgeWafDomains(request);
    System.out.println(response.toString());
}

/**
 /* Query details of the domain name added to edge WAF.*/
 *
 * @param client EdgeSecClient instance
 * @param domain Domain name
 * @return Domain name ID
 */
private static ShowWafDomainResponseBody queryWafDomainByDomain(EdgeSecClient client, String domain) {
    ListEdgeWafDomainsRequest request = new ListEdgeWafDomainsRequest();
    request.setDomainName(domain);
    ListEdgeWafDomainsResponse response = client.listEdgeWafDomains(request);
    System.out.println(response.toString());
    return response.getDomainList().get(0);
}

/**
 /* Query details of the domain name added to edge WAF.*/
 *
 * @param client   EdgeSecClient instance
 * @param domainId Domain name ID
 * @return Domain name ID
 */
private static void queryWafDomainById(EdgeSecClient client, String domainId) {
    ShowEdgeWafDomainsRequest request = new ShowEdgeWafDomainsRequest();
    request.setDomainid(domainId);
    ShowEdgeWafDomainsResponse response = client.showEdgeWafDomains(request);
    System.out.println(response.toString());
}

/**
 * Update the protection status of a domain name added to edge WAF.
 *
 * @param client             EdgeSecClient instance
 * @param domainId           Domain name ID
 * @param protectedSwitchEnum Domain protection status
 * @return Updated result
 */
private static boolean updateWafDomain(EdgeSecClient client, String domainId, ProtectedSwitchEnum protectedSwitchEnum) {
    UpdateEdgeDDoSDomainsRequest request = new UpdateEdgeDDoSDomainsRequest();
    UpdateEdgeDDoSDomainsRequestBody body = new UpdateEdgeDDoSDomainsRequestBody();
    body.setProtectedSwitch(protectedSwitchEnum);
    request.setDomainid(domainId);
    request.setBody(body);
    UpdateEdgeDDoSDomainsResponse response = client.updateEdgeDDoSDomains(request);
    return response.getHttpStatusCode() == 200;
}

/**
 * Delete a domain name from edge WAF.
 *
 * @param client   EdgeSecClient instance
 * @param domainId Domain name ID
 * @return Updated result
 */
private static boolean deleteWafDomain(EdgeSecClient client, String domainId) {
    DeleteEdgeDDoSDomainsRequest request = new DeleteEdgeDDoSDomainsRequest();
    request.setDomainid(domainId);
    DeleteEdgeDDoSDomainsResponse response = client.deleteEdgeDDoSDomains(request);
    return response.getHttpStatusCode() == 200;
}
````

## 5.FAQ
### 5.1 Why do I select Chinese mainland or Outside the Chinese mainland but not the actual region where my services are located for Project and Region?
EdgeSec is a global service. However, when you purchase it, EdgeSec resources need to be located either in or outside the Chinese mainland.

## 6. References
For more information, see API Explorer.

## 7. Change History
|  Released On | Issue|   Description  |
| :--------: | :------: | :----------: |
| 2023-09-07 |   1.0    | This issue is the first official release.|
