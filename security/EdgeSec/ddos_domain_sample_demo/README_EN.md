## 1. Introduction
You can integrate EdgeSec SDKs provided by Huawei Cloud to call EdgeSec APIs, making it easier for you to use the service. This example shows how to add domain names that have been protected with WAF to edge Anti-DDoS and how to remove them from Edge Anti-DDoS.

## 2. Preparations
- You have [registered](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=94fc0f9f861b4f30a85ec1f463d35609&lang=en-us) with Huawei Cloud and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth).
- You have set up the development environment with Java JDK 1.8 or later.
- You have obtained the AK and SK of your Huawei Cloud account. You can create and view an AK/SK on the My Credentials > Access Keys page on the Huawei Cloud management console. For details, see [Obtaining an AK/SK](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).
- You have obtained the project ID of the corresponding region. To obtain it, log in to the Huawei Cloud management console, choose My Credentials > API Credentials > Projects, and locate the project. For resources in the Chinese mainland, use the ID for project cn-north-4. For resources outside the Chinese mainland, use the ID for project ap-southeast-1. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-proid.html).
- You have purchased [CDN](https://www.huaweicloud.com/intl/en-us/product/cdn.html).
- You have purchased [Edge Security](https://www.huaweicloud.com/intl/en-us/product/edgesec.html).
- You have added the domain name you want to protect to edge WAF in EdgeSec.

## 3. Install SDKs

You can obtain and install an SDK in Maven mode. You need to download and install Maven in your operating system (OS). After the installation is complete, you only need to add the corresponding dependencies to the pom.xml file of the Java project.

Before using the service SDK, you need to install huaweicloud-sdk-edgesec. For details about the SDK version, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/EdgeSec?lang=Java).

````xml
<dependency>
	<groupId>com.huaweicloud.sdk</groupId>
	<artifactId>huaweicloud-sdk-edgesec</artifactId>
	<version>3.1.55</version>
</dependency>
````
## 4. Begin to Use
### 4.1 Import Dependent Modules
````java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.edgesec.v1.EdgeSecClient;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeDDoSDomainsRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.DeleteEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.DeleteEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.EdgeDDoSDomainVo;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeWafDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeWafDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ShowWafDomainResponseBody;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequestBody.ProtectedSwitchEnum;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.region.EdgeSecRegion;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
````

### 4.2 Initialize Authentication Information

````java
ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
````

### 4.3 Initialize the EdgeSec Client

````java
HttpConfig config = HttpConfig.getDefaultHttpConfig();
config.withIgnoreSSLVerification(true);
EdgeSecClient.newBuilder().withHttpConfig(config).withCredential(auth).withRegion(region).build();
````

### 4.4 Add, Query, Modify, and Delete Domain Names in Edge Anti-DDoS
Sections 4.4.1 to 4.4.6 describe how to use the EdgeSec functions through the console, and section 4.4.7 describes how to use the same functions with code.

#### 4.4.1 On the Edge Security console, choose Edge Anti-DDoS > Policies > Add Domain Name. In the dialog box displayed, you can view the domain names protected by Edge Anti-DDoS.

![ddos_domain_1](assets/ddos_domain_1_en.png)

#### 4.4.2 Select the domain name you want to add to edge Anti-DDoS and click OK.
![ddos_domain_2](assets/ddos_domain_2_en.png)

#### 4.4.3 Search for the added domain name.
![ddos_domain_3](assets/ddos_domain_3_en.png)

#### 4.4.4 Enable edge Anti-DDoS for a domain name.
![ddos_domain_4](assets/ddos_domain_4_en.png)

#### 4.4.5 Disable edge Anti-DDoS for a domain name.
![ddos_domain_5](assets/ddos_domain_5_en.png)

#### 4.4.6 Delete a domain name from edge Anti-DDoS.
![ddos_domain_6](assets/ddos_domain_6_en.png)

#### 4.4.7 Sample Code
````java
public static void main(String[] args) {
    /*Operations on domain names added to edge Anti-DDoS*/
    System.out.println("Start HUAWEI CLOUD Edge Security DDoS Domain Management Java Demo...");
    /* 
        Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    */
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    String projectId = "<YOUR PROJECT ID>";
    /* EdgeSecRegion.CN_NORTH_4 is used for resources in the Chinese mainland, and EdgeSecRegion.AP_SOUTHEAST_1 is used for resources outside the Chinese mainland.*/
    Region yourRegion = EdgeSecRegion.AP_SOUTHEAST_1;
    // Create an EdgeSecClient instance.
    ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);

    // Create an EdgeSecClient instance.
    EdgeSecClient client = getClient(yourRegion, auth);

    try {
        /* 4.4.1 Query domain names protected by edge WAF.*/
        List<ShowWafDomainResponseBody> wafDomains = queryWafDomains(client);
        if (wafDomains.size() != 0) {
            ShowWafDomainResponseBody wafDomainResponseBody = wafDomains.get(0);
            /* 4.4.2 Select the domain name you want to add to edge Anti-DDoS and click OK.*/
            createDDoSDomain(client, wafDomainResponseBody.getId());

            /* 4.4.3 Query details about domain name protected by edge Anti-DDoS.*/
            EdgeDDoSDomainVo dDoSDomainVo = queryDDoSDomain(client, wafDomainResponseBody.getDomainName());

            /* 4.4.4 Enable edge Anti-DDoS for a domain name.*/
            updateDDoSDomain(client, dDoSDomainVo.getId(), ProtectedSwitchEnum.NUMBER_0);

            /* 4.4.5 Disable edge Anti-DDoS for a domain name.*/
            updateDDoSDomain(client, dDoSDomainVo.getId(), ProtectedSwitchEnum.NUMBER_1);

            /* 4.4.6 Delete a domain name from edge Anti-DDoS.*/
            deleteDDoSDomain(client, dDoSDomainVo.getId());
        }
    } catch (ConnectionException | RequestTimeoutException e) {
        System.out.println(e.getMessage());
    } catch (ServiceResponseException e) {
        System.out.println(e.getHttpStatusCode());
        System.out.println(e.getErrorCode());
        System.out.println(e.getErrorMsg());
    }
    System.out.println("HUAWEI CLOUD Edge Security DDoS Domain Management Java Demo End");
}

/**
 * Query domain names protected by edge WAF.
 *
 * @param client EdgeSecClient instance
 * @return Domain names protected by edge WAF
 */
private static List<ShowWafDomainResponseBody> queryWafDomains(EdgeSecClient client) {
    ListEdgeWafDomainsRequest request = new ListEdgeWafDomainsRequest();
    ListEdgeWafDomainsResponse response = client.listEdgeWafDomains(request);
    return Optional.ofNullable(response.getDomainList()).orElse(new ArrayList<>());
}

/**
 * Add a domain name to edge Anti-DDoS.
 *
 * @param client   EdgeSecClient instance
 * @param domainId ID of the domain name protected by edge WAF.
 * @return
 */
private static boolean createDDoSDomain(EdgeSecClient client, String domainId) {
    CreateEdgeDDoSDomainsRequest request = new CreateEdgeDDoSDomainsRequest();
    CreateEdgeDDoSDomainsRequestBody body = new CreateEdgeDDoSDomainsRequestBody();
    body.setDomainId(domainId);
    request.setBody(body);
    CreateEdgeDDoSDomainsResponse response = client.createEdgeDDoSDomains(request);
    System.out.println(response.toString());
    return response.getHttpStatusCode() == 200;
}


/**
 /* Query details of the domain name added to edge Anti-DDoS.*/
 *
 * @param client EdgeSecClient instance
 * @param domain Domain name
 * @return Domain name ID
 */
private static EdgeDDoSDomainVo queryDDoSDomain(EdgeSecClient client, String domain) {
    ListEdgeDDoSDomainsRequest request = new ListEdgeDDoSDomainsRequest();
    request.setDomainName(domain);
    ListEdgeDDoSDomainsResponse response = client.listEdgeDDoSDomains(request);
    System.out.println(response.toString());
    return Optional.ofNullable(response.getDomainList().get(0)).orElse(new EdgeDDoSDomainVo());
}


/**
 * Update the protection status of a domain name added to edge Anti-DDoS.
 *
 * @param client              EdgeSecClient instance
 * @param domainId            Domain name ID
 * @param protectedSwitchEnum Domain protection status
 * @return Updated result
 */
private static boolean updateDDoSDomain(EdgeSecClient client, String domainId, ProtectedSwitchEnum protectedSwitchEnum) {
    UpdateEdgeDDoSDomainsRequest request = new UpdateEdgeDDoSDomainsRequest();
    UpdateEdgeDDoSDomainsRequestBody body = new UpdateEdgeDDoSDomainsRequestBody();
    body.setProtectedSwitch(protectedSwitchEnum);
    request.setDomainid(domainId);
    request.setBody(body);
    UpdateEdgeDDoSDomainsResponse response = client.updateEdgeDDoSDomains(request);
    System.out.println(response.toString());
    return response.getHttpStatusCode() == 200;
}

/**
 * Delete a domain name from edge Anti-DDoS.
 *
 * @param client   EdgeSecClient instance
 * @param domainId Domain name ID
 * @return Updated result
 */
private static boolean deleteDDoSDomain(EdgeSecClient client, String domainId) {
    DeleteEdgeDDoSDomainsRequest request = new DeleteEdgeDDoSDomainsRequest();
    request.setDomainid(domainId);
    DeleteEdgeDDoSDomainsResponse response = client.deleteEdgeDDoSDomains(request);
    System.out.println(response.toString());
    return response.getHttpStatusCode() == 200;
}


/**
 * Create credentials.
 *
 * @param ak       Access key (AK) corresponding to you account
 * @param sk         Secret access key (SK) of you account
 * @param projectId IAM project ID corresponding to the account
 * @return Credentials
 */
public static ICredential getCredential(String ak, String sk, String projectId) {
    return new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
}

/**
 * Create an EdgeSec client.
 *
 * @param region Region information
 * @param auth Credentials
 * @return EdgeSecClient instance
 */
public static EdgeSecClient getClient(Region region, ICredential auth) {
    // Use the default settings.
    HttpConfig config = HttpConfig.getDefaultHttpConfig();
    config.withIgnoreSSLVerification(true);

    // Initialize the EdgeSec instance.
    return EdgeSecClient.newBuilder().withHttpConfig(config).withCredential(auth).withRegion(region).build();
}
````

## 5.FAQ
### 5.1 Why do I select Chinese mainland or Outside the Chinese mainland but not the actual region where my services are located for the Project and Region?
EdgeSec is a global service. However, when you purchase it, EdgeSec resources need to be located either in or outside the Chinese mainland.

## 6. References
For more information, see API Explorer.

## 7. Change History
| Released On | Issue|   Description  |
|:-----------:| :------: | :----------: |
| 2023-09-07  |   1.0    | This issue is the first official release.|
