/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.edgesec.v1.EdgeSecClient;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeDDoSDomainsRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.DeleteEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.DeleteEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.EdgeDDoSDomainVo;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeWafDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeWafDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ShowWafDomainResponseBody;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequestBody.ProtectedSwitchEnum;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.region.EdgeSecRegion;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DDoSDomainManagementDemo {
    public static void main(String[] args) {
        System.out.println("Start HUAWEI CLOUD Edge Security DDoS Domain Management Java Demo...");
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "<YOUR PROJECT ID>";
        Region yourRegion = EdgeSecRegion.AP_SOUTHEAST_1;
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);

        EdgeSecClient client = getClient(yourRegion, auth);

        try {
            /* 4.4.1 */
            List<ShowWafDomainResponseBody> wafDomains = queryWafDomains(client);
            if (wafDomains.size() != 0) {
                ShowWafDomainResponseBody wafDomainResponseBody = wafDomains.get(0);
                /* 4.4.2 */
                createDDoSDomain(client, wafDomainResponseBody.getId());

                /* 4.4.3 */
                EdgeDDoSDomainVo dDoSDomainVo = queryDDoSDomain(client, wafDomainResponseBody.getDomainName());

                /* 4.4.4 */
                updateDDoSDomain(client, dDoSDomainVo.getId(), ProtectedSwitchEnum.NUMBER_0);

                /* 4.4.5 */
                updateDDoSDomain(client, dDoSDomainVo.getId(), ProtectedSwitchEnum.NUMBER_1);

                /* 4.4.6 */
                deleteDDoSDomain(client, dDoSDomainVo.getId());
            }
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        System.out.println("HUAWEI CLOUD Edge Security DDoS Domain Management Java Demo End");
    }

    private static List<ShowWafDomainResponseBody> queryWafDomains(EdgeSecClient client) {
        ListEdgeWafDomainsRequest request = new ListEdgeWafDomainsRequest();
        ListEdgeWafDomainsResponse response = client.listEdgeWafDomains(request);
        return Optional.ofNullable(response.getDomainList()).orElse(new ArrayList<>());
    }

    private static boolean createDDoSDomain(EdgeSecClient client, String domainId) {
        CreateEdgeDDoSDomainsRequest request = new CreateEdgeDDoSDomainsRequest();
        CreateEdgeDDoSDomainsRequestBody body = new CreateEdgeDDoSDomainsRequestBody();
        body.setDomainId(domainId);
        request.setBody(body);
        CreateEdgeDDoSDomainsResponse response = client.createEdgeDDoSDomains(request);
        System.out.println(response.toString());
        return response.getHttpStatusCode() == 200;
    }

    private static EdgeDDoSDomainVo queryDDoSDomain(EdgeSecClient client, String domain) {
        ListEdgeDDoSDomainsRequest request = new ListEdgeDDoSDomainsRequest();
        request.setDomainName(domain);
        ListEdgeDDoSDomainsResponse response = client.listEdgeDDoSDomains(request);
        System.out.println(response.toString());
        return Optional.ofNullable(response.getDomainList().get(0)).orElse(new EdgeDDoSDomainVo());
    }

    private static boolean updateDDoSDomain(EdgeSecClient client, String domainId, ProtectedSwitchEnum protectedSwitchEnum) {
        UpdateEdgeDDoSDomainsRequest request = new UpdateEdgeDDoSDomainsRequest();
        UpdateEdgeDDoSDomainsRequestBody body = new UpdateEdgeDDoSDomainsRequestBody();
        body.setProtectedSwitch(protectedSwitchEnum);
        request.setDomainid(domainId);
        request.setBody(body);
        UpdateEdgeDDoSDomainsResponse response = client.updateEdgeDDoSDomains(request);
        System.out.println(response.toString());
        return response.getHttpStatusCode() == 200;
    }

    private static boolean deleteDDoSDomain(EdgeSecClient client, String domainId) {
        DeleteEdgeDDoSDomainsRequest request = new DeleteEdgeDDoSDomainsRequest();
        request.setDomainid(domainId);
        DeleteEdgeDDoSDomainsResponse response = client.deleteEdgeDDoSDomains(request);
        System.out.println(response.toString());
        return response.getHttpStatusCode() == 200;
    }

    public static ICredential getCredential(String ak, String sk, String projectId) {
        return new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
    }

    public static EdgeSecClient getClient(Region region, ICredential auth) {
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        return EdgeSecClient.newBuilder().withHttpConfig(config).withCredential(auth).withRegion(region).build();
    }
}
