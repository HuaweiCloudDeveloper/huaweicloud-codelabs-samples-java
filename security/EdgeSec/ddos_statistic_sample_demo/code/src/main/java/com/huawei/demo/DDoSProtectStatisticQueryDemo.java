/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.edgesec.v1.EdgeSecClient;
import com.huaweicloud.sdk.edgesec.v1.model.ShowStatisticsEventRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ShowStatisticsEventRequest.TypeEnum;
import com.huaweicloud.sdk.edgesec.v1.model.ShowStatisticsEventResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ShowStatisticsTrafficRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ShowStatisticsTrafficResponse;
import com.huaweicloud.sdk.edgesec.v1.region.EdgeSecRegion;

import java.util.Optional;

public class DDoSProtectStatisticQueryDemo {
    public static void main(String[] args) {
        System.out.println("Start HUAWEI CLOUD Edge Security DDoS Statistic Java Demo...");
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "<YOUR PROJECT ID>";
        Region yourRegion = EdgeSecRegion.AP_SOUTHEAST_1;
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
        EdgeSecClient client = getClient(yourRegion, auth);

        try {
            long startTime = 1691983276633L;
            long endTime = 1692588076633L;
            /* 4.4.1 */
            queryAttackCount(client, startTime, endTime);
            queryMaxFlowBandwidth(client, startTime, endTime);
            queryMaxDropBandwidth(client, startTime, endTime);
            queryFlowDropTraffic(client, startTime, endTime);

        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        System.out.println("HUAWEI CLOUD Edge Security DDoS Domain Management Java Demo End");
    }

    private static long queryAttackCount(EdgeSecClient client, long startTime, long endTime) {
        ShowStatisticsEventRequest request = new ShowStatisticsEventRequest();
        request.setType(TypeEnum.ATTACK_COUNT);
        request.setStartTime(startTime);
        request.setEndTime(endTime);
        ShowStatisticsEventResponse response = client.showStatisticsEvent(request);
        Long attackCount = Optional.ofNullable(response.getValue()).orElse(0L);
        System.out.println(attackCount);
        return attackCount;
    }

    private static long queryMaxFlowBandwidth(EdgeSecClient client, long startTime, long endTime) {
        ShowStatisticsTrafficRequest request = new ShowStatisticsTrafficRequest();
        request.setType(ShowStatisticsTrafficRequest.TypeEnum.MAX_FLOW_BANDWIDTH);
        request.setStartTime(startTime);
        request.setEndTime(endTime);
        ShowStatisticsTrafficResponse response = client.showStatisticsTraffic(request);
        Long maxFlowBandwidth = Optional.ofNullable(response.getValue()).orElse(0L);
        System.out.println(maxFlowBandwidth + "Kbps");
        return maxFlowBandwidth;
    }

    private static long queryMaxDropBandwidth(EdgeSecClient client, long startTime, long endTime) {
        ShowStatisticsTrafficRequest request = new ShowStatisticsTrafficRequest();
        request.setType(ShowStatisticsTrafficRequest.TypeEnum.MAX_DROP_BANDWIDTH);
        request.setStartTime(startTime);
        request.setEndTime(endTime);
        ShowStatisticsTrafficResponse response = client.showStatisticsTraffic(request);
        Long maxDropBandwidth = Optional.ofNullable(response.getValue()).orElse(0L);
        System.out.println(maxDropBandwidth + "Kbps");
        return maxDropBandwidth;
    }

    private static ShowStatisticsTrafficResponse queryFlowDropTraffic(EdgeSecClient client, long startTime, long endTime) {
        ShowStatisticsTrafficRequest request = new ShowStatisticsTrafficRequest();
        request.setType(ShowStatisticsTrafficRequest.TypeEnum.FLOW_DROP_TRAFFIC);
        request.setStartTime(startTime);
        request.setEndTime(endTime);
        ShowStatisticsTrafficResponse response = client.showStatisticsTraffic(request);
        System.out.println(response.getFlow());
        System.out.println(response.getDrop());
        return response;
    }

    public static EdgeSecClient getClient(Region region, ICredential auth) {
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        return EdgeSecClient.newBuilder().withHttpConfig(config).withCredential(auth).withRegion(region).build();
    }
}
