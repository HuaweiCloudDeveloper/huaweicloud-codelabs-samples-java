## 1.简介
华为云提供了EdgeSec服务端SDK，您可以直接集成服务端SDK来调用EdgeSec的相关API，从而实现对EdgeSec的快速操作。该示例展示如何通过EdgeSec服务查询DDoS防护数据。

## 2.开发前准备
- 已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=94fc0f9f861b4f30a85ec1f463d35609&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问秘钥](https://support.huaweicloud.com/devg-apisign/api-sign-provide-aksk.html)
- 已获取对应区域的项目，请在华为云控制台“我的凭证 > API凭证 > 项目列表”页面上查看项目——中国大陆境内的资源使用cn-north-4的项目，中国大陆境外的资源使用ap-southeast-1的项目。具体请参见 [API凭证](https://support.huaweicloud.com/devg-apisign/api-sign-provide-proid.html) 。
- 已经购买[CDN](https://www.huaweicloud.com/zh-cn/product/cdn.html)服务。
- 需要购买[边缘安全](https://www.huaweicloud.com/zh-cn/product/edgesec.html)服务。

## 3.安装sdk

您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-edgesec”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com/?language=Java) 。

````xml
<dependency>
	<groupId>com.huaweicloud.sdk</groupId>
	<artifactId>huaweicloud-sdk-edgesec</artifactId>
	<version>3.1.55</version>
</dependency>
````
## 4.开始使用
### 4.1 导入依赖模块
````java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.edgesec.v1.EdgeSecClient;
import com.huaweicloud.sdk.edgesec.v1.model.ShowStatisticsEventRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ShowStatisticsEventRequest.TypeEnum;
import com.huaweicloud.sdk.edgesec.v1.model.ShowStatisticsEventResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ShowStatisticsTrafficRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ShowStatisticsTrafficResponse;
import com.huaweicloud.sdk.edgesec.v1.region.EdgeSecRegion;
import java.util.Optional;
````

### 4.2 初始化认证信息

````java
ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
````

### 4.3 初始化边缘安全客户端

````java
HttpConfig config = HttpConfig.getDefaultHttpConfig();
config.withIgnoreSSLVerification(true);
EdgeSecClient.newBuilder().withHttpConfig(config).withCredential(auth).withRegion(region).build();
````

### 4.4 查询DDoS防护数据
此节4.4.1-4.4.2示范了在console界面上如何操作，4.4.2示范了代码如何实现上述操作。

#### 4.4.1 在边缘安全控制台，点击DDoS防护、概览，选择时间段后可查询到DDoS防护数据
![ddos_statistic_1](assets/ddos_statistic_1.png)

#### 4.4.2 示例代码
````java
public static void main(String[] args) {
    /* 边缘安全DDoS防护数据查询 */
    System.out.println("Start HUAWEI CLOUD Edge Security DDoS Statistic Java Demo...");
    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    String projectId = "<YOUR PROJECT ID>";
    /* 中国大陆境内使用EdgeSecRegion.CN_NORTH_4，中国大陆境外使用EdgeSecRegion.AP_SOUTHEAST_1*/
    Region yourRegion = EdgeSecRegion.AP_SOUTHEAST_1;
    /* 创建EdgeSecClient实例 */
    ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);

    /* 创建EdgeSecClient实例 */
    EdgeSecClient client = getClient(yourRegion, auth);

    try {
        /* 根据需要设定开始时间和结束时间*/
        long startTime = 1691983276633L;
        long endTime = 1692588076633L;
        /* 4.4.1 查询开始时间和结束时间之间的DDoS攻击事件次数*/
        queryAttackCount(client, startTime, endTime);

        /* 4.4.1 查询开始时间和结束时间之间的入流量带宽峰值*/
        queryMaxFlowBandwidth(client, startTime, endTime);

        /* 4.4.1 查询开始时间和结束时间之间的清洗流量带宽峰值*/
        queryMaxDropBandwidth(client, startTime, endTime);

        /* 4.4.1 查询开始时间和结束时间之间的清洗流量带宽峰值*/
        queryFlowDropTraffic(client, startTime, endTime);

    } catch (ConnectionException | RequestTimeoutException e) {
        System.out.println(e.getMessage());
    } catch (ServiceResponseException e) {
        System.out.println(e.getHttpStatusCode());
        System.out.println(e.getErrorCode());
        System.out.println(e.getErrorMsg());
    }
    System.out.println("HUAWEI CLOUD Edge Security DDoS Domain Management Java Demo End");
}

/**
 * 查询 DDoS 攻击次数
 *
 * @param client EdgeSecClient 实例
 * @return DDoS 攻击次数信息
 */
private static long queryAttackCount(EdgeSecClient client, long startTime, long endTime) {
    ShowStatisticsEventRequest request = new ShowStatisticsEventRequest();
    request.setType(TypeEnum.ATTACK_COUNT);
    request.setStartTime(startTime);
    request.setEndTime(endTime);
    ShowStatisticsEventResponse response = client.showStatisticsEvent(request);
    Long attackCount = Optional.ofNullable(response.getValue()).orElse(0L);
    System.out.println("DDoS 攻击次数：" + attackCount);
    return attackCount;
}

/**
 * 查询 DDoS 入流量带宽峰值
 *
 * @param client EdgeSecClient 实例
 * @return DDoS 入流量带宽峰值
 */
private static long queryMaxFlowBandwidth(EdgeSecClient client, long startTime, long endTime) {
    ShowStatisticsTrafficRequest request = new ShowStatisticsTrafficRequest();
    request.setType(ShowStatisticsTrafficRequest.TypeEnum.MAX_FLOW_BANDWIDTH);
    request.setStartTime(startTime);
    request.setEndTime(endTime);
    ShowStatisticsTrafficResponse response = client.showStatisticsTraffic(request);
    Long maxFlowBandwidth = Optional.ofNullable(response.getValue()).orElse(0L);
    System.out.println("DDoS入流量带宽峰值：" + maxFlowBandwidth + "Kbps");
    return maxFlowBandwidth;
}

/**
 * 查询 DDoS 清洗流量带宽峰值
 *
 * @param client EdgeSecClient 实例
 * @return DDoS 清洗流量带宽峰值
 */
private static long queryMaxDropBandwidth(EdgeSecClient client, long startTime, long endTime) {
    ShowStatisticsTrafficRequest request = new ShowStatisticsTrafficRequest();
    request.setType(ShowStatisticsTrafficRequest.TypeEnum.MAX_DROP_BANDWIDTH);
    request.setStartTime(startTime);
    request.setEndTime(endTime);
    ShowStatisticsTrafficResponse response = client.showStatisticsTraffic(request);
    Long maxDropBandwidth = Optional.ofNullable(response.getValue()).orElse(0L);
    System.out.println("DDoS清洗流量带宽峰值：" + maxDropBandwidth + "Kbps");
    return maxDropBandwidth;
}

/**
 * 查询 DDoS 入流量和清洗流量
 *
 * @param client EdgeSecClient 实例
 * @return DDoS 入流量和清洗流量
 */
private static ShowStatisticsTrafficResponse queryFlowDropTraffic(EdgeSecClient client, long startTime, long endTime) {
    ShowStatisticsTrafficRequest request = new ShowStatisticsTrafficRequest();
    request.setType(ShowStatisticsTrafficRequest.TypeEnum.FLOW_DROP_TRAFFIC);
    request.setStartTime(startTime);
    request.setEndTime(endTime);
    ShowStatisticsTrafficResponse response = client.showStatisticsTraffic(request);
    System.out.println("DDoS 入流量：" + response.getFlow());
    System.out.println("DDoS 清洗流量：" + response.getDrop());
    return response;
}

/**
 * 创建EdgeSec client
 *
 * @param region region信息
 * @param auth   鉴权凭证
 * @return EdgeSecClient实例
 */
public static EdgeSecClient getClient(Region region, ICredential auth) {
    // 使用默认配置
    HttpConfig config = HttpConfig.getDefaultHttpConfig();
    config.withIgnoreSSLVerification(true);

    // 初始化EdgeSec服务的客户端
    return EdgeSecClient.newBuilder().withHttpConfig(config).withCredential(auth).withRegion(region).build();
}
````

## 5.FAQ
### 5.1 为什么项目和Region不是客户服务所在的区域，而是根据中国大陆境内外区分？
边缘安全是全局级服务，客户购买的边缘安全服务资源只区分中国大陆境内和境外。

## 6.参考
更多信息请参考API Explorer

## 7.修订记录
|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2023-08-23 |   1.0    | 文档首次发布 |