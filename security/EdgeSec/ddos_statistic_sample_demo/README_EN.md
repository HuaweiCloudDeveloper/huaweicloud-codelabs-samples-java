## 1. Introduction
You can integrate EdgeSec SDKs provided by Huawei Cloud to call EdgeSec APIs, making it easier for you to use the service. This example shows how to use the EdgeSec service to query statistic data on edge Anti-DDoS.

## 2. Preparations
- You have [registered](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=94fc0f9f861b4f30a85ec1f463d35609&lang=en-us) with Huawei Cloud and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth).
- You have set up the development environment with Java JDK 1.8 or later.
- You have obtained the AK and SK of your Huawei Cloud account. You can create and view an AK/SK on the My Credentials > Access Keys page on the Huawei Cloud management console. For details, see [Obtaining an AK/SK](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).
- You have obtained the project ID of the corresponding region. To obtain it, log in to the Huawei Cloud management console, choose My Credentials > API Credentials > Projects, and locate the project. For resources in the Chinese mainland, use the ID for project cn-north-4. For resources outside the Chinese mainland, use the ID for project ap-southeast-1. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-proid.html).
- You have purchased [CDN](https://www.huaweicloud.com/intl/en-us/product/cdn.html).
- You have purchased [Edge Security](https://www.huaweicloud.com/intl/en-us/product/edgesec.html).

## 3. Install SDKs

You can obtain and install an SDK in Maven mode. You need to download and install Maven in your operating system (OS). After the installation is complete, you only need to add the corresponding dependencies to the pom.xml file of the Java project.

Before using the service SDK, you need to install huaweicloud-sdk-edgesec. For details about the SDK version, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/EdgeSec?lang=Java).

````xml
<dependency>
	<groupId>com.huaweicloud.sdk</groupId>
	<artifactId>huaweicloud-sdk-edgesec</artifactId>
	<version>3.1.55</version>
</dependency>
````
## 4. Begin to Use
### 4.1 Import Dependent Modules
````java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.edgesec.v1.EdgeSecClient;
import com.huaweicloud.sdk.edgesec.v1.model.ShowStatisticsEventRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ShowStatisticsEventRequest.TypeEnum;
import com.huaweicloud.sdk.edgesec.v1.model.ShowStatisticsEventResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ShowStatisticsTrafficRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ShowStatisticsTrafficResponse;
import com.huaweicloud.sdk.edgesec.v1.region.EdgeSecRegion;
import java.util.Optional;
````

### 4.2 Initialize Authentication Information

````java
ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
````

### 4.3 Initialize the EdgeSec Client

````java
HttpConfig config = HttpConfig.getDefaultHttpConfig();
config.withIgnoreSSLVerification(true);
EdgeSecClient.newBuilder().withHttpConfig(config).withCredential(auth).withRegion(region).build();
````

### 4.4 Query Edge Anti-DDoS Data
In this section, 4.4.1describes how to view edge Anti-DDoS protection statistic data through the console, and 4.4.2 describes how to use the same functions with code.

#### 4.4.1 On the Edge Security console, choose Edge Anti-DDoS > Overview and select a time range to query protection data.
![ddos_statistic_1](assets/ddos_statistic_1_en.png)

#### 4.4.2 Sample Code
````java
public static void main(String[] args) {
    /*Query statistic protection data on edge Anti-DDoS.*/
    System.out.println("Start HUAWEI CLOUD Edge Security DDoS Statistic Java Demo...");
    // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
    // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    String projectId = "<YOUR PROJECT ID>";
    /* EdgeSecRegion.CN_NORTH_4 is used for resources in the Chinese mainland, and EdgeSecRegion.AP_SOUTHEAST_1 is used for resources outside the Chinese mainland.*/
    Region yourRegion = EdgeSecRegion.AP_SOUTHEAST_1;
    // Create an EdgeSecClient instance.
    ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);

    // Create an EdgeSecClient instance.
    EdgeSecClient client = getClient(yourRegion, auth);

    try {
        /*Set the start time and end time as needed.*/
        long startTime = 1691983276633L;
        long endTime = 1692588076633L;
        /* 4.4.1 Query the number of DDoS attack events for the specified time range.*/
        queryAttackCount(client, startTime, endTime);

        /* 4.4.1 Query the peak bandwidth of incoming traffic during the specified time range.*/
        queryMaxFlowBandwidth(client, startTime, endTime);

        /* 4.4.1 Query the peak cleaning traffic bandwidth during the specified time range.*/
        queryMaxDropBandwidth(client, startTime, endTime);

        /* 4.4.1 Query the peak bandwidth of scrubbed traffic during the specified time range.*/
        queryFlowDropTraffic(client, startTime, endTime);

    } catch (ConnectionException | RequestTimeoutException e) {
        System.out.println(e.getMessage());
    } catch (ServiceResponseException e) {
        System.out.println(e.getHttpStatusCode());
        System.out.println(e.getErrorCode());
        System.out.println(e.getErrorMsg());
    }
    System.out.println("Huawei Cloud Edge Security DDoS Domain Management Java Demo End");
}

/**
 * Query the number of DDoS attacks.
 *
 * @param client EdgeSecClient instance
 * @return Number of DDoS attacks
 */
private static long queryAttackCount(EdgeSecClient client, long startTime, long endTime) {
    ShowStatisticsEventRequest request = new ShowStatisticsEventRequest();
    request.setType(TypeEnum.ATTACK_COUNT);
    request.setStartTime(startTime);
    request.setEndTime(endTime);
    ShowStatisticsEventResponse response = client.showStatisticsEvent(request);
    Long attackCount = Optional.ofNullable(response.getValue()).orElse(0L);
    System.out.println ("DDoS attacks: "+ attackCount);
    return attackCount;
}

/**
 * Query the peak bandwidth of inbound traffic to edge DDoS Mitigation.
 *
 * @param client EdgeSecClient instance
 * @return Peak bandwidth of inbound traffic to edge DDoS Mitigation.
 */
private static long queryMaxFlowBandwidth(EdgeSecClient client, long startTime, long endTime) {
    ShowStatisticsTrafficRequest request = new ShowStatisticsTrafficRequest();
    request.setType(ShowStatisticsTrafficRequest.TypeEnum.MAX_FLOW_BANDWIDTH);
    request.setStartTime(startTime);
    request.setEndTime(endTime);
    ShowStatisticsTrafficResponse response = client.showStatisticsTraffic(request);
    Long maxFlowBandwidth = Optional.ofNullable(response.getValue()).orElse(0L);
    System.out.println("Peak bandwidth of inbound traffic to edge DDoS Mitigation: " + maxFlowBandwidth + "Kbps");
    return maxFlowBandwidth;
}

/**
 * Query the peak bandwidth of traffic scrubbed by edge DDoS Mitigation.
 *
 * @param client EdgeSecClient instance
 * @return Peak bandwidth of traffic scrubbed by edge Anti-DDoS
 */
private static long queryMaxDropBandwidth(EdgeSecClient client, long startTime, long endTime) {
    ShowStatisticsTrafficRequest request = new ShowStatisticsTrafficRequest();
    request.setType(ShowStatisticsTrafficRequest.TypeEnum.MAX_DROP_BANDWIDTH);
    request.setStartTime(startTime);
    request.setEndTime(endTime);
    ShowStatisticsTrafficResponse response = client.showStatisticsTraffic(request);
    Long maxDropBandwidth = Optional.ofNullable(response.getValue()).orElse(0L);
    System.out.println("Peak bandwidth of traffic scrubbed by edge Anti-DDoS: " + maxDropBandwidth + "Kbps");
    return maxDropBandwidth;
}

/**
 * Query traffic inbound to and scrubbed by edge Anti-DDoS.
 *
 * @param client EdgeSecClient instance
 * @return Inbound traffic and scrubbed traffic.
 */
private static ShowStatisticsTrafficResponse queryFlowDropTraffic(EdgeSecClient client, long startTime, long endTime) {
    ShowStatisticsTrafficRequest request = new ShowStatisticsTrafficRequest();
    request.setType(ShowStatisticsTrafficRequest.TypeEnum.FLOW_DROP_TRAFFIC);
    request.setStartTime(startTime);
    request.setEndTime(endTime);
    ShowStatisticsTrafficResponse response = client.showStatisticsTraffic(request);
    System.out.println("Traffic inbound to edge Anti-DDoS: "+ response.getFlow());
    System.out.println("Traffic scrubbed by edge Anti-DDoS: "+ response.getDrop());
    return response;
}

/**
 * Create an EdgeSec client.
 *
 * @param region Region information
 * @param auth Credentials
 * @return EdgeSecClient instance
 */
public static EdgeSecClient getClient(Region region, ICredential auth) {
    // Use the default settings.
    HttpConfig config = HttpConfig.getDefaultHttpConfig();
    config.withIgnoreSSLVerification(true);

    // Initialize the EdgeSec instance.
    return EdgeSecClient.newBuilder().withHttpConfig(config).withCredential(auth).withRegion(region).build();
}
````

## 5.FAQ
### 5.1 Why do I select Chinese mainland or Outside the Chinese mainland but not the actual region where my services are located for the Project and Region?
EdgeSec is a global service. However, when you purchase it, EdgeSec resources need to be located either in or outside the Chinese mainland.

## 6. References
For more information, see API Explorer.

## 7. Change History
|  Released On | Issue|   Description  |
| :--------: | :------: | :----------: |
| 2023-09-07 |   1.0    | This issue is the first official release.|
