## 1.简介
该示例展示如何将SCM的证书添加到EdgeSec、将CDN的域名添加EdgeSec并开启WAF防护和DDoS防护。

## 2.开发前准备
- 已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=94fc0f9f861b4f30a85ec1f463d35609&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问秘钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)
- 已获取边缘安全服务区域的项目，请在华为云控制台“我的凭证 > API凭证 > 项目列表”页面上查看项目——中国大陆境内的资源使用cn-north-4的项目，中国大陆境外的资源使用ap-southeast-1的项目。具体请参见 [API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html) 。
- 已经购买[CDN](https://www.huaweicloud.com/intl/zh-cn/product/cdn.html)服务。
- 已经购买[SCM](https://www.huaweicloud.com/intl/zh-cn/product/scm.html)服务。
- 已经购买[边缘安全](https://www.huaweicloud.com/intl/zh-cn/product/edgesec.html)服务。
- 已经在CDN服务上添加加速域名。
- 已经在SCM服务上添加证书。

## 3.开发时序图
![synchronize_from_scm_and_cdn_1.png](assets/synchronize_from_scm_and_cdn_1.png)
![synchronize_from_scm_and_cdn_2.png](assets/synchronize_from_scm_and_cdn_2.png)
## 4.开始使用
### 4.1 安装sdk
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-edgesec, huaweicloud-sdk-scm, huaweicloud-sdk-cdn”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com/?language=Java) 。
````xml
<properties>
    <sdk-version>3.1.65</sdk-version>
</properties>
<dependencies>
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-edgesec</artifactId>
        <version>${sdk-version}</version>
    </dependency>
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-scm</artifactId>
        <version>${sdk-version}</version>
    </dependency>
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-cdn</artifactId>
        <version>${sdk-version}</version>
    </dependency>
</dependencies>
````
### 4.2 示例代码
````java
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo;

import com.huaweicloud.sdk.cdn.v1.region.CdnRegion;
import com.huaweicloud.sdk.cdn.v2.CdnClient;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainFullConfigRequest;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainFullConfigResponse;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.utils.StringUtils;
import com.huaweicloud.sdk.edgesec.v1.EdgeSecClient;
import com.huaweicloud.sdk.edgesec.v1.model.CreateCertificateRequest;
import com.huaweicloud.sdk.edgesec.v1.model.CreateCertificateRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.CreateCertificateResponse;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeDDoSDomainsRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeWafDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeWafDomainsRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeWafDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.CreatePolicyRequest;
import com.huaweicloud.sdk.edgesec.v1.model.CreatePolicyRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.CreatePolicyResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ListCdnDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ListCdnDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeWafDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeWafDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ShowCdnDomainResponseBody;
import com.huaweicloud.sdk.edgesec.v1.model.ShowWafDomainResponseBody;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.region.EdgeSecRegion;
import com.huaweicloud.sdk.scm.v3.ScmClient;
import com.huaweicloud.sdk.scm.v3.model.CertificateDetail;
import com.huaweicloud.sdk.scm.v3.model.ExportCertificateRequest;
import com.huaweicloud.sdk.scm.v3.model.ExportCertificateResponse;
import com.huaweicloud.sdk.scm.v3.model.ListCertificatesRequest;
import com.huaweicloud.sdk.scm.v3.model.ListCertificatesResponse;
import com.huaweicloud.sdk.scm.v3.region.ScmRegion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class DemoSample {
    private static final String SCM_REGION = "<your scm region code>";

    private static final String CDN_REGION = "<your cdn region code>";

    private static final String EDGESEC_REGION = "<your edgesec region code>";

    // 请在华为云控制台“我的凭证 > API凭证 > 项目列表”页面上查看项目——中国大陆境内的资源使用cn-north-4的项目，中国大陆境外的资源使用ap-southeast-1的项目
    private static final String EDGESEC_PROJECT_ID = "<your edgesec project id>";

    // WAF防护策略名称，用于创建WAF防护策略，并将创建的策略应用到新添加的WAF域名
    private static final String POLICY_NAME = "default_policy_";

    // DDoS防护开关，用于添加DDoS域名后，是否开启DDoS防护
    private static final boolean DDOS_PROTECT_ON = true;

    // 请求接口的间隔时间
    private static final long INTERVAL_TIME = 1000L;

    private static EdgeSecClient edgesecClient;
    private static ScmClient scmClient;
    private static CdnClient cdnClient;

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 1.create clients
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        // 1.1 create EdgeSecClient
        ICredential basicCredentials = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(EDGESEC_PROJECT_ID);
        edgesecClient = EdgeSecClient.newBuilder().withHttpConfig(config).withCredential(basicCredentials).withRegion(EdgeSecRegion.valueOf(EDGESEC_REGION)).build();
        // 1.2 create ScmClient
        ICredential scmGlobalCredentials = new GlobalCredentials().withIamEndpoint("https://iam." + SCM_REGION + ".myhuaweicloud.com").withAk(ak).withSk(sk);
        scmClient = ScmClient.newBuilder().withHttpConfig(config).withCredential(scmGlobalCredentials).withRegion(ScmRegion.valueOf(SCM_REGION)).build();
        // 1.3 create ScmClient
        ICredential cdnGlobalCredentials = new GlobalCredentials().withIamEndpoint("https://iam." + CDN_REGION + ".myhuaweicloud.com").withAk(ak).withSk(sk);
        cdnClient = CdnClient.newBuilder().withHttpConfig(config).withCredential(cdnGlobalCredentials).withRegion(CdnRegion.valueOf(CDN_REGION)).build();

        // 2.add certificates to edgesec from scm
        addCertificates();

        // 3.add waf policy
        String policyId;
        policyId = addWafPolicy();

        // 4.add waf domain
        addWafDomain(policyId);

        // 5.add ddos domain and start ddos protect
        addDDoSDomains();
    }

    /**
     * obtain certificate from scm and add them to edgesec
     */
    private static void addCertificates() {
        // 1.obtain certificate details from scm
        System.out.println("Add certificate start.");
        List<CertificateDetail> scmCertificateList = obtainCertificates();

        // 2.obtain certificate detail from scm
        Map<String, ExportCertificateResponse> scmCertificateMap = obtainCertificateDetail(scmCertificateList);

        // 3.add certificate to edgesec
        createEdgesecCertificate(scmCertificateMap);
    }

    /**
     * obtainCertificates
     */
    private static List<CertificateDetail> obtainCertificates() {
        ListCertificatesRequest listCertificatesRequest = new ListCertificatesRequest();
        int offset = 0;
        listCertificatesRequest.setOffset(offset);
        listCertificatesRequest.setLimit(50);
        ListCertificatesResponse listCertificatesResponse = scmClient.listCertificates(listCertificatesRequest);
        // if response is null, finish
        if (listCertificatesResponse == null) {
            return new ArrayList<>();
        }
        List<CertificateDetail> scmCertificateList = new ArrayList<>(listCertificatesResponse.getTotalCount());
        scmCertificateList.addAll(listCertificatesResponse.getCertificates());
        while ((offset + 1) * 50 < listCertificatesResponse.getTotalCount()) {
            offset++;
            listCertificatesRequest.setOffset(offset);
            listCertificatesResponse = scmClient.listCertificates(listCertificatesRequest);
            if (listCertificatesResponse == null || listCertificatesResponse.getCertificates() == null) {
                break;
            }
            scmCertificateList.addAll(listCertificatesResponse.getCertificates());
        }
        return scmCertificateList;
    }

    /**
     * obtainCertificateDetail
     */
    private static Map<String, ExportCertificateResponse> obtainCertificateDetail(List<CertificateDetail> scmCertificateList) {
        Map<String, ExportCertificateResponse> scmCertificateMap = new HashMap<>(scmCertificateList.size() * 2);
        for (CertificateDetail certificate : scmCertificateList) {
            if (!"ISSUED".equals(certificate.getStatus())) {
                System.out.printf("%s: certificate not ready in scm, status:%s.%n", certificate.getName(), certificate.getStatus());
                continue;
            }
            ExportCertificateRequest exportCertificateRequest = new ExportCertificateRequest();
            exportCertificateRequest.setCertificateId(certificate.getId());
            ExportCertificateResponse certificateResponse = scmClient.exportCertificate(exportCertificateRequest);
            scmCertificateMap.put(certificate.getName(), certificateResponse);
        }
        return scmCertificateMap;
    }


    /**
     * createEdgesecCertificate
     *
     * @param scmCertificateMap
     */
    private static void createEdgesecCertificate(Map<String, ExportCertificateResponse> scmCertificateMap) {
        int successNum = 0;
        for (Map.Entry<String, ExportCertificateResponse> certificateEntry : scmCertificateMap.entrySet()) {
            ExportCertificateResponse scmCertificate = certificateEntry.getValue();
            String certificateName = certificateEntry.getKey();
            // 3.1 check before add
            com.huaweicloud.sdk.edgesec.v1.model.ListCertificatesRequest listCertificatesRequestEdgeSec = new com.huaweicloud.sdk.edgesec.v1.model.ListCertificatesRequest();
            listCertificatesRequestEdgeSec.setName(certificateName);
            listCertificatesRequestEdgeSec.setProjectId(EDGESEC_PROJECT_ID);
            com.huaweicloud.sdk.edgesec.v1.model.ListCertificatesResponse listCertificatesResponseEdgeSec = edgesecClient.listCertificates(listCertificatesRequestEdgeSec);
            if (listCertificatesResponseEdgeSec != null && listCertificatesResponseEdgeSec.getTotal() > 0) {
                System.out.printf("%s: edgesec certificate already added.%n", certificateName);
                continue;
            }
            // 3.2 send CreateCertificateRequest
            successNum += sendCreateCertificateRequest(certificateName, scmCertificate);
        }
        System.out.printf("Add certificate end, success count: %s.%n", successNum);
    }

    /**
     * send CreateCertificateRequest
     *
     * @param certificateName
     * @param scmCertificate
     * @return
     */
    private static int sendCreateCertificateRequest(String certificateName, ExportCertificateResponse scmCertificate) {
        CreateCertificateRequest certificateRequest = new CreateCertificateRequest();
        CreateCertificateRequestBody certificateRequestBody = new CreateCertificateRequestBody();
        certificateRequestBody.setName(certificateName);
        certificateRequestBody.setContent(scmCertificate.getEntireCertificate());
        String privatkey = scmCertificate.getPrivateKey();
        privatkey = privatkey.substring(0, privatkey.length() - 1);
        certificateRequestBody.setKey(privatkey);
        certificateRequest.setBody(certificateRequestBody);
        certificateRequest.setProjectId(EDGESEC_PROJECT_ID);
        certificateRequest.setEnterpriseProjectId("0");
        try {
            CreateCertificateResponse createCertificateResponse = edgesecClient.createCertificate(certificateRequest);
            if (createCertificateResponse != null && 200 == createCertificateResponse.getHttpStatusCode()) {
                System.out.printf("%s: certificate already added to edgesec.%n", certificateName);
                return 1;
            }
            Thread.sleep(INTERVAL_TIME);
        } catch (ClientRequestException e) {
            System.out.printf("%s: certificate added failed to edgesec for %s.%n", certificateName, e.getErrorMsg());
        } catch (InterruptedException e) {
            System.out.printf("%s: thread slept failed after certificate added to edgesec for %s.%n", certificateName, e.getMessage());
        } catch (Exception e) {
            System.out.printf("%s: certificate added failed to edgesec for %s.%n", certificateName, e.getMessage());
        }
        return 0;
    }

    /**
     * add waf policy
     *
     * @return policyId
     */
    private static String addWafPolicy() {
        System.out.println("Add waf policy start.");
        CreatePolicyRequest request = new CreatePolicyRequest();
        request.withProjectId(EDGESEC_PROJECT_ID);
        CreatePolicyRequestBody body = new CreatePolicyRequestBody();
        body.setName(POLICY_NAME + System.currentTimeMillis());
        request.withBody(body);
        CreatePolicyResponse response = edgesecClient.createPolicy(request);
        if (response != null && 200 == response.getHttpStatusCode()) {
            System.out.println("Add waf policy success.");
            return response.getId();
        }
        System.out.println("Add waf policy failed.");
        return null;
    }

    /**
     * obtain domain from cdn and add them to edgesec as waf domain
     *
     * @param policyId policy which will be used by waf domain
     */
    private static void addWafDomain(String policyId) {
        System.out.println("Add waf domain start.");
        // 1.obtain domains
        ListCdnDomainsRequest request = new ListCdnDomainsRequest();
        ListCdnDomainsResponse cdnDomainsResponse = edgesecClient.listCdnDomains(request);
        // 2.build CreateEdgeWafDomainsRequest
        int successNum = 0;
        for (ShowCdnDomainResponseBody domain : cdnDomainsResponse.getDomains()) {
            CreateEdgeWafDomainsRequest createEdgeWafDomainsRequest = new CreateEdgeWafDomainsRequest();
            CreateEdgeWafDomainsRequestBody createEdgeWafDomainsRequestBody = new CreateEdgeWafDomainsRequestBody();
            createEdgeWafDomainsRequestBody.setDomainName(domain.getDomainName());
            createEdgeWafDomainsRequestBody.setPolicyId(policyId);
            createEdgeWafDomainsRequestBody.setAreaType(CreateEdgeWafDomainsRequestBody.AreaTypeEnum.valueOf(domain.getServiceArea().getValue()));
            // 2.1 obtain https information from cdn
            if (domain.getHttpsStatus() == 1) {
                obtainHttpsInformationFromCdn(createEdgeWafDomainsRequestBody);

            }
            createEdgeWafDomainsRequest.withBody(createEdgeWafDomainsRequestBody);
            // 2.2 send CreateEdgeWafDomainsRequest
            successNum += sendCreateEdgeWafDomainsRequest(createEdgeWafDomainsRequest);
        }
        System.out.printf("Add waf domain end, success count: %s.%n", successNum);
    }

    /**
     * obtainHttpsInformationFromCdn
     *
     * @param createEdgeWafDomainsRequestBody
     */
    private static void obtainHttpsInformationFromCdn(CreateEdgeWafDomainsRequestBody createEdgeWafDomainsRequestBody) {
        ShowDomainFullConfigRequest showDomainFullConfigRequest = new ShowDomainFullConfigRequest();
        showDomainFullConfigRequest.withDomainName(createEdgeWafDomainsRequestBody.getDomainName());
        ShowDomainFullConfigResponse showDomainFullConfigResponse = cdnClient.showDomainFullConfig(showDomainFullConfigRequest);
        if (showDomainFullConfigResponse == null || showDomainFullConfigResponse.getHttpStatusCode() != 200) {
            System.out.printf("%s: add waf domain failed.%n", createEdgeWafDomainsRequestBody.getDomainName());
            return;
        }
        String httpsStatus = Optional.ofNullable(showDomainFullConfigResponse.getConfigs().getHttps().getHttpsStatus()).orElse("off");
        if (!StringUtils.isEmpty(httpsStatus) && "on".equals(httpsStatus)) {
            String certificateName = showDomainFullConfigResponse.getConfigs().getHttps().getCertificateName();
            com.huaweicloud.sdk.edgesec.v1.model.ListCertificatesRequest listCertificatesRequest = new com.huaweicloud.sdk.edgesec.v1.model.ListCertificatesRequest();
            listCertificatesRequest.setName(certificateName);
            com.huaweicloud.sdk.edgesec.v1.model.ListCertificatesResponse listCertificatesResponse = edgesecClient.listCertificates(listCertificatesRequest);
            if (listCertificatesResponse != null && listCertificatesResponse.getTotal() > 0) {
                createEdgeWafDomainsRequestBody.setCertificateId(listCertificatesResponse.getItems().get(0).getId());
            } else {
                System.out.printf("%s: edgesec certificate not found", certificateName);
            }
        }
    }

    /**
     * sendCreateEdgeWafDomainsRequest
     *
     * @param createEdgeWafDomainsRequest
     */
    private static int sendCreateEdgeWafDomainsRequest(CreateEdgeWafDomainsRequest createEdgeWafDomainsRequest) {
        try {
            CreateEdgeWafDomainsResponse createEdgeWafDomainsResponse = edgesecClient.createEdgeWafDomains(createEdgeWafDomainsRequest);
            if (createEdgeWafDomainsResponse != null && 200 == createEdgeWafDomainsResponse.getHttpStatusCode()) {
                System.out.printf("%s: add waf domain success.%n", createEdgeWafDomainsRequest.getBody().getDomainName());
                return 1;
            } else {
                System.out.printf("%s: add waf domain failed.%n", createEdgeWafDomainsRequest.getBody().getDomainName());
            }
            Thread.sleep(INTERVAL_TIME);
        } catch (ClientRequestException e) {
            System.out.printf("%s: certificate added failed to edgesec for %s.%n", createEdgeWafDomainsRequest.getBody().getDomainName(), e.getErrorMsg());
        } catch (InterruptedException e) {
            System.out.printf("%s: thread slept failed after domain added to edgesec for %s.%n", createEdgeWafDomainsRequest.getBody().getDomainName(), e.getMessage());
        } catch (Exception e) {
            System.out.printf("%s: certificate added failed to edgesec for %s.%n", createEdgeWafDomainsRequest.getBody().getDomainName(), e.getMessage());
        }
        return 0;
    }

    /**
     * add ddos domains and start ddos protection
     */
    private static void addDDoSDomains() {
        // 1.obtain waf domain list
        System.out.println("Add ddos domain start.");
        ListEdgeWafDomainsRequest listEdgeWafDomainsRequest = new ListEdgeWafDomainsRequest();
        listEdgeWafDomainsRequest.withPageNum(0);
        ListEdgeWafDomainsResponse listEdgeWafDomainsResponse = edgesecClient.listEdgeWafDomains(listEdgeWafDomainsRequest);
        int ddosDomainNum = 0;
        int startDdoSNum = 0;
        for (ShowWafDomainResponseBody wafDomainResponseBody : listEdgeWafDomainsResponse.getDomainList()) {
            // 2.add ddos domains
            ddosDomainNum += addDdosDomain(wafDomainResponseBody);
            // 3.start ddos protection
            if (DDOS_PROTECT_ON) {
                startDdoSNum += startDdosProtection(wafDomainResponseBody);
            }
        }
        System.out.printf("Add ddos domain end, ddos domain count: %s, start ddos protection count:%s", ddosDomainNum, startDdoSNum);
    }

    /**
     * addDdosDomain
     *
     * @param wafDomainResponseBody
     * @return
     */
    private static int addDdosDomain(ShowWafDomainResponseBody wafDomainResponseBody) {
        // 1 check whether added
        ListEdgeDDoSDomainsRequest listEdgeDDoSDomainsRequest = new ListEdgeDDoSDomainsRequest();
        listEdgeDDoSDomainsRequest.withDomainName(wafDomainResponseBody.getDomainName());
        ListEdgeDDoSDomainsResponse listEdgeDDoSDomains = edgesecClient.listEdgeDDoSDomains(listEdgeDDoSDomainsRequest);
        if (listEdgeDDoSDomains.getTotal() != 0) {
            System.out.printf("%s: domain already added to ddos protect.%n", wafDomainResponseBody.getDomainName());
            return 0;
        }
        // 2 add ddos domain
        CreateEdgeDDoSDomainsRequest createEdgeDDoSDomainsRequest = new CreateEdgeDDoSDomainsRequest();
        CreateEdgeDDoSDomainsRequestBody createEdgeDDoSDomainsRequestBody = new CreateEdgeDDoSDomainsRequestBody();
        createEdgeDDoSDomainsRequestBody.setDomainId(wafDomainResponseBody.getId());
        createEdgeDDoSDomainsRequest.withBody(createEdgeDDoSDomainsRequestBody);
        try {
            CreateEdgeDDoSDomainsResponse createEdgeDDoSDomainsResponse = edgesecClient.createEdgeDDoSDomains(createEdgeDDoSDomainsRequest);
            if (createEdgeDDoSDomainsResponse != null && createEdgeDDoSDomainsResponse.getHttpStatusCode() == 200) {
                System.out.printf("%s: ddos domain added.%n", wafDomainResponseBody.getDomainName());
                return 1;
            }
            Thread.sleep(INTERVAL_TIME);
        } catch (ClientRequestException e) {
            System.out.printf("%s: add ddos protect domain failed %s.%n", wafDomainResponseBody.getDomainName(), e.getErrorMsg());
        } catch (InterruptedException e) {
            System.out.printf("%s: thread slept failed after domain added to edgesec for %s.%n", wafDomainResponseBody.getDomainName(), e.getMessage());
        } catch (Exception e) {
            System.out.printf("%s: add ddos protect domain failed %s.%n", wafDomainResponseBody.getDomainName(), e.getMessage());
        }
        return 0;
    }

    /**
     * startDdosProtection
     *
     * @param wafDomainResponseBody
     * @return
     */
    private static int startDdosProtection(ShowWafDomainResponseBody wafDomainResponseBody) {
        UpdateEdgeDDoSDomainsRequest updateEdgeDDoSDomainsRequest = new UpdateEdgeDDoSDomainsRequest();
        updateEdgeDDoSDomainsRequest.setDomainid(wafDomainResponseBody.getId());
        UpdateEdgeDDoSDomainsRequestBody updateEdgeDDoSDomainsRequestBody = new UpdateEdgeDDoSDomainsRequestBody();
        updateEdgeDDoSDomainsRequestBody.setProtectedSwitch(UpdateEdgeDDoSDomainsRequestBody.ProtectedSwitchEnum.NUMBER_1);
        updateEdgeDDoSDomainsRequest.withBody(updateEdgeDDoSDomainsRequestBody);
        try {
            UpdateEdgeDDoSDomainsResponse response = edgesecClient.updateEdgeDDoSDomains(updateEdgeDDoSDomainsRequest);
            if (response != null && response.getHttpStatusCode() == 200) {
                System.out.printf("%s: domain start ddos protection success.%n", wafDomainResponseBody.getDomainName());
                return 1;
            }
            Thread.sleep(INTERVAL_TIME);
        } catch (ClientRequestException e) {
            System.out.printf("%s: domain start ddos protection failed %s.%n", wafDomainResponseBody.getDomainName(), e.getErrorMsg());
        } catch (InterruptedException e) {
            System.out.printf("%s: thread slept failed after domain start ddos protection for %s.%n", wafDomainResponseBody.getDomainName(), e.getMessage());
        } catch (Exception e) {
            System.out.printf("%s: domain start ddos protection failed %s.%n", wafDomainResponseBody.getDomainName(), e.getMessage());
        }
        return 0;
    }

}
````
## 5.FAQ
### 5.1 为什么项目和Region不是客户服务所在的区域，而是根据中国大陆境内外区分？
边缘安全、SCM和CDN是全局级服务，客户购买的这些云服务资源只区分中国大陆境内和境外。

## 6.参考
更多信息请参考API Explorer

## 7.修订记录
|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2023-11-23 |   1.0    | 文档首次发布 |