## 1. Introduction
This example shows how to add an SSL certificate purchased through SCM certificate to EdgeSec, add a CDN domain name to EdgeSec, and enable WAF and DDoS protection.

## 2. Preparations
- You have [registered](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=94fc0f9f861b4f30a85ec1f463d35609&lang=zh-cn) with Huawei Cloud and completed [real-name authentication](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth).
- You have set up the development environment with Java JDK 1.8 or later.
- You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html).
- You have obtained the project of the region for EdgeSec. To obtain it, log in to the Huawei Cloud management console, choose My Credentials > API Credentials > Projects, and locate the project. For resources in the Chinese mainland, use project cn-north-4. For resources outside the Chinese mainland, use project ap-southeast-1. For details, see [API Credentials](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0002.html).
- You have purchased [CDN](https://www.huaweicloud.com/intl/en-us/product/cdn.html).
- You have purchased an SSL certificate from [SCM](https://www.huaweicloud.com/intl/en-us/product/scm.html).
- You have purchased [Edge Security](https://www.huaweicloud.com/intl/en-us/product/edgesec.html).
- You have added an acceleration domain name to CDN.
- A certificate has been added to SCM.

## 3. Development Sequence Diagram
![synchronize_from_scm_and_cdn_en_1.png](assets/synchronize_from_scm_and_cdn_en_1.png)
![synchronize_from_scm_and_cdn_en_2.png](assets/synchronize_from_scm_and_cdn_en_2.png)
## 4. Begin to Use
### 4.1. Install SDKs
You can obtain and install an SDK in Maven mode. Download and install Maven in your operating system (OS). After the installation is complete, add the corresponding dependencies to the pom.xml file of the Java project.

Before using the service SDK, you need to install huaweicloud-sdk-edgesec, huaweicloud-sdk-scm, and huaweicloud-sdk-cdn. For details about the SDK version, see [SDK Developer Center](https://sdkcenter.developer.huaweicloud.com/?language=Java).
````xml
<properties>
    <sdk-version>3.1.65</sdk-version>
</properties>
<dependencies>
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-edgesec</artifactId>
        <version>${sdk-version}</version>
    </dependency>
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-scm</artifactId>
        <version>${sdk-version}</version>
    </dependency>
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-cdn</artifactId>
        <version>${sdk-version}</version>
    </dependency>
</dependencies>
````
### 4.2 Sample Code
````java
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo;

import com.huaweicloud.sdk.cdn.v1.region.CdnRegion;
import com.huaweicloud.sdk.cdn.v2.CdnClient;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainFullConfigRequest;
import com.huaweicloud.sdk.cdn.v2.model.ShowDomainFullConfigResponse;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.utils.StringUtils;
import com.huaweicloud.sdk.edgesec.v1.EdgeSecClient;
import com.huaweicloud.sdk.edgesec.v1.model.CreateCertificateRequest;
import com.huaweicloud.sdk.edgesec.v1.model.CreateCertificateRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.CreateCertificateResponse;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeDDoSDomainsRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeWafDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeWafDomainsRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.CreateEdgeWafDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.CreatePolicyRequest;
import com.huaweicloud.sdk.edgesec.v1.model.CreatePolicyRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.CreatePolicyResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ListCdnDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ListCdnDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeWafDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.ListEdgeWafDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.model.ShowCdnDomainResponseBody;
import com.huaweicloud.sdk.edgesec.v1.model.ShowWafDomainResponseBody;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequest;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsRequestBody;
import com.huaweicloud.sdk.edgesec.v1.model.UpdateEdgeDDoSDomainsResponse;
import com.huaweicloud.sdk.edgesec.v1.region.EdgeSecRegion;
import com.huaweicloud.sdk.scm.v3.ScmClient;
import com.huaweicloud.sdk.scm.v3.model.CertificateDetail;
import com.huaweicloud.sdk.scm.v3.model.ExportCertificateRequest;
import com.huaweicloud.sdk.scm.v3.model.ExportCertificateResponse;
import com.huaweicloud.sdk.scm.v3.model.ListCertificatesRequest;
import com.huaweicloud.sdk.scm.v3.model.ListCertificatesResponse;
import com.huaweicloud.sdk.scm.v3.region.ScmRegion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class DemoSample {
    private static final String SCM_REGION = "<your scm region code>";

    private static final String CDN_REGION = "<your cdn region code>";

    private static final String EDGESEC_REGION = "<your edgesec region code>";

    //To obtain it, log in to the Huawei Cloud management console, choose My Credentials > API Credentials > Projects, and locate the project. For resources in the Chinese mainland, use project cn-north-4. For resources outside the Chinese mainland, use project ap-southeast-1.
    private static final String EDGESEC_PROJECT_ID = "<your edgesec project id>";

    // WAF protection policy name, which is used to create a WAF protection policy and apply the created policy to the domain name newly added to WAF.
    private static final String POLICY_NAME = "default_policy_";

    // Anti-DDoS switch, which is used to determine whether to enable Anti-DDoS for a domain name after it is added to Anti-DDoS.
    private static final boolean DDOS_PROTECT_ON = true;

    //Interval for calling the API.
    private static final long INTERVAL_TIME = 1000L;

    private static EdgeSecClient edgesecClient;
    private static ScmClient scmClient;
    private static CdnClient cdnClient;

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 1.create clients
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        // 1.1 create EdgeSecClient
        ICredential basicCredentials = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(EDGESEC_PROJECT_ID);
        edgesecClient = EdgeSecClient.newBuilder().withHttpConfig(config).withCredential(basicCredentials).withRegion(EdgeSecRegion.valueOf(EDGESEC_REGION)).build();
        // 1.2 create ScmClient
        ICredential scmGlobalCredentials = new GlobalCredentials().withIamEndpoint("https://iam." + SCM_REGION + ".myhuaweicloud.com").withAk(ak).withSk(sk);
        scmClient = ScmClient.newBuilder().withHttpConfig(config).withCredential(scmGlobalCredentials).withRegion(ScmRegion.valueOf(SCM_REGION)).build();
        // 1.3 create ScmClient
        ICredential cdnGlobalCredentials = new GlobalCredentials().withIamEndpoint("https://iam." + CDN_REGION + ".myhuaweicloud.com").withAk(ak).withSk(sk);
        cdnClient = CdnClient.newBuilder().withHttpConfig(config).withCredential(cdnGlobalCredentials).withRegion(CdnRegion.valueOf(CDN_REGION)).build();

        // 2.add certificates to edgesec from scm
        addCertificates();

        // 3.add waf policy
        String policyId;
        policyId = addWafPolicy();

        // 4.add waf domain
        addWafDomain(policyId);

        // 5.add ddos domain and start ddos protect
        addDDoSDomains();
    }

    /**
     * obtain certificate from scm and add them to edgesec
     */
    private static void addCertificates() {
        // 1.obtain certificate details from scm
        System.out.println("Add certificate start.");
        List<CertificateDetail> scmCertificateList = obtainCertificates();

        // 2.obtain certificate detail from scm
        Map<String, ExportCertificateResponse> scmCertificateMap = obtainCertificateDetail(scmCertificateList);

        // 3.add certificate to edgesec
        createEdgesecCertificate(scmCertificateMap);
    }

    /**
     * obtainCertificates
     */
    private static List<CertificateDetail> obtainCertificates() {
        ListCertificatesRequest listCertificatesRequest = new ListCertificatesRequest();
        int offset = 0;
        listCertificatesRequest.setOffset(offset);
        listCertificatesRequest.setLimit(50);
        ListCertificatesResponse listCertificatesResponse = scmClient.listCertificates(listCertificatesRequest);
        // if response is null, finish
        if (listCertificatesResponse == null) {
            return new ArrayList<>();
        }
        List<CertificateDetail> scmCertificateList = new ArrayList<>(listCertificatesResponse.getTotalCount());
        scmCertificateList.addAll(listCertificatesResponse.getCertificates());
        while ((offset + 1) * 50 < listCertificatesResponse.getTotalCount()) {
            offset++;
            listCertificatesRequest.setOffset(offset);
            listCertificatesResponse = scmClient.listCertificates(listCertificatesRequest);
            if (listCertificatesResponse == null || listCertificatesResponse.getCertificates() == null) {
                break;
            }
            scmCertificateList.addAll(listCertificatesResponse.getCertificates());
        }
        return scmCertificateList;
    }

    /**
     * obtainCertificateDetail
     */
    private static Map<String, ExportCertificateResponse> obtainCertificateDetail(List<CertificateDetail> scmCertificateList) {
        Map<String, ExportCertificateResponse> scmCertificateMap = new HashMap<>(scmCertificateList.size() * 2);
        for (CertificateDetail certificate : scmCertificateList) {
            if (!"ISSUED".equals(certificate.getStatus())) {
                System.out.printf("%s: certificate not ready in scm, status:%s.%n", certificate.getName(), certificate.getStatus());
                continue;
            }
            ExportCertificateRequest exportCertificateRequest = new ExportCertificateRequest();
            exportCertificateRequest.setCertificateId(certificate.getId());
            ExportCertificateResponse certificateResponse = scmClient.exportCertificate(exportCertificateRequest);
            scmCertificateMap.put(certificate.getName(), certificateResponse);
        }
        return scmCertificateMap;
    }


    /**
     * createEdgesecCertificate
     *
     * @param scmCertificateMap
     */
    private static void createEdgesecCertificate(Map<String, ExportCertificateResponse> scmCertificateMap) {
        int successNum = 0;
        for (Map.Entry<String, ExportCertificateResponse> certificateEntry : scmCertificateMap.entrySet()) {
            ExportCertificateResponse scmCertificate = certificateEntry.getValue();
            String certificateName = certificateEntry.getKey();
            // 3.1 check before add
            com.huaweicloud.sdk.edgesec.v1.model.ListCertificatesRequest listCertificatesRequestEdgeSec = new com.huaweicloud.sdk.edgesec.v1.model.ListCertificatesRequest();
            listCertificatesRequestEdgeSec.setName(certificateName);
            listCertificatesRequestEdgeSec.setProjectId(EDGESEC_PROJECT_ID);
            com.huaweicloud.sdk.edgesec.v1.model.ListCertificatesResponse listCertificatesResponseEdgeSec = edgesecClient.listCertificates(listCertificatesRequestEdgeSec);
            if (listCertificatesResponseEdgeSec != null && listCertificatesResponseEdgeSec.getTotal() > 0) {
                System.out.printf("%s: edgesec certificate already added.%n", certificateName);
                continue;
            }
            // 3.2 send CreateCertificateRequest
            successNum += sendCreateCertificateRequest(certificateName, scmCertificate);
        }
        System.out.printf("Add certificate end, success count: %s.%n", successNum);
    }

    /**
     * send CreateCertificateRequest
     *
     * @param certificateName
     * @param scmCertificate
     * @return
     */
    private static int sendCreateCertificateRequest(String certificateName, ExportCertificateResponse scmCertificate) {
        CreateCertificateRequest certificateRequest = new CreateCertificateRequest();
        CreateCertificateRequestBody certificateRequestBody = new CreateCertificateRequestBody();
        certificateRequestBody.setName(certificateName);
        certificateRequestBody.setContent(scmCertificate.getEntireCertificate());
        String privatkey = scmCertificate.getPrivateKey();
        privatkey = privatkey.substring(0, privatkey.length() - 1);
        certificateRequestBody.setKey(privatkey);
        certificateRequest.setBody(certificateRequestBody);
        certificateRequest.setProjectId(EDGESEC_PROJECT_ID);
        certificateRequest.setEnterpriseProjectId("0");
        try {
            CreateCertificateResponse createCertificateResponse = edgesecClient.createCertificate(certificateRequest);
            if (createCertificateResponse != null && 200 == createCertificateResponse.getHttpStatusCode()) {
                System.out.printf("%s: certificate already added to edgesec.%n", certificateName);
                return 1;
            }
            Thread.sleep(INTERVAL_TIME);
        } catch (ClientRequestException e) {
            System.out.printf("%s: certificate added failed to edgesec for %s.%n", certificateName, e.getErrorMsg());
        } catch (InterruptedException e) {
            System.out.printf("%s: thread slept failed after certificate added to edgesec for %s.%n", certificateName, e.getMessage());
        } catch (Exception e) {
            System.out.printf("%s: certificate added failed to edgesec for %s.%n", certificateName, e.getMessage());
        }
        return 0;
    }

    /**
     * add waf policy
     *
     * @return policyId
     */
    private static String addWafPolicy() {
        System.out.println("Add waf policy start.");
        CreatePolicyRequest request = new CreatePolicyRequest();
        request.withProjectId(EDGESEC_PROJECT_ID);
        CreatePolicyRequestBody body = new CreatePolicyRequestBody();
        body.setName(POLICY_NAME + System.currentTimeMillis());
        request.withBody(body);
        CreatePolicyResponse response = edgesecClient.createPolicy(request);
        if (response != null && 200 == response.getHttpStatusCode()) {
            System.out.println("Add waf policy success.");
            return response.getId();
        }
        System.out.println("Add waf policy failed.");
        return null;
    }

    /**
     * obtain domain from cdn and add them to edgesec as waf domain
     *
     * @param policyId policy which will be used by waf domain
     */
    private static void addWafDomain(String policyId) {
        System.out.println("Add waf domain start.");
        // 1.obtain domains
        ListCdnDomainsRequest request = new ListCdnDomainsRequest();
        ListCdnDomainsResponse cdnDomainsResponse = edgesecClient.listCdnDomains(request);
        // 2.build CreateEdgeWafDomainsRequest
        int successNum = 0;
        for (ShowCdnDomainResponseBody domain : cdnDomainsResponse.getDomains()) {
            CreateEdgeWafDomainsRequest createEdgeWafDomainsRequest = new CreateEdgeWafDomainsRequest();
            CreateEdgeWafDomainsRequestBody createEdgeWafDomainsRequestBody = new CreateEdgeWafDomainsRequestBody();
            createEdgeWafDomainsRequestBody.setDomainName(domain.getDomainName());
            createEdgeWafDomainsRequestBody.setPolicyId(policyId);
            createEdgeWafDomainsRequestBody.setAreaType(CreateEdgeWafDomainsRequestBody.AreaTypeEnum.valueOf(domain.getServiceArea().getValue()));
            // 2.1 obtain https information from cdn
            if (domain.getHttpsStatus() == 1) {
                obtainHttpsInformationFromCdn(createEdgeWafDomainsRequestBody);

            }
            createEdgeWafDomainsRequest.withBody(createEdgeWafDomainsRequestBody);
            // 2.2 send CreateEdgeWafDomainsRequest
            successNum += sendCreateEdgeWafDomainsRequest(createEdgeWafDomainsRequest);
        }
        System.out.printf("Add waf domain end, success count: %s.%n", successNum);
    }

    /**
     * obtainHttpsInformationFromCdn
     *
     * @param createEdgeWafDomainsRequestBody
     */
    private static void obtainHttpsInformationFromCdn(CreateEdgeWafDomainsRequestBody createEdgeWafDomainsRequestBody) {
        ShowDomainFullConfigRequest showDomainFullConfigRequest = new ShowDomainFullConfigRequest();
        showDomainFullConfigRequest.withDomainName(createEdgeWafDomainsRequestBody.getDomainName());
        ShowDomainFullConfigResponse showDomainFullConfigResponse = cdnClient.showDomainFullConfig(showDomainFullConfigRequest);
        if (showDomainFullConfigResponse == null || showDomainFullConfigResponse.getHttpStatusCode() != 200) {
            System.out.printf("%s: add waf domain failed.%n", createEdgeWafDomainsRequestBody.getDomainName());
            return;
        }
        String httpsStatus = Optional.ofNullable(showDomainFullConfigResponse.getConfigs().getHttps().getHttpsStatus()).orElse("off");
        if (!StringUtils.isEmpty(httpsStatus) && "on".equals(httpsStatus)) {
            String certificateName = showDomainFullConfigResponse.getConfigs().getHttps().getCertificateName();
            com.huaweicloud.sdk.edgesec.v1.model.ListCertificatesRequest listCertificatesRequest = new com.huaweicloud.sdk.edgesec.v1.model.ListCertificatesRequest();
            listCertificatesRequest.setName(certificateName);
            com.huaweicloud.sdk.edgesec.v1.model.ListCertificatesResponse listCertificatesResponse = edgesecClient.listCertificates(listCertificatesRequest);
            if (listCertificatesResponse != null && listCertificatesResponse.getTotal() > 0) {
                createEdgeWafDomainsRequestBody.setCertificateId(listCertificatesResponse.getItems().get(0).getId());
            } else {
                System.out.printf("%s: edgesec certificate not found", certificateName);
            }
        }
    }

    /**
     * sendCreateEdgeWafDomainsRequest
     *
     * @param createEdgeWafDomainsRequest
     */
    private static int sendCreateEdgeWafDomainsRequest(CreateEdgeWafDomainsRequest createEdgeWafDomainsRequest) {
        try {
            CreateEdgeWafDomainsResponse createEdgeWafDomainsResponse = edgesecClient.createEdgeWafDomains(createEdgeWafDomainsRequest);
            if (createEdgeWafDomainsResponse != null && 200 == createEdgeWafDomainsResponse.getHttpStatusCode()) {
                System.out.printf("%s: add waf domain success.%n", createEdgeWafDomainsRequest.getBody().getDomainName());
                return 1;
            } else {
                System.out.printf("%s: add waf domain failed.%n", createEdgeWafDomainsRequest.getBody().getDomainName());
            }
            Thread.sleep(INTERVAL_TIME);
        } catch (ClientRequestException e) {
            System.out.printf("%s: certificate added failed to edgesec for %s.%n", createEdgeWafDomainsRequest.getBody().getDomainName(), e.getErrorMsg());
        } catch (InterruptedException e) {
            System.out.printf("%s: thread slept failed after domain added to edgesec for %s.%n", createEdgeWafDomainsRequest.getBody().getDomainName(), e.getMessage());
        } catch (Exception e) {
            System.out.printf("%s: certificate added failed to edgesec for %s.%n", createEdgeWafDomainsRequest.getBody().getDomainName(), e.getMessage());
        }
        return 0;
    }

    /**
     * add ddos domains and start ddos protection
     */
    private static void addDDoSDomains() {
        // 1.obtain waf domain list
        System.out.println("Add ddos domain start.");
        ListEdgeWafDomainsRequest listEdgeWafDomainsRequest = new ListEdgeWafDomainsRequest();
        listEdgeWafDomainsRequest.withPageNum(0);
        ListEdgeWafDomainsResponse listEdgeWafDomainsResponse = edgesecClient.listEdgeWafDomains(listEdgeWafDomainsRequest);
        int ddosDomainNum = 0;
        int startDdoSNum = 0;
        for (ShowWafDomainResponseBody wafDomainResponseBody : listEdgeWafDomainsResponse.getDomainList()) {
            // 2.add ddos domains
            ddosDomainNum += addDdosDomain(wafDomainResponseBody);
            // 3.start ddos protection
            if (DDOS_PROTECT_ON) {
                startDdoSNum += startDdosProtection(wafDomainResponseBody);
            }
        }
        System.out.printf("Add ddos domain end, ddos domain count: %s, start ddos protection count:%s", ddosDomainNum, startDdoSNum);
    }

    /**
     * addDdosDomain
     *
     * @param wafDomainResponseBody
     * @return
     */
    private static int addDdosDomain(ShowWafDomainResponseBody wafDomainResponseBody) {
        // 1 check whether added
        ListEdgeDDoSDomainsRequest listEdgeDDoSDomainsRequest = new ListEdgeDDoSDomainsRequest();
        listEdgeDDoSDomainsRequest.withDomainName(wafDomainResponseBody.getDomainName());
        ListEdgeDDoSDomainsResponse listEdgeDDoSDomains = edgesecClient.listEdgeDDoSDomains(listEdgeDDoSDomainsRequest);
        if (listEdgeDDoSDomains.getTotal() != 0) {
            System.out.printf("%s: domain already added to ddos protect.%n", wafDomainResponseBody.getDomainName());
            return 0;
        }
        // 2 add ddos domain
        CreateEdgeDDoSDomainsRequest createEdgeDDoSDomainsRequest = new CreateEdgeDDoSDomainsRequest();
        CreateEdgeDDoSDomainsRequestBody createEdgeDDoSDomainsRequestBody = new CreateEdgeDDoSDomainsRequestBody();
        createEdgeDDoSDomainsRequestBody.setDomainId(wafDomainResponseBody.getId());
        createEdgeDDoSDomainsRequest.withBody(createEdgeDDoSDomainsRequestBody);
        try {
            CreateEdgeDDoSDomainsResponse createEdgeDDoSDomainsResponse = edgesecClient.createEdgeDDoSDomains(createEdgeDDoSDomainsRequest);
            if (createEdgeDDoSDomainsResponse != null && createEdgeDDoSDomainsResponse.getHttpStatusCode() == 200) {
                System.out.printf("%s: ddos domain added.%n", wafDomainResponseBody.getDomainName());
                return 1;
            }
            Thread.sleep(INTERVAL_TIME);
        } catch (ClientRequestException e) {
            System.out.printf("%s: add ddos protect domain failed %s.%n", wafDomainResponseBody.getDomainName(), e.getErrorMsg());
        } catch (InterruptedException e) {
            System.out.printf("%s: thread slept failed after domain added to edgesec for %s.%n", wafDomainResponseBody.getDomainName(), e.getMessage());
        } catch (Exception e) {
            System.out.printf("%s: add ddos protect domain failed %s.%n", wafDomainResponseBody.getDomainName(), e.getMessage());
        }
        return 0;
    }

    /**
     * startDdosProtection
     *
     * @param wafDomainResponseBody
     * @return
     */
    private static int startDdosProtection(ShowWafDomainResponseBody wafDomainResponseBody) {
        UpdateEdgeDDoSDomainsRequest updateEdgeDDoSDomainsRequest = new UpdateEdgeDDoSDomainsRequest();
        updateEdgeDDoSDomainsRequest.setDomainid(wafDomainResponseBody.getId());
        UpdateEdgeDDoSDomainsRequestBody updateEdgeDDoSDomainsRequestBody = new UpdateEdgeDDoSDomainsRequestBody();
        updateEdgeDDoSDomainsRequestBody.setProtectedSwitch(UpdateEdgeDDoSDomainsRequestBody.ProtectedSwitchEnum.NUMBER_1);
        updateEdgeDDoSDomainsRequest.withBody(updateEdgeDDoSDomainsRequestBody);
        try {
            UpdateEdgeDDoSDomainsResponse response = edgesecClient.updateEdgeDDoSDomains(updateEdgeDDoSDomainsRequest);
            if (response != null && response.getHttpStatusCode() == 200) {
                System.out.printf("%s: domain start ddos protection success.%n", wafDomainResponseBody.getDomainName());
                return 1;
            }
            Thread.sleep(INTERVAL_TIME);
        } catch (ClientRequestException e) {
            System.out.printf("%s: domain start ddos protection failed %s.%n", wafDomainResponseBody.getDomainName(), e.getErrorMsg());
        } catch (InterruptedException e) {
            System.out.printf("%s: thread slept failed after domain start ddos protection for %s.%n", wafDomainResponseBody.getDomainName(), e.getMessage());
        } catch (Exception e) {
            System.out.printf("%s: domain start ddos protection failed %s.%n", wafDomainResponseBody.getDomainName(), e.getMessage());
        }
        return 0;
    }

}
````
## 5.FAQ
### 5.1 Why do I select Chinese mainland or Outside the Chinese mainland but not the actual region where my services are located for Project and Region?
EdgeSec, SCM, and CDN are global services. However, when you purchase them, their resources need to be located either in or outside the Chinese mainland.

## 6. References
For more information, see API Explorer.

## 7. Change History
| Released On | Issue|   Description  |
|:-----------:| :------: | :----------: |
| 2023-11-23  |   1.0    | This issue is the first official release.|
