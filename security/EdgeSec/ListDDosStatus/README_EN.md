### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/Anti-DDoS/doc?api=ListDDosStatus).
2.This API is used to query the anti-DDoS status of all EIPs of a user, regardless of whether the EIP is bound to an ECS.

3.This API is used to querying the EIP Protection Status List.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The current account has the permission to use Anti-DDoS.

6.An instance has been created in Anti-DDoS.

7.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-antiddos. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-antiddos</artifactId>
    <version>3.1.125</version>
</dependency>
```

### Sample Code

``` java
package com.huawei.ddos;

import com.huaweicloud.sdk.antiddos.v1.AntiDDoSClient;
import com.huaweicloud.sdk.antiddos.v1.model.ListDDosStatusRequest;
import com.huaweicloud.sdk.antiddos.v1.model.ListDDosStatusResponse;
import com.huaweicloud.sdk.antiddos.v1.region.AntiDDoSRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ListDDosStatus {
    private static final Logger logger = LoggerFactory.getLogger(ListDDosStatus.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to AntiDDoSRegion.AP_SOUTHEAST_3.
        AntiDDoSClient client = AntiDDoSClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(AntiDDoSRegion.AP_SOUTHEAST_3)
                .build();

        // Build Request
        ListDDosStatusRequest request = new ListDDosStatusRequest();

        try {
            // Send a request
            ListDDosStatusResponse response = client.listDDosStatus(request);
            // Print the response result.
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}
```

### Example of the returned result
```
{
  "total": 1,
  "ddosStatus": [
    {
      "floating_ip_id": "18e6ace5-eb36***********************",
      "floating_ip_address": "139.9.************",
      "network_type": "EIP",
      "status": "normal",
      "blackhole_endtime": 0,
      "protect_type": "default",
      "traffic_threshold": 99,
      "http_threshold": 0
    }
  ]
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/09/26 | 1.0 | This document is released for the first time.