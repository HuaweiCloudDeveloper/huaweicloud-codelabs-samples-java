### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CFW/doc?api=ListFirewallList).
2.Querying the Firewall List

3.This API is used to querying the Firewall List.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The current account has the permission to use CFW.

6.An instance has been created in CFW.

7.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-cfw. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cfw</artifactId>
    <version>3.1.125</version>
</dependency>
```

### Sample Code

``` java
package com.huawei.cfw;

import com.huaweicloud.sdk.cfw.v1.CfwClient;
import com.huaweicloud.sdk.cfw.v1.model.ListFirewallListRequest;
import com.huaweicloud.sdk.cfw.v1.model.ListFirewallListResponse;
import com.huaweicloud.sdk.cfw.v1.model.QueryFireWallInstanceDto;
import com.huaweicloud.sdk.cfw.v1.region.CfwRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ListFirewallList {
    private static final Logger logger = LoggerFactory.getLogger(ListFirewallList.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CfwRegion.AP_SOUTHEAST_3.
        CfwClient client = CfwClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(CfwRegion.AP_SOUTHEAST_3)
                .build();

        // Build Request
        ListFirewallListRequest request = new ListFirewallListRequest();
        request.withEnterpriseProjectId("<PROJECT_ID>");
        QueryFireWallInstanceDto body = new QueryFireWallInstanceDto();
        body.withOffset(0);
        body.withLimit(10);
        request.withBody(body);

        try {
            // Send a request
            ListFirewallListResponse response = client.listFirewallList(request);
            // Print the response result.
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}
```

### Example of the returned result
```
{
  "data" : {
    "limit" : 1,
    "offset" : 0,
    "project_id" : "14181c1245cf4************************",
    "records" : [ {
      "fw_instance_id" : "ebf891cd-2163-48a************************",
      "resource_id" : "ebf891cd-2163-48a0************************",
      "name" : "1709176078374",
      "fw_instance_name" : "test",
      "enterprise_project_id" : "default",
      "tags" : "{\"key_test3\":\"value_test3\"}",
      "ha_type" : 0,
      "charge_mode" : 0,
      "service_type" : 0,
      "engine_type" : 1,
      "flavor" : {
        "version" : 1,
        "eip_count" : 50,
        "vpc_count" : 6,
        "bandwidth" : 50,
        "log_storage" : 0,
        "default_eip_count" : 50,
        "default_vpc_count" : 2,
        "default_bandwidth" : 50,
        "default_log_storage" : 0
      },
      "status" : 2
    } ],
    "total" : 18
  },
  "has_ndr" : false,
  "is_support_basic_version" : true,
  "is_support_buy_professional" : false,
  "is_support_postpaid" : true,
  "user_support_eps" : false
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/09/26 | 1.0 | This document is released for the first time.