## 0.版本说明
本示例基于华为云SDK V3.0版本开发。

## 1.简介
华为云提供了CFW服务端SDK，您可以直接集成服务端SDK来调用CFW的相关API，从而实现对CFW的快速操作。
该示例展示如何通过CFW服务对已防护的eip采用访问控制进行防护，并通过增删改查的方式操作访问控制策略，同时查询因此生成的访问控制日志

## 2.开发前准备
- 已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=94fc0f9f861b4f30a85ec1f463d35609&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问秘钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)
- 已获取对应区域的项目，请在华为云控制台“我的凭证 > API凭证 > 项目列表”页面上查看项目，例如：cn-north-4。具体请参见 [API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html) 。
- 需要在 [云防火墙](https://console.huaweicloud.com/console/?region=cn-north-4#/cfw/overview) 购买防火墙，并且在 [弹性云服务器](https://console.huaweicloud.com/ecm/?region=cn-north-4#/ecs/createVm) 购买弹性云服务器。
- 通过调用[API Explorer 查询防火墙实例](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=CFW&api=ListFirewallUsingGet) 获取防火墙id（FirewallInstanceId）、防护对象id（ObjectId），详见5.FAQ
- 通过弹性云服务器模拟访问控制流量。

## 3.安装sdk

您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-cfw”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com/?language=Java) 。

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cfw</artifactId>
    <version>3.1.62</version>
</dependency>
```
## 4.开始使用
### 4.1 导入依赖模块
```java
import com.huaweicloud.sdk.cfw.v1.CfwClient;
import com.huaweicloud.sdk.cfw.v1.model.AddRuleAclDto;
import com.huaweicloud.sdk.cfw.v1.model.AddRuleAclDtoRules;
import com.huaweicloud.sdk.cfw.v1.model.AddRuleAclUsingPostRequest;
import com.huaweicloud.sdk.cfw.v1.model.AddRuleAclUsingPostResponse;
import com.huaweicloud.sdk.cfw.v1.model.DeleteRuleAclUsingDeleteRequest;
import com.huaweicloud.sdk.cfw.v1.model.DeleteRuleAclUsingDeleteResponse;
import com.huaweicloud.sdk.cfw.v1.model.EipResource;
import com.huaweicloud.sdk.cfw.v1.model.ListAccessControlLogsRequest;
import com.huaweicloud.sdk.cfw.v1.model.ListAccessControlLogsResponse;
import com.huaweicloud.sdk.cfw.v1.model.ListEipResourcesRequest;
import com.huaweicloud.sdk.cfw.v1.model.ListEipResourcesResponse;
import com.huaweicloud.sdk.cfw.v1.model.ListRuleAclsUsingGetRequest;
import com.huaweicloud.sdk.cfw.v1.model.ListRuleAclsUsingGetResponse;
import com.huaweicloud.sdk.cfw.v1.model.OrderRuleAclDto;
import com.huaweicloud.sdk.cfw.v1.model.RuleAddressDto;
import com.huaweicloud.sdk.cfw.v1.model.RuleServiceDto;
import com.huaweicloud.sdk.cfw.v1.model.UpdateRuleAclDto;
import com.huaweicloud.sdk.cfw.v1.model.UpdateRuleAclUsingPutRequest;
import com.huaweicloud.sdk.cfw.v1.model.UpdateRuleAclUsingPutResponse;
import com.huaweicloud.sdk.cfw.v1.region.CfwRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.utils.JsonUtils;
import java.util.ArrayList;
import java.util.List;
```
### 4.2 初始化认证信息
```java
BasicCredentials auth = new BasicCredentials().withAk(ak).withSk(sk);
```

### 4.3 初始化防火墙客户端
```java
CfwClient client = CfwClient.newBuilder().withCredential(auth).withRegion(CfwRegion.valueOf("<REGION ID>")).build();
```
### 4.4 创建acl规则并使用
此节4.4.1-4.4.8示范了在console界面上如何操作，4.4.9示范了代码如何实现上述操作。
#### 4.4.1 通过查询防护eip列表查询到一条防护eip的地址
![acl-1](assets/acl-1.png)
#### 4.4.2 添加一条acl规则，方向为外到内、名称为ceshi、源地址为0.0.0.0/0、目的地址类型为ip地址、目的地址为防护eip、服务类型为服务、协议类型为TCP、源端口为0-65535、目的端口为0-65535、不支持长连接、动作为阻断、启用状态为打开
![acl-2](assets/acl-2.png)
#### 4.4.3 通过acl列表获取规则id
![acl-3](assets/acl-3.png)
#### 4.4.4 查询访问控制日志,获得阻断的访问控制日志
![acl-3](assets/acl-4.png)
#### 4.4.5 查询规则id访问次数,获得访问规则规则击中次数
![acl-3](assets/acl-5.png)
#### 4.4.6 设置规则为置顶
![acl-4](assets/acl-6.png)
#### 4.4.7 更新acl规则为一个非防护eip的值,其余不变
![acl-5](assets/acl-7.png)
#### 4.4.8 删除acl规则 
![acl-6](assets/acl-8.png)
#### 4.4.9 示例代码

```java
public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        BasicCredentials auth = new BasicCredentials().withAk(ak).withSk(sk);
        CfwClient client = CfwClient.newBuilder().withCredential(auth).withRegion(CfwRegion.valueOf("<REGION ID>")).build();
        try {
            /* 4.4.1 通过查询防护eip列表查询到一条防护eip的地址 */
            String publicEIp = queryEip(client);

            /* 4.4.2 添加一条acl规则，方向为外到内、名称为ceshi、源地址为0.0.0.0/0、目的地址类型为ip地址、目的地址为防护eip、服务类型为服务、协议类型为TCP、源端口为0-65535、目的端口为0-65535、不支持长连接、动作为阻断、启用状态为打开 */
            String id = addAcl(client,publicEIp);

            /* 4.4.3 通过acl列表获取规则id */
            queryRuleId(client);

            /* 4.4.4 查询访问控制日志,获得阻断的访问控制日志 */
            queryAccessLog(client,publicEIp);

            /* 4.4.5 查询acl规则的击中次数 */
            queryRuleHitCount(client,id);

            /* 4.4.6 将acl规则置顶 */
            orderRule(client,id);

            /* 4.4.7 更新acl规则为一个非防护eip的值,其余不变 */
            updateAcl(client,id);

            /* 4.4.8 删除acl规则 */
            deleteAcl(client,id);
        } catch (ConnectionException e) {
            System.out.println(e.getMessage());
        } catch (RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    private static void orderRule(CfwClient client, String ruleId) {
        UpdateAclRuleOrderRequest updateAclRuleOrderRequest = new UpdateAclRuleOrderRequest();
        OrderRuleAclDto orderRuleAclDto = new OrderRuleAclDto();
        orderRuleAclDto.setTop(1);
        updateAclRuleOrderRequest.setAclRuleId(ruleId);
        updateAclRuleOrderRequest.setBody(orderRuleAclDto);
        client.updateAclRuleOrder(updateAclRuleOrderRequest);
    }

    private static void queryRuleHitCount(CfwClient client, String ruleId) {
        ListAclRuleHitCountRequest listAclRuleHitCountRequest = new ListAclRuleHitCountRequest();
        ListRuleHitCountDto listRuleHitCountDto = new ListRuleHitCountDto();
        List<String> ruleIds = new ArrayList<>();
        ruleIds.add(ruleId);
        listRuleHitCountDto.setRuleIds(ruleIds);
        listAclRuleHitCountRequest.setBody(listRuleHitCountDto);
        ListAclRuleHitCountResponse listAclRuleHitCountResponse = client.listAclRuleHitCount(listAclRuleHitCountRequest);
        System.out.println(listAclRuleHitCountResponse.toString());
    }

    private static void deleteAcl(CfwClient client, String id) {
        DeleteAclRuleRequest deleteAclRuleRequest = new DeleteAclRuleRequest();
        deleteAclRuleRequest.setAclRuleId(id);
        DeleteAclRuleResponse deleteAclRuleResponse = client.deleteAclRule(deleteAclRuleRequest);
        System.out.println(deleteAclRuleResponse.toString());
    }

    private static void updateAcl(CfwClient client, String id) {
        UpdateAclRuleRequest updateAclRuleRequest = new UpdateAclRuleRequest();
        updateAclRuleRequest.setAclRuleId(id);
        UpdateRuleAclDto updateRuleAclDto = new UpdateRuleAclDto();
        updateRuleAclDto.setActionType(UpdateRuleAclDto.ActionTypeEnum.NUMBER_1);
        updateRuleAclDto.setAddressType(UpdateRuleAclDto.AddressTypeEnum.NUMBER_0);
        updateRuleAclDto.setDescription("");
        RuleAddressDto newDestination = new RuleAddressDto();
        newDestination.setAddress("1.1.1.1");
        newDestination.setType(0);
        updateRuleAclDto.setDestination(newDestination);
        updateRuleAclDto.setDirection(UpdateRuleAclDto.DirectionEnum.NUMBER_0);
        updateRuleAclDto.setLongConnectEnable(UpdateRuleAclDto.LongConnectEnableEnum.NUMBER_0);
        updateRuleAclDto.setName("ceshiAcl");
        RuleServiceDto ruleServiceDto = new RuleServiceDto();
        ruleServiceDto.setDestPort("0-65535");
        ruleServiceDto.setSourcePort("0-65535");
        ruleServiceDto.setProtocol(6);
        ruleServiceDto.setType(0);
        updateRuleAclDto.setService(ruleServiceDto);
        RuleAddressDto source = new RuleAddressDto();
        source.setAddress("0.0.0.0/0");
        source.setType(0);
        updateRuleAclDto.setSource(source);
        updateRuleAclDto.setStatus(1);
        updateRuleAclDto.setType(UpdateRuleAclDto.TypeEnum.NUMBER_0);
        updateAclRuleRequest.setBody(updateRuleAclDto);
        System.out.println(JsonUtils.toJSON(updateAclRuleRequest));
        UpdateAclRuleResponse updateAclRuleResponse = client.updateAclRule(updateAclRuleRequest);
        System.out.println(updateAclRuleResponse);
    }

    private static void queryAccessLog(CfwClient client, String publicEIp) {
        ListAccessControlLogsRequest listAccessControlLogsRequest = new ListAccessControlLogsRequest();
        listAccessControlLogsRequest.setDstIp(publicEIp);
        listAccessControlLogsRequest.setFwInstanceId("<YOUR FirewallInstanceId>");
        listAccessControlLogsRequest.setStartTime(1670427589817L);
        listAccessControlLogsRequest.setEndTime(1670431189817L);
        listAccessControlLogsRequest.setLimit(10);
        ListAccessControlLogsResponse listAccessControlLogsResponse = client.listAccessControlLogs(listAccessControlLogsRequest);
        System.out.println(listAccessControlLogsResponse.toString());
    }

    private static String queryRuleId(CfwClient client) {
        ListAclRulesRequest listAclRulesUsingGetRequest = new ListAclRulesRequest();
        listAclRulesUsingGetRequest.setObjectId("<YOUR ObjectId>");
        listAclRulesUsingGetRequest.setLimit(10);
        listAclRulesUsingGetRequest.setOffset(0);
        ListAclRulesResponse listAclRulesResponse = client.listAclRules(listAclRulesUsingGetRequest);
        String ruleId = listAclRulesResponse.getData().getRecords().get(0).getRuleId();
        System.out.println(ruleId);
        return ruleId;
    }

    private static String addAcl(CfwClient client, String publicEIp) {
        AddAclRuleRequest addAclRuleRequest = new AddAclRuleRequest();
        AddRuleAclDto addRuleAclDto = new AddRuleAclDto();
        addRuleAclDto.setObjectId("<YOUR ObjectId>");
        List<AddRuleAclDtoRules> addRuleAclDtoRulesList = new ArrayList<>();
        AddRuleAclDtoRules addRuleAclDtoRules = new AddRuleAclDtoRules();
        addRuleAclDtoRules.setActionType(1);
        addRuleAclDtoRules.setAddressType(AddRuleAclDtoRules.AddressTypeEnum.NUMBER_0);
        addRuleAclDtoRules.setDescription("");
        RuleAddressDto destination = new RuleAddressDto();
        destination.setAddress(publicEIp);
        destination.setType(0);
        addRuleAclDtoRules.setDestination(destination);
        addRuleAclDtoRules.setDirection(AddRuleAclDtoRules.DirectionEnum.NUMBER_0);
        addRuleAclDtoRules.setLongConnectEnable(AddRuleAclDtoRules.LongConnectEnableEnum.NUMBER_0);
        addRuleAclDtoRules.setName("ceshiAcl");
        OrderRuleAclDto orderRuleAclDto = new OrderRuleAclDto();
        orderRuleAclDto.setTop(1);
        addRuleAclDtoRules.setSequence(orderRuleAclDto);
        RuleServiceDto ruleServiceDto = new RuleServiceDto();
        ruleServiceDto.setDestPort("0-65535");
        ruleServiceDto.setSourcePort("0-65535");
        ruleServiceDto.setProtocol(6);
        ruleServiceDto.setType(0);
        addRuleAclDtoRules.setService(ruleServiceDto);
        RuleAddressDto source = new RuleAddressDto();
        source.setAddress("0.0.0.0/0");
        source.setType(0);
        addRuleAclDtoRules.setSource(source);
        addRuleAclDtoRules.setStatus(AddRuleAclDtoRules.StatusEnum.NUMBER_1);
        addRuleAclDtoRulesList.add(addRuleAclDtoRules);
        addRuleAclDto.setRules(addRuleAclDtoRulesList);
        addRuleAclDto.setType(AddRuleAclDto.TypeEnum.NUMBER_0);
        addAclRuleRequest.setBody(addRuleAclDto);
        AddAclRuleResponse addAclRuleResponse = client.addAclRule(addAclRuleRequest);
        String id = addAclRuleResponse.getData().getRules().get(0).getId();
        System.out.println(id);
        return id;
    }

    private static String queryEip(CfwClient client) {
        ListEipsRequest listEipsRequest = new ListEipsRequest();
        listEipsRequest.setObjectId("<YOUR ObjectId>");
        listEipsRequest.setLimit(10);
        listEipsRequest.setOffset(0);
        listEipsRequest.setSync(ListEipsRequest.SyncEnum.NUMBER_1);
        ListEipsResponse listEipsResponse = client.listEips(listEipsRequest);
        EipResource eipResource = listEipsResponse.getData().getRecords().get(0);
        String publicEIp = eipResource.getPublicIp();
        System.out.println(publicEIp);
        return publicEIp;
    }
```
## 5.FAQ
### 5.1 ObjectId是什么，如何获取
ObjectId是创建云防火墙后用于区分互联网边界防护和VPC边界防护的标志id，可通过调用[API Explorer 查询防火墙实例](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=CFW&api=ListFirewallUsingGet) 获取防护对象id（ObjectId），注意type为0的为互联网边界防护,type为1的为VPC边界防护。
![list-firewallinstance-2](assets/list-firewallinstance-2.png)
### 5.2 FirewallInstanceId是什么，如何获取
FirewallInstanceId是创建云防火墙后用于标志防火墙由系统自动生成的标志id，可通过调用[API Explorer 查询防火墙实例](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=CFW&api=ListFirewallUsingGet) 获取防火墙id（FirewallInstanceId）
![list-firewallinstance-1](assets/list-firewallinstance-1.png)
## 6.参考

更多信息请参考[API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=CFW&api=ListDnsServers)
 
## 7.修订记录

|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2022-12-01 |   1.0    | 文档首次发布 |
| 2023-07-19 |   1.1    | 添加击中次数及更新acl规则api调用 |
| 2023-10-07 |   1.2    | 更新sdk版本至3.1.62 |