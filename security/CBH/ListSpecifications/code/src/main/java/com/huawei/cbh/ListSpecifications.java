package com.huawei.cbh;

import com.huaweicloud.sdk.cbh.v2.model.ListSpecificationsRequest;
import com.huaweicloud.sdk.cbh.v2.model.ListSpecificationsResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.cbh.v2.region.CbhRegion;
import com.huaweicloud.sdk.cbh.v2.CbhClient;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListSpecifications {

    private static final Logger logger = LoggerFactory.getLogger(ListSpecifications.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listSpecificationsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listSpecificationsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listSpecificationsAk)
                .withSk(listSpecificationsSk);
        // Create a service client and set the corresponding region to CN_NORTH_4.
        CbhClient client = CbhClient.newBuilder()
                .withCredential(auth)
                .withRegion(CbhRegion.CN_NORTH_4)
                .build();
        // Construct a request and set request parameters to query the current action of CBH specifications.
        ListSpecificationsRequest request = new ListSpecificationsRequest();
        // This interface is used to query the current action of CBH specifications. create: queries the CBH specifications that can be created. update: queries the CBH specifications that can be modified.
        String action = "<YOUR ACTION>";
        request.withAction(action);
        try {
            ListSpecificationsResponse response = client.listSpecifications(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}