### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CBH/debug?api=ListSpecifications).

2.In this example, you can query CBH specifications.

3.Query the CBH specifications to learn the hardware configuration of the CBH server, such as the processor model, memory capacity, and storage space.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. 
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. 
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.You have enabled the CBH service and have the required permissions.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-cbh. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cbh</artifactId>
    <version>3.1.123</version>
</dependency>
```

### Code example
``` java
package com.huawei.cbh;

import com.huaweicloud.sdk.cbh.v2.model.ListSpecificationsRequest;
import com.huaweicloud.sdk.cbh.v2.model.ListSpecificationsResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.cbh.v2.region.CbhRegion;
import com.huaweicloud.sdk.cbh.v2.CbhClient;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListSpecifications {

    private static final Logger logger = LoggerFactory.getLogger(ListSpecifications.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listSpecificationsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listSpecificationsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listSpecificationsAk)
                .withSk(listSpecificationsSk);
        // Create a service client and set the corresponding region to CN_NORTH_4.
        CbhClient client = CbhClient.newBuilder()
                .withCredential(auth)
                .withRegion(CbhRegion.CN_NORTH_4)
                .build();
        // Construct a request and set request parameters to query the current action of CBH specifications.
        ListSpecificationsRequest request = new ListSpecificationsRequest();
        // This interface is used to query the current action of CBH specifications. create: queries the CBH specifications that can be created. update: queries the CBH specifications that can be modified.
        String action = "<YOUR ACTION>";
        request.withAction(action);
        try {
            ListSpecificationsResponse response = client.listSpecifications(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
```
[
 {
  "resource_spec_code": "cbh.50.kunpeng",
  "ecs_system_data_size": 100,
  "cpu": 4,
  "ram": 8,
  "asset": 50,
  "connection": 50,
  "type": "kunpeng",
  "data_disk_size": 0.5
 }
]
```
### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/12/06 | 1.0 | This document is released for the first time.