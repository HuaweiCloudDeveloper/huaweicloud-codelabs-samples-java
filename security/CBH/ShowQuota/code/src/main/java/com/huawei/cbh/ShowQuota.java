package com.huawei.cbh;

import com.huaweicloud.sdk.cbh.v2.model.ShowQuotaRequest;
import com.huaweicloud.sdk.cbh.v2.model.ShowQuotaResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.cbh.v2.region.CbhRegion;
import com.huaweicloud.sdk.cbh.v2.CbhClient;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowQuota {

    private static final Logger logger = LoggerFactory.getLogger(ShowQuota.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String showQuotaAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showQuotaSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(showQuotaAk)
                .withSk(showQuotaSk);

        // Create a service client and set the corresponding region to CbhRegion.CN_NORTH_4.
        CbhClient client = CbhClient.newBuilder()
                .withCredential(auth)
                .withRegion(CbhRegion.CN_NORTH_4)
                .build();
        // Build Request
        ShowQuotaRequest request = new ShowQuotaRequest();
        try {
            ShowQuotaResponse response = client.showQuota(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}