## 版本说明

本示例配套的SDK版本为：3.0.60

## 数据安全中心服务中图片嵌入水印相关示例

本示例展示如何通过java版本的DSC SDK方式将图片嵌入水印。

## 功能介绍

[数据安全中心服务 ](https://www.huaweicloud.com/product/dsc.html)是新一代的云原生数据安全平台，提供数据分级分类，数据安全风险识别，数据水印溯源，数据脱敏等基础数据安全能力，通过数据安全总览整合数据安全生命周期各阶段状态，对外整体呈现云上数据安全态势。

## 前置条件

- 1.已[ 注册 ](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register)华为云，并完成 [ 实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 3.您需要拥有华为云租户账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 5.已购买DSC服务专业版，购买链接[数据安全中心](https://www.huaweicloud.com/product/dsc.html)。

## 代码示例

以下代码展示如何使用DSC SDK对图片嵌入水印：

```java
package main.java.com.huawei.dsc;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.dsc.v1.DscClient;
import com.huaweicloud.sdk.dsc.v1.model.CreateImageWatermarkRequest;
import com.huaweicloud.sdk.dsc.v1.model.CreateImageWatermarkRequestBody;
import com.huaweicloud.sdk.dsc.v1.model.CreateImageWatermarkResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;

/**
 * 给图片添加暗水印
 * 返回嵌入暗水印的图片
 */
public class CreateImageWatermarkTest {

    /**
     * 基础认证信息：
     * - AK: 华为云账号Access Key
     * - SK: 华为云账号Secret Access Key
     * - IAM_ENDPOINT: 华为云IAM服务访问终端地址 详情见https://support.huaweicloud.com/api-iam/iam_01_0004.html,
     *                 eg: https://iam.myhuaweicloud.com
     * - DSC_ENDPOINT: 华为云DSC服务访问终端地址 详情见https://support.huaweicloud.com/api-dsc/dsc_02_0004.html,
     *                 eg: https://sdg.cn-south-1.myhuaweicloud.com
     * - REGION_ID: 华为云DSC服务访问RegionID, 详情见https://support.huaweicloud.com/api-dsc/dsc_02_0004.html，
     *                 eg: cn-south-1
     */
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String IAM_ENDPOINT = "<IamEndpoint>";
    private static final String DSC_ENDPOINT = "<DscEndpoint>";
    private static final String REGION_ID = "<RegionId>";

    // 1.设置需要加水印的图片路径（包括文件名），建议使用绝对路径
    private static final String INPUT_IMAGE_PATH = "<InputImagePath>";

    // 2.设置需要加水印的图片文件名
    private static final String INPUT_IMAGE_NAME = "<InputImageName>";

    // 3.设置返回的带水印的文件路径（包括文件名），建议使用绝对路径
    private static final String OUTPUT_IMAGE_PATH = "<OutputImageName>";

    private static final int BUFF_SIZE = 1024 * 1024;

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateImageWatermarkTest.class.getName());

    private static final ICredential AUTH = new BasicCredentials()
            .withIamEndpoint(IAM_ENDPOINT)
            .withAk(AK)
            .withSk(SK);

    private static final DscClient CLIENT = DscClient.newBuilder()
            .withCredential(AUTH)
            .withRegion(new Region(REGION_ID, DSC_ENDPOINT))
            .build();

    // 4.内部类，接收返回的文件流，转化为输出流后，写入返回的带暗水印的文件到磁盘
    static class WatermarkConsumer<T> implements Consumer<T> {
        public void accept(T t) {
            if (t instanceof InputStream) {
                try (FileOutputStream fos = new FileOutputStream(OUTPUT_IMAGE_PATH);
                     InputStream inputStreamTemp = (InputStream) t) {
                    int byteCount;
                    byte[] bytes = new byte[BUFF_SIZE];
                    while ((byteCount = inputStreamTemp.read(bytes)) != -1) {
                        fos.write(bytes,0, byteCount);
                    }
                } catch (IOException e) {
                    LOGGER.error(e.toString());
                }
            }
        }
    }

    public static void main(String[] args) {
        CreateImageWatermarkRequest request = new CreateImageWatermarkRequest();
        CreateImageWatermarkRequestBody body = new CreateImageWatermarkRequestBody();

        try (InputStream inputStream = new FileInputStream(INPUT_IMAGE_PATH)) {
            body.withFile(inputStream, INPUT_IMAGE_NAME);

            // 5.设置水印字符串
            body.withBlindWatermark("EnterWatermarkStringInYourMind");
            request.withBody(body);

            CreateImageWatermarkResponse response = CLIENT.createImageWatermark(request);
            Consumer<InputStream> consumer = new WatermarkConsumer<>();
            response.consumeDownloadStream(consumer);

            System.out.printf("response http code: %d", response.getHttpStatusCode());
        } catch (ConnectionException | RequestTimeoutException | IOException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getErrorCode());
            LOGGER.error(e.getErrorMsg());
        }
    }
}
```

您可以在[ API Explorer ](https://console.huaweicloud.com/apiexplorer/#/openapi/DSC/doc?api=BatchAddDataMask)调试该接口

## 修订记录

|   发布日期    | 文档版本 | 修订说明 |
|:---------:| :------: | :----------: |
| 2024-1-23 | 1.0 | 文档首次发布 |