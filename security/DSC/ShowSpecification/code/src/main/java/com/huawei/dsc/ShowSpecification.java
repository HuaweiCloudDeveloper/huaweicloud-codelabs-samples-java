package com.huawei.dsc;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.dsc.v1.DscClient;
import com.huaweicloud.sdk.dsc.v1.model.ShowSpecificationRequest;
import com.huaweicloud.sdk.dsc.v1.model.ShowSpecificationResponse;
import com.huaweicloud.sdk.dsc.v1.region.DscRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowSpecification {
    private static final Logger logger = LoggerFactory.getLogger(ShowSpecification.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential showSpecificationAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // Create a service client and set the corresponding region to CN_NORTH_4.
        DscClient client = DscClient.newBuilder()
            .withCredential(showSpecificationAuth)
            .withRegion(DscRegion.CN_NORTH_4)
            .build();
        // Construction request
        ShowSpecificationRequest request = new ShowSpecificationRequest();
        try {
            // Obtaining Results
            ShowSpecificationResponse response = client.showSpecification(request);
            // Print Results
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}