package com.huawei.secmaster;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.secmaster.v2.SecMasterClient;
import com.huaweicloud.sdk.secmaster.v2.model.DataobjectSearch;
import com.huaweicloud.sdk.secmaster.v2.model.DataobjectSearchCondition;
import com.huaweicloud.sdk.secmaster.v2.model.DataobjectSearchConditionConditions;
import com.huaweicloud.sdk.secmaster.v2.model.ListAlertsRequest;
import com.huaweicloud.sdk.secmaster.v2.model.ListAlertsResponse;
import com.huaweicloud.sdk.secmaster.v2.region.SecMasterRegion;

import java.util.List;
import java.util.ArrayList;

public class ListAlerts {

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");
        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId("<PROJECT ID>");
        // 创建服务客户端并配置对应Region
        SecMasterClient client = SecMasterClient.newBuilder()
            .withCredential(auth)
            .withRegion(SecMasterRegion.valueOf("<REGION ID>"))
            .build();
        ListAlertsRequest request = new ListAlertsRequest();
        // 创建告警等级和告警处理状态的过滤条件
        List<String> listConditionLogics = new ArrayList<>();
        listConditionLogics.add("severity");
        listConditionLogics.add("and");
        listConditionLogics.add("handle_status");
        List<String> listConditionsData = new ArrayList<>();
        listConditionsData.add("handle_status");
        listConditionsData.add("=");
        listConditionsData.add("Open");
        List<String> listConditionsData1 = new ArrayList<>();
        listConditionsData1.add("severity");
        listConditionsData1.add("=");
        listConditionsData1.add("Medium");
        List<DataobjectSearchConditionConditions> listConditionConditions = new ArrayList<>();
        listConditionConditions.add(new DataobjectSearchConditionConditions().withName("severity").withData(listConditionsData1));
        listConditionConditions.add(new DataobjectSearchConditionConditions().withName("handle_status").withData(listConditionsData));
        DataobjectSearchCondition conditionBody = new DataobjectSearchCondition();
        conditionBody.withConditions(listConditionConditions).withLogics(listConditionLogics);

        // 创建查询请求体，设置查询告警的时间范围、分页以及排序等
        DataobjectSearch body = new DataobjectSearch();
        body.withCondition(conditionBody);
        body.withToDate("2024-11-19T23:59:59.999Z+0800");
        body.withFromDate("2024-11-13T00:00:00.000Z+0800");
        body.withOrder(DataobjectSearch.OrderEnum.fromValue("DESC"));
        body.withSortBy("create_time");
        body.withOffset(0);
        body.withLimit(10);
        request.withBody(body);
        request.withWorkspaceId("<WORKSPACE ID>");
        try {
            ListAlertsResponse response = client.listAlerts(request);
            // 打印结果
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}