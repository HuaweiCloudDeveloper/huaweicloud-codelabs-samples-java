### 产品介绍

1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/SecMaster/doc?version=v2&api=ListAlerts)
中直接运行调试该接口。

2.在本样例中，您可以搜索告警列表。

3.我们可以搜索告警列表，来查看一些满足特定条件的告警。

### 前置条件

1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已购买SecMaster服务。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-secmaster”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-secmaster</artifactId>
    <version>3.1.122</version>
</dependency>
```

### 代码示例

``` java
package com.huawei.secmaster;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.secmaster.v2.SecMasterClient;
import com.huaweicloud.sdk.secmaster.v2.model.DataobjectSearch;
import com.huaweicloud.sdk.secmaster.v2.model.DataobjectSearchCondition;
import com.huaweicloud.sdk.secmaster.v2.model.DataobjectSearchConditionConditions;
import com.huaweicloud.sdk.secmaster.v2.model.ListAlertsRequest;
import com.huaweicloud.sdk.secmaster.v2.model.ListAlertsResponse;
import com.huaweicloud.sdk.secmaster.v2.region.SecMasterRegion;

import java.util.List;
import java.util.ArrayList;

public class ListAlerts {

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");
        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId("<PROJECT ID>");
        // 创建服务客户端并配置对应Region
        SecMasterClient client = SecMasterClient.newBuilder()
            .withCredential(auth)
            .withRegion(SecMasterRegion.valueOf("<REGION ID>"))
            .build();
        ListAlertsRequest request = new ListAlertsRequest();
        // 创建告警等级和告警处理状态的过滤条件
        List<String> listConditionLogics = new ArrayList<>();
        listConditionLogics.add("severity");
        listConditionLogics.add("and");
        listConditionLogics.add("handle_status");
        List<String> listConditionsData = new ArrayList<>();
        listConditionsData.add("handle_status");
        listConditionsData.add("=");
        listConditionsData.add("Open");
        List<String> listConditionsData1 = new ArrayList<>();
        listConditionsData1.add("severity");
        listConditionsData1.add("=");
        listConditionsData1.add("Medium");
        List<DataobjectSearchConditionConditions> listConditionConditions = new ArrayList<>();
        listConditionConditions.add(new DataobjectSearchConditionConditions().withName("severity").withData(listConditionsData1));
        listConditionConditions.add(new DataobjectSearchConditionConditions().withName("handle_status").withData(listConditionsData));
        DataobjectSearchCondition conditionBody = new DataobjectSearchCondition();
        conditionBody.withConditions(listConditionConditions).withLogics(listConditionLogics);

        // 创建查询请求体，设置查询告警的时间范围、分页以及排序等
        DataobjectSearch body = new DataobjectSearch();
        body.withCondition(conditionBody);
        body.withToDate("2024-11-19T23:59:59.999Z+0800");
        body.withFromDate("2024-11-13T00:00:00.000Z+0800");
        body.withOrder(DataobjectSearch.OrderEnum.fromValue("DESC"));
        body.withSortBy("create_time");
        body.withOffset(0);
        body.withLimit(10);
        request.withBody(body);
        request.withWorkspaceId("<WORKSPACE ID>");
        try {
            ListAlertsResponse response = client.listAlerts(request);
            // 打印结果
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}
```

### 返回结果示例

#### ListAlerts

```
{
	"code": "000000",
	"message": "message",
	"total": 1,
	"page": 0,
	"size": 10,
	"success": true,
	"data": [
		{
			"create_time": "2024-11-19T04:14:58.763Z+0800",
			"data_object": {
				"first_observed_time": "2024-11-19T04:14:50.000Z+0800",
				"is_auto_closed": null,
				"description": "主机:******执行High-Risk Command Execution",
				"origin_id": "c3d969aa-a5e9-11ef-addd-******",
				"title": "【High-Risk Command Execution】【HSS】主机:******执行High-Risk Command Execution,2024-11-19T04:14:50.000+08:00",
				"alert_type": {
					"id": "648679cfa46a1e9fc3******",
					"category": "系统行为异常",
					"alert_type": "高危命令执行"
				},
				"alert_list": [
					"424cdc6f-5f2d-4efd-9ba1-******",
					"372aef95-20ae-422a-bca6-******",
					"8c5cbcee-d889-4097-9fc0-******",
					"3e455771-4f15-412f-9431-******",
					"0c86b953-63fd-4bfc-bde6-******",
					"0ca3ca5c-b3f2-4ed7-9ee4-******"
				],
				"domain_id": "******",
				"workspace_id": "******",
				"verification_state": "Unknown",
				"domain_name": "******",
				"update_time": "2024-11-19T10:01:32.349Z+0800",
				"is_deleted": false,
				"indicator_list": [
					"47bdab5d-2fca-4ab2-931b-******"
				],
				"app_payload": {
					"vm_uuid": "f406bb43-34c0-4909-bb3d-******",
					"os_version": "2.5",
					"os_type": "Linux",
					"os_name": "EulerOS",
					"host_name": "ecs-vul-******",
					"vm_name": "ecs-vul-******"
				},
				"user_info": [],
				"project_id": "******",
				"file_info": [],
				"region_name": "cn-north-4",
				"id": "0ca3ca5c-b3f2-4ed7-9ee4-******",
				"handle_status": "Open",
				"network_list": [
					{
						"src_ip": null,
						"src_port": null,
						"src_geo": {
							"country_code": null,
							"latitude": null,
							"city_code": null,
							"longitude": null
						},
						"dest_geo": {
							"country_code": null,
							"latitude": null,
							"city_code": null,
							"longitude": null
						},
						"dest_ip": "******",
						"dest_port": null,
						"direction": null
					}
				],
				"severity": "Medium",
				"process": [
					{
						"process_child_cmdline": null,
						"process_child_name": null,
						"process_parent_uid": null,
						"process_child_path": null,
						"process_parent_path": "/usr/bin/bash",
						"process_parent_cmdline": "/bin/sh -c userlist=`cat /etc/passwd | grep -v \"^\\s*#\" | awk -F':' ' $7~\"(/bin/bash)\" {print $1}'`; for user in $userlist;do str=`su - $user -c \"umask\" 2>/dev/null`; out=$(echo $str | egrep \"[0-7][0-7][2367]7\");if [ -z \"$out\" ];then echo \"$user $str\";fi; done;unset userlist;unset str",
						"process_pid": 2670,
						"process_parent_pid": 2667,
						"process_name": null,
						"process_uid": null,
						"process_cmdline": "cat /etc/passwd",
						"process_child_pid": null,
						"process_path": "/usr/bin/cat",
						"process_parent_name": null,
						"process_launche_time": null,
						"process_child_uid": null
					}
				],
				"create_time": "2024-11-19T04:14:58.763Z+0800",
				"ttd": 0,
				"confidence": 79,
				"count": 1,
				"region_id": "cn-north-4",
				"raw": [
					{
						"data": "{\"vm_uuid\":\"f406bb43-34c0-4909-bb3d-******\",\"os_type\":\"Linux\",\"os_version\":\"2.5\",\"os_name\":\"EulerOS\",\"vm_name\":\"ecs-vul-******\",\"host_name\":\"ecs-vul-******\"}",
						"type": "hss"
					}
				],
				"sla": "2024-11-19T08:14:58.763Z+0800",
				"dataclass_id": "799acaf6-3229-360e-956b-******",
				"version": "3.2.16.B10",
				"data_source": {
					"domain_id": "******",
					"product_feature": "hids",
					"project_id": "******",
					"product_module": "hips_detect_module",
					"company_name": "Huawei",
					"region_id": "cn-north-4",
					"source_type": 1,
					"product_name": "hss"
				},
				"arrive_time": "2024-11-19T04:14:50.000Z+0800",
				"resource_list": [
					{
						"domain_id": "******",
						"provider": "ecs",
						"project_id": "******",
						"name": "ecs-vul-******",
						"region_id": "cn-north-4",
						"id": "f406bb43-34c0-4909-bb3d-******",
						"type": "cloudservers",
						"ep_id": "0"
					}
				],
				"environment": {
					"domain_id": "******",
					"domain_name": "******",
					"project_id": "******",
					"region_id": "cn-north-4",
					"region_name": "cn-north-4",
					"vendor_type": "HWC"
				},
				"trigger_flag": true
			},
			"dataclass_ref": {
				"id": "799acaf6-3229-360e-956b-******",
				"name": ""
			},
			"format_version": 0,
			"id": "0ca3ca5c-b3f2-4ed7-9ee4-******",
			"project_id": "******",
			"type": null,
			"update_time": "2024-11-19T10:01:32.349Z+0800",
			"version": 0,
			"workspace_id": "******"
		}
	]
}
```

### 修订记录
|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2024-11-20 |   1.0    | 文档首次发布 |