### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/HSS/doc?version=v5&api=ShowResourceQuotas).

2.In this example, you can query quota information.

3.Based on the returned result, you can learn about the quota statistics list in the current region, including the total quota, used quota, and available quota.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth).

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account.
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console.
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-hss. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-hss</artifactId>
    <version>3.1.125</version>
</dependency>
```

### Code example
The following code shows how to query quota information:
``` java
package com.huawei.hss;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.hss.v5.HssClient;
import com.huaweicloud.sdk.hss.v5.model.ShowResourceQuotasRequest;
import com.huaweicloud.sdk.hss.v5.model.ShowResourceQuotasResponse;
import com.huaweicloud.sdk.hss.v5.region.HssRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowResourceQuotas {
    private static final Logger logger = LoggerFactory.getLogger(ShowResourceQuotas.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential showResourceQuotasAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // Create a service client and set the corresponding region to CN_NORTH_4.
        HssClient client = HssClient.newBuilder()
            .withCredential(showResourceQuotasAuth)
            .withRegion(HssRegion.CN_NORTH_4)
            .build();
        // Construction request
        ShowResourceQuotasRequest request = new ShowResourceQuotasRequest();
        try {
            // Obtaining Results
            ShowResourceQuotasResponse response = client.showResourceQuotas(request);
            // Print Results
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
#### ShowResourceQuotas
```
{
  "data_list": [
    {
      "available_num": 1,
      "available_resources_list": [
        {
          "current_time": "2024-09-17T17:00:24Z",
          "resource_id": "9ecb83a7-xxxxxx-c3e90ca97eea",
          "shared_quota": "shared"
        }
      ],
      "total_num": 2,
      "used_num": 1,
      "version": "hss.version.basic"
    }
  ]
}
```
### Change History

 Released On  |  Issue   | Description
 :----: |:---:| :------:  
 2024/12/06 | 1.0 | This document is released for the first time.