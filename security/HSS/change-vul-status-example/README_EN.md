## 1. Introduction
Host Security Service (HSS) is a workload-centric security product that integrates host security, container security, and web tamper protection to meet the unique protection requirements of server workloads in hybrid cloud and multi-cloud data center infrastructures.
This section describes how to modify the vulnerability status in HSS. After querying the vulnerability list, you can mark the vulnerability status, such as Ignore, Cancel Ignore, and Repair.

## 2. Flow chart
(./assets/changeVulStatus.png)

## 3. Preparations

- You have [registered](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=94fc0f9f861b4f30a85ec1f463d35609&lang=en-us) with Huawei Cloud and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth).
- You have set up the development environment with Java JDK 1.8 or later.
- You have obtained the AK and SK of your Huawei Cloud account. You can create and view an AK/SK on the My Credentials > Access Keys page on the Huawei Cloud management console. For details, see [Obtaining an AK/SK](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).
- You have obtained the project ID of the corresponding region. To obtain it, log in to the Huawei Cloud management console, choose My Credentials > API Credentials > Projects, and locate the project. For resources in the Chinese mainland, use the ID for project cn-north-4. For resources outside the Chinese mainland, use the ID for project ap-southeast-1. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-proid.html).
- Purchasing an HSS Quota（https://support.huaweicloud.com/intl/en-us/usermanual-hss2.0/hss_01_0229.html）
- Installing the Agent on Servers(example: Installing the Agent on Huawei Cloud Servers)
- Enabling Protection(To enable protection, allocate a quota to a server or a container. After protection is disabled or the protected server or container is removed, the quota can be allocated to another server or container. https://support.huaweicloud.com/intl/en-us/usermanual-hss2.0/hss_01_0260.html)

## 4. Obtaining and Installing the SDK
(https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-hss</artifactId>
    <version>3.1.51</version>
</dependency>
```

## 5. Sample Code
```java
public class ChangeVulStatusDemo {
    private static final Logger logger = LoggerFactory.getLogger(ChangeVulStatusDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR AK>"
     * args[3] = "<YOUR SK>"
     * args[4] = "<REGION ID>"
     * args[5] = "<HANDLE STATUS, handleStatus include unhandled and handled>"
     * args[6] = "<Operate type, if the vulnerability status is unhandled, you can ignore (ignore), or if the vulnerability status is handled, you can cancel ignore (not_ignore)>"
     *
     * @param args
     */

    public static void main(String[] args) {
        String iamEndpoint = args[0];
        String endpoint = args[1];

        String ak = args[2];
        String sk = args[3];

        String regionId = args[4];
        // Step 1: Enter the vulnerability handling status and operation type.
        // handleStatus (disposal status) includes unhandled (unhandled) and handled (handled).
        String handleStatus = args[5];
        // If the vulnerability status is unhandled you can ignore (ignore), or if the vulnerability status is handled you can unignore (not_ignore)
        String operateType = args[6];
        ICredential auth = new BasicCredentials().withIamEndpoint(iamEndpoint).withAk(ak).withSk(sk);

        // Configure whether to skip SSL certificate verification as required.
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);
        HssClient client = HssClient.newBuilder()
            .withCredential(auth)
            .withRegion(new Region(regionId, endpoint))
            .withHttpConfig(httpConfig)
            .build();
        // Step 2: Query the vulnerability list.
        List<VulOperateInfo> vulOperateInfoList = queryVulOperateInfoList(client, handleStatus);
        // Step 3: Encapsulate the request body for modifying the vulnerability status.
        ChangeVulStatusRequest changeVulStatusRequest = getChangeVulStatusRequest(vulOperateInfoList, operateType);
        // Step 4: Modify the vulnerability status.
        changeVulStatus(client, changeVulStatusRequest);
    }

    /**
     * Encapsulate the request body for modifying the vulnerability status.
     *
     * @param vulOperateInfoList
     * @param operateType
     * @return
     */
    private static ChangeVulStatusRequest getChangeVulStatusRequest(List<VulOperateInfo> vulOperateInfoList,
        String operateType) {
        if (vulOperateInfoList == null || vulOperateInfoList.isEmpty()) {
            logger.info("vulOperateInfoList is empty");
            return null;
        }
        ChangeVulStatusRequestInfo body = new ChangeVulStatusRequestInfo();
        body.setOperateType(operateType);
        body.setDataList(vulOperateInfoList);
        ChangeVulStatusRequest changeVulStatusRequest = new ChangeVulStatusRequest();
        changeVulStatusRequest.setBody(body);
        return changeVulStatusRequest;
    }

    /**
     * Modifying the Vulnerability Status
     *
     * @param client
     * @param changeVulStatusRequest
     */
    private static void changeVulStatus(HssClient client, ChangeVulStatusRequest changeVulStatusRequest) {
        try {
            if (changeVulStatusRequest == null) {
                logger.info("changeVulStatusRequest is empty");
                return;
            }
            ChangeVulStatusResponse response = client.changeVulStatus(changeVulStatusRequest);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(),
                e.getErrorMsg());
        }
    }

    /**
     * Querying the Vulnerability List and Converting the Result Set
     *
     * @param client
     * @param handleStatus
     * @return
     */
    private static List<VulOperateInfo> queryVulOperateInfoList(HssClient client, String handleStatus) {
        try {
            ListVulnerabilitiesRequest request = new ListVulnerabilitiesRequest();
            request.setHandleStatus(handleStatus);
            ListVulnerabilitiesResponse listVulnerabilitiesResponse = client.listVulnerabilities(request);
            logger.info(listVulnerabilitiesResponse.toString());
            int httpStatusCode = listVulnerabilitiesResponse.getHttpStatusCode();
            if (httpStatusCode / 100 == 2) {
                List<VulInfo> dataList = listVulnerabilitiesResponse.getDataList();
                if (dataList != null && !dataList.isEmpty()) {
                    return dataList.stream().map(vulToVulOperateMapper).collect(Collectors.toList());
                }
            }
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(),
                e.getErrorMsg());
        }
        return new ArrayList<>();
    }

    /**
     * Converting Beans
     */
    private static Function<VulInfo, VulOperateInfo> vulToVulOperateMapper = vulInfo -> {
        VulOperateInfo vulOperateInfo = new VulOperateInfo();
        vulOperateInfo.setVulId(vulInfo.getVulId());
        vulOperateInfo.setHostIdList(vulInfo.getHostIdList());
        return vulOperateInfo;
    };
}
```


## 6.Example of the returned result
- Return value of the ListVulnerabilities API:
```
{
  "total_num": 1,
  "data_list": [
    {
      "description": "It was discovered that FreeType did not correctly handle certain malformed font files. If a user were tricked into using a specially crafted font file, a remote attacker could cause FreeType to crash, or possibly execute arbitrary code.",
      "host_id_list": [
        "caa958ad-a481-4d46-b51e-ccccccc"
      ],
      "host_num": 1,
      "scan_time": 1661752185836,
      "severity_level": "Critical",
      "repair_necessity": "Critical",
      "solution_detail": "To upgrade the affected software",
      "type": "linux_vul",
      "unhandle_host_num": 0,
      "url": "https://ubuntu.com/security/CVE-2022-27405",
      "vul_id": "USN-5528-1",
      "vul_name": "USN-5528-1: FreeType vulnerabilities"
    }
  ]
}
```

- The return value of the ChangeVulStatus interface is as follows:
```
{"error_code":"00000000"}
```

## 7. References
For more information, see API Explorer.
[API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/HSS/doc?api=ListVulnerabilities&version=v5) 
[API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/HSS/doc?api=ChangeVulStatus&version=v5) 

## 8. Change History
| Released On | Issue|   Description  |
|:-----------:| :------: | :----------: |
| 2023-08-01  |   1.0    | This issue is the first official release.|

