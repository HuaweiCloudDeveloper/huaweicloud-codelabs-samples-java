package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.hss.v5.HssClient;
import com.huaweicloud.sdk.hss.v5.model.ChangeVulStatusRequest;
import com.huaweicloud.sdk.hss.v5.model.ChangeVulStatusRequestInfo;
import com.huaweicloud.sdk.hss.v5.model.ChangeVulStatusResponse;
import com.huaweicloud.sdk.hss.v5.model.ListVulnerabilitiesRequest;
import com.huaweicloud.sdk.hss.v5.model.ListVulnerabilitiesResponse;
import com.huaweicloud.sdk.hss.v5.model.VulInfo;
import com.huaweicloud.sdk.hss.v5.model.VulOperateInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChangeVulStatusDemo {
    private static final Logger logger = LoggerFactory.getLogger(ChangeVulStatusDemo.class.getName());

    public static void main(String[] args) {
        String iamEndpoint = args[0];
        String endpoint = args[1];

        String ak = args[2];
        String sk = args[3];

        String regionId = args[4];

        String handleStatus = args[5];

        String operateType = args[6];
        ICredential auth = new BasicCredentials().withIamEndpoint(iamEndpoint).withAk(ak).withSk(sk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);
        HssClient client = HssClient.newBuilder()
            .withCredential(auth)
            .withRegion(new Region(regionId, endpoint))
            .withHttpConfig(httpConfig)
            .build();

        List<VulOperateInfo> vulOperateInfoList = queryVulOperateInfoList(client, handleStatus);

        ChangeVulStatusRequest changeVulStatusRequest = getChangeVulStatusRequest(vulOperateInfoList, operateType);

        changeVulStatus(client, changeVulStatusRequest);
    }

    private static ChangeVulStatusRequest getChangeVulStatusRequest(List<VulOperateInfo> vulOperateInfoList,
        String operateType) {
        if (vulOperateInfoList == null || vulOperateInfoList.isEmpty()) {
            logger.info("vulOperateInfoList is empty");
            return null;
        }
        ChangeVulStatusRequestInfo body = new ChangeVulStatusRequestInfo();
        body.setOperateType(operateType);
        body.setDataList(vulOperateInfoList);
        ChangeVulStatusRequest changeVulStatusRequest = new ChangeVulStatusRequest();
        changeVulStatusRequest.setBody(body);
        return changeVulStatusRequest;
    }

    private static void changeVulStatus(HssClient client, ChangeVulStatusRequest changeVulStatusRequest) {
        try {
            if (changeVulStatusRequest == null) {
                logger.info("changeVulStatusRequest is empty");
                return;
            }
            ChangeVulStatusResponse response = client.changeVulStatus(changeVulStatusRequest);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(),
                e.getErrorMsg());
        }
    }

    private static List<VulOperateInfo> queryVulOperateInfoList(HssClient client, String handleStatus) {
        try {
            ListVulnerabilitiesRequest request = new ListVulnerabilitiesRequest();
            request.setHandleStatus(handleStatus);
            ListVulnerabilitiesResponse listVulnerabilitiesResponse = client.listVulnerabilities(request);
            logger.info(listVulnerabilitiesResponse.toString());
            int httpStatusCode = listVulnerabilitiesResponse.getHttpStatusCode();
            if (httpStatusCode / 100 == 2) {
                List<VulInfo> dataList = listVulnerabilitiesResponse.getDataList();
                if (dataList != null && !dataList.isEmpty()) {
                    return dataList.stream().map(vulToVulOperateMapper).collect(Collectors.toList());
                }
            }
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(),
                e.getErrorMsg());
        }
        return new ArrayList<>();
    }

    private static Function<VulInfo, VulOperateInfo> vulToVulOperateMapper = vulInfo -> {
        VulOperateInfo vulOperateInfo = new VulOperateInfo();
        vulOperateInfo.setVulId(vulInfo.getVulId());
        vulOperateInfo.setHostIdList(vulInfo.getHostIdList());
        return vulOperateInfo;
    };
}
