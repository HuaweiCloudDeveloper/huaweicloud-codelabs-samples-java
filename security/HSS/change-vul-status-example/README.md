## 1. 介绍
主机安全服务（Host Security Service，HSS）是以工作负载为中心的安全产品，集成了主机安全、容器安全和网页防篡改，旨在解决混合云、多云数据中心基础架构中服务器工作负载的独特保护要求。
本示例介绍企业主机安全服务修改漏洞状态的场景，先查询漏洞列表，拿到了漏洞信息的基础上，可以对指定漏洞标记相应的状态，如“忽略”、“取消忽略”、“修复”等。

## 2. 流程图
![修改漏洞的状态下载链接流程图](./assets/changeVulStatus.png)

## 3. 前置条件

1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5、购买主机安全防护配额（https://support.huaweicloud.com/usermanual-hss2.0/hss_01_0229.html）

6、安装Agent （如安装Linux版本Agent https://support.huaweicloud.com/usermanual-hss2.0/hss_01_0234.html）

7、开启主机防护（如基础版/专业版/企业版/旗舰版 https://support.huaweicloud.com/usermanual-hss2.0/hss_01_0230.html）

并开启主机安全防护

## 4. SDK获取和安装
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-hss</artifactId>
    <version>3.1.51</version>
</dependency>
```

## 5. 关键代码片段
以下代码展示如何使用SDK修改漏洞的状态的代码片段：
```java
public class ChangeVulStatusDemo {
    private static final Logger logger = LoggerFactory.getLogger(ChangeVulStatusDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR AK>"
     * args[3] = "<YOUR SK>"
     * args[4] = "<REGION ID>"
     * args[5] = "<HANDLE STATUS, handleStatus(处置状态)包括两种状态unhandled(未处理)和handled(已处理)>"
     * args[6] = "<OPERATE TYPE, 如果漏洞状态是未处理(unhandled)你可以忽略(ignore), 或者如果漏洞状态是已处理(handled)你可以取消忽略(not_ignore)>"
     *
     * @param args
     */

    public static void main(String[] args) {
        String iamEndpoint = args[0];
        String endpoint = args[1];

        String ak = args[2];
        String sk = args[3];

        String regionId = args[4];
        // 第一步：输入漏洞处置状态、操作类型
        // handleStatus(处置状态)包括两种状态unhandled(未处理)和handled(已处理)
        String handleStatus = args[5];
        // 如果漏洞状态是未处理(unhandled)你可以忽略(ignore), 或者如果漏洞状态是已处理(handled)你可以取消忽略(not_ignore)
        String operateType = args[6];
        ICredential auth = new BasicCredentials().withIamEndpoint(iamEndpoint).withAk(ak).withSk(sk);

        // 根据需要配置是否跳过SSL证书验证
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);
        HssClient client = HssClient.newBuilder()
            .withCredential(auth)
            .withRegion(new Region(regionId, endpoint))
            .withHttpConfig(httpConfig)
            .build();
        // 第二步：查询漏洞列表
        List<VulOperateInfo> vulOperateInfoList = queryVulOperateInfoList(client, handleStatus);
        // 第三步：封装修改漏洞状态的请求体
        ChangeVulStatusRequest changeVulStatusRequest = getChangeVulStatusRequest(vulOperateInfoList, operateType);
        // 第四步：修改漏洞状态
        changeVulStatus(client, changeVulStatusRequest);
    }

    /**
     * 封装修改漏洞状态的请求体
     *
     * @param vulOperateInfoList
     * @param operateType
     * @return
     */
    private static ChangeVulStatusRequest getChangeVulStatusRequest(List<VulOperateInfo> vulOperateInfoList,
        String operateType) {
        if (vulOperateInfoList == null || vulOperateInfoList.isEmpty()) {
            logger.info("vulOperateInfoList is empty");
            return null;
        }
        ChangeVulStatusRequestInfo body = new ChangeVulStatusRequestInfo();
        body.setOperateType(operateType);
        body.setDataList(vulOperateInfoList);
        ChangeVulStatusRequest changeVulStatusRequest = new ChangeVulStatusRequest();
        changeVulStatusRequest.setBody(body);
        return changeVulStatusRequest;
    }

    /**
     * 修改漏洞状态
     *
     * @param client
     * @param changeVulStatusRequest
     */
    private static void changeVulStatus(HssClient client, ChangeVulStatusRequest changeVulStatusRequest) {
        try {
            if (changeVulStatusRequest == null) {
                logger.info("changeVulStatusRequest is empty");
                return;
            }
            ChangeVulStatusResponse response = client.changeVulStatus(changeVulStatusRequest);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(),
                e.getErrorMsg());
        }
    }

    /**
     * 查询漏洞列表，并转换结果集
     *
     * @param client
     * @param handleStatus
     * @return
     */
    private static List<VulOperateInfo> queryVulOperateInfoList(HssClient client, String handleStatus) {
        try {
            ListVulnerabilitiesRequest request = new ListVulnerabilitiesRequest();
            request.setHandleStatus(handleStatus);
            ListVulnerabilitiesResponse listVulnerabilitiesResponse = client.listVulnerabilities(request);
            logger.info(listVulnerabilitiesResponse.toString());
            int httpStatusCode = listVulnerabilitiesResponse.getHttpStatusCode();
            if (httpStatusCode / 100 == 2) {
                List<VulInfo> dataList = listVulnerabilitiesResponse.getDataList();
                if (dataList != null && !dataList.isEmpty()) {
                    return dataList.stream().map(vulToVulOperateMapper).collect(Collectors.toList());
                }
            }
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(),
                e.getErrorMsg());
        }
        return new ArrayList<>();
    }

    /**
     * 转换bean
     */
    private static Function<VulInfo, VulOperateInfo> vulToVulOperateMapper = vulInfo -> {
        VulOperateInfo vulOperateInfo = new VulOperateInfo();
        vulOperateInfo.setVulId(vulInfo.getVulId());
        vulOperateInfo.setHostIdList(vulInfo.getHostIdList());
        return vulOperateInfo;
    };
}
```


## 6.返回结果示例
- 查询漏洞列表（ListVulnerabilities）接口的返回值：
```
{
  "total_num": 1,
  "data_list": [
    {
      "description": "It was discovered that FreeType did not correctly handle certain malformed font files. If a user were tricked into using a specially crafted font file, a remote attacker could cause FreeType to crash, or possibly execute arbitrary code.",
      "host_id_list": [
        "caa958ad-a481-4d46-b51e-ccccccc"
      ],
      "host_num": 1,
      "scan_time": 1661752185836,
      "severity_level": "Critical",
      "repair_necessity": "Critical",
      "solution_detail": "To upgrade the affected software",
      "type": "linux_vul",
      "unhandle_host_num": 0,
      "url": "https://ubuntu.com/security/CVE-2022-27405",
      "vul_id": "USN-5528-1",
      "vul_name": "USN-5528-1: FreeType vulnerabilities"
    }
  ]
}
```

- 修改漏洞的状态（ChangeVulStatus）接口的返回值：
```
{"error_code":"00000000"}
```

## 7. 参考链接

请见 [查询漏洞列表API](https://support.huaweicloud.com/api-hss2.0/ListVulnerabilities.html)
您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/HSS/doc?api=ListVulnerabilities&version=v5) 中直接运行调试该接口。

请见 [修改漏洞的状态API](https://support.huaweicloud.com/api-hss2.0/ChangeVulStatus.html)
您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/HSS/doc?api=ChangeVulStatus&version=v5) 中直接运行调试该接口。

## 修订记录
|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2023-08-01 |   1.0    | 文档首次发布 |

