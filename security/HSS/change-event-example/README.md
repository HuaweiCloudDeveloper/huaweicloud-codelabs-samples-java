## 功能介绍
主机安全服务（Host Security Service，HSS）是以工作负载为中心的安全产品，集成了主机安全、容器安全和网页防篡改，旨在解决混合云、多云数据中心基础架构中服务器工作负载的独特保护要求。
本示例介绍企业主机安全服务处理告警事件的场景，首先查出告警事件列表，然后获取单个的告警事件详情，然后对指定的告警事件列表做处理，处理的方式如下：“手动处理”、“忽略”、“加入告警白名单”、“取消忽略”、“删除告警白名单”等

## 流程图
![修改漏洞的状态下载链接流程图](./assets/changeEvent.png)

## 前置条件
- 1、已注册华为云，并完成实名认证。
- 2、具备开发环境 ，支持go 1.14及以上版本。可执行 go version 检查当前 go 的版本信息。
- 3、已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见访问密钥。


## SDK获取和安装
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-hss</artifactId>
    <version>3.1.51</version>
</dependency>
```

## 关键代码片段
以下代码展示如何使用SDK修改漏洞的状态的代码片段：
```java
public class ChangeEventDemo {
    private static final Logger logger = LoggerFactory.getLogger(ChangeEventDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR AK>"
     * args[3] = "<YOUR SK>"
     * args[4] = "<Event Category, 事件分类主要有两种，包括host（主机）和container（容器）>"
     * args[5] = "<YOUR REGION_ID>"
     * args[6] = "<OPERATE TYPE, 操作类型包括mark_as_handled(手动处理), ignore(忽略), unhandle(取消手动处理)等>"
     * args[7] = "<REMARK, remark(备注), 非必填>"
     *
     * @param args
     */

    public static void main(String[] args) {
        if (args.length != 8) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String ak = args[2];
        String sk = args[3];

        // 第一步：输入事件分类、操作类型
        // 事件分类主要有两种，包括host（主机）和container（容器）
        String category = args[4];
        String regionId = args[5];

        // 操作类型包括mark_as_handled(手动处理), ignore(忽略), unhandle(取消手动处理)等
        String operateType = args[6];
        // remark(备注), 非必填
        String remark = args[7];

        ICredential auth = new BasicCredentials().withIamEndpoint(iamEndpoint).withAk(ak).withSk(sk);

        // 根据需要配置是否跳过SSL证书验证
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);
        HssClient client = HssClient.newBuilder()
            .withCredential(auth)
            .withRegion(new Region(regionId, endpoint))
            .withHttpConfig(httpConfig)
            .build();
        String handleStatus = "unhandled";
        // 第二步：查入侵事件列表
        List<EventManagementResponseInfo> eventManagementList = getEventManagementList(client, regionId, category,
            handleStatus);
        // 第三步：获取告警信息
        EventManagementResponseInfo securityEvent = getEventManagement(eventManagementList);
        if (securityEvent == null) {
            logger.info("securityEvent is empty");
            return;
        }
        // 封装请求体
        ChangeEventRequest request = new ChangeEventRequest();
        request.setRegion(regionId);
        ChangeEventRequestInfo body = new ChangeEventRequestInfo();
        body.setOperateType(operateType);
        if (!StringUtils.isEmpty(remark)) {
            body.setHandler(remark);
        }
        List<OperateEventRequestInfo> operateEventList = new ArrayList<>();
        OperateEventRequestInfo eventInfo = new OperateEventRequestInfo();
        eventInfo.setEventId(securityEvent.getEventId());
        eventInfo.setEventClassId(securityEvent.getEventClassId());
        eventInfo.setEventType(securityEvent.getEventType());
        eventInfo.setOccurTime(securityEvent.getOccurTime());
        eventInfo.setOperateDetailList(transEventDetailFunction.apply(securityEvent.getOperateDetailList()));
        operateEventList.add(eventInfo);
        body.setOperateEventList(operateEventList);
        request.setBody(body);
        // 第四步：处理告警事件
        changeEvent(client, request);
    }

    /**
     * 转换请求对象
     */
    private static Function<List<EventDetailResponseInfo>, List<EventDetailRequestInfo>> transEventDetailFunction
        = listEventDetailResponse -> {
        if (listEventDetailResponse == null || listEventDetailResponse.isEmpty()) {
            logger.info("listEventDetailResponse is empty");
            return new ArrayList<>();
        }
        return listEventDetailResponse.stream().map(eventDetailResponseInfo -> {
            EventDetailRequestInfo detail = new EventDetailRequestInfo();
            detail.setAgentId(eventDetailResponseInfo.getAgentId());
            detail.setHash(eventDetailResponseInfo.getHash());
            detail.setKeyword(eventDetailResponseInfo.getKeyword());
            return detail;
        }).collect(Collectors.toList());
    };

    /**
     * 查入侵事件列表
     *
     * @param client
     * @param
     * @return
     */
    private static List<EventManagementResponseInfo> getEventManagementList(HssClient client, String region,
        String category, String handleStatus) {
        ListSecurityEventsRequest request = new ListSecurityEventsRequest();
        request.setRegion(region);
        request.setCategory(category);
        request.setHandleStatus(handleStatus);
        ListSecurityEventsResponse listSecurityEventsResponse = client.listSecurityEvents(request);
        if (listSecurityEventsResponse != null && listSecurityEventsResponse.getDataList() != null
            && !listSecurityEventsResponse.getDataList().isEmpty()) {
            return listSecurityEventsResponse.getDataList();
        }
        return null;
    }

    /**
     * 获取告警信息
     *
     * @param dataList
     * @return
     */
    private static EventManagementResponseInfo getEventManagement(List<EventManagementResponseInfo> dataList) {
        if (dataList != null && dataList.size() > 0) {
            return dataList.get(0);
        }
        return null;
    }

    /**
     * 处理告警事件
     *
     * @param client
     * @param request
     */
    private static void changeEvent(HssClient client, ChangeEventRequest request) {
        try {
            if (request == null) {
                logger.info("ChangeEventRequest is empty");
                return;
            }
            ChangeEventResponse response = client.changeEvent(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(),
                e.getErrorMsg());
        }
    }
}
```

## 参考链接
更多信息请参考[API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/HSS/doc?api=ChangeEvent)

## 修订记录

|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2023-10-26 |   1.0    | 文档首次发布 |

