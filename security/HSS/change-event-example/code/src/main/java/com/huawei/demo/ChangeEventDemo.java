package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.StringUtils;
import com.huaweicloud.sdk.hss.v5.HssClient;
import com.huaweicloud.sdk.hss.v5.model.ChangeEventRequest;
import com.huaweicloud.sdk.hss.v5.model.ChangeEventRequestInfo;
import com.huaweicloud.sdk.hss.v5.model.ChangeEventResponse;
import com.huaweicloud.sdk.hss.v5.model.EventDetailRequestInfo;
import com.huaweicloud.sdk.hss.v5.model.EventDetailResponseInfo;
import com.huaweicloud.sdk.hss.v5.model.EventManagementResponseInfo;
import com.huaweicloud.sdk.hss.v5.model.ListSecurityEventsRequest;
import com.huaweicloud.sdk.hss.v5.model.ListSecurityEventsResponse;
import com.huaweicloud.sdk.hss.v5.model.OperateEventRequestInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChangeEventDemo {
    private static final Logger logger = LoggerFactory.getLogger(ChangeEventDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR AK>"
     * args[3] = "<YOUR SK>"
     * args[4] = "<Event Category, There are two types of events: host and container.>"
     * args[5] = "<YOUR REGION_ID>"
     * args[6] = "<OPERATE TYPE, The operation type can be mark_as_handled, ignore, or unhandle.>"
     * args[7] = "<REMARK, This parameter is optional.>"
     *
     * @param args
     */

    public static void main(String[] args) {
        if (args.length != 8) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String ak = args[2];
        String sk = args[3];

	    // Step 1: Enter the event category and operation type.
	    // There are two types of events: host and container.
        String category = args[4];
        String regionId = args[5];

        // The operation type can be mark_as_handled, ignore, or unhandle.
        String operateType = args[6];
        // remark(备注), 非必填
        String remark = args[7];

        ICredential auth = new BasicCredentials().withIamEndpoint(iamEndpoint).withAk(ak).withSk(sk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);
        HssClient client = HssClient.newBuilder()
            .withCredential(auth)
            .withRegion(new Region(regionId, endpoint))
            .withHttpConfig(httpConfig)
            .build();
        String handleStatus = "unhandled";
        // Step 2: Query the intrusion event list.
        List<EventManagementResponseInfo> eventManagementList = getEventManagementList(client, regionId, category,
            handleStatus);
        // Step 3: Obtain alarm information.
        EventManagementResponseInfo securityEvent = getEventManagement(eventManagementList);
        if (securityEvent == null) {
            logger.info("securityEvent is empty");
            return;
        }
        // Encapsulate the request body.
        ChangeEventRequest request = new ChangeEventRequest();
        request.setRegion(regionId);
        ChangeEventRequestInfo body = new ChangeEventRequestInfo();
        body.setOperateType(operateType);
        if (!StringUtils.isEmpty(remark)) {
            body.setHandler(remark);
        }
        List<OperateEventRequestInfo> operateEventList = new ArrayList<>();
        OperateEventRequestInfo eventInfo = new OperateEventRequestInfo();
        eventInfo.setEventId(securityEvent.getEventId());
        eventInfo.setEventClassId(securityEvent.getEventClassId());
        eventInfo.setEventType(securityEvent.getEventType());
        eventInfo.setOccurTime(securityEvent.getOccurTime());
        eventInfo.setOperateDetailList(transEventDetailFunction.apply(securityEvent.getOperateDetailList()));
        operateEventList.add(eventInfo);
        body.setOperateEventList(operateEventList);
        request.setBody(body);
        // Step 4: Handle alarms and events.
        changeEvent(client, request);
    }

    /**
     * Transformation Request Object
     */
    private static Function<List<EventDetailResponseInfo>, List<EventDetailRequestInfo>> transEventDetailFunction
        = listEventDetailResponse -> {
        if (listEventDetailResponse == null || listEventDetailResponse.isEmpty()) {
            logger.info("listEventDetailResponse is empty");
            return new ArrayList<>();
        }
        return listEventDetailResponse.stream().map(eventDetailResponseInfo -> {
            EventDetailRequestInfo detail = new EventDetailRequestInfo();
            detail.setAgentId(eventDetailResponseInfo.getAgentId());
            detail.setHash(eventDetailResponseInfo.getHash());
            detail.setKeyword(eventDetailResponseInfo.getKeyword());
            return detail;
        }).collect(Collectors.toList());
    };

    /**
     * Querying the Intrusion Event List
     *
     * @param client
     * @param
     * @return
     */
    private static List<EventManagementResponseInfo> getEventManagementList(HssClient client, String region,
        String category, String handleStatus) {
        ListSecurityEventsRequest request = new ListSecurityEventsRequest();
        request.setRegion(region);
        request.setCategory(category);
        request.setHandleStatus(handleStatus);
        ListSecurityEventsResponse listSecurityEventsResponse = client.listSecurityEvents(request);
        if (listSecurityEventsResponse != null && listSecurityEventsResponse.getDataList() != null
            && !listSecurityEventsResponse.getDataList().isEmpty()) {
            return listSecurityEventsResponse.getDataList();
        }
        return null;
    }

    /**
     * Obtaining Alarm Information
     *
     * @param dataList
     * @return
     */
    private static EventManagementResponseInfo getEventManagement(List<EventManagementResponseInfo> dataList) {
        if (dataList != null && dataList.size() > 0) {
            return dataList.get(0);
        }
        return null;
    }

    /**
     * Handling Alarms and Events
     *
     * @param client
     * @param request
     */
    private static void changeEvent(HssClient client, ChangeEventRequest request) {
        try {
            if (request == null) {
                logger.info("ChangeEventRequest is empty");
                return;
            }
            ChangeEventResponse response = client.changeEvent(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(),
                e.getErrorMsg());
        }
    }
}
