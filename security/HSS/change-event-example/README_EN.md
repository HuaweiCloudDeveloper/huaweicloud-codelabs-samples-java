## Introduction
Host Security Service (HSS) is a workload-centric security product that integrates host security, container security, and web tamper protection to meet the unique protection requirements of server workloads in hybrid cloud and multi-cloud data center infrastructures.
This example describes how to process alarms and events. First, query the alarm event list, obtain the details of a single alarm event, and then process the specified alarm event list. The handling method is as follows: Manual handling, Ignore, Add alarm trustlist, Cancel ignore, and Delete alarm trustlist.

## Flow chart
(./assets/changeEvent_en.png)

## Preparations

- You have [registered](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=94fc0f9f861b4f30a85ec1f463d35609&lang=en-us) with Huawei Cloud and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth).
- You have set up the development environment with Java JDK 1.8 or later.
- You have obtained the AK and SK of your Huawei Cloud account. You can create and view an AK/SK on the My Credentials > Access Keys page on the Huawei Cloud management console. For details, see [Obtaining an AK/SK](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

## Obtaining and Installing the SDK
(https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-hss</artifactId>
    <version>3.1.51</version>
</dependency>
```

## Sample Code
```java
public class ChangeEventDemo {
    private static final Logger logger = LoggerFactory.getLogger(ChangeEventDemo.class.getName());

    /**
     * args[0] = "<YOUR IAM_END_POINT>"
     * args[1] = "<YOUR END_POINT>"
     * args[2] = "<YOUR AK>"
     * args[3] = "<YOUR SK>"
     * args[4] = "<Event Category, 事件分类主要有两种，包括host（主机）和container（容器）>"
     * args[5] = "<YOUR REGION_ID>"
     * args[6] = "<OPERATE TYPE, 操作类型包括mark_as_handled(手动处理), ignore(忽略), unhandle(取消手动处理)等>"
     * args[7] = "<REMARK, remark(备注), 非必填>"
     *
     * @param args
     */

    public static void main(String[] args) {
        if (args.length != 8) {
            logger.info("Illegal Arguments");
        }

        String iamEndpoint = args[0];
        String endpoint = args[1];

        String ak = args[2];
        String sk = args[3];

        // 第一步：输入事件分类、操作类型
        // 事件分类主要有两种，包括host（主机）和container（容器）
        String category = args[4];
        String regionId = args[5];

        // 操作类型包括mark_as_handled(手动处理), ignore(忽略), unhandle(取消手动处理)等
        String operateType = args[6];
        // remark(备注), 非必填
        String remark = args[7];

        ICredential auth = new BasicCredentials().withIamEndpoint(iamEndpoint).withAk(ak).withSk(sk);

        // 根据需要配置是否跳过SSL证书验证
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);
        HssClient client = HssClient.newBuilder()
            .withCredential(auth)
            .withRegion(new Region(regionId, endpoint))
            .withHttpConfig(httpConfig)
            .build();
        String handleStatus = "unhandled";
        // 第二步：查入侵事件列表
        List<EventManagementResponseInfo> eventManagementList = getEventManagementList(client, regionId, category,
            handleStatus);
        // 第三步：获取告警信息
        EventManagementResponseInfo securityEvent = getEventManagement(eventManagementList);
        if (securityEvent == null) {
            logger.info("securityEvent is empty");
            return;
        }
        // 封装请求体
        ChangeEventRequest request = new ChangeEventRequest();
        request.setRegion(regionId);
        ChangeEventRequestInfo body = new ChangeEventRequestInfo();
        body.setOperateType(operateType);
        if (!StringUtils.isEmpty(remark)) {
            body.setHandler(remark);
        }
        List<OperateEventRequestInfo> operateEventList = new ArrayList<>();
        OperateEventRequestInfo eventInfo = new OperateEventRequestInfo();
        eventInfo.setEventId(securityEvent.getEventId());
        eventInfo.setEventClassId(securityEvent.getEventClassId());
        eventInfo.setEventType(securityEvent.getEventType());
        eventInfo.setOccurTime(securityEvent.getOccurTime());
        eventInfo.setOperateDetailList(transEventDetailFunction.apply(securityEvent.getOperateDetailList()));
        operateEventList.add(eventInfo);
        body.setOperateEventList(operateEventList);
        request.setBody(body);
        // 第四步：处理告警事件
        changeEvent(client, request);
    }

    /**
     * 转换请求对象
     */
    private static Function<List<EventDetailResponseInfo>, List<EventDetailRequestInfo>> transEventDetailFunction
        = listEventDetailResponse -> {
        if (listEventDetailResponse == null || listEventDetailResponse.isEmpty()) {
            logger.info("listEventDetailResponse is empty");
            return new ArrayList<>();
        }
        return listEventDetailResponse.stream().map(eventDetailResponseInfo -> {
            EventDetailRequestInfo detail = new EventDetailRequestInfo();
            detail.setAgentId(eventDetailResponseInfo.getAgentId());
            detail.setHash(eventDetailResponseInfo.getHash());
            detail.setKeyword(eventDetailResponseInfo.getKeyword());
            return detail;
        }).collect(Collectors.toList());
    };

    /**
     * 查入侵事件列表
     *
     * @param client
     * @param
     * @return
     */
    private static List<EventManagementResponseInfo> getEventManagementList(HssClient client, String region,
        String category, String handleStatus) {
        ListSecurityEventsRequest request = new ListSecurityEventsRequest();
        request.setRegion(region);
        request.setCategory(category);
        request.setHandleStatus(handleStatus);
        ListSecurityEventsResponse listSecurityEventsResponse = client.listSecurityEvents(request);
        if (listSecurityEventsResponse != null && listSecurityEventsResponse.getDataList() != null
            && !listSecurityEventsResponse.getDataList().isEmpty()) {
            return listSecurityEventsResponse.getDataList();
        }
        return null;
    }

    /**
     * 获取告警信息
     *
     * @param dataList
     * @return
     */
    private static EventManagementResponseInfo getEventManagement(List<EventManagementResponseInfo> dataList) {
        if (dataList != null && dataList.size() > 0) {
            return dataList.get(0);
        }
        return null;
    }

    /**
     * 处理告警事件
     *
     * @param client
     * @param request
     */
    private static void changeEvent(HssClient client, ChangeEventRequest request) {
        try {
            if (request == null) {
                logger.info("ChangeEventRequest is empty");
                return;
            }
            ChangeEventResponse response = client.changeEvent(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ConnectionException", e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException ", e);
        } catch (ServiceResponseException e) {
            logger.error("httpStatusCode: {}, errorCode: {}, errorMsg: {}", e.getHttpStatusCode(), e.getErrorCode(),
                e.getErrorMsg());
        }
    }
}
```

## References
[API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/HSS/doc?api=ChangeEvent&version=v5&locale=en-us)

## Change History
| Released On | Issue|   Description  |
|:-----------:| :------: | :----------: |
| 2023-08-01  |   1.0    | This issue is the first official release.|

