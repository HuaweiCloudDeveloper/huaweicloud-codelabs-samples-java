### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/WAF/doc?api=ListNoticeConfigs).

2.In this example, you can query the alarm notification configuration.

3.Based on the returned result, you can learn about the total number of configured alarm notifications and the detailed list of configured alarm notifications. including the alarm notification name, whether to enable the alarm notification function, topic, and notification type.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth).

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account.
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console.
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-waf. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-waf</artifactId>
    <version>3.1.125</version>
</dependency>
```

### Code example
The following code shows how to query alarm notification configurations:
``` java
package com.huawei.waf;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.waf.v1.WafClient;
import com.huaweicloud.sdk.waf.v1.model.ListNoticeConfigsRequest;
import com.huaweicloud.sdk.waf.v1.model.ListNoticeConfigsResponse;
import com.huaweicloud.sdk.waf.v1.region.WafRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListNoticeConfigs {
    private static final Logger logger = LoggerFactory.getLogger(ListNoticeConfigs.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential listNoticeConfigsAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // Create a service client and set the corresponding region to CN_NORTH_4.
        WafClient client = WafClient.newBuilder()
            .withCredential(listNoticeConfigsAuth)
            .withRegion(WafRegion.CN_NORTH_4)
            .build();
        // Construction request
        ListNoticeConfigsRequest request = new ListNoticeConfigsRequest();
        try {
            // Obtaining Results
            ListNoticeConfigsResponse response = client.listNoticeConfigs(request);
            // Print Results
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
#### ListNoticeConfigs
```
{
  "total" : 1,
  "items" : [ {
    "enabled" : true,
    "enterprise_project_id" : "0",
    "id" : "753231xxxxxx",
    "is_all_enterprise_project" : true,
    "locale" : "zh-cn",
    "name" : "test-demo33",
    "nearly_expired_time" : 60,
    "notice_class" : "cert_alert_notice",
    "prefer_html" : false,
    "sendfreq" : 10080,
    "threat" : [ ],
    "times" : 1,
    "topic_urn" : "urn:smn:cn-north-7:550500xxxxxx:ces_zyh_test",
    "update_time" : 1664347553944
  } ]
}
```
### Change History

 Released On  |  Issue   | Description
 :----: |:---:| :------:  
 2024/12/13 | 1.0 | This document is released for the first time.