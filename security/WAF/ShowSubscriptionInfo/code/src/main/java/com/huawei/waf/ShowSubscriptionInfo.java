package com.huawei.waf;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.waf.v1.WafClient;
import com.huaweicloud.sdk.waf.v1.model.ShowSubscriptionInfoRequest;
import com.huaweicloud.sdk.waf.v1.model.ShowSubscriptionInfoResponse;
import com.huaweicloud.sdk.waf.v1.region.WafRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowSubscriptionInfo {
    private static final Logger logger = LoggerFactory.getLogger(ShowSubscriptionInfo.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential showSubscriptionInfoAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // Create a service client and set the corresponding region to CN_NORTH_4.
        WafClient client = WafClient.newBuilder()
            .withCredential(showSubscriptionInfoAuth)
            .withRegion(WafRegion.CN_NORTH_4)
            .build();
        // Construction request
        ShowSubscriptionInfoRequest request = new ShowSubscriptionInfoRequest();
        try {
            // Obtaining Results
            ShowSubscriptionInfoResponse response = client.showSubscriptionInfo(request);
            // Print Results
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}