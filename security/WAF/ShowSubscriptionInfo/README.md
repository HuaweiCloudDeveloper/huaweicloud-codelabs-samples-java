### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/WAF/doc?api=ShowSubscriptionInfo) 中直接运行调试该接口。

2.在本样例中，您可以查询租户订购信息。

3.通过返回的结果，可以了解租户的订购信息，包括云模式版本、资源列表、是否为新用户、独享模式订购信息等等。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-waf”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-waf</artifactId>
    <version>3.1.125</version>
</dependency>
```

### 代码示例
以下代码展示如何查询租户订购信息：
``` java
package com.huawei.waf;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.waf.v1.WafClient;
import com.huaweicloud.sdk.waf.v1.model.ShowSubscriptionInfoRequest;
import com.huaweicloud.sdk.waf.v1.model.ShowSubscriptionInfoResponse;
import com.huaweicloud.sdk.waf.v1.region.WafRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowSubscriptionInfo {
    private static final Logger logger = LoggerFactory.getLogger(ShowSubscriptionInfo.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential showSubscriptionInfoAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // 创建服务客户端并配置对应region为CN_NORTH_4
        WafClient client = WafClient.newBuilder()
            .withCredential(showSubscriptionInfoAuth)
            .withRegion(WafRegion.CN_NORTH_4)
            .build();
        // 构建请求
        ShowSubscriptionInfoRequest request = new ShowSubscriptionInfoRequest();
        try {
            // 获取结果
            ShowSubscriptionInfoResponse response = client.showSubscriptionInfo(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例
#### ShowSubscriptionInfo
```
{
  "type" : 3,
  "resources" : [ {
    "resourceId" : "d2759axxxxxx",
    "cloudServiceType" : "hws.service.type.waf",
    "resourceType" : "hws.resource.type.waf",
    "resourceSpecCode" : "waf.enterprise",
    "resourceSize" : null,
    "expireTime" : "2024-12-07T15:59:59Z",
    "status" : 0
  }, {
    "resourceId" : "6a5a4bxxxxxx",
    "cloudServiceType" : "hws.service.type.waf",
    "resourceType" : "hws.resource.type.waf.rule",
    "resourceSpecCode" : "waf.expack.rule.enterprise",
    "resourceSize" : 5,
    "expireTime" : "2024-12-07T15:59:59Z",
    "status" : 0
  }, {
    "resourceId" : "a9202cxxxxxx",
    "cloudServiceType" : "hws.service.type.waf",
    "resourceType" : "hws.resource.type.waf.domain",
    "resourceSpecCode" : "waf.expack.domain.enterprise",
    "resourceSize" : 10,
    "expireTime" : "2024-12-07T15:59:59Z",
    "status" : 0
  } ],
  "isNewUser" : false,
  "premium" : {
    "purchased" : true,
    "total" : 8,
    "elb" : 0,
    "dedicated" : 8
  }
}
```
### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: |:----:| :------:  
 2024/12/13 | 1.0  | 文档首次发布