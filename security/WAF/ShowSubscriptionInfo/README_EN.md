### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/WAF/doc?api=ShowSubscriptionInfo).

2.In this example, you can query tenant subscription information.

3.Based on the returned result, you can learn about the tenant's subscription information, including the cloud mode version, resource list, whether the user is a new user, and subscription information in exclusive mode.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth).

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account.
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console.
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-waf. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-waf</artifactId>
    <version>3.1.125</version>
</dependency>
```

### Code example
The following code shows how to query tenant subscription information:
``` java
package com.huawei.waf;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.waf.v1.WafClient;
import com.huaweicloud.sdk.waf.v1.model.ShowSubscriptionInfoRequest;
import com.huaweicloud.sdk.waf.v1.model.ShowSubscriptionInfoResponse;
import com.huaweicloud.sdk.waf.v1.region.WafRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowSubscriptionInfo {
    private static final Logger logger = LoggerFactory.getLogger(ShowSubscriptionInfo.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential showSubscriptionInfoAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // Create a service client and set the corresponding region to CN_NORTH_4.
        WafClient client = WafClient.newBuilder()
            .withCredential(showSubscriptionInfoAuth)
            .withRegion(WafRegion.CN_NORTH_4)
            .build();
        // Construction request
        ShowSubscriptionInfoRequest request = new ShowSubscriptionInfoRequest();
        try {
            // Obtaining Results
            ShowSubscriptionInfoResponse response = client.showSubscriptionInfo(request);
            // Print Results
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
#### ShowSubscriptionInfo
```
{
  "type" : 3,
  "resources" : [ {
    "resourceId" : "d2759axxxxxx",
    "cloudServiceType" : "hws.service.type.waf",
    "resourceType" : "hws.resource.type.waf",
    "resourceSpecCode" : "waf.enterprise",
    "resourceSize" : null,
    "expireTime" : "2024-12-07T15:59:59Z",
    "status" : 0
  }, {
    "resourceId" : "6a5a4bxxxxxx",
    "cloudServiceType" : "hws.service.type.waf",
    "resourceType" : "hws.resource.type.waf.rule",
    "resourceSpecCode" : "waf.expack.rule.enterprise",
    "resourceSize" : 5,
    "expireTime" : "2024-12-07T15:59:59Z",
    "status" : 0
  }, {
    "resourceId" : "a9202cxxxxxx",
    "cloudServiceType" : "hws.service.type.waf",
    "resourceType" : "hws.resource.type.waf.domain",
    "resourceSpecCode" : "waf.expack.domain.enterprise",
    "resourceSize" : 10,
    "expireTime" : "2024-12-07T15:59:59Z",
    "status" : 0
  } ],
  "isNewUser" : false,
  "premium" : {
    "purchased" : true,
    "total" : 8,
    "elb" : 0,
    "dedicated" : 8
  }
}
```
### Change History

 Released On  |  Issue   | Description
 :----: |:---:| :------:  
 2024/12/13 | 1.0 | This document is released for the first time.