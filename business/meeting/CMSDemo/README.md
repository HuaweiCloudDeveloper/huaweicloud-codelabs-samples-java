# 华为云会议典型场景_Java
## 0. 版本说明
本示例基于华为云SDK V3.0版本开发。
## 1. 介绍

### 华为云会议介绍

华为云会议的服务端Java SDK(huaweicloud-sdk-meeting)对Meeting服务开放API进行了封装，用于与第三方的业务系统集成。


-   Meeting服务开放API请参考: [服务端API参考](https://support.huaweicloud.com/api-meeting/meeting_21_0202.html) 。


## 2.开发前准备
-   申请华为云账号
-   申请华为云会议测试资源
-   申请华为云会议App ID

具体操作步骤请参考：[开发前准备](https://support.huaweicloud.com/devg-meeting/meeting_20_0002.html) 。

##  3.问题求助通道

-   如果体验过程中遇到问题，可以在华为云会议开发者论坛求助：[云会议开发者论坛](https://bbs.huaweicloud.com/forum/forum.php?mod=forumdisplay&orderby=lastpost&fid=784&filter=typeid&typeid=2843) 。

##  4.安装SDK
开发者可以通过Maven方式获取和安装SDK，包括“huaweicloud-sdk-core”和“huaweicloud-sdk-meeting”。SDK版本号可以参考： [SDK中心](https://sdkcenter.developer.huaweicloud.com/?product=Meeting&language=java) 。

需要在Java项目的pom.xml文件中加入如下的依赖项。
``` xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>3.0.87</version></dependency><dependency>
</dependency>
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-meeting</artifactId>
    <version>3.0.87</version>
</dependency>
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-nop</artifactId>
    <version>1.7.2</version>
</dependency>

```

##  5. 使用SDK
###  华为云SDK基础知识
华为云Java SDK的基础知识请参考： [华为云Java SDK使用指导](https://github.com/huaweicloud/huaweicloud-sdk-java-v3/blob/master/README_CN.md) 。

**说明**：
-   “华为云Java SDK使用指导”中“认证信息配置”不适用于华为云会议。
-   客户端初始化华为云会议仅支持Endpoint方式。

###  Meeting请求方法名
-   华为云会议服务端Java SDK的请求方法名就是[API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/devsample?product=Meeting) 上每个API
    的名称，并以小驼峰风格命名。

## 6. 典型场景

### 接口调用概览


图1 华为云会议典型场景的接口调用时序图

![](./assets/1.png)

#### 场景1：初始化客户端
##### 描述
在调用服务端SDK其他接口前，需要先初始化客户端。每个客户端中包含App ID信息、帐号信息、Endpoint信息等。使用服务端SDK不需要开发者维护接入Token（即不需要开发者调用执行鉴权/执行AppID鉴权，刷新Token等接口），SDK会根据初始化客户端中的信息创建接入Token。

**说明**：
-   中国站的endpoint是api.meeting.huaweicloud.com；国际站的endpoint是api-intl.meeting.huaweicloud.com

##### 示例代码
```java
package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.MeetingCredentials;
import com.huaweicloud.sdk.meeting.v1.model.AuthTypeEnum;

public class MeetingClientDemo {
    private String demoAppId;
    private String demoAppKey;
    private MeetingClient managerClient;
    private MeetingClient userClient;
    private String demoEndPoint = "https://api.meeting.huaweicloud.com";

    MeetingClientDemo(String appId, String appKey) {
        demoAppId = appId;
        demoAppKey = appKey;
    }

    public MeetingClient getManagerClient() {
        return managerClient;
    }

    public MeetingClient getUserClient() {
        return userClient;
    }

    public void createCorpManagerClient() {
        // 第三方UserId不指定时默认是企业管理员
        ICredential auth = new MeetingCredentials()
                .withAuthType(AuthTypeEnum.APP_ID)
                .withAppId(demoAppId)
                .withAppKey(demoAppKey);

        managerClient = MeetingClient.newBuilder()
                .withCredential(auth)
                .withEndpoint(demoEndPoint)
                .build();
    }

    public void createCorpUserClient(String userId) {
        // 第三方UserId指定时不填时默认是普通用户，可以把用户设置成企业管理员
        ICredential auth = new MeetingCredentials()
                .withAuthType(AuthTypeEnum.APP_ID)
                .withAppId(demoAppId)
                .withAppKey(demoAppKey)
                .withUserId(userId);

        userClient = MeetingClient.newBuilder()
                .withCredential(auth)
                .withEndpoint(demoEndPoint)
                .build();
    }
}
```

#### 场景2：部门及用户管理
##### 描述
如果第三方应用需要使用华为云会议的企业通讯录，需要把部门组织和用户帐号同步到华为云会议系统中。

**说明**：
-   如果第三方应用不使用华为云会议的企业通讯录，并且使用App ID鉴权的场景，可以不用同步部门组织和帐号。
##### 示例代码
部门管理的示例代码如下：
```java
package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.model.SearchDepartmentByNameRequest;
import com.huaweicloud.sdk.meeting.v1.model.SearchDepartmentByNameResponse;
import com.huaweicloud.sdk.meeting.v1.model.QueryDeptResultDTO;
import com.huaweicloud.sdk.meeting.v1.model.DeptDTO;
import com.huaweicloud.sdk.meeting.v1.model.AddDepartmentRequest;
import com.huaweicloud.sdk.meeting.v1.model.AddDepartmentResponse;
import com.huaweicloud.sdk.meeting.v1.model.DeleteDepartmentRequest;
import com.huaweicloud.sdk.meeting.v1.model.DeleteDepartmentResponse;

import java.util.Iterator;

public class MeetingDeptDemo {

    public void listDepts(MeetingClient managerClient, String deptName) {
        System.out.println("Start listDepts...");

        SearchDepartmentByNameRequest request = new SearchDepartmentByNameRequest()
                .withDeptName(deptName);

        try {
            SearchDepartmentByNameResponse response = managerClient.searchDepartmentByName(request);
            Iterator<QueryDeptResultDTO> iter = response.getBody().iterator();
            while (iter.hasNext()) {
                QueryDeptResultDTO dept = (QueryDeptResultDTO) iter.next();
                System.out.println(dept.getDeptCode());
            }

        } catch (SdkException e) {
            Demo.processException(e);
        }
    }

    public String addDept(MeetingClient managerClient, String deptName) {

        System.out.println("Start addDept...");

        DeptDTO addDeptRequestBody = new DeptDTO()
                .withDeptName(deptName);
        AddDepartmentRequest request = new AddDepartmentRequest()
                .withBody(addDeptRequestBody);

        String deptCode = "";

        try {
            AddDepartmentResponse response = managerClient.addDepartment(request);
            deptCode = response.getValue();
            System.out.printf("Add department: %s success, deptCode is %s\r\n", deptName, deptCode);
        } catch (SdkException e) {
            Demo.processException(e);
        }

        return deptCode;

    }


    public void deleteDept(MeetingClient managerClient, String deptCode) {
        System.out.println("Start deleteDept...");

        DeleteDepartmentRequest request = new DeleteDepartmentRequest()
                .withDeptCode(deptCode);

        try {
            DeleteDepartmentResponse response = managerClient.deleteDepartment(request);
            System.out.printf("Delete department code: %s success\r\n", deptCode);
        } catch (SdkException e) {
            Demo.processException(e);
        }
    }
}

```

用户管理的示例代码如下：
```java
package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.model.SearchUsersRequest;
import com.huaweicloud.sdk.meeting.v1.model.SearchUsersResponse;
import com.huaweicloud.sdk.meeting.v1.model.SearchUserResultDTO;
import com.huaweicloud.sdk.meeting.v1.model.ShowUserDetailRequest;
import com.huaweicloud.sdk.meeting.v1.model.ShowUserDetailResponse;
import com.huaweicloud.sdk.meeting.v1.model.ShowMyInfoRequest;
import com.huaweicloud.sdk.meeting.v1.model.ShowMyInfoResponse;
import com.huaweicloud.sdk.meeting.v1.model.UserVmrDTO;
import com.huaweicloud.sdk.meeting.v1.model.AddUserDTO;
import com.huaweicloud.sdk.meeting.v1.model.AddUserRequest;
import com.huaweicloud.sdk.meeting.v1.model.AddUserResponse;
import com.huaweicloud.sdk.meeting.v1.model.BatchDeleteUsersRequest;
import com.huaweicloud.sdk.meeting.v1.model.BatchDeleteUsersResponse;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MeetingUserDemo {

    public void listUsers(MeetingClient managerClient) {
        System.out.println("Start listUsers...");

        SearchUsersRequest request = new SearchUsersRequest();
        try {
            SearchUsersResponse response = managerClient.searchUsers(request);
            Iterator<SearchUserResultDTO> iter = response.getData().iterator();
            while (iter.hasNext()) {
                SearchUserResultDTO user = (SearchUserResultDTO) iter.next();
                System.out.println("User name: " + user.getName() + "  Account id: " + user.getUserAccount());
            }

        } catch (SdkException e) {
            Demo.processException(e);
        }

    }

    public void showUsers(MeetingClient managerClient, String accountId) {
        System.out.println("Start showUsers...");

        ShowUserDetailRequest request = new ShowUserDetailRequest()
                .withAccount(accountId);

        try {
            ShowUserDetailResponse response = managerClient.showUserDetail(request);
            System.out.println("Account: " + response.getUserAccount());
            System.out.println("Third account: " + response.getThirdAccount());
            System.out.println("Name: " + response.getName());
            System.out.println("Department: " + response.getDeptName());
            System.out.println("phone:" + response.getPhone());
        } catch (SdkException e) {
            Demo.processException(e);
        }
    }

    public String showMyInfo(MeetingClient userClient) {
        System.out.println("Start showMyInfo...");

        ShowMyInfoRequest request = new ShowMyInfoRequest();
        String vmrUuid = "";
        try {
            ShowMyInfoResponse response = userClient.showMyInfo(request);
            System.out.println("Account: " + response.getUserAccount());
            System.out.println("Third account: " + response.getThirdAccount());
            System.out.println("Name: " + response.getName());
            System.out.println("Department: " + response.getDeptName());
            Iterator<UserVmrDTO> iter = response.getVmrList().iterator();
            while (iter.hasNext()) {
                UserVmrDTO vmr = (UserVmrDTO) iter.next();
                System.out.println("vmrUUID: " + vmr.getId() + "  vmrId: " + vmr.getVmrId() + "  vmrName: " + vmr.getVmrName());
                if (vmr.getVmrMode() == 0) {
                    vmrUuid = vmr.getId();
                }
            }

        } catch (SdkException e) {
            Demo.processException(e);
        }

        return vmrUuid;
    }

    public String addUsers(MeetingClient managerClient, String userId, String userName, String deptCode) {
        System.out.println("Start addUsers...");

        AddUserDTO user = new AddUserDTO()
                .withThirdAccount(userId)
                .withName(userName)
                .withDeptCode(deptCode);

        AddUserRequest request = new AddUserRequest()
                .withBody(user);
        String userAccount = "";
        try {
            AddUserResponse response = managerClient.addUser(request);
            System.out.printf("Add user: %s success, account is %s\r\n", userName, response.getUserAccount());
            userAccount = response.getUserAccount();

        } catch (SdkException e) {
            Demo.processException(e);
        }

        return userAccount;
    }

    public void deleteUser(MeetingClient managerClient, String accountId) {
        System.out.println("Start deleteUser...");

        List<String> users = new ArrayList<String>();
        users.add(accountId);

        BatchDeleteUsersRequest request = new BatchDeleteUsersRequest()
                .withBody(users);

        try {
            BatchDeleteUsersResponse response = managerClient.batchDeleteUsers(request);
            System.out.printf("Delete user account: %s success\r\n", accountId);
        } catch (SdkException e) {
            Demo.processException(e);
        }

    }


}

```

#### 场景3：分配云会议室资源
##### 描述
企业购买的云会议室需要分配给用户帐号，该用户才能在云会议室资源上创建会议。
##### 示例代码
```java
package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.model.SearchCorpVmrRequest;
import com.huaweicloud.sdk.meeting.v1.model.SearchCorpVmrResponse;
import com.huaweicloud.sdk.meeting.v1.model.AssociateVmrRequest;
import com.huaweicloud.sdk.meeting.v1.model.AssociateVmrResponse;

import java.util.ArrayList;
import java.util.List;

public class MeetingVmrManageDemo {
    public String listUnallocatedVmr(MeetingClient managerClient) {
        System.out.println("Start listUnallocatedVmr...");

        SearchCorpVmrRequest request = new SearchCorpVmrRequest()
                .withVmrMode(1)
                .withStatus(2);

        String vmrUuid = "";
        try {
            SearchCorpVmrResponse response = managerClient.searchCorpVmr(request);

            if (response.getCount() > 0) {
                vmrUuid = response.getData().get(0).getId();
                System.out.printf("UnAllocated Vmr ID is %s\r\n", vmrUuid);
            }

        } catch (SdkException e) {
            Demo.processException(e);
        }

        return vmrUuid;
    }

    public void allocateVmr(MeetingClient managerClient, String account, String vmrUuid) {
        System.out.println("Start allocateVmr...");

        List<String> vmrUuids = new ArrayList<String>();
        vmrUuids.add(vmrUuid);
        AssociateVmrRequest request = new AssociateVmrRequest()
                .withAccount(account)
                .withAccountType(1)
                .withBody(vmrUuids);

        try {
            AssociateVmrResponse response = managerClient.associateVmr(request);

        } catch (SdkException e) {
            Demo.processException(e);
        }

    }
}

```

#### 场景4：创建会议
##### 描述
华为云会议的资源有并发资源和云会议室两种，这两种会议资源上都可以创建随机会议ID会议及固定会议ID会议。
**说明**：
- 并发资源：
    - 随机会议ID会议：会议ID是创建会议接口返回的conferenceID。由系统随机生成。
    - 固定会议ID会议：即个人会议ID会议。会议ID是创建会议接口返回的vmrConferenceID。创建会议时需要将vmrFlag设置成1，并且设置vmrID。vmrID可以通过[“用户查询自己的信息”](https://support.huaweicloud.com/api-meeting/meeting_21_0073.html) 接口可获取（vmrMode=0），即接口返回的id。
- 云会议室资源：
    - 随机会议ID会议：会议ID是创建会议接口返回的conferenceID。由系统随机生成。
    - 固定会议ID会议：会议ID是创建会议接口返回的vmrConferenceID。创建会议时需要将vmrFlag设置成1，并且设置vmrID。vmrID可以通过[“用户查询自己的信息”](https://support.huaweicloud.com/api-meeting/meeting_21_0073.html) 接口可获取（vmrMode=1），即接口返回的id。

##### 示例代码
```java
package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.model.RestScheduleConfDTO;
import com.huaweicloud.sdk.meeting.v1.model.CreateMeetingRequest;
import com.huaweicloud.sdk.meeting.v1.model.CreateMeetingResponse;
import com.huaweicloud.sdk.meeting.v1.model.ConferenceInfo;
import com.huaweicloud.sdk.meeting.v1.model.CancelMeetingRequest;
import com.huaweicloud.sdk.meeting.v1.model.CancelMeetingResponse;
import com.huaweicloud.sdk.meeting.v1.model.ShowMeetingDetailRequest;
import com.huaweicloud.sdk.meeting.v1.model.ShowMeetingDetailResponse;
import com.huaweicloud.sdk.meeting.v1.model.StartRequest;
import com.huaweicloud.sdk.meeting.v1.model.StartMeetingRequest;
import com.huaweicloud.sdk.meeting.v1.model.StartMeetingResponse;

public class MeetingManageDemo {

    public String createMeeting(MeetingClient userClient, String meetingSubject, String startTime, Integer length, String vmrUuid) {
        System.out.println("Start createMeeting...");

        RestScheduleConfDTO createMeetingBody = new RestScheduleConfDTO()
                .withSubject(meetingSubject)
                .withStartTime(startTime)
                .withLength(length)
                .withMediaTypes("HDVideo");

        // 会议创建在云会议室或者个人会议室上
        if (!vmrUuid.equals("")) {
            createMeetingBody.setVmrFlag(1);
            createMeetingBody.setVmrID(vmrUuid);
        }

        CreateMeetingRequest request = new CreateMeetingRequest()
                .withBody(createMeetingBody);

        String conferenceId = "";
        try {
            CreateMeetingResponse response = userClient.createMeeting(request);
            ConferenceInfo meetingInfo = response.getBody().get(0);
            conferenceId = meetingInfo.getConferenceID();
            if (!vmrUuid.equals("")) {
                // VMR上的会议（云会议室或者个人会议室）
                System.out.println("Create Meeting on VMR resource. Conference ID is: " + conferenceId + " VMR Conference ID is: " + meetingInfo.getVmrConferenceID());
            } else {
                System.out.println("Create Meeting on concurrent resource. Conference ID is: " + conferenceId);
            }

        } catch (SdkException e) {
            Demo.processException(e);
        }

        return conferenceId;
    }

    public void deleteMeeting(MeetingClient userClient, String conferenceId) {
        System.out.println("Start DeleteMeeting...");

        CancelMeetingRequest request = new CancelMeetingRequest()
                .withConferenceID(conferenceId)
                .withType(1);   // 强制结束正在召开的会议

        try {
            CancelMeetingResponse response = userClient.cancelMeeting(request);
            System.out.printf("Delete Meeting: %s success \r\n", conferenceId);
        } catch (SdkException e) {
            Demo.processException(e);
        }
    }

    public String showMeeting(MeetingClient userClient, String conferenceId) {
        System.out.println("Start showMeeting...");

        ShowMeetingDetailRequest request = new ShowMeetingDetailRequest()
                .withConferenceID(conferenceId);
        String hostPassword = "";
        try {
            ShowMeetingDetailResponse response = userClient.showMeetingDetail(request);
            System.out.println("Meeting subject is: " + response.getConferenceData().getSubject());
            System.out.println("Meeting ID is: " + response.getConferenceData().getConferenceID());
            String role = response.getConferenceData()
                    .getPasswordEntry()
                    .get(0)
                    .getConferenceRole();
            if (role.equals("chair")) {
                hostPassword = response.getConferenceData()
                        .getPasswordEntry()
                        .get(0)
                        .getPassword();
                System.out.println("Meeting host password is: " + hostPassword);
                System.out.println("Meeting guest password is: " + response.getConferenceData()
                        .getPasswordEntry()
                        .get(1)
                        .getPassword());
            } else {
                hostPassword = response.getConferenceData()
                        .getPasswordEntry()
                        .get(0)
                        .getPassword();
                System.out.println("Meeting host password is: " + response.getConferenceData()
                        .getPasswordEntry()
                        .get(1)
                        .getPassword());
                System.out.println("Meeting guest password is: " + hostPassword);
            }

        } catch (SdkException e) {
            Demo.processException(e);
        }

        return hostPassword;
    }

    public void startMeeting(MeetingClient userClient, String conferenceId, String hostPassword) {
        System.out.println("Start startMeeting...");
        StartRequest startMeetingBody = new StartRequest()
                .withConferenceID(conferenceId)
                .withPassword(hostPassword);
        StartMeetingRequest request = new StartMeetingRequest()
                .withBody(startMeetingBody);

        try {
            StartMeetingResponse response = userClient.startMeeting(request);
            System.out.println("Start meeting success, conference id is: " + conferenceId);

        } catch (SdkException e) {
            Demo.processException(e);
        }
    }


}


```

#### 场景5：企业通讯录查询
##### 描述
邀请与会者等场景，需要获取用户的Sip号码等信息，可以通过企业通讯录查询接口获取。

##### 示例代码
```java
package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.model.SearchCorpDirRequest;
import com.huaweicloud.sdk.meeting.v1.model.SearchCorpDirResponse;

public class MeetingCorpDirDemo {
    public String SearchCorpDir(MeetingClient userClient, String searchKey) {
        System.out.println("Start SearchCorpDir...");

        String sipNumber = "";
        SearchCorpDirRequest request = new SearchCorpDirRequest()
                .withSearchKey(searchKey)
                .withLimit(10);
        try {
            SearchCorpDirResponse response = userClient.searchCorpDir(request);
            if (response.getCount() > 0) {
                sipNumber = response.getData().get(0).getNumber();
                System.out.println("phone: " + sipNumber);

            }
        } catch (SdkException e) {
            Demo.processException(e);
        }

        return sipNumber;
    }
}

```

#### 场景6：会议控制
##### 描述
会议开始后，可以调用会控接口。调用会控接口都需要先获取会控Token。
- 邀请与会者：可以通过Sip号码来邀请软终端、硬终端和手机/座机（需开通PSTN权限）。
- 设置多画面
##### 示例代码
```java
package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.model.CreateConfTokenRequest;
import com.huaweicloud.sdk.meeting.v1.model.CreateConfTokenResponse;
import com.huaweicloud.sdk.meeting.v1.model.Attendee;
import com.huaweicloud.sdk.meeting.v1.model.RestInviteReqBody;
import com.huaweicloud.sdk.meeting.v1.model.InviteParticipantRequest;
import com.huaweicloud.sdk.meeting.v1.model.RestSubscriberInPic;
import com.huaweicloud.sdk.meeting.v1.model.RestCustomMultiPictureBody;
import com.huaweicloud.sdk.meeting.v1.model.SetCustomMultiPictureRequest;

import java.util.ArrayList;
import java.util.List;

public class MeetingControlDemo {
    public String createConfToken(MeetingClient userClient, String conferenceId, String hostPassword) {
        System.out.println("Start createConfToken...");
        String confToken = "";

        CreateConfTokenRequest request = new CreateConfTokenRequest()
                .withConferenceID(conferenceId)
                .withXPassword(hostPassword)
                .withXLoginType(1);
        try {
            CreateConfTokenResponse response = userClient.createConfToken(request);
            confToken = response.getData().getToken();
            System.out.printf("The token of conference %s is %s \r\n", conferenceId, confToken);
        } catch (SdkException e) {
            Demo.processException(e);
        }

        return confToken;
    }

    public void inviteParticipants(MeetingClient userClient, String confToken, String conferenceId, String sipNumber, String name) {
        System.out.println("Start inviteParticipants...");

        Attendee participant = new Attendee()
                .withName(name)
                .withPhone(sipNumber)
                .withType("normal");
        List<Attendee> participants = new ArrayList<>();
        participants.add(participant);
        RestInviteReqBody body = new RestInviteReqBody()
                .withAttendees(participants);

        InviteParticipantRequest request = new InviteParticipantRequest()
                .withXConferenceAuthorization(confToken)
                .withConferenceID(conferenceId)
                .withBody(body);

        try {
            userClient.inviteParticipant(request);
        } catch (SdkException e) {
            Demo.processException(e);
        }
    }

    public void setMultiImageLayout(MeetingClient userClient, String confToken, String conferenceId, String sipNumber) {
        System.out.println("Start setMultiImageLayout...");

        List<String> sipNumbers = new ArrayList<>();
        sipNumbers.add(sipNumber);

        RestSubscriberInPic subscriberInPic = new RestSubscriberInPic()
                .withIndex(1)
                .withSubscriber(sipNumbers);

        List<RestSubscriberInPic> subscriberInPics = new ArrayList<>();
        subscriberInPics.add(subscriberInPic);

        RestCustomMultiPictureBody body = new RestCustomMultiPictureBody()
                .withManualSet(1)
                .withSubscriberInPics(subscriberInPics)
                .withImageType("Six")
                .withMultiPicSaveOnly(false);

        SetCustomMultiPictureRequest request = new SetCustomMultiPictureRequest()
                .withXConferenceAuthorization(confToken)
                .withConferenceID(conferenceId)
                .withBody(body);

        try {
            userClient.setCustomMultiPicture(request);
        } catch (SdkException e) {
            Demo.processException(e);
        }
    }
}


```

#### 华为云会议Java SDK Demo入口
##### 描述
调用上述各场景的示例代码，实现添加部门、添加用户、云会议室管理、创建会议、查询企业通讯录、会议控制等功能。

**说明**：
- 运行前需要先替换App ID和App Key
##### 示例代码
```java
package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Demo {
    public static void main(String[] args) {
        System.out.println("Start HUAWEI CLOUD Meeting Server API Java Demo...");

        // 创建管理员和普通用户的client
        String appId = "YOUR APP ID";
        String appKey = "YOUR App SECRET";
        MeetingClientDemo clientDemo = new MeetingClientDemo(appId, appKey);
        clientDemo.createCorpManagerClient();
        MeetingClient managerClient = clientDemo.getManagerClient();
        clientDemo.createCorpUserClient("dev_user01");
        MeetingClient userClient = clientDemo.getUserClient();

        // 部门管理
        MeetingDeptDemo deptDemo = new MeetingDeptDemo();
        String deptCode = deptDemo.addDept(managerClient, "开发部");
        deptDemo.listDepts(managerClient, "开发部");

        // 用户管理
        MeetingUserDemo userDemo = new MeetingUserDemo();
        userDemo.listUsers(managerClient);
        String userAccount = userDemo.addUsers(managerClient, "dev_user02", "用户2", deptCode);
        userDemo.showUsers(managerClient, userAccount);
        String vmrUuid = userDemo.showMyInfo(userClient);

        // 云会议室管理
        MeetingVmrManageDemo vmrManageDemo = new MeetingVmrManageDemo();
        String unallocatedVmeUuid = vmrManageDemo.listUnallocatedVmr(managerClient);
        vmrManageDemo.allocateVmr(managerClient, "dev_user02", unallocatedVmeUuid);

        // 会议管理
        MeetingManageDemo meetingManageDemo = new MeetingManageDemo();
        Date startDate = new Date((new Date()).getTime() + 1 * 3600 * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));  // 会议开始时间是UTC时间
        String startTimeStr = sdf.format(startDate);
        String conferenceId1 = meetingManageDemo.createMeeting(userClient, "Meeting Java SDK booked meeting",
                startTimeStr, 60, "");
        String hostPassword = meetingManageDemo.showMeeting(userClient, conferenceId1);
        String conferenceId2 = meetingManageDemo.createMeeting(userClient, "Meeting Java SDK booked vmr meeting",
                startTimeStr, 60, vmrUuid);

        // 企业通讯录查询
        MeetingCorpDirDemo meetingCorpDirDemo = new MeetingCorpDirDemo();
        String user2SipNumber = meetingCorpDirDemo.SearchCorpDir(userClient, "dev_user02");
        String user1SipNumber = meetingCorpDirDemo.SearchCorpDir(userClient, "dev_user01");

        // 会议控制
        meetingManageDemo.startMeeting(userClient, conferenceId1, hostPassword);  // 会议需要开始后才能进行会议控制
        MeetingControlDemo meetingControlDemo = new MeetingControlDemo();
        String conferenceToken = meetingControlDemo.createConfToken(userClient, conferenceId1, hostPassword);
        meetingControlDemo.inviteParticipants(userClient, conferenceToken, conferenceId1, user2SipNumber, "用户2");
        meetingControlDemo.setMultiImageLayout(userClient, conferenceToken, conferenceId1, user1SipNumber);

        // 清理测试数据
        meetingManageDemo.deleteMeeting(userClient, conferenceId1);
        meetingManageDemo.deleteMeeting(userClient, conferenceId2);
        userDemo.deleteUser(managerClient, userAccount);
        deptDemo.deleteDept(managerClient, deptCode);
        
        System.out.println("Exit HUAWEI CLOUD Meeting Server API Java Demo");
    }

    public static void processException(SdkException exp) {
        try {
            throw exp;
        } catch (ConnectionException e) {
            System.out.println("Connection error.");
        } catch (RequestTimeoutException e) {
            System.out.println("Connection timeout.");
        } catch (ServiceResponseException e) {
            System.out.println("HTTP Status Code: " + e.getHttpStatusCode());
            System.out.println("Error Code: " + e.getErrorCode());
            System.out.println("Error Message: " + e.getErrorMsg());
            System.out.println("Request ID: " + e.getRequestId());
        }
    }
}

```


#####
## 7.参考文件

-   华为云会议[《开发指南》](https://support.huaweicloud.com/devg-meeting/meeting_20_0001.html) 。
-   华为云会议[《API参考》](https://support.huaweicloud.com/api-meeting/meeting_21_0202.html) 。

## 8.修订记录
| 发布日期 | 文档版本 | 修订说明 |
| :--------: | :------: | :----------: |
| 2020-11-24 | 1.0 | 文档首次发布 |  
| 2022-07-31 | 2.0 | Java SDK Demo更新 | 
