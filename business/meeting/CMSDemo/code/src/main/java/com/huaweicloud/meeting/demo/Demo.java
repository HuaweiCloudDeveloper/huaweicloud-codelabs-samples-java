/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Demo {
    public static void main(String[] args) {
        System.out.println("Start HUAWEI CLOUD Meeting Server API Java Demo...");

        // Create administrator and users client
        String appId = "YOUR APP ID";
        String appKey = "YOUR APP SECRET";
        MeetingClientDemo clientDemo = new MeetingClientDemo(appId, appKey);
        clientDemo.createCorpManagerClient();
        MeetingClient managerClient = clientDemo.getManagerClient();
        clientDemo.createCorpUserClient("dev_user01");
        MeetingClient userClient = clientDemo.getUserClient();

        // Department management
        MeetingDeptDemo deptDemo = new MeetingDeptDemo();
        String deptCode = deptDemo.addDept(managerClient, "开发部");
        deptDemo.listDepts(managerClient, "开发部");

        // User management
        MeetingUserDemo userDemo = new MeetingUserDemo();
        userDemo.listUsers(managerClient);
        String userAccount = userDemo.addUsers(managerClient, "dev_user02", "用户2", deptCode);
        userDemo.showUsers(managerClient, userAccount);
        String vmrUuid = userDemo.showMyInfo(userClient);

        // Cloud meeting rooms management
        MeetingVmrManageDemo vmrManageDemo = new MeetingVmrManageDemo();
        String unallocatedVmeUuid = vmrManageDemo.listUnallocatedVmr(managerClient);
        vmrManageDemo.allocateVmr(managerClient, "dev_user02", unallocatedVmeUuid);

        // meeting management
        MeetingManageDemo meetingManageDemo = new MeetingManageDemo();
        Date startDate = new Date((new Date()).getTime() + 1 * 3600 * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));  // 会议开始时间是UTC时间
        String startTimeStr = sdf.format(startDate);
        String conferenceId1 = meetingManageDemo.createMeeting(userClient, "Meeting Java SDK booked meeting",
                startTimeStr, 60, "");
        String hostPassword = meetingManageDemo.showMeeting(userClient, conferenceId1);
        String conferenceId2 = meetingManageDemo.createMeeting(userClient, "Meeting Java SDK booked vmr meeting",
                startTimeStr, 60, vmrUuid);

        // Corporate address book query
        MeetingCorpDirDemo meetingCorpDirDemo = new MeetingCorpDirDemo();
        String user2SipNumber = meetingCorpDirDemo.SearchCorpDir(userClient, "dev_user02");
        String user1SipNumber = meetingCorpDirDemo.SearchCorpDir(userClient, "dev_user01");

        // Meeting control
        meetingManageDemo.startMeeting(userClient, conferenceId1, hostPassword);  // 会议需要开始后才能进行会议控制
        MeetingControlDemo meetingControlDemo = new MeetingControlDemo();
        String conferenceToken = meetingControlDemo.createConfToken(userClient, conferenceId1, hostPassword);
        meetingControlDemo.inviteParticipants(userClient, conferenceToken, conferenceId1, user2SipNumber, "用户2");
        meetingControlDemo.setMultiImageLayout(userClient, conferenceToken, conferenceId1, user1SipNumber);

        // clear test data
        meetingManageDemo.deleteMeeting(userClient, conferenceId1);
        meetingManageDemo.deleteMeeting(userClient, conferenceId2);
        userDemo.deleteUser(managerClient, userAccount);
        deptDemo.deleteDept(managerClient, deptCode);

        System.out.println("Exit HUAWEI CLOUD Meeting Server API Java Demo");
    }

    public static void processException(SdkException exp) {
        try {
            throw exp;
        } catch (ConnectionException e) {
            System.out.println("Connection error.");
        } catch (RequestTimeoutException e) {
            System.out.println("Connection timeout.");
        } catch (ServiceResponseException e) {
            System.out.println("HTTP Status Code: " + e.getHttpStatusCode());
            System.out.println("Error Code: " + e.getErrorCode());
            System.out.println("Error Message: " + e.getErrorMsg());
            System.out.println("Request ID: " + e.getRequestId());
        }
    }
}
