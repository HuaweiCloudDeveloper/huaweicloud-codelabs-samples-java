/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.model.RestScheduleConfDTO;
import com.huaweicloud.sdk.meeting.v1.model.CreateMeetingRequest;
import com.huaweicloud.sdk.meeting.v1.model.CreateMeetingResponse;
import com.huaweicloud.sdk.meeting.v1.model.ConferenceInfo;
import com.huaweicloud.sdk.meeting.v1.model.CancelMeetingRequest;
import com.huaweicloud.sdk.meeting.v1.model.CancelMeetingResponse;
import com.huaweicloud.sdk.meeting.v1.model.ShowMeetingDetailRequest;
import com.huaweicloud.sdk.meeting.v1.model.ShowMeetingDetailResponse;
import com.huaweicloud.sdk.meeting.v1.model.StartRequest;
import com.huaweicloud.sdk.meeting.v1.model.StartMeetingRequest;
import com.huaweicloud.sdk.meeting.v1.model.StartMeetingResponse;

public class MeetingManageDemo {

    public String createMeeting(MeetingClient userClient, String meetingSubject, String startTime, Integer length, String vmrUuid) {
        System.out.println("Start createMeeting...");

        RestScheduleConfDTO createMeetingBody = new RestScheduleConfDTO()
                .withSubject(meetingSubject)
                .withStartTime(startTime)
                .withLength(length)
                .withMediaTypes("HDVideo");

        // The meeting is created in a cloud meeting room or personal meeting room
        if (!vmrUuid.equals("")) {
            createMeetingBody.setVmrFlag(1);
            createMeetingBody.setVmrID(vmrUuid);
        }

        CreateMeetingRequest request = new CreateMeetingRequest()
                .withBody(createMeetingBody);

        String conferenceId = "";
        try {
            CreateMeetingResponse response = userClient.createMeeting(request);
            ConferenceInfo meetingInfo = response.getBody().get(0);
            conferenceId = meetingInfo.getConferenceID();
            if (!vmrUuid.equals("")) {
                // VMR meeting（Cloud meeting room or personal meeting room）
                System.out.println("Create Meeting on VMR resource. Conference ID is: " + conferenceId + " VMR Conference ID is: " + meetingInfo.getVmrConferenceID());
            } else {
                System.out.println("Create Meeting on concurrent resource. Conference ID is: " + conferenceId);
            }

        } catch (SdkException e) {
            Demo.processException(e);
        }

        return conferenceId;
    }

    public void deleteMeeting(MeetingClient userClient, String conferenceId) {
        System.out.println("Start DeleteMeeting...");

        CancelMeetingRequest request = new CancelMeetingRequest()
                .withConferenceID(conferenceId)
                .withType(1);   // Force the end of an ongoing meeting

        try {
            CancelMeetingResponse response = userClient.cancelMeeting(request);
            System.out.printf("Delete Meeting: %s success \r\n", conferenceId);
        } catch (SdkException e) {
            Demo.processException(e);
        }
    }

    public String showMeeting(MeetingClient userClient, String conferenceId) {
        System.out.println("Start showMeeting...");

        ShowMeetingDetailRequest request = new ShowMeetingDetailRequest()
                .withConferenceID(conferenceId);
        String hostPassword = "";
        try {
            ShowMeetingDetailResponse response = userClient.showMeetingDetail(request);
            System.out.println("Meeting subject is: " + response.getConferenceData().getSubject());
            System.out.println("Meeting ID is: " + response.getConferenceData().getConferenceID());
            String role = response.getConferenceData()
                    .getPasswordEntry()
                    .get(0)
                    .getConferenceRole();
            if (role.equals("chair")) {
                hostPassword = response.getConferenceData()
                        .getPasswordEntry()
                        .get(0)
                        .getPassword();
                System.out.println("Meeting host password is: " + hostPassword);
                System.out.println("Meeting guest password is: " + response.getConferenceData()
                        .getPasswordEntry()
                        .get(1)
                        .getPassword());
            } else {
                hostPassword = response.getConferenceData()
                        .getPasswordEntry()
                        .get(0)
                        .getPassword();
                System.out.println("Meeting host password is: " + response.getConferenceData()
                        .getPasswordEntry()
                        .get(1)
                        .getPassword());
                System.out.println("Meeting guest password is: " + hostPassword);
            }

        } catch (SdkException e) {
            Demo.processException(e);
        }

        return hostPassword;
    }

    public void startMeeting(MeetingClient userClient, String conferenceId, String hostPassword) {
        System.out.println("Start startMeeting...");
        StartRequest startMeetingBody = new StartRequest()
                .withConferenceID(conferenceId)
                .withPassword(hostPassword);
        StartMeetingRequest request = new StartMeetingRequest()
                .withBody(startMeetingBody);

        try {
            StartMeetingResponse response = userClient.startMeeting(request);
            System.out.println("Start meeting success, conference id is: " + conferenceId);

        } catch (SdkException e) {
            Demo.processException(e);
        }
    }


}
