/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.model.SearchDepartmentByNameRequest;
import com.huaweicloud.sdk.meeting.v1.model.SearchDepartmentByNameResponse;
import com.huaweicloud.sdk.meeting.v1.model.QueryDeptResultDTO;
import com.huaweicloud.sdk.meeting.v1.model.DeptDTO;
import com.huaweicloud.sdk.meeting.v1.model.AddDepartmentRequest;
import com.huaweicloud.sdk.meeting.v1.model.AddDepartmentResponse;
import com.huaweicloud.sdk.meeting.v1.model.DeleteDepartmentRequest;
import com.huaweicloud.sdk.meeting.v1.model.DeleteDepartmentResponse;

import java.util.Iterator;

public class MeetingDeptDemo {

    public void listDepts(MeetingClient managerClient, String deptName) {
        System.out.println("Start listDepts...");

        SearchDepartmentByNameRequest request = new SearchDepartmentByNameRequest()
                .withDeptName(deptName);

        try {
            SearchDepartmentByNameResponse response = managerClient.searchDepartmentByName(request);
            Iterator<QueryDeptResultDTO> iter = response.getBody().iterator();
            while (iter.hasNext()) {
                QueryDeptResultDTO dept = (QueryDeptResultDTO) iter.next();
                System.out.println(dept.getDeptCode());
            }

        } catch (SdkException e) {
            Demo.processException(e);
        }
    }

    public String addDept(MeetingClient managerClient, String deptName) {

        System.out.println("Start addDept...");

        DeptDTO addDeptRequestBody = new DeptDTO()
                .withDeptName(deptName);
        AddDepartmentRequest request = new AddDepartmentRequest()
                .withBody(addDeptRequestBody);

        String deptCode = "";

        try {
            AddDepartmentResponse response = managerClient.addDepartment(request);
            deptCode = response.getValue();
            System.out.printf("Add department: %s success, deptCode is %s\r\n", deptName, deptCode);
        } catch (SdkException e) {
            Demo.processException(e);
        }

        return deptCode;

    }


    public void deleteDept(MeetingClient managerClient, String deptCode) {
        System.out.println("Start deleteDept...");

        DeleteDepartmentRequest request = new DeleteDepartmentRequest()
                .withDeptCode(deptCode);

        try {
            DeleteDepartmentResponse response = managerClient.deleteDepartment(request);
            System.out.printf("Delete department code: %s success\r\n", deptCode);
        } catch (SdkException e) {
            Demo.processException(e);
        }
    }
}
