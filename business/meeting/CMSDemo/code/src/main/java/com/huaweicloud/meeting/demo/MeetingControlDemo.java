/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.model.CreateConfTokenRequest;
import com.huaweicloud.sdk.meeting.v1.model.CreateConfTokenResponse;
import com.huaweicloud.sdk.meeting.v1.model.Attendee;
import com.huaweicloud.sdk.meeting.v1.model.RestInviteReqBody;
import com.huaweicloud.sdk.meeting.v1.model.InviteParticipantRequest;
import com.huaweicloud.sdk.meeting.v1.model.RestSubscriberInPic;
import com.huaweicloud.sdk.meeting.v1.model.RestCustomMultiPictureBody;
import com.huaweicloud.sdk.meeting.v1.model.SetCustomMultiPictureRequest;

import java.util.ArrayList;
import java.util.List;

public class MeetingControlDemo {
    public String createConfToken(MeetingClient userClient, String conferenceId, String hostPassword) {
        System.out.println("Start createConfToken...");
        String confToken = "";

        CreateConfTokenRequest request = new CreateConfTokenRequest()
                .withConferenceID(conferenceId)
                .withXPassword(hostPassword)
                .withXLoginType(1);
        try {
            CreateConfTokenResponse response = userClient.createConfToken(request);
            confToken = response.getData().getToken();
            System.out.printf("The token of conference %s is %s \r\n", conferenceId, confToken);
        } catch (SdkException e) {
            Demo.processException(e);
        }

        return confToken;
    }

    public void inviteParticipants(MeetingClient userClient, String confToken, String conferenceId, String sipNumber, String name) {
        System.out.println("Start inviteParticipants...");

        Attendee participant = new Attendee()
                .withName(name)
                .withPhone(sipNumber)
                .withType("normal");
        List<Attendee> participants = new ArrayList<>();
        participants.add(participant);
        RestInviteReqBody body = new RestInviteReqBody()
                .withAttendees(participants);

        InviteParticipantRequest request = new InviteParticipantRequest()
                .withXConferenceAuthorization(confToken)
                .withConferenceID(conferenceId)
                .withBody(body);

        try {
            userClient.inviteParticipant(request);
        } catch (SdkException e) {
            Demo.processException(e);
        }
    }

    public void setMultiImageLayout(MeetingClient userClient, String confToken, String conferenceId, String sipNumber) {
        System.out.println("Start setMultiImageLayout...");

        List<String> sipNumbers = new ArrayList<>();
        sipNumbers.add(sipNumber);

        RestSubscriberInPic subscriberInPic = new RestSubscriberInPic()
                .withIndex(1)
                .withSubscriber(sipNumbers);

        List<RestSubscriberInPic> subscriberInPics = new ArrayList<>();
        subscriberInPics.add(subscriberInPic);

        RestCustomMultiPictureBody body = new RestCustomMultiPictureBody()
                .withManualSet(1)
                .withSubscriberInPics(subscriberInPics)
                .withImageType("Six")
                .withMultiPicSaveOnly(false);

        SetCustomMultiPictureRequest request = new SetCustomMultiPictureRequest()
                .withXConferenceAuthorization(confToken)
                .withConferenceID(conferenceId)
                .withBody(body);

        try {
            userClient.setCustomMultiPicture(request);
        } catch (SdkException e) {
            Demo.processException(e);
        }
    }
}
