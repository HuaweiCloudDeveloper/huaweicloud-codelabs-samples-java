/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.MeetingCredentials;
import com.huaweicloud.sdk.meeting.v1.model.AuthTypeEnum;

public class MeetingClientDemo {
    private String demoAppId;
    private String demoAppKey;
    private MeetingClient managerClient;
    private MeetingClient userClient;
    private String demoEndPoint = "https://api.meeting.huaweicloud.com";

    MeetingClientDemo(String appId, String appKey) {
        demoAppId = appId;
        demoAppKey = appKey;
    }

    public MeetingClient getManagerClient() {
        return managerClient;
    }

    public MeetingClient getUserClient() {
        return userClient;
    }

    public void createCorpManagerClient() {
        // Third UserId default is the enterprise administrator.
        ICredential auth = new MeetingCredentials()
                .withAuthType(AuthTypeEnum.APP_ID)
                .withAppId(demoAppId)
                .withAppKey(demoAppKey);

        managerClient = MeetingClient.newBuilder()
                .withCredential(auth)
                .withEndpoint(demoEndPoint)
                .build();
    }

    public void createCorpUserClient(String userId) {
        // If the third-party UserId is not filled in when specified, it defaults to an ordinary user. The user can be set to an enterprise administrator.
        ICredential auth = new MeetingCredentials()
                .withAuthType(AuthTypeEnum.APP_ID)
                .withAppId(demoAppId)
                .withAppKey(demoAppKey)
                .withUserId(userId);

        userClient = MeetingClient.newBuilder()
                .withCredential(auth)
                .withEndpoint(demoEndPoint)
                .build();
    }
}
