/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.model.SearchCorpVmrRequest;
import com.huaweicloud.sdk.meeting.v1.model.SearchCorpVmrResponse;
import com.huaweicloud.sdk.meeting.v1.model.AssociateVmrRequest;
import com.huaweicloud.sdk.meeting.v1.model.AssociateVmrResponse;

import java.util.ArrayList;
import java.util.List;

public class MeetingVmrManageDemo {
    public String listUnallocatedVmr(MeetingClient managerClient) {
        System.out.println("Start listUnallocatedVmr...");

        SearchCorpVmrRequest request = new SearchCorpVmrRequest()
                .withVmrMode(1)
                .withStatus(2);

        String vmrUuid = "";
        try {
            SearchCorpVmrResponse response = managerClient.searchCorpVmr(request);

            if (response.getCount() > 0) {
                vmrUuid = response.getData().get(0).getId();
                System.out.printf("UnAllocated Vmr ID is %s\r\n", vmrUuid);
            }

        } catch (SdkException e) {
            Demo.processException(e);
        }

        return vmrUuid;
    }

    public void allocateVmr(MeetingClient managerClient, String account, String vmrUuid) {
        System.out.println("Start allocateVmr...");

        List<String> vmrUuids = new ArrayList<String>();
        vmrUuids.add(vmrUuid);
        AssociateVmrRequest request = new AssociateVmrRequest()
                .withAccount(account)
                .withAccountType(1)
                .withBody(vmrUuids);

        try {
            AssociateVmrResponse response = managerClient.associateVmr(request);

        } catch (SdkException e) {
            Demo.processException(e);
        }

    }
}
