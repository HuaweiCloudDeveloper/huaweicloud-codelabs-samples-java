/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.model.SearchUsersRequest;
import com.huaweicloud.sdk.meeting.v1.model.SearchUsersResponse;
import com.huaweicloud.sdk.meeting.v1.model.SearchUserResultDTO;
import com.huaweicloud.sdk.meeting.v1.model.ShowUserDetailRequest;
import com.huaweicloud.sdk.meeting.v1.model.ShowUserDetailResponse;
import com.huaweicloud.sdk.meeting.v1.model.ShowMyInfoRequest;
import com.huaweicloud.sdk.meeting.v1.model.ShowMyInfoResponse;
import com.huaweicloud.sdk.meeting.v1.model.UserVmrDTO;
import com.huaweicloud.sdk.meeting.v1.model.AddUserDTO;
import com.huaweicloud.sdk.meeting.v1.model.AddUserRequest;
import com.huaweicloud.sdk.meeting.v1.model.AddUserResponse;
import com.huaweicloud.sdk.meeting.v1.model.BatchDeleteUsersRequest;
import com.huaweicloud.sdk.meeting.v1.model.BatchDeleteUsersResponse;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MeetingUserDemo {

    public void listUsers(MeetingClient managerClient) {
        System.out.println("Start listUsers...");

        SearchUsersRequest request = new SearchUsersRequest();
        try {
            SearchUsersResponse response = managerClient.searchUsers(request);
            Iterator<SearchUserResultDTO> iter = response.getData().iterator();
            while (iter.hasNext()) {
                SearchUserResultDTO user = (SearchUserResultDTO) iter.next();
                System.out.println("User name: " + user.getName() + "  Account id: " + user.getUserAccount());
            }

        } catch (SdkException e) {
            Demo.processException(e);
        }

    }

    public void showUsers(MeetingClient managerClient, String accountId) {
        System.out.println("Start showUsers...");

        ShowUserDetailRequest request = new ShowUserDetailRequest()
                .withAccount(accountId);

        try {
            ShowUserDetailResponse response = managerClient.showUserDetail(request);
            System.out.println("Account: " + response.getUserAccount());
            System.out.println("Third account: " + response.getThirdAccount());
            System.out.println("Name: " + response.getName());
            System.out.println("Department: " + response.getDeptName());
            System.out.println("phone:" + response.getPhone());
        } catch (SdkException e) {
            Demo.processException(e);
        }
    }

    public String showMyInfo(MeetingClient userClient) {
        System.out.println("Start showMyInfo...");

        ShowMyInfoRequest request = new ShowMyInfoRequest();
        String vmrUuid = "";
        try {
            ShowMyInfoResponse response = userClient.showMyInfo(request);
            System.out.println("Account: " + response.getUserAccount());
            System.out.println("Third account: " + response.getThirdAccount());
            System.out.println("Name: " + response.getName());
            System.out.println("Department: " + response.getDeptName());
            Iterator<UserVmrDTO> iter = response.getVmrList().iterator();
            while (iter.hasNext()) {
                UserVmrDTO vmr = (UserVmrDTO) iter.next();
                System.out.println("vmrUUID: " + vmr.getId() + "  vmrId: " + vmr.getVmrId() + "  vmrName: " + vmr.getVmrName());
                if (vmr.getVmrMode() == 0) {
                    vmrUuid = vmr.getId();
                }
            }

        } catch (SdkException e) {
            Demo.processException(e);
        }

        return vmrUuid;
    }

    public String addUsers(MeetingClient managerClient, String userId, String userName, String deptCode) {
        System.out.println("Start addUsers...");

        AddUserDTO user = new AddUserDTO()
                .withThirdAccount(userId)
                .withName(userName)
                .withDeptCode(deptCode);

        AddUserRequest request = new AddUserRequest()
                .withBody(user);
        String userAccount = "";
        try {
            AddUserResponse response = managerClient.addUser(request);
            System.out.printf("Add user: %s success, account is %s\r\n", userName, response.getUserAccount());
            userAccount = response.getUserAccount();

        } catch (SdkException e) {
            Demo.processException(e);
        }

        return userAccount;
    }

    public void deleteUser(MeetingClient managerClient, String accountId) {
        System.out.println("Start deleteUser...");

        List<String> users = new ArrayList<String>();
        users.add(accountId);

        BatchDeleteUsersRequest request = new BatchDeleteUsersRequest()
                .withBody(users);

        try {
            BatchDeleteUsersResponse response = managerClient.batchDeleteUsers(request);
            System.out.printf("Delete user account: %s success\r\n", accountId);
        } catch (SdkException e) {
            Demo.processException(e);
        }

    }


}
