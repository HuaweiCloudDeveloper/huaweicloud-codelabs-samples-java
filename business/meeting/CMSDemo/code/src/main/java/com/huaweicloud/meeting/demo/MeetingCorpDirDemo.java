/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.model.SearchCorpDirRequest;
import com.huaweicloud.sdk.meeting.v1.model.SearchCorpDirResponse;

public class MeetingCorpDirDemo {
    public String SearchCorpDir(MeetingClient userClient, String searchKey) {
        System.out.println("Start SearchCorpDir...");

        String sipNumber = "";
        SearchCorpDirRequest request = new SearchCorpDirRequest()
                .withSearchKey(searchKey)
                .withLimit(10);
        try {
            SearchCorpDirResponse response = userClient.searchCorpDir(request);
            if (response.getCount() > 0) {
                sipNumber = response.getData().get(0).getNumber();
                System.out.println("phone: " + sipNumber);

            }
        } catch (SdkException e) {
            Demo.processException(e);
        }

        return sipNumber;
    }
}
