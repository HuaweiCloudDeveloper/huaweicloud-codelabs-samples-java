# Huawei Cloud Meeting Typical Scenarios_Java
## 0. Version Description
This example is developed based on Huawei Cloud SDK V3.0.
## 1. About

### Introduction to Huawei Cloud Meeting

The server Java SDK (huaweicloud-sdk-meeting) of Huawei Cloud Meeting packages open APIs. You can integrate the SDK with third-party service systems.


-   For details about the open APIs of Huawei Cloud Meeting, see [Server API Reference](https://support.huaweicloud.com/api-meeting/meeting_21_0202.html).


## 2. Preparations
-   Apply for a Huawei Cloud account.
-   Apply for Huawei Cloud Meeting test resources.
-   Apply for a Huawei Cloud Meeting app ID.

For details, see [Preparations](https://support.huaweicloud.com/devg-meeting/meeting_20_0002.html).

##  3. Help

-   If you have any questions, seek help from the [Huawei Cloud Meeting developer forum](https://bbs.huaweicloud.com/forum/forum.php?mod=forumdisplay&orderby=lastpost&fid=784&filter=typeid&typeid=2843).

##  4. SDK Installation
You can use Maven to obtain and install SDKs, including huaweicloud-sdk-core and huaweicloud-sdk-meeting. For details about the SDK version number, visit the [SDK Center](https://sdkcenter.developer.huaweicloud.com/?product=Meeting&language=java).

Add the following dependency items to the **pom.xml** file of the Java project:
``` xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>3.0.87</version></dependency><dependency>
</dependency>
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-meeting</artifactId>
    <version>3.0.87</version>
</dependency>
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-nop</artifactId>
    <version>1.7.2</version>
</dependency>

```

##  5. Getting Started
###  Basic Knowledge of Huawei Cloud SDKs
For details about the basic knowledge of Huawei Cloud Java SDKs, see [Huawei Cloud Java Software Development Kit (Java SDK)](https://github.com/huaweicloud/huaweicloud-sdk-java-v3/blob/master/README.md).

**NOTE**
-   Credentials configuration in *Huawei Cloud Java Software Development Kit (Java SDK)* is not applicable to Huawei Cloud Meeting.
-   Huawei Cloud Meeting clients can be initialized only using an endpoint.

###  Meeting Request Method Names
-   Request method names of the Huawei Cloud Meeting server Java SDK are the API names in lower camel case. For details about the API names, visit [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/devsample?product=Meeting).
     

## 6. Typical Scenarios

### API Calling Overview


Figure 1 API calling sequence diagram in typical scenarios of Huawei Cloud Meeting

![](./assets/2.png)

#### Scenario 1: Initializing the Client
##### Description
Before calling other APIs of a server SDK, initialize the client. Each client contains the app ID, account, and endpoint information. If you use a server SDK, you do not need to maintain access tokens (that is, you do not need to call APIs for authentication or token update). The SDK creates an access token based on the information in the initialized client.

**NOTE**
-   The endpoint of Huawei Cloud (Chinese Mainland) is api.meeting.huaweicloud.com, and that of Huawei Cloud (International) is api-intl.meeting.huaweicloud.com.

##### Sample Code
```java
package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.MeetingCredentials;
import com.huaweicloud.sdk.meeting.v1.model.AuthTypeEnum;

public class MeetingClientDemo {
    private String demoAppId;
    private String demoAppKey;
    private MeetingClient managerClient;
    private MeetingClient userClient;
    private String demoEndPoint = "https://api.meeting.huaweicloud.com";

    MeetingClientDemo(String appId, String appKey) {
        demoAppId = appId;
        demoAppKey = appKey;
    }

    public MeetingClient getManagerClient() {
        return managerClient;
    }

    public MeetingClient getUserClient() {
        return userClient;
    }

    public void createCorpManagerClient() {
        // If the third-party user ID is not specified, the user is the enterprise administrator by default.
        ICredential auth = new MeetingCredentials()
                .withAuthType(AuthTypeEnum.APP_ID)
                .withAppId(demoAppId)
                .withAppKey(demoAppKey);

        managerClient = MeetingClient.newBuilder()
                .withCredential(auth)
                .withEndpoint(demoEndPoint)
                .build();
    }

    public void createCorpUserClient(String userId) {
        // If the third-party user ID is not specified, the user is a common user by default. You can set the user as an enterprise administrator.
        ICredential auth = new MeetingCredentials()
                .withAuthType(AuthTypeEnum.APP_ID)
                .withAppId(demoAppId)
                .withAppKey(demoAppKey)
                .withUserId(userId);

        userClient = MeetingClient.newBuilder()
                .withCredential(auth)
                .withEndpoint(demoEndPoint)
                .build();
    }
}
```

#### Scenario 2: Managing Departments and Users
##### Description
To use the corporate directory function, synchronize the organizational structure and user accounts to Huawei Cloud Meeting.

**NOTE**
-   If you do not require the corporate directory function and you use app ID authentication, you do not need to synchronize the organizational structure or user accounts.
##### Sample Code
Department management:
```java
package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.model.SearchDepartmentByNameRequest;
import com.huaweicloud.sdk.meeting.v1.model.SearchDepartmentByNameResponse;
import com.huaweicloud.sdk.meeting.v1.model.QueryDeptResultDTO;
import com.huaweicloud.sdk.meeting.v1.model.DeptDTO;
import com.huaweicloud.sdk.meeting.v1.model.AddDepartmentRequest;
import com.huaweicloud.sdk.meeting.v1.model.AddDepartmentResponse;
import com.huaweicloud.sdk.meeting.v1.model.DeleteDepartmentRequest;
import com.huaweicloud.sdk.meeting.v1.model.DeleteDepartmentResponse;

import java.util.Iterator;

public class MeetingDeptDemo {

    public void listDepts(MeetingClient managerClient, String deptName) {
        System.out.println("Start listDepts...");

        SearchDepartmentByNameRequest request = new SearchDepartmentByNameRequest()
                .withDeptName(deptName);

        try {
            SearchDepartmentByNameResponse response = managerClient.searchDepartmentByName(request);
            Iterator<QueryDeptResultDTO> iter = response.getBody().iterator();
            while (iter.hasNext()) {
                QueryDeptResultDTO dept = (QueryDeptResultDTO) iter.next();
                System.out.println(dept.getDeptCode());
            }

        } catch (SdkException e) {
            Demo.processException(e);
        }
    }

    public String addDept(MeetingClient managerClient, String deptName) {

        System.out.println("Start addDept...");

        DeptDTO addDeptRequestBody = new DeptDTO()
                .withDeptName(deptName);
        AddDepartmentRequest request = new AddDepartmentRequest()
                .withBody(addDeptRequestBody);

        String deptCode = "";

        try {
            AddDepartmentResponse response = managerClient.addDepartment(request);
            deptCode = response.getValue();
            System.out.printf("Add department: %s success, deptCode is %s\r\n", deptName, deptCode);
        } catch (SdkException e) {
            Demo.processException(e);
        }

        return deptCode;

    }


    public void deleteDept(MeetingClient managerClient, String deptCode) {
        System.out.println("Start deleteDept...");

        DeleteDepartmentRequest request = new DeleteDepartmentRequest()
                .withDeptCode(deptCode);

        try {
            DeleteDepartmentResponse response = managerClient.deleteDepartment(request);
            System.out.printf("Delete department code: %s success\r\n", deptCode);
        } catch (SdkException e) {
            Demo.processException(e);
        }
    }
}

```

User management:
```java
package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.model.SearchUsersRequest;
import com.huaweicloud.sdk.meeting.v1.model.SearchUsersResponse;
import com.huaweicloud.sdk.meeting.v1.model.SearchUserResultDTO;
import com.huaweicloud.sdk.meeting.v1.model.ShowUserDetailRequest;
import com.huaweicloud.sdk.meeting.v1.model.ShowUserDetailResponse;
import com.huaweicloud.sdk.meeting.v1.model.ShowMyInfoRequest;
import com.huaweicloud.sdk.meeting.v1.model.ShowMyInfoResponse;
import com.huaweicloud.sdk.meeting.v1.model.UserVmrDTO;
import com.huaweicloud.sdk.meeting.v1.model.AddUserDTO;
import com.huaweicloud.sdk.meeting.v1.model.AddUserRequest;
import com.huaweicloud.sdk.meeting.v1.model.AddUserResponse;
import com.huaweicloud.sdk.meeting.v1.model.BatchDeleteUsersRequest;
import com.huaweicloud.sdk.meeting.v1.model.BatchDeleteUsersResponse;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MeetingUserDemo {

    public void listUsers(MeetingClient managerClient) {
        System.out.println("Start listUsers...");

        SearchUsersRequest request = new SearchUsersRequest();
        try {
            SearchUsersResponse response = managerClient.searchUsers(request);
            Iterator<SearchUserResultDTO> iter = response.getData().iterator();
            while (iter.hasNext()) {
                SearchUserResultDTO user = (SearchUserResultDTO) iter.next();
                System.out.println("User name: " + user.getName() + "  Account id: " + user.getUserAccount());
            }

        } catch (SdkException e) {
            Demo.processException(e);
        }

    }

    public void showUsers(MeetingClient managerClient, String accountId) {
        System.out.println("Start showUsers...");

        ShowUserDetailRequest request = new ShowUserDetailRequest()
                .withAccount(accountId);

        try {
            ShowUserDetailResponse response = managerClient.showUserDetail(request);
            System.out.println("Account: " + response.getUserAccount());
            System.out.println("Third account: " + response.getThirdAccount());
            System.out.println("Name: " + response.getName());
            System.out.println("Department: " + response.getDeptName());
            System.out.println("phone:" + response.getPhone());
        } catch (SdkException e) {
            Demo.processException(e);
        }
    }

    public String showMyInfo(MeetingClient userClient) {
        System.out.println("Start showMyInfo...");

        ShowMyInfoRequest request = new ShowMyInfoRequest();
        String vmrUuid = "";
        try {
            ShowMyInfoResponse response = userClient.showMyInfo(request);
            System.out.println("Account: " + response.getUserAccount());
            System.out.println("Third account: " + response.getThirdAccount());
            System.out.println("Name: " + response.getName());
            System.out.println("Department: " + response.getDeptName());
            Iterator<UserVmrDTO> iter = response.getVmrList().iterator();
            while (iter.hasNext()) {
                UserVmrDTO vmr = (UserVmrDTO) iter.next();
                System.out.println("vmrUUID: " + vmr.getId() + "  vmrId: " + vmr.getVmrId() + "  vmrName: " + vmr.getVmrName());
                if (vmr.getVmrMode() == 0) {
                    vmrUuid = vmr.getId();
                }
            }

        } catch (SdkException e) {
            Demo.processException(e);
        }

        return vmrUuid;
    }

    public String addUsers(MeetingClient managerClient, String userId, String userName, String deptCode) {
        System.out.println("Start addUsers...");

        AddUserDTO user = new AddUserDTO()
                .withThirdAccount(userId)
                .withName(userName)
                .withDeptCode(deptCode);

        AddUserRequest request = new AddUserRequest()
                .withBody(user);
        String userAccount = "";
        try {
            AddUserResponse response = managerClient.addUser(request);
            System.out.printf("Add user: %s success, account is %s\r\n", userName, response.getUserAccount());
            userAccount = response.getUserAccount();

        } catch (SdkException e) {
            Demo.processException(e);
        }

        return userAccount;
    }

    public void deleteUser(MeetingClient managerClient, String accountId) {
        System.out.println("Start deleteUser...");

        List<String> users = new ArrayList<String>();
        users.add(accountId);

        BatchDeleteUsersRequest request = new BatchDeleteUsersRequest()
                .withBody(users);

        try {
            BatchDeleteUsersResponse response = managerClient.batchDeleteUsers(request);
            System.out.printf("Delete user account: %s success\r\n", accountId);
        } catch (SdkException e) {
            Demo.processException(e);
        }

    }


}

```

#### Scenario 3: Allocating Cloud Meeting Rooms
##### Description
Allocate purchased cloud meeting rooms to user accounts so that the users can create meetings using the cloud meeting rooms.
##### Sample Code
```java
package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.model.SearchCorpVmrRequest;
import com.huaweicloud.sdk.meeting.v1.model.SearchCorpVmrResponse;
import com.huaweicloud.sdk.meeting.v1.model.AssociateVmrRequest;
import com.huaweicloud.sdk.meeting.v1.model.AssociateVmrResponse;

import java.util.ArrayList;
import java.util.List;

public class MeetingVmrManageDemo {
    public String listUnallocatedVmr(MeetingClient managerClient) {
        System.out.println("Start listUnallocatedVmr...");

        SearchCorpVmrRequest request = new SearchCorpVmrRequest()
                .withVmrMode(1)
                .withStatus(2);

        String vmrUuid = "";
        try {
            SearchCorpVmrResponse response = managerClient.searchCorpVmr(request);

            if (response.getCount() > 0) {
                vmrUuid = response.getData().get(0).getId();
                System.out.printf("UnAllocated Vmr ID is %s\r\n", vmrUuid);
            }

        } catch (SdkException e) {
            Demo.processException(e);
        }

        return vmrUuid;
    }

    public void allocateVmr(MeetingClient managerClient, String account, String vmrUuid) {
        System.out.println("Start allocateVmr...");

        List<String> vmrUuids = new ArrayList<String>();
        vmrUuids.add(vmrUuid);
        AssociateVmrRequest request = new AssociateVmrRequest()
                .withAccount(account)
                .withAccountType(1)
                .withBody(vmrUuids);

        try {
            AssociateVmrResponse response = managerClient.associateVmr(request);

        } catch (SdkException e) {
            Demo.processException(e);
        }

    }
}

```

#### Scenario 4: Creating a Meeting
##### Description
Huawei Cloud Meeting has two types of resources: concurrent participants and cloud meeting rooms. Random and fixed meeting IDs can be created for both types of resources.
**NOTE**
- Concurrent participants
    - Random meeting ID: The meeting ID is the value of **conferenceID** returned by the API for creating a meeting. The value is randomly generated by the system.
    - Fixed meeting ID: personal meeting ID. The meeting ID is the value of **vmrConferenceID** returned by the API for creating a meeting. When creating a meeting, set **vmrFlag** to **1** and configure **vmrID**. Obtain the value of **vmrID** using the API for [Querying Personal Details as a User](https://support.huaweicloud.com/api-meeting/meeting_21_0073.html). When calling this API, set **vmrMode** to **0**.
- Cloud meeting rooms
    - Random meeting ID: The meeting ID is the value of **conferenceID** returned by the API for creating a meeting. The value is randomly generated by the system.
    - Fixed meeting ID: The meeting ID is the value of **vmrConferenceID** returned by the API for creating a meeting. When creating a meeting, set **vmrFlag** to **1** and configure **vmrID**. Obtain the value of **vmrID** using the API for [Querying Personal Details as a User](https://support.huaweicloud.com/api-meeting/meeting_21_0073.html). When calling this API, set **vmrMode** to **1**.

##### Sample Code
```java
package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.model.RestScheduleConfDTO;
import com.huaweicloud.sdk.meeting.v1.model.CreateMeetingRequest;
import com.huaweicloud.sdk.meeting.v1.model.CreateMeetingResponse;
import com.huaweicloud.sdk.meeting.v1.model.ConferenceInfo;
import com.huaweicloud.sdk.meeting.v1.model.CancelMeetingRequest;
import com.huaweicloud.sdk.meeting.v1.model.CancelMeetingResponse;
import com.huaweicloud.sdk.meeting.v1.model.ShowMeetingDetailRequest;
import com.huaweicloud.sdk.meeting.v1.model.ShowMeetingDetailResponse;
import com.huaweicloud.sdk.meeting.v1.model.StartRequest;
import com.huaweicloud.sdk.meeting.v1.model.StartMeetingRequest;
import com.huaweicloud.sdk.meeting.v1.model.StartMeetingResponse;

public class MeetingManageDemo {

    public String createMeeting(MeetingClient userClient, String meetingSubject, String startTime, Integer length, String vmrUuid) {
        System.out.println("Start createMeeting...");

        RestScheduleConfDTO createMeetingBody = new RestScheduleConfDTO()
                .withSubject(meetingSubject)
                .withStartTime(startTime)
                .withLength(length)
                .withMediaTypes("HDVideo");

        // Create a meeting using a cloud meeting room or personal meeting room.
        if (!vmrUuid.equals("")) {
            createMeetingBody.setVmrFlag(1);
            createMeetingBody.setVmrID(vmrUuid);
        }

        CreateMeetingRequest request = new CreateMeetingRequest()
                .withBody(createMeetingBody);

        String conferenceId = "";
        try {
            CreateMeetingResponse response = userClient.createMeeting(request);
            ConferenceInfo meetingInfo = response.getBody().get(0);
            conferenceId = meetingInfo.getConferenceID();
            if (!vmrUuid.equals("")) {
                // Meeting held in a cloud meeting room or personal meeting room
                System.out.println("Create Meeting on VMR resource. Conference ID is: " + conferenceId + " VMR Conference ID is: " + meetingInfo.getVmrConferenceID());
            } else {
                System.out.println("Create Meeting on concurrent resource. Conference ID is: " + conferenceId);
            }

        } catch (SdkException e) {
            Demo.processException(e);
        }

        return conferenceId;
    }

    public void deleteMeeting(MeetingClient userClient, String conferenceId) {
        System.out.println("Start DeleteMeeting...");

        CancelMeetingRequest request = new CancelMeetingRequest()
                .withConferenceID(conferenceId)
                .withType(1); // Forcibly end an ongoing meeting.

        try {
            CancelMeetingResponse response = userClient.cancelMeeting(request);
            System.out.printf("Delete Meeting: %s success \r\n", conferenceId);
        } catch (SdkException e) {
            Demo.processException(e);
        }
    }

    public String showMeeting(MeetingClient userClient, String conferenceId) {
        System.out.println("Start showMeeting...");

        ShowMeetingDetailRequest request = new ShowMeetingDetailRequest()
                .withConferenceID(conferenceId);
        String hostPassword = "";
        try {
            ShowMeetingDetailResponse response = userClient.showMeetingDetail(request);
            System.out.println("Meeting subject is: " + response.getConferenceData().getSubject());
            System.out.println("Meeting ID is: " + response.getConferenceData().getConferenceID());
            String role = response.getConferenceData()
                    .getPasswordEntry()
                    .get(0)
                    .getConferenceRole();
            if (role.equals("chair")) {
                hostPassword = response.getConferenceData()
                        .getPasswordEntry()
                        .get(0)
                        .getPassword();
                System.out.println("Meeting host password is: " + hostPassword);
                System.out.println("Meeting guest password is: " + response.getConferenceData()
                        .getPasswordEntry()
                        .get(1)
                        .getPassword());
            } else {
                hostPassword = response.getConferenceData()
                        .getPasswordEntry()
                        .get(0)
                        .getPassword();
                System.out.println("Meeting host password is: " + response.getConferenceData()
                        .getPasswordEntry()
                        .get(1)
                        .getPassword());
                System.out.println("Meeting guest password is: " + hostPassword);
            }

        } catch (SdkException e) {
            Demo.processException(e);
        }

        return hostPassword;
    }

    public void startMeeting(MeetingClient userClient, String conferenceId, String hostPassword) {
        System.out.println("Start startMeeting...");
        StartRequest startMeetingBody = new StartRequest()
                .withConferenceID(conferenceId)
                .withPassword(hostPassword);
        StartMeetingRequest request = new StartMeetingRequest()
                .withBody(startMeetingBody);

        try {
            StartMeetingResponse response = userClient.startMeeting(request);
            System.out.println("Start meeting success, conference id is: " + conferenceId);

        } catch (SdkException e) {
            Demo.processException(e);
        }
    }


}


```

#### Scenario 5: Querying the Corporate Directory
##### Description
Call the API for querying the corporate directory to obtain user information such as SIP number in scenarios such as inviting participants.

##### Sample Code
```java
package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.model.SearchCorpDirRequest;
import com.huaweicloud.sdk.meeting.v1.model.SearchCorpDirResponse;

public class MeetingCorpDirDemo {
    public String SearchCorpDir(MeetingClient userClient, String searchKey) {
        System.out.println("Start SearchCorpDir...");

        String sipNumber = "";
        SearchCorpDirRequest request = new SearchCorpDirRequest()
                .withSearchKey(searchKey)
                .withLimit(10);
        try {
            SearchCorpDirResponse response = userClient.searchCorpDir(request);
            if (response.getCount() > 0) {
                sipNumber = response.getData().get(0).getNumber();
                System.out.println("phone: " + sipNumber);

            }
        } catch (SdkException e) {
            Demo.processException(e);
        }

        return sipNumber;
    }
}

```

#### Scenario 6: Controlling a Meeting
##### Description
Call the meeting control APIs after a meeting starts. Before calling a meeting control API, obtain a meeting control token first.
- Inviting participants: Invite soft clients, hard terminals, mobile phones, and fixed-line phones (PSTN call must be enabled) by dialing SIP numbers.
- Setting continuous presence
##### Sample Code
```java
package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.meeting.v1.model.CreateConfTokenRequest;
import com.huaweicloud.sdk.meeting.v1.model.CreateConfTokenResponse;
import com.huaweicloud.sdk.meeting.v1.model.Attendee;
import com.huaweicloud.sdk.meeting.v1.model.RestInviteReqBody;
import com.huaweicloud.sdk.meeting.v1.model.InviteParticipantRequest;
import com.huaweicloud.sdk.meeting.v1.model.RestSubscriberInPic;
import com.huaweicloud.sdk.meeting.v1.model.RestCustomMultiPictureBody;
import com.huaweicloud.sdk.meeting.v1.model.SetCustomMultiPictureRequest;

import java.util.ArrayList;
import java.util.List;

public class MeetingControlDemo {
    public String createConfToken(MeetingClient userClient, String conferenceId, String hostPassword) {
        System.out.println("Start createConfToken...");
        String confToken = "";

        CreateConfTokenRequest request = new CreateConfTokenRequest()
                .withConferenceID(conferenceId)
                .withXPassword(hostPassword)
                .withXLoginType(1);
        try {
            CreateConfTokenResponse response = userClient.createConfToken(request);
            confToken = response.getData().getToken();
            System.out.printf("The token of conference %s is %s \r\n", conferenceId, confToken);
        } catch (SdkException e) {
            Demo.processException(e);
        }

        return confToken;
    }

    public void inviteParticipants(MeetingClient userClient, String confToken, String conferenceId, String sipNumber, String name) {
        System.out.println("Start inviteParticipants...");

        Attendee participant = new Attendee()
                .withName(name)
                .withPhone(sipNumber)
                .withType("normal");
        List<Attendee> participants = new ArrayList<>();
        participants.add(participant);
        RestInviteReqBody body = new RestInviteReqBody()
                .withAttendees(participants);

        InviteParticipantRequest request = new InviteParticipantRequest()
                .withXConferenceAuthorization(confToken)
                .withConferenceID(conferenceId)
                .withBody(body);

        try {
            userClient.inviteParticipant(request);
        } catch (SdkException e) {
            Demo.processException(e);
        }
    }

    public void setMultiImageLayout(MeetingClient userClient, String confToken, String conferenceId, String sipNumber) {
        System.out.println("Start setMultiImageLayout...");

        List<String> sipNumbers = new ArrayList<>();
        sipNumbers.add(sipNumber);

        RestSubscriberInPic subscriberInPic = new RestSubscriberInPic()
                .withIndex(1)
                .withSubscriber(sipNumbers);

        List<RestSubscriberInPic> subscriberInPics = new ArrayList<>();
        subscriberInPics.add(subscriberInPic);

        RestCustomMultiPictureBody body = new RestCustomMultiPictureBody()
                .withManualSet(1)
                .withSubscriberInPics(subscriberInPics)
                .withImageType("Six")
                .withMultiPicSaveOnly(false);

        SetCustomMultiPictureRequest request = new SetCustomMultiPictureRequest()
                .withXConferenceAuthorization(confToken)
                .withConferenceID(conferenceId)
                .withBody(body);

        try {
            userClient.setCustomMultiPicture(request);
        } catch (SdkException e) {
            Demo.processException(e);
        }
    }
}


```

#### Huawei Cloud Meeting Java SDK demo entry
##### Description
Invoke the sample code in the preceding scenarios to implement functions such as adding departments and users, managing cloud meeting rooms, creating meetings, querying the corporate directory, and controlling meetings.

**NOTE**
- Replace the app ID and app key before running the SDK.
##### Sample Code
```java
package com.huaweicloud.meeting.demo;

import com.huaweicloud.sdk.core.exception.SdkException;
import com.huaweicloud.sdk.meeting.v1.MeetingClient;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Demo {
    public static void main(String[] args) {
        System.out.println("Start HUAWEI CLOUD Meeting Server API Java Demo...");

        // Create clients for an administrator and common user.
        String appId = "YOUR APP ID";
        String appKey = "YOUR App SECRET";
        MeetingClientDemo clientDemo = new MeetingClientDemo(appId, appKey);
        clientDemo.createCorpManagerClient();
        MeetingClient managerClient = clientDemo.getManagerClient();
        clientDemo.createCorpUserClient("dev_user01");
        MeetingClient userClient = clientDemo.getUserClient();

        // Manage departments.
        MeetingDeptDemo deptDemo = new MeetingDeptDemo();
        String deptCode = deptDemo.addDept (managerClient, " Development Dept");
        deptDemo.listDepts (managerClient, " Development Dept");

        // Manage users.
        MeetingUserDemo userDemo = new MeetingUserDemo();
        userDemo.listUsers(managerClient);
        String userAccount = userDemo.addUsers (managerClient, "dev_user02", " User 2", deptCode);
        userDemo.showUsers(managerClient, userAccount);
        String vmrUuid = userDemo.showMyInfo(userClient);

        // Manage cloud meeting rooms.
        MeetingVmrManageDemo vmrManageDemo = new MeetingVmrManageDemo();
        String unallocatedVmeUuid = vmrManageDemo.listUnallocatedVmr(managerClient);
        vmrManageDemo.allocateVmr(managerClient, "dev_user02", unallocatedVmeUuid);

        // Manage meetings.
        MeetingManageDemo meetingManageDemo = new MeetingManageDemo();
        Date startDate = new Date((new Date()).getTime() + 1 * 3600 * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("Etc/UTC")); // The meeting start time is the UTC time.
        String startTimeStr = sdf.format(startDate);
        String conferenceId1 = meetingManageDemo.createMeeting(userClient, "Meeting Java SDK booked meeting",
                startTimeStr, 60, "");
        String hostPassword = meetingManageDemo.showMeeting(userClient, conferenceId1);
        String conferenceId2 = meetingManageDemo.createMeeting(userClient, "Meeting Java SDK booked vmr meeting",
                startTimeStr, 60, vmrUuid);

        // Query the corporate directory.
        MeetingCorpDirDemo meetingCorpDirDemo = new MeetingCorpDirDemo();
        String user2SipNumber = meetingCorpDirDemo.SearchCorpDir(userClient, "dev_user02");
        String user1SipNumber = meetingCorpDirDemo.SearchCorpDir(userClient, "dev_user01");

        // Control a meeting.
        meetingManageDemo.startMeeting(userClient, conferenceId1, hostPassword);  // Only ongoing meetings can be controled.
        MeetingControlDemo meetingControlDemo = new MeetingControlDemo();
        String conferenceToken = meetingControlDemo.createConfToken(userClient, conferenceId1, hostPassword);
        meetingControlDemo.inviteParticipants (userClient, conferenceToken, conferenceId1, user2SipNumber, " User 2");
        meetingControlDemo.setMultiImageLayout(userClient, conferenceToken, conferenceId1, user1SipNumber);

        // Clear test data.
        meetingManageDemo.deleteMeeting(userClient, conferenceId1);
        meetingManageDemo.deleteMeeting(userClient, conferenceId2);
        userDemo.deleteUser(managerClient, userAccount);
        deptDemo.deleteDept(managerClient, deptCode);
        
        System.out.println("Exit HUAWEI CLOUD Meeting Server API Java Demo");
    }

    public static void processException(SdkException exp) {
        try {
            throw exp;
        } catch (ConnectionException e) {
            System.out.println("Connection error.");
        } catch (RequestTimeoutException e) {
            System.out.println("Connection timeout.");
        } catch (ServiceResponseException e) {
            System.out.println("HTTP Status Code: " + e.getHttpStatusCode());
            System.out.println("Error Code: " + e.getErrorCode());
            System.out.println("Error Message: " + e.getErrorMsg());
            System.out.println("Request ID: " + e.getRequestId());
        }
    }
}

```


#####
## 7. References

-   Huawei Cloud Meeting [Developer Guide](https://support.huaweicloud.com/devg-meeting/meeting_20_0001.html)
-   Huawei Cloud Meeting [API Reference](https://support.huaweicloud.com/api-meeting/meeting_21_0202.html)

## 8. Change History
| Released On| Version| Description|
| :--------: | :------: | :----------: |
| 2020-11-24 | 1.0 | Initial release|  
| 2022-07-31 | 2.0 | Updated the Java SDK demo.| 
