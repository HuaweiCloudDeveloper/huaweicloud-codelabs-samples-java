### 产品介绍

1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/ROMA/doc?api=CreateMqsInstanceTopic)
中直接运行调试该接口。

2.在本样例中，可以创建Topic。

3.我们可以使用接口创建Topic，可以用来完善自定义的页面能力。

### 前置条件

1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已开通roma服务，购买实例。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-roma”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-roma</artifactId>
    <version>3.1.117</version>
</dependency>

```

### 代码示例

以下代码展示如何创建Topic：

``` java
package com.huawei.roma.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.roma.v2.RomaClient;
import com.huaweicloud.sdk.roma.v2.model.CreateMqsInstanceTopicReq;
import com.huaweicloud.sdk.roma.v2.model.CreateMqsInstanceTopicRequest;
import com.huaweicloud.sdk.roma.v2.model.CreateMqsInstanceTopicResponse;
import com.huaweicloud.sdk.roma.v2.region.RomaRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateMqsInstanceTopic {

    private static final Logger logger = LoggerFactory.getLogger(CreateMqsInstanceTopic.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 初始化SDK，填写认证信息和站点信息。
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        RomaClient client = RomaClient.newBuilder()
            .withCredential(auth)
            .withRegion(RomaRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // 组装请求体。
        CreateMqsInstanceTopicRequest request = new CreateMqsInstanceTopicRequest();
        // ROMA页面资源管理页面对应实例ID
        request.withInstanceId("11daa954-a186-4f0c-98da-bd0*********");
        CreateMqsInstanceTopicReq body = new CreateMqsInstanceTopicReq();
        // 权限类型 all：发布+订阅 pub：发布 sub：订阅
        body.withAccessPolicy(CreateMqsInstanceTopicReq.AccessPolicyEnum.fromValue("all"));
        // 集成应用Key，于集成应用页面获取。
        body.withAppId("45970f11-b22d-4bc4-aaa0-49**********");
        // Topic名称，以字母开头，仅能包含数字、字母、下划线(_)、中划线（-），长度3-200字符。
        body.withName("topic-name");
        request.withBody(body);
        // 发起HTTP API请求，并处理异常。
        try {
            CreateMqsInstanceTopicResponse response = client.createMqsInstanceTopic(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例

#### CreateMqsInstanceTopic

```
{
 "name": "topic-test"
}
```

### 修订记录

    发布日期    | 文档版本 | 修订说明

:----------:|:----:| :------:  
2024/10/24 | 1.0 | 文档首次发布