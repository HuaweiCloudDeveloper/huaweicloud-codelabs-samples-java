package com.huawei.roma.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.roma.v2.RomaClient;
import com.huaweicloud.sdk.roma.v2.model.CreateMqsInstanceTopicReq;
import com.huaweicloud.sdk.roma.v2.model.CreateMqsInstanceTopicRequest;
import com.huaweicloud.sdk.roma.v2.model.CreateMqsInstanceTopicResponse;
import com.huaweicloud.sdk.roma.v2.region.RomaRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateMqsInstanceTopic {

    private static final Logger logger = LoggerFactory.getLogger(CreateMqsInstanceTopic.class.getName());

    public static void main(String[] args) {
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Initialize the SDK and enter authentication and site information.
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        RomaClient client = RomaClient.newBuilder()
            .withCredential(auth)
            .withRegion(RomaRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // Assemble the request body.
        CreateMqsInstanceTopicRequest request = new CreateMqsInstanceTopicRequest();
        // ID of the instance on the resource management page of the ROMA page.
        request.withInstanceId("11daa954-a186-4f0c-98da-bd0*********");
        CreateMqsInstanceTopicReq body = new CreateMqsInstanceTopicReq();
        // Permission type. all: publish + subscribe. pub: publish. sub: subscribe.
        body.withAccessPolicy(CreateMqsInstanceTopicReq.AccessPolicyEnum.fromValue("all"));
        // Key of the integrated application, which can be obtained from the integrated application page.
        body.withAppId("45970f11-b22d-4bc4-aaa0-49**********");
        // Topic name, which starts with a letter and can contain only digits, letters, underscores (_), and hyphens (-). The value is a string of 3 to 200 characters.
        body.withName("topic-name");
        request.withBody(body);
        // Initiate an HTTP API request and handle the exception.
        try {
            CreateMqsInstanceTopicResponse response = client.createMqsInstanceTopic(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}

