### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/ROMA/doc?api=ListMqsInstanceTopics).

2.In this example, you can query the topic list.

3.You can use the API to query the topic list for customized page display.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The Roma service has been enabled and the instance has been purchased.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-roma. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-roma</artifactId>
    <version>3.1.117</version>
</dependency>

```

### Code example

The following code shows how to query the topic list:

``` java
package com.huawei.roma.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.roma.v2.RomaClient;
import com.huaweicloud.sdk.roma.v2.model.ListMqsInstanceTopicsRequest;
import com.huaweicloud.sdk.roma.v2.model.ListMqsInstanceTopicsResponse;
import com.huaweicloud.sdk.roma.v2.region.RomaRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListMqsInstanceTopics {

    private static final Logger logger = LoggerFactory.getLogger(ListMqsInstanceTopics.class.getName());

    public static void main(String[] args) {
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Initialize the SDK and enter authentication and site information.
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        RomaClient client = RomaClient.newBuilder()
            .withCredential(auth)
            .withRegion(RomaRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // Assemble the request body.
        ListMqsInstanceTopicsRequest request = new ListMqsInstanceTopicsRequest();
        // ID of the instance on the resource management page of the ROMA page.
        request.withInstanceId("11daa954-a186-4f0c-98da-bd********");
        // Initiate an HTTP API request and handle the exception.
        try {
            ListMqsInstanceTopicsResponse response = client.listMqsInstanceTopics(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result

#### ListMqsInstanceTopics

```
{
    "total": 1,
    "size": 1,
    "permissions": ["modify"],
    "topics": [{
        "policiesOnly": false,
        "name": "topic-test",
        "replication": 3,
        "partition": 3,
        "retention_time": 72,
        "sync_replication": false,
        "sync_message_flush": false,
        "topic_other_configs": [{
            "name": "max.message.bytes",
            "valid_values": "[0...10485760]",
            "default_value": "10485760",
            "config_type": "dynamic",
            "value": "10485760",
            "value_type": "int",
            "location": null
        },
        {
            "name": "message.timestamp.type",
            "valid_values": "[CreateTime, LogAppendTime]",
            "default_value": "LogAppendTime",
            "config_type": "dynamic",
            "value": "CreateTime",
            "value_type": "string",
            "location": null
        }],
        "app_id": "45970f11-b22d-4bc4-aaa***********",
        "app_name": "test",
        "permissions": ["read",
        "access",
        "delete",
        "modify"],
        "external_configs": {
            
        },
        "topic_type": 0,
        "created_at": 1729752366280
    }],
    "remain_partitions": 744,
    "max_partitions": 750,
    "topic_max_partitions": 200
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/10/24 | 1.0 | This document is released for the first time.