### 产品介绍

1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/ROMA/doc?api=CheckRomaInstanceListV2)
中直接运行调试该接口。

2.在本样例中，您可以查询实例列表。

3.我们可以使用接口查询实例列表，可以用来做自定义页面展示。

### 前置条件

1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已开通roma服务。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-roma”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-roma</artifactId>
    <version>3.1.117</version>
</dependency>

```

### 代码示例

以下代码展示如何查询实例列表：

``` java
package com.huawei.roma.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.roma.v2.RomaClient;
import com.huaweicloud.sdk.roma.v2.model.CheckRomaInstanceListV2Request;
import com.huaweicloud.sdk.roma.v2.model.CheckRomaInstanceListV2Response;
import com.huaweicloud.sdk.roma.v2.region.RomaRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckRomaInstanceListV2 {

    private static final Logger logger = LoggerFactory.getLogger(CheckRomaInstanceListV2.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 初始化SDK，填写认证信息和站点信息。
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        RomaClient client = RomaClient.newBuilder()
            .withCredential(auth)
            .withRegion(RomaRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // 组装请求体。
        CheckRomaInstanceListV2Request request = new CheckRomaInstanceListV2Request();
        // 发起HTTP API请求，并处理异常。
        try {
            CheckRomaInstanceListV2Response response = client.checkRomaInstanceListV2(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}

```

### 返回结果示例

#### CheckRomaInstanceListV2

```
{
 "total": 1,
 "size": 1,
 "instances": [
  {
   "id": "1514bd28-9e1c-4f17-86*********",
   "name": "*********-250331",
   "description": "测试资源",
   "flavor_id": "00400-30*********",
   "flavor_type": "basic",
   "cpu_arch": "x86_64",
   "vpc_id": "99233e6f-50c3-41fb-8ec9-059*********",
   "subnet_id": "d42195e2-d6d9-47a3-8b97-2*********",
   "security_group_id": "99587874-f838-4b79-9cfb-106*********",
   "publicip_enable": false,
   "publicip_id": null,
   "publicip_address": null,
   "connect_address": "192.***.0.1**",
   "status": "RUNNING",
   "charge_type": "prePaid",
   "project_id": "dd3b293b96754669bea812967*********",
   "create_time": "2024-03-26T01:58:24Z",
   "update_time": "2024-03-26T02:12:52Z",
   "maintain_begin": "22:00",
   "maintain_end": "02:00",
   "available_zone_ids": [
    "effdcbc7d4d64a02aa1fa2*********"
   ],
   "enterprise_project_id": "b8346f31-8315-497d-b193-d25a*********",
   "error_code": null,
   "error_msg": null,
   "site_id": null,
   "order_id": "CS240326*********",
   "ipv6_enable": false,
   "ipv6_connect_address": null
  }
 ]
}
```

### 修订记录

    发布日期    | 文档版本 | 修订说明

:----------:|:----:| :------:  
2024/10/24 | 1.0 | 文档首次发布