### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/ROMA/doc?api=CheckRomaInstanceListV2).

2.In this example, you can query the instance list.

3.You can use the API to query the instance list for customized page display.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The Roma service has been enabled.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-roma. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-roma</artifactId>
    <version>3.1.117</version>
</dependency>

```

### Code example

The following code shows how to query the instance list:

``` java
package com.huawei.roma.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.roma.v2.RomaClient;
import com.huaweicloud.sdk.roma.v2.model.CheckRomaInstanceListV2Request;
import com.huaweicloud.sdk.roma.v2.model.CheckRomaInstanceListV2Response;
import com.huaweicloud.sdk.roma.v2.region.RomaRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckRomaInstanceListV2 {

    private static final Logger logger = LoggerFactory.getLogger(CheckRomaInstanceListV2.class.getName());

    public static void main(String[] args) {
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security;
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Initialize the SDK and enter authentication and site information.
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        RomaClient client = RomaClient.newBuilder()
            .withCredential(auth)
            .withRegion(RomaRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // Assemble the request body.
        CheckRomaInstanceListV2Request request = new CheckRomaInstanceListV2Request();
        // Initiate an HTTP API request and handle the exception.
        try {
            CheckRomaInstanceListV2Response response = client.checkRomaInstanceListV2(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result

#### CheckRomaInstanceListV2

```
{
 "total": 1,
 "size": 1,
 "instances": [
  {
   "id": "1514bd28-9e1c-4f17-86*********",
   "name": "*********-250331",
   "description": "测试资源",
   "flavor_id": "00400-30*********",
   "flavor_type": "basic",
   "cpu_arch": "x86_64",
   "vpc_id": "99233e6f-50c3-41fb-8ec9-059*********",
   "subnet_id": "d42195e2-d6d9-47a3-8b97-2*********",
   "security_group_id": "99587874-f838-4b79-9cfb-106*********",
   "publicip_enable": false,
   "publicip_id": null,
   "publicip_address": null,
   "connect_address": "192.***.0.1**",
   "status": "RUNNING",
   "charge_type": "prePaid",
   "project_id": "dd3b293b96754669bea812967*********",
   "create_time": "2024-03-26T01:58:24Z",
   "update_time": "2024-03-26T02:12:52Z",
   "maintain_begin": "22:00",
   "maintain_end": "02:00",
   "available_zone_ids": [
    "effdcbc7d4d64a02aa1fa2*********"
   ],
   "enterprise_project_id": "b8346f31-8315-497d-b193-d25a*********",
   "error_code": null,
   "error_msg": null,
   "site_id": null,
   "order_id": "CS240326*********",
   "ipv6_enable": false,
   "ipv6_connect_address": null
  }
 ]
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/10/24 | 1.0 | This document is released for the first time.