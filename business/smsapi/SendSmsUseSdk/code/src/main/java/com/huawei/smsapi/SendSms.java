package com.huawei.smsapi;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.smsapi.v1.SMSApiClient;
import com.huaweicloud.sdk.smsapi.v1.SMSApiCredentials;
import com.huaweicloud.sdk.smsapi.v1.model.BatchSendDiffSmsRequest;
import com.huaweicloud.sdk.smsapi.v1.model.BatchSendDiffSmsRequestBody;
import com.huaweicloud.sdk.smsapi.v1.model.BatchSendDiffSmsResponse;
import com.huaweicloud.sdk.smsapi.v1.model.BatchSendSmsRequest;
import com.huaweicloud.sdk.smsapi.v1.model.BatchSendSmsRequestBody;
import com.huaweicloud.sdk.smsapi.v1.model.BatchSendSmsResponse;
import com.huaweicloud.sdk.smsapi.v1.model.SmsContent;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Use the SDK interface to send an SMS message.
 * Before using this example, you need to create an SMS application, signature,
 * and template through the console or by invoking the open APIs of the MSGSMS.
 */
public class SendSms {
    public static void main(String[] args) {
        System.out.println("Start HUAWEI CLOUD MSGSMS SEND SMS Java Demo...");

        /*
         * Send an SMS message using a special AK/SK authentication algorithm.
         * When the MSGSMS is used to send SMS messages, the AK is app_key, and the SK is app_secret.
         * There will be security risks if the app_key/app_secret used for authentication is directly written into code.
         * We suggest encrypting the app_key/app_secret in the configuration file or environment variables for storage.
         * In this sample, the app_key/app_secret is stored in environment variables for identity authentication.
         * Before running this sample, set the environment variables CLOUD_SDK_MSGSMS_APPKEY and CLOUD_SDK_MSGSMS_APPSECRET.
         * CLOUD_SDK_MSGSMS_APPKEY indicates the application key (app_key), and CLOUD_SDK_MSGSMS_APPSECRET indicates the application secret (app_secret).
         * You can obtain the value from Application Management on the console or by calling the open API of Application Management.
         */
        String ak = System.getenv("CLOUD_SDK_MSGSMS_APPKEY");
        String sk = System.getenv("CLOUD_SDK_MSGSMS_APPSECRET");

        /* Creating an SmsApiClient Instance
         * This region of this example is of Beijing 4. Replace it with the actual value.
         */
        SMSApiClient client = getClient(new Region("cn-north-4", new String[]{"https://smsapi.cn-north-4.myhuaweicloud.com:8443"}),
                getCredential(ak, sk));

        try {
            // Example 1: Use POST /sms/batchSendSms/v1 to send an SMS message.
            System.out.println("----Example 1 use POST /sms/batchSendSms/v1 send msg now.");
            int httpcode = batchSendSms(client);
            System.out.println((httpcode == 200) ? "----Example 1 send sms success." : "----Example 1 send sms failed.");

            // Example 2: Using the POST /sms/batchSendDiffSms/v1 to Send SMS Messages in Batches
            System.out.println("----Example 2 use POST /sms/batchSendDiffSms/v1 send msg now.");
            httpcode = batchSendDiffSms(client);
            System.out.println((httpcode == 200) ? "----Example 2 send sms success." : "----Example 2 send sms failed.");
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    /**
     * Sending SMS Messages
     *
     * @param client SMSApiClient Instance
     * @return Indicates whether the SMS message is sent successfully.
     */
    private static int batchSendSms(SMSApiClient client) {
        // Construct a request for sending an SMS message.
        BatchSendSmsRequest request = new BatchSendSmsRequest();
        request.withBody(new BatchSendSmsRequestBody()
                // Channel ID for sending SMS messages.
                .withFrom("8824110605***")
                // List of numbers that receive SMS messages.
                // Note: When there are multiple numbers, do not contain spaces between the numbers.
                .withTo("+86137****3774,+86137****3776")
                // Template Id
                .withTemplateId("e1440669a4354ccdb56ebf2283c6***")
                // Template parameter, which must be enclosed in square brackets ([]).
                .withTemplateParas("[\"12\",\"23\",\"e\"]")
                // Status report callback URL. Set this parameter to a valid value. If status reports are not required, you do not need to set this parameter.
                .withStatusCallback("https://test/report"));

        // Invoke the SDK interface to send an SMS message.
        BatchSendSmsResponse response = client.batchSendSms(request);

        // Print the response result.
        System.out.println(response.toString());
        return response.getHttpStatusCode();
    }

    /**
     * Sending SMS Messages in Batches
     *
     * @param client SMSApiClient Instance
     * @return Whether the batch SMS message is sent successfully.
     */
    private static int batchSendDiffSms(SMSApiClient client) {
        //Construct a request for sending SMS messages in batches.
        BatchSendDiffSmsRequest request = new BatchSendDiffSmsRequest();
        request.withBody(new BatchSendDiffSmsRequestBody()
                // Channel ID for sending SMS messages.
                .withFrom("8824110605***")
                // Status report callback URL. Set this parameter to a valid value. If status reports are not required, you do not need to set this parameter.
                .withStatusCallback("https://test/report")
                .withSmsContent(new ArrayList<>(Arrays.asList(
                        //  Content of the first batch of group SMS messages
                        new SmsContent()
                                // List of SMS Recipient Numbers
                                .withTo(new ArrayList<>((Arrays.asList("+86137****3774", "+86137****3775"))))
                                // Template Id
                                .withTemplateId("cefada4b8eaa4835864eb5b5eae1****")
                                // Template Parameters
                                .withTemplateParas(new ArrayList<>(Arrays.asList("1", "23", "45"))),

                        // Content of the second batch of group SMS messages
                        new SmsContent()
                                // List of SMS Recipient Numbers
                                .withTo(new ArrayList<>((Arrays.asList("+86137****3777", "+86137****3778"))))
                                // Template Id
                                .withTemplateId("e1440669a4354ccdb56ebf2283c6****")
                                // Template Parameters
                                .withTemplateParas(new ArrayList<>(Arrays.asList("3", "4", "5")))))));

        //Invoke the SDK interface to send SMS messages in batche
        BatchSendDiffSmsResponse response = client.batchSendDiffSms(request);

        // Print the response result.
        System.out.println(response.toString());
        return response.getHttpStatusCode();
    }

    /**
     * Creating an Authentication Credential
     *
     * @param ak Application key (AK) generated after an app is created for the SMS service.
     * @param sk Application Secret (SK) generated after an app is created for the SMS service.
     * @return Authentication credential
     */
    private static SMSApiCredentials getCredential(String ak, String sk) {
        return new SMSApiCredentials()
                .withAk(ak)
                .withSk(sk);
    }

    /**
     * Create SMSApiClient Instance
     *
     * @param region region
     * @param auth   Authentication credential
     * @return SMSApiClient Instance
     */
    private static SMSApiClient getClient(Region region, ICredential auth) {
        // Use the default configuration.
        HttpConfig config = HttpConfig.getDefaultHttpConfig();

        /* To prevent API invoking failures caused by HTTPS certificate authentication failures, ignore the
         * certificate trust issue to simplify the sample code, set InsecureSkipVerify to true.
         * Note: Do not ignore the TLS certificate verification in the commercial version.
         */
        config.withIgnoreSSLVerification(true);

        /*
         * Note:
         * By default, the SDK for sending SMS messages uses the okhttp component to send messages. The maximum number of connections is 5.
         * The thread pool uses ForkJoinPool. By default, the number of CPUs in the thread pool is the same as that in the server.
         * The two parameters limit the maximum number of concurrent requests. If the delay increases and the performance cannot be improved,
         * you can redefine the two parameters in HttpConfig to improve the system performance. For example:
         * HttpConfig config = HttpConfig.getDefaultHttpConfig();
         * config.withConnectionPool(new ConnectionPool(50, 5L, TimeUnit.MINUTES));
         * config.withExecutorService(new ForkJoinPool(200));`
         */

        // Initializing the Client of the SMSApi Service
        return SMSApiClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(region)
                .build();
    }
}