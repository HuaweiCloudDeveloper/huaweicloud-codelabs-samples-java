## 1、介绍
**什么是华为云消息&短信（Message & SMS）服务？**

[消息&短信（Message & SMS）](https://support.huaweicloud.com/intl/zh-cn/productdesc-msgsms/sms_desc.html)是华为云携手全球多家优质运营商和渠道，为企业用户（不包括个体工商户、个人独资企业、合伙企业等非法人主体或组织）提供单发、群发短信服务，同时可接收用户回复短信。

**您将学到什么？**

如何通过Java版SDK来体验消息&短信（Message & SMS）服务的发送短信功能。

## 2、开发时序图
![时序图](assets/sendsms.png)

## 3、前置条件
- 1、需要是华为云企业认证账号，并开通华为云MSGSMS云服务。
- 2、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。 使用服务端SDK前，您需要安装“huaweicloud-sdk-smsapi”，具体的SDK版本号请参见[消息&短信（业务API）SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/SMSApi?lang=Java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-smsapi</artifactId>
    <version>3.1.125</version>
</dependency>
```
- 3、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 4、已获取应用的Application Key(app_key)和Application Secret(app_secret)。请在华为云控制台“应用管理”页面上创建和查看您的Application Key和Application Secret。具体请参见 [应用管理](https://console.huaweicloud.com/msgsms/#/msgSms/applicationManage) 。
- 5、已获得您要发送短信的通道号，由于通道号与签名关联，请在控制台“签名管理”页面上查看您的通道号，如果您还没有通道号，则您需要先提交签名申请，待签名申请通过后，系统会分配通道号。具体请参见[签名管理](https://console.huaweicloud.com/msgsms/#/msgSms/signatureManage)。
- 6、已获取您要发送短信的模板ID，请在控制台“模板管理”页面上创建和查看您的模板ID。具体请参见[模板管理](https://console.huaweicloud.com/msgsms/#/msgSms/modelManage)。

## 4、接口参数说明
关于接口参数的详细说明可参见：

[a.发送短信](https://support.huaweicloud.com/api-msgsms/sms_05_0001.html)

[b.发送分批短信](https://support.huaweicloud.com/api-msgsms/sms_05_0002.html)

## 5、关键代码片段

### 5.1、创建SMSApiClient
```java
public static void main(String[] args){
    /*
     * Send an SMS message using a special AK/SK authentication algorithm.
     * When the MSGSMS is used to send SMS messages, the AK is app_key, and the SK is app_secret.
     * There will be security risks if the app_key/app_secret used for authentication is directly written into code.
     * We suggest encrypting the app_key/app_secret in the configuration file or environment variables for storage.
     * In this sample, the app_key/app_secret is stored in environment variables for identity authentication.
     * Before running this sample, set the environment variables CLOUD_SDK_MSGSMS_APPKEY and CLOUD_SDK_MSGSMS_APPSECRET.
     * CLOUD_SDK_MSGSMS_APPKEY indicates the application key (app_key), and CLOUD_SDK_MSGSMS_APPSECRET indicates the application secret (app_secret).
     * You can obtain the value from Application Management on the console or by calling the open API of Application Management.
     */
    String ak=System.getenv("CLOUD_SDK_MSGSMS_APPKEY");
    String sk=System.getenv("CLOUD_SDK_MSGSMS_APPSECRET");

    /* Creating an SmsApiClient Instance
     * This region of this example is of Beijing 4. Replace it with the actual value.
     */
    SMSApiClient client = getClient(new Region("cn-north-4",new String[]{"https://smsapi.cn-north-4.myhuaweicloud.com:8443"}),
    getCredential(ak,sk));
}

/**
 * Create SMSApiClient Instance
 *
 * @param region region
 * @param auth   Authentication credential
 * @return SMSApiClient Instance
 */
private static SMSApiClient getClient(Region region, ICredential auth) {
    // Use the default configuration.
    HttpConfig config = HttpConfig.getDefaultHttpConfig();

    /* To prevent API invoking failures caused by HTTPS certificate authentication failures, ignore the
     * certificate trust issue to simplify the sample code, set InsecureSkipVerify to true.
     * Note: Do not ignore the TLS certificate verification in the commercial version.
     */
    config.withIgnoreSSLVerification(true);

    /*
     * Note:
     * By default, the SDK for sending SMS messages uses the okhttp component to send messages. The maximum number of connections is 5.
     * The thread pool uses ForkJoinPool. By default, the number of CPUs in the thread pool is the same as that in the server.
     * The two parameters limit the maximum number of concurrent requests. If the delay increases and the performance cannot be improved,
     * you can redefine the two parameters in HttpConfig to improve the system performance. For example:
     * HttpConfig config = HttpConfig.getDefaultHttpConfig();
     * config.withConnectionPool(new ConnectionPool(50, 5L, TimeUnit.MINUTES));
     * config.withExecutorService(new ForkJoinPool(200));`
     */
    
    // Initializing the Client of the SMSApi Service
    return SMSApiClient.newBuilder()
    .withHttpConfig(config)
    .withCredential(auth)
    .withRegion(region)
    .build();
}
```
### 5.2、调用发送短信接口（BatchSendSms）发送短信
```java
int httpcode = batchSendSms(client);

/**
 * Sending SMS Messages
 *
 * @param client SMSApiClient Instance
 * @return Indicates whether the SMS message is sent successfully.
 */
private static int batchSendSms(SMSApiClient client) {
    // Construct a request for sending an SMS message.
    BatchSendSmsRequest request = new BatchSendSmsRequest();
    request.withBody(new BatchSendSmsRequestBody()
    // Channel ID for sending SMS messages.
    .withFrom("8824110605***")
    // List of numbers that receive SMS messages.
    // Note: When there are multiple numbers, do not contain spaces between the numbers.
    .withTo("+86137****3774,+86137****3776")
    // Template Id
    .withTemplateId("e1440669a4354ccdb56ebf2283c6***")
    // Template parameter, which must be enclosed in square brackets ([]).
    .withTemplateParas("[\"12\",\"23\",\"e\"]")
    // Status report callback URL. Set this parameter to a valid value. If status reports are not required, you do not need to set this parameter.
    .withStatusCallback("https://test/report"));

    // Invoke the SDK interface to send an SMS message.
    BatchSendSmsResponse response = client.batchSendSms(request);

    // Print the response result.
    System.out.println(response.toString());
    return response.getHttpStatusCode();
}
```

### 5.3、调用发送分批短信接口（BatchSendDiffSms）发送短信
```java
httpcode = batchSendDiffSms(client);

    /**
 * Sending SMS Messages in Batches
 *
 * @param client SMSApiClient Instance
 * @return Whether the batch SMS message is sent successfully.
 */
private static int batchSendDiffSms(SMSApiClient client) {
    //Construct a request for sending SMS messages in batches.
    BatchSendDiffSmsRequest request = new BatchSendDiffSmsRequest();
    request.withBody(new BatchSendDiffSmsRequestBody()
    // Channel ID for sending SMS messages.
    .withFrom("8824110605***")
    // Status report callback URL. Set this parameter to a valid value. If status reports are not required, you do not need to set this parameter.
    .withStatusCallback("https://test/report")
    .withSmsContent(new ArrayList<>(Arrays.asList(
    //  Content of the first batch of group SMS messages
    new SmsContent()
    // List of SMS Recipient Numbers
    .withTo(new ArrayList<>((Arrays.asList("+86137****3774", "+86137****3775"))))
    // Template Id
    .withTemplateId("cefada4b8eaa4835864eb5b5eae1****")
    // Template Parameters
    .withTemplateParas(new ArrayList<>(Arrays.asList("1", "23", "45"))),

    // Content of the second batch of group SMS messages
    new SmsContent()
    // List of SMS Recipient Numbers
    .withTo(new ArrayList<>((Arrays.asList("+86137****3777", "+86137****3778"))))
    // Template Id
    .withTemplateId("e1440669a4354ccdb56ebf2283c6****")
    // Template Parameters
    .withTemplateParas(new ArrayList<>(Arrays.asList("3", "4", "5")))))));

    //Invoke the SDK interface to send SMS messages in batche
    BatchSendDiffSmsResponse response = client.batchSendDiffSms(request);

    // Print the response result.
    System.out.println(response.toString());
    return response.getHttpStatusCode();
}
```

## 6、运行结果
**发送短信（BatchSendSms）**
```json
class BatchSendSmsResponse {
  code: 000000
  description: Success
  result: [class SmsID {
    createTime: 2024-11-22T02:16:46Z
    from: 8824110605***
    originTo: +86137****3774
    smsMsgId: eb4e63dd-0945-4231-a48d-6b54c1a8b614_3434030
    status: 000000
    countryId: CN
    total: 1
  }, class SmsID {
    createTime: 2024-11-22T02:16:46Z
    from: 8824110605***
    originTo: +86137****3776
    smsMsgId: eb4e63dd-0945-4231-a48d-6b54c1a8b614_3434031
    status: 000000
    countryId: CN
    total: 1
  }]
}
```

**发送分批短信（BatchSendDiffSms）**
```json
class BatchSendDiffSmsResponse {
  code: 000000
  description: Success
  result: [class SmsID {
    createTime: 2024-11-22T02:16:46Z
    from: 8824110605***
    originTo: +86137****3774
    smsMsgId: eb4e63dd-0945-4231-a48d-6b54c1a8b614_3435044
    status: 000000
    countryId: CN
    total: 0
  }, class SmsID {
    createTime: 2024-11-22T02:16:46Z
    from: 8824110605***
    originTo: +86137****3775
    smsMsgId: eb4e63dd-0945-4231-a48d-6b54c1a8b614_3435045
    status: 000000
    countryId: CN
    total: 0
  }, class SmsID {
    createTime: 2024-11-22T02:16:46Z
    from: 8824110605***
    originTo: +86137****3777
    smsMsgId: eb4e63dd-0945-4231-a48d-6b54c1a8b614_3435046
    status: 000000
    countryId: CN
    total: 1
  }, class SmsID {
    createTime: 2024-11-22T02:16:46Z
    from: 8824110605***
    originTo: +86137****3778
    smsMsgId: eb4e63dd-0945-4231-a48d-6b54c1a8b614_3435047
    status: 000000
    countryId: CN
    total: 1
  }]
}
```

## 7、参考
本示例的代码工程仅用于简单演示，实际开发过程中应严格遵循开发指南。访问以下链接可以获取详细信息：[开发指南](https://support.huaweicloud.com/devg-msgsms/sms_04_0000.html)