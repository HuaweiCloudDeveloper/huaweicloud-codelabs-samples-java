package com.huawei.smsapi;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.http.HttpMethod;
import com.huaweicloud.sdk.core.http.HttpRequest;
import com.huaweicloud.sdk.smsapi.utils.SmsAkSkSigner;
import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import java.io.IOException;

import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Objects;

/**
 * Example of Invoking the batchSendDiffSms Interface to Send an SMS Message.
 */
public class BatchSendDiffSms {
    private static CloseableHttpClient httpClient = null;

    public static void main(String[] args) throws Exception {
        /*
         * Send an SMS message using a special AK/SK authentication algorithm.
         * When the MSGSMS is used to send SMS messages, the AK is app_key, and the SK is app_secret.
         * There will be security risks if the app_key/app_secret used for authentication is directly written into code.
         * We suggest encrypting the app_key/app_secret in the configuration file or environment variables for storage.
         * In this sample, the app_key/app_secret is stored in environment variables for identity authentication.
         * Before running this sample, set the environment variables CLOUD_SDK_MSGSMS_APPKEY and CLOUD_SDK_MSGSMS_APPSECRET.
         * CLOUD_SDK_MSGSMS_APPKEY indicates the application key (app_key), and CLOUD_SDK_MSGSMS_APPSECRET indicates the application secret (app_secret).
         * You can obtain the value from Application Management on the console or by calling the open API of Application Management.
         */
        String ak = System.getenv("CLOUD_SDK_MSGSMS_APPKEY");
        String sk = System.getenv("CLOUD_SDK_MSGSMS_APPSECRET");
        if (Objects.isNull(ak) || Objects.isNull(sk)) {
            System.out.println("the ak or sk is null. please config the environment CLOUD_SDK_MSGSMS_APPKEY and CLOUD_SDK_MSGSMS_APPSECRET first!");
            return;
        }

        System.out.println("----Use POST /sms/batchSendDiffSms/v1 send msg now.");
        if (sendSmsUsingDiff(ak, sk) == HttpStatus.SC_OK) {
            System.out.println("----batchSendDiffSms success.");
        } else {
            System.out.println("----batchSendDiffSms failed.");
        }
    }

    /**
     * Example of Invoking the batchSendDiffSms Interface to Send an SMS Message
     * @param ak AK is the appkey of the SMS application.
     * @param sk SK is the appsecret of the SMS application.
     * @throws Exception
     */
    private static int sendSmsUsingDiff(String ak, String sk) throws Exception {
        // The apiAddress of this example is of Beijing 4. Replace it with the actual value.
        String apiAddress = "https://smsapi.cn-north-4.myhuaweicloud.com:8443/sms/batchSendDiffSms/v1";

        /* Construct the message body of the sample code.
           For details about how to construct a body, see the API description.
           Sample is:
            {
                "from": "8824110605***",
                "statusCallback": "https://test/report",
                "smsContent": [{
                        "to": ["+86137****3774", "+86137****3775"],
                        "templateId": "1d18a7f4e1b84f6c8fc1546b48b3baea",
                        "templateParas": ["1", "23", "e"]
                    },
                    {
                        "to": ["+86137****3777"],
                        "templateId": "7824a51b3cc34976919a5418f637d0fb",
                        "templateParas": ["4", "5", "6"]
                    }
                ]
            }
         */
        String body = "{" +
                "    \"from\": \"8824110605***\"," +
                "    \"statusCallback\": \"https://test/report\"," +
                "    \"smsContent\": [{" +
                "        \"to\": [\"+86137****3774\", \"+86137****3775\"]," +
                "        \"templateId\": \"1d18a7f4e1b84f6c8fc1546b48b3baea\"," +
                "        \"templateParas\": [\"1\", \"23\", \"e\"]" +
                "    }," +
                "    {" +
                "        \"to\": [\"+86137****3777\"]," +
                "        \"templateId\": \"7824a51b3cc34976919a5418f637d0fb\"," +
                "        \"templateParas\": [\"4\", \"5\", \"6\"]" +
                "    }]" +
                "}";

        System.out.println("body:" + body);

        // Invoke /sms/batchSendDiffSms/v1 api to send messages
        HttpPost postRequest = new HttpPost(apiAddress);
        postRequest.setHeader("Accept", "application/json");
        postRequest.setHeader("Content-Type", "application/json;charset=utf8");
        postRequest.setHeader("User-Agent", "huaweicloud-usdk-java/3.0");
        postRequest.setEntity(new StringEntity(body, StandardCharsets.UTF_8));

        // Signature operation of the batchsenddiffsms interface
        postRequest = sign(postRequest, body, ak, sk);
        if (Objects.isNull(postRequest)) {
            System.out.println("sign failed!");
            return HttpStatus.SC_INTERNAL_SERVER_ERROR;
        }

        return sendMessage(postRequest);
    }

    /**
     * Signing the request message.
     * @param postRequest the request
     * @param body Message body to be signed
     * @param ak AK is the appkey of the SMS application.
     * @param sk SK is the appsecret of the SMS application.
     * @return signed request.
     */
    private static HttpPost sign(HttpPost postRequest, String body, String ak, String sk) {
        // The SDK signature algorithm is invoked. The original request needs to be converted into an SDK request for signature.
        HttpRequest.HttpRequestBuilder sdkHttpRequestBuilder = HttpRequest.newBuilder().withBodyAsString(body)
                .withMethod(HttpMethod.valueOf(postRequest.getMethod()))
                .withEndpoint(postRequest.getURI().toString());
        for (Header header : postRequest.getAllHeaders()) {
            sdkHttpRequestBuilder.addHeader(header.getName(), header.getValue());
        }

        // The signature algorithm uses the AK and SK signature algorithms provided by HUAWEI CLOUD IAM and API Gateway.
        // Signature algorithm implementation. The capabilities provided by the SDK are used here.
        // Developers can also use the signature capability provided by HUAWEI CLOUD APIG. For details, see the following website: https://support.huaweicloud.com/devg-apisign/api-sign-sdk-java.html
        // For the signature operation of an interface, the signature must contain the body.
        Map<String, String> signedHeaders = SmsAkSkSigner.sign(sdkHttpRequestBuilder.build(), new BasicCredentials().withAk(ak).withSk(sk));
        if (Objects.isNull(signedHeaders)) {
            System.out.println("sign failed!");
            return null;
        }

        signedHeaders.forEach((key, value) -> postRequest.setHeader(key, value));
        return postRequest;
    }

    /**
     * Send the request message.
     * @param postRequest Message request to be sent.
     * @return The http status code, indicates whether the SMS message is sent successfully.
     * @throws Exception
     */
    private static int sendMessage(HttpPost postRequest) throws Exception {
        // Note: The sample code is for reference only. For commercial code, you can use the thread pool technology to increase the number of concurrent requests and solve the concurrency problem.
        if (Objects.isNull(httpClient)) {
            // To prevent API invoking failures caused by HTTPS certificate authentication failures, ignore the certificate trust issue to simplify the sample code.
            // Note: Do not ignore the TLS certificate verification in the commercial version.
            httpClient = createIgnoreSSLHttpClient();
        }

        try (CloseableHttpResponse response = httpClient.execute(postRequest)) {
            // Print the response status code.
            System.out.println("Response Status Code: " + response.getStatusLine().getStatusCode());

            // Print the response content.
            System.out.println("Response Content: " + EntityUtils.toString(response.getEntity()));
            return response.getStatusLine().getStatusCode();
        } catch (IOException e) {
            System.out.println("response exception:" + e);
        }

        return HttpStatus.SC_INTERNAL_SERVER_ERROR;
    }

    /**
     * Create an HTTP client that ignores the HTTPS certificate check.
     * @return HTTP client
     * @throws Exception
     */
    private static CloseableHttpClient createIgnoreSSLHttpClient() throws Exception {
        SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null,  (x509CertChain, authType) -> true).build();
        return HttpClients.custom().setSSLSocketFactory(new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE)).build();
    }
}