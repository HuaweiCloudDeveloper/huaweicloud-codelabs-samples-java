## 1、功能介绍

**什么是华为云消息&短信（Message & SMS）服务？**

[消息&短信（Message & SMS）](https://support.huaweicloud.com/intl/zh-cn/productdesc-msgsms/sms_desc.html)是华为云携手全球多家优质运营商和渠道，为企业用户（不包括个体工商户、个人独资企业、合伙企业等非法人主体或组织）提供单发、群发短信服务，同时可接收用户回复短信。

**您将学到什么？**

该示例展示了如何使用Java调用消息&短信（Message & SMS）服务的API发送短信。

## 2、开发时序图
![image](assets/sendsmsuseapi.png)

## 3、前置条件
- 1、需要是华为云企业认证账号，并开通华为云MSGSMS云服务。
- 2、该示例使用了消息&短信（Message & SMS）服务的SDK来生成签名头域(Authorization)，需要获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。 您需要安装“huaweicloud-sdk-smsapi”，具体的SDK版本号请参见[消息&短信（业务API）SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/SMSApi?lang=Java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-smsapi</artifactId>
    <version>3.1.125</version>
</dependency>
```
- 3、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 4、已获取应用的Application Key(app_key)和Application Secret(app_secret)。请在华为云控制台“应用管理”页面上创建和查看您的Application Key和Application Secret。具体请参见 [应用管理](https://console.huaweicloud.com/msgsms/#/msgSms/applicationManage) 。
- 5、已获得您要发送短信的通道号，由于通道号与签名关联，请在控制台“签名管理”页面上查看您的通道号，如果您还没有通道号，则您需要先提交签名申请，待签名申请通过后，系统会分配通道号。具体请参见[签名管理](https://console.huaweicloud.com/msgsms/#/msgSms/signatureManage)。
- 6、已获取您要发送短信的模板ID，请在控制台“模板管理”页面上创建和查看您的模板ID。具体请参见[模板管理](https://console.huaweicloud.com/msgsms/#/msgSms/modelManage)。

## 4、接口参数说明
关于接口参数的详细说明可参见：

[a.发送短信](https://support.huaweicloud.com/api-msgsms/sms_05_0001.html)

[b.发送分批短信](https://support.huaweicloud.com/api-msgsms/sms_05_0002.html)

## 5、关键代码片段

### 5.1、调用BatchSendSms接口发送短信
```java
/**
 * Example of Invoking the batchSendSms Interface to Send an SMS Message
 * @param ak AK is the appkey of the SMS application.
 * @param sk SK is the appsecret of the SMS application.
 * @throws Exception
 */
private static int sendSms(String ak, String sk) throws Exception {
    // The apiAddress of this example is of Beijing 4. Replace it with the actual value.
    String apiAddress = "https://smsapi.cn-north-4.myhuaweicloud.com:8443/sms/batchSendSms/v1";

    // Construct the message body of the sample code.
    // For details about how to construct a body, see the API description.
    StringBuilder body = new StringBuilder();
    appendToBody(body, "from=", "8824110605***");
    appendToBody(body, "&to=", "+86137****3774");
    appendToBody(body, "&templateId=", "1d18a7f4e1b84f6c8fc1546b48b3baea");
    appendToBody(body, "&templateParas=", "[\"1\",\"2\",\"3\"]");
    appendToBody(body, "&statusCallback=", "https://test/report");
    System.out.println("body:" + body);

    HttpPost postRequest = new HttpPost(apiAddress);
    postRequest.setHeader("Accept", "application/json");
    postRequest.setHeader("Content-Type", "application/x-www-form-urlencoded");
    postRequest.setHeader("User-Agent", "huaweicloud-usdk-java/3.0");
    postRequest.setEntity(new StringEntity(body.toString(), StandardCharsets.UTF_8));

    // Signature operation of the batchsendsms interface
    postRequest = sign(postRequest, body.toString(), ak, sk);
    if (Objects.isNull(postRequest)) {
        System.out.println("sign failed!");
        return HttpStatus.SC_INTERNAL_SERVER_ERROR;
    }

    // Invoke /sms/batchSendSms/v1 api to send messages
    return sendMessage(postRequest);
}

/**
 * Signing the request message.
 * @param postRequest the request
 * @param body Message body to be signed
 * @param ak AK is the appkey of the SMS application.
 * @param sk SK is the appsecret of the SMS application.
 * @return signed request.
 */
private static HttpPost sign(HttpPost postRequest, String body, String ak, String sk) {
    // The SDK signature algorithm is invoked. The original request needs to be converted into an SDK request for signature.
    HttpRequest.HttpRequestBuilder sdkHttpRequestBuilder = HttpRequest.newBuilder().withBodyAsString(body)
            .withMethod(HttpMethod.valueOf(postRequest.getMethod()))
            .withEndpoint(postRequest.getURI().toString());
    for (Header header : postRequest.getAllHeaders()) {
        sdkHttpRequestBuilder.addHeader(header.getName(), header.getValue());
    }

    // The signature algorithm uses the AK and SK signature algorithms provided by HUAWEI CLOUD IAM and API Gateway.
    // Signature algorithm implementation. The capabilities provided by the SDK are used here.
    // Developers can also use the signature capability provided by HUAWEI CLOUD APIG. For details, see the following website: https://support.huaweicloud.com/devg-apisign/api-sign-sdk-java.html
    // For the signature operation of an interface, the signature must contain the body.
    Map<String, String> signedHeaders = SmsAkSkSigner.sign(sdkHttpRequestBuilder.build(), new BasicCredentials().withAk(ak).withSk(sk));
    if (Objects.isNull(signedHeaders)) {
        System.out.println("sign failed!");
        return null;
    }

    signedHeaders.forEach((key, value) -> postRequest.setHeader(key, value));
    return postRequest;
}

/**
 * Send the request message.
 * @param postRequest Message request to be sent.
 * @return The http status code, indicates whether the SMS message is sent successfully.
 * @throws Exception
 */
private static int sendMessage(HttpPost postRequest) throws Exception {
    // Note: The sample code is for reference only. For commercial code, you can use the thread pool technology to increase the number of concurrent requests and solve the concurrency problem.
    if (Objects.isNull(httpClient)) {
        // To prevent API invoking failures caused by HTTPS certificate authentication failures, ignore the certificate trust issue to simplify the sample code.
        // Note: Do not ignore the TLS certificate verification in the commercial version.
        httpClient = createIgnoreSSLHttpClient();
    }

    try (CloseableHttpResponse response = httpClient.execute(postRequest)) {
        // Print the response status code.
        System.out.println("Response Status Code: " + response.getStatusLine().getStatusCode());

        // Print the response content.
        System.out.println("Response Content: " + EntityUtils.toString(response.getEntity()));
        return response.getStatusLine().getStatusCode();
    } catch (IOException e) {
        System.out.println("response exception:" + e);
    }

    return HttpStatus.SC_INTERNAL_SERVER_ERROR;
}

/**
 * Add the parameter to the original body after URL encoding.
 * @param body Message body
 * @param key  Parameter key
 * @param val Parameter value
 * @throws UnsupportedEncodingException
 */
private static void appendToBody(StringBuilder body, String key, String val) throws UnsupportedEncodingException {
    if (null != val && !val.isEmpty()) {
        body.append(key).append(URLEncoder.encode(val, StandardCharsets.UTF_8.name()));
    }
}

/**
 * Create an HTTP client that ignores the HTTPS certificate check.
 * @return HTTP client
 * @throws Exception
 */
private static CloseableHttpClient createIgnoreSSLHttpClient() throws Exception {
    SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null,  (x509CertChain, authType) -> true).build();
    return HttpClients.custom().setSSLSocketFactory(new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE)).build();
}
```

### 5.2、调用BatchSendDiffSms接口发送分批短信
```java
/**
 * Example of Invoking the batchSendDiffSms Interface to Send an SMS Message
 * @param ak AK is the appkey of the SMS application.
 * @param sk SK is the appsecret of the SMS application.
 * @throws Exception
 */
private static int sendSmsUsingDiff(String ak, String sk) throws Exception {
    // The apiAddress of this example is of Beijing 4. Replace it with the actual value.
    String apiAddress = "https://smsapi.cn-north-4.myhuaweicloud.com:8443/sms/batchSendDiffSms/v1";

    /* Construct the message body of the sample code.
       For details about how to construct a body, see the API description.
       Sample is:
        {
            "from": "8824110605***",
            "statusCallback": "https://test/report",
            "smsContent": [{
                    "to": ["+86137****3774", "+86137****3775"],
                    "templateId": "1d18a7f4e1b84f6c8fc1546b48b3baea",
                    "templateParas": ["1", "23", "e"]
                },
                {
                    "to": ["+86137****3777"],
                    "templateId": "7824a51b3cc34976919a5418f637d0fb",
                    "templateParas": ["4", "5", "6"]
                }
            ]
        }
     */
    String body = "{" +
            "    \"from\": \"8824110605***\"," +
            "    \"statusCallback\": \"https://test/report\"," +
            "    \"smsContent\": [{" +
            "        \"to\": [\"+86137****3774\", \"+86137****3775\"]," +
            "        \"templateId\": \"1d18a7f4e1b84f6c8fc1546b48b3baea\"," +
            "        \"templateParas\": [\"1\", \"23\", \"e\"]" +
            "    }," +
            "    {" +
            "        \"to\": [\"+86137****3777\"]," +
            "        \"templateId\": \"7824a51b3cc34976919a5418f637d0fb\"," +
            "        \"templateParas\": [\"4\", \"5\", \"6\"]" +
            "    }]" +
            "}";

    System.out.println("body:" + body);

    // Invoke /sms/batchSendDiffSms/v1 api to send messages
    HttpPost postRequest = new HttpPost(apiAddress);
    postRequest.setHeader("Accept", "application/json");
    postRequest.setHeader("Content-Type", "application/json;charset=utf8");
    postRequest.setHeader("User-Agent", "huaweicloud-usdk-java/3.0");
    postRequest.setEntity(new StringEntity(body, StandardCharsets.UTF_8));

    // Signature operation of the batchsenddiffsms interface
    postRequest = sign(postRequest, body, ak, sk);
    if (Objects.isNull(postRequest)) {
        System.out.println("sign failed!");
        return HttpStatus.SC_INTERNAL_SERVER_ERROR;
    }

    return sendMessage(postRequest);
}
```

## 6、运行结果
**发送短信（BatchSendSms）**
```json
{
  "result": [{
    "total": 1,
    "originTo": "+86137****3774",
    "createTime": "2024-11-30T06:50:12Z",
    "from": "8824110605***",
    "smsMsgId": "a0e98318-ae64-47a8-bc75-78116ba6184b_226478999",
    "countryId": "CN",
    "status": "000000"
  }],
  "code": "000000",
  "description": "Success"
}
```

**发送分批短信（BatchSendDiffSms）**
```json
{
  "result": [{
    "total": 1,
    "originTo": "+86137****3774",
    "createTime": "2024-11-30T07:14:51Z",
    "from": "8824110605***",
    "smsMsgId": "a0e98318-ae64-47a8-bc75-78116ba6184b_226774103",
    "countryId": "CN",
    "status": "000000"
  }, {
    "total": 1,
    "originTo": "+86137****3775",
    "createTime": "2024-11-30T07:14:51Z",
    "from": "8824110605***",
    "smsMsgId": "a0e98318-ae64-47a8-bc75-78116ba6184b_226774104",
    "countryId": "CN",
    "status": "000000"
  }, {
    "total": 1,
    "originTo": "+86137****3777",
    "createTime": "2024-11-30T07:14:51Z",
    "from": "8824110605***",
    "smsMsgId": "a0e98318-ae64-47a8-bc75-78116ba6184b_226774105",
    "countryId": "CN",
    "status": "000000"
  }],
  "code": "000000",
  "description": "Success"
}

```

## 7、参考
本示例的代码工程仅用于简单演示，实际开发过程中应严格遵循开发指南。访问以下链接可以获取详细信息：[开发指南](https://support.huaweicloud.com/devg-msgsms/sms_04_0000.html)