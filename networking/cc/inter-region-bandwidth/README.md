### 示例简介
域间带宽表示的是跨区域之间的互通限速值，购买带宽包之后，需要配置连通区域之间的域间带宽。

本示例展示如何使用域间带宽（InterRegionBandwidth）相关SDK，主要介绍域间带宽的增、删、改、查功能。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.创建一个云连接实例。

6.已经购买了带宽包，并且将带宽包绑定到云连接上面。

### SDK获取和安装
您可以通过Maven配置所依赖的虚拟私有云服务SDK

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cc</artifactId>
    <version>3.1.53</version>
</dependency>
```

### 代码示例
以下代码展示如何使用域间带宽（CloudConnection）相关SDK
```java
public class InterRegionBandwidthDemo {

    private static final Logger logger = LoggerFactory.getLogger(InterRegionBandwidthDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);
        //创建CcClient实例
        CcClient client = CcClient.newBuilder()
            .withCredential(auth)
            .withRegion(CcRegion.CN_NORTH_4)
            .build();

        // 1,创建域间带宽
        CreateInterRegionBandwidthResponse response = createInterRegionBandwidth(client);

        // 2,查询域间带宽详情
        showInterRegionBandwidth(client, response.getInterRegionBandwidth().getId());

        // 3,修改域间带宽
        updateInterRegionBandwidth(client, response.getInterRegionBandwidth().getId());

        // 4,查询域间带宽列表
        listInterRegionBandwidths(client, response.getInterRegionBandwidth().getId());

        // 5,删除域间带宽
        deleteInterRegionBandwidth(client, response.getInterRegionBandwidth().getId());
    }

    private static CreateInterRegionBandwidthResponse createInterRegionBandwidth(CcClient client) {
        CreateInterRegionBandwidthRequest request = new CreateInterRegionBandwidthRequest()
            .withBody(new CreateInterRegionBandwidthRequestBody()
                .withInterRegionBandwidth(new CreateInterRegionBandwidth()
                    .withCloudConnectionId("<your cloud connection id>")
                    .withBandwidthPackageId("<your bandwidth package id>")
                    .withBandwidth(10)
                    .withInterRegionIds(Arrays.asList("cn-north-4", "cn-north-1"))));
        CreateInterRegionBandwidthResponse response = null;
        try {
            response = client.createInterRegionBandwidth(request);
            logger.info("createInterRegionBandwidth" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createInterRegionBandwidth ClientRequestException" + e.getHttpStatusCode());
            logger.error("createInterRegionBandwidth ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("createInterRegionBandwidth ServerResponseException" + e.getHttpStatusCode());
            logger.error("createInterRegionBandwidth ServerResponseException" + e.getMessage());
        }
        return response;
    }

    private static void showInterRegionBandwidth(CcClient client, String rbwId) {
        ShowInterRegionBandwidthRequest request = new ShowInterRegionBandwidthRequest().withId(rbwId);
        try {
            ShowInterRegionBandwidthResponse response = client.showInterRegionBandwidth(request);
            logger.info("showInterRegionBandwidth" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showInterRegionBandwidth ClientRequestException" + e.getHttpStatusCode());
            logger.error("showInterRegionBandwidth ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showInterRegionBandwidth ServerResponseException" + e.getHttpStatusCode());
            logger.error("showInterRegionBandwidth ServerResponseException" + e.getMessage());
        }
    }

    private static void updateInterRegionBandwidth(CcClient client, String rbwId) {
        UpdateInterRegionBandwidthRequest request = new UpdateInterRegionBandwidthRequest()
            .withId(rbwId)
            .withBody(new UpdateInterRegionBandwidthRequestBody()
                .withInterRegionBandwidth(new UpdateInterRegionBandwidth()
                    .withBandwidth(20)));
        try {
            UpdateInterRegionBandwidthResponse response = client.updateInterRegionBandwidth(request);
            logger.info("updateInterRegionBandwidth" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("updateInterRegionBandwidth ClientRequestException" + e.getHttpStatusCode());
            logger.error("updateInterRegionBandwidth ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("updateInterRegionBandwidth ServerResponseException" + e.getHttpStatusCode());
            logger.error("updateInterRegionBandwidth ServerResponseException" + e.getMessage());
        }
    }

    private static void listInterRegionBandwidths(CcClient client, String rbwId) {
        ListInterRegionBandwidthsRequest request = new ListInterRegionBandwidthsRequest().withId(Arrays.asList(rbwId));
        try {
            ListInterRegionBandwidthsResponse response = client.listInterRegionBandwidths(request);
            logger.info("listInterRegionBandwidths" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listInterRegionBandwidths ClientRequestException" + e.getHttpStatusCode());
            logger.error("listInterRegionBandwidths ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("listInterRegionBandwidths ServerResponseException" + e.getHttpStatusCode());
            logger.error("listInterRegionBandwidths ServerResponseException" + e.getMessage());
        }
    }

    private static void deleteInterRegionBandwidth(CcClient client, String rbwId) {
        DeleteInterRegionBandwidthRequest request = new DeleteInterRegionBandwidthRequest().withId(rbwId);
        try {
            DeleteInterRegionBandwidthResponse response = client.deleteInterRegionBandwidth(request);
            logger.info("deleteInterRegionBandwidth" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("deleteInterRegionBandwidth ClientRequestException" + e.getHttpStatusCode());
            logger.error("deleteInterRegionBandwidth ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("deleteInterRegionBandwidth ServerResponseException" + e.getHttpStatusCode());
            logger.error("deleteInterRegionBandwidth ServerResponseException" + e.getMessage());
        }
    }
}
```
您可以在 [云连接CC服务文档](https://support.huaweicloud.com/cc/index.html) 和[API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=CC&api=CreateInterRegionBandwidth) 查看具体信息。

### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: | :-----: | :------:  
 2023/09/11 |1.0 | 文档首次发布
