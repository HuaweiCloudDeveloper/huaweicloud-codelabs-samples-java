package com.huawei.cc.demo;

import com.huaweicloud.sdk.cc.v3.CcClient;
import com.huaweicloud.sdk.cc.v3.model.CreateInterRegionBandwidth;
import com.huaweicloud.sdk.cc.v3.model.CreateInterRegionBandwidthRequest;
import com.huaweicloud.sdk.cc.v3.model.CreateInterRegionBandwidthRequestBody;
import com.huaweicloud.sdk.cc.v3.model.CreateInterRegionBandwidthResponse;
import com.huaweicloud.sdk.cc.v3.model.DeleteInterRegionBandwidthRequest;
import com.huaweicloud.sdk.cc.v3.model.DeleteInterRegionBandwidthResponse;
import com.huaweicloud.sdk.cc.v3.model.ListInterRegionBandwidthsRequest;
import com.huaweicloud.sdk.cc.v3.model.ListInterRegionBandwidthsResponse;
import com.huaweicloud.sdk.cc.v3.model.ShowInterRegionBandwidthRequest;
import com.huaweicloud.sdk.cc.v3.model.ShowInterRegionBandwidthResponse;
import com.huaweicloud.sdk.cc.v3.model.UpdateInterRegionBandwidth;
import com.huaweicloud.sdk.cc.v3.model.UpdateInterRegionBandwidthRequest;
import com.huaweicloud.sdk.cc.v3.model.UpdateInterRegionBandwidthRequestBody;
import com.huaweicloud.sdk.cc.v3.model.UpdateInterRegionBandwidthResponse;
import com.huaweicloud.sdk.cc.v3.region.CcRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class InterRegionBandwidthDemo {

    private static final Logger logger = LoggerFactory.getLogger(InterRegionBandwidthDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);
        //创建CcClient实例
        CcClient client = CcClient.newBuilder()
            .withCredential(auth)
            .withRegion(CcRegion.CN_NORTH_4)
            .build();

        // 1,创建域间带宽
        CreateInterRegionBandwidthResponse response = createInterRegionBandwidth(client);

        // 2,查询域间带宽详情
        showInterRegionBandwidth(client, response.getInterRegionBandwidth().getId());

        // 3,修改域间带宽
        updateInterRegionBandwidth(client, response.getInterRegionBandwidth().getId());

        // 4,查询域间带宽列表
        listInterRegionBandwidths(client, response.getInterRegionBandwidth().getId());

        // 5,删除域间带宽
        deleteInterRegionBandwidth(client, response.getInterRegionBandwidth().getId());
    }

    private static CreateInterRegionBandwidthResponse createInterRegionBandwidth(CcClient client) {
        CreateInterRegionBandwidthRequest request = new CreateInterRegionBandwidthRequest()
            .withBody(new CreateInterRegionBandwidthRequestBody()
                .withInterRegionBandwidth(new CreateInterRegionBandwidth()
                    .withCloudConnectionId("<your cloud connection id>")
                    .withBandwidthPackageId("<your bandwidth package id>")
                    .withBandwidth(10)
                    .withInterRegionIds(Arrays.asList("cn-north-4", "cn-north-1"))));
        CreateInterRegionBandwidthResponse response = null;
        try {
            response = client.createInterRegionBandwidth(request);
            logger.info("createInterRegionBandwidth" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createInterRegionBandwidth ClientRequestException" + e.getHttpStatusCode());
            logger.error("createInterRegionBandwidth ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("createInterRegionBandwidth ServerResponseException" + e.getHttpStatusCode());
            logger.error("createInterRegionBandwidth ServerResponseException" + e.getMessage());
        }
        return response;
    }

    private static void showInterRegionBandwidth(CcClient client, String rbwId) {
        ShowInterRegionBandwidthRequest request = new ShowInterRegionBandwidthRequest().withId(rbwId);
        try {
            ShowInterRegionBandwidthResponse response = client.showInterRegionBandwidth(request);
            logger.info("showInterRegionBandwidth" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showInterRegionBandwidth ClientRequestException" + e.getHttpStatusCode());
            logger.error("showInterRegionBandwidth ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showInterRegionBandwidth ServerResponseException" + e.getHttpStatusCode());
            logger.error("showInterRegionBandwidth ServerResponseException" + e.getMessage());
        }
    }

    private static void updateInterRegionBandwidth(CcClient client, String rbwId) {
        UpdateInterRegionBandwidthRequest request = new UpdateInterRegionBandwidthRequest()
            .withId(rbwId)
            .withBody(new UpdateInterRegionBandwidthRequestBody()
                .withInterRegionBandwidth(new UpdateInterRegionBandwidth()
                    .withBandwidth(20)));
        try {
            UpdateInterRegionBandwidthResponse response = client.updateInterRegionBandwidth(request);
            logger.info("updateInterRegionBandwidth" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("updateInterRegionBandwidth ClientRequestException" + e.getHttpStatusCode());
            logger.error("updateInterRegionBandwidth ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("updateInterRegionBandwidth ServerResponseException" + e.getHttpStatusCode());
            logger.error("updateInterRegionBandwidth ServerResponseException" + e.getMessage());
        }
    }

    private static void listInterRegionBandwidths(CcClient client, String rbwId) {
        ListInterRegionBandwidthsRequest request = new ListInterRegionBandwidthsRequest().withId(Arrays.asList(rbwId));
        try {
            ListInterRegionBandwidthsResponse response = client.listInterRegionBandwidths(request);
            logger.info("listInterRegionBandwidths" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listInterRegionBandwidths ClientRequestException" + e.getHttpStatusCode());
            logger.error("listInterRegionBandwidths ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("listInterRegionBandwidths ServerResponseException" + e.getHttpStatusCode());
            logger.error("listInterRegionBandwidths ServerResponseException" + e.getMessage());
        }
    }

    private static void deleteInterRegionBandwidth(CcClient client, String rbwId) {
        DeleteInterRegionBandwidthRequest request = new DeleteInterRegionBandwidthRequest().withId(rbwId);
        try {
            DeleteInterRegionBandwidthResponse response = client.deleteInterRegionBandwidth(request);
            logger.info("deleteInterRegionBandwidth" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("deleteInterRegionBandwidth ClientRequestException" + e.getHttpStatusCode());
            logger.error("deleteInterRegionBandwidth ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("deleteInterRegionBandwidth ServerResponseException" + e.getHttpStatusCode());
            logger.error("deleteInterRegionBandwidth ServerResponseException" + e.getMessage());
        }
    }
}