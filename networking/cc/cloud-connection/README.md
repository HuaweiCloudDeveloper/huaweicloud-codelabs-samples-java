### 示例简介
云连接能够提供能够提供一种快速构建跨区域VPC及云上多VPC与云下多数据中心之间的高速、优质、稳定的网络能力，帮助用户打造一张具有企业级规模和通信能力的全球云上网络。

本示例展示如何使用云连接（CloudConnection）相关SDK，主要介绍云连接的增、删、改、查功能。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

### SDK获取和安装
您可以通过Maven配置所依赖的虚拟私有云服务SDK

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cc</artifactId>
    <version>3.1.53</version>
</dependency>
```
### 代码示例
以下代码展示如何使用云连接（CloudConnection）相关SDK
``` java
import com.huaweicloud.sdk.cc.v3.CcClient;
import com.huaweicloud.sdk.cc.v3.model.CreateCloudConnection;
import com.huaweicloud.sdk.cc.v3.model.CreateCloudConnectionRequest;
import com.huaweicloud.sdk.cc.v3.model.CreateCloudConnectionRequestBody;
import com.huaweicloud.sdk.cc.v3.model.CreateCloudConnectionResponse;
import com.huaweicloud.sdk.cc.v3.model.DeleteCloudConnectionRequest;
import com.huaweicloud.sdk.cc.v3.model.DeleteCloudConnectionResponse;
import com.huaweicloud.sdk.cc.v3.model.ListCloudConnectionsRequest;
import com.huaweicloud.sdk.cc.v3.model.ListCloudConnectionsResponse;
import com.huaweicloud.sdk.cc.v3.model.ShowCloudConnectionRequest;
import com.huaweicloud.sdk.cc.v3.model.ShowCloudConnectionResponse;
import com.huaweicloud.sdk.cc.v3.model.UpdateCloudConnection;
import com.huaweicloud.sdk.cc.v3.model.UpdateCloudConnectionRequest;
import com.huaweicloud.sdk.cc.v3.model.UpdateCloudConnectionRequestBody;
import com.huaweicloud.sdk.cc.v3.model.UpdateCloudConnectionResponse;
import com.huaweicloud.sdk.cc.v3.region.CcRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class CloudConnectionDemo {

    private static final Logger logger = LoggerFactory.getLogger(CloudConnectionDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);
        //创建CcClient实例
        CcClient client = CcClient.newBuilder()
            .withCredential(auth)
            .withRegion(CcRegion.CN_NORTH_4)
            .build();

        // 1,创建云连接
        CreateCloudConnectionResponse response = createCloudConnection(client);

        // 2,查询云连接详情
        showCloudConnection(client, response.getCloudConnection().getId());

        // 3,修改云连接
        updateCloudConnection(client, response.getCloudConnection().getId());

        // 4,查询云连接列表
        listCloudConnections(client, response.getCloudConnection().getId());

        // 5,删除云连接
        deleteCloudConnection(client, response.getCloudConnection().getId());
    }

    private static CreateCloudConnectionResponse createCloudConnection(CcClient client) {
        CreateCloudConnectionRequest request = new CreateCloudConnectionRequest()
            .withBody(new CreateCloudConnectionRequestBody()
                .withCloudConnection(new CreateCloudConnection()
                    .withName("<your cloud-connection name>")
                    .withDescription("<your cloud-connection description>")
                    .withEnterpriseProjectId("<your enterprise project id>")));
        CreateCloudConnectionResponse response = null;
        try {
            response = client.createCloudConnection(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
        return response;
    }

    private static void showCloudConnection(CcClient client, String ccId) {
        ShowCloudConnectionRequest request = new ShowCloudConnectionRequest().withId(ccId);
        try {
            ShowCloudConnectionResponse response = client.showCloudConnection(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void updateCloudConnection(CcClient client, String ccId) {
        UpdateCloudConnectionRequest request = new UpdateCloudConnectionRequest()
            .withId(ccId)
            .withBody(new UpdateCloudConnectionRequestBody().withCloudConnection(new UpdateCloudConnection()
                .withName("<your new cloud-connection name>")
                .withDescription("<your new cloud-connection description>")));
        try {
            UpdateCloudConnectionResponse response = client.updateCloudConnection(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void listCloudConnections(CcClient client, String ccId) {
        ListCloudConnectionsRequest request = new ListCloudConnectionsRequest().withId(Arrays.asList(ccId));
        try {
            ListCloudConnectionsResponse response = client.listCloudConnections(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void deleteCloudConnection(CcClient client, String ccId) {
        DeleteCloudConnectionRequest request = new DeleteCloudConnectionRequest().withId(ccId);
        try {
            DeleteCloudConnectionResponse response = client.deleteCloudConnection(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }
}
```
您可以在 [云连接CC服务文档](https://support.huaweicloud.com/cc/index.html) 和[API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=CC&api=CreateCloudConnection) 查看具体信息。

### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: | :-----: | :------:  
 2023/09/11 |1.0 | 文档首次发布
