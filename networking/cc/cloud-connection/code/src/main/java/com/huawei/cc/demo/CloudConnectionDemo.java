package com.huawei.cc.demo;

import com.huaweicloud.sdk.cc.v3.CcClient;
import com.huaweicloud.sdk.cc.v3.model.CreateCloudConnection;
import com.huaweicloud.sdk.cc.v3.model.CreateCloudConnectionRequest;
import com.huaweicloud.sdk.cc.v3.model.CreateCloudConnectionRequestBody;
import com.huaweicloud.sdk.cc.v3.model.CreateCloudConnectionResponse;
import com.huaweicloud.sdk.cc.v3.model.DeleteCloudConnectionRequest;
import com.huaweicloud.sdk.cc.v3.model.DeleteCloudConnectionResponse;
import com.huaweicloud.sdk.cc.v3.model.ListCloudConnectionsRequest;
import com.huaweicloud.sdk.cc.v3.model.ListCloudConnectionsResponse;
import com.huaweicloud.sdk.cc.v3.model.ShowCloudConnectionRequest;
import com.huaweicloud.sdk.cc.v3.model.ShowCloudConnectionResponse;
import com.huaweicloud.sdk.cc.v3.model.UpdateCloudConnection;
import com.huaweicloud.sdk.cc.v3.model.UpdateCloudConnectionRequest;
import com.huaweicloud.sdk.cc.v3.model.UpdateCloudConnectionRequestBody;
import com.huaweicloud.sdk.cc.v3.model.UpdateCloudConnectionResponse;
import com.huaweicloud.sdk.cc.v3.region.CcRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class CloudConnectionDemo {

    private static final Logger logger = LoggerFactory.getLogger(CloudConnectionDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);
        //创建CcClient实例
        CcClient client = CcClient.newBuilder()
            .withCredential(auth)
            .withRegion(CcRegion.CN_NORTH_4)
            .build();

        // 1,创建云连接
        CreateCloudConnectionResponse response = createCloudConnection(client);

        // 2,查询云连接详情
        showCloudConnection(client, response.getCloudConnection().getId());

        // 3,修改云连接
        updateCloudConnection(client, response.getCloudConnection().getId());

        // 4,查询云连接列表
        listCloudConnections(client, response.getCloudConnection().getId());

        // 5,删除云连接
        deleteCloudConnection(client, response.getCloudConnection().getId());
    }

    private static CreateCloudConnectionResponse createCloudConnection(CcClient client) {
        CreateCloudConnectionRequest request = new CreateCloudConnectionRequest()
            .withBody(new CreateCloudConnectionRequestBody()
                .withCloudConnection(new CreateCloudConnection()
                    .withName("<your cloud-connection name>")
                    .withDescription("<your cloud-connection description>")
                    .withEnterpriseProjectId("<your enterprise project id>")));
        CreateCloudConnectionResponse response = null;
        try {
            response = client.createCloudConnection(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
        return response;
    }

    private static void showCloudConnection(CcClient client, String ccId) {
        ShowCloudConnectionRequest request = new ShowCloudConnectionRequest().withId(ccId);
        try {
            ShowCloudConnectionResponse response = client.showCloudConnection(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void updateCloudConnection(CcClient client, String ccId) {
        UpdateCloudConnectionRequest request = new UpdateCloudConnectionRequest()
            .withId(ccId)
            .withBody(new UpdateCloudConnectionRequestBody().withCloudConnection(new UpdateCloudConnection()
                .withName("<your new cloud-connection name>")
                .withDescription("<your new cloud-connection description>")));
        try {
            UpdateCloudConnectionResponse response = client.updateCloudConnection(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void listCloudConnections(CcClient client, String ccId) {
        ListCloudConnectionsRequest request = new ListCloudConnectionsRequest().withId(Arrays.asList(ccId));
        try {
            ListCloudConnectionsResponse response = client.listCloudConnections(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void deleteCloudConnection(CcClient client, String ccId) {
        DeleteCloudConnectionRequest request = new DeleteCloudConnectionRequest().withId(ccId);
        try {
            DeleteCloudConnectionResponse response = client.deleteCloudConnection(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }
}