### 示例简介
如果涉及跨帐号的VPC互联，需要跨帐号授权网络实例，加载跨帐号网络实例。

本示例展示如何使用授权管理（Authorisation）相关SDK，主要介绍授权管理的增、删、改、查功能。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.本账号下准备一个VPC，记录VPC ID和区域ID。

6.对方账号下准备一个云连接实例。

### SDK获取和安装
您可以通过Maven配置所依赖的虚拟私有云服务SDK

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cc</artifactId>
    <version>3.1.53</version>
</dependency>
```

### 代码示例
以下代码展示如何使用授权管理（Authorisation）相关SDK
```java
public class AuthorisationDemo {

    private static final Logger logger = LoggerFactory.getLogger(AuthorisationDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);
        //创建CcClient实例
        CcClient client = CcClient.newBuilder()
            .withCredential(auth)
            .withRegion(CcRegion.CN_NORTH_4)
            .build();

        // 1,创建授权
        CreateAuthorisationResponse response = createAuthorisation(client);

        // 2,查询授权列表
        listAuthorisations(client, response.getAuthorisation().getId());

        // 3,修改授权
        updateAuthorisation(client, response.getAuthorisation().getId());

        // 4,删除授权
        deleteAuthorisation(client, response.getAuthorisation().getId());

        // 5,查询权限列表
        listPermissions(client);
    }

    private static CreateAuthorisationResponse createAuthorisation(CcClient client) {
        CreateAuthorisationRequest request = new CreateAuthorisationRequest()
            .withBody(new CreateAuthorisationRequestBody()
                .withAuthorisation(new CreateAuthorisation()
                    .withInstanceId("<your vpc id>")
                    .withInstanceType(CreateAuthorisation.InstanceTypeEnum.VPC)
                    .withProjectId("<your project id>")
                    .withRegionId("cn-north-4")
                    .withName("<your authorisation name>")
                    .withDescription("<your authorisation description>")
                    .withCloudConnectionId("<the cloud connection id>")
                    .withCloudConnectionDomainId("<the domain id to which the cloud connection belongs>")));
        CreateAuthorisationResponse response = null;
        try {
            response = client.createAuthorisation(request);
            logger.info("createAuthorisation" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createAuthorisation ClientRequestException" + e.getHttpStatusCode());
            logger.error("createAuthorisation ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("createAuthorisation ServerResponseException" + e.getHttpStatusCode());
            logger.error("createAuthorisation ServerResponseException" + e.getMessage());
        }
        return response;
    }

    private static void listAuthorisations(CcClient client, String authorisationId) {
        ListAuthorisationsRequest request = new ListAuthorisationsRequest().withId(Arrays.asList(authorisationId));
        try {
            ListAuthorisationsResponse response = client.listAuthorisations(request);
            logger.info("listAuthorisations" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listAuthorisations ClientRequestException" + e.getHttpStatusCode());
            logger.error("listAuthorisations ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("listAuthorisations ServerResponseException" + e.getHttpStatusCode());
            logger.error("listAuthorisations ServerResponseException" + e.getMessage());
        }
    }

    private static void updateAuthorisation(CcClient client, String authorisationId) {
        UpdateAuthorisationRequest request = new UpdateAuthorisationRequest()
            .withId(authorisationId)
            .withBody(new UpdateAuthorisationRequestBody()
                .withAuthorisation(new UpdateAuthorisation()
                    .withName("<your new authorisation name>")
                    .withDescription("<your new authorisation description>")));
        try {
            UpdateAuthorisationResponse response = client.updateAuthorisation(request);
            logger.info("updateAuthorisation" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("updateAuthorisation ClientRequestException" + e.getHttpStatusCode());
            logger.error("updateAuthorisation ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("updateAuthorisation ServerResponseException" + e.getHttpStatusCode());
            logger.error("updateAuthorisation ServerResponseException" + e.getMessage());
        }
    }

    private static void deleteAuthorisation(CcClient client, String authorisationId) {
        DeleteAuthorisationRequest request = new DeleteAuthorisationRequest().withId(authorisationId);
        try {
            DeleteAuthorisationResponse response = client.deleteAuthorisation(request);
            logger.info("deleteAuthorisation" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("deleteAuthorisation ClientRequestException" + e.getHttpStatusCode());
            logger.error("deleteAuthorisation ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("deleteAuthorisation ServerResponseException" + e.getHttpStatusCode());
            logger.error("deleteAuthorisation ServerResponseException" + e.getMessage());
        }
    }

    private static void listPermissions(CcClient client) {
        ListPermissionsRequest request = new ListPermissionsRequest();
        try {
            ListPermissionsResponse response = client.listPermissions(request);
            logger.info("listPermissions" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listPermissions ClientRequestException" + e.getHttpStatusCode());
            logger.error("listPermissions ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("listPermissions ServerResponseException" + e.getHttpStatusCode());
            logger.error("listPermissions ServerResponseException" + e.getMessage());
        }
    }
}
```
您可以在 [云连接CC服务文档](https://support.huaweicloud.com/cc/index.html) 和[API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=CC&api=CreateAuthorisation) 查看具体信息。

### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: | :-----: | :------:  
 2023/09/11 |1.0 | 文档首次发布
