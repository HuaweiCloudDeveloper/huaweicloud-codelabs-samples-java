package com.huawei.cc.demo;

import com.huaweicloud.sdk.cc.v3.CcClient;
import com.huaweicloud.sdk.cc.v3.model.CreateAuthorisation;
import com.huaweicloud.sdk.cc.v3.model.CreateAuthorisationRequest;
import com.huaweicloud.sdk.cc.v3.model.CreateAuthorisationRequestBody;
import com.huaweicloud.sdk.cc.v3.model.CreateAuthorisationResponse;
import com.huaweicloud.sdk.cc.v3.model.DeleteAuthorisationRequest;
import com.huaweicloud.sdk.cc.v3.model.DeleteAuthorisationResponse;
import com.huaweicloud.sdk.cc.v3.model.ListAuthorisationsRequest;
import com.huaweicloud.sdk.cc.v3.model.ListAuthorisationsResponse;
import com.huaweicloud.sdk.cc.v3.model.ListPermissionsRequest;
import com.huaweicloud.sdk.cc.v3.model.ListPermissionsResponse;
import com.huaweicloud.sdk.cc.v3.model.UpdateAuthorisation;
import com.huaweicloud.sdk.cc.v3.model.UpdateAuthorisationRequest;
import com.huaweicloud.sdk.cc.v3.model.UpdateAuthorisationRequestBody;
import com.huaweicloud.sdk.cc.v3.model.UpdateAuthorisationResponse;
import com.huaweicloud.sdk.cc.v3.region.CcRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class AuthorisationDemo {

    private static final Logger logger = LoggerFactory.getLogger(AuthorisationDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);
        //创建CcClient实例
        CcClient client = CcClient.newBuilder()
            .withCredential(auth)
            .withRegion(CcRegion.CN_NORTH_4)
            .build();

        // 1,创建授权
        CreateAuthorisationResponse response = createAuthorisation(client);

        // 2,查询授权列表
        listAuthorisations(client, response.getAuthorisation().getId());

        // 3,修改授权
        updateAuthorisation(client, response.getAuthorisation().getId());

        // 4,删除授权
        deleteAuthorisation(client, response.getAuthorisation().getId());

        // 5,查询权限列表
        listPermissions(client);
    }

    private static CreateAuthorisationResponse createAuthorisation(CcClient client) {
        CreateAuthorisationRequest request = new CreateAuthorisationRequest()
            .withBody(new CreateAuthorisationRequestBody()
                .withAuthorisation(new CreateAuthorisation()
                    .withInstanceId("<your vpc id>")
                    .withInstanceType(CreateAuthorisation.InstanceTypeEnum.VPC)
                    .withProjectId("<your project id>")
                    .withRegionId("cn-north-4")
                    .withName("<your authorisation name>")
                    .withDescription("<your authorisation description>")
                    .withCloudConnectionId("<the cloud connection id>")
                    .withCloudConnectionDomainId("<the domain id to which the cloud connection belongs>")));
        CreateAuthorisationResponse response = null;
        try {
            response = client.createAuthorisation(request);
            logger.info("createAuthorisation" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createAuthorisation ClientRequestException" + e.getHttpStatusCode());
            logger.error("createAuthorisation ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("createAuthorisation ServerResponseException" + e.getHttpStatusCode());
            logger.error("createAuthorisation ServerResponseException" + e.getMessage());
        }
        return response;
    }

    private static void listAuthorisations(CcClient client, String authorisationId) {
        ListAuthorisationsRequest request = new ListAuthorisationsRequest().withId(Arrays.asList(authorisationId));
        try {
            ListAuthorisationsResponse response = client.listAuthorisations(request);
            logger.info("listAuthorisations" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listAuthorisations ClientRequestException" + e.getHttpStatusCode());
            logger.error("listAuthorisations ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("listAuthorisations ServerResponseException" + e.getHttpStatusCode());
            logger.error("listAuthorisations ServerResponseException" + e.getMessage());
        }
    }

    private static void updateAuthorisation(CcClient client, String authorisationId) {
        UpdateAuthorisationRequest request = new UpdateAuthorisationRequest()
            .withId(authorisationId)
            .withBody(new UpdateAuthorisationRequestBody()
                .withAuthorisation(new UpdateAuthorisation()
                    .withName("<your new authorisation name>")
                    .withDescription("<your new authorisation description>")));
        try {
            UpdateAuthorisationResponse response = client.updateAuthorisation(request);
            logger.info("updateAuthorisation" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("updateAuthorisation ClientRequestException" + e.getHttpStatusCode());
            logger.error("updateAuthorisation ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("updateAuthorisation ServerResponseException" + e.getHttpStatusCode());
            logger.error("updateAuthorisation ServerResponseException" + e.getMessage());
        }
    }

    private static void deleteAuthorisation(CcClient client, String authorisationId) {
        DeleteAuthorisationRequest request = new DeleteAuthorisationRequest().withId(authorisationId);
        try {
            DeleteAuthorisationResponse response = client.deleteAuthorisation(request);
            logger.info("deleteAuthorisation" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("deleteAuthorisation ClientRequestException" + e.getHttpStatusCode());
            logger.error("deleteAuthorisation ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("deleteAuthorisation ServerResponseException" + e.getHttpStatusCode());
            logger.error("deleteAuthorisation ServerResponseException" + e.getMessage());
        }
    }

    private static void listPermissions(CcClient client) {
        ListPermissionsRequest request = new ListPermissionsRequest();
        try {
            ListPermissionsResponse response = client.listPermissions(request);
            logger.info("listPermissions" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listPermissions ClientRequestException" + e.getHttpStatusCode());
            logger.error("listPermissions ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("listPermissions ServerResponseException" + e.getHttpStatusCode());
            logger.error("listPermissions ServerResponseException" + e.getMessage());
        }
    }
}