### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CC/doc?version=v3&api=ListSiteNetworkQuotas).

2.In this example, you can query the branch network quota.

3.Based on the returned result, you can obtain the quota list of the branch network, including the quota size and used quota of each quota type.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth).

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account.
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console.
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-cc. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cc</artifactId>
    <version>3.1.125</version>
</dependency>
```

### Code example
The following code shows how to query the branch network quota:
``` java
package com.huawei.cc;

import com.huaweicloud.sdk.cc.v3.CcClient;
import com.huaweicloud.sdk.cc.v3.model.ListSiteNetworkQuotasRequest;
import com.huaweicloud.sdk.cc.v3.model.ListSiteNetworkQuotasResponse;
import com.huaweicloud.sdk.cc.v3.region.CcRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListSiteNetworkQuotas {
    private static final Logger logger = LoggerFactory.getLogger(ListSiteNetworkQuotas.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential listSiteNetworkQuotasAuth = new GlobalCredentials().withAk(ak).withSk(sk);
        // Create a CcClient instance.
        CcClient client = CcClient.newBuilder()
            .withCredential(listSiteNetworkQuotasAuth)
            .withRegion(CcRegion.CN_NORTH_4)
            .build();
        // Construction request
        ListSiteNetworkQuotasRequest request = new ListSiteNetworkQuotasRequest();
        try {
            // Obtaining Results
            ListSiteNetworkQuotasResponse response = client.listSiteNetworkQuotas(request);
            // Print Results
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
#### ListSiteNetworkQuotas
```
{
  "request_id" : "XXX",
  "quotas" : [ {
    "quota_key" : "site_networks_per_account",
    "quota_limit" : 6,
    "used" : 0,
    "unit" : "count"
  } ]
}
```
### Change History

 Released On  |  Issue   | Description
 :----: |:---:| :------:  
 2024/12/10 | 1.0 | This document is released for the first time.