package com.huawei.cc;

import com.huaweicloud.sdk.cc.v3.CcClient;
import com.huaweicloud.sdk.cc.v3.model.ListCentralNetworkQuotasRequest;
import com.huaweicloud.sdk.cc.v3.model.ListCentralNetworkQuotasResponse;
import com.huaweicloud.sdk.cc.v3.region.CcRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListCentralNetworkQuotas {
    private static final Logger logger = LoggerFactory.getLogger(ListCentralNetworkQuotas.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential listCentralNetworkQuotasAuth = new GlobalCredentials().withAk(ak).withSk(sk);
        // Create a CcClient instance.
        CcClient client = CcClient.newBuilder()
            .withCredential(listCentralNetworkQuotasAuth)
            .withRegion(CcRegion.CN_NORTH_4)
            .build();
        // Construction request
        ListCentralNetworkQuotasRequest request = new ListCentralNetworkQuotasRequest();
        try {
            // Obtaining Results
            ListCentralNetworkQuotasResponse response = client.listCentralNetworkQuotas(request);
            // Print Results
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}