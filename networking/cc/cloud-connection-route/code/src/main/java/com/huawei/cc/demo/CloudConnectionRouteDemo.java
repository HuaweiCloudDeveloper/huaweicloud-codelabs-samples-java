package com.huawei.cc.demo;

import com.huaweicloud.sdk.cc.v3.CcClient;
import com.huaweicloud.sdk.cc.v3.model.ListCloudConnectionRoutesRequest;
import com.huaweicloud.sdk.cc.v3.model.ListCloudConnectionRoutesResponse;
import com.huaweicloud.sdk.cc.v3.model.ShowCloudConnectionRoutesRequest;
import com.huaweicloud.sdk.cc.v3.model.ShowCloudConnectionRoutesResponse;
import com.huaweicloud.sdk.cc.v3.region.CcRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CloudConnectionRouteDemo {

    private static final Logger logger = LoggerFactory.getLogger(CloudConnectionRouteDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);
        //创建CcClient实例
        CcClient client = CcClient.newBuilder()
            .withCredential(auth)
            .withRegion(CcRegion.CN_NORTH_4)
            .build();

        // 1,查询云连接路由列表
        ListCloudConnectionRoutesResponse response = listCloudConnectionRoutes(client);

        // 2,查询云连接路由详情
        showCloudConnectionRoutes(client, response.getCloudConnectionRoutes().get(0).getId());
    }

    private static ListCloudConnectionRoutesResponse listCloudConnectionRoutes(CcClient client) {
        ListCloudConnectionRoutesRequest request = new ListCloudConnectionRoutesRequest().withLimit(1);
        ListCloudConnectionRoutesResponse response = null;
        try {
            response = client.listCloudConnectionRoutes(request);
            logger.info("listCloudConnectionRoutes" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listCloudConnectionRoutes ClientRequestException" + e.getHttpStatusCode());
            logger.error("listCloudConnectionRoutes ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("listCloudConnectionRoutes ServerResponseException" + e.getHttpStatusCode());
            logger.error("listCloudConnectionRoutes ServerResponseException" + e.getMessage());
        }
        return response;
    }

    private static void showCloudConnectionRoutes(CcClient client, String routesId) {
        ShowCloudConnectionRoutesRequest request = new ShowCloudConnectionRoutesRequest().withId(routesId);
        try {
            ShowCloudConnectionRoutesResponse response = client.showCloudConnectionRoutes(request);
            logger.info("showCloudConnectionRoutes" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showCloudConnectionRoutes ClientRequestException" + e.getHttpStatusCode());
            logger.error("showCloudConnectionRoutes ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showCloudConnectionRoutes ServerResponseException" + e.getHttpStatusCode());
            logger.error("showCloudConnectionRoutes ServerResponseException" + e.getMessage());
        }
    }
}