### 示例简介
当多个区域的VPC进行跨区域的私网通信时，需要先创建一个云连接实例，然后在创建的云连接实例中加载需要互通的VPC实例。

本示例展示如何使用网络实例（NetworkInstance）相关SDK，主要介绍网络实例增、删、改、查功能

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.准备一个VPC，记录VPC ID和区域ID。

6.创建一个云连接实例。

### SDK获取和安装
您可以通过Maven配置所依赖的虚拟私有云服务SDK

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cc</artifactId>
    <version>3.1.53</version>
</dependency>
```
### 代码示例
以下代码展示如何使用网络实例（NetworkInstance）相关SDK
``` java
import com.huaweicloud.sdk.cc.v3.CcClient;
import com.huaweicloud.sdk.cc.v3.model.CreateNetworkInstance;
import com.huaweicloud.sdk.cc.v3.model.CreateNetworkInstanceRequest;
import com.huaweicloud.sdk.cc.v3.model.CreateNetworkInstanceRequestBody;
import com.huaweicloud.sdk.cc.v3.model.CreateNetworkInstanceResponse;
import com.huaweicloud.sdk.cc.v3.model.DeleteNetworkInstanceRequest;
import com.huaweicloud.sdk.cc.v3.model.DeleteNetworkInstanceResponse;
import com.huaweicloud.sdk.cc.v3.model.ListNetworkInstancesRequest;
import com.huaweicloud.sdk.cc.v3.model.ListNetworkInstancesResponse;
import com.huaweicloud.sdk.cc.v3.model.ShowNetworkInstanceRequest;
import com.huaweicloud.sdk.cc.v3.model.ShowNetworkInstanceResponse;
import com.huaweicloud.sdk.cc.v3.model.UpdateNetworkInstance;
import com.huaweicloud.sdk.cc.v3.model.UpdateNetworkInstanceRequest;
import com.huaweicloud.sdk.cc.v3.model.UpdateNetworkInstanceRequestBody;
import com.huaweicloud.sdk.cc.v3.model.UpdateNetworkInstanceResponse;
import com.huaweicloud.sdk.cc.v3.region.CcRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class NetworkInstanceDemo {

    private static final Logger logger = LoggerFactory.getLogger(NetworkInstanceDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);
        //创建CcClient实例
        CcClient client = CcClient.newBuilder()
            .withCredential(auth)
            .withRegion(CcRegion.CN_NORTH_4)
            .build();

        // 1,创建网络实例
        CreateNetworkInstanceResponse response = createNetworkInstance(client);

        // 2,查询网络实例详情
        showNetworkInstance(client, response.getNetworkInstance().getId());

        // 3,修改网络实例
        updateNetworkInstance(client, response.getNetworkInstance().getId());

        // 4,查询网络实例列表
        listNetworkInstances(client, response.getNetworkInstance().getId());

        // 5,删除网络实例
        deleteNetworkInstance(client, response.getNetworkInstance().getId());

    }

    private static CreateNetworkInstanceResponse createNetworkInstance(CcClient client) {
        CreateNetworkInstanceRequest request = new CreateNetworkInstanceRequest()
            .withBody(new CreateNetworkInstanceRequestBody()
                .withNetworkInstance(new CreateNetworkInstance()
                    .withName("<your network-instance name>")
                    .withDescription("<your network-instance description>")
                    .withType(CreateNetworkInstance.TypeEnum.VPC)
                    .withProjectId("<your project id>")
                    .withInstanceId("<your instance id>")
                    .withRegionId("<your region id>")
                    .withCloudConnectionId("<your cloud-connection id>")
                    .withCidrs(Arrays.asList("<your cidrs>"))));
        CreateNetworkInstanceResponse response = null;
        try {
            response = client.createNetworkInstance(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
        return response;
    }

    private static void showNetworkInstance(CcClient client, String networkInstanceId) {
        ShowNetworkInstanceRequest request = new ShowNetworkInstanceRequest().withId(networkInstanceId);
        try {
            ShowNetworkInstanceResponse response = client.showNetworkInstance(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void updateNetworkInstance(CcClient client, String networkInstanceId) {
        UpdateNetworkInstanceRequest request = new UpdateNetworkInstanceRequest()
            .withId(networkInstanceId)
            .withBody(new UpdateNetworkInstanceRequestBody()
                .withNetworkInstance(new UpdateNetworkInstance()
                    .withName("<your new network-instance name>")
                    .withDescription("<your new network-instance description>")
                    .withCidrs(Arrays.asList("<your new cidrs>"))));
        try {
            UpdateNetworkInstanceResponse response = client.updateNetworkInstance(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void listNetworkInstances(CcClient client, String networkInstanceId) {
        ListNetworkInstancesRequest request = new ListNetworkInstancesRequest()
            .withInstanceId(Arrays.asList(networkInstanceId));
        try {
            ListNetworkInstancesResponse response = client.listNetworkInstances(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void deleteNetworkInstance(CcClient client, String networkInstanceId) {
        DeleteNetworkInstanceRequest request = new DeleteNetworkInstanceRequest()
            .withId(networkInstanceId);
        try {
            DeleteNetworkInstanceResponse response = client.deleteNetworkInstance(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }
}
```
您可以在 [云连接CC服务文档](https://support.huaweicloud.com/cc/index.html) 和[API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=CC&api=CreateNetworkInstance) 查看具体信息。

### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: | :-----: | :------:  
 2023/09/11 |1.0 | 文档首次发布
