package com.huawei.cc.demo;

import com.huaweicloud.sdk.cc.v3.CcClient;
import com.huaweicloud.sdk.cc.v3.model.CreateNetworkInstance;
import com.huaweicloud.sdk.cc.v3.model.CreateNetworkInstanceRequest;
import com.huaweicloud.sdk.cc.v3.model.CreateNetworkInstanceRequestBody;
import com.huaweicloud.sdk.cc.v3.model.CreateNetworkInstanceResponse;
import com.huaweicloud.sdk.cc.v3.model.DeleteNetworkInstanceRequest;
import com.huaweicloud.sdk.cc.v3.model.DeleteNetworkInstanceResponse;
import com.huaweicloud.sdk.cc.v3.model.ListNetworkInstancesRequest;
import com.huaweicloud.sdk.cc.v3.model.ListNetworkInstancesResponse;
import com.huaweicloud.sdk.cc.v3.model.ShowNetworkInstanceRequest;
import com.huaweicloud.sdk.cc.v3.model.ShowNetworkInstanceResponse;
import com.huaweicloud.sdk.cc.v3.model.UpdateNetworkInstance;
import com.huaweicloud.sdk.cc.v3.model.UpdateNetworkInstanceRequest;
import com.huaweicloud.sdk.cc.v3.model.UpdateNetworkInstanceRequestBody;
import com.huaweicloud.sdk.cc.v3.model.UpdateNetworkInstanceResponse;
import com.huaweicloud.sdk.cc.v3.region.CcRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class NetworkInstanceDemo {

    private static final Logger logger = LoggerFactory.getLogger(NetworkInstanceDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);
        //创建CcClient实例
        CcClient client = CcClient.newBuilder()
            .withCredential(auth)
            .withRegion(CcRegion.CN_NORTH_4)
            .build();

        // 1,创建网络实例
        CreateNetworkInstanceResponse response = createNetworkInstance(client);

        // 2,查询网络实例详情
        showNetworkInstance(client, response.getNetworkInstance().getId());

        // 3,修改网络实例
        updateNetworkInstance(client, response.getNetworkInstance().getId());

        // 4,查询网络实例列表
        listNetworkInstances(client, response.getNetworkInstance().getId());

        // 5,删除网络实例
        deleteNetworkInstance(client, response.getNetworkInstance().getId());

    }

    private static CreateNetworkInstanceResponse createNetworkInstance(CcClient client) {
        CreateNetworkInstanceRequest request = new CreateNetworkInstanceRequest()
            .withBody(new CreateNetworkInstanceRequestBody()
                .withNetworkInstance(new CreateNetworkInstance()
                    .withName("<your network-instance name>")
                    .withDescription("<your network-instance description>")
                    .withType(CreateNetworkInstance.TypeEnum.VPC)
                    .withProjectId("<your project id>")
                    .withInstanceId("<your instance id>")
                    .withRegionId("<your region id>")
                    .withCloudConnectionId("<your cloud-connection id>")
                    .withCidrs(Arrays.asList("<your cidrs>"))));
        CreateNetworkInstanceResponse response = null;
        try {
            response = client.createNetworkInstance(request);
            logger.info("createNetworkInstance" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createNetworkInstance ClientRequestException" + e.getHttpStatusCode());
            logger.error("createNetworkInstance ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("createNetworkInstance ServerResponseException" + e.getHttpStatusCode());
            logger.error("createNetworkInstance ServerResponseException" + e.getMessage());
        }
        return response;
    }

    private static void showNetworkInstance(CcClient client, String networkInstanceId) {
        ShowNetworkInstanceRequest request = new ShowNetworkInstanceRequest().withId(networkInstanceId);
        try {
            ShowNetworkInstanceResponse response = client.showNetworkInstance(request);
            logger.info("showNetworkInstance" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showNetworkInstance ClientRequestException" + e.getHttpStatusCode());
            logger.error("showNetworkInstance ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showNetworkInstance ServerResponseException" + e.getHttpStatusCode());
            logger.error("showNetworkInstance ServerResponseException" + e.getMessage());
        }
    }

    private static void updateNetworkInstance(CcClient client, String networkInstanceId) {
        UpdateNetworkInstanceRequest request = new UpdateNetworkInstanceRequest()
            .withId(networkInstanceId)
            .withBody(new UpdateNetworkInstanceRequestBody()
                .withNetworkInstance(new UpdateNetworkInstance()
                    .withName("<your new network-instance name>")
                    .withDescription("<your new network-instance description>")
                    .withCidrs(Arrays.asList("<your new cidrs>"))));
        try {
            UpdateNetworkInstanceResponse response = client.updateNetworkInstance(request);
            logger.info("updateNetworkInstance" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("updateNetworkInstance ClientRequestException" + e.getHttpStatusCode());
            logger.error("updateNetworkInstance ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("updateNetworkInstance ServerResponseException" + e.getHttpStatusCode());
            logger.error("updateNetworkInstance ServerResponseException" + e.getMessage());
        }
    }

    private static void listNetworkInstances(CcClient client, String networkInstanceId) {
        ListNetworkInstancesRequest request = new ListNetworkInstancesRequest()
            .withInstanceId(Arrays.asList(networkInstanceId));
        try {
            ListNetworkInstancesResponse response = client.listNetworkInstances(request);
            logger.info("listNetworkInstances" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listNetworkInstances ClientRequestException" + e.getHttpStatusCode());
            logger.error("listNetworkInstances ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("listNetworkInstances ServerResponseException" + e.getHttpStatusCode());
            logger.error("listNetworkInstances ServerResponseException" + e.getMessage());
        }
    }

    private static void deleteNetworkInstance(CcClient client, String networkInstanceId) {
        DeleteNetworkInstanceRequest request = new DeleteNetworkInstanceRequest()
            .withId(networkInstanceId);
        try {
            DeleteNetworkInstanceResponse response = client.deleteNetworkInstance(request);
            logger.info("deleteNetworkInstance" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("deleteNetworkInstance ClientRequestException" + e.getHttpStatusCode());
            logger.error("deleteNetworkInstance ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("deleteNetworkInstance ServerResponseException" + e.getHttpStatusCode());
            logger.error("deleteNetworkInstance ServerResponseException" + e.getMessage());
        }
    }
}