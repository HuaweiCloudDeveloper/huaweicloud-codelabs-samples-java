### 示例简介
本示例展示如何使用带宽包（BandwidthPackage）相关SDK，主要介绍带宽包增、删、改、查、绑定、解绑功能。

### 前置条件

1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

### SDK获取和安装

您可以通过Maven配置所依赖的虚拟私有云服务SDK

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cc</artifactId>
    <version>3.1.53</version>
</dependency>
```

### 代码示例

以下代码展示如何使用带宽包（BandwidthPackage）相关SDK

#### 带宽包创建、修改、查询示例

```java
public class BandwidthPackageDemo {

    private static final Logger logger = LoggerFactory.getLogger(BandwidthPackageDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);
        //创建CcClient实例
        CcClient client = CcClient.newBuilder()
            .withCredential(auth)
            .withRegion(CcRegion.CN_NORTH_4)
            .build();

        // 1,创建带宽包
        CreateBandwidthPackageResponse response = createBandwidthPackage(client);

        // 2,查询带宽包详情
        showBandwidthPackage(client, response.getBandwidthPackage().getId());

        // 3,修改带宽包
        updateBandwidthPackage(client, response.getBandwidthPackage().getId());

        // 4,查询带宽包列表
        listBandwidthPackages(client, response.getBandwidthPackage().getId());
    }

    private static CreateBandwidthPackageResponse createBandwidthPackage(CcClient client) {
        CreateBandwidthPackageRequest request = new CreateBandwidthPackageRequest()
            .withBody(new CreateBandwidthPackageRequestBody()
                .withBandwidthPackage(new CreateBandwidthPackage()
                    .withName("<your bandwidth package name>")
                    .withDescription("<your bandwidth package description>")
                    .withProjectId("<your project id>")
                    .withBillingMode(CreateBandwidthPackage.BillingModeEnum.NUMBER_3)
                    .withChargeMode(CreateBandwidthPackage.ChargeModeEnum.BANDWIDTH)
                    .withBandwidth(10)
                    .withLocalAreaId(CreateBandwidthPackage.LocalAreaIdEnum.CHINESE_MAINLAND)
                    .withRemoteAreaId(CreateBandwidthPackage.RemoteAreaIdEnum.CHINESE_MAINLAND)));
        CreateBandwidthPackageResponse response = null;
        try {
            response = client.createBandwidthPackage(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
        return response;
    }

    private static void showBandwidthPackage(CcClient client, String bandwidthPackageId) {
        ShowBandwidthPackageRequest request = new ShowBandwidthPackageRequest().withId(bandwidthPackageId);
        try {
            ShowBandwidthPackageResponse response = client.showBandwidthPackage(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void updateBandwidthPackage(CcClient client, String bandwidthPackageId) {
        UpdateBandwidthPackageRequest request = new UpdateBandwidthPackageRequest()
            .withId(bandwidthPackageId)
            .withBody(new UpdateBandwidthPackageRequestBody()
                .withBandwidthPackage(new UpdateBandwidthPackage()
                    .withName("<your new bandwidth package name>")
                    .withDescription("<your new bandwidth package description>")
                    .withBandwidth(20)));
        try {
            UpdateBandwidthPackageResponse response = client.updateBandwidthPackage(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void listBandwidthPackages(CcClient client, String bandwidthPackageId) {
        ListBandwidthPackagesRequest request = new ListBandwidthPackagesRequest()
            .withId(Arrays.asList(bandwidthPackageId));
        try {
            ListBandwidthPackagesResponse response = client.listBandwidthPackages(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }
}
```

#### 带宽包绑定、解绑、删除示例

```java
public class BandwidthPackageDemo {

    private static final Logger logger = LoggerFactory.getLogger(BandwidthPackageDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);
        //创建CcClient实例
        CcClient client = CcClient.newBuilder()
            .withCredential(auth)
            .withRegion(CcRegion.CN_NORTH_4)
            .build();

        // 1,绑定带宽包
        associateBandwidthPackage(client, response.getBandwidthPackage().getId());

        // 2,解绑带宽包
        disassociateBandwidthPackage(client, response.getBandwidthPackage().getId());

        // 3,删除带宽包
        deleteBandwidthPackage(client, response.getBandwidthPackage().getId());
    }


    private static void associateBandwidthPackage(CcClient client, String bandwidthPackageId) {
        AssociateBandwidthPackageRequest request = new AssociateBandwidthPackageRequest().withId(bandwidthPackageId)
            .withBody(new AssociateBandwidthPackageRequestBody()
                .withBandwidthPackage(new AssociateBandwidthPackage()
                    .withResourceId("<your cloud connection id>")
                    .withResourceType(AssociateBandwidthPackage.ResourceTypeEnum.CLOUD_CONNECTION)));
        try {
            AssociateBandwidthPackageResponse response = client.associateBandwidthPackage(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void disassociateBandwidthPackage(CcClient client, String bandwidthPackageId) {
        DisassociateBandwidthPackageRequest request = new DisassociateBandwidthPackageRequest().withId(bandwidthPackageId)
            .withBody(new DisassociateBandwidthPackageRequestBody()
                .withBandwidthPackage(new DisassociateBandwidthPackage()
                    .withResourceId("<your cloud connection id>")
                    .withResourceType(DisassociateBandwidthPackage.ResourceTypeEnum.CLOUD_CONNECTION)));
        try {
            DisassociateBandwidthPackageResponse response = client.disassociateBandwidthPackage(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void deleteBandwidthPackage(CcClient client, String bandwidthPackageId) {
        DeleteBandwidthPackageRequest request = new DeleteBandwidthPackageRequest()
            .withId(bandwidthPackageId);
        try {
            DeleteBandwidthPackageResponse response = client.deleteBandwidthPackage(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }
}
```

您可以在 [云连接CC服务文档](https://support.huaweicloud.com/cc/index.html)
和[API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=CC&api=CreateBandwidthPackage)
查看具体信息。

### 修订记录

发布日期  | 文档版本 | 修订说明
 :----: | :-----: | :------:  
2023/09/11 |1.0 | 文档首次发布
