package com.huawei.cc.demo;

import com.huaweicloud.sdk.cc.v3.CcClient;
import com.huaweicloud.sdk.cc.v3.model.AssociateBandwidthPackage;
import com.huaweicloud.sdk.cc.v3.model.AssociateBandwidthPackageRequest;
import com.huaweicloud.sdk.cc.v3.model.AssociateBandwidthPackageRequestBody;
import com.huaweicloud.sdk.cc.v3.model.AssociateBandwidthPackageResponse;
import com.huaweicloud.sdk.cc.v3.model.CreateBandwidthPackage;
import com.huaweicloud.sdk.cc.v3.model.CreateBandwidthPackageRequest;
import com.huaweicloud.sdk.cc.v3.model.CreateBandwidthPackageRequestBody;
import com.huaweicloud.sdk.cc.v3.model.CreateBandwidthPackageResponse;
import com.huaweicloud.sdk.cc.v3.model.DeleteBandwidthPackageRequest;
import com.huaweicloud.sdk.cc.v3.model.DeleteBandwidthPackageResponse;
import com.huaweicloud.sdk.cc.v3.model.DisassociateBandwidthPackage;
import com.huaweicloud.sdk.cc.v3.model.DisassociateBandwidthPackageRequest;
import com.huaweicloud.sdk.cc.v3.model.DisassociateBandwidthPackageRequestBody;
import com.huaweicloud.sdk.cc.v3.model.DisassociateBandwidthPackageResponse;
import com.huaweicloud.sdk.cc.v3.model.ListBandwidthPackagesRequest;
import com.huaweicloud.sdk.cc.v3.model.ListBandwidthPackagesResponse;
import com.huaweicloud.sdk.cc.v3.model.ShowBandwidthPackageRequest;
import com.huaweicloud.sdk.cc.v3.model.ShowBandwidthPackageResponse;
import com.huaweicloud.sdk.cc.v3.model.UpdateBandwidthPackage;
import com.huaweicloud.sdk.cc.v3.model.UpdateBandwidthPackageRequest;
import com.huaweicloud.sdk.cc.v3.model.UpdateBandwidthPackageRequestBody;
import com.huaweicloud.sdk.cc.v3.model.UpdateBandwidthPackageResponse;
import com.huaweicloud.sdk.cc.v3.region.CcRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class BandwidthPackageDemo {

    private static final Logger logger = LoggerFactory.getLogger(BandwidthPackageDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);
        //创建CcClient实例
        CcClient client = CcClient.newBuilder()
            .withCredential(auth)
            .withRegion(CcRegion.CN_NORTH_4)
            .build();

        // 1,创建带宽包
        CreateBandwidthPackageResponse response = createBandwidthPackage(client);

        // 2,查询带宽包详情
        showBandwidthPackage(client, response.getBandwidthPackage().getId());

        // 3,修改带宽包
        updateBandwidthPackage(client, response.getBandwidthPackage().getId());

        // 4,查询带宽包列表
        listBandwidthPackages(client, response.getBandwidthPackage().getId());

        // 5,绑定带宽包
        associateBandwidthPackage(client, response.getBandwidthPackage().getId());

        // 6,解绑带宽包
        disassociateBandwidthPackage(client, response.getBandwidthPackage().getId());

        // 7,删除带宽包
        deleteBandwidthPackage(client, response.getBandwidthPackage().getId());

    }

    private static CreateBandwidthPackageResponse createBandwidthPackage(CcClient client) {
        CreateBandwidthPackageRequest request = new CreateBandwidthPackageRequest()
            .withBody(new CreateBandwidthPackageRequestBody()
                .withBandwidthPackage(new CreateBandwidthPackage()
                    .withName("<your bandwidth package name>")
                    .withDescription("<your bandwidth package description>")
                    .withProjectId("<your project id>")
                    .withBillingMode(CreateBandwidthPackage.BillingModeEnum.NUMBER_3)
                    .withChargeMode(CreateBandwidthPackage.ChargeModeEnum.BANDWIDTH)
                    .withBandwidth(10)
                    .withLocalAreaId(CreateBandwidthPackage.LocalAreaIdEnum.CHINESE_MAINLAND)
                    .withRemoteAreaId(CreateBandwidthPackage.RemoteAreaIdEnum.CHINESE_MAINLAND)));
        CreateBandwidthPackageResponse response = null;
        try {
            response = client.createBandwidthPackage(request);
            logger.info("createBandwidthPackage" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("createBandwidthPackage ClientRequestException" + e.getHttpStatusCode());
            logger.error("createBandwidthPackage ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("createBandwidthPackage ServerResponseException" + e.getHttpStatusCode());
            logger.error("createBandwidthPackage ServerResponseException" + e.getMessage());
        }
        return response;
    }

    private static void showBandwidthPackage(CcClient client, String bandwidthPackageId) {
        ShowBandwidthPackageRequest request = new ShowBandwidthPackageRequest().withId(bandwidthPackageId);
        try {
            ShowBandwidthPackageResponse response = client.showBandwidthPackage(request);
            logger.info("showBandwidthPackage" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("showBandwidthPackage ClientRequestException" + e.getHttpStatusCode());
            logger.error("showBandwidthPackage ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("showBandwidthPackage ServerResponseException" + e.getHttpStatusCode());
            logger.error("showBandwidthPackage ServerResponseException" + e.getMessage());
        }
    }

    private static void updateBandwidthPackage(CcClient client, String bandwidthPackageId) {
        UpdateBandwidthPackageRequest request = new UpdateBandwidthPackageRequest()
            .withId(bandwidthPackageId)
            .withBody(new UpdateBandwidthPackageRequestBody()
                .withBandwidthPackage(new UpdateBandwidthPackage()
                    .withName("<your new bandwidth package name>")
                    .withDescription("<your new bandwidth package description>")
                    .withBandwidth(20)));
        try {
            UpdateBandwidthPackageResponse response = client.updateBandwidthPackage(request);
            logger.info("updateBandwidthPackage" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("updateBandwidthPackage ClientRequestException" + e.getHttpStatusCode());
            logger.error("updateBandwidthPackage ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("updateBandwidthPackage ServerResponseException" + e.getHttpStatusCode());
            logger.error("updateBandwidthPackage ServerResponseException" + e.getMessage());
        }
    }

    private static void listBandwidthPackages(CcClient client, String bandwidthPackageId) {
        ListBandwidthPackagesRequest request = new ListBandwidthPackagesRequest()
            .withId(Arrays.asList(bandwidthPackageId));
        try {
            ListBandwidthPackagesResponse response = client.listBandwidthPackages(request);
            logger.info("listBandwidthPackages" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("listBandwidthPackages ClientRequestException" + e.getHttpStatusCode());
            logger.error("listBandwidthPackages ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("listBandwidthPackages ServerResponseException" + e.getHttpStatusCode());
            logger.error("listBandwidthPackages ServerResponseException" + e.getMessage());
        }
    }

    private static void associateBandwidthPackage(CcClient client, String bandwidthPackageId) {
        AssociateBandwidthPackageRequest request = new AssociateBandwidthPackageRequest().withId(bandwidthPackageId)
            .withBody(new AssociateBandwidthPackageRequestBody()
                .withBandwidthPackage(new AssociateBandwidthPackage()
                    .withResourceId("<your cloud connection id>")
                    .withResourceType(AssociateBandwidthPackage.ResourceTypeEnum.CLOUD_CONNECTION)));
        try {
            AssociateBandwidthPackageResponse response = client.associateBandwidthPackage(request);
            logger.info("associateBandwidthPackage" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("associateBandwidthPackage ClientRequestException" + e.getHttpStatusCode());
            logger.error("associateBandwidthPackage ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("associateBandwidthPackage ServerResponseException" + e.getHttpStatusCode());
            logger.error("associateBandwidthPackage ServerResponseException" + e.getMessage());
        }
    }

    private static void disassociateBandwidthPackage(CcClient client, String bandwidthPackageId) {
        DisassociateBandwidthPackageRequest request = new DisassociateBandwidthPackageRequest().withId(bandwidthPackageId)
            .withBody(new DisassociateBandwidthPackageRequestBody()
                .withBandwidthPackage(new DisassociateBandwidthPackage()
                    .withResourceId("<your cloud connection id>")
                    .withResourceType(DisassociateBandwidthPackage.ResourceTypeEnum.CLOUD_CONNECTION)));
        try {
            DisassociateBandwidthPackageResponse response = client.disassociateBandwidthPackage(request);
            logger.info("disassociateBandwidthPackage" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("disassociateBandwidthPackage ClientRequestException" + e.getHttpStatusCode());
            logger.error("disassociateBandwidthPackage ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("disassociateBandwidthPackage ServerResponseException" + e.getHttpStatusCode());
            logger.error("disassociateBandwidthPackage ServerResponseException" + e.getMessage());
        }
    }

    private static void deleteBandwidthPackage(CcClient client, String bandwidthPackageId) {
        DeleteBandwidthPackageRequest request = new DeleteBandwidthPackageRequest()
            .withId(bandwidthPackageId);
        try {
            DeleteBandwidthPackageResponse response = client.deleteBandwidthPackage(request);
            logger.info("deleteBandwidthPackage" + response.toString());
        } catch (ClientRequestException e) {
            logger.error("deleteBandwidthPackage ClientRequestException" + e.getHttpStatusCode());
            logger.error("deleteBandwidthPackage ClientRequestException" + e);
        } catch (ServerResponseException e) {
            logger.error("deleteBandwidthPackage ServerResponseException" + e.getHttpStatusCode());
            logger.error("deleteBandwidthPackage ServerResponseException" + e.getMessage());
        }
    }
}