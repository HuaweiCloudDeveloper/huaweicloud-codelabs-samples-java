## 1. Functions

You can integrate VPC SDK provided by Huawei Cloud to call VPC APIs, making it easier for you to use the VPC service.
This example describes how to configure a security group for an ECS using the VPC SDK. The access policies of an ECS is provided by security groups, for details, please refer to [Security Group Overview](https://support.huaweicloud.com/intl/en-us/usermanual-vpc/en-us_topic_0073379079.html).

## 2. Flowchart
![Flowchart](./assets/securitygroup_process_diagram_en.png)

## 3. Prerequisites
- You have obtained the Huawei Cloud SDK. You can also install the Java SDK. For details, please refer to [VPC JAVA SDK](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/VPC?lang=Java).
- To use the Huawei Cloud Java SDK, you must have a Huawei Cloud account and obtain the AK and SK of the account. For details, please refer to [Obtaining an AK/SK](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html).
- Huawei Cloud Java SDK supports **Java JDK 1.8** and later versions.

## 4. Obtaining and Installing the SDK
You can use Maven to configure the VPC SDK.
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-vpc</artifactId>
    <version>3.1.49</version>
</dependency>
```

## 5. Key Code Snippets
```java
public static void main(String[] args) {
    // Enter the AK and SK of the Huawei Cloud account.
    // If the AK and SK used for authentication are directly written into the code, there are high security risks. You are advised to store the AK and SK in ciphertext in the configuration file or environment variables and decrypt the AK and SK when using them to ensure security.
    // In this example, the AK and SK are stored in environment variables for identity authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    // ECS id
    String ecsId = "{ecs_id}";

    ICredential auth = new BasicCredentials()
        .withAk(ak)
        .withSk(sk);

    // HTTP Client Configuration
    HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);

    // Vpc Client of v2 API
    // Set 'region_id' to the ID of the running region. For example, if the running region is CN North-Beijing4, please set 'region_id' to 'cn-north-4'.
    VpcClient vpcClient = VpcClient.newBuilder()
        .withCredential(auth)
        .withRegion(VpcRegion.valueOf("{region_id}"))
        .withHttpConfig(httpConfig)
        .build();

    VPCPortBindSecurityGroupDemo demo = new VPCPortBindSecurityGroupDemo();
    // 1. Querying NIC port information based on an ECS id.
    ListPortsResponse listPortsResponse = demo.listPorts(vpcClient, ecsId);
    // 2. Querying existing security groups.
    ListSecurityGroupsResponse securityGroupsResponse = demo.listSecurityGroups(vpcClient);
    String portId = listPortsResponse.getPorts().get(0).getId();
    String oldSecurityGroupId = securityGroupsResponse.getSecurityGroups().get(0).getId();
    // 3. Creating a new security group and new security group rules.
    String newSecurityGroupId = demo.createSecurityGroup(vpcClient);
    // 4. Associating a security group to the NIC port of the ECS.
    demo.updatePort(vpcClient, portId, Arrays.asList(oldSecurityGroupId, newSecurityGroupId));
    // 5. Disassociating the security group from the NIC port of the ECS.
    demo.updatePort(vpcClient, portId, Arrays.asList(oldSecurityGroupId));
    // 6. Deleting the created security group.
    demo.deleteSecurityGroup(vpcClient, newSecurityGroupId);
}
```

## 6. Example Returned Results
#### 6.1 Querying Security Groups
```java
class ListSecurityGroupsResponse {
    securityGroups: [class SecurityGroup {
        name: {security_group_name}
        description: {security_group_description}
        id: {security_group_id}
        vpcId: {vpc_id}
        enterpriseProjectId: {enterprise_project_id}
        securityGroupRules: [class SecurityGroupRule {
            id: {security_group_rule_id}
            description: null
            securityGroupId: {security_group_id}
            direction: ingress
            ethertype: IPv4
            protocol: null
            portRangeMin: null
            portRangeMax: null
            remoteIpPrefix: null
            remoteGroupId: {remote_security_group_id}
            remoteAddressGroupId: null
            tenantId: {tenant_id}
        }]
    }]
}
```

#### 6.2 Creating a Security Group
```java
class CreateSecurityGroupResponse {
    securityGroup: class SecurityGroup {
        name: {security_group_name}
        description: {security_group_description}
        id: {security_group_id}
        vpcId: {vpc_id}
        enterpriseProjectId: {enterprise_project_id}
        securityGroupRules: [class SecurityGroupRule {
            id: {security_group_rule_id_1}
            description: null
            securityGroupId: {security_group_id}
            direction: egress
            ethertype: IPv6
            protocol: null
            portRangeMin: null
            portRangeMax: null
            remoteIpPrefix: null
            remoteGroupId: null
            remoteAddressGroupId: null
            tenantId: {tenant_id}
        }, class SecurityGroupRule {
            id: {security_group_rule_id_2}
            description:
            securityGroupId: {security_group_id}
            direction: ingress
            ethertype: IPv4
            protocol: null
            portRangeMin: null
            portRangeMax: null
            remoteIpPrefix: null
            remoteGroupId: {remote_security_group_id}
            remoteAddressGroupId: null
            tenantId: {tenant_id}
        }]
    }
}
```

#### 6.3 Updating the Security Group List Associated to the NIC Port
```java
class UpdatePortResponse {
    port: class Port {
        id: {port_id}
        name: {port_name}
        networkId: {network_id}
        adminStateUp: true
        macAddress: {port_mac_address}
        fixedIps: [class FixedIp {
            ipAddress: {port_fixed_ip_address}
            subnetId: {port_subnet_id}
        }]
        deviceId: {device_id}
        deviceOwner: {device_owner}
        tenantId: {tenant_id}
        status: ACTIVE
        securityGroups: {security_group_list}
        allowedAddressPairs: []
        extraDhcpOpts: []
        bindingVnicType: normal
        dnsAssignment: [class DnsAssignMent {
            hostname: {dns_host_name}
            ipAddress: {dns_ip}
            fqdn: {dns_fqdn}
        }]
        dnsName: {dns_name}
        bindingVifDetails: class BindingVifDetails {
            primaryInterface: true
            portFilter: null
            ovsHybridPlug: null
        }
        bindingProfile: {}
        instanceId:
        instanceType:
        portSecurityEnabled: true
        zoneId: null
    }
}
```


## 7. References
- API References
  - [API Explorer ListSecurityGroups](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/VPC/doc?version=v2&api=ListSecurityGroups)
  - [API Explorer CreateSecurityGroup](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/VPC/doc?version=v2&api=CreateSecurityGroup)
  - [API Explorer UpdatePort](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/VPC/doc?version=v2&api=UpdatePort)
- SDK reference
[VPC JAVA SDK](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/VPC?lang=Java)
- Obtaining an AK/SK
[Obtaining an AK/SK](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html)

## 8. Change History

|    Release Date    | Issue |   Description   |
|:----------:| :------: | :----------: |
| 2023-08-14 |   1.0    | This issue is the first official release. |