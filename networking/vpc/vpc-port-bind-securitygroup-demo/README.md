## 1、功能介绍

华为云提供了虚拟私有云服务端SDK，您可以直接集成服务端SDK来调用虚拟私有云的相关API，从而实现对虚拟私有云的快速操作。
该示例展示了如何通过java版SDK配置云服务器的访问策略。云服务器的访问策略由安全组提供，请参见[安全组简介](https://support.huaweicloud.com/usermanual-vpc/zh-cn_topic_0073379079.html)。

## 2、流程图
![流程图](./assets/securitygroup_process_diagram.png)

## 3、前置条件
- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。具体请参见[VPC JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/VPC?lang=Java)。
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。具体请参见[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)。
- 华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。

## 4、SDK获取和安装
您可以通过Maven配置所依赖的虚拟私有云服务SDK
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-vpc</artifactId>
    <version>3.1.49</version>
</dependency>
```

## 5、关键代码片段
```java
public static void main(String[] args) {
    // 输入华为云账号的AK、SK
    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    // 云服务器id
    String ecsId = "{ecs_id}";

    ICredential auth = new BasicCredentials()
        .withAk(ak)
        .withSk(sk);

    // HTTP客户端配置
    HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);

    // v2接口的VpcClient，region_id处填写应用区域的id，如北京四填写cn-north-4
    VpcClient vpcClient = VpcClient.newBuilder()
        .withCredential(auth)
        .withRegion(VpcRegion.valueOf("{region_id}"))
        .withHttpConfig(httpConfig)
        .build();

    VPCPortBindSecurityGroupDemo demo = new VPCPortBindSecurityGroupDemo();
    // 1.通过云服务器的ID查询网卡信息
    ListPortsResponse listPortsResponse = demo.listPorts(vpcClient, ecsId);
    // 2.查询当前已有的安全组信息
    ListSecurityGroupsResponse securityGroupsResponse = demo.listSecurityGroups(vpcClient);
    String portId = listPortsResponse.getPorts().get(0).getId();
    String oldSecurityGroupId = securityGroupsResponse.getSecurityGroups().get(0).getId();
    // 3.创建新安全组和安全组规则
    String newSecurityGroupId = demo.createSecurityGroup(vpcClient);
    // 4.为弹性云服务器添加安全组
    demo.updatePort(vpcClient, portId, Arrays.asList(oldSecurityGroupId, newSecurityGroupId));
    // 5.解除云服务器绑定的安全组
    demo.updatePort(vpcClient, portId, Arrays.asList(oldSecurityGroupId));
    // 6.删除创建的安全组
    demo.deleteSecurityGroup(vpcClient, newSecurityGroupId);
}
```

## 6、运行结果
#### 6.1 查询安全组
```java
class ListSecurityGroupsResponse {
    securityGroups: [class SecurityGroup {
        name: {security_group_name}
        description: {security_group_description}
        id: {security_group_id}
        vpcId: {vpc_id}
        enterpriseProjectId: {enterprise_project_id}
        securityGroupRules: [class SecurityGroupRule {
            id: {security_group_rule_id}
            description: null
            securityGroupId: {security_group_id}
            direction: ingress
            ethertype: IPv4
            protocol: null
            portRangeMin: null
            portRangeMax: null
            remoteIpPrefix: null
            remoteGroupId: {remote_security_group_id}
            remoteAddressGroupId: null
            tenantId: {tenant_id}
        }]
    }]
}
```

#### 6.2 创建安全组
```java
class CreateSecurityGroupResponse {
    securityGroup: class SecurityGroup {
        name: {security_group_name}
        description: {security_group_description}
        id: {security_group_id}
        vpcId: {vpc_id}
        enterpriseProjectId: {enterprise_project_id}
        securityGroupRules: [class SecurityGroupRule {
            id: {security_group_rule_id_1}
            description: null
            securityGroupId: {security_group_id}
            direction: egress
            ethertype: IPv6
            protocol: null
            portRangeMin: null
            portRangeMax: null
            remoteIpPrefix: null
            remoteGroupId: null
            remoteAddressGroupId: null
            tenantId: {tenant_id}
        }, class SecurityGroupRule {
            id: {security_group_rule_id_2}
            description:
            securityGroupId: {security_group_id}
            direction: ingress
            ethertype: IPv4
            protocol: null
            portRangeMin: null
            portRangeMax: null
            remoteIpPrefix: null
            remoteGroupId: {remote_security_group_id}
            remoteAddressGroupId: null
            tenantId: {tenant_id}
        }]
    }
}
```

#### 6.3 更新网卡绑定的安全组列表
```java
class UpdatePortResponse {
    port: class Port {
        id: {port_id}
        name: {port_name}
        networkId: {network_id}
        adminStateUp: true
        macAddress: {port_mac_address}
        fixedIps: [class FixedIp {
            ipAddress: {port_fixed_ip_address}
            subnetId: {port_subnet_id}
        }]
        deviceId: {device_id}
        deviceOwner: {device_owner}
        tenantId: {tenant_id}
        status: ACTIVE
        securityGroups: {security_group_list}
        allowedAddressPairs: []
        extraDhcpOpts: []
        bindingVnicType: normal
        dnsAssignment: [class DnsAssignMent {
            hostname: {dns_host_name}
            ipAddress: {dns_ip}
            fqdn: {dns_fqdn}
        }]
        dnsName: {dns_name}
        bindingVifDetails: class BindingVifDetails {
            primaryInterface: true
            portFilter: null
            ovsHybridPlug: null
        }
        bindingProfile: {}
        instanceId:
        instanceType:
        portSecurityEnabled: true
        zoneId: null
    }
}
```


## 7、参考
- API参考
  - [API Explorer 查询安全组列表](https://console.huaweicloud.com/apiexplorer/#/openapi/VPC/doc?version=v2&api=ListSecurityGroups)
  - [API Explorer 创建安全组](https://console.huaweicloud.com/apiexplorer/#/openapi/VPC/doc?version=v2&api=CreateSecurityGroup)
  - [API Explorer 更新端口](https://console.huaweicloud.com/apiexplorer/#/openapi/VPC/doc?version=v2&api=UpdatePort)
- SDK参考
[VPC JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/VPC?lang=Java)
- AK SK获取
[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)

## 8、修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2023-08-14 |   1.0    | 文档首次发布 |