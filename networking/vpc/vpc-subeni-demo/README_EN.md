## 1. Functions

You can integrate VPC SDK provided by Huawei Cloud to call VPC APIs, making it easier for you to use the VPC service.
This example describes how to configure a supplementary network interface for an ECS using the VPC SDK. For details of supplementary network interface, please refer to [Supplementary Network Interface Overview](https://support.huaweicloud.com/intl/en-us/usermanual-vpc/vpc_subeni_0002.html).

## 2. Flowchart
![Flowchart](./assets/subeni_process_diagram_en.png)

## 3. Prerequisites
- You have obtained the Huawei Cloud SDK. You can also install the Java SDK. For details, please refer to [VPC JAVA SDK](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/VPC?lang=Java).
- To use the Huawei Cloud Java SDK, you must have a Huawei Cloud account and obtain the AK and SK of the account. For details, please refer to [Obtaining an AK/SK](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html).
- Huawei Cloud Java SDK supports **Java JDK 1.8** and later versions.

## 4. Obtaining and Installing the SDK
You can use Maven to configure the VPC SDK.
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-vpc</artifactId>
    <version>3.1.49</version>
</dependency>
```

## 5. Key Code Snippets
```java
public static void main(String[] args) {
    // Enter the AK and SK of the Huawei Cloud account.
    // If the AK and SK used for authentication are directly written into the code, there are high security risks. You are advised to store the AK and SK in ciphertext in the configuration file or environment variables and decrypt the AK and SK when using them to ensure security.
    // In this example, the AK and SK are stored in environment variables for identity authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");

    // The id of ECS which supports supplementary network interfaces.
    String ecsId = "{ecs_id}";

    ICredential auth = new BasicCredentials()
        .withAk(ak)
        .withSk(sk);

    // HTTP Client Configuration
    HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);

    // Vpc Client of v2 API
    // Set 'region_id' to the ID of the running region. For example, if the running region is CN North-Beijing4, please set 'region_id' to 'cn-north-4'.
    com.huaweicloud.sdk.vpc.v2.VpcClient clientV2 = com.huaweicloud.sdk.vpc.v2.VpcClient.newBuilder()
        .withCredential(auth)
        .withRegion(VpcRegion.valueOf("{region_id}"))
        .withHttpConfig(httpConfig)
        .build();

    // Vpc Client of v3 API
    // Set 'region_id' to the ID of the running region. For example, if the running region is CN North-Beijing4, please set 'region_id' to 'cn-north-4'.
    com.huaweicloud.sdk.vpc.v3.VpcClient clientV3 = com.huaweicloud.sdk.vpc.v3.VpcClient.newBuilder()
        .withCredential(auth)
        .withRegion(VpcRegion.valueOf("{region_id}"))
        .withHttpConfig(httpConfig)
        .build();

    VPCSubNetworkInterfaceDemo demo = new VPCSubNetworkInterfaceDemo();
    // Querying the NIC Port and the Network based on an ECS id.
    ListPortsResponse listPortsResponse = demo.listPorts(clientV2, ecsId);
    if (Objects.isNull(listPortsResponse)) {
        logger.error("Failed to query the ENI of ECS {}.", ecsId);
        return;
    }
    String portId = listPortsResponse.getPorts().get(0).getId();
    String networkId = listPortsResponse.getPorts().get(0).getNetworkId();

    // Querying existing security groups.
    ListSecurityGroupsResponse listSecurityGroupsResponse = demo.listSecurityGroups(clientV3);
    if (listSecurityGroupsResponse.getSecurityGroups().isEmpty()) {
        logger.error("The current tenant does not have any security groups.");
        return;
    }
    String securityGroupId = listSecurityGroupsResponse.getSecurityGroups().get(0).getId();
    
    // Creating a supplementary network interface with the NIC port, network, and security group information.
    CreateSubNetworkInterfaceResponse createSubEniResponse =
    demo.createSubNetworkInterface(clientV3, portId, networkId, securityGroupId);
    String subEniId = createSubEniResponse.getSubNetworkInterface().getId();
    // Updating the security groups associated to the supplementary network interface.
    demo.updateSubNetworkInterface(clientV3, subEniId,
    listSecurityGroupsResponse.getSecurityGroups().get(1).getId());
    // Deleting the supplementary network interface.
    demo.deleteSubNetworkInterface(clientV3, subEniId);
}
```

## 6. Example Returned Results
#### 6.1 Creating a Supplementary Network Interface
```java
class CreateSubNetworkInterfaceResponse {
    requestId: {request_id}
    subNetworkInterface: class SubNetworkInterface {
        id: {subEni_id}
        virsubnetId: {virsubnet_id}
        privateIpAddress: {subEni_ipv4_ip_address}
        ipv6IpAddress: {subEni_ipv6_ip_address}
        macAddress: {subEni_mac_address}
        parentDeviceId: {parent_device_id}
        parentId: {parent_port_id}
        description: {subEni_description}
        vpcId: {vpc_id}
        vlanId: {vlan_id}
        securityGroups: {bound_security_group_list}
        tags: {subEni_tags}
        projectId: {project_id}
        createdAt: 2023-08-07T06:46:42Z
    }
}
```

#### 6.2 Updating the Security Groups Associated to the Supplementary Network Interface
```java
class UpdateSubNetworkInterfaceResponse {
    requestId: 97adde0cf92ea3dcfc32b66df0d471b3
    subNetworkInterface: class SubNetworkInterface {
        id: {subEni_id}
        virsubnetId: {virsubnet_id}
        privateIpAddress: {subEni_ipv4_ip_address}
        ipv6IpAddress: {subEni_ipv6_ip_address}
        macAddress: {subEni_mac_address}
        parentDeviceId: {parent_device_id}
        parentId: {parent_port_id}
        description: {subEni_description}
        vpcId: {vpc_id}
        vlanId: {vlan_id}
        securityGroups: {new_bound_security_group_list}
        tags: {subEni_tags}
        projectId: {project_id}
        createdAt: 2023-08-07T06:46:42Z
    }
}
```


## 7. References
- API References
  - [API Explorer CreateSubNetworkInterface](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/VPC/doc?version=v3&api=CreateSubNetworkInterface)
  - [API Explorer UpdateSubNetworkInterface](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/VPC/doc?version=v3&api=UpdateSubNetworkInterface)
- SDK reference
[VPC JAVA SDK](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/VPC?lang=Java)
- Obtaining an AK/SK
[Obtaining an AK/SK](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html)

## 8. Change History

|    Release Date    | Issue |   Description   |
|:----------:| :------: | :----------: |
| 2023-08-14 |   1.0    | This issue is the first official release. |