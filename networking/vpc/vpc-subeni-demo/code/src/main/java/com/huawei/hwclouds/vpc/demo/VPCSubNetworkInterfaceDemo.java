package com.huawei.hwclouds.vpc.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.vpc.v2.model.ListPortsRequest;
import com.huaweicloud.sdk.vpc.v2.model.ListPortsResponse;
import com.huaweicloud.sdk.vpc.v3.model.CreateSubNetworkInterfaceOption;
import com.huaweicloud.sdk.vpc.v3.model.CreateSubNetworkInterfaceRequest;
import com.huaweicloud.sdk.vpc.v3.model.CreateSubNetworkInterfaceRequestBody;
import com.huaweicloud.sdk.vpc.v3.model.CreateSubNetworkInterfaceResponse;
import com.huaweicloud.sdk.vpc.v3.model.DeleteSubNetworkInterfaceRequest;
import com.huaweicloud.sdk.vpc.v3.model.DeleteSubNetworkInterfaceResponse;
import com.huaweicloud.sdk.vpc.v3.model.ListSecurityGroupsRequest;
import com.huaweicloud.sdk.vpc.v3.model.ListSecurityGroupsResponse;
import com.huaweicloud.sdk.vpc.v3.model.UpdateSubNetworkInterfaceOption;
import com.huaweicloud.sdk.vpc.v3.model.UpdateSubNetworkInterfaceRequest;
import com.huaweicloud.sdk.vpc.v3.model.UpdateSubNetworkInterfaceRequestBody;
import com.huaweicloud.sdk.vpc.v3.model.UpdateSubNetworkInterfaceResponse;
import com.huaweicloud.sdk.vpc.v3.region.VpcRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Objects;

/**
 * Configuring a Supplementary Network Interface for an ECS
 */
public class VPCSubNetworkInterfaceDemo {
    private static final Logger logger = LoggerFactory.getLogger(VPCSubNetworkInterfaceDemo.class.getName());

    public static void main(String[] args) {
        // Enter the AK and SK of the Huawei Cloud account.
        // If the AK and SK used for authentication are directly written into the code, there are high security risks. You are advised to store the AK and SK in ciphertext in the configuration file or environment variables and decrypt the AK and SK when using them to ensure security.
        // In this example, the AK and SK are stored in environment variables for identity authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // The id of ECS which supports supplementary network interfaces.
        String ecsId = "{ecs_id}";

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // HTTP Client Configuration
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);

        // Vpc Client of v2 API
        // Set 'region_id' to the ID of the running region. For example, if the running region is CN North-Beijing4, please set 'region_id' to 'cn-north-4'.
        com.huaweicloud.sdk.vpc.v2.VpcClient clientV2 = com.huaweicloud.sdk.vpc.v2.VpcClient.newBuilder()
            .withCredential(auth)
            .withRegion(VpcRegion.valueOf("{region_id}"))
            .withHttpConfig(httpConfig)
            .build();

        // Vpc Client of v3 API
        // Set 'region_id' to the ID of the running region. For example, if the running region is CN North-Beijing4, please set 'region_id' to 'cn-north-4'.
        com.huaweicloud.sdk.vpc.v3.VpcClient clientV3 = com.huaweicloud.sdk.vpc.v3.VpcClient.newBuilder()
            .withCredential(auth)
            .withRegion(VpcRegion.valueOf("{region_id}"))
            .withHttpConfig(httpConfig)
            .build();

        VPCSubNetworkInterfaceDemo demo = new VPCSubNetworkInterfaceDemo();
        // Querying the NIC Port and the Network based on an ECS id.
        ListPortsResponse listPortsResponse = demo.listPorts(clientV2, ecsId);
        if (Objects.isNull(listPortsResponse)) {
            logger.error("Failed to query the ENI of ECS {}.", ecsId);
            return;
        }
        String portId = listPortsResponse.getPorts().get(0).getId();
        String networkId = listPortsResponse.getPorts().get(0).getNetworkId();

        // Querying existing security groups.
        ListSecurityGroupsResponse listSecurityGroupsResponse = demo.listSecurityGroups(clientV3);
        if (listSecurityGroupsResponse.getSecurityGroups().isEmpty()) {
            logger.error("The current tenant does not have any security groups.");
            return;
        }
        String securityGroupId = listSecurityGroupsResponse.getSecurityGroups().get(0).getId();

        // Creating a supplementary network interface with the NIC port, network, and security group information.
        CreateSubNetworkInterfaceResponse createSubEniResponse =
            demo.createSubNetworkInterface(clientV3, portId, networkId, securityGroupId);
        String subEniId = createSubEniResponse.getSubNetworkInterface().getId();
        // Updating the security groups associated to the supplementary network interface.
        demo.updateSubNetworkInterface(clientV3, subEniId,
            listSecurityGroupsResponse.getSecurityGroups().get(1).getId());
        // Deleting the supplementary network interface.
        demo.deleteSubNetworkInterface(clientV3, subEniId);
    }

    private ListPortsResponse listPorts(com.huaweicloud.sdk.vpc.v2.VpcClient vpcClient, String ecsId) {
        ListPortsRequest listPortsRequest = new ListPortsRequest().withDeviceId(ecsId);
        ListPortsResponse listPortsResponse = null;
        try {
            listPortsResponse = vpcClient.listPorts(listPortsRequest);
            logger.info(listPortsResponse.toString());
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(e.toString());
        }
        return listPortsResponse;
    }

    private ListSecurityGroupsResponse listSecurityGroups(com.huaweicloud.sdk.vpc.v3.VpcClient vpcClient) {
        ListSecurityGroupsRequest listSecurityGroupsRequest = new ListSecurityGroupsRequest().withLimit(2);
        ListSecurityGroupsResponse listSecurityGroupsResponse = null;
        try {
            listSecurityGroupsResponse = vpcClient.listSecurityGroups(listSecurityGroupsRequest);
            logger.info(listSecurityGroupsResponse.toString());
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(e.toString());
        }
        return listSecurityGroupsResponse;
    }

    private CreateSubNetworkInterfaceResponse createSubNetworkInterface(com.huaweicloud.sdk.vpc.v3.VpcClient vpcClient,
        String parentPortId, String virSubnetId, String securityGroupId) {
        CreateSubNetworkInterfaceRequest createSubEniRequest = new CreateSubNetworkInterfaceRequest()
            .withBody(new CreateSubNetworkInterfaceRequestBody()
                .withSubNetworkInterface(new CreateSubNetworkInterfaceOption()
                    .withParentId(parentPortId)
                    .withVirsubnetId(virSubnetId)
                    .withSecurityGroups(Arrays.asList(securityGroupId))));
        CreateSubNetworkInterfaceResponse createSubEniResponse = null;
        try {
            createSubEniResponse = vpcClient.createSubNetworkInterface(createSubEniRequest);
            logger.info(createSubEniResponse.toString());
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(e.toString());
        }
        return createSubEniResponse;
    }

    private void updateSubNetworkInterface(com.huaweicloud.sdk.vpc.v3.VpcClient vpcClient,
        String subEniId, String securityGroupId) {
        UpdateSubNetworkInterfaceRequest updateSubEniRequest = new UpdateSubNetworkInterfaceRequest()
            .withSubNetworkInterfaceId(subEniId)
            .withBody(new UpdateSubNetworkInterfaceRequestBody()
                .withSubNetworkInterface(new UpdateSubNetworkInterfaceOption()
                    .withSecurityGroups(Arrays.asList(securityGroupId))));
        try {
            UpdateSubNetworkInterfaceResponse updateSubEniResponse =
                vpcClient.updateSubNetworkInterface(updateSubEniRequest);
            logger.info(updateSubEniResponse.toString());
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(e.toString());
        }
    }

    private void deleteSubNetworkInterface(com.huaweicloud.sdk.vpc.v3.VpcClient vpcClient, String subEniId) {
        DeleteSubNetworkInterfaceRequest deleteSubEniRequest = new DeleteSubNetworkInterfaceRequest()
            .withSubNetworkInterfaceId(subEniId);
        try {
            DeleteSubNetworkInterfaceResponse deleteSubEniResponse =
                vpcClient.deleteSubNetworkInterface(deleteSubEniRequest);
            logger.info(deleteSubEniResponse.toString());
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(e.toString());
        }
    }
}