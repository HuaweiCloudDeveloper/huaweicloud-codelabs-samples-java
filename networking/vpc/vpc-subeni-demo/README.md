## 1、功能介绍

华为云提供了虚拟私有云服务端SDK，您可以直接集成服务端SDK来调用虚拟私有云的相关API，从而实现对虚拟私有云的快速操作。
该示例展示了如何通过Java版SDK配置云服务器的辅助弹性网卡。辅助弹性网卡请参见[辅助弹性网卡简介](https://support.huaweicloud.com/usermanual-vpc/vpc_subeni_0002.html)。

## 2、流程图
![流程图](./assets/subeni_process_diagram.png)

## 3、前置条件
- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。具体请参见[VPC JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/VPC?lang=Java)。
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。具体请参见[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)。
- 华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。

## 4、SDK获取和安装
您可以通过Maven配置所依赖的虚拟私有云服务SDK
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-vpc</artifactId>
    <version>3.1.49</version>
</dependency>
```

## 5、关键代码片段
```java
public static void main(String[] args) {
    // 输入华为云账号的AK、SK
    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");

    // 支持辅助弹性网卡的云服务器id
    String ecsId = "{ecs_id}";

    ICredential auth = new BasicCredentials()
        .withAk(ak)
        .withSk(sk);

    // HTTP客户端配置
    HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);

    // v2接口的VpcClient，region_id处填写应用区域的id，如北京四填写cn-north-4
    com.huaweicloud.sdk.vpc.v2.VpcClient clientV2 = com.huaweicloud.sdk.vpc.v2.VpcClient.newBuilder()
        .withCredential(auth)
        .withRegion(VpcRegion.valueOf("{region_id}"))
        .withHttpConfig(httpConfig)
        .build();

    // v3接口的VpcClient，region_id处填写应用区域的id，如北京四填写cn-north-4
    com.huaweicloud.sdk.vpc.v3.VpcClient clientV3 = com.huaweicloud.sdk.vpc.v3.VpcClient.newBuilder()
        .withCredential(auth)
        .withRegion(VpcRegion.valueOf("{region_id}"))
        .withHttpConfig(httpConfig)
        .build();

    VPCSubNetworkInterfaceDemo demo = new VPCSubNetworkInterfaceDemo();
    // 根据云服务器的ID查询网卡和网络信息
    ListPortsResponse listPortsResponse = demo.listPorts(clientV2, ecsId);
    if (Objects.isNull(listPortsResponse)) {
        logger.error("Failed to query the ENI of ECS {}.", ecsId);
        return;
    }
    String portId = listPortsResponse.getPorts().get(0).getId();
    String networkId = listPortsResponse.getPorts().get(0).getNetworkId();

    // 查询当前已有的安全组
    ListSecurityGroupsResponse listSecurityGroupsResponse = demo.listSecurityGroups(clientV3);
    if (listSecurityGroupsResponse.getSecurityGroups().isEmpty()) {
        logger.error("The current tenant does not have any security groups.");
        return;
    }
    String securityGroupId = listSecurityGroupsResponse.getSecurityGroups().get(0).getId();

    // 根据网卡、网络和安全组信息创建辅助弹性网卡
    CreateSubNetworkInterfaceResponse createSubEniResponse =
    demo.createSubNetworkInterface(clientV3, portId, networkId, securityGroupId);
    String subEniId = createSubEniResponse.getSubNetworkInterface().getId();
    // 更新辅助弹性网卡绑定的安全组
    demo.updateSubNetworkInterface(clientV3, subEniId,
    listSecurityGroupsResponse.getSecurityGroups().get(1).getId());
    // 删除辅助弹性网卡
    demo.deleteSubNetworkInterface(clientV3, subEniId);
}
```

## 6、运行结果
#### 6.1 创建辅助弹性网卡
```java
class CreateSubNetworkInterfaceResponse {
    requestId: {request_id}
    subNetworkInterface: class SubNetworkInterface {
        id: {subEni_id}
        virsubnetId: {virsubnet_id}
        privateIpAddress: {subEni_ipv4_ip_address}
        ipv6IpAddress: {subEni_ipv6_ip_address}
        macAddress: {subEni_mac_address}
        parentDeviceId: {parent_device_id}
        parentId: {parent_port_id}
        description: {subEni_description}
        vpcId: {vpc_id}
        vlanId: {vlan_id}
        securityGroups: {bound_security_group_list}
        tags: {subEni_tags}
        projectId: {project_id}
        createdAt: 2023-08-07T06:46:42Z
    }
}
```

#### 6.2 更新辅助弹性网卡绑定的安全组
```java
class UpdateSubNetworkInterfaceResponse {
    requestId: 97adde0cf92ea3dcfc32b66df0d471b3
    subNetworkInterface: class SubNetworkInterface {
        id: {subEni_id}
        virsubnetId: {virsubnet_id}
        privateIpAddress: {subEni_ipv4_ip_address}
        ipv6IpAddress: {subEni_ipv6_ip_address}
        macAddress: {subEni_mac_address}
        parentDeviceId: {parent_device_id}
        parentId: {parent_port_id}
        description: {subEni_description}
        vpcId: {vpc_id}
        vlanId: {vlan_id}
        securityGroups: {new_bound_security_group_list}
        tags: {subEni_tags}
        projectId: {project_id}
        createdAt: 2023-08-07T06:46:42Z
    }
}
```


## 7、参考
- API参考
  - [API Explorer 创建辅助弹性网卡](https://console.huaweicloud.com/apiexplorer/#/openapi/VPC/doc?version=v3&api=CreateSubNetworkInterface)
  - [API Explorer 更新辅助弹性网卡](https://console.huaweicloud.com/apiexplorer/#/openapi/VPC/doc?version=v3&api=UpdateSubNetworkInterface)
- SDK参考
[VPC JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/VPC?lang=Java)
- AK SK获取
[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)

## 8、修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2023-08-14 |   1.0    | 文档首次发布 |