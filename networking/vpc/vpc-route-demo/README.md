## 1、功能介绍

华为云提供了虚拟私有云服务端SDK，您可以直接集成服务端SDK来调用虚拟私有云的相关API，从而实现对虚拟私有云的快速操作。
该示例展示了如何通过java版SDK配置VPC路由规则。VPC路由请参见[路由表简介](https://support.huaweicloud.com/usermanual-vpc/vpc_route01_0001.html)。

## 2、流程图
![流程图](./assets/route_process_diagram.png)

## 3、前置条件
- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。具体请参见[VPC JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/VPC?lang=Java)。
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。具体请参见[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)。
- 华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。

## 4、SDK获取和安装
您可以通过Maven配置所依赖的虚拟私有云服务SDK
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-vpc</artifactId>
    <version>3.1.49</version>
</dependency>
```

## 5、关键代码片段
```java
public static void main(String[] args) {
    // 输入华为云账号的AK、SK
    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");

    // 创建vpc对等连接所需的请求端vpc id和接收端vpc id
    String requestVpcId = "{request_vpc_id}";
    String acceptVpcId = "{accept_vpc_id}";

    ICredential auth = new BasicCredentials()
        .withAk(ak)
        .withSk(sk);

    // HTTP客户端配置
    HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);

    // V2接口的VpcClient，region_id处填写应用区域的id，如北京四填写cn-north-4
    VpcClient clientV2 = com.huaweicloud.sdk.vpc.v2.VpcClient.newBuilder()
        .withCredential(auth)
        .withRegion(VpcRegion.valueOf("{region_id}"))
        .withHttpConfig(httpConfig)
        .build();

    VPCRouteDemo demo = new VPCRouteDemo();
    // 创建VPC对等连接
    CreateVpcPeeringResponse createVpcPeeringResponse = demo.createVpcPeering(clientV2, requestVpcId, acceptVpcId);
    if (Objects.isNull(createVpcPeeringResponse)) {
        logger.error("Failed to create VPC peering.");
        return;
    }
    String peeringId = createVpcPeeringResponse.getPeering().getId();

    // 查询VPC中子网的网段
    List<Subnet> requestSubnetList = demo.listSubnets(clientV2,requestVpcId);
    List<Subnet> acceptSubnetList = demo.listSubnets(clientV2, acceptVpcId);
    if (requestSubnetList.isEmpty() || acceptSubnetList.isEmpty()) {
        logger.error("Failed to get subnet.");
        return;
    }

    // 通过更新路由表接口添加、修改、删除VPC路由
    // 路由类型为VPC对等连接，路由下一跳为VPC对等连接ID，路由目的地址填写对端VPC中的子网网段
    // 查询路由表ID
    String requestRtbId = demo.getRouteTableId(clientV2, requestSubnetList.get(0).getId());
    // 添加路由
    demo.addRoute(clientV2, requestRtbId, "peering", acceptSubnetList.get(0).getCidr(), peeringId, "peering route");
    // 修改路由
    demo.modRoute(clientV2, requestRtbId, "peering", acceptSubnetList.get(0).getCidr(), peeringId,
        "update peering route");
    // 删除路由
    demo.delRoute(clientV2, requestRtbId, "peering", acceptSubnetList.get(0).getCidr(), peeringId, "");

    // 删除对等连接
    demo.deleteVpcPeering(clientV2, peeringId);
}
```

## 6、运行结果
#### 6.1 创建对等连接
```java
class CreateVpcPeeringResponse {
    peering: class VpcPeering {
        id: {peering_id}
        name: {peering_name}
        status: ACTIVE
        requestVpcInfo: class VpcInfo {
            vpcId: {request_vpc_id}
            tenantId: {request_tenant_id}
        }
        acceptVpcInfo: class VpcInfo {
            vpcId: {accept_vpc_id}
            tenantId: {accept_tenant_id}
        }
        createdAt: 2023-08-07T06:28:17Z
        updatedAt: 2023-08-07T06:28:17Z
        description: test
    }
}
```


#### 6.2 更新路由表
```java
class UpdateRouteTableResponse {
    routetable: class RouteTableResp {
        id: {routetable_id}
        name: {routetable_id}
        _default: {is_default_routetable}
        routes: [class RouteTableRoute {
            type: {route_type}
            destination: {route_destination}
            nexthop: {route_nexthop}
            description: {route_description}
        }]
        subnets: [class SubnetList {
            id: {subnet_id}
        }]
        tenantId: {tenant_id}
        vpcId: {vpc_id}
        description: {routetable_description}
        createdAt: 2023-06-16T08:22:55Z
        updatedAt: 2023-08-07T06:28:20Z
    }
}
```


## 7、参考
- API参考
  - [API Explorer 创建对等连接](https://console.huaweicloud.com/apiexplorer/#/openapi/VPC/doc?version=v2&api=CreateVpcPeering)
  - [API Explorer 更新路由表](https://console.huaweicloud.com/apiexplorer/#/openapi/VPC/doc?version=v2&api=UpdateRouteTable)
- SDK参考
[VPC JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/VPC?lang=Java)
- AK SK获取
[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)

## 8、修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2023-08-14 |   1.0    | 文档首次发布 |