package com.huawei.hwclouds.vpc.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.vpc.v2.VpcClient;
import com.huaweicloud.sdk.vpc.v2.model.AddRouteTableRoute;
import com.huaweicloud.sdk.vpc.v2.model.CreateVpcPeeringOption;
import com.huaweicloud.sdk.vpc.v2.model.CreateVpcPeeringRequest;
import com.huaweicloud.sdk.vpc.v2.model.CreateVpcPeeringRequestBody;
import com.huaweicloud.sdk.vpc.v2.model.CreateVpcPeeringResponse;
import com.huaweicloud.sdk.vpc.v2.model.DelRouteTableRoute;
import com.huaweicloud.sdk.vpc.v2.model.DeleteVpcPeeringRequest;
import com.huaweicloud.sdk.vpc.v2.model.DeleteVpcPeeringResponse;
import com.huaweicloud.sdk.vpc.v2.model.ListRouteTablesRequest;
import com.huaweicloud.sdk.vpc.v2.model.ListRouteTablesResponse;
import com.huaweicloud.sdk.vpc.v2.model.ListSubnetsRequest;
import com.huaweicloud.sdk.vpc.v2.model.ListSubnetsResponse;
import com.huaweicloud.sdk.vpc.v2.model.ModRouteTableRoute;
import com.huaweicloud.sdk.vpc.v2.model.RouteTableRouteAction;
import com.huaweicloud.sdk.vpc.v2.model.Subnet;
import com.huaweicloud.sdk.vpc.v2.model.UpdateRouteTableReq;
import com.huaweicloud.sdk.vpc.v2.model.UpdateRouteTableRequest;
import com.huaweicloud.sdk.vpc.v2.model.UpdateRouteTableResponse;
import com.huaweicloud.sdk.vpc.v2.model.UpdateRoutetableReqBody;
import com.huaweicloud.sdk.vpc.v2.model.VpcInfo;
import com.huaweicloud.sdk.vpc.v2.region.VpcRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Configuring VPC Routes
 */
public class VPCRouteDemo {
    private static final Logger logger = LoggerFactory.getLogger(VPCRouteDemo.class.getName());

    public static void main(String[] args) {
        // Enter the AK and SK of the Huawei Cloud account.
        // If the AK and SK used for authentication are directly written into the code, there are high security risks. You are advised to store the AK and SK in ciphertext in the configuration file or environment variables and decrypt the AK and SK when using them to ensure security.
        // In this example, the AK and SK are stored in environment variables for identity authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Specifies the IDs of the request VPC and the accept VPC required for creating a VPC peering connection.
        String requestVpcId = "{request_vpc_id}";
        String acceptVpcId = "{accept_vpc_id}";

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);

        // Vpc Client of v2 API
        // Set 'region_id' to the ID of the running region. For example, if the running region is CN North-Beijing4, please set 'region_id' to 'cn-north-4'.
        VpcClient clientV2 = com.huaweicloud.sdk.vpc.v2.VpcClient.newBuilder()
            .withCredential(auth)
            .withRegion(VpcRegion.valueOf("{region_id}"))
            .withHttpConfig(httpConfig)
            .build();

        VPCRouteDemo demo = new VPCRouteDemo();
        // Creating a VPC peering connection.
        CreateVpcPeeringResponse createVpcPeeringResponse = demo.createVpcPeering(clientV2, requestVpcId, acceptVpcId);
        if (Objects.isNull(createVpcPeeringResponse)) {
            logger.error("Failed to create VPC peering.");
            return;
        }
        String peeringId = createVpcPeeringResponse.getPeering().getId();

        // Querying the subnet cidr of a VPC.
        List<Subnet> requestSubnetList = demo.listSubnets(clientV2,requestVpcId);
        List<Subnet> acceptSubnetList = demo.listSubnets(clientV2, acceptVpcId);
        if (requestSubnetList.isEmpty() || acceptSubnetList.isEmpty()) {
            logger.error("Failed to get subnet.");
            return;
        }

        // Adding, modifying and deleting routes by updating a routeTable.
        // The route type is VPC peering connection, the route nexthop is the ID of the VPC peering connection, and the route destination is the subnet cidr of the accept VPC.
        // Querying the ID of the routeTable.
        String requestRtbId = demo.getRouteTableId(clientV2, requestSubnetList.get(0).getId());
        // Adding routes.
        demo.addRoute(clientV2, requestRtbId, "peering", acceptSubnetList.get(0).getCidr(), peeringId, "peering route");
        // Modifying routes.
        demo.modRoute(clientV2, requestRtbId, "peering", acceptSubnetList.get(0).getCidr(), peeringId,
            "update peering route");
        // Deleting routes.
        demo.delRoute(clientV2, requestRtbId, "peering", acceptSubnetList.get(0).getCidr(), peeringId, "");

        // Deleting the VPC peering connection.
        demo.deleteVpcPeering(clientV2, peeringId);
    }

    private CreateVpcPeeringResponse createVpcPeering(VpcClient vpcClient, String requestVpcId, String acceptVpcId) {
        CreateVpcPeeringRequest createVpcPeeringRequest = new CreateVpcPeeringRequest()
            .withBody(new CreateVpcPeeringRequestBody()
                .withPeering(new CreateVpcPeeringOption()
                    .withName("peering-test")
                    .withRequestVpcInfo(new VpcInfo()
                        .withVpcId(requestVpcId))
                    .withAcceptVpcInfo(new VpcInfo()
                        .withVpcId(acceptVpcId))));
        CreateVpcPeeringResponse createVpcPeeringResponse = null;
        try {
            createVpcPeeringResponse = vpcClient.createVpcPeering(createVpcPeeringRequest);
            logger.info(createVpcPeeringResponse.toString());
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(e.toString());
        }
        return createVpcPeeringResponse;
    }

    private List<Subnet> listSubnets(VpcClient vpcClient, String vpcId) {
        ListSubnetsRequest listSubnetsRequest = new ListSubnetsRequest().withVpcId(vpcId).withLimit(1);
        List<Subnet> subnetList = new ArrayList<>();
        try {
            ListSubnetsResponse listSubnetsResponse = vpcClient.listSubnets(listSubnetsRequest);
            logger.info(listSubnetsResponse.toString());
            subnetList = listSubnetsResponse.getSubnets();
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(e.toString());
        }
        return subnetList;
    }

    private String getRouteTableId(VpcClient vpcClient, String subnetId) {
        ListRouteTablesRequest listRouteTablesRequest =
            new ListRouteTablesRequest().withSubnetId(subnetId);
        String rtbId = "";
        try {
            ListRouteTablesResponse listRouteTablesResponse = vpcClient.listRouteTables(listRouteTablesRequest);
            logger.info(listRouteTablesResponse.toString());
            if (listRouteTablesResponse.getRoutetables().isEmpty()) {
                logger.error("The current subnet does not have an associated routetable.");
                return rtbId;
            }
            rtbId = listRouteTablesResponse.getRoutetables().get(0).getId();
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(e.toString());
        }
        return rtbId;
    }

    private void delRoute(VpcClient vpcClient, String rtbId, String type, String destination, String nexthop,
        String description) {
        UpdateRouteTableRequest routeTableRequest = new UpdateRouteTableRequest()
            .withRoutetableId(rtbId)
            .withBody(new UpdateRoutetableReqBody()
                .withRoutetable(new UpdateRouteTableReq()
                    .withRoutes(new RouteTableRouteAction()
                        .withDel(Arrays.asList(new DelRouteTableRoute()
                            .withType(type)
                            .withDestination(destination)
                            .withNexthop(nexthop)
                            .withDescription(description))))));
        updateRouteTable(vpcClient, routeTableRequest);
    }

    private void addRoute(VpcClient vpcClient, String rtbId, String type, String destination, String nexthop,
        String description) {
        UpdateRouteTableRequest routeTableRequest = new UpdateRouteTableRequest()
            .withRoutetableId(rtbId)
            .withBody(new UpdateRoutetableReqBody()
                .withRoutetable(new UpdateRouteTableReq()
                    .withRoutes(new RouteTableRouteAction()
                        .withAdd(Arrays.asList(new AddRouteTableRoute()
                            .withType(type)
                            .withDestination(destination)
                            .withNexthop(nexthop)
                            .withDescription(description))))));
        updateRouteTable(vpcClient, routeTableRequest);
    }

    private void modRoute(VpcClient vpcClient, String rtbId, String type, String destination, String nexthop,
        String description) {
        UpdateRouteTableRequest routeTableRequest = new UpdateRouteTableRequest()
            .withRoutetableId(rtbId)
            .withBody(new UpdateRoutetableReqBody()
                .withRoutetable(new UpdateRouteTableReq()
                    .withRoutes(new RouteTableRouteAction()
                        .withMod(Arrays.asList(new ModRouteTableRoute()
                            .withType(type)
                            .withDestination(destination)
                            .withNexthop(nexthop)
                            .withDescription(description))))));
        updateRouteTable(vpcClient, routeTableRequest);
    }

    private void updateRouteTable(VpcClient vpcClient, UpdateRouteTableRequest updateRouteTableRequest) {
        try {
            UpdateRouteTableResponse updateRouteTableResponse = vpcClient.updateRouteTable(updateRouteTableRequest);
            logger.info(updateRouteTableResponse.toString());
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(e.toString());
        }
    }

    private void deleteVpcPeering(VpcClient vpcClient, String peeringId) {
        DeleteVpcPeeringRequest deleteVpcPeeringRequest = new DeleteVpcPeeringRequest().withPeeringId(peeringId);
        try {
            DeleteVpcPeeringResponse deleteVpcPeeringResponse = vpcClient.deleteVpcPeering(deleteVpcPeeringRequest);
            logger.info(deleteVpcPeeringResponse.toString());
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(e.toString());
        }
    }
}