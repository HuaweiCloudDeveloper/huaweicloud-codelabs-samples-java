## 1. Functions

You can integrate VPC SDK provided by Huawei Cloud to call VPC APIs, making it easier for you to use the VPC service.
This example describes how to configure VPC routes using the VPC SDK. For details of VPC routes, please refer to [RouteTable Overview](https://support.huaweicloud.com/intl/en-us/usermanual-vpc/vpc_route01_0001.html).

## 2. Flowchart
![Flowchart](./assets/route_process_diagram_en.png)

## 3. Prerequisites
- You have obtained the Huawei Cloud SDK. You can also install the Java SDK. For details, please refer to [VPC JAVA SDK](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/VPC?lang=Java).
- To use the Huawei Cloud Java SDK, you must have a Huawei Cloud account and obtain the AK and SK of the account. For details, please refer to [Obtaining an AK/SK](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html).
- Huawei Cloud Java SDK supports **Java JDK 1.8** and later versions.

## 4. Obtaining and Installing the SDK
You can use Maven to configure the VPC SDK.
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-vpc</artifactId>
    <version>3.1.49</version>
</dependency>
```

## 5. Key Code Snippets
```java
public static void main(String[] args) {
    // Enter the AK and SK of the Huawei Cloud account.
    // If the AK and SK used for authentication are directly written into the code, there are high security risks. You are advised to store the AK and SK in ciphertext in the configuration file or environment variables and decrypt the AK and SK when using them to ensure security.
    // In this example, the AK and SK are stored in environment variables for identity authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");

    // Specifies the IDs of the request VPC and the accept VPC required for creating a VPC peering connection.
    String requestVpcId = "{request_vpc_id}";
    String acceptVpcId = "{accept_vpc_id}";

    ICredential auth = new BasicCredentials()
        .withAk(ak)
        .withSk(sk);

    // HTTP Client Configuration
    HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);

    // Vpc Client of v2 API
    // Set 'region_id' to the ID of the running region. For example, if the running region is CN North-Beijing4, please set 'region_id' to 'cn-north-4'.
    VpcClient clientV2 = com.huaweicloud.sdk.vpc.v2.VpcClient.newBuilder()
        .withCredential(auth)
        .withRegion(VpcRegion.valueOf("{region_id}"))
        .withHttpConfig(httpConfig)
        .build();

    VPCRouteDemo demo = new VPCRouteDemo();
    // Creating a VPC peering connection.
    CreateVpcPeeringResponse createVpcPeeringResponse = demo.createVpcPeering(clientV2, requestVpcId, acceptVpcId);
    if (Objects.isNull(createVpcPeeringResponse)) {
        logger.error("Failed to create VPC peering.");
        return;
    }
    String peeringId = createVpcPeeringResponse.getPeering().getId();

    // Querying the subnet cidr of a VPC.
    List<Subnet> requestSubnetList = demo.listSubnets(clientV2,requestVpcId);
    List<Subnet> acceptSubnetList = demo.listSubnets(clientV2, acceptVpcId);
    if (requestSubnetList.isEmpty() || acceptSubnetList.isEmpty()) {
        logger.error("Failed to get subnet.");
        return;
    }

    // Adding, modifying and deleting routes by updating a routeTable.
    // The route type is VPC peering connection, the route nexthop is the ID of the VPC peering connection, and the route destination is the subnet cidr of the accept VPC.
    // Querying the ID of the routeTable.
    String requestRtbId = demo.getRouteTableId(clientV2, requestSubnetList.get(0).getId());
    // Adding routes.
    demo.addRoute(clientV2, requestRtbId, "peering", acceptSubnetList.get(0).getCidr(), peeringId, "peering route");
    // Modifying routes.
    demo.modRoute(clientV2, requestRtbId, "peering", acceptSubnetList.get(0).getCidr(), peeringId,
        "update peering route");
    // Deleting routes.
    demo.delRoute(clientV2, requestRtbId, "peering", acceptSubnetList.get(0).getCidr(), peeringId, "");

    // Deleting the VPC peering connection.
    demo.deleteVpcPeering(clientV2, peeringId);
}
```

## 6. Example Returned Results
#### 6.1 Creating a VPC Peering Connection
```java
class CreateVpcPeeringResponse {
    peering: class VpcPeering {
        id: {peering_id}
        name: {peering_name}
        status: ACTIVE
        requestVpcInfo: class VpcInfo {
            vpcId: {request_vpc_id}
            tenantId: {request_tenant_id}
        }
        acceptVpcInfo: class VpcInfo {
            vpcId: {accept_vpc_id}
            tenantId: {accept_tenant_id}
        }
        createdAt: 2023-08-07T06:28:17Z
        updatedAt: 2023-08-07T06:28:17Z
        description: test
    }
}
```


#### 6.2 Updating a RouteTable
```java
class UpdateRouteTableResponse {
    routetable: class RouteTableResp {
        id: {routetable_id}
        name: {routetable_id}
        _default: {is_default_routetable}
        routes: [class RouteTableRoute {
            type: {route_type}
            destination: {route_destination}
            nexthop: {route_nexthop}
            description: {route_description}
        }]
        subnets: [class SubnetList {
            id: {subnet_id}
        }]
        tenantId: {tenant_id}
        vpcId: {vpc_id}
        description: {routetable_description}
        createdAt: 2023-06-16T08:22:55Z
        updatedAt: 2023-08-07T06:28:20Z
    }
}
```


## 7. References
- API References
  - [API Explorer CreateVpcPeering](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/VPC/doc?version=v2&api=CreateVpcPeering)
  - [API Explorer UpdateRouteTable](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/VPC/doc?version=v2&api=UpdateRouteTable)
- SDK reference
[VPC JAVA SDK](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/VPC?lang=Java)
- Obtaining an AK/SK
[Obtaining an AK/SK](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0003.html)

## 8. Change History

|    Release Date    | Issue |   Description   |
|:----------:| :------: | :----------: |
| 2023-08-14 |   1.0    | This issue is the first official release. |