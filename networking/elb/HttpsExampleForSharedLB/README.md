## 1、功能介绍

华为云提供了[弹性负载均衡 ELB SDK](https://sdkcenter.developer.huaweicloud.com/?product=ELB)， 您可以直接集成SDK来调用相关API，从而实现对ELB服务的快速操作。

本章示例介绍如何创建支持HTTPS协议的负载均衡，包括负载均衡器、监听器、后端服务器组、健康检查、后端服务器等资源， 并将这些资源关联起来，组成一个完整的HTTPS协议的负载均衡。最后也包括如何释放这些负载均衡资源。

**注意：示例中创建的ELB为按需计费实例，如不再使用请及时删除相关资源。**

## 2、流程图
创建ELB资源流程图

![image](assets/createELBv2Resources.png)

删除ELB资源流程图

![image](assets/deleteELBv2Resources.png)


## 3、前置条件

- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。具体请参见[VPC JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/VPC?lang=Java)。
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。具体请参见[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)。
- 华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。

## 4、SDK获取和安装

您可以通过Maven配置所依赖的虚拟私有云服务SDK

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-elb</artifactId>
    <version>3.0.69</version>
</dependency>
```

## 5、关键代码片段

### 5.1、创建负载均衡器
负载均衡器是指您创建的承载业务的负载均衡服务实体。创建负载均衡器后，您还需要在负载均衡器中添加监听器和后端服务器，然后才能使用负载均衡服务提供的功能。
```java
// 负载均衡器所在的子网IPv4子网ID
String vipSubnetId = "xxx";

// 构造创建LB的请求参数
CreateLoadbalancerReq createLoadbalancerReq = new CreateLoadbalancerReq().withName("sdk-test-elb")
    .withVipSubnetId(vipSubnetId);
CreateLoadbalancerRequestBody createLoadbalancerBody = new CreateLoadbalancerRequestBody().withLoadbalancer(
    createLoadbalancerReq);
CreateLoadbalancerRequest createLoadbalancerRequest = new CreateLoadbalancerRequest().withBody(
    createLoadbalancerBody);

LoadbalancerResp loadbalancer = null;
// 请求创建 LB
try {
    CreateLoadbalancerResponse createLoadbalancerResp = elbClient.createLoadbalancer(createLoadbalancerRequest);
    loadbalancer = createLoadbalancerResp.getLoadbalancer();
} catch (Exception e) {
    logger.error("elbv2 create loadbalancer error: ", e);
    return;
}
```

### 5.2、创建后端服务器组
后端服务器组是一个或多个后端服务器的逻辑集合，用于将客户端的流量转发到一个或多个后端服务器，满足用户同时处理海量并发业务的需求。后端服务器可以是云服务器实例、辅助弹性网卡或IP地址。
```java
// 构造创建pool的请求参数
CreatePoolRequest request = new CreatePoolRequest();
CreatePoolRequestBody body = new CreatePoolRequestBody();
CreatePoolReq req = new CreatePoolReq();
if (loadbalancer != null) {
    req.withLoadbalancerId(loadbalancer.getId());
}
if (listener != null) {
    req.withListenerId(listener.getId());
}
req.withLbAlgorithm(ROUND_ROBIN).withProtocol(CreatePoolReq.ProtocolEnum.valueOf(protocol));
body.withPool(req);
request.withBody(body);

// 请求创建pool
PoolResp poolResp = null;
try {
    CreatePoolResponse creatPoolResp = elbClient.createPool(request);
    poolResp = creatPoolResp.getPool();
} catch (Exception e) {
    logger.error("elbv2 create l7policy error: ", e);
}
```

### 5.3、创建健康检查

```java
// 构造创建health monitor的请求参数
CreateHealthmonitorReq req = new CreateHealthmonitorReq().withPoolId(pool.getId())
    .withType(CreateHealthmonitorReq.TypeEnum.fromValue(pool.getProtocol().toString()))
    .withTimeout(10)
    .withDelay(5)
    .withMaxRetries(3)
    .withPoolId("123");
CreateHealthmonitorRequestBody body = new CreateHealthmonitorRequestBody().withHealthmonitor(req);
CreateHealthmonitorRequest request = new CreateHealthmonitorRequest().withBody(body);

// 请求创建healthMonitor
HealthmonitorResp healthMonitorResp = null;
try {
    CreateHealthmonitorResponse createHmResp = elbClient.createHealthmonitor(request);
    healthMonitorResp = createHmResp.getHealthmonitor();
} catch (Exception e) {
    logger.error("elbv2 create healthmonitor error: ", e);
}
```

### 5.4、创建后端服务器
负载均衡器会将客户端的请求转发给后端服务器处理。
```java
// 构造创建member的请求参数
CreateMemberReq req = new CreateMemberReq().withSubnetId(loadbalancer.getVipSubnetId())
    .withAddress(ip)
    .withProtocolPort(8080);
CreateMemberRequestBody body = new CreateMemberRequestBody().withMember(req);
CreateMemberRequest request = new CreateMemberRequest().withBody(body).withPoolId(pool.getId());

// 请求创建member
MemberResp memberResp = null;
try {
    CreateMemberResponse createMemberResp = elbClient.createMember(request);
    memberResp = createMemberResp.getMember();
} catch (Exception e) {
    logger.error("elbv2 create member error: ", e);
}
```

### 5.5、创建证书
配置HTTPS监听器时，需要为监听器绑定服务器证书。在使用HTTPS协议时，服务器证书用于SSL握手协商，需提供证书内容和私钥。
```java
// 创建证书对象
CreateCertificateRequestBody createCertificateBody = new CreateCertificateRequestBody().withCertificate(
    MY_CERTIFICATE)
    .withPrivateKey(MY_PRIVATE_KEY)
    .withType(CERTIFICATE_TYPE_SERVER)
    .withDomain(CERTIFICATE_DOMAIN)
    .withName("sdk-test-certificate");
CreateCertificateRequest createCertificateRequest = new CreateCertificateRequest().withBody(
    createCertificateBody);
CreateCertificateResponse certificate = null;
try {
    certificate = elbClient.createCertificate(createCertificateRequest);
} catch (Exception e) {
    logger.error("elbv2 create certificate error: ", e);
    return;
}
```

### 5.6、创建监听器
创建负载均衡器后，需要为负载均衡器配置监听器。监听器负责监听负载均衡器上的请求，根据配置流量分配策略，分发流量到后端服务器处理。
```java
// 构造创建 https listener的请求参数
CreateListenerReq createListenerReq = new CreateListenerReq().withLoadbalancerId(loadbalancer.getId())
    .withProtocol(CreateListenerReq.ProtocolEnum.fromValue(PROTOCOL_HTTPS))
    .withProtocolPort(443)
    .withDefaultTlsContainerRef(certificate.getId()) // 使用上面创建的证书
    .withName("sdk-test-https");
CreateListenerRequestBody createListenerBody = new CreateListenerRequestBody().withListener(createListenerReq);
CreateListenerRequest createListenerRequest = new CreateListenerRequest().withBody(createListenerBody);

// 请求创建https listener
ListenerResp listener = null;
try {
    CreateListenerResponse createListenerResp = elbClient.createListener(createListenerRequest);
    listener = createListenerResp.getListener();
} catch (Exception e) {
    logger.error("elbv2 create listener error: ", e);
    return;
}
```

### 5.7、创建转发规则
您可以通过给共享型负载均衡添加转发策略，将来自不同域名或者不同URL的请求转发到不同的后端服务器组处理。
```java
// 创建监听器下的转发策略和转发规则
// 转发规则
List<CreateL7ruleReqInPolicy> rules = new ArrayList<>();
CreateL7ruleReqInPolicy createL7ruleReqInPolicy = new CreateL7ruleReqInPolicy().withType(
    CreateL7ruleReqInPolicy.TypeEnum.fromValue("PATH"))
    .withCompareType("EQUAL_TO")
    .withValue("/elb/test");
rules.add(createL7ruleReqInPolicy);
CreateL7policyReq createL7policyReq = new CreateL7policyReq().withListenerId(listener.getId())
    .withAction(CreateL7policyReq.ActionEnum.fromValue("REDIRECT_TO_POOL"))
    .withRedirectPoolId(pool.getId())
    .withName("sdk-test-l7policy")
    .withRules(rules);
CreateL7policyRequestBody createL7policyRequestBody = new CreateL7policyRequestBody().withL7policy(
    createL7policyReq);
CreateL7policyRequest createL7policyRequest = new CreateL7policyRequest().withBody(createL7policyRequestBody);
// 请求创建转发策略和转发规则（可以同时创建）
L7policyResp l7Policy = null;
try {
    CreateL7policyResponse createL7PolicyResp = elbClient.createL7policy(createL7policyRequest);
    l7Policy = createL7PolicyResp.getL7policy();
} catch (Exception e) {
    logger.error("elbv2 create l7policy error: ", e);
    return;
}
```

### 5.8、更新证书

```java
// 更新证书
UpdateCertificateRequestBody updateCertificateBody = new UpdateCertificateRequestBody().withCertificate(
    MY_CERTIFICATE)
    .withDescription("update my certificate")
    .withName("sdk-test-certificate-updated")
    .withDomain(CERTIFICATE_DOMAIN)
    .withPrivateKey(MY_PRIVATE_KEY);
UpdateCertificateRequest updateCertificateRequest = new UpdateCertificateRequest().withCertificateId(
    certificate.getId()).withBody(updateCertificateBody);
try {
    UpdateCertificateResponse updateCertificateResp = elbClient.updateCertificate(updateCertificateRequest);
    logger.info("elbv2 update certificate http status code: {}", updateCertificateResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 update certificate error: ", e);
    return;
}
```

### 5.9、更新健康检查

```java
// 更新https listener的healthMonitor
UpdateHealthmonitorReq updateHealthmonitorReq = new UpdateHealthmonitorReq().withAdminStateUp(true)
    .withTimeout(25)
    .withDelay(20)
    .withMaxRetries(10)
    .withType(pool.getProtocol().toString());
UpdateHealthmonitorRequestBody updateHealthmonitorBody = new UpdateHealthmonitorRequestBody().withHealthmonitor(
    updateHealthmonitorReq);
UpdateHealthmonitorRequest updateHealthmonitorRequest = new UpdateHealthmonitorRequest().withHealthmonitorId(
    healthMonitor.getId()).withBody(updateHealthmonitorBody);
try {
    UpdateHealthmonitorResponse updateHealthMonitorResp = elbClient.updateHealthmonitor(
        updateHealthmonitorRequest);
    logger.info("elbv2 update healthmonitor http status code: {}", updateHealthMonitorResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 update healthmonitor error: ", e);
    return;
}
```

### 5.10、更新监听器

```java
// 更新https listener
UpdateListenerReq updateListenerReq = new UpdateListenerReq().withName("sdk-test-https-updated")
    .withDefaultTlsContainerRef(certificate.getId())
    .withAdminStateUp(true);
UpdateListenerRequestBody updateListenerBody = new UpdateListenerRequestBody().withListener(updateListenerReq);
UpdateListenerRequest updateListenerRequest = new UpdateListenerRequest().withBody(updateListenerBody)
    .withListenerId(listener.getId());
try {
    UpdateListenerResponse updateListenerResp = elbClient.updateListener(updateListenerRequest);
    logger.info("elbv2 update listener http status code: {}", updateListenerResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 update listener error: ", e);
    return;
}
```

### 5.11、删除后端服务器

```java
DeleteMemberRequest req = new DeleteMemberRequest().withPoolId(pool.getId()).withMemberId(member.getId());
try {
    DeleteMemberResponse resp = elbClient.deleteMember(req);
    logger.info("elbv2 delete member http status code: {}", resp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 delete member error: ", e);
}
```

### 5.12、删除健康检查

```java
// 删除健康检查
DeleteHealthmonitorRequest deleteHealthMonitorReq = new DeleteHealthmonitorRequest();
deleteHealthMonitorReq.withHealthmonitorId(healthMonitor.getId());
try {
    DeleteHealthmonitorResponse deleteHealthMonitorResp = elbClient.deleteHealthmonitor(deleteHealthMonitorReq);
    logger.info("elbv2 delete HealthMonitor http status code: {}", deleteHealthMonitorResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 delete HealthMonitor error: ", e);
    return;
}
```

### 5.13、删除转发规则

```java
// 可以通过直接删除转发策略，来删除转发规则
DeleteL7policyRequest delL7PolicyReq = new DeleteL7policyRequest();
delL7PolicyReq.setL7policyId(l7Policy.getId());
try {
    DeleteL7policyResponse deleteL7policyResponse = elbClient.deleteL7policy(delL7PolicyReq);
    logger.info("elbv2 delete l7policy http status code: {}", deleteL7policyResponse.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 delete l7policy error: ", e);
    return;
}
```

### 5.14、删除后端服务器组

```java
// 删除后端服务器组
DeletePoolRequest deletePoolRequest = new DeletePoolRequest();
deletePoolRequest.withPoolId(pool.getId());
try {
    DeletePoolResponse deletePoolResp = elbClient.deletePool(deletePoolRequest);
    logger.info("elbv2 delete pool http status code: {}", deletePoolResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 delete pool error: ", e);
    return;
}
```

### 5.15、删除监听器

```java
// 删除监听器
DeleteListenerRequest deleteListenerRequest = new DeleteListenerRequest();
deleteListenerRequest.withListenerId(listener.getId());
try {
    DeleteListenerResponse deleteListenerResp = elbClient.deleteListener(deleteListenerRequest);
    logger.info("elbv2 delete Listener http status code: {}", deleteListenerResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 delete listener error: ", e);
    return;
}
```

### 5.16、删除负载均衡器

```java
// 删除负载均衡器
DeleteLoadbalancerRequest deleteLoadbalancerRequest = new DeleteLoadbalancerRequest();
deleteLoadbalancerRequest.withLoadbalancerId(loadbalancer.getId());
try {
    DeleteLoadbalancerResponse deleteLoadbalancerResp = elbClient.deleteLoadbalancer(deleteLoadbalancerRequest);
    logger.info("elbv2 delete loadbalancer http status code: {}", deleteLoadbalancerResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 delete loadbalancer error: ", e);
    return;
}
```

### 5.17、删除证书

```java
// 删除证书
DeleteCertificateRequest deleteCertificateRequest = new DeleteCertificateRequest();
deleteCertificateRequest.withCertificateId(certificate.getId());
try {
    DeleteCertificateResponse deleteCertificateResp = elbClient.deleteCertificate(deleteCertificateRequest);
    logger.info("elbv2 delete certificate http status code: {}", deleteCertificateResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 delete certificate error: ", e);
}
```

## 6、返回结果示例

创建负载均衡器

```json
{
    "loadbalancer": {
        "description": "simple lb",
        "provisioning_status": "ACTIVE",
        "tenant_id": "601240b9c5c94059b63d484c92cfe308",
        "project_id": "601240b9c5c94059b63d484c92cfe308",
        "created_at": "2019-01-19T05:50:23",
        "admin_state_up": true,
        "updated_at": "2019-01-19T05:50:23",
        "id": "9fc511bf-5961-4cfc-860a-f9872b7b7857",
        "pools": [],
        "listeners": [],
        "vip_port_id": "d23c1637-c45c-46eb-aec6-70acf4664184",
        "operating_status": "ONLINE",
        "vip_address": "192.168.0.100",
        "enterprise_project_id": "0aad99bc-f5f6-4f78-8404-c598d76b0ed2",
        "vip_subnet_id": "47c04820-480d-403d-80e5-ee6c4d11d5b0",
        "provider": "vlb",
        "tags": [],
        "name": "loadbalancer1"
    }
}
```

创建后端服务器组

```json
{
    "pool": {
        "lb_algorithm": "ROUND_ROBIN",
        "protocol": "HTTP",
        "description": "",
        "admin_state_up": true,
        "loadbalancers": [
            {
                "id": "63ad9dfe-4750-479f-9630-ada43ccc8117"
            }
        ],
        "tenant_id": "601240b9c5c94059b63d484c92cfe308",
        "project_id": "601240b9c5c94059b63d484c92cfe308",
        "session_persistence": {
            "persistence_timeout": 1440,
            "cookie_name": null,
            "type": "HTTP_COOKIE"
        },
        "healthmonitor_id": null,
        "listeners": [],
        "members": [],
        "id": "d46eab56-d76b-4cd3-8952-3c3c4cf113aa",
        "name": ""
    }
}
```

创建健康检查

```json
{
    "healthmonitor": {
        "name": "",
        "admin_state_up": true,
        "tenant_id": "145483a5107745e9b3d80f956713e6a3",
        "project_id": "145483a5107745e9b3d80f956713e6a3",
        "domain_name": "www.test.com",
        "delay": 10,
        "expected_codes": "200",
        "max_retries": 10,
        "http_method": "GET",
        "timeout": 10,
        "pools": [
            {
                "id": "bb44bffb-05d9-412c-9d9c-b189d9e14193"
            }
        ],
        "url_path": "/",
        "type": "HTTP",
        "id": "2dca3867-98c5-4cde-8f2c-b89ae6bd7e36",
        "monitor_port": 112
    }
}
```

创建后端服务器

```json
{
    "member": {
        "name": "member-jy-tt-1",
        "weight": 1,
        "admin_state_up": true,
        "subnet_id": "33d8b01a-bbe6-41f4-bc45-78a1d284d503",
        "tenant_id": "145483a5107745e9b3d80f956713e6a3",
        "project_id": "145483a5107745e9b3d80f956713e6a3",
        "address": "192.168.44.11",
        "protocol_port": 88,
        "operating_status": "ONLINE",
        "id": "c0042496-e220-44f6-914b-e6ca33bab503"
    }
}
```

创建证书

```json
{
    "domain": "www.elb.com",
    "update_time": "2017-12-04 06:49:13",
    "create_time": "2017-12-04 06:49:13",
    "id": "3d8a7a02f87a40ed931b719edfe75451",
    "private_key": "-----BEGIN RSA PRIVATE KEY-----xxx-----END RSA PRIVATE KEY-----",
    "tenant_id": "930600df07ac4f66964004041bd3deaf",
    "type": "server",
    "certificate": "-----BEGIN CERTIFICATE-----xxx-----END CERTIFICATE-----",
    "name": "https_certificate",
    "description": "description for certificate"
}
```

创建监听器

```json
{
    "listener": {
        "insert_headers": {
            "X-Forwarded-ELB-IP": true
        },
        "protocol_port": 81,
        "protocol": "TCP",
        "description": "",
        "default_tls_container_ref": null,
        "sni_container_refs": [],
        "loadbalancers": [
            {
                "id": "0416b6f1-877f-4a51-987e-978b3f084253"
            }
        ],
        "tenant_id": "601240b9c5c94059b63d484c92cfe308",
        "created_at": "2019-03-06T04:00:07",
        "client_ca_tls_container_ref": null,
        "connection_limit": -1,
        "updated_at": "2019-03-06T04:00:07",
        "http2_enable": false,
        "project_id": "601240b9c5c94059b63d484c92cfe308",
        "admin_state_up": true,
        "default_pool_id": null,
        "id": "7f941f5c-80dc-43ac-8dfd-976fab49fa7a",
        "tags": [],
        "name": "listener-test"
    }
}
```

创建转发策略

```json
{
    "l7policy": {
        "redirect_pool_id": null,
        "description": "",
        "admin_state_up": true,
        "rules": [],
        "tenant_id": "601240b9c5c94059b63d484c92cfe308",
        "project_id": "601240b9c5c94059b63d484c92cfe308",
        "listener_id": "369f1208-ff1d-4f5a-a5e2-f1dc9c567a84",
        "redirect_url": null,
        "position": 100,
        "action": "REDIRECT_TO_LISTENER",
        "redirect_listener_id": "6edb5797-3615-4950-95eb-b60a8b1253a5",
        "provisioning_status": "ACTIVE",
        "id": "98da7cdb-64a8-40d8-bd1f-c5b1f48dbf07",
        "name": ""
    }
}
```

更新证书

```json
{
    "certificate": "MIICwjCC...rrN0=",
    "create_time": "2017-02-25 09:35:27",
    "description": "description for certificate",
    "domain": "www.elb.com",
    "id": "23ef9aad4ecb463580476d324a6c71af",
    "name": "https_certificate",
    "private_key": "-----BEGIN RSA PRIVATE KEY-----xxx-----END RSA PRIVATE KEY-----",
    "type": "server",
    "update_time": "2017-02-25 09:35:27"
}
```

更新健康检查

```json
{
    "healthmonitor": {
        "name": "health-xx",
        "admin_state_up": true,
        "tenant_id": "145483a5107745e9b3d80f956713e6a3",
        "project_id": "145483a5107745e9b3d80f956713e6a3",
        "domain_name": null,
        "delay": 15,
        "expected_codes": "200",
        "max_retries": 10,
        "http_method": "GET",
        "timeout": 12,
        "pools": [
            {
                "id": "bb44bffb-05d9-412c-9d9c-b189d9e14193"
            }
        ],
        "url_path": "/",
        "type": "HTTP",
        "id": "2dca3867-98c5-4cde-8f2c-b89ae6bd7e36",
        "monitor_port": 112
    }
}
```

更新监听器

```json
{
    "listener": {
        "client_ca_tls_container_ref": "417a0976969f497db8cbb083bff343ba",
        "protocol": "TERMINATED_HTTPS",
        "description": "my listener",
        "default_tls_container_ref": "23b58a961a4d4c95be585e98046e657a",
        "admin_state_up": true,
        "http2_enable": false,
        "loadbalancers": [
            {
                "id": "165b6a38-5278-4569-b747-b2ee65ea84a4"
            }
        ],
        "tenant_id": "601240b9c5c94059b63d484c92cfe308",
        "project_id": "601240b9c5c94059b63d484c92cfe308",
        "sni_container_refs": [],
        "tags": [],
        "connection_limit": -1,
        "protocol_port": 443,
        "default_pool_id": "c61310de-9a06-4f0c-850c-6f4797b9984c",
        "id": "f622c150-72f5-4263-a47a-e5003c652aa3",
        "name": "listener-jy-test2",
        "created_at": "2018-07-25T01:54:13",
        "updated_at": "2018-07-25T01:54:14",
        "insert_headers": {
            "X-Forwarded-ELB-IP": true
        }
    }
}
```

## 7、参考链接

- API参考
  - 创建负载均衡器[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=CreateLoadbalancer)
  - 创建后端服务器组[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=CreatePool)
  - 创建健康检查[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=CreateHealthmonitor)
  - 创建后端服务器[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=CreateMember)
  - 创建证书[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=CreateCertificate)
  - 创建监听器[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=CreateListener)
  - 创建转发策略[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=CreateL7policy)
  - 更新证书[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=UpdateCertificate)
  - 更新健康检查[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=UpdateHealthmonitor)
  - 更新监听器[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=UpdateListener)
- SDK参考[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/ELB?lang=Java)
- AK SK获取[我的凭证_用户指南_华为云 (huaweicloud.com)](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)

## 8、修订记录

|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2023-08-21 |   1.0    | 文档首次发布 |