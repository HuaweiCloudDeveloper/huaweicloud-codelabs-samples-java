package com.huaweicloud.elb.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.elb.v2.ElbClient;
import com.huaweicloud.sdk.elb.v2.model.CreateCertificateRequest;
import com.huaweicloud.sdk.elb.v2.model.CreateCertificateRequestBody;
import com.huaweicloud.sdk.elb.v2.model.CreateCertificateResponse;
import com.huaweicloud.sdk.elb.v2.model.CreateHealthmonitorReq;
import com.huaweicloud.sdk.elb.v2.model.CreateHealthmonitorRequest;
import com.huaweicloud.sdk.elb.v2.model.CreateHealthmonitorRequestBody;
import com.huaweicloud.sdk.elb.v2.model.CreateHealthmonitorResponse;
import com.huaweicloud.sdk.elb.v2.model.CreateL7policyReq;
import com.huaweicloud.sdk.elb.v2.model.CreateL7policyRequest;
import com.huaweicloud.sdk.elb.v2.model.CreateL7policyRequestBody;
import com.huaweicloud.sdk.elb.v2.model.CreateL7policyResponse;
import com.huaweicloud.sdk.elb.v2.model.CreateL7ruleReqInPolicy;
import com.huaweicloud.sdk.elb.v2.model.CreateListenerReq;
import com.huaweicloud.sdk.elb.v2.model.CreateListenerRequest;
import com.huaweicloud.sdk.elb.v2.model.CreateListenerRequestBody;
import com.huaweicloud.sdk.elb.v2.model.CreateListenerResponse;
import com.huaweicloud.sdk.elb.v2.model.CreateLoadbalancerReq;
import com.huaweicloud.sdk.elb.v2.model.CreateLoadbalancerRequest;
import com.huaweicloud.sdk.elb.v2.model.CreateLoadbalancerRequestBody;
import com.huaweicloud.sdk.elb.v2.model.CreateLoadbalancerResponse;
import com.huaweicloud.sdk.elb.v2.model.CreateMemberReq;
import com.huaweicloud.sdk.elb.v2.model.CreateMemberRequest;
import com.huaweicloud.sdk.elb.v2.model.CreateMemberRequestBody;
import com.huaweicloud.sdk.elb.v2.model.CreateMemberResponse;
import com.huaweicloud.sdk.elb.v2.model.CreatePoolReq;
import com.huaweicloud.sdk.elb.v2.model.CreatePoolRequest;
import com.huaweicloud.sdk.elb.v2.model.CreatePoolRequestBody;
import com.huaweicloud.sdk.elb.v2.model.CreatePoolResponse;
import com.huaweicloud.sdk.elb.v2.model.DeleteCertificateRequest;
import com.huaweicloud.sdk.elb.v2.model.DeleteCertificateResponse;
import com.huaweicloud.sdk.elb.v2.model.DeleteHealthmonitorRequest;
import com.huaweicloud.sdk.elb.v2.model.DeleteHealthmonitorResponse;
import com.huaweicloud.sdk.elb.v2.model.DeleteL7policyRequest;
import com.huaweicloud.sdk.elb.v2.model.DeleteL7policyResponse;
import com.huaweicloud.sdk.elb.v2.model.DeleteListenerRequest;
import com.huaweicloud.sdk.elb.v2.model.DeleteListenerResponse;
import com.huaweicloud.sdk.elb.v2.model.DeleteLoadbalancerRequest;
import com.huaweicloud.sdk.elb.v2.model.DeleteLoadbalancerResponse;
import com.huaweicloud.sdk.elb.v2.model.DeleteMemberRequest;
import com.huaweicloud.sdk.elb.v2.model.DeleteMemberResponse;
import com.huaweicloud.sdk.elb.v2.model.DeletePoolRequest;
import com.huaweicloud.sdk.elb.v2.model.DeletePoolResponse;
import com.huaweicloud.sdk.elb.v2.model.HealthmonitorResp;
import com.huaweicloud.sdk.elb.v2.model.L7policyResp;
import com.huaweicloud.sdk.elb.v2.model.ListenerResp;
import com.huaweicloud.sdk.elb.v2.model.LoadbalancerResp;
import com.huaweicloud.sdk.elb.v2.model.MemberResp;
import com.huaweicloud.sdk.elb.v2.model.PoolResp;
import com.huaweicloud.sdk.elb.v2.model.UpdateCertificateRequest;
import com.huaweicloud.sdk.elb.v2.model.UpdateCertificateRequestBody;
import com.huaweicloud.sdk.elb.v2.model.UpdateCertificateResponse;
import com.huaweicloud.sdk.elb.v2.model.UpdateHealthmonitorReq;
import com.huaweicloud.sdk.elb.v2.model.UpdateHealthmonitorRequest;
import com.huaweicloud.sdk.elb.v2.model.UpdateHealthmonitorRequestBody;
import com.huaweicloud.sdk.elb.v2.model.UpdateHealthmonitorResponse;
import com.huaweicloud.sdk.elb.v2.model.UpdateListenerReq;
import com.huaweicloud.sdk.elb.v2.model.UpdateListenerRequest;
import com.huaweicloud.sdk.elb.v2.model.UpdateListenerRequestBody;
import com.huaweicloud.sdk.elb.v2.model.UpdateListenerResponse;
import com.huaweicloud.sdk.elb.v2.region.ElbRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Elbv2ApiHTTPSExample {
    private static final Logger logger = LoggerFactory.getLogger(Elbv2ApiHTTPSExample.class);

    // There will be security risks if the AK/SK used for authentication is directly written into code. Encrypt the AK/SK in the configuration file or environment variables for storage.
    // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");

    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    private static final String PROTOCOL_HTTP = "HTTP";

    private static final String PROTOCOL_HTTPS = "TERMINATED_HTTPS";

    private static final String ROUND_ROBIN = "ROUND_ROBIN";

    private static final String MEMBER0_IP = "192.168.0.3";

    private static final String MEMBER1_IP = "192.168.0.4";

    // Domain name of the certificate. The following example is not a real domain name.
    private static final String CERTIFICATE_DOMAIN = "www.myelb-v2-test.com";

    private static final String CERTIFICATE_TYPE_SERVER = "server";

    // Certificate and private key in PEM format. The following example certificate and private key are not valid.
    private static final String MY_PRIVATE_KEY = readFileContent("src/main/resources/privateKey.pem");

    private static final String MY_CERTIFICATE = readFileContent("src/main/resources/certificate.pem");

    private static ElbClient elbClient = getElbClient();

    /* Obtain the client that sends requests to the load balancer. */
    public static ElbClient getElbClient() {
        return ElbClient.newBuilder()
            .withCredential(new BasicCredentials().withAk(AK).withSk(SK))
            .withRegion(ElbRegion.CN_NORTH_1)
            .build();
    }

    private static String readFileContent(String filePath) {
        String fileContent = null;
        try {
            fileContent = new String(Files.readAllBytes(Paths.get(filePath)), Charset.defaultCharset());
        } catch (Exception e) {
            logger.error("read file error: ", e);
        }
        return fileContent;
    }

    public static void main(String[] argvs) {

        // Specify the ID of the IPv4 subnet where the load balancer works.
        String vipSubnetId = "xxx";

        // Construct the request parameters for creating a load balancer.
        CreateLoadbalancerReq createLoadbalancerReq = new CreateLoadbalancerReq().withName("sdk-test-elb")
            .withVipSubnetId(vipSubnetId);
        CreateLoadbalancerRequestBody createLoadbalancerBody = new CreateLoadbalancerRequestBody().withLoadbalancer(
            createLoadbalancerReq);
        CreateLoadbalancerRequest createLoadbalancerRequest = new CreateLoadbalancerRequest().withBody(
            createLoadbalancerBody);

        LoadbalancerResp loadbalancer = null;
        // Request for creating a load balancer.
        try {
            CreateLoadbalancerResponse createLoadbalancerResp = elbClient.createLoadbalancer(createLoadbalancerRequest);
            loadbalancer = createLoadbalancerResp.getLoadbalancer();
        } catch (Exception e) {
            logger.error("elbv2 create loadbalancer error: ", e);
            return;
        }

        // Create a backend server group, configure a health check, and add a backend server to the load balancer.
        PoolResp pool = createPool(loadbalancer, null, PROTOCOL_HTTP);
        HealthmonitorResp healthMonitor = createHm(pool);
        MemberResp member1 = createMember(loadbalancer, pool, MEMBER0_IP);
        MemberResp member2 = createMember(loadbalancer, pool, MEMBER1_IP);

        // Add a certificate.
        CreateCertificateRequestBody createCertificateBody = new CreateCertificateRequestBody().withCertificate(
                MY_CERTIFICATE)
            .withPrivateKey(MY_PRIVATE_KEY)
            .withType(CERTIFICATE_TYPE_SERVER)
            .withDomain(CERTIFICATE_DOMAIN)
            .withName("sdk-test-certificate");
        CreateCertificateRequest createCertificateRequest = new CreateCertificateRequest().withBody(
            createCertificateBody);
        CreateCertificateResponse certificate = null;
        try {
            certificate = elbClient.createCertificate(createCertificateRequest);
        } catch (Exception e) {
            logger.error("elbv2 create certificate error: ", e);
            return;
        }

        // Construct the request parameters for adding an HTTPS listener.
        CreateListenerReq createListenerReq = new CreateListenerReq().withLoadbalancerId(loadbalancer.getId())
            .withProtocol(CreateListenerReq.ProtocolEnum.fromValue(PROTOCOL_HTTPS))
            .withProtocolPort(443)
            .withDefaultTlsContainerRef(certificate.getId())
            .withName("sdk-test-https");
        CreateListenerRequestBody createListenerBody = new CreateListenerRequestBody().withListener(createListenerReq);
        CreateListenerRequest createListenerRequest = new CreateListenerRequest().withBody(createListenerBody);

        // Request for adding an HTTPS listener.
        ListenerResp listener = null;
        try {
            CreateListenerResponse createListenerResp = elbClient.createListener(createListenerRequest);
            listener = createListenerResp.getListener();
        } catch (Exception e) {
            logger.error("elbv2 create listener error: ", e);
            return;
        }

        // Add a forwarding policy.
        // Add a forwarding rule.
        List<CreateL7ruleReqInPolicy> rules = new ArrayList<>();
        CreateL7ruleReqInPolicy createL7ruleReqInPolicy = new CreateL7ruleReqInPolicy().withType(
                CreateL7ruleReqInPolicy.TypeEnum.fromValue("PATH"))
            .withCompareType("EQUAL_TO")
            .withValue("/elb/test");
        rules.add(createL7ruleReqInPolicy);
        CreateL7policyReq createL7policyReq = new CreateL7policyReq().withListenerId(listener.getId())
            .withAction(CreateL7policyReq.ActionEnum.fromValue("REDIRECT_TO_POOL"))
            .withRedirectPoolId(pool.getId())
            .withName("sdk-test-l7policy")
            .withRules(rules);
        CreateL7policyRequestBody createL7policyRequestBody = new CreateL7policyRequestBody().withL7policy(
            createL7policyReq);
        CreateL7policyRequest createL7policyRequest = new CreateL7policyRequest().withBody(createL7policyRequestBody);
        // Request for adding a forwarding policy.
        L7policyResp l7Policy = null;
        try {
            CreateL7policyResponse createL7PolicyResp = elbClient.createL7policy(createL7policyRequest);
            l7Policy = createL7PolicyResp.getL7policy();
        } catch (Exception e) {
            logger.error("elbv2 create l7policy error: ", e);
            return;
        }

        // Change a certificate.
        UpdateCertificateRequestBody updateCertificateBody = new UpdateCertificateRequestBody().withCertificate(
                MY_CERTIFICATE)
            .withDescription("update my certificate")
            .withName("sdk-test-certificate-updated")
            .withDomain(CERTIFICATE_DOMAIN)
            .withPrivateKey(MY_PRIVATE_KEY);
        UpdateCertificateRequest updateCertificateRequest = new UpdateCertificateRequest().withCertificateId(
            certificate.getId()).withBody(updateCertificateBody);
        try {
            UpdateCertificateResponse updateCertificateResp = elbClient.updateCertificate(updateCertificateRequest);
            logger.info("elbv2 update certificate http status code: {}", updateCertificateResp.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv2 update certificate error: ", e);
            return;
        }

        // Modify the health check settings of an HTTPS listener.
        UpdateHealthmonitorReq updateHealthmonitorReq = new UpdateHealthmonitorReq().withAdminStateUp(true)
            .withTimeout(25)
            .withDelay(20)
            .withMaxRetries(10)
            .withType(pool.getProtocol().toString());
        UpdateHealthmonitorRequestBody updateHealthmonitorBody = new UpdateHealthmonitorRequestBody().withHealthmonitor(
            updateHealthmonitorReq);
        UpdateHealthmonitorRequest updateHealthmonitorRequest = new UpdateHealthmonitorRequest().withHealthmonitorId(
            healthMonitor.getId()).withBody(updateHealthmonitorBody);
        try {
            UpdateHealthmonitorResponse updateHealthMonitorResp = elbClient.updateHealthmonitor(
                updateHealthmonitorRequest);
            logger.info("elbv2 update healthmonitor http status code: {}", updateHealthMonitorResp.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv2 update healthmonitor error: ", e);
            return;
        }

        // Modify an HTTPS listener.
        UpdateListenerReq updateListenerReq = new UpdateListenerReq().withName("sdk-test-https-updated")
            .withDefaultTlsContainerRef(certificate.getId())
            .withAdminStateUp(true);
        UpdateListenerRequestBody updateListenerBody = new UpdateListenerRequestBody().withListener(updateListenerReq);
        UpdateListenerRequest updateListenerRequest = new UpdateListenerRequest().withBody(updateListenerBody)
            .withListenerId(listener.getId());
        try {
            UpdateListenerResponse updateListenerResp = elbClient.updateListener(updateListenerRequest);
            logger.info("elbv2 update listener http status code: {}", updateListenerResp.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv2 update listener error: ", e);
            return;
        }

        // Delete resources.
        deleteMember(pool, member1);
        deleteMember(pool, member2);

        // Disable a health check.
        DeleteHealthmonitorRequest deleteHealthMonitorReq = new DeleteHealthmonitorRequest();
        deleteHealthMonitorReq.withHealthmonitorId(healthMonitor.getId());
        try {
            DeleteHealthmonitorResponse deleteHealthMonitorResp = elbClient.deleteHealthmonitor(deleteHealthMonitorReq);
            logger.info("elbv2 delete HealthMonitor http status code: {}", deleteHealthMonitorResp.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv2 delete HealthMonitor error: ", e);
            return;
        }

        // Delete a forwarding policy to delete a forwarding rule.
        DeleteL7policyRequest delL7PolicyReq = new DeleteL7policyRequest();
        delL7PolicyReq.setL7policyId(l7Policy.getId());
        try {
            DeleteL7policyResponse deleteL7policyResponse = elbClient.deleteL7policy(delL7PolicyReq);
            logger.info("elbv2 delete l7policy http status code: {}", deleteL7policyResponse.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv2 delete l7policy error: ", e);
            return;
        }

        // Delete a backend server group.
        DeletePoolRequest deletePoolRequest = new DeletePoolRequest();
        deletePoolRequest.withPoolId(pool.getId());
        try {
            DeletePoolResponse deletePoolResp = elbClient.deletePool(deletePoolRequest);
            logger.info("elbv2 delete pool http status code: {}", deletePoolResp.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv2 delete pool error: ", e);
            return;
        }

        // Delete a listener.
        DeleteListenerRequest deleteListenerRequest = new DeleteListenerRequest();
        deleteListenerRequest.withListenerId(listener.getId());
        try {
            DeleteListenerResponse deleteListenerResp = elbClient.deleteListener(deleteListenerRequest);
            logger.info("elbv2 delete Listener http status code: {}", deleteListenerResp.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv2 delete listener error: ", e);
            return;
        }

        // Delete a load balancer.
        DeleteLoadbalancerRequest deleteLoadbalancerRequest = new DeleteLoadbalancerRequest();
        deleteLoadbalancerRequest.withLoadbalancerId(loadbalancer.getId());
        try {
            DeleteLoadbalancerResponse deleteLoadbalancerResp = elbClient.deleteLoadbalancer(deleteLoadbalancerRequest);
            logger.info("elbv2 delete loadbalancer http status code: {}", deleteLoadbalancerResp.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv2 delete loadbalancer error: ", e);
            return;
        }

        // Delete a certificate.
        DeleteCertificateRequest deleteCertificateRequest = new DeleteCertificateRequest();
        deleteCertificateRequest.withCertificateId(certificate.getId());
        try {
            DeleteCertificateResponse deleteCertificateResp = elbClient.deleteCertificate(deleteCertificateRequest);
            logger.info("elbv2 delete certificate http status code: {}", deleteCertificateResp.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv2 delete certificate error: ", e);
        }
    }

    /** Create a backend server group. If listener is not null, the backend server group is associated with the listener.
     *  If listener is null, the backend server group is associated with the load balancer.
    **/
    private static PoolResp createPool(LoadbalancerResp loadbalancer, ListenerResp listener, String protocol) {
        // Construct the request parameters for adding a backend server group.
        CreatePoolRequest request = new CreatePoolRequest();
        CreatePoolRequestBody body = new CreatePoolRequestBody();
        CreatePoolReq req = new CreatePoolReq();
        if (loadbalancer != null) {
            req.withLoadbalancerId(loadbalancer.getId());
        }
        if (listener != null) {
            req.withListenerId(listener.getId());
        }
        req.withLbAlgorithm(ROUND_ROBIN).withProtocol(CreatePoolReq.ProtocolEnum.valueOf(protocol));
        body.withPool(req);
        request.withBody(body);

        // Request for creating a backend server group.
        PoolResp poolResp = null;
        try {
            CreatePoolResponse creatPoolResp = elbClient.createPool(request);
            poolResp = creatPoolResp.getPool();
        } catch (Exception e) {
            logger.error("elbv2 create l7policy error: ", e);
        }
        return poolResp;
    }

    /* Configure a health check for the associated backend server group. */
    private static HealthmonitorResp createHm(PoolResp pool) {
        // Construct the request parameters for configuring a health check.
        CreateHealthmonitorReq req = new CreateHealthmonitorReq().withPoolId(pool.getId())
            .withType(CreateHealthmonitorReq.TypeEnum.fromValue(pool.getProtocol().toString()))
            .withTimeout(10)
            .withDelay(5)
            .withMaxRetries(3)
            .withPoolId("123");
        CreateHealthmonitorRequestBody body = new CreateHealthmonitorRequestBody().withHealthmonitor(req);
        CreateHealthmonitorRequest request = new CreateHealthmonitorRequest().withBody(body);

        // Request for configuring a health check.
        HealthmonitorResp healthMonitorResp = null;
        try {
            CreateHealthmonitorResponse createHmResp = elbClient.createHealthmonitor(request);
            healthMonitorResp = createHmResp.getHealthmonitor();
        } catch (Exception e) {
            logger.error("elbv2 create healthmonitor error: ", e);
        }
        return healthMonitorResp;
    }

    // Add a backend server.
    private static MemberResp createMember(LoadbalancerResp loadbalancer, PoolResp pool, String ip) {
        // Construct the request parameters for adding a backend server.
        CreateMemberReq req = new CreateMemberReq().withSubnetId(loadbalancer.getVipSubnetId())
            .withAddress(ip)
            .withProtocolPort(8080);
        CreateMemberRequestBody body = new CreateMemberRequestBody().withMember(req);
        CreateMemberRequest request = new CreateMemberRequest().withBody(body).withPoolId(pool.getId());

        // Request for adding a backend server.
        MemberResp memberResp = null;
        try {
            CreateMemberResponse createMemberResp = elbClient.createMember(request);
            memberResp = createMemberResp.getMember();
        } catch (Exception e) {
            logger.error("elbv2 create member error: ", e);
        }

        return memberResp;
    }

    private static void deleteMember(PoolResp pool, MemberResp member) {
        DeleteMemberRequest req = new DeleteMemberRequest().withPoolId(pool.getId()).withMemberId(member.getId());
        try {
            DeleteMemberResponse resp = elbClient.deleteMember(req);
            logger.info("elbv2 delete member http status code: {}", resp.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv2 delete member error: ", e);
        }
    }
}
