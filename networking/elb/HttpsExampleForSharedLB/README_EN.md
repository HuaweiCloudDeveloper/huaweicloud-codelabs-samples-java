## 1. Functions

You can integrate [Elastic Load Balance ELB SDK](https://sdkcenter.developer.huaweicloud.com/?product=ELB) provided by Huawei Cloud to call ELB APIs, making it easier for you to use the service.

You can follow the instructions in this section to create a load balancer, add an HTTPS listener, create a backend server group, configure a health check, add a backend server, and perform other operations. You can also delete these resources by following these instructions.

**Note: If you no longer use a load balancer, delete it and its related resources to avoid unnecessary charges.**

## 2. Flowchart
Creating a load balancer and related resources

![image](assets/createELBv2ResourcesEn.png)

Deleting a load balancer and related resources

![image](assets/deleteELBv2ResourcesEn.png)


## 3. Prerequisites

- You have obtained the Huawei Cloud SDK. You can also install the Java SDK. For details, see [VPC JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/VPC?lang=Java).
- To use the Huawei Cloud Java SDK, you must have a Huawei Cloud account and obtain the AK and SK of the account. For details, see [Obtaining an AK/SK](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- The Java version is **1.8** or later.

## 4. Obtaining and Installing the SDK

You can use Maven to configure the ELB SDK. 

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-elb</artifactId>
    <version>3.0.69</version>
</dependency>
```

## 5. Key Code Snippets

### 5.1. Creating a Load Balancer
A load balancer distributes incoming traffic across multiple backend servers. Before using a load balancer to route requests, you need to add at least one listener to it and associate one backend server with it.
```java
Specify the ID of the IPv4 subnet where the load balancer works.
String vipSubnetId = "xxx";

// Construct the request parameters for creating a load balancer.
CreateLoadbalancerReq createLoadbalancerReq = new CreateLoadbalancerReq().withName("sdk-test-elb")
    .withVipSubnetId(vipSubnetId);
CreateLoadbalancerRequestBody createLoadbalancerBody = new CreateLoadbalancerRequestBody().withLoadbalancer(
    createLoadbalancerReq);
CreateLoadbalancerRequest createLoadbalancerRequest = new CreateLoadbalancerRequest().withBody(
    createLoadbalancerBody);

LoadbalancerResp loadbalancer = null;
// Request for creating a load balancer.
try {
    CreateLoadbalancerResponse createLoadbalancerResp = elbClient.createLoadbalancer(createLoadbalancerRequest);
    loadbalancer = createLoadbalancerResp.getLoadbalancer();
} catch (Exception e) {
    logger.error("elbv2 create loadbalancer error: ", e);
    return;
}
```

### 5.2 Adding a Backend Cloud Server Group
A backend server group is a logical collection of one or more backend servers to receive massive concurrent requests at the same time. A backend server can be an ECS, BMS, supplementary network interface, or IP address.
```java
// Construct the request parameters for adding a backend server group.
CreatePoolRequest request = new CreatePoolRequest();
CreatePoolRequestBody body = new CreatePoolRequestBody();
CreatePoolReq req = new CreatePoolReq();
if (loadbalancer != null) {
    req.withLoadbalancerId(loadbalancer.getId());
}
if (listener != null) {
    req.withListenerId(listener.getId());
}
req.withLbAlgorithm(ROUND_ROBIN).withProtocol(CreatePoolReq.ProtocolEnum.valueOf(protocol));
body.withPool(req);
request.withBody(body);

// Request for creating a backend server group.
PoolResp poolResp = null;
try {
    CreatePoolResponse creatPoolResp = elbClient.createPool(request);
    poolResp = creatPoolResp.getPool();
} catch (Exception e) {
    logger.error("elbv2 create l7policy error: ", e);
}
```

### 5.3. Configuring a Health Check

```java
// Construct the request parameters for configuring a health check.
CreateHealthmonitorReq req = new CreateHealthmonitorReq().withPoolId(pool.getId())
    .withType(CreateHealthmonitorReq.TypeEnum.fromValue(pool.getProtocol().toString()))
    .withTimeout(10)
    .withDelay(5)
    .withMaxRetries(3)
    .withPoolId("123");
CreateHealthmonitorRequestBody body = new CreateHealthmonitorRequestBody().withHealthmonitor(req);
CreateHealthmonitorRequest request = new CreateHealthmonitorRequest().withBody(body);

// Request for configuring a health check.
HealthmonitorResp healthMonitorResp = null;
try {
    CreateHealthmonitorResponse createHmResp = elbClient.createHealthmonitor(request);
    healthMonitorResp = createHmResp.getHealthmonitor();
} catch (Exception e) {
    logger.error("elbv2 create healthmonitor error: ", e);
}
```

### 5.4 Adding a Backend Cloud Server
A backend server processes client requests forwarded by a load balancer.
```java
// Construct the request parameters for adding a backend server.
CreateMemberReq req = new CreateMemberReq().withSubnetId(loadbalancer.getVipSubnetId())
    .withAddress(ip)
    .withProtocolPort(8080);
CreateMemberRequestBody body = new CreateMemberRequestBody().withMember(req);
CreateMemberRequest request = new CreateMemberRequest().withBody(body).withPoolId(pool.getId());

// Request for adding a backend server.
MemberResp memberResp = null;
try {
    CreateMemberResponse createMemberResp = elbClient.createMember(request);
    memberResp = createMemberResp.getMember();
} catch (Exception e) {
    logger.error("elbv2 create member error: ", e);
}
```

### 5.5. Adding a Certificate
When adding an HTTPS listener, you need to bind a server certificate to it. A server certificate is used for SSL handshake negotiations if an HTTPS listener is used. Both the certificate content and private key are required.
```java
// Add a certificate.
CreateCertificateRequestBody createCertificateBody = new CreateCertificateRequestBody().withCertificate(
    MY_CERTIFICATE)
    .withPrivateKey(MY_PRIVATE_KEY)
    .withType(CERTIFICATE_TYPE_SERVER)
    .withDomain(CERTIFICATE_DOMAIN)
    .withName("sdk-test-certificate");
CreateCertificateRequest createCertificateRequest = new CreateCertificateRequest().withBody(
    createCertificateBody);
CreateCertificateResponse certificate = null;
try {
    certificate = elbClient.createCertificate(createCertificateRequest);
} catch (Exception e) {
    logger.error("elbv2 create certificate error: ", e);
    return;
}
```

### 5.6 Adding a Listener
After you create a load balancer, add at least one listener to it. This listener receives requests from clients and routes requests to backend servers using the protocol, port, and load balancing algorithm you select.
```java
// Construct the request parameters for adding an HTTPS listener.
CreateListenerReq createListenerReq = new CreateListenerReq().withLoadbalancerId(loadbalancer.getId())
    .withProtocol(CreateListenerReq.ProtocolEnum.fromValue(PROTOCOL_HTTPS))
    .withProtocolPort(443)
    .withDefaultTlsContainerRef(certificate.getId()) // Bind the server certificate you have added in the previous step to this listener.
    .withName("sdk-test-https");
CreateListenerRequestBody createListenerBody = new CreateListenerRequestBody().withListener(createListenerReq);
CreateListenerRequest createListenerRequest = new CreateListenerRequest().withBody(createListenerBody);

// Request for adding an HTTPS listener.
ListenerResp listener = null;
try {
    CreateListenerResponse createListenerResp = elbClient.createListener(createListenerRequest);
    listener = createListenerResp.getListener();
} catch (Exception e) {
    logger.error("elbv2 create listener error: ", e);
    return;
}
```

### 5.7. Adding a Forwarding Policy
You can add forwarding policies to HTTP or HTTPS listeners to forward requests to different backend server groups based on domain names or URLs.
```java
// Add a forwarding policy and a forwarding rule.
// Add a forwarding rule.
List<CreateL7ruleReqInPolicy> rules = new ArrayList<>();
CreateL7ruleReqInPolicy createL7ruleReqInPolicy = new CreateL7ruleReqInPolicy().withType(
    CreateL7ruleReqInPolicy.TypeEnum.fromValue("PATH"))
    .withCompareType("EQUAL_TO")
    .withValue("/elb/test");
rules.add(createL7ruleReqInPolicy);
CreateL7policyReq createL7policyReq = new CreateL7policyReq().withListenerId(listener.getId())
    .withAction(CreateL7policyReq.ActionEnum.fromValue("REDIRECT_TO_POOL"))
    .withRedirectPoolId(pool.getId())
    .withName("sdk-test-l7policy")
    .withRules(rules);
CreateL7policyRequestBody createL7policyRequestBody = new CreateL7policyRequestBody().withL7policy(
    createL7policyReq);
CreateL7policyRequest createL7policyRequest = new CreateL7policyRequest().withBody(createL7policyRequestBody);
// Request for adding a forwarding policy and a forwarding rule.
L7policyResp l7Policy = null;
try {
    CreateL7policyResponse createL7PolicyResp = elbClient.createL7policy(createL7policyRequest);
    l7Policy = createL7PolicyResp.getL7policy();
} catch (Exception e) {
    logger.error("elbv2 create l7policy error: ", e);
    return;
}
```

### 5.8 Changing a Certificate

```java
// Change a certificate.
UpdateCertificateRequestBody updateCertificateBody = new UpdateCertificateRequestBody().withCertificate(
    MY_CERTIFICATE)
    .withDescription("update my certificate")
    .withName("sdk-test-certificate-updated")
    .withDomain(CERTIFICATE_DOMAIN)
    .withPrivateKey(MY_PRIVATE_KEY);
UpdateCertificateRequest updateCertificateRequest = new UpdateCertificateRequest().withCertificateId(
    certificate.getId()).withBody(updateCertificateBody);
try {
    UpdateCertificateResponse updateCertificateResp = elbClient.updateCertificate(updateCertificateRequest);
    logger.info("elbv2 update certificate http status code: {}", updateCertificateResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 update certificate error: ", e);
    return;
}
```

### 5.9. Modifying a Health Check

```java
// Modify the health check settings of an HTTPS listener.
UpdateHealthmonitorReq updateHealthmonitorReq = new UpdateHealthmonitorReq().withAdminStateUp(true)
    .withTimeout(25)
    .withDelay(20)
    .withMaxRetries(10)
    .withType(pool.getProtocol().toString());
UpdateHealthmonitorRequestBody updateHealthmonitorBody = new UpdateHealthmonitorRequestBody().withHealthmonitor(
    updateHealthmonitorReq);
UpdateHealthmonitorRequest updateHealthmonitorRequest = new UpdateHealthmonitorRequest().withHealthmonitorId(
    healthMonitor.getId()).withBody(updateHealthmonitorBody);
try {
    UpdateHealthmonitorResponse updateHealthMonitorResp = elbClient.updateHealthmonitor(
        updateHealthmonitorRequest);
    logger.info("elbv2 update healthmonitor http status code: {}", updateHealthMonitorResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 update healthmonitor error: ", e);
    return;
}
```

### 5.10. Modifying a Listener

```java
// Modify a listener.
UpdateListenerReq updateListenerReq = new UpdateListenerReq().withName("sdk-test-https-updated")
    .withDefaultTlsContainerRef(certificate.getId())
    .withAdminStateUp(true);
UpdateListenerRequestBody updateListenerBody = new UpdateListenerRequestBody().withListener(updateListenerReq);
UpdateListenerRequest updateListenerRequest = new UpdateListenerRequest().withBody(updateListenerBody)
    .withListenerId(listener.getId());
try {
    UpdateListenerResponse updateListenerResp = elbClient.updateListener(updateListenerRequest);
    logger.info("elbv2 update listener http status code: {}", updateListenerResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 update listener error: ", e);
    return;
}
```

### 5.11. Removing a Backend Server

```java
DeleteMemberRequest req = new DeleteMemberRequest().withPoolId(pool.getId()).withMemberId(member.getId());
try {
    DeleteMemberResponse resp = elbClient.deleteMember(req);
    logger.info("elbv2 delete member http status code: {}", resp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 delete member error: ", e);
}
```

### 5.12. Disabling a Health Check

```java
// Disable a health check.
DeleteHealthmonitorRequest deleteHealthMonitorReq = new DeleteHealthmonitorRequest();
deleteHealthMonitorReq.withHealthmonitorId(healthMonitor.getId());
try {
    DeleteHealthmonitorResponse deleteHealthMonitorResp = elbClient.deleteHealthmonitor(deleteHealthMonitorReq);
    logger.info("elbv2 delete HealthMonitor http status code: {}", deleteHealthMonitorResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 delete HealthMonitor error: ", e);
    return;
}
```

### 5.13. Deleting a Forwarding Rule

```java
// Delete a forwarding policy to delete a forwarding rule.
DeleteL7policyRequest delL7PolicyReq = new DeleteL7policyRequest();
delL7PolicyReq.setL7policyId(l7Policy.getId());
try {
    DeleteL7policyResponse deleteL7policyResponse = elbClient.deleteL7policy(delL7PolicyReq);
    logger.info("elbv2 delete l7policy http status code: {}", deleteL7policyResponse.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 delete l7policy error: ", e);
    return;
}
```

### 5.14. Deleting a Backend Server Group

```java
// Delete a backend server group.
DeletePoolRequest deletePoolRequest = new DeletePoolRequest();
deletePoolRequest.withPoolId(pool.getId());
try {
    DeletePoolResponse deletePoolResp = elbClient.deletePool(deletePoolRequest);
    logger.info("elbv2 delete pool http status code: {}", deletePoolResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 delete pool error: ", e);
    return;
}
```

### 5.15. Deleting a Listener

```java
// Delete a listener.
DeleteListenerRequest deleteListenerRequest = new DeleteListenerRequest();
deleteListenerRequest.withListenerId(listener.getId());
try {
    DeleteListenerResponse deleteListenerResp = elbClient.deleteListener(deleteListenerRequest);
    logger.info("elbv2 delete Listener http status code: {}", deleteListenerResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 delete listener error: ", e);
    return;
}
```

### 5.16. Deleting a Load Balancer

```java
// Delete a load balancer.
DeleteLoadbalancerRequest deleteLoadbalancerRequest = new DeleteLoadbalancerRequest();
deleteLoadbalancerRequest.withLoadbalancerId(loadbalancer.getId());
try {
    DeleteLoadbalancerResponse deleteLoadbalancerResp = elbClient.deleteLoadbalancer(deleteLoadbalancerRequest);
    logger.info("elbv2 delete loadbalancer http status code: {}", deleteLoadbalancerResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 delete loadbalancer error: ", e);
    return;
}
```

### 5.17. Deleting a Certificate

```java
// Delete a certificate.
DeleteCertificateRequest deleteCertificateRequest = new DeleteCertificateRequest();
deleteCertificateRequest.withCertificateId(certificate.getId());
try {
    DeleteCertificateResponse deleteCertificateResp = elbClient.deleteCertificate(deleteCertificateRequest);
    logger.info("elbv2 delete certificate http status code: {}", deleteCertificateResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 delete certificate error: ", e);
}
```

## 6. Example Returned Results

Creating a load balancer

```json
{
    "loadbalancer": {
        "description": "simple lb",
        "provisioning_status": "ACTIVE",
        "tenant_id": "601240b9c5c94059b63d484c92cfe308",
        "project_id": "601240b9c5c94059b63d484c92cfe308",
        "created_at": "2019-01-19T05:50:23",
        "admin_state_up": true,
        "updated_at": "2019-01-19T05:50:23",
        "id": "9fc511bf-5961-4cfc-860a-f9872b7b7857",
        "pools": [],
        "listeners": [],
        "vip_port_id": "d23c1637-c45c-46eb-aec6-70acf4664184",
        "operating_status": "ONLINE",
        "vip_address": "192.168.0.100",
        "enterprise_project_id": "0aad99bc-f5f6-4f78-8404-c598d76b0ed2",
        "vip_subnet_id": "47c04820-480d-403d-80e5-ee6c4d11d5b0",
        "provider": "vlb",
        "tags": [],
        "name": "loadbalancer1"
    }
}
```

Adding a backend server group

```json
{
    "pool": {
        "lb_algorithm": "ROUND_ROBIN",
        "protocol": "HTTP",
        "description": "",
        "admin_state_up": true,
        "loadbalancers": [
            {
                "id": "63ad9dfe-4750-479f-9630-ada43ccc8117"
            }
        ],
        "tenant_id": "601240b9c5c94059b63d484c92cfe308",
        "project_id": "601240b9c5c94059b63d484c92cfe308",
        "session_persistence": {
            "persistence_timeout": 1440,
            "cookie_name": null,
            "type": "HTTP_COOKIE"
        },
        "healthmonitor_id": null,
        "listeners": [],
        "members": [],
        "id": "d46eab56-d76b-4cd3-8952-3c3c4cf113aa",
        "name": ""
    }
}
```

Configuring a health check

```json
{
    "healthmonitor": {
        "name": "",
        "admin_state_up": true,
        "tenant_id": "145483a5107745e9b3d80f956713e6a3",
        "project_id": "145483a5107745e9b3d80f956713e6a3",
        "domain_name": "www.test.com",
        "delay": 10,
        "expected_codes": "200",
        "max_retries": 10,
        "http_method": "GET",
        "timeout": 10,
        "pools": [
            {
                "id": "bb44bffb-05d9-412c-9d9c-b189d9e14193"
            }
        ],
        "url_path": "/",
        "type": "HTTP",
        "id": "2dca3867-98c5-4cde-8f2c-b89ae6bd7e36",
        "monitor_port": 112
    }
}
```

Adding a backend server

```json
{
    "member": {
        "name": "member-jy-tt-1",
        "weight": 1,
        "admin_state_up": true,
        "subnet_id": "33d8b01a-bbe6-41f4-bc45-78a1d284d503",
        "tenant_id": "145483a5107745e9b3d80f956713e6a3",
        "project_id": "145483a5107745e9b3d80f956713e6a3",
        "address": "192.168.44.11",
        "protocol_port": 88,
        "operating_status": "ONLINE",
        "id": "c0042496-e220-44f6-914b-e6ca33bab503"
    }
}
```

Adding a certificate

```json
{
    "domain": "www.elb.com",
    "update_time": "2017-12-04 06:49:13",
    "create_time": "2017-12-04 06:49:13",
    "id": "3d8a7a02f87a40ed931b719edfe75451",
    "private_key": "-----BEGIN RSA PRIVATE KEY-----xxx-----END RSA PRIVATE KEY-----",
    "tenant_id": "930600df07ac4f66964004041bd3deaf",
    "type": "server",
    "certificate": "-----BEGIN CERTIFICATE-----xxx-----END CERTIFICATE-----",
    "name": "https_certificate",
    "description": "description for certificate"
}
```

Adding a listener

```json
{
    "listener": {
        "insert_headers": {
            "X-Forwarded-ELB-IP": true
        },
        "protocol_port": 81,
        "protocol": "TCP",
        "description": "",
        "default_tls_container_ref": null,
        "sni_container_refs": [],
        "loadbalancers": [
            {
                "id": "0416b6f1-877f-4a51-987e-978b3f084253"
            }
        ],
        "tenant_id": "601240b9c5c94059b63d484c92cfe308",
        "created_at": "2019-03-06T04:00:07",
        "client_ca_tls_container_ref": null,
        "connection_limit": -1,
        "updated_at": "2019-03-06T04:00:07",
        "http2_enable": false,
        "project_id": "601240b9c5c94059b63d484c92cfe308",
        "admin_state_up": true,
        "default_pool_id": null,
        "id": "7f941f5c-80dc-43ac-8dfd-976fab49fa7a",
        "tags": [],
        "name": "listener-test"
    }
}
```

Adding a forwarding policy

```json
{
    "l7policy": {
        "redirect_pool_id": null,
        "description": "",
        "admin_state_up": true,
        "rules": [],
        "tenant_id": "601240b9c5c94059b63d484c92cfe308",
        "project_id": "601240b9c5c94059b63d484c92cfe308",
        "listener_id": "369f1208-ff1d-4f5a-a5e2-f1dc9c567a84",
        "redirect_url": null,
        "position": 100,
        "action": "REDIRECT_TO_LISTENER",
        "redirect_listener_id": "6edb5797-3615-4950-95eb-b60a8b1253a5",
        "provisioning_status": "ACTIVE",
        "id": "98da7cdb-64a8-40d8-bd1f-c5b1f48dbf07",
        "name": ""
    }
}
```

Changing a certificate

```json
{
    "certificate": "MIICwjCC...rrN0=",
    "create_time": "2017-02-25 09:35:27",
    "description": "description for certificate",
    "domain": "www.elb.com",
    "id": "23ef9aad4ecb463580476d324a6c71af",
    "name": "https_certificate",
    "private_key": "-----BEGIN RSA PRIVATE KEY-----xxx-----END RSA PRIVATE KEY-----",
    "type": "server",
    "update_time": "2017-02-25 09:35:27"
}
```

Modifying a health check

```json
{
    "healthmonitor": {
        "name": "health-xx",
        "admin_state_up": true,
        "tenant_id": "145483a5107745e9b3d80f956713e6a3",
        "project_id": "145483a5107745e9b3d80f956713e6a3",
        "domain_name": null,
        "delay": 15,
        "expected_codes": "200",
        "max_retries": 10,
        "http_method": "GET",
        "timeout": 12,
        "pools": [
            {
                "id": "bb44bffb-05d9-412c-9d9c-b189d9e14193"
            }
        ],
        "url_path": "/",
        "type": "HTTP",
        "id": "2dca3867-98c5-4cde-8f2c-b89ae6bd7e36",
        "monitor_port": 112
    }
}
```

Modifying a listener

```json
{
    "listener": {
        "client_ca_tls_container_ref": "417a0976969f497db8cbb083bff343ba",
        "protocol": "TERMINATED_HTTPS",
        "description": "my listener",
        "default_tls_container_ref": "23b58a961a4d4c95be585e98046e657a",
        "admin_state_up": true,
        "http2_enable": false,
        "loadbalancers": [
            {
                "id": "165b6a38-5278-4569-b747-b2ee65ea84a4"
            }
        ],
        "tenant_id": "601240b9c5c94059b63d484c92cfe308",
        "project_id": "601240b9c5c94059b63d484c92cfe308",
        "sni_container_refs": [],
        "tags": [],
        "connection_limit": -1,
        "protocol_port": 443,
        "default_pool_id": "c61310de-9a06-4f0c-850c-6f4797b9984c",
        "id": "f622c150-72f5-4263-a47a-e5003c652aa3",
        "name": "listener-jy-test2",
        "created_at": "2018-07-25T01:54:13",
        "updated_at": "2018-07-25T01:54:14",
        "insert_headers": {
            "X-Forwarded-ELB-IP": true
        }
    }
}
```

## 7. References

- API References
  - Creating a load balancer [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=CreateLoadbalancer)
  - Creating a backend server group [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=CreatePool)
  - Configuring a health check [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=CreateHealthmonitor)
  - Adding a backend server [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=CreateMember)
  - Adding a certificate [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=CreateCertificate)
  - Adding a listener [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=CreateListener)
  - Adding a forwarding policy [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=CreateL7policy)
  - Changing a certificate [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=UpdateCertificate)
  - Modifying a health check [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=UpdateHealthmonitor)
  - Modifying a listener [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v2&api=UpdateListener)
- SDK reference [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/ELB?lang=Java)
- Obtaining an AK/SK [My Credential_User Guide_Huawei Cloud (huaweicloud.com)](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html)

## 8. Change History

|  Release Date | Issue|   Description  |
| :--------: | :------: | :----------: |
| 2023-08-21 |   1.0    | This issue is the first official release.|
