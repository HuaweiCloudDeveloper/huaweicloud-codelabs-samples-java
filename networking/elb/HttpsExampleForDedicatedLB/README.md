## 1、功能介绍

华为云提供了[弹性负载均衡 ELB SDK](https://sdkcenter.developer.huaweicloud.com/?product=ELB)， 您可以直接集成SDK来调用相关API，从而实现对ELB服务的快速操作。

本章示例介绍如何创建支持HTTPS协议的负载均衡，包括负载均衡器、监听器、后端服务器组、健康检查、后端服务器等资源， 并将这些资源关联起来，组成一个完整的HTTPS协议的负载均衡。最后也包括如何释放这些负载均衡资源。

**注意：示例中创建的ELB为按需计费实例，如不再使用请及时删除相关资源。**

## 2、流程图
创建ELB资源流程图

![image](assets/createELBv3Resources.png)

删除ELB资源流程图

![image](assets/deleteELBv3Resources.png)

## 3、前置条件

- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。具体请参见[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/ELB?lang=Java)。
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。具体请参见[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)。
- 华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。

## 4、SDK获取和安装

您可以通过Maven配置所依赖的虚拟私有云服务SDK

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-elb</artifactId>
    <version>3.1.53</version>
</dependency>
```

## 5、关键代码片段

### 5.1、创建负载均衡器
负载均衡器是指您创建的承载业务的负载均衡服务实体。创建负载均衡器后，您还需要在负载均衡器中添加监听器和后端服务器，然后才能使用负载均衡服务提供的功能。
```java
// 子网ID
String neutronSubnetId = "xxx";
// 若需要支持多az，则azCodes中添加多个az code即可
List<String> azCodes = Arrays.asList("xxx");

String l4flavorId = "xxx";
String l7flavorId = "xxx";

// 构造创建LB的请求参数
CreateLoadBalancerOption createLoadbalancerOption = new CreateLoadBalancerOption().withName("sdk-test-elb")
    .withAvailabilityZoneList(azCodes)
    .withVipSubnetCidrId(neutronSubnetId)
    .withL4FlavorId(l4flavorId)
    .withL7FlavorId(l7flavorId);
CreateLoadBalancerRequestBody createLoadbalancerBody = new CreateLoadBalancerRequestBody().withLoadbalancer(
    createLoadbalancerOption);
CreateLoadBalancerRequest createLoadbalancerReq = new CreateLoadBalancerRequest().withBody(
    createLoadbalancerBody);

// 请求创建 LB
LoadBalancer loadbalancer = null;
try {
    CreateLoadBalancerResponse createLoadbalancerResp = elbClient.createLoadBalancer(createLoadbalancerReq);
    loadbalancer = createLoadbalancerResp.getLoadbalancer();
} catch (Exception e) {
    logger.error("elbv3 create loadbalancer error: ", e);
    return;
}
```

### 5.2、创建后端服务器组
后端服务器组是一个或多个后端服务器的逻辑集合，用于将客户端的流量转发到一个或多个后端服务器，满足用户同时处理海量并发业务的需求。后端服务器可以是云服务器实例、辅助弹性网卡或IP地址。
```java
// 构造创建pool的请求参数
CreatePoolRequest request = new CreatePoolRequest();
CreatePoolRequestBody body = new CreatePoolRequestBody();
CreatePoolOption option = new CreatePoolOption();
if (loadbalancer != null) {
    option.setLoadbalancerId(loadbalancer.getId());
}
if (listener != null) {
    option.setListenerId(listener.getId());
}
option.withLbAlgorithm(ROUND_ROBIN);
option.withProtocol(protocol);
body.setPool(option);
request.setBody(body);

// 请求创建pool
Pool pool = null;
try {
    CreatePoolResponse creatPoolResp = elbClient.createPool(request);
    pool = creatPoolResp.getPool();
} catch (Exception e) {
    logger.error("elbv3 create pool error: ", e);
}
```

### 5.3、创建健康检查

```java
// 构造创建health monitor的请求参数
CreateHealthMonitorOption option = new CreateHealthMonitorOption().withPoolId(pool.getId())
    .withType(pool.getProtocol())
    .withDelay(5)
    .withMaxRetries(3)
    .withTimeout(10);
CreateHealthMonitorRequestBody body = new CreateHealthMonitorRequestBody().withHealthmonitor(option);
CreateHealthMonitorRequest request = new CreateHealthMonitorRequest().withBody(body);

// 请求创建healthMonitor
HealthMonitor healthMonitor = null;
try {
    CreateHealthMonitorResponse createHmResp = elbClient.createHealthMonitor(request);
    healthMonitor = createHmResp.getHealthmonitor();
} catch (Exception e) {
    logger.error("elbv3 create healthmonitor error: ", e);
}
```

### 5.4、创建后端服务器
负载均衡器会将客户端的请求转发给后端服务器处理。
```java
// 构造创建member的请求参数
CreateMemberOption option = new CreateMemberOption().withSubnetCidrId(loadbalancer.getVipSubnetCidrId())
    .withAddress(ip)
    .withProtocolPort(8080);
CreateMemberRequestBody body = new CreateMemberRequestBody().withMember(option);
CreateMemberRequest request = new CreateMemberRequest().withBody(body).withPoolId(pool.getId());

// 请求创建member
Member member = null;
try {
    CreateMemberResponse createMemberResp = elbClient.createMember(request);
    member = createMemberResp.getMember();
} catch (Exception e) {
    logger.error("elbv3 create member error: ", e);
}
```

### 5.5、创建证书
配置HTTPS监听器时，需要为监听器绑定服务器证书。在使用HTTPS协议时，服务器证书用于SSL握手协商，需提供证书内容和私钥。
```java
// 创建证书对象
CreateCertificateOption createCertificateOption = new CreateCertificateOption().withCertificate(MY_CERTIFICATE)
    .withPrivateKey(MY_PRIVATE_KEY)
    .withType(CERTIFICATE_TYPE_SERVER)
    .withDomain(CERTIFICATE_DOMAIN)
    .withName("sdk-test-certificate");
CreateCertificateRequestBody createCertificateBody = new CreateCertificateRequestBody().withCertificate(
    createCertificateOption);
CreateCertificateRequest createCertificateReq = new CreateCertificateRequest().withBody(createCertificateBody);
CertificateInfo certificate = null;
try {
    CreateCertificateResponse createCertificateResp = elbClient.createCertificate(createCertificateReq);
    certificate = createCertificateResp.getCertificate();
} catch (Exception e) {
    logger.error("elbv3 create certificate error: ", e);
    return;
}
```

### 5.6、创建监听器
创建负载均衡器后，需要为负载均衡器配置监听器。监听器负责监听负载均衡器上的请求，根据配置流量分配策略，分发流量到后端服务器处理。
```java
// 构造创建 https listener的请求参数
CreateListenerOption createListenerOption = new CreateListenerOption().withLoadbalancerId(loadbalancer.getId())
    .withProtocol(PROTOCOL_HTTPS)
    .withProtocolPort(443)
    .withDefaultTlsContainerRef(certificate.getId())
    .withName("sdk-test-https");
CreateListenerRequestBody createListenerBody = new CreateListenerRequestBody().withListener(
    createListenerOption);
CreateListenerRequest createListenerReq = new CreateListenerRequest().withBody(createListenerBody);

// 请求创建https listener
Listener listener = null;
try {
    CreateListenerResponse createListenerResp = elbClient.createListener(createListenerReq);
    listener = createListenerResp.getListener();
} catch (Exception e) {
    logger.error("elbv3 create listener error: ", e);
    return;
}
```

### 5.7、创建转发规则
每个监听器都具有默认转发策略，您也可以选择定义其他转发策略。每条转发策略由优先级、一个或多个转发规则以及转发动作组成。您可以随时添加或编辑转发策略。
```java
// 创建监听器下的转发策略和转发规则
// 转发规则
List<CreateL7PolicyRuleOption> rules = new ArrayList<>();
CreateL7PolicyRuleOption ruleOption = new CreateL7PolicyRuleOption().withCompareType("EQUAL_TO")
    .withType("PATH")
    .withValue("/elb/test");
rules.add(ruleOption);
CreateL7PolicyOption createl7PolicyOption = new CreateL7PolicyOption().withListenerId(listener.getId())
    .withAction("REDIRECT_TO_POOL")
    .withRedirectPoolId(pool.getId())
    .withName("sdk-test-l7policy")
    .withRules(rules);
CreateL7PolicyRequestBody createL7PolicyBody = new CreateL7PolicyRequestBody().withL7policy(
    createl7PolicyOption);
CreateL7PolicyRequest createL7PolicyReq = new CreateL7PolicyRequest().withBody(createL7PolicyBody);

// 请求创建转发策略和转发规则（可以同时创建）
L7Policy l7Policy = null;
try {
    CreateL7PolicyResponse createL7PolicyResp = elbClient.createL7Policy(createL7PolicyReq);
    l7Policy = createL7PolicyResp.getL7policy();
} catch (Exception e) {
    logger.error("elbv3 create l7policy error: ", e);
    return;
}
```

### 5.8、更新证书

```java
// 更新证书
UpdateCertificateOption updateCertificateOption = new UpdateCertificateOption().withCertificate(MY_CERTIFICATE)
    .withDescription("Update my Certificate.")
    .withName("sdk-test-certificate-updated")
    .withPrivateKey(MY_PRIVATE_KEY)
    .withDomain(CERTIFICATE_DOMAIN);
UpdateCertificateRequestBody updateCertificateBody = new UpdateCertificateRequestBody().withCertificate(
    updateCertificateOption);
UpdateCertificateRequest updateCertificateReq = new UpdateCertificateRequest().withCertificateId(
    certificate.getId()).withBody(updateCertificateBody);
try {
    UpdateCertificateResponse updateCertificateResp = elbClient.updateCertificate(updateCertificateReq);
    logger.info("elbv3 update certificate http status code: {}", updateCertificateResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv3 update certificate error: ", e);
    return;
}
```

### 5.9、更新健康检查

```java
// 更新https listener的healthMonitor
UpdateHealthMonitorOption updateHealthMonitorOption = new UpdateHealthMonitorOption().withAdminStateUp(true)
    .withDelay(20)
    .withMaxRetries(10)
    .withTimeout(25)
    .withType(pool.getProtocol());
UpdateHealthMonitorRequestBody updateHealthMonitorBody = new UpdateHealthMonitorRequestBody().withHealthmonitor(
    updateHealthMonitorOption);
UpdateHealthMonitorRequest updateHealthMonitorReq = new UpdateHealthMonitorRequest().withHealthmonitorId(
    healthMonitor.getId()).withBody(
    updateHealthMonitorBody);
try {
    UpdateHealthMonitorResponse updateHealthMonitorResp = elbClient.updateHealthMonitor(updateHealthMonitorReq);
    logger.info("elbv3 update healthmonitor http status code: {}", updateHealthMonitorResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv3 update healthmonitor error: ", e);
    return;
}
```

### 5.10、更新监听器

```java
// 更新https listener
UpdateListenerOption updateListenerOption = new UpdateListenerOption().withAdminStateUp(true)
    .withName("sdk-test-https-updated")
    .withDefaultTlsContainerRef("");
UpdateListenerRequestBody updateListenerBody = new UpdateListenerRequestBody().withListener(
    updateListenerOption);
UpdateListenerRequest updateListenerReq = new UpdateListenerRequest().withListenerId(listener.getId())
    .withBody(updateListenerBody);
try {
    UpdateListenerResponse updateListenerResp = elbClient.updateListener(updateListenerReq);
    logger.info("elbv3 update listener http status code: {}", updateListenerResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv3 update listener error: ", e);
    return;
}
```

### 5.11、删除后端服务器

```java
DeleteMemberRequest req = new DeleteMemberRequest().withPoolId(pool.getId()).withMemberId(member.getId());
try {
    DeleteMemberResponse resp = elbClient.deleteMember(req);
    logger.info("elbv3 delete member http status code: {}", resp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv3 delete member error: ", e);
}
```

### 5.12、删除健康检查

```java
// 删除健康检查
DeleteHealthMonitorRequest deleteHealthMonitorReq = new DeleteHealthMonitorRequest();
deleteHealthMonitorReq.withHealthmonitorId(healthMonitor.getId());
try {
    DeleteHealthMonitorResponse deleteHealthMonitorResp = elbClient.deleteHealthMonitor(deleteHealthMonitorReq);
    logger.info("elbv3 delete healthmonitor http status code: {}", deleteHealthMonitorResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv3 delete healthmonitor error: ", e);
    return;
}
```

### 5.13、删除转发规则

```java
// 可以通过直接删除转发策略，来删除转发规则
DeleteL7PolicyRequest delL7PolicyReq = new DeleteL7PolicyRequest();
delL7PolicyReq.setL7policyId(l7Policy.getId());
try {
    DeleteL7PolicyResponse deleteL7PolicyResponse = elbClient.deleteL7Policy(delL7PolicyReq);
    logger.info("elbv3 delete l7policy http status code: {}", deleteL7PolicyResponse.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv3 delete l7policy error: ", e);
    return;
}
```

### 5.14、删除后端服务器组

```java
// 删除后端服务器组
DeletePoolRequest deletePoolReq = new DeletePoolRequest();
deletePoolReq.withPoolId(pool.getId());
try {
    DeletePoolResponse deletePoolResp = elbClient.deletePool(deletePoolReq);
    logger.info("elbv3 delete pool http status code: {}", deletePoolResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv3 delete pool error: ", e);
    return;
}
```

### 5.15、删除监听器

```java
// 删除监听器
DeleteListenerRequest deleteListenerReq = new DeleteListenerRequest();
deleteListenerReq.withListenerId(listener.getId());
try {
    DeleteListenerResponse deleteListenerResp = elbClient.deleteListener(deleteListenerReq);
    logger.info("elbv3 delete listener http status code: {}", deleteListenerResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv3 delete listener error: ", e);
    return;
}
```

### 5.16、删除负载均衡器

```java
// 删除负载均衡器
DeleteLoadBalancerRequest deleteLoadBalancerReq = new DeleteLoadBalancerRequest();
deleteLoadBalancerReq.withLoadbalancerId(loadbalancer.getId());
try {
    DeleteLoadBalancerResponse deleteLoadBalancerResp = elbClient.deleteLoadBalancer(deleteLoadBalancerReq);
    logger.info("elbv3 delete loadbalancer http status code: {}", deleteLoadBalancerResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv3 delete loadbalancer error: ", e);
    return;
}
```

### 5.17、删除证书

```java
// 删除证书
DeleteCertificateRequest deleteCertificateRequest = new DeleteCertificateRequest();
deleteCertificateRequest.withCertificateId(certificate.getId());
try {
    DeleteCertificateResponse deleteCertificateResp = elbClient.deleteCertificate(deleteCertificateRequest);
    logger.info("elbv2 delete certificate http status code: {}", deleteCertificateResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 delete certificate error: ", e);
}
```

## 6、返回结果示例

创建负载均衡器

```json
{
    "loadbalancer": {
        "name": "my_loadbalancer",
        "id": "29cc669b-3ac8-4498-9094-bdf6193425c2",
        "project_id": "060576798a80d5762fafc01a9b5eedc7",
        "description": "",
        "vip_port_id": "98697944-0cc7-4d3b-a829-001c2fb82232",
        "vip_address": "192.168.0.214",
        "admin_state_up": true,
        "provisioning_status": "ACTIVE",
        "operating_status": "ONLINE",
        "listeners": [],
        "pools": [],
        "tags": [
            {
                "key": "tab_key",
                "value": "tag1"
            }
        ],
        "provider": "vlb",
        "created_at": "2023-03-22T07:59:57Z",
        "updated_at": "2023-03-22T07:59:59Z",
        "vpc_id": "a1f33a4c-95b9-48a7-9350-684e2ed844b3",
        "enterprise_project_id": "134f2181-5720-47e7-bd78-1356ed3737d6",
        "availability_zone_list": [],
        "ipv6_vip_address": null,
        "ipv6_vip_virsubnet_id": null,
        "ipv6_vip_port_id": null,
        "publicips": [
            {
                "publicip_id": "3388574a-4f6f-4471-869e-97d74d21eee9",
                "publicip_address": "88.88.87.205",
                "ip_version": 4
            }
        ],
        "global_eips": [],
        "elb_virsubnet_ids": [],
        "elb_virsubnet_type": null,
        "ip_target_enable": false,
        "autoscaling": {
            "enable": false,
            "min_l7_flavor_id": ""
        },
        "frozen_scene": null,
        "public_border_group": "center",
        "eips": [
            {
                "eip_id": "3388574a-4f6f-4471-869e-97d74d21eee9",
                "eip_address": "88.88.87.205",
                "ip_version": 4
            }
        ],
        "guaranteed": false,
        "billing_info": null,
        "l4_flavor_id": null,
        "l4_scale_flavor_id": null,
        "l7_flavor_id": null,
        "l7_scale_flavor_id": null,
        "waf_failure_action": "",
        "vip_subnet_cidr_id": "abf31f3b-706e-4e55-a6dc-f2fcc707fd3a"
    },
    "request_id": "bf29597181cb81b30d19f1a0115a157d"
}
```

创建后端服务器组

```json
{
    "pool": {
        "type": "",
        "vpc_id": "",
        "lb_algorithm": "LEAST_CONNECTIONS",
        "protocol": "TCP",
        "description": "",
        "admin_state_up": true,
        "member_deletion_protection_enable": false,
        "loadbalancers": [
            {
                "id": "098b2f68-af1c-41a9-8efd-69958722af62"
            }
        ],
        "project_id": "99a3fff0d03c428eac3678da6a7d0f24",
        "session_persistence": null,
        "healthmonitor_id": null,
        "listeners": [
            {
                "id": "0b11747a-b139-492f-9692-2df0b1c87193"
            }
        ],
        "members": [],
        "id": "36ce7086-a496-4666-9064-5ba0e6840c75",
        "name": "My pool",
        "ip_version": "v4",
        "slow_start": null
    },
    "request_id": "2d974978-0733-404d-a21a-b29204f4803a"
}
```

创建健康检查

```json
{
    "request_id": "0e837340-f1bd-4037-8f61-9923d0f0b19e",
    "healthmonitor": {
        "monitor_port": null,
        "id": "c2b210b2-60c4-449d-91e2-9e9ea1dd7441",
        "project_id": "99a3fff0d03c428eac3678da6a7d0f24",
        "domain_name": null,
        "name": "My Healthmonitor",
        "delay": 1,
        "max_retries": 3,
        "pools": [
            {
                "id": "488acc50-6bcf-423d-8f0a-0f4184f5b8a0"
            }
        ],
        "admin_state_up": true,
        "timeout": 30,
        "type": "HTTP",
        "expected_codes": "200",
        "url_path": "/",
        "http_method": "GET"
    }
}
```

创建后端服务器

```json
{
    "member": {
        "name": "My member",
        "weight": 1,
        "admin_state_up": false,
        "subnet_cidr_id": "c09f620e-3492-4429-ac15-445d5dd9ca74",
        "project_id": "99a3fff0d03c428eac3678da6a7d0f24",
        "address": "120.10.10.16",
        "protocol_port": 89,
        "id": "1923923e-fe8a-484f-bdbc-e11559b1f48f",
        "operating_status": "NO_MONITOR",
        "status": [
            {
                "listener_id": "427eee03-b569-4d6c-b1f1-712032f7ec2d",
                "operating_status": "NO_MONITOR"
            }
        ],
        "ip_version": "v4"
    },
    "request_id": "f354090d-41db-41e0-89c6-7a943ec50792"
}
```

创建证书

```json
{
    "certificate": {
        "private_key": "-----BEGIN PRIVATE KEY-----xxx-----END PRIVATE KEY-----",
        "description": "",
        "domain": null,
        "created_at": "2019-03-31T22:23:51Z",
        "expire_time": "2045-11-17T13:25:47Z",
        "id": "233a325e5e3e4ce8beeb320aa714cc12",
        "name": "My Certificate",
        "certificate": "-----BEGIN CERTIFICATE-----xxx---END CERTIFICATE-----",
        "admin_state_up": true,
        "project_id": "99a3fff0d03c428eac3678da6a7d0f24",
        "updated_at": "2019-03-31T23:26:49Z",
        "type": "server"
    },
    "request_id": "98414965-856c-4be3-8a33-3e08432a222e"
}
```

创建监听器

```json
{
    "listener": {
        "id": "0b11747a-b139-492f-9692-2df0b1c87193",
        "name": "My listener",
        "protocol_port": 80,
        "protocol": "TCP",
        "description": null,
        "default_tls_container_ref": null,
        "admin_state_up": true,
        "loadbalancers": [
            {
                "id": "098b2f68-af1c-41a9-8efd-69958722af62"
            }
        ],
        "client_ca_tls_container_ref": null,
        "project_id": "99a3fff0d03c428eac3678da6a7d0f24",
        "sni_container_refs": [],
        "connection_limit": -1,
        "member_timeout": null,
        "client_timeout": null,
        "keepalive_timeout": null,
        "default_pool_id": null,
        "ipgroup": null,
        "tls_ciphers_policy": "tls-1-0",
        "tags": [],
        "created_at": "2019-04-02T00:12:32Z",
        "updated_at": "2019-04-02T00:12:32Z",
        "http2_enable": false,
        "enable_member_retry": true,
        "insert_headers": {
            "X-Forwarded-ELB-IP": true
        },
        "transparent_client_ip_enable": false
    },
    "request_id": "f4c4aca8-df16-42e8-8836-33e4b8e9aa8e"
}
```

创建转发策略

```json
{
    "request_id": "b60d1d9a-5263-45b0-b1d6-2810ac7c52a1",
    "l7policy": {
        "redirect_pool_id": "768e9e8c-e7cb-4fef-b24b-af9399dbb240",
        "description": "",
        "admin_state_up": true,
        "rules": [
            {
                "id": "c5c2d625-676b-431e-a4c7-c59cc2664881"
            }
        ],
        "project_id": "7a9941d34fc1497d8d0797429ecfd354",
        "listener_id": "cdb03a19-16b7-4e6b-bfec-047aeec74f56",
        "redirect_url": null,
        "redirect_url_config": null,
        "fixed_response_config": null,
        "redirect_listener_id": null,
        "action": "REDIRECT_TO_POOL",
        "position": 100,
        "priority": null,
        "provisioning_status": "ACTIVE",
        "id": "01832d99-bbd8-4340-9d0c-6ff8f7a37307",
        "name": "l7policy-67"
    }
}
```

更新证书

```json
{
    "certificate": {
        "private_key": "-----BEGIN PRIVATE KEY-----xxx-----END PRIVATE KEY-----",
        "description": "Update my Certificate.",
        "domain": null,
        "created_at": "2019-03-31T22:23:51Z",
        "expire_time": "2045-11-17T13:25:47Z",
        "id": "233a325e5e3e4ce8beeb320aa714cc12",
        "name": "My Certificate",
        "certificate": "-----BEGIN CERTIFICATE-----xxx---END CERTIFICATE-----",
        "admin_state_up": true,
        "project_id": "99a3fff0d03c428eac3678da6a7d0f24",
        "updated_at": "2019-03-31T23:26:49Z",
        "type": "server"
    },
    "request_id": "d9abea6b-98ee-4ad4-8c5d-185ded48742f"
}
```

更新健康检查

```json
{
    "request_id": "08d6ffea-d092-4cfa-860a-e364f3bef1be",
    "healthmonitor": {
        "monitor_port": null,
        "id": "c2b210b2-60c4-449d-91e2-9e9ea1dd7441",
        "project_id": "99a3fff0d03c428eac3678da6a7d0f24",
        "domain_name": null,
        "name": "My Healthmonitor update",
        "delay": 10,
        "max_retries": 10,
        "pools": [
            {
                "id": "488acc50-6bcf-423d-8f0a-0f4184f5b8a0"
            }
        ],
        "admin_state_up": true,
        "timeout": 30,
        "type": "HTTP",
        "expected_codes": "200",
        "url_path": "/",
        "http_method": "GET"
    }
}
```

更新监听器

```json
{
    "listener": {
        "id": "0b11747a-b139-492f-9692-2df0b1c87193",
        "name": "My listener",
        "protocol_port": 80,
        "protocol": "TCP",
        "description": null,
        "default_tls_container_ref": null,
        "admin_state_up": true,
        "loadbalancers": [
            {
                "id": "098b2f68-af1c-41a9-8efd-69958722af62"
            }
        ],
        "client_ca_tls_container_ref": null,
        "project_id": "99a3fff0d03c428eac3678da6a7d0f24",
        "sni_container_refs": [],
        "connection_limit": -1,
        "member_timeout": null,
        "client_timeout": null,
        "keepalive_timeout": null,
        "default_pool_id": null,
        "ipgroup": null,
        "tls_ciphers_policy": "tls-1-0",
        "tags": [],
        "created_at": "2019-04-02T00:12:32Z",
        "updated_at": "2019-04-02T00:12:32Z",
        "http2_enable": false,
        "enable_member_retry": true,
        "insert_headers": {
            "X-Forwarded-ELB-IP": true
        },
        "transparent_client_ip_enable": false
    },
    "request_id": "f4c4aca8-df16-42e8-8836-33e4b8e9aa8e"
}
```

## 7、参考链接

- API参考
  - 创建负载均衡器[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=CreateLoadBalancer)
  - 创建后端服务器组[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=CreatePool)
  - 创建健康检查[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=CreateHealthMonitor)
  - 创建后端服务器[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=CreateMember)
  - 创建证书[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=CreateCertificate)
  - 创建监听器[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=CreateListener)
  - 创建转发策略[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=CreateL7Policy)
  - 更新证书[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=UpdateCertificate)
  - 更新健康检查[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=UpdateHealthMonitor)
  - 更新监听器[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=UpdateListener)
- SDK参考[API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/ELB?lang=Java)
- AK SK获取[我的凭证_用户指南_华为云 (huaweicloud.com)](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)

## 8、修订记录

|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2023-08-21 |   1.0    | 文档首次发布 |