package com.huaweicloud.elb.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.elb.v3.ElbClient;
import com.huaweicloud.sdk.elb.v3.model.CertificateInfo;
import com.huaweicloud.sdk.elb.v3.model.CreateCertificateOption;
import com.huaweicloud.sdk.elb.v3.model.CreateCertificateRequest;
import com.huaweicloud.sdk.elb.v3.model.CreateCertificateRequestBody;
import com.huaweicloud.sdk.elb.v3.model.CreateCertificateResponse;
import com.huaweicloud.sdk.elb.v3.model.CreateHealthMonitorOption;
import com.huaweicloud.sdk.elb.v3.model.CreateHealthMonitorRequest;
import com.huaweicloud.sdk.elb.v3.model.CreateHealthMonitorRequestBody;
import com.huaweicloud.sdk.elb.v3.model.CreateHealthMonitorResponse;
import com.huaweicloud.sdk.elb.v3.model.CreateL7PolicyOption;
import com.huaweicloud.sdk.elb.v3.model.CreateL7PolicyRequest;
import com.huaweicloud.sdk.elb.v3.model.CreateL7PolicyRequestBody;
import com.huaweicloud.sdk.elb.v3.model.CreateL7PolicyResponse;
import com.huaweicloud.sdk.elb.v3.model.CreateL7PolicyRuleOption;
import com.huaweicloud.sdk.elb.v3.model.CreateListenerOption;
import com.huaweicloud.sdk.elb.v3.model.CreateListenerRequest;
import com.huaweicloud.sdk.elb.v3.model.CreateListenerRequestBody;
import com.huaweicloud.sdk.elb.v3.model.CreateListenerResponse;
import com.huaweicloud.sdk.elb.v3.model.CreateLoadBalancerOption;
import com.huaweicloud.sdk.elb.v3.model.CreateLoadBalancerRequest;
import com.huaweicloud.sdk.elb.v3.model.CreateLoadBalancerRequestBody;
import com.huaweicloud.sdk.elb.v3.model.CreateLoadBalancerResponse;
import com.huaweicloud.sdk.elb.v3.model.CreateMemberOption;
import com.huaweicloud.sdk.elb.v3.model.CreateMemberRequest;
import com.huaweicloud.sdk.elb.v3.model.CreateMemberRequestBody;
import com.huaweicloud.sdk.elb.v3.model.CreateMemberResponse;
import com.huaweicloud.sdk.elb.v3.model.CreatePoolOption;
import com.huaweicloud.sdk.elb.v3.model.CreatePoolRequest;
import com.huaweicloud.sdk.elb.v3.model.CreatePoolRequestBody;
import com.huaweicloud.sdk.elb.v3.model.CreatePoolResponse;
import com.huaweicloud.sdk.elb.v3.model.DeleteCertificateRequest;
import com.huaweicloud.sdk.elb.v3.model.DeleteCertificateResponse;
import com.huaweicloud.sdk.elb.v3.model.DeleteHealthMonitorRequest;
import com.huaweicloud.sdk.elb.v3.model.DeleteHealthMonitorResponse;
import com.huaweicloud.sdk.elb.v3.model.DeleteL7PolicyRequest;
import com.huaweicloud.sdk.elb.v3.model.DeleteL7PolicyResponse;
import com.huaweicloud.sdk.elb.v3.model.DeleteListenerRequest;
import com.huaweicloud.sdk.elb.v3.model.DeleteListenerResponse;
import com.huaweicloud.sdk.elb.v3.model.DeleteLoadBalancerRequest;
import com.huaweicloud.sdk.elb.v3.model.DeleteLoadBalancerResponse;
import com.huaweicloud.sdk.elb.v3.model.DeleteMemberRequest;
import com.huaweicloud.sdk.elb.v3.model.DeleteMemberResponse;
import com.huaweicloud.sdk.elb.v3.model.DeletePoolRequest;
import com.huaweicloud.sdk.elb.v3.model.DeletePoolResponse;
import com.huaweicloud.sdk.elb.v3.model.HealthMonitor;
import com.huaweicloud.sdk.elb.v3.model.L7Policy;
import com.huaweicloud.sdk.elb.v3.model.Listener;
import com.huaweicloud.sdk.elb.v3.model.LoadBalancer;
import com.huaweicloud.sdk.elb.v3.model.Member;
import com.huaweicloud.sdk.elb.v3.model.Pool;
import com.huaweicloud.sdk.elb.v3.model.UpdateCertificateOption;
import com.huaweicloud.sdk.elb.v3.model.UpdateCertificateRequest;
import com.huaweicloud.sdk.elb.v3.model.UpdateCertificateRequestBody;
import com.huaweicloud.sdk.elb.v3.model.UpdateCertificateResponse;
import com.huaweicloud.sdk.elb.v3.model.UpdateHealthMonitorOption;
import com.huaweicloud.sdk.elb.v3.model.UpdateHealthMonitorRequest;
import com.huaweicloud.sdk.elb.v3.model.UpdateHealthMonitorRequestBody;
import com.huaweicloud.sdk.elb.v3.model.UpdateHealthMonitorResponse;
import com.huaweicloud.sdk.elb.v3.model.UpdateListenerOption;
import com.huaweicloud.sdk.elb.v3.model.UpdateListenerRequest;
import com.huaweicloud.sdk.elb.v3.model.UpdateListenerRequestBody;
import com.huaweicloud.sdk.elb.v3.model.UpdateListenerResponse;
import com.huaweicloud.sdk.elb.v3.region.ElbRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Elbv3ApiHTTPSExample {
    private static final Logger logger = LoggerFactory.getLogger(Elbv3ApiHTTPSExample.class);

    // There will be security risks if the AK/SK used for authentication is directly written into code. Encrypt the AK/SK in the configuration file or environment variables for storage.
    // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");

    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    private static final String PROTOCOL_HTTPS = "HTTPS";

    private static final String ROUND_ROBIN = "ROUND_ROBIN";

    private static final String CERTIFICATE_TYPE_SERVER = "server";

    // Domain name of the certificate. The following example is not a real domain name.
    private static final String CERTIFICATE_DOMAIN = "www.myelb-v3-test.com";

    // Certificate and private key in PEM format. The following example certificate and private key are not valid.
    private static final String MY_PRIVATE_KEY = readFileContent("src/main/resources/privateKey.pem");

    private static final String MY_CERTIFICATE = readFileContent("src/main/resources/certificate.pem");

    private static final String MEMBER0_IP = "192.168.0.3";

    private static final String MEMBER1_IP = "192.168.0.4";

    private static ElbClient elbClient = getElbClient();

    /* Obtain the client that sends requests to the load balancer. */
    public static ElbClient getElbClient() {
        return ElbClient.newBuilder()
            .withCredential(new BasicCredentials().withAk(AK).withSk(SK))
            .withRegion(ElbRegion.CN_NORTH_1)
            .build();
    }

    private static String readFileContent(String filePath) {
        String fileContent = null;
        try {
            fileContent = new String(Files.readAllBytes(Paths.get(filePath)), Charset.defaultCharset());
        } catch (Exception e) {
            logger.error("read file content exception: ", e);
        }
        return fileContent;
    }

    public static void main(String[] argvs) {

        String neutronSubnetId = "xxx";

        List<String> azCodes = Arrays.asList("xxx");

        String l4flavorId = "xxx";
        String l7flavorId = "xxx";

        // Construct the request parameters for creating a load balancer.
        CreateLoadBalancerOption createLoadbalancerOption = new CreateLoadBalancerOption().withName("sdk-test-elb")
            .withAvailabilityZoneList(azCodes)
            .withVipSubnetCidrId(neutronSubnetId)
            .withL4FlavorId(l4flavorId)
            .withL7FlavorId(l7flavorId);
        CreateLoadBalancerRequestBody createLoadbalancerBody = new CreateLoadBalancerRequestBody().withLoadbalancer(
            createLoadbalancerOption);
        CreateLoadBalancerRequest createLoadbalancerReq = new CreateLoadBalancerRequest().withBody(
            createLoadbalancerBody);

        // Request for creating a load balancer.
        LoadBalancer loadbalancer = null;
        try {
            CreateLoadBalancerResponse createLoadbalancerResp = elbClient.createLoadBalancer(createLoadbalancerReq);
            loadbalancer = createLoadbalancerResp.getLoadbalancer();
        } catch (Exception e) {
            logger.error("elbv3 create loadbalancer error: ", e);
            return;
        }

        // Create a backend server group, configure a health check, and add a backend server to the load balancer.
        Pool pool = createPool(loadbalancer, null, PROTOCOL_HTTPS);
        HealthMonitor healthMonitor = createHm(pool);
        Member member1 = createMember(loadbalancer, pool, MEMBER0_IP);
        Member member2 = createMember(loadbalancer, pool, MEMBER1_IP);

        // Add a certificate.
        CreateCertificateOption createCertificateOption = new CreateCertificateOption().withCertificate(MY_CERTIFICATE)
            .withPrivateKey(MY_PRIVATE_KEY)
            .withType(CERTIFICATE_TYPE_SERVER)
            .withDomain(CERTIFICATE_DOMAIN)
            .withName("sdk-test-certificate");
        CreateCertificateRequestBody createCertificateBody = new CreateCertificateRequestBody().withCertificate(
            createCertificateOption);
        CreateCertificateRequest createCertificateReq = new CreateCertificateRequest().withBody(createCertificateBody);
        CertificateInfo certificate = null;
        try {
            CreateCertificateResponse createCertificateResp = elbClient.createCertificate(createCertificateReq);
            certificate = createCertificateResp.getCertificate();
        } catch (Exception e) {
            logger.error("elbv3 create certificate error: ", e);
            return;
        }

        // Construct the request parameters for adding an HTTPS listener.
        CreateListenerOption createListenerOption = new CreateListenerOption().withLoadbalancerId(loadbalancer.getId())
            .withProtocol(PROTOCOL_HTTPS)
            .withProtocolPort(443)
            .withDefaultTlsContainerRef(certificate.getId())
            .withName("sdk-test-https");
        CreateListenerRequestBody createListenerBody = new CreateListenerRequestBody().withListener(
            createListenerOption);
        CreateListenerRequest createListenerReq = new CreateListenerRequest().withBody(createListenerBody);

        // Request for adding an HTTPS listener.
        Listener listener = null;
        try {
            CreateListenerResponse createListenerResp = elbClient.createListener(createListenerReq);
            listener = createListenerResp.getListener();
        } catch (Exception e) {
            logger.error("elbv3 create listener error: ", e);
            return;
        }

        // Add a forwarding policy.
        // Add a forwarding rule.
        List<CreateL7PolicyRuleOption> rules = new ArrayList<>();
        CreateL7PolicyRuleOption ruleOption = new CreateL7PolicyRuleOption().withCompareType("EQUAL_TO")
            .withType("PATH")
            .withValue("/elb/test");
        rules.add(ruleOption);
        CreateL7PolicyOption createl7PolicyOption = new CreateL7PolicyOption().withListenerId(listener.getId())
            .withAction("REDIRECT_TO_POOL")
            .withRedirectPoolId(pool.getId())
            .withName("sdk-test-l7policy")
            .withRules(rules);
        CreateL7PolicyRequestBody createL7PolicyBody = new CreateL7PolicyRequestBody().withL7policy(
            createl7PolicyOption);
        CreateL7PolicyRequest createL7PolicyReq = new CreateL7PolicyRequest().withBody(createL7PolicyBody);

        // Request for adding a forwarding policy.
        L7Policy l7Policy = null;
        try {
            CreateL7PolicyResponse createL7PolicyResp = elbClient.createL7Policy(createL7PolicyReq);
            l7Policy = createL7PolicyResp.getL7policy();
        } catch (Exception e) {
            logger.error("elbv3 create l7policy error: ", e);
            return;
        }

        // Change a certificate.
        UpdateCertificateOption updateCertificateOption = new UpdateCertificateOption().withCertificate(MY_CERTIFICATE)
            .withDescription("Update my Certificate.")
            .withName("sdk-test-certificate-updated")
            .withPrivateKey(MY_PRIVATE_KEY)
            .withDomain(CERTIFICATE_DOMAIN);
        UpdateCertificateRequestBody updateCertificateBody = new UpdateCertificateRequestBody().withCertificate(
            updateCertificateOption);
        UpdateCertificateRequest updateCertificateReq = new UpdateCertificateRequest().withCertificateId(
            certificate.getId()).withBody(updateCertificateBody);
        try {
            UpdateCertificateResponse updateCertificateResp = elbClient.updateCertificate(updateCertificateReq);
            logger.info("elbv3 update certificate http status code: {}", updateCertificateResp.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv3 update certificate error: ", e);
            return;
        }

        // Modify the health check settings of an HTTPS listener.
        UpdateHealthMonitorOption updateHealthMonitorOption = new UpdateHealthMonitorOption().withAdminStateUp(true)
            .withDelay(20)
            .withMaxRetries(10)
            .withTimeout(25)
            .withType(pool.getProtocol());
        UpdateHealthMonitorRequestBody updateHealthMonitorBody = new UpdateHealthMonitorRequestBody().withHealthmonitor(
            updateHealthMonitorOption);
        UpdateHealthMonitorRequest updateHealthMonitorReq = new UpdateHealthMonitorRequest().withHealthmonitorId(
            healthMonitor.getId()).withBody(
            updateHealthMonitorBody);
        try {
            UpdateHealthMonitorResponse updateHealthMonitorResp = elbClient.updateHealthMonitor(updateHealthMonitorReq);
            logger.info("elbv3 update healthmonitor http status code: {}", updateHealthMonitorResp.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv3 update healthmonitor error: ", e);
            return;
        }

        // Modify an HTTPS listener.
        UpdateListenerOption updateListenerOption = new UpdateListenerOption().withAdminStateUp(true)
            .withName("sdk-test-https-updated")
            .withDefaultTlsContainerRef("");
        UpdateListenerRequestBody updateListenerBody = new UpdateListenerRequestBody().withListener(
            updateListenerOption);
        UpdateListenerRequest updateListenerReq = new UpdateListenerRequest().withListenerId(listener.getId())
            .withBody(updateListenerBody);
        try {
            UpdateListenerResponse updateListenerResp = elbClient.updateListener(updateListenerReq);
            logger.info("elbv3 update listener http status code: {}", updateListenerResp.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv3 update listener error: ", e);
            return;
        }

        // Delete resources.
        deleteMember(pool, member1);
        deleteMember(pool, member2);

        // Disable a health check.
        DeleteHealthMonitorRequest deleteHealthMonitorReq = new DeleteHealthMonitorRequest();
        deleteHealthMonitorReq.withHealthmonitorId(healthMonitor.getId());
        try {
            DeleteHealthMonitorResponse deleteHealthMonitorResp = elbClient.deleteHealthMonitor(deleteHealthMonitorReq);
            logger.info("elbv3 delete healthmonitor http status code: {}", deleteHealthMonitorResp.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv3 delete healthmonitor error: ", e);
            return;
        }

        // Delete a forwarding policy to delete a forwarding rule.
        DeleteL7PolicyRequest delL7PolicyReq = new DeleteL7PolicyRequest();
        delL7PolicyReq.setL7policyId(l7Policy.getId());
        try {
            DeleteL7PolicyResponse deleteL7PolicyResponse = elbClient.deleteL7Policy(delL7PolicyReq);
            logger.info("elbv3 delete l7policy http status code: {}", deleteL7PolicyResponse.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv3 delete l7policy error: ", e);
            return;
        }

        // Delete a backend server group.
        DeletePoolRequest deletePoolReq = new DeletePoolRequest();
        deletePoolReq.withPoolId(pool.getId());
        try {
            DeletePoolResponse deletePoolResp = elbClient.deletePool(deletePoolReq);
            logger.info("elbv3 delete pool http status code: {}", deletePoolResp.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv3 delete pool error: ", e);
            return;
        }

        // Delete a listener.
        DeleteListenerRequest deleteListenerReq = new DeleteListenerRequest();
        deleteListenerReq.withListenerId(listener.getId());
        try {
            DeleteListenerResponse deleteListenerResp = elbClient.deleteListener(deleteListenerReq);
            logger.info("elbv3 delete listener http status code: {}", deleteListenerResp.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv3 delete listener error: ", e);
            return;
        }

        // Delete a load balancer.
        DeleteLoadBalancerRequest deleteLoadBalancerReq = new DeleteLoadBalancerRequest();
        deleteLoadBalancerReq.withLoadbalancerId(loadbalancer.getId());
        try {
            DeleteLoadBalancerResponse deleteLoadBalancerResp = elbClient.deleteLoadBalancer(deleteLoadBalancerReq);
            logger.info("elbv3 delete loadbalancer http status code: {}", deleteLoadBalancerResp.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv3 delete loadbalancer error: ", e);
            return;
        }

        // Delete a certificate.
        DeleteCertificateRequest delCertificateReq = new DeleteCertificateRequest();
        delCertificateReq.setCertificateId(certificate.getId());
        try {
            DeleteCertificateResponse deleteCertificateResp = elbClient.deleteCertificate(delCertificateReq);
            logger.info("elbv3 delete certificate http status code: {}", deleteCertificateResp.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv3 delete certificate error: ", e);
        }
    }

    /** Create a backend server group. If listener is not null, the backend server group is associated with the listener.
     *  If listener is null, the backend server group is associated with the load balancer.
     **/
    private static Pool createPool(LoadBalancer loadbalancer, Listener listener, String protocol) {
        // Construct the request parameters for adding a backend server group.
        CreatePoolRequest request = new CreatePoolRequest();
        CreatePoolRequestBody body = new CreatePoolRequestBody();
        CreatePoolOption option = new CreatePoolOption();
        if (loadbalancer != null) {
            option.setLoadbalancerId(loadbalancer.getId());
        }
        if (listener != null) {
            option.setListenerId(listener.getId());
        }
        option.withLbAlgorithm(ROUND_ROBIN);
        option.withProtocol(protocol);
        body.setPool(option);
        request.setBody(body);

        // Request for creating a backend server group.
        Pool pool = null;
        try {
            CreatePoolResponse creatPoolResp = elbClient.createPool(request);
            pool = creatPoolResp.getPool();
        } catch (Exception e) {
            logger.error("elbv3 create pool error: ", e);
        }
        return pool;
    }

    /* Configure a health check for the associated backend server group. */
    private static HealthMonitor createHm(Pool pool) {
        // Construct the request parameters for configuring a health check.
        CreateHealthMonitorOption option = new CreateHealthMonitorOption().withPoolId(pool.getId())
            .withType(pool.getProtocol())
            .withDelay(5)
            .withMaxRetries(3)
            .withTimeout(10);
        CreateHealthMonitorRequestBody body = new CreateHealthMonitorRequestBody().withHealthmonitor(option);
        CreateHealthMonitorRequest request = new CreateHealthMonitorRequest().withBody(body);

        // Request for configuring a health check.
        HealthMonitor healthMonitor = null;
        try {
            CreateHealthMonitorResponse createHmResp = elbClient.createHealthMonitor(request);
            healthMonitor = createHmResp.getHealthmonitor();
        } catch (Exception e) {
            logger.error("elbv3 create healthmonitor error: ", e);
        }

        return healthMonitor;
    }

    // Add a backend server.
    private static Member createMember(LoadBalancer loadbalancer, Pool pool, String ip) {
        // Construct the request parameters for adding a backend server.
        CreateMemberOption option = new CreateMemberOption().withSubnetCidrId(loadbalancer.getVipSubnetCidrId())
            .withAddress(ip)
            .withProtocolPort(8080);
        CreateMemberRequestBody body = new CreateMemberRequestBody().withMember(option);
        CreateMemberRequest request = new CreateMemberRequest().withBody(body).withPoolId(pool.getId());

        // Request for adding a backend server.
        Member member = null;
        try {
            CreateMemberResponse createMemberResp = elbClient.createMember(request);
            member = createMemberResp.getMember();
        } catch (Exception e) {
            logger.error("elbv3 create member error: ", e);
        }

        return member;
    }

    private static void deleteMember(Pool pool, Member member) {
        DeleteMemberRequest req = new DeleteMemberRequest().withPoolId(pool.getId()).withMemberId(member.getId());
        try {
            DeleteMemberResponse resp = elbClient.deleteMember(req);
            logger.info("elbv3 delete member http status code: {}", resp.getHttpStatusCode());
        } catch (Exception e) {
            logger.error("elbv3 delete member error: ", e);
        }
    }
}
