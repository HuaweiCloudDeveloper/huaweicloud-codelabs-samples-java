## 1. Functions

You can integrate [Elastic Load Balance ELB SDK](https://sdkcenter.developer.huaweicloud.com/?product=ELB) provided by Huawei Cloud to call ELB APIs, making it easier for you to use the service.

You can follow the instructions in this section to create a load balancer, add an HTTPS listener, create a backend server group, configure a health check, add a backend server, and perform other operations. You can also delete these resources by following these instructions.

**Note: If you no longer use a load balancer, delete it and its related resources to avoid unnecessary charges.**

## 2. Flowchart
Creating a load balancer and related resources

![image](assets/createELBv3ResourcesEn.png)

Deleting a load balancer and related resources

![image](assets/deleteELBv3ResourcesEn.png)

## 3. Prerequisites

- You have obtained the Huawei Cloud SDK. You can also install the Java SDK. For details, see [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/ELB?lang=Java).
- To use the Huawei Cloud Java SDK, you must have a Huawei Cloud account and obtain the AK and SK of the account. For details, see [Obtaining an AK/SK](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- The Java version is **1.8** or later.

## 4. Obtaining and Installing the SDK

You can use Maven to configure the ELB SDK. 

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-elb</artifactId>
    <version>3.1.53</version>
</dependency>
```

## 5. Key Code Snippets

### 5.1. Creating a Load Balancer
A load balancer distributes incoming traffic across multiple backend servers. Before using a load balancer to route requests, you need to add at least one listener to it and associate one backend server with it.
```java
Specify the subnet ID.
String neutronSubnetId = "xxx";
// Add multiple AZ codes if you want to deploy the load balancer in multiple AZs.
List<String> azCodes = Arrays.asList("xxx");

String l4flavorId = "xxx";
String l7flavorId = "xxx";

// Construct the request parameters for creating a load balancer.
CreateLoadBalancerOption createLoadbalancerOption = new CreateLoadBalancerOption().withName("sdk-test-elb")
    .withAvailabilityZoneList(azCodes)
    .withVipSubnetCidrId(neutronSubnetId)
    .withL4FlavorId(l4flavorId)
    .withL7FlavorId(l7flavorId);
CreateLoadBalancerRequestBody createLoadbalancerBody = new CreateLoadBalancerRequestBody().withLoadbalancer(
    createLoadbalancerOption);
CreateLoadBalancerRequest createLoadbalancerReq = new CreateLoadBalancerRequest().withBody(
    createLoadbalancerBody);

// Request for creating a load balancer.
LoadBalancer loadbalancer = null;
try {
    CreateLoadBalancerResponse createLoadbalancerResp = elbClient.createLoadBalancer(createLoadbalancerReq);
    loadbalancer = createLoadbalancerResp.getLoadbalancer();
} catch (Exception e) {
    logger.error("elbv3 create loadbalancer error: ", e);
    return;
}
```

### 5.2 Adding a Backend Cloud Server Group
A backend server group is a logical collection of one or more backend servers to receive massive concurrent requests at the same time. A backend server can be an ECS, BMS, supplementary network interface, or IP address.
```java
// Construct the request parameters for adding a backend server group.
CreatePoolRequest request = new CreatePoolRequest();
CreatePoolRequestBody body = new CreatePoolRequestBody();
CreatePoolOption option = new CreatePoolOption();
if (loadbalancer != null) {
    option.setLoadbalancerId(loadbalancer.getId());
}
if (listener != null) {
    option.setListenerId(listener.getId());
}
option.withLbAlgorithm(ROUND_ROBIN);
option.withProtocol(protocol);
body.setPool(option);
request.setBody(body);

// Request for creating a backend server group.
Pool pool = null;
try {
    CreatePoolResponse creatPoolResp = elbClient.createPool(request);
    pool = creatPoolResp.getPool();
} catch (Exception e) {
    logger.error("elbv3 create pool error: ", e);
}
```

### 5.3. Configuring a Health Check

```java
// Construct the request parameters for configuring a health check.
CreateHealthMonitorOption option = new CreateHealthMonitorOption().withPoolId(pool.getId())
    .withType(pool.getProtocol())
    .withDelay(5)
    .withMaxRetries(3)
    .withTimeout(10);
CreateHealthMonitorRequestBody body = new CreateHealthMonitorRequestBody().withHealthmonitor(option);
CreateHealthMonitorRequest request = new CreateHealthMonitorRequest().withBody(body);

// Request for configuring a health check.
HealthMonitor healthMonitor = null;
try {
    CreateHealthMonitorResponse createHmResp = elbClient.createHealthMonitor(request);
    healthMonitor = createHmResp.getHealthmonitor();
} catch (Exception e) {
    logger.error("elbv3 create healthmonitor error: ", e);
}
```

### 5.4 Adding a Backend Cloud Server
A backend server processes client requests forwarded by a load balancer.
```java
// Construct the request parameters for adding a backend server.
CreateMemberOption option = new CreateMemberOption().withSubnetCidrId(loadbalancer.getVipSubnetCidrId())
    .withAddress(ip)
    .withProtocolPort(8080);
CreateMemberRequestBody body = new CreateMemberRequestBody().withMember(option);
CreateMemberRequest request = new CreateMemberRequest().withBody(body).withPoolId(pool.getId());

// Request for adding a backend server.
Member member = null;
try {
    CreateMemberResponse createMemberResp = elbClient.createMember(request);
    member = createMemberResp.getMember();
} catch (Exception e) {
    logger.error("elbv3 create member error: ", e);
}
```

### 5.5. Adding a Certificate
When adding an HTTPS listener, you need to bind a server certificate to it. A server certificate is used for SSL handshake negotiations if an HTTPS listener is used. Both the certificate content and private key are required.
```java
// Add a certificate.
CreateCertificateOption createCertificateOption = new CreateCertificateOption().withCertificate(MY_CERTIFICATE)
    .withPrivateKey(MY_PRIVATE_KEY)
    .withType(CERTIFICATE_TYPE_SERVER)
    .withDomain(CERTIFICATE_DOMAIN)
    .withName("sdk-test-certificate");
CreateCertificateRequestBody createCertificateBody = new CreateCertificateRequestBody().withCertificate(
    createCertificateOption);
CreateCertificateRequest createCertificateReq = new CreateCertificateRequest().withBody(createCertificateBody);
CertificateInfo certificate = null;
try {
    CreateCertificateResponse createCertificateResp = elbClient.createCertificate(createCertificateReq);
    certificate = createCertificateResp.getCertificate();
} catch (Exception e) {
    logger.error("elbv3 create certificate error: ", e);
    return;
}
```

### 5.6 Adding a Listener
After you create a load balancer, add at least one listener to it. This listener receives requests from clients and routes requests to backend servers using the protocol, port, and load balancing algorithm you select.
```java
// Construct the request parameters for adding an HTTPS listener.
CreateListenerOption createListenerOption = new CreateListenerOption().withLoadbalancerId(loadbalancer.getId())
    .withProtocol(PROTOCOL_HTTPS)
    .withProtocolPort(443)
    .withDefaultTlsContainerRef(certificate.getId())
    .withName("sdk-test-https");
CreateListenerRequestBody createListenerBody = new CreateListenerRequestBody().withListener(
    createListenerOption);
CreateListenerRequest createListenerReq = new CreateListenerRequest().withBody(createListenerBody);

// Request for adding an HTTPS listener.
Listener listener = null;
try {
    CreateListenerResponse createListenerResp = elbClient.createListener(createListenerReq);
    listener = createListenerResp.getListener();
} catch (Exception e) {
    logger.error("elbv3 create listener error: ", e);
    return;
}
```

### 5.7. Adding a Forwarding Policy
Each HTTP or HTTPS listener has a default forwarding policy. You can also define additional forwarding policies. Each forwarding policy consists of a priority, one or more forwarding rules, and an action. You can add or edit forwarding policies at any time.
```java
// Add a forwarding policy and a forwarding rule.
// Add a forwarding rule.
List<CreateL7PolicyRuleOption> rules = new ArrayList<>();
CreateL7PolicyRuleOption ruleOption = new CreateL7PolicyRuleOption().withCompareType("EQUAL_TO")
    .withType("PATH")
    .withValue("/elb/test");
rules.add(ruleOption);
CreateL7PolicyOption createl7PolicyOption = new CreateL7PolicyOption().withListenerId(listener.getId())
    .withAction("REDIRECT_TO_POOL")
    .withRedirectPoolId(pool.getId())
    .withName("sdk-test-l7policy")
    .withRules(rules);
CreateL7PolicyRequestBody createL7PolicyBody = new CreateL7PolicyRequestBody().withL7policy(
    createl7PolicyOption);
CreateL7PolicyRequest createL7PolicyReq = new CreateL7PolicyRequest().withBody(createL7PolicyBody);

// Request for adding a forwarding policy and a forwarding rule.
L7Policy l7Policy = null;
try {
    CreateL7PolicyResponse createL7PolicyResp = elbClient.createL7Policy(createL7PolicyReq);
    l7Policy = createL7PolicyResp.getL7policy();
} catch (Exception e) {
    logger.error("elbv3 create l7policy error: ", e);
    return;
}
```

### 5.8 Changing a Certificate

```java
// Change a certificate.
UpdateCertificateOption updateCertificateOption = new UpdateCertificateOption().withCertificate(MY_CERTIFICATE)
    .withDescription("Update my Certificate.")
    .withName("sdk-test-certificate-updated")
    .withPrivateKey(MY_PRIVATE_KEY)
    .withDomain(CERTIFICATE_DOMAIN);
UpdateCertificateRequestBody updateCertificateBody = new UpdateCertificateRequestBody().withCertificate(
    updateCertificateOption);
UpdateCertificateRequest updateCertificateReq = new UpdateCertificateRequest().withCertificateId(
    certificate.getId()).withBody(updateCertificateBody);
try {
    UpdateCertificateResponse updateCertificateResp = elbClient.updateCertificate(updateCertificateReq);
    logger.info("elbv3 update certificate http status code: {}", updateCertificateResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv3 update certificate error: ", e);
    return;
}
```

### 5.9. Modifying a Health Check

```java
// Modify the health check settings of an HTTPS listener.
UpdateHealthMonitorOption updateHealthMonitorOption = new UpdateHealthMonitorOption().withAdminStateUp(true)
    .withDelay(20)
    .withMaxRetries(10)
    .withTimeout(25)
    .withType(pool.getProtocol());
UpdateHealthMonitorRequestBody updateHealthMonitorBody = new UpdateHealthMonitorRequestBody().withHealthmonitor(
    updateHealthMonitorOption);
UpdateHealthMonitorRequest updateHealthMonitorReq = new UpdateHealthMonitorRequest().withHealthmonitorId(
    healthMonitor.getId()).withBody(
    updateHealthMonitorBody);
try {
    UpdateHealthMonitorResponse updateHealthMonitorResp = elbClient.updateHealthMonitor(updateHealthMonitorReq);
    logger.info("elbv3 update healthmonitor http status code: {}", updateHealthMonitorResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv3 update healthmonitor error: ", e);
    return;
}
```

### 5.10. Modifying a Listener

```java
// Modify a listener.
UpdateListenerOption updateListenerOption = new UpdateListenerOption().withAdminStateUp(true)
    .withName("sdk-test-https-updated")
    .withDefaultTlsContainerRef("");
UpdateListenerRequestBody updateListenerBody = new UpdateListenerRequestBody().withListener(
    updateListenerOption);
UpdateListenerRequest updateListenerReq = new UpdateListenerRequest().withListenerId(listener.getId())
    .withBody(updateListenerBody);
try {
    UpdateListenerResponse updateListenerResp = elbClient.updateListener(updateListenerReq);
    logger.info("elbv3 update listener http status code: {}", updateListenerResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv3 update listener error: ", e);
    return;
}
```

### 5.11. Removing a Backend Server

```java
DeleteMemberRequest req = new DeleteMemberRequest().withPoolId(pool.getId()).withMemberId(member.getId());
try {
    DeleteMemberResponse resp = elbClient.deleteMember(req);
    logger.info("elbv3 delete member http status code: {}", resp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv3 delete member error: ", e);
}
```

### 5.12. Disabling a Health Check

```java
// Disable a health check.
DeleteHealthMonitorRequest deleteHealthMonitorReq = new DeleteHealthMonitorRequest();
deleteHealthMonitorReq.withHealthmonitorId(healthMonitor.getId());
try {
    DeleteHealthMonitorResponse deleteHealthMonitorResp = elbClient.deleteHealthMonitor(deleteHealthMonitorReq);
    logger.info("elbv3 delete healthmonitor http status code: {}", deleteHealthMonitorResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv3 delete healthmonitor error: ", e);
    return;
}
```

### 5.13. Deleting a Forwarding Rule

```java
// Delete a forwarding policy to delete a forwarding rule.
DeleteL7PolicyRequest delL7PolicyReq = new DeleteL7PolicyRequest();
delL7PolicyReq.setL7policyId(l7Policy.getId());
try {
    DeleteL7PolicyResponse deleteL7PolicyResponse = elbClient.deleteL7Policy(delL7PolicyReq);
    logger.info("elbv3 delete l7policy http status code: {}", deleteL7PolicyResponse.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv3 delete l7policy error: ", e);
    return;
}
```

### 5.14. Deleting a Backend Server Group

```java
// Delete a backend server group.
DeletePoolRequest deletePoolReq = new DeletePoolRequest();
deletePoolReq.withPoolId(pool.getId());
try {
    DeletePoolResponse deletePoolResp = elbClient.deletePool(deletePoolReq);
    logger.info("elbv3 delete pool http status code: {}", deletePoolResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv3 delete pool error: ", e);
    return;
}
```

### 5.15. Deleting a Listener

```java
// Delete a listener.
DeleteListenerRequest deleteListenerReq = new DeleteListenerRequest();
deleteListenerReq.withListenerId(listener.getId());
try {
    DeleteListenerResponse deleteListenerResp = elbClient.deleteListener(deleteListenerReq);
    logger.info("elbv3 delete listener http status code: {}", deleteListenerResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv3 delete listener error: ", e);
    return;
}
```

### 5.16. Deleting a Load Balancer

```java
// Delete a load balancer.
DeleteLoadBalancerRequest deleteLoadBalancerReq = new DeleteLoadBalancerRequest();
deleteLoadBalancerReq.withLoadbalancerId(loadbalancer.getId());
try {
    DeleteLoadBalancerResponse deleteLoadBalancerResp = elbClient.deleteLoadBalancer(deleteLoadBalancerReq);
    logger.info("elbv3 delete loadbalancer http status code: {}", deleteLoadBalancerResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv3 delete loadbalancer error: ", e);
    return;
}
```

### 5.17. Deleting a Certificate

```java
// Delete a certificate.
DeleteCertificateRequest deleteCertificateRequest = new DeleteCertificateRequest();
deleteCertificateRequest.withCertificateId(certificate.getId());
try {
    DeleteCertificateResponse deleteCertificateResp = elbClient.deleteCertificate(deleteCertificateRequest);
    logger.info("elbv2 delete certificate http status code: {}", deleteCertificateResp.getHttpStatusCode());
} catch (Exception e) {
    logger.error("elbv2 delete certificate error: ", e);
}
```

## 6. Example Returned Results

Creating a load balancer

```json
{
    "loadbalancer": {
        "name": "my_loadbalancer",
        "id": "29cc669b-3ac8-4498-9094-bdf6193425c2",
        "project_id": "060576798a80d5762fafc01a9b5eedc7",
        "description": "",
        "vip_port_id": "98697944-0cc7-4d3b-a829-001c2fb82232",
        "vip_address": "192.168.0.214",
        "admin_state_up": true,
        "provisioning_status": "ACTIVE",
        "operating_status": "ONLINE",
        "listeners": [],
        "pools": [],
        "tags": [
            {
                "key": "tab_key",
                "value": "tag1"
            }
        ],
        "provider": "vlb",
        "created_at": "2023-03-22T07:59:57Z",
        "updated_at": "2023-03-22T07:59:59Z",
        "vpc_id": "a1f33a4c-95b9-48a7-9350-684e2ed844b3",
        "enterprise_project_id": "134f2181-5720-47e7-bd78-1356ed3737d6",
        "availability_zone_list": [],
        "ipv6_vip_address": null,
        "ipv6_vip_virsubnet_id": null,
        "ipv6_vip_port_id": null,
        "publicips": [
            {
                "publicip_id": "3388574a-4f6f-4471-869e-97d74d21eee9",
                "publicip_address": "88.88.87.205",
                "ip_version": 4
            }
        ],
        "global_eips": [],
        "elb_virsubnet_ids": [],
        "elb_virsubnet_type": null,
        "ip_target_enable": false,
        "autoscaling": {
            "enable": false,
            "min_l7_flavor_id": ""
        },
        "frozen_scene": null,
        "public_border_group": "center",
        "eips": [
            {
                "eip_id": "3388574a-4f6f-4471-869e-97d74d21eee9",
                "eip_address": "88.88.87.205",
                "ip_version": 4
            }
        ],
        "guaranteed": false,
        "billing_info": null,
        "l4_flavor_id": null,
        "l4_scale_flavor_id": null,
        "l7_flavor_id": null,
        "l7_scale_flavor_id": null,
        "waf_failure_action": "",
        "vip_subnet_cidr_id": "abf31f3b-706e-4e55-a6dc-f2fcc707fd3a"
    },
    "request_id": "bf29597181cb81b30d19f1a0115a157d"
}
```

Adding a backend server group

```json
{
    "pool": {
        "type": "",
        "vpc_id": "",
        "lb_algorithm": "LEAST_CONNECTIONS",
        "protocol": "TCP",
        "description": "",
        "admin_state_up": true,
        "member_deletion_protection_enable": false,
        "loadbalancers": [
            {
                "id": "098b2f68-af1c-41a9-8efd-69958722af62"
            }
        ],
        "project_id": "99a3fff0d03c428eac3678da6a7d0f24",
        "session_persistence": null,
        "healthmonitor_id": null,
        "listeners": [
            {
                "id": "0b11747a-b139-492f-9692-2df0b1c87193"
            }
        ],
        "members": [],
        "id": "36ce7086-a496-4666-9064-5ba0e6840c75",
        "name": "My pool",
        "ip_version": "v4",
        "slow_start": null
    },
    "request_id": "2d974978-0733-404d-a21a-b29204f4803a"
}
```

Configuring a health check

```json
{
    "request_id": "0e837340-f1bd-4037-8f61-9923d0f0b19e",
    "healthmonitor": {
        "monitor_port": null,
        "id": "c2b210b2-60c4-449d-91e2-9e9ea1dd7441",
        "project_id": "99a3fff0d03c428eac3678da6a7d0f24",
        "domain_name": null,
        "name": "My Healthmonitor",
        "delay": 1,
        "max_retries": 3,
        "pools": [
            {
                "id": "488acc50-6bcf-423d-8f0a-0f4184f5b8a0"
            }
        ],
        "admin_state_up": true,
        "timeout": 30,
        "type": "HTTP",
        "expected_codes": "200",
        "url_path": "/",
        "http_method": "GET"
    }
}
```

Adding a backend server

```json
{
    "member": {
        "name": "My member",
        "weight": 1,
        "admin_state_up": false,
        "subnet_cidr_id": "c09f620e-3492-4429-ac15-445d5dd9ca74",
        "project_id": "99a3fff0d03c428eac3678da6a7d0f24",
        "address": "120.10.10.16",
        "protocol_port": 89,
        "id": "1923923e-fe8a-484f-bdbc-e11559b1f48f",
        "operating_status": "NO_MONITOR",
        "status": [
            {
                "listener_id": "427eee03-b569-4d6c-b1f1-712032f7ec2d",
                "operating_status": "NO_MONITOR"
            }
        ],
        "ip_version": "v4"
    },
    "request_id": "f354090d-41db-41e0-89c6-7a943ec50792"
}
```

Adding a certificate

```json
{
    "certificate": {
        "private_key": "-----BEGIN PRIVATE KEY-----xxx-----END PRIVATE KEY-----",
        "description": "",
        "domain": null,
        "created_at": "2019-03-31T22:23:51Z",
        "expire_time": "2045-11-17T13:25:47Z",
        "id": "233a325e5e3e4ce8beeb320aa714cc12",
        "name": "My Certificate",
        "certificate": "-----BEGIN CERTIFICATE-----xxx---END CERTIFICATE-----",
        "admin_state_up": true,
        "project_id": "99a3fff0d03c428eac3678da6a7d0f24",
        "updated_at": "2019-03-31T23:26:49Z",
        "type": "server"
    },
    "request_id": "98414965-856c-4be3-8a33-3e08432a222e"
}
```

Adding a listener

```json
{
    "listener": {
        "id": "0b11747a-b139-492f-9692-2df0b1c87193",
        "name": "My listener",
        "protocol_port": 80,
        "protocol": "TCP",
        "description": null,
        "default_tls_container_ref": null,
        "admin_state_up": true,
        "loadbalancers": [
            {
                "id": "098b2f68-af1c-41a9-8efd-69958722af62"
            }
        ],
        "client_ca_tls_container_ref": null,
        "project_id": "99a3fff0d03c428eac3678da6a7d0f24",
        "sni_container_refs": [],
        "connection_limit": -1,
        "member_timeout": null,
        "client_timeout": null,
        "keepalive_timeout": null,
        "default_pool_id": null,
        "ipgroup": null,
        "tls_ciphers_policy": "tls-1-0",
        "tags": [],
        "created_at": "2019-04-02T00:12:32Z",
        "updated_at": "2019-04-02T00:12:32Z",
        "http2_enable": false,
        "enable_member_retry": true,
        "insert_headers": {
            "X-Forwarded-ELB-IP": true
        },
        "transparent_client_ip_enable": false
    },
    "request_id": "f4c4aca8-df16-42e8-8836-33e4b8e9aa8e"
}
```

Adding a forwarding policy

```json
{
    "request_id": "b60d1d9a-5263-45b0-b1d6-2810ac7c52a1",
    "l7policy": {
        "redirect_pool_id": "768e9e8c-e7cb-4fef-b24b-af9399dbb240",
        "description": "",
        "admin_state_up": true,
        "rules": [
            {
                "id": "c5c2d625-676b-431e-a4c7-c59cc2664881"
            }
        ],
        "project_id": "7a9941d34fc1497d8d0797429ecfd354",
        "listener_id": "cdb03a19-16b7-4e6b-bfec-047aeec74f56",
        "redirect_url": null,
        "redirect_url_config": null,
        "fixed_response_config": null,
        "redirect_listener_id": null,
        "action": "REDIRECT_TO_POOL",
        "position": 100,
        "priority": null,
        "provisioning_status": "ACTIVE",
        "id": "01832d99-bbd8-4340-9d0c-6ff8f7a37307",
        "name": "l7policy-67"
    }
}
```

Changing a certificate

```json
{
    "certificate": {
        "private_key": "-----BEGIN PRIVATE KEY-----xxx-----END PRIVATE KEY-----",
        "description": "Update my Certificate.",
        "domain": null,
        "created_at": "2019-03-31T22:23:51Z",
        "expire_time": "2045-11-17T13:25:47Z",
        "id": "233a325e5e3e4ce8beeb320aa714cc12",
        "name": "My Certificate",
        "certificate": "-----BEGIN CERTIFICATE-----xxx---END CERTIFICATE-----",
        "admin_state_up": true,
        "project_id": "99a3fff0d03c428eac3678da6a7d0f24",
        "updated_at": "2019-03-31T23:26:49Z",
        "type": "server"
    },
    "request_id": "d9abea6b-98ee-4ad4-8c5d-185ded48742f"
}
```

Modifying a health check

```json
{
    "request_id": "08d6ffea-d092-4cfa-860a-e364f3bef1be",
    "healthmonitor": {
        "monitor_port": null,
        "id": "c2b210b2-60c4-449d-91e2-9e9ea1dd7441",
        "project_id": "99a3fff0d03c428eac3678da6a7d0f24",
        "domain_name": null,
        "name": "My Healthmonitor update",
        "delay": 10,
        "max_retries": 10,
        "pools": [
            {
                "id": "488acc50-6bcf-423d-8f0a-0f4184f5b8a0"
            }
        ],
        "admin_state_up": true,
        "timeout": 30,
        "type": "HTTP",
        "expected_codes": "200",
        "url_path": "/",
        "http_method": "GET"
    }
}
```

Modifying a listener

```json
{
    "listener": {
        "id": "0b11747a-b139-492f-9692-2df0b1c87193",
        "name": "My listener",
        "protocol_port": 80,
        "protocol": "TCP",
        "description": null,
        "default_tls_container_ref": null,
        "admin_state_up": true,
        "loadbalancers": [
            {
                "id": "098b2f68-af1c-41a9-8efd-69958722af62"
            }
        ],
        "client_ca_tls_container_ref": null,
        "project_id": "99a3fff0d03c428eac3678da6a7d0f24",
        "sni_container_refs": [],
        "connection_limit": -1,
        "member_timeout": null,
        "client_timeout": null,
        "keepalive_timeout": null,
        "default_pool_id": null,
        "ipgroup": null,
        "tls_ciphers_policy": "tls-1-0",
        "tags": [],
        "created_at": "2019-04-02T00:12:32Z",
        "updated_at": "2019-04-02T00:12:32Z",
        "http2_enable": false,
        "enable_member_retry": true,
        "insert_headers": {
            "X-Forwarded-ELB-IP": true
        },
        "transparent_client_ip_enable": false
    },
    "request_id": "f4c4aca8-df16-42e8-8836-33e4b8e9aa8e"
}
```

## 7. References

- API References
  - Creating a load balancer [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=CreateLoadBalancer)
  - Creating a backend server group [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=CreatePool)
  - Configuring a health check [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=CreateHealthMonitor)
  - Adding a backend server [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=CreateMember)
  - Adding a certificate [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=CreateCertificate)
  - Adding a listener [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=CreateListener)
  - Adding a forwarding policy [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=CreateL7Policy)
  - Changing a certificate [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=UpdateCertificate)
  - Modifying a health check [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=UpdateHealthMonitor)
  - Modifying a listener [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/openapi/ELB/mock?version=v3&api=UpdateListener)
- SDK reference [API Explorer (huaweicloud.com)](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/ELB?lang=Java)
- Obtaining an AK/SK [My Credential_User Guide_Huawei Cloud (huaweicloud.com)](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html)

## 8. Change History

|  Release Date | Issue|   Description  |
| :--------: | :------: | :----------: |
| 2023-08-21 |   1.0    | This issue is the first official release.|
