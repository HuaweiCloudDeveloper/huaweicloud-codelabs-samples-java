## 1、功能介绍

企业路由器（Enterprise Router, ER）可以连接虚拟私有云（Virtual Private Cloud, VPC）或本地网络来构建中心辐射型组网，是云上大规格、高带宽、高性能的集中路由器。企业路由器使用边界网关协议（Border Gateway Protocol, BGP），支持路由学习、动态选路以及链路切换，极大的提升网络的可扩展性及运维效率，从而保证业务的连续性。

本示例展示如何使用企业路由器实例（EnterpriseRouter）相关SDK

## 2、流程图
![流程图](./assets/er_instance.png)

## 3、前置条件
- 已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)
- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。具体请参见[ER JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/ER?lang=Java)
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。具体请参见[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。

## 4、SDK获取和安装
您可以通过Maven配置所依赖的企业路由器服务SDK

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java)  (产品类别：企业路由器)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-er</artifactId>
    <version>3.1.2</version>
</dependency>
```

## 5、关键代码片段
该代码示例展示了企业路由器实例的增、删、改、查功能。由于创建、更新和删除操作是异步接口，在进行调试时，需要注释无关的代码。
```java
package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.er.v3.ErClient;
import com.huaweicloud.sdk.er.v3.model.CreateEnterpriseRouterRequest;
import com.huaweicloud.sdk.er.v3.model.CreateEnterpriseRouterRequestBody;
import com.huaweicloud.sdk.er.v3.model.CreateEnterpriseRouterResponse;
import com.huaweicloud.sdk.er.v3.model.DeleteEnterpriseRouterRequest;
import com.huaweicloud.sdk.er.v3.model.DeleteEnterpriseRouterResponse;
import com.huaweicloud.sdk.er.v3.model.ListEnterpriseRoutersRequest;
import com.huaweicloud.sdk.er.v3.model.ListEnterpriseRoutersResponse;
import com.huaweicloud.sdk.er.v3.model.ShowEnterpriseRouterRequest;
import com.huaweicloud.sdk.er.v3.model.ShowEnterpriseRouterResponse;
import com.huaweicloud.sdk.er.v3.model.UpdateEnterpriseRouterRequest;
import com.huaweicloud.sdk.er.v3.model.UpdateEnterpriseRouterRequestBody;
import com.huaweicloud.sdk.er.v3.model.UpdateEnterpriseRouterResponse;
import com.huaweicloud.sdk.er.v3.region.ErRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

public class EnterpriseRouterInstanceDemo {
  private static final Logger LOGGER = LoggerFactory.getLogger(EnterpriseRouterInstanceDemo.class.getName());

  public static void main(String[] args) {
    // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    String erId = "{er_id}";

    ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

    ErClient client = ErClient.newBuilder()
            .withCredential(auth)
            .withRegion(ErRegion.valueOf("cn-south-1"))
            .build();

    // Create enterprise router
    createEnterpriseRouter(client);

    /*
     * the parameter erId can be extracted from response of the previous function
     */
    // Update enterprise router
    updateEnterpriseRouter(client, erId);

    // Show enterprise router
    showEnterpriseRouter(client, erId);

    // Delete enterprise router
    deleteEnterpriseRouter(client, erId);

    // List enterprise routers
    listEnterpriseRouters(client);

  }

  private static CreateEnterpriseRouterResponse createEnterpriseRouter(ErClient client) {
    CreateEnterpriseRouterRequest request = new CreateEnterpriseRouterRequest();
    CreateEnterpriseRouterRequestBody body = new CreateEnterpriseRouterRequestBody();

    body.withInstance(instance -> {
      instance.withName("<your enterprise router name>");
      instance.withDescription("<your enterprise router description>");
      instance.withAvailabilityZoneIds(azs -> azs.add("<availablility zone id>"));
      instance.withEnableDefaultAssociation(false);
      instance.withEnableDefaultPropagation(false);
      instance.withAsn(64512L); // AS number
    });
    request.withBody(body);

    Function<Void, CreateEnterpriseRouterResponse> task = (Void v) -> {
      return client.createEnterpriseRouter(request);
    };
    return execute_instance(task);
  }

  private static ShowEnterpriseRouterResponse showEnterpriseRouter(ErClient client, String erId) {
    ShowEnterpriseRouterRequest request = new ShowEnterpriseRouterRequest().withErId(erId);
    Function<Void, ShowEnterpriseRouterResponse> task = (Void v) -> {
      return client.showEnterpriseRouter(request);
    };
    return execute_instance(task);
  }

  private static UpdateEnterpriseRouterResponse updateEnterpriseRouter(ErClient client, String erId) {
    UpdateEnterpriseRouterRequest request = new UpdateEnterpriseRouterRequest();
    UpdateEnterpriseRouterRequestBody body = new UpdateEnterpriseRouterRequestBody();
    body.withInstance(instance -> {
      instance.withName("<your new enterprise router name>");
      instance.withDescription("<your new enterprise router description>");
    });
    request.withErId(erId).withBody(body);
    Function<Void, UpdateEnterpriseRouterResponse> task = (Void v) -> {
      return client.updateEnterpriseRouter(request);
    };
    return execute_instance(task);
  }

  private static void deleteEnterpriseRouter(ErClient client, String erId) {
    DeleteEnterpriseRouterRequest request = new DeleteEnterpriseRouterRequest().withErId(erId);
    Function<Void, DeleteEnterpriseRouterResponse> task = (Void v) -> {
      return client.deleteEnterpriseRouter(request);
    };
    execute_instance(task);
  }

  private static ListEnterpriseRoutersResponse listEnterpriseRouters(ErClient client) {
    ListEnterpriseRoutersRequest request = new ListEnterpriseRoutersRequest();
    Function<Void, ListEnterpriseRoutersResponse> task = (Void v) -> {
      return client.listEnterpriseRouters(request);
    };
    return execute_instance(task);
  }

  private static <T> T execute_instance(Function<Void, T> task) {
    T response_instance = null;
    try {
      response_instance = task.apply(null);
      LOGGER.info(response_instance.toString());
    } catch (ClientRequestException e) {
      LOGGER.error(String.valueOf(e.getHttpStatusCode()));
      LOGGER.error(e.toString());
    } catch (ServerResponseException e) {
      LOGGER.error(String.valueOf(e.getHttpStatusCode()));
      LOGGER.error(e.getMessage());
    }
    return response_instance;
  }

}

```

## 6、运行结果
#### 6.1 创建企业路由器实例
```json
{
  "instance": {
    "id": "94c2b814-99dc-939a-e811-ae84c61ea3ff",
    "name": "my_er",
    "description": "this is my first enterprise router",
    "asn": 64512,
    "project_id": "08d5a9564a704afda6039ae2babbef3c",
    "enable_default_association": true,
    "enable_default_propagation": true,
    "default_association_route_table_id": "7f7f738f-453c-40b1-be26-28e7b9e390c1",
    "default_propagation_route_table_id": "7f7f738f-453c-40b1-be26-28e7b9e390c1",
    "auto_accept_shared_attachments": false,
    "created_at": "2019-09-06 02:11:13Z",
    "updated_at": "2019-09-06 02:11:13Z",
    "tags": [
      {
        "key": "key1",
        "value": "value1"
      }
    ],
    "enterprise_project_id": 0,
    "availability_zone_ids": [
      "az1",
      "az2"
    ]
  },
  "request_id": "14c2b814-99dc-939a-e811-ae84c61ea3f4"
}
```

#### 6.2 更新企业路由器基本信息
```json
{
  "instance": {
    "id": "94c2b814-99dc-939a-e811-ae84c61ea3ff",
    "name": "my_er",
    "description": "this is my first enterprise router",
    "project_id": "08d5a9564a704afda6039ae2babbef3c",
    "state": "pending",
    "asn": 64512,
    "enable_default_association": true,
    "enable_default_propagation": true,
    "default_propagation_route_table_id": "94c2b814-99dc-939a-e811-ae84c61ea3ff",
    "default_association_route_table_id": "94c2b814-99dc-939a-e811-ae84c61ea3ff",
    "auto_accept_shared_attachments": false,
    "availability_zone_ids": [
      "az1"
    ],
    "created_at": "2019-09-06 02:11:13Z",
    "updated_at": "2019-09-06 02:11:13Z"
  },
  "request_id": "14c2b814-99dc-939b-e81c-ae84c61ea3f7"
}
```

#### 6.3 查询企业路由器详情
```json
{
  "instance": {
    "id": "94c2b814-99dc-939a-e811-ae84c61ea3ff",
    "name": "my_er",
    "description": "this is my first enterprise router",
    "project_id": "08d5a9564a704afda6039ae2babbef3c",
    "state": "pending",
    "asn": 64512,
    "enable_default_association": true,
    "enable_default_propagation": true,
    "default_association_route_table_id": "7f7f738f-453c-40b1-be26-28e7b9e390c1",
    "default_propagation_route_table_id": "7f7f738f-453c-40b1-be26-28e7b9e390c1",
    "auto_accept_shared_attachments": false,
    "availability_zone_ids": [
      "az1"
    ],
    "created_at": "2019-09-06 02:11:13Z",
    "updated_at": "2019-09-06 02:11:13Z"
  },
  "request_id": "14c2b814-99dc-939b-e81c-ae84c61ea3f7"
}
```

#### 6.4 删除企业路由器
``` json
无返回体
```

#### 6.5 查询企业路由器列表
```json
{
  "instances": [
    {
      "id": "94c2b814-99dc-939a-e811-ae84c61ea3ff",
      "name": "my_er",
      "description": "this is my first enterprise router",
      "asn": 64512,
      "project_id": "08d5a9564a704afda6039ae2babbef3c",
      "enable_default_association": true,
      "enable_default_propagation": true,
      "default_association_route_table_id": "7f7f738f-453c-40b1-be26-28e7b9e390c1",
      "default_propagation_route_table_id": "7f7f738f-453c-40b1-be26-28e7b9e390c1",
      "auto_accept_shared_attachments": false,
      "created_at": "2019-09-06 02:11:13Z",
      "updated_at": "2019-09-06 02:11:13Z",
      "tags": [
        {
          "key": "key",
          "value": "value"
        }
      ],
      "enterprise_project_id": 0,
      "availability_zone_ids": [
        "az1",
        "az2"
      ]
    }
  ],
  "request_id": "915a14a6-867b-4af7-83d1-70efceb146f9",
  "page_info": {
    "next_marker": "2",
    "current_count": 1
  }
}
```

## 7、参考
- API参考
  - [API Explorer 创建企业路由器](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=ER&api=CreateEnterpriseRouter)
  - [API Explorer 删除企业路由器](https://console.huaweicloud.com/apiexplorer/#/openapi/ER/doc?api=DeleteEnterpriseRouter)
  - [API Explorer 查询企业路由器列表](https://console.huaweicloud.com/apiexplorer/#/openapi/ER/doc?api=ListEnterpriseRouters)
  - [API Explorer 查询企业路由器详情](https://console.huaweicloud.com/apiexplorer/#/openapi/ER/doc?api=ShowEnterpriseRouter)
  - [API Explorer 更新企业路由器基本信息](https://console.huaweicloud.com/apiexplorer/#/openapi/ER/doc?api=UpdateEnterpriseRouter)
- SDK参考
[ER JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/ER?lang=Java)
- AK SK获取
[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)

## 8、修订记录

|    发布日期    | 文档版本 |  修订说明  |
|:----------:|:----:|:------:|
| 2023/11/08 | 1.0  | 文档首次发布 |