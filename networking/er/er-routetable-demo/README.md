## 1、功能介绍

企业路由器（Enterprise Router, ER）可以连接虚拟私有云（Virtual Private Cloud, VPC）或本地网络来构建中心辐射型组网，是云上大规格、高带宽、高性能的集中路由器。企业路由器使用边界网关协议（Border Gateway Protocol, BGP），支持路由学习、动态选路以及链路切换，极大的提升网络的可扩展性及运维效率，从而保证业务的连续性。

本示例展示如何使用企业路由器路由表（RouteTable）相关SDK

## 2、流程图
![流程图](./assets/er_routetable.png)

## 3、前置条件
- 已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)
- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。具体请参见[ER JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/ER?lang=Java)
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。具体请参见[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。

## 4、SDK获取和安装
您可以通过Maven配置所依赖的企业路由器服务SDK

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java)  (产品类别：企业路由器)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-er</artifactId>
    <version>3.1.2</version>
</dependency>
```

## 5、关键代码片段
企业路由器路由表的增、删、改、查功能。由于创建、更新和删除操作是异步接口，在进行调试时，需要注释无关的代码。
```java
package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.er.v3.ErClient;
import com.huaweicloud.sdk.er.v3.model.CreateRouteTableRequest;
import com.huaweicloud.sdk.er.v3.model.CreateRouteTableRequestBody;
import com.huaweicloud.sdk.er.v3.model.CreateRouteTableResponse;
import com.huaweicloud.sdk.er.v3.model.DeleteRouteTableRequest;
import com.huaweicloud.sdk.er.v3.model.DeleteRouteTableResponse;
import com.huaweicloud.sdk.er.v3.model.ListRouteTablesRequest;
import com.huaweicloud.sdk.er.v3.model.ListRouteTablesResponse;
import com.huaweicloud.sdk.er.v3.model.ShowRouteTableRequest;
import com.huaweicloud.sdk.er.v3.model.ShowRouteTableResponse;
import com.huaweicloud.sdk.er.v3.model.UpdateRouteTableRequest;
import com.huaweicloud.sdk.er.v3.model.UpdateRouteTableRequestBody;
import com.huaweicloud.sdk.er.v3.model.UpdateRouteTableResponse;
import com.huaweicloud.sdk.er.v3.region.ErRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

public class EnterpriseRouterRouteTableDemo {
  private static final Logger LOGGER = LoggerFactory.getLogger(EnterpriseRouterRouteTableDemo.class.getName());

  public static void main(String[] args) {
    // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    String erId = "{er_id}";
    String routeTableId = "{route_table_id}";

    ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

    ErClient client = ErClient.newBuilder()
            .withCredential(auth)
            .withRegion(ErRegion.valueOf("cn-south-1"))
            .build();

    // Create route table
    createRouteTable(client, erId);

    // Update route table
    updateRouteTable(client, erId, routeTableId);

    // Show route table
    showRouteTable(client, erId, routeTableId);

    // Delete route table
    deleteRouteTable(client, erId, routeTableId);

    // List route tables
    listRouteTables(client, erId);
  }

  private static CreateRouteTableResponse createRouteTable(ErClient client, String erId) {
    CreateRouteTableRequest request = new CreateRouteTableRequest();
    CreateRouteTableRequestBody body = new CreateRouteTableRequestBody();
    body.withRouteTable(routeTable -> {
      routeTable.setName("<your route table name>");
      routeTable.setDescription("<your route table description>");
    });
    request.withErId(erId).withBody(body);
    Function<Void, CreateRouteTableResponse> task = (Void v) -> {
      return client.createRouteTable(request);
    };
    return execute_routetable(task);
  }

  private static UpdateRouteTableResponse updateRouteTable(ErClient client, String erId, String routeTableId) {
    UpdateRouteTableRequest request = new UpdateRouteTableRequest();
    UpdateRouteTableRequestBody body = new UpdateRouteTableRequestBody();
    body.withRouteTable(routeTable -> {
      routeTable.setName("<your new route table name>");
      routeTable.setDescription("<your new route table description>");
    });
    request.withErId(erId).withRouteTableId(routeTableId).withBody(body);
    Function<Void, UpdateRouteTableResponse> task = (Void v) -> {
      return client.updateRouteTable(request);
    };
    return execute_routetable(task);
  }

  private static ShowRouteTableResponse showRouteTable(ErClient client, String erId, String routeTableId) {
    ShowRouteTableRequest request = new ShowRouteTableRequest();
    request.withErId(erId).withRouteTableId(routeTableId);
    Function<Void, ShowRouteTableResponse> task = (Void v) -> {
      return client.showRouteTable(request);
    };
    return execute_routetable(task);
  }

  private static DeleteRouteTableResponse deleteRouteTable(ErClient client, String erId, String routeTableId) {
    DeleteRouteTableRequest request = new DeleteRouteTableRequest();
    request.withErId(erId).withRouteTableId(routeTableId);
    Function<Void, DeleteRouteTableResponse> task = (Void v) -> {
      return client.deleteRouteTable(request);
    };
    return execute_routetable(task);
  }

  private static ListRouteTablesResponse listRouteTables(ErClient client, String erId) {
    ListRouteTablesRequest request = new ListRouteTablesRequest();
    request.withErId(erId);
    Function<Void, ListRouteTablesResponse> task = (Void v) -> {
      return client.listRouteTables(request);
    };
    return execute_routetable(task);
  }

  private static <T> T execute_routetable(Function<Void, T> task) {
    T response_routetable = null;
    try {
      response_routetable = task.apply(null);
      LOGGER.info(response_routetable.toString());
    } catch (ClientRequestException e) {
      LOGGER.error(String.valueOf(e.getHttpStatusCode()));
      LOGGER.error(e.toString());
    } catch (ServerResponseException e) {
      LOGGER.error(String.valueOf(e.getHttpStatusCode()));
      LOGGER.error(e.getMessage());
    }
    return response_routetable;
  }
}


```

## 6、运行结果
#### 6.1 创建路由表
``` json
{
  "route_table": {
    "id": "4ab54142-7c92-48ad-8288-77727a231052",
    "name": "my-route-table",
    "is_default_association": false,
    "is_default_propagation": false,
    "state": "pending",
    "created_at": "2020-03-11T15:13:31Z",
    "updated_at": "2020-03-11T15:13:31Z",
    "tags": [
      {
        "key": "key",
        "value": "value"
      }
    ]
  },
  "request_id": "915a14a6-867b-4af7-83d1-70efceb146f9"
}
```

#### 6.2 更新路由表基本信息
``` json
{
  "route_table": {
    "id": "4ab54142-7c92-48ad-8288-77727a231052",
    "name": "new-rtb",
    "is_default_association": false,
    "is_default_propagation": false,
    "state": "pending",
    "created_at": "2020-03-11T15:13:31Z",
    "updated_at": "2020-03-11T15:13:31Z"
  },
  "request_id": "915a14a6-867b-4af7-83d1-70efceb146f9"
}
```

#### 6.3 查询路由表详情
``` json
{
  "route_table": {
    "id": "4ab54142-7c92-48ad-8288-77727a231052",
    "name": "my-route-table",
    "description": "rtb-for-a",
    "is_default_association": false,
    "is_default_propagation": false,
    "state": "available",
    "created_at": "2020-03-11T15:13:31Z",
    "updated_at": "2020-03-11T15:13:31Z"
  },
  "request_id": "915a14a6-867b-4af7-83d1-70efceb146f9"
}
```

#### 6.4 删除路由表
``` json
无返回体
```

#### 6.5 查询路由表列表
``` json
{
  "route_tables": [
    {
      "id": "4ab54142-7c92-48ad-8288-77727a231052",
      "is_default_association": false,
      "is_default_propagation": false,
      "name": "my-router-table1",
      "description": "rtb-for-a",
      "state": "available",
      "tags": [
        {
          "key": "key",
          "value": "value"
        }
      ]
    },
    {
      "id": "4ab54142-7c92-48ad-8288-77727a231053",
      "is_default_association": false,
      "is_default_propagation": false,
      "name": "my-router-table2",
      "description": "rtb-for-b",
      "state": "available",
      "tags": [
        {
          "key": "key",
          "value": "value"
        }
      ]
    }
  ],
  "page_info": {
    "next_marker": "1",
    "current_count": 2
  },
  "request_id": "915a14a6-867b-4af7-83d1-70efceb146f9"
}
```


## 7、参考
- API参考
  - [API Explorer 创建路由表](https://console.huaweicloud.com/apiexplorer/#/openapi/ER/doc?api=CreateRouteTable)
  - [API Explorer 删除路由表](https://console.huaweicloud.com/apiexplorer/#/openapi/ER/doc?api=DeleteRouteTable)
  - [API Explorer 查询路由表列表](https://console.huaweicloud.com/apiexplorer/#/openapi/ER/doc?api=ListRouteTables)
  - [API Explorer 查询路由表详情](https://console.huaweicloud.com/apiexplorer/#/openapi/ER/doc?api=ShowRouteTable)
  - [API Explorer 更新路由表基本信息](https://console.huaweicloud.com/apiexplorer/#/openapi/ER/doc?api=UpdateRouteTable)
- SDK参考
  [ER JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/ER?lang=Java)
- AK SK获取
[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)

## 8、修订记录

|    发布日期    | 文档版本 |  修订说明  |
|:----------:|:----:|:------:|
| 2023/11/08 | 1.0  | 文档首次发布 |