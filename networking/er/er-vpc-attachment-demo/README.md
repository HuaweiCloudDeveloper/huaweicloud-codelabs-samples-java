## 1、功能介绍

企业路由器（Enterprise Router, ER）可以连接虚拟私有云（Virtual Private Cloud, VPC）或本地网络来构建中心辐射型组网，是云上大规格、高带宽、高性能的集中路由器。企业路由器使用边界网关协议（Border Gateway Protocol, BGP），支持路由学习、动态选路以及链路切换，极大的提升网络的可扩展性及运维效率，从而保证业务的连续性。

本示例展示如何使用企业路由器的VPC连接（VpcAttachment）相关SDK

## 2、流程图
![流程图](./assets/er_vpcattachment.png)

## 3、前置条件
- 已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)
- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。具体请参见[ER JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/ER?lang=Java)
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。具体请参见[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。

## 4、SDK获取和安装
您可以通过Maven配置所依赖的企业路由器服务SDK

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java)  (产品类别：企业路由器)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-er</artifactId>
    <version>3.1.2</version>
</dependency>
```

## 5、关键代码片段
企业路由器的VPC连接的增、删、改、查功能.由于创建、更新和删除操作是异步接口，在进行调试时，需要注释无关的代码。
```java
package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.er.v3.ErClient;
import com.huaweicloud.sdk.er.v3.model.CreateVpcAttachmentBody;
import com.huaweicloud.sdk.er.v3.model.CreateVpcAttachmentRequest;
import com.huaweicloud.sdk.er.v3.model.CreateVpcAttachmentResponse;
import com.huaweicloud.sdk.er.v3.model.DeleteVpcAttachmentRequest;
import com.huaweicloud.sdk.er.v3.model.DeleteVpcAttachmentResponse;
import com.huaweicloud.sdk.er.v3.model.ListVpcAttachmentsRequest;
import com.huaweicloud.sdk.er.v3.model.ListVpcAttachmentsResponse;
import com.huaweicloud.sdk.er.v3.model.ShowVpcAttachmentRequest;
import com.huaweicloud.sdk.er.v3.model.ShowVpcAttachmentResponse;
import com.huaweicloud.sdk.er.v3.model.UpdateVpcAttachmentRequest;
import com.huaweicloud.sdk.er.v3.model.UpdateVpcAttachmentRequestBody;
import com.huaweicloud.sdk.er.v3.model.UpdateVpcAttachmentResponse;
import com.huaweicloud.sdk.er.v3.region.ErRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

public class EnterpriseRouterVpcAttachmentDemo {
  private static final Logger LOGGER = LoggerFactory.getLogger(EnterpriseRouterVpcAttachmentDemo.class.getName());

  public static void main(String[] args) {
    // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
    String erId = "{er_id}";
    String vpcAttachmentId = "{vpc_attachment_id}";

    ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

    ErClient client = ErClient.newBuilder()
            .withCredential(auth)
            .withRegion(ErRegion.valueOf("cn-south-1"))
            .build();

    // Create Vpc Attachment
    createVpcAttachment(client, erId);

    // Update Vpc Attachment
    updateVpcAttachment(client, erId, vpcAttachmentId);

    // Show Vpc Attachment
    showVpcAttachment(client, erId, vpcAttachmentId);

    // Delete Vpc Attachment
    deleteVpcAttachment(client, erId, vpcAttachmentId);

    // List Vpc Attachments
    listVpcAttachments(client, erId);
  }

  private static CreateVpcAttachmentResponse createVpcAttachment(ErClient client, String erId) {
    CreateVpcAttachmentRequest request = new CreateVpcAttachmentRequest();
    CreateVpcAttachmentBody body = new CreateVpcAttachmentBody();
    body.withVpcAttachment(vpcAttachment -> {
      vpcAttachment.setName("<your vpc attachment name>");
      vpcAttachment.setVpcId("<your vpc id>");
      vpcAttachment.setVirsubnetId("<your subnet id>");
    });
    request.withErId(erId).withBody(body);

    Function<Void, CreateVpcAttachmentResponse> task = (Void v) -> {
      return client.createVpcAttachment(request);
    };
    return execute_vpc_attachment(task);
  }

  private static UpdateVpcAttachmentResponse updateVpcAttachment(ErClient client, String erId,
          String vpcAttachmentId) {
    UpdateVpcAttachmentRequest request = new UpdateVpcAttachmentRequest();
    UpdateVpcAttachmentRequestBody body = new UpdateVpcAttachmentRequestBody();
    body.withVpcAttachment(vpcAttachment -> {
      vpcAttachment.setName("<your new vpc attachment name>");
      vpcAttachment.setDescription("<your new vpc attachment description>");
    });

    request.withErId(erId).withVpcAttachmentId(vpcAttachmentId).withBody(body);
    Function<Void, UpdateVpcAttachmentResponse> task = (Void v) -> {
      return client.updateVpcAttachment(request);
    };
    return execute_vpc_attachment(task);
  }

  private static ShowVpcAttachmentResponse showVpcAttachment(ErClient client, String erId, String vpcAttachmentId) {
    ShowVpcAttachmentRequest request = new ShowVpcAttachmentRequest();
    request.withErId(erId).withVpcAttachmentId(vpcAttachmentId);
    Function<Void, ShowVpcAttachmentResponse> task = (Void v) -> {
      return client.showVpcAttachment(request);
    };
    return execute_vpc_attachment(task);
  }

  private static DeleteVpcAttachmentResponse deleteVpcAttachment(ErClient client, String erId,
          String vpcAttachmentId) {
    DeleteVpcAttachmentRequest request = new DeleteVpcAttachmentRequest();
    request.withErId(erId).withVpcAttachmentId(vpcAttachmentId);
    Function<Void, DeleteVpcAttachmentResponse> task = (Void v) -> {
      return client.deleteVpcAttachment(request);
    };
    return execute_vpc_attachment(task);
  }

  private static ListVpcAttachmentsResponse listVpcAttachments(ErClient client, String erId) {
    ListVpcAttachmentsRequest request = new ListVpcAttachmentsRequest();
    request.withErId(erId);
    Function<Void, ListVpcAttachmentsResponse> task = (Void v) -> {
      return client.listVpcAttachments(request);
    };
    return execute_vpc_attachment(task);
  }

  private static <T> T execute_vpc_attachment(Function<Void, T> task) {
    T response_vpc_attachment = null;
    try {
      response_vpc_attachment = task.apply(null);
      LOGGER.info(response_vpc_attachment.toString());
    } catch (ClientRequestException e) {
      LOGGER.error(String.valueOf(e.getHttpStatusCode()));
      LOGGER.error(e.toString());
    } catch (ServerResponseException e) {
      LOGGER.error(String.valueOf(e.getHttpStatusCode()));
      LOGGER.error(e.getMessage());
    }
    return response_vpc_attachment;
  }
}


```

## 6、运行结果
#### 6.1 创建VPC连接
```json
{
  "vpc_attachment": {
    "id": "6f83b848-8331-4271-ac0c-ef94b7686402",
    "name": "vpc-atta",
    "vpc_id": "b715e131-3371-4e17-a2de-4f669e24439a",
    "virsubnet_id": "aacdc21d-90f9-45ef-ab48-80ec1bbe15b8",
    "project_id": "08d5a9564a704afda6039ae2babbef3c",
    "state": "pending",
    "auto_create_vpc_routes": false,
    "created_at": "2020-03-11T15:13:31Z",
    "updated_at": "2020-03-11T15:13:31Z"
  },
  "request_id": "915a14a6-867b-4af7-83d1-70efceb146f9"
}
```

#### 6.2 更新VPC连接基本信息
```json
{
  "vpc_attachment": {
    "id": "3b9724e9-49ec-4d21-9191-3d703133b910",
    "name": "new-vpc-attach",
    "vpc_id": "b715e131-3371-4e17-a2de-4f669e24439a",
    "virsubnet_id": "aacdc21d-90f9-45ef-ab48-80ec1bbe15b8",
    "auto_create_vpc_routes": false,
    "project_id": "08d5a9564a704afda6039ae2babbef3c",
    "state": "available",
    "created_at": "2020-03-11T15:13:31Z",
    "updated_at": "2020-03-11T15:13:31Z"
  },
  "request_id": "915a14a6-867b-4af7-83d1-70efceb146f9"
}
```

#### 6.3 查询VPC连接详情
```json
{
  "vpc_attachment": {
    "id": "b70aee08-c671-4cad-9fd5-7381d163bcc8",
    "name": "vpc-attach",
    "vpc_id": "b715e131-3371-4e17-a2de-4f669e24439a",
    "virsubnet_id": "aacdc21d-90f9-45ef-ab48-80ec1bbe15b8",
    "project_id": "08d5a9564a704afda6039ae2babbef3c",
    "state": "available",
    "created_at": "2020-03-11T15:13:31Z",
    "updated_at": "2020-03-11T15:13:31Z"
  },
  "request_id": "915a14a6-867b-4af7-83d1-70efceb146f9"
}
```

#### 6.4 删除VPC连接
``` json
无返回体
```

#### 6.5 查询VPC连接列表
```json
{
  "vpc_attachments": [
    {
      "id": "6f83b848-8331-4271-ac0c-ef94b7686402",
      "name": "vpc-attach-01",
      "vpc_id": "b715e131-3371-4e17-a2de-4f669e24439a",
      "virsubnet_id": "aacdc21d-90f9-45ef-ab48-80ec1bbe15b8",
      "project_id": "08d5a9564a704afda6039ae2babbef3c",
      "state": "available",
      "created_at": "2020-03-11T15:13:31Z",
      "updated_at": "2020-03-11T15:13:31Z"
    },
    {
      "id": "6f83b848-8331-4271-ac0c-ef94b7686402",
      "name": "vpc-attach-01",
      "vpc_id": "4b8567f6-358f-4a7f-8cd3-3cbb82c0b25f",
      "virsubnet_id": "2b55f334-a15e-43a9-ab11-b34c2dbb6fac",
      "project_id": "08d5a9564a704afda6039ae2babbef3c",
      "state": "available",
      "created_at": "2020-03-11T15:13:31Z",
      "updated_at": "2020-03-11T15:13:31Z"
    }
  ],
  "page_info": {
    "next_marker": "1",
    "current_count": 2
  },
  "request_id": "915a14a6-867b-4af7-83d1-70efceb146f9"
}
```


## 7、参考
- API参考
  - [API Explorer 创建VPC连接](https://console.huaweicloud.com/apiexplorer/#/openapi/ER/doc?api=CreateVpcAttachment)
  - [API Explorer 删除VPC连接](https://console.huaweicloud.com/apiexplorer/#/openapi/ER/doc?api=DeleteVpcAttachment)
  - [API Explorer 查询VPC连接列表](https://console.huaweicloud.com/apiexplorer/#/openapi/ER/doc?api=ListVpcAttachments)
  - [API Explorer 查询VPC连接详情](https://console.huaweicloud.com/apiexplorer/#/openapi/ER/doc?api=ShowVpcAttachment)
  - [API Explorer 更新VPC连接基本信息](https://console.huaweicloud.com/apiexplorer/#/openapi/ER/doc?api=UpdateVpcAttachment)
- SDK参考
[ER JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/ER?lang=Java)
- AK SK获取
[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)

## 8、修订记录

|    发布日期    | 文档版本 |  修订说明  |
|:----------:|:----:|:------:|
| 2023/11/08 | 1.0  | 文档首次发布 |