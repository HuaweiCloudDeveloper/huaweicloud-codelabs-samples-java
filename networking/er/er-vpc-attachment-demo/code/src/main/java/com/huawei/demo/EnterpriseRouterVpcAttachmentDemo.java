package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.er.v3.ErClient;
import com.huaweicloud.sdk.er.v3.model.CreateVpcAttachmentBody;
import com.huaweicloud.sdk.er.v3.model.CreateVpcAttachmentRequest;
import com.huaweicloud.sdk.er.v3.model.CreateVpcAttachmentResponse;
import com.huaweicloud.sdk.er.v3.model.DeleteVpcAttachmentRequest;
import com.huaweicloud.sdk.er.v3.model.DeleteVpcAttachmentResponse;
import com.huaweicloud.sdk.er.v3.model.ListVpcAttachmentsRequest;
import com.huaweicloud.sdk.er.v3.model.ListVpcAttachmentsResponse;
import com.huaweicloud.sdk.er.v3.model.ShowVpcAttachmentRequest;
import com.huaweicloud.sdk.er.v3.model.ShowVpcAttachmentResponse;
import com.huaweicloud.sdk.er.v3.model.UpdateVpcAttachmentRequest;
import com.huaweicloud.sdk.er.v3.model.UpdateVpcAttachmentRequestBody;
import com.huaweicloud.sdk.er.v3.model.UpdateVpcAttachmentResponse;
import com.huaweicloud.sdk.er.v3.region.ErRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

public class EnterpriseRouterVpcAttachmentDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(EnterpriseRouterVpcAttachmentDemo.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String erId = "{er_id}";
        String vpcAttachmentId = "{vpc_attachment_id}";

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        ErClient client = ErClient.newBuilder()
            .withCredential(auth)
            .withRegion(ErRegion.valueOf("cn-south-1"))
            .build();

        // Create Vpc Attachment
        createVpcAttachment(client, erId);

        // Update Vpc Attachment
        updateVpcAttachment(client, erId, vpcAttachmentId);

        // Show Vpc Attachment
        showVpcAttachment(client, erId, vpcAttachmentId);

        // Delete Vpc Attachment
        deleteVpcAttachment(client, erId, vpcAttachmentId);

        // List Vpc Attachments
        listVpcAttachments(client, erId);
    }

    private static CreateVpcAttachmentResponse createVpcAttachment(ErClient client, String erId) {
        CreateVpcAttachmentRequest request = new CreateVpcAttachmentRequest();
        CreateVpcAttachmentBody body = new CreateVpcAttachmentBody();
        body.withVpcAttachment(vpcAttachment -> {
            vpcAttachment.setName("<your vpc attachment name>");
            vpcAttachment.setVpcId("<your vpc id>");
            vpcAttachment.setVirsubnetId("<your subnet id>");
        });
        request.withErId(erId).withBody(body);

        Function<Void, CreateVpcAttachmentResponse> task = (Void v) -> {
            return client.createVpcAttachment(request);
        };
        return execute_vpc_attachment(task);
    }

    private static UpdateVpcAttachmentResponse updateVpcAttachment(ErClient client, String erId,
        String vpcAttachmentId) {
        UpdateVpcAttachmentRequest request = new UpdateVpcAttachmentRequest();
        UpdateVpcAttachmentRequestBody body = new UpdateVpcAttachmentRequestBody();
        body.withVpcAttachment(vpcAttachment -> {
            vpcAttachment.setName("<your new vpc attachment name>");
            vpcAttachment.setDescription("<your new vpc attachment description>");
        });

        request.withErId(erId).withVpcAttachmentId(vpcAttachmentId).withBody(body);
        Function<Void, UpdateVpcAttachmentResponse> task = (Void v) -> {
            return client.updateVpcAttachment(request);
        };
        return execute_vpc_attachment(task);
    }

    private static ShowVpcAttachmentResponse showVpcAttachment(ErClient client, String erId, String vpcAttachmentId) {
        ShowVpcAttachmentRequest request = new ShowVpcAttachmentRequest();
        request.withErId(erId).withVpcAttachmentId(vpcAttachmentId);
        Function<Void, ShowVpcAttachmentResponse> task = (Void v) -> {
            return client.showVpcAttachment(request);
        };
        return execute_vpc_attachment(task);
    }

    private static DeleteVpcAttachmentResponse deleteVpcAttachment(ErClient client, String erId,
        String vpcAttachmentId) {
        DeleteVpcAttachmentRequest request = new DeleteVpcAttachmentRequest();
        request.withErId(erId).withVpcAttachmentId(vpcAttachmentId);
        Function<Void, DeleteVpcAttachmentResponse> task = (Void v) -> {
            return client.deleteVpcAttachment(request);
        };
        return execute_vpc_attachment(task);
    }

    private static ListVpcAttachmentsResponse listVpcAttachments(ErClient client, String erId) {
        ListVpcAttachmentsRequest request = new ListVpcAttachmentsRequest();
        request.withErId(erId);
        Function<Void, ListVpcAttachmentsResponse> task = (Void v) -> {
            return client.listVpcAttachments(request);
        };
        return execute_vpc_attachment(task);
    }

    private static <T> T execute_vpc_attachment(Function<Void, T> task) {
        T response_vpc_attachment = null;
        try {
            response_vpc_attachment = task.apply(null);
            LOGGER.info(response_vpc_attachment.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
        return response_vpc_attachment;
    }
}
