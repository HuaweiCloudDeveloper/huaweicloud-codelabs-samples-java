package com.huawei.dc;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.dc.v3.model.CreateResourceTagRequest;
import com.huaweicloud.sdk.dc.v3.model.CreateResourceTagRequestBody;
import com.huaweicloud.sdk.dc.v3.model.CreateResourceTagResponse;
import com.huaweicloud.sdk.dc.v3.model.Tag;
import com.huaweicloud.sdk.dc.v3.region.DcRegion;
import com.huaweicloud.sdk.dc.v3.DcClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateResourceTag {

    private static final Logger logger = LoggerFactory.getLogger(CreateResourceTag.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String createResourceTagAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String createResourceTagSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(createResourceTagAk)
                .withSk(createResourceTagSk);

        // Create a service client and set the corresponding region to CN_NORTH_4.
        DcClient client = DcClient.newBuilder()
                .withCredential(auth)
                .withRegion(DcRegion.CN_NORTH_4)
                .build();
        // Create a request and set the resource instance ID, Direct Connect resource type, and resource tag in the request.
        CreateResourceTagRequest request = new CreateResourceTagRequest();
        // Resource Instance ID
        String resourceId = "<YOUR RESOURCE ID>";
        request.withResourceId(resourceId);
        // Specifies the resource type of the Direct Connect service. The value can be dc-directconnect/dc-vgw/dc-vif.
        // dc-directconnect: Direct Connect physical connection; dc-vgw: virtual gateway; dc-vif: virtual interface
        request.withResourceType(CreateResourceTagRequest.ResourceTypeEnum.fromValue("dc-vgw"));
        CreateResourceTagRequestBody body = new CreateResourceTagRequestBody();
        // Resource Tag
        Tag tagbody = new Tag();
        // Tag key. The value can contain a maximum of 36 Unicode characters, including uppercase letters,
        // lowercase letters, digits, hyphens (-), and underscores (_).
        // Tag value. The value can contain a maximum of 43 Unicode characters, including uppercase letters,
        // lowercase letters, digits, hyphens (-), underscores (_), and periods (.).
        tagbody.withKey("key")
                .withValue("value");
        body.withTag(tagbody);
        request.withBody(body);
        try {
            CreateResourceTagResponse response = client.createResourceTag(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}