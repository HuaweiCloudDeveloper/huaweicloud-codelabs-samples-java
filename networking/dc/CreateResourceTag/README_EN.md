### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DC/debug?api=CreateResourceTag).

2.In this example, you can add resource tags.

3.Add resource tags. You can add custom tags to Direct Connect resources. Resource tags are used to classify and manage cloud resources, helping users better organize and manage cloud resources. This API is an idempotent API. If the created tag already exists (with the same key), the tag will be overwritten.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. 
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. 
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.You have enabled Direct Connect and have the required permissions.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-dc. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-dc</artifactId>
    <version>3.1.125</version>
</dependency>
```

### Code example
``` java
package com.huawei.dc;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.dc.v3.model.CreateResourceTagRequest;
import com.huaweicloud.sdk.dc.v3.model.CreateResourceTagRequestBody;
import com.huaweicloud.sdk.dc.v3.model.CreateResourceTagResponse;
import com.huaweicloud.sdk.dc.v3.model.Tag;
import com.huaweicloud.sdk.dc.v3.region.DcRegion;
import com.huaweicloud.sdk.dc.v3.DcClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateResourceTag {

    private static final Logger logger = LoggerFactory.getLogger(CreateResourceTag.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String createResourceTagAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String createResourceTagSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(createResourceTagAk)
                .withSk(createResourceTagSk);

        // Create a service client and set the corresponding region to CN_NORTH_4.
        DcClient client = DcClient.newBuilder()
                .withCredential(auth)
                .withRegion(DcRegion.CN_NORTH_4)
                .build();
        // Create a request and set the resource instance ID, Direct Connect resource type, and resource tag in the request.
        CreateResourceTagRequest request = new CreateResourceTagRequest();
        // Resource Instance ID
        String resourceId = "<YOUR RESOURCE ID>";
        request.withResourceId(resourceId);
        // Specifies the resource type of the Direct Connect service. The value can be dc-directconnect/dc-vgw/dc-vif.
        // dc-directconnect: Direct Connect physical connection; dc-vgw: virtual gateway; dc-vif: virtual interface
        request.withResourceType(CreateResourceTagRequest.ResourceTypeEnum.fromValue("dc-vgw"));
        CreateResourceTagRequestBody body = new CreateResourceTagRequestBody();
        // Resource Tag
        Tag tagbody = new Tag();
        // Tag key. The value can contain a maximum of 36 Unicode characters, including uppercase letters, 
        // lowercase letters, digits, hyphens (-), and underscores (_).
        // Tag value. The value can contain a maximum of 43 Unicode characters, including uppercase letters, 
        // lowercase letters, digits, hyphens (-), underscores (_), and periods (.).
        tagbody.withKey("key")
                .withValue("value");
        body.withTag(tagbody);
        request.withBody(body);
        try {
            CreateResourceTagResponse response = client.createResourceTag(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
```
CreateResourceTagResponse {
}
```
### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/12/09 | 1.0 | This document is released for the first time.