package com.huawei.dc;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.dc.v3.model.ShowResourceTagRequest;
import com.huaweicloud.sdk.dc.v3.model.ShowResourceTagResponse;
import com.huaweicloud.sdk.dc.v3.region.DcRegion;
import com.huaweicloud.sdk.dc.v3.DcClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowResourceTag {

    private static final Logger logger = LoggerFactory.getLogger(ShowResourceTag.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String showResourceTagAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showResourceTagSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(showResourceTagAk)
                .withSk(showResourceTagSk);

        // Create a service client and set the corresponding region to CN_NORTH_4.
        DcClient client = DcClient.newBuilder()
                .withCredential(auth)
                .withRegion(DcRegion.CN_NORTH_4)
                .build();
        // Construct a request and set the resource instance ID and Direct Connect resource type in the request.
        ShowResourceTagRequest request = new ShowResourceTagRequest();
        // Specifies the resource type of the Direct Connect service. The value can be dc-directconnect/dc-vgw/dc-vif.
        // dc-directconnect: Direct Connect physical connection; dc-vgw: virtual gateway; dc-vif: virtual interface
        request.withResourceType(ShowResourceTagRequest.ResourceTypeEnum.fromValue("dc-vgw"));
        // Resource Instance ID
        String resourceId = "<YOUR RESOURCE ID>";
        request.withResourceId(resourceId);
        try {
            ShowResourceTagResponse response = client.showResourceTag(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}