### Overview
Direct Connect establishes a dedicated network connection between your data center and the cloud. You can use one connection to access cloud computing resources in different regions, helping build a secure and reliable hybrid environment. Three components are required for establishing network connectivity: a connection, a virtual gateway, and a virtual interface. A virtual interface is the entry for your on-premises data center to access a VPC over a connection. You can create a virtual interface to associate a connection with a virtual gateway and connect the on-premises gateway to the virtual gateway so that your on-premises data center can access the VPC.

This example shows how to use SDK to create, delete, modify, and query virtual interfaces. For details about virtual interfaces, see [Virtual Interfaces](https://support.huaweicloud.com/intl/en-us/usermanual-dc/dc_04_0410.html).

### Flowchart
![Flowchart](./assets/VifProcess_en.png)

### Prerequisites
- You have [registered with Huawei Cloud](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%3Fservice%3Dhttps%253A%252F%252Fwww.huaweicloud.com%252Fintl%252Fen-us%252Fproduct%252Fdc.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin%3Fservice%3Dhttps%253A%252F%252Fwww.huaweicloud.com%252Fintl%252Fen-us%252Fproduct%252Fdc.html&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=f4943375a15f4b5bbfe2a242eb950298&lang=en-us).

- You have obtained the Huawei Cloud SDK. You can also install the Java SDK. For details, see [Direct Connect Java SDK](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/DC?lang=Java).

- You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. To create or view an AK/SK, choose **My Credentials** > **Access Keys** on the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).

- A connection and a virtual gateway have been created for the virtual interface. For detailed operations, see [Creating a Connection](https://support.huaweicloud.com/intl/en-us/qs-dc/dc_03_0003.html) and [Creating a Virtual Gateway](https://support.huaweicloud.com/intl/en-us/qs-dc/dc_03_0004.html).

- You have set up the development environment with Java JDK 1.8 or later.

### Obtaining and Installing an SDK
You can use Maven to configure the dependent Direct Connect SDK.

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-dc</artifactId>
    <version>3.1.60</version>
</dependency>
```

### Sample Code
The following code shows how to use SDK to perform operations on virtual interfaces.

```java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.dc.v3.DcClient;
import com.huaweicloud.sdk.dc.v3.model.CreateVirtualInterface;
import com.huaweicloud.sdk.dc.v3.model.CreateVirtualInterfaceRequest;
import com.huaweicloud.sdk.dc.v3.model.CreateVirtualInterfaceRequestBody;
import com.huaweicloud.sdk.dc.v3.model.CreateVirtualInterfaceResponse;
import com.huaweicloud.sdk.dc.v3.model.DeleteVirtualInterfaceRequest;
import com.huaweicloud.sdk.dc.v3.model.DeleteVirtualInterfaceResponse;
import com.huaweicloud.sdk.dc.v3.model.ListVirtualInterfacesRequest;
import com.huaweicloud.sdk.dc.v3.model.ListVirtualInterfacesResponse;
import com.huaweicloud.sdk.dc.v3.model.ShowVirtualInterfaceRequest;
import com.huaweicloud.sdk.dc.v3.model.ShowVirtualInterfaceResponse;
import com.huaweicloud.sdk.dc.v3.model.UpdateVirtualInterface;
import com.huaweicloud.sdk.dc.v3.model.UpdateVirtualInterfaceRequest;
import com.huaweicloud.sdk.dc.v3.model.UpdateVirtualInterfaceRequestBody;
import com.huaweicloud.sdk.dc.v3.model.UpdateVirtualInterfaceResponse;
import com.huaweicloud.sdk.dc.v3.region.DcRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VirtualInterfaceDemo {

    private static final Logger logger = LoggerFactory.getLogger(VirtualInterfaceDemo.class);

    public static void main(String[] args) {

        // Hardcoded or plaintext AK/SK is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);

        // Create a DcClient instance.
        DcClient client = DcClient.newBuilder()
            .withCredential(auth)
            .withRegion(DcRegion.valueOf("{your region id}"))
            .withHttpConfig(httpConfig)
            .build();

        // Create a virtual interface.
        CreateVirtualInterfaceResponse response = createVirtualInterface(client);

        // Query details about a virtual interface.
        showVirtualInterface(client, response.getVirtualInterface().getId());

        // Update a virtual interface.
        updateVirtualInterface(client, response.getVirtualInterface().getId());

        // Query the virtual interface list.
        listVirtualInterface(client, response.getVirtualInterface().getId());

        // Delete a virtual interface.
        deleteVirtualInterface(client, response.getVirtualInterface().getId());
    }

    private static void deleteVirtualInterface(DcClient client, String id) {
        DeleteVirtualInterfaceRequest request = new DeleteVirtualInterfaceRequest().withVirtualInterfaceId(id);
        try {
            DeleteVirtualInterfaceResponse response = client.deleteVirtualInterface(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void listVirtualInterface(DcClient client, String id) {
        ListVirtualInterfacesRequest request = new ListVirtualInterfacesRequest().withId(Arrays.asList(id));
        try {
            ListVirtualInterfacesResponse response = client.listVirtualInterfaces(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void showVirtualInterface(DcClient client, String id) {
        ShowVirtualInterfaceRequest request = new ShowVirtualInterfaceRequest().withVirtualInterfaceId(id);
        try {
            ShowVirtualInterfaceResponse response = client.showVirtualInterface(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void updateVirtualInterface(DcClient client, String id) {
        UpdateVirtualInterfaceRequest request = new UpdateVirtualInterfaceRequest()
            .withVirtualInterfaceId(id)
            .withBody(new UpdateVirtualInterfaceRequestBody().withVirtualInterface(new UpdateVirtualInterface()
                .withName("{your updated vif name}")
                .withDescription("{your updated vif description}")));
        try {
            UpdateVirtualInterfaceResponse response = client.updateVirtualInterface(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static CreateVirtualInterfaceResponse createVirtualInterface(DcClient client) {
        List<String> remoteEpGroup = new ArrayList<>();
        remoteEpGroup.add("{your remote endpoint group CIDR address}");
        CreateVirtualInterfaceRequest request = new CreateVirtualInterfaceRequest()
            .withBody(new CreateVirtualInterfaceRequestBody()
                .withVirtualInterface(new CreateVirtualInterface()
                    .withName("{your vif name}")
                    .withDescription("{your vif description}")
                    .withDirectConnectId("{your direct connect id}")
                    .withVgwId("{your virtual gateway id}")
                    // The VLAN ID ranges from 0 to 3999. When the VLAN ID is 0, the connection port uses the Layer 3 routing, where only one virtual interface can be created for this connection. You can change the VLAN ID as required.
                    .withVlan(1893)
                    // Bandwidth of the virtual interface. Multiple virtual interfaces share the bandwidth of the associated connection. Specify the bandwidth based on the traffic. The maximum bandwidth that can be specified for a virtual interface is the bandwidth of the associated connection. You can change the bandwidth as required.
                    .withBandwidth(2)
                    .withLocalGatewayV4Ip("{your local gateway IPv4 address}")
                    .withRemoteGatewayV4Ip("{your remote gateway IPv4 address}")
                    .withType(CreateVirtualInterface.TypeEnum.PRIVATE)
                    .withRouteMode(CreateVirtualInterface.RouteModeEnum.STATIC)
                    .withRemoteEpGroup(remoteEpGroup)));
        CreateVirtualInterfaceResponse response = null;
        try {
            response = client.createVirtualInterface(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
        return response;
    }
}
```

For details, see [Direct Connect Documentation](https://support.huaweicloud.com/intl/en-us/dc/index.html) and [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/DC/doc?api=CreateVirtualInterface).

### Example Responses
#### - Creating a Virtual Interface
```java
virtualInterface: class VirtualInterface {
    id: 2425c9ac-8ca6-4034-b1c9-34d2e6f5afce
    name: vif-sdktest-created
    adminStateUp: true
    bandwidth: 2
    createTime: 2023-09-26T00:06:54Z
    updateTime: null
    description: test create vif
    directConnectId: c526dfa1-c389-427c-8cb0-790f80220f55
    serviceType: VGW
    status: PENDING_CREATE
    tenantId: f149b9c864584edbb69e5374a81bdae1
    type: private
    vgwId: 1e115445-062e-4e40-b6e2-adc4bd9fed4b
    vlan: 1893
    routeLimit: 50
    enableNqa: false
    enableBfd: false
    lagId: null
    deviceId: 10.94.40.20
    enterpriseProjectId: null
    tags: null
    vifPeers: null
    extendAttribute: null
}
```

#### - Querying Details About a Virtual Interface
```java
virtualInterface: class VirtualInterface {
        id: 2425c9ac-8ca6-4034-b1c9-34d2e6f5afce
        name: vif-sdktest-created
        adminStateUp: true
        bandwidth: 2
        createTime: 2023-09-26T08:06:54Z
        updateTime: null
        description: test create vif
        directConnectId: c526dfa1-c389-427c-8cb0-790f80220f55
        serviceType: VGW
        status: PENDING_CREATE
        tenantId: f149b9c864584edbb69e5374a81bdae1
        type: private
        vgwId: 1e115445-062e-4e40-b6e2-adc4bd9fed4b
        vlan: 1893
        routeLimit: 50
        enableNqa: false
        enableBfd: false
        lagId: null
        deviceId: 10.94.40.20
        enterpriseProjectId: 0
        tags: null
        vifPeers: [class VifPeer {
            id: a5a953d8-47f6-478a-ab69-8bdf3ca728f9
            tenantId: f149b9c864584edbb69e5374a81bdae1
            name: vif-sdktest-created
            description: test create vif
            addressFamily: ipv4
            localGatewayIp: 1.1.1.1/30
            remoteGatewayIp: 1.1.1.2/30
            routeMode: static
            bgpAsn: null
            bgpMd5: null
            remoteEpGroup: [1.1.1.0/30, 1.1.2.0/30]
            serviceEpGroup: null
            deviceId: 10.94.40.20
            bgpRouteLimit: 100
            bgpStatus: null
            status: PENDING_CREATE
            vifId: 2425c9ac-8ca6-4034-b1c9-34d2e6f5afce
        }]
        extendAttribute: null
    }
```

#### - Updating a Virtual Interface
```java
virtualInterface: class VirtualInterface {
        id: 2425c9ac-8ca6-4034-b1c9-34d2e6f5afce
        name: vif-sdktest-updated
        adminStateUp: true
        bandwidth: 2
        createTime: 2023-09-26T08:06:54Z
        updateTime: null
        description: test update vif
        directConnectId: c526dfa1-c389-427c-8cb0-790f80220f55
        serviceType: VGW
        status: PENDING_CREATE
        tenantId: f149b9c864584edbb69e5374a81bdae1
        type: private
        vgwId: 1e115445-062e-4e40-b6e2-adc4bd9fed4b
        vlan: 1893
        routeLimit: 50
        enableNqa: false
        enableBfd: false
        lagId: null
        deviceId: 10.94.40.20
        enterpriseProjectId: 0
        tags: null
        vifPeers: [class VifPeer {
            id: a5a953d8-47f6-478a-ab69-8bdf3ca728f9
            tenantId: f149b9c864584edbb69e5374a81bdae1
            name: vif-sdktest-created
            description: test create vif
            addressFamily: ipv4
            localGatewayIp: 1.1.1.1/30
            remoteGatewayIp: 1.1.1.2/30
            routeMode: static
            bgpAsn: null
            bgpMd5: null
            remoteEpGroup: [1.1.1.0/30, 1.1.2.0/30]
            serviceEpGroup: null
            deviceId: 10.94.40.20
            bgpRouteLimit: 100
            bgpStatus: null
            status: PENDING_CREATE
            vifId: 2425c9ac-8ca6-4034-b1c9-34d2e6f5afce
        }]
        extendAttribute: null
    }
```

#### - Querying the Virtual Interface List
```java
virtualInterfaces: [class VirtualInterface {
        id: 2425c9ac-8ca6-4034-b1c9-34d2e6f5afce
        name: vif-sdktest-updated
        adminStateUp: true
        bandwidth: 2
        createTime: 2023-09-26T08:06:54Z
        updateTime: null
        description: test update vif
        directConnectId: c526dfa1-c389-427c-8cb0-790f80220f55
        serviceType: VGW
        status: PENDING_CREATE
        tenantId: f149b9c864584edbb69e5374a81bdae1
        type: private
        vgwId: 1e115445-062e-4e40-b6e2-adc4bd9fed4b
        vlan: 1893
        routeLimit: 50
        enableNqa: false
        enableBfd: false
        lagId: null
        deviceId: 10.94.40.20
        enterpriseProjectId: 0
        tags: null
        vifPeers: [class VifPeer {
            id: a5a953d8-47f6-478a-ab69-8bdf3ca728f9
            tenantId: f149b9c864584edbb69e5374a81bdae1
            name: vif-sdktest-updated
            description: test update vif
            addressFamily: ipv4
            localGatewayIp: 1.1.1.1/30
            remoteGatewayIp: 1.1.1.2/30
            routeMode: static
            bgpAsn: null
            bgpMd5: null
            remoteEpGroup: [1.1.1.0/30, 1.1.2.0/30]
            serviceEpGroup: null
            deviceId: 10.94.40.20
            bgpRouteLimit: 100
            bgpStatus: null
            status: PENDING_CREATE
            vifId: 2425c9ac-8ca6-4034-b1c9-34d2e6f5afce
        }]
        extendAttribute: null
    }]
```

#### - Deleting a Virtual Interface
No response

### Change History

 Release Date | Issue| Description
 :----: | :-----: | :------:  
 2024/04/18 |1.0 | This issue is the first official release.
