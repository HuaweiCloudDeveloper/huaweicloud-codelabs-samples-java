package com.huawei.dc.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.dc.v3.DcClient;
import com.huaweicloud.sdk.dc.v3.model.CreateVirtualInterface;
import com.huaweicloud.sdk.dc.v3.model.CreateVirtualInterfaceRequest;
import com.huaweicloud.sdk.dc.v3.model.CreateVirtualInterfaceRequestBody;
import com.huaweicloud.sdk.dc.v3.model.CreateVirtualInterfaceResponse;
import com.huaweicloud.sdk.dc.v3.model.DeleteVirtualInterfaceRequest;
import com.huaweicloud.sdk.dc.v3.model.DeleteVirtualInterfaceResponse;
import com.huaweicloud.sdk.dc.v3.model.ListVirtualInterfacesRequest;
import com.huaweicloud.sdk.dc.v3.model.ListVirtualInterfacesResponse;
import com.huaweicloud.sdk.dc.v3.model.ShowVirtualInterfaceRequest;
import com.huaweicloud.sdk.dc.v3.model.ShowVirtualInterfaceResponse;
import com.huaweicloud.sdk.dc.v3.model.UpdateVirtualInterface;
import com.huaweicloud.sdk.dc.v3.model.UpdateVirtualInterfaceRequest;
import com.huaweicloud.sdk.dc.v3.model.UpdateVirtualInterfaceRequestBody;
import com.huaweicloud.sdk.dc.v3.model.UpdateVirtualInterfaceResponse;
import com.huaweicloud.sdk.dc.v3.region.DcRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VirtualInterfaceDemo {

    private static final Logger logger = LoggerFactory.getLogger(VirtualInterfaceDemo.class);

    public static void main(String[] args) {

        // Hardcoded or plaintext AK/SK is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);

        // Create a DcClient instance.
        DcClient client = DcClient.newBuilder()
            .withCredential(auth)
            .withRegion(DcRegion.valueOf("{your region id}"))
            .withHttpConfig(httpConfig)
            .build();

        // Create a virtual interface.
        CreateVirtualInterfaceResponse response = createVirtualInterface(client);

        // Query details about a virtual interface.
        showVirtualInterface(client, response.getVirtualInterface().getId());

        // Update a virtual interface.
        updateVirtualInterface(client, response.getVirtualInterface().getId());

        // Query the virtual interface list.
        listVirtualInterface(client, response.getVirtualInterface().getId());

        // Delete a virtual interface.
        deleteVirtualInterface(client, response.getVirtualInterface().getId());
    }

    private static void deleteVirtualInterface(DcClient client, String id) {
        DeleteVirtualInterfaceRequest request = new DeleteVirtualInterfaceRequest().withVirtualInterfaceId(id);
        try {
            DeleteVirtualInterfaceResponse response = client.deleteVirtualInterface(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void listVirtualInterface(DcClient client, String id) {
        ListVirtualInterfacesRequest request = new ListVirtualInterfacesRequest().withId(Arrays.asList(id));
        try {
            ListVirtualInterfacesResponse response = client.listVirtualInterfaces(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void showVirtualInterface(DcClient client, String id) {
        ShowVirtualInterfaceRequest request = new ShowVirtualInterfaceRequest().withVirtualInterfaceId(id);
        try {
            ShowVirtualInterfaceResponse response = client.showVirtualInterface(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void updateVirtualInterface(DcClient client, String id) {
        UpdateVirtualInterfaceRequest request = new UpdateVirtualInterfaceRequest()
            .withVirtualInterfaceId(id)
            .withBody(new UpdateVirtualInterfaceRequestBody().withVirtualInterface(new UpdateVirtualInterface()
                .withName("{your updated vif name}")
                .withDescription("{your updated vif description}")));
        try {
            UpdateVirtualInterfaceResponse response = client.updateVirtualInterface(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static CreateVirtualInterfaceResponse createVirtualInterface(DcClient client) {
        List<String> remoteEpGroup = new ArrayList<>();
        remoteEpGroup.add("{your remote endpoint group CIDR address}");
        CreateVirtualInterfaceRequest request = new CreateVirtualInterfaceRequest()
            .withBody(new CreateVirtualInterfaceRequestBody()
                .withVirtualInterface(new CreateVirtualInterface()
                    .withName("{your vif name}")
                    .withDescription("{your vif description}")
                    .withDirectConnectId("{your direct connect id}")
                    .withVgwId("{your virtual gateway id}")
                    // The VLAN ID ranges from 0 to 3999. When the VLAN ID is 0, the connection port uses the Layer 3 routing, where only one virtual interface can be created for this connection. You can change the VLAN ID as required.
                    .withVlan(1893)
                    // Bandwidth of the virtual interface. Multiple virtual interfaces share the bandwidth of the associated connection. Specify the bandwidth based on the traffic. The maximum bandwidth that can be specified for a virtual interface is the bandwidth of the associated connection. You can change the bandwidth as required.
                    .withBandwidth(2)
                    .withLocalGatewayV4Ip("{your local gateway IPv4 address}")
                    .withRemoteGatewayV4Ip("{your remote gateway IPv4 address}")
                    .withType(CreateVirtualInterface.TypeEnum.PRIVATE)
                    .withRouteMode(CreateVirtualInterface.RouteModeEnum.STATIC)
                    .withRemoteEpGroup(remoteEpGroup)));
        CreateVirtualInterfaceResponse response = null;
        try {
            response = client.createVirtualInterface(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
        return response;
    }
}
