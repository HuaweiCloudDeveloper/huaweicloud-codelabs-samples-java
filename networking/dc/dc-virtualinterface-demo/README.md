### 介绍
云专线（Direct Connect）为用户搭建本地数据中心与云上VPC之间的专属连接通道，用户可以通过一条物理连接打通不同地域的云计算资源，实现安全可靠的混合云部署。云专线由物理连接、虚拟网关、虚拟接口三部分组成，其中虚拟接口是用户本地数据中心通过专线访问VPC的入口，用户创建虚拟接口关联物理连接和虚拟网关，连通用户网关和虚拟网关，实现云下数据中心和云上VPC的互访。

本示例展示如何使用虚拟接口（VirtualInterface）相关SDK，主要介绍虚拟接口的增、删、改、查功能。虚拟接口请参见[虚拟接口管理](https://support.huaweicloud.com/usermanual-dc/dc_04_0410.html)。

### 流程图
![流程图](./assets/VifProcess.png)

### 前置条件
1. [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2. 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。具体请参见[DC JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/DC?lang=Java)。

3. 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)。

4. 已创建虚拟接口需要绑定的资源，包括[物理连接接入](https://support.huaweicloud.com/intl/zh-cn/qs-dc/dc_03_0003.html)和[创建虚拟网关](https://support.huaweicloud.com/intl/zh-cn/qs-dc/dc_03_0004.html)。

5. 开发环境支持Java JDK 1.8及其以上版本。

### SDK获取和安装
您可以通过Maven配置所依赖的云专线服务SDK

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-dc</artifactId>
    <version>3.1.60</version>
</dependency>
```

### 代码示例
以下代码展示了如何使用虚拟接口（VirtualInterface）相关SDK

```java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.dc.v3.DcClient;
import com.huaweicloud.sdk.dc.v3.model.CreateVirtualInterface;
import com.huaweicloud.sdk.dc.v3.model.CreateVirtualInterfaceRequest;
import com.huaweicloud.sdk.dc.v3.model.CreateVirtualInterfaceRequestBody;
import com.huaweicloud.sdk.dc.v3.model.CreateVirtualInterfaceResponse;
import com.huaweicloud.sdk.dc.v3.model.DeleteVirtualInterfaceRequest;
import com.huaweicloud.sdk.dc.v3.model.DeleteVirtualInterfaceResponse;
import com.huaweicloud.sdk.dc.v3.model.ListVirtualInterfacesRequest;
import com.huaweicloud.sdk.dc.v3.model.ListVirtualInterfacesResponse;
import com.huaweicloud.sdk.dc.v3.model.ShowVirtualInterfaceRequest;
import com.huaweicloud.sdk.dc.v3.model.ShowVirtualInterfaceResponse;
import com.huaweicloud.sdk.dc.v3.model.UpdateVirtualInterface;
import com.huaweicloud.sdk.dc.v3.model.UpdateVirtualInterfaceRequest;
import com.huaweicloud.sdk.dc.v3.model.UpdateVirtualInterfaceRequestBody;
import com.huaweicloud.sdk.dc.v3.model.UpdateVirtualInterfaceResponse;
import com.huaweicloud.sdk.dc.v3.region.DcRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VirtualInterfaceDemo {

    private static final Logger logger = LoggerFactory.getLogger(VirtualInterfaceDemo.class);

    public static void main(String[] args) {

        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);

        // 创建DcClient实例
        DcClient client = DcClient.newBuilder()
            .withCredential(auth)
            .withRegion(DcRegion.valueOf("{your region id}"))
            .withHttpConfig(httpConfig)
            .build();

        // 1. 创建虚拟接口
        CreateVirtualInterfaceResponse response = createVirtualInterface(client);

        // 2. 查询虚拟接口详情
        showVirtualInterface(client, response.getVirtualInterface().getId());

        // 3. 更新虚拟接口
        updateVirtualInterface(client, response.getVirtualInterface().getId());

        // 4. 查询虚拟接口列表
        listVirtualInterface(client, response.getVirtualInterface().getId());

        // 5. 删除虚拟接口
        deleteVirtualInterface(client, response.getVirtualInterface().getId());
    }

    private static void deleteVirtualInterface(DcClient client, String id) {
        DeleteVirtualInterfaceRequest request = new DeleteVirtualInterfaceRequest().withVirtualInterfaceId(id);
        try {
            DeleteVirtualInterfaceResponse response = client.deleteVirtualInterface(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void listVirtualInterface(DcClient client, String id) {
        ListVirtualInterfacesRequest request = new ListVirtualInterfacesRequest().withId(Arrays.asList(id));
        try {
            ListVirtualInterfacesResponse response = client.listVirtualInterfaces(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void showVirtualInterface(DcClient client, String id) {
        ShowVirtualInterfaceRequest request = new ShowVirtualInterfaceRequest().withVirtualInterfaceId(id);
        try {
            ShowVirtualInterfaceResponse response = client.showVirtualInterface(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static void updateVirtualInterface(DcClient client, String id) {
        UpdateVirtualInterfaceRequest request = new UpdateVirtualInterfaceRequest()
            .withVirtualInterfaceId(id)
            .withBody(new UpdateVirtualInterfaceRequestBody().withVirtualInterface(new UpdateVirtualInterface()
                .withName("{your updated vif name}")
                .withDescription("{your updated vif description}")));
        try {
            UpdateVirtualInterfaceResponse response = client.updateVirtualInterface(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
    }

    private static CreateVirtualInterfaceResponse createVirtualInterface(DcClient client) {
        List<String> remoteEpGroup = new ArrayList<>();
        remoteEpGroup.add("{your remote endpoint group CIDR address}");
        CreateVirtualInterfaceRequest request = new CreateVirtualInterfaceRequest()
            .withBody(new CreateVirtualInterfaceRequestBody()
                .withVirtualInterface(new CreateVirtualInterface()
                    .withName("{your vif name}")
                    .withDescription("{your vif description}")
                    .withDirectConnectId("{your direct connect id}")
                    .withVgwId("{your virtual gateway id}")
                    // Vlan取值范围0-3999，当VLAN值为0时代表当前物理连接端口使用三层路由口模式，三层路由口模式下每条物理连接只能创建一个虚拟接口。用户可按需修改。
                    .withVlan(1893)
                    // 带宽值，多个虚拟接口共享物理连接的带宽，请根据业务流量选择合理的带宽值，带宽上限是物理连接的带宽值。用户可按需修改。
                    .withBandwidth(2)
                    .withLocalGatewayV4Ip("{your local gateway IPv4 address}")
                    .withRemoteGatewayV4Ip("{your remote gateway IPv4 address}")
                    .withType(CreateVirtualInterface.TypeEnum.PRIVATE)
                    .withRouteMode(CreateVirtualInterface.RouteModeEnum.STATIC)
                    .withRemoteEpGroup(remoteEpGroup)));
        CreateVirtualInterfaceResponse response = null;
        try {
            response = client.createVirtualInterface(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.getMessage());
        }
        return response;
    }
}
```

您可以在 [云专线DC服务文档](https://support.huaweicloud.com/dc/index.html) 和 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/DC/doc?api=CreateVirtualInterface) 查看具体信息。

### 返回结果示例
#### 1. 创建虚拟接口
```java
virtualInterface: class VirtualInterface {
    id: 2425c9ac-8ca6-4034-b1c9-34d2e6f5afce
    name: vif-sdktest-created
    adminStateUp: true
    bandwidth: 2
    createTime: 2023-09-26T00:06:54Z
    updateTime: null
    description: test create vif
    directConnectId: c526dfa1-c389-427c-8cb0-790f80220f55
    serviceType: VGW
    status: PENDING_CREATE
    tenantId: f149b9c864584edbb69e5374a81bdae1
    type: private
    vgwId: 1e115445-062e-4e40-b6e2-adc4bd9fed4b
    vlan: 1893
    routeLimit: 50
    enableNqa: false
    enableBfd: false
    lagId: null
    deviceId: 10.94.40.20
    enterpriseProjectId: null
    tags: null
    vifPeers: null
    extendAttribute: null
}
```

#### 2. 查询虚拟接口详情
```java
virtualInterface: class VirtualInterface {
        id: 2425c9ac-8ca6-4034-b1c9-34d2e6f5afce
        name: vif-sdktest-created
        adminStateUp: true
        bandwidth: 2
        createTime: 2023-09-26T08:06:54Z
        updateTime: null
        description: test create vif
        directConnectId: c526dfa1-c389-427c-8cb0-790f80220f55
        serviceType: VGW
        status: PENDING_CREATE
        tenantId: f149b9c864584edbb69e5374a81bdae1
        type: private
        vgwId: 1e115445-062e-4e40-b6e2-adc4bd9fed4b
        vlan: 1893
        routeLimit: 50
        enableNqa: false
        enableBfd: false
        lagId: null
        deviceId: 10.94.40.20
        enterpriseProjectId: 0
        tags: null
        vifPeers: [class VifPeer {
            id: a5a953d8-47f6-478a-ab69-8bdf3ca728f9
            tenantId: f149b9c864584edbb69e5374a81bdae1
            name: vif-sdktest-created
            description: test create vif
            addressFamily: ipv4
            localGatewayIp: 1.1.1.1/30
            remoteGatewayIp: 1.1.1.2/30
            routeMode: static
            bgpAsn: null
            bgpMd5: null
            remoteEpGroup: [1.1.1.0/30, 1.1.2.0/30]
            serviceEpGroup: null
            deviceId: 10.94.40.20
            bgpRouteLimit: 100
            bgpStatus: null
            status: PENDING_CREATE
            vifId: 2425c9ac-8ca6-4034-b1c9-34d2e6f5afce
        }]
        extendAttribute: null
    }
```

#### 3. 更新虚拟接口
```java
virtualInterface: class VirtualInterface {
        id: 2425c9ac-8ca6-4034-b1c9-34d2e6f5afce
        name: vif-sdktest-updated
        adminStateUp: true
        bandwidth: 2
        createTime: 2023-09-26T08:06:54Z
        updateTime: null
        description: test update vif
        directConnectId: c526dfa1-c389-427c-8cb0-790f80220f55
        serviceType: VGW
        status: PENDING_CREATE
        tenantId: f149b9c864584edbb69e5374a81bdae1
        type: private
        vgwId: 1e115445-062e-4e40-b6e2-adc4bd9fed4b
        vlan: 1893
        routeLimit: 50
        enableNqa: false
        enableBfd: false
        lagId: null
        deviceId: 10.94.40.20
        enterpriseProjectId: 0
        tags: null
        vifPeers: [class VifPeer {
            id: a5a953d8-47f6-478a-ab69-8bdf3ca728f9
            tenantId: f149b9c864584edbb69e5374a81bdae1
            name: vif-sdktest-created
            description: test create vif
            addressFamily: ipv4
            localGatewayIp: 1.1.1.1/30
            remoteGatewayIp: 1.1.1.2/30
            routeMode: static
            bgpAsn: null
            bgpMd5: null
            remoteEpGroup: [1.1.1.0/30, 1.1.2.0/30]
            serviceEpGroup: null
            deviceId: 10.94.40.20
            bgpRouteLimit: 100
            bgpStatus: null
            status: PENDING_CREATE
            vifId: 2425c9ac-8ca6-4034-b1c9-34d2e6f5afce
        }]
        extendAttribute: null
    }
```

#### 4. 查询虚拟接口列表
```java
virtualInterfaces: [class VirtualInterface {
        id: 2425c9ac-8ca6-4034-b1c9-34d2e6f5afce
        name: vif-sdktest-updated
        adminStateUp: true
        bandwidth: 2
        createTime: 2023-09-26T08:06:54Z
        updateTime: null
        description: test update vif
        directConnectId: c526dfa1-c389-427c-8cb0-790f80220f55
        serviceType: VGW
        status: PENDING_CREATE
        tenantId: f149b9c864584edbb69e5374a81bdae1
        type: private
        vgwId: 1e115445-062e-4e40-b6e2-adc4bd9fed4b
        vlan: 1893
        routeLimit: 50
        enableNqa: false
        enableBfd: false
        lagId: null
        deviceId: 10.94.40.20
        enterpriseProjectId: 0
        tags: null
        vifPeers: [class VifPeer {
            id: a5a953d8-47f6-478a-ab69-8bdf3ca728f9
            tenantId: f149b9c864584edbb69e5374a81bdae1
            name: vif-sdktest-updated
            description: test update vif
            addressFamily: ipv4
            localGatewayIp: 1.1.1.1/30
            remoteGatewayIp: 1.1.1.2/30
            routeMode: static
            bgpAsn: null
            bgpMd5: null
            remoteEpGroup: [1.1.1.0/30, 1.1.2.0/30]
            serviceEpGroup: null
            deviceId: 10.94.40.20
            bgpRouteLimit: 100
            bgpStatus: null
            status: PENDING_CREATE
            vifId: 2425c9ac-8ca6-4034-b1c9-34d2e6f5afce
        }]
        extendAttribute: null
    }]
```

#### 5. 删除虚拟接口
无返回

### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: | :-----: | :------:  
 2023/09/27 |1.0 | 文档首次发布
