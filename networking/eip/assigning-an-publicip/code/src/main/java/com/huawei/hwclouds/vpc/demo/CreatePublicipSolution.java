package com.huawei.hwclouds.vpc.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.eip.v2.EipClient;
import com.huaweicloud.sdk.eip.v2.model.CreatePublicipBandwidthOption;
import com.huaweicloud.sdk.eip.v2.model.CreatePublicipOption;
import com.huaweicloud.sdk.eip.v2.model.CreatePublicipRequest;
import com.huaweicloud.sdk.eip.v2.model.CreatePublicipRequestBody;
import com.huaweicloud.sdk.eip.v2.model.CreatePublicipResponse;
import com.huaweicloud.sdk.eip.v2.region.EipRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CreatePublicipSolution {

    private static final Logger logger = LoggerFactory.getLogger(CreatePublicipSolution.class);

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份认证为例，运行示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        EipClient client = EipClient.newBuilder()
            .withCredential(auth)
            .withRegion(EipRegion.valueOf("{REGION_ID}"))
            .build();
        CreatePublicipRequest request = new CreatePublicipRequest();
        CreatePublicipRequestBody body = new CreatePublicipRequestBody();
        CreatePublicipOption publicipbody = new CreatePublicipOption();
        publicipbody.withType("{EIP TYPE}");
        CreatePublicipBandwidthOption bandwidthbody = new CreatePublicipBandwidthOption();
        bandwidthbody.withName("{BANDWIDTH_NAME}")
            .withShareType(CreatePublicipBandwidthOption.ShareTypeEnum.fromValue("PER"))
            .withSize({BANDWIDTH_SIZE});
        body.withPublicip(publicipbody);
        body.withBandwidth(bandwidthbody);
        request.withBody(body);
        try {
            CreatePublicipResponse response = client.createPublicip(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            logger.error("assigning an publicip ConnectionException is: {}", e.getMessage());
        } catch (RequestTimeoutException e) {
            logger.error("assigning an publicip ClientRequestException is: {}", e.getMessage());
        } catch (ServiceResponseException e) {
            logger.error("assigning an publicip ServerResponseException is: {}", e.getMessage());
        }
    }
}