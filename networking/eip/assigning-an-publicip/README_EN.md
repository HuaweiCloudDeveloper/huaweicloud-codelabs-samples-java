## 1. Function Introduction
HUAWEI CLOUD provides the EIP server SDK. You can directly integrate the server SDK to call EIP APIs, implementing quick operations on EIPs. This example shows how to use the Java SDK to create an EIP. For details about EIPs see [EIP](https://support.huaweicloud.com/intl/zh-cn/productdesc-eip/overview_0001.html) Overview.

## 2. Prerequisites
- You have [registered](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=94fc0f9f861b4f30a85ec1f463d35609&lang=en-us) with Huawei Cloud and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth).
- You have obtained the HUAWEI CLOUD SDK. You can also view and install the Java SDK.
- You have obtained the Access Key (AK) and Secret Access Key (SK) of the HUAWEI CLOUD account. On the HUAWEI CLOUD console, choose My Credential > Access Keys to create and view your AK/SK. For details, see [the access key](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0001.html).
- The development environment is available and Java JDK 1.8 or later is supported.

## 3. Obtain and install the SDK.
For details about the SDK version, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) (Product Category: Elastic IP).

## 4. Key code snippets
Run the following code to create an EIP. Before invoking the code, replace the following variables with {YOUR AK},{YOUR SK},{REGION_ID},{EIP TYPE},{BANDWIDTH_NAME},{BANDWIDTH_SIZE} as required.

```java
package com.huaweicloud.sdk.test;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.eip.v2.region.EipRegion;
import com.huaweicloud.sdk.eip.v2.*;
import com.huaweicloud.sdk.eip.v2.model.*;


public class CreatePublicipSolution {

  public static void main(String[] args) {
    // Hardcoding the AK/SK used for authentication in the code or storing them in plaintext poses high security risks. It is recommended that the AK/SK be stored in ciphertext in the configuration file or environment variables and be decrypted when being used to ensure security.
    // In this example, the AK and SK are stored in environment variables for identity authentication. Before running the example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");

    ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

    EipClient client = EipClient.newBuilder()
            .withCredential(auth)
            .withRegion(EipRegion.valueOf("{REGION_ID}"))
            .build();
    CreatePublicipRequest request = new CreatePublicipRequest();
    CreatePublicipRequestBody body = new CreatePublicipRequestBody();
    CreatePublicipOption publicipbody = new CreatePublicipOption();
    publicipbody.withType("{EIP TYPE}");
    CreatePublicipBandwidthOption bandwidthbody = new CreatePublicipBandwidthOption();
    bandwidthbody.withName("{BANDWIDTH_NAME}")
            .withShareType(CreatePublicipBandwidthOption.ShareTypeEnum.fromValue("PER"))
            .withSize({BANDWIDTH_SIZE});
    body.withPublicip(publicipbody);
    body.withBandwidth(bandwidthbody);
    request.withBody(body);
    try {
      CreatePublicipResponse response = client.createPublicip(request);
      System.out.println(response.toString());
    } catch (ConnectionException e) {
      e.printStackTrace();
    } catch (RequestTimeoutException e) {
      e.printStackTrace();
    } catch (ServiceResponseException e) {
      e.printStackTrace();
      System.out.println(e.getHttpStatusCode());
      System.out.println(e.getErrorCode());
      System.out.println(e.getErrorMsg());
    }
  }
}
```
You can find it at [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/EIP/doc?version=v2&api=CreatePublicip), run and debug the API.

## 5. Running Result
Example of a successful response

```json
{
  "publicip" : {
    "tenant_id" : "8b7e35ad379141fc9df3e178bd64f55c",
    "bandwidth_size" : 0,
    "public_ip_address" : "161.xx.xx.7",
    "ip_version" : 4,
    "create_time" : "2015-07-16 04:10:52",
    "id" : "f588ccfa-8750-4d7c-bf5d-2ede24414706",
    "type" : "5_bgp",
    "status" : "PENDING_CREATE"
  }
}
```

## 6. Reference
- EIP Product Overview
  - [Elastic public IP address](https://support.huaweicloud.com/intl/zh-cn/productdesc-eip/eip_pro_0001.html)
- API Reference
  - EIP Help Document [Applying for an EIP (Pay-per-use)](https://support.huaweicloud.com/intl/zh-cn/api-eip/eip_api_0001.html)
  - EIP Help Document [Querying an EIP](https://support.huaweicloud.com/api-eip/eip_api_0002.html)

## 7. Change History
| Release Date | Document Version | Revision Description |
|------------|------ |-------- |
| 2023 - 08 - 10 | 1.0 | Document First Release |