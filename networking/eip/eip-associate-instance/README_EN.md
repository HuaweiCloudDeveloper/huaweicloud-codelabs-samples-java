## 1. Function Introduction
HUAWEI CLOUD provides the EIP server SDK. You can directly integrate the server SDK to call EIP APIs, implementing quick operations on EIPs. This example shows how to bind an EIP using the Java SDK. For details about EIPs see [EIP](https://support.huaweicloud.com/intl/zh-cn/productdesc-eip/overview_0001.html) Overview.

## 2. Prerequisites
- You have [registered](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=94fc0f9f861b4f30a85ec1f463d35609&lang=en-us) with Huawei Cloud and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth).
- You have obtained the HUAWEI CLOUD SDK. You can also view and install the Java SDK.
- You have obtained the Access Key (AK) and Secret Access Key (SK) of the HUAWEI CLOUD account. On the HUAWEI CLOUD console, choose My Credential > Access Keys to create and view your AK/SK. For details, see [the access key](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0001.html).
- The development environment is available and Java JDK 1.8 or later is supported.
- You have purchased an EIP and the instance to be bound. The instance can be an ECS, NAT gateway, VPN, or ELB load balancer.

## 3. Obtain and install the SDK.
For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) (Product Type: Elastic IP).

## 4. Key code snippets
Use the following code to bind an EIP. Before invoking the API, replace the variables {YOUR AK}, {YOUR SK}, {REGION_ID}, {PUBLICIP ID}, {INSTANCE ID}, {INSTANCE TYPE} based on the site requirements.

```java
package com.huawei;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.eip.v3.EipClient;
import com.huaweicloud.sdk.eip.v3.model.AssociatePublicipsOption;
import com.huaweicloud.sdk.eip.v3.model.AssociatePublicipsRequest;
import com.huaweicloud.sdk.eip.v3.model.AssociatePublicipsRequestBody;
import com.huaweicloud.sdk.eip.v3.model.AssociatePublicipsResponse;
import com.huaweicloud.sdk.eip.v3.region.EipRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @description Before invoking this interface, replace the following variables based on the site requirements:
 * {YOUR AK}, {YOUR SK}, {REGION_ID}, {PUBLICIP ID}, {INSTANCE ID}
 * The instance type can be PORT, NATGW, VPN, or ELB. The enumerated type is AssociateInstanceTypeEnum.
 */
public class AssociateInstanceDemo {

    private static final Logger logger = LoggerFactory.getLogger(AssociateInstanceDemo.class);

    public static void main(String[] args) {
        // Enter the AK and SK of the HUAWEI CLOUD account.
        String ak = "{YOUR AK}";
        String sk = "{YOUR SK}";

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        // For EipClient of the 3 interface, set REGION_ID to the application region ID, for example, cn-north-4 for Beijing.
        EipClient client = EipClient.newBuilder()
                .withCredential(auth)
                .withRegion(EipRegion.valueOf("{REGION_ID}"))
                .build();

        try {
            // Before binding an ECS, you need to purchase the corresponding instance, which can be an ECS, NAT gateway, VPN, or Elastic Load Balance ELB.
            AssociatePublicipsResponse response = client.associatePublicips(getRequest());
            // In the command output, associate_instance_id and associate_instance_type indicate the ID and type of the bound instance.
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            logger.info(e.toString());
        } catch (RequestTimeoutException e) {
            logger.info(e.toString());
        } catch (ServiceResponseException e) {
            logger.info(e.toString());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    public static AssociatePublicipsRequest getRequest() {
        AssociatePublicipsRequest request = new AssociatePublicipsRequest();
        // Specifies the ID of the purchased EIP. For details about how to purchase an EIP, see Applying for an EIP (Pay-per-use) and Applying for an EIP (Yearly/Monthly) in the EIP help document.
        request.withPublicipId("{PUBLICIP ID}");
        AssociatePublicipsRequestBody requestBody = new AssociatePublicipsRequestBody();
        // Construct a request body. In the request body, the instance ID is the instance ID, and the instance type is the instance type.
        AssociatePublicipsOption publicipBody = new AssociatePublicipsOption();
        publicipBody.withAssociateInstanceId("{INSTANCE ID}");
        publicipBody.withAssociateInstanceType("{INSTANCE TYPE}");

        requestBody.withPublicip(publicipBody);
        request.withBody(requestBody);

        return request;
    }
}
```
You can find it at [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/EIP/doc?version=v3&api=AssociatePublicips), run and debug the API.

## 5. Running Result
Example of a successful response

```json
{
  "request_id": "7a8fd564940586b4b06c2534953be9a3",
  "publicip": {
    "created_at": "2023-07-29T07:19:54Z",
    "updated_at": "2023-07-29T07:35:55Z",
    "lock_status": null,
    "id": "94cfaacd-2471-417f-bbba-fa15213a3567",
    "alias": null,
    "project_id": "88060fecd26f41019cb32016bc3d2d94",
    "ip_version": 4,
    "public_ip_address": "100.95.156.127",
    "public_ipv6_address": null,
    "status": "ACTIVE",
    "description": "",
    "enterprise_project_id": "0",
    "billing_info": null,
    "type": "EIP",
    "vnic": {
      "private_ip_address": "192.168.0.103",
      "device_id": "fcee1baa-bc1b-4e60-95e2-5ee3922db22d",
      "device_owner": "compute:cn-north-7a",
      "vpc_id": "2a5f697f-180a-4435-9573-81160feee814",
      "port_id": "b820583f-ebf3-49f2-957e-e53bbb8ad2d1",
      "mac": "fa:16:3e:bc:14:7b",
      "vtep": "10.63.76.16",
      "vni": "430546",
      "instance_id": "",
      "instance_type": "",
      "port_profile": ""
    },
    "bandwidth": {
      "id": "43fd06dd-3da8-41b9-bed5-ff7f430f80f3",
      "size": 1,
      "share_type": "PER",
      "charge_mode": "traffic",
      "name": "ecs-c197-bandwidth-3a92",
      "billing_info": ""
    },
    "associate_instance_type": "PORT",
    "associate_instance_id": "b820583f-ebf3-49f2-957e-e53bbb8ad2d1",
    "publicip_pool_id": "8a425166-82ac-4163-94d5-e6913579e7e7",
    "publicip_pool_name": "5_g-vm",
    "public_border_group": "center"
  }
}
```

## 6. Reference
- EIP Product Introduction
  - [Elastic public IP address](https://support.huaweicloud.com/intl/zh-cn/productdesc-eip/eip_pro_0001.html)
- API Reference
  - EIP Help Document [Applying for an EIP (Yearly/Monthly Package)](https://support.huaweicloud.com/intl/zh-cn/api-eip/eip_api_0006.html)
  - EIP Help Document [Applying for an EIP (Pay-per-use)](https://support.huaweicloud.com/intl/zh-cn/api-eip/eip_api_0001.html)
  - EIP Help Document [Binding an EIP](https://support.huaweicloud.com/intl/zh-cn/api-eip/AssociatePublicips.html)

## 7. Change History
| Release Date | Document Version | Revision Description |
|------------|------ |-------- |
| 2023 - 08 - 10 | 1.0 | Document First Release |