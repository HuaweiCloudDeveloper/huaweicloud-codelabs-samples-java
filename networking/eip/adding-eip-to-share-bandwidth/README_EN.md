## 1. Function Introduction
HUAWEI CLOUD provides an EIP SDK. You can integrate the SDK to call EIP APIs to quickly perform operations on EIPs. This section describes how to add an EIP to a shared bandwidth using the Java SDK. For details about EIPs see [EIP](https://support.huaweicloud.com/intl/zh-cn/productdesc-eip/overview_0001.html) Overview.

## 2. Prerequisites
- You have [registered](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=94fc0f9f861b4f30a85ec1f463d35609&lang=en-us) with Huawei Cloud and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth).
- You have obtained the HUAWEI CLOUD SDK. You can also view and install the Java SDK.
- You have obtained the Access Key (AK) and Secret Access Key (SK) of the HUAWEI CLOUD account. On the HUAWEI CLOUD console, choose My Credential > Access Keys to create and view your AK/SK. For details, see [the access key](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0001.html).
- The development environment is available and Java JDK 1.8 or later is supported.

## 3. Obtain and install the SDK.
For details about the SDK version, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) (Product Category: Elastic IP).

## 4. Key code snippets
Use the following code to share the bandwidth to insert the EIP. Before invoking the interface, replace the {YOUR AK},{YOUR SK},{REGION_ID},{EIP TYPE},{BANDWIDTH_NAME},{BANDWIDTH_SIZE} variable with the actual variable.

```java
package com.huaweicloud.sdk.test;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.eip.v2.region.EipRegion;
import com.huaweicloud.sdk.eip.v2.*;
import com.huaweicloud.sdk.eip.v2.model.*;

import java.util.List;
import java.util.ArrayList;

public class AddPublicipsIntoSharedBandwidthSolution {

  public static void main(String[] args) {
    // If the AK and SK used for authentication are hardcoded or stored in plaintext, there are great security risks. It is recommended that the AK and SK be stored in ciphertext in the configuration file or environment variables and be decrypted when being used to ensure security.
    // In this example, the AK and SK are stored in environment variables for identity authentication. Before running the example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");

    ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

    EipClient client = EipClient.newBuilder()
            .withCredential(auth)
            .withRegion(EipRegion.valueOf("{REGION_ID}"))
            .build();
    AddPublicipsIntoSharedBandwidthRequest request = new AddPublicipsIntoSharedBandwidthRequest();
    request.withBandwidthId("{BANDWIDTH_ID}");
    AddPublicipsIntoSharedBandwidthRequestBody body = new AddPublicipsIntoSharedBandwidthRequestBody();
    List<InsertPublicipInfo> listBandwidthPublicipInfo = new ArrayList<>();
    listBandwidthPublicipInfo.add(
            new InsertPublicipInfo()
                    .withPublicipId("{PUBLICIP_ID}")
    );
    AddPublicipsIntoSharedBandwidthOption bandwidthbody = new AddPublicipsIntoSharedBandwidthOption();
    bandwidthbody.withPublicipInfo(listBandwidthPublicipInfo);
    body.withBandwidth(bandwidthbody);
    request.withBody(body);
    try {
      AddPublicipsIntoSharedBandwidthResponse response = client.addPublicipsIntoSharedBandwidth(request);
      System.out.println(response.toString());
    } catch (ConnectionException e) {
      e.printStackTrace();
    } catch (RequestTimeoutException e) {
      e.printStackTrace();
    } catch (ServiceResponseException e) {
      e.printStackTrace();
      System.out.println(e.getHttpStatusCode());
      System.out.println(e.getErrorCode());
      System.out.println(e.getErrorMsg());
    }
  }
}
```
You can find it at [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/EIP/doc?version=v2&api=AddPublicipsIntoSharedBandwidth), run and debug the API.

## 5. Running Result
Example of a successful response

```json
{
  "bandwidth" : {
    "tenant_id" : "8b7e35ad379141fc9df3e178bd64f55c",
    "billing_info" : "CS1712121146TSQOJ:0616e2a5dc9f4985ba52ea8c0c7e273c:southchina:35f2b308f5d64441a6fa7999fbcd4321",
    "size" : 10,
    "share_type" : "WHOLE",
    "bandwidth_type" : "share",
    "publicip_info" : [ {
      "publicip_id" : "d91b0028-6f6b-4478-808a-297b75b6812a",
      "ip_version" : 4,
      "publicip_type" : "5_dualStack",
      "publicip_address" : "::ffff:192.168.89.9"
    }, {
      "publicip_id" : "1d184b2c-4ec9-49b5-a3f9-27600a76ba3f",
      "ip_version" : 4,
      "publicip_type" : "5_bgp",
      "publicip_address" : "99.xx.xx.82"
    } ],
    "name" : "bandwidth123",
    "enable_bandwidth_rules" : false,
    "rule_quota" : 0,
    "bandwidth_rules" : [ ],
    "charge_mode" : "bandwidth",
    "id" : "3fa5b383-5a73-4dcb-a314-c6128546d855"
  }
}
```

## 6. Reference
- EIP Product Overview
  - [Elastic public IP address](https://support.huaweicloud.com/intl/zh-cn/productdesc-eip/eip_pro_0001.html)
- API Reference
  - EIP Help Document [Inserting an EIP to a Shared Bandwidth](https://support.huaweicloud.com/intl/zh-cn/api-eip/eip_apisharedbandwidth_0004.html)
  - EIP Help Document [Querying an EIP](https://support.huaweicloud.com/api-eip/eip_api_0002.html)

## 7. Change History
| Release Date | Document Version | Revision Description |
|------------|------ |-------- |
| 2023 - 08 - 10 | 1.0 | Document First Release |