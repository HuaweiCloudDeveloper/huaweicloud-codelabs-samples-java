## 1、功能介绍
华为云提供了弹性公网IP服务端SDK，您可以直接集成服务端SDK来调用弹性公网IP的相关API，从而实现对弹性公网IP的快速操作。本示例展示如何通过java版本的SDK方式将弹性公网ip插入共享带宽。什么是弹性公网IP请参见[弹性公网IP](https://support.huaweicloud.com/productdesc-eip/overview_0001.html)简介。

## 2、前置条件
- 已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，
  并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)
- 已获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。

## 3、SDK获取和安装
具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com/?language=Java) (产品类别：弹性公网IP)

## 4、关键代码片段
使用如下代码共享带宽插入eip，调用前请根据实际情况替换如下变量：{YOUR AK},{YOUR SK},{REGION_ID},{EIP TYPE},{BANDWIDTH_NAME},{BANDWIDTH_SIZE}

```java
package com.huaweicloud.sdk.test;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.eip.v2.region.EipRegion;
import com.huaweicloud.sdk.eip.v2.*;
import com.huaweicloud.sdk.eip.v2.model.*;

import java.util.List;
import java.util.ArrayList;

public class AddPublicipsIntoSharedBandwidthSolution {

  public static void main(String[] args) {
    // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份认证为例，运行示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");

    ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

    EipClient client = EipClient.newBuilder()
            .withCredential(auth)
            .withRegion(EipRegion.valueOf("{REGION_ID}"))
            .build();
    AddPublicipsIntoSharedBandwidthRequest request = new AddPublicipsIntoSharedBandwidthRequest();
    request.withBandwidthId("{BANDWIDTH_ID}");
    AddPublicipsIntoSharedBandwidthRequestBody body = new AddPublicipsIntoSharedBandwidthRequestBody();
    List<InsertPublicipInfo> listBandwidthPublicipInfo = new ArrayList<>();
    listBandwidthPublicipInfo.add(
            new InsertPublicipInfo()
                    .withPublicipId("{PUBLICIP_ID}")
    );
    AddPublicipsIntoSharedBandwidthOption bandwidthbody = new AddPublicipsIntoSharedBandwidthOption();
    bandwidthbody.withPublicipInfo(listBandwidthPublicipInfo);
    body.withBandwidth(bandwidthbody);
    request.withBody(body);
    try {
      AddPublicipsIntoSharedBandwidthResponse response = client.addPublicipsIntoSharedBandwidth(request);
      System.out.println(response.toString());
    } catch (ConnectionException e) {
      e.printStackTrace();
    } catch (RequestTimeoutException e) {
      e.printStackTrace();
    } catch (ServiceResponseException e) {
      e.printStackTrace();
      System.out.println(e.getHttpStatusCode());
      System.out.println(e.getErrorCode());
      System.out.println(e.getErrorMsg());
    }
  }
}
```
您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/EIP/sdk?version=v2&api=AddPublicipsIntoSharedBandwidth) 中直接运行调试该接口。

## 5、运行结果
响应成功示例

```json
{
  "bandwidth" : {
    "tenant_id" : "8b7e35ad379141fc9df3e178bd64f55c",
    "billing_info" : "CS1712121146TSQOJ:0616e2a5dc9f4985ba52ea8c0c7e273c:southchina:35f2b308f5d64441a6fa7999fbcd4321",
    "size" : 10,
    "share_type" : "WHOLE",
    "bandwidth_type" : "share",
    "publicip_info" : [ {
      "publicip_id" : "d91b0028-6f6b-4478-808a-297b75b6812a",
      "ip_version" : 4,
      "publicip_type" : "5_dualStack",
      "publicip_address" : "::ffff:192.168.89.9"
    }, {
      "publicip_id" : "1d184b2c-4ec9-49b5-a3f9-27600a76ba3f",
      "ip_version" : 4,
      "publicip_type" : "5_bgp",
      "publicip_address" : "99.xx.xx.82"
    } ],
    "name" : "bandwidth123",
    "enable_bandwidth_rules" : false,
    "rule_quota" : 0,
    "bandwidth_rules" : [ ],
    "charge_mode" : "bandwidth",
    "id" : "3fa5b383-5a73-4dcb-a314-c6128546d855"
  }
}
```

## 6、参考
- EIP产品介绍
  - [图解弹性公网IP](https://support.huaweicloud.com/productdesc-eip/eip_pro_0001.html)
- API参考
  - EIP帮助文档 [共享带宽插入EIP](https://support.huaweicloud.com/api-eip/eip_apisharedbandwidth_0004.html)
  - EIP帮助文档 [查询EIP](https://support.huaweicloud.com/api-eip/eip_api_0002.html)

## 7、修订记录
| 发布日期       | 文档版本 | 修订说明 |
|------------|------   |--------   |
| 2023-08-10 | 1.0  | 文档首次发布 |