## 1. Function Introduction
HUAWEI CLOUD provides an EIP SDK. You can integrate the SDK to call EIP APIs to quickly perform operations on EIPs. This example shows how to remove an EIP from a shared bandwidth using the Java SDK. For details about EIPs see [EIP](https://support.huaweicloud.com/intl/zh-cn/productdesc-eip/overview_0001.html) Overview.

## 2. Prerequisites
- You have [registered](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=94fc0f9f861b4f30a85ec1f463d35609&lang=en-us) with Huawei Cloud and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth).
- You have obtained the HUAWEI CLOUD SDK. You can also view and install the Java SDK.
- You have obtained the Access Key (AK) and Secret Access Key (SK) of the HUAWEI CLOUD account. On the HUAWEI CLOUD console, choose My Credential > Access Keys to create and view your AK/SK. For details, see [the access key](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0001.html).
- The development environment is available and Java JDK 1.8 or later is supported.

## 3. Obtain and install the SDK.
For details about the SDK version, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) (Product Category: Elastic IP).

## 4. Key code snippets
Use the following code to create a yearly/monthly EIP. Before invoking the code, replace the following variables with {YOUR AK},{YOUR SK},{REGION_ID},{EIP TYPE},{BANDWIDTH_NAME},{BANDWIDTH_SIZE} as required.

```java
package com.huawei.hwclouds.vpc.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.eip.v2.EipClient;
import com.huaweicloud.sdk.eip.v2.model.RemoveFromSharedBandwidthOption;
import com.huaweicloud.sdk.eip.v2.model.RemovePublicipInfo;
import com.huaweicloud.sdk.eip.v2.model.RemovePublicipsFromSharedBandwidthRequest;
import com.huaweicloud.sdk.eip.v2.model.RemovePublicipsFromSharedBandwidthRequestBody;
import com.huaweicloud.sdk.eip.v2.model.RemovePublicipsFromSharedBandwidthResponse;
import com.huaweicloud.sdk.eip.v2.region.EipRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.ArrayList;

public class RemovePublicipsFromSharedBandwidthSolution {

  private static final Logger logger = LoggerFactory.getLogger(RemovePublicipsFromSharedBandwidthSolution.class);

  public static void main(String[] args) {
    // Hardcoding the AK/SK used for authentication in the code or storing them in plaintext poses high security risks. It is recommended that the AK/SK be stored in ciphertext in the configuration file or environment variables and be decrypted when being used to ensure security.
    // In this example, the AK and SK are stored in environment variables for identity authentication. Before running the example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");

    ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

    EipClient client = EipClient.newBuilder()
            .withCredential(auth)
            .withRegion(EipRegion.valueOf("{REGION_ID}"))
            .build();
    RemovePublicipsFromSharedBandwidthRequest request = new RemovePublicipsFromSharedBandwidthRequest();
    request.withBandwidthId("{BANDWIDTH_ID}");
    RemovePublicipsFromSharedBandwidthRequestBody body = new RemovePublicipsFromSharedBandwidthRequestBody();
    List<RemovePublicipInfo> listBandwidthPublicipInfo = new ArrayList<>();
    listBandwidthPublicipInfo.add(
            new RemovePublicipInfo()
                    .withPublicipId("{PUBLICIP_ID}")
    );
    RemoveFromSharedBandwidthOption bandwidthbody = new RemoveFromSharedBandwidthOption();
    bandwidthbody.withChargeMode(RemoveFromSharedBandwidthOption.ChargeModeEnum.fromValue("{CHARGE_MODE}"))
            .withPublicipInfo(listBandwidthPublicipInfo)
            .withSize({BANDWIDTH_SIZE});
    body.withBandwidth(bandwidthbody);
    request.withBody(body);
    try {
      RemovePublicipsFromSharedBandwidthResponse response = client.removePublicipsFromSharedBandwidth(request);
      System.out.println(response.toString());
    } catch (ConnectionException e) {
      logger.error("removing eip from share bandwidth ConnectionException is: {}", e.getMessage());
    } catch (RequestTimeoutException e) {
      logger.error("removing eip from share bandwidth ConnectionException is: {}", e.getMessage());
    } catch (ServiceResponseException e) {
      logger.error("removing eip from share bandwidth ConnectionException is: {}", e.getMessage());
    }
  }
}
```
You can find it at [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/EIP/doc?version=v2&api=RemovePublicipsFromSharedBandwidth), run and debug the API.

## 5. Running Result
Example of a successful response

```json
{
  "order_id": "CS2307271951EQHOC",
  "publicip_id": "4d942e7a-14fe-4c24-98a8-a5a18985d006"
}
```

## 6. Reference
- EIP Product Overview
  - [Elastic public IP address](https://support.huaweicloud.com/intl/zh-cn/productdesc-eip/eip_pro_0001.html)
- API Reference
  - EIP Help Document [Applying for an EIP (Yearly/Monthly Package)](https://support.huaweicloud.com/intl/zh-cn/api-eip/eip_api_0006.html)
  - EIP Help Document [Querying an EIP](https://support.huaweicloud.com/intl/zh-cn/api-eip/eip_api_0002.html)
  - EIP Help Document [Removing an EIP from a Shared Bandwidth](https://support.huaweicloud.com/intl/zh-cn/api-eip/eip_apisharedbandwidth_0005.html)

## 7. Change History
| Release Date | Document Version | Revision Description |
|------------|------ |-------- |
| 2023 - 08 - 10 | 1.0 | Document First Release |