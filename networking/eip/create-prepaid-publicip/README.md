## 1、功能介绍
华为云提供了弹性公网IP服务端SDK，您可以直接集成服务端SDK来调用弹性公网IP的相关API，从而实现对弹性公网IP的快速操作。该示例展示了如何通过Java版本SDK创建包周期弹性公网IP。什么是弹性公网IP请参见[弹性公网IP](https://support.huaweicloud.com/productdesc-eip/overview_0001.html)简介。

## 2、前置条件
- 已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，
  并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)
- 已获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。

## 3、SDK获取和安装
具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com/?language=Java) (产品类别：弹性公网IP)

## 4、关键代码片段
使用如下代码创建包年包月的EIP，调用前请根据实际情况替换如下变量：{YOUR AK},{YOUR SK},{REGION_ID},{EIP TYPE},{BANDWIDTH_NAME},{BANDWIDTH_SIZE}

```java
package com.huaweicloud.sdk.test;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.eip.v2.EipClient;
import com.huaweicloud.sdk.eip.v2.model.CreatePrePaidPublicipExtendParamOption;
import com.huaweicloud.sdk.eip.v2.model.CreatePrePaidPublicipOption;
import com.huaweicloud.sdk.eip.v2.model.CreatePrePaidPublicipRequest;
import com.huaweicloud.sdk.eip.v2.model.CreatePrePaidPublicipRequestBody;
import com.huaweicloud.sdk.eip.v2.model.CreatePrePaidPublicipResponse;
import com.huaweicloud.sdk.eip.v2.model.CreatePublicipBandwidthOption;
import com.huaweicloud.sdk.eip.v2.model.ShowPublicipRequest;
import com.huaweicloud.sdk.eip.v2.model.ShowPublicipResponse;
import com.huaweicloud.sdk.eip.v2.region.EipRegion;

public class CreatePrePaidPublicipDemo {

    public static void main(String[] args) {
        // 输入华为云账号的AK、SK
        String ak = "{YOUR AK}";
        String sk = "{YOUR SK}";

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        // v2接口的EipClient，REGION_ID处填写应用区域的id，如北京四填写cn-north-4
        EipClient client = EipClient.newBuilder()
                .withCredential(auth)
                .withRegion(EipRegion.valueOf("{REGION_ID}"))
                .build();
        CreatePrePaidPublicipRequest request = new CreatePrePaidPublicipRequest();
        CreatePrePaidPublicipRequestBody body = new CreatePrePaidPublicipRequestBody();
        CreatePrePaidPublicipOption publicipBody = new CreatePrePaidPublicipOption();
        // 创建的EIP类型，有5_bgp（全动态BGP），5_sbgp（静态BGP），5_youxuanbgp（优选BGP）这几种类型
        publicipBody.withType("{EIP TYPE}");
        CreatePublicipBandwidthOption bandwidthBody = new CreatePublicipBandwidthOption();
        // BANDWIDTH_NAME为宽带的名称，宽带的大小通过BANDWIDTH_SIZE参数传入
        bandwidthBody.withName("{BANDWIDTH_NAME}")
                .withShareType(CreatePublicipBandwidthOption.ShareTypeEnum.fromValue("PER"))
                .withSize("{BANDWIDTH_SIZE}");
        CreatePrePaidPublicipExtendParamOption extendParamBody = new CreatePrePaidPublicipExtendParamOption();
        // 构造扩展参数，下面5个参数含义为：预付费、周期单位为月、订购周期数为1、不自动续费、自动支付
        extendParamBody.withChargeMode(CreatePrePaidPublicipExtendParamOption.ChargeModeEnum.PREPAID)
                .withPeriodType(CreatePrePaidPublicipExtendParamOption.PeriodTypeEnum.MONTH)
                .withPeriodNum(1)
                .withIsAutoRenew(Boolean.FALSE)
                .withIsAutoPay(Boolean.TRUE);
        body.withPublicip(publicipBody);
        body.withBandwidth(bandwidthBody);
        body.withExtendParam(extendParamBody);
        request.withBody(body);
        try {
            CreatePrePaidPublicipResponse response = client.createPrePaidPublicip(request);
            // 构造查询请求类
            ShowPublicipRequest showRequest = new ShowPublicipRequest().withPublicipId(response.getPublicipId());
            // 查询EIP详情，可以查看EIP是否创建成功
            ShowPublicipResponse publicipInfo = client.showPublicip(showRequest);
            System.out.println(response.toString());
            System.out.println(publicipInfo.toString());
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}
```
您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/EIP/sdk?version=v2&api=CreatePrePaidPublicip) 中直接运行调试该接口。

## 5、运行结果
响应成功示例

```json
{
  "order_id": "CS2307271951EQHOC",
  "publicip_id": "4d942e7a-14fe-4c24-98a8-a5a18985d006"
}
```

## 6、参考
- EIP产品介绍
  - [图解弹性公网IP](https://support.huaweicloud.com/productdesc-eip/eip_pro_0001.html)
- API参考
  - EIP帮助文档 [申请EIP(包年/包月)](https://support.huaweicloud.com/api-eip/eip_api_0006.html)
  - EIP帮助文档 [查询EIP](https://support.huaweicloud.com/api-eip/eip_api_0002.html)

## 7、修订记录
| 发布日期       | 文档版本 | 修订说明 |
|------------|------   |--------   |
| 2023-08-10 | 1.0  | 文档首次发布 |