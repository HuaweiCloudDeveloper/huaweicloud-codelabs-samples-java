package com.huawei.hwclouds.vpc.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.eip.v2.EipClient;
import com.huaweicloud.sdk.eip.v2.model.CreatePrePaidPublicipExtendParamOption;
import com.huaweicloud.sdk.eip.v2.model.CreatePrePaidPublicipOption;
import com.huaweicloud.sdk.eip.v2.model.CreatePrePaidPublicipRequest;
import com.huaweicloud.sdk.eip.v2.model.CreatePrePaidPublicipRequestBody;
import com.huaweicloud.sdk.eip.v2.model.CreatePrePaidPublicipResponse;
import com.huaweicloud.sdk.eip.v2.model.CreatePublicipBandwidthOption;
import com.huaweicloud.sdk.eip.v2.region.EipRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @description 调用前请根据实际情况替换如下变量：
 * {YOUR AK},{YOUR SK},{REGION_ID},{EIP TYPE},{BANDWIDTH_NAME},{BANDWIDTH_SIZE}
 */
public class CreatePrePaidPublicipDemo {

    private static final Logger logger = LoggerFactory.getLogger(CreatePrePaidPublicipDemo.class);

    public static void main(String[] args) {
        String ak = "{YOUR AK}";
        String sk = "{YOUR SK}";

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        EipClient client = EipClient.newBuilder()
            .withCredential(auth)
            .withRegion(EipRegion.valueOf("{REGION_ID}"))
            .build();

        try {
            CreatePrePaidPublicipResponse response = client.createPrePaidPublicip(getRequest());
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            logger.error("create prepaid publicip ConnectionException is: {}", e.getMessage());
        } catch (RequestTimeoutException e) {
            logger.error("create prepaid publicip ClientRequestException is: {}", e.getMessage());
        } catch (ServiceResponseException e) {
            logger.error("create prepaid publicip ServerResponseException is: {}", e.getMessage());
        }
    }

    public static CreatePrePaidPublicipRequest getRequest() {
        CreatePrePaidPublicipRequest request = new CreatePrePaidPublicipRequest();
        CreatePrePaidPublicipRequestBody body = new CreatePrePaidPublicipRequestBody();
        CreatePrePaidPublicipOption publicipBody = new CreatePrePaidPublicipOption();
        publicipBody.withType("{EIP TYPE}");
        CreatePublicipBandwidthOption bandwidthBody = new CreatePublicipBandwidthOption();
        bandwidthBody.withName("{BANDWIDTH_NAME}")
            .withShareType(CreatePublicipBandwidthOption.ShareTypeEnum.fromValue("PER"))
            .withSize(Integer.valueOf("{BANDWIDTH_SIZE}"));
        CreatePrePaidPublicipExtendParamOption extendParamBody = new CreatePrePaidPublicipExtendParamOption();
        extendParamBody.withChargeMode(CreatePrePaidPublicipExtendParamOption.ChargeModeEnum.PREPAID)
            .withPeriodType(CreatePrePaidPublicipExtendParamOption.PeriodTypeEnum.MONTH)
            .withPeriodNum(1)
            .withIsAutoRenew(Boolean.FALSE)
            .withIsAutoPay(Boolean.TRUE);
        body.withPublicip(publicipBody);
        body.withBandwidth(bandwidthBody);
        body.withExtendParam(extendParamBody);
        request.withBody(body);

        return request;
    }
}