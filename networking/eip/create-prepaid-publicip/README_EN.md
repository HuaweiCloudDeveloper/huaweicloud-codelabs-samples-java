## 1. Function Introduction
HUAWEI CLOUD provides the EIP server SDK. You can directly integrate the server SDK to call EIP APIs, implementing quick operations on EIPs. This example shows how to use the Java SDK to create a yearly/monthly EIP. For details about EIPs see [EIP](https://support.huaweicloud.com/intl/zh-cn/productdesc-eip/overview_0001.html) Overview.

## 2. Prerequisites
- You have [registered](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=94fc0f9f861b4f30a85ec1f463d35609&lang=en-us) with Huawei Cloud and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth).
- You have obtained the HUAWEI CLOUD SDK. You can also view and install the Java SDK.
- You have obtained the Access Key (AK) and Secret Access Key (SK) of the HUAWEI CLOUD account. On the HUAWEI CLOUD console, choose My Credential > Access Keys to create and view your AK/SK. For details, see [the access key](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0001.html).
- The development environment is available and Java JDK 1.8 or later is supported.

## 3. Obtain and install the SDK.
For details about the SDK version, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) (Product Category: Elastic IP).

## 4. Key code snippets
Use the following code to create a yearly/monthly EIP. Before invoking the code, replace the following variables with {YOUR AK},{YOUR SK},{REGION_ID},{EIP TYPE},{BANDWIDTH_NAME},{BANDWIDTH_SIZE} as required.

```java
package com.huaweicloud.sdk.test;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.eip.v2.EipClient;
import com.huaweicloud.sdk.eip.v2.model.CreatePrePaidPublicipExtendParamOption;
import com.huaweicloud.sdk.eip.v2.model.CreatePrePaidPublicipOption;
import com.huaweicloud.sdk.eip.v2.model.CreatePrePaidPublicipRequest;
import com.huaweicloud.sdk.eip.v2.model.CreatePrePaidPublicipRequestBody;
import com.huaweicloud.sdk.eip.v2.model.CreatePrePaidPublicipResponse;
import com.huaweicloud.sdk.eip.v2.model.CreatePublicipBandwidthOption;
import com.huaweicloud.sdk.eip.v2.model.ShowPublicipRequest;
import com.huaweicloud.sdk.eip.v2.model.ShowPublicipResponse;
import com.huaweicloud.sdk.eip.v2.region.EipRegion;

public class CreatePrePaidPublicipDemo {

    public static void main(String[] args) {
        // Enter the AK and SK of the HUAWEI CLOUD account.
        String ak = "{YOUR AK}";
        String sk = "{YOUR SK}";

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        // For EipClient of the v2 interface, set REGION_ID to the application region ID, for example, cn-north-4 for Beijing.
        EipClient client = EipClient.newBuilder()
                .withCredential(auth)
                .withRegion(EipRegion.valueOf("{REGION_ID}"))
                .build();
        CreatePrePaidPublicipRequest request = new CreatePrePaidPublicipRequest();
        CreatePrePaidPublicipRequestBody body = new CreatePrePaidPublicipRequestBody();
        CreatePrePaidPublicipOption publicipBody = new CreatePrePaidPublicipOption();
        // Specifies the type of the created EIP. The value can be 5_bgp (dynamic BGP), 5_sbgp (static BGP), or 5_youxuanbgp (preferred BGP).
        publicipBody.withType("{EIP TYPE}");
        CreatePublicipBandwidthOption bandwidthBody = new CreatePublicipBandwidthOption();
        // BANDWIDTH_NAME indicates the bandwidth name. The bandwidth size is transferred through the BANDWIDTH_SIZE parameter.
        bandwidthBody.withName("{BANDWIDTH_NAME}")
                .withShareType(CreatePublicipBandwidthOption.ShareTypeEnum.fromValue("PER"))
                .withSize("{BANDWIDTH_SIZE}");
        CreatePrePaidPublicipExtendParamOption extendParamBody = new CreatePrePaidPublicipExtendParamOption();
        // Construct extended parameters. The parameters are as follows: prepayment, period unit (month), number of subscription periods (1), no auto-renewal, and auto-payment.
        extendParamBody.withChargeMode(CreatePrePaidPublicipExtendParamOption.ChargeModeEnum.PREPAID)
                .withPeriodType(CreatePrePaidPublicipExtendParamOption.PeriodTypeEnum.MONTH)
                .withPeriodNum(1)
                .withIsAutoRenew(Boolean.FALSE)
                .withIsAutoPay(Boolean.TRUE);
        body.withPublicip(publicipBody);
        body.withBandwidth(bandwidthBody);
        body.withExtendParam(extendParamBody);
        request.withBody(body);
        try {
            CreatePrePaidPublicipResponse response = client.createPrePaidPublicip(request);
            // Construct a query request class.
            ShowPublicipRequest showRequest = new ShowPublicipRequest().withPublicipId(response.getPublicipId());
            // Query the EIP details to check whether the EIP is successfully created.
            ShowPublicipResponse publicipInfo = client.showPublicip(showRequest);
            System.out.println(response.toString());
            System.out.println(publicipInfo.toString());
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}
```
You can find it at [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/EIP/doc?version=v2&api=CreatePrePaidPublicip), run and debug the API.

## 5. Running Result
Example of a successful response

```json
{
  "order_id": "CS2307271951EQHOC",
  "publicip_id": "4d942e7a-14fe-4c24-98a8-a5a18985d006"
}
```

## 6. Reference
- EIP Product Overview
  - [Elastic public IP address](https://support.huaweicloud.com/intl/zh-cn/productdesc-eip/eip_pro_0001.html)
- API Reference
  - EIP Help Document [Applying for an EIP (Yearly/Monthly Package)](https://support.huaweicloud.com/intl/zh-cn/api-eip/eip_api_0006.html)
  - EIP Help Document [Querying an EIP](https://support.huaweicloud.com/api-eip/eip_api_0002.html)

## 7. Change History
| Release Date | Document Version | Revision Description |
|------------|------ |-------- |
| 2023 - 08 - 10 | 1.0 | Document First Release |