package com.huawei.hwclouds.vpc.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.eip.v3.EipClient;
import com.huaweicloud.sdk.eip.v3.model.DisassociatePublicipsRequest;
import com.huaweicloud.sdk.eip.v3.model.DisassociatePublicipsResponse;
import com.huaweicloud.sdk.eip.v3.region.EipRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @description 调用前请根据实际情况替换如下变量：
 * {YOUR AK}，{YOUR SK}，{REGION_ID}，{PUBLICIP ID}
 */
public class DisAssociateInstanceDemo {

    private static final Logger logger = LoggerFactory.getLogger(DisAssociateInstanceDemo.class);

    public static void main(String[] args) {
        String ak = "{YOUR AK}";
        String sk = "{YOUR SK}";

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        EipClient client = EipClient.newBuilder()
            .withCredential(auth)
            .withRegion(EipRegion.valueOf("{REGION_ID}"))
            .build();

        DisassociatePublicipsRequest request = new DisassociatePublicipsRequest();
        request.withPublicipId("{PUBLICIP ID}");

        try {
            DisassociatePublicipsResponse response = client.disassociatePublicips(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            logger.error("eip disassociate instance ConnectionException is: {}", e.getMessage());
        } catch (RequestTimeoutException e) {
            logger.error("eip disassociate instance RequestTimeoutException is: {}", e.getMessage());
        } catch (ServiceResponseException e) {
            logger.error("eip disassociate instance ServiceResponseException is: {}", e.getMessage());
        }
    }
}