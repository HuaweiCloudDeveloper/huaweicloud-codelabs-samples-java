## 1. Function Introduction
HUAWEI CLOUD provides the EIP server SDK. You can directly integrate the server SDK to call EIP APIs, implementing quick operations on EIPs. This example shows how to unbind an EIP using the Java SDK. For details about EIPs see [EIP](https://support.huaweicloud.com/intl/zh-cn/productdesc-eip/overview_0001.html) Overview.

## 2. Prerequisites
- You have [registered](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=94fc0f9f861b4f30a85ec1f463d35609&lang=en-us) with Huawei Cloud and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth).
- You have obtained the HUAWEI CLOUD SDK. You can also view and install the Java SDK.
- You have obtained the Access Key (AK) and Secret Access Key (SK) of the HUAWEI CLOUD account. On the HUAWEI CLOUD console, choose My Credential > Access Keys to create and view your AK/SK. For details, see [the access key](https://support.huaweicloud.com/intl/en-us/usermanual-ca/ca_01_0001.html).
- The development environment is available and Java JDK 1.8 or later is supported.
- An EIP has been bound to an instance. The instance can be an ECS, NAT gateway, VPN, or ELB load balancer.

## 3. Obtain and install the SDK.
For details about the SDK version, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) (Product Category: Elastic IP).

## 4. Key code snippets
Use the following code to unbind an EIP. Before invoking the API, replace the variables {YOUR AK}, {YOUR SK}, {REGION_ID}, {PUBLICIP ID} based on the site requirements.

```java
package com.huawei;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.eip.v3.EipClient;
import com.huaweicloud.sdk.eip.v3.model.DisassociatePublicipsRequest;
import com.huaweicloud.sdk.eip.v3.model.DisassociatePublicipsResponse;
import com.huaweicloud.sdk.eip.v3.model.ShowPublicipRequest;
import com.huaweicloud.sdk.eip.v3.model.ShowPublicipResponse;
import com.huaweicloud.sdk.eip.v3.region.EipRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @description Before invoking this interface, replace the following variables based on the site requirements:
 * {YOUR AK}, {YOUR SK}, {REGION_ID}, {PUBLICIP ID}
 */
public class DisAssociateInstanceDemo {

  private static final Logger logger = LoggerFactory.getLogger(DisAssociateInstanceDemo.class);

  public static void main(String[] args) {
    // Enter the AK and SK of the HUAWEI CLOUD account.
    String ak = "{YOUR AK}";
    String sk = "{YOUR SK}";

    ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

    // For EipClient of v3 interfaces, set REGION_ID to the application region ID, for example, cn-north-4 for Beijing.
    EipClient client = EipClient.newBuilder()
            .withCredential(auth)
            .withRegion(EipRegion.valueOf("{REGION_ID}"))
            .build();

    // Before binding an EIP to an instance, check whether the EIP has been bound to an instance. Only the EIP that has been bound to an instance can be unbound. Set PUBLICIP ID to the EIP ID.
    ShowPublicipResponse publicipResponse = client.showPublicip(new ShowPublicipRequest().withPublicipId("{PUBLICIP ID}"));

    // The interface does not require a request body.
    DisassociatePublicipsRequest request = new DisassociatePublicipsRequest();
    // Specifies the ID of the purchased EIP. For details about how to purchase an EIP, see Applying for an EIP (Pay-per-use) and Applying for an EIP (Yearly/Monthly) in the EIP help document.
    request.withPublicipId("{PUBLICIP ID}");

    try {
      // Prerequisites for unbinding: The values of associate_instance_type and associate_instance_id in the publicip structure of publicipResponse are not null.
      DisassociatePublicipsResponse response = client.disassociatePublicips(request);
      // The values of associate_instance_id and associate_instance_type are null.
      System.out.println(response.toString());
    } catch (ConnectionException e) {
      logger.info(e.toString());
    } catch (RequestTimeoutException e) {
      logger.info(e.toString());
    } catch (ServiceResponseException e) {
      logger.info(e.toString());
      System.out.println(e.getHttpStatusCode());
      System.out.println(e.getErrorCode());
      System.out.println(e.getErrorMsg());
    }
  }
}
```
You can find it at [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/EIP/doc?version=v3&api=DisassociatePublicips), run and debug the API.

## 6. Running Result
Example of a successful response

```json
{
  "request_id": "38d8698601ced6bae669fdaeeb49f1de",
  "publicip": {
    "created_at": "2023-07-29T07:19:54Z",
    "updated_at": "2023-07-29T08:50:05Z",
    "lock_status": null,
    "id": "94cfaacd-2471-417f-bbba-fa15213a3567",
    "alias": null,
    "project_id": "88060fecd26f41019cb32016bc3d2d94",
    "ip_version": 4,
    "public_ip_address": "100.95.156.127",
    "public_ipv6_address": null,
    "status": "DOWN",
    "description": "",
    "enterprise_project_id": "0",
    "billing_info": null,
    "type": "EIP",
    "vnic": null,
    "bandwidth": {
      "id": "43fd06dd-3da8-41b9-bed5-ff7f430f80f3",
      "size": 1,
      "share_type": "PER",
      "charge_mode": "traffic",
      "name": "ecs-c197-bandwidth-3a92",
      "billing_info": ""
    },
    "associate_instance_type": null,
    "associate_instance_id": null,
    "publicip_pool_id": "8a425166-82ac-4163-94d5-e6913579e7e7",
    "publicip_pool_name": "5_g-vm",
    "public_border_group": "center"
  }
}
```

## 6. Reference
- EIP Product Introduction
  - [Elastic public IP address](https://support.huaweicloud.com/intl/zh-cn/productdesc-eip/eip_pro_0001.html)
- API Reference
  - EIP Help Document [Applying for an EIP (Yearly/Monthly Package)](https://support.huaweicloud.com/intl/zh-cn/api-eip/eip_api_0006.html)
  - EIP Help Document [Applying for an EIP (Pay-per-use)](https://support.huaweicloud.com/intl/zh-cn/api-eip/eip_api_0001.html)
  - EIP Help Document [Unbinding an EIP](https://support.huaweicloud.com/intl/zh-cn/api-eip/DisassociatePublicips.html)

## 7. Change History
| Release Date | Document Version | Revision Description |
|------------|------ |-------- |
| 2023 - 08 - 10 | 1.0 | Document First Release |