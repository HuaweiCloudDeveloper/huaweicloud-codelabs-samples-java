## 1、功能介绍
华为云提供了弹性公网IP服务端SDK，您可以直接集成服务端SDK来调用弹性公网IP的相关API，从而实现对弹性公网IP的快速操作。该示例展示了如何通过Java版本SDK解绑弹性公网IP。解绑弹性公网IP参考文档请参见[解绑弹性公网IP](https://support.huaweicloud.com/api-eip/DisassociatePublicips.html)。

## 2、前置条件
- 已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，
  并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)
- 已获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 存在已绑定实例的EIP，实例可以为ECS、NAT网关、VPN、ELB负载均衡器。

## 3、SDK获取和安装
具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com/?language=Java) (产品类别：弹性公网IP)

## 4、关键代码片段
使用如下代码解绑弹性公网IP，调用前请根据实际情况替换如下变量：{YOUR AK}，{YOUR SK}，{REGION_ID}，{PUBLICIP ID}。

```java
package com.huawei;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.eip.v3.EipClient;
import com.huaweicloud.sdk.eip.v3.model.DisassociatePublicipsRequest;
import com.huaweicloud.sdk.eip.v3.model.DisassociatePublicipsResponse;
import com.huaweicloud.sdk.eip.v3.model.ShowPublicipRequest;
import com.huaweicloud.sdk.eip.v3.model.ShowPublicipResponse;
import com.huaweicloud.sdk.eip.v3.region.EipRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @description 调用前请根据实际情况替换如下变量：
 * {YOUR AK}，{YOUR SK}，{REGION_ID}，{PUBLICIP ID}
 */
public class DisAssociateInstanceDemo {

  private static final Logger logger = LoggerFactory.getLogger(DisAssociateInstanceDemo.class);

  public static void main(String[] args) {
    // 输入华为云账号的AK、SK
    String ak = "{YOUR AK}";
    String sk = "{YOUR SK}";

    ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

    // v3接口的EipClient，REGION_ID处填写应用区域的id，如北京四填写cn-north-4
    EipClient client = EipClient.newBuilder()
            .withCredential(auth)
            .withRegion(EipRegion.valueOf("{REGION_ID}"))
            .build();

    // 解绑之前先查询该弹性公网IP是否已绑定实例，只有已绑定实例的EIP才允许解绑，PUBLICIP ID为EIP的ID
    ShowPublicipResponse publicipResponse = client.showPublicip(new ShowPublicipRequest().withPublicipId("{PUBLICIP ID}"));

    // 该接口无需请求体
    DisassociatePublicipsRequest request = new DisassociatePublicipsRequest();
    // 已购买EIP的ID，如何购买EIP请参考EIP帮助文档中申请EIP(按需计费)和申请EIP(包年/包月)章节
    request.withPublicipId("{PUBLICIP ID}");

    try {
      // 解绑的前提：publicipResponse的publicip结构体中associate_instance_type和associate_instance_id都不为null
      DisassociatePublicipsResponse response = client.disassociatePublicips(request);
      // 返回结果中associate_instance_id和associate_instance_type字段应该为null
      System.out.println(response.toString());
    } catch (ConnectionException e) {
      logger.info(e.toString());
    } catch (RequestTimeoutException e) {
      logger.info(e.toString());
    } catch (ServiceResponseException e) {
      logger.info(e.toString());
      System.out.println(e.getHttpStatusCode());
      System.out.println(e.getErrorCode());
      System.out.println(e.getErrorMsg());
    }
  }
}
```
您可以在 [API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=EIP&api=DisassociatePublicips&version=v3) 中直接运行调试该接口。

## 6、运行结果
响应成功示例

```json
{
  "request_id": "38d8698601ced6bae669fdaeeb49f1de",
  "publicip": {
    "created_at": "2023-07-29T07:19:54Z",
    "updated_at": "2023-07-29T08:50:05Z",
    "lock_status": null,
    "id": "94cfaacd-2471-417f-bbba-fa15213a3567",
    "alias": null,
    "project_id": "88060fecd26f41019cb32016bc3d2d94",
    "ip_version": 4,
    "public_ip_address": "100.95.156.127",
    "public_ipv6_address": null,
    "status": "DOWN",
    "description": "",
    "enterprise_project_id": "0",
    "billing_info": null,
    "type": "EIP",
    "vnic": null,
    "bandwidth": {
      "id": "43fd06dd-3da8-41b9-bed5-ff7f430f80f3",
      "size": 1,
      "share_type": "PER",
      "charge_mode": "traffic",
      "name": "ecs-c197-bandwidth-3a92",
      "billing_info": ""
    },
    "associate_instance_type": null,
    "associate_instance_id": null,
    "publicip_pool_id": "8a425166-82ac-4163-94d5-e6913579e7e7",
    "publicip_pool_name": "5_g-vm",
    "public_border_group": "center"
  }
}
```

## 6、参考
- EIP产品介绍
  - [图解弹性公网IP](https://support.huaweicloud.com/productdesc-eip/eip_pro_0001.html)
- API参考
  - EIP帮助文档 [申请EIP(包年/包月)](https://support.huaweicloud.com/api-eip/eip_api_0006.html)
  - EIP帮助文档 [申请EIP(按需计费)](https://support.huaweicloud.com/api-eip/eip_api_0001.html)
  - EIP帮助文档 [解绑弹性公网IP](https://support.huaweicloud.com/api-eip/DisassociatePublicips.html)

## 7、修订记录
| 发布日期       | 文档版本 | 修订说明 |
|------------|------   |--------   |
| 2023-08-10 | 1.0  | 文档首次发布 |