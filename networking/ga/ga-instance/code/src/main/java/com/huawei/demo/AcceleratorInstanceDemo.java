package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.ga.v1.GaClient;
import com.huaweicloud.sdk.ga.v1.model.Area;
import com.huaweicloud.sdk.ga.v1.model.ClientAffinity;
import com.huaweicloud.sdk.ga.v1.model.ConfigStatus;
import com.huaweicloud.sdk.ga.v1.model.CreateAcceleratorOptionIpSets;
import com.huaweicloud.sdk.ga.v1.model.CreateAcceleratorRequest;
import com.huaweicloud.sdk.ga.v1.model.CreateAcceleratorRequestBody;
import com.huaweicloud.sdk.ga.v1.model.CreateAcceleratorResponse;
import com.huaweicloud.sdk.ga.v1.model.CreateEndpointGroupRequest;
import com.huaweicloud.sdk.ga.v1.model.CreateEndpointGroupRequestBody;
import com.huaweicloud.sdk.ga.v1.model.CreateEndpointGroupResponse;
import com.huaweicloud.sdk.ga.v1.model.CreateEndpointRequest;
import com.huaweicloud.sdk.ga.v1.model.CreateEndpointRequestBody;
import com.huaweicloud.sdk.ga.v1.model.CreateEndpointResponse;
import com.huaweicloud.sdk.ga.v1.model.CreateHealthCheckRequest;
import com.huaweicloud.sdk.ga.v1.model.CreateHealthCheckRequestBody;
import com.huaweicloud.sdk.ga.v1.model.CreateHealthCheckResponse;
import com.huaweicloud.sdk.ga.v1.model.CreateListenerRequest;
import com.huaweicloud.sdk.ga.v1.model.CreateListenerRequestBody;
import com.huaweicloud.sdk.ga.v1.model.CreateListenerResponse;
import com.huaweicloud.sdk.ga.v1.model.EndpointType;
import com.huaweicloud.sdk.ga.v1.model.HealthCheckProtocol;
import com.huaweicloud.sdk.ga.v1.model.Id;
import com.huaweicloud.sdk.ga.v1.model.ListRegionsRequest;
import com.huaweicloud.sdk.ga.v1.model.ListRegionsResponse;
import com.huaweicloud.sdk.ga.v1.model.ListenerProtocol;
import com.huaweicloud.sdk.ga.v1.model.PortRange;
import com.huaweicloud.sdk.ga.v1.model.ShowAcceleratorRequest;
import com.huaweicloud.sdk.ga.v1.model.ShowAcceleratorResponse;
import com.huaweicloud.sdk.ga.v1.model.ShowEndpointGroupRequest;
import com.huaweicloud.sdk.ga.v1.model.ShowEndpointGroupResponse;
import com.huaweicloud.sdk.ga.v1.model.ShowEndpointRequest;
import com.huaweicloud.sdk.ga.v1.model.ShowEndpointResponse;
import com.huaweicloud.sdk.ga.v1.model.ShowHealthCheckRequest;
import com.huaweicloud.sdk.ga.v1.model.ShowHealthCheckResponse;
import com.huaweicloud.sdk.ga.v1.model.ShowListenerRequest;
import com.huaweicloud.sdk.ga.v1.model.ShowListenerResponse;
import com.huaweicloud.sdk.ga.v1.region.GaRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.function.Function;

public class AcceleratorInstanceDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(AcceleratorInstanceDemo.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        int showDetailRetryInterval = 5000;

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        GaClient client = GaClient.newBuilder()
            .withCredential(auth)
            .withRegion(GaRegion.valueOf("cn-east-3"))
            .build();

        // 创建全球加速实例
        CreateAcceleratorResponse accelerator = createAccelerator(client);
        String acceleratorId = accelerator.getAccelerator().getId();
        // 查询全球加速实例详情，直到资源状态变成ACTIVE再继续创建关联资源
        ShowAcceleratorResponse showAcceleratorResponse;
        do {
            try {
                Thread.sleep(showDetailRetryInterval);
            } catch (InterruptedException exception) {
                LOGGER.error("Thread sleep exception", exception);
            }
            showAcceleratorResponse = showAccelerator(client, acceleratorId);
        } while (!ConfigStatus.ACTIVE.equals(showAcceleratorResponse.getAccelerator().getStatus()));

        // 创建监听器
        CreateListenerResponse listener = createListener(client, acceleratorId);
        String listenerId = listener.getListener().getId();
        // 查询监听器详情，直到资源状态变成ACTIVE再继续创建关联资源
        ShowListenerResponse showListenerResponse;
        do {
            try {
                Thread.sleep(showDetailRetryInterval);
            } catch (InterruptedException exception) {
                LOGGER.error("Thread sleep exception", exception);
            }
            showListenerResponse = showListener(client, listenerId);
        } while (!ConfigStatus.ACTIVE.equals(showListenerResponse.getListener().getStatus()));

        // 查询支持的region列表
        ListRegionsResponse listRegionsResponse = listRegions(client);
        String regionId = listRegionsResponse.getRegions().get(0).getRegionId();

        // 创建终端节点组
        CreateEndpointGroupResponse endpointGroup = createEndpointGroup(client, listenerId, regionId);
        String endpointGroupId = endpointGroup.getEndpointGroup().getId();
        // 查询终端节点组详情，直到资源状态变成ACTIVE再继续创建关联资源
        ShowEndpointGroupResponse showEndpointGroupResponse;
        do {
            try {
                Thread.sleep(showDetailRetryInterval);
            } catch (InterruptedException exception) {
                LOGGER.error("Thread sleep exception", exception);
            }
            showEndpointGroupResponse = showEndpointGroup(client, endpointGroupId);
        } while (!ConfigStatus.ACTIVE.equals(showEndpointGroupResponse.getEndpointGroup().getStatus()));

        // 创建终端节点
        CreateEndpointResponse endpoint = createEndpoint(client, endpointGroupId);
        String endpointId = endpoint.getEndpoint().getId();
        // 查询终端节点详情
        showEndpoint(client, endpointGroupId, endpointId);

        // 创建健康检查
        CreateHealthCheckResponse healthCheck = createHealthCheck(client, endpointGroupId);
        String healthCheckId = healthCheck.getHealthCheck().getId();
        // 查询健康检查详情
        showHealthCheck(client, healthCheckId);
    }

    // 创建加速器
    private static CreateAcceleratorResponse createAccelerator(GaClient client) {
        CreateAcceleratorRequest request = new CreateAcceleratorRequest();
        CreateAcceleratorRequestBody body = new CreateAcceleratorRequestBody();

        body.withAccelerator(createAcceleratorOption -> {
            createAcceleratorOption.withName("<your accelerator name>");
            createAcceleratorOption.withDescription("<your accelerator description>");
            createAcceleratorOption.withEnterpriseProjectId("<your enterprise project id>");
            createAcceleratorOption.withIpSets(Collections.singletonList(
                new CreateAcceleratorOptionIpSets().withIpType(CreateAcceleratorOptionIpSets.IpTypeEnum.IPV4)
                    .withArea(Area.OUTOFCM)));
        });
        request.withBody(body);
        Function<Void, CreateAcceleratorResponse> task = (Void v) -> client.createAccelerator(request);
        return execute(task);
    }

    // 查询加速器详情
    private static ShowAcceleratorResponse showAccelerator(GaClient client, String acceleratorId) {
        ShowAcceleratorRequest request = new ShowAcceleratorRequest();
        request.withAcceleratorId(acceleratorId);
        Function<Void, ShowAcceleratorResponse> task = (Void v) -> client.showAccelerator(request);
        return execute(task);
    }

    // 创建监听器
    private static CreateListenerResponse createListener(GaClient client, String acceleratorId) {
        CreateListenerRequest request = new CreateListenerRequest();
        CreateListenerRequestBody body = new CreateListenerRequestBody();

        body.withListener(createListenerOption -> {
            createListenerOption.withName("<your listener name>");
            createListenerOption.withDescription("<your listener description>");
            createListenerOption.withAcceleratorId(acceleratorId);
            createListenerOption.withClientAffinity(ClientAffinity.SOURCE_IP);
            createListenerOption.withProtocol(ListenerProtocol.TCP);
            createListenerOption.withPortRanges(
                Collections.singletonList(new PortRange().withFromPort(4000).withToPort(4200)));
        });
        request.withBody(body);
        Function<Void, CreateListenerResponse> task = (Void v) -> client.createListener(request);
        return execute(task);
    }

    // 查询监听器详情
    private static ShowListenerResponse showListener(GaClient client, String listenerId) {
        ShowListenerRequest request = new ShowListenerRequest().withListenerId(listenerId);
        Function<Void, ShowListenerResponse> task = (Void v) -> client.showListener(request);
        return execute(task);
    }

    // 查询区域列表
    private static ListRegionsResponse listRegions(GaClient client) {
        ListRegionsRequest request = new ListRegionsRequest();
        Function<Void, ListRegionsResponse> task = (Void v) -> client.listRegions(request);
        return execute(task);
    }

    // 创建终端节点组
    private static CreateEndpointGroupResponse createEndpointGroup(GaClient client, String listenerId,
        String regionId) {
        CreateEndpointGroupRequest request = new CreateEndpointGroupRequest();
        CreateEndpointGroupRequestBody body = new CreateEndpointGroupRequestBody();
        body.withEndpointGroup(createEndpointGroupOption -> {
            createEndpointGroupOption.withName("<your endpointGroup name>");
            createEndpointGroupOption.withDescription("<your endpointGroup description>");
            createEndpointGroupOption.withListeners(Collections.singletonList(new Id().withId(listenerId)));
            createEndpointGroupOption.withRegionId(regionId);
            createEndpointGroupOption.withTrafficDialPercentage(10);
        });
        request.withBody(body);
        Function<Void, CreateEndpointGroupResponse> task = (Void v) -> client.createEndpointGroup(request);
        return execute(task);
    }

    // 查询终端节点组详情
    private static ShowEndpointGroupResponse showEndpointGroup(GaClient client, String endpointGroupId) {
        ShowEndpointGroupRequest request = new ShowEndpointGroupRequest().withEndpointGroupId(endpointGroupId);
        Function<Void, ShowEndpointGroupResponse> task = (Void v) -> client.showEndpointGroup(request);
        return execute(task);
    }

    // 创建终端节点
    private static CreateEndpointResponse createEndpoint(GaClient client, String endpointGroupId) {
        CreateEndpointRequest request = new CreateEndpointRequest().withEndpointGroupId(endpointGroupId);
        CreateEndpointRequestBody body = new CreateEndpointRequestBody();
        body.withEndpoint(createEndpointOption -> {
            createEndpointOption.withIpAddress("<your endpoint ip address>");
            createEndpointOption.withResourceId("<your endpoint id>");
            createEndpointOption.withResourceType(EndpointType.EIP);
            createEndpointOption.withWeight(1);
        });
        request.withBody(body);
        Function<Void, CreateEndpointResponse> task = (Void v) -> client.createEndpoint(request);
        return execute(task);
    }

    // 查询终端节点详情
    private static ShowEndpointResponse showEndpoint(GaClient client, String endpointGroupId, String endpointId) {
        ShowEndpointRequest request = new ShowEndpointRequest().withEndpointGroupId(endpointGroupId)
            .withEndpointId(endpointId);
        Function<Void, ShowEndpointResponse> task = (Void v) -> client.showEndpoint(request);
        return execute(task);
    }

    // 创建健康检查
    private static CreateHealthCheckResponse createHealthCheck(GaClient client, String endpointGroupId) {
        CreateHealthCheckRequest request = new CreateHealthCheckRequest();
        CreateHealthCheckRequestBody body = new CreateHealthCheckRequestBody();
        body.withHealthCheck(createHealthCheckOption -> {
            createHealthCheckOption.withEndpointGroupId(endpointGroupId);
            createHealthCheckOption.withProtocol(HealthCheckProtocol.TCP);
            createHealthCheckOption.withPort(3333);
            createHealthCheckOption.withInterval(30);
            createHealthCheckOption.withTimeout(30);
            createHealthCheckOption.withMaxRetries(1);
            createHealthCheckOption.withEnabled(true);
        });
        request.withBody(body);
        Function<Void, CreateHealthCheckResponse> task = (Void v) -> client.createHealthCheck(request);
        return execute(task);
    }

    // 查询健康检查详情
    private static ShowHealthCheckResponse showHealthCheck(GaClient client, String healthCheckId) {
        ShowHealthCheckRequest request = new ShowHealthCheckRequest().withHealthCheckId(healthCheckId);
        Function<Void, ShowHealthCheckResponse> task = (Void v) -> client.showHealthCheck(request);
        return execute(task);
    }

    private static <T> T execute(Function<Void, T> task) {
        T response = null;
        try {
            response = task.apply(null);
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
        return response;
    }
}
