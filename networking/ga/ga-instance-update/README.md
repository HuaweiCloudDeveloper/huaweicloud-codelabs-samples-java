### 示例简介
本示例展示如何使用全球加速服务（Global Accelerator）相关SDK对全球加速实例、监听器、终端节点组、终端节点、健康检查的更新功能

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

### SDK获取和安装
您可以通过Maven配置所依赖的全球加速服务SDK

具体的SDK版本号请参见 [GA JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/GA?lang=Java)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-ga</artifactId>
    <version>3.1.67</version>
</dependency>
```
### 代码示例
以下代码展示如何使用全球加速服务（Global Accelerator）相关SDK对全球加速实例、监听器、终端节点组、终端节点、健康检查的更新功能。
``` java
public class AcceleratorInstanceUpdateDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(AcceleratorInstanceUpdateDemo.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        GaClient client = GaClient.newBuilder()
            .withCredential(auth)
            .withRegion(GaRegion.valueOf("cn-east-3"))
            .build();

        // 更新全球加速实例
        updateAccelerator(client, "<your accelerator id>");

        // 更新监听器
        updateListener(client, "<your listener id>");

        // 更新终端节点组
        updateEndpointGroup(client, "<your endpointGroup id>");

        // 更新终端节点
        updateEndpoint(client, "<your endpointGroup id>", "<your endpoint id>");

        // 更新健康检查
        updateHealthCheck(client, "<your health check id>");
    }

    // 更新加速器
    private static UpdateAcceleratorResponse updateAccelerator(GaClient client, String acceleratorId) {
        UpdateAcceleratorRequest request = new UpdateAcceleratorRequest();
        UpdateAcceleratorRequestBody body = new UpdateAcceleratorRequestBody();

        body.withAccelerator(updateAcceleratorOption -> {
            updateAcceleratorOption.withName("<your new accelerator name>");
            updateAcceleratorOption.withDescription("<your new accelerator description>");
        });
        request.withAcceleratorId(acceleratorId).withBody(body);
        Function<Void, UpdateAcceleratorResponse> task = (Void v) -> client.updateAccelerator(request);
        return execute(task);
    }

    // 更新监听器
    private static UpdateListenerResponse updateListener(GaClient client, String listenerId) {
        UpdateListenerRequest request = new UpdateListenerRequest();
        UpdateListenerRequestBody body = new UpdateListenerRequestBody();

        body.withListener(updateListenerOption -> {
            updateListenerOption.withName("<your new listener name>");
            updateListenerOption.withDescription("<your new listener description>");
            updateListenerOption.withClientAffinity(ClientAffinity.NONE);
            updateListenerOption.withPortRanges(
                Collections.singletonList(new PortRange().withFromPort(5000).withToPort(5200)));
        });
        request.withListenerId(listenerId).withBody(body);
        Function<Void, UpdateListenerResponse> task = (Void v) -> client.updateListener(request);
        return execute(task);
    }

    // 更新终端节点组
    private static UpdateEndpointGroupResponse updateEndpointGroup(GaClient client, String endpointGroupId) {
        UpdateEndpointGroupRequest request = new UpdateEndpointGroupRequest();
        UpdateEndpointGroupRequestBody body = new UpdateEndpointGroupRequestBody();
        body.withEndpointGroup(updateEndpointGroupOption -> {
            updateEndpointGroupOption.withName("<your new endpointGroup name>");
            updateEndpointGroupOption.withDescription("<your new endpointGroup description>");
            updateEndpointGroupOption.withTrafficDialPercentage(20);
        });
        request.withEndpointGroupId(endpointGroupId).withBody(body);
        Function<Void, UpdateEndpointGroupResponse> task = (Void v) -> client.updateEndpointGroup(request);
        return execute(task);
    }

    // 更新终端节点
    private static UpdateEndpointResponse updateEndpoint(GaClient client, String endpointGroupId, String endpointId) {
        UpdateEndpointRequest request = new UpdateEndpointRequest();
        UpdateEndpointRequestBody body = new UpdateEndpointRequestBody();
        body.withEndpoint(updateEndpointOption -> updateEndpointOption.withWeight(2));
        request.withEndpointGroupId(endpointGroupId).withEndpointId(endpointId).withBody(body);
        Function<Void, UpdateEndpointResponse> task = (Void v) -> client.updateEndpoint(request);
        return execute(task);
    }

    // 更新健康检查
    private static UpdateHealthCheckResponse updateHealthCheck(GaClient client, String healthCheckId) {
        UpdateHealthCheckRequest request = new UpdateHealthCheckRequest();
        UpdateHealthCheckRequestBody body = new UpdateHealthCheckRequestBody();
        body.withHealthCheck(updateHealthCheckOption -> {
            updateHealthCheckOption.withProtocol(HealthCheckProtocol.TCP);
            updateHealthCheckOption.withPort(3334);
            updateHealthCheckOption.withInterval(20);
            updateHealthCheckOption.withTimeout(20);
            updateHealthCheckOption.withMaxRetries(2);
            updateHealthCheckOption.withEnabled(true);
        });
        request.withHealthCheckId(healthCheckId).withBody(body);
        Function<Void, UpdateHealthCheckResponse> task = (Void v) -> client.updateHealthCheck(request);
        return execute(task);
    }

    private static <T> T execute(Function<Void, T> task) {
        T response = null;
        try {
            response = task.apply(null);
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
        return response;
    }
}
```
您可以在 [全球加速服务文档](https://support.huaweicloud.com/productdesc-ga/ga_01_0001.html) 和[API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=GA&api=CreateAccelerator) 查看具体信息。

### 修订记录

发布日期  | 文档版本 | 修订说明
 :----: | :-----: | :------:  
2022/11/17 |1.0 | 文档首次发布