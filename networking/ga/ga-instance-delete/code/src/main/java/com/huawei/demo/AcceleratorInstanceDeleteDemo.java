package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.ga.v1.GaClient;
import com.huaweicloud.sdk.ga.v1.model.DeleteAcceleratorRequest;
import com.huaweicloud.sdk.ga.v1.model.DeleteAcceleratorResponse;
import com.huaweicloud.sdk.ga.v1.model.DeleteEndpointGroupRequest;
import com.huaweicloud.sdk.ga.v1.model.DeleteEndpointGroupResponse;
import com.huaweicloud.sdk.ga.v1.model.DeleteEndpointRequest;
import com.huaweicloud.sdk.ga.v1.model.DeleteEndpointResponse;
import com.huaweicloud.sdk.ga.v1.model.DeleteHealthCheckRequest;
import com.huaweicloud.sdk.ga.v1.model.DeleteHealthCheckResponse;
import com.huaweicloud.sdk.ga.v1.model.DeleteListenerRequest;
import com.huaweicloud.sdk.ga.v1.model.DeleteListenerResponse;
import com.huaweicloud.sdk.ga.v1.model.ListAcceleratorsRequest;
import com.huaweicloud.sdk.ga.v1.model.ListAcceleratorsResponse;
import com.huaweicloud.sdk.ga.v1.model.ListEndpointGroupsRequest;
import com.huaweicloud.sdk.ga.v1.model.ListEndpointGroupsResponse;
import com.huaweicloud.sdk.ga.v1.model.ListEndpointsRequest;
import com.huaweicloud.sdk.ga.v1.model.ListEndpointsResponse;
import com.huaweicloud.sdk.ga.v1.model.ListHealthChecksRequest;
import com.huaweicloud.sdk.ga.v1.model.ListHealthChecksResponse;
import com.huaweicloud.sdk.ga.v1.model.ListListenersRequest;
import com.huaweicloud.sdk.ga.v1.model.ListListenersResponse;
import com.huaweicloud.sdk.ga.v1.region.GaRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

public class AcceleratorInstanceDeleteDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(AcceleratorInstanceDeleteDemo.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        int listRetryInterval = 5000;

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        GaClient client = GaClient.newBuilder()
            .withCredential(auth)
            .withRegion(GaRegion.valueOf("cn-east-3"))
            .build();

        // 删除终端节点
        deleteEndpoint(client, "<your endpointGroup id>", "<your endpoint id>");
        // 查询终端节点列表，查询该资源是否删除成功；直到该资源删除成功，再进行后续资源的删除操作
        ListEndpointsResponse listEndpointsResponse;
        do {
            try {
                Thread.sleep(listRetryInterval);
            } catch (InterruptedException exception) {
                LOGGER.error("Thread sleep exception", exception);
            }
            listEndpointsResponse = listEndpoints(client, "<your endpointGroup id>", "<your endpoint id>");
        } while (!listEndpointsResponse.getEndpoints().isEmpty());

        // 删除健康检查
        deleteHealthCheck(client, "<your health check id>");
        // 查询健康检查列表，查询该资源是否删除成功；直到该资源删除成功，再进行后续资源的删除操作
        ListHealthChecksResponse listHealthChecksResponse;
        do {
            try {
                Thread.sleep(listRetryInterval);
            } catch (InterruptedException exception) {
                LOGGER.error("Thread sleep exception", exception);
            }
            listHealthChecksResponse = listHealthChecks(client, "<your health check id>");
        } while (!listHealthChecksResponse.getHealthChecks().isEmpty());

        // 删除终端节点组
        deleteEndpointGroup(client, "<your endpointGroup id>");
        // 查询终端节点组列表，查询该资源是否删除成功；直到该资源删除成功，再进行后续资源的删除操作
        ListEndpointGroupsResponse listEndpointGroupsResponse;
        do {
            try {
                Thread.sleep(listRetryInterval);
            } catch (InterruptedException exception) {
                LOGGER.error("Thread sleep exception", exception);
            }
            listEndpointGroupsResponse = listEndpointGroups(client, "<your endpointGroup id>");
        } while (!listEndpointGroupsResponse.getEndpointGroups().isEmpty());

        // 删除监听器
        deleteListener(client, "<your listener id>");
        // 查询监听器列表，查询该资源是否删除成功；直到该资源删除成功，再进行后续资源的删除操作
        ListListenersResponse listListenersResponse;
        do {
            try {
                Thread.sleep(listRetryInterval);
            } catch (InterruptedException exception) {
                LOGGER.error("Thread sleep exception", exception);
            }
            listListenersResponse = listListeners(client, "<your listener id>");
        } while (!listListenersResponse.getListeners().isEmpty());

        // 删除全球加速实例
        deleteAccelerator(client, "<your accelerator id>");
        // 查询全球加速实例列表，查询该资源是否删除成功；直到该资源删除成功，再进行后续资源的删除操作
        ListAcceleratorsResponse listAcceleratorsResponse;
        do {
            try {
                Thread.sleep(listRetryInterval);
            } catch (InterruptedException exception) {
                LOGGER.error("Thread sleep exception", exception);
            }
            listAcceleratorsResponse = listAccelerators(client, "<your accelerator id>");
        } while (!listAcceleratorsResponse.getAccelerators().isEmpty());
    }

    // 删除终端节点
    private static DeleteEndpointResponse deleteEndpoint(GaClient client, String endpointGroupId, String endpointId) {
        DeleteEndpointRequest request = new DeleteEndpointRequest().withEndpointGroupId(endpointGroupId)
            .withEndpointId(endpointId);
        Function<Void, DeleteEndpointResponse> task = (Void v) -> client.deleteEndpoint(request);
        return execute(task);
    }

    // 查询终端节点列表
    private static ListEndpointsResponse listEndpoints(GaClient client, String endpointGroupId, String endpointId) {
        ListEndpointsRequest request = new ListEndpointsRequest().withEndpointGroupId(endpointGroupId)
            .withId(endpointId);
        Function<Void, ListEndpointsResponse> task = (Void v) -> client.listEndpoints(request);
        return execute(task);
    }

    // 删除健康检查
    private static DeleteHealthCheckResponse deleteHealthCheck(GaClient client, String healthCheckId) {
        DeleteHealthCheckRequest request = new DeleteHealthCheckRequest().withHealthCheckId(healthCheckId);
        Function<Void, DeleteHealthCheckResponse> task = (Void v) -> client.deleteHealthCheck(request);
        return execute(task);
    }

    // 查询健康检查列表
    private static ListHealthChecksResponse listHealthChecks(GaClient client, String healthCheckId) {
        ListHealthChecksRequest request = new ListHealthChecksRequest().withId(healthCheckId);
        Function<Void, ListHealthChecksResponse> task = (Void v) -> client.listHealthChecks(request);
        return execute(task);
    }

    // 删除终端节点组
    private static DeleteEndpointGroupResponse deleteEndpointGroup(GaClient client, String endpointGroupId) {
        DeleteEndpointGroupRequest request = new DeleteEndpointGroupRequest().withEndpointGroupId(endpointGroupId);
        Function<Void, DeleteEndpointGroupResponse> task = (Void v) -> client.deleteEndpointGroup(request);
        return execute(task);
    }

    // 查询终端节点组列表
    private static ListEndpointGroupsResponse listEndpointGroups(GaClient client, String endpointGroupId) {
        ListEndpointGroupsRequest request = new ListEndpointGroupsRequest().withId(endpointGroupId);
        Function<Void, ListEndpointGroupsResponse> task = (Void v) -> client.listEndpointGroups(request);
        return execute(task);
    }

    // 删除监听器
    private static DeleteListenerResponse deleteListener(GaClient client, String listenerId) {
        DeleteListenerRequest request = new DeleteListenerRequest().withListenerId(listenerId);
        Function<Void, DeleteListenerResponse> task = (Void v) -> client.deleteListener(request);
        return execute(task);
    }

    // 查询监听器列表
    private static ListListenersResponse listListeners(GaClient client, String listenerId) {
        ListListenersRequest request = new ListListenersRequest().withId(listenerId);
        Function<Void, ListListenersResponse> task = (Void v) -> client.listListeners(request);
        return execute(task);
    }

    // 删除全球加速实例
    private static DeleteAcceleratorResponse deleteAccelerator(GaClient client, String acceleratorId) {
        DeleteAcceleratorRequest request = new DeleteAcceleratorRequest().withAcceleratorId(acceleratorId);
        Function<Void, DeleteAcceleratorResponse> task = (Void v) -> client.deleteAccelerator(request);
        return execute(task);
    }

    // 查询全球加速实例列表
    private static ListAcceleratorsResponse listAccelerators(GaClient client, String acceleratorId) {
        ListAcceleratorsRequest request = new ListAcceleratorsRequest().withId(acceleratorId);
        Function<Void, ListAcceleratorsResponse> task = (Void v) -> client.listAccelerators(request);
        return execute(task);
    }

    private static <T> T execute(Function<Void, T> task) {
        T response = null;
        try {
            response = task.apply(null);
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
        return response;
    }
}
