### 示例简介
本示例展示如何使用全球加速服务（Global Accelerator）相关SDK删除全球加速资源。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

### SDK获取和安装
您可以通过Maven配置所依赖的全球加速服务SDK

具体的SDK版本号请参见 [GA JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/GA?lang=Java)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-ga</artifactId>
    <version>3.1.67</version>
</dependency>
```
### 代码示例
该代码示例展示了全球加速实例、监听器、终端节点组、终端节点、健康检查的删除功能。由于删除操作是异步接口，在进行调试时，需要注释无关的代码。
以删除终端节点为例, 进行测试时，将其它功能的测试函数注释掉。

删除全球加速资源需要从终端节点和健康检查开始，只有将资源的关联资源删除之后，才能删除此资源。
``` java
public class AcceleratorInstanceDeleteDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(AcceleratorInstanceDeleteDemo.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        int listRetryInterval = 5000;

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        GaClient client = GaClient.newBuilder()
            .withCredential(auth)
            .withRegion(GaRegion.valueOf("cn-east-3"))
            .build();

        // 删除终端节点
        deleteEndpoint(client, "<your endpointGroup id>", "<your endpoint id>");
        // 查询终端节点列表，查询该资源是否删除成功；直到该资源删除成功，再进行后续资源的删除操作
        ListEndpointsResponse listEndpointsResponse;
        do {
            try {
                Thread.sleep(listRetryInterval);
            } catch (InterruptedException exception) {
                LOGGER.error("Thread sleep exception", exception);
            }
            listEndpointsResponse = listEndpoints(client, "<your endpointGroup id>", "<your endpoint id>");
        } while (!listEndpointsResponse.getEndpoints().isEmpty());

        // 删除健康检查
        deleteHealthCheck(client, "<your health check id>");
        // 查询健康检查列表，查询该资源是否删除成功；直到该资源删除成功，再进行后续资源的删除操作
        ListHealthChecksResponse listHealthChecksResponse;
        do {
            try {
                Thread.sleep(listRetryInterval);
            } catch (InterruptedException exception) {
                LOGGER.error("Thread sleep exception", exception);
            }
            listHealthChecksResponse = listHealthChecks(client, "<your health check id>");
        } while (!listHealthChecksResponse.getHealthChecks().isEmpty());

        // 删除终端节点组
        deleteEndpointGroup(client, "<your endpointGroup id>");
        // 查询终端节点组列表，查询该资源是否删除成功；直到该资源删除成功，再进行后续资源的删除操作
        ListEndpointGroupsResponse listEndpointGroupsResponse;
        do {
            try {
                Thread.sleep(listRetryInterval);
            } catch (InterruptedException exception) {
                LOGGER.error("Thread sleep exception", exception);
            }
            listEndpointGroupsResponse = listEndpointGroups(client, "<your endpointGroup id>");
        } while (!listEndpointGroupsResponse.getEndpointGroups().isEmpty());

        // 删除监听器
        deleteListener(client, "<your listener id>");
        // 查询监听器列表，查询该资源是否删除成功；直到该资源删除成功，再进行后续资源的删除操作
        ListListenersResponse listListenersResponse;
        do {
            try {
                Thread.sleep(listRetryInterval);
            } catch (InterruptedException exception) {
                LOGGER.error("Thread sleep exception", exception);
            }
            listListenersResponse = listListeners(client, "<your listener id>");
        } while (!listListenersResponse.getListeners().isEmpty());

        // 删除全球加速实例
        deleteAccelerator(client, "<your accelerator id>");
        // 查询全球加速实例列表，查询该资源是否删除成功；直到该资源删除成功，再进行后续资源的删除操作
        ListAcceleratorsResponse listAcceleratorsResponse;
        do {
            try {
                Thread.sleep(listRetryInterval);
            } catch (InterruptedException exception) {
                LOGGER.error("Thread sleep exception", exception);
            }
            listAcceleratorsResponse = listAccelerators(client, "<your accelerator id>");
        } while (!listAcceleratorsResponse.getAccelerators().isEmpty());
    }

    // 删除终端节点
    private static DeleteEndpointResponse deleteEndpoint(GaClient client, String endpointGroupId, String endpointId) {
        DeleteEndpointRequest request = new DeleteEndpointRequest().withEndpointGroupId(endpointGroupId)
            .withEndpointId(endpointId);
        Function<Void, DeleteEndpointResponse> task = (Void v) -> client.deleteEndpoint(request);
        return execute(task);
    }

    // 查询终端节点列表
    private static ListEndpointsResponse listEndpoints(GaClient client, String endpointGroupId, String endpointId) {
        ListEndpointsRequest request = new ListEndpointsRequest().withEndpointGroupId(endpointGroupId)
            .withId(endpointId);
        Function<Void, ListEndpointsResponse> task = (Void v) -> client.listEndpoints(request);
        return execute(task);
    }

    // 删除健康检查
    private static DeleteHealthCheckResponse deleteHealthCheck(GaClient client, String healthCheckId) {
        DeleteHealthCheckRequest request = new DeleteHealthCheckRequest().withHealthCheckId(healthCheckId);
        Function<Void, DeleteHealthCheckResponse> task = (Void v) -> client.deleteHealthCheck(request);
        return execute(task);
    }

    // 查询健康检查列表
    private static ListHealthChecksResponse listHealthChecks(GaClient client, String healthCheckId) {
        ListHealthChecksRequest request = new ListHealthChecksRequest().withId(healthCheckId);
        Function<Void, ListHealthChecksResponse> task = (Void v) -> client.listHealthChecks(request);
        return execute(task);
    }

    // 删除终端节点组
    private static DeleteEndpointGroupResponse deleteEndpointGroup(GaClient client, String endpointGroupId) {
        DeleteEndpointGroupRequest request = new DeleteEndpointGroupRequest().withEndpointGroupId(endpointGroupId);
        Function<Void, DeleteEndpointGroupResponse> task = (Void v) -> client.deleteEndpointGroup(request);
        return execute(task);
    }

    // 查询终端节点组列表
    private static ListEndpointGroupsResponse listEndpointGroups(GaClient client, String endpointGroupId) {
        ListEndpointGroupsRequest request = new ListEndpointGroupsRequest().withId(endpointGroupId);
        Function<Void, ListEndpointGroupsResponse> task = (Void v) -> client.listEndpointGroups(request);
        return execute(task);
    }

    // 删除监听器
    private static DeleteListenerResponse deleteListener(GaClient client, String listenerId) {
        DeleteListenerRequest request = new DeleteListenerRequest().withListenerId(listenerId);
        Function<Void, DeleteListenerResponse> task = (Void v) -> client.deleteListener(request);
        return execute(task);
    }

    // 查询监听器列表
    private static ListListenersResponse listListeners(GaClient client, String listenerId) {
        ListListenersRequest request = new ListListenersRequest().withId(listenerId);
        Function<Void, ListListenersResponse> task = (Void v) -> client.listListeners(request);
        return execute(task);
    }

    // 删除全球加速实例
    private static DeleteAcceleratorResponse deleteAccelerator(GaClient client, String acceleratorId) {
        DeleteAcceleratorRequest request = new DeleteAcceleratorRequest().withAcceleratorId(acceleratorId);
        Function<Void, DeleteAcceleratorResponse> task = (Void v) -> client.deleteAccelerator(request);
        return execute(task);
    }

    // 查询全球加速实例列表
    private static ListAcceleratorsResponse listAccelerators(GaClient client, String acceleratorId) {
        ListAcceleratorsRequest request = new ListAcceleratorsRequest().withId(acceleratorId);
        Function<Void, ListAcceleratorsResponse> task = (Void v) -> client.listAccelerators(request);
        return execute(task);
    }

    private static <T> T execute(Function<Void, T> task) {
        T response = null;
        try {
            response = task.apply(null);
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
        return response;
    }
}
```
您可以在 [全球加速服务文档](https://support.huaweicloud.com/productdesc-ga/ga_01_0001.html) 和[API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=GA&api=CreateAccelerator) 查看具体信息。

### 修订记录

发布日期  | 文档版本 | 修订说明
 :----: | :-----: | :------:  
2022/11/17 |1.0 | 文档首次发布