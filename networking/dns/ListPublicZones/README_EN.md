### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/DNS/doc?api=ListPublicZones).
2.Querying the Public Zone List

3.This API is used to query the public zone list.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The current account has the permission to use DNS Kafka.

6.An instance has been created in DNS.

7.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-dns. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-dns</artifactId>
    <version>3.1.124</version>
</dependency>
```

### Sample Code

``` java
package com.huawei.dns;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.dns.v2.DnsClient;
import com.huaweicloud.sdk.dns.v2.model.ListPublicZonesRequest;
import com.huaweicloud.sdk.dns.v2.model.ListPublicZonesResponse;
import com.huaweicloud.sdk.dns.v2.region.DnsRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ListPublicZones {
    private static final Logger logger = LoggerFactory.getLogger(ListPublicZones.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to DnsRegion.AP_SOUTHEAST_3.
        DnsClient client = DnsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(DnsRegion.AP_SOUTHEAST_3)
                .build();

        // Build Request
        ListPublicZonesRequest request = new ListPublicZonesRequest();

        try {
            // Send a request
            ListPublicZonesResponse response = client.listPublicZones(request);
            // Print the response result.
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}
```

### Example of the returned result
```
{
  "links" : {
    "self" : "https://Endpoint/v2/zones?type=public&limit=10",
    "next" : "https://Endpoint/v2/zones?type=public&limit=10&marker=2c9eb155587194e*****************"
  },
  "zones" : [ {
    "id" : "2c9eb1555871************************",
    "name" : "example.com.",
    "description" : "This is an example zone.",
    "email" : "xx@example.com",
    "ttl" : 300,
    "serial" : 0,
    "masters" : [ ],
    "status" : "ACTIVE",
    "links" : {
      "self" : "https://Endpoint/v2/zones/2c9eb155587194e*****************"
    },
    "pool_id" : "00000000570e************************",
    "project_id" : "e55c6f3dc4e34c9************************",
    "zone_type" : "public",
    "created_at" : "2016-11-17T11:56:03.439",
    "updated_at" : "2016-11-17T11:56:05.528",
    "record_num" : 2
  }, {
    "id" : "2c9eb155587228************************",
    "name" : "example.org.",
    "description" : "This is an example zone.",
    "email" : "xx@example.org",
    "ttl" : 300,
    "serial" : 0,
    "masters" : [ ],
    "status" : "PENDING_CREATE",
    "links" : {
      "self" : "https://Endpoint/v2/zones/2c9eb1555872285*****************"
    },
    "pool_id" : "00000000570e54e************************",
    "project_id" : "e55c6f3dc4e34c************************",
    "zone_type" : "public",
    "created_at" : "2016-11-17T12:01:17.996",
    "updated_at" : "2016-11-17T12:01:18.528",
    "record_num" : 2
  } ],
  "metadata" : {
    "total_count" : 2
  }
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/09/26 | 1.0 | This document is released for the first time.