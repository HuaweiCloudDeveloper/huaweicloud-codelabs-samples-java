package com.huawei.dns;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.dns.v2.model.CreateRecordSetRequestBody;
import com.huaweicloud.sdk.dns.v2.model.CreateRecordSetResponse;
import com.huaweicloud.sdk.dns.v2.model.CreateRecordSetWithLineRequestBody;
import com.huaweicloud.sdk.dns.v2.model.CreateRecordSetWithLineResponse;
import com.huaweicloud.sdk.dns.v2.model.SetRecordSetsStatusReq;
import com.huaweicloud.sdk.dns.v2.model.SetRecordSetsStatusResponse;
import com.huaweicloud.sdk.dns.v2.model.UpdateRecordSetReq;
import com.huaweicloud.sdk.dns.v2.model.UpdateRecordSetResponse;
import com.huaweicloud.sdk.dns.v2.region.DnsRegion;
import com.huaweicloud.sdk.dns.v2.DnsClient;
import com.huaweicloud.sdk.dns.v2.model.CreateRecordSetRequest;
import com.huaweicloud.sdk.dns.v2.model.CreateRecordSetWithLineRequest;
import com.huaweicloud.sdk.dns.v2.model.UpdateRecordSetRequest;
import com.huaweicloud.sdk.dns.v2.model.SetRecordSetsStatusRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class DnsCodeLabsDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(DnsCodeLabsDemo.class.getName());
    public static void main(String[] args) {
        // Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);

        DnsClient client = DnsClient.newBuilder()
            .withCredential(auth)
            .withRegion(DnsRegion.valueOf("cn-north-4"))
            .build();

        // - Create a record set without resolution lines and weights.
        List<String> listbodyRecords = new ArrayList<>();
        CreateRecordSetRequest createRecordSetRequest = initCreateRecordSetRequestBody("<zoneID>", "<description>",
            "<A、AAAA、MX、CNAME、TXT、NS、SRV、CAA>", listbodyRecords, "<name>");
        createRecordSet(createRecordSetRequest, client);

        // - Create a record set with resolution lines and weights.
        List<String> listbodyRecordsWithLine = new ArrayList<>();
        CreateRecordSetWithLineRequest createRecordSetWithLineRequest = initCreateRecordSetWithLineRequestBody("<zoneID>", "<description>", "<A、AAAA、MX、CNAME、TXT、NS、SRV、CAA>", "<ENABLE/DISABLE,default:ENABLE>",
            listbodyRecordsWithLine, "<name>", "<line ID>", 1);
        createRecordSetWithLine(createRecordSetWithLineRequest, client);

        // - Modify a record set.
        List<String> listModifyRecords = new ArrayList<>();
        UpdateRecordSetRequest updateRecordSetRequest = initUpdateRecordSetRequestBody("<zoneID>", "<recordsetId>",
            "<A、AAAA、MX、CNAME、TXT、NS、SRV、CAA>", "<description>", 1, listModifyRecords, "<name>");
        updateRecordSet(updateRecordSetRequest, client);

        // - Set the status of a record set.
        SetRecordSetsStatusRequest setRecordSetsStatusRequest = initSetStatusRequestBody("<your recordsetId>",
            "<ENABLE/DISABLE,default:ENABLE>");
        setRecordSetsStatus(setRecordSetsStatusRequest, client);

    }

    public static CreateRecordSetRequest initCreateRecordSetRequestBody(String zoneId, String description, String type,
        List<String> listbodyRecords, String name) {
        CreateRecordSetRequest request = new CreateRecordSetRequest();
        request.withZoneId(zoneId);
        CreateRecordSetRequestBody body = new CreateRecordSetRequestBody();
        body.withRecords(listbodyRecords);
        body.withType(type);
        body.withDescription(description);
        body.withName(name);
        request.withBody(body);
        return request;
    }

    // Create a record set without resolution lines and weights.
    public static void createRecordSet(CreateRecordSetRequest request, DnsClient client) {
        try {
            CreateRecordSetResponse response = client.createRecordSet(request);
            System.out.println(response.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(String.valueOf(e.getErrorMsg()));
            display(e);
        }
    }

    public static CreateRecordSetWithLineRequest initCreateRecordSetWithLineRequestBody(String zoneId, String description, String type, String status,
        List<String> listbodyRecords, String name, String line, Integer weight) {
        CreateRecordSetWithLineRequest request = new CreateRecordSetWithLineRequest();
        request.withZoneId(zoneId);
        CreateRecordSetWithLineRequestBody body = new CreateRecordSetWithLineRequestBody();
        body.withWeight(weight);
        body.withRecords(listbodyRecords);
        body.withStatus(status);
        body.withType(type);
        body.withName(name);
        request.withBody(body);
        return request;
    }

    // Create a record set with resolution lines and weights.
    public static void createRecordSetWithLine(CreateRecordSetWithLineRequest request, DnsClient client) {
        try {
            CreateRecordSetWithLineResponse response = client.createRecordSetWithLine(request);
            System.out.println(response.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(String.valueOf(e.getErrorMsg()));
            display(e);
        }
    }

    public static UpdateRecordSetRequest initUpdateRecordSetRequestBody(String zoneId, String recordsetId, String type,
        String description, Integer ttl, List<String> listbodyRecords, String name) {
        UpdateRecordSetRequest request = new UpdateRecordSetRequest();
        request.withZoneId(zoneId);
        request.withRecordsetId(recordsetId);
        UpdateRecordSetReq body = new UpdateRecordSetReq();
        body.withRecords(listbodyRecords);
        body.withTtl(ttl);
        body.withType(type);
        body.withDescription(description);
        body.withName(name);
        request.withBody(body);
        return request;
    }

    // Modify a record set.
    public static void updateRecordSet(UpdateRecordSetRequest request, DnsClient client) {
        try {
            UpdateRecordSetResponse response = client.updateRecordSet(request);
            System.out.println(response.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(String.valueOf(e.getErrorMsg()));
            display(e);
        }
    }

    public static SetRecordSetsStatusRequest initSetStatusRequestBody(String recordsetId, String status) {
        SetRecordSetsStatusRequest request = new SetRecordSetsStatusRequest();
        request.withRecordsetId(recordsetId);
        SetRecordSetsStatusReq body = new SetRecordSetsStatusReq();
        body.withStatus(status);
        request.withBody(body);
        return request;
    }

    // Set the status of a record set.
    public static void setRecordSetsStatus(SetRecordSetsStatusRequest request, DnsClient client) {
        try {
            SetRecordSetsStatusResponse response = client.setRecordSetsStatus(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            LOGGER.error(e.toString());
        } catch (RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(String.valueOf(e.getErrorMsg()));
            display(e);
        }
    }

    public static void display(ServiceResponseException e) {
        System.out.println(e.getHttpStatusCode());
        System.out.println(e.getRequestId());
        System.out.println(e.getErrorCode());
        System.out.println(e.getErrorMsg());
    }
}