## 1. Version Description
This sample is developed based on Huawei Cloud SDK V3.0.

## 2. Feature Description
Huawei Cloud provides an SDK for Domain Name Service (DNS). You can integrate the SDK to perform operations on DNS by calling DNS APIs.
This sample shows how you can use the SDK to create a record set without resolution lines and weights, create a record set with resolution lines and weights, modify a record set, and set the record set status.

## 3. Prerequisites
- You have obtained the Huawei Cloud SDK. You can also install the Java SDK.
- You have a Huawei Cloud account and the AK and SK of the account. (On the Huawei Cloud management console, hover the cursor over your account name and select **My Credentials**. In the navigation pane on the left, choose **Access Keys** and view your AK and SK. If you do not have the AK and SK, create them.) For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- The Java version is 1.8 or later.

## 4. Installing the SDK
You can obtain and install the SDK using Maven. You only need to add the corresponding dependency items to the pom.xml file of the Java project.

Obtain the SDK version from [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependencies>
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-dns</artifactId>
        <version>3.0.66</version>
    </dependency>
</dependencies>
```

## 5. Running the Code
Sample code
```java
public class DnsCodeLabsDemo {
    public static void main(String[] args) {
        // Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);

        DnsClient client = DnsClient.newBuilder()
                .withCredential(auth)
                .withRegion(DnsRegion.valueOf("cn-north-4"))
                .build();
        // - Create a record set without resolution lines and weights.
        CreateRecordSetRequest createRecordSetRequest = initCreateRecordSetRequestBody("<zoneID>", "<description>",
                "<A, AAAA, MX, CNAME, TXT, NS, SRV, CAA>", "<listbodyRecords>", "<name>");
        createRecordSet(createRecordSetRequest);

        // - Create a record set with resolution lines and weights.
        CreateRecordSetWithLineRequest createRecordSetWithLineRequest = initCreateRecordSetWithLineRequestBody(
                "<zoneID>", "<description>", "<A, AAAA, MX, CNAME, TXT, NS, SRV, CAA>", "<ENABLE/DISABLE,default:ENABLE>",
                "<name>", "<line ID>", "<weight>");
        createRecordSetWithLine(createRecordSetWithLineRequest);

        // - Modify a record set.
        UpdateRecordSetRequest updateRecordSetRequest = initUpdateRecordSetRequestBody("<zoneID>", "<recordsetId>",
                "<A, AAAA, MX, CNAME, TXT, NS, SRV, CAA>", "<description>", "<ttl>", "<list record>", "<name>");
        updateRecordSet(updateRecordSetRequest);

        // - Set the status of a record set.
        SetRecordSetsStatusRequest setRecordSetsStatusRequest = initSetStatusRequestBody("<your recordsetId>",
                "<ENABLE/DISABLE,default:ENABLE>");
        setRecordSetsStatus(setRecordSetsStatusRequest);

    }
}
```

## 6. Example Responses
- Response to the request for creating a record set without a resolution line and a weight
```json
{
  "id" : "2c9eb155587228570158722b6ac30007",
  "name" : "www.example.com.",
  "description" : "This is an example record set.",
  "type" : "A",
  "ttl" : 300,
  "records" : [ "192.168.10.1", "192.168.10.2" ],
  "status" : "PENDING_CREATE",
  "links" : {
    "self" : "https://Endpoint/v2/zones/2c9eb155587194ec01587224c9f90149/recordsets/2c9eb155587228570158722b6ac30007"
  },
  "zone_id" : "2c9eb155587194ec01587224c9f90149",
  "zone_name" : "example.com.",
  "create_at" : "2016-11-17T12:03:17.827",
  "update_at" : null,
  "default" : false,
  "project_id" : "e55c6f3dc4e34c9f86353b664ae0e70c"
}
```
- Response to the request for creating a record set with a resolution line and a weight
```json
{
  "id" : "2c9eb155587228570158722b6ac30007",
  "name" : "www.example.com.",
  "description" : "This is an example record set.",
  "type" : "A",
  "ttl" : 300,
  "records" : [ "192.168.10.1", "192.168.10.2" ],
  "status" : "PENDING_CREATE",
  "links" : {
    "self" : "https://Endpoint/v2.1/zones/2c9eb155587194ec01587224c9f90149/recordsets/2c9eb155587228570158722b6ac30007"
  },
  "zone_id" : "2c9eb155587194ec01587224c9f90149",
  "zone_name" : "example.com.",
  "created_at" : "2016-11-17T12:03:17.827",
  "updated_at" : null,
  "default" : false,
  "project_id" : "e55c6f3dc4e34c9f86353b664ae0e70c",
  "line" : "default_view",
  "weight" : 1,
  "health_check_id" : null
}
```
- Response to the request for modifying a record set
```json
{
  "id" : "2c9eb155587228570158722b6ac30007",
  "name" : "www.example.com.",
  "description" : "This is an example record set.",
  "type" : "A",
  "ttl" : 3600,
  "records" : [ "192.168.10.1", "192.168.10.2" ],
  "status" : "PENDING_UPDATE",
  "links" : {
    "self" : "https://Endpoint/v2/zones/2c9eb155587194ec01587224c9f90149/recordsets/2c9eb155587228570158722b6ac30007"
  },
  "zone_id" : "2c9eb155587194ec01587224c9f90149",
  "zone_name" : "example.com.",
  "create_at" : "2016-11-17T12:03:17.827",
  "update_at" : "2016-11-17T12:56:03.827",
  "default" : false,
  "project_id" : "e55c6f3dc4e34c9f86353b664ae0e70c"
}
```
- Response to the request for setting the status of a record set
```json
{
  "id" : "2c9eb155587228570158722b6ac30007",
  "name" : "www.example.com.",
  "description" : "This is an example record set.",
  "type" : "A",
  "ttl" : 3600,
  "records" : [ "192.168.10.1", "192.168.10.2" ],
  "status" : "DISABLE",
  "links" : {
    "self" : "https://Endpoint/v2.1/zones/2c9eb155587194ec01587224c9f90149/recordsets/2c9eb155587228570158722b6ac30007"
  },
  "zone_id" : "2c9eb155587194ec01587224c9f90149",
  "zone_name" : "example.com.",
  "created_at" : "2017-11-09T11:13:17.827",
  "updated_at" : "2017-11-10T12:03:18.827",
  "default" : false,
  "project_id" : "e55c6f3dc4e34c9f86353b664ae0e70c",
  "line" : "default_view",
  "weight" : 1,
  "health_check_id" : null
}
```

## 7. APIs and Parameters
- [Creating a Record Set](https://support.huaweicloud.com/intl/en-us/api-dns/dns_api_64001.html)
- [Creating a Record Set](https://support.huaweicloud.com/intl/en-us/api-dns/CreateRecordSetWithLine.html)
- [Modifying a Record Set](https://support.huaweicloud.com/intl/en-us/api-dns/UpdateRecordSet.html)
- [Setting the Status of a Record Set](https://support.huaweicloud.com/intl/en-us/api-dns/SetRecordSetsStatus.html)

## 8. Reference
For more information, see [DNS APIs](https://support.huaweicloud.com/intl/en-us/api-dns/dns_api_64001.html).

## 9. Change History
|  Release Date | Issue|   Description  |
| :--------: | :------: | :----------: |
| 2024-1-30|   1.0    | This issue is the first official release.|
