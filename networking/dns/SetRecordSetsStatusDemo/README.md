## 1. 版本说明
本示例基于华为云SDK V3.0版本开发。

## 2. 功能介绍
华为云提供了DNS云解析服务的SDK，您可以直接集成SDK来调用DNS的相关API，从而实现对DNS的快速操作。
该示例展示了如何通过java版SDK创建公网域名。

## 3. 前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 3、华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。

## 4. 安装SDK
您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependencies>
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-dns</artifactId>
        <version>3.0.66</version>
    </dependency>
</dependencies>
```

## 5. 开始使用
创建公网域名示例代码
```java
public class DnsCodeLabsDemo {
    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);

        DnsClient client = DnsClient.newBuilder()
                .withCredential(auth)
                .withRegion(DnsRegion.valueOf("cn-north-4"))
                .build();
        // 1.创建Record Set,不带权重和解析线路
        CreateRecordSetRequest createRecordSetRequest = initCreateRecordSetRequestBody("<zoneID>", "<description>",
                "<A、AAAA、MX、CNAME、TXT、NS、SRV、CAA>", "<listbodyRecords>", "<name>");
        createRecordSet(createRecordSetRequest);

        // 2. 创建单个Record Set, 带解析线路和权重
        CreateRecordSetWithLineRequest createRecordSetWithLineRequest = initCreateRecordSetWithLineRequestBody(
                "<zoneID>", "<description>", "<A、AAAA、MX、CNAME、TXT、NS、SRV、CAA>", "<ENABLE/DISABLE,default:ENABLE>",
                "<name>", "<line ID>", "<weight>");
        createRecordSetWithLine(createRecordSetWithLineRequest);

        // 3. 修改单个Record Set
        UpdateRecordSetRequest updateRecordSetRequest = initUpdateRecordSetRequestBody("<zoneID>", "<recordsetId>",
                "<A、AAAA、MX、CNAME、TXT、NS、SRV、CAA>", "<description>", "<ttl>", "<list record>", "<name>");
        updateRecordSet(updateRecordSetRequest);

        // 4. 设置Record Set状态
        SetRecordSetsStatusRequest setRecordSetsStatusRequest = initSetStatusRequestBody("<your recordsetId>",
                "<ENABLE/DISABLE,default:ENABLE>");
        setRecordSetsStatus(setRecordSetsStatusRequest);

    }
}
```

## 6. 返回示例
- 创建单个Record Set 响应
```json
{
  "id" : "2c9eb155587228570158722b6ac30007",
  "name" : "www.example.com.",
  "description" : "This is an example record set.",
  "type" : "A",
  "ttl" : 300,
  "records" : [ "192.168.10.1", "192.168.10.2" ],
  "status" : "PENDING_CREATE",
  "links" : {
    "self" : "https://Endpoint/v2/zones/2c9eb155587194ec01587224c9f90149/recordsets/2c9eb155587228570158722b6ac30007"
  },
  "zone_id" : "2c9eb155587194ec01587224c9f90149",
  "zone_name" : "example.com.",
  "create_at" : "2016-11-17T12:03:17.827",
  "update_at" : null,
  "default" : false,
  "project_id" : "e55c6f3dc4e34c9f86353b664ae0e70c"
}
```
- 创建单个Record Set响应。
```json
{
  "id" : "2c9eb155587228570158722b6ac30007",
  "name" : "www.example.com.",
  "description" : "This is an example record set.",
  "type" : "A",
  "ttl" : 300,
  "records" : [ "192.168.10.1", "192.168.10.2" ],
  "status" : "PENDING_CREATE",
  "links" : {
    "self" : "https://Endpoint/v2.1/zones/2c9eb155587194ec01587224c9f90149/recordsets/2c9eb155587228570158722b6ac30007"
  },
  "zone_id" : "2c9eb155587194ec01587224c9f90149",
  "zone_name" : "example.com.",
  "created_at" : "2016-11-17T12:03:17.827",
  "updated_at" : null,
  "default" : false,
  "project_id" : "e55c6f3dc4e34c9f86353b664ae0e70c",
  "line" : "default_view",
  "weight" : 1,
  "health_check_id" : null
}
```
- 修改单个Record Set响应。
```json
{
  "id" : "2c9eb155587228570158722b6ac30007",
  "name" : "www.example.com.",
  "description" : "This is an example record set.",
  "type" : "A",
  "ttl" : 3600,
  "records" : [ "192.168.10.1", "192.168.10.2" ],
  "status" : "PENDING_UPDATE",
  "links" : {
    "self" : "https://Endpoint/v2.1/zones/2c9eb155587194ec01587224c9f90149/recordsets/2c9eb155587228570158722b6ac30007"
  },
  "zone_id" : "2c9eb155587194ec01587224c9f90149",
  "zone_name" : "example.com.",
  "created_at" : "2016-11-17T12:03:17.827",
  "updated_at" : "2016-11-17T12:56:06.439",
  "default" : false,
  "project_id" : "e55c6f3dc4e34c9f86353b664ae0e70c",
  "line" : "default_view",
  "weight" : 1,
  "health_check_id" : null
}
```
- 修改单个Record Set响应。
```json
{
  "id" : "2c9eb155587228570158722b6ac30007",
  "name" : "www.example.com.",
  "description" : "This is an example record set.",
  "type" : "A",
  "ttl" : 3600,
  "records" : [ "192.168.10.1", "192.168.10.2" ],
  "status" : "PENDING_UPDATE",
  "links" : {
    "self" : "https://Endpoint/v2/zones/2c9eb155587194ec01587224c9f90149/recordsets/2c9eb155587228570158722b6ac30007"
  },
  "zone_id" : "2c9eb155587194ec01587224c9f90149",
  "zone_name" : "example.com.",
  "create_at" : "2016-11-17T12:03:17.827",
  "update_at" : "2016-11-17T12:56:03.827",
  "default" : false,
  "project_id" : "e55c6f3dc4e34c9f86353b664ae0e70c"
}
```
- 设置Record Set状态响应。
```json
{
  "id" : "2c9eb155587228570158722b6ac30007",
  "name" : "www.example.com.",
  "description" : "This is an example record set.",
  "type" : "A",
  "ttl" : 3600,
  "records" : [ "192.168.10.1", "192.168.10.2" ],
  "status" : "DISABLE",
  "links" : {
    "self" : "https://Endpoint/v2.1/zones/2c9eb155587194ec01587224c9f90149/recordsets/2c9eb155587228570158722b6ac30007"
  },
  "zone_id" : "2c9eb155587194ec01587224c9f90149",
  "zone_name" : "example.com.",
  "created_at" : "2017-11-09T11:13:17.827",
  "updated_at" : "2017-11-10T12:03:18.827",
  "default" : false,
  "project_id" : "e55c6f3dc4e34c9f86353b664ae0e70c",
  "line" : "default_view",
  "weight" : 1,
  "health_check_id" : null
}
```

## 7.接口及参数说明
- 参见：[创建单个Record Set](https://support.huaweicloud.com/api-dns/dns_api_64001.html)
- 参见：[创建单个Record Set,带有解析线路和权重](https://support.huaweicloud.com/api-dns/CreateRecordSetWithLine.html)
- 参见：[修改单个Record Set](https://support.huaweicloud.com/api-dns/UpdateRecordSet.html)
- 参见：[设置Record Set状态](https://support.huaweicloud.com/api-dns/SetRecordSetsStatus.html)

## 8. 参考
更多示例信息请参考[DNS](https://support.huaweicloud.com/api-dns/dns_api_64001.html)

## 9. 修订记录
|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2021-10-22 |   1.0    | 文档首次发布 |
| 2024-01-30 |   1.0    | 文档内容订正 |
