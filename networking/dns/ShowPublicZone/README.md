### 产品介绍

1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/DNS/doc?api=ShowPublicZone)
中直接运行调试该接口。

2.查询单个公网Zone

3.当需要查询单个公网Zone时，可以调用此接口达到预期。

### 前置条件

1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.当前登录账号拥有使用DNS的权限。

6.已在DNS创建实例。

7.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-dns”，具体的SDK版本号请参见 [SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。

```xml
 <dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-dns</artifactId>
    <version>3.1.124</version>
</dependency>
```

### 代码示例

``` java
package com.huawei.dns;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.dns.v2.DnsClient;
import com.huaweicloud.sdk.dns.v2.model.ShowPublicZoneRequest;
import com.huaweicloud.sdk.dns.v2.model.ShowPublicZoneResponse;
import com.huaweicloud.sdk.dns.v2.region.DnsRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ShowPublicZone {
    private static final Logger logger = LoggerFactory.getLogger(ShowPublicZone.class.getName());

    public static void main(String[] args) {

        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为DnsRegion.CN_NORTH_4
        DnsClient client = DnsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(httpConfig)
                .withRegion(DnsRegion.CN_NORTH_4)
                .build();

        // 构建请求
        ShowPublicZoneRequest request = new ShowPublicZoneRequest();
        request.withZoneId("<YOUR ZONE ID>");

        try {
            // 发送请求
            ShowPublicZoneResponse response = client.showPublicZone(request);
            // 打印响应结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        }

    }
}
```

### 返回结果示例

```
{
  "id" : "2c9eb15558719**********************",
  "name" : "example.com.",
  "description" : "This is an example zone.",
  "email" : "xx@example.com",
  "ttl" : 300,
  "serial" : 0,
  "masters" : [ ],
  "status" : "ACTIVE",
  "links" : {
    "self" : "https://Endpoint/v2/zones/2c9eb1555871************************"
  },
  "pool_id" : "00000000570e54e********************",
  "project_id" : "e55c6f3dc4e34c********************",
  "zone_type" : "public",
  "created_at" : "2016-11-17T11:56:03.439",
  "updated_at" : "2016-11-17T11:56:05.528",
  "record_num" : 2,
  "enterprise_project_id" : "0"
}
```

### 修订记录

    发布日期    | 文档版本 | 修订说明

:----------:|:----:| :------:  
2024/09/10 | 1.0 | 文档首次发布