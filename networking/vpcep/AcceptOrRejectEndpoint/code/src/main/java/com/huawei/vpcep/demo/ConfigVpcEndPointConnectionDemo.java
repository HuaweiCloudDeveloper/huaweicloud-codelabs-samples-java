package com.huawei.vpcep.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.vpcep.v1.VpcepClient;
import com.huaweicloud.sdk.vpcep.v1.region.VpcepRegion;
import com.huaweicloud.sdk.vpcep.v1.model.AcceptOrRejectEndpointRequest;
import com.huaweicloud.sdk.vpcep.v1.model.AcceptOrRejectEndpointRequestBody;
import com.huaweicloud.sdk.vpcep.v1.model.AcceptOrRejectEndpointResponse;
import com.huaweicloud.sdk.vpcep.v1.model.*;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConfigVpcEndPointConnectionDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigVpcEndPointConnectionDemo.class.getName());

    public static void main(String[] args) {
        // There will be security risks if the AK/SK used for authentication is directly written into code. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        // HTTP client configuration
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);
        VpcepClient client = VpcepClient.newBuilder()
                .withCredential(auth)
                .withRegion(VpcepRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();

        ConfigVpcEndPointConnectionDemo demo = new ConfigVpcEndPointConnectionDemo();

        // 1. Creating a VPC endpoint service
        CreateEndpointServiceResponse createEndpointServiceResponse = demo.createEndpointServiceResponse(client);
        String epsId = createEndpointServiceResponse.getId();

        // 2. Querying connections of a VPC endpoint service
        demo.listServiceConnectionsResponse(client, epsId);

        // 3. Rejecting a VPC endpoint
        AcceptOrRejectEndpointRequest request = new AcceptOrRejectEndpointRequest();
        AcceptOrRejectEndpointRequestBody body = new AcceptOrRejectEndpointRequestBody();
        body.setAction(AcceptOrRejectEndpointRequestBody.ActionEnum.fromValue("reject"));
        body.setEndpoints(Arrays.asList("{vpc_endpoint_id}")); // VPC endpoint ID
        request.setVpcEndpointServiceId(epsId); // VPC endpoint service ID
        request.withBody(body);

        try {
            AcceptOrRejectEndpointResponse response = client.acceptOrRejectEndpoint(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(String.valueOf(e.getErrorCode()));
            LOGGER.error(String.valueOf(e.getErrorMsg()));
            LOGGER.error(e.toString());
        }
    }

    private void listServiceConnectionsResponse(VpcepClient client, String vpcEndpointServiceId) {
        ListServiceConnectionsRequest request = new ListServiceConnectionsRequest();
        request.withVpcEndpointServiceId(vpcEndpointServiceId);
        try {
            ListServiceConnectionsResponse response = client.listServiceConnections(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(String.valueOf(e.getErrorMsg()));
            LOGGER.error(e.toString());
        }
    }

    private CreateEndpointServiceResponse createEndpointServiceResponse(VpcepClient client) {
        CreateEndpointServiceRequest request = new CreateEndpointServiceRequest();
        CreateEndpointServiceRequestBody body = new CreateEndpointServiceRequestBody();
        body.setPortId("{YOUR VM_PORT_ID}");
        body.setVpcId("{YOUR VPC_ID}");
        body.setApprovalEnabled(false);
        body.setServiceType("interface");
        body.setServerType(CreateEndpointServiceRequestBody.ServerTypeEnum.VM);

        List<PortList> ports = new ArrayList<>();
        PortList port = new PortList();
        port.setClientPort(8080);
        port.setServerPort(90);
        port.setProtocol(PortList.ProtocolEnum.TCP);
        ports.add(port);

        body.setPorts(ports);

        request.withBody(body);
        CreateEndpointServiceResponse response = null;
        try {
            response = client.createEndpointService(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(String.valueOf(e.getErrorCode()));
            LOGGER.error(String.valueOf(e.getErrorMsg()));
        }
        return response;
    }
}

