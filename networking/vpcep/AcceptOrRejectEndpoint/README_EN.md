## 1. Sample Overview

Huawei Cloud provides an SDK for VPCEP. You can integrate the SDK to perform operations on VPCEP by calling VPCEP APIs. This sample shows how to use the Java SDK to reject a VPC endpoint.

## 2. Prerequisites
- You have obtained the Huawei Cloud SDK. You can also install the Java SDK.
- To use the Huawei Cloud Java SDK, you must have a Huawei Cloud account and obtain the AK and SK of the account. For details, see [Obtaining an AK/SK](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- The Java version is 1.8 or later.

## 3. Obtaining and Installing the SDK
You can use Maven to configure the VPCEP SDK. 
```xml
<dependencies>
   <dependency>
      <groupId>com.huaweicloud.sdk</groupId>
      <artifactId>huaweicloud-sdk-vpcep</artifactId>
      <version>3.1.60</version>
   </dependency>
</dependencies>
```

## 4. Key Code Snippets
```java
 public class ConfigVpcEndPointConnectionDemo {
   private static final Logger LOGGER = LoggerFactory.getLogger(ConfigVpcEndPointConnectionDemo.class.getName());

   public static void main(String[] args) {
      //Enter the AK and SK of your Huawei Cloud account.
      // There will be security risks if the AK/SK used for authentication is directly written into code. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
      // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
      String ak = System.getenv("HUAWEICLOUD_SDK_AK");
      String sk = System.getenv("HUAWEICLOUD_SDK_SK");

      ICredential auth = new BasicCredentials()
              .withAk(ak)
              .withSk(sk);
      // HTTP client configuration
      HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);
      VpcepClient client = VpcepClient.newBuilder()
              .withCredential(auth)
              .withRegion(VpcepRegion.CN_NORTH_4)
              .withHttpConfig(httpConfig)
              .build();

     1. Creating a VPC endpoint service
     CreateEndpointServiceResponse createEndpointServiceResponse = demo.createEndpointServiceResponse(client);
     String epsId = createEndpointServiceResponse.getId();

     // 2. Querying connections of a VPC endpoint service
     demo.listServiceConnectionsResponse(client, epsId);

     // 3. Rejecting a VPC endpoint
     AcceptOrRejectEndpointRequest request = new AcceptOrRejectEndpointRequest();
     AcceptOrRejectEndpointRequestBody body = new AcceptOrRejectEndpointRequestBody();
     body.setAction(AcceptOrRejectEndpointRequestBody.ActionEnum.fromValue("reject"));
     body.setEndpoints(Arrays.asList("{vpc_endpoint_id}")); // VPC endpoint ID
     request.setVpcEndpointServiceId(epsId); // VPC endpoint service ID
     request.withBody(body);
   }
}
```

## 5. Example Responses
#### 5.1 Creating a VPC Endpoint Service
```java
class CreateEndpointServiceResponse {
  id: 4100a4b8-538b-4064-92e7-f0ecdcbe426b
  portId: 4fdf7a47-2c96-4a2a-880b-e05864e59f77
  serviceName: cn-north-4.4100a4b8-538b-4064-92e7-f0ecdcbe426b
  serverType: VM
  vpcId: 4cbf8757-86d1-459a-a7db-0fac9c1f679f
  poolId: c3e4bd8f-4c17-4c65-9401-5cb71fc169fe
  approvalEnabled: false
  status: creating
  serviceType: interface
  createdAt: 2023-10-23T12:36:59Z
  updatedAt: 2023-10-23T12:36:59Z
  projectId: 0df25bbc87****2f88c00c2959df9a
  ports: [class PortList {
    clientPort: 8080
    serverPort: 90
    protocol: TCP
  }]
  tcpProxy: close
  tags: []
  description:
  enablePolicy: false
}
```
#### 5.2 Querying Connections of a VPC Endpoint Service
```java
class ListserviceconnectionsResponse {
    connections:[

    class ConnectionEndpoints {
        id:4ff28ba7-2fdf-424b-a3a5-12d04ddb6a2e
        markerId:201366875
        createdAt:2023-10-23TO7:17:22Z
        UpdatedAt:2023-10-23TO7:17:22Z
        domainId:9ab3fc2
        error:null
        status:pendingAcceptance
        description:
    }]
    totalCount:1
}
```
#### 5.3 Rejecting a VPC Endpoint
```java
class AcceptOrRejectEndpointResponse {
    connections:[

    class ConnectionEndpoints {
        id:4ff28ba7-2fdf-424b-a3a5-12d04ddb6a2e
        markerId:201366875
        createdAt:2023-10-23TO7:17:22Z
        updatedAt:2023-10-23T07:31:47Z
        domainId:4b198ca2d***1fc3f0ab3fc2
        error:null
        status:rejected
        description:
    }]
}
```


## 6. References
- API reference
  - [Accepting or Rejecting a VPC Endpoint](https://support.huaweicloud.com/intl/en-us/api-vpcep/AcceptOrRejectEndpoint.html)
  - [Querying Connections of a VPC Endpoint Service](https://support.huaweicloud.com/intl/en-us/api-vpcep/vpcep_06_0206.html)
  - [Creating a VPC Endpoint Service](https://support.huaweicloud.com/intl/en-us/api-vpcep/vpcep_06_0201.html)
- SDK reference
  [VPCEP JAVA SDK](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/VPCEP?lang=Java)
- AK and SK
  [Obtaining AK/SK](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html)

## 7. Change History

|    Release Date   | Issue|   Description  |
|:----------:| :------: | :----------: |
| 2023-09-28 |   1.0    | This issue is the first official release.|
