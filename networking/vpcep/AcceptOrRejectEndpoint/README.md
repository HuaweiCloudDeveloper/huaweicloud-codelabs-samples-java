## 1、示例简介

华为云提供了VPC终端节点服务（VPCEP）的SDK，您可以直接集成SDK来调用VPCEP的相关API，从而实现对VPCEP的快速操作。 该示例展示了如何通过java版SDK拒绝终端节点的连接。

## 2、前置条件
- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。具体请参见[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)。
- 华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。

## 3、SDK获取和安装
您可以通过Maven配置所依赖的虚拟私有云服务SDK
```xml
<dependencies>
   <dependency>
      <groupId>com.huaweicloud.sdk</groupId>
      <artifactId>huaweicloud-sdk-vpcep</artifactId>
      <version>3.1.60</version>
   </dependency>
</dependencies>
```

## 4、关键代码片段
```java
 public class ConfigVpcEndPointConnectionDemo {
   private static final Logger LOGGER = LoggerFactory.getLogger(ConfigVpcEndPointConnectionDemo.class.getName());

   public static void main(String[] args) {
      // 输入华为云账号的AK、SK
      // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
      // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
      String ak = System.getenv("HUAWEICLOUD_SDK_AK");
      String sk = System.getenv("HUAWEICLOUD_SDK_SK");

      ICredential auth = new BasicCredentials()
              .withAk(ak)
              .withSk(sk);
      // HTTP客户端配置
      HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig().withIgnoreSSLVerification(true);
      VpcepClient client = VpcepClient.newBuilder()
              .withCredential(auth)
              .withRegion(VpcepRegion.CN_NORTH_4)
              .withHttpConfig(httpConfig)
              .build();

     // 1.创建终端节点服务
     CreateEndpointServiceResponse createEndpointServiceResponse = demo.createEndpointServiceResponse(client);
     String epsId = createEndpointServiceResponse.getId();

     // 2.查询连接终端节点服务的连接列表
     demo.listServiceConnectionsResponse(client, epsId);

     // 3.拒绝终端节点连接
     AcceptOrRejectEndpointRequest request = new AcceptOrRejectEndpointRequest();
     AcceptOrRejectEndpointRequestBody body = new AcceptOrRejectEndpointRequestBody();
     body.setAction(AcceptOrRejectEndpointRequestBody.ActionEnum.fromValue("reject"));
     body.setEndpoints(Arrays.asList("{vpc_endpoint_id}")); // 终端节点ID
     request.setVpcEndpointServiceId(epsId); // 终端节点服务ID
     request.withBody(body);
   }
}
```

## 5、返回示例
#### 5.1 创建终端节点服务
```java
class CreateEndpointServiceResponse {
  id: 4100a4b8-538b-4064-92e7-f0ecdcbe426b
  portId: 4fdf7a47-2c96-4a2a-880b-e05864e59f77
  serviceName: cn-north-4.4100a4b8-538b-4064-92e7-f0ecdcbe426b
  serverType: VM
  vpcId: 4cbf8757-86d1-459a-a7db-0fac9c1f679f
  poolId: c3e4bd8f-4c17-4c65-9401-5cb71fc169fe
  approvalEnabled: false
  status: creating
  serviceType: interface
  createdAt: 2023-10-23T12:36:59Z
  updatedAt: 2023-10-23T12:36:59Z
  projectId: 0df25bbc87****2f88c00c2959df9a
  ports: [class PortList {
    clientPort: 8080
    serverPort: 90
    protocol: TCP
  }]
  tcpProxy: close
  tags: []
  description:
  enablePolicy: false
}
```
#### 5.2 查询连接终端节点服务的连接列表
```java
class ListserviceconnectionsResponse {
    connections:[

    class ConnectionEndpoints {
        id:4ff28ba7-2fdf-424b-a3a5-12d04ddb6a2e
        markerId:201366875
        createdAt:2023-10-23TO7:17:22Z
        UpdatedAt:2023-10-23TO7:17:22Z
        domainId:9ab3fc2
        error:null
        status:pendingAcceptance
        description:
    }]
    totalCount:1
}
```
#### 5.3 拒绝终端节点连接
```java
class AcceptOrRejectEndpointResponse {
    connections:[

    class ConnectionEndpoints {
        id:4ff28ba7-2fdf-424b-a3a5-12d04ddb6a2e
        markerId:201366875
        createdAt:2023-10-23TO7:17:22Z
        updatedAt:2023-10-23T07:31:47Z
        domainId:4b198ca2d***1fc3f0ab3fc2
        error:null
        status:rejected
        description:
    }]
}
```


## 6、参考
- API参考
  - [接受或拒绝终端节点的连接](https://support.huaweicloud.com/api-vpcep/AcceptOrRejectEndpoint.html)
  - [查询连接终端节点服务的连接列表](https://support.huaweicloud.com/api-vpcep/vpcep_06_0206.html)
  - [创建终端节点服务](https://support.huaweicloud.com/api-vpcep/vpcep_06_0201.html)
- SDK参考
  [VPCEP JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/VPCEP?lang=Java)
- AK SK获取
  [AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)

## 7、修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2023-09-28 |   1.0    | 文档首次发布 |