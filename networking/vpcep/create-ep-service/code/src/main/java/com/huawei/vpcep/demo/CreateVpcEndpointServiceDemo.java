package com.huawei.vpcep.demo;


import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.vpcep.v1.VpcepClient;
import com.huaweicloud.sdk.vpcep.v1.model.CreateEndpointServiceRequest;
import com.huaweicloud.sdk.vpcep.v1.model.CreateEndpointServiceRequestBody;
import com.huaweicloud.sdk.vpcep.v1.model.CreateEndpointServiceResponse;
import com.huaweicloud.sdk.vpcep.v1.model.PortList;
import com.huaweicloud.sdk.vpcep.v1.region.VpcepRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CreateVpcEndpointServiceDemo {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateVpcEndpointServiceDemo.class.getName());

    public static void main(String[] args) {
        // Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        VpcepClient client = VpcepClient.newBuilder()
            .withCredential(auth)
            .withRegion(VpcepRegion.valueOf("cn-north-4"))
            .build();

        CreateEndpointServiceRequest request = new CreateEndpointServiceRequest();
        CreateEndpointServiceRequestBody body = new CreateEndpointServiceRequestBody();
        body.setPortId("{YOUR VM_PORT_ID}");
        body.setVpcId("{YOUR VPC_ID}");
        body.setApprovalEnabled(false);
        body.setServiceType("interface");
        body.setServerType(CreateEndpointServiceRequestBody.ServerTypeEnum.VM);

        List<PortList> ports = new ArrayList<>();
        PortList port = new PortList();
        port.setClientPort(8080);
        port.setServerPort(90);
        port.setProtocol(PortList.ProtocolEnum.TCP);
        ports.add(port);

        body.setPorts(ports);

        request.withBody(body);
        try {
            CreateEndpointServiceResponse response = client.createEndpointService(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            System.out.println(e.getErrorMsg());
            LOGGER.error(e.toString());
        }
    }
}