## 0. Version Description
This example is developed based on Huawei Cloud SDK V3.0.
## 1. Example Overview
Huawei Cloud provides an SDK for VPCEP. You can integrate the SDK to perform operations on VPCEP by calling VPCEP APIs.
This example shows how to use the Java SDK to create a VPC endpoint service.
## 2. Prerequisites
- You have obtained the Huawei Cloud SDK. You can also install the Java SDK.
- You have a Huawei Cloud account and the AK and SK of the account (On the Huawei Cloud management console, hover the cursor over your account name and select **My Credentials**. In the navigation pane on the left, choose **Access Keys** and view your AK and SK. If you do not have the AK and SK, create them.) For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- The Java version is 1.8 or later.
## 3. Installing an SDK
You can obtain and install the SDK using Maven. You only need to add the corresponding dependency items to the **pom.xml** file of the Java project.
Obtain the SDK version from [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/VPCEP?lang=Java).
```xml
<dependencies>
   <dependency>
       <groupId>com.huaweicloud.sdk</groupId>
       <artifactId>huaweicloud-sdk-vpcep</artifactId>
       <version>3.0.67</version>
   </dependency>
</dependencies>
```
## 4. Running the Code
Sample code for creating a VPC endpoint service
```java
    public static void main(String[] args) {
        // There will be security risks if the AK/SK used for authentication is directly written into code. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        VpcepClient client = VpcepClient.newBuilder()
            .withCredential(auth)
            .withRegion(VpcepRegion.valueOf("cn-north-4"))
            .build();

        CreateEndpointServiceRequest request = new CreateEndpointServiceRequest();
        CreateEndpointServiceRequestBody body = new CreateEndpointServiceRequestBody();
        body.setPortId("{YOUR VM_PORT_ID}");
        body.setVpcId("{YOUR VPC_ID}");
        body.setApprovalEnabled(false);
        body.setServiceType("interface");
        body.setServerType(CreateEndpointServiceRequestBody.ServerTypeEnum.VM);

        List<PortList> ports = new ArrayList<>();
        PortList port = new PortList();
        port.setClientPort(8080);
        port.setServerPort(90);
        port.setProtocol(PortList.ProtocolEnum.TCP);
        ports.add(port);

        body.setPorts(ports);

        request.withBody(body);
        try {
            CreateEndpointServiceResponse response = client.createEndpointService(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(String.valueOf(e.getErrorCode()));
            LOGGER.error(String.valueOf(e.getErrorMsg()));
            LOGGER.error(e.toString());
        }
    }
```
## 5. Example Response
Creating a VPC endpoint service
```json
{
  "port_id":"4189d3c2-8882-4871-a3c2-d380272eed88",
  "vpc_id":"4189d3c2-8882-4871-a3c2-d380272eed80",
  "approval_enabled":false,
  "service_type":"interface",
  "server_type":"VM",
  "ports":
  [
    {
      "client_port":8080,
      "server_port":90,
      "protocol":"TCP"
    },
    {
      "client_port":8081,
      "server_port":80,
      "protocol":"TCP"
    }
  ]
}
```
## 6. APIs and Parameters
For details, see [Creating a VPC Endpoint Service](https://support.huaweicloud.com/intl/en-us/api-vpcep/vpcep_06_0201.html).

## 7. Change History
|  Release Date | Issue|   Description  |
| :--------: | :------: | :----------: |
| 2021-10-26 |   1.0    | This issue is the first official release.|
