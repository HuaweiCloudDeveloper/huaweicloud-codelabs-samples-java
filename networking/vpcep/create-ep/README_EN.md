## 0. Version Description
This sample is developed based on Huawei Cloud SDK V3.0.
## 1. Sample Overview
Huawei Cloud provides an SDK for VPCEP. You can integrate the SDK to perform operations on VPCEP by calling VPCEP APIs.
This sample shows how to use the Java SDK to create a VPC endpoint.
## 2. Prerequisites
- You have obtained the Huawei Cloud SDK. You can also install the Java SDK.
- You have a Huawei Cloud account and the AK and SK of the account (On the Huawei Cloud management console, hover the cursor over your account name and select **My Credentials**. In the navigation pane on the left, choose **Access Keys** and view your AK and SK. If you do not have the AK and SK, create them.) For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- The Java version is 1.8 or later.
## 3. Installing an SDK
You can obtain and install the SDK using Maven. You only need to add the corresponding dependency items to the **pom.xml** file of the Java project.
Obtain the SDK version from [SDK Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/VPCEP?lang=Java).
```xml
<dependencies>
   <dependency>
       <groupId>com.huaweicloud.sdk</groupId>
       <artifactId>huaweicloud-sdk-vpcep</artifactId>
       <version>3.0.67</version>
   </dependency>
</dependencies>
```
## 4. Running the Code
Sample code for creating a VPC endpoint
```java
    public static void main(String[] args) {
        // There will be security risks if the AK/SK used for authentication is directly written into code. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        VpcepClient client = VpcepClient.newBuilder()
            .withCredential(auth)
            .withRegion(VpcepRegion.valueOf("cn-north-4"))
            .build();

        CreateEndpointRequest request = new CreateEndpointRequest();
        CreateEndpointRequestBody body = new CreateEndpointRequestBody();
        body.setVpcId("{YOUR VPC ID}");
        body.setSubnetId("{YOUR SUBNET ID}");
        body.setEndpointServiceId("{YOUR EndPointService ID}");
        request.withBody(body);
        try {
            CreateEndpointResponse response = client.createEndpoint(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(String.valueOf(e.getErrorCode()));
            LOGGER.error(String.valueOf(e.getErrorMsg()));
            LOGGER.error(e.toString());
        }
    }
```
## 5. Example Response
Creating a VPC endpoint
```json
{
  "id": "4189d3c2-8882-4871-a3c2-d380272eed83",
  "service_type": "interface",
  "marker_id": 322312312312,
  "status": "creating",
  "vpc_id": "4189d3c2-8882-4871-a3c2-d380272eed83",
  "enable_dns": false,
  "endpoint_service_name": "test123",
  "endpoint_service_id": "test123",
  "project_id": "6e9dfd51d1124e8d8498dce894923a0d",
  "whitelist": [
    "127.0.0.1"
  ],
  "enable_whitelist": true,
  "created_at": "2018-01-30T07:42:01.174",
  "update_at": "2018-01-30T07:42:01.174",
  "tags": []
}
```
## 6. APIs and Parameters
For details, see [Creating a VPC Endpoint](https://support.huaweicloud.com/intl/en-us/api-vpcep/CreateEndpoint.html).
  
 
## 7. Change History
|  Release Date | Issue|   Description  |
| :--------: | :------: | :----------: |
| 2021-10-26 |   1.0    | This issue is the first official release.|
