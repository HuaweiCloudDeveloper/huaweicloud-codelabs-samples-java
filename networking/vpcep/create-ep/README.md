## 0. 版本说明
本示例基于华为云SDK V3.0版本开发。
## 1. 示例简介
华为云提供了VPC终端节点服务（VPCEP）的SDK，您可以直接集成SDK来调用VPCEP的相关API，从而实现对VPCEP的快速操作。
该示例展示了如何通过java版SDK创建终端节点。
## 2. 前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 3、华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。
## 3. 安装SDK
您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependencies>
   <dependency>
       <groupId>com.huaweicloud.sdk</groupId>
       <artifactId>huaweicloud-sdk-vpcep</artifactId>
       <version>3.0.67</version>
   </dependency>
</dependencies>
```
## 4.开始使用
创建终端节点示例代码
```java
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        VpcepClient client = VpcepClient.newBuilder()
            .withCredential(auth)
            .withRegion(VpcepRegion.valueOf("cn-north-4"))
            .build();

        CreateEndpointRequest request = new CreateEndpointRequest();
        CreateEndpointRequestBody body = new CreateEndpointRequestBody();
        body.setVpcId("{YOUR VPC ID}");
        body.setSubnetId("{YOUR SUBNET ID}");
        body.setEndpointServiceId("{YOUR EndPointService ID}");
        request.withBody(body);
        try {
            CreateEndpointResponse response = client.createEndpoint(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(String.valueOf(e.getErrorCode()));
            LOGGER.error(String.valueOf(e.getErrorMsg()));
            LOGGER.error(e.toString());
        }
    }
```
## 5. 返回示例
终端节点
```json
{
  "id": "4189d3c2-8882-4871-a3c2-d380272eed83",
  "service_type": "interface",
  "marker_id": 322312312312,
  "status": "creating",
  "vpc_id": "4189d3c2-8882-4871-a3c2-d380272eed83",
  "enable_dns": false,
  "endpoint_service_name": "test123",
  "endpoint_service_id": "test123",
  "project_id": "6e9dfd51d1124e8d8498dce894923a0d",
  "whitelist": [
    "127.0.0.1"
  ],
  "enable_whitelist": true,
  "created_at": "2018-01-30T07:42:01.174",
  "update_at": "2018-01-30T07:42:01.174",
  "tags": []
}
```
## 6.接口及参数说明
参见：[创建终端节点](https://support.huaweicloud.com/api-vpcep/vpcep_06_0303.html)

## 7. 修订记录
|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2021-10-26 |   1.0    | 文档首次发布 |