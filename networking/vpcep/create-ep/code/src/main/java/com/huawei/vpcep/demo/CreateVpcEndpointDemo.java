package com.huawei.vpcep.demo;


import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.vpcep.v1.VpcepClient;
import com.huaweicloud.sdk.vpcep.v1.model.CreateEndpointRequest;
import com.huaweicloud.sdk.vpcep.v1.model.CreateEndpointRequestBody;
import com.huaweicloud.sdk.vpcep.v1.model.CreateEndpointResponse;
import com.huaweicloud.sdk.vpcep.v1.region.VpcepRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateVpcEndpointDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateVpcEndpointDemo.class.getName());

    public static void main(String[] args) {
        // Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        VpcepClient client = VpcepClient.newBuilder()
            .withCredential(auth)
            .withRegion(VpcepRegion.CN_NORTH_4)
            .build();

        CreateEndpointRequest request = new CreateEndpointRequest();
        CreateEndpointRequestBody body = new CreateEndpointRequestBody();
        body.setVpcId("{YOUR VPC ID}");
        body.setSubnetId("{YOUR SUBNET ID}");
        body.setEndpointServiceId("{YOUR EndPointService ID}");
        request.withBody(body);
        try {
            CreateEndpointResponse response = client.createEndpoint(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(String.valueOf(e.getErrorMsg()));
            LOGGER.error(e.toString());
        }
    }
}