package com.huawei.vpcep.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.vpcep.v1.VpcepClient;
import com.huaweicloud.sdk.vpcep.v1.model.*;

import com.huaweicloud.sdk.vpcep.v1.region.VpcepRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class ConfigVpcEndPointServicePermissionDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigVpcEndPointServicePermissionDemo.class.getName());

    public static void main(String[] args) {
        // There will be security risks if the AK/SK used for authentication is directly written into code. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        VpcepClient client = VpcepClient.newBuilder()
                .withCredential(auth)
                .withRegion(VpcepRegion.valueOf("cn-north-4"))
                .build();

        ConfigVpcEndPointServicePermissionDemo demo = new ConfigVpcEndPointServicePermissionDemo();

        // 1. Adding whitelist records for a VPC endpoint service
        AddOrRemoveServicePermissionsRequest request = new AddOrRemoveServicePermissionsRequest();
        AddOrRemoveServicePermissionsRequestBody body = new AddOrRemoveServicePermissionsRequestBody();
        body.setAction(AddOrRemoveServicePermissionsRequestBody.ActionEnum.ADD);
        body.setPermissions(Arrays.asList("{iam:domain::domain_id}")); // Format: iam:domain::domain_id
        request.setVpcEndpointServiceId("{vpc_endpoint_service_id}");
        request.withBody(body);

        // 2. Querying whitelist records of a VPC endpoint service
        demo.listServicePermissionsDetailsResponse(client, "{vpc_endpoint_service_id}");

        try {
            AddOrRemoveServicePermissionsResponse response = client.addOrRemoveServicePermissions(request);
            LOGGER.info(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(String.valueOf(e.getErrorCode()));
            LOGGER.error(String.valueOf(e.getErrorMsg()));
        }
    }


    private void listServicePermissionsDetailsResponse(VpcepClient client, String vpcEndpointServiceId) {
        ListServicePermissionsDetailsRequest request = new ListServicePermissionsDetailsRequest();
        request.withVpcEndpointServiceId(vpcEndpointServiceId);
        try {
            ListServicePermissionsDetailsResponse response = client.listServicePermissionsDetails(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            LOGGER.error(e.toString());
        } catch (RequestTimeoutException e) {
            LOGGER.error(e.toString());
        } catch (ServiceResponseException e) {
            LOGGER.error(e.toString());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}
