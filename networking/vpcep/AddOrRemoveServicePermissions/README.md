## 1、示例简介

华为云提供了VPC终端节点服务（VPCEP）的SDK，您可以直接集成SDK来调用VPCEP的相关API，从而实现对VPCEP的快速操作。 该示例展示了如何通过java版SDK配置VPC终端节点服务的白名单。

## 2、前置条件
- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。具体请参见[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)。
- 华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。

## 3、SDK获取和安装
您可以通过Maven配置所依赖的虚拟私有云服务SDK
```xml
<dependency>
   <groupId>com.huaweicloud.sdk</groupId>
   <artifactId>huaweicloud-sdk-vpcep</artifactId>
   <version>3.1.60</version>
</dependency>
```

## 4、关键代码片段
```java
 public class ConfigVpcEndPointServicePermissionDemo {
   private static final Logger LOGGER = LoggerFactory.getLogger(ConfigVpcEndPointServicePermissionDemo.class.getName());

   public static void main(String[] args) {
      // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
      // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
      String ak = System.getenv("HUAWEICLOUD_SDK_AK");
      String sk = System.getenv("HUAWEICLOUD_SDK_SK");

      ICredential auth = new BasicCredentials()
              .withAk(ak)
              .withSk(sk);

      VpcepClient client = VpcepClient.newBuilder()
              .withCredential(auth)
              .withRegion(VpcepRegion.valueOf("cn-north-4"))
              .build();

     // 1.添加终端节点服务的白名单
     AddOrRemoveServicePermissionsRequest request = new AddOrRemoveServicePermissionsRequest();
     AddOrRemoveServicePermissionsRequestBody body = new AddOrRemoveServicePermissionsRequestBody();
     body.setAction(AddOrRemoveServicePermissionsRequestBody.ActionEnum.ADD);
     body.setPermissions(Arrays.asList("{iam:domain::domain_id}")); // 格式为iam:domain::domain_id
     request.setVpcEndpointServiceId("{vpc_endpoint_service_id}");
     request.withBody(body);

     // 2.查询终端节点服务的白名单列表
     demo.listServicePermissionsDetailsResponse(client, "{vpc_endpoint_service_id}");
   }
}
```

## 5、返回示例

#### 5.1 添加终端节点服务的白名单

```java
class AddOrRemoveServicePermissionsResponse{
 
  permissions:
          [
          "iam:domain::5fc973***d0102"
          ]
  permissionType: domainId
}
```
#### 5.2 查询终端节点服务的白名单列表

```java
class ListServicePermissionsDetailsResponse{
  permissions:
          [
          "iam:domain::5fc973***d0101",
          "iam:domain::5fc973***d0102"
          ]
  totalCount: 2
}
```


## 6、参考
- API参考
  - [批量添加或移除终端节点服务的白名单](https://support.huaweicloud.com/api-vpcep/vpcep_06_0209.html)
  - [查询终端节点服务的白名单列表](https://support.huaweicloud.com/api-vpcep/ListServicePermissionsDetails.html)
- SDK参考
  [VPCEP JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/VPCEP?lang=Java)
- AK SK获取
  [AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)

## 7、修订记录

|    发布日期    | 文档版本 |   修订说明   |
|:----------:| :------: | :----------: |
| 2023-09-28 |   1.0    | 文档首次发布 |