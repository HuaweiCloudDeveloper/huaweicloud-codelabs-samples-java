## 1. Sample Overview

Huawei Cloud provides an SDK for VPCEP. You can integrate the SDK to perform operations on VPCEP by calling VPCEP APIs. This sample shows how to use the Java SDK to add whitelist records for a VPC endpoint service.

## 2. Prerequisites
- You have obtained the Huawei Cloud SDK. You can also install the Java SDK.
- To use the Huawei Cloud Java SDK, you must have a Huawei Cloud account and obtain the AK and SK of the account. For details, see [Obtaining an AK/SK](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- The Java version is 1.8 or later.

## 3. Obtaining and Installing the SDK
You can use Maven to configure the VPCEP SDK. 
```xml
<dependency>
   <groupId>com.huaweicloud.sdk</groupId>
   <artifactId>huaweicloud-sdk-vpcep</artifactId>
   <version>3.1.60</version>
</dependency>
```

## 4. Key Code Snippets
```java
 public class ConfigVpcEndPointServicePermissionDemo {
   private static final Logger LOGGER = LoggerFactory.getLogger(ConfigVpcEndPointServicePermissionDemo.class.getName());

   public static void main(String[] args) {
      // There will be security risks if the AK/SK used for authentication is directly written into code. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
      // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
      String ak = System.getenv("HUAWEICLOUD_SDK_AK");
      String sk = System.getenv("HUAWEICLOUD_SDK_SK");

      ICredential auth = new BasicCredentials()
              .withAk(ak)
              .withSk(sk);

      VpcepClient client = VpcepClient.newBuilder()
              .withCredential(auth)
              .withRegion(VpcepRegion.valueOf("cn-north-4"))
              .build();

     // 1. Adding whitelist records for a VPC endpoint service
     AddOrRemoveServicePermissionsRequest request = new AddOrRemoveServicePermissionsRequest();
     AddOrRemoveServicePermissionsRequestBody body = new AddOrRemoveServicePermissionsRequestBody();
     body.setAction(AddOrRemoveServicePermissionsRequestBody.ActionEnum.ADD);
     body.setPermissions(Arrays.asList("{iam:domain::domain_id}")); // Format: iam:domain::domain_id
     request.setVpcEndpointServiceId("{vpc_endpoint_service_id}");
     request.withBody(body);

     // 2. Querying whitelist records of a VPC endpoint service
     demo.listServicePermissionsDetailsResponse(client, "{vpc_endpoint_service_id}");
   }
}
```

## 5. Example Responses

#### 5.1 Adding Whitelist Records for a VPC Endpoint Service

```java
class AddOrRemoveServicePermissionsResponse{
 
  permissions:
          [
          "iam:domain::5fc973***d0102"
          ]
  permissionType: domainId
}
```
#### 5.2 Querying Whitelist Records of a VPC Endpoint Service

```java
class ListServicePermissionsDetailsResponse{
  permissions:
          [
          "iam:domain::5fc973***d0101",
          "iam:domain::5fc973***d0102"
          ]
  totalCount: 2
}
```


## 6. References
- API reference
  - [Batch Adding or Deleting Whitelist Records of a VPC Endpoint Service](https://support.huaweicloud.com/intl/en-us/api-vpcep/vpcep_06_0209.html)
  - [Querying Whitelist Records of a VPC Endpoint Service](https://support.huaweicloud.com/intl/en-us/api-vpcep/ListServicePermissionsDetails.html)
- SDK reference
  [VPCEP JAVA SDK](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/VPCEP?lang=Java)
- AK and SK
  [Obtaining AK/SK](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html)

## 7. Change History

|    Release Date   | Issue|   Description  |
|:----------:| :------: | :----------: |
| 2023-09-28 |   1.0    | This issue is the first official release.|
