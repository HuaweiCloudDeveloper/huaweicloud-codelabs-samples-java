## 1. Introduction

Huawei Cloud provides NAT Gateway SDKs. You can integrate the SDKs to call NAT Gateway APIs to quickly perform operations on NAT Gateway.

This example shows how to use NAT Gateway SDKs to create, show, update, and delete an SNAT rule on a public NAT gateway as well as querying SNAT rules.

## 2. Prerequisites

- You have obtained and installed the Huawei Cloud SDK, including the JAVA SDK of NAT Gateway. For details, see [NAT JAVA SDK](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/NAT?lang=Java).
- To use the Huawei Cloud Java SDK, you must have a Huawei Cloud account and obtain the AK and SK of the account. For details, see [Obtaining an AK/SK](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).
- The Huawei Cloud Java SDK can be used in **Java JDK 1.8** or later.

## 3. Obtaining and Installing the SDK

You can use Maven to configure the NAT Gateway SDK.

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-nat</artifactId>
    <version>3.1.60</version>
</dependency>
```

## 4. Key Code Snippets
```java
/**
 * Basic Operations on a Public NAT Gateway SNAT Rule
 */
public class SnatRuleDemo {
    private static final Logger logger = LoggerFactory.getLogger(SnatRuleDemo.class);

    public static void main(String[] args) {

        // AK and SK of the Huawei Cloud account
        // There will be security risks if the AK and SK used for authentication is written into code. Encrypt the AK/SK in the configuration file or environment variables for storage
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in your environment
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Create a credential
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // Create a NAT Gateway client
        NatClient client = NatClient.newBuilder()
            .withCredential(auth)
            .withRegion(NatRegion.CN_NORTH_4)
            .build();

        // 1. Creating an SNAT rule on a public NAT gateway
        CreateNatGatewaySnatRuleResponse createNatGatewaySnatRuleResponse = createSnatRule(client);

        // 2. Querying details of an SNAT rule on a public NAT gateway
        showSnatRule(client, createNatGatewaySnatRuleResponse.getSnatRule().getId());

        // 3. Updating an SNAT rule on a public NAT gateway
        updateSnatRule(client, createNatGatewaySnatRuleResponse.getSnatRule().getId());

        // 4. Querying SNAT rules on a public NAT gateway
        listSnatRules(client);

        // 5. Deleting an SNAT rule on a public NAT gateway
        deleteSnatRule(client, createNatGatewaySnatRuleResponse.getSnatRule().getId());
    }

    /**
     * Creating an SNAT rule on a public NAT gateway
     */
    public static CreateNatGatewaySnatRuleResponse createSnatRule(NatClient client) {
        CreateNatGatewaySnatRuleRequest request = new CreateNatGatewaySnatRuleRequest()
            .withBody(new CreateNatGatewaySnatRuleRequestOption()
                .withSnatRule(new CreateNatGatewaySnatRuleOption()
                    .withNatGatewayId("<nat gateway id>")
                    .withDescription("<snat rule description>")
                    .withNetworkId("<network id>")
                    .withSourceType(0)
                    .withFloatingIpId("<floating ip id>")
                ));

        CreateNatGatewaySnatRuleResponse response = null;
        try {
            response = client.createNatGatewaySnatRule(request);
            logger.info("create snat rule response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("create snat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("create snat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("create snat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("create snat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("create snat rule ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return response;
    }

    /**
     * Querying details of an SNAT rule on a public NAT gateway
     */
    public static void showSnatRule(NatClient client, String SnatRuleId) {
        ShowNatGatewaySnatRuleRequest request = new ShowNatGatewaySnatRuleRequest().withSnatRuleId(SnatRuleId);

        try {
            ShowNatGatewaySnatRuleResponse response = client.showNatGatewaySnatRule(request);
            logger.info("show snat rule response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("show snat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("show snat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("show snat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("show snat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("show snat rule ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Updating an SNAT rule on a public NAT gateway
     */
    public static void updateSnatRule(NatClient client, String SnatRuleId) {
        UpdateNatGatewaySnatRuleRequest request = new UpdateNatGatewaySnatRuleRequest()
            .withSnatRuleId(SnatRuleId)
            .withBody(new UpdateNatGatewaySnatRuleRequestOption()
                .withSnatRule(new UpdateNatGatewaySnatRuleOption()
                    .withNatGatewayId("<nat gateway id>")
                    .withDescription("<new description>")
                    .withPublicIpAddress("<public ip address>")));

        try {
            UpdateNatGatewaySnatRuleResponse response = client.updateNatGatewaySnatRule(request);
            logger.info("update snat rule response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("update snat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("update snat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("update snat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("update snat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("update snat rule ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Querying SNAT rules on a public NAT gateway
     */
    private static void listSnatRules(NatClient client) {
        ListNatGatewaySnatRulesRequest request = new ListNatGatewaySnatRulesRequest();

        try {
            ListNatGatewaySnatRulesResponse response = client.listNatGatewaySnatRules(request);
            logger.info("list snat rules response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("list snat rules ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("list snat rules ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("list snat rules ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("list snat rules ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("list snat rules ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Deleting an SNAT rule on a public NAT gateway
     */
    private static void deleteSnatRule(NatClient client, String SnatRuleId) {
        DeleteNatGatewaySnatRuleRequest request = new DeleteNatGatewaySnatRuleRequest()
            .withNatGatewayId("<nat gateway id>")
            .withSnatRuleId(SnatRuleId);

        try {
            DeleteNatGatewaySnatRuleResponse response = client.deleteNatGatewaySnatRule(request);
            logger.info("delete snat rule HTTP Status Code is: {}", response.getHttpStatusCode());
        } catch (ConnectionException e) {
            logger.error("delete snat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("delete snat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("delete snat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("delete snat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("delete snat rule ServerResponseException is: {}", e.getMessage());
        }
    }
}
```

## 5. Sample Return Values

Creating an SNAT rule on a public NAT gateway

```json
{
  "snat_rule" : {
    "id" : "275f1db0-95d1-4573-9054-030625736c17",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "nat_gateway_id" : "14dd1af5-b000-4208-96f1-f9cf32570fb4",
    "source_type" : 0,
    "floating_ip_id" : "ac6c97eb-3969-4fcd-9705-3262aa0a0887",
    "description" : "snat rule description",
    "status" : "PENDING_CREATE",
    "created_at" : "2023-10-09 03:47:57.000000",
    "network_id" : "bfed98bd-e807-4943-a089-57fefda4f6a6",
    "admin_state_up" : true,
    "floating_ip_address" : "120.46.182.13"
  }
}
```

Querying details of an SNAT rule on a public NAT gateway

```json
{
  "snat_rule" : {
    "id" : "275f1db0-95d1-4573-9054-030625736c17",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "nat_gateway_id" : "14dd1af5-b000-4208-96f1-f9cf32570fb4",
    "source_type" : 0,
    "floating_ip_id" : "ac6c97eb-3969-4fcd-9705-3262aa0a0887",
    "description" : "snat rule description",
    "status" : "PENDING_CREATE",
    "created_at" : "2023-10-09 03:47:57.000000",
    "network_id" : "bfed98bd-e807-4943-a089-57fefda4f6a6",
    "admin_state_up" : true,
    "floating_ip_address" : "120.46.182.13",
    "freezed_ip_address" : ""
    }
}
```

Updating an SNAT rule on a public NAT gateway

```json
{
  "snat_rule" : {
    "id" : "275f1db0-95d1-4573-9054-030625736c17",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "nat_gateway_id" : "14dd1af5-b000-4208-96f1-f9cf32570fb4",
    "source_type" : 0,
    "floating_ip_id" : "533bf882-3ab4-4654-9791-5fba83ed63f4",
    "description" : "snat rule new description",
    "status" : "PENDING_UPDATE",
    "created_at" : "2023-10-09 03:47:57.000000",
    "network_id" : "bfed98bd-e807-4943-a089-57fefda4f6a6",
    "admin_state_up" : true,
    "floating_ip_address" : "120.46.214.134",
    "public_ip_address" : "120.46.214.134"
  }
}
```

Querying SNAT rules on a public NAT gateway

```json
{
  "snat_rules" : [ {
    "id" : "275f1db0-95d1-4573-9054-030625736c17",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "nat_gateway_id" : "14dd1af5-b000-4208-96f1-f9cf32570fb4",
    "source_type" : 0,
    "floating_ip_id" : "ac6c97eb-3969-4fcd-9705-3262aa0a0887",
    "description" : "snat rule description",
    "status" : "PENDING_CREATE",
    "created_at" : "2023-10-09 03:47:57.000000",
    "network_id" : "bfed98bd-e807-4943-a089-57fefda4f6a6",
    "admin_state_up" : true,
    "floating_ip_address" : "120.46.182.13",
    "freezed_ip_address" : ""
  } ]
}
```