package com.huaweicloud.nat.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.nat.v2.NatClient;
import com.huaweicloud.sdk.nat.v2.model.CreateNatGatewaySnatRuleOption;
import com.huaweicloud.sdk.nat.v2.model.CreateNatGatewaySnatRuleRequest;
import com.huaweicloud.sdk.nat.v2.model.CreateNatGatewaySnatRuleRequestOption;
import com.huaweicloud.sdk.nat.v2.model.CreateNatGatewaySnatRuleResponse;
import com.huaweicloud.sdk.nat.v2.model.DeleteNatGatewaySnatRuleRequest;
import com.huaweicloud.sdk.nat.v2.model.DeleteNatGatewaySnatRuleResponse;
import com.huaweicloud.sdk.nat.v2.model.ListNatGatewaySnatRulesRequest;
import com.huaweicloud.sdk.nat.v2.model.ListNatGatewaySnatRulesResponse;
import com.huaweicloud.sdk.nat.v2.model.ShowNatGatewaySnatRuleRequest;
import com.huaweicloud.sdk.nat.v2.model.ShowNatGatewaySnatRuleResponse;
import com.huaweicloud.sdk.nat.v2.model.UpdateNatGatewaySnatRuleOption;
import com.huaweicloud.sdk.nat.v2.model.UpdateNatGatewaySnatRuleRequest;
import com.huaweicloud.sdk.nat.v2.model.UpdateNatGatewaySnatRuleRequestOption;
import com.huaweicloud.sdk.nat.v2.model.UpdateNatGatewaySnatRuleResponse;
import com.huaweicloud.sdk.nat.v2.region.NatRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  公网Snat规则基本操作
 */
public class SnatRuleDemo {
    private static final Logger logger = LoggerFactory.getLogger(SnatRuleDemo.class);

    public static void main(String[] args) {

        // 华为云账号Access Key, Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 创建认证
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // 创建NatClient实例
        NatClient client = NatClient.newBuilder()
            .withCredential(auth)
            .withRegion(NatRegion.CN_NORTH_4)
            .build();

        // 1. 创建公网SNAT规则
        CreateNatGatewaySnatRuleResponse createNatGatewaySnatRuleResponse = createSnatRule(client);

        // 2. 查询指定的公网SNAT规则详情
        showSnatRule(client, createNatGatewaySnatRuleResponse.getSnatRule().getId());

        // 3. 更新公网SNAT规则
        updateSnatRule(client, createNatGatewaySnatRuleResponse.getSnatRule().getId());

        // 4. 查询公网SNAT规则列表
        listSnatRules(client);

        // 5. 删除公网SNAT规则
        deleteSnatRule(client, createNatGatewaySnatRuleResponse.getSnatRule().getId());
    }

    /**
     *  创建公网SNAT规则
     */
    public static CreateNatGatewaySnatRuleResponse createSnatRule(NatClient client) {
        CreateNatGatewaySnatRuleRequest request = new CreateNatGatewaySnatRuleRequest()
            .withBody(new CreateNatGatewaySnatRuleRequestOption()
                .withSnatRule(new CreateNatGatewaySnatRuleOption()
                    .withNatGatewayId("<nat gateway id>")
                    .withDescription("<snat rule description>")
                    .withNetworkId("<network id>")
                    .withSourceType(0)
                    .withFloatingIpId("<floating ip id>")
                ));

        CreateNatGatewaySnatRuleResponse response = null;
        try {
            response = client.createNatGatewaySnatRule(request);
            logger.info("create snat rule response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("create snat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("create snat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("create snat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("create snat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("create snat rule ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return response;
    }

    /**
     *  查询指定的公网SNAT规则详情
     */
    public static void showSnatRule(NatClient client, String SnatRuleId) {
        ShowNatGatewaySnatRuleRequest request = new ShowNatGatewaySnatRuleRequest().withSnatRuleId(SnatRuleId);

        try {
            ShowNatGatewaySnatRuleResponse response = client.showNatGatewaySnatRule(request);
            logger.info("show snat rule response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("show snat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("show snat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("show snat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("show snat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("show snat rule ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  更新公网SNAT规则
     */
    public static void updateSnatRule(NatClient client, String SnatRuleId) {
        UpdateNatGatewaySnatRuleRequest request = new UpdateNatGatewaySnatRuleRequest()
            .withSnatRuleId(SnatRuleId)
            .withBody(new UpdateNatGatewaySnatRuleRequestOption()
                .withSnatRule(new UpdateNatGatewaySnatRuleOption()
                    .withNatGatewayId("<nat gateway id>")
                    .withDescription("<new description>")
                    .withPublicIpAddress("<public ip address>")));

        try {
            UpdateNatGatewaySnatRuleResponse response = client.updateNatGatewaySnatRule(request);
            logger.info("update snat rule response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("update snat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("update snat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("update snat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("update snat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("update snat rule ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  查询公网SNAT规则列表
     */
    private static void listSnatRules(NatClient client) {
        ListNatGatewaySnatRulesRequest request = new ListNatGatewaySnatRulesRequest();

        try {
            ListNatGatewaySnatRulesResponse response = client.listNatGatewaySnatRules(request);
            logger.info("list snat rules response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("list snat rules ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("list snat rules ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("list snat rules ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("list snat rules ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("list snat rules ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  删除公网SNAT规则
     */
    private static void deleteSnatRule(NatClient client, String SnatRuleId) {
        DeleteNatGatewaySnatRuleRequest request = new DeleteNatGatewaySnatRuleRequest()
            .withNatGatewayId("<nat gateway id>")
            .withSnatRuleId(SnatRuleId);

        try {
            DeleteNatGatewaySnatRuleResponse response = client.deleteNatGatewaySnatRule(request);
            logger.info("delete snat rule HTTP Status Code is: {}", response.getHttpStatusCode());
        } catch (ConnectionException e) {
            logger.error("delete snat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("delete snat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("delete snat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("delete snat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("delete snat rule ServerResponseException is: {}", e.getMessage());
        }
    }
}