## 1、功能介绍

华为云提供了NAT网关（NAT Gateway）服务SDK，您可以直接集成SDK来调用NAT网关相关API，从而实现对NAT网关服务的快速操作。

本示例展示如何通过NAT网关（NAT Gateway）相关SDK实践公网SNAT规则创建、查询指定资源详情、更新、查询资源列表、删除的功能。

## 2、前置条件

- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。具体请参见[NAT JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/NAT?lang=Java)。
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。具体请参见[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)。
- 华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。

## 3、SDK获取和安装

您可以通过Maven配置所依赖的NAT网关服务SDK

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-nat</artifactId>
    <version>3.1.60</version>
</dependency>
```

## 4、关键代码片段
```java
/**
 *  公网Snat规则基本操作
 */
public class SnatRuleDemo {
    private static final Logger logger = LoggerFactory.getLogger(SnatRuleDemo.class);

    public static void main(String[] args) {

        // 华为云账号Access Key, Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 创建认证
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // 创建NatClient实例
        NatClient client = NatClient.newBuilder()
            .withCredential(auth)
            .withRegion(NatRegion.CN_NORTH_4)
            .build();

        // 1. 创建公网SNAT规则
        CreateNatGatewaySnatRuleResponse createNatGatewaySnatRuleResponse = createSnatRule(client);

        // 2. 查询指定的公网SNAT规则详情
        showSnatRule(client, createNatGatewaySnatRuleResponse.getSnatRule().getId());

        // 3. 更新公网SNAT规则
        updateSnatRule(client, createNatGatewaySnatRuleResponse.getSnatRule().getId());

        // 4. 查询公网SNAT规则列表
        listSnatRules(client);

        // 5. 删除公网SNAT规则
        deleteSnatRule(client, createNatGatewaySnatRuleResponse.getSnatRule().getId());
    }

    /**
     *  创建公网SNAT规则
     */
    public static CreateNatGatewaySnatRuleResponse createSnatRule(NatClient client) {
        CreateNatGatewaySnatRuleRequest request = new CreateNatGatewaySnatRuleRequest()
            .withBody(new CreateNatGatewaySnatRuleRequestOption()
                .withSnatRule(new CreateNatGatewaySnatRuleOption()
                    .withNatGatewayId("<nat gateway id>")
                    .withDescription("<snat rule description>")
                    .withNetworkId("<network id>")
                    .withSourceType(0)
                    .withFloatingIpId("<floating ip id>")
                ));

        CreateNatGatewaySnatRuleResponse response = null;
        try {
            response = client.createNatGatewaySnatRule(request);
            logger.info("create snat rule response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("create snat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("create snat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("create snat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("create snat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("create snat rule ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return response;
    }

    /**
     *  查询指定的公网SNAT规则详情
     */
    public static void showSnatRule(NatClient client, String SnatRuleId) {
        ShowNatGatewaySnatRuleRequest request = new ShowNatGatewaySnatRuleRequest().withSnatRuleId(SnatRuleId);

        try {
            ShowNatGatewaySnatRuleResponse response = client.showNatGatewaySnatRule(request);
            logger.info("show snat rule response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("show snat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("show snat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("show snat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("show snat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("show snat rule ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  更新公网SNAT规则
     */
    public static void updateSnatRule(NatClient client, String SnatRuleId) {
        UpdateNatGatewaySnatRuleRequest request = new UpdateNatGatewaySnatRuleRequest()
            .withSnatRuleId(SnatRuleId)
            .withBody(new UpdateNatGatewaySnatRuleRequestOption()
                .withSnatRule(new UpdateNatGatewaySnatRuleOption()
                    .withNatGatewayId("<nat gateway id>")
                    .withDescription("<new description>")
                    .withPublicIpAddress("<public ip address>")));

        try {
            UpdateNatGatewaySnatRuleResponse response = client.updateNatGatewaySnatRule(request);
            logger.info("update snat rule response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("update snat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("update snat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("update snat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("update snat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("update snat rule ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  查询公网SNAT规则列表
     */
    private static void listSnatRules(NatClient client) {
        ListNatGatewaySnatRulesRequest request = new ListNatGatewaySnatRulesRequest();

        try {
            ListNatGatewaySnatRulesResponse response = client.listNatGatewaySnatRules(request);
            logger.info("list snat rules response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("list snat rules ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("list snat rules ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("list snat rules ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("list snat rules ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("list snat rules ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  删除公网SNAT规则
     */
    private static void deleteSnatRule(NatClient client, String SnatRuleId) {
        DeleteNatGatewaySnatRuleRequest request = new DeleteNatGatewaySnatRuleRequest()
            .withNatGatewayId("<nat gateway id>")
            .withSnatRuleId(SnatRuleId);

        try {
            DeleteNatGatewaySnatRuleResponse response = client.deleteNatGatewaySnatRule(request);
            logger.info("delete snat rule HTTP Status Code is: {}", response.getHttpStatusCode());
        } catch (ConnectionException e) {
            logger.error("delete snat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("delete snat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("delete snat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("delete snat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("delete snat rule ServerResponseException is: {}", e.getMessage());
        }
    }
}
```

## 5、返回结果示例

创建公网SNAT规则

```json
{
  "snat_rule" : {
    "id" : "275f1db0-95d1-4573-9054-030625736c17",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "nat_gateway_id" : "14dd1af5-b000-4208-96f1-f9cf32570fb4",
    "source_type" : 0,
    "floating_ip_id" : "ac6c97eb-3969-4fcd-9705-3262aa0a0887",
    "description" : "snat rule description",
    "status" : "PENDING_CREATE",
    "created_at" : "2023-10-09 03:47:57.000000",
    "network_id" : "bfed98bd-e807-4943-a089-57fefda4f6a6",
    "admin_state_up" : true,
    "floating_ip_address" : "120.46.182.13"
  }
}
```

查询指定的公网SNAT规则详情

```json
{
  "snat_rule" : {
    "id" : "275f1db0-95d1-4573-9054-030625736c17",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "nat_gateway_id" : "14dd1af5-b000-4208-96f1-f9cf32570fb4",
    "source_type" : 0,
    "floating_ip_id" : "ac6c97eb-3969-4fcd-9705-3262aa0a0887",
    "description" : "snat rule description",
    "status" : "PENDING_CREATE",
    "created_at" : "2023-10-09 03:47:57.000000",
    "network_id" : "bfed98bd-e807-4943-a089-57fefda4f6a6",
    "admin_state_up" : true,
    "floating_ip_address" : "120.46.182.13",
    "freezed_ip_address" : ""
    }
}
```

更新公网SNAT规则

```json
{
  "snat_rule" : {
    "id" : "275f1db0-95d1-4573-9054-030625736c17",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "nat_gateway_id" : "14dd1af5-b000-4208-96f1-f9cf32570fb4",
    "source_type" : 0,
    "floating_ip_id" : "533bf882-3ab4-4654-9791-5fba83ed63f4",
    "description" : "snat rule new description",
    "status" : "PENDING_UPDATE",
    "created_at" : "2023-10-09 03:47:57.000000",
    "network_id" : "bfed98bd-e807-4943-a089-57fefda4f6a6",
    "admin_state_up" : true,
    "floating_ip_address" : "120.46.214.134",
    "public_ip_address" : "120.46.214.134"
  }
}
```

查询公网SNAT规则列表

```json
{
  "snat_rules" : [ {
    "id" : "275f1db0-95d1-4573-9054-030625736c17",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "nat_gateway_id" : "14dd1af5-b000-4208-96f1-f9cf32570fb4",
    "source_type" : 0,
    "floating_ip_id" : "ac6c97eb-3969-4fcd-9705-3262aa0a0887",
    "description" : "snat rule description",
    "status" : "PENDING_CREATE",
    "created_at" : "2023-10-09 03:47:57.000000",
    "network_id" : "bfed98bd-e807-4943-a089-57fefda4f6a6",
    "admin_state_up" : true,
    "floating_ip_address" : "120.46.182.13",
    "freezed_ip_address" : ""
  } ]
}
```