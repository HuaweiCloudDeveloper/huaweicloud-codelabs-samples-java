## 1、功能介绍

华为云提供了NAT网关（NAT Gateway）服务SDK，您可以直接集成SDK来调用NAT网关相关API，从而实现对NAT网关服务的快速操作。

本示例展示如何通过NAT网关（NAT Gateway）相关SDK实践公网DNAT规则创建、查询指定资源详情、更新、查询资源列表、删除、批量创建的功能。

## 2、前置条件

- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。具体请参见[NAT JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/NAT?lang=Java)。
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。具体请参见[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)。
- 华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。

## 3、SDK获取和安装

您可以通过Maven配置所依赖的NAT网关服务SDK

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-nat</artifactId>
    <version>3.1.60</version>
</dependency>
```

## 4、关键代码片段
```java

/**
 *  公网DNAT规则基本操作
 */
public class DnatRuleDemo {

    private static final Logger logger = LoggerFactory.getLogger(DnatRuleDemo.class);

    public static void main(String[] args) {

        // 华为云账号Access Key, Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 创建认证
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // 创建NatClient实例
        NatClient client = NatClient.newBuilder()
            .withCredential(auth)
            .withRegion(NatRegion.CN_NORTH_4)
            .build();

        // 1. 创建公网DNAT规则
        CreateNatGatewayDnatRuleResponse createNatGatewayDnatRuleResponse = createDnatRule(client);

        // 2. 查询指定的公网DNAT规则详情
        showDnatRule(client, createNatGatewayDnatRuleResponse.getDnatRule().getId());

        // 3. 更新公网DNAT规则
        updateDnatRule(client, createNatGatewayDnatRuleResponse.getDnatRule().getId());

        // 4. 查询公网DNAT规则列表
        listDnatRules(client);

        // 5. 删除公网DNAT规则
        deleteDnatRule(client, createNatGatewayDnatRuleResponse.getDnatRule().getId());

        // 6. 批量创建公网DNAT规则
        batchCreateDnatRules(client);
    }

    /**
     *  创建公网DNAT规则
     */
    public static CreateNatGatewayDnatRuleResponse createDnatRule(NatClient client) {
        CreateNatGatewayDnatRuleRequest request = new CreateNatGatewayDnatRuleRequest()
            .withBody(new CreateNatGatewayDnatRuleOption()
                .withDnatRule(new CreateNatGatewayDnatOption()
                    .withNatGatewayId("<nat gateway id>")
                    .withDescription("<dnat rule description>")
                    .withProtocol("<protocol>")
                    .withFloatingIpId("<floating ip id>")
                    .withPortId("<port id>")
                    .withInternalServicePort(993)
                    .withExternalServicePort(242)
                ));

        CreateNatGatewayDnatRuleResponse response = null;
        try {
            response = client.createNatGatewayDnatRule(request);
            logger.info("create dnat rule response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("create dnat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("create dnat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("create dnat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("create dnat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("create dnat rule ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return response;
    }

    /**
     *  查询指定的公网DNAT规则详情
     */
    public static void showDnatRule(NatClient client, String dnatRuleId) {
        ShowNatGatewayDnatRuleRequest request = new ShowNatGatewayDnatRuleRequest().withDnatRuleId(dnatRuleId);

        try {
            ShowNatGatewayDnatRuleResponse response = client.showNatGatewayDnatRule(request);
            logger.info("show dnat rule response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("show dnat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("show dnat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("show dnat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("show dnat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("show dnat rule ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  更新公网DNAT规则
     */
    public static void updateDnatRule(NatClient client, String dnatRuleId) {
        UpdateNatGatewayDnatRuleRequest request = new UpdateNatGatewayDnatRuleRequest()
            .withDnatRuleId(dnatRuleId)
            .withBody(new UpdateNatGatewayDnatRuleRequestBody()
                .withDnatRule(new UpdateNatGatewayDnatRuleOption()
                    .withNatGatewayId("<nat gateway id>")
                    .withDescription("<new description>")
                    .withFloatingIpId("<floating ip id>")
                    .withProtocol(UpdateNatGatewayDnatRuleOption.ProtocolEnum.UDP)
                    .withExternalServicePort(22)
                    .withInternalServicePort(33)
                    .withPortId("<port id>")));

        try {
            UpdateNatGatewayDnatRuleResponse response = client.updateNatGatewayDnatRule(request);
            logger.info("update dnat rule response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("update dnat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("update dnat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("update dnat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("update dnat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("update dnat rule ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  查询公网DNAT规则列表
     */
    private static void listDnatRules(NatClient client) {
        ListNatGatewayDnatRulesRequest request = new ListNatGatewayDnatRulesRequest();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            ListNatGatewayDnatRulesResponse response = client.listNatGatewayDnatRules(request);
            logger.info("list dnat rules response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("list dnat rules ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("list dnat rules ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("list dnat rules ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("list dnat rules ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("list dnat rules ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  删除公网DNAT规则
     */
    private static void deleteDnatRule(NatClient client, String dnatRuleId) {
        DeleteNatGatewayDnatRuleRequest request = new DeleteNatGatewayDnatRuleRequest()
            .withNatGatewayId("<nat gateway id>")
            .withDnatRuleId(dnatRuleId);

        try {
            DeleteNatGatewayDnatRuleResponse response = client.deleteNatGatewayDnatRule(request);
            logger.info("delete dnat rule HTTP Status Code is: {}", response.getHttpStatusCode());
        } catch (ConnectionException e) {
            logger.error("delete dnat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("delete dnat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("delete dnat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("delete dnat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("delete dnat rule ServerResponseException is: {}", e.getMessage());
        }
    }

    /**
     *  批量创建公网DNAT规则
     */
    private static void batchCreateDnatRules(NatClient client) {
        BatchCreateNatGatewayDnatRulesRequest request = new BatchCreateNatGatewayDnatRulesRequest()
            .withBody(new BatchCreateNatGatewayDnatRulesRequestBody()
                .withDnatRules(Collections.singletonList(new CreateNatGatewayDnatOption()
                    .withNatGatewayId("<nat gateway id>")
                    .withDescription("<dnat rule description>")
                    .withProtocol("<protocol>")
                    .withFloatingIpId("<floating ip id>")
                    .withPortId("<port id>")
                    .withInternalServicePort(993)
                    .withExternalServicePort(242))));

        try {
            BatchCreateNatGatewayDnatRulesResponse response = client.batchCreateNatGatewayDnatRules(request);
            logger.info("batch create dnat rules response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("batch create ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("batch create ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("batch create ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("batch create ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("batch create ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
```

## 5、返回结果示例

创建公网DNAT规则

```json
{
  "dnat_rule" : {
    "id" : "d20b5e03-7afc-4be3-bebe-4fac1b8d074d",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "description" : "nat gateway description",
    "port_id" : "9a836845-5cd6-4581-8e9e-06caf39b3d49",
    "private_ip" : "192.168.0.83",
    "internal_service_port" : 993,
    "nat_gateway_id" : "14dd1af5-b000-4208-96f1-f9cf32570fb4",
    "floating_ip_id" : "ac6c97eb-3969-4fcd-9705-3262aa0a0887",
    "floating_ip_address" : "120.46.182.13",
    "external_service_port" : 242,
    "status" : "PENDING_CREATE",
    "admin_state_up" : true,
    "protocol" : "tcp",
    "created_at" : "2023-10-09 06:17:31.000000"
  }
}
```

查询指定的公网DNAT规则详情

```json
{
  "dnat_rule" : {
    "id" : "d20b5e03-7afc-4be3-bebe-4fac1b8d074d",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "description" : "nat gateway description",
    "port_id" : "9a836845-5cd6-4581-8e9e-06caf39b3d49",
    "private_ip" : "192.168.0.83",
    "internal_service_port" : 993,
    "nat_gateway_id" : "14dd1af5-b000-4208-96f1-f9cf32570fb4",
    "floating_ip_id" : "ac6c97eb-3969-4fcd-9705-3262aa0a0887",
    "floating_ip_address" : "120.46.182.13",
    "external_service_port" : 242,
    "status" : "ACTIVE",
    "admin_state_up" : true,
    "protocol" : "tcp",
    "created_at" : "2023-10-09 06:17:31.000000"
    }
}
```

更新公网DNAT规则

```json
{
  "dnat_rule" : {
    "id" : "a4828358-f8cf-4ddf-9d20-b2590901565e",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "description" : "new description",
    "port_id" : "9a836845-5cd6-4581-8e9e-06caf39b3d49",
    "private_ip" : "192.168.0.83",
    "internal_service_port" : 33,
    "nat_gateway_id" : "14dd1af5-b000-4208-96f1-f9cf32570fb4",
    "floating_ip_id" : "533bf882-3ab4-4654-9791-5fba83ed63f4",
    "floating_ip_address" : "120.46.214.134",
    "external_service_port" : 22,
    "status" : "PENDING_UPDATE",
    "admin_state_up" : true,
    "protocol" : "udp",
    "created_at" : "2023-10-09 06:30:15.000000"
  }
}
```

查询公网DNAT规则列表

```json
{
  "dnat_rules" : [ {
    "id" : "d20b5e03-7afc-4be3-bebe-4fac1b8d074d",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "description" : "nat gateway description",
    "port_id" : "9a836845-5cd6-4581-8e9e-06caf39b3d49",
    "private_ip" : "192.168.0.83",
    "internal_service_port" : 993,
    "nat_gateway_id" : "14dd1af5-b000-4208-96f1-f9cf32570fb4",
    "floating_ip_id" : "ac6c97eb-3969-4fcd-9705-3262aa0a0887",
    "floating_ip_address" : "120.46.182.13",
    "external_service_port" : 242,
    "status" : "ACTIVE",
    "admin_state_up" : true,
    "protocol" : "tcp",
    "created_at" : "2023-10-09 06:17:31.000000"
  } ]
}
```

批量创建公网DNAT规则

```json
{
  "dnat_rules" : [ {
    "id" : "a4828358-f8cf-4ddf-9d20-b2590901565e",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "description" : "nat gateway description",
    "port_id" : "9a836845-5cd6-4581-8e9e-06caf39b3d49",
    "private_ip" : "192.168.0.83",
    "internal_service_port" : 993,
    "nat_gateway_id" : "14dd1af5-b000-4208-96f1-f9cf32570fb4",
    "floating_ip_id" : "ac6c97eb-3969-4fcd-9705-3262aa0a0887",
    "floating_ip_address" : "120.46.182.13",
    "external_service_port" : 242,
    "status" : "PENDING_CREATE",
    "admin_state_up" : true,
    "protocol" : "tcp",
    "created_at" : "2023-10-09 06:30:15.000000"
  } ]
}
```
