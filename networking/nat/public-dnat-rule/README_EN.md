## 1. Introduction

Huawei Cloud provides NAT Gateway SDKs. You can integrate the SDKs to call NAT Gateway APIs to quickly perform operations on NAT Gateway.

This example shows how to use NAT Gateway SDKs to create, show, update, and delete a DNAT rule on a public NAT gateway, how to query DNAT rules, and how to batch create DNAT rules on a public NAT gateway.

## 2. Prerequisites

- You have obtained and installed the Huawei Cloud SDK, including the JAVA SDK of NAT Gateway. For details, see [NAT JAVA SDK](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/NAT?lang=Java).
- To use the Huawei Cloud Java SDK, you must have a Huawei Cloud account and obtain the AK and SK of the account. For details, see [Obtaining an AK/SK](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).
- The Huawei Cloud Java SDK can be used in **Java JDK 1.8** or later.

## 3. Obtaining and Installing the SDK

You can use Maven to configure the NAT Gateway SDK.

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-nat</artifactId>
    <version>3.1.60</version>
</dependency>
```

## 4. Key Code Snippets
```java

/**
 *  Basic Operations on the Public NAT Gateway DNAT Rule
 */
public class DnatRuleDemo {

    private static final Logger logger = LoggerFactory.getLogger(DnatRuleDemo.class);

    public static void main(String[] args) {

        // AK and SK of the Huawei Cloud account
        // There will be security risks if the AK and SK used for authentication is written into code. Encrypt the AK/SK in the configuration file or environment variables for storage
        // In this example, the AK and SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in your environment
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Create a credential
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // Create a NAT Gateway client
        NatClient client = NatClient.newBuilder()
            .withCredential(auth)
            .withRegion(NatRegion.CN_NORTH_4)
            .build();

        // 1. Creating a DNAT rule on a public NAT gateway
        CreateNatGatewayDnatRuleResponse createNatGatewayDnatRuleResponse = createDnatRule(client);

        // 2. Querying details of a DNAT rule on a public NAT gateway
        showDnatRule(client, createNatGatewayDnatRuleResponse.getDnatRule().getId());

        // 3. Updating a DNAT rule on a public NAT gateway
        updateDnatRule(client, createNatGatewayDnatRuleResponse.getDnatRule().getId());

        // 4. Querying DNAT rules on a public NAT gateway
        listDnatRules(client);

        // 5. Deleting a DNAT rule on a public NAT gateway
        deleteDnatRule(client, createNatGatewayDnatRuleResponse.getDnatRule().getId());

        // 6. Batch creating DNAT rules on a public NAT gateway
        batchCreateDnatRules(client);
    }

    /**
     *  Creating a DNAT rule on a public NAT gateway
     */
    public static CreateNatGatewayDnatRuleResponse createDnatRule(NatClient client) {
        CreateNatGatewayDnatRuleRequest request = new CreateNatGatewayDnatRuleRequest()
            .withBody(new CreateNatGatewayDnatRuleOption()
                .withDnatRule(new CreateNatGatewayDnatOption()
                    .withNatGatewayId("<nat gateway id>")
                    .withDescription("<dnat rule description>")
                    .withProtocol("<protocol>")
                    .withFloatingIpId("<floating ip id>")
                    .withPortId("<port id>")
                    .withInternalServicePort(993)
                    .withExternalServicePort(242)
                ));

        CreateNatGatewayDnatRuleResponse response = null;
        try {
            response = client.createNatGatewayDnatRule(request);
            logger.info("create dnat rule response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("create dnat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("create dnat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("create dnat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("create dnat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("create dnat rule ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return response;
    }

    /**
     *  Querying details of a DNAT rule on a public NAT gateway
     */
    public static void showDnatRule(NatClient client, String dnatRuleId) {
        ShowNatGatewayDnatRuleRequest request = new ShowNatGatewayDnatRuleRequest().withDnatRuleId(dnatRuleId);

        try {
            ShowNatGatewayDnatRuleResponse response = client.showNatGatewayDnatRule(request);
            logger.info("show dnat rule response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("show dnat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("show dnat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("show dnat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("show dnat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("show dnat rule ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  Updating a DNAT rule on a public NAT gateway
     */
    public static void updateDnatRule(NatClient client, String dnatRuleId) {
        UpdateNatGatewayDnatRuleRequest request = new UpdateNatGatewayDnatRuleRequest()
            .withDnatRuleId(dnatRuleId)
            .withBody(new UpdateNatGatewayDnatRuleRequestBody()
                .withDnatRule(new UpdateNatGatewayDnatRuleOption()
                    .withNatGatewayId("<nat gateway id>")
                    .withDescription("<new description>")
                    .withFloatingIpId("<floating ip id>")
                    .withProtocol(UpdateNatGatewayDnatRuleOption.ProtocolEnum.UDP)
                    .withExternalServicePort(22)
                    .withInternalServicePort(33)
                    .withPortId("<port id>")));

        try {
            UpdateNatGatewayDnatRuleResponse response = client.updateNatGatewayDnatRule(request);
            logger.info("update dnat rule response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("update dnat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("update dnat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("update dnat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("update dnat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("update dnat rule ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  Querying DNAT rules on a public NAT gateway
     */
    private static void listDnatRules(NatClient client) {
        ListNatGatewayDnatRulesRequest request = new ListNatGatewayDnatRulesRequest();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            ListNatGatewayDnatRulesResponse response = client.listNatGatewayDnatRules(request);
            logger.info("list dnat rules response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("list dnat rules ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("list dnat rules ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("list dnat rules ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("list dnat rules ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("list dnat rules ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  Deleting a DNAT rule on a public NAT gateway
     */
    private static void deleteDnatRule(NatClient client, String dnatRuleId) {
        DeleteNatGatewayDnatRuleRequest request = new DeleteNatGatewayDnatRuleRequest()
            .withNatGatewayId("<nat gateway id>")
            .withDnatRuleId(dnatRuleId);

        try {
            DeleteNatGatewayDnatRuleResponse response = client.deleteNatGatewayDnatRule(request);
            logger.info("delete dnat rule HTTP Status Code is: {}", response.getHttpStatusCode());
        } catch (ConnectionException e) {
            logger.error("delete dnat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("delete dnat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("delete dnat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("delete dnat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("delete dnat rule ServerResponseException is: {}", e.getMessage());
        }
    }

    /**
     *  Batch creating DNAT rules on a public NAT gateway
     */
    private static void batchCreateDnatRules(NatClient client) {
        BatchCreateNatGatewayDnatRulesRequest request = new BatchCreateNatGatewayDnatRulesRequest()
            .withBody(new BatchCreateNatGatewayDnatRulesRequestBody()
                .withDnatRules(Collections.singletonList(new CreateNatGatewayDnatOption()
                    .withNatGatewayId("<nat gateway id>")
                    .withDescription("<dnat rule description>")
                    .withProtocol("<protocol>")
                    .withFloatingIpId("<floating ip id>")
                    .withPortId("<port id>")
                    .withInternalServicePort(993)
                    .withExternalServicePort(242))));

        try {
            BatchCreateNatGatewayDnatRulesResponse response = client.batchCreateNatGatewayDnatRules(request);
            logger.info("batch create dnat rules response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("batch create ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("batch create ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("batch create ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("batch create ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("batch create ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
```

## 5. Sample Return Values

Creating a DNAT rule on a public NAT gateway

```json
{
  "dnat_rule" : {
    "id" : "d20b5e03-7afc-4be3-bebe-4fac1b8d074d",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "description" : "nat gateway description",
    "port_id" : "9a836845-5cd6-4581-8e9e-06caf39b3d49",
    "private_ip" : "192.168.0.83",
    "internal_service_port" : 993,
    "nat_gateway_id" : "14dd1af5-b000-4208-96f1-f9cf32570fb4",
    "floating_ip_id" : "ac6c97eb-3969-4fcd-9705-3262aa0a0887",
    "floating_ip_address" : "120.46.182.13",
    "external_service_port" : 242,
    "status" : "PENDING_CREATE",
    "admin_state_up" : true,
    "protocol" : "tcp",
    "created_at" : "2023-10-09 06:17:31.000000"
  }
}
```

Querying details of a DNAT rule on a public NAT gateway

```json
{
  "dnat_rule" : {
    "id" : "d20b5e03-7afc-4be3-bebe-4fac1b8d074d",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "description" : "nat gateway description",
    "port_id" : "9a836845-5cd6-4581-8e9e-06caf39b3d49",
    "private_ip" : "192.168.0.83",
    "internal_service_port" : 993,
    "nat_gateway_id" : "14dd1af5-b000-4208-96f1-f9cf32570fb4",
    "floating_ip_id" : "ac6c97eb-3969-4fcd-9705-3262aa0a0887",
    "floating_ip_address" : "120.46.182.13",
    "external_service_port" : 242,
    "status" : "ACTIVE",
    "admin_state_up" : true,
    "protocol" : "tcp",
    "created_at" : "2023-10-09 06:17:31.000000"
    }
}
```

Updating a DNAT rule on a public NAT gateway

```json
{
  "dnat_rule" : {
    "id" : "a4828358-f8cf-4ddf-9d20-b2590901565e",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "description" : "new description",
    "port_id" : "9a836845-5cd6-4581-8e9e-06caf39b3d49",
    "private_ip" : "192.168.0.83",
    "internal_service_port" : 33,
    "nat_gateway_id" : "14dd1af5-b000-4208-96f1-f9cf32570fb4",
    "floating_ip_id" : "533bf882-3ab4-4654-9791-5fba83ed63f4",
    "floating_ip_address" : "120.46.214.134",
    "external_service_port" : 22,
    "status" : "PENDING_UPDATE",
    "admin_state_up" : true,
    "protocol" : "udp",
    "created_at" : "2023-10-09 06:30:15.000000"
  }
}
```

Querying DNAT rules on a public NAT gateway

```json
{
  "dnat_rules" : [ {
    "id" : "d20b5e03-7afc-4be3-bebe-4fac1b8d074d",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "description" : "nat gateway description",
    "port_id" : "9a836845-5cd6-4581-8e9e-06caf39b3d49",
    "private_ip" : "192.168.0.83",
    "internal_service_port" : 993,
    "nat_gateway_id" : "14dd1af5-b000-4208-96f1-f9cf32570fb4",
    "floating_ip_id" : "ac6c97eb-3969-4fcd-9705-3262aa0a0887",
    "floating_ip_address" : "120.46.182.13",
    "external_service_port" : 242,
    "status" : "ACTIVE",
    "admin_state_up" : true,
    "protocol" : "tcp",
    "created_at" : "2023-10-09 06:17:31.000000"
  } ]
}
```

Batch creating DNAT rules on a public NAT gateway

```json
{
  "dnat_rules" : [ {
    "id" : "a4828358-f8cf-4ddf-9d20-b2590901565e",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "description" : "nat gateway description",
    "port_id" : "9a836845-5cd6-4581-8e9e-06caf39b3d49",
    "private_ip" : "192.168.0.83",
    "internal_service_port" : 993,
    "nat_gateway_id" : "14dd1af5-b000-4208-96f1-f9cf32570fb4",
    "floating_ip_id" : "ac6c97eb-3969-4fcd-9705-3262aa0a0887",
    "floating_ip_address" : "120.46.182.13",
    "external_service_port" : 242,
    "status" : "PENDING_CREATE",
    "admin_state_up" : true,
    "protocol" : "tcp",
    "created_at" : "2023-10-09 06:30:15.000000"
  } ]
}
```
