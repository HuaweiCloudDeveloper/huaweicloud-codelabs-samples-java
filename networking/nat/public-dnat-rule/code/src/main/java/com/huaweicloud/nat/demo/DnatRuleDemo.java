package com.huaweicloud.nat.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.nat.v2.NatClient;
import com.huaweicloud.sdk.nat.v2.model.BatchCreateNatGatewayDnatRulesRequest;
import com.huaweicloud.sdk.nat.v2.model.BatchCreateNatGatewayDnatRulesRequestBody;
import com.huaweicloud.sdk.nat.v2.model.BatchCreateNatGatewayDnatRulesResponse;
import com.huaweicloud.sdk.nat.v2.model.CreateNatGatewayDnatOption;
import com.huaweicloud.sdk.nat.v2.model.CreateNatGatewayDnatRuleOption;
import com.huaweicloud.sdk.nat.v2.model.CreateNatGatewayDnatRuleRequest;
import com.huaweicloud.sdk.nat.v2.model.CreateNatGatewayDnatRuleResponse;
import com.huaweicloud.sdk.nat.v2.model.DeleteNatGatewayDnatRuleRequest;
import com.huaweicloud.sdk.nat.v2.model.DeleteNatGatewayDnatRuleResponse;
import com.huaweicloud.sdk.nat.v2.model.ListNatGatewayDnatRulesRequest;
import com.huaweicloud.sdk.nat.v2.model.ListNatGatewayDnatRulesResponse;
import com.huaweicloud.sdk.nat.v2.model.ShowNatGatewayDnatRuleRequest;
import com.huaweicloud.sdk.nat.v2.model.ShowNatGatewayDnatRuleResponse;
import com.huaweicloud.sdk.nat.v2.model.UpdateNatGatewayDnatRuleOption;
import com.huaweicloud.sdk.nat.v2.model.UpdateNatGatewayDnatRuleRequest;
import com.huaweicloud.sdk.nat.v2.model.UpdateNatGatewayDnatRuleRequestBody;
import com.huaweicloud.sdk.nat.v2.model.UpdateNatGatewayDnatRuleResponse;
import com.huaweicloud.sdk.nat.v2.region.NatRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;

/**
 *  公网DNAT规则基本操作
 */
public class DnatRuleDemo {

    private static final Logger logger = LoggerFactory.getLogger(DnatRuleDemo.class);

    public static void main(String[] args) {

        // 华为云账号Access Key, Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 创建认证
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // 创建NatClient实例
        NatClient client = NatClient.newBuilder()
            .withCredential(auth)
            .withRegion(NatRegion.CN_NORTH_4)
            .build();

        // 1. 创建公网DNAT规则
        CreateNatGatewayDnatRuleResponse createNatGatewayDnatRuleResponse = createDnatRule(client);

        // 2. 查询指定的公网DNAT规则详情
        showDnatRule(client, createNatGatewayDnatRuleResponse.getDnatRule().getId());

        // 3. 更新公网DNAT规则
        updateDnatRule(client, createNatGatewayDnatRuleResponse.getDnatRule().getId());

        // 4. 查询公网DNAT规则列表
        listDnatRules(client);

        // 5. 删除公网DNAT规则
        deleteDnatRule(client, createNatGatewayDnatRuleResponse.getDnatRule().getId());

        // 6. 批量创建公网DNAT规则
        batchCreateDnatRules(client);
    }

    /**
     *  创建公网DNAT规则
     */
    public static CreateNatGatewayDnatRuleResponse createDnatRule(NatClient client) {
        CreateNatGatewayDnatRuleRequest request = new CreateNatGatewayDnatRuleRequest()
            .withBody(new CreateNatGatewayDnatRuleOption()
                .withDnatRule(new CreateNatGatewayDnatOption()
                    .withNatGatewayId("<nat gateway id>")
                    .withDescription("<dnat rule description>")
                    .withProtocol("<protocol>")
                    .withFloatingIpId("<floating ip id>")
                    .withPortId("<port id>")
                    .withInternalServicePort(993)
                    .withExternalServicePort(242)
                ));

        CreateNatGatewayDnatRuleResponse response = null;
        try {
            response = client.createNatGatewayDnatRule(request);
            logger.info("create dnat rule response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("create dnat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("create dnat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("create dnat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("create dnat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("create dnat rule ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return response;
    }

    /**
     *  查询指定的公网DNAT规则详情
     */
    public static void showDnatRule(NatClient client, String dnatRuleId) {
        ShowNatGatewayDnatRuleRequest request = new ShowNatGatewayDnatRuleRequest().withDnatRuleId(dnatRuleId);

        try {
            ShowNatGatewayDnatRuleResponse response = client.showNatGatewayDnatRule(request);
            logger.info("show dnat rule response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("show dnat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("show dnat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("show dnat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("show dnat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("show dnat rule ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  更新公网DNAT规则
     */
    public static void updateDnatRule(NatClient client, String dnatRuleId) {
        UpdateNatGatewayDnatRuleRequest request = new UpdateNatGatewayDnatRuleRequest()
            .withDnatRuleId(dnatRuleId)
            .withBody(new UpdateNatGatewayDnatRuleRequestBody()
                .withDnatRule(new UpdateNatGatewayDnatRuleOption()
                    .withNatGatewayId("<nat gateway id>")
                    .withDescription("<new description>")
                    .withFloatingIpId("<floating ip id>")
                    .withProtocol(UpdateNatGatewayDnatRuleOption.ProtocolEnum.UDP)
                    .withExternalServicePort(22)
                    .withInternalServicePort(33)
                    .withPortId("<port id>")));

        try {
            UpdateNatGatewayDnatRuleResponse response = client.updateNatGatewayDnatRule(request);
            logger.info("update dnat rule response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("update dnat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("update dnat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("update dnat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("update dnat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("update dnat rule ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  查询公网DNAT规则列表
     */
    private static void listDnatRules(NatClient client) {
        ListNatGatewayDnatRulesRequest request = new ListNatGatewayDnatRulesRequest();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            ListNatGatewayDnatRulesResponse response = client.listNatGatewayDnatRules(request);
            logger.info("list dnat rules response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("list dnat rules ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("list dnat rules ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("list dnat rules ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("list dnat rules ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("list dnat rules ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  删除公网DNAT规则
     */
    private static void deleteDnatRule(NatClient client, String dnatRuleId) {
        DeleteNatGatewayDnatRuleRequest request = new DeleteNatGatewayDnatRuleRequest()
            .withNatGatewayId("<nat gateway id>")
            .withDnatRuleId(dnatRuleId);

        try {
            DeleteNatGatewayDnatRuleResponse response = client.deleteNatGatewayDnatRule(request);
            logger.info("delete dnat rule HTTP Status Code is: {}", response.getHttpStatusCode());
        } catch (ConnectionException e) {
            logger.error("delete dnat rule ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("delete dnat rule ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("delete dnat rule ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("delete dnat rule ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("delete dnat rule ServerResponseException is: {}", e.getMessage());
        }
    }

    /**
     *  批量创建公网DNAT规则
     */
    private static void batchCreateDnatRules(NatClient client) {
        BatchCreateNatGatewayDnatRulesRequest request = new BatchCreateNatGatewayDnatRulesRequest()
            .withBody(new BatchCreateNatGatewayDnatRulesRequestBody()
                .withDnatRules(Collections.singletonList(new CreateNatGatewayDnatOption()
                    .withNatGatewayId("<nat gateway id>")
                    .withDescription("<dnat rule description>")
                    .withProtocol("<protocol>")
                    .withFloatingIpId("<floating ip id>")
                    .withPortId("<port id>")
                    .withInternalServicePort(993)
                    .withExternalServicePort(242))));

        try {
            BatchCreateNatGatewayDnatRulesResponse response = client.batchCreateNatGatewayDnatRules(request);
            logger.info("batch create dnat rules response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("batch create ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("batch create ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("batch create ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("batch create ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("batch create ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}