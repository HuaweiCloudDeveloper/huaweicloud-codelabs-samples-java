package com.huaweicloud.nat.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.nat.v2.NatClient;
import com.huaweicloud.sdk.nat.v2.model.CreateNatGatewayOption;
import com.huaweicloud.sdk.nat.v2.model.CreateNatGatewayRequest;
import com.huaweicloud.sdk.nat.v2.model.CreateNatGatewayRequestBody;
import com.huaweicloud.sdk.nat.v2.model.CreateNatGatewayResponse;
import com.huaweicloud.sdk.nat.v2.model.DeleteNatGatewayRequest;
import com.huaweicloud.sdk.nat.v2.model.DeleteNatGatewayResponse;
import com.huaweicloud.sdk.nat.v2.model.ListNatGatewaysRequest;
import com.huaweicloud.sdk.nat.v2.model.ListNatGatewaysResponse;
import com.huaweicloud.sdk.nat.v2.model.SessionConfiguration;
import com.huaweicloud.sdk.nat.v2.model.ShowNatGatewayRequest;
import com.huaweicloud.sdk.nat.v2.model.ShowNatGatewayResponse;
import com.huaweicloud.sdk.nat.v2.model.UpdateNatGatewayOption;
import com.huaweicloud.sdk.nat.v2.model.UpdateNatGatewayRequest;
import com.huaweicloud.sdk.nat.v2.model.UpdateNatGatewayRequestBody;
import com.huaweicloud.sdk.nat.v2.model.UpdateNatGatewayResponse;
import com.huaweicloud.sdk.nat.v2.region.NatRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  公网NAT网关基本操作
 */
public class NatGatewayDemo {

    private static final Logger logger = LoggerFactory.getLogger(NatGatewayDemo.class);

    public static void main(String[] args) {

        // 华为云账号Access Key, Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 创建认证
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // 创建NatClient实例
        NatClient client = NatClient.newBuilder()
            .withCredential(auth)
            .withRegion(NatRegion.CN_NORTH_4)
            .build();

        // 1. 创建公网NAT网关
        CreateNatGatewayResponse createNatGatewayResponse = createNatGateway(client);

        // 2. 查询指定的公网NAT网关详情
        showNatGateway(client, createNatGatewayResponse.getNatGateway().getId());

        // 3. 更新公网NAT网关
        updateNatGateway(client, createNatGatewayResponse.getNatGateway().getId());

        // 4. 查询公网NAT网关列表
        listNatGateways(client);

        // 5. 删除公网NAT网关
        deleteNatGateway(client, createNatGatewayResponse.getNatGateway().getId());
    }

    /**
     *  创建公网NAT网关
     */
    public static CreateNatGatewayResponse createNatGateway(NatClient client) {
        CreateNatGatewayRequest request = new CreateNatGatewayRequest()
            .withBody(new CreateNatGatewayRequestBody()
                .withNatGateway(new CreateNatGatewayOption()
                    .withName("<nat gateway name>")
                    .withDescription("<nat gateway description>")
                    .withEnterpriseProjectId("0")
                    .withRouterId("<router id>")
                    .withInternalNetworkId("<internal network id>")
                    .withSpec(CreateNatGatewayOption.SpecEnum._1)
                    // 会话参数配置，非必选
                    .withSessionConf(new SessionConfiguration()
                        .withIcmpSessionExpireTime(20)
                        .withTcpSessionExpireTime(50)
                        .withUdpSessionExpireTime(50)
                        .withTcpTimeWaitTime(10))));

        CreateNatGatewayResponse response = null;
        try {
            response = client.createNatGateway(request);
            logger.info("create nat gateway response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("create nat gateway ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("create nat gateway ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("create nat gateway ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("create nat gateway ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("create nat gateway ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return response;
    }

    /**
     *  查询指定的公网NAT网关详情
     */
    public static void showNatGateway(NatClient client, String natGatewayId) {
        ShowNatGatewayRequest request = new ShowNatGatewayRequest().withNatGatewayId(natGatewayId);

        try {
            ShowNatGatewayResponse response = client.showNatGateway(request);
            logger.info("show nat gateway response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("show nat gateway ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("show nat gateway ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("show nat gateway ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("show nat gateway ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("show nat gateway ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  更新公网NAT网关
     */
    public static void updateNatGateway(NatClient client, String natGatewayId) {
        UpdateNatGatewayRequest request = new UpdateNatGatewayRequest()
            .withNatGatewayId(natGatewayId)
            .withBody(new UpdateNatGatewayRequestBody()
                .withNatGateway(new UpdateNatGatewayOption()
                    .withName("<new nat gateway name>")
                    .withDescription("<new nat gateway description>")
                    .withSpec(UpdateNatGatewayOption.SpecEnum._2)
                    // 会话参数配置，非必选
                    .withSessionConf(new SessionConfiguration()
                        .withIcmpSessionExpireTime(30)
                        .withTcpSessionExpireTime(60)
                        .withUdpSessionExpireTime(60)
                        .withTcpTimeWaitTime(20))));

        try {
            UpdateNatGatewayResponse response = client.updateNatGateway(request);
            logger.info("update nat gateway response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("update nat gateway ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("update nat gateway ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("update nat gateway ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("update nat gateway ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("update nat gateway ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  查询公网NAT网关列表
     */
    public static void listNatGateways(NatClient client) {
        ListNatGatewaysRequest request = new ListNatGatewaysRequest();

        try {
            ListNatGatewaysResponse response = client.listNatGateways(request);
            logger.info("list nat gateways response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("list nat gateways ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("list nat gateways ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("list nat gateways ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("list nat gateways ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("list nat gateways ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  删除公网NAT网关
     */
    public static void deleteNatGateway(NatClient client, String natGatewayId) {
        DeleteNatGatewayRequest request = new DeleteNatGatewayRequest().withNatGatewayId(natGatewayId);

        try {
            DeleteNatGatewayResponse response = client.deleteNatGateway(request);
            logger.info("delete nat gateway HTTP Status Code is: {}", response.getHttpStatusCode());
        } catch (ConnectionException e) {
            logger.error("delete nat gateway ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("delete nat gateway ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("delete nat gateway ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("delete nat gateway ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("delete nat gateway ServerResponseException is: {}", e.getMessage());
        }
    }
}