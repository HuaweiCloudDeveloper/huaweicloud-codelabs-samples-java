## 1. Introduction

Huawei Cloud provides NAT Gateway SDKs. You can integrate the SDKs to call NAT Gateway APIs to quickly perform operations on NAT Gateway.

This example shows how to use NAT Gateway SDKs to create, show, update, and delete a public NAT gateway as well as querying NAT gateways.

## 2. Prerequisites

- You have obtained and installed the Huawei Cloud SDK, including the JAVA SDK of NAT Gateway. For details, see [NAT JAVA SDK](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter/NAT?lang=Java).
- To use the Huawei Cloud Java SDK, you must have a Huawei Cloud account and obtain the AK and SK of the account. For details, see [Obtaining an AK/SK](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).
- The Huawei Cloud Java SDK can be used in **Java JDK 1.8** or later.

## 3. Obtaining and Installing the SDK

You can use Maven to configure the NAT Gateway SDK.

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-nat</artifactId>
    <version>3.1.60</version>
</dependency>
```

## 4. Key Code Snippets
```java
/**
 * Basic Operations on a Public NAT Gateway
 */
public class NatGatewayDemo {

    private static final Logger logger = LoggerFactory.getLogger(NatGatewayDemo.class);

    public static void main(String[] args) {

        // AK and SK of the Huawei Cloud account
        // There will be security risks if the AK and SK used for authentication is written into code. Encrypt the AK/SK in the configuration file or environment variables for storage
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in your environment
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Create a credential
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // Create a NAT Gateway client
        NatClient client = NatClient.newBuilder()
            .withCredential(auth)
            .withRegion(NatRegion.CN_NORTH_4)
            .build();

        // 1. Creating a public NAT gateway
        CreateNatGatewayResponse createNatGatewayResponse = createNatGateway(client);

        // 2. Querying details of a public NAT gateway
        showNatGateway(client, createNatGatewayResponse.getNatGateway().getId());

        // 3. Updating a public NAT gateway
        updateNatGateway(client, createNatGatewayResponse.getNatGateway().getId());

        // 4. Querying public NAT gateways
        listNatGateways(client);

        // 5. Deleting a public NAT gateway
        deleteNatGateway(client, createNatGatewayResponse.getNatGateway().getId());
    }

    /**
     * Creating a public NAT gateway
     */
    public static CreateNatGatewayResponse createNatGateway(NatClient client) {
        CreateNatGatewayRequest request = new CreateNatGatewayRequest()
            .withBody(new CreateNatGatewayRequestBody()
                .withNatGateway(new CreateNatGatewayOption()
                    .withName("<nat gateway name>")
                    .withDescription("<nat gateway description>")
                    .withEnterpriseProjectId("0")
                    .withRouterId("<router id>")
                    .withInternalNetworkId("<internal network id>")
                    .withSpec(CreateNatGatewayOption.SpecEnum._1)
                    // (Optional) Session parameters
                    .withSessionConf(new SessionConfiguration()
                        .withIcmpSessionExpireTime(20)
                        .withTcpSessionExpireTime(50)
                        .withUdpSessionExpireTime(50)
                        .withTcpTimeWaitTime(10))));

        CreateNatGatewayResponse response = null;
        try {
            response = client.createNatGateway(request);
            logger.info("create nat gateway response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("create nat gateway ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("create nat gateway ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("create nat gateway ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("create nat gateway ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("create nat gateway ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return response;
    }

    /**
     * Querying details of a public NAT gateway
     */
    public static void showNatGateway(NatClient client, String natGatewayId) {
        ShowNatGatewayRequest request = new ShowNatGatewayRequest().withNatGatewayId(natGatewayId);

        try {
            ShowNatGatewayResponse response = client.showNatGateway(request);
            logger.info("show nat gateway response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("show nat gateway ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("show nat gateway ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("show nat gateway ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("show nat gateway ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("show nat gateway ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Updating a public NAT gateway
     */
    public static void updateNatGateway(NatClient client, String natGatewayId) {
        UpdateNatGatewayRequest request = new UpdateNatGatewayRequest()
            .withNatGatewayId(natGatewayId)
            .withBody(new UpdateNatGatewayRequestBody()
                .withNatGateway(new UpdateNatGatewayOption()
                    .withName("<new nat gateway name>")
                    .withDescription("<new nat gateway description>")
                    .withSpec(UpdateNatGatewayOption.SpecEnum._2)
                    // (Optional) Session parameter configuration
                    .withSessionConf(new SessionConfiguration()
                        .withIcmpSessionExpireTime(30)
                        .withTcpSessionExpireTime(60)
                        .withUdpSessionExpireTime(60)
                        .withTcpTimeWaitTime(20))));

        try {
            UpdateNatGatewayResponse response = client.updateNatGateway(request);
            logger.info("update nat gateway response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("update nat gateway ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("update nat gateway ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("update nat gateway ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("update nat gateway ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("update nat gateway ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Querying public NAT gateways
     */
    public static void listNatGateways(NatClient client) {
        ListNatGatewaysRequest request = new ListNatGatewaysRequest();

        try {
            ListNatGatewaysResponse response = client.listNatGateways(request);
            logger.info("list nat gateways response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("list nat gateways ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("list nat gateways ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("list nat gateways ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("list nat gateways ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("list nat gateways ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Deleting a public NAT gateway
     */
    public static void deleteNatGateway(NatClient client, String natGatewayId) {
        DeleteNatGatewayRequest request = new DeleteNatGatewayRequest().withNatGatewayId(natGatewayId);

        try {
            DeleteNatGatewayResponse response = client.deleteNatGateway(request);
            logger.info("delete nat gateway HTTP Status Code is: {}", response.getHttpStatusCode());
        } catch (ConnectionException e) {
            logger.error("delete nat gateway ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("delete nat gateway ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("delete nat gateway ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("delete nat gateway ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("delete nat gateway ServerResponseException is: {}", e.getMessage());
        }
    }
}
```

## 5. Sample Return Values

Creating a public NAT gateway

```json
{
  "nat_gateway" : {
    "id" : "b3dbab6a-7611-4de7-9a47-1bbe4c9a9618",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "name" : "your-nat-gateway-name",
    "description" : "your nat gateway description",
    "spec" : "1",
    "status" : "PENDING_CREATE",
    "admin_state_up" : true,
    "created_at" : "2023-10-09 03:32:59.000000",
    "router_id" : "569345aa-7270-61f3-a515-ead05a111a35",
    "internal_network_id" : "bfed98bd-e807-4943-a089-57fefda4f6a6",
    "enterprise_project_id" : "0",
    "session_conf" : {
      "tcp_session_expire_time" : 50,
      "udp_session_expire_time" : 50,
      "icmp_session_expire_time" : 20,
      "tcp_time_wait_time" : 10
    }
  }
}
```

Querying details of a public NAT gateway

```json
{
  "nat_gateway" : {
    "id" : "b3dbab6a-7611-4de7-9a47-1bbe4c9a9618",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "name" : "your-nat-gateway-name",
    "description" : "your nat gateway description",
    "spec" : "1",
    "status" : "PENDING_CREATE",
    "admin_state_up" : true,
    "created_at" : "2023-10-09 03:32:59.000000",
    "router_id" : "569345aa-7270-61f3-a515-ead05a111a35",
    "internal_network_id" : "bfed98bd-e807-4943-a089-57fefda4f6a6",
    "enterprise_project_id" : "0",
    "session_conf" : {
      "tcp_session_expire_time" : 50,
      "udp_session_expire_time" : 50,
      "icmp_session_expire_time" : 20,
      "tcp_time_wait_time" : 10
    }
  }
}
```

Updating a public NAT gateway

```json
{
  "nat_gateway" : {
    "id" : "b3dbab6a-7611-4de7-9a47-1bbe4c9a9618",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "name" : "your-new-nat-gateway-name",
    "description" : "your new nat gateway description",
    "spec" : "2",
    "status" : "PENDING_UPDATE",
    "admin_state_up" : true,
    "created_at" : "2023-10-09 03:32:59.000000",
    "router_id" : "569345aa-7270-61f3-a515-ead05a111a35",
    "internal_network_id" : "bfed98bd-e807-4943-a089-57fefda4f6a6",
    "enterprise_project_id" : "0",
    "session_conf" : {
      "tcp_session_expire_time" : 60,
      "udp_session_expire_time" : 60,
      "icmp_session_expire_time" : 30,
      "tcp_time_wait_time" : 20
    }
  }
}
```

Querying public NAT gateways

```json
{
  "nat_gateways" : [ {
    "id" : "b3dbab6a-7611-4de7-9a47-1bbe4c9a9618",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "name" : "your-nat-gateway-name",
    "description" : "your nat gateway description",
    "spec" : "1",
    "status" : "PENDING_CREATE",
    "admin_state_up" : true,
    "created_at" : "2023-10-09 03:32:59.000000",
    "router_id" : "569345aa-7270-61f3-a515-ead05a111a35",
    "internal_network_id" : "bfed98bd-e807-4943-a089-57fefda4f6a6",
    "enterprise_project_id" : "0",
    "session_conf" : {
      "tcp_session_expire_time" : 50,
      "udp_session_expire_time" : 50,
      "icmp_session_expire_time" : 20,
      "tcp_time_wait_time" : 10
    }
  } ]
}
```