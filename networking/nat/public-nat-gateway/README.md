## 1、功能介绍

华为云提供了NAT网关（NAT Gateway）服务SDK，您可以直接集成SDK来调用NAT网关相关API，从而实现对NAT网关服务的快速操作。

本示例展示如何通过NAT网关（NAT Gateway）相关SDK实践公网NAT网关创建、查询指定资源详情、更新、查询资源列表、删除的功能。

## 2、前置条件

- 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。具体请参见[NAT JAVA SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/NAT?lang=Java)。
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。具体请参见[AK SK获取](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)。
- 华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。

## 3、SDK获取和安装

您可以通过Maven配置所依赖的NAT网关服务SDK

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-nat</artifactId>
    <version>3.1.60</version>
</dependency>
```

## 4、关键代码片段
```java
/**
 *  公网NAT网关基本操作
 */
public class NatGatewayDemo {

    private static final Logger logger = LoggerFactory.getLogger(NatGatewayDemo.class);

    public static void main(String[] args) {

        // 华为云账号Access Key, Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 创建认证
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // 创建NatClient实例
        NatClient client = NatClient.newBuilder()
            .withCredential(auth)
            .withRegion(NatRegion.CN_NORTH_4)
            .build();

        // 1. 创建公网NAT网关
        CreateNatGatewayResponse createNatGatewayResponse = createNatGateway(client);

        // 2. 查询指定的公网NAT网关详情
        showNatGateway(client, createNatGatewayResponse.getNatGateway().getId());

        // 3. 更新公网NAT网关
        updateNatGateway(client, createNatGatewayResponse.getNatGateway().getId());

        // 4. 查询公网NAT网关列表
        listNatGateways(client);

        // 5. 删除公网NAT网关
        deleteNatGateway(client, createNatGatewayResponse.getNatGateway().getId());
    }

    /**
     *  创建公网NAT网关
     */
    public static CreateNatGatewayResponse createNatGateway(NatClient client) {
        CreateNatGatewayRequest request = new CreateNatGatewayRequest()
            .withBody(new CreateNatGatewayRequestBody()
                .withNatGateway(new CreateNatGatewayOption()
                    .withName("<nat gateway name>")
                    .withDescription("<nat gateway description>")
                    .withEnterpriseProjectId("0")
                    .withRouterId("<router id>")
                    .withInternalNetworkId("<internal network id>")
                    .withSpec(CreateNatGatewayOption.SpecEnum._1)
                    // 会话参数配置，非必选
                    .withSessionConf(new SessionConfiguration()
                        .withIcmpSessionExpireTime(20)
                        .withTcpSessionExpireTime(50)
                        .withUdpSessionExpireTime(50)
                        .withTcpTimeWaitTime(10))));

        CreateNatGatewayResponse response = null;
        try {
            response = client.createNatGateway(request);
            logger.info("create nat gateway response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("create nat gateway ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("create nat gateway ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("create nat gateway ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("create nat gateway ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("create nat gateway ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return response;
    }

    /**
     *  查询指定的公网NAT网关详情
     */
    public static void showNatGateway(NatClient client, String natGatewayId) {
        ShowNatGatewayRequest request = new ShowNatGatewayRequest().withNatGatewayId(natGatewayId);

        try {
            ShowNatGatewayResponse response = client.showNatGateway(request);
            logger.info("show nat gateway response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("show nat gateway ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("show nat gateway ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("show nat gateway ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("show nat gateway ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("show nat gateway ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  更新公网NAT网关
     */
    public static void updateNatGateway(NatClient client, String natGatewayId) {
        UpdateNatGatewayRequest request = new UpdateNatGatewayRequest()
            .withNatGatewayId(natGatewayId)
            .withBody(new UpdateNatGatewayRequestBody()
                .withNatGateway(new UpdateNatGatewayOption()
                    .withName("<new nat gateway name>")
                    .withDescription("<new nat gateway description>")
                    .withSpec(UpdateNatGatewayOption.SpecEnum._2)
                    // 会话参数配置，非必选
                    .withSessionConf(new SessionConfiguration()
                        .withIcmpSessionExpireTime(30)
                        .withTcpSessionExpireTime(60)
                        .withUdpSessionExpireTime(60)
                        .withTcpTimeWaitTime(20))));

        try {
            UpdateNatGatewayResponse response = client.updateNatGateway(request);
            logger.info("update nat gateway response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("update nat gateway ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("update nat gateway ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("update nat gateway ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("update nat gateway ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("update nat gateway ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  查询公网NAT网关列表
     */
    public static void listNatGateways(NatClient client) {
        ListNatGatewaysRequest request = new ListNatGatewaysRequest();

        try {
            ListNatGatewaysResponse response = client.listNatGateways(request);
            logger.info("list nat gateways response is: {}",
                new ObjectMapper()
                    .enable(SerializationFeature.INDENT_OUTPUT)
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(response));
        } catch (ConnectionException e) {
            logger.error("list nat gateways ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("list nat gateways ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("list nat gateways ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("list nat gateways ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("list nat gateways ServerResponseException is: {}", e.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  删除公网NAT网关
     */
    public static void deleteNatGateway(NatClient client, String natGatewayId) {
        DeleteNatGatewayRequest request = new DeleteNatGatewayRequest().withNatGatewayId(natGatewayId);

        try {
            DeleteNatGatewayResponse response = client.deleteNatGateway(request);
            logger.info("delete nat gateway HTTP Status Code is: {}", response.getHttpStatusCode());
        } catch (ConnectionException e) {
            logger.error("delete nat gateway ConnectionException is: {}", e.getMessage());
        } catch (ClientRequestException e) {
            logger.error("delete nat gateway ClientRequestException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("delete nat gateway ClientRequestException is: {}", e.getMessage());
        } catch (ServerResponseException e) {
            logger.error("delete nat gateway ServerResponseException HTTP Status Code is: {}", e.getHttpStatusCode());
            logger.error("delete nat gateway ServerResponseException is: {}", e.getMessage());
        }
    }
}
```

## 5、返回结果示例

创建公网NAT网关

```json
{
  "nat_gateway" : {
    "id" : "b3dbab6a-7611-4de7-9a47-1bbe4c9a9618",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "name" : "your-nat-gateway-name",
    "description" : "your nat gateway description",
    "spec" : "1",
    "status" : "PENDING_CREATE",
    "admin_state_up" : true,
    "created_at" : "2023-10-09 03:32:59.000000",
    "router_id" : "569345aa-7270-61f3-a515-ead05a111a35",
    "internal_network_id" : "bfed98bd-e807-4943-a089-57fefda4f6a6",
    "enterprise_project_id" : "0",
    "session_conf" : {
      "tcp_session_expire_time" : 50,
      "udp_session_expire_time" : 50,
      "icmp_session_expire_time" : 20,
      "tcp_time_wait_time" : 10
    }
  }
}
```

查询指定的公网NAT网关详情

```json
{
  "nat_gateway" : {
    "id" : "b3dbab6a-7611-4de7-9a47-1bbe4c9a9618",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "name" : "your-nat-gateway-name",
    "description" : "your nat gateway description",
    "spec" : "1",
    "status" : "PENDING_CREATE",
    "admin_state_up" : true,
    "created_at" : "2023-10-09 03:32:59.000000",
    "router_id" : "569345aa-7270-61f3-a515-ead05a111a35",
    "internal_network_id" : "bfed98bd-e807-4943-a089-57fefda4f6a6",
    "enterprise_project_id" : "0",
    "session_conf" : {
      "tcp_session_expire_time" : 50,
      "udp_session_expire_time" : 50,
      "icmp_session_expire_time" : 20,
      "tcp_time_wait_time" : 10
    }
  }
}
```

更新公网NAT网关

```json
{
  "nat_gateway" : {
    "id" : "b3dbab6a-7611-4de7-9a47-1bbe4c9a9618",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "name" : "your-new-nat-gateway-name",
    "description" : "your new nat gateway description",
    "spec" : "2",
    "status" : "PENDING_UPDATE",
    "admin_state_up" : true,
    "created_at" : "2023-10-09 03:32:59.000000",
    "router_id" : "569345aa-7270-61f3-a515-ead05a111a35",
    "internal_network_id" : "bfed98bd-e807-4943-a089-57fefda4f6a6",
    "enterprise_project_id" : "0",
    "session_conf" : {
      "tcp_session_expire_time" : 60,
      "udp_session_expire_time" : 60,
      "icmp_session_expire_time" : 30,
      "tcp_time_wait_time" : 20
    }
  }
}
```

查询公网NAT网关列表

```json
{
  "nat_gateways" : [ {
    "id" : "b3dbab6a-7611-4de7-9a47-1bbe4c9a9618",
    "tenant_id" : "8g15bf26f69026472f70c003565ee66d",
    "name" : "your-nat-gateway-name",
    "description" : "your nat gateway description",
    "spec" : "1",
    "status" : "PENDING_CREATE",
    "admin_state_up" : true,
    "created_at" : "2023-10-09 03:32:59.000000",
    "router_id" : "569345aa-7270-61f3-a515-ead05a111a35",
    "internal_network_id" : "bfed98bd-e807-4943-a089-57fefda4f6a6",
    "enterprise_project_id" : "0",
    "session_conf" : {
      "tcp_session_expire_time" : 50,
      "udp_session_expire_time" : 50,
      "icmp_session_expire_time" : 20,
      "tcp_time_wait_time" : 10
    }
  } ]
}
```