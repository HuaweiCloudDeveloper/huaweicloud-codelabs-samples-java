package com.huawei.gsl;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.gsl.v3.GslClient;
import com.huaweicloud.sdk.gsl.v3.model.AddOrModifyTagReq;
import com.huaweicloud.sdk.gsl.v3.model.BatchSetTagsReq;
import com.huaweicloud.sdk.gsl.v3.model.BatchSetTagsRequest;
import com.huaweicloud.sdk.gsl.v3.model.BatchSetTagsResponse;
import com.huaweicloud.sdk.gsl.v3.model.CreateTagRequest;
import com.huaweicloud.sdk.gsl.v3.model.CreateTagResponse;
import com.huaweicloud.sdk.gsl.v3.model.DeleteTagRequest;
import com.huaweicloud.sdk.gsl.v3.model.DeleteTagResponse;
import com.huaweicloud.sdk.gsl.v3.model.ListTagsRequest;
import com.huaweicloud.sdk.gsl.v3.model.ListTagsResponse;
import com.huaweicloud.sdk.gsl.v3.region.GslRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SimTagApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimTagApplication.class);

    /**
     * - IAM_ENDPOINT: 华为云IAM服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?IAM
     */
    private static final String IAM_ENDPOINT = "https://<IamEndpoint>";

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 创建认证
        BasicCredentials auth = new BasicCredentials()
            .withIamEndpoint(IAM_ENDPOINT)
            .withAk(ak)
            .withSk(sk);

        GslClient gslClient = GslClient.newBuilder()
            .withCredential(auth)
            .withRegion(GslRegion.CN_NORTH_4)
            .build();

        // 创建SIM卡标签
        createTag(gslClient);
        // 查询SIM卡标签列表
        listTags(gslClient);
        // 批量绑定/取消SIM卡
        batchSetTags(gslClient);
        // 删除SIM卡标签
        deleteTag(gslClient);

    }

    private static void createTag(GslClient gslClient) {
        String tagName = "SDK测试标签";
        try {
            CreateTagResponse response = gslClient.createTag(
                new CreateTagRequest().withBody(
                    new AddOrModifyTagReq().withTagName(tagName)));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    private static void listTags(GslClient gslClient) {
        // 需要查询的标签名，如果为空则查询全部标签
        String tagName = "SDK测试标签";
        Long limit = 10L;
        Long offset = 1L;
        // 标签状态，0表示未使用， 1表示使用中
        Integer status = 0;
        try {
            ListTagsResponse response = gslClient.listTags(
                new ListTagsRequest().withTagName(tagName)
                    .withLimit(limit)
                    .withOffset(offset)
                    .withStatus(status));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    private static void batchSetTags(GslClient gslClient) {
        // 需要绑定标签的SIM卡列表
        List<Long> simCardIds = new ArrayList<>();
        simCardIds.add(3449878744318976L);
        simCardIds.add(3449878744318977L);

        // SIM卡对应的标签列表，会清除SIM上原有标签，绑定新的标签。如果标签列表为空，表示清空sim卡绑定的标签。
        List<Long> tagIds = new ArrayList<>();
        tagIds.add(12L);
        tagIds.add(13L);
        try {
            BatchSetTagsResponse response = gslClient.batchSetTags(
                new BatchSetTagsRequest().withBody(
                    new BatchSetTagsReq()
                        .withSimCardIds(simCardIds)
                        .withTagIds(tagIds)));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    private static void deleteTag(GslClient gslClient) {
        Long tagId = 12L;
        try {
            DeleteTagResponse response = gslClient.deleteTag(
                new DeleteTagRequest().withTagId(tagId));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
}
