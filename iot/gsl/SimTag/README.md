## 0. 版本说明
   本示例基于华为云Java SDK V3.0版本开发
## 1. 简介
   基于华为云Java SDK, 与SIM卡标签相关的操作
## 2. 开发前准备
   已注册华为云, 并完成实名认证;
   已获取华为云账号对应的Access Key（AK）和Secret Access Key(SK);
   Java JDK 1.8及其以上版本
## 3. 安装SDK
   通过 Maven 安装项目依赖是使用 Java SDK 的推荐方法，首先您需要在您的操作系统中下载并安装 Maven ，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
   本示例使用GSL JDK
```xml
   <dependency>
   <groupId>com.huaweicloud.sdk</groupId>
   <artifactId>huaweicloud-sdk-gsl</artifactId>
   <version>3.1.64</version>
   </dependency>
```
## 4. 开始使用
   ### 4.1 导入依赖模块
```java
   import com.huaweicloud.sdk.core.exception.ClientRequestException;
   // 导入相应产品的 {Service}Client
   import com.huaweicloud.sdk.gsl.v3.GslClient;
   // 导入待请求接口的 request 和 response 类
   import com.huaweicloud.sdk.gsl.v3.model.*;
   // 导入Region
   import com.huaweicloud.sdk.gsl.v3.region.GslRegion;
   // 日志打印
   import org.slf4j.Logger;
   import org.slf4j.LoggerFactory;
```
   ### 4.2.1 初始化认证信息
```java 
   BasicCredentials auth = new BasicCredentials()
        .withIamEndpoint(IAM_ENDPOINT)
        .withAk(ak)
        .withSk(sk);
```
IAM_ENDPOINT: 华为云IAM服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?IAM
### 4.2.2 初始化客户端
```java 
   GslClient gslClient = GslClient.newBuilder()
        .withRegion(GslRegion.CN_NORTH_4)
        .withCredential(auth)
        .build();
```
## 5. 示例代码
   使用如下代码进行SIM卡标签相关的操作，调用前请根据实际情况替换变量：
```java
public class SimTagApplication {
   private static final Logger LOGGER = LoggerFactory.getLogger(SimTagApplication.class);

   /**
    * - IAM_ENDPOINT: 华为云IAM服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?IAM
    */
   private static final String IAM_ENDPOINT = "https://<IamEndpoint>";

   public static void main(String[] args) {
      // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
      // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
      String ak = System.getenv("HUAWEICLOUD_SDK_AK");
      String sk = System.getenv("HUAWEICLOUD_SDK_SK");

      // 创建认证
      BasicCredentials auth = new BasicCredentials()
              .withIamEndpoint(IAM_ENDPOINT)
              .withAk(ak)
              .withSk(sk);

      GslClient gslClient = GslClient.newBuilder()
              .withCredential(auth)
              .withRegion(GslRegion.CN_NORTH_4)
              .build();

      // 创建SIM卡标签
      createTag(gslClient);
      // 查询SIM卡标签列表
      listTags(gslClient);
      // 批量绑定/取消SIM卡
      batchSetTags(gslClient);
      // 删除SIM卡标签
      deleteTag(gslClient);

   }

   private static void createTag(GslClient gslClient) {
      String tagName = "SDK测试标签";
      try {
         CreateTagResponse response = gslClient.createTag(
                 new CreateTagRequest().withBody(
                         new AddOrModifyTagReq().withTagName(tagName)));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void listTags(GslClient gslClient) {
      // 需要查询的标签名，如果为空则查询全部标签
      String tagName = "SDK测试标签";
      Long limit = 10L;
      Long offset = 1L;
      // 标签状态，0表示未使用， 1表示使用中
      Integer status = 0;
      try {
         ListTagsResponse response = gslClient.listTags(
                 new ListTagsRequest().withTagName(tagName)
                         .withLimit(limit)
                         .withOffset(offset)
                         .withStatus(status));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void batchSetTags(GslClient gslClient) {
      // 需要绑定标签的SIM卡列表
      List<Long> simCardIds = new ArrayList<>();
      simCardIds.add(3449878744318976L);
      simCardIds.add(3449878744318977L);

      // SIM卡对应的标签列表，会清除SIM上原有标签，绑定新的标签。如果标签列表为空，表示清空sim卡绑定的标签。
      List<Long> tagIds = new ArrayList<>();
      tagIds.add(12L);
      tagIds.add(13L);
      try {
         BatchSetTagsResponse response = gslClient.batchSetTags(
                 new BatchSetTagsRequest().withBody(
                         new BatchSetTagsReq()
                                 .withSimCardIds(simCardIds)
                                 .withTagIds(tagIds)));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void deleteTag(GslClient gslClient) {
      Long tagId = 12L;
      try {
         DeleteTagResponse response = gslClient.deleteTag(
                 new DeleteTagRequest().withTagId(tagId));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }
}

```
## 6. 运行结果
调用listTags接口查询标签列表：
```http request
GET https://{endpoint}/v1/tags?offset=1&limit=10
```
返回结果：
```json
{
   "limit": 10,
   "offset": 1,
   "count": 1,
   "tags": [
      {
         "id": 5510069767624000,
         "tag_name": "测试标签",
         "status": 0
      }
   ]
}
```
对应返回的标签id可以用于其他SIM卡标签操作的入参。
## 7. 参考
更多信息请参考[全球SIM联接 GSL](https://support.huaweicloud.com/qs-ocgsl/oceanlink_03_0001.html)
## 8. 修订记录
|  发布日期  | 文档版本  |   修订说明   |
| :--------: |:-----:| :----------: |
| 2022-06-12 | 1.0.1 | 文档首次发布 |
| 2022-08-02 | 1.0.2 | 更新依赖 |
| 2023-11-08 | 1.0.3 |          更新文档           |