package com.huawei.gsl;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.gsl.v3.GslClient;
import com.huaweicloud.sdk.gsl.v3.model.ListFlowBySimCardsReq;
import com.huaweicloud.sdk.gsl.v3.model.ListFlowBySimCardsRequest;
import com.huaweicloud.sdk.gsl.v3.model.ListFlowBySimCardsResponse;
import com.huaweicloud.sdk.gsl.v3.model.ListSimPricePlansRequest;
import com.huaweicloud.sdk.gsl.v3.model.ListSimPricePlansResponse;
import com.huaweicloud.sdk.gsl.v3.region.GslRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SimPricePlansApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimPricePlansApplication.class);

    /**
     * - IAM_ENDPOINT: 华为云IAM服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?IAM
     */
    private static final String IAM_ENDPOINT = "https://<IamEndpoint>";

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 创建认证
        BasicCredentials auth = new BasicCredentials()
            .withIamEndpoint(IAM_ENDPOINT)
            .withAk(ak)
            .withSk(sk);

        GslClient gslClient = GslClient.newBuilder()
            .withCredential(auth)
            .withRegion(GslRegion.CN_NORTH_4)
            .build();
        
       
        // 批量查询实体卡流量
        listFlowBySimCards(gslClient);
        // sim卡套餐列表查询
        listSimPricePlans(gslClient);
        
    }

    private static void listFlowBySimCards(GslClient gslClient) {
        List<String> iccids = new ArrayList<>();
        iccids.add("898602B4151880002907");
        iccids.add("89860317422045106999");
        iccids.add("898602B4151880002849");
        try {
            ListFlowBySimCardsResponse response = gslClient.listFlowBySimCards(
                new ListFlowBySimCardsRequest().withBody(new ListFlowBySimCardsReq().withIccids(iccids)));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
    
    private static void listSimPricePlans(GslClient gslClient) {
        Long simCardId = 13L;
        Boolean realTime = false;
        Long limit = 10L;
        Long offset = 1L;
        try {
            ListSimPricePlansResponse response = gslClient.listSimPricePlans(
                new ListSimPricePlansRequest().withLimit(limit).withOffset(offset).withRealTime(realTime).withSimCardId(simCardId));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
}
