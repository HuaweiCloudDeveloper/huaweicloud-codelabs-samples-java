package com.huawei.gsl;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.gsl.v3.GslClient;
import com.huaweicloud.sdk.gsl.v3.model.AddOrModifyAttributeReq;
import com.huaweicloud.sdk.gsl.v3.model.AttributeReq;
import com.huaweicloud.sdk.gsl.v3.model.BatchSetAttributesReq;
import com.huaweicloud.sdk.gsl.v3.model.BatchSetAttributesRequest;
import com.huaweicloud.sdk.gsl.v3.model.BatchSetAttributesResponse;
import com.huaweicloud.sdk.gsl.v3.model.CreateAttributeRequest;
import com.huaweicloud.sdk.gsl.v3.model.CreateAttributeResponse;
import com.huaweicloud.sdk.gsl.v3.model.DisableAttributeRequest;
import com.huaweicloud.sdk.gsl.v3.model.DisableAttributeResponse;
import com.huaweicloud.sdk.gsl.v3.model.EnableAttributeRequest;
import com.huaweicloud.sdk.gsl.v3.model.EnableAttributeResponse;
import com.huaweicloud.sdk.gsl.v3.model.ListAttributesRequest;
import com.huaweicloud.sdk.gsl.v3.model.ListAttributesResponse;
import com.huaweicloud.sdk.gsl.v3.model.UpdateAttributeRequest;
import com.huaweicloud.sdk.gsl.v3.model.UpdateAttributeResponse;
import com.huaweicloud.sdk.gsl.v3.region.GslRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SimAttributesApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimAttributesApplication.class);

    /**
     * - IAM_ENDPOINT: 华为云IAM服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?IAM
     */
    private static final String IAM_ENDPOINT = "https://<IamEndpoint>";

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 创建认证
        BasicCredentials auth = new BasicCredentials()
            .withIamEndpoint(IAM_ENDPOINT)
            .withAk(ak)
            .withSk(sk);

        GslClient gslClient = GslClient.newBuilder()
            .withCredential(auth)
            .withRegion(GslRegion.CN_NORTH_4)
            .build();

        // 创建自定义属性
        createAttribute(gslClient);
        // 查询自定义属性列表
        listAttributes(gslClient);
        // 批量设置SIM卡自定义属性
        batchSetAttributes(gslClient);
        // 启用自定义属性
        enableAttribute(gslClient);
        // 修改自定义属性名称
        updateAttribute(gslClient);
        // 禁用自定义属性
        disableAttribute(gslClient);
    }

    private static void createAttribute(GslClient gslClient) {
        String custAttrName = "SDK测试自定义属性";
        try {
            CreateAttributeResponse response = gslClient.createAttribute(
                new CreateAttributeRequest().withBody(
                    new AddOrModifyAttributeReq().withCustAttrName(custAttrName)));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    private static void listAttributes(GslClient gslClient) {
        // 需要查询的自定义属性名，如果为空则查询全部自定义属性
        String custAttrName = "SDK测试自定义属性";
        Long limit = 10L;
        Long offset = 1L;
        // 自定义属性状态，0表示未启用， 1表示已启用
        Integer status = 0;
        try {
            ListAttributesResponse response = gslClient.listAttributes(
                new ListAttributesRequest().withCustAttrName(custAttrName)
                    .withLimit(limit)
                    .withOffset(offset)
                    .withStatus(status));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    private static void batchSetAttributes(GslClient gslClient) {
        // 需要修改自定义属性的列表请求
        List<AttributeReq> attributes = new ArrayList<>();
        AttributeReq attributeReq1 = new AttributeReq();
        attributeReq1.setSimCardId(1234567L);
        attributeReq1.setCustomerAttribute1("自定义属性1测试");
        attributeReq1.setCustomerAttribute2("自定义属性2测试");
        attributeReq1.setCustomerAttribute3("自定义属性3测试");
        attributeReq1.setCustomerAttribute4("自定义属性4测试");
        attributeReq1.setCustomerAttribute5("自定义属性5测试");
        attributeReq1.setCustomerAttribute6("自定义属性6测试");
        attributes.add(attributeReq1);
        try {
            BatchSetAttributesResponse response = gslClient.batchSetAttributes(
                new BatchSetAttributesRequest().withBody(
                    new BatchSetAttributesReq().withAttributes(attributes)));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    private static void enableAttribute(GslClient gslClient) {
        Long attributeId = 12L;
        try {
            EnableAttributeResponse response = gslClient.enableAttribute(
                new EnableAttributeRequest().withAttributeId(attributeId));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    private static void updateAttribute(GslClient gslClient) {
        Long attributeId = 12L;
        String custAttrName = "SDK测试自定义属性新";
        try {
            UpdateAttributeResponse response = gslClient.updateAttribute(
                new UpdateAttributeRequest().withAttributeId(attributeId).withBody(
                    new AddOrModifyAttributeReq().withCustAttrName(custAttrName)));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    private static void disableAttribute(GslClient gslClient) {
        Long attributeId = 12L;
        try {
            DisableAttributeResponse response = gslClient.disableAttribute(
                new DisableAttributeRequest().withAttributeId(attributeId));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
}
