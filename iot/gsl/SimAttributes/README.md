## 0. 版本说明
   本示例基于华为云Java SDK V3.0版本开发
## 1. 简介
   基于华为云Java SDK, 与SIM卡自定义属性相关的操作
## 2. 开发前准备
   已注册华为云, 并完成实名认证;
   已获取华为云账号对应的Access Key（AK）和Secret Access Key(SK);
   Java JDK 1.8及其以上版本
## 3. 安装SDK
   通过 Maven 安装项目依赖是使用 Java SDK 的推荐方法，首先您需要在您的操作系统中下载并安装 Maven ，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
   本示例使用GSL JDK
```xml
   <dependency>
   <groupId>com.huaweicloud.sdk</groupId>
   <artifactId>huaweicloud-sdk-gsl</artifactId>
   <version>3.1.64</version>
   </dependency>
```
## 4. 开始使用
   ### 4.1 导入依赖模块
```java
   import com.huaweicloud.sdk.core.exception.ClientRequestException;
   // 导入相应产品的 {Service}Client
   import com.huaweicloud.sdk.gsl.v3.GslClient;
   // 导入待请求接口的 request 和 response 类
   import com.huaweicloud.sdk.gsl.v3.model.*;
   // 导入Region
   import com.huaweicloud.sdk.gsl.v3.region.GslRegion;
   // 日志打印
   import org.slf4j.Logger;
   import org.slf4j.LoggerFactory;
```
### 4.2.1 初始化认证信息
```java 
   BasicCredentials auth = new BasicCredentials()
        .withIamEndpoint(IAM_ENDPOINT)
        .withAk(ak)
        .withSk(sk);
```
IAM_ENDPOINT: 华为云IAM服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?IAM
### 4.2.2 初始化客户端
```java 
   GslClient gslClient = GslClient.newBuilder()
        .withRegion(GslRegion.CN_NORTH_4)
        .withCredential(auth)
        .build();
```
## 5. 示例代码
   使用如下代码进行SIM卡自定义属性相关的操作，调用前请根据实际情况替换变量：

```java
public class SimAttributesApplication {
   private static final Logger LOGGER = LoggerFactory.getLogger(SimAttributesApplication.class);

   /**
    * - IAM_ENDPOINT: 华为云IAM服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?IAM
    */
   private static final String IAM_ENDPOINT = "https://<IamEndpoint>";

   public static void main(String[] args) {
      // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
      // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
      String ak = System.getenv("HUAWEICLOUD_SDK_AK");
      String sk = System.getenv("HUAWEICLOUD_SDK_SK");

      // 创建认证
      BasicCredentials auth = new BasicCredentials()
              .withIamEndpoint(IAM_ENDPOINT)
              .withAk(ak)
              .withSk(sk);

      GslClient gslClient = GslClient.newBuilder()
              .withCredential(auth)
              .withRegion(GslRegion.CN_NORTH_4)
              .build();

      // 创建自定义属性
      createAttribute(gslClient);
      // 查询自定义属性列表
      listAttributes(gslClient);
      // 批量设置SIM卡自定义属性
      batchSetAttributes(gslClient);
      // 启用自定义属性
      enableAttribute(gslClient);
      // 修改自定义属性名称
      updateAttribute(gslClient);
      // 禁用自定义属性
      disableAttribute(gslClient);
   }

   private static void createAttribute(GslClient gslClient) {
      String custAttrName = "SDK测试自定义属性";
      try {
         CreateAttributeResponse response = gslClient.createAttribute(
                 new CreateAttributeRequest().withBody(
                         new AddOrModifyAttributeReq().withCustAttrName(custAttrName)));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void listAttributes(GslClient gslClient) {
      // 需要查询的自定义属性名，如果为空则查询全部自定义属性
      String custAttrName = "SDK测试自定义属性";
      Long limit = 10L;
      Long offset = 1L;
      // 自定义属性状态，0表示未启用， 1表示已启用
      Integer status = 0;
      try {
         ListAttributesResponse response = gslClient.listAttributes(
                 new ListAttributesRequest().withCustAttrName(custAttrName)
                         .withLimit(limit)
                         .withOffset(offset)
                         .withStatus(status));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void batchSetAttributes(GslClient gslClient) {
      // 需要修改自定义属性的列表请求
      List<AttributeReq> attributes = new ArrayList<>();
      AttributeReq attributeReq1 = new AttributeReq();
      attributeReq1.setSimCardId(1234567L);
      attributeReq1.setCustomerAttribute1("自定义属性1测试");
      attributeReq1.setCustomerAttribute2("自定义属性2测试");
      attributeReq1.setCustomerAttribute3("自定义属性3测试");
      attributeReq1.setCustomerAttribute4("自定义属性4测试");
      attributeReq1.setCustomerAttribute5("自定义属性5测试");
      attributeReq1.setCustomerAttribute6("自定义属性6测试");
      attributes.add(attributeReq1);
      try {
         BatchSetAttributesResponse response = gslClient.batchSetAttributes(
                 new BatchSetAttributesRequest().withBody(
                         new BatchSetAttributesReq().withAttributes(attributes)));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void enableAttribute(GslClient gslClient) {
      Long attributeId = 12L;
      try {
         EnableAttributeResponse response = gslClient.enableAttribute(
                 new EnableAttributeRequest().withAttributeId(attributeId));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void updateAttribute(GslClient gslClient) {
      Long attributeId = 12L;
      String custAttrName = "SDK测试自定义属性新";
      try {
         UpdateAttributeResponse response = gslClient.updateAttribute(
                 new UpdateAttributeRequest().withAttributeId(attributeId).withBody(
                         new AddOrModifyAttributeReq().withCustAttrName(custAttrName)));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void disableAttribute(GslClient gslClient) {
      Long attributeId = 12L;
      try {
         DisableAttributeResponse response = gslClient.disableAttribute(
                 new DisableAttributeRequest().withAttributeId(attributeId));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }
}
```
## 6. 运行结果
通过调用ListAttributes接口，获取当前自定义属性信息：
请求参数：
```http request
GET https://{endpoint}/v1/attributes?offset=1&limit=10&status=1
```
返回响应：
```json
{
   "limit": 10,
   "offset": 1,
   "count": 1,
   "attributes": [
      {
         "id": 5229570883266944,
         "default_attr_name_cn": "自定义属性一",
         "default_attr_name_en": "Custom attribute 1",
         "cust_attr_name": "333444",
         "status": 1,
         "create_time": "2023-04-08T08:10:18.000+00:00",
         "modify_time": "2023-10-31T01:28:03.000+00:00"
      }
   ]
}
```
其中，列表返回的id可以用来做之后例如自定义属性停用，启用等操作的入参。
## 7. 参考
更多信息请参考[全球SIM联接 GSL](https://support.huaweicloud.com/qs-ocgsl/oceanlink_03_0001.html)
## 8. 修订记录
|  发布日期  | 文档版本  |   修订说明   |
| :--------: |:-----:| :----------: |
| 2022-06-12 | 1.0.1 | 文档首次发布 |
| 2022-08-02 | 1.0.2 | 更新依赖 |
| 2023-11-08 | 1.0.3 |          更新文档           |
