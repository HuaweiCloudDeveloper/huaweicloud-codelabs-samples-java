package com.huawei.gsl;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.gsl.v3.GslClient;
import com.huaweicloud.sdk.gsl.v3.model.ListProPricePlansRequest;
import com.huaweicloud.sdk.gsl.v3.model.ListProPricePlansResponse;
import com.huaweicloud.sdk.gsl.v3.region.GslRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PricePlansApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(PricePlansApplication.class);

    /**
     * - IAM_ENDPOINT: 华为云IAM服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?IAM
     */
    private static final String IAM_ENDPOINT = "https://<IamEndpoint>";

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 创建认证
        BasicCredentials auth = new BasicCredentials()
            .withIamEndpoint(IAM_ENDPOINT)
            .withAk(ak)
            .withSk(sk);
                                    
        GslClient gslClient = GslClient.newBuilder()
            .withCredential(auth)
            .withRegion(GslRegion.CN_NORTH_4)
            .build();

        // 查询套餐列表
        getPricePlansList(gslClient);
        
    }

    private static void getPricePlansList(GslClient gslClient) {
        Long offset = 1L;
        Long limit = 10L;
        // 运营商类型
        Integer carrierType = 2;
        try {
            ListProPricePlansResponse response = gslClient.listProPricePlans(
                new ListProPricePlansRequest().withOffset(offset).withLimit(limit).withCarrierType(carrierType));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
}
