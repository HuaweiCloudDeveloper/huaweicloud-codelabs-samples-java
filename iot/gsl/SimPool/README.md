## 0. 版本说明
   本示例基于华为云Java SDK V3.0版本开发
## 1. 简介
   基于华为云Java SDK, 与前向流量池的相关操作
## 2. 开发前准备
   已注册华为云, 并完成实名认证;
   已获取华为云账号对应的Access Key（AK）和Secret Access Key(SK);
   Java JDK 1.8及其以上版本
## 3. 安装SDK
   通过 Maven 安装项目依赖是使用 Java SDK 的推荐方法，首先您需要在您的操作系统中下载并安装 Maven ，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
   本示例使用GSL JDK
```xml
   <dependency>
   <groupId>com.huaweicloud.sdk</groupId>
   <artifactId>huaweicloud-sdk-gsl</artifactId>
   <version>3.1.64</version>
   </dependency>
```
## 4. 开始使用
   ### 4.1 导入依赖模块
```java
   import com.huaweicloud.sdk.core.exception.ClientRequestException;
   // 导入相应产品的 {Service}Client
   import com.huaweicloud.sdk.gsl.v3.GslClient;
   // 导入待请求接口的 request 和 response 类
   import com.huaweicloud.sdk.gsl.v3.model.*;
   // 导入Region
   import com.huaweicloud.sdk.gsl.v3.region.GslRegion;
   // 日志打印
   import org.slf4j.Logger;
   import org.slf4j.LoggerFactory;
```
### 4.2.1 初始化认证信息
```java 
   BasicCredentials auth = new BasicCredentials()
        .withIamEndpoint(IAM_ENDPOINT)
        .withAk(ak)
        .withSk(sk);
```
IAM_ENDPOINT: 华为云IAM服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?IAM
### 4.2.2 初始化客户端
```java 
   GslClient gslClient = GslClient.newBuilder()
        .withRegion(GslRegion.CN_NORTH_4)
        .withCredential(auth)
        .build();
```
## 5. 示例代码
   使用如下代码对前向流量池进行操作，调用前请根据实际情况替换变量：
```java
public class SimPoolApplication {
   private static final Logger LOGGER = LoggerFactory.getLogger(SimPoolApplication.class);

   /**
    * - IAM_ENDPOINT: 华为云IAM服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?IAM
    */
   private static final String IAM_ENDPOINT = "https://<IamEndpoint>";

   public static void main(String[] args) {
      // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
      // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
      String ak = System.getenv("HUAWEICLOUD_SDK_AK");
      String sk = System.getenv("HUAWEICLOUD_SDK_SK");

      // 创建认证
      BasicCredentials auth = new BasicCredentials()
              .withIamEndpoint(IAM_ENDPOINT)
              .withAk(ak)
              .withSk(sk);

      GslClient gslClient = GslClient.newBuilder()
              .withCredential(auth)
              .withRegion(GslRegion.CN_NORTH_4)
              .build();


      // 查询流量池成员列表
      listSimPoolMembers(gslClient);
      // 查询流量池列表
      listSimPools(gslClient);

   }

   private static void listSimPoolMembers(GslClient gslClient) {
      Long simPoolId = 3639843601089537L;
      String cid = "8986031746205479425";
      Long limit = 10L;
      Long offset = 1L;
      String billingCycle = "2021-08";
      try {
         ListSimPoolMembersResponse response = gslClient.listSimPoolMembers(
                 new ListSimPoolMembersRequest().withSimPoolId(simPoolId)
                         .withLimit(limit)
                         .withOffset(offset)
                         .withCid(cid)
                         .withBillingCycle(billingCycle));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void listSimPools(GslClient gslClient) {
      String poolName = "本地测试流量池";
      Long limit = 10L;
      Long offset = 1L;
      String billingCycle = "2022-04";
      try {
         ListSimPoolsResponse response = gslClient.listSimPools(
                 new ListSimPoolsRequest().withBillingCycle(billingCycle)
                         .withLimit(limit)
                         .withOffset(offset)
                         .withPoolName(poolName));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }
}
```
## 6. 运行结果
调用listSimPools查询前向流量池列表，请求参数：
```http request
GET https://{endpoint}/v1/sim-pools?offset=1&limit=10
```
返回响应：
```json
{
  "limit" : 10,
  "offset" : 1,
  "count" : 1,
  "pools" : [ {
    "id" : 1000000000,
    "pool_name" : "流量池名称",
    "price_plan_name" : "中国联通消费级每月30M联接服务（1年）",
    "price_plan_id" : "10000",
    "effective_time" : "2022-08-26T16:00:00.000+00:00",
    "expired_time" : "2022-09-26T15:59:59.000+00:00",
    "billing_cycle" : "2022-09",
    "flow_total" : 60,
    "flow_used" : 102.59,
    "flow_left" : -42.59,
    "quantity" : 100,
    "modify_time" : "2022-09-22T18:57:15.000+00:00",
    "order_id" : 100000000,
    "activated_sim_quantity" : 2,
    "inactive_sim_quantity" : 0,
    "disassembled_sim_quantity" : 98,
    "order_ids" : "1000000"
  } ]
}
```
## 7. 参考
   更多信息请参考[全球SIM联接 GSL](https://support.huaweicloud.com/qs-ocgsl/oceanlink_03_0001.html)
## 8. 修订记录
|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2021-09-28 |   1.0.1    | 文档首次发布 |
| 2022-04-12 |   1.0.2    | 更新依赖，修改部分接口 |
| 2022-06-12 |   1.0.3    | 更新依赖，新增标签管理，自定义属性管理接口说明 |
| 2022-08-02 |   1.0.4    | 更新依赖 |
| 2023-11-08 | 1.0.5 |          更新文档           |