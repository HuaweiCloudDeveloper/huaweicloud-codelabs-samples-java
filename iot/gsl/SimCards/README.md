## 0. 版本说明
   本示例基于华为云Java SDK V3.0版本开发
## 1. 简介
   基于华为云Java SDK, 与SIM卡的相关操作
## 2. 开发前准备
   已注册华为云, 并完成实名认证;
   已获取华为云账号对应的Access Key（AK）和Secret Access Key(SK);
   Java JDK 1.8及其以上版本
## 3. 安装SDK
   通过 Maven 安装项目依赖是使用 Java SDK 的推荐方法，首先您需要在您的操作系统中下载并安装 Maven ，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
   本示例使用GSL JDK
```xml
   <dependency>
   <groupId>com.huaweicloud.sdk</groupId>
   <artifactId>huaweicloud-sdk-gsl</artifactId>
   <version>3.1.64</version>
   </dependency>
```
## 4. 开始使用
   ### 4.1 导入依赖模块
```java
   import com.huaweicloud.sdk.core.exception.ClientRequestException;
   // 导入相应产品的 {Service}Client
   import com.huaweicloud.sdk.gsl.v3.GslClient;
   // 导入待请求接口的 request 和 response 类
   import com.huaweicloud.sdk.gsl.v3.model.*;
   // 导入Region
   import com.huaweicloud.sdk.gsl.v3.region.GslRegion;
   // 日志打印
   import org.slf4j.Logger;
   import org.slf4j.LoggerFactory;
```
### 4.2.1 初始化认证信息
```java 
   BasicCredentials auth = new BasicCredentials()
        .withIamEndpoint(IAM_ENDPOINT)
        .withAk(ak)
        .withSk(sk);
```
IAM_ENDPOINT: 华为云IAM服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?IAM
### 4.2.2 初始化客户端
```java 
   GslClient gslClient = GslClient.newBuilder()
        .withRegion(GslRegion.CN_NORTH_4)
        .withCredential(auth)
        .build();
```

## 5. 示例代码
   使用如下代码对SIM卡进行相关操作，调用前请根据实际情况替换变量：

```java
public class SimCardsApplication {
   private static final Logger LOGGER = LoggerFactory.getLogger(SimCardsApplication.class);

   /**
    * - IAM_ENDPOINT: 华为云IAM服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?IAM
    */
   private static final String IAM_ENDPOINT = "https://<IamEndpoint>";
   
   public static void main(String[] args) {
      // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
      // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
      String ak = System.getenv("HUAWEICLOUD_SDK_AK");
      String sk = System.getenv("HUAWEICLOUD_SDK_SK");

      // 创建认证
      BasicCredentials auth = new BasicCredentials()
              .withIamEndpoint(IAM_ENDPOINT)
              .withAk(ak)
              .withSk(sk);

      GslClient gslClient = GslClient.newBuilder()
              .withCredential(auth)
              .withRegion(GslRegion.CN_NORTH_4)
              .build();
      // 查询SIM卡实名认证信息
      showRealNamed(gslClient);
      // 清除SIM卡的实名认证信息
      deleteRealName(gslClient);
      // 查询SIM卡列表
      listSimCards(gslClient);
      // 查询SIM卡详情
      showSimCard(gslClient);
      // 实体卡限速
      setSpeedValue(gslClient);
      // SIM卡单卡复机
      resetSimCard(gslClient);
      // SIM卡单卡停机
      stopSimCard(gslClient);
      // SIM卡达量断网/恢复在用
      setExceedCutNet(gslClient);
      // SIM卡申请断网/恢复在用
      startStopNet(gslClient);
      // SIM卡机卡重绑
      registerImei(gslClient);
      // 激活实体卡
      enableSimCard(gslClient);
      // 批量查询SIM卡月用量
      showMonthUsages(gslClient);
   }
   private static void deleteRealName(GslClient gslClient) {
      Long simCardId = 4084101881827328L;
      try {
         DeleteRealNameResponse response = gslClient.deleteRealName(new DeleteRealNameRequest().withSimCardId(simCardId));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void enableSimCard(GslClient gslClient) {
      Long simCardId = 3449878700524511L;
      try {
         EnableSimCardResponse response = gslClient.enableSimCard(new EnableSimCardRequest().withSimCardId(simCardId));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void listSimCards(GslClient gslClient) {
      // 显示的数据数量
      Long limit = 10L;
      // 展示的当前页
      Long offset = 1L;
      // SIM卡状态-11-未激活
      Integer simStatus = 11;
      try {
         ListSimCardsResponse response = gslClient.listSimCards(
                 new ListSimCardsRequest().withLimit(limit).withOffset(offset).withSimStatus(simStatus));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void registerImei(GslClient gslClient) {
      Long simCardId = 3449878744318976L;
      Integer bindType = 1;
      String imei = "866758045090897";
      try {
         RegisterImeiResponse response = gslClient.registerImei(
                 new RegisterImeiRequest().withSimCardId(simCardId)
                         .withBody(new RegisterImeiReq().withImei(imei).withBindType(bindType)));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void resetSimCard(GslClient gslClient) {
      Long simCardId = 27L;
      Integer downUpSwitch = 1;
      try {
         ResetSimCardResponse response = gslClient.resetSimCard(
                 new ResetSimCardRequest().withSimCardId(simCardId)
                         .withBody(
                                 new DownUpTimeForSimCardReq().withDownUpSwitch(downUpSwitch)));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void setExceedCutNet(GslClient gslClient) {
      // 只支持中国电信
      Long simCardId = 15847632244711L;
      // 1:设置达量断网域值,2:取消达量断网域值
      Integer action = 1;
      // 只能是0,-1,正整数,-1表示无限制,0表示有上网流量产生就会立即断网
      String quota = "0";
      try {
         SetExceedCutNetResponse response = gslClient.setExceedCutNet(
                 new SetExceedCutNetRequest().withSimCardId(simCardId)
                         .withBody(new ExceedCutNetReq().withAction(action).withQuota(quota)));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void setSpeedValue(GslClient gslClient) {
      // 中国电信
      Long simCardId = 15847632244712L;
      // 不同运营商支持的限速速率不同
      Integer speedValue = 256;
      try {
         SetSpeedValueResponse response = gslClient.setSpeedValue(
                 new SetSpeedValueRequest().withSimCardId(simCardId)
                         .withBody(new SetSpeedValueReq().withSpeedValue(speedValue)));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void showRealNamed(GslClient gslClient) {
      Long simCardId = 3851944051704838L;
      try {
         ShowRealNamedResponse response = gslClient.showRealNamed(
                 new ShowRealNamedRequest().withSimCardId(simCardId));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void showSimCard(GslClient gslClient) {
      Long simCardId = 3851944051704838L;
      try {
         ShowSimCardResponse response = gslClient.showSimCard(
                 new ShowSimCardRequest().withSimCardId(simCardId));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void startStopNet(GslClient gslClient) {
      Long simCardId = 3851944051704842L;
      // (ADD:断网,DEL:取消断网)
      String action = "ADD";
      try {
         StartStopNetResponse response = gslClient.startStopNet(
                 new StartStopNetRequest().withSimCardId(simCardId).withBody(new CutNetReq().withAction(action)));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void stopSimCard(GslClient gslClient) {
      Long simCardId = 14L;
      Integer downUpSwitch = 1;
      try {
         StopSimCardResponse response = gslClient.stopSimCard(
                 new StopSimCardRequest().withSimCardId(simCardId)
                         .withBody(
                                 new DownUpTimeForSimCardReq().withDownUpSwitch(downUpSwitch)));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }

   private static void showMonthUsages(GslClient gslClient) {
      List<Long> simCardIds = new ArrayList<>();
      simCardIds.add(14L);
      List<String> billingCycles = new ArrayList<>();
      billingCycles.add("2022-07");
      try {
         ShowMonthUsagesResponse response = gslClient.showMonthUsages(
                 new ShowMonthUsagesRequest().withBody(
                         new ShowMonthUsageReq().withBillingCycles(billingCycles)
                                 .withSimCardIds(simCardIds)));
         LOGGER.info(response.toString());
      } catch (Exception e) {
         LOGGER.error(e.getMessage());
      }
   }
}
```
## 6. 运行结果
调用listSimCards查看当前用户下卡在用卡（sim_status为20）：
请求参数
```http request
GET https://{endpoint}/v1/sim-cards?offset=1&limit=10&sim_status=20
```
响应：
```json
{
  "limit" : 10,
  "offset" : 1,
  "count" : 1,
  "sim_cards" : [ {
    "sim_card_id" : 4900000000000,
    "account_id" : "04eef84xxxxxxxxxxx",
    "cid" : "8986xxxxxxxxxx",
    "sim_pool_id" : 4940000000,
    "imei" : null,
    "sim_status" : 20,
    "device_status" : null,
    "device_model" : null,
    "act_date" : "2023-09-16T01:14:58.000+00:00",
    "device_status_date" : null,
    "node_id" : null,
    "iccid" : "8986xxxxxxxxxxx",
    "network_type" : null,
    "dbm" : null,
    "signal_level" : null,
    "sim_type" : 3,
    "tag_names" : null,
    "order_id" : 494000000000,
    "expire_time" : "2024-08-26T15:59:59.000+00:00",
    "price_plan_name" : "中国联通消费级每月30M联接服务（1年）",
    "sim_price_plan_id" : null,
    "flow_left" : 30,
    "flow_used" : 0,
    "operator_status" : 5,
    "msisdn" : "86xxxxxxxxx",
    "imsi" : "46xxxxxxxxxx",
    "customer_attribute1" : null,
    "customer_attribute2" : null,
    "customer_attribute3" : null,
    "customer_attribute4" : null,
    "customer_attribute5" : null,
    "customer_attribute6" : null,
    "real_named" : false,
    "cut_net_flag" : false,
    "exceed_cut_net_flag" : false,
    "exceed_cut_net_quota" : null,
    "imei_bind_remain_times" : null,
    "speed_value" : null
  } ]
}
```
其中响应返回的sim_pool_id，cid等字段可以用于其他API接口的查询条件。
## 7. 参考
更多信息请参考[全球SIM联接 GSL](https://support.huaweicloud.com/qs-ocgsl/oceanlink_03_0001.html)
## 8. 修订记录
|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2021-09-28 |   1.0.1    | 文档首次发布 |
| 2022-04-12 |   1.0.2    | 更新依赖，修改部分接口 |
| 2022-06-12 |   1.0.3    | 更新依赖，新增标签管理，自定义属性管理接口说明 |
| 2022-08-02 |   1.0.4    | 更新依赖，新增月用量查询接口 |
| 2023-11-08 | 1.0.5 |          更新文档           |