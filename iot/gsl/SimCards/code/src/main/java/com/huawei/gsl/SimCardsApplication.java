package com.huawei.gsl;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.gsl.v3.GslClient;
import com.huaweicloud.sdk.gsl.v3.model.CutNetReq;
import com.huaweicloud.sdk.gsl.v3.model.DeleteRealNameRequest;
import com.huaweicloud.sdk.gsl.v3.model.DeleteRealNameResponse;
import com.huaweicloud.sdk.gsl.v3.model.DownUpTimeForSimCardReq;
import com.huaweicloud.sdk.gsl.v3.model.EnableSimCardRequest;
import com.huaweicloud.sdk.gsl.v3.model.EnableSimCardResponse;
import com.huaweicloud.sdk.gsl.v3.model.ExceedCutNetReq;
import com.huaweicloud.sdk.gsl.v3.model.ListSimCardsRequest;
import com.huaweicloud.sdk.gsl.v3.model.ListSimCardsResponse;
import com.huaweicloud.sdk.gsl.v3.model.RegisterImeiReq;
import com.huaweicloud.sdk.gsl.v3.model.RegisterImeiRequest;
import com.huaweicloud.sdk.gsl.v3.model.RegisterImeiResponse;
import com.huaweicloud.sdk.gsl.v3.model.ResetSimCardRequest;
import com.huaweicloud.sdk.gsl.v3.model.ResetSimCardResponse;
import com.huaweicloud.sdk.gsl.v3.model.SetExceedCutNetRequest;
import com.huaweicloud.sdk.gsl.v3.model.SetExceedCutNetResponse;
import com.huaweicloud.sdk.gsl.v3.model.SetSpeedValueReq;
import com.huaweicloud.sdk.gsl.v3.model.SetSpeedValueRequest;
import com.huaweicloud.sdk.gsl.v3.model.SetSpeedValueResponse;
import com.huaweicloud.sdk.gsl.v3.model.ShowMonthUsageReq;
import com.huaweicloud.sdk.gsl.v3.model.ShowMonthUsagesRequest;
import com.huaweicloud.sdk.gsl.v3.model.ShowMonthUsagesResponse;
import com.huaweicloud.sdk.gsl.v3.model.ShowRealNamedRequest;
import com.huaweicloud.sdk.gsl.v3.model.ShowRealNamedResponse;
import com.huaweicloud.sdk.gsl.v3.model.ShowSimCardRequest;
import com.huaweicloud.sdk.gsl.v3.model.ShowSimCardResponse;
import com.huaweicloud.sdk.gsl.v3.model.StartStopNetRequest;
import com.huaweicloud.sdk.gsl.v3.model.StartStopNetResponse;
import com.huaweicloud.sdk.gsl.v3.model.StopSimCardRequest;
import com.huaweicloud.sdk.gsl.v3.model.StopSimCardResponse;
import com.huaweicloud.sdk.gsl.v3.region.GslRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SimCardsApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimCardsApplication.class);

    /**
     * - IAM_ENDPOINT: 华为云IAM服务访问终端地址,详情见https://developer.huaweicloud.com/endpoint?IAM
     */
    private static final String IAM_ENDPOINT = "https://<IamEndpoint>";

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 创建认证
        BasicCredentials auth = new BasicCredentials()
            .withIamEndpoint(IAM_ENDPOINT)
            .withAk(ak)
            .withSk(sk);

        GslClient gslClient = GslClient.newBuilder()
            .withCredential(auth)
            .withRegion(GslRegion.CN_NORTH_4)
            .build();
        // 查询SIM卡实名认证信息
        showRealNamed(gslClient);
        // 清除SIM卡的实名认证信息
        deleteRealName(gslClient);
        // 查询SIM卡列表
        listSimCards(gslClient);
        // 查询SIM卡详情
        showSimCard(gslClient);
        // 实体卡限速
        setSpeedValue(gslClient);
        // SIM卡单卡复机
        resetSimCard(gslClient);
        // SIM卡单卡停机
        stopSimCard(gslClient);
        // SIM卡达量断网/恢复在用
        setExceedCutNet(gslClient);
        // SIM卡申请断网/恢复在用
        startStopNet(gslClient);
        // SIM卡机卡重绑
        registerImei(gslClient);
        // 激活实体卡
        enableSimCard(gslClient);
        // 批量查询SIM卡月用量
        showMonthUsages(gslClient);
    }

    private static void deleteRealName(GslClient gslClient) {
        Long simCardId = 4084101881827328L;
        try {
            DeleteRealNameResponse response = gslClient.deleteRealName(new DeleteRealNameRequest().withSimCardId(simCardId));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
    
    private static void enableSimCard(GslClient gslClient) {
        Long simCardId = 3449878700524511L;
        try {
            EnableSimCardResponse response = gslClient.enableSimCard(new EnableSimCardRequest().withSimCardId(simCardId));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
    
    private static void listSimCards(GslClient gslClient) {
        // 显示的数据数量
        Long limit = 10L;
        // 展示的当前页
        Long offset = 1L;
        // SIM卡状态-11-未激活
        Integer simStatus = 11;
        try {
            ListSimCardsResponse response = gslClient.listSimCards(
                new ListSimCardsRequest().withLimit(limit).withOffset(offset).withSimStatus(simStatus));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
    
    private static void registerImei(GslClient gslClient) {
        Long simCardId = 3449878744318976L;
        Integer bindType = 1;
        String imei = "866758045090897";
        try {
            RegisterImeiResponse response = gslClient.registerImei(
                new RegisterImeiRequest().withSimCardId(simCardId)
                    .withBody(new RegisterImeiReq().withImei(imei).withBindType(bindType)));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
    
    private static void resetSimCard(GslClient gslClient) {
        Long simCardId = 27L;
        Integer downUpSwitch = 1;
        try {
            ResetSimCardResponse response = gslClient.resetSimCard(
                new ResetSimCardRequest().withSimCardId(simCardId)
                    .withBody(
                        new DownUpTimeForSimCardReq().withDownUpSwitch(downUpSwitch)));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
    
    private static void setExceedCutNet(GslClient gslClient) {
        // 只支持中国电信
        Long simCardId = 15847632244711L;
        // 1:设置达量断网域值,2:取消达量断网域值
        Integer action = 1;
        // 只能是0,-1,正整数,-1表示无限制,0表示有上网流量产生就会立即断网
        String quota = "0";
        try {
            SetExceedCutNetResponse response = gslClient.setExceedCutNet(
                new SetExceedCutNetRequest().withSimCardId(simCardId)
                    .withBody(new ExceedCutNetReq().withAction(action).withQuota(quota)));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
    
    private static void setSpeedValue(GslClient gslClient) {
        // 中国电信
        Long simCardId = 15847632244712L;
        // 不同运营商支持的限速速率不同
        Integer speedValue = 256;
        try {
            SetSpeedValueResponse response = gslClient.setSpeedValue(
                new SetSpeedValueRequest().withSimCardId(simCardId)
                    .withBody(new SetSpeedValueReq().withSpeedValue(speedValue)));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
    
    private static void showRealNamed(GslClient gslClient) {
        Long simCardId = 3851944051704838L;
        try {
            ShowRealNamedResponse response = gslClient.showRealNamed(
                new ShowRealNamedRequest().withSimCardId(simCardId));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
    
    private static void showSimCard(GslClient gslClient) {
        Long simCardId = 3851944051704838L;
        try {
            ShowSimCardResponse response = gslClient.showSimCard(
                new ShowSimCardRequest().withSimCardId(simCardId));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
    
    private static void startStopNet(GslClient gslClient) {
        Long simCardId = 3851944051704842L;
        // (ADD:断网,DEL:取消断网)
        String action = "ADD";
        try {
            StartStopNetResponse response = gslClient.startStopNet(
                new StartStopNetRequest().withSimCardId(simCardId).withBody(new CutNetReq().withAction(action)));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
    
    private static void stopSimCard(GslClient gslClient) {
        Long simCardId = 14L;
        Integer downUpSwitch = 1;
        try {
            StopSimCardResponse response = gslClient.stopSimCard(
                new StopSimCardRequest().withSimCardId(simCardId)
                    .withBody(
                        new DownUpTimeForSimCardReq().withDownUpSwitch(downUpSwitch)));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    private static void showMonthUsages(GslClient gslClient) {
        List<Long> simCardIds = new ArrayList<>();
        simCardIds.add(14L);
        List<String> billingCycles = new ArrayList<>();
        billingCycles.add("2022-07");
        try {
            ShowMonthUsagesResponse response = gslClient.showMonthUsages(
                new ShowMonthUsagesRequest().withBody(
                    new ShowMonthUsageReq().withBillingCycles(billingCycles)
                        .withSimCardIds(simCardIds)));
            LOGGER.info(response.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
}
