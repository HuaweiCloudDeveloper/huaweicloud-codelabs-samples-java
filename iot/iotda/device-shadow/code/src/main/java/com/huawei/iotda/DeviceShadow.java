package com.huawei.iotda;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.JsonUtils;
import com.huaweicloud.sdk.iotda.v5.IoTDAClient;
import com.huaweicloud.sdk.iotda.v5.model.CreateBatchTask;
import com.huaweicloud.sdk.iotda.v5.model.CreateBatchTaskRequest;
import com.huaweicloud.sdk.iotda.v5.model.CreateBatchTaskResponse;
import com.huaweicloud.sdk.iotda.v5.model.ShowBatchTaskRequest;
import com.huaweicloud.sdk.iotda.v5.model.ShowBatchTaskResponse;
import com.huaweicloud.sdk.iotda.v5.model.ShowDeviceShadowRequest;
import com.huaweicloud.sdk.iotda.v5.model.ShowDeviceShadowResponse;
import com.huaweicloud.sdk.iotda.v5.model.Task;
import com.huaweicloud.sdk.iotda.v5.model.UpdateDesired;
import com.huaweicloud.sdk.iotda.v5.model.UpdateDesireds;
import com.huaweicloud.sdk.iotda.v5.model.UpdateDeviceShadowDesiredDataRequest;
import com.huaweicloud.sdk.iotda.v5.model.UpdateDeviceShadowDesiredDataResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeviceShadow {
    private static final Logger logger = LoggerFactory.getLogger(DeviceShadow.class);

    // REGION_ID：If your region is 上海一，please fill in "cn-east-3"；If your region is 北京四，please fill in "cn-north-4"; If your region is 华南广州，please fill in "cn-south-4"
    private static final String REGION_ID = "<YOUR REGION ID>";

    // ENDPOINT：On the console, choose **Overview** in the navigation pane and click **Access Addresses** to view the HTTPS application access address.
    private static final String ENDPOINT = "<YOUR ENDPOINT>";

    // INSTANCE_ID
    private static final String INSTANCE_ID = "<YOUR INSTANCEID>";

    private static final String PROJECT_ID = "<YOUR PROJECTID>";

    // ak/sk：For details, see the method of obtaining AK/SK in iotda document https://support.huaweicloud.com/api-iothub/iot_06_v5_0091.html.
    // There will be security risks if the AK/SK used for authentication is directly written into code. We suggest encrypting the AK/SK in the configuration file or environment variables for storage.
    // In this sample, the AK/SK is stored in environment variables for identity authentication. Before running this sample, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");

    private static final ObjectMapper OBJECTMAPPER = new ObjectMapper();

    private static IoTDAClient initClient() {
        ICredential auth = new BasicCredentials()
            .withProjectId(PROJECT_ID)
            .withAk(AK)
            .withSk(SK)
            // Derivative algorithms are used for the standard and enterprise editions.
            // For the basic edition, delete the configuration "withDerivedPredicate".
            .withDerivedPredicate(BasicCredentials.DEFAULT_DERIVED_PREDICATE);

        // Create and initialize an IoTDAClient instance
        return IoTDAClient.newBuilder()
            // Use IoTDARegion for basic editions like "withRegion(IoTDARegion.CN_NORTH_4)".
            // Create regions for standard/enterprise editions.
            .withRegion(new Region(REGION_ID, ENDPOINT))
            // .withRegion(IoTDARegion.CN_NORTH_4)
            .withCredential(auth)
            .build();
    }

    /**
     * Configure Desired Values in a Device Shadow
     *
     * @param iotdaClient iotda client
     * @param deviceId    ID of the device whose device shadow needs to be configured
     * @param body        Structure for configuring the device shadow
     */
    private static void updateShadow(IoTDAClient iotdaClient, String deviceId, UpdateDesireds body) throws ServiceResponseException {
        UpdateDeviceShadowDesiredDataRequest request = new UpdateDeviceShadowDesiredDataRequest();
        request.setDeviceId(deviceId);
        request.setBody(body);
        request.setInstanceId(INSTANCE_ID);
        UpdateDeviceShadowDesiredDataResponse response = iotdaClient.updateDeviceShadowDesiredData(request);
        logger.info("The device shadow configuration result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    /**
     * Query the Details About a Device Shadow
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device ID
     */
    private static void queryShadowDetail(IoTDAClient iotdaClient, String deviceId) throws ServiceResponseException {
        ShowDeviceShadowRequest request = new ShowDeviceShadowRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        ShowDeviceShadowResponse messageResponse = iotdaClient.showDeviceShadow(request);
        logger.info("The device shadow query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, messageResponse));
    }

    /**
     * Create a Batch Task
     *
     * @param iotdaClient     iotda client
     * @param createBatchTask Structure for a batch task creation
     * @return The task obtained after the creation
     */
    private static CreateBatchTaskResponse createTask(IoTDAClient iotdaClient, CreateBatchTask createBatchTask) throws ServiceResponseException {
        CreateBatchTaskRequest request = new CreateBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(createBatchTask);
        CreateBatchTaskResponse batchTask = iotdaClient.createBatchTask(request);
        logger.info("The batch task creation result is:{}", JsonUtils.toJSON(OBJECTMAPPER, batchTask));
        return batchTask;
    }

    /**
     * Query the Details About a Batch Task
     *
     * @param iotdaClient iotda client
     * @param taskId      Task ID
     * @return The details of the task
     */
    private static Task queryTask(IoTDAClient iotdaClient, String taskId) throws ServiceResponseException {
        ShowBatchTaskRequest request = new ShowBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setTaskId(taskId);
        ShowBatchTaskResponse showBatchTaskResponse = iotdaClient.showBatchTask(request);
        logger.info("The batch task details query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, showBatchTaskResponse));
        return showBatchTaskResponse.getBatchtask();
    }

    public static void main(String[] args) {
        // Initialize the IoTDA client
        IoTDAClient iotdaClient = initClient();
        // Enter the deviceId of the device shadow to be configured.
        String deviceId = "64111bcd06ca5933b28a058b_saadas";
        // ID of the resource space to which the device belongs. Set this parameter to the ID of your resource space.
        String appId = "9d988b5efdf646f0828724c45e1d794e";

        try {
            logger.info("========1.Query delivery shadow details========");
            queryShadowDetail(iotdaClient, deviceId);
            logger.info("========1.Querying the delivered shadow details completed========");

            logger.info("========2.Configuring the Device Shadow========");
            // Set the expected structure of the device shadow.
            // The structure must be the same as that defined in the product. Modify the structure based on the site requirements.
            UpdateDesireds updateShadowRequest = new UpdateDesireds();
            List<UpdateDesired> shadow = new ArrayList<>();
            UpdateDesired updateDesired = new UpdateDesired();
            updateDesired.setServiceId("BasicData");
            Map<String, Object> map = new HashMap<>();
            map.put("luminance", 99);
            shadow.add(updateDesired);
            updateDesired.setDesired(map);
            updateShadowRequest.setShadow(shadow);
            // Update Device Shadow
            updateShadow(iotdaClient, deviceId, updateShadowRequest);
            logger.info("========2.Configuring the device shadow is complete========");

            logger.info("========3.Querying Device Shadow Details========");
            queryShadowDetail(iotdaClient, deviceId);
            logger.info("========3.Querying device shadow details is complete========");

            logger.info("========4.Creating a Task for Configuring Expected Values of Device Shadows in Batches========");
            // If you need to deliver the expected device shadow values of multiple devices at the same time,
            // you can use the function of setting expected device shadow values in batches.
            CreateBatchTask createBatchTask = new CreateBatchTask();
            createBatchTask.setAppId(appId);
            // Task name, which is unique in the resource space. If the task is invoked for multiple times, the task name needs to be changed.
            createBatchTask.setTaskName("batchShadows");
            // The task type is fixed to updateDeviceShadows. Do not change it.
            createBatchTask.setTaskType("updateDeviceShadows");
            // The target deviceId is the deviceId to be delivered and is added to the list.
            List<String> targetDeviceIds = new ArrayList<>();
            targetDeviceIds.add(deviceId);
            createBatchTask.setTargets(targetDeviceIds);
            // Expected value of the device shadow to be set. The format is as follows:
            // document {"shadow": [{"service_id": "WaterMeter","desired": {"temperature": "60"}}]}
            Map<String, Object> document = new HashMap<>();
            document.put("shadow", shadow);
            createBatchTask.setDocument(document);
            // After a task is created, obtain the task ID.
            CreateBatchTaskResponse createShadowTask = createTask(iotdaClient, createBatchTask);
            logger.info("========4.The task for configuring expected values of device shadows in batches is created successfully========");

            logger.info("========5.Querying Batch Task Details========");
            // Query the execution details of the batch task to know the task execution status.
            String status = createShadowTask.getStatus();
            int times = 0;
            // Wait until the task is complete. The task status is Initializing. Waiting: The task is waiting. Processing: The task is being executed.
            // Success: The operation is successful. Fail: failed. PartialSuccess: partial success. Stopped: stopped. StoppingStopping.
            while (("Initializing".equals(status) || "Waitting".equals(status) || "Processing".equals(status)) && times++ < 5) {
                // Query the execution details of the batch task to know the task execution status.
                Task task = queryTask(iotdaClient, createShadowTask.getTaskId());
                status = task.getStatus();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    logger.warn("Exception Information：{}", e.getMessage());
                }
            }
            logger.info("========5.Querying the batch task details is complete========");

            logger.info("========6.Querying Device Shadow Details========");
            // Querying and Delivering the Device Shadow List
            queryShadowDetail(iotdaClient, deviceId);
            logger.info("========6.Querying device shadow details is complete========");

            logger.info("========7.Deleting a Device Shadow========");
            // Deletes the expected value of the device shadow. If the value of an attribute is set to null, the expected value is deleted.
            map.put("luminance", null);
            updateShadow(iotdaClient, deviceId, updateShadowRequest);
            logger.info("========7.Deleting the device shadow is complete========");

            logger.info("========8.Querying device shadow details is complete========");
            queryShadowDetail(iotdaClient, deviceId);
            logger.info("========8.Querying device shadow details is complete========");
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.warn("Demonstration failed. Exception information is {}", e.getMessage());
        } catch (ServiceResponseException e) {
            logger.warn("Demonstration failed. Exception information is {}, {}", e.getErrorCode(), e.getErrorMsg());
        }
    }
}