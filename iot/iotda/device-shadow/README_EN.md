## 1. Functions

You can integrate the IoTDA server SDKs provided by Huawei Cloud to call IoTDA APIs and use IoTDA smoothly.
This example shows and demonstrates how to use the Java SDK to query and configure a device shadow as well as configure device shadows in batches.

Device Shadow: IoTDA supports the creation of device shadows. A device shadow is a JSON file that stores the device status, latest device properties reported, and device configurations to deliver. Each device has only one shadow. A device can retrieve and set its shadow to synchronize properties, either from the shadow to the device or from the device to the shadow.
For details, see [Device Shadow](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0049.html)

**What You Will Learn**

The ways to use the Java SDK to configure and query a device shadow.

## 2. Prerequisites
- 1. You have created a Huawei Cloud account and obtained the access key (AK) and secret access key (SK) of your account. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 2. You have set up the development environment with Java JDK 1.8 or later.
- 3. You have obtained the project ID of the target region. View the project ID on the **My Credentials** > **API Credentials** page of the Huawei Cloud console. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_1001.html).
- 4. You have enabled IoTDA. For details about how to obtain the ID of the current instance, see [Viewing Instance Details](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0121.html).
- 5. You have created a device and connected it to the platform for viewing interconnection details.

## 3. Obtaining and Installing the SDK
Download and install Maven, and add dependencies to the **pom.xml** file of the Java project.
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4. API Parameters
For details about API parameters, see:

[Query a Device Shadow](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0079.html)

[Configure Desired Properties in a Device Shadow](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0072.html)

[Create a Batch Task](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0045.html)

[Query a Batch Task](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0017.html)

## 5. Key Code Snippets
### 5.1 Configure Desired Values in a Device Shadow

```java
    /**
     * Configure Desired Values in a Device Shadow
     *
     * @param iotdaClient iotda client
     * @param deviceId    ID of the device whose device shadow needs to be configured
     * @param body        Structure for configuring the device shadow
     */
    private static void updateShadow(IoTDAClient iotdaClient, String deviceId, UpdateDesireds body) throws ServiceResponseException {
        UpdateDeviceShadowDesiredDataRequest request = new UpdateDeviceShadowDesiredDataRequest();
        request.setDeviceId(deviceId);
        request.setBody(body);
        request.setInstanceId(INSTANCE_ID);
        UpdateDeviceShadowDesiredDataResponse response = iotdaClient.updateDeviceShadowDesiredData(request);
        logger.info("The device shadow configuration result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.2 Query the Details About a Device Shadow

```java
    /**
     * Query the Details About a Device Shadow
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device ID
     */
    private static void queryShadowDetail(IoTDAClient iotdaClient, String deviceId) throws ServiceResponseException {
        ShowDeviceShadowRequest request = new ShowDeviceShadowRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        ShowDeviceShadowResponse messageResponse = iotdaClient.showDeviceShadow(request);
        logger.info("The device shadow query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, messageResponse));
    }
```

### 5.3 Create a Batch Task
```java
    /**
     * Create a Batch Task
     *
     * @param iotdaClient     iotda client
     * @param createBatchTask Structure for a batch task creation
     * @return The task obtained after the creation
     */
    private static CreateBatchTaskResponse createTask(IoTDAClient iotdaClient, CreateBatchTask createBatchTask) throws ServiceResponseException {
        CreateBatchTaskRequest request = new CreateBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(createBatchTask);
        CreateBatchTaskResponse batchTask = iotdaClient.createBatchTask(request);
        logger.info("The batch task creation result is:{}", JsonUtils.toJSON(OBJECTMAPPER, batchTask));
        return batchTask;
    }
```

### 5.4 Query the Details About a Batch Task
```java
    /**
     * Query the Details About a Batch Task
     *
     * @param iotdaClient iotda client
     * @param taskId      Task ID
     * @return The details of the task
     */
    private static Task queryTask(IoTDAClient iotdaClient, String taskId) throws ServiceResponseException {
        ShowBatchTaskRequest request = new ShowBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setTaskId(taskId);
        ShowBatchTaskResponse showBatchTaskResponse = iotdaClient.showBatchTask(request);
        logger.info("The batch task details query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, showBatchTaskResponse));
        return showBatchTaskResponse.getBatchtask();
    }
```

## 6. Execution Results
Replace the parameters and execute the code in the sequence of the main method. The program demonstrates how to query and configure a device shadow as well as configure device shadows in batches. The execution results of each step of the main method are shown as follows.

### 6.1 Query the Details About a Device Shadow(Before the Device Shadow Configuration)
```json
{
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "shadow": [
    {
      "service_id": "BasicData",
      "desired": {
        "properties": {},
        "event_time": "20230829T083942Z"
      },
      "reported": {
        "properties": {
          "luminance": "2"
        },
        "event_time": "20230904T021317Z"
      },
      "version": 22
    }
  ]
}
```
### 6.2 Configure a Device Shadow
```json
{
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "shadow": [
    {
      "service_id": "BasicData",
      "desired": {
        "properties": {
          "luminance": 99
        },
        "event_time": "20230904T095758Z"
      },
      "reported": {
        "properties": {
          "luminance": "2"
        },
        "event_time": "20230904T021317Z"
      },
      "version": 23
    }
  ]
}
```

### 6.3 Query a Device Shadow(After the Device Shadow Configuration)
```json
{
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "shadow": [
    {
      "service_id": "BasicData",
      "desired": {
        "properties": {
          "luminance": 99
        },
        "event_time": "20230904T095758Z"
      },
      "reported": {
        "properties": {
          "luminance": "2"
        },
        "event_time": "20230904T021317Z"
      },
      "version": 23
    }
  ]
}
```
### 6.4 Create a Batch Task
```json
{
  "task_id": "64f5bf901a98d00c657d2886",
  "task_name": "batchShadows_1",
  "task_type": "updateDeviceShadows",
  "targets": [
    "64111bcd06ca5933b28a058b_7758dsfdsfsd"
  ],
  "targets_filter": {},
  "document": {
    "shadow": [
      {
        "service_id": "BasicData",
        "desired": {
          "luminance": 100
        }
      }
    ]
  },
  "task_policy": {},
  "status": "Initializing",
  "task_progress": {
    "total": 0,
    "processing": 0,
    "success": 0,
    "fail": 0,
    "waitting": 0,
    "fail_wait_retry": 0,
    "stopped": 0,
    "removed": 0
  },
  "create_time": "20230904T112920Z"
}
```


### 6.5 Query the Details About a Batch Task
```json
{
  "batchtask": {
    "task_id": "64f5bf901a98d00c657d2886",
    "task_name": "batchShadows_1",
    "task_type": "updateDeviceShadows",
    "targets": [
      "64111bcd06ca5933b28a058b_7758dsfdsfsd"
    ],
    "targets_filter": {},
    "document": {
      "shadow": [
        {
          "service_id": "BasicData",
          "desired": {
            "luminance": 100
          }
        }
      ]
    },
    "task_policy": {},
    "status": "Processing",
    "status_desc": "",
    "task_progress": {
      "total": 1,
      "processing": 1,
      "success": 0,
      "fail": 0,
      "waitting": 0,
      "fail_wait_retry": 0,
      "stopped": 0,
      "removed": 0
    },
    "create_time": "20230904T112920Z"
  },
  "task_details": [
    {
      "target": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
      "status": "Processing",
      "error": {}
    }
  ],
  "page": {
    "count": 1,
    "marker": "64f5bf901a98d00c657d2888"
  }
}
```

### 6.6 Query the Details About a Device Shadow(After the Batch Task)
```json
{
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "shadow": [
    {
      "service_id": "BasicData",
      "desired": {
        "properties": {
          "luminance": 100
        },
        "event_time": "20230904T112930Z"
      },
      "reported": {
        "properties": {
          "luminance": "2"
        },
        "event_time": "20230904T021317Z"
      },
      "version": 24
    }
  ]
}
```

### 6.7 Delete the Desired Value of a Device Shadow
```json
{
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "shadow": [
    {
      "service_id": "BasicData",
      "desired": {
        "properties": {},
        "event_time": "20230904T112931Z"
      },
      "reported": {
        "properties": {
          "luminance": "2"
        },
        "event_time": "20230904T021317Z"
      },
      "version": 25
    }
  ]
}
```
### 6.8 Device Shadow Details Queried(After the Device Shadow Desired Deletion)
```json
{
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "shadow": [
    {
      "service_id": "BasicData",
      "desired": {
        "properties": {},
        "event_time": "20230904T112931Z"
      },
      "reported": {
        "properties": {
          "luminance": "2"
        },
        "event_time": "20230904T021317Z"
      },
      "version": 25
    }
  ]
}
```