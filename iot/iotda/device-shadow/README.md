## 1、功能介绍

华为云提供了IoTDA服务端SDK，您可以直接集成服务端SDK来调用IoTDA的相关API，从而实现对IoTDA的快速操作。
该示例展示了如何通过java版SDK来实现对设备影子进行配置，查询，批量配置设备影子操作。

设备影子：物联网平台支持创建设备的“影子”。设备影子是一个JSON文件，用于存储设备的在线状态、设备最近一次上报的设备属性值、应用服务器期望下发的配置。
每个设备有且只有一个设备影子，设备可以获取和设置设备影子以此来同步设备属性值，这个同步可以是影子同步给设备，也可以是设备同步给影子。
详情请参考[设备影子](https://support.huaweicloud.com/usermanual-iothub/iot_01_0049.html)

**您将学到什么？**

如何通过java版SDK来实现对配置设备影子，以及查询设备影子的功能。

## 2、前置条件
- 1、已经开通华为云账号，并获得到您账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。您可以参考[获取项目ID](https://support.huaweicloud.com/api-iothub/iot_06_v5_1001.html)
- 4、已经开通IoTDA服务，获取当前实例的实例ID，请参考[查看实例详情](https://support.huaweicloud.com/usermanual-iothub/iot_01_0121.html) 。
- 5、已经在平台注册一个设备，建议设备处于在线状态，可以观察到平台与设备交互过程。

## 3、SDK获取和安装
您需要下载并安装 Maven，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4、接口参数说明
关于接口参数的详细说明可参见：

[查询设备影子数据](https://support.huaweicloud.com/api-iothub/iot_06_v5_0079.html)

[配置设备影子预期数据](https://support.huaweicloud.com/api-iothub/iot_06_v5_0072.html)

[创建批量任务](https://support.huaweicloud.com/api-iothub/iot_06_v5_0045.html)

[查询批量任务](https://support.huaweicloud.com/api-iothub/iot_06_v5_0017.html)

## 5、关键代码片段
### 5.1、配置设备影子期望值

```java
    /**
     * 配置设备影子期望值
     *
     * @param iotdaClient iotda client
     * @param deviceId    需要配置影子的设备ID
     * @param body        需要配置设备影子的结构体
     */
    private static void updateShadow(IoTDAClient iotdaClient, String deviceId, UpdateDesireds body) throws ServiceResponseException {
        UpdateDeviceShadowDesiredDataRequest request = new UpdateDeviceShadowDesiredDataRequest();
        request.setDeviceId(deviceId);
        request.setBody(body);
        request.setInstanceId(INSTANCE_ID);
        UpdateDeviceShadowDesiredDataResponse response = iotdaClient.updateDeviceShadowDesiredData(request);
        logger.info("创建设备影子的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.2、查询设备影子详情

```java
    /**
     * 查询设备影子详情
     *
     * @param iotdaClient iotda client
     * @param deviceId    设备ID
     */
    private static void queryShadowDetail(IoTDAClient iotdaClient, String deviceId) throws ServiceResponseException {
        ShowDeviceShadowRequest request = new ShowDeviceShadowRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        ShowDeviceShadowResponse messageResponse = iotdaClient.showDeviceShadow(request);
        logger.info("查询设备影子的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, messageResponse));
    }
```

### 5.3、创建批量任务
```java
    /**
     * 创建批量任务
     *
     * @param iotdaClient     iotda client
     * @param createBatchTask 创建批量任务结构体
     * @return 创建完成后获取的任务
     */
    private static CreateBatchTaskResponse createTask(IoTDAClient iotdaClient, CreateBatchTask createBatchTask) throws ServiceResponseException {
        CreateBatchTaskRequest request = new CreateBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(createBatchTask);
        CreateBatchTaskResponse batchTask = iotdaClient.createBatchTask(request);
        logger.info("创建批量任务的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, batchTask));
        return batchTask;
    }
```

### 5.4、查询批量任务详情
```java
    /**
     * 查询批量任务详情
     *
     * @param iotdaClient iotda client
     * @param taskId      任务ID
     * @return 返回任务整体情况
     */
    private static Task queryTask(IoTDAClient iotdaClient, String taskId) throws ServiceResponseException {
        ShowBatchTaskRequest request = new ShowBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setTaskId(taskId);
        ShowBatchTaskResponse showBatchTaskResponse = iotdaClient.showBatchTask(request);
        logger.info("查询任务详情的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, showBatchTaskResponse));
        return showBatchTaskResponse.getBatchtask();
    }
```

## 6、运行结果
如果您替换main方法的参数，并按照main方法的顺序执行，程序将演示如果配置设备影子，查询设备影子，批量配置设备影子。
以下为main方法中每一个步骤的运行结果。

### 6.1 查询设备影子详情(配置设备影子前)
```json
{
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "shadow": [
    {
      "service_id": "BasicData",
      "desired": {
        "properties": {},
        "event_time": "20230829T083942Z"
      },
      "reported": {
        "properties": {
          "luminance": "2"
        },
        "event_time": "20230904T021317Z"
      },
      "version": 22
    }
  ]
}
```
### 6.2 配置设备影子
```json
{
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "shadow": [
    {
      "service_id": "BasicData",
      "desired": {
        "properties": {
          "luminance": 99
        },
        "event_time": "20230904T095758Z"
      },
      "reported": {
        "properties": {
          "luminance": "2"
        },
        "event_time": "20230904T021317Z"
      },
      "version": 23
    }
  ]
}
```

### 6.3 查询设备影子(配置设备影子后)
```json
{
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "shadow": [
    {
      "service_id": "BasicData",
      "desired": {
        "properties": {
          "luminance": 99
        },
        "event_time": "20230904T095758Z"
      },
      "reported": {
        "properties": {
          "luminance": "2"
        },
        "event_time": "20230904T021317Z"
      },
      "version": 23
    }
  ]
}
```
### 6.4 创建批量任务
```json
{
  "task_id": "64f5bf901a98d00c657d2886",
  "task_name": "batchShadows_1",
  "task_type": "updateDeviceShadows",
  "targets": [
    "64111bcd06ca5933b28a058b_7758dsfdsfsd"
  ],
  "targets_filter": {},
  "document": {
    "shadow": [
      {
        "service_id": "BasicData",
        "desired": {
          "luminance": 100
        }
      }
    ]
  },
  "task_policy": {},
  "status": "Initializing",
  "task_progress": {
    "total": 0,
    "processing": 0,
    "success": 0,
    "fail": 0,
    "waitting": 0,
    "fail_wait_retry": 0,
    "stopped": 0,
    "removed": 0
  },
  "create_time": "20230904T112920Z"
}
```


### 6.5 查询批量任务详情
```json
{
  "batchtask": {
    "task_id": "64f5bf901a98d00c657d2886",
    "task_name": "batchShadows_1",
    "task_type": "updateDeviceShadows",
    "targets": [
      "64111bcd06ca5933b28a058b_7758dsfdsfsd"
    ],
    "targets_filter": {},
    "document": {
      "shadow": [
        {
          "service_id": "BasicData",
          "desired": {
            "luminance": 100
          }
        }
      ]
    },
    "task_policy": {},
    "status": "Processing",
    "status_desc": "",
    "task_progress": {
      "total": 1,
      "processing": 1,
      "success": 0,
      "fail": 0,
      "waitting": 0,
      "fail_wait_retry": 0,
      "stopped": 0,
      "removed": 0
    },
    "create_time": "20230904T112920Z"
  },
  "task_details": [
    {
      "target": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
      "status": "Processing",
      "error": {}
    }
  ],
  "page": {
    "count": 1,
    "marker": "64f5bf901a98d00c657d2888"
  }
}
```

### 6.6 查询设备影子详情(批量任务后)
```json
{
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "shadow": [
    {
      "service_id": "BasicData",
      "desired": {
        "properties": {
          "luminance": 100
        },
        "event_time": "20230904T112930Z"
      },
      "reported": {
        "properties": {
          "luminance": "2"
        },
        "event_time": "20230904T021317Z"
      },
      "version": 24
    }
  ]
}
```

### 6.7 删除设备影子期望值
```json
{
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "shadow": [
    {
      "service_id": "BasicData",
      "desired": {
        "properties": {},
        "event_time": "20230904T112931Z"
      },
      "reported": {
        "properties": {
          "luminance": "2"
        },
        "event_time": "20230904T021317Z"
      },
      "version": 25
    }
  ]
}
```
### 6.8 查询设备影子详情完成(删除设备影子后)
```json
{
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "shadow": [
    {
      "service_id": "BasicData",
      "desired": {
        "properties": {},
        "event_time": "20230904T112931Z"
      },
      "reported": {
        "properties": {
          "luminance": "2"
        },
        "event_time": "20230904T021317Z"
      },
      "version": 25
    }
  ]
}
```