## 1. Functions

You can integrate the IoTDA server SDKs provided by Huawei Cloud to call IoTDA APIs and use IoTDA smoothly.
This example shows and demonstrates how to use the Java SDK to bind tags to resources, unbind tags and search for resources using tags.

Tag: You can add tags to cloud resources for quicker search. You can view, modify, and delete these tags in a unified manner, facilitating cloud resource management. You can also use the tags to collect resource cost statistics from the service dimension.
For details, see [Tag Management - Overview](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0151.html)

**What You Will Learn**

The ways to use the Java SDK to bind tags to resources, unbind tags and search for resources using tags.

## 2. Prerequisites
- 1. You have created a Huawei Cloud account and obtained the access key (AK) and secret access key (SK) of your account. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 2. You have set up the development environment with Java JDK 1.8 or later.
- 3. You have obtained the project ID of the target region. View the project ID on the **My Credentials** > **API Credentials** page of the Huawei Cloud console. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_1001.html).
- 4. You have enabled IoTDA. For details about how to obtain the ID of the current instance, see [Viewing Instance Details](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0121.html).
- 5. You have registered a device on the platform for a simple demonstration of tags.

## 3. Obtaining and Installing the SDK
Download and install Maven, and add dependencies to the **pom.xml** file of the Java project.
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4. API Parameters
For details about API parameters, see:

[Bind Tags](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0009.html)

[Unbind Tags](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0010.html)

[Query Resources by Tag](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0030.html)

## 5. Key Code Snippets
### 5.1 Bind a Tag

```java
    /**
     * Bind tags to resource, currently only supporting devices.
     *
     * @param iotdaClient iotda client
     * @param body        Structure for binding a tag
     */
    private static void bindTags(IoTDAClient iotdaClient, BindTagsDTO body) throws ServiceResponseException {
        TagDeviceRequest request = new TagDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        TagDeviceResponse response = iotdaClient.tagDevice(request);
        logger.info("The binding tag result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.2 Query Resources by Tags

```java
    /**
     * Query Resources by Tags
     *
     * @param iotdaClient iotda client
     * @param body        Structure for querying resources by tags
     */
    private static void queryResourcesByTags(IoTDAClient iotdaClient, QueryResourceByTagsDTO body) throws ServiceResponseException {
        ListResourcesByTagsRequest request = new ListResourcesByTagsRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        ListResourcesByTagsResponse response = iotdaClient.listResourcesByTags(request);
        logger.info("The result of querying resources by tags is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.3 Unbind a Tag

```java
    /**
     * Unbind a Tag
     *
     * @param iotdaClient iotda client
     * @param body        Structure for unbinding a tag
     */
    private static void unbindTags(IoTDAClient iotdaClient, UnbindTagsDTO body) throws ServiceResponseException {
        UntagDeviceRequest request = new UntagDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        UntagDeviceResponse response = iotdaClient.untagDevice(request);
        logger.info("The unbinding tag result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

## 6. Execution Results
Replace the parameters and execute the code in the sequence of the main method. The program demonstrates how to bind tags to resources, unbind tags and search for resources using tags.
The execution results of each step of the main method are shown as follows.

### 6.1 Query the Resource List by Tags(Before Binding Tags)

Because the tags are not created yet, the query result is empty.
```json
{
  "resources": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```
### 6.2 Bind Tags to Device
```json
{}
```

### 6.3 Query the Resource List by Tags(After Binding Tags)
```json
{
  "resources": [
    {
      "resource_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd"
    }
  ],
  "page": {
    "count": 1,
    "marker": "6462fbb16af16627fd5849bd"
  }
}
```
### 6.4 Unbind Tags to Resources
```json
{}
```


### 6.5 Query the Resource List by Tags(After Unbinding Tags)
```json
{
  "resources": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```