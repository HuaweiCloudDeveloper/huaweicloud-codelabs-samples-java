package com.huawei.iotda;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.JsonUtils;
import com.huaweicloud.sdk.iotda.v5.IoTDAClient;
import com.huaweicloud.sdk.iotda.v5.model.BindTagsDTO;
import com.huaweicloud.sdk.iotda.v5.model.ListResourcesByTagsRequest;
import com.huaweicloud.sdk.iotda.v5.model.ListResourcesByTagsResponse;
import com.huaweicloud.sdk.iotda.v5.model.QueryResourceByTagsDTO;
import com.huaweicloud.sdk.iotda.v5.model.TagDeviceRequest;
import com.huaweicloud.sdk.iotda.v5.model.TagDeviceResponse;
import com.huaweicloud.sdk.iotda.v5.model.TagV5DTO;
import com.huaweicloud.sdk.iotda.v5.model.UnbindTagsDTO;
import com.huaweicloud.sdk.iotda.v5.model.UntagDeviceRequest;
import com.huaweicloud.sdk.iotda.v5.model.UntagDeviceResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TagManagement {
    private static final Logger logger = LoggerFactory.getLogger(TagManagement.class);
    private static final ObjectMapper OBJECTMAPPER = new ObjectMapper();

    // REGION_ID：If your region is 上海一，please fill in "cn-east-3"；If your region is 北京四，please fill in "cn-north-4"; If your region is 华南广州，please fill in "cn-south-4"
    private static final String REGION_ID = "<YOUR REGION ID>";

    // ENDPOINT：On the console, choose **Overview** in the navigation pane and click **Access Addresses** to view the HTTPS application access address.
    private static final String ENDPOINT = "<YOUR ENDPOINT>";

    // ak/sk：For details, see the method of obtaining AK/SK in iotda document https://support.huaweicloud.com/api-iothub/iot_06_v5_0091.html.
    // There will be security risks if the AK/SK used for authentication is directly written into code. We suggest encrypting the AK/SK in the configuration file or environment variables for storage.
    // In this sample, the AK/SK is stored in environment variables for identity authentication. Before running this sample, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String PROJECT_ID = "<YOUR PROJECTID>";

    // Specifies the instance ID, which can be obtained from the current instance on the IoTDA overview page.
    private static final String INSTANCE_ID = "<YOUR INSTANCEID>";

    private static IoTDAClient initClient() {
        // Create a credential
        ICredential auth = new BasicCredentials()
            .withAk(AK)
            .withSk(SK)
            // Derivative algorithms are used for the standard and enterprise editions. For the basic edition, delete the configuration "withDerivedPredicate".
            .withDerivedPredicate(BasicCredentials.DEFAULT_DERIVED_PREDICATE)
            .withProjectId(PROJECT_ID);

        // Create and initialize an IoTDAClient instance
        return IoTDAClient.newBuilder()
            .withCredential(auth)
            // Use IoTDARegion for basic editions like "withRegion(IoTDARegion.CN_NORTH_4)". Create regions for standard/enterprise editions.
            .withRegion(new Region(REGION_ID, ENDPOINT))
            // .withRegion(IoTDARegion.CN_NORTH_4)
            .build();
    }

    /**
     * Bind tags to resource, currently only supporting devices.
     *
     * @param iotdaClient iotda client
     * @param body        Structure for binding a tag
     */
    private static void bindTags(IoTDAClient iotdaClient, BindTagsDTO body) throws ServiceResponseException {
        TagDeviceRequest request = new TagDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        TagDeviceResponse response = iotdaClient.tagDevice(request);
        logger.info("The binding tag result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    /**
     * Query Resources by Tags
     *
     * @param iotdaClient iotda client
     * @param body        Structure for querying resources by tags
     */
    private static void queryResourcesByTags(IoTDAClient iotdaClient, QueryResourceByTagsDTO body) throws ServiceResponseException {
        ListResourcesByTagsRequest request = new ListResourcesByTagsRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        ListResourcesByTagsResponse response = iotdaClient.listResourcesByTags(request);
        logger.info("The result of querying resources by tags is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    /**
     * Unbind a Tag
     *
     * @param iotdaClient iotda client
     * @param body        Structure for unbinding a tag
     */
    private static void unbindTags(IoTDAClient iotdaClient, UnbindTagsDTO body) throws ServiceResponseException {
        UntagDeviceRequest request = new UntagDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        UntagDeviceResponse response = iotdaClient.untagDevice(request);
        logger.info("The unbinding tag result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    public static void main(String[] args) {
        // Initialize the IoTDA client
        IoTDAClient iotdaClient = initClient();

        // Resource type. Currently, only device is supported.
        String resourceType = "device";

        // Devices that need to bind labels
        String deviceId = "";

        String tagKey = "huawei";
        String tahValue = "iot";

        try {
            // Assemble the DTO required for querying tag resources.
            QueryResourceByTagsDTO queryResourceByTagsDTO = new QueryResourceByTagsDTO();
            // Currently, only the device resource type is supported.
            queryResourceByTagsDTO.setResourceType(resourceType);
            // List of tags to be bound. This parameter is used for querying and binding tags.
            List<TagV5DTO> tagV5DTOS = new ArrayList<>();
            TagV5DTO tagV5DTO = new TagV5DTO();
            tagV5DTO.setTagKey(tagKey);
            tagV5DTO.setTagValue(tahValue);
            tagV5DTOS.add(tagV5DTO);
            queryResourceByTagsDTO.setTags(tagV5DTOS);

            logger.info("========1.Querying the List of Tag Resources========");
            queryResourcesByTags(iotdaClient, queryResourceByTagsDTO);
            logger.info("========1.Querying the tag resource list is complete========");

            logger.info("========2.Device Binding Tag========");
            BindTagsDTO bindTagsDTO = new BindTagsDTO();
            bindTagsDTO.setTags(tagV5DTOS);
            bindTagsDTO.setResourceType(resourceType);
            bindTagsDTO.setResourceId(deviceId);
            bindTags(iotdaClient, bindTagsDTO);
            logger.info("========2.The device is bound to a tag========");

            logger.info("========3.Querying the List of Tag Resources========");
            queryResourcesByTags(iotdaClient, queryResourceByTagsDTO);
            logger.info("========3.Querying the tag resource list is complete========");

            // Unbinding a Device Tag
            logger.info("========4.Device Unbinding Tag========");
            UnbindTagsDTO unbindTagsDTO = new UnbindTagsDTO();
            unbindTagsDTO.setResourceType(resourceType);
            unbindTagsDTO.setResourceId(deviceId);
            // List of tags to be unbound. Multiple tag keys can be entered.
            unbindTagsDTO.setTagKeys(Collections.singletonList(tagKey));
            unbindTags(iotdaClient, unbindTagsDTO);
            logger.info("========4.Device unbinding tag completed========");

            logger.info("========5.Querying the List of Tag Resources========");
            queryResourcesByTags(iotdaClient, queryResourceByTagsDTO);
            logger.info("========5.Querying the tag resource list is complete========");
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.warn("Demonstration failed. Exception information is {}", e.getMessage());
        } catch (ServiceResponseException e) {
            logger.warn("Demonstration failed. Exception information is {}, {}", e.getErrorCode(), e.getErrorMsg());
        }
    }
}
