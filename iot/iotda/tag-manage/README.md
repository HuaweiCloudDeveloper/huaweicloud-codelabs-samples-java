## 1、功能介绍

华为云提供了IoTDA服务端SDK，您可以直接集成服务端SDK来调用IoTDA的相关API，从而实现对IoTDA的快速操作。
该示例展示了如何通过java版SDK来实现资源进行绑定，解绑标签，使用标签检索资源操作。

标签：对于拥有大量云资源的用户，可以通过给云资源打标签，快速查找具有某标签的云资源，
可对这些资源标签统一进行检视、修改、删除等操作，方便用户对云资源的管理。还可利用标签功能实现基于业务维度的资源成本统计。
详情请参考[标签概述](https://support.huaweicloud.com/usermanual-iothub/iot_01_0151.html)

**您将学到什么？**

如何通过java版SDK来实现资源进行绑定，解绑标签，使用标签检索资源操作。

## 2、前置条件
- 1、已经开通华为云账号，并获得到您账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。您可以参考[获取项目ID](https://support.huaweicloud.com/api-iothub/iot_06_v5_1001.html) 。
- 4、已经开通IoTDA服务，获取当前实例的实例ID，请参考[查看实例详情](https://support.huaweicloud.com/usermanual-iothub/iot_01_0121.html) 。
- 5、已经在平台注册一个设备用于标签简单演示。

## 3、SDK获取和安装
您需要下载并安装 Maven，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4、接口参数说明
关于接口参数的详细说明可参见：

[绑定标签](https://support.huaweicloud.com/api-iothub/iot_06_v5_0009.html)

[解绑标签](https://support.huaweicloud.com/api-iothub/iot_06_v5_0010.html)

[按标签查询资源](https://support.huaweicloud.com/api-iothub/iot_06_v5_0030.html)

## 5、关键代码片段
### 5.1、绑定标签

```java
    /**
     * 给资源打标签，目前资源仅支持设备
     *
     * @param iotdaClient iotda client
     * @param body        标签结构体
     */
    private static void bindTags(IoTDAClient iotdaClient, BindTagsDTO body) throws ServiceResponseException {
        TagDeviceRequest request = new TagDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        TagDeviceResponse response = iotdaClient.tagDevice(request);
        logger.info("绑定标签的的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.2、通过标签查询资源

```java
    /**
     * 通过标签查询资源
     *
     * @param iotdaClient iotda client
     * @param body        查询资源结构体
     */
    private static void queryResourcesByTags(IoTDAClient iotdaClient, QueryResourceByTagsDTO body) throws ServiceResponseException {
        ListResourcesByTagsRequest request = new ListResourcesByTagsRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        ListResourcesByTagsResponse response = iotdaClient.listResourcesByTags(request);
        logger.info("根据标签查询资源列表的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.3、解绑标签

```java
    /**
     * 资源解绑标签
     *
     * @param iotdaClient iotda client
     * @param body        解绑标签结构体
     */
    private static void unbindTags(IoTDAClient iotdaClient, UnbindTagsDTO body) throws ServiceResponseException {
        UntagDeviceRequest request = new UntagDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        UntagDeviceResponse response = iotdaClient.untagDevice(request);
        logger.info("解绑标签的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

## 6、运行结果
如果您替换main方法的参数，并按照main方法的顺序执行，程序将演示给设备绑定标签，解绑标签功能。
以下为main方法中每一个步骤的运行结果。

### 6.1 查询标签资源列表(绑定标签前)

由于还未绑定标签，查询结果为空
```json
{
  "resources": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```
### 6.2 设备绑定标签
```json
{}
```

### 6.3 查询标签资源列表(绑定标签后)
```json
{
  "resources": [
    {
      "resource_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd"
    }
  ],
  "page": {
    "count": 1,
    "marker": "6462fbb16af16627fd5849bd"
  }
}
```
### 6.4 设备解绑标签
```json
{}
```


### 6.5 查询标签资源列表(解绑标签后)
```json
{
  "resources": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```