## 1. Functions

You can integrate the IoTDA server SDKs provided by Huawei Cloud to call IoTDA APIs and use IoTDA smoothly.
This example shows how to use the Java SDK to deliver messages to devices (individually or in batches), query messages and query batch message delivery tasks.

Message delivery: Message delivery does not rely on product models. The platform provides one-way notifications for devices and caches messages. It delivers messages from the cloud to devices in asynchronous mode (without waiting for responses from devices). If a device is offline, data is sent after the device is online. The maximum cache duration is 24 hours.
For details, see [Message Delivery Overview](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0332.html)

**What You Will Learn**

The ways to use the Java SDK to deliver and query device messages, and to use batch task to deliver batch messages and query batch message delivery tasks.

## 2. Prerequisites
- 1. You have created a Huawei Cloud account and obtained the access key (AK) and secret access key (SK) of your account. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 2. You have set up the development environment with Java JDK 1.8 or later.
- 3. You have obtained the project ID of the target region. View the project ID on the **My Credentials** > **API Credentials** page of the Huawei Cloud console. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_1001.html).
- 4. You have enabled IoTDA. For details about how to obtain the ID of the current instance, see [Viewing Instance Details](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0121.html).
- 5. You have created a device and connected it to the platform for viewing interconnection details.

## 3. Obtaining and Installing the SDK
Download and install Maven, and add dependencies to the **pom.xml** file of the Java project.
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4. API Parameters
For details about API parameters, see:

[Deliver a Message to a Device](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0059.html)

[Query Device Messages](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0058.html)

[Query a Message by Message ID](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0057.html)

[Create a Batch Task](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0045.html)

[Query a Batch Task](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0017.html)

## 5. Key Code Snippets
### 5.1 Deliver a Device Message

```java
    /**
     * Deliver a device message
     *
     * @param iotdaClient iotda client
     * @param deviceId    Message recipient device
     * @param body        Message to deliver
     * @return Message Id
     */
    private static String createMessage(IoTDAClient iotdaClient, String deviceId, DeviceMessageRequest body) throws ServiceResponseException {
        CreateMessageRequest request = new CreateMessageRequest();
        request.setDeviceId(deviceId);
        request.setBody(body);
        request.setInstanceId(INSTANCE_ID);
        CreateMessageResponse message = iotdaClient.createMessage(request);
        logger.info("The device message creation result is {}", JsonUtils.toJSON(OBJECTMAPPER, message));
        return message.getMessageId();
    }
```

### 5.2 Query a Device Message List

```java
    /**
     * Query a device messsage list
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device ID of the device that needs to query
     */
    private static void queryMessageList(IoTDAClient iotdaClient, String deviceId) {
        ListDeviceMessagesRequest request = new ListDeviceMessagesRequest();
        request.setDeviceId(deviceId);
        request.setInstanceId(INSTANCE_ID);
        ListDeviceMessagesResponse messageResponse = iotdaClient.listDeviceMessages(request);
        logger.info("The message list query result is {}", JsonUtils.toJSON(OBJECTMAPPER, messageResponse));
    }
```

### 5.3 Query a Message by Message ID

```java
    /**
     * Query a message by message ID
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device ID
     * @param messageId   Message ID
     */
    private static void queryMessageDetail(IoTDAClient iotdaClient, String deviceId, String messageId) throws ServiceResponseException {
        ShowDeviceMessageRequest request = new ShowDeviceMessageRequest();
        request.setDeviceId(deviceId);
        request.setMessageId(messageId);
        request.setInstanceId(INSTANCE_ID);
        ShowDeviceMessageResponse messageResponse = iotdaClient.showDeviceMessage(request);
        logger.info("The device message query result is {}", JsonUtils.toJSON(OBJECTMAPPER, messageResponse));
    }
```

### 5.4 Create a Batch Task
```java
    /**
     * Create a batch task
     *
     * @param iotdaClient     iotda client
     * @param createBatchTask Structure for creating a batch task
     * @return The task obtained after the creation
     */
    private static CreateBatchTaskResponse createTask(IoTDAClient iotdaClient, CreateBatchTask createBatchTask) throws ServiceResponseException {
        CreateBatchTaskRequest request = new CreateBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(createBatchTask);
        CreateBatchTaskResponse batchTask = iotdaClient.createBatchTask(request);
        logger.info("The batch task creation result is {}", JsonUtils.toJSON(OBJECTMAPPER, batchTask));
        return batchTask;
    }
```

### 5.5 Query Details About a Batch Task
```java
    /**
     * Query Details About a Batch Task
     *
     * @param iotdaClient iotda client
     * @param taskId      Task ID
     * @return Task details
     */
    private static Task queryTask(IoTDAClient iotdaClient, String taskId) throws ServiceResponseException {
        ShowBatchTaskRequest request = new ShowBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setTaskId(taskId);
        ShowBatchTaskResponse showBatchTaskResponse = iotdaClient.showBatchTask(request);
        logger.info("The task query result is {}", JsonUtils.toJSON(OBJECTMAPPER, showBatchTaskResponse));
        return showBatchTaskResponse.getBatchtask();
    }
```

## 6. Execution Results
Replace the parameters and execute the code in the sequence of the main method. The program demonstrates how to send "Hello World" message to the device.
The execution results of each step of the main method are shown as follows.

### 6.1 Deliver a Device Message
```json
{
  "message_id": "a3288605-5866-41f9-a35b-23f5dfebca30",
  "result": {
    "status": "DELIVERED",
    "created_time": "20230831T092338Z",
    "finished_time": "20230831T092338Z"
  }
}
```
### 6.2 Query a Message
```json
{
  "message_id": "a3288605-5866-41f9-a35b-23f5dfebca30",
  "message": "hello world",
  "encoding": "none",
  "payload_format": "standard",
  "status": "DELIVERED",
  "created_time": "20230831T092338Z",
  "finished_time": "20230831T092338Z"
}
```
### 6.3 Create a Batch Task
```json
{
  "task_id": "64f05c1a8c2b6271ddeb0852",
  "task_name": "batchMessages6",
  "task_type": "createMessages",
  "targets": [
    "64111bcd06ca5933b28a058b_7758dsfdsfsd"
  ],
  "targets_filter": {},
  "document": {
    "message": "hello world"
  },
  "task_policy": {},
  "status": "Initializing",
  "task_progress": {
    "total": 0,
    "processing": 0,
    "success": 0,
    "fail": 0,
    "waitting": 0,
    "fail_wait_retry": 0,
    "stopped": 0,
    "removed": 0
  },
  "create_time": "20230831T092338Z"
}
```
### 6.4 Query Task Details
```json
{
  "batchtask": {
    "task_id": "64f05c1a8c2b6271ddeb0852",
    "task_name": "batchMessages6",
    "task_type": "createMessages",
    "targets": [
      "64111bcd06ca5933b28a058b_7758dsfdsfsd"
    ],
    "targets_filter": {},
    "document": {
      "message": "hello world"
    },
    "task_policy": {},
    "status": "Success",
    "status_desc": "",
    "task_progress": {
      "total": 1,
      "processing": 0,
      "success": 1,
      "fail": 0,
      "waitting": 0,
      "fail_wait_retry": 0,
      "stopped": 0,
      "removed": 0
    },
    "create_time": "20230831T092338Z"
  },
  "task_details": [
    {
      "target": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
      "status": "Success",
      "output": "{\"message_id\":\"fc513b23-1e5d-472c-a574-dd80a71251e9\",\"result\":{\"status\":\"DELIVERED\",\"created_time\":\"20230831T092340Z\",\"finished_time\":\"20230831T092340Z\"}}",
      "error": {}
    }
  ],
  "page": {
    "count": 1,
    "marker": "64f05c1a8c2b6271ddeb0854"
  }
}
```
### 6.5 Query a Message List

You can query two messages that are delivered to the device separately or in batches.
```json
{
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "messages": [
    {
      "message_id": "fc513b23-1e5d-472c-a574-dd80a71251e9",
      "message": "hello world",
      "encoding": "none",
      "payload_format": "standard",
      "status": "DELIVERED",
      "created_time": "20230831T092340Z",
      "finished_time": "20230831T092340Z"
    },
    {
      "message_id": "a3288605-5866-41f9-a35b-23f5dfebca30",
      "message": "hello world",
      "encoding": "none",
      "payload_format": "standard",
      "status": "DELIVERED",
      "created_time": "20230831T092338Z",
      "finished_time": "20230831T092338Z"
    }
  ]
}
```