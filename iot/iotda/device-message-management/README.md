## 1、功能介绍

华为云提供了IoTDA服务端SDK，您可以直接集成服务端SDK来调用IoTDA的相关API，从而实现对IoTDA的快速操作。
该示例展示了如何通过java版SDK来实现消息下发，消息查询，批量下发消息，查询批量下发消息任务详情。

消息下发：消息下发不依赖产品模型，提供给设备的单向通知，具有消息缓存功能；云端消息下发中，平台会以异步方式（消息下发后无需等待设备侧回复响应）下发消息给设备；若设备不在线，则在设备在线后发送数据（支持配置，最长缓存时间24小时）。
详情请参考[消息下发概述](https://support.huaweicloud.com/usermanual-iothub/iot_01_0332.html)

**您将学到什么？**

如何通过java版SDK来处理设备消息的下发，查询，如何通过批量任务实现批量消息下发，查询批量任务详情。

## 2、前置条件
- 1、已经开通华为云账号，并获得到您账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。您可以参考[获取项目ID](https://support.huaweicloud.com/api-iothub/iot_06_v5_1001.html)
- 4、已经开通IoTDA服务，获取当前实例的实例ID，请参考[查看实例详情](https://support.huaweicloud.com/usermanual-iothub/iot_01_0121.html) 。 
- 5、并且存在一个设备，建议设备已经连接到平台，你可以具体看到平台与设备交互。

## 3、SDK获取和安装
您需要下载并安装 Maven，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4、接口参数说明
关于接口参数的详细说明可参见：

[下发设备消息](https://support.huaweicloud.com/api-iothub/iot_06_v5_0059.html)

[查询设备消息](https://support.huaweicloud.com/api-iothub/iot_06_v5_0058.html)

[查询指定消息id的消息](https://support.huaweicloud.com/api-iothub/iot_06_v5_0057.html)

[创建批量任务](https://support.huaweicloud.com/api-iothub/iot_06_v5_0045.html)

[查询批量任务](https://support.huaweicloud.com/api-iothub/iot_06_v5_0017.html)

## 5、关键代码片段
### 5.1、下发设备消息

```java
    /**
     * 下发设备消息
     *
     * @param iotdaClient iotda client
     * @param deviceId    需要下发消息的设备
     * @param body        需要下发的消息体
     * @return 消息Id
     */
    private static String createMessage(IoTDAClient iotdaClient, String deviceId, DeviceMessageRequest body) throws ServiceResponseException {
        CreateMessageRequest request = new CreateMessageRequest();
        request.setDeviceId(deviceId);
        request.setBody(body);
        request.setInstanceId(INSTANCE_ID);
        CreateMessageResponse message = iotdaClient.createMessage(request);
        logger.info("创建设备消息的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, message));
        return message.getMessageId();
    }
```

### 5.2、查询设备消息列表

```java
    /**
     * 查询消息列表
     *
     * @param iotdaClient iotda client
     * @param deviceId    需要查询的设备的设备Id
     */
    private static void queryMessageList(IoTDAClient iotdaClient, String deviceId) {
        ListDeviceMessagesRequest request = new ListDeviceMessagesRequest();
        request.setDeviceId(deviceId);
        request.setInstanceId(INSTANCE_ID);
        ListDeviceMessagesResponse messageResponse = iotdaClient.listDeviceMessages(request);
        logger.info("查询消息列表结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, messageResponse));
    }
```

### 5.3、查询指定id的消息详情

```java
    /**
     * 查询消息详情
     *
     * @param iotdaClient iotda客户端
     * @param deviceId    设备id
     * @param messageId   消息id
     */
    private static void queryMessageDetail(IoTDAClient iotdaClient, String deviceId, String messageId) throws ServiceResponseException {
        ShowDeviceMessageRequest request = new ShowDeviceMessageRequest();
        request.setDeviceId(deviceId);
        request.setMessageId(messageId);
        request.setInstanceId(INSTANCE_ID);
        ShowDeviceMessageResponse messageResponse = iotdaClient.showDeviceMessage(request);
        logger.info("查询设备消息的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, messageResponse));
    }
```

### 5.4、创建批量任务
```java
    /**
     * 创建批量任务
     *
     * @param iotdaClient     iotda client
     * @param createBatchTask 创建批量任务结构体
     * @return 创建完成后获取的任务
     */
    private static CreateBatchTaskResponse createTask(IoTDAClient iotdaClient, CreateBatchTask createBatchTask) throws ServiceResponseException {
        CreateBatchTaskRequest request = new CreateBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(createBatchTask);
        CreateBatchTaskResponse batchTask = iotdaClient.createBatchTask(request);
        logger.info("创建批量任务的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, batchTask));
        return batchTask;
    }
```

### 5.5、查询批量任务详情
```java
    /**
     * 查询批量任务详情
     *
     * @param iotdaClient iotda client
     * @param taskId      任务ID
     * @return 返回任务整体情况
     */
    private static Task queryTask(IoTDAClient iotdaClient, String taskId) throws ServiceResponseException {
        ShowBatchTaskRequest request = new ShowBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setTaskId(taskId);
        ShowBatchTaskResponse showBatchTaskResponse = iotdaClient.showBatchTask(request);
        logger.info("查询任务详情的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, showBatchTaskResponse));
        return showBatchTaskResponse.getBatchtask();
    }
```

## 6、运行结果
如果您替换main方法的参数，并按照main方法的顺序执行，程序将演示将hello world以消息的形式发送给设备。
以下为main方法中每一个步骤的运行结果。

### 6.1 下发设备消息
```json
{
  "message_id": "a3288605-5866-41f9-a35b-23f5dfebca30",
  "result": {
    "status": "DELIVERED",
    "created_time": "20230831T092338Z",
    "finished_time": "20230831T092338Z"
  }
}
```
### 6.2 查询消息详情
```json
{
  "message_id": "a3288605-5866-41f9-a35b-23f5dfebca30",
  "message": "hello world",
  "encoding": "none",
  "payload_format": "standard",
  "status": "DELIVERED",
  "created_time": "20230831T092338Z",
  "finished_time": "20230831T092338Z"
}
```
### 6.3 创建批量任务
```json
{
  "task_id": "64f05c1a8c2b6271ddeb0852",
  "task_name": "batchMessages6",
  "task_type": "createMessages",
  "targets": [
    "64111bcd06ca5933b28a058b_7758dsfdsfsd"
  ],
  "targets_filter": {},
  "document": {
    "message": "hello world"
  },
  "task_policy": {},
  "status": "Initializing",
  "task_progress": {
    "total": 0,
    "processing": 0,
    "success": 0,
    "fail": 0,
    "waitting": 0,
    "fail_wait_retry": 0,
    "stopped": 0,
    "removed": 0
  },
  "create_time": "20230831T092338Z"
}
```
### 6.4 查询任务详情
```json
{
  "batchtask": {
    "task_id": "64f05c1a8c2b6271ddeb0852",
    "task_name": "batchMessages6",
    "task_type": "createMessages",
    "targets": [
      "64111bcd06ca5933b28a058b_7758dsfdsfsd"
    ],
    "targets_filter": {},
    "document": {
      "message": "hello world"
    },
    "task_policy": {},
    "status": "Success",
    "status_desc": "",
    "task_progress": {
      "total": 1,
      "processing": 0,
      "success": 1,
      "fail": 0,
      "waitting": 0,
      "fail_wait_retry": 0,
      "stopped": 0,
      "removed": 0
    },
    "create_time": "20230831T092338Z"
  },
  "task_details": [
    {
      "target": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
      "status": "Success",
      "output": "{\"message_id\":\"fc513b23-1e5d-472c-a574-dd80a71251e9\",\"result\":{\"status\":\"DELIVERED\",\"created_time\":\"20230831T092340Z\",\"finished_time\":\"20230831T092340Z\"}}",
      "error": {}
    }
  ],
  "page": {
    "count": 1,
    "marker": "64f05c1a8c2b6271ddeb0854"
  }
}
```
### 6.5 查询消息列表

可以查询到通过单独下发意见批量任务下发的两条消息，且均下发到设备。
```json
{
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "messages": [
    {
      "message_id": "fc513b23-1e5d-472c-a574-dd80a71251e9",
      "message": "hello world",
      "encoding": "none",
      "payload_format": "standard",
      "status": "DELIVERED",
      "created_time": "20230831T092340Z",
      "finished_time": "20230831T092340Z"
    },
    {
      "message_id": "a3288605-5866-41f9-a35b-23f5dfebca30",
      "message": "hello world",
      "encoding": "none",
      "payload_format": "standard",
      "status": "DELIVERED",
      "created_time": "20230831T092338Z",
      "finished_time": "20230831T092338Z"
    }
  ]
}
```