package com.huawei.iotda;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.JsonUtils;
import com.huaweicloud.sdk.iotda.v5.IoTDAClient;
import com.huaweicloud.sdk.iotda.v5.model.CreateBatchTask;
import com.huaweicloud.sdk.iotda.v5.model.CreateBatchTaskRequest;
import com.huaweicloud.sdk.iotda.v5.model.CreateBatchTaskResponse;
import com.huaweicloud.sdk.iotda.v5.model.CreateMessageRequest;
import com.huaweicloud.sdk.iotda.v5.model.CreateMessageResponse;
import com.huaweicloud.sdk.iotda.v5.model.DeviceMessageRequest;
import com.huaweicloud.sdk.iotda.v5.model.ListDeviceMessagesRequest;
import com.huaweicloud.sdk.iotda.v5.model.ListDeviceMessagesResponse;
import com.huaweicloud.sdk.iotda.v5.model.ShowBatchTaskRequest;
import com.huaweicloud.sdk.iotda.v5.model.ShowBatchTaskResponse;
import com.huaweicloud.sdk.iotda.v5.model.ShowDeviceMessageRequest;
import com.huaweicloud.sdk.iotda.v5.model.ShowDeviceMessageResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.huaweicloud.sdk.iotda.v5.model.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class DeviceMessagesManagement {
    // Instance id
    private static final String INSTANCE_ID = "<YOUR INSTANCEID>";
    private static final String PROJECT_ID = "<YOUR PROJECTID>";

    private static final Logger logger = LoggerFactory.getLogger(DeviceMessagesManagement.class);

    // ENDPOINT：On the console, choose **Overview** in the navigation pane and click **Access Addresses** to view the HTTPS application access address.
    private static final String ENDPOINT = "<YOUR ENDPOINT>";

    private static final ObjectMapper OBJECTMAPPER = new ObjectMapper();

    // REGION_ID：If your region is 上海一，please fill in "cn-east-3"；
    // If your region is 北京四，please fill in "cn-north-4"; If your region is 华南广州，please fill in "cn-south-4"
    private static final String REGION_ID = "<YOUR REGION ID>";
    // ak/sk：For details, see the method of obtaining AK/SK in iotda document https://support.huaweicloud.com/api-iothub/iot_06_v5_0091.html.
    // There will be security risks if the AK/SK used for authentication is directly written into code. We suggest encrypting the AK/SK in the configuration file or environment variables for storage.
    // In this sample, the AK/SK is stored in environment variables for identity authentication. Before running this sample, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    private static IoTDAClient initClient() {
        // Create a credential
        ICredential auth = new BasicCredentials()
            .withSk(SK).withAk(AK)
            // Derivative algorithms are used for the standard and enterprise editions.
            // For the basic edition, delete the configuration "withDerivedPredicate".
            .withDerivedPredicate(BasicCredentials.DEFAULT_DERIVED_PREDICATE)
            .withProjectId(PROJECT_ID);

        // Create and initialize an IoTDAClient instance
        return IoTDAClient.newBuilder()
            .withCredential(auth)
            // Use IoTDARegion for basic editions like "withRegion(IoTDARegion.CN_NORTH_4)". Create regions for standard/enterprise editions.
            .withRegion(new Region(REGION_ID, ENDPOINT))
            // .withRegion(IoTDARegion.CN_NORTH_4)
            .build();
    }

    /**
     * Deliver a device message
     *
     * @param iotdaClient iotda client
     * @param deviceId    Message recipient device
     * @param body        Message to deliver
     * @return Message Id
     */
    private static String createMessage(IoTDAClient iotdaClient, String deviceId, DeviceMessageRequest body) throws ServiceResponseException {
        CreateMessageRequest request = new CreateMessageRequest();
        request.setDeviceId(deviceId);
        request.setBody(body);
        request.setInstanceId(INSTANCE_ID);
        CreateMessageResponse message = iotdaClient.createMessage(request);
        logger.info("The device message creation result is {}", JsonUtils.toJSON(OBJECTMAPPER, message));
        return message.getMessageId();
    }

    /**
     * Query a message by message ID
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device ID
     * @param messageId   Message ID
     */
    private static void queryMessageDetail(IoTDAClient iotdaClient, String deviceId, String messageId) throws ServiceResponseException {
        ShowDeviceMessageRequest request = new ShowDeviceMessageRequest();
        request.setDeviceId(deviceId);
        request.setMessageId(messageId);
        request.setInstanceId(INSTANCE_ID);
        ShowDeviceMessageResponse messageResponse = iotdaClient.showDeviceMessage(request);
        logger.info("The device message query result is {}", JsonUtils.toJSON(OBJECTMAPPER, messageResponse));
    }

    /**
     * Create a batch task
     *
     * @param iotdaClient     iotda client
     * @param createBatchTask Structure for creating a batch task
     * @return The task obtained after the creation
     */
    private static CreateBatchTaskResponse createTask(IoTDAClient iotdaClient, CreateBatchTask createBatchTask) throws ServiceResponseException {
        CreateBatchTaskRequest request = new CreateBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(createBatchTask);
        CreateBatchTaskResponse batchTask = iotdaClient.createBatchTask(request);
        logger.info("The batch task creation result is {}", JsonUtils.toJSON(OBJECTMAPPER, batchTask));
        return batchTask;
    }

    /**
     * Query Details About a Batch Task
     *
     * @param iotdaClient iotda client
     * @param taskId      Task ID
     * @return Task details
     */
    private static Task queryTask(IoTDAClient iotdaClient, String taskId) throws ServiceResponseException {
        ShowBatchTaskRequest request = new ShowBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setTaskId(taskId);
        ShowBatchTaskResponse showBatchTaskResponse = iotdaClient.showBatchTask(request);
        logger.info("The task query result is {}", JsonUtils.toJSON(OBJECTMAPPER, showBatchTaskResponse));
        return showBatchTaskResponse.getBatchtask();
    }

    /**
     * Query a messsage list
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device ID of the device that needs to query
     */
    private static void queryMessageList(IoTDAClient iotdaClient, String deviceId) {
        ListDeviceMessagesRequest request = new ListDeviceMessagesRequest();
        request.setDeviceId(deviceId);
        request.setInstanceId(INSTANCE_ID);
        ListDeviceMessagesResponse messageResponse = iotdaClient.listDeviceMessages(request);
        logger.info("The message list query result is {}", JsonUtils.toJSON(OBJECTMAPPER, messageResponse));
    }

    public static void main(String[] args) {
        // Initialize the IoTDA client
        IoTDAClient iotdaClient = initClient();
        // ID of the message recipient device. Replace the value with the required one.
        String deviceId = "64111bcd06ca5933b28a058b_saadas";
        // ID of the resource space to which the device belongs. Replace the value with the required one.
        String appId = "9d988b5efdf646f0828724c45e1d794e";

        try {
            logger.info("========1.Send a device message========");
            // Message sending structure
            DeviceMessageRequest messageRequest = new DeviceMessageRequest();
            messageRequest.setMessage("hello world");
            // Message delivery
            String messageId = createMessage(iotdaClient, deviceId, messageRequest);
            logger.info("========1.Device message delivery sent========");

            logger.info("========2.Query device message details========");
            queryMessageDetail(iotdaClient, deviceId, messageId);
            logger.info("========2.Device message details queried========");

            logger.info("========3.Create a batch message delivery task========");
            // You can send multiple device messages in batches at a time using the batch message delivery.
            CreateBatchTask createBatchTask = new CreateBatchTask();
            createBatchTask.setAppId(appId);
            // Task name is unique in the resource space. If this interface is called for multiple times, the task name needs to be changed.
            createBatchTask.setTaskName("batchMessages");
            // The task type is fixed to createMessages. Do not change the value.
            createBatchTask.setTaskType("createMessages");
            // Target device ID. Add the target device ID to the list.
            List<String> targetDeviceIds = new ArrayList<>();
            targetDeviceIds.add(deviceId);
            createBatchTask.setTargets(targetDeviceIds);
            // Message to deliver
            createBatchTask.setDocument(messageRequest);
            // Obtain the task ID after a task is created.
            CreateBatchTaskResponse createMessageTask = createTask(iotdaClient, createBatchTask);
            logger.info("========3.Batch message delivery task created========");

            logger.info("========4.Query details about a batch task========");
            String status = createMessageTask.getStatus();
            int times = 0;
            // Wait until the task is complete. Task status description: Initializing, Waitting, Processing, Success, Fail, PartialSuccess, Stopped, Stopping.
            while (("Initializing".equals(status) || "Waitting".equals(status) || "Processing".equals(status)) && times++ < 5) {
                // Query the execution details of a batch task to learn about the message delivery status.
                Task task = queryTask(iotdaClient, createMessageTask.getTaskId());
                status = task.getStatus();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    logger.warn("Exception information: {}", e.getMessage());
                }
            }
            logger.info("========4.Batch task details queried========");

            logger.info("========5.Query a message delivery list========");
            // Query a device message delivery list
            queryMessageList(iotdaClient, deviceId);
            logger.info("========5.Message delivery list queried========");
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.warn("Demonstration failed. Exception information is {}", e.getMessage());
        } catch (ServiceResponseException e) {
            logger.warn("Demonstration failed. Exception information is {}, {}", e.getErrorCode(), e.getErrorMsg());
        }
    }
}
