package com.huawei.iotda;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.JsonUtils;
import com.huaweicloud.sdk.iotda.v5.IoTDAClient;
import com.huaweicloud.sdk.iotda.v5.model.AddDeviceGroupDTO;
import com.huaweicloud.sdk.iotda.v5.model.AddDeviceGroupRequest;
import com.huaweicloud.sdk.iotda.v5.model.AddDeviceGroupResponse;
import com.huaweicloud.sdk.iotda.v5.model.CreateOrDeleteDeviceInGroupRequest;
import com.huaweicloud.sdk.iotda.v5.model.CreateOrDeleteDeviceInGroupResponse;
import com.huaweicloud.sdk.iotda.v5.model.DeleteDeviceGroupRequest;
import com.huaweicloud.sdk.iotda.v5.model.DeleteDeviceGroupResponse;
import com.huaweicloud.sdk.iotda.v5.model.ListDeviceGroupsRequest;
import com.huaweicloud.sdk.iotda.v5.model.ListDeviceGroupsResponse;
import com.huaweicloud.sdk.iotda.v5.model.ShowDeviceGroupRequest;
import com.huaweicloud.sdk.iotda.v5.model.ShowDeviceGroupResponse;
import com.huaweicloud.sdk.iotda.v5.model.ShowDevicesInGroupRequest;
import com.huaweicloud.sdk.iotda.v5.model.ShowDevicesInGroupResponse;
import com.huaweicloud.sdk.iotda.v5.model.UpdateDeviceGroupDTO;
import com.huaweicloud.sdk.iotda.v5.model.UpdateDeviceGroupRequest;
import com.huaweicloud.sdk.iotda.v5.model.UpdateDeviceGroupResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeviceGroupManagement {
    private static final Logger logger = LoggerFactory.getLogger(DeviceGroupManagement.class);
    private static final ObjectMapper OBJECTMAPPER = new ObjectMapper();

    // REGION_ID：If your region is 上海一，please fill in "cn-east-3"；If your region is 北京四，please fill in "cn-north-4"; If your region is 华南广州，please fill in "cn-south-4"
    private static final String REGION_ID = "<YOUR REGION ID>";

    // Obtain the project ID of your region on the My Credentials > API Credentials page.
    private static final String PROJECT_ID = "<YOUR PROJECTID>";

    // ENDPOINT：On the console, choose **Overview** in the navigation pane and click **Access Addresses** to view the HTTPS application access address.
    private static final String ENDPOINT = "<YOUR ENDPOINT>";

    // ak/sk：For details, see the method of obtaining AK/SK in iotda document https://support.huaweicloud.com/api-iothub/iot_06_v5_0091.html.
    // There will be security risks if the AK/SK used for authentication is directly written into code. We suggest encrypting the AK/SK in the configuration file or environment variables for storage.
    // In this sample, the AK/SK is stored in environment variables for identity authentication. Before running this sample, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    // INSTANCE_ID
    private static final String INSTANCE_ID = "<YOUR INSTANCEID>";

    private static IoTDAClient initClient() {
        // Create a credential
        ICredential auth = new BasicCredentials()
            .withAk(AK)
            .withSk(SK)
            // Derivative algorithms are used for the standard and enterprise editions. For the basic edition, delete the configuration "withDerivedPredicate".
            .withDerivedPredicate(BasicCredentials.DEFAULT_DERIVED_PREDICATE)
            .withProjectId(PROJECT_ID);

        // Create and initialize an IoTDAClient instance
        return IoTDAClient.newBuilder()
            .withCredential(auth)
            // Use IoTDARegion for basic editions like "withRegion(IoTDARegion.CN_NORTH_4)". Create regions for standard/enterprise editions.
            .withRegion(new Region(REGION_ID, ENDPOINT))
            // .withRegion(IoTDARegion.CN_NORTH_4)
            .build();
    }

    /**
     * Create a device group
     *
     * @param iotdaClient iotda client
     * @param body        Structure for device group creation
     * @return Device group ID
     */
    private static String createDeviceGroup(IoTDAClient iotdaClient, AddDeviceGroupDTO body) throws ServiceResponseException {
        AddDeviceGroupRequest request = new AddDeviceGroupRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        AddDeviceGroupResponse response = iotdaClient.addDeviceGroup(request);
        logger.info("The device group creation result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        return response.getGroupId();
    }

    /**
     * Query the Device Group List
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space ID
     */
    private static void queryDeviceGroupList(IoTDAClient iotdaClient, String appId) throws ServiceResponseException {
        ListDeviceGroupsRequest request = new ListDeviceGroupsRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        ListDeviceGroupsResponse response = iotdaClient.listDeviceGroups(request);
        logger.info("The device list query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    /**
     * Query the Details About a Device Group
     *
     * @param iotdaClient iotda client
     * @param groupId     Device group ID
     */
    private static void queryDeviceGroupDetail(IoTDAClient iotdaClient, String groupId) throws ServiceResponseException {
        ShowDeviceGroupRequest request = new ShowDeviceGroupRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setGroupId(groupId);
        ShowDeviceGroupResponse productResponse = iotdaClient.showDeviceGroup(request);
        logger.info("The device group details query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, productResponse));
    }

    /**
     * Update a Device Group
     *
     * @param iotdaClient iotda client
     * @param groupId     Device group ID
     * @param body        Structure for device group update
     */
    private static void updateDeviceGroup(IoTDAClient iotdaClient, String groupId, UpdateDeviceGroupDTO body) {
        UpdateDeviceGroupRequest request = new UpdateDeviceGroupRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        request.setGroupId(groupId);
        UpdateDeviceGroupResponse updateDeviceGroup = iotdaClient.updateDeviceGroup(request);
        logger.info("The device group update result is:{}", JsonUtils.toJSON(OBJECTMAPPER, updateDeviceGroup));
    }

    /**
     * Manage Devices of a Device Group，including add a device to and remove a device from a device group
     *
     * @param iotdaClient iotda client
     * @param groupId     Device group ID
     * @param actionId    actionId，// addDevice: add a device to a device group/removeDevice：remove a device from a device group
     * @param deviceId    ID of the manipulated device
     */
    private static void manageDeviceInGroup(IoTDAClient iotdaClient, String groupId, String actionId, String deviceId) throws ServiceResponseException {
        CreateOrDeleteDeviceInGroupRequest request = new CreateOrDeleteDeviceInGroupRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setGroupId(groupId);
        request.setActionId(actionId);
        request.setDeviceId(deviceId);
        CreateOrDeleteDeviceInGroupResponse response = iotdaClient.createOrDeleteDeviceInGroup(request);
        logger.info("The device management result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    /**
     * Query Devices in a Device Group
     *
     * @param iotdaClient iotda client
     * @param groupId     Device group ID
     */
    private static void queryDeviceInGroup(IoTDAClient iotdaClient, String groupId) throws ServiceResponseException {
        ShowDevicesInGroupRequest request = new ShowDevicesInGroupRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setGroupId(groupId);
        ShowDevicesInGroupResponse response = iotdaClient.showDevicesInGroup(request);
        logger.info("The result of querying a device in a device group is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    /**
     * Delete a Device Group
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space ID
     * @param groupId     Device group ID
     */
    private static void deleteDeviceGroup(IoTDAClient iotdaClient, String appId, String groupId) throws ServiceResponseException {
        DeleteDeviceGroupRequest request = new DeleteDeviceGroupRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setGroupId(groupId);
        DeleteDeviceGroupResponse response = iotdaClient.deleteDeviceGroup(request);
        logger.info("The device group deletion result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    public static void main(String[] args) {
        // Initialize the IoTDA client
        IoTDAClient iotdaClient = initClient();
        // ID of the resource space to which the device belongs. Replace the value with the required one.
        String appId = "9d988b5efdf646f0828724c45e1d794e";
        // ID of the device. Replace the value with the required one.
        String deviceId = "";

        try {
            logger.info("========1.Query the Device Group List========");
            queryDeviceGroupList(iotdaClient, appId);
            logger.info("========1.Query The Device Group List Completed========");

            logger.info("========2.Create A Device Group========");
            // Assemble the device group data. You can assemble the data based on the actual situation of the device group.
            AddDeviceGroupDTO addDeviceGroup = new AddDeviceGroupDTO();
            addDeviceGroup.setAppId(appId);
            addDeviceGroup.setName("group-1");
            addDeviceGroup.setGroupType("STATIC");
            addDeviceGroup.setDescription("demo");

            String groupId = createDeviceGroup(iotdaClient, addDeviceGroup);
            logger.info("========2.Create A Device Group Completed========");

            logger.info("========3.Query the Device Group List========");
            queryDeviceGroupList(iotdaClient, appId);
            logger.info("========3.Query The Device Group List Completed========");

            logger.info("========4.Query The Details About A Device Group========");
            queryDeviceGroupDetail(iotdaClient, groupId);
            logger.info("========4.Query The Details About A Device Group Completed========");

            logger.info("========5.Update A Device Group========");
            // update the name based on the site requirements. The following describes how to change the device group name.
            UpdateDeviceGroupDTO updateDeviceGroup = new UpdateDeviceGroupDTO();
            updateDeviceGroup.setName("group-2");
            updateDeviceGroup(iotdaClient, groupId, updateDeviceGroup);
            logger.info("========5.Update A Device Group Completed========");

            logger.info("========6.Query Devices In A Device Group========");
            queryDeviceInGroup(iotdaClient, groupId);
            logger.info("========6.Query Devices In A Device Group Completed========");

            logger.info("========7.Adding Devices to a Device Group========");
            // actionId: action ID. The value is fixed to addDevice when a device is added to a device group.
            manageDeviceInGroup(iotdaClient, groupId, "addDevice", deviceId);
            logger.info("========7.Adding a device to a device group is complete========");

            logger.info("========8.Query Devices In A Device Group========");
            queryDeviceInGroup(iotdaClient, groupId);
            logger.info("========8.Query The Details About A Device Group Completed========");

            logger.info("========9.Removing a Device from a Device Group========");
            // actionId: ID of the action for deleting a device to a device group. The value is fixed to removeDevice.
            manageDeviceInGroup(iotdaClient, groupId, "removeDevice", deviceId);
            logger.info("========9.Removing the device from the device group is complete========");

            logger.info("========10.Querying Devices in a Device Group========");
            queryDeviceInGroup(iotdaClient, groupId);
            logger.info("========10.Querying devices in the device group is complete========");

            logger.info("========11.Deleting a Device Group========");
            deleteDeviceGroup(iotdaClient, appId, groupId);
            logger.info("========11.Device group deleted========");

            logger.info("========12.Querying the Device Group List========");
            queryDeviceGroupList(iotdaClient, appId);
            logger.info("========12.Querying the device group list is complete========");
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.warn("Demonstration failed. Exception information is {}", e.getMessage());
        } catch (ServiceResponseException e) {
            logger.warn("Demonstration failed. Exception information is {}, {}", e.getErrorCode(), e.getErrorMsg());
        }
    }
}