## 1. Functions

You can integrate the IoTDA server SDKs provided by Huawei Cloud to call IoTDA APIs and use IoTDA smoothly.
This example shows and demonstrates how to use the Java SDK to add, modify, delete and query a device group as well as manage the devices of a device group. 

Device Group (Group): A group is a collection of devices. You can create groups for all the devices in a resource space based on different rules, such as regions and types, and you can operate the devices by group. For example, you can perform a firmware upgrade on a group of water meters in the resource space. 
For details, see [Group](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0020.html)

**What You Will Learn**

The ways to use the Java SDK to add, modify, delete and query a device group. 

## 2. Prerequisites
- 1. You have created a Huawei Cloud account and obtained the access key (AK) and secret access key (SK) of your account. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 2. You have set up the development environment with Java JDK 1.8 or later.
- 3. You have obtained the project ID of the target region. View the project ID on the **My Credentials** > **API Credentials** page of the Huawei Cloud console. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_1001.html).
- 4. You have enabled IoTDA. For details about how to obtain the ID of the current instance, see [Viewing Instance Details](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0121.html).
- 5. You have created a device and connected it to the platform for viewing interconnection details.

## 3. Obtaining and Installing the SDK
Download and install Maven, and add dependencies to the pom.xml file of the Java project.
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4. API Parameters
For details about API parameters, see:

[Create a Device Group](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0051.html)

[Query the Device Group List](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0053.html)

[Query a Device Group](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0075.html)

[Modify a Device Group](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0073.html)

[Delete a Device Group](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0071.html)

[Manage Devices in a Device Group](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0070.html)

[Query Devices in a Device Group](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0074.html)

## 5. Key Code Snippets
### 5.1 Create a Device Group

```java
    /**
     * Create a device group
     *
     * @param iotdaClient iotda client
     * @param body        Structure for device group creation
     * @return Device group ID
     */
    private static String createDeviceGroup(IoTDAClient iotdaClient, AddDeviceGroupDTO body) throws ServiceResponseException {
        AddDeviceGroupRequest request = new AddDeviceGroupRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        AddDeviceGroupResponse response = iotdaClient.addDeviceGroup(request);
        logger.info("The device group creation result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        return response.getGroupId();
    }
```

### 5.2 Query the Device Group List

```java
    /**
     * Query the Device Group List
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space ID
     */
    private static void queryDeviceGroupList(IoTDAClient iotdaClient, String appId) throws ServiceResponseException {
        ListDeviceGroupsRequest request = new ListDeviceGroupsRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        ListDeviceGroupsResponse response = iotdaClient.listDeviceGroups(request);
        logger.info("The device list query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.3 Query the Details About a Device Group

```java
    /**
     * Query the Details About a Device Group
     *
     * @param iotdaClient iotda client
     * @param groupId     Device group ID
     */
    private static void queryDeviceGroupDetail(IoTDAClient iotdaClient, String groupId) throws ServiceResponseException {
        ShowDeviceGroupRequest request = new ShowDeviceGroupRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setGroupId(groupId);
        ShowDeviceGroupResponse productResponse = iotdaClient.showDeviceGroup(request);
        logger.info("The device group details query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, productResponse));
}
```

### 5.4 Update a Device Group
```java
    /**
     * Update a Device Group
     *
     * @param iotdaClient iotda client
     * @param groupId     Device group ID
     * @param body        Structure for device group update
     */
    private static void updateDeviceGroup(IoTDAClient iotdaClient, String groupId, UpdateDeviceGroupDTO body) {
        UpdateDeviceGroupRequest request = new UpdateDeviceGroupRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        request.setGroupId(groupId);
        UpdateDeviceGroupResponse updateDeviceGroup = iotdaClient.updateDeviceGroup(request);
        logger.info("The device group update result is:{}", JsonUtils.toJSON(OBJECTMAPPER, updateDeviceGroup));
    }
```

### 5.5 Manage Devices of a Device Group
```java
    /**
     * Manage Devices of a Device Group，including add a device to and remove a device from a device group
     *
     * @param iotdaClient iotda client
     * @param groupId     Device group ID
     * @param actionId    actionId，// addDevice: add a device to a device group/removeDevice：remove a device from a device group
     * @param deviceId    ID of the manipulated device
     */
    private static void manageDeviceInGroup(IoTDAClient iotdaClient, String groupId, String actionId, String deviceId) throws ServiceResponseException {
        CreateOrDeleteDeviceInGroupRequest request = new CreateOrDeleteDeviceInGroupRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setGroupId(groupId);
        request.setActionId(actionId);
        request.setDeviceId(deviceId);
        CreateOrDeleteDeviceInGroupResponse response = iotdaClient.createOrDeleteDeviceInGroup(request);
        logger.info("The device management result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.6 Query a Device in a Device Group
```java
    /**
     * Query a Device in a Device Group
     *
     * @param iotdaClient iotda client
     * @param groupId     Device group ID
     */
    private static void queryDeviceInGroup(IoTDAClient iotdaClient, String groupId) throws ServiceResponseException {
        ShowDevicesInGroupRequest request = new ShowDevicesInGroupRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setGroupId(groupId);
        ShowDevicesInGroupResponse response = iotdaClient.showDevicesInGroup(request);
        logger.info("The result of querying a device in a device group is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.7 Delete a Device Group
```java
    /**
     * Delete a Device Group
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space ID
     * @param groupId     Device group ID
     */
    private static void deleteDeviceGroup(IoTDAClient iotdaClient, String appId, String groupId) throws ServiceResponseException {
        DeleteDeviceGroupRequest request = new DeleteDeviceGroupRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setGroupId(groupId);
        DeleteDeviceGroupResponse response = iotdaClient.deleteDeviceGroup(request);
        logger.info("The device group deletion result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

## 6. Execution Results
Replace the parameters and execute the code in the sequence of the main method. The program demonstrates how to add, modify, query and delete a device group, as well as manage and query a device in a device group.
The execution results of each step of the main method are shown as follows.

### 6.1 Query the Device Group List(Before the Device List Creation)

Because the device group is not created yet, the query result is empty.
```json
{
  "device_groups": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```
### 6.2 Create a Device Group
```json
{
  "group_id": "437b3149-6c72-4323-8052-843d5f7a36a8",
  "name": "DeviceGroup-1",
  "description": "This is a sample device group demonstration",
  "group_type": "STATIC"
}
```

### 6.3 Query the Device Group List(After the Device group Creation)
```json
{
  "device_groups": [
    {
      "group_id": "437b3149-6c72-4323-8052-843d5f7a36a8",
      "name": "DeviceGroup-1",
      "description": "This is a sample device group demonstration",
      "group_type": "STATIC"
    }
  ],
  "page": {
    "count": 1,
    "marker": "64ed5af60c635a0ce62f1283"
  }
}
```
### 6.4 Query the Details About a Device Group
```json
{
  "group_id": "437b3149-6c72-4323-8052-843d5f7a36a8",
  "name": "DeviceGroup-1",
  "description": "This is a sample device group demonstration",
  "group_type": "STATIC"
}
```
### 6.5 Update a Device Group
```json
{
  "group_id": "437b3149-6c72-4323-8052-843d5f7a36a8",
  "name": "DeviceGroup-2",
  "description": "This is a sample device group demonstration",
  "group_type": "STATIC"
}
```

### 6.6 Query a Device in a Device Group(Before Adding a Device)
```json
{"devices":[],"page":{"count":0,"marker":"ffffffffffffffffffffffff"}}
```
### 6.7 Add a Device to a Device Group
```json
{}
```

### 6.8 Query a Device in a Device Group(After Adding a Device)
```json
{
  "devices": [
    {
      "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
      "node_id": "7758dsfdsfsd",
      "product_id": "64111bcd06ca5933b28a058b"
    }
  ],
  "page": {
    "count": 1,
    "marker": "64f5caf50c635a0ce62f1f55"
  }
}
```

### 6.9 Remove a Device from a Device Group
```json
{}
```

### 6.10 Query a Device in a Device Group(After the Device Removal)
```json
{
  "devices": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```

### 6.11 Delete a Device Group
```json
{}
```

### 6.12 Query the Device List(After the Device Group Deletion)
```json
{
  "device_groups": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```