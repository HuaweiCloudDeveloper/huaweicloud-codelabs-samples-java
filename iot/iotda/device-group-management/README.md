## 1、功能介绍

华为云提供了IoTDA服务端SDK，您可以直接集成服务端SDK来调用IoTDA的相关API，从而实现对IoTDA的快速操作。
该示例展示了如何通过java版SDK来实现对设备组的新增，修改，查询，删除，以及管理设备组中设备，查询设备组中设备，同时通过demo进行简单的演示操作。

设备组（群组）：一组具有相同的某个特性的的设备集合，你可以使用设备组进行分类，对设备组中设备进行一些批量操作，
如对某个设备组的设备进行ota升级等。详情请参考[群组](https://support.huaweicloud.com/usermanual-iothub/iot_01_0020.html)

**您将学到什么？**

如何通过java版SDK来实现对设备组的新增，修改，删除，查询，列表查询功能。

## 2、前置条件
- 1、已经开通华为云账号，并获得到您账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。您可以参考[获取项目ID](https://support.huaweicloud.com/api-iothub/iot_06_v5_1001.html)
- 4、已经开通IoTDA服务，获取当前实例的实例ID，请参考[查看实例详情](https://support.huaweicloud.com/usermanual-iothub/iot_01_0121.html) 。
- 5、已注册设备并获取设备ID。

## 3、SDK获取和安装
您需要下载并安装 Maven，安装完成后您只需在 Java 项目的 **pom.xml** 文件加入相应的依赖项即可。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4、接口参数说明
关于接口参数的详细说明可参见：

[添加设备组](https://support.huaweicloud.com/api-iothub/iot_06_v5_0051.html)

[查询设备组列表](https://support.huaweicloud.com/api-iothub/iot_06_v5_0053.html)

[查询设备组](https://support.huaweicloud.com/api-iothub/iot_06_v5_0075.html)

[修改设备组](https://support.huaweicloud.com/api-iothub/iot_06_v5_0073.html)

[删除设备组](https://support.huaweicloud.com/api-iothub/iot_06_v5_0071.html)

[管理设备组中的设备](https://support.huaweicloud.com/api-iothub/iot_06_v5_0070.html)

[查询设备组设备列表](https://support.huaweicloud.com/api-iothub/iot_06_v5_0074.html)

## 5、关键代码片段
### 5.1、创建设备组

```java
    /**
     * 创建设备组
     *
     * @param iotdaClient iotda client
     * @param body        创建设备组结构体
     * @return 设备组ID
     */
    private static String createDeviceGroup(IoTDAClient iotdaClient, AddDeviceGroupDTO body) throws ServiceResponseException {
        AddDeviceGroupRequest request = new AddDeviceGroupRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        AddDeviceGroupResponse response = iotdaClient.addDeviceGroup(request);
        logger.info("创建设备组的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        return response.getGroupId();
    }
```

### 5.2、查询设备组列表

```java
    /**
     * 查询设备组列表
     *
     * @param iotdaClient iotda client
     * @param appId       资源空间ID
     */
    private static void queryDeviceGroupList(IoTDAClient iotdaClient, String appId) throws ServiceResponseException {
        ListDeviceGroupsRequest request = new ListDeviceGroupsRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        ListDeviceGroupsResponse response = iotdaClient.listDeviceGroups(request);
        logger.info("查询设备组列表的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.3、查询设备组详情

```java
    /**
     * 查询设备组详情
     *
     * @param iotdaClient iotda client
     * @param groupId     设备组ID
     */
    private static void queryDeviceGroupDetail(IoTDAClient iotdaClient, String groupId) throws ServiceResponseException {
        ShowDeviceGroupRequest request = new ShowDeviceGroupRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setGroupId(groupId);
        ShowDeviceGroupResponse productResponse = iotdaClient.showDeviceGroup(request);
        logger.info("查询设备组详情的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, productResponse));
}
```

### 5.4、更新设备组信息
```java
    /**
     * 更新设备组信息
     *
     * @param iotdaClient iotda client
     * @param groupId     设备组ID
     * @param body        更新设备组结构体
     */
    private static void updateDeviceGroup(IoTDAClient iotdaClient, String groupId, UpdateDeviceGroupDTO body) {
        UpdateDeviceGroupRequest request = new UpdateDeviceGroupRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        request.setGroupId(groupId);
        UpdateDeviceGroupResponse updateDeviceGroup = iotdaClient.updateDeviceGroup(request);
        logger.info("更新设备组的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, updateDeviceGroup));
    }
```

### 5.5、管理设备组中设备
```java
    /**
     * 管理设备组中设备，包括往设备组中添加设备和从设备组中移除设备
     *
     * @param iotdaClient iotda client
     * @param groupId     设备组ID
     * @param actionId    actionId，// addDevice:往设备组中新增设备/removeDevice：从设备组中移除设备
     * @param deviceId    要操作的设备ID
     */
    private static void manageDeviceInGroup(IoTDAClient iotdaClient, String groupId, String actionId, String deviceId) throws ServiceResponseException {
        CreateOrDeleteDeviceInGroupRequest request = new CreateOrDeleteDeviceInGroupRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setGroupId(groupId);
        request.setActionId(actionId);
        request.setDeviceId(deviceId);
        CreateOrDeleteDeviceInGroupResponse response = iotdaClient.createOrDeleteDeviceInGroup(request);
        logger.info("管理设备组中设备的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.6、查询设备组中设备
```java
    /**
     * 查询设备组中设备
     *
     * @param iotdaClient iotda client
     * @param groupId     设备组ID
     */
    private static void queryDeviceInGroup(IoTDAClient iotdaClient, String groupId) throws ServiceResponseException {
        ShowDevicesInGroupRequest request = new ShowDevicesInGroupRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setGroupId(groupId);
        ShowDevicesInGroupResponse response = iotdaClient.showDevicesInGroup(request);
        logger.info("查询设备组中的设备的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.7、删除设备组
```java
    /**
     * 删除设备组
     *
     * @param iotdaClient iotda client
     * @param appId       资源空间ID
     * @param groupId     设备组ID
     */
    private static void deleteDeviceGroup(IoTDAClient iotdaClient, String appId, String groupId) throws ServiceResponseException {
        DeleteDeviceGroupRequest request = new DeleteDeviceGroupRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setGroupId(groupId);
        DeleteDeviceGroupResponse response = iotdaClient.deleteDeviceGroup(request);
        logger.info("删除设备组的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

## 6、运行结果
如果您替换main方法的参数，并按照main方法的顺序执行，程序将演示创建，修改，查询，删除设备组，以及管理设备组中设备，查询设备组设备。
以下为main方法中每一个步骤的运行结果。

### 6.1 查询设备组列表(创建设备组前)

由于还未创建设备组，查出来为空
```json
{
  "device_groups": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```
### 6.2 创建设备组
```json
{
  "group_id": "437b3149-6c72-4323-8052-843d5f7a36a8",
  "name": "设备组-1",
  "description": "这是一个设备组演示样例",
  "group_type": "STATIC"
}
```

### 6.3 查询设备组列表(创建设备组后)
```json
{
  "device_groups": [
    {
      "group_id": "437b3149-6c72-4323-8052-843d5f7a36a8",
      "name": "设备组-1",
      "description": "这是一个设备组演示样例",
      "group_type": "STATIC"
    }
  ],
  "page": {
    "count": 1,
    "marker": "64ed5af60c635a0ce62f1283"
  }
}
```
### 6.4 查询设备组详情
```json
{
  "group_id": "437b3149-6c72-4323-8052-843d5f7a36a8",
  "name": "设备组-1",
  "description": "这是一个设备组演示样例",
  "group_type": "STATIC"
}
```
### 6.5 更新设备组
```json
{
  "group_id": "437b3149-6c72-4323-8052-843d5f7a36a8",
  "name": "设备组-2",
  "description": "这是一个设备组演示样例",
  "group_type": "STATIC"
}
```

### 6.6 查询设备组设备(添加设备前)
```json
{"devices":[],"page":{"count":0,"marker":"ffffffffffffffffffffffff"}}
```
### 6.7 添加设备至设备组
```json
{}
```

### 6.8 查询设备组中设备(添加设备后)
```json
{
  "devices": [
    {
      "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
      "node_id": "7758dsfdsfsd",
      "product_id": "64111bcd06ca5933b28a058b"
    }
  ],
  "page": {
    "count": 1,
    "marker": "64f5caf50c635a0ce62f1f55"
  }
}
```

### 6.9 将设备从设备组中移除
```json
{}
```

### 6.10 查询设备组中设备(将设备从设备组中移除后)
```json
{
  "devices": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```

### 6.11 删除设备组
```json
{}
```

### 6.12 查询设备组列表(删除设备组后)
```json
{
  "device_groups": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```