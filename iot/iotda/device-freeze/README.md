## 1、功能介绍

华为云提供了IoTDA服务端SDK，您可以直接集成服务端SDK来调用IoTDA的相关API，从而实现对IoTDA的快速操作。
该示例展示了如何通过java版SDK来实现设备冻结和解冻操作。

设备冻结：即为冻结设备，被冻结设备将不允许建链连接至平台，已经连接到平台设备设备将会断开连接。

设备解冻：解除冻结状态，允许设备连接至平台。

**您将学到什么？**

如何通过java版SDK来管理设备的链接状态，您将学习到如何使用SDK去冻结和解冻设备。

## 2、前置条件
- 1、已经开通华为云账号，并获得到您账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。您可以参考[获取项目ID](https://support.huaweicloud.com/api-iothub/iot_06_v5_1001.html)
- 4、已经开通IoTDA服务，获取当前实例的实例ID，请参考[查看实例详情](https://support.huaweicloud.com/usermanual-iothub/iot_01_0121.html) 。
- 5、并且存在一个设备，建议设备已经连接到平台，你可以具体看到平台与设备交互。

## 3、SDK获取和安装
您需要下载并安装 Maven，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4、接口参数说明
关于接口参数的详细说明可参见：

[冻结设备](https://support.huaweicloud.com/api-iothub/iot_06_v5_0094.html)

[解冻设备](https://support.huaweicloud.com/api-iothub/iot_06_v5_0095.html)

[查询设备](https://support.huaweicloud.com/api-iothub/iot_06_v5_0055.html)

## 5、关键代码片段
### 5.1、冻结设备

```java
    /**
     * 冻结设备
     *
     * @param iotdaClient iotda client
     * @param deviceId    设备ID
     */
    private static void freezeDevice(IoTDAClient iotdaClient, String deviceId) throws ServiceResponseException {
        FreezeDeviceRequest request = new FreezeDeviceRequest();
        request.setDeviceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        FreezeDeviceResponse freezeDevice = iotdaClient.freezeDevice(request);
        logger.info("冻结设备的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, freezeDevice));
    }
```

### 5.2、查询设备详情

```java
    /**
     * 查询设备详情
     *
     * @param iotdaClient iotda client
     * @param deviceId    设备ID
     */
    private static void queryDeviceDetail(IoTDAClient iotdaClient, String deviceId) throws ServiceResponseException {
        ShowDeviceRequest request = new ShowDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        ShowDeviceResponse deviceResponse = iotdaClient.showDevice(request);
        logger.info("查询设备详情的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, deviceResponse));
    }
```

### 5.3、解冻设备

```java
    /**
     * 解冻设备
     *
     * @param iotdaClient iotda client
     * @param deviceId    设备ID
     */
    private static void unFreezeDevice(IoTDAClient iotdaClient, String deviceId) throws ServiceResponseException {
        UnfreezeDeviceRequest request = new UnfreezeDeviceRequest();
        request.setDeviceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        UnfreezeDeviceResponse unfreezeDevice = iotdaClient.unfreezeDevice(request);
        logger.info("解冻设备的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, unfreezeDevice));
    }
```

## 6、运行结果
如果您替换main方法的参数，并按照main方法的顺序执行，程序将演示将设备冻结之后在进行解冻操作。
以下为main方法中每一个步骤的运行结果。

### 6.1 查询设备详情(冻结前)
```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "testApp",
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "node_id": "7758dsfdsfsd",
  "gateway_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "node_type": "GATEWAY",
  "auth_info": {
    "auth_type": "SECRET",
    "secret": "******",
    "secure_access": true,
    "timeout": 0
  },
  "product_id": "64111bcd06ca5933b28a058b",
  "product_name": "testProduct",
  "status": "ONLINE",
  "create_time": "20230516T034241Z",
  "connection_status_update_time": "2023-09-04T02:13:16.932Z",
  "active_time": "2023-05-23T09:12:59.179Z",
  "tags": [
    {
      "tag_key": "testTagName",
      "tag_value": "testTagValue"
    }
  ]
}
```
### 6.2 冻结设备
```json
{}
```
### 6.3 查询设备(冻结设备后)
```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "testApp",
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "node_id": "7758dsfdsfsd",
  "gateway_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "node_type": "GATEWAY",
  "auth_info": {
    "auth_type": "SECRET",
    "secret": "******",
    "secure_access": true,
    "timeout": 0
  },
  "product_id": "64111bcd06ca5933b28a058b",
  "product_name": "testProduct",
  "status": "FROZEN",
  "create_time": "20230516T034241Z",
  "connection_status_update_time": "2023-09-04T02:13:16.932Z",
  "active_time": "2023-05-23T09:12:59.179Z",
  "tags": [
    {
      "tag_key": "testTagName",
      "tag_value": "testTagValue"
    }
  ]
}
```
### 6.4 解冻设备
```json
{}
```
### 6.5 查询设备详情

冻结设备会导致设备断链，所以设备处于离线状态，如果设备有自动重连，也可能处于在线状态。
```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "testApp",
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "node_id": "7758dsfdsfsd",
  "gateway_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "node_type": "GATEWAY",
  "auth_info": {
    "auth_type": "SECRET",
    "secret": "******",
    "secure_access": true,
    "timeout": 0
  },
  "product_id": "64111bcd06ca5933b28a058b",
  "product_name": "testProduct",
  "status": "OFFLINE",
  "create_time": "20230516T034241Z",
  "connection_status_update_time": "2023-09-04T02:26:08.246Z",
  "active_time": "2023-05-23T09:12:59.179Z",
  "tags": [
    {
      "tag_key": "testTagName",
      "tag_value": "testTagValue"
    }
  ]
}
```