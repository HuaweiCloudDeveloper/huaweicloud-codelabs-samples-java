## 1. Functions

You can integrate the IoTDA server SDKs provided by Huawei Cloud to call IoTDA APIs and use IoTDA smoothly.
This example shows and demonstrates how to use the Java SDK to freeze and unfreeze devices.

Freeze a device: The freezed device is not allowed to connect to the platform. The devices which are already connected to the platform will disconnect.

Unfreeze a device: The unfreezed device is allowed to connect to the platform.

**What You Will Learn**

The ways to use the Java SDK to manage the connection status of devices, freeze and unfreeze devices.

## 2. Prerequisites
- 1. You have created a Huawei Cloud account and obtained the access key (AK) and secret access key (SK) of your account. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 2. You have set up the development environment with Java JDK 1.8 or later.
- 3. You have obtained the project ID of the target region. View the project ID on the **My Credentials** > **API Credentials** page of the Huawei Cloud console. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_1001.html).
- 4. You have enabled IoTDA. For details about how to obtain the ID of the current instance, see [Viewing Instance Details](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0121.html).
- 5. You have created a device and connected it to the platform for viewing interconnection details.

## 3. Obtaining and Installing the SDK
Download and install Maven, and add dependencies to the **pom.xml** file of the Java project.
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4. API Parameters
For details about API parameters, see:

[Freeze a Device](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0094.html)

[Unfreeze a Device](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0095.html)

[Query a Device](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0055.html)

## 5. Key Code Snippets
### 5.1 Freeze a Device

```java
    /**
     * Freeze a Device
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device ID
     */
    private static void freezeDevice(IoTDAClient iotdaClient, String deviceId) throws ServiceResponseException {
        FreezeDeviceRequest request = new FreezeDeviceRequest();
        request.setDeviceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        FreezeDeviceResponse freezeDevice = iotdaClient.freezeDevice(request);
        logger.info("The result of freezing a device is:{}", JsonUtils.toJSON(OBJECTMAPPER, freezeDevice));
    }
```

### 5.2 Query the Details of a Device

```java
    /**
     * Query the Details of a Device
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device ID
     */
    private static void queryDeviceDetail(IoTDAClient iotdaClient, String deviceId) throws ServiceResponseException {
        ShowDeviceRequest request = new ShowDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        ShowDeviceResponse deviceResponse = iotdaClient.showDevice(request);
        logger.info("The result of querying the details of a device is:{}", JsonUtils.toJSON(OBJECTMAPPER, deviceResponse));
    }
```

### 5.3 Unfreeze a Device

```java
    /**
     * Unfreeze a Device
     *
     * @param iotdaClient iotda client
     * @param deviceId    Devcie ID
     */
    private static void unFreezeDevice(IoTDAClient iotdaClient, String deviceId) throws ServiceResponseException {
        UnfreezeDeviceRequest request = new UnfreezeDeviceRequest();
        request.setDeviceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        UnfreezeDeviceResponse unfreezeDevice = iotdaClient.unfreezeDevice(request);
        logger.info("The result of unfreezing a device is:{}", JsonUtils.toJSON(OBJECTMAPPER, unfreezeDevice));
    }
```

## 6. Execution Results
Replace the parameters and execute the code in the sequence of the main method. The program demonstrates how to unfreeze a freezed device.
The execution results of each step of the main method are shown as follows.

### 6.1 Query the Details of a Device(Before Freezing)
```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "testApp",
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "node_id": "7758dsfdsfsd",
  "gateway_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "node_type": "GATEWAY",
  "auth_info": {
    "auth_type": "SECRET",
    "secret": "******",
    "secure_access": true,
    "timeout": 0
  },
  "product_id": "64111bcd06ca5933b28a058b",
  "product_name": "testProduct",
  "status": "ONLINE",
  "create_time": "20230516T034241Z",
  "connection_status_update_time": "2023-09-04T02:13:16.932Z",
  "active_time": "2023-05-23T09:12:59.179Z",
  "tags": [
    {
      "tag_key": "testTagName",
      "tag_value": "testTagValue"
    }
  ]
}
```
### 6.2 Freeze a Device
```json
{}
```
### 6.3 Query a Device(After Freezing)
```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "testApp",
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "node_id": "7758dsfdsfsd",
  "gateway_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "node_type": "GATEWAY",
  "auth_info": {
    "auth_type": "SECRET",
    "secret": "******",
    "secure_access": true,
    "timeout": 0
  },
  "product_id": "64111bcd06ca5933b28a058b",
  "product_name": "testProduct",
  "status": "FROZEN",
  "create_time": "20230516T034241Z",
  "connection_status_update_time": "2023-09-04T02:13:16.932Z",
  "active_time": "2023-05-23T09:12:59.179Z",
  "tags": [
    {
      "tag_key": "testTagName",
      "tag_value": "testTagValue"
    }
  ]
}
```
### 6.4 Unfreeze a Device
```json
{}
```
### 6.5 Query the Details of a Device

Freezing a device will disconnect the device. After a device is unfreezed, the status of the device may be online or offline because the device will try to auto-re-connect.

```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "testApp",
  "device_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "node_id": "7758dsfdsfsd",
  "gateway_id": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
  "node_type": "GATEWAY",
  "auth_info": {
    "auth_type": "SECRET",
    "secret": "******",
    "secure_access": true,
    "timeout": 0
  },
  "product_id": "64111bcd06ca5933b28a058b",
  "product_name": "testProduct",
  "status": "OFFLINE",
  "create_time": "20230516T034241Z",
  "connection_status_update_time": "2023-09-04T02:26:08.246Z",
  "active_time": "2023-05-23T09:12:59.179Z",
  "tags": [
    {
      "tag_key": "testTagName",
      "tag_value": "testTagValue"
    }
  ]
}
```