package com.huawei.iotda;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.JsonUtils;
import com.huaweicloud.sdk.iotda.v5.IoTDAClient;
import com.huaweicloud.sdk.iotda.v5.model.FreezeDeviceRequest;
import com.huaweicloud.sdk.iotda.v5.model.FreezeDeviceResponse;
import com.huaweicloud.sdk.iotda.v5.model.ShowDeviceRequest;
import com.huaweicloud.sdk.iotda.v5.model.ShowDeviceResponse;
import com.huaweicloud.sdk.iotda.v5.model.UnfreezeDeviceRequest;
import com.huaweicloud.sdk.iotda.v5.model.UnfreezeDeviceResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeviceFreeze {
    private static final Logger logger = LoggerFactory.getLogger(DeviceFreeze.class);
    private static final ObjectMapper OBJECTMAPPER = new ObjectMapper();

    // REGION_ID：If your region is 上海一，please fill in "cn-east-3"；If your region is 北京四，please fill in "cn-north-4"; If your region is 华南广州，please fill in "cn-south-4"
    private static final String REGION_ID = "<YOUR REGION ID>";

    // ENDPOINT：On the console, choose **Overview** in the navigation pane and click **Access Addresses** to view the HTTPS application access address.
    private static final String ENDPOINT = "<YOUR ENDPOINT>";

    // Obtain the project ID of the corresponding region. You can view the project ID on the My Credentials > API Credentials page.
    private static final String PROJECT_ID = "<YOUR PROJECTID>";

    // ak/sk：For details, see the method of obtaining AK/SK in iotda document https://support.huaweicloud.com/api-iothub/iot_06_v5_0091.html.
    // There will be security risks if the AK/SK used for authentication is directly written into code. We suggest encrypting the AK/SK in the configuration file or environment variables for storage.
    // In this sample, the AK/SK is stored in environment variables for identity authentication. Before running this sample, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    // INSTANCE_ID
    private static final String INSTANCE_ID = "<YOUR INSTANCEID>";

    private static IoTDAClient initClient() {
        // Create a credential
        ICredential auth = new BasicCredentials()
            .withAk(AK)
            .withSk(SK)
            // Derivative algorithms are used for the standard and enterprise editions. For the basic edition, delete the configuration "withDerivedPredicate".
            .withDerivedPredicate(BasicCredentials.DEFAULT_DERIVED_PREDICATE)
            .withProjectId(PROJECT_ID);

        // Create and initialize an IoTDAClient instance
        return IoTDAClient.newBuilder()
            .withCredential(auth)
            // Use IoTDARegion for basic editions like "withRegion(IoTDARegion.CN_NORTH_4)". Create regions for standard/enterprise editions.
            .withRegion(new Region(REGION_ID, ENDPOINT))
            // .withRegion(IoTDARegion.CN_NORTH_4)
            .build();
    }

    /**
     * Freeze a Device
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device ID
     */
    private static void freezeDevice(IoTDAClient iotdaClient, String deviceId) throws ServiceResponseException {
        FreezeDeviceRequest request = new FreezeDeviceRequest();
        request.setDeviceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        FreezeDeviceResponse freezeDevice = iotdaClient.freezeDevice(request);
        logger.info("The result of freezing a device is:{}", JsonUtils.toJSON(OBJECTMAPPER, freezeDevice));
    }

    /**
     * Query the Details of a Device
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device ID
     */
    private static void queryDeviceDetail(IoTDAClient iotdaClient, String deviceId) throws ServiceResponseException {
        ShowDeviceRequest request = new ShowDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        ShowDeviceResponse deviceResponse = iotdaClient.showDevice(request);
        logger.info("The result of querying the details of a device is:{}", JsonUtils.toJSON(OBJECTMAPPER, deviceResponse));
    }

    /**
     * Unfreeze a Device
     *
     * @param iotdaClient iotda client
     * @param deviceId    Devcie ID
     */
    private static void unFreezeDevice(IoTDAClient iotdaClient, String deviceId) throws ServiceResponseException {
        UnfreezeDeviceRequest request = new UnfreezeDeviceRequest();
        request.setDeviceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        UnfreezeDeviceResponse unfreezeDevice = iotdaClient.unfreezeDevice(request);
        logger.info("The result of unfreezing a device is:{}", JsonUtils.toJSON(OBJECTMAPPER, unfreezeDevice));
    }

    public static void main(String[] args) {
        // Initialize the IoTDA client
        IoTDAClient iotdaClient = initClient();

        // ID of the device to be operated.
        String deviceId = "";

        try {
            logger.info("========1.Query the Details of a Device========");
            queryDeviceDetail(iotdaClient, deviceId);
            logger.info("========1.Query the Details of a Device Completed========");

            logger.info("========2.Freeze Device========");
            freezeDevice(iotdaClient, deviceId);
            logger.info("========2.Freeze Device Completed========");

            logger.info("========3.Query the Details of a Device========");
            queryDeviceDetail(iotdaClient, deviceId);
            logger.info("========3.Query the Details of a Device Completed========");

            logger.info("========4.Unfreeze Device========");
            unFreezeDevice(iotdaClient, deviceId);
            logger.info("========4.Unfreeze Device Completed========");

            logger.info("========5.Query the Details of a Device========");
            queryDeviceDetail(iotdaClient, deviceId);
            logger.info("========5.Query the Details of a Device Completed========");
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.warn("Demonstration failed. Exception information is {}", e.getMessage());
        } catch (ServiceResponseException e) {
            logger.warn("Demonstration failed. Exception information is {}, {}", e.getErrorCode(), e.getErrorMsg());
        }
    }
}
