package com.huawei.iotda;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.JsonUtils;
import com.huaweicloud.sdk.iotda.v5.IoTDAClient;
import com.huaweicloud.sdk.iotda.v5.model.AddCertificateRequest;
import com.huaweicloud.sdk.iotda.v5.model.AddCertificateResponse;
import com.huaweicloud.sdk.iotda.v5.model.CheckCertificateRequest;
import com.huaweicloud.sdk.iotda.v5.model.CheckCertificateResponse;
import com.huaweicloud.sdk.iotda.v5.model.CreateCertificateDTO;
import com.huaweicloud.sdk.iotda.v5.model.DeleteCertificateRequest;
import com.huaweicloud.sdk.iotda.v5.model.DeleteCertificateResponse;
import com.huaweicloud.sdk.iotda.v5.model.ListCertificatesRequest;
import com.huaweicloud.sdk.iotda.v5.model.ListCertificatesResponse;
import com.huaweicloud.sdk.iotda.v5.model.VerifyCertificateDTO;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeviceCertificateManagement {
    private static final Logger logger = LoggerFactory.getLogger(DeviceCertificateManagement.class);
    private static final ObjectMapper OBJECTMAPPER = new ObjectMapper();

    private static final String INSTANCE_ID = "<YOUR INSTANCEID>";

    // ENDPOINT：On the console, choose **Overview** in the navigation pane and click **Access Addresses** to view the HTTPS application access address.
    private static final String ENDPOINT = "<YOUR ENDPOINT>";

    // REGION_ID：If your region is 上海一，please fill in "cn-east-3"；If your region is 北京四，please fill in "cn-north-4"; If your region is 华南广州，please fill in "cn-south-4"
    private static final String REGION_ID = "<YOUR REGION ID>";

    // ak/sk：For details, see the method of obtaining AK/SK in iotda document https://support.huaweicloud.com/api-iothub/iot_06_v5_0091.html.
    // There will be security risks if the AK/SK used for authentication is directly written into code. We suggest encrypting the AK/SK in the configuration file or environment variables for storage.
    // In this sample, the AK/SK is stored in environment variables for identity authentication. Before running this sample, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");

    // Obtain the project ID of your region on the My Credentials > API Credentials page.
    private static final String PROJECT_ID = "<YOUR PROJECTID>";

    private static IoTDAClient initClient() {
        // Create a credential
        ICredential auth = new BasicCredentials()
            .withSk(SK)
            .withAk(AK)
            .withProjectId(PROJECT_ID)
            // Derivative algorithms are used for the standard and enterprise editions.
            // For the basic edition, delete the configuration "withDerivedPredicate".
            .withDerivedPredicate(BasicCredentials.DEFAULT_DERIVED_PREDICATE);

        // Create and initialize an IoTDAClient instance
        return IoTDAClient.newBuilder()
            // Use IoTDARegion for basic editions like "withRegion(IoTDARegion.CN_NORTH_4)". Create regions for standard/enterprise editions.
            .withRegion(new Region(REGION_ID, ENDPOINT))
            // .withRegion(IoTDARegion.CN_NORTH_4)
            .withCredential(auth)
            .build();
    }

    /**
     * Upload a Device Certificate
     *
     * @param iotdaClient iotda client
     * @param body        Structure for device certificate creation
     * @return Certificate ID
     */
    private static String addDeviceCACertificate(IoTDAClient iotdaClient, CreateCertificateDTO body) throws ServiceResponseException {
        AddCertificateRequest request = new AddCertificateRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        AddCertificateResponse response = iotdaClient.addCertificate(request);
        logger.info("The CA certificate upload result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        return response.getCertificateId();
    }

    /**
     * Query the Device Certificate List
     * Notice: A verifyCode will be returned after the device certificate creation. You need this parameter to make the certificate.
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space ID
     */
    private static void queryDeviceCACertificateList(IoTDAClient iotdaClient, String appId) throws ServiceResponseException {
        ListCertificatesRequest request = new ListCertificatesRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        ListCertificatesResponse response = iotdaClient.listCertificates(request);
        logger.info("The query result of the device CA certificate list is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    /**
     * Delete a Device Certificate
     *
     * @param iotdaClient   iotda client
     * @param certificateId Certificate ID
     */
    private static void deleteDeviceCACertificate(IoTDAClient iotdaClient, String certificateId) throws ServiceResponseException {
        DeleteCertificateRequest request = new DeleteCertificateRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setCertificateId(certificateId);
        DeleteCertificateResponse response = iotdaClient.deleteCertificate(request);
        logger.info("The device CA certificate deletion result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    /**
     * Verify a Device CA Certificate
     *
     * @param iotdaClient   iotda client
     * @param certificateId Certificate ID
     * @param body          Structure for device certificate verification
     */
    private static void verifyDeviceCACertificate(IoTDAClient iotdaClient, String certificateId, VerifyCertificateDTO body) throws ServiceResponseException {
        CheckCertificateRequest request = new CheckCertificateRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setCertificateId(certificateId);
        request.setBody(body);
        // The actionId here currently only supports "verify"
        request.setActionId("verify");
        CheckCertificateResponse response = iotdaClient.checkCertificate(request);
        logger.info("The device CA certificate verification result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    public static void main(String[] args) {
        // Initialize the IoTDA client
        IoTDAClient iotdaClient = initClient();
        // ID of the resource space to which the device belongs. Replace the value with the required one.
        String appId = "9d988b5efdf646f0828724c45e1d794e";

        try {
            logger.info("========1.Query the Device Certificate List========");
            queryDeviceCACertificateList(iotdaClient, appId);
            logger.info("========1.Query the Device Certificate List Completed========");

            logger.info("========2.Upload a Device Certificate========");
            // Assemble the device CA certificate data based on the actual situation of the device CA certificate.
            CreateCertificateDTO addDeviceCACertificate = new CreateCertificateDTO();
            addDeviceCACertificate.setAppId(appId);
            // The following is an example certificate. Replace it based on the site requirements.
            addDeviceCACertificate.setContent("******");
            String certificateId = addDeviceCACertificate(iotdaClient, addDeviceCACertificate);
            logger.info("========2.Upload a Device Certificate Completed========");

            logger.info("========3.Query the Device Certificate List========");
            queryDeviceCACertificateList(iotdaClient, appId);
            logger.info("========3.Query the Device Certificate List Completed========");

            logger.info("========4.Verifying the Device CA Certificate========");
            VerifyCertificateDTO verifyCertificateDTO = new VerifyCertificateDTO();
            // This is only an example. Replace it based on the site requirements.
            verifyCertificateDTO.setVerifyContent("******");
            verifyDeviceCACertificate(iotdaClient, certificateId, verifyCertificateDTO);
            logger.info("========4.Verifying the device CA certificate is complete========");

            logger.info("========5.Query the Device Certificate List========");
            queryDeviceCACertificateList(iotdaClient, appId);
            logger.info("========5.Query the Device Certificate List Completed========");

            logger.info("========6.Deleting a Device CA Certificate========");
            deleteDeviceCACertificate(iotdaClient, certificateId);
            logger.info("========6.The device CA certificate is deleted========");

            logger.info("========7.Query the Device Certificate List========");
            queryDeviceCACertificateList(iotdaClient, appId);
            logger.info("========7.Query the Device Certificate List Completed========");
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.warn("Demonstration failed. Exception information is {}", e.getMessage());
        } catch (ServiceResponseException e) {
            logger.warn("Demonstration failed. Exception information is {}, {}", e.getErrorCode(), e.getErrorMsg());
        }
    }
}