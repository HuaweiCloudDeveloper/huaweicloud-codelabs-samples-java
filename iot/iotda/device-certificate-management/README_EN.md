## 1. Functions

You can integrate the IoTDA server SDKs provided by Huawei Cloud to call IoTDA APIs and use IoTDA smoothly.
This example shows and demonstrates how to use the Java SDK to add, delete, query and certify the device certificates.

Device Certificate：An X.509 certificate is a digital certificate used for communication entity authentication. IoTDA allows devices to use their X.509 certificates for authentication. The use of X.509 certificate authentication protects devices from being spoofed.
For details, see [Connecting a Device That Uses the X.509 Certificate Based on MQTT.fx](https://support.huaweicloud.com/intl/en-us/bestpractice-iothub/iot_bp_0077.html)

**What You Will Learn**

The ways to use the Java SDK to add, delete, query and certify the device certificates.

## 2. Prerequisites
- 1. You have created a Huawei Cloud account and obtained the access key (AK) and secret access key (SK) of your account. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 2. You have set up the development environment with Java JDK 1.8 or later.
- 3. You have obtained the project ID of the target region. View the project ID on the **My Credentials** > **API Credentials** page of the Huawei Cloud console. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_1001.html).
- 4. You have enabled IoTDA. For details about how to obtain the ID of the current instance, see [Viewing Instance Details](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0121.html).

## 3. Obtaining and Installing the SDK
Download and install Maven, and add dependencies to the **pom.xml** file of the Java project.
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4. API Parameters
For details about API parameters, see:

[Upload a Device CA Certificate](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0014.html)

[Obtain the Device CA Certificate List](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0099.html)

[Delete a Device CA Certificate](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0022.html)

[Verify a Device CA Certificate](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0016.html)

## 5. Key Code Snippets
### 5.1 Upload a Device Certificate
```java
    /**
     * Upload a Device Certificate
     *
     * @param iotdaClient iotda client
     * @param body        Structure for device certificate creation
     * @return Certificate ID
     */
    private static String addDeviceCACertificate(IoTDAClient iotdaClient, CreateCertificateDTO body) throws ServiceResponseException {
        AddCertificateRequest request = new AddCertificateRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        AddCertificateResponse response = iotdaClient.addCertificate(request);
        logger.info("The CA certificate upload result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        return response.getCertificateId();
    }
```

### 5.2 Query the Device Certificate List

```java
    /**
     * Query the Device Certificate List
     * Notice: A verifyCode will be returned after the device certificate creation. You need this parameter to make the certificate.
     * 
     * @param iotdaClient iotda client
     * @param appId       Resource space ID
     */
    private static void queryDeviceCACertificateList(IoTDAClient iotdaClient, String appId) throws ServiceResponseException {
        ListCertificatesRequest request = new ListCertificatesRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        ListCertificatesResponse response = iotdaClient.listCertificates(request);
        logger.info("The query result of the device CA certificate list is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.3 Delete a Device Certificate
```java
    /**
     * Delete a Device Certificate
     *
     * @param iotdaClient   iotda client
     * @param certificateId Certificate ID
     */
    private static void deleteDeviceCACertificate(IoTDAClient iotdaClient, String certificateId) throws ServiceResponseException {
        DeleteCertificateRequest request = new DeleteCertificateRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setCertificateId(certificateId);
        DeleteCertificateResponse response = iotdaClient.deleteCertificate(request);
        logger.info("The device CA certificate deletion result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.4 Verify a Device CA Certificate

```java
    /**
     * Verify a Device CA Certificate
     *
     * @param iotdaClient   iotda client
     * @param certificateId Certificate ID
     * @param body          Structure for device certificate verification
     */
    private static void verifyDeviceCACertificate(IoTDAClient iotdaClient, String certificateId, VerifyCertificateDTO body) throws ServiceResponseException {
        CheckCertificateRequest request = new CheckCertificateRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setCertificateId(certificateId);
        request.setBody(body);
        // The actionId here currently only supports "verify"
        request.setActionId("verify");
        CheckCertificateResponse response = iotdaClient.checkCertificate(request);
        logger.info("The device CA certificate verification result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

## 6. Execution Results
Replace the parameters and execute the code in the sequence of the main method. The program demonstrates how to add, delete, query and certify the device certificates. The execution results of each step of the main method are shown as follows.

### 6.1 Query the Device CA Certificate List(Before the Device CA Certificate Creation)

```json
{
  "certificates": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffffffffffffffff"
  }
}
```
### 6.2 Create a Device CA Certificate

Notice, please use verify_code to verify this certificate.
```json
{
  "certificate_id": "64f695d5cce67c1de766503e",
  "cn_name": "******",
  "owner": "EMAILADDRESS=******, CN=******, OU=iot, O=huawei, L=sz, ST=gd, C=cn",
  "status": false,
  "verify_code": "******",
  "create_date": "20230905T024334Z",
  "effective_date": "20230905T023503Z",
  "expiry_date": "20260625T023503Z"
}
```

### 6.3 Query the Device CA Certificate List(Before the Device CA Certificate Creation)
```json
{
  "certificates": [
    {
      "certificate_id": "64f695d5cce67c1de766503e",
      "cn_name": "******",
      "owner": "EMAILADDRESS=******, CN=******, OU=iot, O=huawei, L=sz, ST=gd, C=cn",
      "status": false,
      "verify_code": "******",
      "create_date": "20230905T024334Z",
      "effective_date": "20230905T023503Z",
      "expiry_date": "20260625T023503Z"
    }
  ],
  "page": {
    "count": 1,
    "marker": "64f695d5cce67c1de766503e"
  }
}
```
### 6.4 Verify a Device CA Certificate
```json
{}
```

### 6.5 Query the Device CA Certificate List(After the Device CA Certificate Verification)
```json
{
  "certificates": [
    {
      "certificate_id": "64f695d5cce67c1de766503e",
      "cn_name": "******",
      "owner": "EMAILADDRESS=******, CN=******, OU=iot, O=huawei, L=sz, ST=gd, C=cn",
      "status": true,
      "verify_code": "******",
      "create_date": "20230905T024334Z",
      "effective_date": "20230905T023503Z",
      "expiry_date": "20260625T023503Z"
    }
  ],
  "page": {
    "count": 1,
    "marker": "64f695d5cce67c1de766503e"
  }
}
```

### 6.6 Delete a Device CA Certificate
```json
{}
```

### 6.7 Query the Device CA Certificate List(After the Device CA Certificate Deletion)
```json
{
  "certificates": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffffffffffffffff"
  }
}
```