## 1、功能介绍

华为云提供了IoTDA服务端SDK，您可以直接集成服务端SDK来调用IoTDA的相关API，从而实现对IoTDA的快速操作。
该示例展示了如何通过java版SDK来实现设备证书的上传，查询，删除，验证功能。

设备证书：X.509是一种用于通信实体鉴别的数字证书，物联网平台支持设备使用自己的X.509证书进行认证鉴权。使用X.509认证技术时，设备无法被仿冒，避免了密钥被泄露的风险。
详情请参考[基于MQTT.fx的X.509证书接入指导](https://support.huaweicloud.com/bestpractice-iothub/iot_bp_0077.html)

**您将学到什么？**

如何通过java版SDK来实现设备证书的上传，查询，删除，验证功能。

## 2、前置条件
- 1、已经开通华为云账号，并获得到您账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。您可以参考[获取项目ID](https://support.huaweicloud.com/api-iothub/iot_06_v5_1001.html)
- 4、已经开通IoTDA服务，获取当前实例的实例ID，请参考[查看实例详情](https://support.huaweicloud.com/usermanual-iothub/iot_01_0121.html) 。

## 3、SDK获取和安装
您需要下载并安装 Maven，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4、接口参数说明
关于接口参数的详细说明可参见：

[上传设备CA证书](https://support.huaweicloud.com/api-iothub/iot_06_v5_0014.html)

[获取设备CA证书列表](https://support.huaweicloud.com/api-iothub/iot_06_v5_0099.html)

[删除设备CA证书](https://support.huaweicloud.com/api-iothub/iot_06_v5_0022.html)

[验证设备CA证书](https://support.huaweicloud.com/api-iothub/iot_06_v5_0016.html)

## 5、关键代码片段
### 5.1、上传设备证书
```java
    /**
     * 上传设备证书
     *
     * @param iotdaClient iotda client
     * @param body        创建设备证书结构体
     * 注意：创建完证书后会返回verifyCode，需要使用该参数去制作验证证书
     * @return 证书ID
     */
    private static String addDeviceCACertificate(IoTDAClient iotdaClient, CreateCertificateDTO body) throws ServiceResponseException {
        AddCertificateRequest request = new AddCertificateRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        AddCertificateResponse response = iotdaClient.addCertificate(request);
        logger.info("上传CA证书的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        return response.getCertificateId();
    }
```

### 5.2、查询设备证书列表

```java
    /**
     * 查询设备证书列表
     * 
     * @param iotdaClient iotda client
     * @param appId       资源空间ID
     */
    private static void queryDeviceCACertificateList(IoTDAClient iotdaClient, String appId) throws ServiceResponseException {
        ListCertificatesRequest request = new ListCertificatesRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        ListCertificatesResponse response = iotdaClient.listCertificates(request);
        logger.info("查询设备CA证书列表的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.3、删除设备证书
```java
    /**
     * 删除设备证书
     *
     * @param iotdaClient   iotda client
     * @param certificateId 证书ID
     */
    private static void deleteDeviceCACertificate(IoTDAClient iotdaClient, String certificateId) throws ServiceResponseException {
        DeleteCertificateRequest request = new DeleteCertificateRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setCertificateId(certificateId);
        DeleteCertificateResponse response = iotdaClient.deleteCertificate(request);
        logger.info("删除设备CA证书的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.3、验证设备CA证书

```java
    /**
     * 验证设备CA证书
     *
     * @param iotdaClient   iotda client
     * @param certificateId 证书ID
     * @param body          验证设备证书结构体
     */
    private static void verifyDeviceCACertificate(IoTDAClient iotdaClient, String certificateId, VerifyCertificateDTO body) throws ServiceResponseException {
        CheckCertificateRequest request = new CheckCertificateRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setCertificateId(certificateId);
        request.setBody(body);
        // 此处actionId 目前仅支持verify
        request.setActionId("verify");
        CheckCertificateResponse response = iotdaClient.checkCertificate(request);
        logger.info("验证设备CA证书的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

## 6、运行结果
如果您替换main方法的参数，并按照main方法的顺序执行，程序将演示设备证书的新增，查询，验证，删除功能。
以下为main方法中每一个步骤的运行结果。

### 6.1 查询设备CA证书列表(创建设备CA证书前)

```json
{
  "certificates": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffffffffffffffff"
  }
}
```
### 6.2 创建设备CA证书

此处请注意，请使用该verify_code去制作验证证书
```json
{
  "certificate_id": "64f695d5cce67c1de766503e",
  "cn_name": "******",
  "owner": "EMAILADDRESS=******, CN=******, OU=iot, O=huawei, L=sz, ST=gd, C=cn",
  "status": false,
  "verify_code": "******",
  "create_date": "20230905T024334Z",
  "effective_date": "20230905T023503Z",
  "expiry_date": "20260625T023503Z"
}
```

### 6.3 查询设备CA证书列表(创建设备CA证书前)
```json
{
  "certificates": [
    {
      "certificate_id": "64f695d5cce67c1de766503e",
      "cn_name": "******",
      "owner": "EMAILADDRESS=******, CN=******, OU=iot, O=huawei, L=sz, ST=gd, C=cn",
      "status": false,
      "verify_code": "******",
      "create_date": "20230905T024334Z",
      "effective_date": "20230905T023503Z",
      "expiry_date": "20260625T023503Z"
    }
  ],
  "page": {
    "count": 1,
    "marker": "64f695d5cce67c1de766503e"
  }
}
```
### 6.4 验证设备CA证书
```json
{}
```

### 6.5 查询设备CA证书列表(验证设备CA证书后)
```json
{
  "certificates": [
    {
      "certificate_id": "64f695d5cce67c1de766503e",
      "cn_name": "******",
      "owner": "EMAILADDRESS=******, CN=******, OU=iot, O=huawei, L=sz, ST=gd, C=cn",
      "status": true,
      "verify_code": "******",
      "create_date": "20230905T024334Z",
      "effective_date": "20230905T023503Z",
      "expiry_date": "20260625T023503Z"
    }
  ],
  "page": {
    "count": 1,
    "marker": "64f695d5cce67c1de766503e"
  }
}
```

### 6.6 删除设备CA证书
```json
{}
```

### 6.7 查询设备CA证书列表(删除设备CA证书后)
```json
{
  "certificates": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffffffffffffffff"
  }
}
```