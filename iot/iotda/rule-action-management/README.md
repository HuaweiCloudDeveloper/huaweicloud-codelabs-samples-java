## 1、功能介绍

华为云提供了IoTDA服务端SDK，您可以直接集成服务端SDK来调用IoTDA的相关API，从而实现对IoTDA的快速操作。
该示例展示了如何通过java版SDK来实现对规则条件的新增，修改，删除，查询，列表查询功能，同时通过demo进行简单的演示操作。

规则动作：当规则触发条件触发后，将要执行的动作，属于数据转发的一部分，配合规则触发条件使用，用于决定数据流转的方式和目的地。
详情可参考：

[规则引擎介绍](https://support.huaweicloud.com/usermanual-iothub/iot_01_0022.html)

[数据转发简介](https://support.huaweicloud.com/usermanual-iothub/iot_01_0024.html)

[订阅推送方式概述](https://support.huaweicloud.com/usermanual-iothub/iot_01_00140.html)

**您将学到什么？**

如何通过java版SDK来实现对规则动作条件的新增，修改，删除，查询，列表查询功能。

## 2、前置条件
- 1、已经开通华为云账号，并获得到您账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。您可以参考[获取项目ID](https://support.huaweicloud.com/api-iothub/iot_06_v5_1001.html)
- 4、已经开通IoTDA服务，获取当前实例的实例ID，请参考[查看实例详情](https://support.huaweicloud.com/usermanual-iothub/iot_01_0121.html) 。
- 5、已经创建规则动作条件，并获取到对应规则的ID。

## 3、SDK获取和安装
您需要下载并安装 Maven，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```ndency>
```

## 4、接口参数说明
关于接口参数的详细说明可参见：

[创建规则动作](https://support.huaweicloud.com/api-iothub/iot_06_v5_01307.html)

[查询规则动作列表](https://support.huaweicloud.com/api-iothub/iot_06_v5_01306.html)

[查询规则动作](https://support.huaweicloud.com/api-iothub/iot_06_v5_01308.html)

[修改规则动作](https://support.huaweicloud.com/api-iothub/iot_06_v5_01309.html)

[删除规则动作](https://support.huaweicloud.com/api-iothub/iot_06_v5_01310.html)

## 5、关键代码片段
### 5.1、创建规则动作
```java
    /**
     * 创建规则动作
     *
     * @param iotdaClient iotda client
     * @param body        创建规则动作结构体
     * @return 规则动作ID
     */
    private static String createRuleAction(IoTDAClient iotdaClient, AddActionReq body) throws ServiceResponseException {
        CreateRuleActionRequest request = new CreateRuleActionRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        CreateRuleActionResponse response = iotdaClient.createRuleAction(request);
        logger.info("创建规则动作的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        return response.getActionId();
    }
```

### 5.2、查询规则动作列表

```java
    /**
     * 查询规则动作列表
     *
     * @param iotdaClient iotda client
     * @param appId       资源空间ID
     * @param ruleId      流转规则ID
     */
    private static void queryRuleActionList(IoTDAClient iotdaClient, String appId, String ruleId) throws ServiceResponseException {
        ListRuleActionsRequest request = new ListRuleActionsRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        request.setRuleId(ruleId);
        ListRuleActionsResponse response = iotdaClient.listRuleActions(request);
        logger.info("查询创建规则动作列表的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.3、查询规则动作详情
```java
    /**
     * 查询规则动作详情
     *
     * @param iotdaClient iotda client
     * @param actionId    规则动作ID
     */
    private static void queryRuleActionDetail(IoTDAClient iotdaClient, String actionId) throws ServiceResponseException {
        ShowRuleActionRequest request = new ShowRuleActionRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setActionId(actionId);
        ShowRuleActionResponse productResponse = iotdaClient.showRuleAction(request);
        logger.info("查询创建规则动作详情的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, productResponse));
    }
```

### 5.4、修改规则动作
```java
    /**
     * 更新规则动作
     *
     * @param iotdaClient iotda client
     * @param actionId    规则动作ID
     * @param body        规则动作结构体
     */
    private static void updateRuleAction(IoTDAClient iotdaClient, String actionId, UpdateActionReq body) throws ServiceResponseException {
        UpdateRuleActionRequest request = new UpdateRuleActionRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        request.setActionId(actionId);
        UpdateRuleActionResponse updateRuleAction = iotdaClient.updateRuleAction(request);
        logger.info("更新创建规则动作的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, updateRuleAction));
    }
```

### 5.5、删除规则动作
```java
    /**
     * 删除规则动作
     *
     * @param iotdaClient iotda client
     * @param appId       资源空间ID
     * @param actionId    规则动作ID
     */
    private static void deleteRuleAction(IoTDAClient iotdaClient, String appId, String actionId) throws ServiceResponseException {
        DeleteRuleActionRequest request = new DeleteRuleActionRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setActionId(actionId);
        DeleteRuleActionResponse response = iotdaClient.deleteRuleAction(request);
        logger.info("删除创建规则动作的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

## 6、运行结果
如果您替换main方法的参数，并按照main方法的顺序执行，程序将演示规则动作的新增，修改，查询，修改，删除功能。
本次规则动作动作为属性上报为例，使用AMQP通道进行流转，以下为main方法中每一个步骤的运行结果。

### 6.1 查询规则动作列表(创建规则动作条件前)

由于还未规则动作，查出来为空
```json
{
  "actions": [],
  "count": 0
}
```
### 6.2 创建规则动作
```json
{
  "action_id": "bf0d850d-99be-4174-8068-677b63e4408d",
  "rule_id": "dd868e3b-71aa-4514-95e0-77e5c36f8980",
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "channel": "AMQP_FORWARDING",
  "channel_detail": {
    "amqp_forwarding": {
      "queue_name": "my-queue-1"
    }
  }
}
```

### 6.3 查询规则动作列表(创建规则动作条件后)
```json
{
  "actions": [
    {
      "action_id": "bf0d850d-99be-4174-8068-677b63e4408d",
      "rule_id": "dd868e3b-71aa-4514-95e0-77e5c36f8980",
      "app_id": "9d988b5efdf646f0828724c45e1d794e",
      "channel": "AMQP_FORWARDING",
      "channel_detail": {
        "amqp_forwarding": {
          "queue_name": "my-queue-1"
        }
      }
    }
  ],
  "count": 1,
  "marker": "64f5a3105b1cd172d3b43a4c"
}
```
### 6.4 查询规则动作详情
```json
{
  "action_id": "bf0d850d-99be-4174-8068-677b63e4408d",
  "rule_id": "dd868e3b-71aa-4514-95e0-77e5c36f8980",
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "channel": "AMQP_FORWARDING",
  "channel_detail": {
    "amqp_forwarding": {
      "queue_name": "my-queue-1"
    }
  }
}
```


### 6.5 更新规则动作
```json
{
  "action_id": "bf0d850d-99be-4174-8068-677b63e4408d",
  "rule_id": "dd868e3b-71aa-4514-95e0-77e5c36f8980",
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "channel": "AMQP_FORWARDING",
  "channel_detail": {
    "amqp_forwarding": {
      "queue_name": "device-rule-test-1"
    }
  }
}
```

### 6.6 查询规则动作详情
```json
{
  "action_id": "bf0d850d-99be-4174-8068-677b63e4408d",
  "rule_id": "dd868e3b-71aa-4514-95e0-77e5c36f8980",
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "channel": "AMQP_FORWARDING",
  "channel_detail": {
    "amqp_forwarding": {
      "queue_name": "device-rule-test-1"
    }
  }
}
```

### 6.7 删除规则动作
```json
{}
```
### 6.8 查询规则动作条件列表(删除规则动作条件后）
```json
{
  "actions": [],
  "count": 0
}
```