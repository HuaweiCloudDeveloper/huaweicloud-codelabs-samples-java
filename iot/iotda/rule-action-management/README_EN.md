## 1. Functions

You can integrate the IoTDA server SDKs provided by Huawei Cloud to call IoTDA APIs and use IoTDA smoothly.
This example shows and demonstrates how to use the Java SDK to add, modify, delete and query a rule, as well as query the rule list.

Rule Action: The action performed when the rule triggering conditions are satisfied. It is a part of data forwarding step and decides the way and destinations of data forwarding.

For details, see:

[Rules - Overview](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0022.html)

[Data Forwarding](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0024.html)

[Subscription and Push - Overview](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_00140.html)

**What You Will Learn**

The ways to use the Java SDK to add, modify, delete and query a rule, as well as query the rule list.

## 2. Prerequisites
- 1. You have created a Huawei Cloud account and obtained the access key (AK) and secret access key (SK) of your account. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 2. You have set up the development environment with Java JDK 1.8 or later.
- 3. You have obtained the project ID of the target region. View the project ID on the **My Credentials** > **API Credentials** page of the Huawei Cloud console. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_1001.html).
- 4. You have enabled IoTDA. For details about how to obtain the ID of the current instance, see [Viewing Instance Details](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0121.html).
- 5. You have created a rule action, and you have obtained the ID of the rule.

## 3. Obtaining and Installing the SDK
Download and install Maven, and add dependencies to the **pom.xml** file of the Java project.
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```ndency>
```

## 4. API Parameters
For details about API parameters, see:

[Creating a Rule Triggering Condition](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_01307.html)

[Querying the Rule Triggering Condition List](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_01306.html)

[Querying a Rule Triggering Condition](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_01308.html)

[Modifying a Rule Triggering Condition](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_01309.html)

[Deleting a Rule Triggering Condition](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_01310.html)

## 5. Key Code Snippets
### 5.1 Create a Rule Action
```java
    /**
     * Create a Rule Action
     *
     * @param iotdaClient iotda client
     * @param body        Structure for creating a rule action
     * @return Rule action ID
     */
    private static String createRuleAction(IoTDAClient iotdaClient, AddActionReq body) throws ServiceResponseException {
        CreateRuleActionRequest request = new CreateRuleActionRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        CreateRuleActionResponse response = iotdaClient.createRuleAction(request);
        logger.info("The rule action creation result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        return response.getActionId();
    }
```

### 5.2 Query the Rule Action List

```java
    /**
     * Query the Rule Action List
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space ID
     * @param ruleId      Rule ID
     */
    private static void queryRuleActionList(IoTDAClient iotdaClient, String appId, String ruleId) throws ServiceResponseException {
        ListRuleActionsRequest request = new ListRuleActionsRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        request.setRuleId(ruleId);
        ListRuleActionsResponse response = iotdaClient.listRuleActions(request);
        logger.info("The result of querying the rule action list is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.3 Query the Details About a Rule Action
```java
    /**
     * Query the Details About a Rule Action
     *
     * @param iotdaClient iotda client
     * @param actionId    Rule action ID
     */
    private static void queryRuleActionDetail(IoTDAClient iotdaClient, String actionId) throws ServiceResponseException {
        ShowRuleActionRequest request = new ShowRuleActionRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setActionId(actionId);
        ShowRuleActionResponse productResponse = iotdaClient.showRuleAction(request);
        logger.info("The result of querying the details about a rule action is:{}", JsonUtils.toJSON(OBJECTMAPPER, productResponse));
    }
```

### 5.4 Update a Rule Action
```java
    /**
     * Update a Rule Action
     *
     * @param iotdaClient iotda client
     * @param actionId    Rule action ID
     * @param body        Structure for updating a rule action
     */
    private static void updateRuleAction(IoTDAClient iotdaClient, String actionId, UpdateActionReq body) throws ServiceResponseException {
        UpdateRuleActionRequest request = new UpdateRuleActionRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        request.setActionId(actionId);
        UpdateRuleActionResponse updateRuleAction = iotdaClient.updateRuleAction(request);
        logger.info("The result of updating a rule action is:{}", JsonUtils.toJSON(OBJECTMAPPER, updateRuleAction));
    }
```

### 5.5 Delete a Rule Action
```java
    /**
     * Delete a Rule Action
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space ID
     * @param actionId    Rule action ID
     */
    private static void deleteRuleAction(IoTDAClient iotdaClient, String appId, String actionId) throws ServiceResponseException {
        DeleteRuleActionRequest request = new DeleteRuleActionRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setActionId(actionId);
        DeleteRuleActionResponse response = iotdaClient.deleteRuleAction(request);
        logger.info("The result of deleting a rule action is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

## 6. Execution Results
Replace the parameters and execute the code in the sequence of the main method. The program demonstrates how to add, modify, delete and query a rule, as well as query the rule list.
The execution results of each step of the main method are shown as follows.

### 6.1 Query the Rule Action List(Before Creating a Rule Action)

Because the rule action is not created yet, the result is empty.
```json
{
  "actions": [],
  "count": 0
}
```
### 6.2 Create a Rule Action
```json
{
  "action_id": "bf0d850d-99be-4174-8068-677b63e4408d",
  "rule_id": "dd868e3b-71aa-4514-95e0-77e5c36f8980",
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "channel": "AMQP_FORWARDING",
  "channel_detail": {
    "amqp_forwarding": {
      "queue_name": "my-queue-1"
    }
  }
}
```

### 6.3 Query the Rule Action List(After Creating a Rule Action)
```json
{
  "actions": [
    {
      "action_id": "bf0d850d-99be-4174-8068-677b63e4408d",
      "rule_id": "dd868e3b-71aa-4514-95e0-77e5c36f8980",
      "app_id": "9d988b5efdf646f0828724c45e1d794e",
      "channel": "AMQP_FORWARDING",
      "channel_detail": {
        "amqp_forwarding": {
          "queue_name": "my-queue-1"
        }
      }
    }
  ],
  "count": 1,
  "marker": "64f5a3105b1cd172d3b43a4c"
}
```
### 6.4 Query the Details About a Rule Action
```json
{
  "action_id": "bf0d850d-99be-4174-8068-677b63e4408d",
  "rule_id": "dd868e3b-71aa-4514-95e0-77e5c36f8980",
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "channel": "AMQP_FORWARDING",
  "channel_detail": {
    "amqp_forwarding": {
      "queue_name": "my-queue-1"
    }
  }
}
```

### 6.5 Update a Rule Action
```json
{
  "action_id": "bf0d850d-99be-4174-8068-677b63e4408d",
  "rule_id": "dd868e3b-71aa-4514-95e0-77e5c36f8980",
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "channel": "AMQP_FORWARDING",
  "channel_detail": {
    "amqp_forwarding": {
      "queue_name": "device-rule-test-1"
    }
  }
}
```

### 6.6 Query the Details About a Rule Action
```json
{
  "action_id": "bf0d850d-99be-4174-8068-677b63e4408d",
  "rule_id": "dd868e3b-71aa-4514-95e0-77e5c36f8980",
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "channel": "AMQP_FORWARDING",
  "channel_detail": {
    "amqp_forwarding": {
      "queue_name": "device-rule-test-1"
    }
  }
}
```

### 6.7 Delete a Rule Action
```json
{}
```
### 6.8 Query the Rule Action List(After Deleting the Rule Action)
```json
{
  "actions": [],
  "count": 0
}
```