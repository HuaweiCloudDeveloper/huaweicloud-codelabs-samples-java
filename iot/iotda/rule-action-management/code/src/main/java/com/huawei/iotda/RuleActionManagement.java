package com.huawei.iotda;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.JsonUtils;
import com.huaweicloud.sdk.iotda.v5.IoTDAClient;
import com.huaweicloud.sdk.iotda.v5.model.AddActionReq;
import com.huaweicloud.sdk.iotda.v5.model.AmqpForwarding;
import com.huaweicloud.sdk.iotda.v5.model.ChannelDetail;
import com.huaweicloud.sdk.iotda.v5.model.CreateRuleActionRequest;
import com.huaweicloud.sdk.iotda.v5.model.CreateRuleActionResponse;
import com.huaweicloud.sdk.iotda.v5.model.DeleteRuleActionRequest;
import com.huaweicloud.sdk.iotda.v5.model.DeleteRuleActionResponse;
import com.huaweicloud.sdk.iotda.v5.model.ListRuleActionsRequest;
import com.huaweicloud.sdk.iotda.v5.model.ListRuleActionsResponse;
import com.huaweicloud.sdk.iotda.v5.model.ShowRuleActionRequest;
import com.huaweicloud.sdk.iotda.v5.model.ShowRuleActionResponse;
import com.huaweicloud.sdk.iotda.v5.model.UpdateActionReq;
import com.huaweicloud.sdk.iotda.v5.model.UpdateRuleActionRequest;
import com.huaweicloud.sdk.iotda.v5.model.UpdateRuleActionResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RuleActionManagement {
    private static final Logger logger = LoggerFactory.getLogger(RuleActionManagement.class);
    private static final ObjectMapper OBJECTMAPPER = new ObjectMapper();

    // REGION_ID：If your region is 上海一，please fill in "cn-east-3"；If your region is 北京四，please fill in "cn-north-4"; If your region is 华南广州，please fill in "cn-south-4"
    private static final String REGION_ID = "<YOUR REGION ID>";

    // ENDPOINT：On the console,
    // choose **Overview** in the navigation pane and click **Access Addresses** to view the HTTPS application access address.
    private static final String ENDPOINT = "<YOUR ENDPOINT>";

    // ak/sk：For details, see the method of obtaining AK/SK in iotda document https://support.huaweicloud.com/api-iothub/iot_06_v5_0091.html.
    // There will be security risks if the AK/SK used for authentication is directly written into code. We suggest encrypting the AK/SK in the configuration file or environment variables for storage.
    // In this sample, the AK/SK is stored in environment variables for identity authentication.
    // Before running this sample, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String PROJECT_ID = "<YOUR PROJECTID>";

    // INSTANCE_ID
    private static final String INSTANCE_ID = "<YOUR INSTANCEID>";

    private static IoTDAClient initClient() {
        // Create a credential
        ICredential auth = new BasicCredentials()
            .withAk(AK)
            .withSk(SK)
            // Derivative algorithms are used for the standard and enterprise editions. For the basic edition, delete the configuration "withDerivedPredicate".
            .withDerivedPredicate(BasicCredentials.DEFAULT_DERIVED_PREDICATE)
            .withProjectId(PROJECT_ID);

        // Create and initialize an IoTDAClient instance
        return IoTDAClient.newBuilder()
            .withCredential(auth)
            // Use IoTDARegion for basic editions like
            // "withRegion(IoTDARegion.CN_NORTH_4)".
            // Create regions for standard/enterprise editions.
            .withRegion(new Region(REGION_ID, ENDPOINT))
            // .withRegion(IoTDARegion.CN_NORTH_4)
            .build();
    }

    /**
     * Create a Rule Action
     *
     * @param iotdaClient iotda client
     * @param body        Structure for creating a rule action
     * @return Rule action ID
     */
    private static String createRuleAction(IoTDAClient iotdaClient, AddActionReq body) throws ServiceResponseException {
        CreateRuleActionRequest request = new CreateRuleActionRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        CreateRuleActionResponse response = iotdaClient.createRuleAction(request);
        logger.info("The rule action creation result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        return response.getActionId();
    }

    /**
     * Query the Rule Action List
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space ID
     * @param ruleId      Rule ID
     */
    private static void queryRuleActionList(IoTDAClient iotdaClient, String appId, String ruleId) throws ServiceResponseException {
        ListRuleActionsRequest request = new ListRuleActionsRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        request.setRuleId(ruleId);
        ListRuleActionsResponse response = iotdaClient.listRuleActions(request);
        logger.info("The result of querying the rule action list is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    /**
     * Query the Details About a Rule Action
     *
     * @param iotdaClient iotda client
     * @param actionId    Rule action ID
     */
    private static void queryRuleActionDetail(IoTDAClient iotdaClient, String actionId) throws ServiceResponseException {
        ShowRuleActionRequest request = new ShowRuleActionRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setActionId(actionId);
        ShowRuleActionResponse productResponse = iotdaClient.showRuleAction(request);
        logger.info("The result of querying the details about a rule action is:{}", JsonUtils.toJSON(OBJECTMAPPER, productResponse));
    }

    /**
     * Update a Rule Action
     *
     * @param iotdaClient iotda client
     * @param actionId    Rule action ID
     * @param body        Structure for updating a rule action
     */
    private static void updateRuleAction(IoTDAClient iotdaClient, String actionId, UpdateActionReq body) throws ServiceResponseException {
        UpdateRuleActionRequest request = new UpdateRuleActionRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        request.setActionId(actionId);
        UpdateRuleActionResponse updateRuleAction = iotdaClient.updateRuleAction(request);
        logger.info("The result of updating a rule action is:{}", JsonUtils.toJSON(OBJECTMAPPER, updateRuleAction));
    }

    /**
     * Delete a Rule Action
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space ID
     * @param actionId    Rule action ID
     */
    private static void deleteRuleAction(IoTDAClient iotdaClient, String appId, String actionId) throws ServiceResponseException {
        DeleteRuleActionRequest request = new DeleteRuleActionRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setActionId(actionId);
        DeleteRuleActionResponse response = iotdaClient.deleteRuleAction(request);
        logger.info("The result of deleting a rule action is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    public static void main(String[] args) {
        // Initialize the IoTDA client
        IoTDAClient iotdaClient = initClient();
        // ID of the resource space to which the device belongs. Replace the value with the required one.
        String appId = "9d988b5efdf646f0828724c45e1d794e";

        // Replace it with the ID of the rule you have created.
        String ruleId = "dd868e3b-71aa-4514-95e0-77e5c36f8980";

        try {
            logger.info("========1.Querying the Action List of Rule Creation========");

            queryRuleActionList(iotdaClient, appId, ruleId);
            logger.info("========1.Querying the action list of the created rule is complete========");

            logger.info("========2.Creating a rule creation action========");
            // Assemble and create rule action data. You can assemble the data based on the actual situation. The following uses the data transfer to the AMQP as an example.
            AddActionReq addRuleAction = new AddActionReq();
            addRuleAction.setRuleId(ruleId);
            addRuleAction.setChannel("AMQP_FORWARDING");
            // The options are as follows: GLOBAL: The rule takes effect at the tenant level. APP: takes effect at the resource space level. If the type is APP, the created rule takes effect in the resource space specified by the app_id.
            // If the app_id is not specified, the created rule takes effect in the default resource space.
            ChannelDetail channelDetail = new ChannelDetail();
            AmqpForwarding amqpForwarding = new AmqpForwarding();
            amqpForwarding.setQueueName("21222222");
            channelDetail.setAmqpForwarding(amqpForwarding);
            addRuleAction.setChannelDetail(channelDetail);
            String actionId = createRuleAction(iotdaClient, addRuleAction);
            logger.info("========2.Creating a rule action is complete========");

            logger.info("========3.Querying the Action List of Rule Creation========");

            queryRuleActionList(iotdaClient, appId, ruleId);
            logger.info("========3.Querying the action list of the created rule is complete========");

            logger.info("========4.Queries details about a rule creation action========");

            queryRuleActionDetail(iotdaClient, actionId);
            logger.info("========4.Querying rule creation action details completed========");

            logger.info("========5.Update Create Rule Action========");
            // Change the queue name based on the site requirements.
            UpdateActionReq updateRuleAction = new UpdateActionReq();
            // Replace it with the name of the AMQP queue created on the platform.
            amqpForwarding.setQueueName("device-rule-test-1");
            updateRuleAction.setChannelDetail(channelDetail);
            updateRuleAction.setChannel("AMQP_FORWARDING");
            updateRuleAction(iotdaClient, actionId, updateRuleAction);
            logger.info("========5.Updating the action of creating a rule is complete========");

            logger.info("========6.Queries details about a rule creation action========");

            queryRuleActionDetail(iotdaClient, actionId);
            logger.info("========6.Querying rule creation action details completed========");

            logger.info("========7.Delete a rule creation action========");

            deleteRuleAction(iotdaClient, appId, actionId);
            logger.info("========7.Deleting the action of creating a rule is complete========");

            logger.info("========8.Querying the Action List of Rule Creation========");

            queryRuleActionList(iotdaClient, appId, ruleId);
            logger.info("========8.Querying the action list of the created rule is complete========");
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.warn("Demonstration failed. Exception information is {}", e.getMessage());
        } catch (ServiceResponseException e) {
            logger.warn("Demonstration failed. Exception information is {}, {}", e.getErrorCode(), e.getErrorMsg());
        }
    }
}