## 1. Functions

You can integrate the IoTDA server SDKs provided by Huawei Cloud to call IoTDA APIs and use IoTDA smoothly.
This example shows and demonstrates how to use the Java SDK to reset the device credential when you forget or need to update the credential.

Reset a Device Secret: If the device credential is by a device secret, you need to reset the device secret.

Reset a Device Fingerprint: If the device credential is by a device certificate, you need to reset the device certificate's fingerprint.

**What You Will Learn**

The ways to use the Java SDK to reset the device credential.

## 2. Prerequisites
- 1. You have created a Huawei Cloud account and obtained the access key (AK) and secret access key (SK) of your account. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 2. You have set up the development environment with Java JDK 1.8 or later.
- 3. You have obtained the project ID of the target region. View the project ID on the **My Credentials** > **API Credentials** page of the Huawei Cloud console. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_1001.html).
- 4. You have enabled IoTDA. For details about how to obtain the ID of the current instance, see [Viewing Instance Details](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0121.html).
- 5. You have created two devices, one's credential is by a device secret and one's credential is by a certificate.

## 3. Obtaining and Installing the SDK
Download and install Maven, and add dependencies to the **pom.xml** file of the Java project.
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4. API Parameters
For details about API parameters, see:

[Reset a Device Secret](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0093.html)

[Reset a Device Fingerprint](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0196.html)

[Query a Device](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0055.html)

## 5. Key Code Snippets
### 5.1 Reset a Device Secret

```java
    /**
     * Reset a Device Secret
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device ID
     * @param body        Structure for resetting a device secret
     */
    private static void resetSecret(IoTDAClient iotdaClient, String deviceId, ResetDeviceSecret body) throws ServiceResponseException {
        ResetDeviceSecretRequest request = new ResetDeviceSecretRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        request.setBody(body);
        ResetDeviceSecretResponse resetDeviceSecret = iotdaClient.resetDeviceSecret(request);
        logger.info("The device secret reset result is:{}", JsonUtils.toJSON(OBJECTMAPPER, resetDeviceSecret));
    }
```

### 5.2 Query the Details About a Device
```java
    /**
     * Query the Details About a Device
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device ID
     */
    private static void queryDeviceDetail(IoTDAClient iotdaClient, String deviceId) throws ServiceResponseException {
        ShowDeviceRequest request = new ShowDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        ShowDeviceResponse deviceResponse = iotdaClient.showDevice(request);
        logger.info("The device details query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, deviceResponse));
    }
```

### 5.3 Reset a Device Fingerprint

```java
    /**
     * Reset a Device Fingerprint
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device ID
     * @param body        Structure for resetting a device fingerprint
     */
    private static void resetFingerprint(IoTDAClient iotdaClient, String deviceId, ResetFingerprint body) throws ServiceResponseException {
        ResetFingerprintRequest request = new ResetFingerprintRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        request.setBody(body);
        ResetFingerprintResponse resetFingerprint = iotdaClient.resetFingerprint(request);
        logger.info("The device fingerprint reset result is:{}", JsonUtils.toJSON(OBJECTMAPPER, resetFingerprint));
    }
```

## 6. Execution Results
Replace the parameters and execute the code in the sequence of the main method. The program demonstrates how to reset the device secret and the device fingerprint.
The execution results of each step of the main method are shown as follows.

### 6.1 Query the Details About a Device(Before the Device Secret Reset)
```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "teatApp",
  "device_id": "64111bcd06ca5933b28a058b_7765dsfdsfsd",
  "node_id": "7765dsfdsfsd",
  "gateway_id": "64111bcd06ca5933b28a058b_7765dsfdsfsd",
  "node_type": "GATEWAY",
  "auth_info": {
    "auth_type": "SECRET",
    "secret": "728f****8f8d",
    "secure_access": true,
    "timeout": 0
  },
  "product_id": "64111bcd06ca5933b28a058b",
  "product_name": "testProduct",
  "status": "INACTIVE",
  "create_time": "20230516T034341Z",
  "tags": [
    {
      "tag_key": "testTagName",
      "tag_value": "testTagValue"
    }
  ]
}
```
### 6.2 Reset a Device Secret
```json
{
  "device_id": "64111bcd06ca5933b28a058b_7765dsfdsfsd",
  "secret": "dc0f****ae2f"
}
```
### 6.3 Query the Details About a Device(After the Device Secret Reset)
```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "teatApp",
  "device_id": "64111bcd06ca5933b28a058b_7765dsfdsfsd",
  "node_id": "7765dsfdsfsd",
  "gateway_id": "64111bcd06ca5933b28a058b_7765dsfdsfsd",
  "node_type": "GATEWAY",
  "auth_info": {
    "auth_type": "SECRET",
    "secret": "dc0f****df2f",
    "secure_access": true,
    "timeout": 0
  },
  "product_id": "64111bcd06ca5933b28a058b",
  "product_name": "testProduct",
  "status": "INACTIVE",
  "create_time": "20230516T034341Z",
  "tags": [
    {
      "tag_key": "testTagName",
      "tag_value": "testTagValue"
    }
  ]
}
```
### 6.4 Query the Details About a Device(Before the Device Fingerprint Reset)
```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "teatApp",
  "device_id": "64111bcd06ca5933b28a058b_SASDASDAS",
  "node_id": "SASDASDAS",
  "gateway_id": "64111bcd06ca5933b28a058b_SASDASDAS",
  "node_type": "GATEWAY",
  "auth_info": {
    "auth_type": "CERTIFICATES",
    "fingerprint": "deaf****ca13",
    "secure_access": false,
    "timeout": 0
  },
  "product_id": "64111bcd06ca5933b28a058b",
  "product_name": "testProduct",
  "status": "INACTIVE",
  "create_time": "20230828T120150Z",
  "tags": []
}
```
### 6.5 Reset a Device Fingerprint

```json
{
  "device_id": "64111bcd06ca5933b28a058b_SASDASDAS",
  "fingerprint": "aef1****de2f"
}
```
### 6.6 Query the Details About a Device(After the Device Fingerprint Reset)
```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "teatApp",
  "device_id": "64111bcd06ca5933b28a058b_SASDASDAS",
  "node_id": "SASDASDAS",
  "gateway_id": "64111bcd06ca5933b28a058b_SASDASDAS",
  "node_type": "GATEWAY",
  "auth_info": {
    "auth_type": "CERTIFICATES",
    "fingerprint": "aef1****de2f",
    "secure_access": false,
    "timeout": 0
  },
  "product_id": "64111bcd06ca5933b28a058b",
  "product_name": "testProduct",
  "status": "INACTIVE",
  "create_time": "20230828T120150Z",
  "tags": []
}
```