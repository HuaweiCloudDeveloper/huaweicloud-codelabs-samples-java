package com.huawei.iotda;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.JsonUtils;
import com.huaweicloud.sdk.iotda.v5.IoTDAClient;
import com.huaweicloud.sdk.iotda.v5.model.ResetDeviceSecret;
import com.huaweicloud.sdk.iotda.v5.model.ResetDeviceSecretRequest;
import com.huaweicloud.sdk.iotda.v5.model.ResetDeviceSecretResponse;
import com.huaweicloud.sdk.iotda.v5.model.ResetFingerprint;
import com.huaweicloud.sdk.iotda.v5.model.ResetFingerprintRequest;
import com.huaweicloud.sdk.iotda.v5.model.ResetFingerprintResponse;
import com.huaweicloud.sdk.iotda.v5.model.ShowDeviceRequest;
import com.huaweicloud.sdk.iotda.v5.model.ShowDeviceResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResetDeviceCredential {
    private static final Logger logger = LoggerFactory.getLogger(ResetDeviceCredential.class);
    private static final ObjectMapper OBJECTMAPPER = new ObjectMapper();

    // REGION_ID：If your region is 上海一，please fill in "cn-east-3"；If your region is 北京四，please fill in "cn-north-4"; If your region is 华南广州，please fill in "cn-south-4"
    private static final String REGION_ID = "<YOUR REGION ID>";

    // ENDPOINT：On the console, choose **Overview** in the navigation pane and click **Access Addresses** to view the HTTPS application access address.
    private static final String ENDPOINT = "<YOUR ENDPOINT>";

    // ak/sk：For details, see the method of obtaining AK/SK in iotda document https://support.huaweicloud.com/api-iothub/iot_06_v5_0091.html.
    // There will be security risks if the AK/SK used for authentication is directly written into code. We suggest encrypting the AK/SK in the configuration file or environment variables for storage.
    // In this sample, the AK/SK is stored in environment variables for identity authentication. Before running this sample, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String PROJECT_ID = "<YOUR PROJECTID>";

    // INSTANCE_ID
    private static final String INSTANCE_ID = "<YOUR INSTANCEID>";

    private static IoTDAClient initClient() {
        // Create a credential
        ICredential auth = new BasicCredentials()
                .withAk(AK)
                .withSk(SK)
                // Derivative algorithms are used for the standard and enterprise editions. For the basic edition, delete the configuration "withDerivedPredicate".
                .withDerivedPredicate(BasicCredentials.DEFAULT_DERIVED_PREDICATE)
                .withProjectId(PROJECT_ID);

        // Create and initialize an IoTDAClient instance
        return IoTDAClient.newBuilder()
                .withCredential(auth)
                // Use IoTDARegion for basic editions like "withRegion(IoTDARegion.CN_NORTH_4)". Create regions for standard/enterprise editions.
                .withRegion(new Region(REGION_ID, ENDPOINT))
                // .withRegion(IoTDARegion.CN_NORTH_4)
                .build();
    }

    /**
     * Reset a Device Secret
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device ID
     * @param body        Structure for resetting a device secret
     */
    private static void resetSecret(IoTDAClient iotdaClient, String deviceId, ResetDeviceSecret body) throws ServiceResponseException {
        ResetDeviceSecretRequest request = new ResetDeviceSecretRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        request.setBody(body);
        ResetDeviceSecretResponse resetDeviceSecret = iotdaClient.resetDeviceSecret(request);
        logger.info("The device secret reset result is:{}", JsonUtils.toJSON(OBJECTMAPPER, resetDeviceSecret));
    }

    /**
     * Query the Details About a Device
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device ID
     */
    private static void queryDeviceDetail(IoTDAClient iotdaClient, String deviceId) throws ServiceResponseException {
        ShowDeviceRequest request = new ShowDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        ShowDeviceResponse deviceResponse = iotdaClient.showDevice(request);
        logger.info("The device details query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, deviceResponse));
    }

    /**
     * Reset a Device Fingerprint
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device ID
     * @param body        Structure for resetting a device fingerprint
     */
    private static void resetFingerprint(IoTDAClient iotdaClient, String deviceId, ResetFingerprint body) throws ServiceResponseException {
        ResetFingerprintRequest request = new ResetFingerprintRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        request.setBody(body);
        ResetFingerprintResponse resetFingerprint = iotdaClient.resetFingerprint(request);
        logger.info("The device fingerprint reset result is:{}", JsonUtils.toJSON(OBJECTMAPPER, resetFingerprint));
    }

    public static void main(String[] args) {
        // Initialize the IoTDA client
        IoTDAClient iotdaClient = initClient();
        // ID of the resource space to which the device belongs. Replace the value with the required one.
        String appId = "9d988b5efdf646f0828724c45e1d794e";

        // ID of the device whose key needs to be reset.
        String deviceId_secret = "";

        // ID of the device whose fingerprint needs to be reset.
        String deviceId_fingerprint = "";

        try {
            logger.info("========1.Querying Device Details========");

            queryDeviceDetail(iotdaClient, deviceId_secret);
            logger.info("========1.Device details query completed========");

            logger.info("========2.Resetting the Device Key========");
            ResetDeviceSecret resetDeviceSecret = new ResetDeviceSecret();
            // Replace it with the key to be set again.
            resetDeviceSecret.setSecret("dc0f****f22f");
            // Whether to disconnect from the device after the key is reset
            resetDeviceSecret.setForceDisconnect(true);

            resetSecret(iotdaClient, deviceId_secret, resetDeviceSecret);
            logger.info("========2.Device key reset completed========");

            logger.info("========3.Querying Device Details========");

            queryDeviceDetail(iotdaClient, deviceId_secret);
            logger.info("========3.Device details query completed========");

            logger.info("========4.Querying Device Details========");

            queryDeviceDetail(iotdaClient, deviceId_fingerprint);
            logger.info("========4.Device details query completed========");

            logger.info("========5.Reset Device Fingerprint========");
            ResetFingerprint resetFingerprint = new ResetFingerprint();
            resetFingerprint.setFingerprint("dc0f****f22f");
            // Whether to disconnect from the device after the key is reset
            resetFingerprint.setForceDisconnect(true);

            resetFingerprint(iotdaClient, deviceId_fingerprint, resetFingerprint);
            logger.info("========5.Device fingerprint reset completed========");

            logger.info("========6.Querying Device Details========");

            queryDeviceDetail(iotdaClient, deviceId_fingerprint);
            logger.info("========6.Device details query completed========");
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.warn("Demonstration failed. Exception information is {}", e.getMessage());
        } catch (ServiceResponseException e) {
            logger.warn("Demonstration failed. Exception information is {}, {}", e.getErrorCode(), e.getErrorMsg());
        }
    }
}
