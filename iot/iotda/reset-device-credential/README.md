## 1、功能介绍

华为云提供了IoTDA服务端SDK，您可以直接集成服务端SDK来调用IoTDA的相关API，从而实现对IoTDA的快速操作。
该示例展示了如何通过java版SDK来实现对设备认证凭证的重置，可在您忘记或是需要重新更新认证凭证时使用。

重置设备秘钥：秘钥认证设备重置设备凭证方式，将设备秘钥进行重置。

重置设备指纹：证书认证设备重置凭证方式，将设备的证书指纹重置。

**您将学到什么？**

如何通过java版SDK来管理设备的凭证，您将学习到如何使用SDK去重置设备秘钥和重置设备指纹。

## 2、前置条件
- 1、已经开通华为云账号，并获得到您账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。您可以参考[获取项目ID](https://support.huaweicloud.com/api-iothub/iot_06_v5_1001.html)
- 4、已经开通IoTDA服务，获取当前实例的实例ID，请参考[查看实例详情](https://support.huaweicloud.com/usermanual-iothub/iot_01_0121.html) 。 
- 5、并且存在两个设备，其中证书认证和指纹认证设备各一个。

## 3、SDK获取和安装
您需要下载并安装 Maven，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4、接口参数说明
关于接口参数的详细说明可参见：

[重置设备密钥](https://support.huaweicloud.com/api-iothub/iot_06_v5_0093.html)

[重置设备指纹](https://support.huaweicloud.com/api-iothub/iot_06_v5_0196.html)

[查询设备](https://support.huaweicloud.com/api-iothub/iot_06_v5_0055.html)

## 5、关键代码片段
### 5.1、重置设备秘钥

```java
    /**
     * 重置设备秘钥
     *
     * @param iotdaClient iotda client
     * @param deviceId    设备ID
     * @param body        重置设备秘钥结构体
     */
    private static void resetSecret(IoTDAClient iotdaClient, String deviceId, ResetDeviceSecret body) throws ServiceResponseException {
        ResetDeviceSecretRequest request = new ResetDeviceSecretRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        request.setBody(body);
        ResetDeviceSecretResponse resetDeviceSecret = iotdaClient.resetDeviceSecret(request);
        logger.info("重置设备秘钥的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, resetDeviceSecret));
    }
```

### 5.2、查询设备详情
```java
    /**
     * 查询设备详情
     *
     * @param iotdaClient iotda client
     * @param deviceId    设备ID
     */
    private static void queryDeviceDetail(IoTDAClient iotdaClient, String deviceId) throws ServiceResponseException {
        ShowDeviceRequest request = new ShowDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        ShowDeviceResponse deviceResponse = iotdaClient.showDevice(request);
        logger.info("查询设备详情的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, deviceResponse));
    }
```

### 5.3、重置设备指纹

```java
    /**
     * 重置设备指纹
     *
     * @param iotdaClient iotda client
     * @param deviceId    设备ID
     * @param body        重置设备指纹结构体
     */
    private static void resetFingerprint(IoTDAClient iotdaClient, String deviceId, ResetFingerprint body) throws ServiceResponseException {
        ResetFingerprintRequest request = new ResetFingerprintRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        request.setBody(body);
        ResetFingerprintResponse resetFingerprint = iotdaClient.resetFingerprint(request);
        logger.info("重置设备指纹的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, resetFingerprint));
    }
```

## 6、运行结果
如果您替换main方法的参数，并按照main方法的顺序执行，程序将演示将秘钥设备和指纹设备的认证凭证进行重置。
以下为main方法中每一个步骤的运行结果。

### 6.1 查询设备详情(重置秘钥前)
```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "teatApp",
  "device_id": "64111bcd06ca5933b28a058b_7765dsfdsfsd",
  "node_id": "7765dsfdsfsd",
  "gateway_id": "64111bcd06ca5933b28a058b_7765dsfdsfsd",
  "node_type": "GATEWAY",
  "auth_info": {
    "auth_type": "SECRET",
    "secret": "728f****8f8d",
    "secure_access": true,
    "timeout": 0
  },
  "product_id": "64111bcd06ca5933b28a058b",
  "product_name": "testProduct",
  "status": "INACTIVE",
  "create_time": "20230516T034341Z",
  "tags": [
    {
      "tag_key": "testTagName",
      "tag_value": "testTagValue"
    }
  ]
}
```
### 6.2 重置秘钥
```json
{
  "device_id": "64111bcd06ca5933b28a058b_7765dsfdsfsd",
  "secret": "dc0f****ae2f"
}
```
### 6.3 查询设备(重置秘钥后)
```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "teatApp",
  "device_id": "64111bcd06ca5933b28a058b_7765dsfdsfsd",
  "node_id": "7765dsfdsfsd",
  "gateway_id": "64111bcd06ca5933b28a058b_7765dsfdsfsd",
  "node_type": "GATEWAY",
  "auth_info": {
    "auth_type": "SECRET",
    "secret": "dc0f****df2f",
    "secure_access": true,
    "timeout": 0
  },
  "product_id": "64111bcd06ca5933b28a058b",
  "product_name": "testProduct",
  "status": "INACTIVE",
  "create_time": "20230516T034341Z",
  "tags": [
    {
      "tag_key": "testTagName",
      "tag_value": "testTagValue"
    }
  ]
}
```
### 6.4 查询设备详情(重置设备指纹前)
```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "teatApp",
  "device_id": "64111bcd06ca5933b28a058b_SASDASDAS",
  "node_id": "SASDASDAS",
  "gateway_id": "64111bcd06ca5933b28a058b_SASDASDAS",
  "node_type": "GATEWAY",
  "auth_info": {
    "auth_type": "CERTIFICATES",
    "fingerprint": "deaf****ca13",
    "secure_access": false,
    "timeout": 0
  },
  "product_id": "64111bcd06ca5933b28a058b",
  "product_name": "testProduct",
  "status": "INACTIVE",
  "create_time": "20230828T120150Z",
  "tags": []
}
```
### 6.5 重置设备指纹

```json
{
  "device_id": "64111bcd06ca5933b28a058b_SASDASDAS",
  "fingerprint": "aef1****de2f"
}
```
### 6.6 查询设备详情(重置设备指纹后)
```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "teatApp",
  "device_id": "64111bcd06ca5933b28a058b_SASDASDAS",
  "node_id": "SASDASDAS",
  "gateway_id": "64111bcd06ca5933b28a058b_SASDASDAS",
  "node_type": "GATEWAY",
  "auth_info": {
    "auth_type": "CERTIFICATES",
    "fingerprint": "aef1****de2f",
    "secure_access": false,
    "timeout": 0
  },
  "product_id": "64111bcd06ca5933b28a058b",
  "product_name": "testProduct",
  "status": "INACTIVE",
  "create_time": "20230828T120150Z",
  "tags": []
}
```