## 1、功能介绍

华为云提供了IoTDA服务端SDK，您可以直接集成服务端SDK来调用IoTDA的相关API，从而实现对IoTDA的快速操作。
该示例展示了如何通过java版SDK来实现对设备的新增，修改，删除，查询，列表查询功能，同时通过demo进行简单的演示操作。

设备：归属于某个产品下的设备实体，每个设备具有一个唯一的标识码。设备可以是直连物联网平台的设备，也可以是代理子设备连接物联网平台的网关。您可以在物联网平台注册您的实体设备，
通过平台分配的设备ID和密钥，在集成了SDK后，您的设备可以接入到物联网平台，实现与平台的通信及交互。
详情可参考[注册单个设备](https://support.huaweicloud.com/usermanual-iothub/iot_01_0031.html)

**您将学到什么？**

如何通过java版SDK来实现对设备的新增，修改，删除，查询，列表查询功能。

## 2、前置条件
- 1、已经开通华为云账号，并获得到您账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。您可以参考[获取项目ID](https://support.huaweicloud.com/api-iothub/iot_06_v5_1001.html)
- 4、已经开通IoTDA服务，获取当前实例的实例ID，请参考[查看实例详情](https://support.huaweicloud.com/usermanual-iothub/iot_01_0121.html) 。
- 5、在平台已经注册产品，并获取到产品ID。

## 3、SDK获取和安装
您需要下载并安装 Maven，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4、接口参数说明
关于接口参数的详细说明可参见：

[创建设备](https://support.huaweicloud.com/api-iothub/iot_06_v5_0046.html)

[查询设备列表](https://support.huaweicloud.com/api-iothub/iot_06_v5_0048.html)

[查询设备](https://support.huaweicloud.com/api-iothub/iot_06_v5_0055.html)

[修改设备](https://support.huaweicloud.com/api-iothub/iot_06_v5_1079.html)

[删除设备](https://support.huaweicloud.com/api-iothub/iot_06_v5_0041.html)

[上传批量任务文件](https://support.huaweicloud.com/api-iothub/iot_06_v5_0021.html)

[创建批量任务](https://support.huaweicloud.com/api-iothub/iot_06_v5_0045.html)

[查询批量任务](https://support.huaweicloud.com/api-iothub/iot_06_v5_0017.html)

## 5、关键代码片段
### 5.1、创建设备

```java
    /**
     * 创建设备
     *
     * @param iotdaClient iotda client
     * @param body        创建设备结构体
     * @return 设备ID
     */
    private static String createDevice(IoTDAClient iotdaClient, AddDevice body) throws ServiceResponseException {
        AddDeviceRequest request = new AddDeviceRequest();
        request.setBody(body);
        AddDeviceResponse device = iotdaClient.addDevice(request);
        logger.info("创建设备的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, device));
        return device.getDeviceId();
    }
```

### 5.2、查询设备列表

```java
    /**
     * 查询设备列表，此处为查询产品下设备列表
     *
     * @param iotdaClient iotda client
     * @param appId       资源空间id
     * @param productId   产品id
     * @return 设备列表
     */
    private static void queryDeviceList(IoTDAClient iotdaClient, String appId, String productId) throws ServiceResponseException {
        ListDevicesRequest request = new ListDevicesRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        request.setProductId(productId);
        ListDevicesResponse response = iotdaClient.listDevices(request);
        logger.info("查询设备列表的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.3、查询设备详情

```java
    /**
     * 查询设备详情
     *
     * @param iotdaClient iotda client
     * @param appId       资源空间id
     * @param deviceId    设备id
     */
    private static void queryDeviceDetail(IoTDAClient iotdaClient, String appId, String deviceId) throws ServiceResponseException {
        ShowDeviceRequest request = new ShowDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        ShowDeviceResponse deviceResponse = iotdaClient.showDevice(request);
        logger.info("查询设备详情的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, deviceResponse));
    }
```

### 5.4、更新设备

```java
    /**
     * 更新设备信息
     *
     * @param iotdaClient iotda client
     * @param deviceId    设备id
     * @param body        更新设备结构体
     */
    private static void updateDevice(IoTDAClient iotdaClient, String deviceId, UpdateDevice body) throws ServiceResponseException {
        UpdateDeviceRequest request = new UpdateDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        request.setDeviceId(deviceId);
        UpdateDeviceResponse updateDevice = iotdaClient.updateDevice(request);
        logger.info("更新设备的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, updateDevice));
    }
```

### 5.5、删除设备
```java
    /**
     * 删除设备
     *
     * @param iotdaClient iotda client
     * @param deviceId    设备id
     */
    private static void deleteDevice(IoTDAClient iotdaClient, String deviceId) throws ServiceResponseException {
        DeleteDeviceRequest request = new DeleteDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        DeleteDeviceResponse response = iotdaClient.deleteDevice(request);
        logger.info("删除设备的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.6、上传批量任务文件
```java
    /**
     * 上传批量任务文件
     *
     * @param iotdaClient iotda client
     * @param path        批量文件路径，包含文件名
     * @param fileName    文件名
     * @return 文件id
     */
    private static String uploadFile(IoTDAClient iotdaClient, String path, String fileName) throws ServiceResponseException, FileNotFoundException {
        UploadBatchTaskFileRequest request = new UploadBatchTaskFileRequest();
        request.setInstanceId(INSTANCE_ID);
        UploadBatchTaskFileRequestBody body = new UploadBatchTaskFileRequestBody();
        body.withFile(new FileInputStream(path), fileName);
        request.setBody(body);
        UploadBatchTaskFileResponse taskFile = iotdaClient.uploadBatchTaskFile(request);
        logger.info("上传任务文件的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, taskFile));
        return taskFile.getFileId();
    }
```

### 5.7、创建批量任务
```java
    /**
     * 创建批量任务
     *
     * @param iotdaClient     iotda client
     * @param createBatchTask 创建批量任务结构体
     * @return 创建完成后获取的任务ID
     */
    private static CreateBatchTaskResponse createTask(IoTDAClient iotdaClient, CreateBatchTask createBatchTask) throws ServiceResponseException {
        CreateBatchTaskRequest request = new CreateBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(createBatchTask);
        CreateBatchTaskResponse batchTask = iotdaClient.createBatchTask(request);
        logger.info("创建批量任务的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, batchTask));
        return batchTask;
    }
```

### 5.8、查询批量任务详情
```java
    /**
     * 查询批量任务详情
     *
     * @param iotdaClient iotda client
     * @param taskId      任务ID
     * @return 返回任务整体情况
     */
    private static Task queryTask(IoTDAClient iotdaClient, String taskId) throws ServiceResponseException {
        ShowBatchTaskRequest request = new ShowBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setTaskId(taskId);
        ShowBatchTaskResponse showBatchTaskResponse = iotdaClient.showBatchTask(request);
        logger.info("查询任务详情的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, showBatchTaskResponse));
        return showBatchTaskResponse.getBatchtask();
    }
```

## 6、运行结果
如果您替换main方法的参数，并按照main方法的顺序执行，程序将演示设备的新增，查询，修改，删除功能，
同时使用批量文件的进行批量注册设备，以下为main方法中每一个步骤的运行结果。

### 6.1 查询设备列表(创建设备前)

由于还未创建设备，查出来为空
```json
{
  "devices": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```
### 6.2 创建设备
```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "testapp",
  "device_id": "64ebfadbde6c2e298319186c_testDevice",
  "node_id": "testDevice",
  "gateway_id": "64ebfadbde6c2e298319186c_testDevice",
  "device_name": "测试设备-0",
  "node_type": "GATEWAY",
  "description": "这是一个设备样例",
  "auth_info": {
    "auth_type": "SECRET",
    "secret": "bb06****4aad",
    "secure_access": false,
    "timeout": 0
  },
  "product_id": "64ebfadbde6c2e298319186c",
  "product_name": "testProduct",
  "status": "INACTIVE",
  "create_time": "20230901T070142Z",
  "tags": []
}
```

### 6.3 查询设备列表(创建设备后)
```json
{
  "devices": [
    {
      "app_id": "9d988b5efdf646f0828724c45e1d794e",
      "app_name": "testapp",
      "device_id": "64ebfadbde6c2e298319186c_testDevice",
      "node_id": "testDevice",
      "gateway_id": "64ebfadbde6c2e298319186c_testDevice",
      "device_name": "测试设备-0",
      "node_type": "GATEWAY",
      "description": "这是一个设备样例",
      "product_id": "64ebfadbde6c2e298319186c",
      "product_name": "testProduct",
      "status": "INACTIVE",
      "tags": []
    }
  ],
  "page": {
    "count": 1,
    "marker": "64f18c56a40b153a4338578c"
  }
}
```
### 6.4 查询设备详情
```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "testapp",
  "device_id": "64ebfadbde6c2e298319186c_testDevice",
  "node_id": "testDevice",
  "gateway_id": "64ebfadbde6c2e298319186c_testDevice",
  "device_name": "测试设备-0",
  "node_type": "GATEWAY",
  "description": "这是一个设备样例",
  "auth_info": {
    "auth_type": "SECRET",
    "secret": "bb06****4aad",
    "secure_access": false,
    "timeout": 0
  },
  "product_id": "64ebfadbde6c2e298319186c",
  "product_name": "testProduct",
  "status": "INACTIVE",
  "create_time": "20230901T070142Z",
  "tags": []
}
```
### 6.5 更新设备

```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "testapp",
  "device_id": "64ebfadbde6c2e298319186c_testDevice",
  "node_id": "testDevice",
  "gateway_id": "64ebfadbde6c2e298319186c_testDevice",
  "device_name": "测试设备-1",
  "node_type": "GATEWAY",
  "description": "这是一个设备样例",
  "auth_info": {
    "auth_type": "SECRET",
    "secret": "bb06****4aad",
    "secure_access": false,
    "timeout": 0
  },
  "product_id": "64ebfadbde6c2e298319186c",
  "product_name": "testProduct",
  "status": "INACTIVE",
  "create_time": "20230901T070142Z",
  "tags": []
}
```

### 6.6 删除设备
```json
{}
```
### 6.7 查询设备列表(删除设备后）
```json
{
  "devices": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```

### 6.8 批量注册设备

上传批量文件
```json
{
  "file_id": "aa57a0c9-8468-4c7f-83ab-d34a73fed573",
  "file_name": "addDeviceFile.xlsx",
  "upload_time": "20230901T070144Z"
}
```
创建批量注册设备任务
```json
{
  "task_id": "64f18c581a98d00c657d287b",
  "task_name": "createDevices",
  "task_type": "createDevices",
  "targets_filter": {},
  "task_policy": {},
  "status": "Initializing",
  "task_progress": {
    "total": 0,
    "processing": 0,
    "success": 0,
    "fail": 0,
    "waitting": 0,
    "fail_wait_retry": 0,
    "stopped": 0,
    "removed": 0
  },
  "create_time": "20230901T070144Z"
}
```

### 6.9 查询批量任务详情
```json
{
  "batchtask": {
    "task_id": "64f18c581a98d00c657d287b",
    "task_name": "createDevices",
    "task_type": "createDevices",
    "targets_filter": {},
    "task_policy": {},
    "status": "Processing",
    "status_desc": "",
    "task_progress": {
      "total": 4,
      "processing": 0,
      "success": 4,
      "fail": 0,
      "waitting": 0,
      "fail_wait_retry": 0,
      "stopped": 0,
      "removed": 0
    },
    "create_time": "20230901T070144Z"
  },
  "task_details": [
    {
      "target": "product_id=64ebfadbde6c2e298319186c, node_id=TEST_10004",
      "status": "Success",
      "output": "deviceId=64ebfadbde6c2e298319186c_TEST_10004, appId=9d988b5efdf646f0828724c45e1d794e, secret=0535****542b, fingerprint=null",
      "error": {}
    },
    {
      "target": "product_id=64ebfadbde6c2e298319186c, node_id=TEST_10003",
      "status": "Success",
      "output": "deviceId=64ebfadbde6c2e298319186c_TEST_10003, appId=9d988b5efdf646f0828724c45e1d794e, secret=a1c9****f21a, fingerprint=null",
      "error": {}
    },
    {
      "target": "product_id=64ebfadbde6c2e298319186c, node_id=TEST_10002",
      "status": "Success",
      "output": "deviceId=64ebfadbde6c2e298319186c_TEST_10002, appId=9d988b5efdf646f0828724c45e1d794e, secret=f84d****04952, fingerprint=null",
      "error": {}
    },
    {
      "target": "product_id=64ebfadbde6c2e298319186c, node_id=TEST_10001",
      "status": "Success",
      "output": "deviceId=64ebfadbde6c2e298319186c_TEST_10001, appId=9d988b5efdf646f0828724c45e1d794e, secret=3282****1091, fingerprint=null",
      "error": {}
    }
  ],
  "page": {
    "count": 4,
    "marker": "64f18c581a98d00c657d287d"
  }
}
```

### 6.10 查询设备列表(批量注册设备后）
```json
{
  "devices": [
    {
      "app_id": "9d988b5efdf646f0828724c45e1d794e",
      "app_name": "testapp",
      "device_id": "64ebfadbde6c2e298319186c_TEST_10002",
      "node_id": "TEST_10002",
      "gateway_id": "64ebfadbde6c2e298319186c_TEST_10002",
      "node_type": "GATEWAY",
      "product_id": "64ebfadbde6c2e298319186c",
      "product_name": "testProduct",
      "status": "INACTIVE",
      "tags": []
    },
    {
      "app_id": "9d988b5efdf646f0828724c45e1d794e",
      "app_name": "testapp",
      "device_id": "64ebfadbde6c2e298319186c_TEST_10004",
      "node_id": "TEST_10004",
      "gateway_id": "64ebfadbde6c2e298319186c_TEST_10004",
      "node_type": "GATEWAY",
      "product_id": "64ebfadbde6c2e298319186c",
      "product_name": "testProduct",
      "status": "INACTIVE",
      "tags": []
    },
    {
      "app_id": "9d988b5efdf646f0828724c45e1d794e",
      "app_name": "testapp",
      "device_id": "64ebfadbde6c2e298319186c_TEST_10001",
      "node_id": "TEST_10001",
      "gateway_id": "64ebfadbde6c2e298319186c_TEST_10001",
      "node_type": "GATEWAY",
      "product_id": "64ebfadbde6c2e298319186c",
      "product_name": "testProduct",
      "status": "INACTIVE",
      "tags": []
    },
    {
      "app_id": "9d988b5efdf646f0828724c45e1d794e",
      "app_name": "testapp",
      "device_id": "64ebfadbde6c2e298319186c_TEST_10003",
      "node_id": "TEST_10003",
      "gateway_id": "64ebfadbde6c2e298319186c_TEST_10003",
      "node_type": "GATEWAY",
      "product_id": "64ebfadbde6c2e298319186c",
      "product_name": "testProduct",
      "status": "INACTIVE",
      "tags": []
    }
  ],
  "page": {
    "count": 4,
    "marker": "64f18c5e0c635a0ce62f19d0"
  }
}
```

### 6.11 创建批量删除设备任务
```json
{
  "task_id": "64f18c628c2b6271ddeb0874",
  "task_name": "deleteDevices",
  "task_type": "deleteDevices",
  "targets": [
    "64ebfadbde6c2e298319186c_TEST_10002",
    "64ebfadbde6c2e298319186c_TEST_10004",
    "64ebfadbde6c2e298319186c_TEST_10001",
    "64ebfadbde6c2e298319186c_TEST_10003"
  ],
  "targets_filter": {},
  "task_policy": {},
  "status": "Initializing",
  "task_progress": {
    "total": 0,
    "processing": 0,
    "success": 0,
    "fail": 0,
    "waitting": 0,
    "fail_wait_retry": 0,
    "stopped": 0,
    "removed": 0
  },
  "create_time": "20230901T070154Z"
}
```

### 6.12 查询任务详情
```json
{
  "batchtask": {
    "task_id": "64f18c628c2b6271ddeb0874",
    "task_name": "deleteDevices",
    "task_type": "deleteDevices",
    "targets": [
      "64ebfadbde6c2e298319186c_TEST_10002",
      "64ebfadbde6c2e298319186c_TEST_10004",
      "64ebfadbde6c2e298319186c_TEST_10001",
      "64ebfadbde6c2e298319186c_TEST_10003"
    ],
    "targets_filter": {},
    "task_policy": {},
    "status": "Processing",
    "status_desc": "",
    "task_progress": {
      "total": 4,
      "processing": 0,
      "success": 4,
      "fail": 0,
      "waitting": 0,
      "fail_wait_retry": 0,
      "stopped": 0,
      "removed": 0
    },
    "create_time": "20230901T070154Z"
  },
  "task_details": [
    {
      "target": "64ebfadbde6c2e298319186c_TEST_10003",
      "status": "Success",
      "error": {}
    },
    {
      "target": "64ebfadbde6c2e298319186c_TEST_10001",
      "status": "Success",
      "error": {}
    },
    {
      "target": "64ebfadbde6c2e298319186c_TEST_10004",
      "status": "Success",
      "error": {}
    },
    {
      "target": "64ebfadbde6c2e298319186c_TEST_10002",
      "status": "Success",
      "error": {}
    }
  ],
  "page": {
    "count": 4,
    "marker": "64f18c628c2b6271ddeb0876"
  }
}
```

### 6.13 查询设备列表(批量删除设备后)
```json1
{
  "devices": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```