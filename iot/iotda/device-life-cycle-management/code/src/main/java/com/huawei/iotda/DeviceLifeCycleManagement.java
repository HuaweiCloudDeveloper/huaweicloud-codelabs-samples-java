package com.huawei.iotda;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.JsonUtils;
import com.huaweicloud.sdk.iotda.v5.IoTDAClient;
import com.huaweicloud.sdk.iotda.v5.model.AddDevice;
import com.huaweicloud.sdk.iotda.v5.model.AddDeviceRequest;
import com.huaweicloud.sdk.iotda.v5.model.AddDeviceResponse;
import com.huaweicloud.sdk.iotda.v5.model.CreateBatchTask;
import com.huaweicloud.sdk.iotda.v5.model.CreateBatchTaskRequest;
import com.huaweicloud.sdk.iotda.v5.model.CreateBatchTaskResponse;
import com.huaweicloud.sdk.iotda.v5.model.DeleteDeviceRequest;
import com.huaweicloud.sdk.iotda.v5.model.DeleteDeviceResponse;
import com.huaweicloud.sdk.iotda.v5.model.ListDevicesRequest;
import com.huaweicloud.sdk.iotda.v5.model.ListDevicesResponse;
import com.huaweicloud.sdk.iotda.v5.model.ShowBatchTaskRequest;
import com.huaweicloud.sdk.iotda.v5.model.ShowBatchTaskResponse;
import com.huaweicloud.sdk.iotda.v5.model.ShowDeviceRequest;
import com.huaweicloud.sdk.iotda.v5.model.ShowDeviceResponse;
import com.huaweicloud.sdk.iotda.v5.model.Task;
import com.huaweicloud.sdk.iotda.v5.model.UpdateDevice;
import com.huaweicloud.sdk.iotda.v5.model.UpdateDeviceRequest;
import com.huaweicloud.sdk.iotda.v5.model.UpdateDeviceResponse;
import com.huaweicloud.sdk.iotda.v5.model.UploadBatchTaskFileRequest;
import com.huaweicloud.sdk.iotda.v5.model.UploadBatchTaskFileRequestBody;
import com.huaweicloud.sdk.iotda.v5.model.UploadBatchTaskFileResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class DeviceLifeCycleManagement {
    private static final Logger logger = LoggerFactory.getLogger(DeviceLifeCycleManagement.class);
    private static final ObjectMapper OBJECTMAPPER = new ObjectMapper();

    // INSTANCE_ID
    private static final String INSTANCE_ID = "<YOUR INSTANCEID>";

    // REGION_ID：If your region is 上海一，please fill in "cn-east-3"；
    // If your region is 北京四，please fill in "cn-north-4", If your region is 华南广州，please fill in "cn-south-4"
    private static final String REGION_ID = "<YOUR REGION ID>";

    private static final String PROJECT_ID = "<YOUR PROJECTID>";

    // ENDPOINT: On the console, choose **Overview** in the navigation pane and
    // click **Access Addresses** to view the HTTPS application access address.
    private static final String ENDPOINT = "<YOUR ENDPOINT>";

    // ak/sk：For details, see the method of obtaining AK/SK in iotda document https://support.huaweicloud.com/api-iothub/iot_06_v5_0091.html.
    // There will be security risks if the AK/SK used for authentication is directly written into code. We suggest encrypting the AK/SK in the configuration file or environment variables for storage.
    // In this sample, the AK/SK is stored in environment variables for identity authentication. Before running this sample, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    private static IoTDAClient initClient() {
        // Create a credential
        ICredential auth = new BasicCredentials()
            .withAk(AK)
            .withSk(SK)
            .withProjectId(PROJECT_ID)
            // Derivative algorithms are used for the standard and enterprise editions.
            // For the basic edition, delete the configuration "withDerivedPredicate".
            .withDerivedPredicate(BasicCredentials.DEFAULT_DERIVED_PREDICATE);

        // Create and initialize an IoTDAClient instance
        return IoTDAClient.newBuilder()
            // Use IoTDARegion for basic editions like "withRegion(IoTDARegion.CN_NORTH_4)".
            // Create regions for standard/enterprise editions.
            .withRegion(new Region(REGION_ID, ENDPOINT))
            // .withRegion(IoTDARegion.CN_NORTH_4)
            .withCredential(auth)
            .build();
    }

    /**
     * Create a Device
     *
     * @param iotdaClient iotda client
     * @param body        Structure for device creation
     * @return Device ID
     */
    private static String createDevice(IoTDAClient iotdaClient, AddDevice body) throws ServiceResponseException {
        AddDeviceRequest request = new AddDeviceRequest();
        request.setBody(body);
        AddDeviceResponse device = iotdaClient.addDevice(request);
        logger.info("The device creation result is:{}", JsonUtils.toJSON(OBJECTMAPPER, device));
        return device.getDeviceId();
    }

    /**
     * Query the Device List. Here queries the device list under a product.
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space id
     * @param productId   Product id
     * @return Device list
     */
    private static void queryDeviceList(IoTDAClient iotdaClient, String appId, String productId) throws ServiceResponseException {
        ListDevicesRequest request = new ListDevicesRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        request.setProductId(productId);
        ListDevicesResponse response = iotdaClient.listDevices(request);
        logger.info("The device list query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    /**
     * Query Details About a Device
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space id
     * @param deviceId    Device id
     */
    private static void queryDeviceDetail(IoTDAClient iotdaClient, String appId, String deviceId) throws ServiceResponseException {
        ShowDeviceRequest request = new ShowDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        ShowDeviceResponse deviceResponse = iotdaClient.showDevice(request);
        logger.info("The device details query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, deviceResponse));
    }

    /**
     * Update a Device
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device id
     * @param body        Structure for device update
     */
    private static void updateDevice(IoTDAClient iotdaClient, String deviceId, UpdateDevice body) throws ServiceResponseException {
        UpdateDeviceRequest request = new UpdateDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        request.setDeviceId(deviceId);
        UpdateDeviceResponse updateDevice = iotdaClient.updateDevice(request);
        logger.info("The device update result is:{}", JsonUtils.toJSON(OBJECTMAPPER, updateDevice));
    }

    /**
     * Delete a Device
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device id
     */
    private static void deleteDevice(IoTDAClient iotdaClient, String deviceId) throws ServiceResponseException {
        DeleteDeviceRequest request = new DeleteDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        DeleteDeviceResponse response = iotdaClient.deleteDevice(request);
        logger.info("The device deletion result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    /**
     * Create a Batch Task
     *
     * @param iotdaClient     iotda client
     * @param createBatchTask Structure for creating a batch task
     * @return The task ID obtained after the batch task is created.
     */
    private static CreateBatchTaskResponse createTask(IoTDAClient iotdaClient, CreateBatchTask createBatchTask) throws ServiceResponseException {
        CreateBatchTaskRequest request = new CreateBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(createBatchTask);
        CreateBatchTaskResponse batchTask = iotdaClient.createBatchTask(request);
        logger.info("The batch task creation result is:{}", JsonUtils.toJSON(OBJECTMAPPER, batchTask));
        return batchTask;
    }

    /**
     * Query Details About a Batch Task
     *
     * @param iotdaClient iotda client
     * @param taskId      Task ID
     * @return Details about the task
     */
    private static Task queryTask(IoTDAClient iotdaClient, String taskId) throws ServiceResponseException {
        ShowBatchTaskRequest request = new ShowBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setTaskId(taskId);
        ShowBatchTaskResponse showBatchTaskResponse = iotdaClient.showBatchTask(request);
        logger.info("The batch task details query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, showBatchTaskResponse));
        return showBatchTaskResponse.getBatchtask();
    }

    /**
     * Upload a Batch Task File
     *
     * @param iotdaClient iotda client
     * @param path        Batch file path, including the file name
     * @param fileName    File name
     * @return File id
     */
    private static String uploadFile(IoTDAClient iotdaClient, String path, String fileName) throws ServiceResponseException, FileNotFoundException {
        UploadBatchTaskFileRequest request = new UploadBatchTaskFileRequest();
        request.setInstanceId(INSTANCE_ID);
        UploadBatchTaskFileRequestBody body = new UploadBatchTaskFileRequestBody();
        body.withFile(new FileInputStream(path), fileName);
        request.setBody(body);
        UploadBatchTaskFileResponse taskFile = iotdaClient.uploadBatchTaskFile(request);
        logger.info("The batch file upload result is:{}", JsonUtils.toJSON(OBJECTMAPPER, taskFile));
        return taskFile.getFileId();
    }

    public static void main(String[] args) {
        // Initialize the IoTDA client
        IoTDAClient iotdaClient = initClient();
        // ID of the resource space to which the device belongs. Replace the value with the required one.
        String appId = "9d988b5efdf646f0828724c45e1d794e";

        // Replace this parameter with the ID of the product you have created.
        String productId = "64ebfadbde6c2e298319186c";

        try {
            logger.info("========1.Querying the Device List========");
            queryDeviceList(iotdaClient, appId, productId);
            logger.info("========1.Querying the device list is complete========");

            logger.info("========2.Creating a Device========");
            // Assemble equipment data, you can assemble according to the actual situation of your equipment
            AddDevice addDevice = new AddDevice();
            addDevice.setAppId(appId);
            addDevice.setProductId(productId);
            addDevice.setNodeId("testDevice");
            addDevice.setDeviceName("test-0");
            addDevice.setDescription("demo");

            String deviceId = createDevice(iotdaClient, addDevice);
            logger.info("========2.Device creation completed========");

            logger.info("========3.Querying the Device List========");
            queryDeviceList(iotdaClient, appId, productId);
            logger.info("========3.Querying the device list is complete========");

            logger.info("========4.Querying Device Details========");
            queryDeviceDetail(iotdaClient, appId, deviceId);
            logger.info("========4.Device details query completed========");

            logger.info("========5.Update Device========");
            // Change the value according to the actual situation. The following describes how to change the device name.
            UpdateDevice updateDevice = new UpdateDevice();
            updateDevice.setDeviceName("test-1");
            updateDevice(iotdaClient, deviceId, updateDevice);
            logger.info("========5.Update Device Complete========");

            logger.info("========6.Deleting a device========");
            deleteDevice(iotdaClient, deviceId);
            logger.info("========6.Device deletion completed========");

            logger.info("========7.Querying the Device List========");
            queryDeviceList(iotdaClient, appId, productId);
            logger.info("========7.Querying the device list is complete========");

            logger.info("========8.Registering Devices in Batches========");
            // If multiple devices need to be registered, use the batch device registration function.
            // Before registering devices in batches, fill in the template (https://iot-developer.obs.cn-north-4.myhuaweicloud.com/IoTM/2023-3/BatchCreateDevices_Template.xlsx) and upload it.
            String fileId = uploadFile(iotdaClient, "File Path", "fileName");
            CreateBatchTask createDeviceBatchTask = new CreateBatchTask();
            createDeviceBatchTask.setAppId(appId);
            // Task name, which is unique in the resource space. If the task is invoked for multiple times, the task name needs to be changed.
            createDeviceBatchTask.setTaskName("createDevices");
            // The task type is fixed to createDevices. Do not change the value.
            createDeviceBatchTask.setTaskType("createDevices");
            // The file ID needs to be entered.
            createDeviceBatchTask.setDocumentSource(fileId);
            // After creating a task, obtain the task ID.
            CreateBatchTaskResponse createTask = createTask(iotdaClient, createDeviceBatchTask);
            logger.info("========8.Batch device registration completed========");

            logger.info("========9.Querying Batch Task Details========");
            String status = createTask.getStatus();
            int times = 0;
            // Wait until the task is complete. The task status is Initializing. Waiting: The task is waiting. Processing: The task is being executed.
            // Success: The operation is successful. Fail: failed. PartialSuccess: partial success. Stopped: stopped. StoppingStopping.
            while (("Initializing".equals(status) || "Waitting".equals(status) || "Processing".equals(status)) && times++ < 5) {
                // Query the batch task execution details to obtain the execution status of the device registration task.
                Task task = queryTask(iotdaClient, createTask.getTaskId());
                status = task.getStatus();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    logger.warn("Exception Information：{}", e.getMessage());
                }
            }
            logger.info("========9.Querying the batch task details is complete========");

            logger.info("========10.Querying the Device List========");
            queryDeviceList(iotdaClient, appId, productId);
            logger.info("========10.Querying the device list is complete========");

            logger.info("========11.Deleting Devices in Batches========");
            CreateBatchTask deleteDevice = new CreateBatchTask();
            deleteDevice.setAppId(appId);
            // Task name, which is unique in the resource space. If the task is invoked for multiple times, the task name needs to be changed.
            deleteDevice.setTaskName("deleteDevices");
            // The task type is fixed to deleteDevices. Do not change the value.
            deleteDevice.setTaskType("deleteDevices");
            // Target deviceId. Add the deviceId to be deleted to the list.
            List<String> targetDeviceIds = new ArrayList<>();
            targetDeviceIds.add("");
            deleteDevice.setTargets(targetDeviceIds);
            // Obtain the task ID after the task is created.
            CreateBatchTaskResponse deleteTask = createTask(iotdaClient, deleteDevice);
            logger.info("========11.Batch device deletion completed========");

            logger.info("========12.Querying Batch Task Details========");
            // Query the execution details of the batch task.
            status = deleteTask.getStatus();
            times = 0;
            // Wait until the task is complete. The task status is Initializing. Waiting: The task is waiting. Processing: The task is being executed.
            // Success: The operation is successful. Fail: failed. PartialSuccess: partial success. Stopped: stopped. StoppingStopping.
            while (("Initializing".equals(status) || "Waitting".equals(status) || "Processing".equals(status)) && times++ < 5) {
                // Query the execution details of the batch task to obtain the execution status of the batch device deletion task.
                Task task = queryTask(iotdaClient, deleteTask.getTaskId());
                status = task.getStatus();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    logger.warn("Exception Information：{}", e.getMessage());
                }
            }
            logger.info("========12.Querying the batch task details is complete========");

            logger.info("========13.Querying the Device List========");
            queryDeviceList(iotdaClient, appId, productId);
            logger.info("========13.Querying the device list is complete========");
        } catch (ConnectionException | RequestTimeoutException | FileNotFoundException e) {
            logger.warn("Demonstration failed. Exception information is {}", e.getMessage());
        } catch (ServiceResponseException e) {
            logger.warn("Demonstration failed. Exception information is {}, {}", e.getErrorCode(), e.getErrorMsg());
        }
    }
}