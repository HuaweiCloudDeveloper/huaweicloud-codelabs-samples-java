## 1. Functions

You can integrate the IoTDA server SDKs provided by Huawei Cloud to call IoTDA APIs and use IoTDA smoothly.
This example shows and demonstrates how to use the Java SDK to add, modify, delete and query a device as well as query the device list. 

Device: A device is a physical entity that belongs to a product. Each device has a unique ID. It can be a device directly connected to the platform, or a gateway that connects child devices to the platform. You can register a physical device with the platform, and use the device ID and secret allocated by the platform to connect your SDK-integrated device to the platform.
For details, see [Registering an Individual Device](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0031.html)

**What You Will Learn**

The ways to use the Java SDK to add, modify, delete and query a device as well as query the device list. 

## 2. Prerequisites
- 1. You have created a Huawei Cloud account and obtained the access key (AK) and secret access key (SK) of your account. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 2. You have set up the development environment with Java JDK 1.8 or later.
- 3. You have obtained the project ID of the target region. View the project ID on the **My Credentials** > **API Credentials** page of the Huawei Cloud console. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_1001.html).
- 4. You have enabled IoTDA. For details about how to obtain the ID of the current instance, see [Viewing Instance Details](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0121.html).
- 5. You have created a device and connected it to the platform for viewing interconnection details.

## 3. Obtaining and Installing the SDK
Download and install Maven, and add dependencies to the **pom.xml** file of the Java project.
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4. API Parameters
For details about API parameters, see:

[Create a Device](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0046.html)

[Query the Device List](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0048.html)

[Query a Device](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0055.html)

[Modify a Device](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_1079.html)

[Delete a Device](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0041.html)

[Upload a Batch Task File](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0021.html)

[Create a Batch Task](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0045.html)

[Query a Batch Task](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0017.html)

## 5. Key Code Snippets
### 5.1 Create a Device

```java
    /**
     * Create a Device
     *
     * @param iotdaClient iotda client
     * @param body        Structure for device creation
     * @return Device ID
     */
    private static String createDevice(IoTDAClient iotdaClient, AddDevice body) throws ServiceResponseException {
        AddDeviceRequest request = new AddDeviceRequest();
        request.setBody(body);
        AddDeviceResponse device = iotdaClient.addDevice(request);
        logger.info("The device creation result is:{}", JsonUtils.toJSON(OBJECTMAPPER, device));
        return device.getDeviceId();
    }
```

### 5.2 Query the Device List

```java
    /**
     * Query the Device List. Here queries the device list under a product.
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space id
     * @param productId   Product id
     * @return Device list
     */
    private static void queryDeviceList(IoTDAClient iotdaClient, String appId, String productId) throws ServiceResponseException {
        ListDevicesRequest request = new ListDevicesRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        request.setProductId(productId);
        ListDevicesResponse response = iotdaClient.listDevices(request);
        logger.info("The device list query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.3 Query Details About a Device

```java
    /**
     * Query Details About a Device
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space id
     * @param deviceId    Device id
     */
    private static void queryDeviceDetail(IoTDAClient iotdaClient, String appId, String deviceId) throws ServiceResponseException {
        ShowDeviceRequest request = new ShowDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        ShowDeviceResponse deviceResponse = iotdaClient.showDevice(request);
        logger.info("The device details query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, deviceResponse));
    }
```

### 5.4 Update a Device

```java
    /**
     * Update a Device
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device id
     * @param body        Structure for device update
     */
    private static void updateDevice(IoTDAClient iotdaClient, String deviceId, UpdateDevice body) throws ServiceResponseException {
        UpdateDeviceRequest request = new UpdateDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        request.setDeviceId(deviceId);
        UpdateDeviceResponse updateDevice = iotdaClient.updateDevice(request);
        logger.info("The device update result is:{}", JsonUtils.toJSON(OBJECTMAPPER, updateDevice));
    }
```

### 5.5 Delete a Device
```java
    /**
     * Delete a Device
     *
     * @param iotdaClient iotda client
     * @param deviceId    Device id
     */
    private static void deleteDevice(IoTDAClient iotdaClient, String deviceId) throws ServiceResponseException {
        DeleteDeviceRequest request = new DeleteDeviceRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setDeviceId(deviceId);
        DeleteDeviceResponse response = iotdaClient.deleteDevice(request);
        logger.info("The device deletion result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.6 Upload a Batch Task File
```java
    /**
     * Upload a Batch Task File
     *
     * @param iotdaClient iotda client
     * @param path        Batch file path, including the file name
     * @param fileName    File name
     * @return File id
     */
    private static String uploadFile(IoTDAClient iotdaClient, String path, String fileName) throws ServiceResponseException, FileNotFoundException {
        UploadBatchTaskFileRequest request = new UploadBatchTaskFileRequest();
        request.setInstanceId(INSTANCE_ID);
        UploadBatchTaskFileRequestBody body = new UploadBatchTaskFileRequestBody();
        body.withFile(new FileInputStream(path), fileName);
        request.setBody(body);
        UploadBatchTaskFileResponse taskFile = iotdaClient.uploadBatchTaskFile(request);
        logger.info("The batch file upload result is:{}", JsonUtils.toJSON(OBJECTMAPPER, taskFile));
        return taskFile.getFileId();
    }
```

### 5.7 Create a Batch Task
```java
    /**
     * Create a Batch Task
     *
     * @param iotdaClient     iotda client
     * @param createBatchTask Structure for creating a batch task
     * @return The task ID obtained after the batch task is created.
     */
    private static CreateBatchTaskResponse createTask(IoTDAClient iotdaClient, CreateBatchTask createBatchTask) throws ServiceResponseException {
        CreateBatchTaskRequest request = new CreateBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(createBatchTask);
        CreateBatchTaskResponse batchTask = iotdaClient.createBatchTask(request);
        logger.info("The batch task creation result is:{}", JsonUtils.toJSON(OBJECTMAPPER, batchTask));
        return batchTask;
    }
```

### 5.8 Query Details About a Batch Task
```java
    /**
     * Query Details About a Batch Task
     *
     * @param iotdaClient iotda client
     * @param taskId      Task ID
     * @return Details about the task
     */
    private static Task queryTask(IoTDAClient iotdaClient, String taskId) throws ServiceResponseException {
        ShowBatchTaskRequest request = new ShowBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setTaskId(taskId);
        ShowBatchTaskResponse showBatchTaskResponse = iotdaClient.showBatchTask(request);
        logger.info("The batch task details query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, showBatchTaskResponse));
        return showBatchTaskResponse.getBatchtask();
    }
```

## 6. Execution Results
Replace the parameters and execute the code in the sequence of the main method. The program demonstrates how to add, query, modify and delete a device, and meanwhile use a batch file to register devices in batches. The execution results of each step of the main method are shown as follows.

### 6.1 Query the Device List(Before the Device Creation)

Because the device is not created yet, the query result is empty.
```json
{
  "devices": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```
### 6.2 Create a Device
```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "testapp",
  "device_id": "64ebfadbde6c2e298319186c_testDevice",
  "node_id": "testDevice",
  "gateway_id": "64ebfadbde6c2e298319186c_testDevice",
  "device_name": "demo-device-0",
  "node_type": "GATEWAY",
  "description": "demo device",
  "auth_info": {
    "auth_type": "SECRET",
    "secret": "bb06****4aad",
    "secure_access": false,
    "timeout": 0
  },
  "product_id": "64ebfadbde6c2e298319186c",
  "product_name": "testProduct",
  "status": "INACTIVE",
  "create_time": "20230901T070142Z",
  "tags": []
}
```

### 6.3 Query the Device List(After the Device Creation)
```json
{
  "devices": [
    {
      "app_id": "9d988b5efdf646f0828724c45e1d794e",
      "app_name": "testapp",
      "device_id": "64ebfadbde6c2e298319186c_testDevice",
      "node_id": "testDevice",
      "gateway_id": "64ebfadbde6c2e298319186c_testDevice",
      "device_name": "demo-device-0",
      "node_type": "GATEWAY",
      "description": "demo device",
      "product_id": "64ebfadbde6c2e298319186c",
      "product_name": "testProduct",
      "status": "INACTIVE",
      "tags": []
    }
  ],
  "page": {
    "count": 1,
    "marker": "64f18c56a40b153a4338578c"
  }
}
```
### 6.4 Query the Device Details
```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "testapp",
  "device_id": "64ebfadbde6c2e298319186c_testDevice",
  "node_id": "testDevice",
  "gateway_id": "64ebfadbde6c2e298319186c_testDevice",
  "device_name": "device-0",
  "node_type": "GATEWAY",
  "description": "This is a sample device",
  "auth_info": {
    "auth_type": "SECRET",
    "secret": "bb06****4aad",
    "secure_access": false,
    "timeout": 0
  },
  "product_id": "64ebfadbde6c2e298319186c",
  "product_name": "testProduct",
  "status": "INACTIVE",
  "create_time": "20230901T070142Z",
  "tags": []
}
```
### 6.5 Update a Device

// Change the device name
```json
{
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "app_name": "testapp",
  "device_id": "64ebfadbde6c2e298319186c_testDevice",
  "node_id": "testDevice",
  "gateway_id": "64ebfadbde6c2e298319186c_testDevice",
  "device_name": "device-1",
  "node_type": "GATEWAY",
  "description": "This is a sample device",
  "auth_info": {
    "auth_type": "SECRET",
    "secret": "bb06****4aad",
    "secure_access": false,
    "timeout": 0
  },
  "product_id": "64ebfadbde6c2e298319186c",
  "product_name": "testProduct",
  "status": "INACTIVE",
  "create_time": "20230901T070142Z",
  "tags": []
}
```

### 6.6 Delete a Device
```json
{}
```
### 6.7 Query the Device List(After the Device Deletion)
```json
{
  "devices": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```

### 6.8 Register Devices in Batches

Upload a batch file
```json
{
  "file_id": "aa57a0c9-8468-4c7f-83ab-d34a73fed573",
  "file_name": "addDeviceFile.xlsx",
  "upload_time": "20230901T070144Z"
}
```
Create a batch device registration task
```json
{
  "task_id": "64f18c581a98d00c657d287b",
  "task_name": "createDevices",
  "task_type": "createDevices",
  "targets_filter": {},
  "task_policy": {},
  "status": "Initializing",
  "task_progress": {
    "total": 0,
    "processing": 0,
    "success": 0,
    "fail": 0,
    "waitting": 0,
    "fail_wait_retry": 0,
    "stopped": 0,
    "removed": 0
  },
  "create_time": "20230901T070144Z"
}
```

### 6.9 Query the Details About a Batch Task
```json
{
  "batchtask": {
    "task_id": "64f18c581a98d00c657d287b",
    "task_name": "createDevices",
    "task_type": "createDevices",
    "targets_filter": {},
    "task_policy": {},
    "status": "Processing",
    "status_desc": "",
    "task_progress": {
      "total": 4,
      "processing": 0,
      "success": 4,
      "fail": 0,
      "waitting": 0,
      "fail_wait_retry": 0,
      "stopped": 0,
      "removed": 0
    },
    "create_time": "20230901T070144Z"
  },
  "task_details": [
    {
      "target": "product_id=64ebfadbde6c2e298319186c, node_id=TEST_10004",
      "status": "Success",
      "output": "deviceId=64ebfadbde6c2e298319186c_TEST_10004, appId=9d988b5efdf646f0828724c45e1d794e, secret=0535****542b, fingerprint=null",
      "error": {}
    },
    {
      "target": "product_id=64ebfadbde6c2e298319186c, node_id=TEST_10003",
      "status": "Success",
      "output": "deviceId=64ebfadbde6c2e298319186c_TEST_10003, appId=9d988b5efdf646f0828724c45e1d794e, secret=a1c9****f21a, fingerprint=null",
      "error": {}
    },
    {
      "target": "product_id=64ebfadbde6c2e298319186c, node_id=TEST_10002",
      "status": "Success",
      "output": "deviceId=64ebfadbde6c2e298319186c_TEST_10002, appId=9d988b5efdf646f0828724c45e1d794e, secret=f84d****4952, fingerprint=null",
      "error": {}
    },
    {
      "target": "product_id=64ebfadbde6c2e298319186c, node_id=TEST_10001",
      "status": "Success",
      "output": "deviceId=64ebfadbde6c2e298319186c_TEST_10001, appId=9d988b5efdf646f0828724c45e1d794e, secret=3282****1091, fingerprint=null",
      "error": {}
    }
  ],
  "page": {
    "count": 4,
    "marker": "64f18c581a98d00c657d287d"
  }
}
```

### 6.10 Query the Device List (After Device Registration in Batches)
```json
{
  "devices": [
    {
      "app_id": "9d988b5efdf646f0828724c45e1d794e",
      "app_name": "testapp",
      "device_id": "64ebfadbde6c2e298319186c_TEST_10002",
      "node_id": "TEST_10002",
      "gateway_id": "64ebfadbde6c2e298319186c_TEST_10002",
      "node_type": "GATEWAY",
      "product_id": "64ebfadbde6c2e298319186c",
      "product_name": "testProduct",
      "status": "INACTIVE",
      "tags": []
    },
    {
      "app_id": "9d988b5efdf646f0828724c45e1d794e",
      "app_name": "testapp",
      "device_id": "64ebfadbde6c2e298319186c_TEST_10004",
      "node_id": "TEST_10004",
      "gateway_id": "64ebfadbde6c2e298319186c_TEST_10004",
      "node_type": "GATEWAY",
      "product_id": "64ebfadbde6c2e298319186c",
      "product_name": "testProduct",
      "status": "INACTIVE",
      "tags": []
    },
    {
      "app_id": "9d988b5efdf646f0828724c45e1d794e",
      "app_name": "testapp",
      "device_id": "64ebfadbde6c2e298319186c_TEST_10001",
      "node_id": "TEST_10001",
      "gateway_id": "64ebfadbde6c2e298319186c_TEST_10001",
      "node_type": "GATEWAY",
      "product_id": "64ebfadbde6c2e298319186c",
      "product_name": "testProduct",
      "status": "INACTIVE",
      "tags": []
    },
    {
      "app_id": "9d988b5efdf646f0828724c45e1d794e",
      "app_name": "testapp",
      "device_id": "64ebfadbde6c2e298319186c_TEST_10003",
      "node_id": "TEST_10003",
      "gateway_id": "64ebfadbde6c2e298319186c_TEST_10003",
      "node_type": "GATEWAY",
      "product_id": "64ebfadbde6c2e298319186c",
      "product_name": "testProduct",
      "status": "INACTIVE",
      "tags": []
    }
  ],
  "page": {
    "count": 4,
    "marker": "64f18c5e0c635a0ce62f19d0"
  }
}
```

### 6.11 Create a Task to Delete Devices in Batches
```json
{
  "task_id": "64f18c628c2b6271ddeb0874",
  "task_name": "deleteDevices",
  "task_type": "deleteDevices",
  "targets": [
    "64ebfadbde6c2e298319186c_TEST_10002",
    "64ebfadbde6c2e298319186c_TEST_10004",
    "64ebfadbde6c2e298319186c_TEST_10001",
    "64ebfadbde6c2e298319186c_TEST_10003"
  ],
  "targets_filter": {},
  "task_policy": {},
  "status": "Initializing",
  "task_progress": {
    "total": 0,
    "processing": 0,
    "success": 0,
    "fail": 0,
    "waitting": 0,
    "fail_wait_retry": 0,
    "stopped": 0,
    "removed": 0
  },
  "create_time": "20230901T070154Z"
}
```

### 6.12 Query the Task Details
```json
{
  "batchtask": {
    "task_id": "64f18c628c2b6271ddeb0874",
    "task_name": "deleteDevices",
    "task_type": "deleteDevices",
    "targets": [
      "64ebfadbde6c2e298319186c_TEST_10002",
      "64ebfadbde6c2e298319186c_TEST_10004",
      "64ebfadbde6c2e298319186c_TEST_10001",
      "64ebfadbde6c2e298319186c_TEST_10003"
    ],
    "targets_filter": {},
    "task_policy": {},
    "status": "Processing",
    "status_desc": "",
    "task_progress": {
      "total": 4,
      "processing": 0,
      "success": 4,
      "fail": 0,
      "waitting": 0,
      "fail_wait_retry": 0,
      "stopped": 0,
      "removed": 0
    },
    "create_time": "20230901T070154Z"
  },
  "task_details": [
    {
      "target": "64ebfadbde6c2e298319186c_TEST_10003",
      "status": "Success",
      "error": {}
    },
    {
      "target": "64ebfadbde6c2e298319186c_TEST_10001",
      "status": "Success",
      "error": {}
    },
    {
      "target": "64ebfadbde6c2e298319186c_TEST_10004",
      "status": "Success",
      "error": {}
    },
    {
      "target": "64ebfadbde6c2e298319186c_TEST_10002",
      "status": "Success",
      "error": {}
    }
  ],
  "page": {
    "count": 4,
    "marker": "64f18c628c2b6271ddeb0876"
  }
}
```

### 6.13 Query the Device List(After the Device Deletion in Batches)
```json
{
  "devices": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```