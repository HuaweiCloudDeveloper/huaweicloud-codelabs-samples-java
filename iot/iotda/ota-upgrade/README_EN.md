## 1. Functions

You can integrate the IoTDA server SDKs provided by Huawei Cloud to call IoTDA APIs and use IoTDA smoothly.
This example shows and demonstrates how to use the Java SDK to update the OTA upgrade packages of devices and designate a batch of devices to upgrade in OTA mode.

OTA Upgrade：A software upgrade refers to the upgrade of the system software and application software of the device. A firmware upgrade refers to the upgrade of the underlying drivers of the device hardware.You can upload a software/firmware upgrade package to the IoTDA platform or use a file associated with an object on OBS for device remote upgrades.

For details, see [FAQs - OTA Upgrades](https://support.huaweicloud.com/intl/en-us/iothub_faq/iot_faq_01001.html) and [Upgrading the MQTT Device Firmware in OTA Mode](https://support.huaweicloud.com/intl/en-us/bestpractice-iothub/iot_bp_0039.html)

**What You Will Learn**

The ways to use the Java SDK to upload OTA upgrade packages and designate a batch of devices to upgrade in OTA mode in the form of a batch task.

## 2. Prerequisites
- 1. You have created a Huawei Cloud account and obtained the access key (AK) and secret access key (SK) of your account. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 2. You have set up the development environment with Java JDK 1.8 or later.
- 3. You have obtained the project ID of the target region. View the project ID on the **My Credentials** > **API Credentials** page of the Huawei Cloud console. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_1001.html).
- 4. You have enabled IoTDA. For details about how to obtain the ID of the current instance, see [Viewing Instance Details](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0121.html).

## 3. Obtaining and Installing the SDK
Download and install Maven, and add dependencies to the **pom.xml** file of the Java project.
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4. API Parameters
For details about API parameters, see:

[Create an OTA Upgrade Package](https://support.huaweicloud.com/intl/en-us/api-iothub/CreateOtaPackage.html)

[Create a Batch Task](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0045.html)

[Query a Batch Task](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0017.html)

## 5. Key Code Snippets
### 5.1 Upload an OTA Upgrade Package
```java
    /**
     * Upload an OTA Upgrade Package
     *
     * @param iotdaClient iotda client
     * @param body        Structure for uploading an OTA upgrade package
     * @return Upgrade package ID
     */
    private static String uploadOTAPackages(IoTDAClient iotdaClient, CreateOtaPackage body) throws ServiceResponseException {
        CreateOtaPackageRequest request = new CreateOtaPackageRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        CreateOtaPackageResponse response = iotdaClient.createOtaPackage(request);
        logger.info("The result of uploading an OTA upgrade package is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        return response.getPackageId();
    }
```

### 5.2 Create a Batch Task
```java
    /**
     * Create a Batch Task
     *
     * @param iotdaClient     iotda client
     * @param createBatchTask Structure for creating a batch task
     * @return The task obtained after the creation
     */
    private static CreateBatchTaskResponse createTask(IoTDAClient iotdaClient, CreateBatchTask createBatchTask) throws ServiceResponseException {
        CreateBatchTaskRequest request = new CreateBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(createBatchTask);
        CreateBatchTaskResponse batchTask = iotdaClient.createBatchTask(request);
        logger.info("The result of creating a batch task is:{}", JsonUtils.toJSON(OBJECTMAPPER, batchTask));
        return batchTask;
    }
```

### 5.3 Query the Details About a Batch Task
```java
    /**
     * Query the Details About a Batch Task
     *
     * @param iotdaClient iotda client
     * @param taskId      Task ID
     * @return The details about the task
     */
    private static Task queryTask(IoTDAClient iotdaClient, String taskId) throws ServiceResponseException {
        ShowBatchTaskRequest request = new ShowBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setTaskId(taskId);
        ShowBatchTaskResponse showBatchTaskResponse = iotdaClient.showBatchTask(request);
        logger.info("The result of querying a batch task is:{}", JsonUtils.toJSON(OBJECTMAPPER, showBatchTaskResponse));
        return showBatchTaskResponse.getBatchtask();
    }
```

## 6. Execution Results
Replace the parameters and execute the code in the sequence of the main method. The program demonstrates how to update the OTA upgrade packages of devices and designate a batch of devices to upgrade in OTA mode.
The execution results of each step of the main method are shown as follows.

### 6.1 Upload an OTA upgrade package

```json
{
  "package_id": "64f6a17e63066026291dfb37",
  "app_id": "dd708a9bcbeb4e55bbd1d348cdb7b94a",
  "version": "1.0",
  "support_source_versions": null,
  "file_name": "2022/08/18/09/mqtt_ota.v1.zip",
  "description": null,
  "custom_info": null,
  "package_type": "softwarePackage",
  "product_id": "64748b4afeb89944a0df75e8",
  "product_name": "test_ota",
  "create_time": "20230905T033318Z",
  "storage_type": "OBS"
}
```
### 6.2 Create a Batch Task of OTA Upgrade

Here we take the software upgrade as an example
```json
{
  "task_id": "64f6a2b88c2b6271ddeb087b",
  "task_name": "softwareUpgradeTask",
  "task_type": "softwareUpgrade",
  "targets": [
    "64111bcd06ca5933b28a058b_7758dsfdsfsd"
  ],
  "targets_filter": {},
  "document": {
    "package_id": "64eebce6bcbe1947d21b9823"
  },
  "task_policy": {},
  "status": "Initializing",
  "task_progress": {
    "total": 0,
    "processing": 0,
    "success": 0,
    "fail": 0,
    "waitting": 0,
    "fail_wait_retry": 0,
    "stopped": 0,
    "removed": 0
  },
  "create_time": "20230905T033832Z"
}
```

### 6.3 Query the Details About an OTA Upgrade Task

The task status is Processing, indicating the task is under execution, and query the software and firmware versions of the device.
```json
{
  "batchtask": {
    "task_id": "64f6a2b88c2b6271ddeb087b",
    "task_name": "softwareUpgradeTask",
    "task_type": "softwareUpgrade",
    "targets": [
      "64111bcd06ca5933b28a058b_7758dsfdsfsd"
    ],
    "targets_filter": {},
    "document": {
      "package_id": "64eebce6bcbe1947d21b9823"
    },
    "task_policy": {},
    "status": "Processing",
    "status_desc": "",
    "task_progress": {
      "total": 1,
      "processing": 1,
      "success": 0,
      "fail": 0,
      "waitting": 0,
      "fail_wait_retry": 0,
      "stopped": 0,
      "removed": 0
    },
    "create_time": "20230905T034348Z"
  },
  "task_details": [
    {
      "target": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
      "status": "Processing",
      "output": "QueryingVersion",
      "error": {}
    }
  ],
  "page": {
    "count": 1,
    "marker": "64f6a3f48c2b6271ddeb0886"
  }
}
```