package com.huawei.iotda;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.JsonUtils;
import com.huaweicloud.sdk.iotda.v5.IoTDAClient;
import com.huaweicloud.sdk.iotda.v5.model.CreateBatchTask;
import com.huaweicloud.sdk.iotda.v5.model.CreateBatchTaskRequest;
import com.huaweicloud.sdk.iotda.v5.model.CreateBatchTaskResponse;
import com.huaweicloud.sdk.iotda.v5.model.CreateOtaPackage;
import com.huaweicloud.sdk.iotda.v5.model.CreateOtaPackageRequest;
import com.huaweicloud.sdk.iotda.v5.model.CreateOtaPackageResponse;
import com.huaweicloud.sdk.iotda.v5.model.FileLocation;
import com.huaweicloud.sdk.iotda.v5.model.ObsLocation;
import com.huaweicloud.sdk.iotda.v5.model.ShowBatchTaskRequest;
import com.huaweicloud.sdk.iotda.v5.model.ShowBatchTaskResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.huaweicloud.sdk.iotda.v5.model.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OtaUpgrade {
    private static final Logger logger = LoggerFactory.getLogger(OtaUpgrade.class);
    private static final ObjectMapper OBJECTMAPPER = new ObjectMapper();

    private static final String PROJECT_ID = "<YOUR PROJECTID>";

    // INSTANCE_ID
    private static final String INSTANCE_ID = "<YOUR INSTANCEID>";


    // REGION_ID：If your region is 上海一，please fill in "cn-east-3"；If your region is 北京四，please fill in "cn-north-4"; If your region is 华南广州，please fill in "cn-south-4"
    private static final String REGION_ID = "<YOUR REGION ID>";

    // ENDPOINT：On the console, choose **Overview** in the navigation pane and click **Access Addresses** to view the HTTPS application access address.
    private static final String ENDPOINT = "<YOUR ENDPOINT>";

    // ak/sk：For details, see the method of obtaining AK/SK in iotda document https://support.huaweicloud.com/api-iothub/iot_06_v5_0091.html.
    // There will be security risks if the AK/SK used for authentication is directly written into code. We suggest encrypting the AK/SK in the configuration file or environment variables for storage.
    // In this sample, the AK/SK is stored in environment variables for identity authentication. Before running this sample, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");
    private static IoTDAClient initClient() {
        // Create a credential
        ICredential auth = new BasicCredentials()
            .withSk(SK)
            .withAk(AK)
            // Derivative algorithms are used for the standard and enterprise editions. For the basic edition, delete the configuration "withDerivedPredicate".
            .withDerivedPredicate(BasicCredentials.DEFAULT_DERIVED_PREDICATE)
            .withProjectId(PROJECT_ID);

        // Create and initialize an IoTDAClient instance
        return IoTDAClient.newBuilder()
            .withCredential(auth)
            // Use IoTDARegion for basic editions like "withRegion(IoTDARegion.CN_NORTH_4)".
            // Create regions for standard/enterprise editions.
            .withRegion(new Region(REGION_ID, ENDPOINT))
            // .withRegion(IoTDARegion.CN_NORTH_4)
            .build();
    }

    /**
     * Upload an OTA Upgrade Package
     *
     * @param iotdaClient iotda client
     * @param body        Structure for uploading an OTA upgrade package
     * @return Upgrade package ID
     */
    private static String uploadOTAPackages(IoTDAClient iotdaClient, CreateOtaPackage body) throws ServiceResponseException {
        CreateOtaPackageRequest request = new CreateOtaPackageRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        CreateOtaPackageResponse response = iotdaClient.createOtaPackage(request);
        logger.info("The result of uploading an OTA upgrade package is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        return response.getPackageId();
    }


    /**
     * Create a Batch Task
     *
     * @param iotdaClient     iotda client
     * @param createBatchTask Structure for creating a batch task
     * @return The task obtained after the creation
     */
    private static CreateBatchTaskResponse createTask(IoTDAClient iotdaClient, CreateBatchTask createBatchTask) throws ServiceResponseException {
        CreateBatchTaskRequest request = new CreateBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(createBatchTask);
        CreateBatchTaskResponse batchTask = iotdaClient.createBatchTask(request);
        logger.info("The result of creating a batch task is:{}", JsonUtils.toJSON(OBJECTMAPPER, batchTask));
        return batchTask;
    }

    /**
     * Query the Details About a Batch Task
     *
     * @param iotdaClient iotda client
     * @param taskId      Task ID
     * @return The details about the task
     */
    private static Task queryTask(IoTDAClient iotdaClient, String taskId) throws ServiceResponseException {
        ShowBatchTaskRequest request = new ShowBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setTaskId(taskId);
        ShowBatchTaskResponse showBatchTaskResponse = iotdaClient.showBatchTask(request);
        logger.info("The result of querying a batch task is:{}", JsonUtils.toJSON(OBJECTMAPPER, showBatchTaskResponse));
        return showBatchTaskResponse.getBatchtask();
    }

    public static void main(String[] args) {
        // Initialize the IoTDA client
        IoTDAClient iotdaClient = initClient();
        // ID of the resource space to which the device belongs. Replace the value with the required one.
        String appId = "9d988b5efdf646f0828724c45e1d794e";
        // Product corresponding to the OTA upgrade package
        String productId = "";

        // ID of the device to be upgraded in OTA mode. Only one device is used as an example. Multiple devices can be upgraded at a time.
        String deviceId = "";

        try {
            logger.info("========1.Uploading the OTA Upgrade Package========");
            CreateOtaPackage createOtaPackage = new CreateOtaPackage();
            createOtaPackage.setAppId(appId);
            createOtaPackage.setProductId(productId);
            // Software package version
            createOtaPackage.setVersion("v1.0");
            // Upgrade package type. Value range: softwarePackage for a software package and firmwarePackage for a firmware package.
            createOtaPackage.setPackageType("softwarePackage");
            // Upgrade Package Location
            FileLocation fileLocation = new FileLocation();
            ObsLocation obsLocation = new ObsLocation();
            obsLocation.setRegionName(REGION_ID);
            // Name of the bucket where the upgrade package is located
            obsLocation.setBucketName("");
            // OBS object of the upgrade package, including the path
            obsLocation.setObjectKey("");
            fileLocation.setObsLocation(obsLocation);
            createOtaPackage.setFileLocation(fileLocation);
            String packageId = uploadOTAPackages(iotdaClient, createOtaPackage);
            logger.info("========1.OTA upgrade package uploaded successfully========");

            logger.info("========2.Creating an OTA Upgrade Task========");
            CreateBatchTask createBatchTask = new CreateBatchTask();
            createBatchTask.setAppId(appId);
            // Task name, which is unique in the resource space. If the task is invoked for multiple times, the task name needs to be changed.
            createBatchTask.setTaskName("softwareUpgradeTask");
            // The task type is fixed to softwareUpgrade. Do not change the value.
            createBatchTask.setTaskType("softwareUpgrade");
            // Target deviceId: Add the deviceId to be delivered to the list.
            // You can enter multiple device IDs. A maximum of 30,000 device IDs are supported.
            List<String> targetDeviceIds = new ArrayList<>();
            targetDeviceIds.add(deviceId);
            createBatchTask.setTargets(targetDeviceIds);
            // OTA upgrade package
            Map<String, String> map = new HashMap<>();
            // The key is fixed to package_id, and the value is the OTA upgrade package that has been uploaded to the platform.
            map.put("package_id", packageId);
            createBatchTask.setDocument(map);
            // After a task is created, obtain the task ID.
            CreateBatchTaskResponse otaTask = createTask(iotdaClient, createBatchTask);
            logger.info("========2.OTA upgrade task created successfully========");

            logger.info("========3.Querying Batch Task Details========");
            // Query the execution details of the batch task to know the task execution status.
            String status = otaTask.getStatus();
            int times = 0;
            // Wait until the task is complete. The task status is Initializing. Waiting: The task is waiting.
            // Processing: The task is being executed. Success: The operation is successful. Fail: failed. PartialSuccess: partial success. Stopped: stopped. StoppingStopping.
            while (("Initializing".equals(status) || "Waitting".equals(status) || "Processing".equals(status)) && times++ < 5) {
                // Query the execution details of the batch task to obtain the execution status of the OTA upgrade task.
                Task task = queryTask(iotdaClient, otaTask.getTaskId());
                status = task.getStatus();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    logger.warn("Exception Information：{}", e.getMessage());
                }
            }
            logger.info("========3.Querying the batch task details is complete========");
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.warn("Demonstration failed. Exception information is {}", e.getMessage());
        } catch (ServiceResponseException e) {
            logger.warn("Demonstration failed. Exception information is {}, {}", e.getErrorCode(), e.getErrorMsg());
        }
    }
}
