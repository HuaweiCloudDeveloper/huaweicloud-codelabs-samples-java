## 1、功能介绍

华为云提供了IoTDA服务端SDK，您可以直接集成服务端SDK来调用IoTDA的相关API，从而实现对IoTDA的快速操作。
该示例展示了如何通过java版SDK来设备OTA升级包的上传，指定一批设备进行OTA升级。

ota升级：软件升级指升级设备的系统软件和应用软件，固件升级指升级设备硬件的底层“驱动程序”。 升级方式均为将软/固件包上传到物联网平台或者关联用户OBS上的对象，设备从物联网平台或用户OBS获取软/固件包实现远程升级。
详情请参考[OTA升级相关问题](https://support.huaweicloud.com/iothub_faq/iot_faq_01001.html)
以及[MQTT协议设备OTA升级实践](https://support.huaweicloud.com/bestpractice-iothub/iot_bp_0039.html)

**您将学到什么？**

如何通过java版SDK来上传OTA升级包，并通过批量任务的形式指定一批设备进行OTA升级。

## 2、前置条件
- 1、已经开通华为云账号，并获得到您账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。您可以参考[获取项目ID](https://support.huaweicloud.com/api-iothub/iot_06_v5_1001.html)
- 4、已经开通IoTDA服务，获取当前实例的实例ID，请参考[查看实例详情](https://support.huaweicloud.com/usermanual-iothub/iot_01_0121.html) 。

## 3、SDK获取和安装
您需要下载并安装 Maven，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4、接口参数说明
关于接口参数的详细说明可参见：

[创建OTA升级包](https://support.huaweicloud.com/api-iothub/CreateOtaPackage.html)

[创建批量任务](https://support.huaweicloud.com/api-iothub/iot_06_v5_0045.html)

[查询批量任务](https://support.huaweicloud.com/api-iothub/iot_06_v5_0017.html)

## 5、关键代码片段
### 5.1、上传OTA升级包
```java
    /**
     * 上传OTA升级包
     *
     * @param iotdaClient iotda client
     * @param body        上传ota升级包结构体
     * @return 升级包ID
     */
    private static String uploadOTAPackages(IoTDAClient iotdaClient, CreateOtaPackage body) throws ServiceResponseException {
        CreateOtaPackageRequest request = new CreateOtaPackageRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        CreateOtaPackageResponse response = iotdaClient.createOtaPackage(request);
        logger.info("上传OTA升级包的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        return response.getPackageId();
    }
```

### 5.2、创建批量任务
```java
    /**
     * 创建批量任务
     *
     * @param iotdaClient     iotda client
     * @param createBatchTask 创建批量任务结构体
     * @return 创建完成后获取的任务
     */
    private static CreateBatchTaskResponse createTask(IoTDAClient iotdaClient, CreateBatchTask createBatchTask) throws ServiceResponseException {
        CreateBatchTaskRequest request = new CreateBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(createBatchTask);
        CreateBatchTaskResponse batchTask = iotdaClient.createBatchTask(request);
        logger.info("创建批量任务的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, batchTask));
        return batchTask;
    }
```

### 5.3、查询批量任务详情
```java
    /**
     * 查询批量任务详情
     *
     * @param iotdaClient iotda client
     * @param taskId      任务ID
     * @return 返回任务整体情况
     */
    private static Task queryTask(IoTDAClient iotdaClient, String taskId) throws ServiceResponseException {
        ShowBatchTaskRequest request = new ShowBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setTaskId(taskId);
        ShowBatchTaskResponse showBatchTaskResponse = iotdaClient.showBatchTask(request);
        logger.info("查询任务详情的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, showBatchTaskResponse));
        return showBatchTaskResponse.getBatchtask();
    }
```

## 6、运行结果
如果您替换main方法的参数，并按照main方法的顺序执行，程序将演示ota升级包的上传，创建升级任务，查询升级任务详情功能。
以下为main方法中每一个步骤的运行结果。

### 6.1 上传OTA升级包

```json
{
  "package_id": "64f6a17e63066026291dfb37",
  "app_id": "dd708a9bcbeb4e55bbd1d348cdb7b94a",
  "version": "1.0",
  "support_source_versions": null,
  "file_name": "2022/08/18/09/mqtt_ota.v1.zip",
  "description": null,
  "custom_info": null,
  "package_type": "softwarePackage",
  "product_id": "64748b4afeb89944a0df75e8",
  "product_name": "test_ota",
  "create_time": "20230905T033318Z",
  "storage_type": "OBS"
}
```
### 6.2 创建批量OTA升级任务

以软件升级为例
```json
{
  "task_id": "64f6a2b88c2b6271ddeb087b",
  "task_name": "softwareUpgradeTask",
  "task_type": "softwareUpgrade",
  "targets": [
    "64111bcd06ca5933b28a058b_7758dsfdsfsd"
  ],
  "targets_filter": {},
  "document": {
    "package_id": "64eebce6bcbe1947d21b9823"
  },
  "task_policy": {},
  "status": "Initializing",
  "task_progress": {
    "total": 0,
    "processing": 0,
    "success": 0,
    "fail": 0,
    "waitting": 0,
    "fail_wait_retry": 0,
    "stopped": 0,
    "removed": 0
  },
  "create_time": "20230905T033832Z"
}
```

### 6.3 查询OTA升级任务详情

任务状态一般为Processing，表明任务正在执行task_details的表明任务已经启动，并且在查询设备的软固件版本号。
```json
{
  "batchtask": {
    "task_id": "64f6a2b88c2b6271ddeb087b",
    "task_name": "softwareUpgradeTask",
    "task_type": "softwareUpgrade",
    "targets": [
      "64111bcd06ca5933b28a058b_7758dsfdsfsd"
    ],
    "targets_filter": {},
    "document": {
      "package_id": "64eebce6bcbe1947d21b9823"
    },
    "task_policy": {},
    "status": "Processing",
    "status_desc": "",
    "task_progress": {
      "total": 1,
      "processing": 1,
      "success": 0,
      "fail": 0,
      "waitting": 0,
      "fail_wait_retry": 0,
      "stopped": 0,
      "removed": 0
    },
    "create_time": "20230905T034348Z"
  },
  "task_details": [
    {
      "target": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
      "status": "Processing",
      "output": "QueryingVersion",
      "error": {}
    }
  ],
  "page": {
    "count": 1,
    "marker": "64f6a3f48c2b6271ddeb0886"
  }
}
```