package com.huawei.iotda;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.JsonUtils;
import com.huaweicloud.sdk.iotda.v5.IoTDAClient;
import com.huaweicloud.sdk.iotda.v5.model.AddRuleReq;
import com.huaweicloud.sdk.iotda.v5.model.CreateRoutingRuleRequest;
import com.huaweicloud.sdk.iotda.v5.model.CreateRoutingRuleResponse;
import com.huaweicloud.sdk.iotda.v5.model.DeleteRoutingRuleRequest;
import com.huaweicloud.sdk.iotda.v5.model.DeleteRoutingRuleResponse;
import com.huaweicloud.sdk.iotda.v5.model.ListRoutingRulesRequest;
import com.huaweicloud.sdk.iotda.v5.model.ListRoutingRulesResponse;
import com.huaweicloud.sdk.iotda.v5.model.RoutingRuleSubject;
import com.huaweicloud.sdk.iotda.v5.model.ShowRoutingRuleRequest;
import com.huaweicloud.sdk.iotda.v5.model.ShowRoutingRuleResponse;
import com.huaweicloud.sdk.iotda.v5.model.UpdateRoutingRuleRequest;
import com.huaweicloud.sdk.iotda.v5.model.UpdateRoutingRuleResponse;
import com.huaweicloud.sdk.iotda.v5.model.UpdateRuleReq;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RuleTriggeringConditionManagement {
    private static final Logger logger = LoggerFactory.getLogger(RuleTriggeringConditionManagement.class);
    private static final ObjectMapper OBJECTMAPPER = new ObjectMapper();

    // REGION_ID：If your region is 上海一，please fill in "cn-east-3"；If your region is 北京四，please fill in "cn-north-4"; If your region is 华南广州，please fill in "cn-south-4"
    private static final String REGION_ID = "<YOUR REGION ID>";

    // ENDPOINT：On the console, choose **Overview** in the navigation pane and click **Access Addresses** to view the HTTPS application access address.
    private static final String ENDPOINT = "<YOUR ENDPOINT>";

    // ak/sk：For details, see the method of obtaining AK/SK in iotda document https://support.huaweicloud.com/api-iothub/iot_06_v5_0091.html.
    // There will be security risks if the AK/SK used for authentication is directly written into code. We suggest encrypting the AK/SK in the configuration file or environment variables for storage.
    // In this sample, the AK/SK is stored in environment variables for identity authentication. Before running this sample, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String PROJECT_ID = "<YOUR PROJECTID>";

    // INSTANCE_ID
    private static final String INSTANCE_ID = "<YOUR INSTANCEID>";

    private static IoTDAClient initClient() {
        // Create a credential
        ICredential auth = new BasicCredentials()
            .withAk(AK)
            .withSk(SK)
            // Derivative algorithms are used for the standard
            // and enterprise editions. For the basic edition, delete the configuration "withDerivedPredicate".
            .withDerivedPredicate(BasicCredentials.DEFAULT_DERIVED_PREDICATE)
            .withProjectId(PROJECT_ID);

        // Create and initialize an IoTDAClient instance
        return IoTDAClient.newBuilder()
            .withCredential(auth)
            // Use IoTDARegion for basic editions like "withRegion(IoTDARegion.CN_NORTH_4)".
            // Create regions for standard/enterprise editions.
            .withRegion(new Region(REGION_ID, ENDPOINT))
            // .withRegion(IoTDARegion.CN_NORTH_4)
            .build();
    }

    /**
     * Creating a Rule Triggering Condition
     *
     * @param iotdaClient iotda client
     * @param body        Structure for Creating a Rule Triggering Condition
     * @return Rule ID
     */
    private static String createRoutingRule(IoTDAClient iotdaClient, AddRuleReq body) throws ServiceResponseException {
        CreateRoutingRuleRequest request = new CreateRoutingRuleRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        CreateRoutingRuleResponse response = iotdaClient.createRoutingRule(request);
        logger.info("The result of creaing a rule triggering condition is:", JsonUtils.toJSON(OBJECTMAPPER, response));
        return response.getRuleId();
    }

    /**
     * Querying the Rule Triggering Condition List
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space ID
     */
    private static void queryRoutingRuleList(IoTDAClient iotdaClient, String appId) throws ServiceResponseException {
        ListRoutingRulesRequest request = new ListRoutingRulesRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        ListRoutingRulesResponse response = iotdaClient.listRoutingRules(request);
        logger.info("The result of querying a rule triggering condition list is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    /**
     * Querying the Details About a Rule Triggering Condition
     *
     * @param iotdaClient iotda client
     * @param ruleId      Rule ID
     */
    private static void queryRoutingRuleDetail(IoTDAClient iotdaClient, String ruleId) throws ServiceResponseException {
        ShowRoutingRuleRequest request = new ShowRoutingRuleRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setRuleId(ruleId);
        ShowRoutingRuleResponse productResponse = iotdaClient.showRoutingRule(request);
        logger.info("The result of querying the details about a rule triggering condition is:{}", JsonUtils.toJSON(OBJECTMAPPER, productResponse));
    }

    /**
     * Update a Rule Triggering Condition
     *
     * @param iotdaClient iotda client
     * @param ruleId      Rule ID
     * @param body        Structure for update a rule triggering condition
     */
    private static void updateRoutingRule(IoTDAClient iotdaClient, String ruleId, UpdateRuleReq body) throws ServiceResponseException {
        UpdateRoutingRuleRequest request = new UpdateRoutingRuleRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        request.setRuleId(ruleId);
        UpdateRoutingRuleResponse updateRoutingRule = iotdaClient.updateRoutingRule(request);
        logger.info("The result of updating a rule triggering condition is:{}", JsonUtils.toJSON(OBJECTMAPPER, updateRoutingRule));
    }

    /**
     * Delete a Rule Triggering Condition
     *
     * @param iotdaClient iotda client
     * @param ruleId      Rule ID
     */
    private static void deleteRoutingRule(IoTDAClient iotdaClient, String ruleId) throws ServiceResponseException {
        DeleteRoutingRuleRequest request = new DeleteRoutingRuleRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setRuleId(ruleId);
        DeleteRoutingRuleResponse response = iotdaClient.deleteRoutingRule(request);
        logger.info("The result of deleting a rule triggering condition is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    public static void main(String[] args) {
        // Initialize the IoTDA client
        IoTDAClient iotdaClient = initClient();
        // ID of the resource space to which the device belongs. Replace the value with the required one.
        String appId = "9d988b5efdf646f0828724c45e1d794e";

        try {
            logger.info("========1.Querying the List of Rule Triggering Conditions========");
            queryRoutingRuleList(iotdaClient, appId);
            logger.info("========1.Querying the rule triggering condition list is complete========");

            logger.info("========2.Creating a Rule Trigger Condition========");
            // Assemble and create rule triggering condition data. You can assemble the data based on the actual situation.
            AddRuleReq addRoutingRule = new AddRuleReq();
            addRoutingRule.setAppId(appId);
            addRoutingRule.setRuleName("deviceCrete");
            // Effective scope of the rule. GLOBAL: The rule takes effect at the tenant level. APP: takes effect at the resource space level. If the type is APP, the created rule takes effect in the resource space specified by the app_id.
            // If the app_id is not specified, the created rule takes effect in the default resource space.
            addRoutingRule.setAppType("APP");
            // Notification of adding a device
            RoutingRuleSubject subject = new RoutingRuleSubject();
            subject.setResource("device");
            subject.setEvent("device：create");
            addRoutingRule.setSubject(subject);
            String ruleId = createRoutingRule(iotdaClient, addRoutingRule);
            logger.info("========2.Rule triggering condition created successfully========");

            logger.info("========3.Querying the List of Rule Triggering Conditions========");
            queryRoutingRuleList(iotdaClient, appId);
            logger.info("========3.Querying the rule triggering condition list is complete========");

            logger.info("========4.Querying Rule Triggering Condition Details========");
            queryRoutingRuleDetail(iotdaClient, ruleId);
            logger.info("========4.Querying rule triggering condition details completed========");

            logger.info("========5.Update Rule Triggering Condition========");
            // Change the name based on the site requirements. The following describes how to change the name of the trigger condition for creating a rule.
            UpdateRuleReq updateRoutingRule = new UpdateRuleReq();
            updateRoutingRule.setRuleName("deviceCreteInApp");
            updateRoutingRule(iotdaClient, ruleId, updateRoutingRule);
            logger.info("========5.Rule triggering condition update completed========");

            logger.info("========6.Deleting a rule trigger condition========");
            deleteRoutingRule(iotdaClient, ruleId);
            logger.info("========6.Deleting the triggering condition of the rule is complete========");

            logger.info("========7.Querying the List of Rule Triggering Conditions========");
            queryRoutingRuleList(iotdaClient, appId);
            logger.info("========7.Querying the rule triggering condition list is complete========");
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.warn("Demonstration failed. Exception information is {}", e.getMessage());
        } catch (ServiceResponseException e) {
            logger.warn("Demonstration failed. Exception information is {}, {}", e.getErrorCode(), e.getErrorMsg());
        }
    }
}