## 1. Functions

You can integrate the IoTDA server SDKs provided by Huawei Cloud to call IoTDA APIs and use IoTDA smoothly.
This example shows and demonstrates how to use the Java SDK to add, modify, delete and query the rule triggering conditions, as well as query the rule triggering condition list.

Rule Triggering Condition: Users can set rules for the devices connected to the IoT platform on the cloud using the rule engine. When the triggering conditions of a rule are satisfied, the platform will perform corresponding actions to meet the users' requirements. The actions include device linkage and data forwarding.

For details see:

[Rule - Overview](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0022.html)

[Data Forwarding](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0024.html)

[Subscription and Push - Overview](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_00140.html)


**What You Will Learn**

The ways to use the Java SDK to add, modify, delete and query the rule triggering conditions, as well as query the rule triggering condition list.

## 2. Prerequisites
- 1. You have created a Huawei Cloud account and obtained the access key (AK) and secret access key (SK) of your account. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 2. You have set up the development environment with Java JDK 1.8 or later.
- 3. You have obtained the project ID of the target region. View the project ID on the **My Credentials** > **API Credentials** page of the Huawei Cloud console. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_1001.html).
- 4. You have enabled IoTDA. For details about how to obtain the ID of the current instance, see [Viewing Instance Details](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0121.html).

## 3. Obtaining and Installing the SDK
Download and install Maven, and add dependencies to the **pom.xml** file of the Java project.
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4. API Parameters
For details about API parameters, see:

[Create a Rule Triggering Condition](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_01307.html)

[Query the Rule Triggering Condition List](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_01306.html)

[Query a Rule Triggering Condition](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_01308.html)

[Modify a Rule Triggering Condition](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_01309.html)

[Delete a Rule Triggering Condition](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_01310.html)

## 5. Key Code Snippets
### 5.1 Creating a Rule Triggering Condition

```java

/**
 * Creating a Rule Triggering Condition
 *
 * @param iotdaClient iotda client
 * @param body        Structure for Creating a Rule Triggering Condition
 * @return Rule ID
 */
private static String createRoutingRule(IoTDAClient iotdaClient, AddRuleReq body) throws ServiceResponseException {
        CreateRoutingRuleRequest request = new CreateRoutingRuleRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        CreateRoutingRuleResponse response = iotdaClient.createRoutingRule(request);
        logger.info("The result of creaing a rule triggering condition is:", JsonUtils.toJSON(OBJECTMAPPER, response));
        return response.getRuleId();
        }
```

### 5.2 Querying the Rule Triggering Condition List

```java
    /**
 * Querying the Rule Triggering Condition List
 *
 * @param iotdaClient iotda client
 * @param appId       Resource space ID
 */
private static void queryRoutingRuleList(IoTDAClient iotdaClient, String appId) throws ServiceResponseException {
        ListRoutingRulesRequest request = new ListRoutingRulesRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        ListRoutingRulesResponse response = iotdaClient.listRoutingRules(request);
        logger.info("The result of querying a rule triggering condition list is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        }
```

### 5.3 Querying the Details About a Rule Triggering Condition

```java
    /**
 * Querying the Details About a Rule Triggering Condition
 *
 * @param iotdaClient iotda client
 * @param ruleId      Rule ID
 */
private static void queryRoutingRuleDetail(IoTDAClient iotdaClient, String ruleId) throws ServiceResponseException {
        ShowRoutingRuleRequest request = new ShowRoutingRuleRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setRuleId(ruleId);
        ShowRoutingRuleResponse productResponse = iotdaClient.showRoutingRule(request);
        logger.info("The result of querying the details about a rule triggering condition is:{}", JsonUtils.toJSON(OBJECTMAPPER, productResponse));
        }
```

### 5.4 Update a Rule Triggering Condition
```java
    /**
 * Update a Rule Triggering Condition
 *
 * @param iotdaClient iotda client
 * @param ruleId      Rule ID
 * @param body        Structure for update a rule triggering condition
 */
private static void updateRoutingRule(IoTDAClient iotdaClient, String ruleId, UpdateRuleReq body) throws ServiceResponseException {
        UpdateRoutingRuleRequest request = new UpdateRoutingRuleRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        request.setRuleId(ruleId);
        UpdateRoutingRuleResponse updateRoutingRule = iotdaClient.updateRoutingRule(request);
        logger.info("The result of updating a rule triggering condition is:{}", JsonUtils.toJSON(OBJECTMAPPER, updateRoutingRule));
        }
```

### 5.5 Delete a Rule Triggering Condition
```java
    /**
 * Delete a Rule Triggering Condition
 *
 * @param iotdaClient iotda client
 * @param ruleId      Rule ID
 */
private static void deleteRoutingRule(IoTDAClient iotdaClient, String ruleId) throws ServiceResponseException {
        DeleteRoutingRuleRequest request = new DeleteRoutingRuleRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setRuleId(ruleId);
        DeleteRoutingRuleResponse response = iotdaClient.deleteRoutingRule(request);
        logger.info("The result of deleting a rule triggering condition is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        }
```

## 6. Execution Results
Replace the parameters and execute the code in the sequence of the main method. The program demonstrates how to add, modify, delete and query the rule triggering conditions, as well as query the rule triggering condition list.
The device property reporting is used as an example of the rule triggering condition here. The execution results of each step of the main method are shown as follows.

### 6.1 Query the Rule Triggering Condition List(Before Creating a Rule Triggering Condition)

Because the rule triggering condition is not created yet, the query result is empty.
```json
{
  "rules": [],
  "count": 0,
  "marker": "ffffffffffffffffffffffff"
}
```
### 6.2 Create a Rule Triggering Condition
```json
{
  "rule_id": "e9cca3f8-503d-469b-8a9b-8434a50f6353",
  "rule_name": "devicePropertyReport",
  "subject": {
    "resource": "device.property",
    "event": "report"
  },
  "app_type": "APP",
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "active": false
}
```

### 6.3 Query the Rule Triggering Condition List(After Creating a Rule Triggering Condition)
```json
{
  "rules": [
    {
      "rule_id": "e9cca3f8-503d-469b-8a9b-8434a50f6353",
      "rule_name": "devicePropertyReport",
      "subject": {
        "resource": "device.property",
        "event": "report"
      },
      "app_type": "APP",
      "app_id": "9d988b5efdf646f0828724c45e1d794e",
      "active": false
    }
  ],
  "count": 1,
  "marker": "61615c8c2d06ec02b003338d"
}
```
### 6.4 Querying the Details About a Rule Triggering Condition
```json
{
  "rule_id": "e9cca3f8-503d-469b-8a9b-8434a50f6353",
  "rule_name": "devicePropertyReport",
  "subject": {
    "resource": "device.property",
    "event": "report"
  },
  "app_type": "APP",
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "active": false
}
```


### 6.5 Update a Rule Triggering Condition
```json
{
  "rule_id": "e9cca3f8-503d-469b-8a9b-8434a50f6353",
  "rule_name": "devicePropertyReportInApp",
  "subject": {
    "resource": "device.property",
    "event": "report"
  },
  "app_type": "APP",
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "active": false
}
```

### 6.6 Delete a Rule Triggering Condition
```json
{}
```
### 6.7 Query the Rule Triggering Condition List(After Deleting a Rule Triggering Condition)
```json
{
  "rules": [],
  "count": 0,
  "marker": "ffffffffffffffffffffffff"
}
```