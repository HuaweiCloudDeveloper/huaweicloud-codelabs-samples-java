## 1、功能介绍

华为云提供了IoTDA服务端SDK，您可以直接集成服务端SDK来调用IoTDA的相关API，从而实现对IoTDA的快速操作。
该示例展示了如何通过java版SDK来实现对流转规则条件的新增，修改，删除，查询，列表查询功能，同时通过demo进行简单的演示操作。

流转规则条件：规则引擎是指用户可以在物联网平台上对接入平台的设备设定相应的规则，在条件满足所设定的规则后，平台会触发相应的动作来满足用户需求。包含设备联动和数据转发两种类型。
本次实例为数据转发的规则条件管理，详情请参考:

[规则引擎介绍](https://support.huaweicloud.com/usermanual-iothub/iot_01_0022.html)

[数据转发简介](https://support.huaweicloud.com/usermanual-iothub/iot_01_0024.html)

[订阅推送方式概述](https://support.huaweicloud.com/usermanual-iothub/iot_01_00140.html)


**您将学到什么？**

如何通过java版SDK来实现对流转规则条件的新增，修改，删除，查询，列表查询功能。

## 2、前置条件
- 1、已经开通华为云账号，并获得到您账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。您可以参考[获取项目ID](https://support.huaweicloud.com/api-iothub/iot_06_v5_1001.html)
- 4、已经开通IoTDA服务，获取当前实例的实例ID，请参考[查看实例详情](https://support.huaweicloud.com/usermanual-iothub/iot_01_0121.html) 。

## 3、SDK获取和安装
您需要下载并安装 Maven，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4、接口参数说明
关于接口参数的详细说明可参见：

[创建规则触发条件](https://support.huaweicloud.com/api-iothub/iot_06_v5_01307.html)

[查询规则条件列表](https://support.huaweicloud.com/api-iothub/iot_06_v5_01306.html)

[查询规则条件](https://support.huaweicloud.com/api-iothub/iot_06_v5_01308.html)

[修改规则触发条件](https://support.huaweicloud.com/api-iothub/iot_06_v5_01309.html)

[删除规则触发条件](https://support.huaweicloud.com/api-iothub/iot_06_v5_01310.html)

## 5、关键代码片段
### 5.1、创建流转规则条件

```java

/**
 * 创建规则触发条件
 *
 * @param iotdaClient iotda client
 * @param body        创建规则触发条件结构体
 * @return 规则ID
 */
private static String createRoutingRule(IoTDAClient iotdaClient, AddRuleReq body) throws ServiceResponseException {
        CreateRoutingRuleRequest request = new CreateRoutingRuleRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        CreateRoutingRuleResponse response = iotdaClient.createRoutingRule(request);
        logger.info("创建规则触发条件的结果为:", JsonUtils.toJSON(OBJECTMAPPER, response));
        return response.getRuleId();
        }
```

### 5.2、查询流转规则条件列表

```java
    /**
 * 查询规则触发条件列表
 *
 * @param iotdaClient iotda client
 * @param appId       资源空间ID
 */
private static void queryRoutingRuleList(IoTDAClient iotdaClient, String appId) throws ServiceResponseException {
        ListRoutingRulesRequest request = new ListRoutingRulesRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        ListRoutingRulesResponse response = iotdaClient.listRoutingRules(request);
        logger.info("查询创建规则触发条件列表的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        }
```

### 5.3、查询流转规则条件详情

```java
    /**
 * 查询规则触发条件详情
 *
 * @param iotdaClient iotda client
 * @param ruleId      规则ID
 */
private static void queryRoutingRuleDetail(IoTDAClient iotdaClient, String ruleId) throws ServiceResponseException {
        ShowRoutingRuleRequest request = new ShowRoutingRuleRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setRuleId(ruleId);
        ShowRoutingRuleResponse productResponse = iotdaClient.showRoutingRule(request);
        logger.info("查询创建规则触发条件详情的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, productResponse));
        }
```

### 5.4、修改流转规则条件
```java
    /**
 * 更新规则触发条件
 *
 * @param iotdaClient iotda client
 * @param ruleId      规则ID
 * @param body        更新规则触发条件结构体
 */
private static void updateRoutingRule(IoTDAClient iotdaClient, String ruleId, UpdateRuleReq body) throws ServiceResponseException {
        UpdateRoutingRuleRequest request = new UpdateRoutingRuleRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        request.setRuleId(ruleId);
        UpdateRoutingRuleResponse updateRoutingRule = iotdaClient.updateRoutingRule(request);
        logger.info("更新创建规则触发条件的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, updateRoutingRule));
        }
```

### 5.5、删除流转规则条件
```java
    /**
 * 删除规则触发条件
 *
 * @param iotdaClient iotda client
 * @param ruleId      规则ID
 */
private static void deleteRoutingRule(IoTDAClient iotdaClient, String ruleId) throws ServiceResponseException {
        DeleteRoutingRuleRequest request = new DeleteRoutingRuleRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setRuleId(ruleId);
        DeleteRoutingRuleResponse response = iotdaClient.deleteRoutingRule(request);
        logger.info("删除创建规则触发条件的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        }
```

## 6、运行结果
如果您替换main方法的参数，并按照main方法的顺序执行，程序将演示流转规则条件的新增，修改，查询，修改，删除功能。
本次流转规则触发条件为属性上报为例，以下为main方法中每一个步骤的运行结果。

### 6.1 查询流转规则条件列表(创建流转规则条件前)

由于还未创建流转规则条件，查出来为空
```json
{
  "rules": [],
  "count": 0,
  "marker": "ffffffffffffffffffffffff"
}
```
### 6.2 创建流转规则条件
```json
{
  "rule_id": "e9cca3f8-503d-469b-8a9b-8434a50f6353",
  "rule_name": "devicePropertyReport",
  "subject": {
    "resource": "device.property",
    "event": "report"
  },
  "app_type": "APP",
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "active": false
}
```

### 6.3 查询流转规则条件列表(创建流转规则条件后)
```json
{
  "rules": [
    {
      "rule_id": "e9cca3f8-503d-469b-8a9b-8434a50f6353",
      "rule_name": "devicePropertyReport",
      "subject": {
        "resource": "device.property",
        "event": "report"
      },
      "app_type": "APP",
      "app_id": "9d988b5efdf646f0828724c45e1d794e",
      "active": false
    }
  ],
  "count": 1,
  "marker": "61615c8c2d06ec02b003338d"
}
```
### 6.4 查询流转规则条件详情
```json
{
  "rule_id": "e9cca3f8-503d-469b-8a9b-8434a50f6353",
  "rule_name": "devicePropertyReport",
  "subject": {
    "resource": "device.property",
    "event": "report"
  },
  "app_type": "APP",
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "active": false
}
```


### 6.5 更新流转规则条件
```json
{
  "rule_id": "e9cca3f8-503d-469b-8a9b-8434a50f6353",
  "rule_name": "devicePropertyReportInApp",
  "subject": {
    "resource": "device.property",
    "event": "report"
  },
  "app_type": "APP",
  "app_id": "9d988b5efdf646f0828724c45e1d794e",
  "active": false
}
```

### 6.6 删除流转规则条件
```json
{}
```
### 6.7 查询流转规则条件列表(删除流转规则条件后）
```json
{
  "rules": [],
  "count": 0,
  "marker": "ffffffffffffffffffffffff"
}
```