## 1、功能介绍

华为云提供了IoTDA服务端SDK，您可以直接集成服务端SDK来调用IoTDA的相关API，从而实现对IoTDA的快速操作。
该示例展示了如何通过java版SDK来实现属性下发以及查询设备属性功能。

设备属性：属性下发分为查询设备属性和修改属性参数两种，查询设备属性用于应用侧或平台主动获取设备属性数据，修改属性参数用于应用侧或平台设置设备属性值并同步到设备侧。设备接收到属性下发指令后需要立即响应，如果设备没有响应，平台会认为命令执行超时。
详情请参考[属性下发概述](https://support.huaweicloud.com/usermanual-iothub/iot_01_0336.html)

**您将学到什么？**

如何通过java版SDK来实现属性下发以及查询设备属性功能。

## 2、前置条件
- 1、已经开通华为云账号，并获得到您账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。您可以参考[获取项目ID](https://support.huaweicloud.com/api-iothub/iot_06_v5_1001.html)
- 4、已经开通IoTDA服务，获取当前实例的实例ID，请参考[查看实例详情](https://support.huaweicloud.com/usermanual-iothub/iot_01_0121.html) 。 
- 5、并且存在一个设备，建议设备已经连接到平台，你可以具体看到平台与设备交互。

## 3、SDK获取和安装
您需要下载并安装 Maven，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4、接口参数说明
关于接口参数的详细说明可参见：

[修改设备属性](https://support.huaweicloud.com/api-iothub/iot_06_v5_0034.html)

[查询设备属性](https://support.huaweicloud.com/api-iothub/iot_06_v5_0035.html)

## 5、关键代码片段
### 5.1、修改设备属性

```java
    /**
     * @param iotdaClient iotda client
     * @param deviceId    需要设置属性的设备
     * @param body        需要下发的属性
     */
    private static void updateProperty(IoTDAClient iotdaClient, String deviceId, DevicePropertiesRequest body) throws ServiceResponseException {
        UpdatePropertiesRequest request = new UpdatePropertiesRequest();
        request.setDeviceId(deviceId);
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        UpdatePropertiesResponse response = iotdaClient.updateProperties(request);
        logger.info("更新设备属性的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.2、查询设备属性

```java
    /**
     * @param iotdaClient iotda client
     * @param deviceId    设备Id
     * @param serviceId   要查询属性所属的服务。
     */
    private static void queryProperty(IoTDAClient iotdaClient, String deviceId, String serviceId) throws ServiceResponseException {
        ListPropertiesRequest request = new ListPropertiesRequest();
        request.setDeviceId(deviceId);
        request.setInstanceId(INSTANCE_ID);
        request.setServiceId(serviceId);
        ListPropertiesResponse propertiesResponse = iotdaClient.listProperties(request);
        logger.info("查询设备属性的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, propertiesResponse));
    }
```

## 6、运行结果
如果您替换main方法的参数，并按照main方法的顺序执行，程序将演示将电灯亮度属性发送给设备，并查询实时设备的亮度属性
以下为main方法中每一个步骤的运行结果。

### 6.1 下发设备属性

如果设备在线，且正常回复响应，您将会收到以下结果
```json
{
  "response": {
    "result_code": 0,
    "result_desc": "success"
  }
}
```
### 6.2 查询设备属性

如果设备在线，且正常回复响应，您将会收到以下结果
```json
{
  "response": {
    "services": [
      {
        "service_id": "BasicData",
        "properties": {
          "luminance": "1"
        },
        "event_time": "20190606T121212Z"
      }
    ]
  }
}
```