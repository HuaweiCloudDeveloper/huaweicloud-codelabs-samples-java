## 1. Functions

You can integrate the IoTDA server SDKs provided by Huawei Cloud to call IoTDA APIs and use IoTDA smoothly.
This example shows how to use the Java SDK to deliver and query device properties.

Device Property: Property delivery is used for property query or modification. An application or the platform can obtain device property information actively or modify the properties, and synchronize the modification result to the device. After receiving a message, the device needs to return the command execution result to the platform immediately. If the device does not respond, the platform considers that the command execution times out.
For details, see [Property Delivery Overview](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0336.html)

**What You Will Learn**

The ways to use the Java SDK to deliver and query device properties.

## 2. Prerequisites
- 1. You have created a Huawei Cloud account and obtained the access key (AK) and secret access key (SK) of your account. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 2. You have set up the development environment with Java JDK 1.8 or later.
- 3. You have obtained the project ID of the target region. View the project ID on the **My Credentials** > **API Credentials** page of the Huawei Cloud console. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_1001.html).
- 4. You have enabled IoTDA. For details about how to obtain the ID of the current instance, see [Viewing Instance Details](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0121.html).
- 5. You have created a device and connected it to the platform for viewing interconnection details.

## 3. Obtaining and Installing the SDK
Download and install Maven, and add dependencies to the **pom.xml** file of the Java project.
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4. API Parameters
For details about API parameters, see:

[Query Device Properties](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0034.html)

[Modify Device Properties](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0035.html)

## 5. Key Code Snippets
### 5.1 Modify Device Properties

```java
    /**
     * @param iotdaClient iotda client
     * @param deviceId    Property recipient device
     * @param body        Property to deliver
     */
    private static void updateProperty(IoTDAClient iotdaClient, String deviceId, DevicePropertiesRequest body) throws ServiceResponseException {
        UpdatePropertiesRequest request = new UpdatePropertiesRequest();
        request.setDeviceId(deviceId);
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        UpdatePropertiesResponse response = iotdaClient.updateProperties(request);
        logger.info("The device property update result is {}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.2 Query Device Properties

```java
    /**
     * @param iotdaClient iotda client
     * @param deviceId    Device ID
     * @param serviceId   Service ID of the property to be queried.
     */
    private static void queryProperty(IoTDAClient iotdaClient, String deviceId, String serviceId) throws ServiceResponseException {
        ListPropertiesRequest request = new ListPropertiesRequest();
        request.setDeviceId(deviceId);
        request.setInstanceId(INSTANCE_ID);
        request.setServiceId(serviceId);
        ListPropertiesResponse propertiesResponse = iotdaClient.listProperties(request);
        logger.info("The device property query result is {}", JsonUtils.toJSON(OBJECTMAPPER, propertiesResponse));
    }
```

## 6. Execution Results
Replace the parameters and execute the code in the sequence of the main method. The program demonstrates how to send the luminance property to the lighting device and queries the luminance property of the device in real time.
The execution results of each step of the main method are shown as follows.

### 6.1 Deliver a Device Property

If the device is online and correctly responds, the following results will be obtained:
```json
{
  "response": {
    "result_code": 0,
    "result_desc": "success"
  }
}
```
### 6.2 Query a Device Property

If the device is online and correctly responds, the following results will be obtained:
```json
{
  "response": {
    "services": [
      {
        "service_id": "BasicData",
        "properties": {
          "luminance": "1"
        },
        "event_time": "20190606T121212Z"
      }
    ]
  }
}
```