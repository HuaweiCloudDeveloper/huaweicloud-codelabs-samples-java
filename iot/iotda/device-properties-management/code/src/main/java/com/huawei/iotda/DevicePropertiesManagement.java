package com.huawei.iotda;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.JsonUtils;
import com.huaweicloud.sdk.iotda.v5.IoTDAClient;
import com.huaweicloud.sdk.iotda.v5.model.DevicePropertiesRequest;
import com.huaweicloud.sdk.iotda.v5.model.ListPropertiesRequest;
import com.huaweicloud.sdk.iotda.v5.model.ListPropertiesResponse;
import com.huaweicloud.sdk.iotda.v5.model.UpdatePropertiesRequest;
import com.huaweicloud.sdk.iotda.v5.model.UpdatePropertiesResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DevicePropertiesManagement {
    private static final ObjectMapper OBJECTMAPPER = new ObjectMapper();
    // REGION_ID：If your region is 上海一，please fill in "cn-east-3"；If your region is 北京四，please fill in "cn-north-4"; If your region is 华南广州，please fill in "cn-south-4"
    private static final String REGION_ID = "<YOUR REGION ID>";

    private static final String PROJECT_ID = "<YOUR PROJECTID>";

    // ENDPOINT：On the console, choose **Overview** in the navigation pane and click
    // **Access Addresses** to view the HTTPS application access address.
    private static final String ENDPOINT = "<YOUR ENDPOINT>";

    // ak/sk：For details, see the method of obtaining AK/SK in iotda document https://support.huaweicloud.com/api-iothub/iot_06_v5_0091.html.
    // There will be security risks if the AK/SK used for authentication is directly written into code.
    // We suggest encrypting the AK/SK in the configuration file or environment variables for storage.
    // In this sample, the AK/SK is stored in environment variables for identity authentication. Before running this sample, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");

    // Instance id
    private static final String INSTANCE_ID = "<YOUR INSTANCEID>";

    private static final Logger logger = LoggerFactory.getLogger(DevicePropertiesManagement.class);

    private static IoTDAClient initClient() {
        // Create a credential
        ICredential auth = new BasicCredentials()
            .withAk(AK)
            .withSk(SK)
            .withProjectId(PROJECT_ID)
            // Derivative algorithms are used for the standard and enterprise editions. For the basic edition, delete the configuration "withDerivedPredicate".
            .withDerivedPredicate(BasicCredentials.DEFAULT_DERIVED_PREDICATE);


        // Create and initialize an IoTDAClient instance
        return IoTDAClient.newBuilder()
            .withCredential(auth)
            // Use IoTDARegion for basic editions like "withRegion(IoTDARegion.CN_NORTH_4)". Create regions for standard/enterprise editions.
            .withRegion(new Region(REGION_ID, ENDPOINT))
            // .withRegion(IoTDARegion.CN_NORTH_4)
            .build();
    }

    /**
     * @param iotdaClient iotda client
     * @param deviceId    Property recipient device
     * @param body        Property to deliver
     */
    private static void updateProperty(IoTDAClient iotdaClient, String deviceId, DevicePropertiesRequest body) throws ServiceResponseException {
        UpdatePropertiesRequest request = new UpdatePropertiesRequest();
        request.setDeviceId(deviceId);
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        UpdatePropertiesResponse response = iotdaClient.updateProperties(request);
        logger.info("The device property update result is {}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    /**
     * @param iotdaClient iotda client
     * @param deviceId    Device ID
     * @param serviceId   Service ID of the property to be queried.
     */
    private static void queryProperty(IoTDAClient iotdaClient, String deviceId, String serviceId) throws ServiceResponseException {
        ListPropertiesRequest request = new ListPropertiesRequest();
        request.setDeviceId(deviceId);
        request.setInstanceId(INSTANCE_ID);
        request.setServiceId(serviceId);
        ListPropertiesResponse propertiesResponse = iotdaClient.listProperties(request);
        logger.info("The device property query result is {}", JsonUtils.toJSON(OBJECTMAPPER, propertiesResponse));
    }

    public static void main(String[] args) {
        // Initialize the IoTDA client
        IoTDAClient iotdaClient = initClient();
        // ID of the property recipient device. Replace the value with the required one.
        String deviceId = "64111bcd06ca5933b28a058b_saadas";
        // Service ID defined in the product.
        String serviceId = "BasicData";

        try {
            logger.info("========1.Update a device property========");
            // Structure for property update
            DevicePropertiesRequest deviceProperties = new DevicePropertiesRequest();
            List<Property> properties = new ArrayList<>();
            Property property = new Property();
            property.setService_id(serviceId);
            Map<String, Object> value = new HashMap<>();
            value.put("luminance", 88);
            property.setProperties(value);
            properties.add(property);
            deviceProperties.setServices(properties);
            // Update a device property
            updateProperty(iotdaClient, deviceId, deviceProperties);
            logger.info("========1.Device property updated========");

            logger.info("========2.Query a device property========");
            queryProperty(iotdaClient, deviceId, serviceId);
            logger.info("========2.Device property queried========");
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.warn("Demonstration failed. Exception information is {}", e.getMessage());
        } catch (ServiceResponseException e) {
            logger.warn("Demonstration failed. Exception information is {}, {}", e.getErrorCode(), e.getErrorMsg());
        }
    }

    static class Property {
        private String service_id;
        private Object properties;

        public String getService_id() {
            return service_id;
        }

        public void setService_id(String service_id) {
            this.service_id = service_id;
        }

        public Object getProperties() {
            return properties;
        }

        public void setProperties(Object properties) {
            this.properties = properties;
        }
    }
}
