package com.huawei.iotda;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.JsonUtils;
import com.huaweicloud.sdk.iotda.v5.IoTDAClient;
import com.huaweicloud.sdk.iotda.v5.model.AddQueueRequest;
import com.huaweicloud.sdk.iotda.v5.model.AddQueueResponse;
import com.huaweicloud.sdk.iotda.v5.model.BatchShowQueueRequest;
import com.huaweicloud.sdk.iotda.v5.model.BatchShowQueueResponse;
import com.huaweicloud.sdk.iotda.v5.model.DeleteQueueRequest;
import com.huaweicloud.sdk.iotda.v5.model.DeleteQueueResponse;
import com.huaweicloud.sdk.iotda.v5.model.QueueInfo;
import com.huaweicloud.sdk.iotda.v5.model.ShowQueueRequest;
import com.huaweicloud.sdk.iotda.v5.model.ShowQueueResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AmqpQueueManagement {
    private static final Logger logger = LoggerFactory.getLogger(AmqpQueueManagement.class);
    private static final ObjectMapper OBJECTMAPPER = new ObjectMapper();

    // REGION_ID：If your region is 上海一，please fill in "cn-east-3"；If your region is 北京四，please fill in "cn-north-4"; If your region is 华南广州，please fill in "cn-south-4"
    private static final String REGION_ID = "<YOUR REGION ID>";

    // ENDPOINT：On the console, choose **Overview** in the navigation pane and click **Access Addresses** to view the HTTPS application access address.
    private static final String ENDPOINT = "<YOUR ENDPOINT>";

    // Instance id
    private static final String INSTANCE_ID = "<YOUR INSTANCEID>";

    // ak/sk：For details, see the method of obtaining AK/SK in iotda document https://support.huaweicloud.com/api-iothub/iot_06_v5_0091.html.
    // There will be security risks if the AK/SK used for authentication is directly written into code. We suggest encrypting the AK/SK in the configuration file or environment variables for storage.
    // In this sample, the AK/SK is stored in environment variables for identity authentication. Before running this sample, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String PROJECT_ID = "<YOUR PROJECTID>";

    private static IoTDAClient initClient() {
        // Create a credential
        ICredential auth = new BasicCredentials()
            .withAk(AK)
            .withSk(SK)
            // Derivative algorithms are used for the standard and enterprise editions.
            // For the basic edition, delete the configuration "withDerivedPredicate".
            .withDerivedPredicate(BasicCredentials.DEFAULT_DERIVED_PREDICATE)
            .withProjectId(PROJECT_ID);

        // Create and initialize an IoTDAClient instance
        return IoTDAClient.newBuilder()
            .withCredential(auth)
            // Use IoTDARegion for basic editions like "withRegion(IoTDARegion.CN_NORTH_4)".
            // Create regions for standard/enterprise editions.
            .withRegion(new Region(REGION_ID, ENDPOINT))
            // .withRegion(IoTDARegion.CN_NORTH_4)
            .build();
    }

    /**
     * Create an AMQP queue
     *
     * @param iotdaClient iotda client
     * @param body        Structure for AMQP creation
     * @return AMQP queue ID
     */
    private static String createAmqpQueue(IoTDAClient iotdaClient, QueueInfo body) throws ServiceResponseException {
        AddQueueRequest request = new AddQueueRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        AddQueueResponse response = iotdaClient.addQueue(request);
        logger.info("The AMQP queue creation result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        return response.getQueueId();
    }

    /**
     * Query the AMQP List
     *
     * @param iotdaClient iotda client
     */
    private static void queryAmqpQueueList(IoTDAClient iotdaClient) throws ServiceResponseException {
        BatchShowQueueRequest request = new BatchShowQueueRequest();
        request.setInstanceId(INSTANCE_ID);
        BatchShowQueueResponse response = iotdaClient.batchShowQueue(request);
        logger.info("The AMQP list query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    /**
     * Query the Details About an AMQP Queue
     *
     * @param iotdaClient iotda client
     * @param queueId     Queue ID
     */
    private static void queryAmqpQueueDetail(IoTDAClient iotdaClient, String queueId) throws ServiceResponseException {
        ShowQueueRequest request = new ShowQueueRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setQueueId(queueId);
        ShowQueueResponse productResponse = iotdaClient.showQueue(request);
        logger.info("The AMQP queue details query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, productResponse));
    }

    /**
     * Delete an AMQP queue
     *
     * @param iotdaClient iotda client
     * @param queueId     Queue ID
     */
    private static void deleteAmqpQueue(IoTDAClient iotdaClient, String queueId) throws ServiceResponseException {
        DeleteQueueRequest request = new DeleteQueueRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setQueueId(queueId);
        DeleteQueueResponse response = iotdaClient.deleteQueue(request);
        logger.info("The AMQP Deletion result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    public static void main(String[] args) {
        // Initialize the IoTDA client
        IoTDAClient iotdaClient = initClient();
        // ID of the resource space to which the device belongs. Replace the value with the required one.
        String appId = "9d988b5efdf646f0828724c45e1d794e";

        try {
            logger.info("========1.Query amqpQueue list========");
            queryAmqpQueueList(iotdaClient);
            logger.info("========1.Query amqpQueue list completed========");

            logger.info("========2.create amqpQueue========");
            // Assemble the AMQP queue data. You can assemble the data according to the actual situation of the AMQP queue.
            QueueInfo addAmqpQueue = new QueueInfo();
            addAmqpQueue.setQueueName("myQueue_1");
            String queueId = createAmqpQueue(iotdaClient, addAmqpQueue);
            logger.info("========2.create amqpQueue completed========");

            logger.info("========3.Query amqpQueue list========");
            queryAmqpQueueList(iotdaClient);
            logger.info("========3.Query amqpQueue list completed========");

            logger.info("========4.Query amqpQueue detail========");
            queryAmqpQueueDetail(iotdaClient, queueId);
            logger.info("========4.Query amqpQueue detail completed========");

            logger.info("========5.delete amqpQueue========");
            deleteAmqpQueue(iotdaClient, queueId);
            logger.info("========5.delete amqpQueue completed========");

            logger.info("========6.Query amqpQueue list========");
            queryAmqpQueueList(iotdaClient);
            logger.info("========6.Query amqpQueue list completed========");
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.warn("Demonstration failed. Exception information is {}", e.getMessage());
        } catch (ServiceResponseException e) {
            logger.warn("Demonstration failed. Exception information is {}, {}", e.getErrorCode(), e.getErrorMsg());
        }
    }
}
