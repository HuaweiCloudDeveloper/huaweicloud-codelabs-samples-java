## 1. Functions

You can integrate the IoTDA server SDKs provided by Huawei Cloud to call IoTDA APIs and use IoTDA smoothly.
This example shows and demonstrates how to use the Java SDK to add, delete, query and query the list of an AMQP queue.

AMQP Queue: IoT platform pushes the update messages to the corresponding AMQP message queue according to user's subscribed data types. Users can connect the AMQP client to the IoT platform to receive data.

**What You Will Learn**

The ways to use the Java SDK to add, delete, query and query the list of an AMQP queue.

## 2. Prerequisites
- 1. You have created a Huawei Cloud account and obtained the access key (AK) and secret access key (SK) of your account. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 2. You have set up the development environment with Java JDK 1.8 or later.
- 3. You have obtained the project ID of the target region. View the project ID on the **My Credentials** > **API Credentials** page of the Huawei Cloud console. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_1001.html).
- 4. You have enabled IoTDA. For details about how to obtain the ID of the current instance, see [Viewing Instance Details](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0121.html).

## 3. Obtaining and Installing the SDK
Download and install Maven, and add dependencies to the **pom.xml** file of the Java project.
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4. API Parameters
For details about API parameters, see:

[Create an AMQP Queue](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0103.html)

[Query the AMQP List](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0102.html)

[Query an AMQP Queue](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0104.html)

[Delete an AMQP Queue](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0105.html)

## 5. Key Code Snippets
### 5.1 Create an AMQP Queue

```java
    /**
     * Create an AMQP queue
     *
     * @param iotdaClient iotda client
     * @param body        Structure for AMQP creation
     * @return AMQP queue ID
     */
    private static String createAmqpQueue(IoTDAClient iotdaClient, QueueInfo body) throws ServiceResponseException {
        AddQueueRequest request = new AddQueueRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        AddQueueResponse response = iotdaClient.addQueue(request);
        logger.info("The AMQP queue creation result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        return response.getQueueId();
    }
```

### 5.2 Query the AMQP List
```java
    /**
     * Query the AMQP List
     *
     * @param iotdaClient iotda client
     */
    private static void queryAmqpQueueList(IoTDAClient iotdaClient) throws ServiceResponseException {
        BatchShowQueueRequest request = new BatchShowQueueRequest();
        request.setInstanceId(INSTANCE_ID);
        BatchShowQueueResponse response = iotdaClient.batchShowQueue(request);
        logger.info("The AMQP list query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.3 Query the Details About an AMQP Queue

```java
    /**
     * Query the Details About an AMQP Queue
     *
     * @param iotdaClient iotda client
     * @param queueId     Queue ID
     */
    private static void queryAmqpQueueDetail(IoTDAClient iotdaClient, String queueId) throws ServiceResponseException {
        ShowQueueRequest request = new ShowQueueRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setQueueId(queueId);
        ShowQueueResponse productResponse = iotdaClient.showQueue(request);
        logger.info("The AMQP queue details query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, productResponse));
    }
```

### 5.4 Delete an AMQP queue

```java
    /**
     * Delete an AMQP queue
     *
     * @param iotdaClient iotda client
     * @param queueId     Queue ID
     */
    private static void deleteAmqpQueue(IoTDAClient iotdaClient, String queueId) throws ServiceResponseException {
        DeleteQueueRequest request = new DeleteQueueRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setQueueId(queueId);
        DeleteQueueResponse response = iotdaClient.deleteQueue(request);
        logger.info("The AMQP Deletion result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

## 6. Execution Results
Replace the parameters and execute the code in the sequence of the main method. The program demonstrates how to add, query and delete an AMQP queue. The execution results of each step of the main method are shown as follows.

### 6.1 Query the AMQP List(Before the AMQP Queue Creation)

Because the AMQP queue is not created yet, the query result is empty.
```json
{
  "queues": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```
### 6.2 Create an AMQP queue
```json
{
  "queue_id": "4871e9b6-ee17-406b-b165-8f8c4dba8e1e",
  "queue_name": "myQueue-1",
  "create_time": "2023-09-04T03:28:59.554Z",
  "last_modify_time": "2023-09-04T03:28:59.554Z"
}
```

### 6.3 Query the AMQP List(After the AMQP Queue Creation)
```json
{
  "queues": [
    {
      "queue_id": "4871e9b6-ee17-406b-b165-8f8c4dba8e1e",
      "queue_name": "myQueue-1",
      "create_time": "2023-09-04T03:28:59.554Z",
      "last_modify_time": "2023-09-04T03:28:59.554Z"
    }
  ],
  "page": {
    "count": 1,
    "marker": "5faa5c4538e6f101d16cf30f"
  }
}
```
### 6.4 Query the Details About an AMQP Queue
```json
{
  "queue_id": "4871e9b6-ee17-406b-b165-8f8c4dba8e1e",
  "queue_name": "myQueue-1",
  "create_time": "2023-09-04T03:28:59.554Z",
  "last_modify_time": "2023-09-04T03:28:59.554Z"
}
```

### 6.5 Delete an AMQP Queue
```json
{}
```
### 6.6 Query the AMQP List(After the AMQP Queue Deletion)
```json
{
  "queues": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```