## 1、功能介绍

华为云提供了IoTDA服务端SDK，您可以直接集成服务端SDK来调用IoTDA的相关API，从而实现对IoTDA的快速操作。
该示例展示了如何通过java版SDK来实现对amqp队列的新增，删除，查询，列表查询功能，同时通过demo进行简单的演示操作。

amqp队列：物联网平台根据用户订阅的数据类型，将对应的变更信息推送给指定的AMQP消息队列。用户可通过AMQP的客户端与IoT平台建立链接，来接收数据。

**您将学到什么？**

如何通过java版SDK来实现对amqp队列的新增，删除，查询，列表查询功能。

## 2、前置条件
- 1、已经开通华为云账号，并获得到您账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。您可以参考[获取项目ID](https://support.huaweicloud.com/api-iothub/iot_06_v5_1001.html)
- 4、已经开通IoTDA服务，获取当前实例的实例ID，请参考[查看实例详情](https://support.huaweicloud.com/usermanual-iothub/iot_01_0121.html) 。

## 3、SDK获取和安装
您需要下载并安装 Maven，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4、接口参数说明
关于接口参数的详细说明可参见：

[创建AMQP队列](https://support.huaweicloud.com/api-iothub/iot_06_v5_0103.html)

[查询AMQP列表](https://support.huaweicloud.com/api-iothub/iot_06_v5_0102.html)

[查询单个AMQP队列](https://support.huaweicloud.com/api-iothub/iot_06_v5_0104.html)

[删除AMQP队列](https://support.huaweicloud.com/api-iothub/iot_06_v5_0105.html)

## 5、关键代码片段
### 5.1、创建amqp队列

```java
    /**
     * 创建amqp队列
     *
     * @param iotdaClient iotda客户端
     * @param body        创建amqp结构体
     * @return amqp队列ID
     */
    private static String createAmqpQueue(IoTDAClient iotdaClient, QueueInfo body) throws ServiceResponseException {
        AddQueueRequest request = new AddQueueRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        AddQueueResponse response = iotdaClient.addQueue(request);
        logger.info("创建amqp队列的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
        return response.getQueueId();
    }
```

### 5.2、查询amqp队列列表

```java
    /**
     * 查询amqp队列
     *
     * @param iotdaClient iotda client
     */
    private static void queryAmqpQueueList(IoTDAClient iotdaClient) throws ServiceResponseException {
        BatchShowQueueRequest request = new BatchShowQueueRequest();
        request.setInstanceId(INSTANCE_ID);
        BatchShowQueueResponse response = iotdaClient.batchShowQueue(request);
        logger.info("查询amqp队列列表的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.3、查询amqp队列详情

```java
    /**
     * 查询amqp队列详情
     *
     * @param iotdaClient iotda client
     * @param queueId     队列ID
     */
    private static void queryAmqpQueueDetail(IoTDAClient iotdaClient, String queueId) throws ServiceResponseException {
        ShowQueueRequest request = new ShowQueueRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setQueueId(queueId);
        ShowQueueResponse productResponse = iotdaClient.showQueue(request);
        logger.info("查询amqp队列详情的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, productResponse));
    }
```

### 5.4、删除amqp队列

```java
    /**
     * 删除amqp队列
     *
     * @param iotdaClient iotda client
     * @param queueId     队列ID
     */
    private static void deleteAmqpQueue(IoTDAClient iotdaClient, String queueId) throws ServiceResponseException {
        DeleteQueueRequest request = new DeleteQueueRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setQueueId(queueId);
        DeleteQueueResponse response = iotdaClient.deleteQueue(request);
        logger.info("删除amqp队列的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

## 6、运行结果
如果您替换main方法的参数，并按照main方法的顺序执行，程序将演示amqp队列的新增，查询，删除功能。
以下为main方法中每一个步骤的运行结果。

### 6.1 查询amqp队列列表(创建amqp队列前)

由于还未创建amqp队列，查出来为空
```json
{
  "queues": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```
### 6.2 创建amqp队列
```json
{
  "queue_id": "4871e9b6-ee17-406b-b165-8f8c4dba8e1e",
  "queue_name": "myQueue-1",
  "create_time": "2023-09-04T03:28:59.554Z",
  "last_modify_time": "2023-09-04T03:28:59.554Z"
}
```

### 6.3 查询amqp队列列表(创建amqp队列后)
```json
{
  "queues": [
    {
      "queue_id": "4871e9b6-ee17-406b-b165-8f8c4dba8e1e",
      "queue_name": "myQueue-1",
      "create_time": "2023-09-04T03:28:59.554Z",
      "last_modify_time": "2023-09-04T03:28:59.554Z"
    }
  ],
  "page": {
    "count": 1,
    "marker": "5faa5c4538e6f101d16cf30f"
  }
}
```
### 6.4 查询amqp队列详情
```json
{
  "queue_id": "4871e9b6-ee17-406b-b165-8f8c4dba8e1e",
  "queue_name": "myQueue-1",
  "create_time": "2023-09-04T03:28:59.554Z",
  "last_modify_time": "2023-09-04T03:28:59.554Z"
}
```

### 6.5 删除amqp队列
```json
{}
```
### 6.6 查询amqp队列列表(删除amqp队列后）
```json
{
  "queues": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```