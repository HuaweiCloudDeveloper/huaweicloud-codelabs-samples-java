package com.huawei.iotda;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.JsonUtils;
import com.huaweicloud.sdk.iotda.v5.IoTDAClient;
import com.huaweicloud.sdk.iotda.v5.model.CreateBatchTask;
import com.huaweicloud.sdk.iotda.v5.model.CreateBatchTaskRequest;
import com.huaweicloud.sdk.iotda.v5.model.CreateBatchTaskResponse;
import com.huaweicloud.sdk.iotda.v5.model.CreateCommandRequest;
import com.huaweicloud.sdk.iotda.v5.model.CreateCommandResponse;
import com.huaweicloud.sdk.iotda.v5.model.DeviceCommandRequest;
import com.huaweicloud.sdk.iotda.v5.model.ShowBatchTaskRequest;
import com.huaweicloud.sdk.iotda.v5.model.ShowBatchTaskResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeviceCommandsManagement {
    private static final Logger logger = LoggerFactory.getLogger(DeviceCommandsManagement.class);

    // REGION_ID：If the region is 上海一，please fill in "cn-east-3"；if the region is 北京四，please fill in"cn-north-4";if the region is 华南广州，please fill in "cn-south-4".
    private static final String REGION_ID = "<YOUR REGION ID>";

    // ENDPOINT：On the console, choose **Overview** in the navigation pane and click **Access Addresses** to view the HTTPS application access address.
    private static final String ENDPOINT = "<YOUR ENDPOINT>";

    // ak/sk：For details, see the method of obtaining AK/SK in the following IoTDA document https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0091.html.
    // There will be security risks if the AK/SK used for authentication is directly written into code. We suggest encrypting the AK/SK in the configuration file or environment variables for storage.
    // In this sample, the AK/SK is stored in environment variables for identity authentication. Before running this sample, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");
    private static final String PROJECT_ID = "<YOUR PROJECTID>";

    // Instance id
    private static final String INSTANCE_ID = "<YOUR INSTANCEID>";

    // Used for displaying the response body
    private static final ObjectMapper OBJECTMAPPER = new ObjectMapper();

    private static IoTDAClient initClient() {
        // Create a credential
        ICredential auth = new BasicCredentials()
            .withProjectId(PROJECT_ID)
            .withAk(AK)
            .withSk(SK)
            // Derivative algorithms are used for the standard and enterprise editions.
            // For the basic edition, delete the configuration "withDerivedPredicate".
            .withDerivedPredicate(BasicCredentials.DEFAULT_DERIVED_PREDICATE);

        // Create and initialize an IoTDAClient instance
        return IoTDAClient.newBuilder()
            // Use IoTDARegion for basic editions like "withRegion(IoTDARegion.CN_NORTH_4)". Create regions for standard/enterprise editions.
            // .withRegion(IoTDARegion.CN_NORTH_4)
            .withRegion(new Region(REGION_ID, ENDPOINT))
            .withCredential(auth)
            .build();
    }

    /**
     * Create a device command.
     *
     * @param iotdaClient iotda client
     * @param deviceId    Command recipient device
     * @param body        Command to deliver
     */
    private static void createCommand(IoTDAClient iotdaClient, String deviceId, DeviceCommandRequest body) throws ServiceResponseException {
        CreateCommandRequest request = new CreateCommandRequest();
        request.setDeviceId(deviceId);
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        CreateCommandResponse response = iotdaClient.createCommand(request);
        logger.info("The command delivery result is {}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    /**
     * Create a batch task
     *
     * @param iotdaClient     iotda client
     * @param createBatchTask Structure for creating a batch task
     * @return Task ID obtained after the creation
     */
    private static String createTask(IoTDAClient iotdaClient, CreateBatchTask createBatchTask) throws ServiceResponseException {
        CreateBatchTaskRequest request = new CreateBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(createBatchTask);
        CreateBatchTaskResponse batchTask = iotdaClient.createBatchTask(request);
        logger.info("The command delivery result is {}", JsonUtils.toJSON(OBJECTMAPPER, batchTask));
        return batchTask.getTaskId();
    }

    /**
     * Query details about a batch task
     *
     * @param iotdaClient iotda client
     * @param taskId      Task ID
     */
    private static ShowBatchTaskResponse queryTask(IoTDAClient iotdaClient, String taskId) throws ServiceResponseException {
        ShowBatchTaskRequest request = new ShowBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setTaskId(taskId);
        ShowBatchTaskResponse showBatchTaskResponse = iotdaClient.showBatchTask(request);
        logger.info("The task details query result is {}", JsonUtils.toJSON(OBJECTMAPPER, showBatchTaskResponse));
        return showBatchTaskResponse;
    }

    public static void main(String[] args) {
        // Initialize the IoTDA client
        IoTDAClient iotdaClient = initClient();
        // ID of the command recipient device. Replace the value with the required one.
        String deviceId = "64111bcd06ca5933b28a058b_7758dsfdsfsd";
        // ID of the resource space to which the device belongs. Replace the value with the required one.
        String appId = "9d988b5efdf646f0828724c45e1d794e";

        try {
            logger.info("========1.Send a device command========");
            // Command sending structure
            DeviceCommandRequest commandRequest = new DeviceCommandRequest();
            // Service ID defined in the product. Replace the value with the required one.
            commandRequest.setServiceId("LightControl");
            // Name of the command to deliver in the service. Replace the value with the required one.
            commandRequest.setCommandName("switch");
            // Enter the properties (and corresponding values) of the command in the map.
            Map<String, Object> hashMap = new HashMap<>();
            hashMap.put("value", "ON");
            commandRequest.setParas(hashMap);
            // Command delivery
            createCommand(iotdaClient, deviceId, commandRequest);
            logger.info("========1.Device command sent========");

            logger.info("========2.Create a batch command delivery task========");
            // You can send multiple commands in batches at a time using batch commands delivery.
            CreateBatchTask createBatchTask = new CreateBatchTask();
            createBatchTask.setAppId(appId);
            // Task name is unique in the resource space. If this interface is called for multiple times, the task name needs to be changed.
            createBatchTask.setTaskName("batchCommand");
            // The task type is fixed to createCommands. Do not change the value.
            createBatchTask.setTaskType("createCommands");
            // Target device ID. Add the target device ID to the list.
            List<String> targetDeviceIds = new ArrayList<>();
            targetDeviceIds.add(deviceId);
            createBatchTask.setTargets(targetDeviceIds);
            // Command to deliver
            createBatchTask.setDocument(commandRequest);
            // Obtain the task ID after a task is created.
            String taskId = createTask(iotdaClient, createBatchTask);
            logger.info("========2.Batch command delivery task created========");

            logger.info("========3.Query details about a batch task========");
            String status = "Initializing";
            int times = 0;
            // Wait until the task is complete. Task status description: Initializing, Waitting, Processing, Success, Fail, PartialSuccess, Stopped, Stopping.
            while (("Initializing".equals(status) || "Waitting".equals(status) || "Processing".equals(status)) && times++ < 5) {
                // Query the execution details of a batch task to learn about the command delivery status.
                ShowBatchTaskResponse showBatchTaskResponse = queryTask(iotdaClient, taskId);
                status = showBatchTaskResponse.getBatchtask().getStatus();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    logger.warn("Exception information is {}", e.getMessage());
                }
            }
            logger.info("========3.Batch task details queried========");
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.warn("Demonstration failed. Exception information is {}", e.getMessage());
        } catch (ServiceResponseException e) {
            logger.warn("Demonstration failed. Exception information is {}, {}", e.getErrorCode(), e.getErrorMsg());
        }
    }
}