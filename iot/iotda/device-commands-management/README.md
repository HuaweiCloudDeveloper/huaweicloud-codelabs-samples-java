## 1、功能介绍

华为云提供了IoTDA服务端SDK，您可以直接集成服务端SDK来调用IoTDA的相关API，从而实现对IoTDA的快速操作。
该示例展示了如何通过java版SDK来实现设备命令下发，批量设备命令下发，查询批量任务。

命令下发：为能有效地对设备进行管理，设备的产品模型中定义了物联网平台可向设备下发的命令，
应用服务器可以调用物联网平台应用侧API接口向设备下发命令，以实现对设备的远程控制。
详情请参考[同步命令下发](https://support.huaweicloud.com/usermanual-iothub/iot_01_0341.html)

**您将学到什么？**

如何通过java版SDK来处理设备命令的下发，如何通过批量任务实现批量命令下发，查询批量任务详情。

## 2、前置条件
- 1、已经开通华为云账号，并获得到您账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。您可以参考[获取项目ID](https://support.huaweicloud.com/api-iothub/iot_06_v5_1001.html)
- 4、已经开通IoTDA服务，获取当前实例的实例ID，请参考[查看实例详情](https://support.huaweicloud.com/usermanual-iothub/iot_01_0121.html) 。 
- 5、并且存在一个设备，建议设备已经连接到平台，你可以具体看到平台与设备交互。

## 3、SDK获取和安装
您需要下载并安装 Maven，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4、接口参数说明
关于接口参数的详细说明可参见：

[下发设备命令](https://support.huaweicloud.com/api-iothub/iot_06_v5_0038.html)

[创建批量任务](https://support.huaweicloud.com/api-iothub/iot_06_v5_0045.html)

[查询批量任务](https://support.huaweicloud.com/api-iothub/iot_06_v5_0017.html)

## 5、关键代码片段
### 5.1、下发设备命令

```java
    /**
     * 创建设备命令
     *
     * @param iotdaClient iotda client
     * @param deviceId    需要下发命令的设备
     * @param body        需要下发的命令
     */
    private static void createCommand(IoTDAClient iotdaClient, String deviceId, DeviceCommandRequest body) throws ServiceResponseException {
        CreateCommandRequest request = new CreateCommandRequest();
        request.setDeviceId(deviceId);
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        CreateCommandResponse response = iotdaClient.createCommand(request);
        logger.info("设备命令下发的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.2、创建批量任务
```java
    /**
     * 创建批量任务
     *
     * @param iotdaClient     iotda client
     * @param createBatchTask 创建批量任务结构体
     * @return 创建完成后获取的任务Id
     */
    private static String createTask(IoTDAClient iotdaClient, CreateBatchTask createBatchTask) throws ServiceResponseException {
        CreateBatchTaskRequest request = new CreateBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(createBatchTask);
        CreateBatchTaskResponse batchTask = iotdaClient.createBatchTask(request);
        logger.info("设备命令下发的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, batchTask));
        return batchTask.getTaskId();
    }
```

### 5.3、查询批量任务详情
```java
    /**
     * 查询批量任务详情
     *
     * @param iotdaClient iotda client
     * @param taskId      任务Id
     */
    private static ShowBatchTaskResponse queryTask(IoTDAClient iotdaClient, String taskId) throws ServiceResponseException {
        ShowBatchTaskRequest request = new ShowBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setTaskId(taskId);
        ShowBatchTaskResponse showBatchTaskResponse = iotdaClient.showBatchTask(request);
        logger.info("查询任务详情的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, showBatchTaskResponse));
        return showBatchTaskResponse;
    }
```

## 6、运行结果
如果您替换main方法的参数，并按照main方法的顺序执行，程序将演示将命令发送给设备。
以下为main方法中每一个步骤的运行结果。

### 6.1 下发设备命令

如果设备在线，且正确回复响应就会得到以下结果
```json
{
  "command_id": "b7919b1a-8942-459b-8dc6-abe7694445cc",
  "response": {
    "result_code": 0
  }
}
```
### 6.2 创建批量任务
```json
{
  "task_id": "64f080b78c2b6271ddeb0861",
  "task_name": "batchCommand",
  "task_type": "createCommands",
  "targets": [
    "64111bcd06ca5933b28a058b_7758dsfdsfsd"
  ],
  "targets_filter": {},
  "document": {
    "service_id": "LightControl",
    "command_name": "switch",
    "paras": {
      "value": "ON"
    }
  },
  "task_policy": {},
  "status": "Initializing",
  "task_progress": {
    "total": 0,
    "processing": 0,
    "success": 0,
    "fail": 0,
    "waitting": 0,
    "fail_wait_retry": 0,
    "stopped": 0,
    "removed": 0
  },
  "create_time": "20230831T115951Z"
}
```
### 6.3 查询任务详情
```json
{
  "batchtask": {
    "task_id": "64f080b78c2b6271ddeb0861",
    "task_name": "batchCommand",
    "task_type": "createCommands",
    "targets": [
      "64111bcd06ca5933b28a058b_7758dsfdsfsd"
    ],
    "targets_filter": {},
    "document": {
      "service_id": "LightControl",
      "command_name": "switch",
      "paras": {
        "value": "ON"
      }
    },
    "task_policy": {},
    "status": "Success",
    "status_desc": "",
    "task_progress": {
      "total": 1,
      "processing": 0,
      "success": 1,
      "fail": 0,
      "waitting": 0,
      "fail_wait_retry": 0,
      "stopped": 0,
      "removed": 0
    },
    "create_time": "20230831T115951Z"
  },
  "task_details": [
    {
      "target": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
      "status": "Success",
      "output": "command_id:3d25684b-e819-43e0-9a7f-77686636cbe1, command_status:success",
      "error": {}
    }
  ],
  "page": {
    "count": 1,
    "marker": "64f080b78c2b6271ddeb0863"
  }
}
```