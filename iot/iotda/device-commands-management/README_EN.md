## 1. Functions

You can integrate the IoTDA server SDKs provided by Huawei Cloud to call IoTDA APIs and use IoTDA smoothly.
This example shows how to use the Java SDK to deliver commands to devices (individually or in batches) and query batch tasks.

Command delivery: Product models define the commands that can be delivered by the IoT platform to the device for effective management.
Applications can call application-side APIs to deliver commands to devices for remote control.
For details, see [Synchronous Command Delivery](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0341.html).

**What You Will Learn**

The ways to use the Java SDK to deliver device commands, deliver commands in batches through batch tasks, and query batch task details.

## 2. Prerequisites
- 1. You have created a Huawei Cloud account and obtained the access key (AK) and secret access key (SK) of your account. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 2. You have set up the development environment with Java JDK 1.8 or later.
- 3. You have obtained the project ID of the target region. View the project ID on the **My Credentials** > **API Credentials** page of the Huawei Cloud console. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_1001.html).
- 4. You have enabled IoTDA. For details about how to obtain the ID of the current instance, see [Viewing Instance Details](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0121.html).
- 5. You have created a device and connected it to the platform for viewing interconnection details.

## 3. Obtaining and Installing the SDK
Download and install Maven, and add dependencies to the **pom.xml** file of the Java project.
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4. API Parameters
For details about API parameters, see:

[Deliver a Command to a Device](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0038.html)

[Create a Batch Task](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0045.html)

[Query a Batch Task](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0017.html)

## 5. Key Code Snippets
### 5.1 Deliver a Device Command

```java
    /**
     * Create a device command
     *
     * @param iotdaClient iotda client
     * @param deviceId    Command recipient device
     * @param body        Command to deliver
     */
    private static void createCommand(IoTDAClient iotdaClient, String deviceId, DeviceCommandRequest body) throws ServiceResponseException {
        CreateCommandRequest request = new CreateCommandRequest();
        request.setDeviceId(deviceId);
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        CreateCommandResponse response = iotdaClient.createCommand(request);
        logger.info("The command delivery result is {}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.2 Create a Batch Task
```java
    /**
     * Create a batch task
     *
     * @param iotdaClient     iotda client
     * @param createBatchTask Structure for creating a batch task
     * @return Task ID obtained after the creation
     */
    private static String createTask(IoTDAClient iotdaClient, CreateBatchTask createBatchTask) throws ServiceResponseException {
        CreateBatchTaskRequest request = new CreateBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(createBatchTask);
        CreateBatchTaskResponse batchTask = iotdaClient.createBatchTask(request);
        logger.info("The command delivery result is {}", JsonUtils.toJSON(OBJECTMAPPER, batchTask));
        return batchTask.getTaskId();
    }
```

### 5.3 Query Details About a Batch Task
```java
    /**
     * Query details about a batch task
     *
     * @param iotdaClient iotda client
     * @param taskId      Task ID
     */
    private static ShowBatchTaskResponse queryTask(IoTDAClient iotdaClient, String taskId) throws ServiceResponseException {
        ShowBatchTaskRequest request = new ShowBatchTaskRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setTaskId(taskId);
        ShowBatchTaskResponse showBatchTaskResponse = iotdaClient.showBatchTask(request);
        logger.info("The task details query result is {}", JsonUtils.toJSON(OBJECTMAPPER, showBatchTaskResponse)).
        return showBatchTaskResponse;
    }
```

## 6. Execution Results
Replace the parameters and execute the code in the sequence of the main method. The program demonstrates how to send the command to the device.
The execution results of each step of the main method are shown as follows.

### 6.1 Deliver a Device Command

If the device is online and correctly responds, the following results will be obtained:
```json
{
  "command_id": "b7919b1a-8942-459b-8dc6-abe7694445cc",
  "response": {
    "result_code": 0
  }
}
```
### 6.2 Create a Batch Task
```json
{
  "task_id": "64f080b78c2b6271ddeb0861",
  "task_name": "batchCommand",
  "task_type": "createCommands",
  "targets": [
    "64111bcd06ca5933b28a058b_7758dsfdsfsd"
  ],
  "targets_filter": {},
  "document": {
    "service_id": "LightControl",
    "command_name": "switch",
    "paras": {
      "value": "ON"
    }
  },
  "task_policy": {},
  "status": "Initializing",
  "task_progress": {
    "total": 0,
    "processing": 0,
    "success": 0,
    "fail": 0,
    "waitting": 0,
    "fail_wait_retry": 0,
    "stopped": 0,
    "removed": 0
  },
  "create_time": "20230831T115951Z"
}
```
### 6.3 Query Task Details
```json
{
  "batchtask": {
    "task_id": "64f080b78c2b6271ddeb0861",
    "task_name": "batchCommand",
    "task_type": "createCommands",
    "targets": [
      "64111bcd06ca5933b28a058b_7758dsfdsfsd"
    ],
    "targets_filter": {},
    "document": {
      "service_id": "LightControl",
      "command_name": "switch",
      "paras": {
        "value": "ON"
      }
    },
    "task_policy": {},
    "status": "Success",
    "status_desc": "",
    "task_progress": {
      "total": 1,
      "processing": 0,
      "success": 1,
      "fail": 0,
      "waitting": 0,
      "fail_wait_retry": 0,
      "stopped": 0,
      "removed": 0
    },
    "create_time": "20230831T115951Z"
  },
  "task_details": [
    {
      "target": "64111bcd06ca5933b28a058b_7758dsfdsfsd",
      "status": "Success",
      "output": "command_id:3d25684b-e819-43e0-9a7f-77686636cbe1, command_status:success",
      "error": {}
    }
  ],
  "page": {
    "count": 1,
    "marker": "64f080b78c2b6271ddeb0863"
  }
}
```
