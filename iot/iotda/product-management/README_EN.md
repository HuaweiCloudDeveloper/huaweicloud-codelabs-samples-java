## 1. Functions

You can integrate the IoTDA server SDKs provided by Huawei Cloud to call IoTDA APIs and use IoTDA smoothly.
This example shows and demonstrates how to use the Java SDK to add, modify, delete, query a product as well as query the product list.

Product Model: A product model describes the capabilities and features of a device. You can build an abstract model of a device by defining a product model on the IoT platform so that the platform can know what services, properties, and commands are supported by the device, such as its color or any on/off switches. After defining a product model, you can use it during device registration.
For details, see [Product Model Definition](https://support.huaweicloud.com/intl/en-us/devg-iothub/iot_01_0017.html)

**What You Will Learn**

The ways to use the Java SDK to add, modify, delete, query a product as well as query the product list.

## 2. Prerequisites
- 1. You have created a Huawei Cloud account and obtained the access key (AK) and secret access key (SK) of your account. To create and view an AK/SK, go to the **My Credentials** > **Access Keys** page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 2. You have set up the development environment with Java JDK 1.8 or later.
- 3. You have obtained the project ID of the target region. View the project ID on the **My Credentials** > **API Credentials** page of the Huawei Cloud console. For details, see [Obtaining a Project ID](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_1001.html).
- 4. You have enabled IoTDA. For details about how to obtain the ID of the current instance, see [Viewing Instance Details](https://support.huaweicloud.com/intl/en-us/usermanual-iothub/iot_01_0121.html).

## 3. Obtaining and Installing the SDK
Download and install Maven, and add dependencies to the **pom.xml** file of the Java project.
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4. API Parameters
For details about API parameters, see:

[Create a Product](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0050.html)

[Query the Product List](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0080.html)

[Query a Product](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0052.html)

[Modify a Product](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0054.html)

[Delete a Product](https://support.huaweicloud.com/intl/en-us/api-iothub/iot_06_v5_0056.html)

## 5. Key Code Snippets
### 5.1 Create a Product
```java
    /**
     * Create a Product
     *
     * @param iotdaClient iotda client
     * @param body        Structure for creating a product
     * @return Product ID
     */
    private static String createProduct(IoTDAClient iotdaClient, AddProduct body) throws ServiceResponseException {
        CreateProductRequest request = new CreateProductRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        CreateProductResponse product = iotdaClient.createProduct(request);
        logger.info("The product creation result is:{}", JsonUtils.toJSON(OBJECTMAPPER, product));
        return product.getProductId();
    }
```

### 5.2 Query the Product List
```java
    /**
     * Query the product list under an app
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space id
     */
    private static void queryProductList(IoTDAClient iotdaClient, String appId) throws ServiceResponseException {
        ListProductsRequest request = new ListProductsRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        ListProductsResponse response = iotdaClient.listProducts(request);
        logger.info("The product list query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.3 Query the Details About a Product

```java
    /**
     * Query the Details About a Product
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space id
     * @param productId   Product id
     */
    private static void queryProductDetail(IoTDAClient iotdaClient, String appId, String productId) throws ServiceResponseException {
        ShowProductRequest request = new ShowProductRequest();
        request.setAppId(appId);
        request.setInstanceId(INSTANCE_ID);
        request.setProductId(productId);
        ShowProductResponse productResponse = iotdaClient.showProduct(request);
        logger.info("The product details query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, productResponse));
    }
```

### 5.4 Update a Product
```java
    /**
     * Update a Product
     *
     * @param iotdaClient iotda client
     * @param productId   Product id
     * @param body        Structure for updating a product
     */
    private static void updateProduct(IoTDAClient iotdaClient, String productId, UpdateProduct body) throws ServiceResponseException {
        UpdateProductRequest request = new UpdateProductRequest();
        request.setProductId(INSTANCE_ID);
        request.setBody(body);
        request.setProductId(productId);
        UpdateProductResponse updateProduct = iotdaClient.updateProduct(request);
        logger.info("The product update result is:{}", JsonUtils.toJSON(OBJECTMAPPER, updateProduct));
    }
```

### 5.5 Delete a Product
```java
    /**
     * Delete a Product
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space id
     * @param productId   ID of the product that needs to be deleted
     */
    private static void deleteProduct(IoTDAClient iotdaClient, String appId, String productId) throws ServiceResponseException {
        DeleteProductRequest request = new DeleteProductRequest();
        request.setAppId(appId);
        request.setInstanceId(INSTANCE_ID);
        request.setProductId(productId);
        DeleteProductResponse response = iotdaClient.deleteProduct(request);
        logger.info("The product deletion result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

## 6. Execution Results
Replace the parameters and execute the code in the sequence of the main method. The program demonstrates how to add, modify, delete, query a product as well as query the product list.
The execution results of each step of the main method are shown as follows.

### 6.1 Query the Product List(Before the Product Creation)

Because the product is not created yet, the query result is empty.
```json
{
  "products": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```
### 6.2 Create a Product
```json
{
  "app_id": "70446fee4b8f457a9b214a5248dcf80c",
  "app_name": "sdktest",
  "product_id": "64f156c1c0a35711e14b4ff4",
  "name": "SmartStreetLamp",
  "device_type": "SmartStreetLamp",
  "protocol_type": "MQTT",
  "data_format": "json",
  "manufacturer_name": "huawei",
  "industry": "Smart city",
  "description": "This is a product sample",
  "service_capabilities": [
    {
      "service_id": "BasicData",
      "service_type": "BasicData",
      "properties": [
        {
          "property_name": "luminance",
          "data_type": "decimal",
          "required": false,
          "min": "0",
          "max": "100",
          "max_length": 1024,
          "step": 0.0,
          "method": "RW",
          "description": "Street Lamp Brightness Properties"
        }
      ],
      "description": "Basic data of street lamps",
      "option": "Optional"
    }
  ],
  "create_time": "20230901T031305Z"
}
```

### 6.3 Query the Product List(After the Product Creation)
```json
{
  "products": [
    {
      "app_id": "70446fee4b8f457a9b214a5248dcf80c",
      "app_name": "SDKTEST",
      "product_id": "64f156c1c0a35711e14b4ff4",
      "name": "SmartStreetLamp",
      "device_type": "SmartStreetLamp",
      "protocol_type": "MQTT",
      "data_format": "json",
      "manufacturer_name": "huawei",
      "industry": "Smart city",
      "description": "This is a product sample",
      "create_time": "20230901T031305Z"
    }
  ],
  "page": {
    "count": 1,
    "marker": "64f156c1c0a35711e14b4ff6"
  }
}
```
### 6.4 Query the Details About a Product
```json
{
  "app_id": "70446fee4b8f457a9b214a5248dcf80c",
  "app_name": "SDKTEST",
  "product_id": "64f156c1c0a35711e14b4ff4",
  "name": "SmartStreetLamp",
  "device_type": "SmartStreetLamp",
  "protocol_type": "MQTT",
  "data_format": "json",
  "manufacturer_name": "huawei",
  "industry": "Smart city",
  "description": "This is a product sample",
  "service_capabilities": [
    {
      "service_id": "BasicData",
      "service_type": "BasicData",
      "properties": [
        {
          "property_name": "luminance",
          "data_type": "decimal",
          "required": false,
          "min": "0",
          "max": "100",
          "max_length": 1024,
          "step": 0.0,
          "method": "RW",
          "description": "Street Lamp Brightness Properties"
        }
      ],
      "description": "Basic data of street lamps",
      "option": "Optional"
    }
  ],
  "create_time": "20230901T031305Z"
}
```
### 6.5 Update a Product
```json
{
  "app_id": "70446fee4b8f457a9b214a5248dcf80c",
  "app_name": "SDKTEST",
  "product_id": "64f156c1c0a35711e14b4ff4",
  "name": "SmartStreetLamp",
  "device_type": "SmartStreetLamp",
  "protocol_type": "MQTT",
  "data_format": "json",
  "manufacturer_name": "huawei",
  "industry": "Smart city",
  "description": "This is a product sample",
  "service_capabilities": [
    {
      "service_id": "BasicData",
      "service_type": "BasicData",
      "properties": [
        {
          "property_name": "luminance",
          "data_type": "decimal",
          "required": false,
          "min": "0",
          "max": "100",
          "max_length": 1024,
          "step": 0.0,
          "method": "RW",
          "description": "Street Lamp Brightness Properties"
        }
      ],
      "description": "Basic data of street lamps",
      "option": "Optional"
    }
  ],
  "create_time": "20230901T031305Z"
}
```

### 6.6 Delete a Product
```json
{}
```
### 6.7 Query the Product List(After the Product Deletion)
```json
{
  "products": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```