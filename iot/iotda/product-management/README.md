## 1、功能介绍

华为云提供了IoTDA服务端SDK，您可以直接集成服务端SDK来调用IoTDA的相关API，从而实现对IoTDA的快速操作。
该示例展示了如何通过java版SDK来实现对产品的新增，修改，删除，查询，列表查询功能，同时通过demo进行简单的演示操作。

产品模型：产品模型用于描述设备具备的能力和特性。开发者通过定义产品模型，在物联网平台构建一款设备的抽象模型，使平台理解该款设备支持的服务、属性、命令等信息，如颜色、开关等。当定义完一款产品模型后，在进行注册设备时，就可以使用在控制台上定义的产品模型。
详情请参考[什么是产品模型](https://support.huaweicloud.com/devg-iothub/iot_01_0017.html)

**您将学到什么？**

如何通过java版SDK来实现对产品的新增，修改，删除，查询，列表查询功能。

## 2、前置条件
- 1、已经开通华为云账号，并获得到您账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 2、已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 3、获取您对应区域的项目ID，请在控制台“我的凭证 > API凭证”页面上查看项目ID。您可以参考[获取项目ID](https://support.huaweicloud.com/api-iothub/iot_06_v5_1001.html)
- 4、已经开通IoTDA服务，获取当前实例的实例ID，请参考[查看实例详情](https://support.huaweicloud.com/usermanual-iothub/iot_01_0121.html) 。

## 3、SDK获取和安装
您需要下载并安装 Maven，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-iotda</artifactId>
    <version>[3.0.40-rc, 3.2.0)</version>
</dependency>
```
```xml
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.36</version>
</dependency>
```

## 4、接口参数说明
关于接口参数的详细说明可参见：

[创建产品](https://support.huaweicloud.com/api-iothub/iot_06_v5_0050.html)

[查询产品列表](https://support.huaweicloud.com/api-iothub/iot_06_v5_0080.html)

[查询产品](https://support.huaweicloud.com/api-iothub/iot_06_v5_0052.html)

[修改产品](https://support.huaweicloud.com/api-iothub/iot_06_v5_0054.html)

[删除产品](https://support.huaweicloud.com/api-iothub/iot_06_v5_0056.html)

## 5、关键代码片段
### 5.1、创建产品
```java
    /**
     * 创建产品
     *
     * @param iotdaClient iotda client
     * @param body        新增产品结构体
     * @return 返回产品id
     */
    private static String createProduct(IoTDAClient iotdaClient, AddProduct body) throws ServiceResponseException {
        CreateProductRequest request = new CreateProductRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        CreateProductResponse product = iotdaClient.createProduct(request);
        logger.info("创建产品的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, product));
        return product.getProductId();
    }
```

### 5.2、查询产品列表
```java
    /**
     * 查询app下产品列表
     *
     * @param iotdaClient iotda client
     * @param appId       资源空间id
     */
    private static void queryProductList(IoTDAClient iotdaClient, String appId) throws ServiceResponseException {
        ListProductsRequest request = new ListProductsRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        ListProductsResponse response = iotdaClient.listProducts(request);
        logger.info("查询产品列表的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

### 5.3、查询产品详情

```java
    /**
     * 查询产品详情
     *
     * @param iotdaClient iotda client
     * @param appId       资源空间id
     * @param productId   产品id
     */
    private static void queryProductDetail(IoTDAClient iotdaClient, String appId, String productId) throws ServiceResponseException {
        ShowProductRequest request = new ShowProductRequest();
        request.setAppId(appId);
        request.setInstanceId(INSTANCE_ID);
        request.setProductId(productId);
        ShowProductResponse productResponse = iotdaClient.showProduct(request);
        logger.info("查询产品详情的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, productResponse));
    }
```

### 5.4、更新产品
```java
    /**
     * 更新产品
     *
     * @param iotdaClient iotda client
     * @param productId   产品id
     * @param body        修改产品结构体
     */
    private static void updateProduct(IoTDAClient iotdaClient, String productId, UpdateProduct body) throws ServiceResponseException {
        UpdateProductRequest request = new UpdateProductRequest();
        request.setProductId(INSTANCE_ID);
        request.setBody(body);
        request.setProductId(productId);
        UpdateProductResponse updateProduct = iotdaClient.updateProduct(request);
        logger.info("更新产品的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, updateProduct));
    }
```

### 5.5、删除产品
```java
    /**
     * 删除产品
     *
     * @param iotdaClient iotda client
     * @param appId       资源空间id
     * @param productId   需要删除的产品id
     */
    private static void deleteProduct(IoTDAClient iotdaClient, String appId, String productId) throws ServiceResponseException {
        DeleteProductRequest request = new DeleteProductRequest();
        request.setAppId(appId);
        request.setInstanceId(INSTANCE_ID);
        request.setProductId(productId);
        DeleteProductResponse response = iotdaClient.deleteProduct(request);
        logger.info("删除产品的结果为:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }
```

## 6、运行结果
如果您替换main方法的参数，并按照main方法的顺序执行，程序将演示产品的新增，查询，修改，删除功能。
以下为main方法中每一个步骤的运行结果。

### 6.1 查询产品列表(创建产品前)

由于还未创建产品，查出来为空
```json
{
  "products": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```
### 6.2 创建产品
```json
{
  "app_id": "70446fee4b8f457a9b214a5248dcf80c",
  "app_name": "sdktest",
  "product_id": "64f156c1c0a35711e14b4ff4",
  "name": "智能路灯",
  "device_type": "智能路灯",
  "protocol_type": "MQTT",
  "data_format": "json",
  "manufacturer_name": "huawei",
  "industry": "智慧城市",
  "description": "这是一个产品样例",
  "service_capabilities": [
    {
      "service_id": "BasicData",
      "service_type": "BasicData",
      "properties": [
        {
          "property_name": "luminance",
          "data_type": "decimal",
          "required": false,
          "min": "0",
          "max": "100",
          "max_length": 1024,
          "step": 0.0,
          "method": "RW",
          "description": "路灯亮度属性"
        }
      ],
      "description": "路灯的基本数据",
      "option": "Optional"
    }
  ],
  "create_time": "20230901T031305Z"
}
```

### 6.3 查询产品列表(创建产品后)
```json
{
  "products": [
    {
      "app_id": "70446fee4b8f457a9b214a5248dcf80c",
      "app_name": "SDKTEST",
      "product_id": "64f156c1c0a35711e14b4ff4",
      "name": "智能路灯",
      "device_type": "智能路灯",
      "protocol_type": "MQTT",
      "data_format": "json",
      "manufacturer_name": "huawei",
      "industry": "智慧城市",
      "description": "这是一个产品样例",
      "create_time": "20230901T031305Z"
    }
  ],
  "page": {
    "count": 1,
    "marker": "64f156c1c0a35711e14b4ff6"
  }
}
```
### 6.4 查询产品详情
```json
{
  "app_id": "70446fee4b8f457a9b214a5248dcf80c",
  "app_name": "SDKTEST",
  "product_id": "64f156c1c0a35711e14b4ff4",
  "name": "智能路灯",
  "device_type": "智能路灯",
  "protocol_type": "MQTT",
  "data_format": "json",
  "manufacturer_name": "huawei",
  "industry": "智慧城市",
  "description": "这是一个产品样例",
  "service_capabilities": [
    {
      "service_id": "BasicData",
      "service_type": "BasicData",
      "properties": [
        {
          "property_name": "luminance",
          "data_type": "decimal",
          "required": false,
          "min": "0",
          "max": "100",
          "max_length": 1024,
          "step": 0.0,
          "method": "RW",
          "description": "路灯亮度属性"
        }
      ],
      "description": "路灯的基本数据",
      "option": "Optional"
    }
  ],
  "create_time": "20230901T031305Z"
}
```
### 6.5 更新产品
```json
{
  "app_id": "70446fee4b8f457a9b214a5248dcf80c",
  "app_name": "SDKTEST",
  "product_id": "64f156c1c0a35711e14b4ff4",
  "name": "智能路灯",
  "device_type": "智能路灯",
  "protocol_type": "MQTT",
  "data_format": "json",
  "manufacturer_name": "huawei",
  "industry": "智慧城市",
  "description": "这是一个产品样例",
  "service_capabilities": [
    {
      "service_id": "BasicData",
      "service_type": "BasicData",
      "properties": [
        {
          "property_name": "luminance",
          "data_type": "decimal",
          "required": false,
          "min": "0",
          "max": "100",
          "max_length": 1024,
          "step": 0.0,
          "method": "RW",
          "description": "路灯亮度属性"
        }
      ],
      "description": "路灯的基本数据",
      "option": "Optional"
    }
  ],
  "create_time": "20230901T031305Z"
}
```

### 6.6 删除产品
```json
{}
```
### 6.7 查询产品列表(删除产品后）
```json
{
  "products": [],
  "page": {
    "count": 0,
    "marker": "ffffffffffffffffffffffff"
  }
}
```