package com.huawei.iotda;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.core.utils.JsonUtils;
import com.huaweicloud.sdk.iotda.v5.IoTDAClient;
import com.huaweicloud.sdk.iotda.v5.model.AddProduct;
import com.huaweicloud.sdk.iotda.v5.model.CreateProductRequest;
import com.huaweicloud.sdk.iotda.v5.model.CreateProductResponse;
import com.huaweicloud.sdk.iotda.v5.model.DeleteProductRequest;
import com.huaweicloud.sdk.iotda.v5.model.DeleteProductResponse;
import com.huaweicloud.sdk.iotda.v5.model.ListProductsRequest;
import com.huaweicloud.sdk.iotda.v5.model.ListProductsResponse;
import com.huaweicloud.sdk.iotda.v5.model.ServiceCapability;
import com.huaweicloud.sdk.iotda.v5.model.ServiceProperty;
import com.huaweicloud.sdk.iotda.v5.model.ShowProductRequest;
import com.huaweicloud.sdk.iotda.v5.model.ShowProductResponse;
import com.huaweicloud.sdk.iotda.v5.model.UpdateProduct;
import com.huaweicloud.sdk.iotda.v5.model.UpdateProductRequest;
import com.huaweicloud.sdk.iotda.v5.model.UpdateProductResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ProductManagement {
    private static final Logger logger = LoggerFactory.getLogger(ProductManagement.class);
    private static final ObjectMapper OBJECTMAPPER = new ObjectMapper();

    // REGION_ID：If your region is 上海一，please fill in "cn-east-3"；If your region is 北京四，please fill in "cn-north-4"; If your region is 华南广州，please fill in "cn-south-4"
    private static final String REGION_ID = "<YOUR REGION ID>";

    private static final String PROJECT_ID = "<YOUR PROJECTID>";
    // Instance ID
    private static final String INSTANCE_ID = "<YOUR INSTANCEID>";

    // ENDPOINT：On the console, choose **Overview** in the navigation pane and click **Access Addresses** to view the HTTPS application access address.
    private static final String ENDPOINT = "<YOUR ENDPOINT>";

    // ak/sk：For details, see the method of obtaining AK/SK in iotda document https://support.huaweicloud.com/api-iothub/iot_06_v5_0091.html.
    // There will be security risks if the AK/SK used for authentication is directly written into code. We suggest encrypting the AK/SK in the configuration file or environment variables for storage.
    // In this sample, the AK/SK is stored in environment variables for identity authentication. Before running this sample, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");
    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");


    private static IoTDAClient initClient() {
        // Create a credential
        ICredential auth = new BasicCredentials()
            .withAk(AK)
            .withSk(SK)
            // Derivative algorithms are used for the standard and enterprise editions. For the basic edition, delete the configuration "withDerivedPredicate".
            .withDerivedPredicate(BasicCredentials.DEFAULT_DERIVED_PREDICATE)
            .withProjectId(PROJECT_ID);

        // Create and initialize an IoTDAClient instance
        return IoTDAClient.newBuilder()
            .withCredential(auth)
            // Use IoTDARegion for basic editions like
            // "withRegion(IoTDARegion.CN_NORTH_4)". Create regions for standard/enterprise editions.
            .withRegion(new Region(REGION_ID, ENDPOINT))
            // .withRegion(IoTDARegion.CN_NORTH_4)
            .build();
    }

    /**
     * Create a Product
     *
     * @param iotdaClient iotda client
     * @param body        Structure for creating a product
     * @return Product ID
     */
    private static String createProduct(IoTDAClient iotdaClient, AddProduct body) throws ServiceResponseException {
        CreateProductRequest request = new CreateProductRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setBody(body);
        CreateProductResponse product = iotdaClient.createProduct(request);
        logger.info("The product creation result is:{}", JsonUtils.toJSON(OBJECTMAPPER, product));
        return product.getProductId();
    }

    /**
     * Query the product list under an app
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space id
     */
    private static void queryProductList(IoTDAClient iotdaClient, String appId) throws ServiceResponseException {
        ListProductsRequest request = new ListProductsRequest();
        request.setInstanceId(INSTANCE_ID);
        request.setAppId(appId);
        ListProductsResponse response = iotdaClient.listProducts(request);
        logger.info("The product list query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    /**
     * Query the Details About a Product
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space id
     * @param productId   Product id
     */
    private static void queryProductDetail(IoTDAClient iotdaClient, String appId, String productId) throws ServiceResponseException {
        ShowProductRequest request = new ShowProductRequest();
        request.setAppId(appId);
        request.setInstanceId(INSTANCE_ID);
        request.setProductId(productId);
        ShowProductResponse productResponse = iotdaClient.showProduct(request);
        logger.info("The product details query result is:{}", JsonUtils.toJSON(OBJECTMAPPER, productResponse));
    }

    /**
     * Update a Product
     *
     * @param iotdaClient iotda client
     * @param productId   Product id
     * @param body        Structure for updating a product
     */
    private static void updateProduct(IoTDAClient iotdaClient, String productId, UpdateProduct body) throws ServiceResponseException {
        UpdateProductRequest request = new UpdateProductRequest();
        request.setProductId(INSTANCE_ID);
        request.setBody(body);
        request.setProductId(productId);
        UpdateProductResponse updateProduct = iotdaClient.updateProduct(request);
        logger.info("The product update result is:{}", JsonUtils.toJSON(OBJECTMAPPER, updateProduct));
    }

    /**
     * Delete a Product
     *
     * @param iotdaClient iotda client
     * @param appId       Resource space id
     * @param productId   ID of the product that needs to be deleted
     */
    private static void deleteProduct(IoTDAClient iotdaClient, String appId, String productId) throws ServiceResponseException {
        DeleteProductRequest request = new DeleteProductRequest();
        request.setAppId(appId);
        request.setInstanceId(INSTANCE_ID);
        request.setProductId(productId);
        DeleteProductResponse response = iotdaClient.deleteProduct(request);
        logger.info("The product deletion result is:{}", JsonUtils.toJSON(OBJECTMAPPER, response));
    }

    public static void main(String[] args) {
        // Initialize the IoTDA client
        IoTDAClient iotdaClient = initClient();
        // ID of the resource space to which the device belongs. Replace the value with the required one.
        String appId = "9d988b5efdf646f0828724c45e1d794e";

        try {
            logger.info("========1.Querying the Product List========");
            queryProductList(iotdaClient, appId);
            logger.info("========1.Querying the product list is complete========");

            logger.info("========2.Creating a Product========");
            // Assemble product data, you can assemble according to the actual situation of your product
            AddProduct addProduct = new AddProduct();
            addProduct.setAppId(appId);
            addProduct.setName("SmartStreetLamp");
            addProduct.setDeviceType("SmartStreetLamp");
            addProduct.setProtocolType("MQTT");
            addProduct.setDataFormat("json");
            addProduct.setManufacturerName("huawei");
            addProduct.setIndustry("SmartCity");
            addProduct.setDescription("This is a product sample");
            // Product Capability Definition
            List<ServiceCapability> capabilities = new ArrayList<>();
            ServiceCapability capability = new ServiceCapability();
            capability.setServiceId("BasicData");
            capability.setServiceType("BasicData");
            capability.setDescription("Basic data of street lamps");
            List<ServiceProperty> properties = new ArrayList<>();
            ServiceProperty property = new ServiceProperty();
            property.setPropertyName("luminance");
            property.setDataType("decimal");
            property.setMethod("RW");
            property.setMax("100");
            property.setMin("0");
            property.setDescription("Street Lamp Brightness Properties");
            properties.add(property);
            capability.setProperties(properties);
            capabilities.add(capability);
            addProduct.setServiceCapabilities(capabilities);

            String productId = createProduct(iotdaClient, addProduct);
            logger.info("========2.Product creation completed========");

            logger.info("========3.Querying the Product List========");
            queryProductList(iotdaClient, appId);
            logger.info("========3.Querying the product list is complete========");

            logger.info("========4.Querying Product Details========");
            queryProductDetail(iotdaClient, appId, productId);
            logger.info("========4.Product details query completed========");

            logger.info("========5.Update Product========");
            // Change the value according to the actual situation. Here, the product name is changed.
            UpdateProduct updateProduct = new UpdateProduct();
            updateProduct.setAppId(appId);
            updateProduct.setName("Model-1");
            updateProduct(iotdaClient, productId, updateProduct);
            logger.info("========5.Update Product Complete========");

            logger.info("========6.Delete Product========");
            deleteProduct(iotdaClient, appId, productId);
            logger.info("========6.Product deletion completed========");

            logger.info("========7.Querying the Product List========");
            queryProductList(iotdaClient, appId);
            logger.info("========7.Querying the product list is complete========");
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.warn("Demonstration failed. Exception information is {}", e.getMessage());
        } catch (ServiceResponseException e) {
            logger.warn("Demonstration failed. Exception information is {}, {}", e.getErrorCode(), e.getErrorMsg());
        }
    }
}
