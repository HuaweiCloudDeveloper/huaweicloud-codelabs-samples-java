## 1、介绍

**公有云版本iDME的事务一致性API**

在应用开发过程中，可能会存在一个核心业务逻辑的执行需要同时调用多个下游业务进行处理的场景。
因此，如何保证核心业务和多个下游业务的执行结果完全一致，是事务需要解决的主要问题。
为了保证多个任务执行结果一致，具备提交、回滚和统一协调的能力，工业数字模型驱动引擎-数据建模引擎（xDM Foundation，简称xDM-F）
提供事务型任务功能，编排多个原子接口，并与本地事务绑定，实现多接口提交结果的一致性，并提升接口调用速率和性能。

**您将学到什么？**

1、您将了解到工业数字模型驱动引擎-数据建模引擎（xDM Foundation，简称xDM-F）事务API中V1与V2版本区别，供您做接口选择。  
2、您将了解到iDME-SDK包如何使用事务管理API，供您做技术参考。  
3、您将了解到iDME-SDK包如何封装事务管理API实现自动获取事务ID以及任务编号，供您做技术参考。

**注意**  
本次代码实例使用事务管理API V2版本接口展示，V1只需额外提供一个执行子任务API数量；可参考以下V2版本示例做修改。

## 2、时序图/流程图

![时序图](./assets/sequenceDiagram.png)

**说明**  
- 创建事务型任务：创建事务有V1、V2两种类型，V1需要传入事务中执行子任务的数量，V2不用传入；
初始化后会返回两个字段transaction_id表示初始化后的事务ID，task_no为任务序列编码，从0开始。  
- 执行iDME原子接口：在执行前需要将原子接口作为事务子任务绑定到事务中，绑定需要将transaction_id以及task_no作为请求头放在原子接口中。
- 提交事务型任务：提交类型有同步、异步，根据当前的业务是否可以支持串行或并行进行选择。
- 查询事务型任务执行结果：返回子任务执行的结果以及整体事务执行的结果；若整体事务失败则会进行回滚。

## 3、前置条件

1、已注册华为云，并完成实名认证。  
2、已开通iDME设计态服务和订购iDME数据建模引擎。  
3、已经在iDME设计态建模、发布应用，并且将iDME应用部署到了数据建模引擎上。  
4、具备开发环境，支持Java JDK 1.8及以上版本。  
5、开发环境中通过maven导入iDME SDK，并配置好环境变量，相关SDK安装可参考[iDME全量数据服务API封装](https://codelabs.developer.huaweicloud.com/codelabs/samples/1a913b978cb24de797fb6bbe9d95d19b)中Delegator使用代码示例。

## 4、事务一致性API介绍

**注意事项**

应用部署到运行态后事务一致性API可在：数据服务管理 -> 全量数据服务 -> 事务系统管理API -> 事务型A任务API中查看
iDME-SDK包中封装的事务API接口可在：rdm-delegate-1.0.0-SNAPSHOT.jar com.huawei.innovation.rdm.delegate.service
.TransactionApiDelegateService进行查看。

**API介绍**

```
|--- V1
    |--- XDM_v1_transaction-apis_transactions：开启事务型任务，此接口需要设置执行iDME原子接口的数量，用于完成事务型任务的初始化。
    |--- XDM_v1_transaction-apis_transactions_async_{transaction-id}：异步提交事务，使用于需要同时执行多个子任务的场景。
    |--- XDM_v1_transaction-apis_transactions_{transaction-id}：查询事务执行的结果，V1与V2版本均使用此接口进行查询。
    |--- XDM_v1_transaction-apis_transactions_{transaction-id}：同步提交任务，让事务中的任务串行执行。
|--- V2
    |--- XDM_v2_transaction-apis_transactions：开启事务型任务，该接口不用传入原子接口的数量。
    |--- XDM_v2_transaction-apis_transactions_async：异步提交事务，使用于需要同时执行多个子任务的场景，提交时需要传入开启事务接口获取的事务ID以及当前事务中待执行的子任务数量。
    |--- XDM_v1_transaction-apis_transactions_{transaction-id}：查询事务执行的结果，V1与V2版本均使用此接口进行查询。
|--- 子任务绑定
    |--- X-Dme-Transaction-Task-Id：开启V1、V2型任务时会返回字段transaction_id，在绑定子任务是需要将X-Dme-Transaction-Task-Id放在子任务的请求头中，并写入transaction_id的值。
    |--- X-Dme-Transaction-Task-No：开启V1、V2型任务时会返回字段transaction_id，在绑定子任务是需要将X-Dme-Transaction-Task-No放在子任务的请求头中，并写入task_no的值，task_no在绑定时需要根据任务执行的顺序进行增加，开启事务返回的task_no从0开始。
```

## 5、客户端SDK安装示例

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-dependencies</artifactId>
        <version>2.7.12</version>
    </parent>

    <artifactId>idme-api</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <idme.jar.path>D:/project/iDME-TransactionAPI/lib</idme.jar.path>
        <idme.app.name>idmeclassicapi</idme.app.name>
    </properties>

    <dependencies>
        <!--dme core-sdk-api jar包依赖-->
        <dependency>
            <groupId>com.huawei.xdm</groupId>
            <artifactId>core-sdk-api</artifactId>
            <version>1.0.0-SNAPSHOT</version>
            <scope>system</scope>
            <systemPath>${idme.jar.path}/core-sdk-api-1.0.0-SNAPSHOT.jar</systemPath>
        </dependency>
        <!--dme rdm-common jar包依赖-->
        <dependency>
            <groupId>com.huawei.xdm</groupId>
            <artifactId>rdm-common</artifactId>
            <version>1.0.0-SNAPSHOT</version>
            <scope>system</scope>
            <systemPath>${idme.jar.path}/rdm-common-1.0.0-SNAPSHOT.jar</systemPath>
        </dependency>
        <!--dme rdm-delegate jar包依赖-->
        <dependency>
            <groupId>com.huawei.xdm</groupId>
            <artifactId>rdm-delegate</artifactId>
            <version>1.0.0-SNAPSHOT</version>
            <scope>system</scope>
            <systemPath>${idme.jar.path}/rdm-delegate-1.0.0-SNAPSHOT.jar</systemPath>
        </dependency>
        <!--dme api jar包依赖-->
        <dependency>
            <groupId>com.huawei.xdm</groupId>
            <artifactId>${idme.app.name}.api</artifactId>
            <version>1.0.0-optimization-SNAPSHOT</version>
            <scope>system</scope>
            <systemPath>${idme.jar.path}/${idme.app.name}.api-1.0.0-optimization-SNAPSHOT.jar</systemPath>
        </dependency>
    </dependencies>
</project>
```

**说明**

需要将properties标签中的参数替换为自己的参数。  
- idme.jar.path：下载jar包的绝对路径。 
- idme.app.name：应用名称全小写。  

## 6、代码示例

在iDME-SDK包中开启事务提交事务需要手动执行，以下介绍通过切面AOP+拦截器自动开启、提交事务并绑定子任务。  

本次示例的模型信息：模型中字段uniqueKey为唯一键字段。

![模型基本信息](./assets/model_info.png)

![模型属性信息](./assets/model_attr.png)

**说明**

以下代码示例会基于Delegator方式实现切面和拦截器。

### 6.1、添加pom依赖

在章节5的pom树基础上添加以下依赖：  

```xml
<dependency>
    <groupId>javax.validation</groupId>
    <artifactId>validation-api</artifactId>
    <version>2.0.1.Final</version>
</dependency>
<dependency>
    <groupId>org.apache.httpcomponents</groupId>
    <artifactId>httpclient</artifactId>
    <version>4.5.13</version>
</dependency>
<dependency>
    <groupId>com.mikesamuel</groupId>
    <artifactId>json-sanitizer</artifactId>
    <version>1.2.3</version>
</dependency>
<dependency>
    <groupId>org.hibernate</groupId>
    <artifactId>hibernate-core</artifactId>
    <version>5.6.9.Final</version>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
<dependency>
    <groupId>org.reflections</groupId>
    <artifactId>reflections</artifactId>
    <version>0.10.2</version>
</dependency>
<dependency>
    <groupId>javax.persistence</groupId>
    <artifactId>javax.persistence-api</artifactId>
    <version>2.2</version>
</dependency>
<dependency>
    <groupId>org.apache.commons</groupId>
    <artifactId>commons-lang3</artifactId>
</dependency>
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
</dependency>
<dependency>
    <groupId>com.google.guava</groupId>
    <artifactId>guava</artifactId>
    <version>32.1.2-jre</version>
</dependency>
```

### 6.2、配置文件

配置信息需要与当前导入的iDME-SDK相匹配。  

```properties
delegate.subAppId=rdm_b3f9b7523a6141f4b2d76b92d6595281_app
delegate.domain=https://dme.cn-north-4.huaweicloud.com
delegate.domainName={IAM_DOMAIN_NAME}
delegate.userName={IAM_USERNAME}
delegate.password={IAM_PASSWORD}
delegate.endpoint=https://iam.cn-north-4.myhuaweicloud.com
delegate.regionName=cn-north-4
delegate.serviceType=services/dynamic
```

**说明**

- delegate.subAppId：应用唯一标识URI，组成为rdm_{app_id}_app，可以从运行态的URL中截取。 
- delegate.domain：iDME的运行态域名，可以从运行态URL中截取，北京四环境保持默认即可。 
- delegate.domainName：iDME当前应用登录的IAM账号。 
- delegate.userName：iDME当前应用登录的IAM账号用户名。 
- delegate.password：iDME当前应用登录的IAM账号密码。 
- delegate.endpoint：IAM域名，后续获取Token使用，北京四环境保持默认即可。 
- delegate.regionName：局域名，北京四为cn-north-4。 
- delegate.serviceType：服务uri类型，默认保持为services/dynamic即可。

### 6.3、拦截器

```java
package com.idme.intercepter;

import com.huawei.innovation.rdm.coresdk.transactionapi.bean.TransactionApiTaskDtoV2;

import com.idme.annotation.TransactionAPI;
import com.idme.aspect.TransactionApiAspect;
import com.sun.javafx.collections.UnmodifiableListSet;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * 自定义拦截器，用于自动给接口添加事务信息
 * 
 * @since 2024-05-11
 */
public class RestTemplateHeadersModifierInterceptor implements ClientHttpRequestInterceptor {

    /**
     * IAM获取Token接口不进行拦截
     */
    public static final Set<String> interceptorUrls = Collections.unmodifiableSet(new HashSet<String>() {
        {
            add("/v3/auth/tokens");
        }
    });

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
        throws IOException {
        TransactionApiTaskDtoV2 transactionV2Info = TransactionApiAspect.getTransactionV2Info();
        // 环境线程中存在事务信息，并且接口不为IAM 获取token接口，则设置请求头
        if (ObjectUtils.isNotEmpty(transactionV2Info) && interceptorUrls.stream()
            .noneMatch(item -> request.getURI().toString().contains(item))) {
            // 设置请求头信息
            HttpHeaders headers = request.getHeaders();
            headers.add(TransactionApiAspect.X_DME_TRANSACTION_TASK_ID,
                String.valueOf(transactionV2Info.getTransactionId()));
            headers.add(TransactionApiAspect.X_DME_TRANSACTION_TASK_NO,
                String.valueOf(transactionV2Info.getTaskNo() + 1));
            // 任务编号+1
            transactionV2Info.setTaskNo(transactionV2Info.getTaskNo() + 1);
            TransactionApiAspect.setTransactionV2Info(transactionV2Info);
        }
        return execution.execute(request, body);
    }
}
```

### 6.4、RestTemplate配置

```java
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.idme.config;

import com.huawei.innovation.rdm.delegate.exception.RdmDelegateException;

import com.idme.intercepter.RestTemplateHeadersModifierInterceptor;

import org.apache.http.HttpHost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * RestTemplate 配置。
 *
 * @since 2024-05-11
 */
@Configuration
public class RestTemplateConfig {
    /**
     * 构建 RestTemplate 对象，忽略证书校验。
     *
     * @return RestTemplate 对象
     */
    @Bean
    public RestTemplateBuilder restTemplateBuilder() {
        return new RestTemplateBuilder();
    }

    /**
     * 构建 RestTemplate 对象，忽略证书校验。
     *
     * @param builder builder
     * @return RestTemplate 对象
     */
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        TrustManager[] trustAllCerts = new TrustManager[] {
            new X509TrustManager() {
                /**
                 * 获取证书颁发者列表
                 *
                 * @return 证书颁发者列表
                 */
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }

                /**
                 * 校验客户端证书
                 *
                 * @param certs the peer certificate chain
                 * @param authType the authentication type based on the client certificate
                 */
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                /**
                 * 校验服务端证书
                 *
                 * @param certs the peer certificate chain
                 * @param authType the key exchange algorithm used
                 */
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
        };
        SSLContext sslContext = null;
        try {
            sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            throw new RdmDelegateException("config.1", e.getMessage());
        }
        CloseableHttpClient httpClient = HttpClients.custom()
            .setSSLContext(sslContext)
            .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
            // 需要替换为自己的代理服务器，如果没有，需要去除
            .setProxy(new HttpHost("proxy.huawei.com", 8080))
            .build();
        HttpComponentsClientHttpRequestFactory customRequestFactory = new HttpComponentsClientHttpRequestFactory();
        customRequestFactory.setHttpClient(httpClient);

        // 设置拦截器，给事务API子任务设置请求头
        RestTemplate restTemplate = builder.requestFactory(() -> customRequestFactory).build();
        List<ClientHttpRequestInterceptor> interceptors = restTemplate.getInterceptors();
        if (CollectionUtils.isEmpty(interceptors)) {
            interceptors = new ArrayList<>();
        }
        interceptors.add(new RestTemplateHeadersModifierInterceptor());
        restTemplate.setInterceptors(interceptors);
        return restTemplate;
    }
}
```

### 6.4、事务切面设置

```java
package com.idme.aspect;

import com.huawei.innovation.rdm.coresdk.transactionapi.bean.TransactionApiExecuteResultDto;
import com.huawei.innovation.rdm.coresdk.transactionapi.bean.TransactionApiTaskDtoV2;
import com.huawei.innovation.rdm.delegate.service.TransactionApiDelegateService;

import com.idme.vo.RDMResponseVO;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 事务API切面，用于在切点接口中自动开启事务提交事务
 *
 * @since 2024-05-11
 */
@Aspect
@Component
public class TransactionApiAspect {
    /**
     * 保存V2版本中的事务信息（事务ID、任务编号）
     */
    private static ThreadLocal<TransactionApiTaskDtoV2> transactionV2Info = new ThreadLocal<>();

    /**
     * 用于事务管理添加原子接口的事务ID信息
     */
    public static final String X_DME_TRANSACTION_TASK_ID = "X-Dme-Transaction-Task-Id";

    /**
     * 用于事务管理添加原子接口的任务编码信息
     */
    public static final String X_DME_TRANSACTION_TASK_NO = "X-Dme-Transaction-Task-No";

    /**
     * 事务API代理类，用于多接口统一事务管理
     */
    @Autowired
    private TransactionApiDelegateService transactionApiDelegateService;

    /**
     * 开启事务切入点，使用注解标识在需要用事务API管理的业务逻辑接口上
     */
    @Pointcut(value = "@annotation(com.idme.annotation.TransactionAPI)")
    public void transactionPointCut() {}

    /**
     * 环绕通知用于事务开启、提交、获取事务结果
     */
    @Around(value = "transactionPointCut()")
    public Object commitTransaction(ProceedingJoinPoint joinPoint) throws Throwable {
        // 前置开启事务
        TransactionApiTaskDtoV2 transactionApiTaskDtoV2 = transactionApiDelegateService.openTransactionV2();
        setTransactionV2Info(transactionApiTaskDtoV2);
        // 执行业务逻辑
        Object result = joinPoint.proceed();
        // 后置提交事务
        TransactionApiTaskDtoV2 transactionV2Info = getTransactionV2Info();
        // 清除线程变量中的事务信息
        setTransactionV2Info(null);
        transactionApiDelegateService.asyncCommitTransactionV2(transactionV2Info.getTransactionId(), transactionV2Info.getTaskNo());

        if (result instanceof RDMResponseVO) {
            // 查询事务结果，并覆盖返回值
            TransactionApiExecuteResultDto executeResultDto = transactionApiDelegateService.findTransaction(
                transactionApiTaskDtoV2.getTransactionId());
            ((RDMResponseVO) result).setExecuteResultDto(executeResultDto);
        }
        return result;
    }

    public static TransactionApiTaskDtoV2 getTransactionV2Info() {
        return transactionV2Info.get();
    }

    public static void setTransactionV2Info(TransactionApiTaskDtoV2 transactionV2Info) {
        TransactionApiAspect.transactionV2Info.set(transactionV2Info);
    }
}
```

### 6.5、切点注解

```java
package com.idme.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TransactionAPI { }
```

### 6.6、控制层DelegatorController.java

```java
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.idme.controller;

import com.idme.service.DelegatorService;
import com.idme.vo.RDMResponseVO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * DelegatorController
 *
 * @since 2024-05-11
 */
@RequestMapping("/delegator")
@RestController
public class DelegatorController {
    @Autowired
    private DelegatorService delegatorService;

    /**
     * 重复创建唯一键相同的对象，进行事务回滚
     *
     * @return 查询结果
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public RDMResponseVO createDuplicateKey() {
        return delegatorService.repeatedCreate();
    }
}

```

### 6.7、业务逻辑层

**DelegatorService.java**

```java
package com.idme.service;

import com.idme.vo.RDMResponseVO;

/**
 * DelegatorService
 *
 * @since 2024-05-11
 */
public interface DelegatorService {
    /**
     * 模拟重复创建业务，进行事务回滚
     *
     * @return 查询结果
     */
    RDMResponseVO repeatedCreate();
}
```

**DelegatorServiceImpl.java**

```java
package com.idme.service.impl;

import com.huawei.innovation.rdm.coresdk.basic.enums.ConditionType;
import com.huawei.innovation.rdm.coresdk.basic.vo.QueryRequestVo;
import com.huawei.innovation.rdm.coresdk.basic.vo.RDMPageVO;
import com.huawei.innovation.rdm.coresdk.basic.vo.RDMResultVO;
import com.huawei.innovation.rdm.idmeclassicapi.delegator.PersistableModelDelegator;
import com.huawei.innovation.rdm.idmeclassicapi.dto.entity.PersistableModelCreateDTO;
import com.huawei.innovation.rdm.idmeclassicapi.dto.entity.PersistableModelViewDTO;

import com.idme.annotation.TransactionAPI;
import com.idme.service.DelegatorService;
import com.idme.vo.RDMResponseVO;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * DelegatorServiceImpl
 *
 * @since 2024-05-11
 */
@Service
public class DelegatorServiceImpl implements DelegatorService {
    @Autowired
    private PersistableModelDelegator persistableModelDelegator;

    @TransactionAPI
    @Override
    public RDMResponseVO repeatedCreate() {
        // 创建模型必填项
        PersistableModelCreateDTO params = new PersistableModelCreateDTO();
        // 当前模型中uniqueKey字段具有唯一性
        params.setUniqueKey("value");

        // 创建查询条件
        QueryRequestVo uniqueKeyRequestVo = new QueryRequestVo();
        uniqueKeyRequestVo.addCondition("uniqueKey", ConditionType.EQUAL, "value");

        // 1、创建唯一键属性值为value的数据
        persistableModelDelegator.create(params);
        // 2、查询当前模型中唯一键值为value的数据
        persistableModelDelegator.find(uniqueKeyRequestVo, new RDMPageVO(1, 10));
        // 3、重复创建有唯一键的数据，异常回滚
        persistableModelDelegator.create(params);
        // 4、查询当前模型中唯一键值为value的数据，3任务失败后，后续的任务不会继续执行
        List<PersistableModelViewDTO> persistableModelViewDTOS = persistableModelDelegator.find(uniqueKeyRequestVo,
            new RDMPageVO(1, 10));

        return buildResponse(persistableModelViewDTOS);
    }

    private RDMResponseVO buildResponse(List<PersistableModelViewDTO> persistableModelViewDTOS) {
        RDMResponseVO responseVO = new RDMResponseVO();
        if (ObjectUtils.isNotEmpty(persistableModelViewDTOS)) {
            List<Object> data = persistableModelViewDTOS.stream()
                .filter(ObjectUtils::isNotEmpty)
                .map(item -> (Object) item)
                .collect(Collectors.toList());
            responseVO.setData(data);
        }
        responseVO.setResult(RDMResultVO.SUCCESS);
        return responseVO;
    }
}
```

### 6.8、Spring Boot启动类

```java
package com.idme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 启动类，需要扫描客户端SDK包和用户项目路径包
 *
 * @since 2024-05-11
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.huawei.innovation", "com.idme"})
public class TransactionAPIApplication {
    public static void main(String[] args) {
        SpringApplication.run(TransactionAPIApplication.class, args);
    }
}
```

## 7、返回结果示例

业务逻辑接口，重复创建唯一键数据，事务失败回滚：  

**接口响应解析**
- data：查询唯一键值等于“value”的结果。
- errors：异常信息集合。
- result：接口封装后的响应状态。
- executeResultDto：事务中子任务执行结果。
  - id：统一事务ID。
  - status：整体事务执行状态。
  - task_count：执行子任务总数。
  - tasks：子任务执行结果集合。
    - result：子任务执行后返回结果。
    - task_no：子任务执行序号。
    - task_status：子任务执行状态。

```json
{
  "result": "SUCCESS",
  "data": [],
  "errors": [],
  "executeResultDto": {
    "id": 632223721750274049,
    "status": "fail",
    "tasks": [
      {
        "result": "{\"id\": 632223728389853184, \"tenant\": {\"id\": -1, \"code\": \"basicTenant\", \"name\": \"basicTenant\", \"creator\": \"xdmAdmin\", \"modifier\": \"xdmAdmin\", \"className\": \"Tenant\", \"createTime\": 1701658854000, \"dataSource\": \"DefaultDataSource\", \"rdmVersion\": 1, \"description\": \"默认租户\", \"disableFlag\": false, \"rdmDeleteFlag\": 0, \"securityLevel\": \"internal\", \"lastUpdateTime\": 1701658854000, \"rdmExtensionType\": \"Tenant\"}, \"creator\": \"test1 1b628f2e5d9249aba0704de29e73f2c1\", \"modifier\": \"test1 1b628f2e5d9249aba0704de29e73f2c1\", \"className\": \"PersistableModel\", \"uniqueKey\": \"value\", \"createTime\": 1715754682997, \"rdmVersion\": 1, \"rdmDeleteFlag\": 0, \"lastUpdateTime\": 1715754682997, \"rdmExtensionType\": \"PersistableModel\"}",
        "task_no": 1,
        "task_status": "success"
      },
      {
        "result": "[{\"id\": 632223728389853184, \"tenant\": {\"id\": -1, \"code\": \"basicTenant\", \"name\": \"basicTenant\", \"creator\": \"xdmAdmin\", \"modifier\": \"xdmAdmin\", \"className\": \"Tenant\", \"createTime\": 1701658854000, \"dataSource\": \"DefaultDataSource\", \"rdmVersion\": 1, \"description\": \"默认租户\", \"disableFlag\": false, \"rdmDeleteFlag\": 0, \"securityLevel\": \"internal\", \"lastUpdateTime\": 1701658854000, \"rdmExtensionType\": \"Tenant\"}, \"creator\": \"test1 1b628f2e5d9249aba0704de29e73f2c1\", \"modifier\": \"test1 1b628f2e5d9249aba0704de29e73f2c1\", \"className\": \"PersistableModel\", \"uniqueKey\": \"value\", \"createTime\": 1715754683000, \"rdmVersion\": 1, \"rdmDeleteFlag\": 0, \"lastUpdateTime\": 1715754683000, \"rdmExtensionType\": \"PersistableModel\"}]",
        "task_no": 2,
        "task_status": "success"
      },
      {
        "result": "{\"message\": \"The PersistableModel of [uniqueKey] should be unique.\"}",
        "task_no": 3,
        "task_status": "fail"
      },
      {
        "result": null,
        "task_no": 4,
        "task_status": "not start"
      }
    ],
    "task_count": 4
  }
}
```

## 8、参考链接

更多信息参考：
- [客户端SDK概述](https://support.huaweicloud.com/sdkreference-idme/idme_csdk_0001.html)
- [iDME全量数据服务API封装](https://codelabs.developer.huaweicloud.com/codelabs/samples/1a913b978cb24de797fb6bbe9d95d19b#id-17262182011593152)

## 9、修订记录

|    发布日期    | 文档版本 |  修订说明  |
|:----------:|:----:|:------:|
| 2024-05-11 | 1.0  | 文档首次发布 |