## 1、介绍

**公有云版本iDME的Token管理**

公有云版本iDME在获取Token时使用统一身份认证服务 IAM获取Token进行API鉴权，Token是IAM服务颁发给IAM用户的令牌，承载用户的身份、权限等信息。  

公有云版本iDME在应用发布后会生成以下客户端SDK包：  
core-sdk-api-1.0.0-SNAPSHOT.jar  
rdm-common-1.0.0-SNAPSHOT.jar  
rdm-delegate-1.0.0-SNAPSHOT.jar  
应用名称.api-1.0.0-optimization-SNAPSHOT.jar  
应用名称.impl-1.0.0-optimization-SNAPSHOT.jar  
应用名称.controller.api-1.0.0-{应用环境标识}-SNAPSHOT.jar  
应用名称.controller.impl-1.0.0-{应用环境标识}-SNAPSHOT.jar  
应用名称.ext.api-1.0.0-optimization-SNAPSHOT.jar  
在rdm-delegate-1.0.0-SNAPSHOT.jar中，整合了Token相关的获取、刷新、缓存等操作。  

**您将学到什么？**

1、您将了解到iDME-SDK包如何自动获取、刷新、缓存Token，供您做技术参考。

## 2、时序图/流程图

![时序图](./assets/sequenceDiagram.png)

## 3、前置条件

1、已注册华为云，并完成实名认证。  
2、已开通iDME设计态服务和订购iDME数据建模引擎。  
3、已经在iDME设计态建模、发布应用，并且将iDME应用部署到了数据建模引擎上。  
4、具备开发环境，支持Java JDK 1.8及以上版本。  
5、开发环境中通过maven导入iDME SDK，并配置好环境变量，相关SDK安装可参考[iDME全量数据服务API封装](https://codelabs.developer.huaweicloud.com/codelabs/samples/1a913b978cb24de797fb6bbe9d95d19b)中Delegator使用代码示例。

## 4、接口工具获取Token

**接口调用工具获取Token**

![接口工具获取Token](./assets/api_tool_token.png)

- 返回结果Headers中“X-Subject-Token”为获取的Token信息。

**接口请求格式**

```

POST https://{{endpoint}}/v3/auth/tokens 
Content-Type: application/json 
 
{ 
    "auth": { 
        "identity": { 
            "methods": [ 
                "password" 
            ], 
            "password": { 
                "user": { 
                    "name": "{{username}}", 
                    "password": "{{password}}", 
                    "domain": { 
                        "name": "{{domainname}}" 
                    } 
                } 
            } 
        }, 
        "scope": { 
            "project": { 
                "name": "{{projectname}}" 
            } 
        } 
    } 
}

```

**参数信息说明**

- endpoint：指定承载REST服务端点的服务器域名或IP，不同服务不同区域的Endpoint不同，您可以从[地区和终端节点](https://developer.huaweicloud.com/endpoint)中获取。例如IAM服务在“华北-北京四”区域的Endpoint为“iam.cn-north-4.myhuaweicloud.com”。
- username：iDME当前应用登录的IAM用户名。
- password：iDME当前应用登录的IAM用户密码。
- domainname：iDME当前应用登录的IAM用户所属的租户名。
- projectname：Token的作用域范围；scope参数定义了Token的作用域，projectname定义作用域名称，如“cn-north-4”，表示当前Token仅在“cn-north-4”有效。

**接口返回信息**

- 接口返回的响应消息头中“x-subject-token”就是需要获取的用户Token。后续Token缓存、刷新、有效性校验，可以通过Redis缓存、API封装方式进行；API封装可参考[iDME全量数据服务API封装](https://codelabs.developer.huaweicloud.com/codelabs/samples/1a913b978cb24de797fb6bbe9d95d19b#id-17262182011593152)

## 5、客户端SDK获取Token

**XdmTokenService**  
- XdmTokenService中封装了SDK方式管理Token的方式，包括Token的获取、刷新、缓存以及有效性校验；
- XdmTokenService.getToken()方法用于向IAM获取Token，获取Token所需的用户信息从环境变量中获取；
- 本地token缓存，用于设置token过期时间、初始容量、最大容量、刷新时间、回收机制等；
```java
public void initTokenCache() {
    this.tokenCache = CacheBuilder.newBuilder()
        .initialCapacity(100)
        .maximumSize(200L)
        .expireAfterWrite((long) this.tokenExpireTime, TimeUnit.HOURS)
        .refreshAfterWrite((long) this.tokenExpireTime, TimeUnit.HOURS)
        .softValues()
        .build(new CacheLoader<String, String>() {
            public String load(String key) {
                return XdmTokenService.this.getTokenWithUserNameAndPassword();
            }
        });
}
```
- getToken()方法用于从本地缓存中获取token信息，当获取异常时会调用方法getTokenWithUserNameAndPassword()从IAM获取新token；
```java
public String getToken() {
    String cacheKey = this.getKey();

    try {
        return (String) this.tokenCache.get(cacheKey);
    } catch (ExecutionException var3) {
        ExecutionException e = var3;
        log.warn("tokenCache error", e);
        return this.getTokenWithUserNameAndPassword();
    }
}
// 从IAM获取token
private String getTokenWithUserNameAndPassword() {
    IamTokenRequest request = IamTokenRequest.getIamTokenRequest(this.config.getUserName(), this.config.getPassword(),
        this.config.getDomainName(), Collections.singletonList("password"), this.config.getRegionName());
    HttpEntity<String> responseEntity = this.exchage(HttpMethod.POST, XDMDelegatorJsonUtil.object2string(request),
        (Map) null);
    return responseEntity.getHeaders().getFirst("X-Subject-Token");
}
```
- token刷新机制，当业务请求中的token无效时，会进行业务请求重试，此时会刷新当前环境中的token信息
```java
    private RDMResultVO exchange(String url, Object param, Map<String, String> headerParam) {
    boolean isUrl = RestTemplateUtil.isUrl(url);
    if (!isUrl) {
        throw new RdmDelegateException("IIT.61003001", "The param url can't be empty!");
    } else {
        int i = 0;

        RDMResultVO rdmResultVO;
        do {
            Map<String, String> headers = RestTemplateUtil.getHeadersMap(this.config.getDomain(),
                "application/json;charset=utf8");
            headers.put("X-Auth-Token", this.xdmTokenService.getToken());
            headers.put("tenantId", String.valueOf(TenantUtil.getTenantId()));
            if (!ObjectUtils.isEmpty(headerParam)) {
                headers.putAll(headerParam);
                headerParam.clear();
            }

            HttpEntity<String> requestEntity = RestTemplateUtil.buildRequestEntity(
                XDMDelegatorJsonUtil.object2string(param), headers);
            rdmResultVO = this.postForEntity(url, requestEntity);
            if (CollectionUtils.isEmpty(rdmResultVO.getErrors())) {
                // 请求成功跳出循环
                break;
            }

            // 失败时会进行有限次数重试，重试次数可以手动设置
            List<RDMErrorVO> errorVOList = rdmResultVO.getErrors();
            StringBuffer messages = new StringBuffer();
            // 若业务请求失败是由token失效引起，将会通过needCleanTokenCache()，清除本地缓存token
            if (!this.needCleanTokenCache((RDMErrorVO) errorVOList.get(0))) {
                assembleMessage(errorVOList, messages);
            }

            ++i;
        } while (i <= this.retryNumber);

        if (!CollectionUtils.isEmpty(rdmResultVO.getErrors())) {
            List<RDMErrorVO> errorVoList = rdmResultVO.getErrors();
            assembleMessage(errorVoList, new StringBuffer());
        }

        return rdmResultVO;
    }
}
```

## 6、返回结果示例

接口调用工具/客户端SDK返回信息相同，以下统一展示。

### 6.1 Headers

Header中“X-Subject-Token”存储了当前Token的信息。

```json
{
  "Server": "CloudWAF",
  "Date": "Thu, 09 May 2024 07:54:37 GMT",
  "Content-Type": "application/json; charset=UTF-8",
  "Content-Length": "16542",
  "Connection": "keep-alive",
  "X-IAM-Trace-Id": "token_cn-north-4_null_ba1c9ea67823851abbc5400167b13a57",
  "X-Subject-Token": "****************************************************",
  "X-Request-Id": "ba1c9ea67823851abbc5400167b13a57",
  "Strict-Transport-Security": "max-age=31536000; includeSubdomains;",
  "X-Frame-Options": "SAMEORIGIN",
  "X-Content-Type-Options": "nosniff",
  "X-Download-Options": "noopen",
  "X-XSS-Protection": "1; mode=block;"
}
```

### 6.2 Body

Body中“expires_at”存储了当前Token的过期时间。

```json
{
  "token": {
    "expires_at": "2024-05-10T07:54:37.835000Z",
    "methods": [
      "password"
    ],
    "catalog": [
      {
        "endpoints": [
          {
            "id": "33e1cbdd86d34e89a63cf8ad16a5f49f",
            "interface": "public",
            "region": "*",
            "region_id": "*",
            "url": "https://iam.myhuaweicloud.com/v3.0"
          }
        ],
        "id": "100a6a3477f1495286579b819d399e36",
        "name": "iam",
        "type": "iam"
      }
    ],
    "roles": [
      {
        "id": "0",
        "name": "te_admin"
      }
    ],
    "project": {
      "domain": {
        "id": "4a90bf899f8643e0b38d3ebb8524b31c",
        "name": "******"
      },
      "id": "444334286458452da1fc7aed0e1b01ba",
      "name": "cn-north-4"
    },
    "issued_at": "2024-05-09T07:54:37.835000Z",
    "user": {
      "domain": {
        "id": "4a90bf899f8643e0b38d3ebb8524b31c",
        "name": "******"
      },
      "id": "1b628f2e5d9249aba0704de29e73f2c1",
      "name": "******",
      "password_expires_at": ""
    }
  }
}
```

## 7、参考链接

更多信息参考：
- [客户端SDK概述](https://support.huaweicloud.com/sdkreference-idme/idme_csdk_0001.html)
- [iDME全量数据服务API封装](https://codelabs.developer.huaweicloud.com/codelabs/samples/1a913b978cb24de797fb6bbe9d95d19b#id-17262182011593152)

## 9、修订记录

|    发布日期    | 文档版本 |  修订说明  |
|:----------:|:----:|:------:|
| 2024-05-09 | 1.0  | 文档首次发布 |