## Version Description
In this sample, the SDK version is 3.0.65.

## Example of Enabling an AS Group

This example shows how to use the Java SDK to enable an Auto Scaling (AS) group.

## Function Description
This API is used to enable a specified AS group.

## Prerequisites

- You have [signed up](https://id5.cloud.huawei.com/CAS/portal/userRegister/regbyphone.html?&lang=en-us) to Huawei Cloud and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?locale=en-us#/accountindex/realNameAuth).

- You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

- You have obtained a pair of access key ID (AK) and secret access key (SK) for your Huawei Cloud account. To create or view an AK/SK pair, choose **My Credentials** > **Access Keys** on the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).

- You have set up the development environment with Java JDK 1.8 or later.

## Sample Code
The following code shows how to use the SDK to enable an AS group:

```java
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.as.v1.region.AsRegion;
import com.huaweicloud.sdk.as.v1.AsClient;
import com.huaweicloud.sdk.as.v1.model.ResumeScalingGroupOption;
import com.huaweicloud.sdk.as.v1.model.ResumeScalingGroupRequest;
import com.huaweicloud.sdk.as.v1.model.ResumeScalingGroupResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Enable an AS group.
 */
public class ASResumeScalingGroupDemo {
    private static final Logger logger = LoggerFactory.getLogger(ASResumeScalingGroupDemo.class.getName());

    public static void main(String[] args) {
        String ak = "<YOUR AK>";
        String sk = "<YOUR SK>";

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        AsClient client = AsClient.newBuilder()
            .withCredential(auth)
            .withRegion(AsRegion.valueOf("cn-east-2"))
            .build();

        ResumeScalingGroupOption body = new ResumeScalingGroupOption();
        body.withAction(ResumeScalingGroupOption.ActionEnum.valueOf("resume"));

        ResumeScalingGroupRequest request = new ResumeScalingGroupRequest();
        request.withScalingGroupId("{ScalingGroupId}");
        request.withBody(body);
        try {
            ResumeScalingGroupResponse response = client.resumeScalingGroup(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }
}


```

You can directly run and debug the API in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/AS/doc?api=ResumeScalingGroup).

##  Change History

|  Date | Issue|   Description  |
| :--------: | :------: | :----------: |
| 2021-10-18 |   1.0    | This issue is the first official release.|
