## 版本说明
本示例配套的SDK版本为：3.0.65

## 启用弹性伸缩组相关示例

本示例展示如何通过java版本的SDK方式启用弹性伸缩组。

## 功能介绍
启用一个指定弹性伸缩组。

## 前置条件

1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

## 代码示例
以下代码展示如何使用SDK启用弹性伸缩组：

```java
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.as.v1.region.AsRegion;
import com.huaweicloud.sdk.as.v1.AsClient;
import com.huaweicloud.sdk.as.v1.model.ResumeScalingGroupOption;
import com.huaweicloud.sdk.as.v1.model.ResumeScalingGroupRequest;
import com.huaweicloud.sdk.as.v1.model.ResumeScalingGroupResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 启用伸缩组
 */
public class ASResumeScalingGroupDemo {
    private static final Logger logger = LoggerFactory.getLogger(ASResumeScalingGroupDemo.class.getName());

    public static void main(String[] args) {
        String ak = "<YOUR AK>";
        String sk = "<YOUR SK>";

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        AsClient client = AsClient.newBuilder()
            .withCredential(auth)
            .withRegion(AsRegion.valueOf("cn-east-2"))
            .build();

        ResumeScalingGroupOption body = new ResumeScalingGroupOption();
        body.withAction(ResumeScalingGroupOption.ActionEnum.valueOf("resume"));

        ResumeScalingGroupRequest request = new ResumeScalingGroupRequest();
        request.withScalingGroupId("{ScalingGroupId}");
        request.withBody(body);
        try {
            ResumeScalingGroupResponse response = client.resumeScalingGroup(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }
}


```

您可以在[API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=AS&api=ResumeScalingGroup)中直接运行调试该接口。

##  修订记录

|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2021-10-18 |   1.0    | 文档首次发布 |







