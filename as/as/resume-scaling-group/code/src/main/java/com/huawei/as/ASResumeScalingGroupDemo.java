package com.huawei.as;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.as.v1.region.AsRegion;
import com.huaweicloud.sdk.as.v1.AsClient;
import com.huaweicloud.sdk.as.v1.model.ResumeScalingGroupOption;
import com.huaweicloud.sdk.as.v1.model.ResumeScalingGroupRequest;
import com.huaweicloud.sdk.as.v1.model.ResumeScalingGroupResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Enabling an AS Group
 */
public class ASResumeScalingGroupDemo {
    private static final Logger logger = LoggerFactory.getLogger(ASResumeScalingGroupDemo.class.getName());

    public static void main(String[] args) {
        String ak = "<YOUR AK>";
        String sk = "<YOUR SK>";

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        AsClient client = AsClient.newBuilder()
            .withCredential(auth)
            .withRegion(AsRegion.valueOf("cn-east-2"))
            .build();

        ResumeScalingGroupOption body = new ResumeScalingGroupOption();
        body.withAction(ResumeScalingGroupOption.ActionEnum.valueOf("resume"));

        ResumeScalingGroupRequest request = new ResumeScalingGroupRequest();
        request.withScalingGroupId("{ScalingGroupId}");
        request.withBody(body);
        try {
            ResumeScalingGroupResponse response = client.resumeScalingGroup(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }
}