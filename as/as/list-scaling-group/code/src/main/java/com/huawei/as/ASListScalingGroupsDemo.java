package com.huawei.as;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.as.v1.region.AsRegion;
import com.huaweicloud.sdk.as.v1.AsClient;
import com.huaweicloud.sdk.as.v1.model.ListScalingGroupsRequest;
import com.huaweicloud.sdk.as.v1.model.ListScalingGroupsResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Querying AS Groups
 */
public class ASListScalingGroupsDemo {
    private static final Logger logger = LoggerFactory.getLogger(ASListScalingGroupsDemo.class.getName());

    public static void main(String[] args) {
        String ak = "<YOUR_AK>";
        String sk = "<YOUR_SK>";

        ICredential credential = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        AsClient client = AsClient.newBuilder()
            .withCredential(credential)
            .withRegion(AsRegion.valueOf("cn-east-2"))
            .build();

        ListScalingGroupsRequest request = new ListScalingGroupsRequest();
        request.withScalingGroupName("{ScalingGroupName}");
        request.withStartNumber(0);
        request.withLimit(20);
        try {
            ListScalingGroupsResponse response = client.listScalingGroups(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.getMessage());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }
}