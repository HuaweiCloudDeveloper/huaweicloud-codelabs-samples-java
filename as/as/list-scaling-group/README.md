## 版本说明
本示例配套的SDK版本为：3.0.65

## 查询弹性伸缩组列表相关示例

本示例展示如何通过java版本的SDK方式查询弹性伸缩组列表。

## 功能介绍
根据输入条件过滤查询弹性伸缩组列表。查询结果分页显示。

可根据伸缩组名称，伸缩配置ID，伸缩组状态，企业项目ID，起始行号，记录条数进行条件过滤查询。
若不加过滤条件默认最多查询租户下20条伸缩组信息。

## 前置条件

1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

## 代码示例
以下代码展示如何使用SDK查询伸缩组列表：

```java
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.as.v1.region.AsRegion;
import com.huaweicloud.sdk.as.v1.AsClient;
import com.huaweicloud.sdk.as.v1.model.ListScalingGroupsRequest;
import com.huaweicloud.sdk.as.v1.model.ListScalingGroupsResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 查询伸缩组列表
 */
public class ASListScalingGroupsDemo {
    private static final Logger logger = LoggerFactory.getLogger(ASListScalingGroupsDemo.class.getName());

    public static void main(String[] args) {
        String ak = "<YOUR_AK>";
        String sk = "<YOUR_SK>";

        ICredential credential = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        AsClient client = AsClient.newBuilder()
            .withCredential(credential)
            .withRegion(AsRegion.valueOf("cn-east-2"))
            .build();

        ListScalingGroupsRequest request = new ListScalingGroupsRequest();
        request.withScalingGroupName("{ScalingGroupName}");
        request.withStartNumber(0);
        request.withLimit(20);
        try {
            ListScalingGroupsResponse response = client.listScalingGroups(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.getMessage());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }
}


```

您可以在[API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=AS&api=ListScalingGroups)中直接运行调试该接口。

##  修订记录

|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2021-10-18 |   1.0    | 文档首次发布 |







