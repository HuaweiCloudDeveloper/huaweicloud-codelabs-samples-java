package com.huawei.as;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.as.v1.region.AsRegion;
import com.huaweicloud.sdk.as.v1.AsClient;
import com.huaweicloud.sdk.as.v1.model.CreateScalingGroupOption;
import com.huaweicloud.sdk.as.v1.model.CreateScalingGroupRequest;
import com.huaweicloud.sdk.as.v1.model.CreateScalingGroupResponse;
import com.huaweicloud.sdk.as.v1.model.Networks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.ArrayList;

/**
 * Creating an AS Group
 */
public class ASCreateScalingGroupDemo {
    private static final Logger logger = LoggerFactory.getLogger(ASCreateScalingGroupDemo.class.getName());

    public static void main(String[] args) {
        String ak = "<YOUR AK>";
        String sk = "<YOUR SK>";

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        AsClient asClient = AsClient.newBuilder()
            .withCredential(auth)
            .withRegion(AsRegion.valueOf("cn-north-4"))
            .build();

        CreateScalingGroupRequest request = new CreateScalingGroupRequest();
        CreateScalingGroupOption body = new CreateScalingGroupOption();
        request.withBody(body);
        Networks networks = new Networks();
        networks.withId("{network id}");
        List<Networks> networksList = new ArrayList<>();
        networksList.add(networks);
        body.withScalingGroupName("{ScalingGroupName}");
        body.withScalingConfigurationId("{ScalingConfigurationId}");
        body.withDesireInstanceNumber(1);
        body.withMinInstanceNumber(0);
        body.withMaxInstanceNumber(1);
        body.withNetworks(networksList);
        body.withVpcId("{vpc id}");
        try {
            CreateScalingGroupResponse response = asClient.createScalingGroup(request);
            logger.info(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }
}