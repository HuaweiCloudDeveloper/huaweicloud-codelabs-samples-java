## Version Description
In this sample, the SDK version is 3.0.65.

## Example of Creating an AS Group

This example shows how to use the Java SDK to create an Auto Scaling (AS) group.

## Function Description
An AS group is a collection of instances that apply to the same scenario. It is the basis for enabling or disabling AS policies and performing scaling actions. An AS group specifies parameters, such as the maximum number of instances, expected number of instances, minimum number of instances, VPC, subnet, and load balancing.

This sample shows how you can use the Java SDK to create an AS group.

## Prerequisites

- You have [signed up](https://id5.cloud.huawei.com/CAS/portal/userRegister/regbyphone.html?&lang=en-us) to Huawei Cloud and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?locale=en-us#/accountindex/realNameAuth).

- You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

- You have obtained a pair of access key ID (AK) and secret access key (SK) for your Huawei Cloud account. To create or view an AK/SK pair, choose **My Credentials** > **Access Keys** on the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).

- You have set up the development environment with Java JDK 1.8 or later.

## Sample Code

```java
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.as.v1.region.AsRegion;
import com.huaweicloud.sdk.as.v1.AsClient;
import com.huaweicloud.sdk.as.v1.model.CreateScalingGroupOption;
import com.huaweicloud.sdk.as.v1.model.CreateScalingGroupRequest;
import com.huaweicloud.sdk.as.v1.model.CreateScalingGroupResponse;
import com.huaweicloud.sdk.as.v1.model.Networks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.ArrayList;

/**
 * Create an AS group.
 */
public class ASCreateScalingGroupDemo {
    private static final Logger logger = LoggerFactory.getLogger(ASCreateScalingGroupDemo.class.getName());

    public static void main(String[] args) {
        String ak = "<YOUR AK>";
        String sk = "<YOUR SK>";

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        AsClient asClient = AsClient.newBuilder()
            .withCredential(auth)
            .withRegion(AsRegion.valueOf("cn-north-4"))
            .build();

        CreateScalingGroupRequest request = new CreateScalingGroupRequest();
        CreateScalingGroupOption body = new CreateScalingGroupOption();
        request.withBody(body);
        Networks networks = new Networks();
        networks.withId("{network id}");
        List<Networks> networksList = new ArrayList<>();
        networksList.add(networks);
        body.withScalingGroupName("{ScalingGroupName}");
        body.withScalingConfigurationId("{ScalingConfigurationId}");
        body.withDesireInstanceNumber(1);
        body.withMinInstanceNumber(0);
        body.withMaxInstanceNumber(1);
        body.withNetworks(networksList);
        body.withVpcId("{vpc id}");
        try {
            CreateScalingGroupResponse response = asClient.createScalingGroup(request);
            logger.info(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }
}

```

You can directly run and debug the API in [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/AS/doc?api=CreateScalingGroup).

##  Change History

|  Date | Issue|   Description  |
| :--------: | :------: | :----------: |
| 2021-10-18 |   1.0    | This issue is the first official release.|
