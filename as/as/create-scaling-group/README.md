## 版本说明
本示例配套的SDK版本为：3.0.65

## 创建弹性伸缩组相关示例

本示例展示如何通过java版本的SDK方式创建弹性伸缩组。

## 功能介绍
伸缩组是具有相同应用场景的实例的集合，是启停伸缩策略和进行伸缩活动的基本单位。伸缩组内定义了最大实例数、期望实例数、最小实例数、虚拟私有云、子网、负载均衡等信息。

该示例展示了如何通过java版SDK创建弹性伸缩组。

## 前置条件

1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

## 代码示例

```java
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.as.v1.region.AsRegion;
import com.huaweicloud.sdk.as.v1.AsClient;
import com.huaweicloud.sdk.as.v1.model.CreateScalingGroupOption;
import com.huaweicloud.sdk.as.v1.model.CreateScalingGroupRequest;
import com.huaweicloud.sdk.as.v1.model.CreateScalingGroupResponse;
import com.huaweicloud.sdk.as.v1.model.Networks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.ArrayList;

/**
 * 创建伸缩组
 */
public class ASCreateScalingGroupDemo {
    private static final Logger logger = LoggerFactory.getLogger(ASCreateScalingGroupDemo.class.getName());

    public static void main(String[] args) {
        String ak = "<YOUR AK>";
        String sk = "<YOUR SK>";

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        AsClient asClient = AsClient.newBuilder()
            .withCredential(auth)
            .withRegion(AsRegion.valueOf("cn-north-4"))
            .build();

        CreateScalingGroupRequest request = new CreateScalingGroupRequest();
        CreateScalingGroupOption body = new CreateScalingGroupOption();
        request.withBody(body);
        Networks networks = new Networks();
        networks.withId("{network id}");
        List<Networks> networksList = new ArrayList<>();
        networksList.add(networks);
        body.withScalingGroupName("{ScalingGroupName}");
        body.withScalingConfigurationId("{ScalingConfigurationId}");
        body.withDesireInstanceNumber(1);
        body.withMinInstanceNumber(0);
        body.withMaxInstanceNumber(1);
        body.withNetworks(networksList);
        body.withVpcId("{vpc id}");
        try {
            CreateScalingGroupResponse response = asClient.createScalingGroup(request);
            logger.info(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }
}

```

您可以在[API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=AS&api=CreateScalingGroup)中直接运行调试该接口。

##  修订记录

|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2021-10-18 |   1.0    | 文档首次发布 |







