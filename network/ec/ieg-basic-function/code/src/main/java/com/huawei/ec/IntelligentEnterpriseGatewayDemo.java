package com.huawei.ec;

import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.ec.v1.EcClient;
import com.huaweicloud.sdk.ec.v1.model.ListIegRequest;
import com.huaweicloud.sdk.ec.v1.model.ListIegResponse;
import com.huaweicloud.sdk.ec.v1.model.ShowIegInfoRequest;
import com.huaweicloud.sdk.ec.v1.model.ShowIegInfoResponse;
import com.huaweicloud.sdk.ec.v1.model.UpdateIegRequest;
import com.huaweicloud.sdk.ec.v1.model.UpdateIegRequestBody;
import com.huaweicloud.sdk.ec.v1.model.UpdateIegResponse;
import com.huaweicloud.sdk.ec.v1.region.EcRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

public class IntelligentEnterpriseGatewayDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(IntelligentEnterpriseGatewayDemo.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String iegId = "<your ieg id>";

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        EcClient client = EcClient.newBuilder()
            .withCredential(auth)
            .withRegion(EcRegion.CN_NORTH_4)
            .build();

        // Update Intelligent Enterprise Gateway
        updateIeg(client, iegId);

        // Show Intelligent Enterprise Gateway
        showIegInfo(client, iegId);

        // List Intelligent Enterprise Gateway
        listIeg(client);
    }

    private static UpdateIegResponse updateIeg(EcClient client, String iegId) {
        UpdateIegRequest request = new UpdateIegRequest();
        UpdateIegRequestBody body = new UpdateIegRequestBody();
        body.withName("<your new ieg name>")
            .withHighAvailability(UpdateIegRequestBody.HighAvailabilityEnum.DOUBLE);
        request.withIegId(iegId).withBody(body);
        Function<Void, UpdateIegResponse> task = (Void v) -> client.updateIeg(request);
        return execute(task);
    }

    private static ShowIegInfoResponse showIegInfo(EcClient client, String iegId) {
        ShowIegInfoRequest request = new ShowIegInfoRequest();
        request.withIegId(iegId);
        Function<Void, ShowIegInfoResponse> task = (Void v) -> client.showIegInfo(request);
        return execute(task);
    }

    private static ListIegResponse listIeg(EcClient client) {
        ListIegRequest request = new ListIegRequest();
        Function<Void, ListIegResponse> task = (Void v) -> client.listIeg(request);
        return execute(task);
    }

    private static <T> T execute(Function<Void, T> task) {
        T response = null;
        try {
            response = task.apply(null);
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
        return response;
    }
}
