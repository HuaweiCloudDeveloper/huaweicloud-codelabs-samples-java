## 1、介绍
**什么是智能企业网关？**

[智能企业网关](https://support.huaweicloud.com/productdesc-ec/ec_01_0003.html)，是华为云提供的企业侧的智能网关。智能企业网关设备（简称IEG设备）是智能企业网关的硬件设备形态，支持的设备款型为华为AR651。

**您将学到什么？**

如何通过java版SDK来体验智能企业网关的基本功能，包括更新、查询和列表查询

## 2、前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云租户账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 3、华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。
- 4、用户需要在企业连接服务（Enterprise Connect）中购买智能企业网关（Intelligent Enterprise Gateway）。

## 3、SDK获取和安装
您可以通过Maven配置所依赖的企业连接服务SDK
具体的SDK版本号请参见[SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/EC?lang=Java)

## 4、接口参数说明
关于接口参数的详细说明可参见：

[a.更新IEG](https://console.huaweicloud.com/apiexplorer/#/openapi/EC/doc?api=UpdateIeg)

[b.查询IEG](https://console.huaweicloud.com/apiexplorer/#/openapi/EC/doc?api=ShowIegInfo)

[c.列表IEG](https://console.huaweicloud.com/apiexplorer/#/openapi/EC/doc?api=ListIeg)

## 5、代码示例
以下代码展示如何使用智能企业网关（Intelligent Enterprise Gateway）相关SDK
```java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.ec.v1.EcClient;
import com.huaweicloud.sdk.ec.v1.model.ListIegRequest;
import com.huaweicloud.sdk.ec.v1.model.ListIegResponse;
import com.huaweicloud.sdk.ec.v1.model.ShowIegInfoRequest;
import com.huaweicloud.sdk.ec.v1.model.ShowIegInfoResponse;
import com.huaweicloud.sdk.ec.v1.model.UpdateIegRequest;
import com.huaweicloud.sdk.ec.v1.model.UpdateIegRequestBody;
import com.huaweicloud.sdk.ec.v1.model.UpdateIegResponse;
import com.huaweicloud.sdk.ec.v1.region.EcRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

public class IntelligentEnterpriseGatewayDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(IntelligentEnterpriseGatewayDemo.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String iegId = "{ieg_id}";

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        EcClient client = EcClient.newBuilder()
            .withCredential(auth)
            .withRegion(EcRegion.CN_NORTH_4)
            .build();

        // Update Intelligent Enterprise Gateway
        updateIeg(client, iegId);

        // Show Intelligent Enterprise Gateway
        showIegInfo(client, iegId);

        // List Intelligent Enterprise Gateway
        listIeg(client);
    }

    private static UpdateIegResponse updateIeg(EcClient client, String iegId) {
        UpdateIegRequest request = new UpdateIegRequest();
        UpdateIegRequestBody body = new UpdateIegRequestBody();
        body.withName("<your new ieg name>")
            .withHighAvailability(UpdateIegRequestBody.HighAvailabilityEnum.DOUBLE);
        request.withIegId(iegId).withBody(body);
        Function<Void, UpdateIegResponse> task = (Void v) -> client.updateIeg(request);
        return execute(task);
    }

    private static ShowIegInfoResponse showIegInfo(EcClient client, String iegId) {
        ShowIegInfoRequest request = new ShowIegInfoRequest();
        request.withIegId(iegId);
        Function<Void, ShowIegInfoResponse> task = (Void v) -> client.showIegInfo(request);
        return execute(task);
    }

    private static ListIegResponse listIeg(EcClient client) {
        ListIegRequest request = new ListIegRequest();
        Function<Void, ListIegResponse> task = (Void v) -> client.listIeg(request);
        return execute(task);
    }

    private static <T> T execute(Function<Void, T> task) {
        T response = null;
        try {
            response = task.apply(null);
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
        return response;
    }
}

```

## 6、修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-08-23 | 1.0 | 文档首次发布 |