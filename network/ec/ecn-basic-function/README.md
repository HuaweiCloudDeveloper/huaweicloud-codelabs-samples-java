## 1、介绍
**什么是企业连接网络？**

[企业连接网络](https://support.huaweicloud.com/productdesc-ec/ec_01_0004.html)，是由华为云高性能分布式智能企业网关组成的跨地域网络。企业侧IEG设备就近选择华为云Region或接入点，与华为云SGW网关建立连接，实现客户本地网络快速上云。

**您将学到什么？**

如何通过java版SDK来体验企业连接网络的基本功能，包括更新、查询和列表查询

## 2、前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云租户账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 3、华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。
- 4、用户需要在企业连接服务（Enterprise Connect）中购买企业连接网络（Enterprise Connect Network）。

## 3、SDK获取和安装
您可以通过Maven配置所依赖的企业连接服务SDK
具体的SDK版本号请参见[SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/EC?lang=Java)

## 4、接口参数说明
关于接口参数的详细说明可参见：

[a.更新ECN](https://console.huaweicloud.com/apiexplorer/#/openapi/EC/doc?api=UpdateEcn)

[b.查询ECN](https://console.huaweicloud.com/apiexplorer/#/openapi/EC/doc?api=ShowEcnInfo)

[c.列表ECN](https://console.huaweicloud.com/apiexplorer/#/openapi/EC/doc?api=ListEcn)

## 5、代码示例
以下代码展示如何使用企业连接网络（Enterprise Connect Network）相关SDK
```java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.ec.v1.EcClient;
import com.huaweicloud.sdk.ec.v1.model.ListEcnRequest;
import com.huaweicloud.sdk.ec.v1.model.ListEcnResponse;
import com.huaweicloud.sdk.ec.v1.model.ShowEcnInfoRequest;
import com.huaweicloud.sdk.ec.v1.model.ShowEcnInfoResponse;
import com.huaweicloud.sdk.ec.v1.model.UpdateEcnRequest;
import com.huaweicloud.sdk.ec.v1.model.UpdateEcnRequestBody;
import com.huaweicloud.sdk.ec.v1.model.UpdateEcnResponse;
import com.huaweicloud.sdk.ec.v1.region.EcRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

public class EnterpriseConnectNetworkDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(EnterpriseConnectNetworkDemo.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String ecnId = "{ecn_id}";

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        EcClient client = EcClient.newBuilder()
            .withCredential(auth)
            .withRegion(EcRegion.CN_NORTH_4)
            .build();

        // Update Enterprise Connect Network
        updateEcn(client, ecnId);

        // Show Enterprise Connect Network
        showEcnInfo(client, ecnId);

        // List Enterprise Connect Network
        listEcn(client);
    }

    private static UpdateEcnResponse updateEcn(EcClient client, String ecnId) {
        UpdateEcnRequest request = new UpdateEcnRequest();
        UpdateEcnRequestBody body = new UpdateEcnRequestBody();
        body.withName("<your new ecn name>")
            .withDescription("<your new description>")
            .withEcnAsn(65510L)
            .withIegAsn(65520L)
            .withHubEnable(true);
        request.withEcnId(ecnId).withBody(body);
        Function<Void, UpdateEcnResponse> task = (Void v) -> client.updateEcn(request);
        return execute(task);
    }

    private static ShowEcnInfoResponse showEcnInfo(EcClient client, String ecnId) {
        ShowEcnInfoRequest request = new ShowEcnInfoRequest();
        request.withEcnId(ecnId);
        Function<Void, ShowEcnInfoResponse> task = (Void v) -> client.showEcnInfo(request);
        return execute(task);
    }

    private static ListEcnResponse listEcn(EcClient client) {
        ListEcnRequest request = new ListEcnRequest();
        Function<Void, ListEcnResponse> task = (Void v) -> client.listEcn(request);
        return execute(task);
    }

    private static <T> T execute(Function<Void, T> task) {
        T response = null;
        try {
            response = task.apply(null);
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
        return response;
    }
}
```

## 6、修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-08-23 | 1.0 | 文档首次发布 |