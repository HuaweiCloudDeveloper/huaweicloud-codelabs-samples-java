package com.huawei.ec;

import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.ec.v1.EcClient;
import com.huaweicloud.sdk.ec.v1.model.ListEcnRequest;
import com.huaweicloud.sdk.ec.v1.model.ListEcnResponse;
import com.huaweicloud.sdk.ec.v1.model.ShowEcnInfoRequest;
import com.huaweicloud.sdk.ec.v1.model.ShowEcnInfoResponse;
import com.huaweicloud.sdk.ec.v1.model.UpdateEcnRequest;
import com.huaweicloud.sdk.ec.v1.model.UpdateEcnRequestBody;
import com.huaweicloud.sdk.ec.v1.model.UpdateEcnResponse;
import com.huaweicloud.sdk.ec.v1.region.EcRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

public class EnterpriseConnectNetworkDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(EnterpriseConnectNetworkDemo.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String ecnId = "<your ecn id>";

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        EcClient client = EcClient.newBuilder()
            .withCredential(auth)
            .withRegion(EcRegion.CN_NORTH_4)
            .build();

        // Update Enterprise Connect Network
        updateEcn(client, ecnId);

        // Show Enterprise Connect Network
        showEcnInfo(client, ecnId);

        // List Enterprise Connect Network
        listEcn(client);
    }

    private static UpdateEcnResponse updateEcn(EcClient client, String ecnId) {
        UpdateEcnRequest request = new UpdateEcnRequest();
        UpdateEcnRequestBody body = new UpdateEcnRequestBody();
        body.withName("<your new ecn name>")
            .withDescription("<your new ecn description>")
            .withEcnAsn(65510L)
            .withIegAsn(65520L)
            .withHubEnable(false);
        request.withEcnId(ecnId).withBody(body);
        Function<Void, UpdateEcnResponse> task = (Void v) -> client.updateEcn(request);
        return execute(task);
    }

    private static ShowEcnInfoResponse showEcnInfo(EcClient client, String ecnId) {
        ShowEcnInfoRequest request = new ShowEcnInfoRequest();
        request.withEcnId(ecnId);
        Function<Void, ShowEcnInfoResponse> task = (Void v) -> client.showEcnInfo(request);
        return execute(task);
    }

    private static ListEcnResponse listEcn(EcClient client) {
        ListEcnRequest request = new ListEcnRequest();
        Function<Void, ListEcnResponse> task = (Void v) -> client.listEcn(request);
        return execute(task);
    }

    private static <T> T execute(Function<Void, T> task) {
        T response = null;
        try {
            response = task.apply(null);
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
        return response;
    }
}
