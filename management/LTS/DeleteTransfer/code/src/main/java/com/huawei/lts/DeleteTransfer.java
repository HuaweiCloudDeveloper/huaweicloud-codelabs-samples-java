package com.huawei.lts;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.lts.v2.LtsClient;
import com.huaweicloud.sdk.lts.v2.model.DeleteTransferRequest;
import com.huaweicloud.sdk.lts.v2.model.DeleteTransferResponse;
import com.huaweicloud.sdk.lts.v2.region.LtsRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteTransfer {

    private static final Logger logger = LoggerFactory.getLogger(DeleteTransfer.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String deleteTransferAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String deleteTransferSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(deleteTransferAk)
                .withSk(deleteTransferSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to LtsRegion.CN_NORTH_4.
        LtsClient client = LtsClient.newBuilder()
                .withCredential(auth)
                .withRegion(LtsRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request and set the request parameter log dump ID.
        DeleteTransferRequest request = new DeleteTransferRequest();
        // Log dump ID. The ID can be obtained in the following ways: 1. Invoke the interface for querying log dumps. The return value contains the log dump ID. 2. Invoke the interface for adding log dumps, the return value contains the log dump ID.
        String transferId = "<YOUR TRANSFER ID>";
        request.withLogTransferId(transferId);
        try {
            DeleteTransferResponse response = client.deleteTransfer(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}