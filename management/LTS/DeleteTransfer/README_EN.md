### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/LTS/doc?api=DeleteTransfer).

2.In this example, you can delete a log dump.

3.By deleting the log dump configuration, you can stop exporting specific log data to other storage services to meet your service requirements and reduce storage costs.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The current account has the permission to use LTS and has created a log dump task.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-lts. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-lts</artifactId>
    <version>3.1.120</version>
</dependency>

```

### Code example

``` java
package com.huawei.lts;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.lts.v2.LtsClient;
import com.huaweicloud.sdk.lts.v2.model.DeleteTransferRequest;
import com.huaweicloud.sdk.lts.v2.model.DeleteTransferResponse;
import com.huaweicloud.sdk.lts.v2.region.LtsRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteTransfer {

    private static final Logger logger = LoggerFactory.getLogger(DeleteTransfer.class.getName());

    public static void main(String[] args) {

        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String deleteTransferAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String deleteTransferSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(deleteTransferAk)
                .withSk(deleteTransferSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to LtsRegion.CN_NORTH_4.
        LtsClient client = LtsClient.newBuilder()
                .withCredential(auth)
                .withRegion(LtsRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request and set the request parameter log dump ID.
        DeleteTransferRequest request = new DeleteTransferRequest();
        // Log dump ID. The ID can be obtained in the following ways: 1. Invoke the interface for querying log dumps. The return value contains the log dump ID. 2. Invoke the interface for adding log dumps, the return value contains the log dump ID.
        String transferId = "<YOUR TRANSFER ID>";
        request.withLogTransferId(transferId);
        try {
            DeleteTransferResponse response = client.deleteTransfer(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result

#### DeleteTransfer

```
{
 "log_group_id": "277a****",
 "log_group_name": "lts-group-test",
 "log_group_name_alias": "lts-group-test",
 "log_streams": [
  {
   "log_stream_id": "1c2a****",
   "log_stream_name": "lts-topic-58gn",
   "log_stream_name_alias": "lts-topic-58gn"
  }
 ],
 "log_transfer_id": "cecb****",
 "log_transfer_info": {
  "log_create_time": 1730767464713,
  "log_storage_format": "RAW",
  "log_transfer_detail": {
   "obs_time_zone_id": "Etc/GMT",
   "obs_period": 3,
   "obs_prefix_name": "",
   "obs_period_unit": "hour",
   "obs_transfer_path": "/obs-demo/LogTanks/cn-north-4/",
   "obs_bucket_name": "obs-demo",
   "obs_encrypted_enable": false,
   "obs_dir_pre_fix_name": "",
   "obs_time_zone": "UTC"
  },
  "log_transfer_mode": "cycle",
  "log_transfer_status": "ENABLE",
  "log_transfer_type": "OBS"
 }
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/11/05 | 1.0 | This document is released for the first time.