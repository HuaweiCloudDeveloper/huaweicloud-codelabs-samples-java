package com.huawei.lts;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.lts.v2.LtsClient;
import com.huaweicloud.sdk.lts.v2.model.ListTransfersRequest;
import com.huaweicloud.sdk.lts.v2.model.ListTransfersResponse;
import com.huaweicloud.sdk.lts.v2.region.LtsRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListTransfers {

    private static final Logger logger = LoggerFactory.getLogger(ListTransfers.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listTransfersAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listTransfersSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listTransfersAk)
                .withSk(listTransfersSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to LtsRegion.CN_NORTH_4.
        LtsClient client = LtsClient.newBuilder()
                .withCredential(auth)
                .withRegion(LtsRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request.
        ListTransfersRequest request = new ListTransfersRequest();
        try {
            ListTransfersResponse response = client.listTransfers(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}