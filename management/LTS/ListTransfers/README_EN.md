### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/LTS/doc?api=ListTransfers).

2.In this example, you can query log dumps.

3.Query log dump. You can query log groups, log streams, and log dump details, including OBS dump, DIS dump, and DMS dump configurations.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.You have the permission to use LTS and have created log groups and log streams.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-lts. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-lts</artifactId>
    <version>3.1.120</version>
</dependency>

```

### Code example

``` java
package com.huawei.lts;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.lts.v2.LtsClient;
import com.huaweicloud.sdk.lts.v2.model.ListTransfersRequest;
import com.huaweicloud.sdk.lts.v2.model.ListTransfersResponse;
import com.huaweicloud.sdk.lts.v2.region.LtsRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListTransfers {

    private static final Logger logger = LoggerFactory.getLogger(ListTransfers.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listTransfersAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listTransfersSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listTransfersAk)
                .withSk(listTransfersSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to LtsRegion.CN_NORTH_4.
        LtsClient client = LtsClient.newBuilder()
                .withCredential(auth)
                .withRegion(LtsRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request.
        ListTransfersRequest request = new ListTransfersRequest();
        try {
            ListTransfersResponse response = client.listTransfers(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result

#### ListTransfers

```
{
 "environments": [
  {
   "id": "0a18****",
   "name": "env-test",
   "alias": null,
   "description": "",
   "project_id": "16d5****",
   "enterprise_project_id": "ee9f****",
   "charge_mode": "provided",
   "vpc_id": "9bc1****",
   "base_resources": [
    {
     "id": "0b6e****",
     "az": null,
     "cse": false,
     "type": "cce",
     "name": "demouser"
    }
   ],
   "optional_resources": [
    {
     "id": "bb5c****",
     "az": null,
     "cse": true,
     "type": "cse",
     "name": "cse-demo"
    }
   ],
   "creator": "demouser",
   "create_time": 1723169055601,
   "update_time": 1724029659322,
   "deploy_mode": "container",
   "vm_cluster_size": 50,
   "status": null,
   "broker_id": null,
   "labels": [],
   "offline_hosts": "",
   "alert_status": "successful",
   "component_count": 0,
   "type": "normal",
   "availability_zone": null
  }
 ],
 "count": 1
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/11/04 | 1.0 | This document is released for the first time.