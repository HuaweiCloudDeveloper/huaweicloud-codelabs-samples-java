## 1.介绍
IAM访问分析器（IAM Access Analyzer）帮助您识别组织或账号中与外部共享的资源，例如OBS桶策略、KMS密钥或IAM委托，此类访问会带来安全风险。
访问分析器主要提供以下两个功能：识别组织或账号中和外部主体共享的资源。通过访问分析，可以直观地了解组织或账号和外部主体共享的资源，从而及时识别资源和数据的访问风险。根据策略语法验证自定义策略。通过策略检查来验证策略，并且提供检查结果。检查结果包括：安全、警告、错误、建议。
该示例展现了如何通过java版本的SDK创建分析器归档规则。

## 2.开发流程图

![](assets/image.png)

## 3.前置条件

1. 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
2. 您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 访问密钥 。
3. 华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。

## 4.关键代码片段
```java
public class AnalyzerArchiveRuleDemo {
    private static final Logger logger = LoggerFactory.getLogger(AnalyzerArchiveRuleDemo.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String iamEndpoint = "<iam endpoint>";

        HttpConfig ArchiveRuleDemoConfig = HttpConfig.getDefaultHttpConfig();
        ArchiveRuleDemoConfig.withIgnoreSSLVerification(true);
        ICredential ArchiveRuleDemoCredentials = new GlobalCredentials().withAk(ak).withSk(sk).withIamEndpoint(iamEndpoint);

        IAMAccessAnalyzerClient client = IAMAccessAnalyzerClient.newBuilder()
            .withHttpConfig(ArchiveRuleDemoConfig)
            .withCredential(ArchiveRuleDemoCredentials)
            .withRegion(new Region(regionId, endpoint))
            .build();

        AnalyzerArchiveRuleDemo analyzerArchiveRuleDemo = new AnalyzerArchiveRuleDemo();
        // 1、创建分析器
        String analyzerId = analyzerArchiveRuleDemo.CreateAnalyzer(client);
        // 2、创建归档规则
        String archiveRuleId = analyzerArchiveRuleDemo.createArchiveRule(client, analyzerId);
        // 3、应用归档规则
        analyzerArchiveRuleDemo.applyArchiveRule(client, analyzerId, archiveRuleId);
        // 4、更新归档规则
        analyzerArchiveRuleDemo.updateArchiveRule(client, analyzerId, archiveRuleId);
        // 5、删除归档规则
        analyzerArchiveRuleDemo.deleteArchiveRule(client, analyzerId, archiveRuleId);
        // 6、删除分析器
        analyzerArchiveRuleDemo.deleteAnalyzer(client, analyzerId);

    }

    public String CreateAnalyzer(IAMAccessAnalyzerClient client) {
        CreateAnalyzerRequest analyzerRequest = new CreateAnalyzerRequest();
        // 构建分析器的名称和类型
        analyzerRequest.withBody(new CreateAnalyzerReqBody().withName("Demo").withType(AnalyzerType.ACCOUNT));
        String analyzerId = "";
        try {
            CreateAnalyzerResponse analyzerResponse = client.createAnalyzer(analyzerRequest);
            analyzerId = analyzerResponse.getId();
            logger.info(analyzerResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
        return analyzerId;
    }

    public String createArchiveRule(IAMAccessAnalyzerClient client, String analyzerId) {
        if (analyzerId == null || analyzerId.isEmpty()) {
            return "";
        }
        CreateArchiveRuleRequest createArchiveRuleRequest = new CreateArchiveRuleRequest();
        createArchiveRuleRequest.withAnalyzerId(analyzerId);
        CreateArchiveRuleReqBody createArchiveRuleReqBody = new CreateArchiveRuleReqBody();
        // 构建存档规则的名称
        createArchiveRuleReqBody.withName("archive-rule-demo");
        // 构建存档规则的filters
        List<FindingFilter> filterList = createFilterList(FindingFilter.KeyEnum.RESOURCE_TYPE, "obs:bucket");
        createArchiveRuleReqBody.withFilters(filterList);
        // 构建请求body
        createArchiveRuleRequest.withBody(createArchiveRuleReqBody);
        String archiveRuleId = "";
        try {
            CreateArchiveRuleResponse archiveRule = client.createArchiveRule(createArchiveRuleRequest);
            archiveRuleId = archiveRule.getId();
            logger.info(archiveRule.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
        return archiveRuleId;
    }

    public void applyArchiveRule(IAMAccessAnalyzerClient client, String analyzerId, String archiveRuleId) {
        if (analyzerId == null || analyzerId.isEmpty() || archiveRuleId == null || archiveRuleId.isEmpty()) {
            return;
        }
        ApplyArchiveRuleRequest applyArchiveRuleRequest = new ApplyArchiveRuleRequest();
        applyArchiveRuleRequest.withAnalyzerId(analyzerId);
        applyArchiveRuleRequest.withArchiveRuleId(archiveRuleId);
        try {
            ApplyArchiveRuleResponse applyArchiveRuleResponse = client.applyArchiveRule(applyArchiveRuleRequest);
            logger.info(applyArchiveRuleResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void updateArchiveRule(IAMAccessAnalyzerClient client, String analyzerId, String archiveRuleId) {
        if (analyzerId == null || analyzerId.isEmpty() || archiveRuleId == null || archiveRuleId.isEmpty()) {
            return;
        }
        UpdateArchiveRuleRequest updateArchiveRuleRequest = new UpdateArchiveRuleRequest();
        updateArchiveRuleRequest.withArchiveRuleId(archiveRuleId);
        updateArchiveRuleRequest.withAnalyzerId(analyzerId);
        UpdateArchiveRuleReqBody updateArchiveRuleReqBody = new UpdateArchiveRuleReqBody();
        // 构建存档规则的filters
        List<FindingFilter> filterList = createFilterList(FindingFilter.KeyEnum.RESOURCE_TYPE, "iam:agency");
        updateArchiveRuleReqBody.withFilters(filterList);
        // 构建请求body
        updateArchiveRuleRequest.withBody(updateArchiveRuleReqBody);
        try {
            UpdateArchiveRuleResponse updateArchiveRuleResponse = client.updateArchiveRule(updateArchiveRuleRequest);
            logger.info(updateArchiveRuleResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void deleteArchiveRule(IAMAccessAnalyzerClient client, String analyzerId, String archiveRuleId) {
        if (analyzerId == null || analyzerId.isEmpty() || archiveRuleId == null || archiveRuleId.isEmpty()) {
            return;
        }
        DeleteArchiveRuleRequest deleteArchiveRuleRequest = new DeleteArchiveRuleRequest();
        deleteArchiveRuleRequest.withAnalyzerId(analyzerId);
        deleteArchiveRuleRequest.withArchiveRuleId(archiveRuleId);
        try {
            DeleteArchiveRuleResponse deleteArchiveRuleResponse = client.deleteArchiveRule(deleteArchiveRuleRequest);
            logger.info(deleteArchiveRuleResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void deleteAnalyzer(IAMAccessAnalyzerClient client, String analyzerId) {
        if (analyzerId == null || analyzerId.isEmpty()) {
            return;
        }
        DeleteAnalyzerRequest deleteRequest = new DeleteAnalyzerRequest().withAnalyzerId(analyzerId);
        try {
            DeleteAnalyzerResponse deleteResponse = client.deleteAnalyzer(deleteRequest);
            logger.info(deleteResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static List<FindingFilter> createFilterList(FindingFilter.KeyEnum key, String value) {
        // 构建filter的过滤键
        List<String> keyList = new ArrayList<>();
        keyList.add(value);
        // 构建filters参数
        FindingFilter findingFilter = new FindingFilter()
            .withKey(key)
            .withCriterion(new Criterion().withEq(keyList));
        List<FindingFilter> findingFilterList = new ArrayList<>();
        findingFilterList.add(findingFilter);
        return findingFilterList;
    }

    private static void LogError(ClientRequestException e) {
        logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        logger.error("RequestId: " + e.getRequestId());
        logger.error("ErrorCode: " + e.getErrorCode());
        logger.error("ErrorMsg: " + e.getErrorMsg());
    }
}
```

## 5.返回结果示例

5.1创建分析器返回结果示例：
```
{
"id": "035905e1-73e5-426a-a498-d04890c0fa92",
"urn": "AccessAnalyzer:cn-north-7:e74e043fab784a45ad88f5ef6a4bcffc:analyzer:035905e1-73e5-426a-a498-d04890c0fa92"
}
```

5.2创建归档规则返回结果示例：
```
{
  "id": "035905e1-73e5-426a-a498-d04890c0fa92",
  "urn": "AccessAnalyzer:cn-north-7:e74e043fab784a45ad88f5ef6a4bcffc:analyzer:035905e1-73e5-426a-a498-d04890c0fa92"
}
```

5.3应用归档规则返回结果示例：
```
无返回
```

5.4更新归档规则返回结果示例：
```
无返回
```

5.5删除归档规则返回结果示例：
```
无返回
```

5.6删除分析器返回结果示例：
```
无返回
```

## 6.修订记录
| 发布日期    | 文档版本  | 修订说明  |
|---------| ------------ | ------------ |
| 2024-03 | 1.0  | 文档首次发布  |