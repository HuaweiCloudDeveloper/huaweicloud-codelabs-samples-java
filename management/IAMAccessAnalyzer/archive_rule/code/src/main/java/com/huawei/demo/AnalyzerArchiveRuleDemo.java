package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.IAMAccessAnalyzerClient;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.AnalyzerType;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.ApplyArchiveRuleRequest;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.ApplyArchiveRuleResponse;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.CreateAnalyzerReqBody;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.CreateAnalyzerRequest;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.CreateAnalyzerResponse;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.CreateArchiveRuleReqBody;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.CreateArchiveRuleRequest;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.CreateArchiveRuleResponse;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.Criterion;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.DeleteAnalyzerRequest;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.DeleteAnalyzerResponse;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.DeleteArchiveRuleRequest;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.DeleteArchiveRuleResponse;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.FindingFilter;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.UpdateArchiveRuleReqBody;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.UpdateArchiveRuleRequest;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.UpdateArchiveRuleResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class AnalyzerArchiveRuleDemo {
    private static final Logger logger = LoggerFactory.getLogger(AnalyzerArchiveRuleDemo.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String iamEndpoint = "<iam endpoint>";

        HttpConfig ArchiveRuleDemoConfig = HttpConfig.getDefaultHttpConfig();
        ArchiveRuleDemoConfig.withIgnoreSSLVerification(true);
        ICredential ArchiveRuleDemoCredentials = new GlobalCredentials().withAk(ak).withSk(sk).withIamEndpoint(iamEndpoint);

        IAMAccessAnalyzerClient client = IAMAccessAnalyzerClient.newBuilder()
            .withHttpConfig(ArchiveRuleDemoConfig)
            .withCredential(ArchiveRuleDemoCredentials)
            .withRegion(new Region(regionId, endpoint))
            .build();

        AnalyzerArchiveRuleDemo analyzerArchiveRuleDemo = new AnalyzerArchiveRuleDemo();
        // 1、创建分析器
        String analyzerId = analyzerArchiveRuleDemo.CreateAnalyzer(client);
        // 2、创建归档规则
        String archiveRuleId = analyzerArchiveRuleDemo.createArchiveRule(client, analyzerId);
        // 3、应用归档规则
        analyzerArchiveRuleDemo.applyArchiveRule(client, analyzerId, archiveRuleId);
        // 4、更新归档规则
        analyzerArchiveRuleDemo.updateArchiveRule(client, analyzerId, archiveRuleId);
        // 5、删除归档规则
        analyzerArchiveRuleDemo.deleteArchiveRule(client, analyzerId, archiveRuleId);
        // 6、删除分析器
        analyzerArchiveRuleDemo.deleteAnalyzer(client, analyzerId);

    }

    public String CreateAnalyzer(IAMAccessAnalyzerClient client) {
        CreateAnalyzerRequest analyzerRequest = new CreateAnalyzerRequest();
        // 构建分析器的名称和类型
        analyzerRequest.withBody(new CreateAnalyzerReqBody().withName("Demo").withType(AnalyzerType.ACCOUNT));
        String analyzerId = "";
        try {
            CreateAnalyzerResponse analyzerResponse = client.createAnalyzer(analyzerRequest);
            analyzerId = analyzerResponse.getId();
            logger.info(analyzerResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
        return analyzerId;
    }

    public String createArchiveRule(IAMAccessAnalyzerClient client, String analyzerId) {
        if (analyzerId == null || analyzerId.isEmpty()) {
            return "";
        }
        CreateArchiveRuleRequest createArchiveRuleRequest = new CreateArchiveRuleRequest();
        createArchiveRuleRequest.withAnalyzerId(analyzerId);
        CreateArchiveRuleReqBody createArchiveRuleReqBody = new CreateArchiveRuleReqBody();
        // 构建存档规则的名称
        createArchiveRuleReqBody.withName("archive-rule-demo");
        // 构建存档规则的filters
        List<FindingFilter> filterList = createFilterList(FindingFilter.KeyEnum.RESOURCE_TYPE, "obs:bucket");
        createArchiveRuleReqBody.withFilters(filterList);
        // 构建请求body
        createArchiveRuleRequest.withBody(createArchiveRuleReqBody);
        String archiveRuleId = "";
        try {
            CreateArchiveRuleResponse archiveRule = client.createArchiveRule(createArchiveRuleRequest);
            archiveRuleId = archiveRule.getId();
            logger.info(archiveRule.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
        return archiveRuleId;
    }

    public void applyArchiveRule(IAMAccessAnalyzerClient client, String analyzerId, String archiveRuleId) {
        if (analyzerId == null || analyzerId.isEmpty() || archiveRuleId == null || archiveRuleId.isEmpty()) {
            return;
        }
        ApplyArchiveRuleRequest applyArchiveRuleRequest = new ApplyArchiveRuleRequest();
        applyArchiveRuleRequest.withAnalyzerId(analyzerId);
        applyArchiveRuleRequest.withArchiveRuleId(archiveRuleId);
        try {
            ApplyArchiveRuleResponse applyArchiveRuleResponse = client.applyArchiveRule(applyArchiveRuleRequest);
            logger.info(applyArchiveRuleResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void updateArchiveRule(IAMAccessAnalyzerClient client, String analyzerId, String archiveRuleId) {
        if (analyzerId == null || analyzerId.isEmpty() || archiveRuleId == null || archiveRuleId.isEmpty()) {
            return;
        }
        UpdateArchiveRuleRequest updateArchiveRuleRequest = new UpdateArchiveRuleRequest();
        updateArchiveRuleRequest.withArchiveRuleId(archiveRuleId);
        updateArchiveRuleRequest.withAnalyzerId(analyzerId);
        UpdateArchiveRuleReqBody updateArchiveRuleReqBody = new UpdateArchiveRuleReqBody();
        // 构建存档规则的filters
        List<FindingFilter> filterList = createFilterList(FindingFilter.KeyEnum.RESOURCE_TYPE, "iam:agency");
        updateArchiveRuleReqBody.withFilters(filterList);
        // 构建请求body
        updateArchiveRuleRequest.withBody(updateArchiveRuleReqBody);
        try {
            UpdateArchiveRuleResponse updateArchiveRuleResponse = client.updateArchiveRule(updateArchiveRuleRequest);
            logger.info(updateArchiveRuleResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void deleteArchiveRule(IAMAccessAnalyzerClient client, String analyzerId, String archiveRuleId) {
        if (analyzerId == null || analyzerId.isEmpty() || archiveRuleId == null || archiveRuleId.isEmpty()) {
            return;
        }
        DeleteArchiveRuleRequest deleteArchiveRuleRequest = new DeleteArchiveRuleRequest();
        deleteArchiveRuleRequest.withAnalyzerId(analyzerId);
        deleteArchiveRuleRequest.withArchiveRuleId(archiveRuleId);
        try {
            DeleteArchiveRuleResponse deleteArchiveRuleResponse = client.deleteArchiveRule(deleteArchiveRuleRequest);
            logger.info(deleteArchiveRuleResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void deleteAnalyzer(IAMAccessAnalyzerClient client, String analyzerId) {
        if (analyzerId == null || analyzerId.isEmpty()) {
            return;
        }
        DeleteAnalyzerRequest deleteRequest = new DeleteAnalyzerRequest().withAnalyzerId(analyzerId);
        try {
            DeleteAnalyzerResponse deleteResponse = client.deleteAnalyzer(deleteRequest);
            logger.info(deleteResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static List<FindingFilter> createFilterList(FindingFilter.KeyEnum key, String value) {
        // 构建filter的过滤键
        List<String> keyList = new ArrayList<>();
        keyList.add(value);
        // 构建filters参数
        FindingFilter findingFilter = new FindingFilter()
            .withKey(key)
            .withCriterion(new Criterion().withEq(keyList));
        List<FindingFilter> findingFilterList = new ArrayList<>();
        findingFilterList.add(findingFilter);
        return findingFilterList;
    }

    private static void LogError(ClientRequestException e) {
        logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        logger.error("RequestId: " + e.getRequestId());
        logger.error("ErrorCode: " + e.getErrorCode());
        logger.error("ErrorMsg: " + e.getErrorMsg());
    }
}
