package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.IAMAccessAnalyzerClient;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.PolicyType;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.ValidatePolicyReqBody;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.ValidatePolicyRequest;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.ValidatePolicyResourceType;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.ValidatePolicyResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ValidatePolicyDemo {

    private static final Logger logger = LoggerFactory.getLogger(ValidatePolicyDemo.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String iamEndpoint = "<iam endpoint>";

        HttpConfig validatePolicyDemoConfig = HttpConfig.getDefaultHttpConfig();
        validatePolicyDemoConfig.withIgnoreSSLVerification(true);
        ICredential validatePolicyDemoCredentials = new GlobalCredentials().withAk(ak).withSk(sk).withIamEndpoint(iamEndpoint);

        IAMAccessAnalyzerClient client = IAMAccessAnalyzerClient.newBuilder()
            .withHttpConfig(validatePolicyDemoConfig)
            .withCredential(validatePolicyDemoCredentials)
            .withRegion(new Region(regionId, endpoint))
            .build();
        ValidatePolicyRequest validatePolicyRequest = new ValidatePolicyRequest();
        ValidatePolicyReqBody validatePolicyReqBody = new ValidatePolicyReqBody();
        // 设置要校验的策略类型
        validatePolicyReqBody.withPolicyType(PolicyType.RESOURCE_POLICY);
        // 设置要附加到资源策略的资源类型
        validatePolicyReqBody.withValidatePolicyResourceType(ValidatePolicyResourceType.IAM_AGENCY);
        // 需要校验的json格式策略文档。
        validatePolicyReqBody.withPolicyDocument("your policy Document");
        validatePolicyRequest.withBody(validatePolicyReqBody);
        try {
            // 进行策略校验
            ValidatePolicyResponse validatePolicyResponse = client.validatePolicy(validatePolicyRequest);
            logger.info(validatePolicyResponse.toString());
        } catch (ClientRequestException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("RequestId: " + e.getRequestId());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }
}
