## 1.介绍
IAM访问分析器（IAM Access Analyzer）帮助您识别组织或账号中与外部共享的资源，例如OBS桶策略、KMS密钥或IAM委托，此类访问会带来安全风险。
访问分析器主要提供以下两个功能：识别组织或账号中和外部主体共享的资源。通过访问分析，可以直观地了解组织或账号和外部主体共享的资源，从而及时识别资源和数据的访问风险。根据策略语法验证自定义策略。通过策略检查来验证策略，并且提供检查结果。检查结果包括：安全、警告、错误、建议。
该示例展现了如何通过java版本的SDK进行策略校验。

## 2.开发流程图

![](assets/image.png)

## 3.前置条件

1. 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
2. 您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 访问密钥 。
3. 华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。

## 4.关键代码片段
```java
public class ValidatePolicyDemo {

    private static final Logger logger = LoggerFactory.getLogger(ValidatePolicyDemo.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String iamEndpoint = "<iam endpoint>";

        HttpConfig validatePolicyDemoConfig = HttpConfig.getDefaultHttpConfig();
        validatePolicyDemoConfig.withIgnoreSSLVerification(true);
        ICredential validatePolicyDemoCredentials = new GlobalCredentials().withAk(ak).withSk(sk).withIamEndpoint(iamEndpoint);

        IAMAccessAnalyzerClient client = IAMAccessAnalyzerClient.newBuilder()
            .withHttpConfig(validatePolicyDemoConfig)
            .withCredential(validatePolicyDemoCredentials)
            .withRegion(new Region(regionId, endpoint))
            .build();
        ValidatePolicyRequest validatePolicyRequest = new ValidatePolicyRequest();
        ValidatePolicyReqBody validatePolicyReqBody = new ValidatePolicyReqBody();
        // 设置要校验的策略类型
        validatePolicyReqBody.withPolicyType(PolicyType.RESOURCE_POLICY);
        // 设置要附加到资源策略的资源类型
        validatePolicyReqBody.withValidatePolicyResourceType(ValidatePolicyResourceType.IAM_AGENCY);
        // 需要校验的json格式策略文档。
        validatePolicyReqBody.withPolicyDocument("your policy Document");
        validatePolicyRequest.withBody(validatePolicyReqBody);
        try {
            // 进行策略校验
            ValidatePolicyResponse validatePolicyResponse = client.validatePolicy(validatePolicyRequest);
            logger.info(validatePolicyResponse.toString());
        } catch (ClientRequestException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("RequestId: " + e.getRequestId());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }
}
```

## 5.返回结果示例
```
{
  "findings": [
    {
      "finding_details": "修复索引 0 第 1 行第 0 列的 JSON 语法错误。",
      "finding_type": "error",
      "issue_code": "JSON_SYNTAX_ERROR",
      "learn_more_link": "https://{endpoint}/section0",
      "locations": [
        {
          "path": [],
          "span": {
            "start": {
              "line": 1,
              "column": 0,
              "offset": 0
            },
            "end": {
              "line": 1,
              "column": 1,
              "offset": 1
            }
          }
        }
      ]
    }
  ],
  "page_info": {
    "current_count": 1,
    "next_marker": null
  }
}
```

## 6.修订记录
| 发布日期    | 文档版本  | 修订说明  |
|---------| ------------ | ------------ |
| 2024-03 | 1.0  | 文档首次发布  |