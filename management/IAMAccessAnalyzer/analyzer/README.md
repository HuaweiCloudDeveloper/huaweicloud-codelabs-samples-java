## 1.介绍
IAM访问分析器（IAM Access Analyzer）帮助您识别组织或账号中与外部共享的资源，例如OBS桶策略、KMS密钥或IAM委托，此类访问会带来安全风险。
访问分析器主要提供以下两个功能：识别组织或账号中和外部主体共享的资源。通过访问分析，可以直观地了解组织或账号和外部主体共享的资源，从而及时识别资源和数据的访问风险。根据策略语法验证自定义策略。通过策略检查来验证策略，并且提供检查结果。检查结果包括：安全、警告、错误、建议。
该示例展现了如何通过java版本的SDK创建分析器和查询分析结果。

## 2.开发流程图

![](assets/image.png)

## 3.前置条件

1. 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
2. 您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 访问密钥 。
3. 华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。

## 4.关键代码片段
```java
public class CreateIAMAccessAnalyzerDemo {

    private static final Logger logger = LoggerFactory.getLogger(CreateIAMAccessAnalyzerDemo.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String iamEndpoint = "<iam endpoint>";

        HttpConfig accessAnalyzerDemoConfig = HttpConfig.getDefaultHttpConfig();
        accessAnalyzerDemoConfig.withIgnoreSSLVerification(true);
        ICredential accessAnalyzerDemoCredentials = new GlobalCredentials().withAk(ak)
            .withSk(sk)
            .withIamEndpoint(iamEndpoint);

        IAMAccessAnalyzerClient client = IAMAccessAnalyzerClient.newBuilder()
            .withHttpConfig(accessAnalyzerDemoConfig)
            .withCredential(accessAnalyzerDemoCredentials)
            .withRegion(new Region(regionId, endpoint))
            .build();
        CreateIAMAccessAnalyzerDemo createIAMAccessAnalyzerDemo = new CreateIAMAccessAnalyzerDemo();
        // 1、创建分析器
        String analyzerId = createIAMAccessAnalyzerDemo.CreateAnalyzer(client);
        // 2、查询指定分析器
        createIAMAccessAnalyzerDemo.showShowAnalyzer(client, analyzerId);
        // 3、检索指定分析器生成的结果列表
        String findingId = createIAMAccessAnalyzerDemo.ListFindings(client, analyzerId);
        // 4、查询指定分析结果的信息
        createIAMAccessAnalyzerDemo.ShowFinding(client, analyzerId, findingId);
        // 5、删除分析器
        createIAMAccessAnalyzerDemo.DeleteAnalyzer(client, analyzerId);

    }

    public String CreateAnalyzer(IAMAccessAnalyzerClient client) {
        CreateAnalyzerRequest createAnalyzerRequest = new CreateAnalyzerRequest();
        // 构建分析器的名称和类型
        createAnalyzerRequest.withBody(new CreateAnalyzerReqBody().withName("test").withType(AnalyzerType.ACCOUNT));
        String analyzerId = "";
        try {
            CreateAnalyzerResponse createAnalyzerResponse = client.createAnalyzer(createAnalyzerRequest);
            analyzerId = createAnalyzerResponse.getId();
            logger.info(createAnalyzerResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
        return analyzerId;
    }

    public void showShowAnalyzer(IAMAccessAnalyzerClient client, String analyzerId) {
        if (analyzerId == null || analyzerId.isEmpty()) {
            return;
        }
        ShowAnalyzerRequest showAnalyzerRequest = new ShowAnalyzerRequest();
        showAnalyzerRequest.withAnalyzerId(analyzerId);
        try {
            ShowAnalyzerResponse showAnalyzerResponse = client.showAnalyzer(showAnalyzerRequest);
            logger.info(showAnalyzerResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public String ListFindings(IAMAccessAnalyzerClient client, String analyzerId) {
        if (analyzerId == null || analyzerId.isEmpty()) {
            return "";
        }
        ListFindingsRequest listFindingsRequest = new ListFindingsRequest();
        listFindingsRequest.withAnalyzerId(analyzerId);
        // 构建filter的过滤键
        List<String> keyList = new ArrayList<>();
        keyList.add("active");
        // 构建filters参数
        FindingFilter findingFilter = new FindingFilter()
            .withKey(FindingFilter.KeyEnum.STATUS)
            .withCriterion(new Criterion().withEq(keyList));
        List<FindingFilter> findingFilterList = new ArrayList<>();
        findingFilterList.add(findingFilter);
        // 构建request的body
        listFindingsRequest.withBody(new ListFindingsReqBody().withLimit(100).withFilters(findingFilterList));
        String findingId = "";
        try {
            ListFindingsResponse listFindingsResponse = client.listFindings(listFindingsRequest);
            List<Finding> findings = listFindingsResponse.getFindings();
            if (!findings.isEmpty()) {
                findingId = findings.get(0).getId();
            }
            logger.info(listFindingsResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
        return findingId;
    }

    public void ShowFinding(IAMAccessAnalyzerClient client, String analyzerId, String findingId) {
        if (analyzerId == null || analyzerId.isEmpty() || findingId == null || findingId.isEmpty()) {
            return;
        }
        ShowFindingRequest showFindingRequest = new ShowFindingRequest().withAnalyzerId(analyzerId)
            .withFindingId(findingId);
        try {
            ShowFindingResponse showFindingResponse = client.showFinding(showFindingRequest);
            logger.info(showFindingResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void DeleteAnalyzer(IAMAccessAnalyzerClient client, String analyzerId) {
        if (analyzerId == null || analyzerId.isEmpty()) {
            return;
        }
        DeleteAnalyzerRequest deleteAnalyzerRequest = new DeleteAnalyzerRequest().withAnalyzerId(analyzerId);
        try {
            DeleteAnalyzerResponse deleteAnalyzerResponse = client.deleteAnalyzer(deleteAnalyzerRequest);
            logger.info(deleteAnalyzerResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        logger.error("RequestId: " + e.getRequestId());
        logger.error("ErrorCode: " + e.getErrorCode());
        logger.error("ErrorMsg: " + e.getErrorMsg());
    }
}
```

## 5.返回结果示例

5.1创建分析器返回结果示例：
```
{
"id": "035905e1-73e5-426a-a498-d04890c0fa92",
"urn": "AccessAnalyzer:cn-north-7:e74e043fab784a45ad88f5ef6a4bcffc:analyzer:035905e1-73e5-426a-a498-d04890c0fa92"
}
```

5.2查询指定分析器返回结果示例：
```
{
  "analyzer": {
    "created_at": "2023-09-07T07:51:10.502Z",
    "id": "035905e1-73e5-426a-a498-d04890c0fa92",
    "last_analyzed_resource": "AccessAnalyzer:cn-north-7:e74e043fab784a45ad88f5ef6a4bcffc:analyzer:035905e1-73e5-426a-a498-d04890c0fa92",
    "last_resource_analyzed_at": "2023-09-07T07:51:10.502Z",
    "name": "test-1",
    "status": "active",
    "tags": [
      {
        "key": "key-1",
        "value": "value-1"
      }
    ],
    "type": "account",
    "urn": "AccessAnalyzer:cn-north-7:e74e043fab784a45ad88f5ef6a4bcffc:analyzer:035905e1-73e5-426a-a498-d04890c0fa92"
  }
}
```

5.3检索指定分析器生成的结果列表返回结果示例：
```
{
  "findings": [
    {
      "action": [
        "sts:agencies:assume"
      ],
      "analyzed_at": "2023-09-07T08:04:41.698Z",
      "condition": [
        {
          "key": "key-1",
          "value": "value-1"
        }
      ],
      "created_at": "2023-09-07T08:04:41.698Z",
      "id": "fc5615d2-b0f3-4be3-9a12-55fcb4914cc1",
      "is_public": true,
      "principal": {
        "identifier": "09a92a3b0000d29e0fdbc01127966100",
        "type": "account"
      },
      "resource": "iam::602a50bf7afc4d6fbbcf5bf3c6bb9b5d:agency:ssa_admin_trust",
      "resource_owner_account": "602a50bf7afc4d6fbbcf5bf3c6bb9b5d",
      "resource_id": "340ac5a2-05a2-4372-9e8b-0de2fe163ede",
      "resource_type": "iam:agency",
      "status": "active",
      "updated_at": "2023-09-07T08:04:41.698Z"
    }
  ],
  "page_info": {
    "current_count": 1,
    "next_marker": null
  }

```

5.4查询指定分析结果返回结果示例：
```
{
  "finding": {
    "action": [
      "obs:bucket:listBucket"
    ],
    "analyzed_at": "2023-09-07T08:04:41.698Z",
    "condition": [
      {
        "key": "key-1",
        "value": "value-1"
      }
    ],
    "created_at": "2023-09-07T08:04:41.698Z",
    "id": "fc5615d2-b0f3-4be3-9a12-55fcb4914cc1",
    "is_public": true,
    "principal": {
      "identifier": "09a92a3b0000d29e0fdbc01127966100",
      "type": "account"
    },
    "resource": "obs:cn-north-4::bucket:bucket-name",
    "resource_owner_account": "602a50bf7afc4d6fbbcf5bf3c6bb9b5d",
    "resource_type": "obs:bucket",
    "sources": [
      "bucket_acl"
    ],
    "status": "active",
    "updated_at": "2023-09-07T08:04:41.698Z"
  }
}
```

5.5删除分析器返回结果示例：

无返回

## 6.修订记录
| 发布日期    | 文档版本  | 修订说明  |
|---------| ------------ | ------------ |
| 2024-03 | 1.0  | 文档首次发布  |