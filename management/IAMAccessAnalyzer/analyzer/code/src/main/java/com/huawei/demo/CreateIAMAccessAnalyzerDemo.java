package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.IAMAccessAnalyzerClient;

import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.AnalyzerType;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.CreateAnalyzerReqBody;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.CreateAnalyzerRequest;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.CreateAnalyzerResponse;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.Criterion;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.DeleteAnalyzerRequest;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.DeleteAnalyzerResponse;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.Finding;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.FindingFilter;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.ListFindingsReqBody;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.ListFindingsRequest;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.ListFindingsResponse;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.ShowAnalyzerRequest;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.ShowAnalyzerResponse;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.ShowFindingRequest;
import com.huaweicloud.sdk.iamaccessanalyzer.v1.model.ShowFindingResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CreateIAMAccessAnalyzerDemo {

    private static final Logger logger = LoggerFactory.getLogger(CreateIAMAccessAnalyzerDemo.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "{your regionId string}";
        String endpoint = "{your endpoint string}";
        String iamEndpoint = "<iam endpoint>";

        HttpConfig accessAnalyzerDemoConfig = HttpConfig.getDefaultHttpConfig();
        accessAnalyzerDemoConfig.withIgnoreSSLVerification(true);
        ICredential accessAnalyzerDemoCredentials = new GlobalCredentials().withAk(ak)
            .withSk(sk)
            .withIamEndpoint(iamEndpoint);

        IAMAccessAnalyzerClient client = IAMAccessAnalyzerClient.newBuilder()
            .withHttpConfig(accessAnalyzerDemoConfig)
            .withCredential(accessAnalyzerDemoCredentials)
            .withRegion(new Region(regionId, endpoint))
            .build();
        CreateIAMAccessAnalyzerDemo createIAMAccessAnalyzerDemo = new CreateIAMAccessAnalyzerDemo();
        // 1、创建分析器
        String analyzerId = createIAMAccessAnalyzerDemo.CreateAnalyzer(client);
        // 2、查询指定分析器
        createIAMAccessAnalyzerDemo.showShowAnalyzer(client, analyzerId);
        // 3、检索指定分析器生成的结果列表
        String findingId = createIAMAccessAnalyzerDemo.ListFindings(client, analyzerId);
        // 4、查询指定分析结果的信息
        createIAMAccessAnalyzerDemo.ShowFinding(client, analyzerId, findingId);
        // 5、删除分析器
        createIAMAccessAnalyzerDemo.DeleteAnalyzer(client, analyzerId);

    }

    public String CreateAnalyzer(IAMAccessAnalyzerClient client) {
        CreateAnalyzerRequest createAnalyzerRequest = new CreateAnalyzerRequest();
        // 构建分析器的名称和类型
        createAnalyzerRequest.withBody(new CreateAnalyzerReqBody().withName("test").withType(AnalyzerType.ACCOUNT));
        String analyzerId = "";
        try {
            CreateAnalyzerResponse createAnalyzerResponse = client.createAnalyzer(createAnalyzerRequest);
            analyzerId = createAnalyzerResponse.getId();
            logger.info(createAnalyzerResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
        return analyzerId;
    }

    public void showShowAnalyzer(IAMAccessAnalyzerClient client, String analyzerId) {
        if (analyzerId == null || analyzerId.isEmpty()) {
            return;
        }
        ShowAnalyzerRequest showAnalyzerRequest = new ShowAnalyzerRequest();
        showAnalyzerRequest.withAnalyzerId(analyzerId);
        try {
            ShowAnalyzerResponse showAnalyzerResponse = client.showAnalyzer(showAnalyzerRequest);
            logger.info(showAnalyzerResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public String ListFindings(IAMAccessAnalyzerClient client, String analyzerId) {
        if (analyzerId == null || analyzerId.isEmpty()) {
            return "";
        }
        ListFindingsRequest listFindingsRequest = new ListFindingsRequest();
        listFindingsRequest.withAnalyzerId(analyzerId);
        // 构建filter的过滤键
        List<String> keyList = new ArrayList<>();
        keyList.add("active");
        // 构建filters参数
        FindingFilter findingFilter = new FindingFilter()
            .withKey(FindingFilter.KeyEnum.STATUS)
            .withCriterion(new Criterion().withEq(keyList));
        List<FindingFilter> findingFilterList = new ArrayList<>();
        findingFilterList.add(findingFilter);
        // 构建request的body
        listFindingsRequest.withBody(new ListFindingsReqBody().withLimit(100).withFilters(findingFilterList));
        String findingId = "";
        try {
            ListFindingsResponse listFindingsResponse = client.listFindings(listFindingsRequest);
            List<Finding> findings = listFindingsResponse.getFindings();
            if (!findings.isEmpty()) {
                findingId = findings.get(0).getId();
            }
            logger.info(listFindingsResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
        return findingId;
    }

    public void ShowFinding(IAMAccessAnalyzerClient client, String analyzerId, String findingId) {
        if (analyzerId == null || analyzerId.isEmpty() || findingId == null || findingId.isEmpty()) {
            return;
        }
        ShowFindingRequest showFindingRequest = new ShowFindingRequest().withAnalyzerId(analyzerId)
            .withFindingId(findingId);
        try {
            ShowFindingResponse showFindingResponse = client.showFinding(showFindingRequest);
            logger.info(showFindingResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void DeleteAnalyzer(IAMAccessAnalyzerClient client, String analyzerId) {
        if (analyzerId == null || analyzerId.isEmpty()) {
            return;
        }
        DeleteAnalyzerRequest deleteAnalyzerRequest = new DeleteAnalyzerRequest().withAnalyzerId(analyzerId);
        try {
            DeleteAnalyzerResponse deleteAnalyzerResponse = client.deleteAnalyzer(deleteAnalyzerRequest);
            logger.info(deleteAnalyzerResponse.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        logger.error("RequestId: " + e.getRequestId());
        logger.error("ErrorCode: " + e.getErrorCode());
        logger.error("ErrorMsg: " + e.getErrorMsg());
    }
}
