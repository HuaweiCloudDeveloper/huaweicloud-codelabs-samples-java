package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.utils.StringUtils;
import com.huaweicloud.sdk.iam.v3.IamClient;
import com.huaweicloud.sdk.iam.v3.model.AgencyTokenAssumerole;
import com.huaweicloud.sdk.iam.v3.model.AgencyTokenAuth;
import com.huaweicloud.sdk.iam.v3.model.AgencyTokenIdentity;
import com.huaweicloud.sdk.iam.v3.model.AgencyTokenScope;
import com.huaweicloud.sdk.iam.v3.model.AgencyTokenScopeDomain;
import com.huaweicloud.sdk.iam.v3.model.AgencyTokenScopeProject;
import com.huaweicloud.sdk.iam.v3.model.AuthScope;
import com.huaweicloud.sdk.iam.v3.model.AuthScopeDomain;
import com.huaweicloud.sdk.iam.v3.model.AuthScopeProject;
import com.huaweicloud.sdk.iam.v3.model.KeystoneCreateAgencyTokenRequest;
import com.huaweicloud.sdk.iam.v3.model.KeystoneCreateAgencyTokenRequestBody;
import com.huaweicloud.sdk.iam.v3.model.KeystoneCreateAgencyTokenResponse;
import com.huaweicloud.sdk.iam.v3.model.KeystoneCreateScopedTokenRequest;
import com.huaweicloud.sdk.iam.v3.model.KeystoneCreateScopedTokenRequestBody;
import com.huaweicloud.sdk.iam.v3.model.KeystoneCreateScopedTokenResponse;
import com.huaweicloud.sdk.iam.v3.model.KeystoneCreateUserTokenByPasswordAndMfaRequest;
import com.huaweicloud.sdk.iam.v3.model.KeystoneCreateUserTokenByPasswordAndMfaRequestBody;
import com.huaweicloud.sdk.iam.v3.model.KeystoneCreateUserTokenByPasswordAndMfaResponse;
import com.huaweicloud.sdk.iam.v3.model.KeystoneCreateUserTokenByPasswordRequest;
import com.huaweicloud.sdk.iam.v3.model.KeystoneCreateUserTokenByPasswordRequestBody;
import com.huaweicloud.sdk.iam.v3.model.KeystoneCreateUserTokenByPasswordResponse;
import com.huaweicloud.sdk.iam.v3.model.MfaAuth;
import com.huaweicloud.sdk.iam.v3.model.MfaIdentity;
import com.huaweicloud.sdk.iam.v3.model.MfaTotp;
import com.huaweicloud.sdk.iam.v3.model.MfaTotpUser;
import com.huaweicloud.sdk.iam.v3.model.PwdAuth;
import com.huaweicloud.sdk.iam.v3.model.PwdIdentity;
import com.huaweicloud.sdk.iam.v3.model.PwdPassword;
import com.huaweicloud.sdk.iam.v3.model.PwdPasswordUser;
import com.huaweicloud.sdk.iam.v3.model.PwdPasswordUserDomain;
import com.huaweicloud.sdk.iam.v3.model.ScopeDomainOption;
import com.huaweicloud.sdk.iam.v3.model.ScopeProjectOption;
import com.huaweicloud.sdk.iam.v3.model.ScopedToken;
import com.huaweicloud.sdk.iam.v3.model.ScopedTokenAuth;
import com.huaweicloud.sdk.iam.v3.model.ScopedTokenIdentity;
import com.huaweicloud.sdk.iam.v3.model.TokenSocpeOption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class AuthTokens {
    private static final Logger logger = LoggerFactory.getLogger(AuthTokens.class);

    public static void main(String[] args) {
        // Hard-coded or plaintext AK and SK are insecure. So, encrypt your AK and SK and store them in the configuration file or environment variables.
        // In this example, the AK and SK are stored in environment variables. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String domainId = ""; // (Mandatory) Account ID.

        // Configure client attributes.
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // Create a credential.
        GlobalCredentials auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk)
            .withDomainId(domainId);

        // Create a request client.
        ArrayList<String> endpoints = new ArrayList<String>();
        endpoints.add(""); // Fixed value. Enter https://iam.myhuaweicloud.com.
        IamClient client = IamClient.newBuilder()
            .withCredential(auth)
            .withEndpoints(endpoints)
            .withHttpConfig(config).build();

        // Obtain an IAM user token (using the password).
        createTokenByPassword(client);

        // Obtain an IAM user token (using the password and virtual MFA device).
        createUserTokenByPasswordAndMfa(client);

        // Obtain an agency token.
        createAgencyToken(client);

        // Obtain a federation scoped token.
        createScopedToken(client);

    }

    private static void createTokenByPassword(IamClient client) {
        logger.info("####createTokenByPassword start!####");
        String userName = ""; // (Mandatory) Username.
        String userPwd = System.getenv("HUAWEICLOUD_SDK_USER_PWD"); // (Mandatory) Password. Set the local environment variable HUAWEICLOUD_SDK_USER_PWD first.
        String domainName = ""; // (Mandatory) Account name.

        KeystoneCreateUserTokenByPasswordRequest request = new KeystoneCreateUserTokenByPasswordRequest();
        KeystoneCreateUserTokenByPasswordRequestBody body = new KeystoneCreateUserTokenByPasswordRequestBody();

        // Set the scope using the setScope() function.
        AuthScope authScope = setScope();

        PwdPasswordUserDomain domainUser = new PwdPasswordUserDomain();
        domainUser.withName(domainName);
        PwdPasswordUser userPassword = new PwdPasswordUser();
        userPassword.withDomain(domainUser)
            .withName(userName)
            .withPassword(userPwd);
        PwdPassword passwordIdentity = new PwdPassword();
        passwordIdentity.withUser(userPassword);
        List<PwdIdentity.MethodsEnum> listIdentityMethods = new ArrayList<PwdIdentity.MethodsEnum>();
        listIdentityMethods.add(PwdIdentity.MethodsEnum.fromValue("password"));
        PwdIdentity identityAuth = new PwdIdentity();
        identityAuth.withMethods(listIdentityMethods)
            .withPassword(passwordIdentity);
        PwdAuth authbody = new PwdAuth();
        authbody.withIdentity(identityAuth)
            .withScope(authScope);
        body.withAuth(authbody);
        request.withBody(body);
        try {
            KeystoneCreateUserTokenByPasswordResponse response = client.keystoneCreateUserTokenByPassword(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void createUserTokenByPasswordAndMfa(IamClient client) {
        logger.info("####createUserTokenByPasswordAndMfa start!####");
        String userName = ""; // (Mandatory) Username.
        String userPwd = System.getenv("HUAWEICLOUD_SDK_USER_PWD"); // (Mandatory) Password. Set the local environment variable HUAWEICLOUD_SDK_USER_PWD first.
        String domainName = ""; // (Mandatory) Account name.
        String userId = ""; // (Mandatory) ID of the IAM user for whom MFA authentication was enabled.
        String passcode = ""; // (Mandatory) Virtual MFA verification code.

        KeystoneCreateUserTokenByPasswordAndMfaRequest request = new KeystoneCreateUserTokenByPasswordAndMfaRequest();
        KeystoneCreateUserTokenByPasswordAndMfaRequestBody body
            = new KeystoneCreateUserTokenByPasswordAndMfaRequestBody();

        // Set the scope.
        AuthScope authScope = setScope();

        MfaTotpUser userTotp = new MfaTotpUser();
        userTotp.withId(userId)
            .withPasscode(passcode);
        MfaTotp totpIdentity = new MfaTotp();
        totpIdentity.withUser(userTotp);
        PwdPasswordUserDomain domainUser = new PwdPasswordUserDomain();
        domainUser.withName(domainName);
        PwdPasswordUser userPassword = new PwdPasswordUser();
        userPassword.withDomain(domainUser)
            .withName(userName)
            .withPassword(userPwd);
        PwdPassword passwordIdentity = new PwdPassword();
        passwordIdentity.withUser(userPassword);
        List<MfaIdentity.MethodsEnum> listIdentityMethods = new ArrayList<MfaIdentity.MethodsEnum>();
        listIdentityMethods.add(MfaIdentity.MethodsEnum.fromValue("password"));
        listIdentityMethods.add(MfaIdentity.MethodsEnum.fromValue("totp"));
        MfaIdentity identityAuth = new MfaIdentity();
        identityAuth.withMethods(listIdentityMethods)
            .withPassword(passwordIdentity)
            .withTotp(totpIdentity);
        MfaAuth authbody = new MfaAuth();
        authbody.withIdentity(identityAuth)
            .withScope(authScope);
        body.withAuth(authbody);
        request.withBody(body);
        try {
            KeystoneCreateUserTokenByPasswordAndMfaResponse response = client.keystoneCreateUserTokenByPasswordAndMfa(
                request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void createAgencyToken(IamClient client) {
        logger.info("####createAgencyToken start!####");
        // Use either domainId or domainName.
        String domainId = ""; // (Mandatory) Account ID of the delegating party.
        String domainName = ""; // (Mandatory) Account name of the delegating party.
        String agencyName = ""; // (Mandatory) Name of the agency created by the delegating party.

        KeystoneCreateAgencyTokenRequest request = new KeystoneCreateAgencyTokenRequest();
        KeystoneCreateAgencyTokenRequestBody body = new KeystoneCreateAgencyTokenRequestBody();

        // Set the scope using the setScopeForAgencyToken() function.
        AgencyTokenScope agencyTokenScope = setScopeForAgencyToken();

        // Set assume_role.
        AgencyTokenAssumerole assumeRoleIdentity = new AgencyTokenAssumerole();
        if (!StringUtils.isEmpty(domainId)) {
            assumeRoleIdentity.withDomainId(domainId).withAgencyName(agencyName);
        } else if (!StringUtils.isEmpty(domainName)) {
            assumeRoleIdentity.withDomainName(domainName).withAgencyName(agencyName);
        } else {
            logger.error("Need domainId or domainName!");
        }

        List<AgencyTokenIdentity.MethodsEnum> listIdentityMethods = new ArrayList<AgencyTokenIdentity.MethodsEnum>();
        listIdentityMethods.add(AgencyTokenIdentity.MethodsEnum.fromValue("assume_role"));
        AgencyTokenIdentity identityAuth = new AgencyTokenIdentity();
        identityAuth.withMethods(listIdentityMethods)
            .withAssumeRole(assumeRoleIdentity);
        AgencyTokenAuth authbody = new AgencyTokenAuth();
        authbody.withIdentity(identityAuth)
            .withScope(agencyTokenScope);
        body.withAuth(authbody);
        request.withBody(body);
        try {
            KeystoneCreateAgencyTokenResponse response = client.keystoneCreateAgencyToken(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void createScopedToken(IamClient client) {
        logger.info("####createScopedToken start!####");
        String tokenId = ""; // (Mandatory) Federation unscoped token.

        KeystoneCreateScopedTokenRequest request = new KeystoneCreateScopedTokenRequest();
        KeystoneCreateScopedTokenRequestBody body = new KeystoneCreateScopedTokenRequestBody();

        // Set the scope using the setScopeForToken() function.
        TokenSocpeOption tokenSocpeOption = setScopeForToken();

        // Set assume_role.
        ScopedToken tokenIdentity = new ScopedToken();
        tokenIdentity.withId(tokenId);
        List<String> listIdentityMethods = new ArrayList<String>();
        listIdentityMethods.add("token");
        ScopedTokenIdentity identityAuth = new ScopedTokenIdentity();
        identityAuth.withMethods(listIdentityMethods)
            .withToken(tokenIdentity);
        ScopedTokenAuth authbody = new ScopedTokenAuth();
        authbody.withIdentity(identityAuth)
            .withScope(tokenSocpeOption);
        body.withAuth(authbody);
        request.withBody(body);
        try {
            KeystoneCreateScopedTokenResponse response = client.keystoneCreateScopedToken(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static AuthScope setScope() {
        // For the domain scope, use either scopeDomainId or scopeDomainName.
        String scopeDomainId = ""; // Account ID.
        String scopeDomainName = ""; // Account name.
        // For the project scope, use either scopeProjectId or scopeProjectName.
        String scopeProjectId = ""; // Project ID.
        String scopeProjectName = ""; // Project name.

        // Set the scope.
        AuthScopeProject authScopeProject = new AuthScopeProject();
        AuthScopeDomain authScopeDomain = new AuthScopeDomain();
        AuthScope authScope = new AuthScope();
        if (!StringUtils.isEmpty(scopeDomainId)) {
            authScopeDomain.withId(scopeDomainId);
            authScope.withDomain(authScopeDomain);
        } else if (!StringUtils.isEmpty(scopeDomainName)) {
            authScopeDomain.withName(scopeDomainName);
            authScope.withDomain(authScopeDomain);
        } else if (!StringUtils.isEmpty(scopeProjectId)) {
            authScopeProject.withId(scopeProjectId);
            authScope.withProject(authScopeProject);
        } else if (!StringUtils.isEmpty(scopeProjectName)) {
            authScopeProject.withName(scopeProjectName);
            authScope.withProject(authScopeProject);
        }
        return authScope;
    }

    private static AgencyTokenScope setScopeForAgencyToken() {

        // For the domain scope, use either scopeDomainId or scopeDomainName.
        String scopeDomainId = ""; // Account ID.
        String scopeDomainName = ""; // Account name.
        // For the project scope, use either scopeProjectId or scopeProjectName.
        String scopeProjectId = ""; // Project ID.
        String scopeProjectName = ""; // Project name.

        // Set the scope.
        AgencyTokenScopeProject agencyTokenScopeProject = new AgencyTokenScopeProject();
        AgencyTokenScopeDomain agencyTokenScopeDomain = new AgencyTokenScopeDomain();
        AgencyTokenScope agencyTokenScope = new AgencyTokenScope();
        if (!StringUtils.isEmpty(scopeDomainId)) {
            agencyTokenScopeDomain.withId(scopeDomainId);
            agencyTokenScope.withDomain(agencyTokenScopeDomain);
        } else if (!StringUtils.isEmpty(scopeDomainName)) {
            agencyTokenScopeDomain.withName(scopeDomainName);
            agencyTokenScope.withDomain(agencyTokenScopeDomain);
        } else if (!StringUtils.isEmpty(scopeProjectId)) {
            agencyTokenScopeProject.withId(scopeProjectId);
            agencyTokenScope.withProject(agencyTokenScopeProject);
        } else if (!StringUtils.isEmpty(scopeProjectName)) {
            agencyTokenScopeProject.withName(scopeProjectName);
            agencyTokenScope.withProject(agencyTokenScopeProject);
        }
        return agencyTokenScope;
    }

    private static TokenSocpeOption setScopeForToken() {
        // For the domain scope, use either scopeDomainId or scopeDomainName.
        String scopeDomainId = ""; // Account ID.
        String scopeDomainName = ""; // Account name.
        // For the project scope, use scopeProjectId.
        String scopeProjectId = ""; // Project ID.

        // Set the scope.
        ScopeProjectOption scopeProjectOption = new ScopeProjectOption();
        ScopeDomainOption scopeDomainOption = new ScopeDomainOption();
        TokenSocpeOption tokenSocpeOption = new TokenSocpeOption();
        if (!StringUtils.isEmpty(scopeDomainId)) {
            scopeDomainOption.withId(scopeDomainId);
            tokenSocpeOption.withDomain(scopeDomainOption);
        } else if (!StringUtils.isEmpty(scopeDomainName)) {
            scopeDomainOption.withName(scopeDomainName);
            tokenSocpeOption.withDomain(scopeDomainOption);
        } else if (!StringUtils.isEmpty(scopeProjectId)) {
            scopeProjectOption.withId(scopeProjectId);
            tokenSocpeOption.withProject(scopeProjectOption);
        }
        return tokenSocpeOption;
    }

    private static void LogError(ClientRequestException e) {
        logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        logger.error("RequestId: " + e.getRequestId());
        logger.error("ErrorCode: " + e.getErrorCode());
        logger.error("ErrorMsg: " + e.getErrorMsg());
    }
}
