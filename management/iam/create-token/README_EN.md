## 1. Introduction
A token is an access credential issued to an IAM user. It carries their identity and permissions. When calling the APIs of IAM or other cloud services, you can use the following methods to obtain a user token for authentication.  
A token can be used at the domain or project level.  
This sample describes how to:
* Obtain an IAM user token (using the username and password for authentication).
* Obtain an IAM user token (using the username, password, and virtual MFA device).
* Obtain an agency token (for example, the delegated party can obtain the agency token to access resources of the delegating party).
* Obtain a federation scoped token.

## 2. Preparations
### 2.1 Obtaining Required Parameters   
Obtain the required parameters of the account, including **ak** and **sk** (access keys), **userName** (user name), **userPwd** (password), **domainName** (account name), and **agencyName** (agency name). To view or create an access key (AK/SK), sign in to Huawei Cloud console and choose **My Credentials** > **Access Keys**. For more information, visit [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).   
* Parameters required for obtaining an IAM user token (using the password): **userName** (username), **userPwd** (password), and **domainName** (account name)    
* Parameters required for obtaining an IAM user token (using the password and virtual MFA device): **userName** (username), **userPwd** (password), **domainName** (account name), **userId** (ID of the IAM user for whom MFA authentication was enabled), and **passcode** (virtual MFA authentication code)     
* Parameters required for obtaining an agency token: **domainId** (account ID of the delegating party), **domainName** (account name of the delegating party), and **agencyName** (name of the agency created by the delegating party)     
* Parameters required for obtaining a federation scoped token: **tokenId** (unscoped token ID)     
* Common parameters: **endpoint** (In this example, the endpoint is "https://iam.myhuaweicloud.com"), **scopeDomainId** (account ID), **scopeDomainName** (account name), **scopeProjectId** (project ID), and **scopeProjectName** (project name)

### 2.2 Obtaining and Installing the SDK  
You are advised to use Maven to obtain and install the SDK.
Download and install Maven. Then, add the IAM SDK to the **pom.xml** file in the Java project.
```xml
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-iam</artifactId>
        <version>3.1.28</version>
    </dependency>
```
### 2.3 General Preparations  
#### 2.3.1 Importing Dependent Modules  
```java
    import com.huaweicloud.sdk.core.auth.GlobalCredentials;
    import com.huaweicloud.sdk.core.exception.ClientRequestException;
    import com.huaweicloud.sdk.core.http.HttpConfig;
    import com.huaweicloud.sdk.core.utils.StringUtils;
```
#### 2.3.2 Configuring Client Attributes  
```
    HttpConfig config = HttpConfig.getDefaultHttpConfig();
    config.withIgnoreSSLVerification(true);
    // For local IDEA debugging, a proxy is required. The code is as follows:
    // config.setProxyHost("");
    // config.setProxyPort();
    // config.setProxyUsername("");
    // config.setProxyPassword("");
```
#### 2.3.3 Creating a Credential  
```
     GlobalCredentials auth = new GlobalCredentials()
        .withAk(ak)
        .withSk(sk)
        .withDomainId(domainId);
```
#### 2.3.4 Creating a Request Client  
```
    ArrayList<String> endpoints = new ArrayList<String>();
    endpoints.add("https://iam.myhuaweicloud.com"); 
    IamClient client = IamClient.newBuilder()
        .withCredential(auth)
        .withEndpoints(endpoints)
        .withHttpConfig(config).build();
```
## 3. Runtime Environment
Java JDK 1.8 or later.

## 4. Key Code 
```
    public class AuthTokens {
        private static final Logger logger = LoggerFactory.getLogger(AuthTokens.class);
    
        public static void main(String[] args) {
            // Hard-coded or plaintext AK and SK are insecure. So, encrypt your AK and SK and store them in the configuration file or environment variables.
            // In this example, the AK and SK are stored in environment variables. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
            String ak = System.getenv("HUAWEICLOUD_SDK_AK");
            String sk = System.getenv("HUAWEICLOUD_SDK_SK");
            String domainId = ""; // (Mandatory) Account ID.
    
            // Configure client attributes.
            HttpConfig config = HttpConfig.getDefaultHttpConfig();
            config.withIgnoreSSLVerification(true);
    
            // Create a credential.
            GlobalCredentials auth = new GlobalCredentials()
                .withAk(ak)
                .withSk(sk)
                .withDomainId(domainId);
    
            // Create a request client.
            ArrayList<String> endpoints = new ArrayList<String>();
            endpoints.add(""); // Fixed value. Enter https://iam.myhuaweicloud.com.
            IamClient client = IamClient.newBuilder()
                .withCredential(auth)
                .withEndpoints(endpoints)
                .withHttpConfig(config).build();
    
            // Obtain an IAM user token (using the password).
            createTokenByPassword(client);
    
            // Obtain an IAM user token (using the password and virtual MFA device).
            createUserTokenByPasswordAndMfa(client);
    
            // Obtain an agency token.
            createAgencyToken(client);
    
            // Obtain a federation scoped token.
            createScopedToken(client);
    
        }
    
        private static void createTokenByPassword(IamClient client) {
            logger.info("####createTokenByPassword start!####");
            String userName = ""; // (Mandatory) Username.
            String userPwd = System.getenv("HUAWEICLOUD_SDK_USER_PWD"); // (Mandatory) Password. Set the local environment variable HUAWEICLOUD_SDK_USER_PWD first.
            String domainName = ""; // (Mandatory) Account name.
    
            KeystoneCreateUserTokenByPasswordRequest request = new KeystoneCreateUserTokenByPasswordRequest();
            KeystoneCreateUserTokenByPasswordRequestBody body = new KeystoneCreateUserTokenByPasswordRequestBody();
    
            // Set the scope using the setScope() function.。
            AuthScope authScope = setScope();
    
            PwdPasswordUserDomain domainUser = new PwdPasswordUserDomain();
            domainUser.withName(domainName);
            PwdPasswordUser userPassword = new PwdPasswordUser();
            userPassword.withDomain(domainUser)
                .withName(userName)
                .withPassword(userPwd);
            PwdPassword passwordIdentity = new PwdPassword();
            passwordIdentity.withUser(userPassword);
            List<PwdIdentity.MethodsEnum> listIdentityMethods = new ArrayList<PwdIdentity.MethodsEnum>();
            listIdentityMethods.add(PwdIdentity.MethodsEnum.fromValue("password"));
            PwdIdentity identityAuth = new PwdIdentity();
            identityAuth.withMethods(listIdentityMethods)
                .withPassword(passwordIdentity);
            PwdAuth authbody = new PwdAuth();
            authbody.withIdentity(identityAuth)
                .withScope(authScope);
            body.withAuth(authbody);
            request.withBody(body);
            try {
                KeystoneCreateUserTokenByPasswordResponse response = client.keystoneCreateUserTokenByPassword(request);
                logger.info(response.toString());
            } catch (ClientRequestException e) {
                LogError(e);
            }
        }
    
        private static void createUserTokenByPasswordAndMfa(IamClient client) {
            logger.info("####createUserTokenByPasswordAndMfa start!####");
            String userName = ""; // (Mandatory) Username.
            String userPwd = System.getenv("HUAWEICLOUD_SDK_USER_PWD"); // (Mandatory) Password. Set the local environment variable HUAWEICLOUD_SDK_USER_PWD first.
            String domainName = ""; // (Mandatory) Account name.
            String userId = ""; // (Mandatory) ID of the IAM user for whom MFA authentication was enabled.
            String passcode = ""; // (Mandatory) Virtual MFA verification code.
    
            KeystoneCreateUserTokenByPasswordAndMfaRequest request = new KeystoneCreateUserTokenByPasswordAndMfaRequest();
            KeystoneCreateUserTokenByPasswordAndMfaRequestBody body
                = new KeystoneCreateUserTokenByPasswordAndMfaRequestBody();
    
            // Set the scope.
            AuthScope authScope = setScope();
    
            MfaTotpUser userTotp = new MfaTotpUser();
            userTotp.withId(userId)
                .withPasscode(passcode);
            MfaTotp totpIdentity = new MfaTotp();
            totpIdentity.withUser(userTotp);
            PwdPasswordUserDomain domainUser = new PwdPasswordUserDomain();
            domainUser.withName(domainName);
            PwdPasswordUser userPassword = new PwdPasswordUser();
            userPassword.withDomain(domainUser)
                .withName(userName)
                .withPassword(userPwd);
            PwdPassword passwordIdentity = new PwdPassword();
            passwordIdentity.withUser(userPassword);
            List<MfaIdentity.MethodsEnum> listIdentityMethods = new ArrayList<MfaIdentity.MethodsEnum>();
            listIdentityMethods.add(MfaIdentity.MethodsEnum.fromValue("password"));
            listIdentityMethods.add(MfaIdentity.MethodsEnum.fromValue("totp"));
            MfaIdentity identityAuth = new MfaIdentity();
            identityAuth.withMethods(listIdentityMethods)
                .withPassword(passwordIdentity)
                .withTotp(totpIdentity);
            MfaAuth authbody = new MfaAuth();
            authbody.withIdentity(identityAuth)
                .withScope(authScope);
            body.withAuth(authbody);
            request.withBody(body);
            try {
                KeystoneCreateUserTokenByPasswordAndMfaResponse response = client.keystoneCreateUserTokenByPasswordAndMfa(
                    request);
                logger.info(response.toString());
            } catch (ClientRequestException e) {
                LogError(e);
            }
        }
    
        private static void createAgencyToken(IamClient client) {
            logger.info("####createAgencyToken start!####");
            // Use either domainId or domainName.
            String domainId = ""; // (Mandatory) Account ID of the delegating party.
            String domainName = ""; // (Mandatory) Account name of the delegating party.
            String agencyName = ""; // (Mandatory) Name of the agency created by the delegating party.
    
            KeystoneCreateAgencyTokenRequest request = new KeystoneCreateAgencyTokenRequest();
            KeystoneCreateAgencyTokenRequestBody body = new KeystoneCreateAgencyTokenRequestBody();
    
            // Set the scope using the setScopeForAgencyToken() function.
            AgencyTokenScope agencyTokenScope = setScopeForAgencyToken();
    
            // Set assume_role.
            AgencyTokenAssumerole assumeRoleIdentity = new AgencyTokenAssumerole();
            if (!StringUtils.isEmpty(domainId)) {
                assumeRoleIdentity.withDomainId(domainId).withAgencyName(agencyName);
            } else if (!StringUtils.isEmpty(domainName)) {
                assumeRoleIdentity.withDomainName(domainName).withAgencyName(agencyName);
            } else {
                logger.error("Need domainId or domainName!");
            }
    
            List<AgencyTokenIdentity.MethodsEnum> listIdentityMethods = new ArrayList<AgencyTokenIdentity.MethodsEnum>();
            listIdentityMethods.add(AgencyTokenIdentity.MethodsEnum.fromValue("assume_role"));
            AgencyTokenIdentity identityAuth = new AgencyTokenIdentity();
            identityAuth.withMethods(listIdentityMethods)
                .withAssumeRole(assumeRoleIdentity);
            AgencyTokenAuth authbody = new AgencyTokenAuth();
            authbody.withIdentity(identityAuth)
                .withScope(agencyTokenScope);
            body.withAuth(authbody);
            request.withBody(body);
            try {
                KeystoneCreateAgencyTokenResponse response = client.keystoneCreateAgencyToken(request);
                logger.info(response.toString());
            } catch (ClientRequestException e) {
                LogError(e);
            }
        }
    
        private static void createScopedToken(IamClient client) {
            logger.info("####createScopedToken start!####");
            String tokenId = ""; // (Mandatory) Federation unscoped token.
    
            KeystoneCreateScopedTokenRequest request = new KeystoneCreateScopedTokenRequest();
            KeystoneCreateScopedTokenRequestBody body = new KeystoneCreateScopedTokenRequestBody();
    
            // Set the scope using the setScopeForToken() function.
            TokenSocpeOption tokenSocpeOption = setScopeForToken();
    
            // Set assume_role.
            ScopedToken tokenIdentity = new ScopedToken();
            tokenIdentity.withId(tokenId);
            List<String> listIdentityMethods = new ArrayList<String>();
            listIdentityMethods.add("token");
            ScopedTokenIdentity identityAuth = new ScopedTokenIdentity();
            identityAuth.withMethods(listIdentityMethods)
                .withToken(tokenIdentity);
            ScopedTokenAuth authbody = new ScopedTokenAuth();
            authbody.withIdentity(identityAuth)
                .withScope(tokenSocpeOption);
            body.withAuth(authbody);
            request.withBody(body);
            try {
                KeystoneCreateScopedTokenResponse response = client.keystoneCreateScopedToken(request);
                logger.info(response.toString());
            } catch (ClientRequestException e) {
                LogError(e);
            }
        }
    
        private static AuthScope setScope() {
            // For the domain scope, use either scopeDomainId or scopeDomainName.
            String scopeDomainId = ""; // Account ID.
            String scopeDomainName = ""; // Account name.
            // For the project scope, use either scopeProjectId or scopeProjectName.
            String scopeProjectId = ""; // Project ID.
            String scopeProjectName = ""; // Project name.
    
            // Set the scope.
            AuthScopeProject authScopeProject = new AuthScopeProject();
            AuthScopeDomain authScopeDomain = new AuthScopeDomain();
            AuthScope authScope = new AuthScope();
            if (!StringUtils.isEmpty(scopeDomainId)) {
                authScopeDomain.withId(scopeDomainId);
                authScope.withDomain(authScopeDomain);
            } else if (!StringUtils.isEmpty(scopeDomainName)) {
                authScopeDomain.withName(scopeDomainName);
                authScope.withDomain(authScopeDomain);
            } else if (!StringUtils.isEmpty(scopeProjectId)) {
                authScopeProject.withId(scopeProjectId);
                authScope.withProject(authScopeProject);
            } else if (!StringUtils.isEmpty(scopeProjectName)) {
                authScopeProject.withName(scopeProjectName);
                authScope.withProject(authScopeProject);
            }
            return authScope;
        }
    
        private static AgencyTokenScope setScopeForAgencyToken() {
    
            // For the domain scope, use either scopeDomainId or scopeDomainName.
            String scopeDomainId = ""; // Account ID.
            String scopeDomainName = ""; // Account name.
            // For the project scope, use either scopeProjectId or scopeProjectName.
            String scopeProjectId = ""; // Project ID.
            String scopeProjectName = ""; // Project name.
    
            // Set the scope.
            AgencyTokenScopeProject agencyTokenScopeProject = new AgencyTokenScopeProject();
            AgencyTokenScopeDomain agencyTokenScopeDomain = new AgencyTokenScopeDomain();
            AgencyTokenScope agencyTokenScope = new AgencyTokenScope();
            if (!StringUtils.isEmpty(scopeDomainId)) {
                agencyTokenScopeDomain.withId(scopeDomainId);
                agencyTokenScope.withDomain(agencyTokenScopeDomain);
            } else if (!StringUtils.isEmpty(scopeDomainName)) {
                agencyTokenScopeDomain.withName(scopeDomainName);
                agencyTokenScope.withDomain(agencyTokenScopeDomain);
            } else if (!StringUtils.isEmpty(scopeProjectId)) {
                agencyTokenScopeProject.withId(scopeProjectId);
                agencyTokenScope.withProject(agencyTokenScopeProject);
            } else if (!StringUtils.isEmpty(scopeProjectName)) {
                agencyTokenScopeProject.withName(scopeProjectName);
                agencyTokenScope.withProject(agencyTokenScopeProject);
            }
            return agencyTokenScope;
        }
    
        private static TokenSocpeOption setScopeForToken() {
            // For the domain scope, use either scopeDomainId or scopeDomainName.
            String scopeDomainId = ""; // Account ID.
            String scopeDomainName = ""; // Account name.
            // For the project scope, use scopeProjectId.
            String scopeProjectId = ""; // Project ID.
    
            // Set the scope
            ScopeProjectOption scopeProjectOption = new ScopeProjectOption();
            ScopeDomainOption scopeDomainOption = new ScopeDomainOption();
            TokenSocpeOption tokenSocpeOption = new TokenSocpeOption();
            if (!StringUtils.isEmpty(scopeDomainId)) {
                scopeDomainOption.withId(scopeDomainId);
                tokenSocpeOption.withDomain(scopeDomainOption);
            } else if (!StringUtils.isEmpty(scopeDomainName)) {
                scopeDomainOption.withName(scopeDomainName);
                tokenSocpeOption.withDomain(scopeDomainOption);
            } else if (!StringUtils.isEmpty(scopeProjectId)) {
                scopeProjectOption.withId(scopeProjectId);
                tokenSocpeOption.withProject(scopeProjectOption);
            }
            return tokenSocpeOption;
        }
    
        private static void LogError(ClientRequestException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("RequestId: " + e.getRequestId());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }
```
## 5. Application Scenario
This sample shows how to obtain an IAM user token (using the password), an IAM user token (using the password and virtual MFA device), an agency token, and a federation scoped token.
Each method can be used independently. When you use a method, comment out parameters of the other methods and run the main function.

## 6. Example Result
### 6.1 Result for Obtaining an IAM User Token (Using the Password)
```
    {
        token: class TokenResult {
            catalog: null
            domain: class TokenDomainResult {
                name: h********4
                id: 09b9***************1200
            }
            expiresAt: 2023-07-25T08:50:05.445000Z
            issuedAt: 2023-07-24T08:50:05.445000Z
            methods: [password]
            project: null
            roles: [class TokenRole {
                name: te_admin
                id: 0
            }, class TokenRole {
                name: secu_admin
                id: 0
            }, class TokenRole {
                name: te_agency
                id: 0
            }]
            user: class TokenUserResult {
                name: l*****i
                id: 673c***************b05f
                passwordExpiresAt:
                    domain: class TokenUserDomainResult {
                        name: h********4
                        id: 09b9***************1200
                    }
            }
        }
        xSubjectToken: MIIU………………
    }
```
### 6.2 Result for Obtaining an IAM User Token (Using the Password and Virtual MFA Device)  
```
    {
        token: class TokenResult {
            catalog: null
            domain: class TokenDomainResult {
                name: hid***********ou7
                id: 2b29*******************964e
            }
            expiresAt: 2023-07-26T08:08:04.058000Z
            issuedAt: 2023-07-25T08:08:04.058000Z
            methods: [password, totp]
            project: null
            roles: [class TokenRole {
                name: op**************nce
                id: 0
            }, class TokenRole {
                name: op*************inum
                id: 0
            }]
            user: class TokenUserResult {
                name: lz******25
                id: 0592**********************d651
                passwordExpiresAt: 
                domain: class TokenUserDomainResult {
                    name: hid***********ou7
                    id: 2b29*******************964e
                }
            }
        }
        xSubjectToken: MIIVfwYJKoZIhv……
    }
```
### 6.3 Result for Obtaining an Agency Token  
```
    {
        token: class AgencyTokenResult {
            methods: [assume_role]
            expiresAt: 2023-07-26T08:11:09.698000Z
            issuedAt: 2023-07-25T08:11:09.698000Z
            assumedBy: class AgencyAssumedby {
                user: class AgencyAssumedbyUser {
                    name: hid*********u7
                    id: 81b*********************151
                    domain: class AgencyAssumedbyUserDomain {
                        name: hid***********hou7
                        id: 2b29*******************d964e
                    }
                    passwordExpiresAt: 
                }
            }
            catalog: null
            domain: class AgencyTokenDomain {
                name: hid*********3hou7
                id: 2b29*************************964e
            }
            project: null
            roles: [class TokenRole {
                name: secu_admin
                id: 0
            }, class TokenRole {
                name: op_fine_grained
                id: 7
            }]
            user: class AgencyTokenUser {
                name: hid******3hou7/hid******nn3hou7
                id: 99b46**********************c1991c3
                domain: class AgencyTokenUserDomain {
                    id: 2b295********************2d964e
                    name: hid*************n3hou7
                }
            }
        }
        xSubjectToken: MIIWdQYJKoZIhvcNA……
    }
```
### 6.4 Result for Obtaining a Federation Scoped Token   
```
    {
        token: class ScopeTokenResult {
            methods: [token]
            expiresAt: 2023-07-26T09:33:49.283000Z
            catalog: null
            domain: class TokenDomainResult {
                name: li*****n
                id: 3d63*****************802
            }
            project: null
            roles: [class TokenRole {
                name: op_gated_ecs_spot_instance
                id: 0
            }}]
            user: class ScopedTokenUser {
                domain: class TokenDomainResult {
                    name: li*****n
                    id: 3d6*****************802
                }
                osFederation: class TokenUserOsfederation {
                    groups: []
                    identityProvider: class OsfederationIdentityprovider {
                        id: lzplzp
                    }
                    protocol: class OsfederationProtocol {
                        id: saml
                    }
                }
                id: yaoY************************PzMM
                name: Fed*************ser
                passwordExpiresAt: 
            }
            issuedAt: 2023-07-25T09:33:49.283000Z
        }
        xSubjectToken: MIIVFgYJKoZIhvc...
    }
```
## 7. Other Languages
The sample code takes reference from Huawei Cloud API Explorer Java code and runs depending on SDK. For details about the sample code developed by other languages, see the following:
* [Obtaining an IAM User Token (Using the Password)](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/IAM/doc?api=KeystoneCreateUserTokenByPassword)
* [Obtaining an IAM User Token (Using the Password and Virtual MFA Device)](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/IAM/doc?api=KeystoneCreateUserTokenByPasswordAndMfa)
* [Obtaining an Agency Token](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/IAM/doc?api=KeystoneCreateAgencyToken)
* [Obtaining a Federation Scoped Token](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/IAM/doc?api=KeystoneCreateScopedToken)

## 8. Change History
| Releast Date | Version  | Description  |
|:------------:| :------: | :----------: |
|   2023-08    | 1.0  | This issue is the first official release.  |