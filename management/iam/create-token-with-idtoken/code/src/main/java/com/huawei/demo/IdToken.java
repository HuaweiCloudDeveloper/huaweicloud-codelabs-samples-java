package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.utils.StringUtils;
import com.huaweicloud.sdk.iam.v3.IamClient;
import com.huaweicloud.sdk.iam.v3.model.CreateTokenWithIdTokenRequest;
import com.huaweicloud.sdk.iam.v3.model.CreateTokenWithIdTokenResponse;
import com.huaweicloud.sdk.iam.v3.model.GetIdTokenAuthParams;
import com.huaweicloud.sdk.iam.v3.model.GetIdTokenIdScopeBody;
import com.huaweicloud.sdk.iam.v3.model.GetIdTokenIdTokenBody;
import com.huaweicloud.sdk.iam.v3.model.GetIdTokenRequestBody;
import com.huaweicloud.sdk.iam.v3.model.GetIdTokenScopeDomainOrProjectBody;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class IdToken {
    private static final Logger logger = LoggerFactory.getLogger(IdToken.class);

    public static void main(String[] args) {
        // Hard-coded or plaintext AK and SK are insecure. For security purpose, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored in environment variables. Before running this example, set environment variables, HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String domainId = ""; // (Mandatory) Account ID.

        // Configure client attributes.
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // Create a credential.
        GlobalCredentials auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk)
            .withDomainId(domainId);

        // Create a request client.
        ArrayList<String> endpoints = new ArrayList<String>();
        endpoints.add(""); // Fixed value. Enter https://iam.myhuaweicloud.com.
        IamClient client = IamClient.newBuilder()
            .withCredential(auth)
            .withEndpoints(endpoints)
            .build();

        // Obtain a Token with an OpenID Connect ID Token.
        createTokenWithIdToken(client);
    }

    private static void createTokenWithIdToken(IamClient client) {
        String xIdpId = ""; // (Mandatory) Identity provider ID.
        String idToken = ""; // (Mandatory) ID token.

        // To obtain an unscoped token, do not specify the following four parameters. To obtain a scoped token, specify required parameters as follows:
        String scopeDomainId = ""; // Obtain a federation domain scoped token and specify both domain Id and domain name.
        String scopeDomainName = "";
        String scopeProjectId = ""; // Obtain a federation project scoped token and specify both project Id and project name.
        String scopeProjectName = "";

        // Construct a request.
        CreateTokenWithIdTokenRequest request = new CreateTokenWithIdTokenRequest(); // Request object.
        GetIdTokenAuthParams authbody = new GetIdTokenAuthParams(); // Specigy request body parameters in authbody.

        // Specify the scope.
        GetIdTokenIdScopeBody scopeAuth = new GetIdTokenIdScopeBody(); // Scope object.
        if (!StringUtils.isEmpty(scopeProjectId) && !StringUtils.isEmpty(scopeProjectName)) {
            GetIdTokenScopeDomainOrProjectBody projectScope = new GetIdTokenScopeDomainOrProjectBody();
            projectScope.withId(scopeProjectId).withName(scopeProjectName); // Specify project Id and project name for a project scope.
            scopeAuth.withProject(projectScope);
            authbody.withScope(scopeAuth); // Set the scope.
        } else if (!StringUtils.isEmpty(scopeDomainId) && !StringUtils.isEmpty(scopeDomainName)) {
            GetIdTokenScopeDomainOrProjectBody domainScope = new GetIdTokenScopeDomainOrProjectBody();
            domainScope.withId(scopeDomainId).withName(scopeDomainName);
            scopeAuth.withDomain(domainScope);
            authbody.withScope(scopeAuth); // Set the scope.
        }

        request.withXIdpId(xIdpId); // Set the request header to include X-Idp-Id.
        GetIdTokenIdTokenBody idTokenAuth = new GetIdTokenIdTokenBody();
        idTokenAuth.withId(idToken);
        authbody.withIdToken(idTokenAuth); // Set id_token.
        GetIdTokenRequestBody body = new GetIdTokenRequestBody(); // Request body object.
        body.withAuth(authbody);
        request.withBody(body);

        try {
            // Execute a request.
            CreateTokenWithIdTokenResponse response = client.createTokenWithIdToken(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        logger.error("RequestId: " + e.getRequestId());
        logger.error("ErrorCode: " + e.getErrorCode());
        logger.error("ErrorMsg: " + e.getErrorMsg());
    }
}
