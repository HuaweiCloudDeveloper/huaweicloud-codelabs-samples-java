## 1. Introduction
This sample shows how to obtain a federation token with an OpenID Connect ID token.

## 2. Preparations
### 2.1 Obtaining Required Parameters  
Obtain required information about the account, including the AK, SK, domainId, xIdpId, and idToken. For details, see the sample code.
* domainId：Account ID.
* xIdpId：Identity provider ID.
* idToken：ID token. An ID token is provided by an identity provider and contains identity information of a federated user. For details about how to obtain an ID token, see [Identity Provider](https://support.huaweicloud.com/intl/en-us/usermanual-iam/iam_08_0001.html).

You must obtain the Huawei Cloud account as well as the Access Key ID (AK) and Secret Access Key (SK) of the account.
To view or create an access key (AK/SK), sign in to Huawei Cloud console and choose My Credentials > Access Keys. For more information, see [Access Keys](https://support.huaweicloud.com/intl/en-us/my-kualalumpur-1-usermanual-ca/ca_01_0003.html).

### 2.2 Obtaining and Installing SDK
You are advised to use Maven to obtain and install the SDK.
Download and install Maven. Then, add the IAM SDK to the bpom.xmlb file in the Java project.The IAM SDK is used as an example:
```xml
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-iam</artifactId>
        <version>3.1.28</version>
    </dependency>
```
### 2.3 General Preparations  
#### 2.3.1 Importing Dependent Modules  
```java
    import com.huaweicloud.sdk.core.auth.GlobalCredentials;
    import com.huaweicloud.sdk.core.exception.ClientRequestException;
    import com.huaweicloud.sdk.core.http.HttpConfig;
    import com.huaweicloud.sdk.core.utils.StringUtils;
```
#### 2.3.2 Configuring Client Attributes  
```
    HttpConfig config = HttpConfig.getDefaultHttpConfig();
    config.withIgnoreSSLVerification(true);
    // For local IDEA debugging, a proxy is required. The code is as follows:
    // config.setProxyHost("");
    // config.setProxyPort();
    // config.setProxyUsername("");
    // config.setProxyPassword("");
```
#### 2.3.3 Creating a Credential  
```
     GlobalCredentials auth = new GlobalCredentials()
        .withAk(ak)
        .withSk(sk)
        .withDomainId(domainId);
```
#### 2.3.4 Creating a Request Client  
```
    ArrayList<String> endpoints = new ArrayList<String>();
    endpoints.add("https://iam.myhuaweicloud.com"); 
    IamClient client = IamClient.newBuilder()
        .withCredential(auth)
        .withEndpoints(endpoints)
        .withHttpConfig(config).build();
```
## 3. Runtime Environment
Java JDK 1.8 or later.

## 4. Key Code  
```
    public class IdToken {
        private static final Logger logger = LoggerFactory.getLogger(IdToken.class);
    
        public static void main(String[] args) {
            // Hard-coded or plaintext AK and SK are insecure. For security purpose, encrypt your AK and SK and store them in the configuration file or as environment variables.
            // In this sample, the AK and SK are stored in environment variables. Before running this example, set environment variables, HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
            String ak = System.getenv("HUAWEICLOUD_SDK_AK");
            String sk = System.getenv("HUAWEICLOUD_SDK_SK");
            String domainId = ""; // (Mandatory) Account ID.
    
            // Confiture client attributes.
            HttpConfig config = HttpConfig.getDefaultHttpConfig();
            config.withIgnoreSSLVerification(true);
    
            // Create a credential.
            GlobalCredentials auth = new GlobalCredentials()
                .withAk(ak)
                .withSk(sk)
                .withDomainId(domainId);
    
            // Create a request client.
            ArrayList<String> endpoints = new ArrayList<String>();
            endpoints.add(""); // Fixed value. Enter bhttps://iam.myhuaweicloud.comb.
            IamClient client = IamClient.newBuilder()
                .withCredential(auth)
                .withEndpoints(endpoints)
                .build();
    
            // Obtain a federation token with an OpenID Connect ID token.
            createTokenWithIdToken(client);
        }
    
        private static void createTokenWithIdToken(IamClient client) {
            String xIdpId = ""; // (Mandatory) Identity provider ID.
            String idToken = ""; // (Mandatory) ID token.
    
            // To obtain an unscoped token, do not specify the following four parameters. To obtain a scoped token, specify required parameters as follows:
            String scopeDomainId = ""; // Obtain a federation domain scoped token and specify both domain Id and domain name.
            String scopeDomainName = "";
            String scopeProjectId = ""; // Obtain a federation project scoped token abd specify both project Id and project name.
            String scopeProjectName = "";
    
            // Construct a request.
            CreateTokenWithIdTokenRequest request = new CreateTokenWithIdTokenRequest(); // Request object.
            GetIdTokenAuthParams authbody = new GetIdTokenAuthParams(); // Specify request body parameters in authbody.
    
            // Set the scope.
            GetIdTokenIdScopeBody scopeAuth = new GetIdTokenIdScopeBody(); // Scope object
            if (!StringUtils.isEmpty(scopeProjectId) && !StringUtils.isEmpty(scopeProjectName)) {
                GetIdTokenScopeDomainOrProjectBody projectScope = new GetIdTokenScopeDomainOrProjectBody();
                projectScope.withId(scopeProjectId).withName(scopeProjectName); // Specify project Id and project name for a project scope.
                scopeAuth.withProject(projectScope);
                authbody.withScope(scopeAuth); // Set the scope.
            } else if (!StringUtils.isEmpty(scopeDomainId) && !StringUtils.isEmpty(scopeDomainName)) {
                GetIdTokenScopeDomainOrProjectBody domainScope = new GetIdTokenScopeDomainOrProjectBody();
                domainScope.withId(scopeDomainId).withName(scopeDomainName);
                scopeAuth.withDomain(domainScope);
                authbody.withScope(scopeAuth); // Set the scope.
            }
    
            request.withXIdpId(xIdpId); // Set the request header to X-Idp-Id.
            GetIdTokenIdTokenBody idTokenAuth = new GetIdTokenIdTokenBody();
            idTokenAuth.withId(idToken);
            authbody.withIdToken(idTokenAuth); // Set id_token.
            GetIdTokenRequestBody body = new GetIdTokenRequestBody(); // Request body object.
            body.withAuth(authbody);
            request.withBody(body);
    
            try {
                // Execute a request.
                CreateTokenWithIdTokenResponse response = client.createTokenWithIdToken(request);
                logger.info(response.toString());
            } catch (ClientRequestException e) {
                LogError(e);
            }
        }
    
        private static void LogError(ClientRequestException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("RequestId: " + e.getRequestId());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }
```

## 5. Application Scenario
This sample shows how to obtain a federation token using an OpenID Connect ID token. After specifying required parameters, run the main function.

## 6. Example Result
Result for obtaining a federation token with an OpenID Connect ID token:  
```
    {
        token: class ScopedTokenInfo {
            expiresAt: 2023-07-26T11:04:57.547000Z
            methods: [mapped]
            issuedAt: 2023-07-25T11:04:57.547000Z
            user: class FederationUserBody {
                osFederation: class OsFederationInfo {
                    identityProvider: class IdpIdInfo {
                        id: lzp*****idc
                    }
                    protocol: class ProtocolIdInfo {
                        id: oi**dc
                    }
                    groups: []
                }
                domain: class DomainInfo {
                    id: 3d63*******************9802
                    name: li****xun
                }
                id: yaoYn***********************PzMM
                name: Fed*********er
            }
            domain: null
            project: null
            roles: []
            catalog: []
        }
        xSubjectToken: MIIDegYJKoZIhv...
    }
```
## 7. Other Languages
This sample code takes reference from Huawei Cloud API Explore Java code and runs depending on SDK. For details about the sample code in other languages, see the following:
* [Obtaining a Federated Token with an OpenID Connect ID Token](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/IAM/doc?api=CreateTokenWithIdToken)

## 8. Change History
| Releast Date | Version  | Description  |
|:------------:| :------: | :----------: |
|   2023-08    | 1.0  | This issue is the first official release.  |