## 1.功能介绍
该场景可以用于通过OpenID Connect ID token方式获取联邦认证token。

## 2.前提条件
### 2.1获取AK/SK与其他参数  
开发者在使用前需先获取账号的ak、sk、domainId、xIdpId、idToken等字段（每个字段在demo中有详细文字描述，参见代码）。
* domainId：账号ID。
* xIdpId：身份提供商Id。
* idToken：id_token信息。id_token由企业IdP构建，携带联邦用户身份信息。请参考[企业IdP](https://support.huaweicloud.com/usermanual-iam/iam_08_0001.html)文档了解获取id_token的方法。

您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。
请在华为云控制台“我的凭证-访问密钥”页面上创建和查看您的 AK/SK，更多信息请查看[访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html)。

### 2.2SDK的获取和安装  
您可以通过如下方式获取和安装 SDK：通过 Maven 安装项目依赖是使用 Java SDK 的推荐方法。
首先您需要在您的操作系统中下载并安装 Maven ，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。本示例使用IAM SDK：
```xml
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-iam</artifactId>
        <version>3.1.28</version>
    </dependency>
```
### 2.3通用准备  
#### 2.3.1导入依赖模块部分示例  
```java
    import com.huaweicloud.sdk.core.auth.GlobalCredentials;
    import com.huaweicloud.sdk.core.exception.ClientRequestException;
    import com.huaweicloud.sdk.core.http.HttpConfig;
    import com.huaweicloud.sdk.core.utils.StringUtils;
```
#### 2.3.2配置客户端属性  
```
    HttpConfig config = HttpConfig.getDefaultHttpConfig();
    config.withIgnoreSSLVerification(true);
    // 如果是本地IDEA调试，需要使用代理，代码如下：
    config.setProxyHost("");
    config.setProxyPort();
    config.setProxyUsername("");
    config.setProxyPassword("");
```
#### 2.3.3创建认证  
```
     GlobalCredentials auth = new GlobalCredentials()
        .withAk(ak)
        .withSk(sk)
        .withDomainId(domainId);
```
#### 2.3.4创建请求客户端  
```
    ArrayList<String> endpoints = new ArrayList<String>();
    endpoints.add("https://iam.myhuaweicloud.com"); 
    IamClient client = IamClient.newBuilder()
        .withCredential(auth)
        .withEndpoints(endpoints)
        .withHttpConfig(config).build();
```
## 3.运行环境
Java JDK 1.8 及其以上版本。

## 4.关键代码  
```
    public class IdToken {
        private static final Logger logger = LoggerFactory.getLogger(IdToken.class);
    
        public static void main(String[] args) {
            // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
            // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
            String ak = System.getenv("HUAWEICLOUD_SDK_AK");
            String sk = System.getenv("HUAWEICLOUD_SDK_SK");
            String domainId = ""; // 租户id:domainId,必传
    
            // 配置客户端属性
            HttpConfig config = HttpConfig.getDefaultHttpConfig();
            config.withIgnoreSSLVerification(true);
    
            // 创建认证
            GlobalCredentials auth = new GlobalCredentials()
                .withAk(ak)
                .withSk(sk)
                .withDomainId(domainId);
    
            // 创建请求客户端
            ArrayList<String> endpoints = new ArrayList<String>();
            endpoints.add(""); // 固定值，填入：https://iam.myhuaweicloud.com
            IamClient client = IamClient.newBuilder()
                .withCredential(auth)
                .withEndpoints(endpoints)
                .build();
    
            // 获取联邦认证token(OpenID Connect ID token方式)
            createTokenWithIdToken(client);
        }
    
        private static void createTokenWithIdToken(IamClient client) {
            String xIdpId = ""; // 身份提供商Id。必传
            String idToken = ""; // id_token信息。必传
    
            // 获取unscoped token，下面四个参数都不传，否则根据下面条件传入
            String scopeDomainId = ""; // 获取联邦认证project scoped token，同时填入projectId和projectName
            String scopeDomainName = "";
            String scopeProjectId = ""; // 获取联邦认证domain scoped token，同时填入projectId和projectName
            String scopeProjectName = "";
    
            // 构造请求
            CreateTokenWithIdTokenRequest request = new CreateTokenWithIdTokenRequest(); // 请求对象
            GetIdTokenAuthParams authbody = new GetIdTokenAuthParams(); // authbody用于存放各种请求体参数
    
            // 设置scope信息
            GetIdTokenIdScopeBody scopeAuth = new GetIdTokenIdScopeBody(); // scope对象
            if (!StringUtils.isEmpty(scopeProjectId) && !StringUtils.isEmpty(scopeProjectName)) {
                GetIdTokenScopeDomainOrProjectBody projectScope = new GetIdTokenScopeDomainOrProjectBody();
                projectScope.withId(scopeProjectId).withName(scopeProjectName); // scope为project，设置projectId和projectName
                scopeAuth.withProject(projectScope);
                authbody.withScope(scopeAuth); // 设置scope
            } else if (!StringUtils.isEmpty(scopeDomainId) && !StringUtils.isEmpty(scopeDomainName)) {
                GetIdTokenScopeDomainOrProjectBody domainScope = new GetIdTokenScopeDomainOrProjectBody();
                domainScope.withId(scopeDomainId).withName(scopeDomainName);
                scopeAuth.withDomain(domainScope);
                authbody.withScope(scopeAuth); // 设置scope
            }
    
            request.withXIdpId(xIdpId); // 请求头设置X-Idp-Id
            GetIdTokenIdTokenBody idTokenAuth = new GetIdTokenIdTokenBody();
            idTokenAuth.withId(idToken);
            authbody.withIdToken(idTokenAuth); // 设置id_token
            GetIdTokenRequestBody body = new GetIdTokenRequestBody(); // 请求体对象
            body.withAuth(authbody);
            request.withBody(body);
    
            try {
                // 执行请求
                CreateTokenWithIdTokenResponse response = client.createTokenWithIdToken(request);
                logger.info(response.toString());
            } catch (ClientRequestException e) {
                LogError(e);
            }
        }
    
        private static void LogError(ClientRequestException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("RequestId: " + e.getRequestId());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }
```

## 5.如何运行
该场景可以用于通过OpenID Connect ID token方式获取联邦认证token。替换参数后，执行Run启动main方法即可。  

## 6.返回结果示例
返回结果示例：通过OpenID Connect ID token方式获取联邦认证token   
```
    {
        token: class ScopedTokenInfo {
            expiresAt: 2023-07-26T11:04:57.547000Z
            methods: [mapped]
            issuedAt: 2023-07-25T11:04:57.547000Z
            user: class FederationUserBody {
                osFederation: class OsFederationInfo {
                    identityProvider: class IdpIdInfo {
                        id: lzp*****idc
                    }
                    protocol: class ProtocolIdInfo {
                        id: oi**dc
                    }
                    groups: []
                }
                domain: class DomainInfo {
                    id: 3d63*******************9802
                    name: li****xun
                }
                id: yaoYn***********************PzMM
                name: Fed*********er
            }
            domain: null
            project: null
            roles: []
            catalog: []
        }
        xSubjectToken: MIIDegYJKoZIhv...
    }
```
## 7.其他语言版本的说明
本代码参考华为云API Explorer的代码示例开发Java语言版本，对接SDK，如需使用其他版本，亦可参考华为云API Explorer提供的其他语言代码示例，链接如下：
* [获取联邦认证token(OpenID Connect ID token方式)](https://console.huaweicloud.com/apiexplorer/#/openapi/IAM/sdk?api=CreateTokenWithIdToken)

## 8.修订记录
| 发布日期    | 文档版本  | 修订说明  |
|---------| ------------ | ------------ |
| 2023-08 | 1.0  | 文档首次发布  |