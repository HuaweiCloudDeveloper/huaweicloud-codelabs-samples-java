package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.utils.StringUtils;
import com.huaweicloud.sdk.iam.v3.IamClient;
import com.huaweicloud.sdk.iam.v3.model.AgencyAuth;
import com.huaweicloud.sdk.iam.v3.model.AgencyAuthIdentity;
import com.huaweicloud.sdk.iam.v3.model.AssumeroleSessionuser;
import com.huaweicloud.sdk.iam.v3.model.CreateTemporaryAccessKeyByAgencyRequest;
import com.huaweicloud.sdk.iam.v3.model.CreateTemporaryAccessKeyByAgencyRequestBody;
import com.huaweicloud.sdk.iam.v3.model.CreateTemporaryAccessKeyByAgencyResponse;
import com.huaweicloud.sdk.iam.v3.model.CreateTemporaryAccessKeyByTokenRequest;
import com.huaweicloud.sdk.iam.v3.model.CreateTemporaryAccessKeyByTokenRequestBody;
import com.huaweicloud.sdk.iam.v3.model.CreateTemporaryAccessKeyByTokenResponse;
import com.huaweicloud.sdk.iam.v3.model.IdentityAssumerole;
import com.huaweicloud.sdk.iam.v3.model.IdentityToken;
import com.huaweicloud.sdk.iam.v3.model.ServicePolicy;
import com.huaweicloud.sdk.iam.v3.model.ServiceStatement;
import com.huaweicloud.sdk.iam.v3.model.TokenAuth;
import com.huaweicloud.sdk.iam.v3.model.TokenAuthIdentity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SecurityTokens {
    private static final Logger logger = LoggerFactory.getLogger(SecurityTokens.class);

    public static void main(String[] args) {
        // Hard-coded or plaintext AK and SK are insecure. So, encrypt your AK and SK and store them in the configuration file or environment variables.
        // In this example, the AK and SK are stored in environment variables. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String domainId = ""; // (Mandatory) Account ID.

        // Configure client attributes.
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // Create a credential.
        GlobalCredentials auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk)
            .withDomainId(domainId);

        // Create a request client.
        ArrayList<String> endpoints = new ArrayList<String>();
        endpoints.add(""); // Fixed value. Enter https://iam.myhuaweicloud.com.
        IamClient client = IamClient.newBuilder()
            .withCredential(auth)
            .withEndpoints(endpoints)
            .build();

        // Obtain a temporary access key and security token through a token.
        createTemporaryAccessKeyByToken(client);

        // Obtain a temporary access key and security token through an agency.
        createTemporaryAccessKeyByAgency(client);
    }

    private static void createTemporaryAccessKeyByToken(IamClient client) {
        int durationSeconds = 0; // (Optional) Validity period of the AK, SK, and security token, in seconds. Value range: [900,86400]. The default value is 900.

        String version = ""; // (Mandatory) Permission version. 1.0: System-defined role. 1.1: Custom policy.
        // (Mandatory) Actions, which describe the operations that are allowed or denied on resources.
        // The value format is Service name:Resource type:Operation, for example, vpc:ports:create. You can use a wildcard to specify all actions.
        ArrayList<String> actions = new ArrayList<String>();
        // Add actions. For example:
        // actions.add("vpc:ports:create");
        String effect = ""; // (Mandatory) Fixed value. Enter Allow or Deny.

        // Make an API request.
        CreateTemporaryAccessKeyByTokenRequest request = new CreateTemporaryAccessKeyByTokenRequest();
        CreateTemporaryAccessKeyByTokenRequestBody body = new CreateTemporaryAccessKeyByTokenRequestBody();

        // Obtain the listPolicyStatement.
        List<ServiceStatement> listPolicyStatement = getListPolicyStatement(actions, effect);

        ServicePolicy policyIdentity = new ServicePolicy();
        policyIdentity.withVersion(version)
            .withStatement(listPolicyStatement);
        IdentityToken tokenIdentity = new IdentityToken();
        if (durationSeconds >= 900 && durationSeconds <= 86400) {
            tokenIdentity.withDurationSeconds(durationSeconds);
        }

        List<TokenAuthIdentity.MethodsEnum> listIdentityMethods = new ArrayList<TokenAuthIdentity.MethodsEnum>();
        listIdentityMethods.add(TokenAuthIdentity.MethodsEnum.fromValue("token"));
        TokenAuthIdentity identityAuth = new TokenAuthIdentity();
        identityAuth.withMethods(listIdentityMethods)
            .withToken(tokenIdentity)
            .withPolicy(policyIdentity);
        TokenAuth authbody = new TokenAuth();
        authbody.withIdentity(identityAuth);
        body.withAuth(authbody);
        request.withBody(body);

        try {
            // Execute the request.
            CreateTemporaryAccessKeyByTokenResponse response = client.createTemporaryAccessKeyByToken(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void createTemporaryAccessKeyByAgency(IamClient client) {

        String version = "1.1"; // (Mandatory) Permission version. 1.0: System-defined role. 1.1: Custom policy.
        // (Mandatory) Actions, which describe the operations that are allowed or denied on resources.
        // The value format is Service name:Resource type:Operation, for example, vpc:ports:create. You can use a wildcard to specify all actions.
        ArrayList<String> actions = new ArrayList<String>();
        // Add actions. For example:
        // actions.add("vpc:ports:create");
        String effect = ""; // (Mandatory) Fixed value. Enter Allow or Deny.
        String agencyName = ""; // (Mandatory) Agency name.
        // Use either domainId or domainName.
        String domainId = ""; // Account ID of the delegating party.
        String domainName = ""; // Account name of the delegating party.

        // (Optional) Enterprise username of the delegating party. It starts with a letter and contains 5 to 64 characters, which can only be letters, digits (0-9), hyphens (-), or underscores (_).
        String sessionUserName = "";
        int durationSeconds = 0; // (Optional) Validity period of the AK, SK, and security token, in seconds. Value range: [900,86400]. The default value is 900.

        // Make an API request.
        CreateTemporaryAccessKeyByAgencyRequest request = new CreateTemporaryAccessKeyByAgencyRequest();
        CreateTemporaryAccessKeyByAgencyRequestBody body = new CreateTemporaryAccessKeyByAgencyRequestBody();

        // Obtain the listPolicyStatement.
        List<ServiceStatement> listPolicyStatement = getListPolicyStatement(actions, effect);

        ServicePolicy policyIdentity = new ServicePolicy();
        policyIdentity.withVersion(version)
            .withStatement(listPolicyStatement);

        IdentityAssumerole assumeRoleIdentity = new IdentityAssumerole();
        assumeRoleIdentity.withAgencyName(agencyName);
        if (!StringUtils.isEmpty(domainId)) {
            assumeRoleIdentity.withDomainId(domainId);
        } else if (!StringUtils.isEmpty(domainName)) {
            assumeRoleIdentity.withDomainName(domainName);
        }
        if (durationSeconds >= 900 && durationSeconds <= 86400) {
            assumeRoleIdentity.withDurationSeconds(durationSeconds);
        }
        if (!StringUtils.isEmpty(sessionUserName)) {
            AssumeroleSessionuser sessionUserAssumeRole = new AssumeroleSessionuser();
            sessionUserAssumeRole.withName(sessionUserName);
            assumeRoleIdentity.withSessionUser(sessionUserAssumeRole);
        }

        List<AgencyAuthIdentity.MethodsEnum> listIdentityMethods = new ArrayList<AgencyAuthIdentity.MethodsEnum>();
        listIdentityMethods.add(AgencyAuthIdentity.MethodsEnum.fromValue("assume_role"));
        AgencyAuthIdentity identityAuth = new AgencyAuthIdentity();
        identityAuth.withMethods(listIdentityMethods)
            .withAssumeRole(assumeRoleIdentity)
            .withPolicy(policyIdentity);
        AgencyAuth authbody = new AgencyAuth();
        authbody.withIdentity(identityAuth);
        body.withAuth(authbody);
        request.withBody(body);

        try {
            // Execute the request.
            CreateTemporaryAccessKeyByAgencyResponse response = client.createTemporaryAccessKeyByAgency(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static List<ServiceStatement> getListPolicyStatement(ArrayList<String> actions, String effect) {
        List<String> listStatementAction = new ArrayList<String>();
        for (String action : actions) {
            listStatementAction.add(action);
        }
        List<ServiceStatement> listPolicyStatement = new ArrayList<ServiceStatement>();
        listPolicyStatement.add(
            new ServiceStatement()
                .withAction(listStatementAction)
                .withEffect(ServiceStatement.EffectEnum.fromValue(effect))
        );
        return listPolicyStatement;
    }

    private static void LogError(ClientRequestException e) {
        logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        logger.error("RequestId: " + e.getRequestId());
        logger.error("ErrorCode: " + e.getErrorCode());
        logger.error("ErrorMsg: " + e.getErrorMsg());
    }
}

