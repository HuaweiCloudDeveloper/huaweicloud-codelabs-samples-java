## 1. Introduction
This sample shows how to obtain a temporary access key and security token through an agency or a token.
Temporary access keys and security tokens are issued by the system to IAM users, and can be valid for 15 minutes to 24 hours.
The temporary access keys and security tokens follow the principle of least privilege. During authentication, a temporary access key must be used together with a security token and the **x-security-token** field must be added to the request header.
For details, see [How Do I Use a Temporary AK/SK to Sign Requests?](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-securetoken.html)

## 2. Preparations
### 2.1 Obtaining Required Parameters  
Obtain the required parameters of the delegating party's account, including **ak**, **sk**, **domainId**, **domainName**, and **agencyName**. For details, see the sample code.
* **domainId**: account ID of the delegating party.
* **domainName**: account name of the delegating party.
* **agencyName**: name of the agency created by the delegating party.
* **version**: a fixed value. **1.0** indicates system roles and **1.1** indicates custom policies.
* **effect**: a fixed value. Enter **Allow** or **Deny**.

You must obtain the delegating party's account as well as the Access Key ID (AK) and Secret Access Key (SK) of the account.
To view or create an access key (AK/SK), sign in to Huawei Cloud console and choose **My Credentials** > **Access Keys**. For more information, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).

### 2.2 Obtaining and Installing the SDK
You are advised to use Maven to obtain and install the SDK.
Download and install Maven. Then, add the IAM SDK to the **pom.xml** file in the Java project.
```xml
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-iam</artifactId>
        <version>3.1.28</version>
    </dependency>
```
### 2.3 General Preparations  
#### 2.3.1 Importing Dependent Modules  
```java
    import com.huaweicloud.sdk.core.auth.GlobalCredentials;
    import com.huaweicloud.sdk.core.exception.ClientRequestException;
    import com.huaweicloud.sdk.core.http.HttpConfig;
    import com.huaweicloud.sdk.core.utils.StringUtils;
```
#### 2.3.2 Configuring Client Attributes 
```
    HttpConfig config = HttpConfig.getDefaultHttpConfig();
    config.withIgnoreSSLVerification(true);
    // For local IDEA debugging, a proxy is required. The code is as follows:
    // config.setProxyHost("");
    // config.setProxyPort();
    // config.setProxyUsername("");
    // config.setProxyPassword("");
```
#### 2.3.3 Creating a Credential 
```
     GlobalCredentials auth = new GlobalCredentials()
        .withAk(ak)
        .withSk(sk)
        .withDomainId(domainId);
```
#### 2.3.4 Creating a Request Client  
```
    ArrayList<String> endpoints = new ArrayList<String>();
    endpoints.add("https://iam.myhuaweicloud.com"); 
    IamClient client = IamClient.newBuilder()
        .withCredential(auth)
        .withEndpoints(endpoints)
        .withHttpConfig(config).build();
```
## 3. Runtime Environment
Java JDK 1.8 or later.

## 4. Key Code  
```
    public class SecurityTokens {
        private static final Logger logger = LoggerFactory.getLogger(SecurityTokens.class);
    
        public static void main(String[] args) {
            // Hard-coded or plaintext AK and SK are insecure. So, encrypt your AK and SK and store them in the configuration file or environment variables.
            // In this example, the AK and SK are stored in environment variables. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
            String ak = System.getenv("HUAWEICLOUD_SDK_AK");
            String sk = System.getenv("HUAWEICLOUD_SDK_SK");
            String domainId = ""; // (Mandatory) Account ID.
    
            // Configure client attributes.
            HttpConfig config = HttpConfig.getDefaultHttpConfig();
            config.withIgnoreSSLVerification(true);
    
            // Create a credential.
            GlobalCredentials auth = new GlobalCredentials()
                .withAk(ak)
                .withSk(sk)
                .withDomainId(domainId);
    
            // Create a request client.
            ArrayList<String> endpoints = new ArrayList<String>();
            endpoints.add(""); // Fixed value. Enter https://iam.myhuaweicloud.com.
            IamClient client = IamClient.newBuilder()
                .withCredential(auth)
                .withEndpoints(endpoints)
                .build();
    
            // Obtain a temporary access key and security token through a token.
            createTemporaryAccessKeyByToken(client);
    
            // Obtain a temporary access key and security token through an agency.
            createTemporaryAccessKeyByAgency(client);
        }
    
        private static void createTemporaryAccessKeyByToken(IamClient client) {
            int durationSeconds = 0; // (Optional) Validity period of the AK, SK, and security token, in seconds. Value range: [900,86400]. The default value is 900.
    
            String version = ""; // (Mandatory) Permission version. 1.0: System-defined role. 1.1: Custom policy.
            // (Mandatory) Actions, which describe the operations that are allowed or denied on resources. 
            // The value format is Service name:Resource type:Operation, for example, vpc:ports:create. You can use a wildcard to specify all actions.
            ArrayList<String> actions = new ArrayList<String>();
            // Add actions. For example:
            // actions.add("vpc:ports:create");
            String effect = ""; // (Mandatory) Fixed value. Enter Allow or Deny.
    
            // Make an API request.
            CreateTemporaryAccessKeyByTokenRequest request = new CreateTemporaryAccessKeyByTokenRequest();
            CreateTemporaryAccessKeyByTokenRequestBody body = new CreateTemporaryAccessKeyByTokenRequestBody();
    
            // Obtain the listPolicyStatement.
            List<ServiceStatement> listPolicyStatement = getListPolicyStatement(actions, effect);
    
            ServicePolicy policyIdentity = new ServicePolicy();
            policyIdentity.withVersion(version)
                .withStatement(listPolicyStatement);
            IdentityToken tokenIdentity = new IdentityToken();
            if (durationSeconds >= 900 && durationSeconds <= 86400) {
                tokenIdentity.withDurationSeconds(durationSeconds);
            }
    
            List<TokenAuthIdentity.MethodsEnum> listIdentityMethods = new ArrayList<TokenAuthIdentity.MethodsEnum>();
            listIdentityMethods.add(TokenAuthIdentity.MethodsEnum.fromValue("token"));
            TokenAuthIdentity identityAuth = new TokenAuthIdentity();
            identityAuth.withMethods(listIdentityMethods)
                .withToken(tokenIdentity)
                .withPolicy(policyIdentity);
            TokenAuth authbody = new TokenAuth();
            authbody.withIdentity(identityAuth);
            body.withAuth(authbody);
            request.withBody(body);
    
            try {
                // Execute the request.
                CreateTemporaryAccessKeyByTokenResponse response = client.createTemporaryAccessKeyByToken(request);
                logger.info(response.toString());
            } catch (ClientRequestException e) {
                LogError(e);
            }
        }
    
        private static void createTemporaryAccessKeyByAgency(IamClient client) {
    
            String version = "1.1"; // (Mandatory) Permission version. 1.0: System-defined role. 1.1: Custom policy.
            // (Mandatory) Actions, which describe the operations that are allowed or denied on resources. 
            // The value format is Service name:Resource type:Operation, for example, vpc:ports:create. You can use a wildcard to specify all actions.
            ArrayList<String> actions = new ArrayList<String>();
            // Add actions. For example:
            // actions.add("vpc:ports:create");
            String effect = ""; // (Mandatory) Fixed value. Enter Allow or Deny.
            String agencyName = ""; // (Mandatory) Agency name.
            // Use either domainId or domainName.
            String domainId = ""; // Account ID of the delegating party.
            String domainName = ""; // Account name of the delegating party.
    
            // (Optional) Enterprise username of the delegating party. It starts with a letter and contains 5 to 64 characters, which can only be letters, digits (0-9), hyphens (-), or underscores (_).
            String sessionUserName = "";
            int durationSeconds = 0; // (Optional) Validity period of the AK, SK, and security token, in seconds. Value range: [900,86400]. The default value is 900.
    
            // Make an API request.
            CreateTemporaryAccessKeyByAgencyRequest request = new CreateTemporaryAccessKeyByAgencyRequest();
            CreateTemporaryAccessKeyByAgencyRequestBody body = new CreateTemporaryAccessKeyByAgencyRequestBody();
    
            // Obtain the listPolicyStatement.
            List<ServiceStatement> listPolicyStatement = getListPolicyStatement(actions, effect);
    
            ServicePolicy policyIdentity = new ServicePolicy();
            policyIdentity.withVersion(version)
                .withStatement(listPolicyStatement);
    
            IdentityAssumerole assumeRoleIdentity = new IdentityAssumerole();
            assumeRoleIdentity.withAgencyName(agencyName);
            if (!StringUtils.isEmpty(domainId)) {
                assumeRoleIdentity.withDomainId(domainId);
            } else if (!StringUtils.isEmpty(domainName)) {
                assumeRoleIdentity.withDomainName(domainName);
            }
            if (durationSeconds >= 900 && durationSeconds <= 86400) {
                assumeRoleIdentity.withDurationSeconds(durationSeconds);
            }
            if (!StringUtils.isEmpty(sessionUserName)) {
                AssumeroleSessionuser sessionUserAssumeRole = new AssumeroleSessionuser();
                sessionUserAssumeRole.withName(sessionUserName);
                assumeRoleIdentity.withSessionUser(sessionUserAssumeRole);
            }
    
            List<AgencyAuthIdentity.MethodsEnum> listIdentityMethods = new ArrayList<AgencyAuthIdentity.MethodsEnum>();
            listIdentityMethods.add(AgencyAuthIdentity.MethodsEnum.fromValue("assume_role"));
            AgencyAuthIdentity identityAuth = new AgencyAuthIdentity();
            identityAuth.withMethods(listIdentityMethods)
                .withAssumeRole(assumeRoleIdentity)
                .withPolicy(policyIdentity);
            AgencyAuth authbody = new AgencyAuth();
            authbody.withIdentity(identityAuth);
            body.withAuth(authbody);
            request.withBody(body);
    
            try {
                // Execute the request.
                CreateTemporaryAccessKeyByAgencyResponse response = client.createTemporaryAccessKeyByAgency(request);
                logger.info(response.toString());
            } catch (ClientRequestException e) {
                LogError(e);
            }
        }
    
        private static List<ServiceStatement> getListPolicyStatement(ArrayList<String> actions, String effect) {
            List<String> listStatementAction = new ArrayList<String>();
            for (String action : actions) {
                listStatementAction.add(action);
            }
            List<ServiceStatement> listPolicyStatement = new ArrayList<ServiceStatement>();
            listPolicyStatement.add(
                new ServiceStatement()
                    .withAction(listStatementAction)
                    .withEffect(ServiceStatement.EffectEnum.fromValue(effect))
            );
            return listPolicyStatement;
        }
    
        private static void LogError(ClientRequestException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("RequestId: " + e.getRequestId());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }
```
## 5. Application Scenario
This sample shows how to obtain a temporary access key and security token through an agency or a token.
Each method can be used independently. When you use one method, comment out parameters of the other method and run the main function.  

## 6. Example Result
### 6.1 Result for Obtaining a Temporary Access Key Through a Token  
```
    {
        credential: class Credential {
            expiresAt: 2023-07-25T08:50:05.445000Z
            access: IAH4**********9N4V
            secret: 7mMG********************XCC1
            securitytoken: ggpjbi1ub3J0a……
        }
    }
```
### 6.2 Result for Obtaining a Temporary Access Key Through an Agency  
```
     {
        credential: class Credential {
            expiresAt: 2023-07-25T08:50:05.445000Z
            access: 5RTW***************DI70
            secret: 41Xt*************************3C
            securitytoken: ggpjbi1ub3J0aC0...
        }
    }
```
## 7. Other Languages
The sample code takes reference from Huawei Cloud API Explorer Java code and runs depending on SDK. For details about the sample code in other languages, see the following:
* [Obtaining a Temporary Access Key Through a Token](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/IAM/doc?api=CreateTemporaryAccessKeyByToken)
* [Obtaining a Temporary Access Key Through an Agency](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/IAM/doc?api=CreateTemporaryAccessKeyByAgency)

## 8. Change History
| Releast Date | Version  | Description  |
|:------------:| :------: | :----------: |
|   2023-08    | 1.0  | This issue is the first official release.  |