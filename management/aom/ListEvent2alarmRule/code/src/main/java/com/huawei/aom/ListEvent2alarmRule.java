package com.huawei.aom;

import com.huaweicloud.sdk.aom.v2.AomClient;
import com.huaweicloud.sdk.aom.v2.model.ListEvent2alarmRuleRequest;
import com.huaweicloud.sdk.aom.v2.model.ListEvent2alarmRuleResponse;
import com.huaweicloud.sdk.aom.v2.region.AomRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListEvent2alarmRule {

    private static final Logger logger = LoggerFactory.getLogger(ListEvent2alarmRule.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential icredential = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // Create a service client and set the corresponding region to CN_NORTH_4.
        AomClient client = AomClient.newBuilder()
            .withCredential(icredential)
            .withHttpConfig(httpConfig)
            .withRegion(AomRegion.CN_NORTH_4)
            .build();
        // Create a query request header.
        ListEvent2alarmRuleRequest request = new ListEvent2alarmRuleRequest();
        try {
            ListEvent2alarmRuleResponse response = client.listEvent2alarmRule(request);
            // Print Results
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}