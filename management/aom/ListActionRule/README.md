### 产品介绍

1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/AOM/doc?api=ListActionRule)
中直接运行调试该接口。

2.在本样例中，您可以获取告警行动规则列表。

3.我们可以获取告警行动规则列表，用于自定义告警模块管理。

### 前置条件

1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已开通AOM服务。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-aom”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-aom</artifactId>
    <version>3.1.119</version>
</dependency>
```

### 代码示例

``` java
package com.huawei.aom;

import com.huaweicloud.sdk.aom.v2.AomClient;
import com.huaweicloud.sdk.aom.v2.model.ListActionRuleRequest;
import com.huaweicloud.sdk.aom.v2.model.ListActionRuleResponse;
import com.huaweicloud.sdk.aom.v2.region.AomRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListActionRule {

    private static final Logger logger = LoggerFactory.getLogger(ListActionRule.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential icredential = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CN_NORTH_4
        AomClient client = AomClient.newBuilder().withCredential(icredential).withRegion(AomRegion.CN_NORTH_4).build();
        // 创建查询请求头
        ListActionRuleRequest request = new ListActionRuleRequest();
        try {
            ListActionRuleResponse response = client.listActionRule(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例

#### ListActionRule

```
{
 "actionRules": [
  {
   "create_time": 1671089456314,
   "enterprise_project_id": "0",
   "notification_template": "aom.built-in.te**********",
   "project_id": "16d59f3090e441dba22814**********",
   "rule_name": "vempire-chen",
   "smn_topics": [
    {
     "display_name": "display_name",
     "name": "dxx-test",
     "push_policy": 0,
     "status": 0,
     "topic_urn": "urn:smn:cn-north-4:16d59f3090e441dba228147e1**********"
    }
   ],
   "time_zone": "Asia/Shanghai",
   "type": "1",
   "update_time": 1671089456314,
   "user_name": "**********"
  }
 ],
 "action_rules": [
  {
   "create_time": 1671089456314,
   "enterprise_project_id": "0",
   "notification_template": "aom.built-in.te**********",
   "project_id": "16d59f3090e441dba22814**********",
   "rule_name": "vempire-chen",
   "smn_topics": [
    {
     "display_name": "display_name",
     "name": "dxx-test",
     "push_policy": 0,
     "status": 0,
     "topic_urn": "urn:smn:cn-north-4:16d59f3090e441dba22814**********"
    }
   ],
   "time_zone": "Asia/Shanghai",
   "type": "1",
   "update_time": 1671089456314,
   "user_name": **********"
  }
 ]
}
```

### 修订记录

    发布日期    | 文档版本 | 修订说明

:----------:|:----:| :------:  
2024/10/31 | 1.0 | 文档首次发布