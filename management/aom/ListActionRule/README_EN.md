### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/AOM/doc?api=ListActionRule).

2.In this example, you can obtain the alarm action rule list.

3.You can obtain the alarm action rule list for managing customized alarm modules.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The AOM service has been enabled.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-aom. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-aom</artifactId>
    <version>3.1.119</version>
</dependency>
```

### Code example

``` java
package com.huawei.aom;

import com.huaweicloud.sdk.aom.v2.AomClient;
import com.huaweicloud.sdk.aom.v2.model.ListActionRuleRequest;
import com.huaweicloud.sdk.aom.v2.model.ListActionRuleResponse;
import com.huaweicloud.sdk.aom.v2.region.AomRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListActionRule {

    private static final Logger logger = LoggerFactory.getLogger(ListActionRule.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential icredential = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // Create a service client and set the corresponding region to CN_NORTH_4.
        AomClient client = AomClient.newBuilder()
            .withCredential(icredential)
            .withHttpConfig(httpConfig)
            .withRegion(AomRegion.CN_NORTH_4)
            .build();
        // Create a query request header.
        ListActionRuleRequest request = new ListActionRuleRequest();
        try {
            ListActionRuleResponse response = client.listActionRule(request);
            // Print Results
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result

#### ListActionRule

```
{
 "actionRules": [
  {
   "create_time": 1671089456314,
   "enterprise_project_id": "0",
   "notification_template": "aom.built-in.te**********",
   "project_id": "16d59f3090e441dba22814**********",
   "rule_name": "vempire-chen",
   "smn_topics": [
    {
     "display_name": "display_name",
     "name": "dxx-test",
     "push_policy": 0,
     "status": 0,
     "topic_urn": "urn:smn:cn-north-4:16d59f3090e441dba228147e1**********"
    }
   ],
   "time_zone": "Asia/Shanghai",
   "type": "1",
   "update_time": 1671089456314,
   "user_name": "**********"
  }
 ],
 "action_rules": [
  {
   "create_time": 1671089456314,
   "enterprise_project_id": "0",
   "notification_template": "aom.built-in.te**********",
   "project_id": "16d59f3090e441dba22814**********",
   "rule_name": "vempire-chen",
   "smn_topics": [
    {
     "display_name": "display_name",
     "name": "dxx-test",
     "push_policy": 0,
     "status": 0,
     "topic_urn": "urn:smn:cn-north-4:16d59f3090e441dba22814**********"
    }
   ],
   "time_zone": "Asia/Shanghai",
   "type": "1",
   "update_time": 1671089456314,
   "user_name": **********"
  }
 ]
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/10/31 | 1.0 | This document is released for the first time.