## 功能介绍

华为云提供了应用运维管理服务端SDK，您可以直接集成服务端SDK来调用应用运维管理服务的相关API，从而实现对应用运维管理服务的快速操作。 若您想对您的应用实现可视化管理及健康状态的实时观测，您可以在 AOM-CMDB-应用管理
菜单下创建应用拓扑树。 应用拓扑树创建成功后，再完成其他一系列后续操作，您就可以在 AOM-CMDB 界面对您的应用和资源进行管理，也可以在AOM2.0-应用洞察-应用监控界面，查看您的应用健康状态。
该示例展示了如何通过Java版SDK创建应用拓扑树。

## 前置条件

获取AK/SK 开发者在使用前需先获取账号的ak、sk、endpoint。

您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证-访问密钥”页面上创建和查看您的 AK/SK。更多信息请查看访问密钥。

运行环境 Java JDK 1.8 及其以上版本。

SDK获取和安装 您可以通过Maven配置所依赖的应用运维管理SDK，本示例基于华为云SDK V3.1.64版本开发。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.huawei</groupId>
    <artifactId>cmdb-create-topology-tree</artifactId>
    <version>1.0-SNAPSHOT</version>

    <parent>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-services</artifactId>
        <version>3.1.64</version>
    </parent>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>8</source>
                    <target>8</target>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <dependencies>
        <dependency>
            <groupId>com.huaweicloud.sdk</groupId>
            <artifactId>huaweicloud-sdk-core</artifactId>
            <version>3.1.64</version>
        </dependency>
        <dependency>
            <groupId>com.huaweicloud.sdk</groupId>
            <artifactId>huaweicloud-sdk-aom</artifactId>
            <version>3.1.64</version>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>1.7.21</version>
        </dependency>
    </dependencies>
</project>
```

## 示例代码

```java
package com.huawei.aom.demo;

import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.huaweicloud.sdk.aom.v3.AomClient;
import com.huaweicloud.sdk.aom.v3.model.BizAppParam;
import com.huaweicloud.sdk.aom.v3.model.ComponentParam;
import com.huaweicloud.sdk.aom.v3.model.CreateAppRequest;
import com.huaweicloud.sdk.aom.v3.model.CreateAppResponse;
import com.huaweicloud.sdk.aom.v3.model.CreateComponentRequest;
import com.huaweicloud.sdk.aom.v3.model.CreateComponentResponse;
import com.huaweicloud.sdk.aom.v3.model.CreateEnvRequest;
import com.huaweicloud.sdk.aom.v3.model.CreateEnvResponse;
import com.huaweicloud.sdk.aom.v3.model.CreateSubAppRequest;
import com.huaweicloud.sdk.aom.v3.model.CreateSubAppResponse;
import com.huaweicloud.sdk.aom.v3.model.EnvParam;
import com.huaweicloud.sdk.aom.v3.model.SubAppCreateParam;
import com.huaweicloud.sdk.aom.v3.region.AomRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

public class CreateTopologyTreeSolution {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateTopologyTreeSolution.class);

    private static final Pattern PATTERN = Pattern.compile("\"id\":\"(\\S+)\"");

    /*认证用的 ak 和 sk 硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    本示例以 ak 和 sk 保存在环境变量中为例，运行本示例前请先在本地环境中设置环境变量 HUAWEICLOUD_SDK_AK 和 HUAWEICLOUD_SDK_SK 。*/
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");

    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    // eg: cn-north-4
    private static final String REGION_ID = "<YOUR REGION ID>";

    public static void main(String[] args) {
        ICredential auth = new BasicCredentials()
            .withAk(AK)
            .withSk(SK);

        AomClient client = AomClient.newBuilder()
            .withCredential(auth)
            .withRegion(AomRegion.valueOf(REGION_ID))
            .build();

        // 这里为避免应用、子应用等唯一标识重复，使用时间戳加以区分
        LocalDateTime now = LocalDateTime.now();
        String dateTime = now.toString().replace(":", "-");

        String appId = createApp(client, dateTime);
        String subAppId = createSubApp(client, appId, dateTime);
        String componentId = createComponent(client, subAppId, dateTime);
        createEnv(client, componentId, dateTime);

        LOGGER.info("create app_topology successfully. app's display_name is {}", "app_display_name_" + dateTime);
    }

    private static String createApp(AomClient client, String dateTime) {
        CreateAppRequest request = new CreateAppRequest();
        BizAppParam body = new BizAppParam();
        body.withEpsId("0")
            .withName("app_name_" + dateTime)
            .withDisplayName("app_display_name_" + dateTime)
            .withDescription("app_description");
        request.withBody(body);
        try {
            CreateAppResponse response = client.createApp(request);
            LOGGER.info("create app response: {}", response.toString());
            String responseBody = response.getBody();
            return getIdFromString(responseBody);
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            LOGGER.error("request aom-cmdb create_app interface occur error.", e);
        }
        return null;
    }

    private static String createSubApp(AomClient client, String appId, String dateTime) {
        CreateSubAppRequest request = new CreateSubAppRequest();
        SubAppCreateParam body = new SubAppCreateParam();
        body.withName("sub_app_name_" + dateTime)
            .withDisplayName("sub_app_display_name_" + dateTime)
            .withModelType(SubAppCreateParam.ModelTypeEnum.APPLICATION)
            .withModelId(appId)
            .withDescription("sub_app_description");
        request.withBody(body);
        try {
            CreateSubAppResponse response = client.createSubApp(request);
            LOGGER.info("create sub_app response: {}", response.toString());
            String responseBody = response.getBody();
            return getIdFromString(responseBody);
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            LOGGER.error("request aom-cmdb create_sub_app interface occur error.", e);
        }
        return null;
    }

    private static String createComponent(AomClient client, String subAppId, String dateTime) {
        CreateComponentRequest request = new CreateComponentRequest();
        ComponentParam body = new ComponentParam();
        body.withName("component_name_" + dateTime)
            .withModelType(ComponentParam.ModelTypeEnum.SUB_APPLICATION)
            .withModelId(subAppId)
            .withDescription("component_description");
        request.withBody(body);
        try {
            CreateComponentResponse response = client.createComponent(request);
            LOGGER.info("create component response: {}", response.toString());
            return response.getId();
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            LOGGER.error("request aom-cmdb create_component interface occur error.", e);
        }
        return null;
    }

    private static void createEnv(AomClient client, String componentId, String dateTime) {
        CreateEnvRequest request = new CreateEnvRequest();
        EnvParam body = new EnvParam();
        body.withEnvName("env_name_" + dateTime)
            .withDescription("env_description")
            .withComponentId(componentId)
            .withEnvType(EnvParam.EnvTypeEnum.DEV)
            .withOsType(EnvParam.OsTypeEnum.LINUX)
            .withRegion(REGION_ID)
            .withRegisterType(EnvParam.RegisterTypeEnum.API);
        request.withBody(body);
        try {
            CreateEnvResponse response = client.createEnv(request);
            LOGGER.info("create env response: {}", response.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            LOGGER.error("request aom-cmdb create_env interface occur error.", e);
        }
    }

    private static String getIdFromString(String idStr) {
        Matcher matcher = PATTERN.matcher(idStr);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }
}
```

## 返回示例

create app_topology successfully. app display_name: my_test_app_display_name_2023-11-07T11-24-50.044

## 参考链接

更多信息请参考 [应用运维管理AOM](https://support.huaweicloud.com/aom/index.html)

## 修订记录

|  发布日期  |  文档版本 |  修订说明 |
   | ------------ | ------------ | ------------ |
|  2023-11-07 |  1.0 |  文档首次发布 |
   