package com.huawei.aom.demo;

import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.huaweicloud.sdk.aom.v3.AomClient;
import com.huaweicloud.sdk.aom.v3.model.BizAppParam;
import com.huaweicloud.sdk.aom.v3.model.ComponentParam;
import com.huaweicloud.sdk.aom.v3.model.CreateAppRequest;
import com.huaweicloud.sdk.aom.v3.model.CreateAppResponse;
import com.huaweicloud.sdk.aom.v3.model.CreateComponentRequest;
import com.huaweicloud.sdk.aom.v3.model.CreateComponentResponse;
import com.huaweicloud.sdk.aom.v3.model.CreateEnvRequest;
import com.huaweicloud.sdk.aom.v3.model.CreateEnvResponse;
import com.huaweicloud.sdk.aom.v3.model.CreateSubAppRequest;
import com.huaweicloud.sdk.aom.v3.model.CreateSubAppResponse;
import com.huaweicloud.sdk.aom.v3.model.EnvParam;
import com.huaweicloud.sdk.aom.v3.model.SubAppCreateParam;
import com.huaweicloud.sdk.aom.v3.region.AomRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

public class CreateTopologyTreeSolution {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateTopologyTreeSolution.class);

    private static final Pattern PATTERN = Pattern.compile("\"id\":\"(\\S+)\"");

    /*认证用的 ak 和 sk 硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    本示例以 ak 和 sk 保存在环境变量中为例，运行本示例前请先在本地环境中设置环境变量 HUAWEICLOUD_SDK_AK 和 HUAWEICLOUD_SDK_SK 。*/
    private static final String AK = System.getenv("HUAWEICLOUD_SDK_AK");

    private static final String SK = System.getenv("HUAWEICLOUD_SDK_SK");

    // eg: cn-north-4
    private static final String REGION_ID = "<YOUR REGION ID>";

    public static void main(String[] args) {
        ICredential auth = new BasicCredentials()
            .withAk(AK)
            .withSk(SK);

        AomClient client = AomClient.newBuilder()
            .withCredential(auth)
            .withRegion(AomRegion.valueOf(REGION_ID))
            .build();

        // 这里为避免应用、子应用等唯一标识重复，使用时间戳加以区分
        LocalDateTime now = LocalDateTime.now();
        String dateTime = now.toString().replace(":", "-");

        String appId = createApp(client, dateTime);
        String subAppId = createSubApp(client, appId, dateTime);
        String componentId = createComponent(client, subAppId, dateTime);
        createEnv(client, componentId, dateTime);

        LOGGER.info("create app_topology successfully. app's display_name is {}", "app_display_name_" + dateTime);
    }

    private static String createApp(AomClient client, String dateTime) {
        CreateAppRequest request = new CreateAppRequest();
        BizAppParam body = new BizAppParam();
        body.withEpsId("0")
            .withName("app_name_" + dateTime)
            .withDisplayName("app_display_name_" + dateTime)
            .withDescription("app_description");
        request.withBody(body);
        try {
            CreateAppResponse response = client.createApp(request);
            LOGGER.info("create app response: {}", response.toString());
            String responseBody = response.getBody();
            return getIdFromString(responseBody);
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            LOGGER.error("request aom-cmdb create_app interface occur error.", e);
        }
        return null;
    }

    private static String createSubApp(AomClient client, String appId, String dateTime) {
        CreateSubAppRequest request = new CreateSubAppRequest();
        SubAppCreateParam body = new SubAppCreateParam();
        body.withName("sub_app_name_" + dateTime)
            .withDisplayName("sub_app_display_name_" + dateTime)
            .withModelType(SubAppCreateParam.ModelTypeEnum.APPLICATION)
            .withModelId(appId)
            .withDescription("sub_app_description");
        request.withBody(body);
        try {
            CreateSubAppResponse response = client.createSubApp(request);
            LOGGER.info("create sub_app response: {}", response.toString());
            String responseBody = response.getBody();
            return getIdFromString(responseBody);
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            LOGGER.error("request aom-cmdb create_sub_app interface occur error.", e);
        }
        return null;
    }

    private static String createComponent(AomClient client, String subAppId, String dateTime) {
        CreateComponentRequest request = new CreateComponentRequest();
        ComponentParam body = new ComponentParam();
        body.withName("component_name_" + dateTime)
            .withModelType(ComponentParam.ModelTypeEnum.SUB_APPLICATION)
            .withModelId(subAppId)
            .withDescription("component_description");
        request.withBody(body);
        try {
            CreateComponentResponse response = client.createComponent(request);
            LOGGER.info("create component response: {}", response.toString());
            return response.getId();
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            LOGGER.error("request aom-cmdb create_component interface occur error.", e);
        }
        return null;
    }

    private static void createEnv(AomClient client, String componentId, String dateTime) {
        CreateEnvRequest request = new CreateEnvRequest();
        EnvParam body = new EnvParam();
        body.withEnvName("env_name_" + dateTime)
            .withDescription("env_description")
            .withComponentId(componentId)
            .withEnvType(EnvParam.EnvTypeEnum.DEV)
            .withOsType(EnvParam.OsTypeEnum.LINUX)
            .withRegion(REGION_ID)
            .withRegisterType(EnvParam.RegisterTypeEnum.API);
        request.withBody(body);
        try {
            CreateEnvResponse response = client.createEnv(request);
            LOGGER.info("create env response: {}", response.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            LOGGER.error("request aom-cmdb create_env interface occur error.", e);
        }
    }

    private static String getIdFromString(String idStr) {
        Matcher matcher = PATTERN.matcher(idStr);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }
}