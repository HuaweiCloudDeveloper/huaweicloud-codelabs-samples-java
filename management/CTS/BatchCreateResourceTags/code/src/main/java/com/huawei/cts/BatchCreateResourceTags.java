package com.huawei.cts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.cts.v3.CtsClient;
import com.huaweicloud.sdk.cts.v3.model.BatchCreateResourceTagsRequest;
import com.huaweicloud.sdk.cts.v3.model.BatchCreateResourceTagsRequestBody;
import com.huaweicloud.sdk.cts.v3.model.BatchCreateResourceTagsResponse;
import com.huaweicloud.sdk.cts.v3.model.Tags;
import com.huaweicloud.sdk.cts.v3.region.CtsRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class BatchCreateResourceTags {
    private static final Logger logger = LoggerFactory.getLogger(BatchCreateResourceTags.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential batchCreateResourceTagsAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // 创建服务客户端并配置对应region为CtsRegion.CN_NORTH_4
        CtsClient client = CtsClient.newBuilder()
            .withCredential(batchCreateResourceTagsAuth)
            .withRegion(CtsRegion.CN_NORTH_4)
            .build();
        // 构建请求
        BatchCreateResourceTagsRequest request = new BatchCreateResourceTagsRequest();
        // 追踪器的id，可通过“查询追踪器”接口获取
        request.withResourceId("{resourceId}");
        // CTS服务的资源类型,目前仅支持cts-tracker
        request.withResourceType(BatchCreateResourceTagsRequest.ResourceTypeEnum.fromValue("cts-tracker"));
        // 构建请求体并设置参数
        BatchCreateResourceTagsRequestBody body = new BatchCreateResourceTagsRequestBody();
        // 标签列表
        List<Tags> listBodyTags = new ArrayList<>();
        // 标签的键和值
        listBodyTags.add(new Tags().withKey("key1").withValue("value1"));
        body.withTags(listBodyTags);
        request.withBody(body);
        try {
            // 获取结果
            BatchCreateResourceTagsResponse response = client.batchCreateResourceTags(request);
            // 打印结果
            logger.info(String.valueOf(response.getHttpStatusCode()));
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}