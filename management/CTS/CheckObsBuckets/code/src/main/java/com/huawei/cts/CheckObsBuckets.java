package com.huawei.cts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.cts.v3.CtsClient;
import com.huaweicloud.sdk.cts.v3.model.CheckBucketRequest;
import com.huaweicloud.sdk.cts.v3.model.CheckObsBucketsRequest;
import com.huaweicloud.sdk.cts.v3.model.CheckObsBucketsRequestBody;
import com.huaweicloud.sdk.cts.v3.model.CheckObsBucketsResponse;
import com.huaweicloud.sdk.cts.v3.region.CtsRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CheckObsBuckets {
    private static final Logger logger = LoggerFactory.getLogger(CheckObsBuckets.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential checkObsBucketsAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // 创建服务客户端并配置对应region为CtsRegion.CN_NORTH_4
        CtsClient client = CtsClient.newBuilder()
            .withCredential(checkObsBucketsAuth)
            .withRegion(CtsRegion.CN_NORTH_4)
            .build();
        // 构建请求
        CheckObsBucketsRequest request = new CheckObsBucketsRequest();
        // 账号ID
        request.withDomainId("{domainId}");
        // 构建请求体并设置参数
        CheckObsBucketsRequestBody body = new CheckObsBucketsRequestBody();
        // 请求检查的OBS桶列表
        List<CheckBucketRequest> listBodyBuckets = new ArrayList<>();
        // 标识OBS桶名称和标识OBS桶位置
        listBodyBuckets.add(new CheckBucketRequest().withBucketName("test-obs-zc").withBucketLocation("cn-north-4"));
        body.withBuckets(listBodyBuckets);
        request.withBody(body);
        try {
            // 获取结果
            CheckObsBucketsResponse response = client.checkObsBuckets(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}