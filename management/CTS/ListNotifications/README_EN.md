### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CTS/doc?api=ListNotifications).

2.In this example, you can query key operation notifications.

3.Based on the returned result, you can obtain the key operation notification list, including the operation type, operation event list, notified user list, key operation notification status, and key operation notification ID.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. 
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. 
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.You have enabled CTS and have the required permissions.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-cts. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cts</artifactId>
    <version>3.1.120</version>
</dependency>
```

### Code example
The following code shows how to query key operation notifications:
``` java
package com.huawei.cts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.cts.v3.CtsClient;
import com.huaweicloud.sdk.cts.v3.model.ListNotificationsRequest;
import com.huaweicloud.sdk.cts.v3.model.ListNotificationsResponse;
import com.huaweicloud.sdk.cts.v3.region.CtsRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListNotifications {

    private static final Logger logger = LoggerFactory.getLogger(ListNotifications.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential listNotificationsAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // Create a service client and set the corresponding region to CtsRegion.CN_NORTH_4.
        CtsClient client = CtsClient.newBuilder()
            .withCredential(listNotificationsAuth)
            .withRegion(CtsRegion.CN_NORTH_4)
            .build();
        // Construction request
        ListNotificationsRequest request = new ListNotificationsRequest();
        // Notification type, which can be SMN or FAN.
        request.withNotificationType(ListNotificationsRequest.NotificationTypeEnum.SMN);
        try {
            // Obtaining Results
            ListNotificationsResponse response = client.listNotifications(request);
            // Print Results
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
#### ListNotifications
```
{
    notifications: [class NotificationsResponseBody {
        notificationName: keyOperate_info_myhn
        operationType: complete
        agencyName: null
        operations: []
        notifyUserList: []
        status: disabled
        topicId: 
        notificationId: f328xxxxxx6821
        notificationType: smn
        projectId: 0ba7xxxxxx
        createTime: 1730776547658
        filter: class Filter {
            condition: 
            isSupportFilter: false
            rule: []
        }
    }]
}
```
### Change History

 Released On  |  Issue   | Description
 :----: |:---:| :------:  
 2024/11/05 | 1.0 | This document is released for the first time.