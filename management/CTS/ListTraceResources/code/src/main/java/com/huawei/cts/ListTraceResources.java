package com.huawei.cts;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.cts.v3.CtsClient;
import com.huaweicloud.sdk.cts.v3.model.ListTraceResourcesRequest;
import com.huaweicloud.sdk.cts.v3.model.ListTraceResourcesResponse;
import com.huaweicloud.sdk.cts.v3.region.CtsRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListTraceResources {
    private static final Logger logger = LoggerFactory.getLogger(ListTraceResources.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential listTraceResourcesAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // 创建服务客户端并配置对应region为CtsRegion.CN_NORTH_4
        CtsClient client = CtsClient.newBuilder()
            .withCredential(listTraceResourcesAuth)
            .withRegion(CtsRegion.CN_NORTH_4)
            .build();
        // 构建请求
        ListTraceResourcesRequest request = new ListTraceResourcesRequest();
        // 账号ID
        request.withDomainId("{domainId}");
        try {
            // 获取结果
            ListTraceResourcesResponse response = client.listTraceResources(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}