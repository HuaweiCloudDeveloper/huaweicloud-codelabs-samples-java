## 1. 示例简介
配置审计(Config)提供全局资源配置的检索，配置历史追溯，以及基于资源配置的持续的审计评估能力，确保云上资源配置变更符合客户预期。您可以使用Config查看您所拥有的资源有哪些；可以查看资源详情、资源之间的关系、资源历史；Config会在资源变更时发送消息通知给您，并定期（6小时）对您的资源变更消息进行存储；Config还会定期（24小时）对您的资源进行存储；您还可以通过配置合规规则来对您的资源进行合规性检查。

该示例展示了如何通过Java版本SDK查询资源与资源关系的变更历史。

1. [资源详情](https://support.huaweicloud.com/usermanual-rms/rms_03_0102.html) 默认展示资源的部分属性，如果您需要查看某个资源的资源详情，可按如下操作查看。
2. [资源关系](https://support.huaweicloud.com/usermanual-rms/rms_03_0200.html) 记录了您在华为云上的不同资源之间的关联情况。
3. [资源历史](https://support.huaweicloud.com/usermanual-rms/rms_03_0300.html) 是过去某段时间内资源不同状态的集合。对接服务上报Config的资源属性和资源关系的变化，都会在资源时间线中生成一条记录，该记录会包含资源变更情况的详细信息，默认的保存期限为7年。

## 2. 开发前准备

1. 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
2. 您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 访问密钥 。
3. [配置审计 Config SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/Config?lang=Java)，支持 Java JDK 1.8 及其以上版本。

## 3. 安装SDK

您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。 具体的SDK版本号请参见 [配置审计 Config SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/Config?lang=Java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-config</artifactId>
    <version>{sdk-version}</version>
</dependency>
```

## 4. 代码示例
```java

public class ShowResourceRelationDemo {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "<region id>";
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        ICredential auth = new GlobalCredentials().withAk(ak).withSk(sk);
        ConfigClient client = ConfigClient.newBuilder().withHttpConfig(config).withCredential(auth)
            .withRegion(ConfigRegion.valueOf(regionId)).build();

        try {
            String resourceId = "<resource id>";
            // 查询资源详情
            ShowResourceDetailRequest resourceDetailRequest = new ShowResourceDetailRequest()
                .withResourceId(resourceId);
            System.out.println(client.showResourceDetail(resourceDetailRequest));
            // 查询资源关系
            ShowResourceRelationsRequest resourceRelationsRequest = new ShowResourceRelationsRequest()
                .withResourceId(resourceId)
                .withDirection(ShowResourceRelationsRequest.DirectionEnum.IN);
            System.out.println(client.showResourceRelations(resourceRelationsRequest).toString());
            // 查询资源历史
            ShowResourceHistoryRequest resourceHistoryRequest = new ShowResourceHistoryRequest()
                .withResourceId(resourceId);
            System.out.println(client.showResourceHistory(resourceHistoryRequest).toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException ex) {
            System.out.println(ex);
        }
    }
}

```


## 5.返回结果示例

```
class ShowResourceDetailResponse {
    id: 81fi****a864
    name: zh****ng
    provider: iam
    type: users
    regionId: global
    projectId: 
    projectName: 
    epId: 0
    epName: default
    checksum: 522u****e689
    created: 2023-09-18T12:56:30.000Z
    updated: 2023-09-18T12:56:30.000Z
    provisioningState: Succeeded
    state: Normal
    tags: {}
    properties: {pwd_status=false, pwd_strength=high, group_list=[f588****54c5], role_list=[], last_login_time=2023-09-18T12:57:45Z, virtual_mfa_device=false, login_protect={enabled=false}, credentials=[], policy_list=[], access_mode=default, is_root_user=false, enabled=true}
}

class ShowResourceRelationsResponse {
    relations: [class ResourceRelation {
        relationType: contains
        fromResourceType: iam.groups
        toResourceType: iam.users
        fromResourceId: f587****54c5
        toResourceId: 81fa****a864
    }]
    pageInfo: class PageInfo {
        currentCount: 1
        nextMarker: null
    }
}

class ShowResourceHistoryResponse {
    items: [class HistoryItem {
        domainId: 39f4****ea39
        resourceId: 81fa****a864
        resourceType: iam.users
        captureTime: 2023-09-21T15:39:27.632Z
        status: ResourceChanged.CREATE
        relations: [class ResourceRelation {
            relationType: isContainedIn
            fromResourceType: iam.users
            toResourceType: iam.groups
            fromResourceId: 81fa****a864
            toResourceId: b04e****8dd2
        }]
        resource: class ResourceEntity {
            id: 81fa****a864
            name: zh****ng
            provider: iam
            type: users
            regionId: global
            projectId: 
            projectName: 
            epId: 0
            epName: default
            checksum: 00ce****f053
            created: 2023-09-18T12:56:30Z
            updated: 2023-09-18T12:56:30Z
            provisioningState: Succeeded
            state: null
            tags: {}
            properties: {pwd_status=false, pwd_strength=high, group_list=[b04e****8dd2], role_list=[], virtual_mfa_device=false, login_protect={enabled=false}, credentials=[], policy_list=[], access_mode=default, enabled=true}
        }
    }]
    pageInfo: class PageInfo {
        currentCount: 1
        nextMarker: null
    }
}
```

## 6. 参考
更多信息请参考 [配置审计Config文档](https://support.huaweicloud.com/rms/index.html)

## 7. 修订记录
| 发布日期       | 文档版本  | 修订说明  |
|------------| ------------ | ------------ |
| 2024-12-25 | 1.0  | 文档首次发布  |
