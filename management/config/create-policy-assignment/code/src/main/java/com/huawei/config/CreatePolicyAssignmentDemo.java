package com.huawei.config;

import com.huaweicloud.sdk.config.v1.ConfigClient;
import com.huaweicloud.sdk.config.v1.model.CreatePolicyAssignmentsRequest;
import com.huaweicloud.sdk.config.v1.model.CreatePolicyAssignmentsResponse;
import com.huaweicloud.sdk.config.v1.model.PolicyAssignmentRequestBody;
import com.huaweicloud.sdk.config.v1.model.PolicyFilterDefinition;
import com.huaweicloud.sdk.config.v1.model.ShowPolicyAssignmentRequest;
import com.huaweicloud.sdk.config.v1.model.ShowPolicyAssignmentResponse;
import com.huaweicloud.sdk.config.v1.region.ConfigRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

public class CreatePolicyAssignmentDemo {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "<region id>";
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        ICredential auth = new GlobalCredentials().withAk(ak).withSk(sk);
        ConfigClient client = ConfigClient.newBuilder().withHttpConfig(config).withCredential(auth)
            .withRegion(ConfigRegion.valueOf(regionId)).build();

        CreatePolicyAssignmentsRequest createRequest = new CreatePolicyAssignmentsRequest()
            .withBody(new PolicyAssignmentRequestBody().withPolicyAssignmentType(PolicyAssignmentRequestBody.PolicyAssignmentTypeEnum.BUILTIN)
            .withName("<Your policy_assignment_name>").withDescription("<Your policy_assignment_description>")
            .withPolicyFilter(new PolicyFilterDefinition()).withPolicyDefinitionId("<Your policy_definition_id>"));

        try {
            CreatePolicyAssignmentsResponse createResponse = client.createPolicyAssignments(createRequest);
            System.out.println(createResponse.toString());
            ShowPolicyAssignmentRequest showPolicyAssignmentRequest = new ShowPolicyAssignmentRequest()
                .withPolicyAssignmentId(createResponse.getId());
            ShowPolicyAssignmentResponse showResponse = client.showPolicyAssignment(showPolicyAssignmentRequest);
            System.out.println(showResponse.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException ex) {
            System.out.println(ex);
        }
    }
}