## 1. 示例简介
配置审计(Config)提供全局资源配置的检索，配置历史追溯，以及基于资源配置的持续的审计评估能力，确保云上资源配置变更符合客户预期。您可以使用Config查看您所拥有的资源有哪些；可以查看资源详情、资源之间的关系、资源历史；Config会在资源变更时发送消息通知给您，并定期（6小时）对您的资源变更消息进行存储；Config还会定期（24小时）对您的资源进行存储；您还可以通过配置合规规则来对您的资源进行合规性检查。

该示例展示了如何通过Java版本SDK为Config服务创建合规规则和查询合规规则。

[合规规则](https://support.huaweicloud.com/usermanual-rms/rms_05_0402.html) 通过指定合规策略和合规策略所应用的范围（如：在某一区域的某些资源）来构成合规规则。

## 2. 开发前准备

1. 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
2. 您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 访问密钥 。
3. [配置审计 Config SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/Config?lang=Java)，支持 Java JDK 1.8 及其以上版本。

## 3. 安装SDK

您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。 具体的SDK版本号请参见 [配置审计 Config SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/Config?lang=Java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-config</artifactId>
    <version>{sdk-version}</version>
</dependency>
```

## 4. 代码示例
```java

public class CreatePolicyAssignmentDemo {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "<region id>";
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        ICredential auth = new GlobalCredentials().withAk(ak).withSk(sk);
        ConfigClient client = ConfigClient.newBuilder().withHttpConfig(config).withCredential(auth)
            .withRegion(ConfigRegion.valueOf(regionId)).build();

        CreatePolicyAssignmentsRequest createRequest = new CreatePolicyAssignmentsRequest()
            .withBody(new PolicyAssignmentRequestBody().withPolicyAssignmentType(PolicyAssignmentRequestBody.PolicyAssignmentTypeEnum.BUILTIN)
                .withName("<Your policy_assignment_name>").withDescription("<Your policy_assignment_description>")
                .withPolicyFilter(new PolicyFilterDefinition()).withPolicyDefinitionId("<Your policy_definition_id>"));

        try {
            CreatePolicyAssignmentsResponse createResponse = client.createPolicyAssignments(createRequest);
            System.out.println(createResponse.toString());
            ShowPolicyAssignmentRequest showPolicyAssignmentRequest = new ShowPolicyAssignmentRequest()
                .withPolicyAssignmentId(createResponse.getId());
            ShowPolicyAssignmentResponse showResponse = client.showPolicyAssignment(showPolicyAssignmentRequest);
            System.out.println(showResponse.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException ex) {
            System.out.println(ex);
        }
    }
}

```


## 5.返回结果示例

```
class CreatePolicyAssignmentsResponse {
	policyAssignmentType: "policyAssignmentType"
	id: "id"
	name: "name"
	description: "description"
	policyFilter: class PolicyFilterDefinition {}
	period: "period"
	state: "state"
	created: "created"
	updated: "updated"
	policyDefinitionId: "policyDefinitionId"
	customPolicy: "customPolicy"
	parameters: {}
	createdBy: "createdBy"
}

class ShowPolicyAssignmentResponse {
	policyAssignmentType: "policyAssignmentType"
	id: "id"
	name: "name"
	description: "description"
	policyFilter: class PolicyFilterDefinition {}
	period: "period"
	state: "state"
	created: "created"
	updated: "updated"
	policyDefinitionId: "policyDefinitionId"
	customPolicy: "customPolicy"
	parameters: {}
	createdBy: "createdBy"
}
```

## 6. 参考
更多信息请参考 [配置审计Config文档](https://support.huaweicloud.com/rms/index.html)

## 7. 修订记录
| 发布日期      | 文档版本 | 修订说明  |
|-----------|------| ------------ |
| 2024-12-25 | 1.1  | 文档首次发布  |
