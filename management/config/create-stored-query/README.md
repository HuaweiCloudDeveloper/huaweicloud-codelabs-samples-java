## 1.介绍
配置审计(Config)提供全局资源配置的检索，配置历史追溯，以及基于资源配置的持续的审计评估能力，确保云上资源配置变更符合客户预期。您可以使用Config查看您所拥有的资源有哪些；可以查看资源详情、资源之间的关系、资源历史；Config会在资源变更时发送消息通知给您，并定期（6小时）对您的资源变更消息进行存储；Config还会定期（24小时）对您的资源进行存储；您还可以通过配置合规规则来对您的资源进行合规性检查。

该示例展示了如何通过Java版本SDK为Config服务创建高级查询和运行高级查询。

[高级查询](https://support.huaweicloud.com/usermanual-rms/rms_10_0100.html)支持用户自定义查询和浏览华为云云服务资源，对当前资源数据执行基于属性的查询和聚合。

## 2.开发流程图

![](assets/image.png)

## 3.前置条件

1. 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
2. 您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 访问密钥 。
3. 华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。

## 4.关键代码片段
```java
public class Main {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "<region id>";

        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        ConfigClient client = ConfigClient.newBuilder()
            .withHttpConfig(config)
            .withCredential(auth)
            .withRegion(ConfigRegion.valueOf(regionId))
            .build();

        CreateStoredQueryRequest createRequest = new CreateStoredQueryRequest()
            .withBody(new StoredQueryRequestBody()
                .withName("config_test")
                .withDescription("description_test")
                .withExpression("select count(*) from resources"));

        RunQueryRequest runRequest = new RunQueryRequest()
            .withBody(new QueryRunRequestBody()
                .withExpression("select count(*) from resources"));

        try {
            CreateStoredQueryResponse createResponse = client.createStoredQuery(createRequest);
            System.out.println(createResponse.toString());
            RunQueryResponse runResponse = client.runQuery(runRequest);
            System.out.println(runResponse.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            System.out.println(e);
        }
    }
}
```


## 5.返回结果示例

```
class CreateStoredQueryResponse {
        id: {id}
        name: {name}
        description: {description}
        expression: {expression}
        created: {created}
        updated: {updated}
}

class RunQueryResponse {
        _col0: {count}
}
```

## 6.修订记录
| 发布日期       | 文档版本  | 修订说明  |
|------------| ------------ | ------------ |
| 2023-08-18 | 1.0  | 文档首次发布  |
