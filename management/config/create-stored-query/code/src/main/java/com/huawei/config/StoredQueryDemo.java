package com.huawei.config;

import com.huaweicloud.sdk.config.v1.ConfigClient;
import com.huaweicloud.sdk.config.v1.model.CreateStoredQueryRequest;
import com.huaweicloud.sdk.config.v1.model.CreateStoredQueryResponse;
import com.huaweicloud.sdk.config.v1.model.QueryRunRequestBody;
import com.huaweicloud.sdk.config.v1.model.RunQueryRequest;
import com.huaweicloud.sdk.config.v1.model.RunQueryResponse;
import com.huaweicloud.sdk.config.v1.model.StoredQueryRequestBody;
import com.huaweicloud.sdk.config.v1.region.ConfigRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

public class StoredQueryDemo {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "<region id>";

        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        ConfigClient client = ConfigClient.newBuilder()
            .withHttpConfig(config)
            .withCredential(auth)
            .withRegion(ConfigRegion.valueOf(regionId))
            .build();

        CreateStoredQueryRequest createRequest = new CreateStoredQueryRequest()
            .withBody(new StoredQueryRequestBody()
                .withName("config_test")
                .withDescription("description_test")
                .withExpression("select count(*) from resources"));

        RunQueryRequest runRequest = new RunQueryRequest()
            .withBody(new QueryRunRequestBody()
                .withExpression("select count(*) from resources"));

        try {
            CreateStoredQueryResponse createResponse = client.createStoredQuery(createRequest);
            System.out.println(createResponse.toString());
            RunQueryResponse runResponse = client.runQuery(runRequest);
            System.out.println(runResponse.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            System.out.println(e);
        }
    }
}