## 1.介绍
资源治理中心（Resource Governance Center，简称RGC）,对标AWS Control Tower，按照规范性最佳实践提供一种简单的方案发设置和管理多账户环境，用户可以通过RGC服务创建一个Landing Zone，包含一个管理账号和多个成员账号，同时对这些账号进行各种自动化的护栏配置或者资源创建，帮助客户方便快速安全上云。

[高级查询](https://support.huaweicloud.com/usermanual-rms/rms_10_0100.html)支持用户自定义查询和浏览华为云云服务资源，对当前资源数据执行基于属性的查询和聚合。

## 2.前置条件
1. 获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
2. 您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 访问密钥 。
3. 华为云 Java SDK 支持 Java JDK 1.8 及其以上版本。

## 3.关键代码片段
```java
public class Main {
    public static void main(String[] args) {
            ICredential auth = new GlobalCredentials()
                .withAk(AK)
                .withSk(SK);
            RgcClient client = RgcClient.newBuilder()
                .withCredential(auth)
                .withRegion(RgcRegion.valueOf(REGION_ID))
                .build();
            ShowAvailableUpdatesRequest showAvailableUpdatesRequest = new ShowAvailableUpdatesRequest();
            try {
                ShowAvailableUpdatesResponse showAvailableUpdatesResponse = client.showAvailableUpdates(
                    showAvailableUpdatesRequest);
                LOGGER.info("showAvailableUpdates response: {}", showAvailableUpdatesResponse.toString());
            } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
                LOGGER.error("showAvailableUpdates response occur error.", e);
        }
    }
}
```


## 4.返回结果示例

```
class ShowAvailableUpdatesResponse {
        baseline_update_available: {baseline_update_available}
        control_update_available: {control_update_available}
        landing_zone_update_available: {landing_zone_update_available}
        service_landing_zone_version: {service_landing_zone_version}
        user_landing_zone_version: {user_landing_zone_version}
}
```

## 5.修订记录
| 发布日期       | 文档版本  | 修订说明  |
|------------| ------------ | ------------ |
| 2023-11-30 | 1.0  | 文档首次发布  |
