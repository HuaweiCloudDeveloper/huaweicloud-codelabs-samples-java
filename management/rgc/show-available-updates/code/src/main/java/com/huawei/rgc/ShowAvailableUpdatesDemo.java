package com.huawei.rgc;

import com.huaweicloud.sdk.rgc.v1.model.ShowAvailableUpdatesRequest;
import com.huaweicloud.sdk.rgc.v1.model.ShowAvailableUpdatesResponse;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.rgc.v1.RgcClient;
import com.huaweicloud.sdk.rgc.v1.region.RgcRegion;

public class ShowAvailableUpdatesDemo {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String regionId = "<region id>";
        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);
        RgcClient client = RgcClient.newBuilder()
            .withCredential(auth)
            .withRegion(RgcRegion.valueOf(regionId))
            .build();
        ShowAvailableUpdatesRequest showAvailableUpdatesRequest = new ShowAvailableUpdatesRequest();
        try {
            ShowAvailableUpdatesResponse showAvailableUpdatesResponse = client.showAvailableUpdates(
                showAvailableUpdatesRequest);
            System.out.println(showAvailableUpdatesResponse.toString());
        } catch (ConnectionException | RequestTimeoutException | ServiceResponseException e) {
            System.out.println(e);
        }
    }
}