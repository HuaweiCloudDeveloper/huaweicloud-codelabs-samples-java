package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.ram.v1.RamClient;
import com.huaweicloud.sdk.ram.v1.model.*;
import com.huaweicloud.sdk.ram.v1.region.RamRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class RamTagDemo {
    private static final Logger logger = LoggerFactory.getLogger(RamTagDemo.class.getName());
    public static void main(String[] args) {
        // Hard-coded or plaintext AK and SK are insecure. So, encrypt your AK and SK and store them in the configuration file or environment variables.
        // In this example, the AK and SK are stored in environment variables. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials().withAk(ak).withSk(sk);

        RamClient ramClient = RamClient.newBuilder()
            .withCredential(auth)
            .withRegion(RamRegion.valueOf("cn-north-4"))
            .build();

        RamTagDemo demo = new RamTagDemo();

        // 1. Create resource sharing.
        CreateResourceShareResponse createResourceShareResponse = demo.createResourceShare(ramClient);
        // 2. Add a tag to a specified resource.
        demo.batchCreateResourceShareTags(ramClient, createResourceShareResponse.getResourceShare().getId());
        // 3. Query the list of used tags.
        demo.listResourceShareTags(ramClient);
        // 4. Delete the tag of the resource sharing instance.
        demo.batchDeleteResourceShareTags(ramClient, createResourceShareResponse.getResourceShare().getId());
        // 5. Delete the resource sharing instance.
        demo.deleteResourceShare(ramClient, createResourceShareResponse.getResourceShare().getId());
    }

    public CreateResourceShareResponse createResourceShare(RamClient ramClient) {
        List<String> tagPrincipals = new ArrayList<>();
        tagPrincipals.add("your principals");  // List of one or more resource consumers associated with the resource sharing instance
        List<String> tagResourceUrns = new ArrayList<>();
        tagResourceUrns.add("your urns");  // List of one or more shared resource URNs associated with a resource sharing instance
        List<String> tagPermissionIds = new ArrayList<>();
        tagPermissionIds.add("your permission");  // RAM permission list associated with the resource sharing instance
        CreateResourceShareReqBody body = new CreateResourceShareReqBody();
        body.withResourceUrns(tagResourceUrns)
            .withPrincipals(tagPrincipals)
            .withPermissionIds(tagPermissionIds)
            .withDescription("your description")  // Description of a resource sharing instance
            .withName("your share name");  // Name of a resource sharing instance
        CreateResourceShareRequest request = new CreateResourceShareRequest().withBody(body);
        CreateResourceShareResponse tagResponse = null;
        try {
            tagResponse = ramClient.createResourceShare(request);
            logger.info(tagResponse.toString());
        } catch (ClientRequestException e) {
            tagLogError(e);
        }
        return tagResponse;
    }

    public void batchCreateResourceShareTags(RamClient ramClient, String id) {
        if (id == null || id.length() == 0) {
            return;
        }
        BatchCreateResourceShareTagsRequest request = new BatchCreateResourceShareTagsRequest();
        request.withResourceShareId(id);  // id:id of a resource sharing instance
        TagResourceReqBody body = new TagResourceReqBody();
        List<Tag> Tags = new ArrayList<>();
        Tags.add(
            new Tag()
                .withKey("your tag key")
                .withValue("your tag value")
        );
        body.withTags(Tags);
        request.withBody(body);
        try {
            BatchCreateResourceShareTagsResponse tagResponse = ramClient.batchCreateResourceShareTags(request);
            logger.info(tagResponse.toString());
        } catch (ClientRequestException e) {
            tagLogError(e);
        }
    }

    public void listResourceShareTags(RamClient ramClient) {
        ListResourceShareTagsRequest request = new ListResourceShareTagsRequest();
        try {
            ListResourceShareTagsResponse tagResponse = ramClient.listResourceShareTags(request);
            logger.info(tagResponse.toString());
        } catch (ClientRequestException e) {
            tagLogError(e);
        }
    }

    public void batchDeleteResourceShareTags(RamClient ramClient, String id) {
        if (id == null || id.length() == 0) {
            return;
        }
        BatchDeleteResourceShareTagsRequest request = new BatchDeleteResourceShareTagsRequest();
        request.withResourceShareId(id);  // id:id of a resource sharing instance
        UntagResourceReqBody body = new UntagResourceReqBody();
        request.withBody(body);
        try {
            BatchDeleteResourceShareTagsResponse tagResponse = ramClient.batchDeleteResourceShareTags(request);
            logger.info(tagResponse.toString());
        } catch (ClientRequestException e) {
            tagLogError(e);
        }
    }

    public void deleteResourceShare(RamClient ramClient, String id) {
        if (id == null || id.length() == 0) {
            return;
        }
        DeleteResourceShareRequest request = new DeleteResourceShareRequest()
            .withResourceShareId(id);  // id:id of a resource sharing instance
        try {
            DeleteResourceShareResponse tagResponse = ramClient.deleteResourceShare(request);
            logger.info(tagResponse.toString());
        } catch (ClientRequestException e) {
            tagLogError(e);
        }
    }

    private static void tagLogError(ClientRequestException e) {
        logger.error("tagHttpStatusCode: " + e.getHttpStatusCode());
        logger.error("tagRequestId: " + e.getRequestId());
        logger.error("tagErrorCode: " + e.getErrorCode());
        logger.error("tagErrorMsg: " + e.getErrorMsg());
    }
}
