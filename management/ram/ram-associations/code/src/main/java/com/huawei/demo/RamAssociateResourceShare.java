package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.ram.v1.RamClient;
import com.huaweicloud.sdk.ram.v1.model.*;
import com.huaweicloud.sdk.ram.v1.region.RamRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class RamAssociateResourceShare {
    private static final Logger logger = LoggerFactory.getLogger(RamAssociateResourceShare.class.getName());
    public static void main(String[] args) {
        // Hard-coded or plaintext AK and SK are insecure. So, encrypt your AK and SK and store them in the configuration file or environment variables.
        // In this example, the AK and SK are stored in environment variables. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials().withAk(ak).withSk(sk);

        RamClient ramClient = RamClient.newBuilder()
            .withCredential(auth)
            .withRegion(RamRegion.valueOf("cn-north-4"))
            .build();

        RamAssociateResourceShare demo = new RamAssociateResourceShare();

        // 1. Bind resource users and shared resources.
        demo.associateResourceShare(ramClient);
        // 2. Remove resource consumers and shared resources
        demo.disassociateResourceShare(ramClient);
        // 3. Retrieve bound resource consumers and shared resources
        demo.searchResourceShareAssociations(ramClient);
    }

    public void associateResourceShare(RamClient ramClient) {
        AssociateResourceShareRequest request = new AssociateResourceShareRequest()
            .withResourceShareId("your resource share id");  // ID of a resource sharing instance
        List<String> resourceUrns = new ArrayList<>();
        resourceUrns.add("your principals");  // List of one or more resource consumers associated with the resource sharing instance
        List<String> principals = new ArrayList<>();
        principals.add("your resource urns");  // A list of one or more shared resource URNs associated with a resource sharing instance
        ResourceShareAssociationReqBody body = new ResourceShareAssociationReqBody()
            .withResourceUrns(resourceUrns)
            .withPrincipals(principals);
        request.withBody(body);
        try {
            AssociateResourceShareResponse response = ramClient.associateResourceShare(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void disassociateResourceShare(RamClient ramClient) {
        DisassociateResourceShareRequest request = new DisassociateResourceShareRequest()
            .withResourceShareId("your resource share id");  // ID of a resource sharing instance
        List<String> principals = new ArrayList<>();
        principals.add("your resource urns");  // List of one or more shared resource URNs associated with a resource sharing instance
        List<String> resourceUrns = new ArrayList<>();
        resourceUrns.add("your principals");  // List of one or more resource consumers associated with the resource sharing instance
        ResourceShareAssociationReqBody body = new ResourceShareAssociationReqBody()
            .withResourceUrns(resourceUrns)
            .withPrincipals(principals);
        request.withBody(body);
        try {
            DisassociateResourceShareResponse response = ramClient.disassociateResourceShare(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void searchResourceShareAssociations(RamClient ramClient) {
        SearchResourceShareAssociationsRequest request = new SearchResourceShareAssociationsRequest()
            .withBody(new SearchResourceShareAssociationsReqBody()
                .withAssociationType(SearchResourceShareAssociationsReqBody
                    .AssociationTypeEnum.fromValue("principal or resource")));  // Specifies the type of binding (principal or resource)
        try {
            SearchResourceShareAssociationsResponse response = ramClient.searchResourceShareAssociations(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        logger.error("RequestId: " + e.getRequestId());
        logger.error("ErrorCode: " + e.getErrorCode());
        logger.error("ErrorMsg: " + e.getErrorMsg());
    }
}
