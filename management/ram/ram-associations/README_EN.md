### Release Notes
In this example, the SDK version is 3.1.35 or later.

### Introduction to the Sample
This example shows how to use the SDK related to Resource Access Manager.

### Introduction
Resource access management services bind resource consumers and shared resource operations

### Preparations
1.You have [registered](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) with HUAWEI CLOUD,and completed [real-name authentication](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth).

2.Obtain the HUAWEI CLOUD SDK. For details, see Installing the JAVA SDK.

3.You have obtained the access key (AK) and secret access key (SK) of your HUAWEI CLOUD account. Create and view your AK/SK on the Access Keys page of the HUAWEI CLOUD management console.For details,see [Access Keys](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html).

4.The development environment is available. Java JDK 1.8 or later is supported.


### Obtaining and Installing the SDK
You can configure the resources that Maven depends on to access the management service SDK.

For details about the SDK version, see the [SDK Development Center](https://sdkcenter.developer.huaweicloud.com?language=java)  (Product Category: Resource Access Management)

### Key Code
The following code shows how to use the SDK related to Resource Access Manager
``` java
package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.ram.v1.RamClient;
import com.huaweicloud.sdk.ram.v1.model.*;
import com.huaweicloud.sdk.ram.v1.region.RamRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class RamAssociateResourceShare {
    private static final Logger logger = LoggerFactory.getLogger(RamAssociateResourceShare.class.getName());
    public static void main(String[] args) {
        // Hard-coded or plaintext AK and SK are insecure. So, encrypt your AK and SK and store them in the configuration file or environment variables.
        // In this example, the AK and SK are stored in environment variables. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials().withAk(ak).withSk(sk);

        RamClient ramClient = RamClient.newBuilder()
            .withCredential(auth)
            .withRegion(RamRegion.valueOf("cn-north-4"))
            .build();

        RamAssociateResourceShare demo = new RamAssociateResourceShare();

        // 1. Bind resource users and shared resources.
        demo.associateResourceShare(ramClient);
        // 2. Remove resource consumers and shared resources
        demo.disassociateResourceShare(ramClient);
        // 3. Retrieve bound resource consumers and shared resources
        demo.searchResourceShareAssociations(ramClient);
    }

    public void associateResourceShare(RamClient ramClient) {
        AssociateResourceShareRequest request = new AssociateResourceShareRequest()
            .withResourceShareId("your resource share id");  // ID of a resource sharing instance
        List<String> resourceUrns = new ArrayList<>();
        resourceUrns.add("your principals");  // List of one or more resource consumers associated with the resource sharing instance
        List<String> principals = new ArrayList<>();
        principals.add("your resource urns");  // A list of one or more shared resource URNs associated with a resource sharing instance
        ResourceShareAssociationReqBody body = new ResourceShareAssociationReqBody()
            .withResourceUrns(resourceUrns)
            .withPrincipals(principals);
        request.withBody(body);
        try {
            AssociateResourceShareResponse response = ramClient.associateResourceShare(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void disassociateResourceShare(RamClient ramClient) {
        DisassociateResourceShareRequest request = new DisassociateResourceShareRequest()
            .withResourceShareId("your resource share id");  // ID of a resource sharing instance
        List<String> principals = new ArrayList<>();
        principals.add("your resource urns");  // List of one or more shared resource URNs associated with a resource sharing instance
        List<String> resourceUrns = new ArrayList<>();
        resourceUrns.add("your principals");  // List of one or more resource consumers associated with the resource sharing instance
        ResourceShareAssociationReqBody body = new ResourceShareAssociationReqBody()
            .withResourceUrns(resourceUrns)
            .withPrincipals(principals);
        request.withBody(body);
        try {
            DisassociateResourceShareResponse response = ramClient.disassociateResourceShare(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void searchResourceShareAssociations(RamClient ramClient) {
        SearchResourceShareAssociationsRequest request = new SearchResourceShareAssociationsRequest()
            .withBody(new SearchResourceShareAssociationsReqBody()
                .withAssociationType(SearchResourceShareAssociationsReqBody
                    .AssociationTypeEnum.fromValue("principal or resource")));  // Specifies the type of binding (principal or resource)
        try {
            SearchResourceShareAssociationsResponse response = ramClient.searchResourceShareAssociations(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        logger.error("RequestId: " + e.getRequestId());
        logger.error("ErrorCode: " + e.getErrorCode());
        logger.error("ErrorMsg: " + e.getErrorMsg());
    }
}


```
You can find more information in the [Resource Access Management RAM Service document](https://support.huaweicloud.com/productdesc-ram/ram_01_0001.html) and in [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RAM/doc?api=AssociateResourceShare).

### Change History

Releast Date  | Version | Description
 :----: | :-----: | :------:  
2023/4/10 |1.0 | This issue is the first official release


