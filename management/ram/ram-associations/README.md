### 版本说明
本示例配套的SDK版本为：3.1.35及以上版本

### 示例简介
本示例展示如何使用资源访问管理（Resource Access Manager）相关SDK

### 功能介绍
资源访问管理服务绑定资源使用者和共享资源操作

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。


### SDK获取和安装
您可以通过Maven配置所依赖的资源访问管理服务SDK

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java)  (产品类别：资源访问管理)

### 代码示例
以下代码展示如何使用资源访问管理（Resource Access Manager）相关SDK
``` java
package com.huawei.demo;

// 用户身份认证
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
// 请求异常类
import com.huaweicloud.sdk.core.exception.ClientRequestException;
// 导入待请求接口的 request 和 response 类
import com.huaweicloud.sdk.ram.v1.region.RamRegion;
import com.huaweicloud.sdk.ram.v1.*;
import com.huaweicloud.sdk.ram.v1.model.*;
// 日志打印
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class RamAssociateResourceShare {
    private static final Logger logger = LoggerFactory.getLogger(RamAssociateResourceShare.class.getName());
    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials().withAk(ak).withSk(sk);

        RamClient ramClient = RamClient.newBuilder()
            .withCredential(auth)
            .withRegion(RamRegion.valueOf("cn-north-4"))
            .build();

        RamAssociateResourceShare demo = new RamAssociateResourceShare();

        // 1、绑定资源使用者和共享资源
        demo.associateResourceShare(ramClient);
        // 2、移除资源使用者和共享资源
        demo.disassociateResourceShare(ramClient);
        // 3、检索绑定的资源使用者和共享资源
        demo.searchResourceShareAssociations(ramClient);
    }

    public void associateResourceShare(RamClient ramClient) {
        AssociateResourceShareRequest request = new AssociateResourceShareRequest()
            .withResourceShareId("your resource share id");  //资源共享实例的ID
        List<String> resourceUrns = new ArrayList<>();
        resourceUrns.add("your principals");  //与资源共享实例关联的一个或多个资源使用者的列表
        List<String> principals = new ArrayList<>();
        principals.add("your resource urns");  //与资源共享实例关联的一个或多个共享资源URN的列表
        ResourceShareAssociationReqBody body = new ResourceShareAssociationReqBody()
            .withResourceUrns(resourceUrns)
            .withPrincipals(principals);
        request.withBody(body);
        try {
            AssociateResourceShareResponse response = ramClient.associateResourceShare(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void disassociateResourceShare(RamClient ramClient) {
        DisassociateResourceShareRequest request = new DisassociateResourceShareRequest()
            .withResourceShareId("your resource share id");  //资源共享实例的ID
        List<String> principals = new ArrayList<>();
        principals.add("your resource urns");  //与资源共享实例关联的一个或多个共享资源URN的列表
        List<String> resourceUrns = new ArrayList<>();
        resourceUrns.add("your principals");  //与资源共享实例关联的一个或多个资源使用者的列表
        ResourceShareAssociationReqBody body = new ResourceShareAssociationReqBody()
            .withResourceUrns(resourceUrns)
            .withPrincipals(principals);
        request.withBody(body);
        try {
            DisassociateResourceShareResponse response = ramClient.disassociateResourceShare(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void searchResourceShareAssociations(RamClient ramClient) {
        SearchResourceShareAssociationsRequest request = new SearchResourceShareAssociationsRequest()
            .withBody(new SearchResourceShareAssociationsReqBody()
                .withAssociationType(SearchResourceShareAssociationsReqBody
                    .AssociationTypeEnum.fromValue("principal or resource")));  //指定绑定的类型（principal或resource）
        try {
            SearchResourceShareAssociationsResponse response = ramClient.searchResourceShareAssociations(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        logger.error("RequestId: " + e.getRequestId());
        logger.error("ErrorCode: " + e.getErrorCode());
        logger.error("ErrorMsg: " + e.getErrorMsg());
    }
}

```
您可以在 [资源访问管理RAM服务文档](https://support.huaweicloud.com/productdesc-ram/ram_01_0001.html) 和[API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RAM/doc?api=AssociateResourceShare) 查看具体信息。

### 修订记录

发布日期  | 文档版本 | 修订说明
 :----: | :-----: | :------:  
2023/4/10 |1.0 | 文档首次发布


