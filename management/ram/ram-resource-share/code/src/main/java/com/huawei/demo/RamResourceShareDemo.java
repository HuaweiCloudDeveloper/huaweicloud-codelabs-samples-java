package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.ram.v1.RamClient;
import com.huaweicloud.sdk.ram.v1.model.*;
import com.huaweicloud.sdk.ram.v1.region.RamRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class RamResourceShareDemo {
    private static final Logger logger = LoggerFactory.getLogger(RamResourceShareDemo.class.getName());
    public static void main(String[] args) {
        // Hard-coded or plaintext AK and SK are insecure. So, encrypt your AK and SK and store them in the configuration file or environment variables.
        // In this example, the AK and SK are stored in environment variables. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials().withAk(ak).withSk(sk);

        RamClient ramClient = RamClient.newBuilder()
            .withCredential(auth)
            .withRegion(RamRegion.valueOf("cn-north-4"))
            .build();

        RamResourceShareDemo demo = new RamResourceShareDemo();

        // 1. Create a resource sharing instance.
        CreateResourceShareResponse createResourceShareResponse = demo.createResourceShare(ramClient);
        // 2. Update the resource sharing instance.
        demo.updateResourceShare(ramClient, createResourceShareResponse.getResourceShare().getId());
        // 3. Delete the resource sharing instance.
        demo.deleteResourceShare(ramClient, createResourceShareResponse.getResourceShare().getId());
    }

    public CreateResourceShareResponse createResourceShare(RamClient ramClient) {
        List<String> shareResourceUrns = new ArrayList<>();
        shareResourceUrns.add("your urns");  // List of one or more shared resource URNs associated with a resource sharing instance
        List<String> sharePrincipals = new ArrayList<>();
        sharePrincipals.add("your principals");  // List of one or more resource consumers associated with the resource sharing instance
        List<String> sharePermissionIds = new ArrayList<>();
        sharePermissionIds.add("your permission");  // RAM permission list associated with the resource sharing instance
        CreateResourceShareReqBody body = new CreateResourceShareReqBody();
        body.withResourceUrns(shareResourceUrns)
            .withPrincipals(sharePrincipals)
            .withPermissionIds(sharePermissionIds)
            .withDescription("your description")  // Description of a resource sharing instance
            .withName("your share name");  // Name of a resource sharing instance
        CreateResourceShareRequest request = new CreateResourceShareRequest();
        request.withBody(body);
        CreateResourceShareResponse shareResponse = null;
        try {
            shareResponse = ramClient.createResourceShare(request);
            logger.info(shareResponse.toString());
        } catch (ClientRequestException e) {
            shareLogError(e);
        }
        return shareResponse;
    }

    public void updateResourceShare(RamClient ramClient, String id) {
        if (id == null || id.length() == 0) {
            return;
        }
        UpdateResourceShareRequest request = new UpdateResourceShareRequest()
            .withBody(new UpdateResourceShareReqBody().withName("name"))  // name:name of a resource sharing instance
            .withResourceShareId(id);  // id:id of a resource sharing instance
        try {
            UpdateResourceShareResponse shareResponse = ramClient.updateResourceShare(request);
            logger.info(shareResponse.toString());
        } catch (ClientRequestException e) {
            shareLogError(e);
        }
    }

    public void deleteResourceShare(RamClient ramClient, String id) {
        if (id == null || id.length() == 0) {
            return;
        }
        DeleteResourceShareRequest request = new DeleteResourceShareRequest()
            .withResourceShareId(id);  // id:id of a resource sharing instance
        try {
            DeleteResourceShareResponse shareResponse = ramClient.deleteResourceShare(request);
            logger.info(shareResponse.toString());
        } catch (ClientRequestException e) {
            shareLogError(e);
        }
    }

    private static void shareLogError(ClientRequestException e) {
        logger.error("shareHttpStatusCode: " + e.getHttpStatusCode());
        logger.error("shareRequestId: " + e.getRequestId());
        logger.error("shareErrorCode: " + e.getErrorCode());
        logger.error("shareErrorMsg: " + e.getErrorMsg());
    }
}
