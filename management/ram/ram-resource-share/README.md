### 版本说明
本示例配套的SDK版本为：3.1.35及以上版本

### 示例简介
本示例展示如何使用资源访问管理（Resource Access Manager）相关SDK

### 功能介绍
资源访问管理服务资源共享基本操作

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。


### SDK获取和安装
您可以通过Maven配置所依赖的资源访问管理服务SDK

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java)  (产品类别：资源访问管理)

### 代码示例
以下代码展示如何使用资源访问管理（Resource Access Manager）相关SDK
``` java
package com.huawei.demo;

// 用户身份认证
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
// 请求异常类
import com.huaweicloud.sdk.core.exception.ClientRequestException;
// 导入待请求接口的 request 和 response 类
import com.huaweicloud.sdk.ram.v1.region.RamRegion;
import com.huaweicloud.sdk.ram.v1.*;
import com.huaweicloud.sdk.ram.v1.model.*;
// 日志打印
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class RamResourceShareDemo {
    private static final Logger logger = LoggerFactory.getLogger(RamResourceShareDemo.class.getName());
    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials().withAk(ak).withSk(sk);

        RamClient ramClient = RamClient.newBuilder()
            .withCredential(auth)
            .withRegion(RamRegion.valueOf("cn-north-4"))
            .build();

        RamResourceShareDemo demo = new RamResourceShareDemo();

        // 1、创建资源共享实例
        CreateResourceShareResponse createResourceShareResponse = demo.createResourceShare(ramClient);
        // 2、更新资源共享实例
        demo.updateResourceShare(ramClient, createResourceShareResponse.getResourceShare().getId());
        // 3、删除资源共享实例
        demo.deleteResourceShare(ramClient, createResourceShareResponse.getResourceShare().getId());
    }

    public CreateResourceShareResponse createResourceShare(RamClient ramClient) {
        List<String> shareResourceUrns = new ArrayList<>();
        shareResourceUrns.add("your urns");  //资源共享实例关联的一个或多个共享资源URN的列表
        List<String> sharePrincipals = new ArrayList<>();
        sharePrincipals.add("your principals");  //资源共享实例关联的一个或多个资源使用者的列表
        List<String> sharePermissionIds = new ArrayList<>();
        sharePermissionIds.add("your permission");  //资源共享实例关联的RAM权限列表
        CreateResourceShareReqBody body = new CreateResourceShareReqBody();
        body.withResourceUrns(shareResourceUrns)
            .withPrincipals(sharePrincipals)
            .withPermissionIds(sharePermissionIds)
            .withDescription("your description")  //资源共享实例的描述
            .withName("your share name");  //资源共享实例的名称
        CreateResourceShareRequest request = new CreateResourceShareRequest();
        request.withBody(body);
        CreateResourceShareResponse shareResponse = null;
        try {
            shareResponse = ramClient.createResourceShare(request);
            logger.info(shareResponse.toString());
        } catch (ClientRequestException e) {
            shareLogError(e);
        }
        return shareResponse;
    }

    public void updateResourceShare(RamClient ramClient, String id) {
        if (id == null || id.length() == 0) {
            return;
        }
        UpdateResourceShareRequest request = new UpdateResourceShareRequest()
            .withBody(new UpdateResourceShareReqBody().withName("name"))  //name:资源共享实例的名称
            .withResourceShareId(id);  //id:资源共享实例的ID
        try {
            UpdateResourceShareResponse shareResponse = ramClient.updateResourceShare(request);
            logger.info(shareResponse.toString());
        } catch (ClientRequestException e) {
            shareLogError(e);
        }
    }

    public void deleteResourceShare(RamClient ramClient, String id) {
        if (id == null || id.length() == 0) {
            return;
        }
        DeleteResourceShareRequest request = new DeleteResourceShareRequest()
            .withResourceShareId(id);  //id:资源共享实例的ID
        try {
            DeleteResourceShareResponse shareResponse = ramClient.deleteResourceShare(request);
            logger.info(shareResponse.toString());
        } catch (ClientRequestException e) {
            shareLogError(e);
        }
    }

    private static void shareLogError(ClientRequestException e) {
        logger.error("shareHttpStatusCode: " + e.getHttpStatusCode());
        logger.error("shareRequestId: " + e.getRequestId());
        logger.error("shareErrorCode: " + e.getErrorCode());
        logger.error("shareErrorMsg: " + e.getErrorMsg());
    }
}

```
您可以在 [资源访问管理RAM服务文档](https://support.huaweicloud.com/productdesc-ram/ram_01_0001.html) 和[API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/RAM/doc?api=CreateResourceShare) 查看具体信息。

### 修订记录

发布日期  | 文档版本 | 修订说明
 :----: | :-----: | :------:  
2023/4/10 |1.0 | 文档首次发布


