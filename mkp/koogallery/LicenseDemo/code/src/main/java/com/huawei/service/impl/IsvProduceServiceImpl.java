/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.service.impl;

import static com.huawei.constant.IsvProduceConstant.DEBUG_TEST;

import com.huawei.constant.Activity;
import com.huawei.constant.ChangeStatus;
import com.huawei.constant.RenewAction;
import com.huawei.model.IMessageResp;
import com.huawei.model.OrderInfoResp;
import com.huawei.service.ApigService;
import com.huawei.service.IsvProduceService;
import com.huawei.util.ResultCodeEnum;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import javax.annotation.Resource;

@Component
public class IsvProduceServiceImpl implements IsvProduceService {
    @Resource
    private ApigService apigService;

    /**
     * 使用Collections.unmodifiableMap修饰可变集合，保证在方法处理过程中不会对常量方法成员误删除、修改等操作
     */
    private final Map<String, Function<Map<String, String>, IMessageResp>> isvProduceConsumers =
        Collections.unmodifiableMap(initConsumers());

    /**
     * 根据活动类型调用对应的实现方法
     */
    private Map<String, Function<Map<String, String>, IMessageResp>> initConsumers() {
        Map<String, Function<Map<String, String>, IMessageResp>> consumers = new HashMap<>(5);
        consumers.put(Activity.NEW_INSTANCE, this::newInstance);
        consumers.put(Activity.REFRESH_INSTANCE, this::refreshInstance);
        consumers.put(Activity.LICENSE_ASYNC_INSTANCE, this::getLicenseAsync);
        consumers.put(Activity.UPDATE_INSTANCE_STATUS, this::updateInstanceStatus);
        consumers.put(Activity.RELEASE_INSTANCE, this::releaseInstance);
        return consumers;
    }

    @Override
    public IMessageResp processProduceReq(Map<String, String> isvProduceReq) {
        return isvProduceConsumers.get(isvProduceReq.get("activity")).apply(isvProduceReq);
    }

    /**
     * 接收更新实例消息
     *
     * @param isvProduceReq 入参
     * @return 响应结果
     */
    private IMessageResp refreshInstance(Map<String, String> isvProduceReq) {
        IMessageResp resp = getSuccessResp();

        // 入参可自添加必填项校验

        // 校验实例id不能为空
        if (StringUtils.isEmpty(isvProduceReq.get("instanceId"))) {
            resp.setResultCode(ResultCodeEnum.INVALID_INSTANCE.getResultCode());
            resp.setResultMsg(ResultCodeEnum.INVALID_INSTANCE.getResultMsg());
            return resp;
        }

        // 调测数据不做业务处理
        if (DEBUG_TEST.equals(isvProduceReq.get("testFlag"))) {
            return resp;
        }

        // TODO 根据instanceId到数据库查到详情信息，然后根据操作类型做对应的业务处理

        switch (isvProduceReq.get("scene")) {
            case RenewAction.SUB:
                // TRIAL_TO_FORMAL：试用转正

                break;
            case RenewAction.RENEW:
                // RENEWAL：续费

                break;
            case RenewAction.UNSUB:
                // UNSUBSCRIBE_RENEWAL_PERIOD：退续费

                break;
            default:
                break;
        }
        return resp;
    }

    /**
     * 更新实例状态消息
     *
     * @param isvProduceReq 请求入参
     * @return 更新实例处理结果
     */
    private IMessageResp updateInstanceStatus(Map<String, String> isvProduceReq) {
        IMessageResp resp = getSuccessResp();

        // 可自添加必填项校验

        // 校验实例id不能为空
        if (StringUtils.isEmpty(isvProduceReq.get("instanceId"))) {
            resp.setResultCode(ResultCodeEnum.INVALID_INSTANCE.getResultCode());
            resp.setResultMsg(ResultCodeEnum.INVALID_INSTANCE.getResultMsg());
            return resp;
        }

        // 调测数据不做业务处理
        if (DEBUG_TEST.equals(isvProduceReq.get("testFlag"))) {
            return resp;
        }

        // TODO 根据instanceId到数据库查到详情信息，然后根据状态做对应的业务处理

        switch (isvProduceReq.get("status")) {
            case ChangeStatus.FREEZE:
                // 冻结

                break;
            case ChangeStatus.UNFREEZE:
                // 解冻

                break;
            default:
                break;
        }
        return resp;
    }

    /**
     * 接收释放消息
     *
     * @param isvProduceReq 请求入参
     * @return 释放操作结果
     */
    private IMessageResp releaseInstance(Map<String, String> isvProduceReq) {
        IMessageResp resp = getSuccessResp();

        // 可自添加必填项校验

        // 校验实例id不能为空
        if (StringUtils.isEmpty(isvProduceReq.get("instanceId"))) {
            resp.setResultCode(ResultCodeEnum.INVALID_INSTANCE.getResultCode());
            resp.setResultMsg(ResultCodeEnum.INVALID_INSTANCE.getResultMsg());
            return resp;
        }

        // 调测数据不做业务处理
        if (DEBUG_TEST.equals(isvProduceReq.get("testFlag"))) {
            return resp;
        }

        // TODO 根据instanceId到数据库查到详情信息，然后实现资源释放逻辑

        return resp;
    }

    /**
     * 异步申请License
     *
     * @param isvProduceReq 请求入参
     * @return 申请操作结果
     */
    private IMessageResp getLicenseAsync(Map<String, String> isvProduceReq) {
        IMessageResp resp = getSuccessResp();

        // 可自添加必填项校验

        // 校验实例id不能为空
        if (StringUtils.isEmpty(isvProduceReq.get("instanceId"))) {
            resp.setResultCode(ResultCodeEnum.INVALID_INSTANCE.getResultCode());
            resp.setResultMsg(ResultCodeEnum.INVALID_INSTANCE.getResultMsg());
            return resp;
        }

        // 调测数据不做业务处理
        if (DEBUG_TEST.equals(isvProduceReq.get("testFlag"))) {
            return resp;
        }

        // TODO 需要实现申请信息入库的逻辑，并且异步向华为云市场发送License

        return resp;
    }

    /**
     * 创建实例
     *
     * @param isvProduceReq 请求入参
     * @return 操作结果
     */
    private IMessageResp newInstance(Map<String, String> isvProduceReq) {
        IMessageResp resp = getSuccessResp();

        // 调测数据不做业务处理
        if (DEBUG_TEST.equals(isvProduceReq.get("testFlag"))) {
            resp.setInstanceId(isvProduceReq.get("businessId"));
            return resp;
        }

        // TODO 需要实现一个简单的本地库，保证实现幂等：根据订单号和订单行查询instanceId，如果能查到则直接返回
        // 避免创建实例接口本地维护成功但因为超时等原因云商店接收响应失败后重新同步该订单数据后本地又维护了一条新的实例
        String instanceId = queryInstanceIdByOrderId(isvProduceReq.get("orderId"), isvProduceReq.get("orderLineId"));
        if (StringUtils.isNotBlank(instanceId)) {
            resp.setInstanceId(instanceId);
        } else {
            // 根据同步过来的请求参数本地做业务处理，instanceId也可以按照自身业务赋值，只要保证每个新购订单号都对应唯一的一个instanceId就好
        }
        return resp;
    }

    @NotNull
    private static IMessageResp getSuccessResp() {
        IMessageResp resp = new IMessageResp();
        resp.setResultCode(ResultCodeEnum.SUCCESS.getResultCode());
        resp.setResultMsg(ResultCodeEnum.SUCCESS.getResultMsg());
        return resp;
    }

    /**
     * 根据订单号查找本地是否已有对应的实例id 如果有则返回实例id
     *
     * @param orderId 订单id
     * @param orderLineId 订单行id
     * @return 实例id
     */
    private String queryInstanceIdByOrderId(String orderId, String orderLineId) {
        return "";
    }

    @Override
    public void syncLicense() {
        // TODO 需要实现：从数据库中获取相关参数
        String orderId = "xxx";
        String orderLineId = "xxx";
        String eventId = "xxx";
        // 获取订单信息
        String orderInfoResp = apigService.queryOrder(orderId, orderLineId);
        OrderInfoResp resp;
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            resp = objectMapper.readValue(orderInfoResp, OrderInfoResp.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        // TODO 需要实现：根据订单信息，生成license的业务逻辑

        // 上传license
        apigService.syncLicense(eventId);
    }
}
