
package com.huawei.service.impl;

import com.huawei.service.ApigService;
import com.huawei.util.Constant;
import com.huawei.util.SSLCipherSuiteUtil;

import com.cloud.apigateway.sdk.utils.Client;
import com.cloud.apigateway.sdk.utils.Request;

import lombok.SneakyThrows;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Component
public class ApigServiceImpl implements ApigService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApigServiceImpl.class);

    @Value("${syncLicense_url}")
    private String syncLicenseUrl;

    @Value("${queryOrder_url}")
    private String queryOrderUrl;

    @Value("${ak}")
    private String AK;

    @Value("${sk}")
    private String SK;

    @SneakyThrows
    public void syncLicense(String eventId) {
        Request request = new Request();
        CloseableHttpClient client = null;
        try {
            // Set the request parameters.
            // AppKey, AppSecrect, Method and Url are required parameters.
            // key和secret的值获取见文档5.1.2生成ak、sk，key对应ak（访问密钥id），secret对应sk（下载的密钥文件内容）
            request.setKey(AK);
            request.setSecret(SK);
            request.setMethod("POST");
            request.setUrl(syncLicenseUrl.replace("{event_id}", eventId));
            request.addHeader("x-sdk-content-sha256", "UNSIGNED-PAYLOAD");
            String boundary = UUID.randomUUID().toString().replace("-", "");
            request.addHeader("Content-Type", "multipart/form-data" + "; boundary=" + boundary);
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            // 替换为具体的文件地址
            File file = new File("D:\\xxx\\xxx\\try.lic");
            builder.addPart("file", new FileBody(file, ContentType.DEFAULT_BINARY));
            // 通过转换生成的body注意检查key和value要带上""号
            String body =
                "{\"type\":\"******\",\"sha256\":\"******\",\"characters\":\"******\",\"fileName\":\"******\"}";
            builder.addTextBody("isvUpdateFileReq", body, ContentType.APPLICATION_JSON);
            builder.setBoundary(boundary);
            // Build the HttpEntity
            HttpEntity multipartEntity = builder.build();
            request.setEntity(multipartEntity);

            // Sign the request.
            HttpRequestBase signedRequest = Client.sign(request, Constant.SIGNATURE_ALGORITHM_SDK_HMAC_SHA256);
            URIBuilder uriBuilder = new URIBuilder("https://" + request.getHost() + request.getPath());

            Iterator<Map.Entry<String, List<String>>> iterator = request.getQueryStringParams().entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, List<String>> entry = iterator.next();
                String key = entry.getKey();
                List<String> value = entry.getValue();
                uriBuilder.setParameter(key, value.get(0));
            }

            HttpPost httpPost = new HttpPost(uriBuilder.build());
            httpPost.setEntity(request.getEntity());
            for (Header header : signedRequest.getAllHeaders()) {
                httpPost.addHeader(header);
            }

            // Do not verify ssl certificate
            client = (CloseableHttpClient) SSLCipherSuiteUtil.createHttpClient(Constant.INTERNATIONAL_PROTOCOL);
            HttpResponse response = client.execute(httpPost);
            // Print the body of the response.
            HttpEntity resEntity = response.getEntity();
            if (resEntity != null) {
                LOGGER.info("Processing Body with name: {} and value: {}", System.getProperty("line.separator"),
                    EntityUtils.toString(resEntity, "UTF-8"));
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        } finally {
            if (client != null) {
                client.close();
            }
        }

    }

    @SneakyThrows
    public String queryOrder(String orderId, String orderLineId) {
        Request request = new Request();
        CloseableHttpClient client = null;
        try {
            // Set the request parameters.
            // AppKey, AppSecrect, Method and Url are required parameters.
            request.setKey(AK);
            request.setSecret(SK);
            request.setMethod("GET");
            String url = queryOrderUrl + "?" + "orderId=" + orderId + "&orderLineId=" + orderLineId;
            request.setUrl(url);
            request.addHeader("Content-Type", "application/json");

            // Sign the request.
            HttpRequestBase signedRequest = Client.sign(request, Constant.SIGNATURE_ALGORITHM_SDK_HMAC_SHA256);
            URIBuilder uriBuilder = new URIBuilder("https://" + request.getHost() + request.getPath());

            Iterator<Map.Entry<String, List<String>>> iterator = request.getQueryStringParams().entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, List<String>> entry = iterator.next();
                String key = entry.getKey();
                List<String> value = entry.getValue();
                uriBuilder.setParameter(key, value.get(0));
            }

            HttpGet httpGet = new HttpGet(uriBuilder.build());
            for (Header header : signedRequest.getAllHeaders()) {
                httpGet.addHeader(header);
            }

            // Do not verify ssl certificate
            client = (CloseableHttpClient) SSLCipherSuiteUtil.createHttpClient(Constant.INTERNATIONAL_PROTOCOL);
            HttpResponse response = client.execute(httpGet);
            // Print the body of the response.
            HttpEntity resEntity = response.getEntity();
            if (resEntity != null) {
                String content = EntityUtils.toString(resEntity, "UTF-8");
                LOGGER.info("Processing Body with name: {} and value: {}", System.getProperty("line.separator"),
                    content);
                return content;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        } finally {
            if (client != null) {
                client.close();
            }
        }
        return null;
    }
}
