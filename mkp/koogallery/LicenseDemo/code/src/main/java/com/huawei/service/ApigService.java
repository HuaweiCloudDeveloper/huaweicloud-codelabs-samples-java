/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.service;

public interface ApigService {
    /**
     * 根据eventId上传License
     *
     * @param eventId 事件id
     */
    void syncLicense(String eventId);

    /**
     * 根据OrderId和OrderLineId查询订单信息
     *
     * @param orderId 订单Id
     * @param orderLineId 订单行Id
     * @return 订单详情响应
     */
    String queryOrder(String orderId, String orderLineId);
}
