/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 释放场景时云商品请求传递的参数，使用Map作为入参，该类仅作为参考
 * 具体定义参考：https://support.huaweicloud.com/accessg-marketplace/zh-cn_topic_0070649254.html
 */
@Getter
@Setter
public class ReleaseInstanceReq {
    /**
     * 接口请求标识，用于区分接口请求场景。
     */
    private String activity;

    /**
     * 云市场订单ID。
     * 续费操作会产生新的订单，与新购时订单ID不一致，请通过instance Id做资源识别。
     * 必返回
     */
    private String orderId;

    /**
     * 实例ID
     * 必返回
     */
    private String instanceId;

    /**
     * 产云商店订单行ID。
     * 必返回
     */
    private String orderLineId;

    /**
     * 是否为调试请求。
     * <p>
     * 1：调试请求
     * 0：非调试业务
     * 默认取值为“0”。
     * 非必返回
     */
    private String testFlag;
}
