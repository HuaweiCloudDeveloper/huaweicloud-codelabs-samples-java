/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IMessageResp {
    private String resultCode;

    private String resultMsg;

    private String instanceId;
}
