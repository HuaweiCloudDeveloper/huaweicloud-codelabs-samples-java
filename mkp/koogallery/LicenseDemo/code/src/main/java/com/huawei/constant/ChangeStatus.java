/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.constant;

/**
 * 授权码变更状态
 */
public interface ChangeStatus {
    /**
     * 冻结
     */
    String FREEZE = "FREEZE";

    /**
     * 取消冻结
     */
    String UNFREEZE = "UNFREEZE";
}
