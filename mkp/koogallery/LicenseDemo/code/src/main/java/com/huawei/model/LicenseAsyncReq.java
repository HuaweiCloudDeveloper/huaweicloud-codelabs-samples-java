/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 异步申请License，使用Map作为入参，该类仅作为参考
 * 具体定义参考：https://support.huaweicloud.com/accessg-marketplace/zh-cn_topic_0070649255.html
 */
@Getter
@Setter
public class LicenseAsyncReq {
    /**
     * 接口请求标识，用于区分接口请求场景。
     */
    private String activity;

    /**
     * 产品标识
     * 必返回
     */
    private String deviceFingerprint;

    /**
     * 实例ID。
     * 必返回
     */
    private String instanceId;

    /**
     * 生效时间
     */
    private String effectiveTime;

    /**
     * 业务流水线id
     */
    private String eventId;

    /**
     * 过期时间。
     * 格式：yyyyMMddHHmmss
     * 必返回
     */
    private String expireTime;

    /**
     * 是否为调试请求。
     * <p>
     * 1：调试请求
     * 0：非调试请求
     * 默认取值为“0”。
     * 非必返回
     */
    private String testFlag;
}
