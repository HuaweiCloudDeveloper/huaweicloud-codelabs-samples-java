/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.service;

import java.util.Map;

import com.huawei.model.IMessageResp;

public interface IsvProduceService {
    /**
     * 工业软件云接收云商店同步数据业务实现
     *
     * @param isvProduceReq 请求参数
     * @return 响应结果
     */
    IMessageResp processProduceReq(Map<String, String> isvProduceReq);

    /**
     * 异步上传License
     */
    void syncLicense();
}
