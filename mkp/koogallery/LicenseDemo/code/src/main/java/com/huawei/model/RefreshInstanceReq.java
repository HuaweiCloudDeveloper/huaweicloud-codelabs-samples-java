/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 更新实例场景时云商店请求传递的参数，使用Map作为入参，该类仅作为参考
 * 具体定义参考：https://support.huaweicloud.com/accessg-marketplace/zh-cn_topic_0070649253.html
 */
@Getter
@Setter
public class RefreshInstanceReq {
    /**
     * 接口请求标识，用于区分接口请求场景。
     */
    private String activity;

    /**
     * 云市场订单ID。
     * 续费操作会产生新的订单，与新购时订单ID不一致，请通过instance Id做资源识别。
     * 必返回
     */
    private String orderId;

    /**
     * 实例ID。
     * 必返回
     */
    private String instanceId;

    /**
     * 产品标识，租户续费或转正产品实例时，如果订购周期类型发生变化，会传入变化后的产品类型对应的productId。
     * 非必返回
     */
    private String productId;

    /**
     * 产云商店订单行ID。
     * 必返回
     */
    private String orderLineId;

    /**
     * 过期时间。
     * 格式：yyyyMMddHHmmss
     * 必返回
     */
    private String expireTime;

    /**
     * 是否为调试请求。
     * <p>
     * 1：调试请求
     * 0：非调试请求
     * 默认取值为“0”。
     * 非必返回
     */
    private String testFlag;

    /**
     * 场景，触发授权码变更的场景：
     * RENEWAL：续费
     * UNSUBSCRIBE_RENEWAL_PERIOD：退续费
     * TRIAL_TO_FORMAL： 试用转正
     */
    private String scene;
}
