/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

/**
 * 云商店工业软件云接口请求传递的参数，使用Map作为入参，该类仅作为参考
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IsvProduceReq {
    /**
     * 接口请求标识，用于区分接口请求场景。
     */
    private String activity;

    /**
     * 云市场业务ID
     * 每一次请求，businessId皆不一致。
     * 必返回
     */
    private String businessId;

    /**
     * 云市场订单ID
     * 必返回
     */
    private String orderId;

    /**
     * 产云商店订单行ID。
     * 必返回
     */
    private String orderLineId;

    /**
     * 实例ID。
     * 必返回
     */
    private String instanceId;

    /**
     * 产品标识，租户续费或转正产品实例时，如果订购周期类型发生变化，会传入变化后的产品类型对应的productId。
     * 非必返回
     */
    private String productId;

    /**
     * 过期时间。
     * 格式：yyyyMMddHHmmss
     * 必返回
     */
    private String expireTime;

    /**
     * 场景，触发授权码变更的场景：
     * RENEWAL：续费
     * UNSUBSCRIBE_RENEWAL_PERIOD：退续费
     */
    private String scene;

    /**
     * 变更状态：
     * FREEZE：冻结
     * UNFREEZE：解冻
     */
    private String status;

    /**
     * 产品标识
     * 必返回
     */
    private String deviceFingerprint;

    /**
     * 生效时间
     */
    private String effectiveTime;

    /**
     * 业务流水线id
     */
    private String eventId;

    /**
     * 是否为调试请求。
     * 1：调试请求
     * 0： 非调试请求
     * 取值为“0”时默认不传。
     */
    private String testFlag;
}
