/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 更新实例状态场景时云商店请求传递的参数，使用Map作为入参，该类仅作为参考
 * 具体定义参考：https://support.huaweicloud.com/accessg-marketplace/zh-cn_topic_0070649252.html
 */
@Getter
@Setter
public class UpdateInstanceReq {
    /**
     * 接口请求标识，用于区分接口请求场景。
     */
    private String activity;

    /**
     * 实例ID
     * 必返回
     */
    private String instanceId;

    /**
     * 变更状态：
     * FREEZE：冻结
     * UNFREEZE：解冻
     */
    private String status;

    /**
     * 是否为调试请求：
     * 1：调试请求
     * 0：非调试业务
     * 默认取值为“0”
     */
    private String testFlag;
}
