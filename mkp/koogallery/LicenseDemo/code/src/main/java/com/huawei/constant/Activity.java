/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.constant;

/**
 * 操作类型
 */
public interface Activity {
    /**
     * 创建实例
     */
    String NEW_INSTANCE = "newInstance";

    /**
     * 更新实例
     */
    String REFRESH_INSTANCE = "refreshInstance";

    /**
     * 变更实例状态
     */
    String UPDATE_INSTANCE_STATUS = "updateInstanceStatus";

    /**
     * 释放
     */
    String RELEASE_INSTANCE = "releaseInstance";

    /**
     * 异步申请License
     */
    String LICENSE_ASYNC_INSTANCE = "getLicenseAsync";
}
