/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class OrderInfoResp {
    /**
     * 结果码
     */
    private String resultCode;

    /**
     * 结果消息
     */
    private String resultMsg;

    /**
     * 附加信息
     */
    private OrderInfo orderInfo;

    @Getter
    @Setter
    public static class OrderInfo {
        /**
         * 云商店订单号
         */
        private String orderId;

        /**
         * 订单类型
         */
        private String orderType;

        /**
         * 订单创建时间
         */
        private String createTime;

        /**
         * 订单行信息
         */
        private List<OrderLine> orderLine;

        /**
         * 客户信息
         */
        private BuyerInfo buyerInfo;

        /**
         * 扩展参数
         */
        private List<ExtendParam> extendParams;

        @Getter
        @Setter
        public static class OrderLine {
            /**
             * 云商店订单行ID
             */
            private String orderLineId;

            /**
             * 计费模式
             */
            private String chargingMode;

            /**
             * 过期时间
             */
            private String expireTime;

            /**
             * 周期类型
             */
            private String periodType;

            /**
             * 扩展参数。非必填
             */
            private List<ExtendParam> extendParams;

            /**
             * 周期数量
             */
            private Integer periodNumber;

            /**
             * 订单行关联的商品信息
             */
            private List<ProductInfo> productInfo;

            @Getter
            @Setter
            public static class ProductInfo {
                /**
                 * 产品标识，同一skuCode下，不同周期类型的productId不同。
                 */
                private String productId;

                /**
                 * 产品规格标识。
                 */
                private String skuCode;

                /**
                 * 线性单位值，如果当前商品存在数量属性，用户在下单时选择的线性数值
                 */
                private Integer linearValue;

                /**
                 * 商品名称
                 */
                private String productName;
            }
        }

        @Getter
        @Setter
        public static class ExtendParam {
            /**
             * 参数名
             */
            private String name;

            /**
             * 参数的值
             */
            private String value;
        }

        @Getter
        @Setter
        public static class BuyerInfo {
            /**
             * 用户手机号
             */
            private String mobilePhone;

            /**
             * 客户ID
             */
            private String customerId;

            /**
             * 客户账号名
             */
            private String customerName;

            /**
             * 用户邮箱
             */
            private String email;

            /**
             * 用户id
             */
            private String userId;

            /**
             * 用户名称
             */
            private String userName;
        }
    }
}
