/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.constant;

/**
 * 变更场景
 */
public interface RenewAction {
    /**
     * 转正试用
     */
    String SUB = "TRIAL_TO_FORMAL";

    /**
     * 续费
     */
    String RENEW = "RENEWAL";

    /**
     * 退续费
     */
    String UNSUB = "UNSUBSCRIBE_RENEWAL_PERIOD";
}
