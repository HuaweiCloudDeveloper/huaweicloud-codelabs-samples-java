/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.util;

public enum ResultCodeEnum {
    /**
     * 成功
     */
    SUCCESS("000000", "success."),

    /**
     * 鉴权失败
     */
    INVALID_TOKEN("000001", "token is invalid"),

    /**
     * 请求参数不合法
     */
    INVALID_PARAM("000002", "param is invalid"),

    /**
     * 实例ID不存在（更新实例状态、变更实例、释放、异步申请License需要校验）
     */
    INVALID_INSTANCE("000003", "instance is invalid"),

    /**
     * 请求处理中（特殊场景使用，普通ISV请忽略）
     */
    IN_PROCESS("000004", "request is processing"),

    /**
     * 其他内部错误
     */
    OTHER_INNER_ERROR("000005", "other inner error");

    /**
     * 错误码
     */
    private final String resultCode;

    /**
     * 错误码说明
     */
    private final String resultMsg;

    ResultCodeEnum(String resultCode, String resultMsg) {
        this.resultCode = resultCode;
        this.resultMsg = resultMsg;
    }

    public String getResultCode() {
        return resultCode;
    }

    public String getResultMsg() {
        return resultMsg;
    }
}
