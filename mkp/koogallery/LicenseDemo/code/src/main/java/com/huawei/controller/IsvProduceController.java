/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.controller;

import com.huawei.model.IMessageResp;
import com.huawei.service.IsvProduceService;
import com.huawei.util.IsvProduceAPI;
import com.huawei.util.ResultCodeEnum;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 工业软件云对接
 */
@RestController
@RequestMapping(value = "isv/produce")
@Slf4j
public class IsvProduceController {
    /**
     * 密钥
     */
    @Value("${access_key}")
    private String ACCESS_KEY;

    @Resource
    private IsvProduceService isvProduceService;

    /**
     * 主动通知接口请求处理
     *
     * @param isvProduceReq 请求体
     * @param request 请求
     * @return response 返回
     */
    @PostMapping(value = "")
    @ResponseBody
    public IMessageResp processProduceReq(@RequestBody Map<String, String> isvProduceReq, HttpServletRequest request)
        throws Exception {
        // 验证签名
        IMessageResp resp = IsvProduceAPI.verifySignature(isvProduceReq, request, ACCESS_KEY);
        // 如果鉴权返回成功则进行业务处理 区分五种接口 验证参数 及异常场景校验
        if (ResultCodeEnum.SUCCESS.getResultCode().equals(resp.getResultCode())) {
            resp = isvProduceService.processProduceReq(isvProduceReq);
        }
        return resp;
    }
}
