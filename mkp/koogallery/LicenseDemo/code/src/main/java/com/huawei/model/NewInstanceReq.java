/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 创建实例场景时云商品请求传递的参数，使用Map作为入参，该类仅作为参考
 * 具体定义参考：https://support.huaweicloud.com/accessg-marketplace/zh-cn_topic_0070649251.html
 */
@Getter
@Setter
public class NewInstanceReq {
    /**
     * 接口请求标识，用于区分接口请求场景。
     */
    private String activity;

    /**
     * 云市场业务ID
     * 每一次请求，businessId皆不一致。
     * 必返回
     */
    private String businessId;

    /**
     * 云市场订单ID
     * 必返回
     */
    private String orderId;

    /**
     * 产云商店订单行ID。
     * 必返回
     */
    private String orderLineId;

    /**
     * 是否为调试请求
     * 1：调试请求
     * 0： 非调试请求
     * 取值为“0”时默认不传。
     * 非必返回
     */
    private String testFlag;
}
