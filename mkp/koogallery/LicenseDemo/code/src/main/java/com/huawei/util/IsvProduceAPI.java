/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.util;

import static com.huawei.constant.IsvProduceConstant.NONCE;
import static com.huawei.constant.IsvProduceConstant.SIGNATURE;
import static com.huawei.constant.IsvProduceConstant.TIMESTAMP;

import com.huawei.model.IMessageResp;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

public class IsvProduceAPI {

    /**
     * 默认编码：UTF-8
     */
    private static final String CHARSET = "UTF-8";

    private static ObjectMapper objectMapper = new ObjectMapper();

    /**
     * 验证签名
     *
     * @param temp
     * @param request
     * @param accessKey
     */
    public static IMessageResp verifySignature(Map<String, String> temp, HttpServletRequest request, String accessKey)
        throws Exception {
        IMessageResp resp = new IMessageResp();
        resp.setResultCode(ResultCodeEnum.SUCCESS.getResultCode());
        resp.setResultMsg(ResultCodeEnum.SUCCESS.getResultMsg());
        Map<String, String[]> paramsMap = request.getParameterMap();

        // 获取请求时间戳
        String reqTimestamp = getParamValue(paramsMap, TIMESTAMP);
        if (StringUtils.isEmpty(reqTimestamp)) {
            resp.setResultCode(ResultCodeEnum.INVALID_PARAM.getResultCode());
            resp.setResultMsg(ResultCodeEnum.INVALID_PARAM.getResultMsg());
            return resp;
        }

        // 获取当前UTC时间戳
        long currentUTCTime = getUTCTimeLong();

        // 请求时间戳与当前时间相差不超过60s
        if (!validateReqTime(reqTimestamp, currentUTCTime)) {
            resp.setResultCode(ResultCodeEnum.INVALID_PARAM.getResultCode());
            resp.setResultMsg(ResultCodeEnum.INVALID_PARAM.getResultMsg());
            return resp;
        }

        // 获取随机字符串
        String nonce = getParamValue(paramsMap, NONCE);
        if (StringUtils.isEmpty(nonce)) {
            resp.setResultCode(ResultCodeEnum.INVALID_PARAM.getResultCode());
            resp.setResultMsg(ResultCodeEnum.INVALID_PARAM.getResultMsg());
            return resp;
        }

        // 获取请求里的签名
        String reqSignature = getParamValue(paramsMap, SIGNATURE);
        if (StringUtils.isEmpty(reqSignature)) {
            resp.setResultCode(ResultCodeEnum.INVALID_TOKEN.getResultCode());
            resp.setResultMsg(ResultCodeEnum.INVALID_TOKEN.getResultMsg());
            return resp;
        }

        // 对入参进行顺序排序并排除value为空的key
        Map<String, String> sortedMap = new TreeMap<>(temp);
        sortedMap.entrySet().removeIf(entry -> Objects.isNull(entry.getValue()));
        String reqParams = objectMapper.writeValueAsString(sortedMap);

        // 加密请求体
        String encryptBody = generateSignature(accessKey, reqParams);

        // 生成签名
        String signature = generateSignature(accessKey, accessKey + nonce + reqTimestamp + encryptBody);

        // 判断计算后的签名与云市场请求中传递的签名是否一致 不区分大小写
        if (!reqSignature.equalsIgnoreCase(signature)) {
            resp.setResultCode(ResultCodeEnum.INVALID_TOKEN.getResultCode());
            resp.setResultMsg(ResultCodeEnum.INVALID_TOKEN.getResultMsg());
            return resp;
        }
        return resp;
    }

    private static String getParamValue(Map<String, String[]> paramsMap, String param) {
        String[] paramArray = paramsMap.get(param);
        if (null == paramArray || paramArray.length == 0) {
            return null;
        }
        return paramArray[0];
    }

    /**
     * 获取UTC时间
     *
     * @return UTC时间
     */
    private static long getUTCTimeLong() {
        Calendar cal = Calendar.getInstance();
        int zoneOffset = cal.get(Calendar.ZONE_OFFSET);
        int dstOffset = cal.get(Calendar.DST_OFFSET);
        cal.add(Calendar.MILLISECOND, -(zoneOffset + dstOffset));
        return cal.getTime().getTime();
    }

    private static boolean validateReqTime(String reqTimestamp, long currentUTCTime) {
        long reqUTCTime = Long.parseLong(reqTimestamp);
        return currentUTCTime - reqUTCTime <= 36000000000L;
    }

    public static byte[] hmacSHA256(String macKey, String macData) {
        try {
            SecretKeySpec secret = new SecretKeySpec(macKey.getBytes(CHARSET), "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(secret);
            return mac.doFinal(macData.getBytes(CHARSET));
        } catch (UnsupportedEncodingException | InvalidKeyException | NoSuchAlgorithmException e) {
            // error message log
            return new byte[0];
        }
    }

    /**
     * 获取请求体签名
     *
     * @param key accessKey
     * @param body 待签名的body体字符串
     * @return 生成的签名
     */
    public static String generateSignature(String key, String body) {
        return Hex.encodeHexString(hmacSHA256(key, body));
    }
}
