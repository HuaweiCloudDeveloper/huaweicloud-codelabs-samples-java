## 1. 示例简介

对象存储迁移服务（Object Storage Migration Service，OMS）是一种线上数据迁移服务，帮助您将其他云服务商对象存储服务中的数据在线迁移至华为云的对象存储服务（Object Storage Service，OBS）中。

说明：
目前支持亚马逊云（中国）、阿里云、微软云、百度云、华为云、金山云、优刻得、青云、七牛云、腾讯云平台的对象存储数据迁移。

对象存储迁移服务的典型应用场景有：

对象数据搬迁：将典型Web应用搬迁到华为云上时，把用户的对象存储数据搬迁到华为云中。
云上容灾：出于灾备，把用户对象存储数据复制到华为云中。
对象数据恢复：利用其他云服务提供商备份的数据，恢复用户在华为云丢失的对象存储数据。

本示例会指导用户使用前缀来创建迁移任务组进行迁移动作


## 2. 开发前准备
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已具备开发环境 ，支持java8及以上版本。
- OMS服务软件开发工具包（OMS SDK，Object Storage Migration Service Software Development Kit）是对OMS服务提供的REST API进行的封装，获取[华为云OMS开发工具包（SDK）](https://github.com/huaweicloud/huaweicloud-sdk-java-v3) ，SDK的使用方法请参见[OMS SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK），[AK/SK参考](https://support.huaweicloud.com/iam_faq/iam_01_0022.html) 。
### 2.1 认证鉴权
华为云JAVA SDK支持两种认证方式：token认证和AK/SK认证，可以选择其中一种进行认证鉴权。
- Token认证：通过Token认证通用请求。[参考](https://support.huaweicloud.com/api-iam/iam_30_0000.html)
- AK/SK认证：通过AK（Access Key ID）/SK（Secret Access Key）加密调用请求。推荐使用AK/SK认证，其安全性比Token认证要高。[参考](https://support.huaweicloud.com/iam_faq/iam_01_0022.html)
## 3. 安装SDK
内容分发网络服务端SDK支持java8及以上版本。执行“ java -version” 检查当前java的版本信息。
使用服务端SDK前，您需要安装“huaweicloudsdkcore ”和 “huaweicloudsdkoms”，具体的SDK版本号请参见 [SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。



## 4. 代码示例
### 4.1 导入pom依赖
``` java
<dependency>
	<groupId>com.huaweicloud.sdk</groupId>
	<artifactId>huaweicloud-sdk-core</artifactId>
	<version>{oms.sdk.version}</version>
</dependency>
```
### 4.2 导入依赖模块
``` java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.oms.v2.OmsClient;
import com.huaweicloud.sdk.oms.v2.model.BandwidthPolicyDto;
import com.huaweicloud.sdk.oms.v2.model.CreateTaskGroupReq;
import com.huaweicloud.sdk.oms.v2.model.CreateTaskGroupRequest;
import com.huaweicloud.sdk.oms.v2.model.CreateTaskGroupResponse;
import com.huaweicloud.sdk.oms.v2.model.TaskGroupDstNode;
import com.huaweicloud.sdk.oms.v2.model.TaskGroupSrcNode;
```
#### 4.3 使用前缀创建迁移任务组
``` java
    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");
	String iamEndpoint = "https://iam.cn-north-4.myhuaweicloud.com";
	String endpoint = "https://oms.cn-north-4.myhuaweicloud.com";

	ICredential auth = new BasicCredentials()
		.withIamEndpoint(iamEndpoint)
		.withAk(ak)
		.withSk(sk);

	OmsClient client = OmsClient.newBuilder()
		.withCredential(auth)
		.withRegion(new Region("cn-north-4", endpoint)) // 设置目的端region
		.build();
	CreateTaskGroupRequest request = new CreateTaskGroupRequest();
	CreateTaskGroupReq body = new CreateTaskGroupReq();
	// 设置限速策略，此条限速策略为全天限速10M，如不需要删除以下限速策略
	List<BandwidthPolicyDto> listbodyBandwidthPolicy = new ArrayList<>();
	listbodyBandwidthPolicy.add(
		new BandwidthPolicyDto()
			.withEnd("23:59")
			.withMaxBandwidth(1048576L)
			.withStart("00:01")
	);
	// 设置目的端参数
	TaskGroupDstNode dstNodebody = new TaskGroupDstNode();
	dstNodebody.withAk("******")
		.withSk("******")
		.withRegion("cn-north-4")
		.withBucket("dst-bucket");
	// 指定要迁移的前缀。整桶迁移prefix设置为"";
	List<String> listSrcNodeObjectKey = new ArrayList<>();
	listSrcNodeObjectKey.add("Objectlist");
	// 设置源端参数
	TaskGroupSrcNode srcNodebody = new TaskGroupSrcNode();
	srcNodebody.withAk("******")
		.withSk("******.")
		.withRegion("cn-north-4")
		.withObjectKey(listSrcNodeObjectKey)
		.withBucket("src-bucket")
		.withCloudType("HuaweiCloud");
	body.withBandwidthPolicy(listbodyBandwidthPolicy);
	// 设置任务类型为prefix
	body.withTaskType(CreateTaskGroupReq.TaskTypeEnum.fromValue("PREFIX"));
	body.withEnableKms(false);
	body.withEnableFailedObjectRecording(true);
	body.withDstNode(dstNodebody);
	body.withSrcNode(srcNodebody);
	request.withBody(body);
	try {
		CreateTaskGroupResponse response = client.createTaskGroup(request);
		System.out.println(response.toString());
	} catch (ConnectionException e) {
		e.printStackTrace();
	} catch (RequestTimeoutException e) {
		e.printStackTrace();
	} catch (ServiceResponseException e) {
		e.printStackTrace();
		System.out.println(e.getHttpStatusCode());
		System.out.println(e.getRequestId());
		System.out.println(e.getErrorCode());
		System.out.println(e.getErrorMsg());
	}
```
## 5. 参考

更多信息请参考 [对象存储迁移OMS文档](https://support.huaweicloud.com/oms/index.html)

## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-08-21 | 1.0 | 文档首次发布 |