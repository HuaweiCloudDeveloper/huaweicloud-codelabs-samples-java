/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.oms;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.oms.v2.OmsClient;
import com.huaweicloud.sdk.oms.v2.model.BandwidthPolicyDto;
import com.huaweicloud.sdk.oms.v2.model.CreateTaskGroupReq;
import com.huaweicloud.sdk.oms.v2.model.CreateTaskGroupRequest;
import com.huaweicloud.sdk.oms.v2.model.CreateTaskGroupResponse;
import com.huaweicloud.sdk.oms.v2.model.TaskGroupDstNode;
import com.huaweicloud.sdk.oms.v2.model.TaskGroupSrcNode;

import java.util.ArrayList;
import java.util.List;

public class CreateTaskGroup {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String iamEndpoint = "https://iam.cn-north-4.myhuaweicloud.com";
        String endpoint = "https://oms.cn-north-4.myhuaweicloud.com";

        ICredential auth = new BasicCredentials()
            .withIamEndpoint(iamEndpoint)
            .withAk(ak)
            .withSk(sk);

        OmsClient client = OmsClient.newBuilder()
            .withCredential(auth)
            .withRegion(new Region("cn-north-4", endpoint)) // 设置目的端region
            .build();
        CreateTaskGroupRequest request = new CreateTaskGroupRequest();
        CreateTaskGroupReq body = new CreateTaskGroupReq();
        // 设置限速策略，此条限速策略为全天限速1M，如不需要删除以下限速策略
        List<BandwidthPolicyDto> listbodyBandwidthPolicy = new ArrayList<>();
        listbodyBandwidthPolicy.add(
            new BandwidthPolicyDto()
                .withEnd("23:59")
                .withMaxBandwidth(1048576L)
                .withStart("00:01")
        );
        // 设置目的端参数
        TaskGroupDstNode dstNodebody = new TaskGroupDstNode();
        dstNodebody.withAk("******")
            .withSk("******")
            .withRegion("cn-north-4")
            .withBucket("dst-bucket");
        // 指定要迁移的前缀。整桶迁移prefix设置为"";
        List<String> listSrcNodeObjectKey = new ArrayList<>();
        listSrcNodeObjectKey.add("Objectlist");
        // 设置源端参数
        TaskGroupSrcNode srcNodebody = new TaskGroupSrcNode();
        srcNodebody.withAk("******")
            .withSk("******.")
            .withRegion("cn-north-4")
            .withObjectKey(listSrcNodeObjectKey)
            .withBucket("src-bucket")
            .withCloudType("HuaweiCloud"); // 可选值有AWS、Azure、Aliyun、Tencent、HuaweiCloud、QingCloud、KingsoftCloud、Baidu、Qiniu或者UCloud。默认值为Aliyun。
        body.withBandwidthPolicy(listbodyBandwidthPolicy);
        // 设置任务类型为prefix
        body.withTaskType(CreateTaskGroupReq.TaskTypeEnum.fromValue("PREFIX"));
        body.withEnableKms(false);
        body.withEnableFailedObjectRecording(true);
        body.withDstNode(dstNodebody);
        body.withSrcNode(srcNodebody);
        request.withBody(body);
        try {
            CreateTaskGroupResponse response = client.createTaskGroup(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            System.out.println(e.getMessage());
        } catch (RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}