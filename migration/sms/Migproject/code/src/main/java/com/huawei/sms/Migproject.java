/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.sms;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.sms.v3.SmsClient;
import com.huaweicloud.sdk.sms.v3.model.CreateMigprojectRequest;
import com.huaweicloud.sdk.sms.v3.model.ListMigprojectsRequest;
import com.huaweicloud.sdk.sms.v3.model.ListMigprojectsResponse;
import com.huaweicloud.sdk.sms.v3.model.MigprojectsResponseBody;
import com.huaweicloud.sdk.sms.v3.model.PostMigProjectBody;
import com.huaweicloud.sdk.sms.v3.model.CreateMigprojectResponse;
import com.huaweicloud.sdk.sms.v3.model.UpdateDefaultMigprojectRequest;
import com.huaweicloud.sdk.sms.v3.model.UpdateDefaultMigprojectResponse;
import com.huaweicloud.sdk.sms.v3.model.UpdateMigprojectRequest;
import com.huaweicloud.sdk.sms.v3.model.MigProject;
import com.huaweicloud.sdk.sms.v3.model.UpdateMigprojectResponse;
import com.huaweicloud.sdk.sms.v3.model.DeleteMigprojectRequest;
import com.huaweicloud.sdk.sms.v3.model.DeleteMigprojectResponse;
import com.huaweicloud.sdk.sms.v3.region.SmsRegion;

import java.util.List;

public class Migproject {

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
                .withAk(ak)
                .withSk(sk);

        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // REGION ID:中国站填写ap-southeast-1,国际站填写ap-southeast-3
        SmsClient client = SmsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(SmsRegion.valueOf("<REGION ID>"))
                .build();
        try {
            String migprojectId = createMigproject(client);
            UpdateDefaultMigproject(client, migprojectId);
            updateMigproject(client, migprojectId);
            deleteMigproject(client, migprojectId);
        } catch (ConnectionException e) {
            System.out.println(e.getMessage());
        } catch (RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }

    }

    // 获取项目列表
    public static List<MigprojectsResponseBody> listMigprojects(SmsClient client) {
        ListMigprojectsRequest request = new ListMigprojectsRequest();
        ListMigprojectsResponse response = client.listMigprojects(request);
        System.out.println(response.getHttpStatusCode());
        return response.getMigprojects();
    }

    // 新建迁移项目
    public static String createMigproject(SmsClient client) {
        CreateMigprojectRequest request = new CreateMigprojectRequest();
        // 以下部分请替换成用户实际参数
        PostMigProjectBody body = new PostMigProjectBody();
        body.withSyncing(false);
        body.withType(PostMigProjectBody.TypeEnum.fromValue("MIGRATE_FILE"));
        body.withExistServer(true);
        body.withUsePublicIp(true);
        body.withRegion("ap-southeast-1");
        body.withDescription("");
        body.withName("MIGPROJECT_NAME");
        request.withBody(body);
        CreateMigprojectResponse response = client.createMigproject(request);
        System.out.println(response.toString());
        return response.getId();
    }

    // 更新默认迁移项目
    public static void UpdateDefaultMigproject(SmsClient client, String id) {
        UpdateDefaultMigprojectRequest request = new UpdateDefaultMigprojectRequest();
        request.withMigProjectId(id);
        String defaultMigprojectId = null;
        List<MigprojectsResponseBody> migprojectsResponseBodies = listMigprojects(client);
        for (MigprojectsResponseBody migprojct: migprojectsResponseBodies) {
            if (migprojct.getIsdefault()) {
                defaultMigprojectId = migprojct.getId();
                break;
            }
        }
        UpdateDefaultMigprojectResponse response = client.updateDefaultMigproject(request);
        System.out.println(response.toString());
        request.withMigProjectId(defaultMigprojectId);
        UpdateDefaultMigprojectResponse responseDefalut = client.updateDefaultMigproject(request);
        System.out.println(responseDefalut.toString());
    }

    // 更新迁移项目信息
    public static void updateMigproject(SmsClient client, String id) {
        UpdateMigprojectRequest request = new UpdateMigprojectRequest();
        // 以下部分请替换成用户实际参数
        request.withMigProjectId(id);
        MigProject body = new MigProject();
        body.withSyncing(true);
        body.withType(MigProject.TypeEnum.fromValue("MIGRATE_FILE"));
        body.withExistServer(true);
        body.withUsePublicIp(true);
        body.withRegion("ap-southeast-1");
        body.withName("migproject_name");
        request.withBody(body);
        UpdateMigprojectResponse response = client.updateMigproject(request);
        System.out.println(response.toString());
    }

    // 删除企业项目
    public static void deleteMigproject(SmsClient client, String id) {
        DeleteMigprojectRequest request = new DeleteMigprojectRequest();
        request.withMigProjectId(id);
        DeleteMigprojectResponse response = client.deleteMigproject(request);
        System.out.println(response.toString());
    }

}