## 1. 示例简介

主机迁移服务（Server Migration Service，SMS）是一种P2V/V2V迁移服务，可以帮您把X86物理服务器或者私有云、公有云平台上的虚拟机迁移到华为云弹性云服务器云主机上，从而帮助您轻松地把服务器上的应用和数据迁移到华为云。

主机迁移服务的典型应用场景有：

1、OS迁移：通过主机迁移服务把服务器中的OS和系统配置（如安全加固、用户权限管理等配置）迁移到华为云弹性云服务器（ECS）上，包括华为云的一个账户中的ECS迁移另外一个账号的ECS里，进行资源整合。  
2、应用迁移：主机迁移服务可以把您源端服务器上所有数据迁移到华为云弹性云服务器（ECS）上，包括OS、应用及配置、文件，您无须在华为云重新部署和配置应用。  
3、数据库迁移：在中断业务的情况下，主机迁移服务可以把您源端单节点数据库服务器整机都迁移到华为云弹性云服务器（ECS）上，无须额外安装和配置数据库及迁移数据。  

本示例会指导用户解锁指定任务的目的端服务器


## 2. 开发前准备
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已具备开发环境 ，支持java8及以上版本。
- SMS服务软件开发工具包（SMS SDK，Server Migration Service Software Development Kit）是对SMS服务提供的REST API进行的封装，获取[华为云SMS开发工具包（SDK）](https://github.com/huaweicloud/huaweicloud-sdk-java-v3) ，SDK的使用方法请参见[SMS SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。
- 要使用华为云 Java SDK，您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK），[AK/SK参考](https://support.huaweicloud.com/iam_faq/iam_01_0022.html) 。
### 2.1 认证鉴权
华为云JAVA SDK支持两种认证方式：token认证和AK/SK认证，可以选择其中一种进行认证鉴权。
- Token认证：通过Token认证通用请求。[参考](https://support.huaweicloud.com/api-iam/iam_30_0000.html)
- AK/SK认证：通过AK（Access Key ID）/SK（Secret Access Key）加密调用请求。推荐使用AK/SK认证，其安全性比Token认证要高。[参考](https://support.huaweicloud.com/iam_faq/iam_01_0022.html)
## 3. 安装SDK
内容分发网络服务端SDK支持java8及以上版本。执行“ java -version” 检查当前java的版本信息。  
使用服务端SDK前，您需要安装“huaweicloudsdkcore ”和 “huaweicloudsdksms”，具体的SDK版本号请参见 [SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=Java) 。



## 4. 代码示例
### 4.1 导入pom依赖
``` java
<dependency>
	<groupId>com.huaweicloud.sdk</groupId>
	<artifactId>huaweicloud-sdk-core</artifactId>
	<version>3.1.63</version>
</dependency>
```
### 4.2 导入依赖模块
``` java
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.sms.v3.SmsClient;
import com.huaweicloud.sdk.sms.v3.model.UnlockTargetEcsRequest;
import com.huaweicloud.sdk.sms.v3.model.UnlockTargetEcsResponse;
import com.huaweicloud.sdk.sms.v3.region.SmsRegion;
```
### 4.3 解锁指定任务的目的端服务器
``` java
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
                .withAk(ak)
                .withSk(sk);

        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // REGION ID:中国站填写ap-southeast-1,国际站填写ap-southeast-3
        SmsClient client = SmsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(SmsRegion.valueOf("<REGION ID>"))
                .build();

        UnlockTargetEcsRequest request = new UnlockTargetEcsRequest();
        request.withTaskId("<TASK ID>");
        
        try {
            UnlockTargetEcsResponse response = client.unlockTargetEcs(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            System.out.println(e.getMessage());
        } catch (RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
```
## 5. 参考

更多信息请参考 [主机迁移SMS文档](https://support.huaweicloud.com/sms/index.html)

## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-11-09 | 1.0 | 文档首次发布 |