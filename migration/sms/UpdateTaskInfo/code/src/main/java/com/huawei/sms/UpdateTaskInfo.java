/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.sms;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.sms.v3.SmsClient;
import com.huaweicloud.sdk.sms.v3.model.UpdateTaskSpeedRequest ;
import com.huaweicloud.sdk.sms.v3.model.UpdateTaskSpeedReq ;
import com.huaweicloud.sdk.sms.v3.model.UpdateTaskSpeedResponse ;
import com.huaweicloud.sdk.sms.v3.model.UpdateNetworkCheckInfoRequest ;
import com.huaweicloud.sdk.sms.v3.model.NetworkCheckInfoRequestBody ;
import com.huaweicloud.sdk.sms.v3.model.UpdateNetworkCheckInfoResponse ;
import com.huaweicloud.sdk.sms.v3.model.UpdateTaskStatusRequest ;
import com.huaweicloud.sdk.sms.v3.model.UpdateTaskStatusReq ;
import com.huaweicloud.sdk.sms.v3.model.UpdateTaskStatusResponse ;
import com.huaweicloud.sdk.sms.v3.model.UpdateTaskRequest ;
import com.huaweicloud.sdk.sms.v3.model.PutTaskReq ;
import com.huaweicloud.sdk.sms.v3.model.UpdateTaskResponse;
import com.huaweicloud.sdk.sms.v3.model.ConfigBody;
import com.huaweicloud.sdk.sms.v3.model.ConfigurationRequestBody;
import com.huaweicloud.sdk.sms.v3.model.UploadSpecialConfigurationSettingRequest;
import com.huaweicloud.sdk.sms.v3.model.UploadSpecialConfigurationSettingResponse;
import com.huaweicloud.sdk.sms.v3.model.UpdateSpeedRequest;
import com.huaweicloud.sdk.sms.v3.model.SpeedLimit;
import com.huaweicloud.sdk.sms.v3.model.SpeedLimitlJson;
import com.huaweicloud.sdk.sms.v3.model.UpdateSpeedResponse;
import com.huaweicloud.sdk.sms.v3.region.SmsRegion;

import java.util.List;
import java.util.ArrayList;

public class UpdateTaskInfo {

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
                .withAk(ak)
                .withSk(sk);
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // REGION ID:中国站填写ap-southeast-1,国际站填写ap-southeast-3
        SmsClient client = SmsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(SmsRegion.valueOf("<REGION ID>"))
                .build();
        try {
            String taskId = "<TASK ID>";
            // 选择更新任务的信息
            UploadSpecialConfigurationSetting(client, taskId);
            UpdateSpeed(client, taskId);
            UpdateNetworkCheckInfo(client, taskId);
            UpdateTaskSpeed(client, taskId);
            UpdateTaskStatus(client, taskId);
            UpdateTask(client, taskId);
        } catch (ConnectionException e) {
            System.out.println(e.getMessage());
        } catch (RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    // 迁移任务配置设置
    public static void UploadSpecialConfigurationSetting(SmsClient client, String taskId) {
        UploadSpecialConfigurationSettingRequest request = new UploadSpecialConfigurationSettingRequest();
        request.withTaskId(taskId);
        ConfigurationRequestBody body = new ConfigurationRequestBody();
        List<ConfigBody> listbodyConfigurations = new ArrayList<>();
        listbodyConfigurations.add(
                new ConfigBody()
                        .withConfigKey("LINUX_CPU_LIMIT")
                        .withConfigValue("50")
        );
        body.withConfigurations(listbodyConfigurations);
        request.withBody(body);
        UploadSpecialConfigurationSettingResponse response = client.uploadSpecialConfigurationSetting(request);
        System.out.println(response.toString());
    }

    // 设置迁移限速规则
    public static void UpdateSpeed(SmsClient client, String taskId) {
        UpdateSpeedRequest request = new UpdateSpeedRequest();
        request.withTaskId(taskId);
        SpeedLimit body = new SpeedLimit();
        List<SpeedLimitlJson> listbodySpeedLimit = new ArrayList<>();
        listbodySpeedLimit.add(
                new SpeedLimitlJson()
                        .withStart("0:00")
                        .withEnd("8:00")
                        .withSpeed(20)
        );
        body.withSpeedLimit(listbodySpeedLimit);
        request.withBody(body);
        UpdateSpeedResponse response = client.updateSpeed(request);
        System.out.println(response.toString());
    }

    // 更新网络检测相关的信息
    public static void UpdateNetworkCheckInfo(SmsClient client, String taskId) {
        UpdateNetworkCheckInfoRequest request = new UpdateNetworkCheckInfoRequest();
        request.withTaskId(taskId);
        NetworkCheckInfoRequestBody body = new NetworkCheckInfoRequestBody();
        body.withEvaluationResult("20.00");
        body.withMemUsage((double)20);
        body.withCpuUsage((double)20);
        body.withLossPercentage((double)20);
        body.withMigrationSpeed((double)20);
        body.withNetworkJitter((double)20);
        body.withNetworkDelay((double)20);
        request.withBody(body);
        UpdateNetworkCheckInfoResponse response = client.updateNetworkCheckInfo(request);
        System.out.println(response.toString());
    }

    // 上报数据迁移进度和速率
    public static void UpdateTaskSpeed(SmsClient client, String taskId) {
        UpdateTaskSpeedRequest request = new UpdateTaskSpeedRequest();
        request.withTaskId(taskId);
        UpdateTaskSpeedReq body = new UpdateTaskSpeedReq();
        body.withProcessTrace("50");
        body.withTotalsize(4000L);
        body.withReplicatesize(2000L);
        body.withProgress(50);
        body.withSubtaskName(UpdateTaskSpeedReq.SubtaskNameEnum.fromValue("CREATE_CLOUD_SERVER"));
        request.withBody(body);
        UpdateTaskSpeedResponse response = client.updateTaskSpeed(request);
        System.out.println(response.toString());
    }

    // 管理迁移任务
    public static void UpdateTaskStatus(SmsClient client, String taskId) {
        UpdateTaskStatusRequest request = new UpdateTaskStatusRequest();
        request.withTaskId(taskId);
        UpdateTaskStatusReq body = new UpdateTaskStatusReq();
        body.withOperation(UpdateTaskStatusReq.OperationEnum.fromValue("test"));
        request.withBody(body);
        UpdateTaskStatusResponse response = client.updateTaskStatus(request);
        System.out.println(response.toString());
    }

    // 更新指定ID的迁移任务
    public static void UpdateTask(SmsClient client, String taskId) {
        UpdateTaskRequest request = new UpdateTaskRequest();
        request.withTaskId(taskId);
        PutTaskReq body = new PutTaskReq();
        body.withName("test_name");
        request.withBody(body);
        UpdateTaskResponse response = client.updateTask(request);
        System.out.println(response.toString());
    }

}