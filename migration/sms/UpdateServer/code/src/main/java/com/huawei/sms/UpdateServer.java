/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.sms;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.sms.v3.SmsClient;
import com.huaweicloud.sdk.sms.v3.model.PutSourceServerBody;
import com.huaweicloud.sdk.sms.v3.model.UpdateCommandResultRequest;
import com.huaweicloud.sdk.sms.v3.model.UpdateServerNameRequest;
import com.huaweicloud.sdk.sms.v3.model.UpdateServerNameResponse;
import com.huaweicloud.sdk.sms.v3.model.CommandBody;
import com.huaweicloud.sdk.sms.v3.model.UpdateCommandResultResponse;
import com.huaweicloud.sdk.sms.v3.model.UpdateDiskInfoRequest;
import com.huaweicloud.sdk.sms.v3.model.PhysicalVolume;
import com.huaweicloud.sdk.sms.v3.model.PutDiskInfoReq;
import com.huaweicloud.sdk.sms.v3.model.ServerDisk;
import com.huaweicloud.sdk.sms.v3.model.UpdateDiskInfoResponse;
import com.huaweicloud.sdk.sms.v3.model.UpdateCopyStateRequest;
import com.huaweicloud.sdk.sms.v3.model.PutCopyStateReq;
import com.huaweicloud.sdk.sms.v3.model.UpdateCopyStateResponse;
import com.huaweicloud.sdk.sms.v3.region.SmsRegion;

import java.util.ArrayList;
import java.util.List;

public class UpdateServer {

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
                .withAk(ak)
                .withSk(sk);
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // REGION ID:中国站填写ap-southeast-1,国际站填写ap-southeast-3
        SmsClient client = SmsClient.newBuilder()
                .withCredential(auth)
                .withHttpConfig(config)
                .withRegion(SmsRegion.valueOf("<REGION ID>"))
                .build();
        try {
            String sourceId = "<SOURCE ID>";
            UpdateServerName(client, sourceId);
            UpdateCommandResult(client, sourceId);
            UpdateDiskInfo(client, sourceId);
            UpdateCopyState(client, sourceId);
        } catch (ConnectionException e) {
            System.out.println(e.getMessage());
        } catch (RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    // 修改指定ID的源端服务器名称
    public static void UpdateServerName(SmsClient client, String sourceId) {
        UpdateServerNameRequest request = new UpdateServerNameRequest();
        request.withSourceId(sourceId);
        PutSourceServerBody body = new PutSourceServerBody();
        body.withName("source_name");
        request.withBody(body);
        UpdateServerNameResponse response = client.updateServerName(request);
        System.out.println(response.toString());
    }

    // 上报服务端命令执行结果
    public static void UpdateCommandResult(SmsClient client, String sourceId) {
        UpdateCommandResultRequest request = new UpdateCommandResultRequest();
        request.withServerId(sourceId);
        CommandBody body = new CommandBody();
        body.withResultDetail("{\"msg\":\"xxx\"}");
        body.withResult("success");
        body.withCommandName("START");
        request.withBody(body);
        UpdateCommandResultResponse response = client.updateCommandResult(request);
        System.out.println(response.toString());
    }

    // 更新磁盘信息
    public static void UpdateDiskInfo(SmsClient client, String sourceId) {
        UpdateDiskInfoRequest request = new UpdateDiskInfoRequest();
        request.withSourceId(sourceId);
        PutDiskInfoReq body = new PutDiskInfoReq();
        List<PhysicalVolume> listDisksPhysicalVolumes = new ArrayList<>();
        listDisksPhysicalVolumes.add(
                new PhysicalVolume()
                        .withSize(102400L)
        );
        List<ServerDisk> listbodyDisks = new ArrayList<>();
        listbodyDisks.add(
                new ServerDisk()
                        .withName("/vda")
                        .withDeviceUse(ServerDisk.DeviceUseEnum.fromValue("BOOT"))
                        .withSize(102400L)
                        .withUsedSize(10240L)
                        .withPhysicalVolumes(listDisksPhysicalVolumes)
        );
        body.withDisks(listbodyDisks);
        request.withBody(body);
        UpdateDiskInfoResponse response = client.updateDiskInfo(request);
        System.out.println(response.toString());
    }

    // 更新任务对应源端复制状态
    public static void UpdateCopyState(SmsClient client, String sourceId) {
        UpdateCopyStateRequest request = new UpdateCopyStateRequest();
        request.withSourceId(sourceId);
        PutCopyStateReq body = new PutCopyStateReq();
        body.withMigrationcycle(PutCopyStateReq.MigrationcycleEnum.fromValue("cutovered"));
        body.withCopystate(PutCopyStateReq.CopystateEnum.fromValue("STOPPED"));
        request.withBody(body);
        UpdateCopyStateResponse response = client.updateCopyState(request);
        System.out.println(response.toString());
    }

}