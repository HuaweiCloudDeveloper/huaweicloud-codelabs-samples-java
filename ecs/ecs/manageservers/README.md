## 1、介绍
**删除弹性云服务器ECS**

[删除云服务器](https://support.huaweicloud.com/api-ecs/ecs_02_0103.html)，根据指定的云服务器ID列表，删除云服务器。
本接口为异步接口，当前删除云服务器请求下发成功后会返回job_id，此时删除云服务器并没有立即完成，需要通过调用查询任务的执行状态查询job状态，当Job状态为 SUCCESS 时代表云服务器删除成功。
系统支持删除单台云服务器和批量删除多台云服务器操作，批量删除云服务器时，一次最多可以删除1000台。
仅支持删除按需计费的云服务器。

**您将学到什么？**

如何通过java版SDK来体验删除弹性云服务器。

## 2、前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云租户账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 3、endpoint 华为云各服务应用区域和各服务的终端节点，具体请参见[地区和终端节点](https://developer.huaweicloud.com/endpoint)。
- 4、华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。
- 5、用户需要在弹性云服务器（ECS）中购买弹性云服务器。

## 3、SDK获取和安装
您可以通过如下方式获取和安装 SDK： 通过 Maven 安装依赖（推荐） 通过 Maven 安装项目依赖是使用 Java SDK 的推荐方法，首先您需要在您的操作系统中下载并安装 Maven ，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
本示例使用ECS SDK：
```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-ecs</artifactId>
    <version>3.0.7-beta</version>
</dependency>
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>2.0.5</version>
</dependency>
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>3.1.58</version>
    <scope>compile</scope>
</dependency>
```


## 4、接口参数说明
关于接口参数的详细说明可参见：

[a.删除云服务器](https://support.huaweicloud.com/api-ecs/ecs_02_0103.html)

## 5、代码示例
以下代码展示如何使用删除云服务器相关SDK
```java
package com.huawei.ecs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.ecs.v2.EcsClient;
import com.huaweicloud.sdk.ecs.v2.model.DeleteServersRequest;
import com.huaweicloud.sdk.ecs.v2.model.DeleteServersRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.DeleteServersResponse;
import com.huaweicloud.sdk.ecs.v2.model.ServerId;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ManageEcsDemo {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManageEcsDemo.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份认证为例，运行示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // 配置客户端属性
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // 创建认证
        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        // 创建EcsClient实例并初始化
        EcsClient ecsClient = EcsClient.newBuilder()
            .withHttpConfig(config)
            .withCredential(auth)
            .withRegion(new Region("cn-north-4", new String[] {"https://ecs.cn-north-4.myhuaweicloud.com"}))
            .build();

        // 删除云服务器
        deleteServer(ecsClient);

    }

    private static void deleteServer(EcsClient client) {
        // 云服务器ID
        String serverIdStr = "{your serverId string}";
        ServerId serverId = new ServerId().withId(serverIdStr);
        List<ServerId> serverIds = new ArrayList<>();
        serverIds.add(serverId);
        try {
            DeleteServersResponse response = client.deleteServers(
                new DeleteServersRequest().withBody(new DeleteServersRequestBody().withServers(serverIds)));
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
        LOGGER.error("RequestId: " + e.getRequestId());
        LOGGER.error("ErrorCode: " + e.getErrorCode());
        LOGGER.error("ErrorMsg: " + e.getErrorMsg());
    }

}
```

## 6、修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-12-30 | 1.0 | 文档首次发布 |