## 1.Introduction
**Deleting the ECSs**

[Deleting ECSs](https://support.huaweicloud.com/intl/en-us/api-ecs/ecs_02_0103.html)，Delete cloud servers based on the specified cloud server ID list.
This interface is asynchronous. The job_id will be returned after the current cloud server deletion request is successfully issued. 
At this time, the cloud server deletion is not completed immediately. You need to query the job status by calling the execution status of the query task. 
When the job status is SUCCESS, it represents the cloud server deleted successfully.
This interface supports deleting a single cloud server and deleting multiple cloud servers in batches. 
When deleting cloud servers in batches, up to 1,000 cloud servers can be deleted at a time.
This interface only support the Pay-per-Use billing ECSs.

**What will you learn?**

How to delete the ECSs by the Java SDK.

## 2.Preconditions

- 1.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.
- 2.You have a Huawei Cloud account and the AK and SK of the account. (On the Huawei Cloud management console, hover the cursor over your account name and select **My Credentials**. In the navigation pane on the left, choose **Access Keys** and view your AK and SK. If you do not have the AK and SK, create them.) For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 3.Endpoint: Regions and endpoints of HUAWEI CLOUD services. For details, see [Regions and Endpoints](https://developer.huaweicloud.com/intl/en-us/endpoint).
- 4.The **Java JDK version is 1.8** or later.
- 5.You need to purchase elastic cloud servers(ECS).

## 3.SDK Installation

You can obtain and install the SDK in the following ways: Installing project dependencies through Maven is the recommended method for using the Java SDK. 
First, you need to download and install Maven in your operating system. After the installation is complete, you only need to add the corresponding dependencies to the pom.xml file of the Java project.
This example uses the ECS SDK:
``` xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-ecs</artifactId>
    <version>3.0.7-beta</version>
</dependency>
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>2.0.5</version>
</dependency>
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>3.1.58</version>
    <scope>compile</scope>
</dependency>
```

## 4.Interface parameter description
For detailed descriptions of interface parameters:
[Deleting ECSs](https://support.huaweicloud.com/intl/en-us/api-ecs/ecs_02_0103.html)

## 5.Code Sample
```java
package com.huawei.ecs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.ecs.v2.EcsClient;
import com.huaweicloud.sdk.ecs.v2.model.DeleteServersRequest;
import com.huaweicloud.sdk.ecs.v2.model.DeleteServersRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.DeleteServersResponse;
import com.huaweicloud.sdk.ecs.v2.model.ServerId;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ManageEcsDemo {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManageEcsDemo.class.getName());

    public static void main(String[] args) {
        // Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // Configure client properties
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // Create certificate
        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        // Create an EcsClient and initialize it
        EcsClient ecsClient = EcsClient.newBuilder()
            .withHttpConfig(config)
            .withCredential(auth)
            .withRegion(new Region("cn-north-4", new String[] {"https://ecs.cn-north-4.myhuaweicloud.com"}))
            .build();

        // Delete ECSs
        deleteServer(ecsClient);

    }

    private static void deleteServer(EcsClient client) {
        // Server ID
        String serverIdStr = "{your serverId string}";
        ServerId serverId = new ServerId().withId(serverIdStr);
        List<ServerId> serverIds = new ArrayList<>();
        serverIds.add(serverId);
        try {
            DeleteServersResponse response = client.deleteServers(
                new DeleteServersRequest().withBody(new DeleteServersRequestBody().withServers(serverIds)));
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
        LOGGER.error("RequestId: " + e.getRequestId());
        LOGGER.error("ErrorCode: " + e.getErrorCode());
        LOGGER.error("ErrorMsg: " + e.getErrorMsg());
    }

}
```
## 6. Change History
|  Release Date | Issue|   Description  |
| :--------: | :------: | :----------: |
| 2024-1-30|   1.0    | This issue is the first official release.|