package com.huawei.ecs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.ecs.v2.EcsClient;
import com.huaweicloud.sdk.ecs.v2.model.DeleteServersRequest;
import com.huaweicloud.sdk.ecs.v2.model.DeleteServersRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.DeleteServersResponse;
import com.huaweicloud.sdk.ecs.v2.model.ServerId;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ManageEcsDemo {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManageEcsDemo.class.getName());

    public static void main(String[] args) {
        // Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // Configure client properties
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // Create certificate
        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        // Create an EcsClient and initialize it
        EcsClient ecsClient = EcsClient.newBuilder()
            .withHttpConfig(config)
            .withCredential(auth)
            .withRegion(new Region("cn-north-4", new String[] {"https://ecs.cn-north-4.myhuaweicloud.com"}))
            .build();

        // Delete ECSs
        deleteServer(ecsClient);

    }

    private static void deleteServer(EcsClient client) {
        // Server ID
        String serverIdStr = "{your serverId string}";
        ServerId serverId = new ServerId().withId(serverIdStr);
        List<ServerId> serverIds = new ArrayList<>();
        serverIds.add(serverId);
        try {
            DeleteServersResponse response = client.deleteServers(
                new DeleteServersRequest().withBody(new DeleteServersRequestBody().withServers(serverIds)));
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        LOGGER.error("HttpStatusCode: " + e.getHttpStatusCode());
        LOGGER.error("RequestId: " + e.getRequestId());
        LOGGER.error("ErrorCode: " + e.getErrorCode());
        LOGGER.error("ErrorMsg: " + e.getErrorMsg());
    }

}