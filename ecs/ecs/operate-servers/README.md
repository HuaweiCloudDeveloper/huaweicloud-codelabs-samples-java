## 1、介绍
**管理弹性云服务器ECS**

管理弹性云服务器ECS，包括创建、关机、启动、变更规格、创建弹性IP、绑定弹性IP、解绑弹性IP、添加安全组、移除安全组等操作。

**您将学到什么？**

如何通过java版SDK来体验管理弹性云服务器。

## 2、前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云租户账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 3、endpoint 华为云各服务应用区域和各服务的终端节点，具体请参见[地区和终端节点](https://developer.huaweicloud.com/endpoint)。
- 4、华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。

## 3、SDK获取和安装
您可以通过如下方式获取和安装 SDK： 通过Maven安装项目依赖是使用Java SDK的推荐方法，首先您需要在您的操作系统中下载并安装Maven，安装完成后您只需在Java项目的pom.xml文件加入相应的依赖项即可。
本示例使用ECS SDK和EIP SDK：
``` xml
    <dependencies>
        <dependency>
            <groupId>com.huaweicloud.sdk</groupId>
            <artifactId>huaweicloud-sdk-ecs</artifactId>
            <version>3.1.80</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>2.0.5</version>
        </dependency>
        <dependency>
            <groupId>com.huaweicloud.sdk</groupId>
            <artifactId>huaweicloud-sdk-eip</artifactId>
            <version>3.1.80</version>
        </dependency>
    </dependencies>
```

## 4、接口参数说明
关于接口参数的详细说明可参见：

[创建云服务器](https://support.huaweicloud.com/api-ecs/ecs_02_0101.html)

[创建云服务器（按需）](https://support.huaweicloud.com/api-ecs/zh-cn_topic_0020212668.html)

[查询云服务器详情列表](https://support.huaweicloud.com/api-ecs/zh-cn_topic_0094148850.html)

[批量关闭云服务器](https://support.huaweicloud.com/api-ecs/ecs_02_0303.html)

[批量启动云服务器](https://support.huaweicloud.com/api-ecs/ecs_02_0301.html)

[变更云服务器规格（按需）](https://support.huaweicloud.com/api-ecs/ecs_02_0210.html)

[添加安全组](https://support.huaweicloud.com/api-ecs/ecs_03_0601.html)

[移除安全组](https://support.huaweicloud.com/api-ecs/ecs_03_0602.html)

[查询指定云服务器安全组列表](https://support.huaweicloud.com/api-ecs/ecs_03_0603.html)

[申请EIP(按需计费)](https://support.huaweicloud.com/api-eip/eip_api_0001.html)

[更新EIP](https://support.huaweicloud.com/api-eip/eip_api_0004.html)

[删除EIP](https://support.huaweicloud.com/api-eip/eip_api_0005.html)

## 5、 代码示例

``` java
package com.huawei.ecs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.ecs.v2.EcsClient;
import com.huaweicloud.sdk.ecs.v2.model.BatchStartServersOption;
import com.huaweicloud.sdk.ecs.v2.model.BatchStartServersRequest;
import com.huaweicloud.sdk.ecs.v2.model.BatchStartServersRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.BatchStartServersResponse;
import com.huaweicloud.sdk.ecs.v2.model.BatchStopServersOption;
import com.huaweicloud.sdk.ecs.v2.model.BatchStopServersRequest;
import com.huaweicloud.sdk.ecs.v2.model.BatchStopServersRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.BatchStopServersResponse;
import com.huaweicloud.sdk.ecs.v2.model.CreatePostPaidServersRequest;
import com.huaweicloud.sdk.ecs.v2.model.CreatePostPaidServersRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.CreatePostPaidServersResponse;
import com.huaweicloud.sdk.ecs.v2.model.CreateServersRequest;
import com.huaweicloud.sdk.ecs.v2.model.CreateServersRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.CreateServersResponse;
import com.huaweicloud.sdk.ecs.v2.model.ListServersDetailsRequest;
import com.huaweicloud.sdk.ecs.v2.model.ListServersDetailsResponse;
import com.huaweicloud.sdk.ecs.v2.model.NovaAddSecurityGroupOption;
import com.huaweicloud.sdk.ecs.v2.model.NovaAssociateSecurityGroupRequest;
import com.huaweicloud.sdk.ecs.v2.model.NovaAssociateSecurityGroupRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.NovaAssociateSecurityGroupResponse;
import com.huaweicloud.sdk.ecs.v2.model.NovaDisassociateSecurityGroupRequest;
import com.huaweicloud.sdk.ecs.v2.model.NovaDisassociateSecurityGroupRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.NovaDisassociateSecurityGroupResponse;
import com.huaweicloud.sdk.ecs.v2.model.NovaListServerSecurityGroupsRequest;
import com.huaweicloud.sdk.ecs.v2.model.NovaListServerSecurityGroupsResponse;
import com.huaweicloud.sdk.ecs.v2.model.NovaRemoveSecurityGroupOption;
import com.huaweicloud.sdk.ecs.v2.model.PostPaidServer;
import com.huaweicloud.sdk.ecs.v2.model.PostPaidServerNic;
import com.huaweicloud.sdk.ecs.v2.model.PostPaidServerRootVolume;
import com.huaweicloud.sdk.ecs.v2.model.PrePaidServer;
import com.huaweicloud.sdk.ecs.v2.model.PrePaidServerExtendParam;
import com.huaweicloud.sdk.ecs.v2.model.PrePaidServerNic;
import com.huaweicloud.sdk.ecs.v2.model.PrePaidServerRootVolume;
import com.huaweicloud.sdk.ecs.v2.model.ResizePostPaidServerOption;
import com.huaweicloud.sdk.ecs.v2.model.ResizePostPaidServerRequest;
import com.huaweicloud.sdk.ecs.v2.model.ResizePostPaidServerRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.ResizePostPaidServerResponse;
import com.huaweicloud.sdk.ecs.v2.model.ServerId;
import com.huaweicloud.sdk.eip.v2.EipClient;
import com.huaweicloud.sdk.eip.v2.model.CreatePublicipBandwidthOption;
import com.huaweicloud.sdk.eip.v2.model.CreatePublicipOption;
import com.huaweicloud.sdk.eip.v2.model.CreatePublicipRequest;
import com.huaweicloud.sdk.eip.v2.model.CreatePublicipRequestBody;
import com.huaweicloud.sdk.eip.v2.model.CreatePublicipResponse;
import com.huaweicloud.sdk.eip.v2.model.DeletePublicipRequest;
import com.huaweicloud.sdk.eip.v2.model.DeletePublicipResponse;
import com.huaweicloud.sdk.eip.v2.model.UpdatePublicipOption;
import com.huaweicloud.sdk.eip.v2.model.UpdatePublicipRequest;
import com.huaweicloud.sdk.eip.v2.model.UpdatePublicipResponse;
import com.huaweicloud.sdk.eip.v2.model.UpdatePublicipsRequestBody;
import com.sun.org.slf4j.internal.Logger;
import com.sun.org.slf4j.internal.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class OperateServersDemo {
    private static final Logger logger = LoggerFactory.getLogger(OperateServersDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // 配置客户端属性
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // 创建认证
        BasicCredentials auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        // 创建EcsClient实例并初始化
        EcsClient ecsClient = EcsClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(new Region("cn-east-3", "https://ecs.cn-east-3.myhuaweicloud.com"))
                .build();

        EipClient eipClient = EipClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(new Region("cn-east-3", "https://vpc.cn-east-3.myhuaweicloud.com"))
                .build();

        // 创建包年包月云服务器
        // CreatePrePaidServer(ecsClient);

        // 创建按需云服务器
        // CreatePostPaidServers(ecsClient);

        // 查询云服务器列表
        // ListServers(ecsClient);

        // 关闭云服务器
        // StopServer(ecsClient);

        // 变更规格(按需)
        // ResizePostPaidServer(ecsClient);

        // 启动云服务器
        // StartServers(ecsClient);

        // 购买弹性公网IP
        // ApplyPublicIp(eipClient);

        // 绑定弹性公网IP
        // AssociatePublicIp(eipClient);

        // 解绑弹性公网IP
        // DisassociatePublicIp(eipClient);

        // 删除弹性IP
        // DeletePublicIp(eipClient);

        // 添加安全组
        // NovaAddSecurityGroup(ecsClient);

        // 查询安全组
        // NovaListServerSecurityGroups(ecsClient);

        // 移除安全组
        // NovaRemoveSecurityGroup(ecsClient);

    }

    public static void CreatePrePaidServer(EcsClient ecsClient) {
        // 规格
        String flavorId = "{your flavorId string}";
        // 镜像ID
        String imageId = "{your imageId string}";
        String vpcId = "{your vpcId string}";
        String subnetId = "{your subnetId string}";

        PrePaidServer server = new PrePaidServer();
        server.setImageRef(imageId);
        server.setFlavorRef(flavorId);
        // 名称
        server.setName("{your ecsName string}");
        server.setVpcid(vpcId);
        List<PrePaidServerNic> nics = new ArrayList<PrePaidServerNic>();

        PrePaidServerNic nic = new PrePaidServerNic();
        nic.setSubnetId(subnetId);
        nics.add(nic);
        server.setNics(nics);

        server.setCount(1);

        PrePaidServerRootVolume rootVolume = new PrePaidServerRootVolume();
        rootVolume.setVolumetype(PrePaidServerRootVolume.VolumetypeEnum.fromValue("GPSSD"));
        server.setRootVolume(rootVolume);

        // 预付费(包月)
        PrePaidServerExtendParam extendParam = new PrePaidServerExtendParam();
        extendParam.setChargingMode(PrePaidServerExtendParam.ChargingModeEnum.PREPAID);
        extendParam.setPeriodType(PrePaidServerExtendParam.PeriodTypeEnum.MONTH);
        extendParam.setPeriodNum(1);
        extendParam.setIsAutoPay(PrePaidServerExtendParam.IsAutoPayEnum.TRUE);

        server.setExtendparam(extendParam);

        CreateServersRequestBody body = new CreateServersRequestBody();
        body.setServer(server);
        CreateServersRequest request = new CreateServersRequest();
        request.setBody(body);

        try {
            CreateServersResponse response = ecsClient.createServers(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public static void CreatePostPaidServers(EcsClient ecsClient) {
        PostPaidServer server = new PostPaidServer();
        // 镜像ID
        server.setImageRef("{your imageId string}");
        // 规格
        server.setFlavorRef("{your flavorId string}");
        server.setName("{your ecsName string}");
        server.setVpcid("{your vpcId string}");
        List<PostPaidServerNic> nics = new ArrayList<PostPaidServerNic>();

        PostPaidServerNic nic = new PostPaidServerNic();
        nic.setSubnetId("{your subnetId string}");
        nics.add(nic);

        server.setNics(nics);
        server.setCount(1);
        PostPaidServerRootVolume rootVolume = new PostPaidServerRootVolume();
        rootVolume.setVolumetype(PostPaidServerRootVolume.VolumetypeEnum.fromValue("GPSSD"));
        server.setRootVolume(rootVolume);

        CreatePostPaidServersRequestBody body = new CreatePostPaidServersRequestBody();
        body.setServer(server);
        CreatePostPaidServersRequest request = new CreatePostPaidServersRequest();
        request.setBody(body);

        try {
            CreatePostPaidServersResponse response = ecsClient.createPostPaidServers(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void ListServers(EcsClient client) {
        try {
            ListServersDetailsRequest request = new ListServersDetailsRequest();
            ListServersDetailsResponse response = client.listServersDetails(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void StopServer(EcsClient client) {
        String serverId = "{your serverId string}";
        BatchStopServersOption.TypeEnum type = BatchStopServersOption.TypeEnum.SOFT;
        try {
            BatchStopServersResponse response = client.batchStopServers(new BatchStopServersRequest().withBody
                    (new BatchStopServersRequestBody().withOsStop(new BatchStopServersOption().withType(type)
                            .addServersItem(new ServerId().withId(serverId)))));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void ResizePostPaidServer(EcsClient client) {
        String serverId = "{your serverId string}";
        String flavorId = "{your flavorId string}";
        try {
            ResizePostPaidServerResponse response = client.resizePostPaidServer(new ResizePostPaidServerRequest().withServerId(serverId)
                    .withBody(new ResizePostPaidServerRequestBody().withResize(new ResizePostPaidServerOption().withFlavorRef(flavorId))));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void StartServers(EcsClient client) {
        String serverId = "{your serverId string}";
        try {
            BatchStartServersResponse response = client.batchStartServers(new BatchStartServersRequest().withBody
                    (new BatchStartServersRequestBody().withOsStart(new BatchStartServersOption().addServersItem(new ServerId().withId(serverId)))));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void ApplyPublicIp(EipClient client) {
        String name = "{ your eip name}";
        // 带宽大小.单位M
        int size = 1;
        // 计费类型
        CreatePublicipBandwidthOption.ChargeModeEnum chargeMode = CreatePublicipBandwidthOption.ChargeModeEnum.TRAFFIC;
        // 带宽类型
        CreatePublicipBandwidthOption.ShareTypeEnum shareTypeEnum = CreatePublicipBandwidthOption.ShareTypeEnum.PER;
        CreatePublicipOption.IpVersionEnum ipVersionEnum = CreatePublicipOption.IpVersionEnum.NUMBER_4;
        // 弹性公网IP的类型。静态BGP类型为”5_sbgp“
        String type = "{your type string}";
        try {
            CreatePublicipResponse response = client.createPublicip(new CreatePublicipRequest().withBody(new CreatePublicipRequestBody()
                    .withBandwidth(new CreatePublicipBandwidthOption().withChargeMode(chargeMode).withName(name).withShareType(shareTypeEnum).withSize(size))
                    .withPublicip(new CreatePublicipOption().withIpVersion(ipVersionEnum).withType(type))));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void AssociatePublicIp(EipClient client) {
        String publicIpId = "{your publicIpId string}";
        String portId = "{your portId string}";
        try {
            UpdatePublicipResponse response = client.updatePublicip(new UpdatePublicipRequest().withPublicipId(publicIpId).
                    withBody(new UpdatePublicipsRequestBody().withPublicip(new UpdatePublicipOption().withPortId(portId))));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void DisassociatePublicIp(EipClient client) {
        String publicIpId = "{your publicIpId string}";
        try {
            UpdatePublicipResponse response = client.updatePublicip(new UpdatePublicipRequest().withPublicipId(publicIpId).
                    withBody(new UpdatePublicipsRequestBody().withPublicip(new UpdatePublicipOption())));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public static void DeletePublicIp(EipClient client) {
        String publicIpId = "{your publicIpId string}";
        try {
            DeletePublicipResponse response = client.deletePublicip(new DeletePublicipRequest().withPublicipId(publicIpId));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void NovaAddSecurityGroup(EcsClient client) {
        String serverId = "{your serverId string}";
        String securityGroupName = "{your securityGroupName string}";
        try {
            NovaAssociateSecurityGroupResponse response = client.novaAssociateSecurityGroup(new NovaAssociateSecurityGroupRequest()
                    .withServerId(serverId).withBody(new NovaAssociateSecurityGroupRequestBody().withAddSecurityGroup(new NovaAddSecurityGroupOption()
                            .withName(securityGroupName))));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void NovaListServerSecurityGroups(EcsClient client) {
        String serverId = "{your serverId string}";
        try {
            NovaListServerSecurityGroupsResponse response = client.novaListServerSecurityGroups(new NovaListServerSecurityGroupsRequest().withServerId(serverId));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void NovaRemoveSecurityGroup(EcsClient client) {
        String serverId = "{your serverId string}";
        String securityGroupName = "{your securityGroupName string}";
        try {
            NovaDisassociateSecurityGroupResponse response = client.novaDisassociateSecurityGroup(new NovaDisassociateSecurityGroupRequest()
                    .withServerId(serverId).withBody(new NovaDisassociateSecurityGroupRequestBody().withRemoveSecurityGroup(new NovaRemoveSecurityGroupOption()
                            .withName(securityGroupName))));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        logger.error("RequestId: " + e.getRequestId());
        logger.error("ErrorCode: " + e.getErrorCode());
        logger.error("ErrorMsg: " + e.getErrorMsg());
    }
}

```

## 6、修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-02-05 | 1.0 | 文档首次发布 |