## 1.Introduction
**Managing the ECSs**

This example shows how to manage the ECSs, including operations such as creating, shutting down, starting,
changing flavor, creating elastic IPs, binding elastic IPs, unbinding elastic IPs, adding security groups, and removing security groups.

**What will you learn?**

How to manage the ECSs by the Java SDK.

## 2.Preconditions
- 1.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.
- 2.You have a Huawei Cloud account and the AK and SK of the account. (On the Huawei Cloud management console, hover the cursor over your account name and select **My Credentials**. In the navigation pane on the left, choose **Access Keys** and view your AK and SK. If you do not have the AK and SK, create them.) For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 3.Endpoint: Regions and endpoints of HUAWEI CLOUD services. For details, see [Regions and Endpoints](https://developer.huaweicloud.com/intl/en-us/endpoint).
- 4.The **Java JDK version is 1.8** or later.

## 3.SDK Installation
You can obtain and install the SDK in the following ways: Installing project dependencies through Maven is the recommended method for using the Java SDK.
First, you need to download and install Maven in your operating system. After the installation is complete, you only need to add the corresponding dependencies to the pom.xml file of the Java project.
This example uses the ECS SDK and the EIP SDK:

``` xml
    <dependencies>
        <dependency>
            <groupId>com.huaweicloud.sdk</groupId>
            <artifactId>huaweicloud-sdk-ecs</artifactId>
            <version>3.1.80</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>2.0.5</version>
        </dependency>
        <dependency>
            <groupId>com.huaweicloud.sdk</groupId>
            <artifactId>huaweicloud-sdk-eip</artifactId>
            <version>3.1.80</version>
        </dependency>
    </dependencies>
```

## 4.Interface parameter description
For detailed descriptions of interface parameters:

[Creating an ECS](https://support.huaweicloud.com/api-ecs/ecs_02_0101.html)

[Creating an ECS (Pay-per-Use)](https://support.huaweicloud.com/intl/en-us/api-ecs/en-us_topic_0020212668.html)

[Querying Details About ECSs](https://support.huaweicloud.com/intl/en-us/api-ecs/en-us_topic_0094148850.html)

[Stopping ECSs in a Batch](https://support.huaweicloud.com/intl/en-us/api-ecs/ecs_02_0303.html)

[Starting ECSs in a Batch](https://support.huaweicloud.com/intl/en-us/api-ecs/ecs_02_0301.html)

[Modifying the Specifications of an ECS (Pay-per-Use)](https://support.huaweicloud.com/intl/en-us/api-ecs/ecs_02_0210.html)

[Adding an ECS to a Security Group](https://support.huaweicloud.com/intl/en-us/api-ecs/ecs_03_0601.html)

[Removing a Security Group](https://support.huaweicloud.com/intl/en-us/api-ecs/ecs_03_0602.html)

[Querying Security Groups of a Specified ECS](https://support.huaweicloud.com/intl/en-us/api-ecs/ecs_03_0603.html)

[Assigning an EIP (Pay-per-Use)](https://support.huaweicloud.com/intl/en-us/api-eip/eip_api_0001.html)

[Updating an EIP](https://support.huaweicloud.com/intl/en-us/api-eip/eip_api_0004.html)

[Releasing an EIP](https://support.huaweicloud.com/intl/en-us/api-eip/eip_api_0005.html)

## 5.Code Sample

``` java
package com.huawei.ecs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.ecs.v2.EcsClient;
import com.huaweicloud.sdk.ecs.v2.model.BatchStartServersOption;
import com.huaweicloud.sdk.ecs.v2.model.BatchStartServersRequest;
import com.huaweicloud.sdk.ecs.v2.model.BatchStartServersRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.BatchStartServersResponse;
import com.huaweicloud.sdk.ecs.v2.model.BatchStopServersOption;
import com.huaweicloud.sdk.ecs.v2.model.BatchStopServersRequest;
import com.huaweicloud.sdk.ecs.v2.model.BatchStopServersRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.BatchStopServersResponse;
import com.huaweicloud.sdk.ecs.v2.model.CreatePostPaidServersRequest;
import com.huaweicloud.sdk.ecs.v2.model.CreatePostPaidServersRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.CreatePostPaidServersResponse;
import com.huaweicloud.sdk.ecs.v2.model.CreateServersRequest;
import com.huaweicloud.sdk.ecs.v2.model.CreateServersRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.CreateServersResponse;
import com.huaweicloud.sdk.ecs.v2.model.ListServersDetailsRequest;
import com.huaweicloud.sdk.ecs.v2.model.ListServersDetailsResponse;
import com.huaweicloud.sdk.ecs.v2.model.NovaAddSecurityGroupOption;
import com.huaweicloud.sdk.ecs.v2.model.NovaAssociateSecurityGroupRequest;
import com.huaweicloud.sdk.ecs.v2.model.NovaAssociateSecurityGroupRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.NovaAssociateSecurityGroupResponse;
import com.huaweicloud.sdk.ecs.v2.model.NovaDisassociateSecurityGroupRequest;
import com.huaweicloud.sdk.ecs.v2.model.NovaDisassociateSecurityGroupRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.NovaDisassociateSecurityGroupResponse;
import com.huaweicloud.sdk.ecs.v2.model.NovaListServerSecurityGroupsRequest;
import com.huaweicloud.sdk.ecs.v2.model.NovaListServerSecurityGroupsResponse;
import com.huaweicloud.sdk.ecs.v2.model.NovaRemoveSecurityGroupOption;
import com.huaweicloud.sdk.ecs.v2.model.PostPaidServer;
import com.huaweicloud.sdk.ecs.v2.model.PostPaidServerNic;
import com.huaweicloud.sdk.ecs.v2.model.PostPaidServerRootVolume;
import com.huaweicloud.sdk.ecs.v2.model.PrePaidServer;
import com.huaweicloud.sdk.ecs.v2.model.PrePaidServerExtendParam;
import com.huaweicloud.sdk.ecs.v2.model.PrePaidServerNic;
import com.huaweicloud.sdk.ecs.v2.model.PrePaidServerRootVolume;
import com.huaweicloud.sdk.ecs.v2.model.ResizePostPaidServerOption;
import com.huaweicloud.sdk.ecs.v2.model.ResizePostPaidServerRequest;
import com.huaweicloud.sdk.ecs.v2.model.ResizePostPaidServerRequestBody;
import com.huaweicloud.sdk.ecs.v2.model.ResizePostPaidServerResponse;
import com.huaweicloud.sdk.ecs.v2.model.ServerId;
import com.huaweicloud.sdk.eip.v2.EipClient;
import com.huaweicloud.sdk.eip.v2.model.CreatePublicipBandwidthOption;
import com.huaweicloud.sdk.eip.v2.model.CreatePublicipOption;
import com.huaweicloud.sdk.eip.v2.model.CreatePublicipRequest;
import com.huaweicloud.sdk.eip.v2.model.CreatePublicipRequestBody;
import com.huaweicloud.sdk.eip.v2.model.CreatePublicipResponse;
import com.huaweicloud.sdk.eip.v2.model.DeletePublicipRequest;
import com.huaweicloud.sdk.eip.v2.model.DeletePublicipResponse;
import com.huaweicloud.sdk.eip.v2.model.UpdatePublicipOption;
import com.huaweicloud.sdk.eip.v2.model.UpdatePublicipRequest;
import com.huaweicloud.sdk.eip.v2.model.UpdatePublicipResponse;
import com.huaweicloud.sdk.eip.v2.model.UpdatePublicipsRequestBody;
import com.sun.org.slf4j.internal.Logger;
import com.sun.org.slf4j.internal.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class OperateServersDemo {
    private static final Logger logger = LoggerFactory.getLogger(OperateServersDemo.class);

    public static void main(String[] args) {
        // Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // Configure client properties
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // Create certificate
        BasicCredentials auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        // Create an EcsClient and initialize it
        EcsClient ecsClient = EcsClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(new Region("cn-east-3", "https://ecs.cn-east-3.myhuaweicloud.com"))
                .build();

        EipClient eipClient = EipClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(new Region("cn-east-3", "https://vpc.cn-east-3.myhuaweicloud.com"))
                .build();

        // Create yearly/monthly ECSs
        // CreatePrePaidServer(ecsClient);

        // Create Pay-per-Use ECSs
        // CreatePostPaidServers(ecsClient);

        // Querying Details About ECSs
        // ListServers(ecsClient);

        // Stopping ECSs
        // StopServer(ecsClient);

        // Modifying the Specifications of an ECS (Pay-per-Use)
        // ResizePostPaidServer(ecsClient);

        // Starting ECSs
        // StartServers(ecsClient);

        // Assigning an EIP (Pay-per-Use)
        // ApplyPublicIp(eipClient);

        // Binding an EIP
        // AssociatePublicIp(eipClient);

        // Unbinding an EIP
        // DisassociatePublicIp(eipClient);

        // Releasing an EIP
        // DeletePublicIp(eipClient);

        // Adding an ECS to a Security Group
        // NovaAddSecurityGroup(ecsClient);

        // Querying Security Groups of a Specified ECS
        // NovaListServerSecurityGroups(ecsClient);

        // Removing a Security Group
        // NovaRemoveSecurityGroup(ecsClient);

    }

    public static void CreatePrePaidServer(EcsClient ecsClient) {
        // Flavor ID
        String flavorId = "{your flavorId string}";
        // Image ID
        String imageId = "{your imageId string}";
        String vpcId = "{your vpcId string}";
        String subnetId = "{your subnetId string}";

        PrePaidServer server = new PrePaidServer();
        server.setImageRef(imageId);
        server.setFlavorRef(flavorId);
        // Ecs Name
        server.setName("{your ecsName string}");
        server.setVpcid(vpcId);
        List<PrePaidServerNic> nics = new ArrayList<PrePaidServerNic>();

        PrePaidServerNic nic = new PrePaidServerNic();
        nic.setSubnetId(subnetId);
        nics.add(nic);
        server.setNics(nics);

        server.setCount(1);

        PrePaidServerRootVolume rootVolume = new PrePaidServerRootVolume();
        rootVolume.setVolumetype(PrePaidServerRootVolume.VolumetypeEnum.fromValue("GPSSD"));
        server.setRootVolume(rootVolume);

        // yearly/monthly
        PrePaidServerExtendParam extendParam = new PrePaidServerExtendParam();
        extendParam.setChargingMode(PrePaidServerExtendParam.ChargingModeEnum.PREPAID);
        extendParam.setPeriodType(PrePaidServerExtendParam.PeriodTypeEnum.MONTH);
        extendParam.setPeriodNum(1);
        extendParam.setIsAutoPay(PrePaidServerExtendParam.IsAutoPayEnum.TRUE);

        server.setExtendparam(extendParam);

        CreateServersRequestBody body = new CreateServersRequestBody();
        body.setServer(server);
        CreateServersRequest request = new CreateServersRequest();
        request.setBody(body);

        try {
            CreateServersResponse response = ecsClient.createServers(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public static void CreatePostPaidServers(EcsClient ecsClient) {
        PostPaidServer server = new PostPaidServer();
        // Image ID
        server.setImageRef("{your imageId string}");
        // Flavor ID
        server.setFlavorRef("{your flavorId string}");
        server.setName("{your ecsName string}");
        server.setVpcid("{your vpcId string}");
        List<PostPaidServerNic> nics = new ArrayList<PostPaidServerNic>();

        PostPaidServerNic nic = new PostPaidServerNic();
        nic.setSubnetId("{your subnetId string}");
        nics.add(nic);

        server.setNics(nics);
        server.setCount(1);
        PostPaidServerRootVolume rootVolume = new PostPaidServerRootVolume();
        rootVolume.setVolumetype(PostPaidServerRootVolume.VolumetypeEnum.fromValue("GPSSD"));
        server.setRootVolume(rootVolume);

        CreatePostPaidServersRequestBody body = new CreatePostPaidServersRequestBody();
        body.setServer(server);
        CreatePostPaidServersRequest request = new CreatePostPaidServersRequest();
        request.setBody(body);

        try {
            CreatePostPaidServersResponse response = ecsClient.createPostPaidServers(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void ListServers(EcsClient client) {
        try {
            ListServersDetailsRequest request = new ListServersDetailsRequest();
            ListServersDetailsResponse response = client.listServersDetails(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void StopServer(EcsClient client) {
        String serverId = "{your serverId string}";
        BatchStopServersOption.TypeEnum type = BatchStopServersOption.TypeEnum.SOFT;
        try {
            BatchStopServersResponse response = client.batchStopServers(new BatchStopServersRequest().withBody
                    (new BatchStopServersRequestBody().withOsStop(new BatchStopServersOption().withType(type)
                            .addServersItem(new ServerId().withId(serverId)))));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void ResizePostPaidServer(EcsClient client) {
        String serverId = "{your serverId string}";
        String flavorId = "{your flavorId string}";
        try {
            ResizePostPaidServerResponse response = client.resizePostPaidServer(new ResizePostPaidServerRequest().withServerId(serverId)
                    .withBody(new ResizePostPaidServerRequestBody().withResize(new ResizePostPaidServerOption().withFlavorRef(flavorId))));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void StartServers(EcsClient client) {
        String serverId = "{your serverId string}";
        try {
            BatchStartServersResponse response = client.batchStartServers(new BatchStartServersRequest().withBody
                    (new BatchStartServersRequestBody().withOsStart(new BatchStartServersOption().addServersItem(new ServerId().withId(serverId)))));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void ApplyPublicIp(EipClient client) {
        String name = "{ your eip name}";
        // Bandwidth size, unit: M
        int size = 1;
        // Billing type
        CreatePublicipBandwidthOption.ChargeModeEnum chargeMode = CreatePublicipBandwidthOption.ChargeModeEnum.TRAFFIC;
        // Bandwidth type
        CreatePublicipBandwidthOption.ShareTypeEnum shareTypeEnum = CreatePublicipBandwidthOption.ShareTypeEnum.PER;
        CreatePublicipOption.IpVersionEnum ipVersionEnum = CreatePublicipOption.IpVersionEnum.NUMBER_4;
        // EIP Type. Static BGP:"5_sbgp"
        String type = "{your type string}";
        try {
            CreatePublicipResponse response = client.createPublicip(new CreatePublicipRequest().withBody(new CreatePublicipRequestBody()
                    .withBandwidth(new CreatePublicipBandwidthOption().withChargeMode(chargeMode).withName(name).withShareType(shareTypeEnum).withSize(size))
                    .withPublicip(new CreatePublicipOption().withIpVersion(ipVersionEnum).withType(type))));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void AssociatePublicIp(EipClient client) {
        String publicIpId = "{your publicIpId string}";
        String portId = "{your portId string}";
        try {
            UpdatePublicipResponse response = client.updatePublicip(new UpdatePublicipRequest().withPublicipId(publicIpId).
                    withBody(new UpdatePublicipsRequestBody().withPublicip(new UpdatePublicipOption().withPortId(portId))));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void DisassociatePublicIp(EipClient client) {
        String publicIpId = "{your publicIpId string}";
        try {
            UpdatePublicipResponse response = client.updatePublicip(new UpdatePublicipRequest().withPublicipId(publicIpId).
                    withBody(new UpdatePublicipsRequestBody().withPublicip(new UpdatePublicipOption())));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public static void DeletePublicIp(EipClient client) {
        String publicIpId = "{your publicIpId string}";
        try {
            DeletePublicipResponse response = client.deletePublicip(new DeletePublicipRequest().withPublicipId(publicIpId));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void NovaAddSecurityGroup(EcsClient client) {
        String serverId = "{your serverId string}";
        String securityGroupName = "{your securityGroupName string}";
        try {
            NovaAssociateSecurityGroupResponse response = client.novaAssociateSecurityGroup(new NovaAssociateSecurityGroupRequest()
                    .withServerId(serverId).withBody(new NovaAssociateSecurityGroupRequestBody().withAddSecurityGroup(new NovaAddSecurityGroupOption()
                            .withName(securityGroupName))));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void NovaListServerSecurityGroups(EcsClient client) {
        String serverId = "{your serverId string}";
        try {
            NovaListServerSecurityGroupsResponse response = client.novaListServerSecurityGroups(new NovaListServerSecurityGroupsRequest().withServerId(serverId));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void NovaRemoveSecurityGroup(EcsClient client) {
        String serverId = "{your serverId string}";
        String securityGroupName = "{your securityGroupName string}";
        try {
            NovaDisassociateSecurityGroupResponse response = client.novaDisassociateSecurityGroup(new NovaDisassociateSecurityGroupRequest()
                    .withServerId(serverId).withBody(new NovaDisassociateSecurityGroupRequestBody().withRemoveSecurityGroup(new NovaRemoveSecurityGroupOption()
                            .withName(securityGroupName))));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        logger.error("RequestId: " + e.getRequestId());
        logger.error("ErrorCode: " + e.getErrorCode());
        logger.error("ErrorMsg: " + e.getErrorMsg());
    }
}

```


## 6. Change History
| Release Date | Issue|   Description  |
|:------------:| :------: | :----------: |
|  2024-02-05  |   1.0    | This issue is the first official release.|