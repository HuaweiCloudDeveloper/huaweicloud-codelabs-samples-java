## 1.Introduction
**Managing the ECS security group rules**

**What will you learn?**

How to manage the ECS security group rules by the Java SDK.

## 2.Preconditions
- 1.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.
- 2.You have a Huawei Cloud account and the AK and SK of the account. (On the Huawei Cloud management console, hover the cursor over your account name and select **My Credentials**. In the navigation pane on the left, choose **Access Keys** and view your AK and SK. If you do not have the AK and SK, create them.) For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 3.The **Java JDK version is 1.8** or later.
- 4.Endpoint: Regions and endpoints of HUAWEI CLOUD services. For details, see [Regions and Endpoints](https://developer.huaweicloud.com/intl/en-us/endpoint).

## 3. SDK获取和安装

You can obtain and install the SDK in the following ways: Installing project dependencies through Maven is the recommended method for using the Java SDK.
First, you need to download and install Maven in your operating system. After the installation is complete, you only need to add the corresponding dependencies to the pom.xml file of the Java project.
This example uses the ECS SDK:

``` xml
        <dependency>
            <groupId>com.huaweicloud.sdk</groupId>
            <artifactId>huaweicloud-sdk-ecs</artifactId>
            <version>3.1.80</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>2.0.5</version>
        </dependency>
        <dependency>
            <groupId>com.huaweicloud.sdk</groupId>
            <artifactId>huaweicloud-sdk-vpc</artifactId>
            <version>3.1.87</version>
        </dependency>
```

## 4.Interface parameter description
For detailed descriptions of interface parameters:

[Querying Security Groups of a Specified ECS](https://support.huaweicloud.com/intl/en-us/api-ecs/ecs_03_0603.html)

[Querying Security Group Details](https://support.huaweicloud.com/intl/en-us/api-vpc/vpc_sg01_0002.html)

[Querying Security Group Rule Details](https://support.huaweicloud.com/intl/en-us/api-vpc/vpc_sg01_0006.html)

[Creating a Security Group](https://support.huaweicloud.com/intl/en-us/api-vpc/vpc_sg01_0001.html)

[Creating a Security Group Rule](https://support.huaweicloud.com/intl/en-us/api-vpc/vpc_sg01_0005.html)

[Deleting a Security Group](https://support.huaweicloud.com/intl/en-us/api-vpc/vpc_sg01_0004.html)

[Deleting a Security Group Rule](https://support.huaweicloud.com/intl/en-us/api-vpc/vpc_sg01_0008.html)

## 5.Code Sample

``` java
package com.huawei.ecs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.ecs.v2.EcsClient;
import com.huaweicloud.sdk.ecs.v2.model.NovaListServerSecurityGroupsRequest;
import com.huaweicloud.sdk.ecs.v2.model.NovaListServerSecurityGroupsResponse;
import com.huaweicloud.sdk.vpc.v2.VpcClient;
import com.huaweicloud.sdk.vpc.v2.model.NeutronCreateSecurityGroupOption;
import com.huaweicloud.sdk.vpc.v2.model.NeutronCreateSecurityGroupRequest;
import com.huaweicloud.sdk.vpc.v2.model.NeutronCreateSecurityGroupRequestBody;
import com.huaweicloud.sdk.vpc.v2.model.NeutronCreateSecurityGroupResponse;
import com.huaweicloud.sdk.vpc.v2.model.NeutronCreateSecurityGroupRuleOption;
import com.huaweicloud.sdk.vpc.v2.model.NeutronCreateSecurityGroupRuleRequest;
import com.huaweicloud.sdk.vpc.v2.model.NeutronCreateSecurityGroupRuleRequestBody;
import com.huaweicloud.sdk.vpc.v2.model.NeutronCreateSecurityGroupRuleResponse;
import com.huaweicloud.sdk.vpc.v2.model.NeutronDeleteSecurityGroupRequest;
import com.huaweicloud.sdk.vpc.v2.model.NeutronDeleteSecurityGroupResponse;
import com.huaweicloud.sdk.vpc.v2.model.NeutronDeleteSecurityGroupRuleRequest;
import com.huaweicloud.sdk.vpc.v2.model.NeutronDeleteSecurityGroupRuleResponse;
import com.huaweicloud.sdk.vpc.v2.model.NeutronShowSecurityGroupRequest;
import com.huaweicloud.sdk.vpc.v2.model.NeutronShowSecurityGroupResponse;
import com.huaweicloud.sdk.vpc.v2.model.NeutronShowSecurityGroupRuleRequest;
import com.huaweicloud.sdk.vpc.v2.model.NeutronShowSecurityGroupRuleResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ManageSecurityGroup {

    private static final Logger logger = LoggerFactory.getLogger(ManageSecurityGroup.class);

    public static void main(String[] args) {
        // Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // Configure client properties
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // Create certificate
        BasicCredentials auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        // Create an EcsClient and initialize it
        EcsClient ecsClient = EcsClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(new Region("cn-east-3", "https://ecs.cn-east-3.myhuaweicloud.com"))
                .build();

        VpcClient vpcClient = VpcClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region("cn-east-3", "https://vpc.cn-east-3.myhuaweicloud.com"))
                .withHttpConfig(config).build();

        // Query Security Groups of a Specified ECS
        // ListSecurityGroups(vpcClient);

        // Query Security Group Details
        // ShowSecurityGroup(vpcClient);

        // Query Security Group Rule Details
        // ShowSecurityGroupRule(vpcClient);

        // Create a Security Group
        // CreateSecurityGroup(vpcClient);

        // Create a Security Group Rule
        // CreateSecurityGroupRule(vpcClient);

        // Delete a Security Group
        // DeleteSecurityGroupRule(vpcClient);

        // Delete a Security Group Rule
        // DeleteSecurityGroup(vpcClient);
    }

    private static void NovaListServerSecurityGroups(EcsClient client) {
        String serverId = "{your serverId string}";
        try {
            NovaListServerSecurityGroupsResponse response = client.novaListServerSecurityGroups(new NovaListServerSecurityGroupsRequest().withServerId(serverId));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void ShowSecurityGroupRule(VpcClient vpcClient) {
        // security group rule id
        String securityGroupRuleId = "{your securityGroupRuleId string}";
        try {
            NeutronShowSecurityGroupRuleResponse response = vpcClient.neutronShowSecurityGroupRule(new NeutronShowSecurityGroupRuleRequest().withSecurityGroupRuleId(securityGroupRuleId));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void ShowSecurityGroup(VpcClient vpcClient) {
        // security group id 
        String securityGroupId = "{your securityGroupId string}";
        try {
            NeutronShowSecurityGroupResponse response = vpcClient.neutronShowSecurityGroup(new NeutronShowSecurityGroupRequest().withSecurityGroupId(securityGroupId));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void DeleteSecurityGroup(VpcClient vpcClient) {
        // security group id 
        String securityGroupId = "{your securityGroupId string}";
        NeutronDeleteSecurityGroupRequest request = new NeutronDeleteSecurityGroupRequest();
        request.withSecurityGroupId("8d389386-49f7-4304-9453-242dfca9d533");
        try {
            NeutronDeleteSecurityGroupResponse response = vpcClient.neutronDeleteSecurityGroup(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void DeleteSecurityGroupRule(VpcClient vpcClient) {
        // security group rule id
        String securityGroupId = "{your securityGroupId string}";
        NeutronDeleteSecurityGroupRuleRequest request = new NeutronDeleteSecurityGroupRuleRequest();
        request.withSecurityGroupRuleId(securityGroupId);
        try {
            NeutronDeleteSecurityGroupRuleResponse response = vpcClient.neutronDeleteSecurityGroupRule(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void CreateSecurityGroupRule(VpcClient vpcClient) {
        // security group id 
        String securityGroupId = "{your securityGroupId string}";
        // direction
        String direction = "egress";
        // protocol, tcp、udp、icmp or IP number (0~255)。
        String protocol = "tcp";
        // etherType
        String etherType = "IPv4";
        NeutronCreateSecurityGroupRuleRequest request = new NeutronCreateSecurityGroupRuleRequest();
        NeutronCreateSecurityGroupRuleRequestBody body = new NeutronCreateSecurityGroupRuleRequestBody();
        NeutronCreateSecurityGroupRuleOption securityGroupRulebody = new NeutronCreateSecurityGroupRuleOption();
        securityGroupRulebody.withDirection(NeutronCreateSecurityGroupRuleOption.DirectionEnum.fromValue(direction))
                .withEthertype(NeutronCreateSecurityGroupRuleOption.EthertypeEnum.fromValue(etherType))
                .withProtocol(protocol)
                .withSecurityGroupId(securityGroupId);
        body.withSecurityGroupRule(securityGroupRulebody);
        request.withBody(body);
        try {
            NeutronCreateSecurityGroupRuleResponse response = vpcClient.neutronCreateSecurityGroupRule(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void CreateSecurityGroup(VpcClient vpcClient) {
        NeutronCreateSecurityGroupRequest request = new NeutronCreateSecurityGroupRequest();
        NeutronCreateSecurityGroupRequestBody body = new NeutronCreateSecurityGroupRequestBody();
        NeutronCreateSecurityGroupOption securityGroupbody = new NeutronCreateSecurityGroupOption();
        // security group name
        securityGroupbody.withName("{your securityGroup name}");
        body.withSecurityGroup(securityGroupbody);
        request.withBody(body);
        try {
            NeutronCreateSecurityGroupResponse response = vpcClient.neutronCreateSecurityGroup(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        logger.error("RequestId: " + e.getRequestId());
        logger.error("ErrorCode: " + e.getErrorCode());
        logger.error("ErrorMsg: " + e.getErrorMsg());
    }
}

```

## 6. Change History
| Release Date | Issue|   Description  |
|:------------:| :------: | :----------: |
|  2024-3-05   |   1.0    | This issue is the first official release.|