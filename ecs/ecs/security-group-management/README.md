## 1、介绍

**ECS安全组规则管理**

**您将学到什么？**

基于华为云 Java SDK，对ECS安全组规则进行管理的代码示例。

关于安全组与弹性云服务器绑定、解绑、查询等相关操作可参考[《管理弹性云服务器ECS》](https://codelabs.developer.huaweicloud.com/codelabs/samples/c490def1a8774610be6e1ae64a83da31)
。

## 2、前置条件

- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云租户账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 >
  访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 3、华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。
- 4、endpoint 华为云各服务应用区域和各服务的终端节点，具体请参见[地区和终端节点](https://developer.huaweicloud.com/endpoint)。

## 3、SDK获取和安装

您可以通过如下方式获取和安装 SDK： 通过Maven安装项目依赖是使用Java SDK的推荐方法，首先您需要在您的操作系统中下载并安装Maven，安装完成后您只需在Java项目的pom.xml文件加入相应的依赖项即可。
本示例使用ECS SDK和VPC SDK：

``` xml
        <dependency>
            <groupId>com.huaweicloud.sdk</groupId>
            <artifactId>huaweicloud-sdk-ecs</artifactId>
            <version>3.1.80</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>2.0.5</version>
        </dependency>
        <dependency>
            <groupId>com.huaweicloud.sdk</groupId>
            <artifactId>huaweicloud-sdk-vpc</artifactId>
            <version>3.1.87</version>
        </dependency>
```

## 4、接口参数说明
关于接口参数的详细说明可参见：

[查询指定云服务器安全组列表](https://support.huaweicloud.com/api-ecs/ecs_03_0603.html)

[查询安全组](https://support.huaweicloud.com/api-vpc/vpc_sg01_0002.html)

[查询安全组规则](https://support.huaweicloud.com/api-vpc/vpc_sg01_0006.html)

[创建安全组](https://support.huaweicloud.com/api-vpc/vpc_sg01_0001.html)

[创建安全组规则](https://support.huaweicloud.com/api-vpc/vpc_sg01_0005.html)

[删除安全组](https://support.huaweicloud.com/api-vpc/vpc_sg01_0004.html)

[删除安全组规则](https://support.huaweicloud.com/api-vpc/vpc_sg01_0008.html)

## 5、代码示例
``` java
package com.huawei.ecs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.ecs.v2.EcsClient;
import com.huaweicloud.sdk.ecs.v2.model.NovaListServerSecurityGroupsRequest;
import com.huaweicloud.sdk.ecs.v2.model.NovaListServerSecurityGroupsResponse;
import com.huaweicloud.sdk.vpc.v2.VpcClient;
import com.huaweicloud.sdk.vpc.v2.model.NeutronCreateSecurityGroupOption;
import com.huaweicloud.sdk.vpc.v2.model.NeutronCreateSecurityGroupRequest;
import com.huaweicloud.sdk.vpc.v2.model.NeutronCreateSecurityGroupRequestBody;
import com.huaweicloud.sdk.vpc.v2.model.NeutronCreateSecurityGroupResponse;
import com.huaweicloud.sdk.vpc.v2.model.NeutronCreateSecurityGroupRuleOption;
import com.huaweicloud.sdk.vpc.v2.model.NeutronCreateSecurityGroupRuleRequest;
import com.huaweicloud.sdk.vpc.v2.model.NeutronCreateSecurityGroupRuleRequestBody;
import com.huaweicloud.sdk.vpc.v2.model.NeutronCreateSecurityGroupRuleResponse;
import com.huaweicloud.sdk.vpc.v2.model.NeutronDeleteSecurityGroupRequest;
import com.huaweicloud.sdk.vpc.v2.model.NeutronDeleteSecurityGroupResponse;
import com.huaweicloud.sdk.vpc.v2.model.NeutronDeleteSecurityGroupRuleRequest;
import com.huaweicloud.sdk.vpc.v2.model.NeutronDeleteSecurityGroupRuleResponse;
import com.huaweicloud.sdk.vpc.v2.model.NeutronShowSecurityGroupRequest;
import com.huaweicloud.sdk.vpc.v2.model.NeutronShowSecurityGroupResponse;
import com.huaweicloud.sdk.vpc.v2.model.NeutronShowSecurityGroupRuleRequest;
import com.huaweicloud.sdk.vpc.v2.model.NeutronShowSecurityGroupRuleResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ManageSecurityGroup {

    private static final Logger logger = LoggerFactory.getLogger(ManageSecurityGroup.class);

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // 配置客户端属性
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // 创建认证
        BasicCredentials auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        // 创建EcsClient实例并初始化
        EcsClient ecsClient = EcsClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(new Region("cn-east-3", "https://ecs.cn-east-3.myhuaweicloud.com"))
                .build();

        VpcClient vpcClient = VpcClient.newBuilder()
                .withCredential(auth)
                .withRegion(new Region("cn-east-3", "https://vpc.cn-east-3.myhuaweicloud.com"))
                .withHttpConfig(config).build();

        // 查询指定云服务器安全组列表
        // ListSecurityGroups(vpcClient);

        // 查询安全组
        // ShowSecurityGroup(vpcClient);

        // 查询安全组规则
        // ShowSecurityGroupRule(vpcClient);

        // 创建安全组
        // CreateSecurityGroup(vpcClient);

        // 创建安全组规则
        // CreateSecurityGroupRule(vpcClient);

        // 删除安全组规则
        // DeleteSecurityGroupRule(vpcClient);

        // 删除安全组
        // DeleteSecurityGroup(vpcClient);
    }

    private static void NovaListServerSecurityGroups(EcsClient client) {
        String serverId = "{your serverId string}";
        try {
            NovaListServerSecurityGroupsResponse response = client.novaListServerSecurityGroups(new NovaListServerSecurityGroupsRequest().withServerId(serverId));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void ShowSecurityGroupRule(VpcClient vpcClient) {
        // 安全组规则ID
        String securityGroupRuleId = "{your securityGroupRuleId string}";
        try {
            NeutronShowSecurityGroupRuleResponse response = vpcClient.neutronShowSecurityGroupRule(new NeutronShowSecurityGroupRuleRequest().withSecurityGroupRuleId(securityGroupRuleId));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void ShowSecurityGroup(VpcClient vpcClient) {
        // 安全组ID
        String securityGroupId = "{your securityGroupId string}";
        try {
            NeutronShowSecurityGroupResponse response = vpcClient.neutronShowSecurityGroup(new NeutronShowSecurityGroupRequest().withSecurityGroupId(securityGroupId));
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void DeleteSecurityGroup(VpcClient vpcClient) {
        // 安全组ID
        String securityGroupId = "{your securityGroupId string}";
        NeutronDeleteSecurityGroupRequest request = new NeutronDeleteSecurityGroupRequest();
        request.withSecurityGroupId("8d389386-49f7-4304-9453-242dfca9d533");
        try {
            NeutronDeleteSecurityGroupResponse response = vpcClient.neutronDeleteSecurityGroup(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void DeleteSecurityGroupRule(VpcClient vpcClient) {
        // 安全组规则ID
        String securityGroupId = "{your securityGroupId string}";
        NeutronDeleteSecurityGroupRuleRequest request = new NeutronDeleteSecurityGroupRuleRequest();
        request.withSecurityGroupRuleId(securityGroupId);
        try {
            NeutronDeleteSecurityGroupRuleResponse response = vpcClient.neutronDeleteSecurityGroupRule(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void CreateSecurityGroupRule(VpcClient vpcClient) {
        // 安全组ID
        String securityGroupId = "{your securityGroupId string}";
        // 出方向
        String direction = "egress";
        // 协议类型。tcp、udp、icmp或IP协议编号(0~255)。
        String protocol = "tcp";
        // IP地址协议类型。取值范围:IPv4,IPv6，默认值为IPv4
        String etherType = "IPv4";
        NeutronCreateSecurityGroupRuleRequest request = new NeutronCreateSecurityGroupRuleRequest();
        NeutronCreateSecurityGroupRuleRequestBody body = new NeutronCreateSecurityGroupRuleRequestBody();
        NeutronCreateSecurityGroupRuleOption securityGroupRulebody = new NeutronCreateSecurityGroupRuleOption();
        securityGroupRulebody.withDirection(NeutronCreateSecurityGroupRuleOption.DirectionEnum.fromValue(direction))
                .withEthertype(NeutronCreateSecurityGroupRuleOption.EthertypeEnum.fromValue(etherType))
                .withProtocol(protocol)
                .withSecurityGroupId(securityGroupId);
        body.withSecurityGroupRule(securityGroupRulebody);
        request.withBody(body);
        try {
            NeutronCreateSecurityGroupRuleResponse response = vpcClient.neutronCreateSecurityGroupRule(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void CreateSecurityGroup(VpcClient vpcClient) {
        NeutronCreateSecurityGroupRequest request = new NeutronCreateSecurityGroupRequest();
        NeutronCreateSecurityGroupRequestBody body = new NeutronCreateSecurityGroupRequestBody();
        NeutronCreateSecurityGroupOption securityGroupbody = new NeutronCreateSecurityGroupOption();
        // 安全组名称
        securityGroupbody.withName("{your securityGroup name}");
        body.withSecurityGroup(securityGroupbody);
        request.withBody(body);
        try {
            NeutronCreateSecurityGroupResponse response = vpcClient.neutronCreateSecurityGroup(request);
            System.out.println(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        logger.error("RequestId: " + e.getRequestId());
        logger.error("ErrorCode: " + e.getErrorCode());
        logger.error("ErrorMsg: " + e.getErrorMsg());
    }
}

```

## 6、修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-03-05 | 1.0 | 文档首次发布 |