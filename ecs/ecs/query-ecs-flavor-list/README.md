## 1、介绍
**查询规格详情和规格扩展信息列表**

[查询规格详情和规格扩展信息列表](https://support.huaweicloud.com/api-ecs/zh-cn_topic_0020212656.html)

**您将学到什么？**

如何通过java版SDK来查询规格详情和规格扩展信息列表

## 2、前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云租户账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 3、endpoint 华为云各服务应用区域和各服务的终端节点，具体请参见[地区和终端节点](https://developer.huaweicloud.com/endpoint)。
- 4、华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。

## 3、SDK获取和安装
您可以通过如下方式获取和安装 SDK： 通过Maven安装项目依赖是使用Java SDK的推荐方法，首先您需要在您的操作系统中下载并安装Maven，安装完成后您只需在Java项目的pom.xml文件加入相应的依赖项即可。
本示例使用ECS SDK：
``` xml
    <dependencies>
        <dependency>
            <groupId>com.huaweicloud.sdk</groupId>
            <artifactId>huaweicloud-sdk-ecs</artifactId>
            <version>3.1.80</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>2.0.5</version>
        </dependency>
    </dependencies>
```


## 4、接口参数说明
关于接口参数的详细说明可参见：

[查询规格详情和规格扩展信息列表](https://support.huaweicloud.com/api-ecs/zh-cn_topic_0020212656.html)


## 5、代码示例

``` java
package com.huawei.ecs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.ecs.v2.EcsClient;

import com.huaweicloud.sdk.ecs.v2.model.ListFlavorsRequest;
import com.huaweicloud.sdk.ecs.v2.model.ListFlavorsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QueryFlavorListDemo {
    private static final Logger logger = LoggerFactory.getLogger(QueryFlavorListDemo.class);

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份认证为例，运行示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // 配置客户端属性
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // 创建认证
        BasicCredentials auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        // 创建EcsClient实例并初始化
        EcsClient ecsClient = EcsClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(new Region("cn-north-4", "https://ecs.cn-north-4.myhuaweicloud.com"))
                .build();
        // 查询规格详情和规格扩展信息列表
        listFlavor(ecsClient);
    }

    public static void listFlavor(EcsClient client) {
        ListFlavorsRequest listFlavorsRequest = new ListFlavorsRequest();
        try {
            ListFlavorsResponse listFlavorResponse = client.listFlavors(listFlavorsRequest);
            logger.info(listFlavorResponse.toString());
            System.out.println(listFlavorResponse);
        } catch (ClientRequestException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("RequestId: " + e.getRequestId());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }
}
```

## 6、修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-02-05 | 1.0 | 文档首次发布 |