package com.huawei.ecs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.ecs.v2.EcsClient;

import com.huaweicloud.sdk.ecs.v2.model.ListFlavorsRequest;
import com.huaweicloud.sdk.ecs.v2.model.ListFlavorsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QueryFlavorListDemo {
    private static final Logger logger = LoggerFactory.getLogger(QueryFlavorListDemo.class);

    public static void main(String[] args) {
        // Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // Configure client properties
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // Create certificate
        BasicCredentials auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        // Create an EcsClient and initialize it
        EcsClient ecsClient = EcsClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(new Region("cn-north-4", "https://ecs.cn-north-4.myhuaweicloud.com"))
                .build();
        // Querying details about flavors and extended flavor information
        listFlavor(ecsClient);
    }

    public static void listFlavor(EcsClient client) {
        ListFlavorsRequest listFlavorsRequest = new ListFlavorsRequest();
        try {
            ListFlavorsResponse listFlavorResponse = client.listFlavors(listFlavorsRequest);
            logger.info(listFlavorResponse.toString());
            System.out.println(listFlavorResponse);
        } catch (ClientRequestException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("RequestId: " + e.getRequestId());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }
}
