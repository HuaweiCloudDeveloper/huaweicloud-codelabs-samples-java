## 1.Introduction
**Querying Details About Flavors and Extended Flavor Information**

[Querying Details About Flavors and Extended Flavor Information](https://support.huaweicloud.com/intl/en-us/api-ecs/en-us_topic_0020212656.html)

**What will you learn?**

How to query details about flavors and extended flavor information by the Java SDK.

## 2.Preconditions
- 1.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.
- 2.You have a Huawei Cloud account and the AK and SK of the account. (On the Huawei Cloud management console, hover the cursor over your account name and select **My Credentials**. In the navigation pane on the left, choose **Access Keys** and view your AK and SK. If you do not have the AK and SK, create them.) For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/usermanual-ca/en-us_topic_0046606340.html).
- 3.Endpoint: Regions and endpoints of HUAWEI CLOUD services. For details, see [Regions and Endpoints](https://developer.huaweicloud.com/intl/en-us/endpoint).
- 4.The **Java JDK version is 1.8** or later.

## 3.SDK Installation
You can obtain and install the SDK in the following ways: Installing project dependencies through Maven is the recommended method for using the Java SDK.
First, you need to download and install Maven in your operating system. After the installation is complete, you only need to add the corresponding dependencies to the pom.xml file of the Java project.
This example uses the ECS SDK:

``` xml
    <dependencies>
        <dependency>
            <groupId>com.huaweicloud.sdk</groupId>
            <artifactId>huaweicloud-sdk-ecs</artifactId>
            <version>3.1.80</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>2.0.5</version>
        </dependency>
    </dependencies>
```

## 4.Interface parameter description
For detailed descriptions of interface parameters:
[Querying Details About Flavors and Extended Flavor Information](https://support.huaweicloud.com/intl/en-us/api-ecs/en-us_topic_0020212656.html)

## 5.Code Sample

``` java
package com.huawei.ecs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.ecs.v2.EcsClient;

import com.huaweicloud.sdk.ecs.v2.model.ListFlavorsRequest;
import com.huaweicloud.sdk.ecs.v2.model.ListFlavorsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QueryFlavorListDemo {
    private static final Logger logger = LoggerFactory.getLogger(QueryFlavorListDemo.class);

    public static void main(String[] args) {
        // Hardcoded or plaintext AK and SK are risky. For security, encrypt your AK and SK and store them in the configuration file or as environment variables.
        // In this sample, the AK and SK are stored as environment variables for identity authentication. Before running this sample, set the HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK environment variables in your environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "{your projectId string}";

        // Configure client properties
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);

        // Create certificate
        BasicCredentials auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        // Create an EcsClient and initialize it
        EcsClient ecsClient = EcsClient.newBuilder()
                .withHttpConfig(config)
                .withCredential(auth)
                .withRegion(new Region("cn-north-4", "https://ecs.cn-north-4.myhuaweicloud.com"))
                .build();
        // Querying details about flavors and extended flavor information
        listFlavor(ecsClient);
    }

    public static void listFlavor(EcsClient client) {
        ListFlavorsRequest listFlavorsRequest = new ListFlavorsRequest();
        try {
            ListFlavorsResponse listFlavorResponse = client.listFlavors(listFlavorsRequest);
            logger.info(listFlavorResponse.toString());
            System.out.println(listFlavorResponse);
        } catch (ClientRequestException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("RequestId: " + e.getRequestId());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }
}

```

## 6. Change History
| Release Date | Issue|   Description  |
|:------------:| :------: | :----------: |
|  2024-02-05  |   1.0    | This issue is the first official release.|