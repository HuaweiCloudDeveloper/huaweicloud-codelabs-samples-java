package com.huawei.codeartsbuild;

import com.huaweicloud.sdk.codeartsbuild.v3.CodeArtsBuildClient;
import com.huaweicloud.sdk.codeartsbuild.v3.model.EnableBuildJobRequest;
import com.huaweicloud.sdk.codeartsbuild.v3.model.EnableBuildJobResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.codeartsbuild.v3.region.CodeArtsBuildRegion;

import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnableBuildJob {

    private static final Logger logger = LoggerFactory.getLogger(EnableBuildJob.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String enableBuildJobAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String enableBuildJobSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(enableBuildJobAk)
                .withSk(enableBuildJobSk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CodeArtsBuildRegion.CN_NORTH_4.
        CodeArtsBuildClient client = CodeArtsBuildClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeArtsBuildRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct the request header and set the ID of the task for constructing request parameters.
        EnableBuildJobRequest request = new EnableBuildJobRequest();
        // ID of the build task.
        // Indicates the 32-bit character string consisting of digits and letters at the end of the browser URL when you edit build task.
        String jobId = "<YOUR JOB ID>";
        request.withJobId(jobId);

        try {
            EnableBuildJobResponse response = client.enableBuildJob(request);
            logger.info("EnableBuildJob: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("EnableBuildJob connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("EnableBuildJob RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("EnableBuildJob ServiceResponseException", e);
        }
    }
}