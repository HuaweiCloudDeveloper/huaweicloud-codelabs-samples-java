### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsBuild/debug?api=ShowHistoryDetails) 中直接运行调试该接口。
2.在本样例中，您可以获取构建历史详情信息接口。
3.获取构建历史详情信息，可以知道构建任务的步骤，每一个构建步骤的执行时间等信息。

### 前置条件
1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
2.已具备开发环境 ，支持Java JDK 1.8及其以上版本。
3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
4.已在CodeArts平台创建项目。
5.已在项目中创建了构建任务。
6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartsbuild”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartsbuild</artifactId>
    <version>3.1.113</version>
</dependency>
```

### 代码示例
``` java
public class ShowHistoryDetails {

    private static final Logger logger = LoggerFactory.getLogger(ShowHistoryDetails.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String showHistoryDetailsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showHistoryDetailsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(showHistoryDetailsAk)
                .withSk(showHistoryDetailsSk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeArtsBuildRegion.CN_NORTH_4
        CodeArtsBuildClient client = CodeArtsBuildClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeArtsBuildRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();

        // 构建请求头，设置请求参数构建的任务ID，构建任务的构建编号
        ShowHistoryDetailsRequest request = new ShowHistoryDetailsRequest();
        // 构建的任务ID，编辑构建任务时，浏览器URL末尾的32位数字、字母组合的字符串。
        String jobId = "<YOUR JOB ID>";
        request.withJobId(jobId);
        // 构建任务的构建编号，从1开始，每次构建递增1。使用时，请替换成实际的构建编号
        Integer buildNumber = 1;
        request.withBuildNumber(buildNumber);
        try {
            ShowHistoryDetailsResponse response = client.showHistoryDetails(request);
            logger.info("ShowHistoryDetails: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ShowHistoryDetails connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ShowHistoryDetails RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ShowHistoryDetails ServiceResponseException", e);
        }
    }
}
```

### 返回结果示例
```
{
 "job_name": "javawebdemo-build",
 "build_number": 1,
 "project_id": "ad164****",
 "project_name": "Scrum-后端技术",
 "parameters": {
  "codeBranch": "master"
 },
 "build_steps": [
  {
   "name": "Maven构建",
   "status": "success",
   "build_time": 11461
  },
  {
   "name": "代码检出",
   "status": "success",
   "build_time": 5992
  },
  {
   "name": "上传软件包到软件发布库",
   "status": "success",
   "build_time": 5383
  },
  {
   "name": "Cache Check",
   "status": "success",
   "build_time": 817
  }
 ]
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-09-09 | 1.0 | 文档首次发布 |