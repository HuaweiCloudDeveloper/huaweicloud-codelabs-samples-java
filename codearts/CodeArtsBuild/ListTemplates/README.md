### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsBuild/debug?api=ListTemplates) 中直接运行调试该接口。
2.在本样例中，您可以查询构建模板。
3.获取构建模板，可以使用构建模板获得的配置信息，创建对应模板的构建任务。

### 前置条件
1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
2.已具备开发环境 ，支持Java JDK 1.8及其以上版本。
3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
4.已在CodeArts平台创建项目。
5.已在项目中创建了构建任务。
6.已在项目中创建了构建任务的模板。
7.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartsbuild”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartsbuild</artifactId>
    <version>3.1.112</version>
</dependency>
```

### 代码示例
``` java
public class ListTemplates {

    private static final Logger logger = LoggerFactory.getLogger(ListTemplates.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listTemplatesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listTemplatesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(listTemplatesAk)
                .withSk(listTemplatesSk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeArtsBuildRegion.CN_NORTH_4
        CodeArtsBuildClient client = CodeArtsBuildClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeArtsBuildRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求
        ListTemplatesRequest request = new ListTemplatesRequest();

        try {
            ListTemplatesResponse response = client.listTemplates(request);
            logger.info("ListTemplates: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListTemplates connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListTemplates RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListTemplates ServiceResponseException", e);
        }
    }
}
```

### 返回结果示例
```
{
 "result": {
  "total_size": 1,
  "items": [
   {
    "public": true,
    "favourite": false,
    "nick_name": "Developer",
    "id": "66d95****",
    "uuid": "c68c****",
    "template": {
     "steps": [
      {
       "properties": {
        "image": "shell4.2.46-git1.8.3-zip6.00",
        "command": "echo 'hello'"
       },
       "module_id": "devcloud2018.codeci_action_20017.action",
       "name": "执行shell命令",
       "version": null,
       "enable": true
      }
     ]
    },
    "type": "codeci",
    "name": "shell-build",
    "create_time": "2024-09-05T06:41:46.753+00:00",
    "domain_id": "c67ba****",
    "weight": 0,
    "user_id": "f1ac4****",
    "user_name": "demouser",
    "domain_name": "demouser",
    "scope": "custom",
    "description": "",
    "tool_type": "",
    "intl_description": {},
    "parameters": [
     {
      "name": "hudson.model.StringParameterDefinition",
      "params": [
       {
        "name": "name",
        "value": "codeBranch",
        "limits": null
       },
       {
        "name": "type",
        "value": "normalparam",
        "limits": null
       },
       {
        "name": "defaultValue",
        "value": "master",
        "limits": null
       },
       {
        "name": "description",
        "value": "代码分支,系统预定义参数",
        "limits": null
       },
       {
        "name": "deletion",
        "value": "false",
        "limits": null
       },
       {
        "name": "defaults",
        "value": "true",
        "limits": null
       },
       {
        "name": "staticVar",
        "value": "false",
        "limits": null
       },
       {
        "name": "sensitiveVar",
        "value": "false",
        "limits": null
       }
      ]
     }
    ],
    "i18n": {}
   }
  ]
 },
 "error": null,
 "status": "success"
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-09-05 | 1.0 | 文档首次发布 |