package com.huawei.codeartsbuild;

import com.huaweicloud.sdk.codeartsbuild.v3.CodeArtsBuildClient;
import com.huaweicloud.sdk.codeartsbuild.v3.model.StopBuildJobResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.codeartsbuild.v3.region.CodeArtsBuildRegion;

import com.huaweicloud.sdk.codeartsbuild.v3.model.StopBuildJobRequest;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StopBuildJob {

    private static final Logger logger = LoggerFactory.getLogger(StopBuildJob.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String stopBuildAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String stopBuildSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(stopBuildAk)
                .withSk(stopBuildSk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeArtsBuildRegion.CN_NORTH_4
        CodeArtsBuildClient client = CodeArtsBuildClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeArtsBuildRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头，设置请求参数构建任务ID，构建任务的构建编号
        StopBuildJobRequest request = new StopBuildJobRequest();
        // 构建的任务ID,编辑构建任务时,浏览器URL末尾的32位数字、字母组合的字符串。
        String jobId = "<YOUR JOB ID>";
        request.withJobId(jobId);
        // 构建任务的构建编号,从1开始,每次构建递增1,要设置成正在执行的构建任务的实际编号
        request.withBuildNo(1);
        try {
            StopBuildJobResponse response = client.stopBuildJob(request);
            logger.info("StopBuildJob: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("StopBuildJob connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("StopBuildJob RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("StopBuildJob ServiceResponseException", e);
        }
    }
}