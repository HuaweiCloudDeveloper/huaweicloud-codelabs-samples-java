### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsBuild/sdk?api=ShowLastHistory) 中直接运行调试该接口。

2.在本样例中，您可以查询指定代码仓库最近一次成功的构建历史。

### 前置条件
1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。

2.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已在CodeArts平台创建项目。

5.已在项目中创建了构建任务，执行过构建任务。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartsbuild”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartsbuild</artifactId>
    <version>3.1.113</version>
</dependency>
```

### 代码示例
``` java
public class ShowLastHistory {
    private static final Logger logger = LoggerFactory.getLogger(ShowLastHistory.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String showLastHistoryAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showLastHistorySk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(showLastHistoryAk)
                .withSk(showLastHistorySk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeArtsBuildRegion.CN_NORTH_4
        CodeArtsBuildClient client = CodeArtsBuildClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeArtsBuildRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头，设置请求参数代码仓库名、CodeArts项目ID
        ShowLastHistoryRequest request = new ShowLastHistoryRequest();
        // 代码仓库名，不支持中文
        String repositoryName = "<YOUR REPOSITORY NAME>";
        request.withRepositoryName(repositoryName);
        // 注意,如下的 projectId 请使用CodeArts对应项目首页，点击某个项目链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/List
        String projectId = "<YOUR PROJECT ID>";
        request.withProjectId(projectId);
        try {
            ShowLastHistoryResponse response = client.showLastHistory(request);
            logger.info("ShowLastHistory: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ShowLastHistory connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ShowLastHistory RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ShowLastHistory ServiceResponseException", e);
        }
    }
}
```

### 返回结果示例
```
{
 "record_id": "20240903.1",
 "job_id": "d06e****",
 "job_name": "javawebdemo-build",
 "build_number": 8,
 "start_time": "2024-09-03T10:04:58+08:00",
 "end_time": "2024-09-03T10:05:55+08:00",
 "result": "SUCCESS",
 "commit_id": "a274****"
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-09-11 | 1.0 | 文档首次发布 |