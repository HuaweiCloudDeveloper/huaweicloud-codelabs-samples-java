package com.huawei.codeartsbuild;

import com.huaweicloud.sdk.codeartsbuild.v3.CodeArtsBuildClient;
import com.huaweicloud.sdk.codeartsbuild.v3.model.ShowLastHistoryRequest;
import com.huaweicloud.sdk.codeartsbuild.v3.model.ShowLastHistoryResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.codeartsbuild.v3.region.CodeArtsBuildRegion;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowLastHistory {
    private static final Logger logger = LoggerFactory.getLogger(ShowLastHistory.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String showLastHistoryAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showLastHistorySk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(showLastHistoryAk)
                .withSk(showLastHistorySk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeArtsBuildRegion.CN_NORTH_4
        CodeArtsBuildClient client = CodeArtsBuildClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeArtsBuildRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头，设置请求参数代码仓库名、CodeArts项目ID
        ShowLastHistoryRequest request = new ShowLastHistoryRequest();
        // 代码仓库名，不支持中文
        String repositoryName = "<YOUR REPOSITORY NAME>";
        request.withRepositoryName(repositoryName);
        // 注意,如下的 projectId 请使用CodeArts对应项目首页，点击某个项目链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/List
        String projectId = "<YOUR PROJECT ID>";
        request.withProjectId(projectId);
        try {
            ShowLastHistoryResponse response = client.showLastHistory(request);
            logger.info("ShowLastHistory: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ShowLastHistory connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ShowLastHistory RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ShowLastHistory ServiceResponseException", e);
        }
    }
}