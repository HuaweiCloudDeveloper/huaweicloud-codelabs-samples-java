### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeArtsBuild/sdk?api=DeleteBuildJob).

2.In this example, you can delete the build task.

3.You can remove build task that are no longer needed from the build system to free up build resources.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The build task has been created in the project.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-codeartsbuild. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartsbuild</artifactId>
    <version>3.1.112</version>
</dependency>

```

### Code example

``` java
package com.huawei.codeartsbuild;

import com.huaweicloud.sdk.codeartsbuild.v3.model.DeleteBuildJobRequest;
import com.huaweicloud.sdk.codeartsbuild.v3.model.DeleteBuildJobResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.codeartsbuild.v3.region.CodeArtsBuildRegion;
import com.huaweicloud.sdk.codeartsbuild.v3.CodeArtsBuildClient;

import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteBuildJob {

    private static final Logger logger = LoggerFactory.getLogger(DeleteBuildJob.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String deleteBuildJobAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String deleteBuildJobSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(deleteBuildJobAk)
                .withSk(deleteBuildJobSk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CodeArtsBuildRegion.CN_NORTH_4.
        CodeArtsBuildClient client = CodeArtsBuildClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeArtsBuildRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct the request header and set the ID of the task for constructing request parameters.
        DeleteBuildJobRequest request = new DeleteBuildJobRequest();
        // ID of the build task.
        // Indicates the 32-bit character string consisting of digits and letters at the end of the browser URL when you edit build task.
        String jobId = "<YOUR JOB ID>";
        request.withJobId(jobId);

        try {
            DeleteBuildJobResponse response = client.deleteBuildJob(request);
            logger.info("DeleteBuildJob: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("DeleteBuildJob connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("DeleteBuildJob RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("DeleteBuildJob ServiceResponseException", e);
        }
    }
}
```

### Example of the returned result

#### DeleteBuildJob

```
{
 "result": {
  "job_id": "fa72a****",
  "project_id": "ad164****"
 },
 "error": null,
 "status": "success"
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/09/04 | 1.0 | This document is released for the first time.