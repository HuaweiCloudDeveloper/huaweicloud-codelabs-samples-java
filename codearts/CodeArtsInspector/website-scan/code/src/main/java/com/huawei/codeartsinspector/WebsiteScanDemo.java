package com.huawei.codeartsinspector;

import com.huaweicloud.sdk.codeartsinspector.v3.CodeArtsInspectorClient;
import com.huaweicloud.sdk.codeartsinspector.v3.model.AuthorizeDomainsRequest;
import com.huaweicloud.sdk.codeartsinspector.v3.model.AuthorizeDomainsRequestBody;
import com.huaweicloud.sdk.codeartsinspector.v3.model.AuthorizeDomainsResponse;
import com.huaweicloud.sdk.codeartsinspector.v3.model.CreateDomainsRequest;
import com.huaweicloud.sdk.codeartsinspector.v3.model.CreateDomainsRequestBody;
import com.huaweicloud.sdk.codeartsinspector.v3.model.CreateDomainsResponse;
import com.huaweicloud.sdk.codeartsinspector.v3.model.CreateTasksRequest;
import com.huaweicloud.sdk.codeartsinspector.v3.model.CreateTasksRequestBody;
import com.huaweicloud.sdk.codeartsinspector.v3.model.CreateTasksResponse;
import com.huaweicloud.sdk.codeartsinspector.v3.model.ShowResultsRequest;
import com.huaweicloud.sdk.codeartsinspector.v3.model.ShowResultsResponse;
import com.huaweicloud.sdk.codeartsinspector.v3.model.ShowTasksRequest;
import com.huaweicloud.sdk.codeartsinspector.v3.model.ShowTasksResponse;
import com.huaweicloud.sdk.codeartsinspector.v3.region.CodeArtsInspectorRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebsiteScanDemo {
    private static final Logger logger = LoggerFactory.getLogger(WebsiteScanDemo.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 待扫描的测试网站
        String domainName = "https://test.myhuaweicloud.com";

        // 创建CodeArtsInspectorClient实例
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        CodeArtsInspectorClient client =
                CodeArtsInspectorClient.newBuilder()
                        .withCredential(auth)
                        .withRegion(CodeArtsInspectorRegion.CN_NORTH_4)
                        .build();

        try {
            // 创建<创建网站资产>请求，并指定要创建的网站和网站的别名
            CreateDomainsRequestBody createDomainsRequestBody = new CreateDomainsRequestBody();
            // 要创建的网站资产
            createDomainsRequestBody.withDomainName(domainName);
            // 网站资产的别名
            createDomainsRequestBody.withAlias("测试网站");
            CreateDomainsRequest createDomainsRequest = new CreateDomainsRequest().withBody(createDomainsRequestBody);
            // 执行创建网站资产请求
            CreateDomainsResponse createDomainsResponse = client.createDomains(createDomainsRequest);
            logger.info(createDomainsResponse.toString());
            if (createDomainsResponse.getHttpStatusCode() != 200
                    || createDomainsResponse.getInfoCode() != CreateDomainsResponse.InfoCodeEnum.SUCCESS) {
                logger.error("Create domain failed");
                return;
            }

            // 创建<认证网站资产>请求，并指定要认证的网站和网站认证的方式
            AuthorizeDomainsRequestBody authorizeDomainsRequestBody = new AuthorizeDomainsRequestBody();
            // 要认证的网站
            authorizeDomainsRequestBody.withDomainName(domainName);
            // 网站认证的方式，此处选择免认证方式
            authorizeDomainsRequestBody.withAuthMode(AuthorizeDomainsRequestBody.AuthModeEnum.FREE);
            AuthorizeDomainsRequest authorizeDomainsRequest =
                    new AuthorizeDomainsRequest().withBody(authorizeDomainsRequestBody);
            // 执行认证网站资产请求
            AuthorizeDomainsResponse authorizeDomainsResponse = client.authorizeDomains(authorizeDomainsRequest);
            logger.info(authorizeDomainsResponse.toString());
            if (authorizeDomainsResponse.getHttpStatusCode() != 200
                    || authorizeDomainsResponse.getInfoCode() != AuthorizeDomainsResponse.InfoCodeEnum.SUCCESS) {
                logger.error("Authorize domain failed");
                return;
            }

            // 创建<创建网站扫描任务并启动>请求，并指定要创建的扫描任务名和待扫描的URL
            CreateTasksRequestBody createTasksRequestBody = new CreateTasksRequestBody();
            // 要创建的扫描任务名
            createTasksRequestBody.withTaskName("测试任务");
            // 待扫描的URL
            createTasksRequestBody.withUrl(domainName);
            CreateTasksRequest createTasksRequest = new CreateTasksRequest().withBody(createTasksRequestBody);
            // 执行创建网站扫描任务并启动请求
            CreateTasksResponse createTasksResponse = client.createTasks(createTasksRequest);
            logger.info(createTasksResponse.toString());
            if (createTasksResponse.getHttpStatusCode() != 200
                    || createTasksResponse.getInfoCode() != CreateTasksResponse.InfoCodeEnum.SUCCESS) {
                logger.error("Create task failed");
                return;
            }

            // 获取扫描任务ID
            String taskId = createTasksResponse.getTaskId();

            // 创建<获取网站扫描任务详情>请求，并指定任务ID
            ShowTasksRequest showTasksRequest = new ShowTasksRequest().withTaskId(taskId);
            // 执行获取网站扫描任务详情请求
            ShowTasksResponse showTasksResponse = client.showTasks(showTasksRequest);
            logger.info(showTasksResponse.toString());
            if (showTasksResponse.getHttpStatusCode() != 200) {
                logger.error("Show task failed");
                return;
            }

            // 获取扫描任务状态，直到扫描状态为成功
            ShowTasksResponse.TaskStatusEnum taskStatus = showTasksResponse.getTaskStatus();
            while (taskStatus != ShowTasksResponse.TaskStatusEnum.SUCCESS
                    && taskStatus != ShowTasksResponse.TaskStatusEnum.FAILURE) {
                Thread.sleep(3000);
                showTasksResponse = client.showTasks(showTasksRequest);
                taskStatus = showTasksResponse.getTaskStatus();
            }

            // 获取网站扫描结果
            // 创建<获取网站扫描结果>请求，并指定任务ID
            ShowResultsRequest showResultsRequest = new ShowResultsRequest().withTaskId(taskId);
            // 执行获取网站扫描结果请求
            ShowResultsResponse showResultsResponse = client.showResults(showResultsRequest);
            logger.info(showResultsResponse.toString());
            if (showResultsResponse.getHttpStatusCode() != 200) {
                logger.error("Show results failed");
            }
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        } catch (InterruptedException e) {
            logger.error(e.toString());
        }
    }
}
