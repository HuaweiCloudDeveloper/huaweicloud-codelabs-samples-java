## 1. 示例简介
华为云提供了漏洞管理服务端SDK，您可以直接集成服务端SDK来调用漏洞管理服务的相关API，从而实现对漏洞管理服务的快速操作。

本示例展示了如何通过java版SDK创建并扫描网站资产以获取漏洞扫描报告。

## 2. 开发前准备
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

## 3. 安装SDK
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartsinspector”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartsinspector</artifactId>
    <version>3.1.59</version>
</dependency>
```

## 4. 示例代码
```java
public static void main(String[] args) {
    // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    String ak = System.getenv("HUAWEICLOUD_SDK_AK");
    String sk = System.getenv("HUAWEICLOUD_SDK_SK");

    // 待扫描的测试网站
    String domainName = "https://test.myhuaweicloud.com";

    // 创建CodeArtsInspectorClient实例
    ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
    CodeArtsInspectorClient client =
            CodeArtsInspectorClient.newBuilder()
                    .withCredential(auth)
                    .withRegion(CodeArtsInspectorRegion.CN_NORTH_4)
                    .build();

    try {
        // 创建<创建网站资产>请求，并指定要创建的网站和网站的别名
        CreateDomainsRequestBody createDomainsRequestBody = new CreateDomainsRequestBody();
        // 要创建的网站资产
        createDomainsRequestBody.withDomainName(domainName);
        // 网站资产的别名
        createDomainsRequestBody.withAlias("测试网站");
        CreateDomainsRequest createDomainsRequest = new CreateDomainsRequest().withBody(createDomainsRequestBody);
        // 执行创建网站资产请求
        CreateDomainsResponse createDomainsResponse = client.createDomains(createDomainsRequest);
        logger.info(createDomainsResponse.toString());
        if (createDomainsResponse.getHttpStatusCode() != 200
                || createDomainsResponse.getInfoCode() != CreateDomainsResponse.InfoCodeEnum.SUCCESS) {
            logger.error("Create domain failed");
            return;
        }
    
        // 创建<认证网站资产>请求，并指定要认证的网站和网站认证的方式
        AuthorizeDomainsRequestBody authorizeDomainsRequestBody = new AuthorizeDomainsRequestBody();
        // 要认证的网站
        authorizeDomainsRequestBody.withDomainName(domainName);
        // 网站认证的方式，此处选择免认证方式
        authorizeDomainsRequestBody.withAuthMode(AuthorizeDomainsRequestBody.AuthModeEnum.FREE);
        AuthorizeDomainsRequest authorizeDomainsRequest =
                new AuthorizeDomainsRequest().withBody(authorizeDomainsRequestBody);
        // 执行认证网站资产请求
        AuthorizeDomainsResponse authorizeDomainsResponse = client.authorizeDomains(authorizeDomainsRequest);
        logger.info(authorizeDomainsResponse.toString());
        if (authorizeDomainsResponse.getHttpStatusCode() != 200
                || authorizeDomainsResponse.getInfoCode() != AuthorizeDomainsResponse.InfoCodeEnum.SUCCESS) {
            logger.error("Authorize domain failed");
            return;
        }
    
        // 创建<创建网站扫描任务并启动>请求，并指定要创建的扫描任务名和待扫描的URL
        CreateTasksRequestBody createTasksRequestBody = new CreateTasksRequestBody();
        // 要创建的扫描任务名
        createTasksRequestBody.withTaskName("测试任务");
        // 待扫描的URL
        createTasksRequestBody.withUrl(domainName);
        CreateTasksRequest createTasksRequest = new CreateTasksRequest().withBody(createTasksRequestBody);
        // 执行创建网站扫描任务并启动请求
        CreateTasksResponse createTasksResponse = client.createTasks(createTasksRequest);
        logger.info(createTasksResponse.toString());
        if (createTasksResponse.getHttpStatusCode() != 200
                || createTasksResponse.getInfoCode() != CreateTasksResponse.InfoCodeEnum.SUCCESS) {
            logger.error("Create task failed");
            return;
        }
    
        // 获取扫描任务ID
        String taskId = createTasksResponse.getTaskId();
    
        // 创建<获取网站扫描任务详情>请求，并指定任务ID
        ShowTasksRequest showTasksRequest = new ShowTasksRequest().withTaskId(taskId);
        // 执行获取网站扫描任务详情请求
        ShowTasksResponse showTasksResponse = client.showTasks(showTasksRequest);
        logger.info(showTasksResponse.toString());
        if (showTasksResponse.getHttpStatusCode() != 200) {
            logger.error("Show task failed");
            return;
        }
    
        // 获取扫描任务状态，直到扫描状态为成功
        ShowTasksResponse.TaskStatusEnum taskStatus = showTasksResponse.getTaskStatus();
        while (taskStatus != ShowTasksResponse.TaskStatusEnum.SUCCESS
                && taskStatus != ShowTasksResponse.TaskStatusEnum.FAILURE) {
            Thread.sleep(3000);
            showTasksResponse = client.showTasks(showTasksRequest);
            taskStatus = showTasksResponse.getTaskStatus();
        }
    
        // 获取网站扫描结果
        // 创建<获取网站扫描结果>请求，并指定任务ID
        ShowResultsRequest showResultsRequest = new ShowResultsRequest().withTaskId(taskId);
        // 执行获取网站扫描结果请求
        ShowResultsResponse showResultsResponse = client.showResults(showResultsRequest);
        logger.info(showResultsResponse.toString());
        if (showResultsResponse.getHttpStatusCode() != 200) {
            logger.error("Show results failed");
        }
    } catch (ClientRequestException | ServerResponseException e) {
        logger.error(String.valueOf(e.getHttpStatusCode()));
        logger.error(e.toString());
    } catch (InterruptedException e) {
        logger.error(e.toString());
    }
}
```

相关参数说明如下所示：
- ak：华为云账号Access Key。
- sk：华为云账号Secret Access Key。
- region：服务所在区域，当前支持北京四。

## 5. 参考
更多信息请参考[漏洞管理服务 CodeArtsInspector](https://support.huaweicloud.com/api-vss/vss_api_04_014.html)

## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-09-15 | 1.0 | 文档首次发布 |