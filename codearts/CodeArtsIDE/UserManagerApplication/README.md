## 1、功能介绍

CodeArts IDE for Java是一个集成开发环境，将文本编辑器和强大的开发工具（如智能代码补全、导航、重构和调试）集成在一起。
 
- 调试热替换：支持在调试模式下运行程序。当程序挂起时，在“运行”和“调试”视图中检查其输出。找到错误并更正它，然后重新运行程序。在Java上下文中，可通过热代码替换功能来动态修改和重新加载类，无需停止调试会话。
 
- 强大的编码辅助能力：CodeArts IDE for Java 可以深入理解代码，提供上下文感知的机器学习辅助补全功能，可以补全单条语句或整行代码。对于代码编辑器中的任何符号，开发者可以查看其定义和相关文档。集成开发环境可高亮显示错误，并让开发者直接在代码编辑器中对错误采取行动。"问题 "视图会列出当前打开的文件中检测到的所有问题。
 
- Java构建工具集成：CodeArts IDE for Java 提供对 Maven 和 Gradle 构建系统的内置支持，包括项目和依赖关系解析，以及运行 Maven 目标和 Gradle 任务的能力。
 
- 丰富的插件生态：CodeArts IDE for Java 不仅支持华为云CodeArts插件市场，还支持Open VSX插件市场，开发者也可以通过安装自己喜爱的插件，把CodeArts IDE变成个人开发的“定制桌面”。华为是Eclipse Open VSX 工作组的发起成员和主要Sponsor之一。

您将学到什么？

Spring Boot和Thymeleaf是目前非常流行的Java开发框架和模板引擎。Spring Boot提供了快速构建Web应用程序的能力，而Thymeleaf则是一种功能强大且易于使用的模板引擎，可以帮助我们在服务器端生成动态的HTML页面。在本文中，我们将使用Spring Boot和Thymeleaf来实现一个完整的用户管理功能，包括增加、删除、修改和查询用户信息。
以及使用CodeArts IDE for Java构建，运行Java工程，并且学到Java语言的一些基础知识，例如Java的一些基本类型。通过Java语言创建，读取，并修改本地文件的能力。

## 2、前置条件

- 已注册华为账号或华为云账号，并且进行实名认证。具体请参见[华为云官网](https://www.huaweicloud.com/intl/zh-cn/)。
- 下载并安装CodeArts IDE for Java，了解IDE的基础功能，并且使用华为云账号登录。具体请参见[CodeArts IDE for Java产品页](https://devcloud.cn-north-4.huaweicloud.com/codeartside/home?product=java)。
- Java语言基础
- 安装Java JDK 1.8及其以上版本
- 引入spring-boot-starter-web、spring-boot-starter-thymeleaf和lombok（可选）相关依赖

## 3、开发时序图

![Alt text](assets/runtime.png)

## 4、关键代码片段

### 4.1、后端实现过程
pojo类：（@Data是lombod注解,旨在不需要写get、set方法了.）
```java
@Data
public class User {
    private String uuid;
    private Integer id;
    private String name;
    private Integer age;
    private String department;

    public User(){}

    public User(Integer id, String name, Integer age, String department){
        this.uuid = UUID.randomUUID().toString();
        this.id = id;
        this.name = name;
        this.age = age;
        this.department = department;
    }
}
```

mapper接口：
```java
public interface IUserMapper {

    public List<User> getUserList();
    public boolean addUser(User user);
    public boolean updateUser(User user);
    public boolean deleteUser(String uuid);

}
```

mapperImpl实现类：
```java
@Component
public class UserMapperImpl implements IUserMapper {

    @Override
    public List<User> getUserList() {
        return UserData.userList;
    }

    @Override
    public boolean addUser(User user) {
        user.setUuid(UUID.randomUUID().toString());
        return UserData.userList.add(user);
    }

    @Override
    public boolean updateUser(User user) {
        Stream<User> userStream = UserData.userList.stream().filter(userItem -> {
            if (user.getId().equals(userItem.getId())) {
                if (user.getId() != null) {
                    userItem.setId(user.getId());
                }
                if ( !StringUtils.isEmpty(user.getName())) {
                    userItem.setName(user.getName());
                }
                if (!StringUtils.isEmpty(user.getDepartment())) {
                    userItem.setDepartment(user.getDepartment());
                }
                if (!StringUtils.isEmpty(user.getAge())) {
                    userItem.setAge(user.getAge());
                }
                return true;
            }
            return false;
        });
        return userStream.count() >= 1;
    }

    @Override
    public boolean deleteUser(String uuid) {
        int sizeBeforeDelete = UserData.userList.size();
        List<User> deletedUsers = UserData.userList.stream().filter(user -> uuid.equals(user.getUuid())).collect(Collectors.toList());
        deletedUsers.forEach(UserData.userList::remove);
        return sizeBeforeDelete != UserData.userList.size();
    }
}
```

service接口：
```java
public interface IUserService {

    public List<User> getUserList();
    public boolean addUser(User user);
    public boolean updateUser(User user);
    public boolean deleteUser(String id);

}
```

serviceImpl实现类：
```java
@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    IUserMapper userMapperImpl;

    @Override
    public List<User> getUserList() {
        return userMapperImpl.getUserList();
    }

    @Override
    public boolean addUser(User user) {
        return userMapperImpl.addUser(user);
    }

    @Override
    public boolean updateUser(User user) {
        return userMapperImpl.updateUser(user);
    }

    @Override
    public boolean deleteUser(String id) {
        return userMapperImpl.deleteUser(id);
    }
}
```

controller控制层：
用户管理接口Controller：
```java
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    IUserService userServiceImpl;

    @GetMapping("/getUserList")
    public List<User> getUserList() {
        return userServiceImpl.getUserList();
    }

    @PostMapping("/addUser")
    public boolean addUser(User user) {
        return userServiceImpl.addUser(user);
    }

    @PostMapping("/updateUser")
    public boolean updateUser(User user) {
        return userServiceImpl.updateUser(user);
    }

    @PostMapping("/deleteUser")
    public boolean deleteUser(String uuid) {
        return userServiceImpl.deleteUser(uuid);
    }
}
```

用户欢迎界面Controller：
```java
@Controller
public class WelcomePageController {

    @RequestMapping("/")
    public String user(){
        return "user";
    }
}
```

用户数据：
```java
public class UserData {
    public static List<User> userList = new ArrayList<>();
   
    public static void initUserData(){
        userList.add(new User(1, "张三", 20, "人力资源部"));
        userList.add(new User(2, "李四", 18, "后勤部"));
        userList.add(new User(3, "王五", 21, "研发部"));
        userList.add(new User(4, "赵六", 25, "产品部"));
    }
}
```

启动类：（每次启动调试需要初始化用户数据）
```java
@SpringBootApplication
public class UserManagerApplication {

    public static void main(String[] args) {
        UserData.initUserData(); // 初始化用户数据
        SpringApplication.run(UserManagerApplication.class, args);
    }

}
```

### 4.2、前端实现过程(thymeleaf模板+html+ajax)
user.html
```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>User Manager</title>
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        $(function () {
            queryData();
        });

        function queryData() {
            // 发送 AJAX 请求
            $.ajax({
                url: 'http://localhost:8080/user/getUserList',
                type: 'GET',
                dataType: 'json',
                success(res) {
                    var html = '';
                    for (var i = 0; i < res.length; i++) {
                        html += '<tr><td>' + res[i].id + '</td>' + '<td>' + res[i].name + '<td>' + res[i].age + '<td>' + res[i].department + '</td><td> <button onclick="del(\'' + res[i].uuid + '\')">删除</button></td></tr>';
                    }
                    // console.log(html);
                    $('#tt').html(html);
                },
                error: function (xhr, status, error) {
                    console.error(error); // 处理错误情况
                }
            });
        }

        function add() {
            const id = document.getElementById('InputId').value;
            const name = document.getElementById('InputName').value;
            const age = document.getElementById('InputAge').value;
            const department = document.getElementById('InputDepartment').value;

            if (!validateId(id)) {
                return;
            }

            $.ajax({
                url: `http://localhost:8080/user/addUser?id=${id}&name=${name}&age=${age}&department=${department}`,
                type: 'post',
                dataType: 'json',
                success: function (res) {
                    queryData();
                    if (res == true) {
                        alert('添加成功');
                    } else {
                        alert('添加失败');
                    }
                },
                error: function (xhr, status, error) {
                    console.error(error);
                }
            });
            document.getElementById('InputId').value = '',
            document.getElementById('InputName').value = '';
            document.getElementById('InputAge').value = '';
            document.getElementById('InputDepartment').value = '';
        }

        function update() {
            const id = document.getElementById('updateInputId').value;
            const name = document.getElementById('updateInputName').value;
            const age = document.getElementById('updateInputAge').value;
            const department = document.getElementById('updateInputDepartment').value;

            if (!validateId(id)) {
                return;
            }

            $.ajax({
                url: `http://localhost:8080/user/updateUser?id=${id}&name=${name}&age=${age}&department=${department}`,
                type: 'post',
                dataType: 'json',
                success: function (res) {
                    queryData();
                    if (res == true) {
                        alert('修改成功');
                    } else {
                        alert('修改失败');
                    }
                },
                error: function (xhr, status, error) {
                    console.error(error);
                }
            });
            document.getElementById('updateInputId').value = '',
            document.getElementById('updateInputName').value = '';
            document.getElementById('updateInputAge').value = '';
            document.getElementById('updateInputDepartment').value = '';
        }

        function del(uuid) {
            $.ajax({
                url: `http://localhost:8080/user/deleteUser?uuid=${uuid}`,
                type: 'post',
                dataType: 'json',
                success: function (res) {
                    queryData();
                    if (res == true) {
                        alert('删除成功');
                    } else {
                        alert('删除失败');
                    }
                },
                error: function (xhr, status, error) {
                    console.error(error);
                }
            });
        }

        function validateId(id) {
            if (id == '') {
                alert("Id值不能为空"); //空值校验
                return false;
            }
            return true;
        }
    </script>
</head>

<body>
    <div class="header">
        <!--<button onclick="queryData()">查询</button>-->
        <div class="items">
            <input type="number" value="" placeholder="请输入id" id="InputId" />
            <input type="text" value="" placeholder="请输入名称" id="InputName">
            <input type="number" value="" placeholder="请输入年龄" id="InputAge">
            <input type="text" value="" placeholder="请输入部门" id="InputDepartment">
            <button onclick="add()">添加</button>
        </div>
        <div class="items">
            <input type="number" value="" placeholder="请输入id" id="updateInputId" />
            <input type="text" value="" placeholder="请输入名称" id="updateInputName">
            <input type="number" value="" placeholder="请输入年龄" id="updateInputAge">
            <input type="text" value="" placeholder="请输入部门" id="updateInputDepartment">
            <button onclick="update()">修改</button>
        </div>
    </div>
    <div class="table">
        <table>
            <thead>
                <tr>
                    <th>用户Id</th>
                    <th>姓名</th>
                    <th>年龄</th>
                    <th>部门</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody id="tt">
            </tbody>
        </table>
    </div>
</body>
<style>
    table {
        margin: 20px;
        border-collapse: collapse;
    }

    th {
        background-color: #cccccc;
    }

    tr {
        text-align: center;
        border: 1px solid #ccc;
    }

    td {
        padding: 20px;
        border: 1px solid #ccc;
    }

    button {
        background-color: white;
        width: 70px;
        border-radius: 20px;
        border: 1px solid #ccc;
    }

    button :hover {
        background-color: #cccccc;
    }

    .header {
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
    }

    .items {
        display: grid;
        margin-left: 10px;
    }

    .table {
        display: flex;
        justify-content: center;
    }
</style>

</html>
```

### 4.3、相关配置文件：
相关的依赖信息如下，引入spring-boot-starter-web、spring-boot-starter-thymeleaf和lombok（可选）相关依赖：
```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <scope>annotationProcessor</scope>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-thymeleaf</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter</artifactId>
    </dependency>

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <scope>test</scope>
    </dependency>
</dependencies>
```

application.properties：
```javascript
spring.application.name=user-manager
server.port=8080
```

application.yml
```yml
thymeleaf:
  prefix:
    classpath: /templates   # 访问template下的html文件需要配置模板，映射
```

### 4.4、代码解析
在html代码中,我们主要利用onclick点击事件和jquery框架中的ajax交互。
jquery中ajax的语法格式
```javascript
$.ajax({
    url: 接口名,
    type: 'get',
    dataType: 'json',
    success: function(res) {
        queryData();
    },
    error: function(xhr, status, error) {
        console.error(error); // 处理错误情况
    }
});
```

## 5、输出结果示例
实现截图
![Alt text](assets/result.gif)