package com.huawei.dao.impl;

import com.huawei.dao.IUserMapper;

import java.util.List;
import com.huawei.datas.UserData;
import com.huawei.pojo.User;

import java.util.UUID;
import java.util.stream.Stream;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import java.util.stream.Collectors;

@Component
public class UserMapperImpl implements IUserMapper {

    @Override
    public List<User> getUserList() {
        return UserData.userList;
    }

    @Override
    public boolean addUser(User user) {
        user.setUuid(UUID.randomUUID().toString());
        return UserData.userList.add(user);
    }

    @Override
    public boolean updateUser(User user) {
        Stream<User> userStream = UserData.userList.stream().filter(userItem -> {
            if (user.getId().equals(userItem.getId())) {
                if (user.getId() != null) {
                    userItem.setId(user.getId());
                }
                if ( !StringUtils.isEmpty(user.getName())) {
                    userItem.setName(user.getName());
                }
                if (!StringUtils.isEmpty(user.getDepartment())) {
                    userItem.setDepartment(user.getDepartment());
                }
                if (!StringUtils.isEmpty(user.getAge())) {
                    userItem.setAge(user.getAge());
                }
                return true;
            }
            return false;
        });
        return userStream.count() >= 1;
    }

    @Override
    public boolean deleteUser(String uuid) {
        int sizeBeforeDelete = UserData.userList.size();
        List<User> deletedUsers = UserData.userList.stream().filter(user -> uuid.equals(user.getUuid())).collect(Collectors.toList());
        deletedUsers.forEach(UserData.userList::remove);
        return sizeBeforeDelete != UserData.userList.size();
    }
}
