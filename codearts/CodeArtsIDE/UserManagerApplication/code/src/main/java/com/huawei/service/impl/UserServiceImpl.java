package com.huawei.service.impl;

import com.huawei.dao.IUserMapper;
import com.huawei.pojo.User;
import com.huawei.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    IUserMapper userMapperImpl;

    @Override
    public List<User> getUserList() {
        return userMapperImpl.getUserList();
    }

    @Override
    public boolean addUser(User user) {
        return userMapperImpl.addUser(user);
    }

    @Override
    public boolean updateUser(User user) {
        return userMapperImpl.updateUser(user);
    }

    @Override
    public boolean deleteUser(String id) {
        return userMapperImpl.deleteUser(id);
    }
}
