package com.huawei;

import com.huawei.datas.UserData;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserManagerApplication {

    public static void main(String[] args) {
        UserData.initUserData(); // 初始化用户数据
        SpringApplication.run(UserManagerApplication.class, args);
    }

}
