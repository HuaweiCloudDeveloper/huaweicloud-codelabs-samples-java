package com.huawei.datas;

import com.huawei.pojo.User;

import java.util.List;
import java.util.ArrayList;

public class UserData {
    public static List<User> userList = new ArrayList<>();
    
    public static void initUserData() {
        userList.add(new User(1, "张三", 20, "人力资源部"));
        userList.add(new User(2, "李四", 18, "后勤部"));
        userList.add(new User(3, "王五", 21, "研发部"));
        userList.add(new User(4, "赵六", 25, "产品部"));
    }
}
