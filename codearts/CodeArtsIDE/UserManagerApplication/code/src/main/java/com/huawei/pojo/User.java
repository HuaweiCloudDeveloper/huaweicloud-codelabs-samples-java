package com.huawei.pojo;

import lombok.Data;
import java.util.UUID;

@Data
public class User {
    private String uuid;
    private Integer id;
    private String name;
    private Integer age;
    private String department;

    public User(){}

    public User(Integer id, String name, Integer age, String department) {
        this.uuid = UUID.randomUUID().toString();
        this.id = id;
        this.name = name;
        this.age = age;
        this.department = department;
    }
}
