package com.huawei.service;

import com.huawei.pojo.User;

import java.util.List;

public interface IUserService {

    public List<User> getUserList();
    public boolean addUser(User user);
    public boolean updateUser(User user);
    public boolean deleteUser(String id);

}
