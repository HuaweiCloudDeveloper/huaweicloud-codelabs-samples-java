package com.huawei.controller;

import com.huawei.pojo.User;
import com.huawei.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    IUserService userServiceImpl;

    @GetMapping("/getUserList")
    public List<User> getUserList() {
        return userServiceImpl.getUserList();
    }

    @PostMapping("/addUser")
    public boolean addUser(User user) {
        return userServiceImpl.addUser(user);
    }

    @PostMapping("/updateUser")
    public boolean updateUser(User user) {
        return userServiceImpl.updateUser(user);
    }

    @PostMapping("/deleteUser")
    public boolean deleteUser(String uuid) {
        return userServiceImpl.deleteUser(uuid);
    }
}
