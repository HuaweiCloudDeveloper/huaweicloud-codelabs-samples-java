package com.huawei.dao;

import com.huawei.pojo.User;

import java.util.List;

public interface IUserMapper {

    public List<User> getUserList();
    public boolean addUser(User user);
    public boolean updateUser(User user);
    public boolean deleteUser(String uuid);

}
