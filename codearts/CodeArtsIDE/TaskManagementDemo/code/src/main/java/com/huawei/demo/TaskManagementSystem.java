package com.huawei.demo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;
import java.awt.Desktop;
import java.util.ArrayList;
import java.util.Scanner;

public class TaskManagementSystem {
    public static void main(String[] args) {
        String filePath = "tasks.txt";
        String password = "password123";

        TaskManager taskManager = new TaskManager(filePath, password);
        taskManager.loadFromFile();

        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Enter password: ");
            String enteredPassword = scanner.nextLine();

            if (taskManager.login(enteredPassword)) {
                boolean running = true;

                while (running) {
                    System.out.println("Task Management System");
                    System.out.println("1. Add Task");
                    System.out.println("2. Modify Task Status");
                    System.out.println("3. Modify Task Description");
                    System.out.println("4. Delete Task");
                    System.out.println("5. Display Tasks");
                    System.out.println("6. Modify Password");
                    System.out.println("7. Open File");
                    System.out.println("8. Exit");
                    System.out.print("Enter your choice: ");
                    int choice = scanner.nextInt();
                    scanner.nextLine(); // Consume newline character

                    switch (choice) {
                        case 1:
                            System.out.print("Enter task description: ");
                            String description = scanner.nextLine();
                            taskManager.addTask(description);
                            break;
                        case 2:
                            taskManager.displayTasks();
                            System.out.print("Enter task index to modify: ");
                            int index = scanner.nextInt();
                            scanner.nextLine(); // Consume newline character
                            System.out.println("Select task status:");
                            System.out.println("1. Not Started");
                            System.out.println("2. In Progress");
                            System.out.println("3. Completed");
                            System.out.print("Enter status choice: ");
                            int statusChoice = scanner.nextInt();
                            scanner.nextLine(); // Consume newline character
                            String status;
                            switch (statusChoice) {
                                case 1:
                                    status = "Not Started";
                                    break;
                                case 2:
                                    status = "In Progress";
                                    break;
                                case 3:
                                    status = "Completed";
                                    break;
                                default:
                                    System.out.println("Invalid status choice. Task status not modified.");
                                    continue;
                            }
                            taskManager.modifyTaskStatus(index - 1, status);
                            break;
                        case 3:
                            taskManager.displayTasks();
                            System.out.print("Enter task index to modify: ");
                            index = scanner.nextInt();
                            scanner.nextLine(); // Consume newline character
                            System.out.print("Enter new task description: ");
                            String newDescription = scanner.nextLine();
                            taskManager.modifyTaskDescription(index - 1, newDescription);
                            break;
                        case 4:
                            taskManager.displayTasks();
                            System.out.print("Enter task index to delete: ");
                            index = scanner.nextInt();
                            scanner.nextLine(); // Consume newline character
                            taskManager.deleteTask(index - 1);
                            break;
                        case 5:
                            taskManager.displayTasks();
                            break;
                        case 6:
                            System.out.print("Enter new password: ");
                            String newPassword = scanner.nextLine();
                            taskManager.modifyPassword(newPassword);
                            break;
                        case 7:
                            taskManager.openFile();
                            break;
                        case 8:
                            running = false;
                            break;
                        default:
                            System.out.println("Invalid choice. Please try again.");
                    }
                }
            } else {
                System.out.println("Incorrect password. Access denied.");
            }
        }
    }

    public static class Task {
        String description;
        String status;
    
        public Task(String description, String status) {
            this.description = description;
            this.status = status;
        }
    }

    public static class TaskManager {
        private String filePath;
        private String password;
        private ArrayList<Task> taskList;
    
        public TaskManager(String filePath, String password) {
            this.filePath = filePath;
            this.password = password;
            this.taskList = new ArrayList<>();
        }
    
        // 用户可以添加任务以及任务描述
        public void addTask(String description) {
            Task task = new Task(description, "Not Started");
            taskList.add(task);
            saveToFile();
            System.out.println("Task added successfully.");
        }
    
        // 修改任务进度，分为Not Started, In Progress, Finished
        public void modifyTaskStatus(int index, String status) {
            if (index >= 0 && index < taskList.size()) {
                Task task = taskList.get(index);
                task.status = status;
                saveToFile();
                System.out.println("Task status modified successfully.");
            } else {
                System.out.println("Invalid task index.");
            }
        }
    
        // 修改任务以及任务描述
        public void modifyTaskDescription(int index, String description) {
            if (index >= 0 && index < taskList.size()) {
                Task task = taskList.get(index);
                task.description = description;
                saveToFile();
                System.out.println("Task description modified successfully.");
            } else {
                System.out.println("Invalid task index.");
            }
        }
    
        // 该功能可以删除文件中的任务
        public void deleteTask(int index) {
            if (index >= 0 && index < taskList.size()) {
                taskList.remove(index);
                saveToFile();
                System.out.println("Task deleted successfully.");
            } else {
                System.out.println("Invalid task index.");
            }
        }
    
        // 该功能可以展示文件中所有的任务
        public void displayTasks() {
            if (taskList.isEmpty()) {
                System.out.println("No tasks found.");
            } else {
                System.out.println("Tasks:");
                for (int i = 0; i < taskList.size(); i++) {
                    Task task = taskList.get(i);
                    System.out.println((i + 1) + ". " + task.description + " - " + task.status);
                }
            }
        }
    
        // 将任务写入filePath指定的文件中。它使用FileWriter将任务描述和状态写入文件。
        private void saveToFile() {
            try (FileWriter writer = new FileWriter(filePath)) {
                for (Task task : taskList) {
                    writer.write(task.description + "," + task.status + "\n");
                }
                System.out.println("Tasks saved successfully.");
            } catch (IOException e) {
                System.out.println("Error saving tasks.");
            }
        }
    
        // 方法从filePath指定的文件中读取任务，并使用加载的任务填充任务列表。
        // 它使用FileReader和BufferedReader逐行读取文件并将每行拆分为描述和状态。
        public void loadFromFile() {
            try (FileReader reader = new FileReader(filePath);
                BufferedReader bufferedReader = new BufferedReader(reader)) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    String[] data = line.split(",");
                    String description = data[0];
                    String status = data[1];
                    Task task = new Task(description, status);
                    taskList.add(task);
                }
                System.out.println("Tasks loaded successfully.");
            } catch (IOException e) {
                System.out.println("Error loading tasks.");
            }
        }
    
        // 将输入的密码与存储的密码进行比较，如果匹配则返回true 。
        public boolean login(String enteredPassword) {
            return password.equals(enteredPassword);
        }
    
        // 更新密码并将其保存到文件中。
        public void modifyPassword(String newPassword) {
            password = newPassword;
            saveToFile();
            System.out.println("Password modified successfully.");
        }
    
        // 使用与文件类型关联的默认应用程序打开由filePath指定的文件。
        public void openFile() {
            try {
                File file = new File(filePath);
                Desktop.getDesktop().open(file);
            } catch (IOException e) {
                System.out.println("Error opening file.");
            }
        }
    }
}
