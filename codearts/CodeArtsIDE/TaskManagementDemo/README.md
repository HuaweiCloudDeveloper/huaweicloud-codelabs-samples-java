## 1、功能介绍

CodeArts IDE for Java是一个集成开发环境，将文本编辑器和强大的开发工具（如智能代码补全、导航、重构和调试）集成在一起。

- 调试热替换：支持在调试模式下运行程序。当程序挂起时，在“运行”和“调试”视图中检查其输出。找到错误并更正它，然后重新运行程序。在Java上下文中，可通过热代码替换功能来动态修改和重新加载类，无需停止调试会话。

- 强大的编码辅助能力：CodeArts IDE for Java 可以深入理解代码，提供上下文感知的机器学习辅助补全功能，可以补全单条语句或整行代码。对于代码编辑器中的任何符号，开发者可以查看其定义和相关文档。集成开发环境可高亮显示错误，并让开发者直接在代码编辑器中对错误采取行动。"问题 "视图会列出当前打开的文件中检测到的所有问题。

- Java构建工具集成：CodeArts IDE for Java 提供对 Maven 和 Gradle 构建系统的内置支持，包括项目和依赖关系解析，以及运行 Maven 目标和 Gradle 任务的能力。

- 丰富的插件生态：CodeArts IDE for Java 不仅支持华为云CodeArts插件市场，还支持Open VSX插件市场，开发者也可以通过安装自己喜爱的插件，把CodeArts IDE变成个人开发的“定制桌面”。华为是Eclipse Open VSX 工作组的发起成员和主要Sponsor之一。

您将学到什么？

此工程将以CodeArts IDE for Java为开发环境，目的是为了提供一个基本任务管理系统，具有添加任务、修改任务状态和描述、删除任务、显示任务、修改密码、打开任务文件等功能。您也可以在此示例基础上添加可视化的操作流程。以及使用CodeArts IDE for Java构建，运行Java工程，并且学到Java语言的一些基础知识，例如Java的一些基本类型。通过Java语言创建，读取，并修改本地文件的能力。

## 2、前置条件

- 已注册华为账号或华为云账号，并且进行实名认证。具体请参见[华为云官网](https://www.huaweicloud.com/intl/zh-cn/)。
- 下载并安装CodeArts IDE for Java，了解IDE的基础功能，并且使用华为云账号登录。具体请参见[CodeArts IDE for Java产品页](https://devcloud.cn-north-4.huaweicloud.com/codeartside/home?product=java)。
- Java语言基础
- 安装Java JDK 1.8及其以上版本

## 3、开发时序图

![image](assets/runtime.png)

## 4、关键代码片段

### 4.1、创建Task类
Task类表示一个任务，它有两个属性：描述和状态。它有一个构造函数来初始化这些属性。
```java
public static class Task {
    String description;
    String status;

    public Task(String description, String status) {
        this.description = description;
        this.status = status;
    }
}
```

### 4.2、创建TaskManager类
TaskManager类负责管理任务。它有私有实例变量 filePath 和 password，用于存储任务管理器的文件路径和密码。它还有一个名为 taskList 的 ArrayList<Task> 用来存储任务。
TaskManager类提供了各种对任务进行操作的方法，如添加任务、修改任务状态、修改任务描述、删除任务、显示任务、将任务保存到文件、从文件加载任务、使用密码登录等、修改密码、打开文件。
```java
public static class TaskManager {
    private String filePath;
    private String password;
    private ArrayList<Task> taskList;

    public TaskManager(String filePath, String password) {
        this.filePath = filePath;
        this.password = password;
        this.taskList = new ArrayList<>();
    }

    // 用户可以添加任务以及任务描述
    public void addTask(String description) {
        Task task = new Task(description, "Not Started");
        taskList.add(task);
        saveToFile();
        System.out.println("Task added successfully.");
    }

    // 修改任务进度，分为Not Started, In Progress, Finished
    public void modifyTaskStatus(int index, String status) {
        if (index >= 0 && index < taskList.size()) {
            Task task = taskList.get(index);
            task.status = status;
            saveToFile();
            System.out.println("Task status modified successfully.");
        } else {
            System.out.println("Invalid task index.");
        }
    }

    // 修改任务以及任务描述
    public void modifyTaskDescription(int index, String description) {
        if (index >= 0 && index < taskList.size()) {
            Task task = taskList.get(index);
            task.description = description;
            saveToFile();
            System.out.println("Task description modified successfully.");
        } else {
            System.out.println("Invalid task index.");
        }
    }

    // 该功能可以删除文件中的任务
    public void deleteTask(int index) {
        if (index >= 0 && index < taskList.size()) {
            taskList.remove(index);
            saveToFile();
            System.out.println("Task deleted successfully.");
        } else {
            System.out.println("Invalid task index.");
        }
    }

    // 该功能可以展示文件中所有的任务
    public void displayTasks() {
        if (taskList.isEmpty()) {
            System.out.println("No tasks found.");
        } else {
            System.out.println("Tasks:");
            for (int i = 0; i < taskList.size(); i++) {
                Task task = taskList.get(i);
                System.out.println((i + 1) + ". " + task.description + " - " + task.status);
            }
        }
    }

    // 将任务写入filePath指定的文件中。它使用FileWriter将任务描述和状态写入文件。
    private void saveToFile() {
        try (FileWriter writer = new FileWriter(filePath)) {
            for (Task task : taskList) {
                writer.write(task.description + "," + task.status + "\n");
            }
            System.out.println("Tasks saved successfully.");
        } catch (IOException e) {
            System.out.println("Error saving tasks.");
        }
    }

    // 方法从filePath指定的文件中读取任务，并使用加载的任务填充任务列表。
    // 它使用FileReader和BufferedReader逐行读取文件并将每行拆分为描述和状态。
    public void loadFromFile() {
        try (FileReader reader = new FileReader(filePath);
            BufferedReader bufferedReader = new BufferedReader(reader)) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] data = line.split(",");
                String description = data[0];
                String status = data[1];
                Task task = new Task(description, status);
                taskList.add(task);
            }
            System.out.println("Tasks loaded successfully.");
        } catch (IOException e) {
            System.out.println("Error loading tasks.");
        }
    }

    // 将输入的密码与存储的密码进行比较，如果匹配则返回true 。
    public boolean login(String enteredPassword) {
        return password.equals(enteredPassword);
    }

    // 更新密码并将其保存到文件中。
    public void modifyPassword(String newPassword) {
        password = newPassword;
        saveToFile();
        System.out.println("Password modified successfully.");
    }

    // 使用与文件类型关联的默认应用程序打开由filePath指定的文件。
    public void openFile() {
        try {
            File file = new File(filePath);
            Desktop.getDesktop().open(file);
        } catch (IOException e) {
            System.out.println("Error opening file.");
        }
    }
}
```

### 4.3、创建TaskManagementSystem类
TaskManagementSystem类是程序的入口点。它创建一个TaskManager实例，从文件加载任务，并提示用户输入密码（初始密码为：password123），用户也可以自行设定密码。如果输入的密码与存储的密码匹配，则会向用户显示一个菜单，允许他们对任务执行各种操作。从控制台读取用户的选择，并根据该选择调用TaskManager类的相应方法来执行所需的操作。程序将继续运行，直到用户选择退出。
```java
public class TaskManagementSystem {
    public static void main(String[] args) {
        String filePath = "tasks.txt";
        String password = "password123";

        TaskManager taskManager = new TaskManager(filePath, password);
        taskManager.loadFromFile();

        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Enter password: ");
            String enteredPassword = scanner.nextLine();

            if (taskManager.login(enteredPassword)) {
                boolean running = true;

                while (running) {
                    System.out.println("Task Management System");
                    System.out.println("1. Add Task");
                    System.out.println("2. Modify Task Status");
                    System.out.println("3. Modify Task Description");
                    System.out.println("4. Delete Task");
                    System.out.println("5. Display Tasks");
                    System.out.println("6. Modify Password");
                    System.out.println("7. Open File");
                    System.out.println("8. Exit");
                    System.out.print("Enter your choice: ");
                    int choice = scanner.nextInt();
                    scanner.nextLine(); // Consume newline character

                    switch (choice) {
                        case 1:
                            System.out.print("Enter task description: ");
                            String description = scanner.nextLine();
                            taskManager.addTask(description);
                            break;
                        case 2:
                            taskManager.displayTasks();
                            System.out.print("Enter task index to modify: ");
                            int index = scanner.nextInt();
                            scanner.nextLine(); // Consume newline character
                            System.out.println("Select task status:");
                            System.out.println("1. Not Started");
                            System.out.println("2. In Progress");
                            System.out.println("3. Completed");
                            System.out.print("Enter status choice: ");
                            int statusChoice = scanner.nextInt();
                            scanner.nextLine(); // Consume newline character
                            String status;
                            switch (statusChoice) {
                                case 1:
                                    status = "Not Started";
                                    break;
                                case 2:
                                    status = "In Progress";
                                    break;
                                case 3:
                                    status = "Completed";
                                    break;
                                default:
                                    System.out.println("Invalid status choice. Task status not modified.");
                                    continue;
                            }
                            taskManager.modifyTaskStatus(index - 1, status);
                            break;
                        case 3:
                            taskManager.displayTasks();
                            System.out.print("Enter task index to modify: ");
                            index = scanner.nextInt();
                            scanner.nextLine(); // Consume newline character
                            System.out.print("Enter new task description: ");
                            String newDescription = scanner.nextLine();
                            taskManager.modifyTaskDescription(index - 1, newDescription);
                            break;
                        case 4:
                            taskManager.displayTasks();
                            System.out.print("Enter task index to delete: ");
                            index = scanner.nextInt();
                            scanner.nextLine(); // Consume newline character
                            taskManager.deleteTask(index - 1);
                            break;
                        case 5:
                            taskManager.displayTasks();
                            break;
                        case 6:
                            System.out.print("Enter new password: ");
                            String newPassword = scanner.nextLine();
                            taskManager.modifyPassword(newPassword);
                            break;
                        case 7:
                            taskManager.openFile();
                            break;
                        case 8:
                            running = false;
                            break;
                        default:
                            System.out.println("Invalid choice. Please try again.");
                    }
                }
            } else {
                System.out.println("Incorrect password. Access denied.");
            }
        }
    }
}
```

## 5、输出结果示例

### 5.1、运行代码
```txt
Enter password: password123
Task Management System
1. Add Task
2. Modify Task Status
3. Modify Task Description
4. Delete Task
5. Display Tasks
6. Modify Password
7. Open File
8. Exit
Enter your choice: 
```

### 5.2、提示输入错误选项
```txt
Task Management System
1. Add Task
2. Modify Task Status
3. Modify Task Description
4. Delete Task
5. Display Tasks
6. Modify Password
7. Open File
8. Exit
Enter your choice: 9
Invalid choice. Please try again.
```

### 5.3、选择Add Task
添加任务，并且会在tasks.txt文件中保存（如果tasks.txt文件未在指定路径中创建，则会新建该文件）
```txt
Enter your choice: 1
Enter task description: task1
Tasks saved successfully.
Task added successfully.
```

### 5.4、选择Modify Task Status（修改任务状态）
```txt
Enter your choice: 2
Tasks:
1. task1 - Not Started
Enter task index to modify: 1
Select task status:
1. Not Started
2. In Progress
3. Completed
Enter status choice: 2
Tasks saved successfully.
Task status modified successfully.
```

### 5.5、选择Modify Task Description（修改任务描述）
```txt
Enter your choice: 3
Tasks:
1. task1 - In Progress
2. task2 - Not Started
3. task3 - Not Started
4. task4 - Not Started
Enter task index to modify: 4
Enter new task description: task10
Tasks saved successfully.
Task description modified successfully.
```

### 5.6、选择Delete Task（删除任务）
```txt
Enter your choice: 4
Tasks:
1. task1 - In Progress
2. task2 - Not Started
3. task3 - Not Started
4. task10 - Not Started
Enter task index to delete: 4
Tasks saved successfully.
Task deleted successfully.
```

### 5.7、选择Display Tasks（展示所有任务）
```txt
Enter your choice: 5
Tasks:
1. task1 - In Progress
2. task2 - Not Started
3. task3 - Not Started
```

### 5.8、选择Modify Password（修改登录密码）
```txt
Enter your choice: 6
Enter new password: 123
Tasks saved successfully.
Password modified successfully.
```

### 5.9、选择打开tasks.txt文件，则会打开已创建的文件，该文件中会展示所有记录的信息。

### 5.10、选择Exit则会退出该系统，并且下次登录需要重新使用密码。

## 6、修订记录

|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2023-10-16 |   1.0    | 文档首次发布 |