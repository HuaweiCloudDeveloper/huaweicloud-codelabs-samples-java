package com.huawei.util;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.ocr.v1.OcrClient;
import com.huaweicloud.sdk.ocr.v1.model.LicensePlateRequestBody;
import com.huaweicloud.sdk.ocr.v1.model.RecognizeLicensePlateRequest;
import com.huaweicloud.sdk.ocr.v1.model.RecognizeLicensePlateResponse;
import com.huaweicloud.sdk.ocr.v1.region.OcrRegion;

/**
 * 实验者需要实现的类
 *
 * @since 2023-06-26
 */
public class DiyUtil {

    /**
     * 实验者利用华为云OCR服务SDK对云端车牌图片识别
     *
     * @param pictureUrl  公网车牌图片Url
     * @param myAk        用户Access Key Id
     * @param mySk        用户Serret Key Id
     * @param myRegion    开通车牌识别服务的Region，如 "cn-north-4"
     * @return 返回response
     */
    public static RecognizeLicensePlateResponse debugByUrl(String pictureUrl, String myAk, String mySk, String myRegion) {
        OcrClient client = buildOcrClient(myAk, mySk, myRegion);
        /**
         * --------------------------------------
         * 输入”recognizeLicensePlate“，补全后将形参传入
         *  --------------------------------------
         */
        RecognizeLicensePlateRequest request = new RecognizeLicensePlateRequest();
        LicensePlateRequestBody body = new LicensePlateRequestBody();
        body.setUrl(pictureUrl);
        request.setBody(body);
        try {
            RecognizeLicensePlateResponse response = client.recognizeLicensePlate(request);
            System.out.println(response.toString());
            return response;
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        /**
         * --------------------------------------
         * 以上部分是通过SDK代码补全助手实现
         *  --------------------------------------
         */
        return null;
    }

    /**
     * 实验者利用华为云OCR服务SDK对本地车牌图片识别
     *
     * @param picturePath 传入本地图片路径
     * @param myAk        用户Access Key Id
     * @param mySk        用户Serret Key Id
     * @param myRegion    开通车牌识别服务的Region，如 "cn-north-4"
     * @return 返回response
     */
    public static RecognizeLicensePlateResponse debugByLocalPath(String picturePath, String myAk, String mySk, String myRegion) {
        String base64 = DiyUtil.base64Encode(picturePath);
        OcrClient client = buildOcrClient(myAk, mySk, myRegion);
        /**
         * --------------------------------------
         * 输入”recognizeLicensePlate“，补全后将形参传入,需要调用base64Encode方法对本地图片进行编码
         *  --------------------------------------
         */
        RecognizeLicensePlateRequest request = new RecognizeLicensePlateRequest();
        LicensePlateRequestBody body = new LicensePlateRequestBody();
        body.withImage(base64);
        request.withBody(body);
        try {
            RecognizeLicensePlateResponse response = client.recognizeLicensePlate(request);
            System.out.println(response.toString());
            return response;
        } catch (ConnectionException |  RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return null;
    }

    /**
     * 实验者完成对本地图片的Base64编码
     *
     * @param picturePath 传入本地图片路径
     * @return 返回base64格式字符串
     */
    public static String base64Encode(String picturePath) {
        String res = "";
        /**
         * --------------------------------------
         * 这里实现图片转base64编码
         *  --------------------------------------
         */
        res = Base64Util.base64ToString(picturePath);
        /**
         * --------------------------------------
         *  上部分是实现base64编码，具体实现参考Base64Util类
         *  --------------------------------------
         */
        System.out.println("base64Encode result: " + res);
        return res;

    }

    private static OcrClient buildOcrClient(String ak, String sk, String regionId) {
        return OcrClient.newBuilder()
            .withCredential(auth(ak, sk))
            .withRegion(OcrRegion.valueOf(regionId))
            .build();
    }

    private static BasicCredentials auth(String ak, String sk) {
        return new BasicCredentials().withAk(ak).withSk(sk);
    }
}
