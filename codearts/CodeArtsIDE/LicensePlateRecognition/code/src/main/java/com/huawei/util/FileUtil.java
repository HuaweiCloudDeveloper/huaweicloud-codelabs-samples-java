package com.huawei.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * 文件读取工具类
 */
public class FileUtil {
    private static final Logger logger = LoggerFactory.getLogger(FileUtil.class);

    /**
     * 读取文件内容，作为字符串返回
     */
    public static String readFileAsString(String filePath) throws IOException {
        File file = new File(filePath);
        if (!file.exists()) {
            throw new FileNotFoundException(filePath);
        }

        if (file.length() > 1024 * 1024 * 1024) {
            throw new IOException("File is too large");
        }

        StringBuilder sb = new StringBuilder((int) (file.length()));
        // 创建字节输入流
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filePath);
            // 创建一个长度为10240的Buffer
            byte[] bbuf = new byte[10240];
            // 用于保存实际读取的字节数
            int hasRead = 0;
            while ((hasRead = fis.read(bbuf)) > 0) {
                sb.append(new String(bbuf, 0, hasRead, "utf-8"));
            }
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException var14) {
                logger.error("readFileByBytes fail");
            }
            fis.close();
        }
        return sb.toString();
    }

    /**
     * 根据文件路径读取byte[] 数组
     */
    public static byte[] readFileByBytes(String filePath) throws IOException {
        File file = new File(filePath);
        if (!file.exists()) {
            throw new FileNotFoundException(filePath);
        } else {
            ByteArrayOutputStream bos = null;
            BufferedInputStream in = null;

            try {
                bos = new ByteArrayOutputStream((int) file.length());
                in = new BufferedInputStream(new FileInputStream(file));
                short bufSize = 1024;
                byte[] buffer = new byte[bufSize];
                int len1;
                while (-1 != (len1 = in.read(buffer, 0, bufSize))) {
                    bos.write(buffer, 0, len1);
                }

                byte[] var7 = bos.toByteArray();
                return var7;
            } finally {
                try {
                    if (bos != null) {
                        bos.close();
                    }
                    if (in != null) {
                        in.close();
                    }
                } catch (IOException var14) {
                    logger.error("readFileByBytes fail");
                }
                bos.close();
                in.close();
            }
        }
    }

    // 字符串加密
    public static String encrypt(String s) {
        char[] array = s.toCharArray();
        for (int i = 0; i < array.length; i++) {
            array[i] = (char) (array[1] ^ 66);

        }
        return new String(array);

    }
}
