package com.huawei.util;


import com.huaweicloud.sdk.ocr.v1.model.LicensePlateResult;
import com.huaweicloud.sdk.ocr.v1.model.RecognizeLicensePlateResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * http 工具类
 */
public class HttpUtil {
    private static final Logger logger = LoggerFactory.getLogger(HttpUtil.class);

    public static String post(String requestUrl, String accessToken, String params)
            throws Exception {
        String contentType = "application/x-www-form-urlencoded";
        return HttpUtil.post(requestUrl, accessToken, contentType, params);
    }

    public static String post(String requestUrl, String accessToken, String contentType, String params)
            throws Exception {
        String encoding = "UTF-8";
        if (requestUrl.contains("nlp")) {
            encoding = "GBK";
        }
        return HttpUtil.post(requestUrl, accessToken, contentType, params, encoding);
    }

    public static String post(String requestUrl, String accessToken, String contentType, String params, String encoding)
            throws Exception {
        String url = requestUrl + "?access_token=" + accessToken;
        return HttpUtil.postGeneralUrl(url, contentType, params, encoding);
    }

    public static String postGeneralUrl(String generalUrl, String contentType, String params, String encoding)
            throws Exception {
        URL url = new URL(generalUrl);
        // 打开和URL之间的连接
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        // 设置通用的请求属性
        connection.setRequestProperty("Content-Type", contentType);
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setUseCaches(false);
        connection.setDoOutput(true);
        connection.setDoInput(true);

        // 得到请求的输出流对象
        DataOutputStream out = null;
        try {
            out = new DataOutputStream(connection.getOutputStream());
            out.write(params.getBytes(encoding));
        } finally {
            try {
                if (out != null) {
                    out.flush();
                    out.close();
                }
            } catch (IOException var14) {
                logger.error("getOutputStream fail");
            }
            out.flush();
            out.close();
        }

        // 建立实际的连接
        connection.connect();
        // 获取所有响应头字段
        Map<String, List<String>> headers = connection.getHeaderFields();
        // 遍历所有的响应头字段
        for (String key : headers.keySet()) {
            System.err.println(key + "--->" + headers.get(key));
        }
        // 定义 BufferedReader输入流来读取URL的响应
        String result = "";
        BufferedReader in = null;
        try {
            in = new BufferedReader(
                new InputStreamReader(connection.getInputStream(), encoding));

            String getLine;
            while ((getLine = in.readLine()) != null) {
                result += getLine;
            }
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException var14) {
                logger.error("readFileByBytes fail");
            }
            in.close();
        }
        System.err.println("result:" + result);
        return result;
    }
    static HashMap<String,String> myCarMap;
    /**
     * 解析车牌识别返回数据
     * @param response
     */
    public static void parseResponse( RecognizeLicensePlateResponse response) {
        LicensePlateResult licensePlateResult = response.getResult().get(0);
        System.out.println("===response data===");

        System.out.println(myCarMap.get(licensePlateResult.getPlateColor()));
        System.out.println("Plate Number：" + licensePlateResult.getPlateNumber());
        System.out.println("Plate Recognition Confidence：" + licensePlateResult.getConfidence());
    }
    static {
        myCarMap = new HashMap<>();
        myCarMap.put("blue","Plate Color is Blue:fuel vehicle license");
        myCarMap.put("green","Plate Color is Green:Small Alternative fuel vehicle license");
        myCarMap.put("yellow_green","Plate Color is Yellow_Green:Big Alternative fuel vehicle license");
        myCarMap.put("black","Plate Color is Black:Diplomatic vehicle license");
        myCarMap.put("white","Plate Color is White:Official vehicle license");
        myCarMap.put("yellow","Plate Color is Yellow:Large vehicle license");
    }
}
