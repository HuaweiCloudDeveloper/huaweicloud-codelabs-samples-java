package com.huawei;

import com.huawei.util.DiyUtil;
import com.huawei.util.HttpUtil;

import com.huaweicloud.sdk.ocr.v1.model.RecognizeLicensePlateResponse;


public class App {

    // 本地车牌图片资源,可以替换本地的图片
    static String localPicture = "";

    // 公网车牌图片Url,可以替换
    static String remotePictureUrl = "";

    // 本地车牌Base64编码数据
    static String encode = "";


    public static void main(String[] args) {
        //  picture选择encode或者remotePictureUrl
        String picture = encode;
        String pictureUrl = "";
        // 车牌图片的本地路径：XXX";
        String picturePath = "";
        String myAk = "";
        String mySk = "";
        String region = "cn-north-4";
        String myProjectID = "";
        RecognizeLicensePlateResponse recognizeLicensePlateResponse = DiyUtil.debugByLocalPath(encode, myAk, mySk, picturePath);
        HttpUtil.parseResponse(recognizeLicensePlateResponse);
    }
}