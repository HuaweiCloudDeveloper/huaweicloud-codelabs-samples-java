package com.huawei;

import static org.junit.Assert.assertTrue;

import com.huawei.util.DiyUtil;
import com.huawei.util.FileUtil;
import com.huawei.util.PropertiesUtil;

import com.alibaba.fastjson.JSONObject;
import com.huaweicloud.sdk.ocr.v1.model.LicensePlateResult;
import com.huaweicloud.sdk.ocr.v1.model.RecognizeLicensePlateResponse;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * 车牌识别UT
 * 需要实验者首先完成diy方法的补全 .
 */
public class DemoTest {

    // 车牌图片url
    String pictureUrl;
    // 车牌图片的本地路径：车牌图片本地路径";
    String picturePath;
    // 华为云用户的ak
    String myAk;
    // 华为云用户的sk
    String mySk;
    // 默认是北京四, 开通车牌识别服务的Region
    String region = "cn-north-4";
    // myProjectID 用API在线调试查看
	
	String base64StrPrefix = "/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBk";
	String base64StrSuffix = "mKasTxQxJHd0AOWS9wwvrbpU4AZocOzat3O/Wl4hm+l4Zcxy5xpfSgVHJGoPLlmy5jYoum9ZVfHEri3CkgXO3qayg//Z";
	String encryptBase64StrPrefix = "{{";
	String encryptBase64StrSuffix = "99";


    @Before
    public void diy() {
        // todo 将API在线调试返回的json格式手动拷贝到 onlineDebugResult.json

        // todo 补全下列参数
        pictureUrl = getPictureUrl();
        picturePath = getPicturePath();
        myAk = getAccessKey();
        mySk = getSecretKey();
        region = getRegionId();
    }

    /**
     * 实验者用API插件在线调试
     */
    @Test
    public void onlineDebugResultShouldEqualSpecificValue() throws IOException {
        String s = FileUtil.readFileAsString("src/test/resources/onlineDebugResult.json");
        String t = FileUtil.readFileAsString("src/test/resources/specificValue.json");
        JSONObject o1 = JSONObject.parseObject(s);
        JSONObject o2 = JSONObject.parseObject(t);
        assertTrue(o1.equals(o2));
    }

    /**
     * 实验者用华为云OCR服务SDK调试云端图片
     */
    @Test
    public void ApiDebugResultShouldEqualSpecificValueByUrl() throws IOException {
        RecognizeLicensePlateResponse response = DiyUtil.debugByUrl(pictureUrl, myAk, mySk, region);
		List<LicensePlateResult> res = response.getResult();
		// 验证识别结果
        assertTrue(res.size() == 2);
        assertTrue(res.get(0).getPlateNumber().equals("闽AT8888"));
        assertTrue(res.get(0).getPlateColor().equals("blue"));
        assertTrue(res.get(0).getConfidence().toString().equals("0.9992"));
        assertTrue(res.get(1).getPlateNumber().equals("皖AK0T81"));
		assertTrue(res.get(1).getPlateColor().equals("blue"));
        assertTrue(res.get(1).getConfidence().toString().equals("0.9829"));
    }

    /**
     * 实验者用华为云OCR服务SDK调试本地图片
     */
    @Test
    public void ApiDebugResultShouldEqualSpecificValueByBase64() throws IOException {
        // 验证base64编码是否正确
        String base64 = DiyUtil.base64Encode(picturePath);
        assertTrue(base64.startsWith(base64StrPrefix));
		assertTrue(base64.endsWith(base64StrSuffix));
        String encryptRes = FileUtil.encrypt(base64);
        assertTrue(encryptRes.startsWith(encryptBase64StrPrefix) && encryptRes.endsWith(encryptBase64StrSuffix));

        // 验证识别结果
        RecognizeLicensePlateResponse response = DiyUtil.debugByLocalPath(picturePath, myAk, mySk, region);
        List<LicensePlateResult> res = response.getResult();
        assertTrue(res.size() == 2);
        assertTrue(res.get(0).getPlateNumber().equals("闽AT8888"));
        assertTrue(res.get(0).getPlateColor().equals("blue"));
		assertTrue(res.get(0).getConfidence().toString().equals("0.9992"));

        assertTrue(res.get(1).getPlateNumber().equals("皖AK0T81"));
        assertTrue(res.get(1).getPlateColor().equals("blue"));
        assertTrue(res.get(1).getConfidence().toString().equals("0.9829"));
    }

    private String getAccessKey() {
        return PropertiesUtil.getInstance().getValue("ak");
    }

    private String getSecretKey() {
        return PropertiesUtil.getInstance().getValue("sk");
    }

    private String getPictureUrl() {
        return PropertiesUtil.getInstance().getValue("pictureUrl");
    }

    private String getPicturePath() {
        return PropertiesUtil.getInstance().getValue("picturePath");
    }

    private String getRegionId() {
        return  PropertiesUtil.getInstance().getValue("region");
    }
}
