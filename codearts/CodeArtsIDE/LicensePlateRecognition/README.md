## 1、功能介绍

CodeArts IDE for Java是一个集成开发环境，将文本编辑器和强大的开发工具（如智能代码补全、导航、重构和调试）集成在一起。
 
- 调试热替换：支持在调试模式下运行程序。当程序挂起时，在“运行”和“调试”视图中检查其输出。找到错误并更正它，然后重新运行程序。在Java上下文中，可通过热代码替换功能来动态修改和重新加载类，无需停止调试会话。
 
- 强大的编码辅助能力：CodeArts IDE for Java 可以深入理解代码，提供上下文感知的机器学习辅助补全功能，可以补全单条语句或整行代码。对于代码编辑器中的任何符号，开发者可以查看其定义和相关文档。集成开发环境可高亮显示错误，并让开发者直接在代码编辑器中对错误采取行动。"问题 "视图会列出当前打开的文件中检测到的所有问题。
 
- Java构建工具集成：CodeArts IDE for Java 提供对 Maven 和 Gradle 构建系统的内置支持，包括项目和依赖关系解析，以及运行 Maven 目标和 Gradle 任务的能力。
 
- 丰富的插件生态：CodeArts IDE for Java 不仅支持华为云CodeArts插件市场，还支持Open VSX插件市场，开发者也可以通过安装自己喜爱的插件，把CodeArts IDE变成个人开发的“定制桌面”。华为是Eclipse Open VSX 工作组的发起成员和主要Sponsor之一。

- 集成华为云丰富API：CodeArts IDE for Java内置了华为云API，全面覆盖华为云200+云服务、7000+API，支持用户检索API、查看API文档、调试API、以及提供SDK示例代码供用户学习如何使用API。同时提供华为云SDK代码片段补全功能，SDK依赖包自动引入，加速用户集成华为云API。

您将学到什么？

本实验基于华为云自研CodeArts IDE，指导用户通过使用华为云API，来实现一个车牌识别的应用。用户将车牌的图片，通过调用华为云OCR服务的车牌识别recognizeLicensePlate的接口，将图片中车牌进行识别。
- 通过完成此实验让开发者学习使用华为云CodeArts IDE，通过其强大功能完成应用开发
- 通过完成此实验让开发者更加了解华为云API，基于华为云强大的API可以实现更多复杂的功能和应用


## 2、前置条件

- 已注册华为账号或华为云账号，并且进行实名认证。具体请参见[华为云官网](https://www.huaweicloud.com/intl/zh-cn/)。
- 下载并安装CodeArts IDE for Java，了解IDE的基础功能，并且使用华为云账号登录。具体请参见[CodeArts IDE for Java产品页](https://devcloud.cn-north-4.huaweicloud.com/codeartside/home?product=java)。
- Java语言基础
- 安装Java JDK 1.8及其以上版本
- 打开示例代码程序

## 3、工程代码讲解

--文件DiyUtil.java：实验者主要实现的类，包括对图片进行base64编码的方法base64Encode，利用华为云OCR服务SDK对云端车牌图片识别的方法debugByUrl，以及利用华为云OCR服务SDK对本地车牌图片识别debugByLocalPath；
![image](assets/3.0.1.png)

--文件DemoTest.java：实验者完成代码补全后的测试类，主要包括用API插件在线调试、用华为云OCR服务SDK调试云端图片、用华为云OCR服务SDK调试本地图片的测试，正确完成DiyUtil.java以及本文件中diy方法部分属性的补全，测试会正常通过。
![image](assets/3.0.2.png)

**相关配置文件：**
src\main\resources\credentials.properties

## 4、初阶版：体验华为云API

### 华为云API插件在线调试功能介绍

华为云API插件是CodeArts IDE内置插件，对接华为云API开放平台，支持用户检索API、查看API文档、调试API、以及提供SDK示例代码供用户学习如何使用API。其中在线调试功能可以支撑使用者在IDE直接调试API。我们使用API插件的在线调试功能去完成车牌识别的体验。

1. 在IDE右侧点击“华为云API”，在搜索框中搜索“recognizeLicensePlate”，点击搜索结果。
![image](assets/searchAPI.png)
2. 查看这个接口的说明，请求参数是啥、返回结果是啥，再查看请求示例和返回示例，了解到这个API是识别输入图片中的车牌信息,并返回其坐标和内容。
![image](assets/APIDetail1.png)
![image](assets/APIDetail2.png)
3. 切换到“调试API”页签，可以查看这个API整个调用过程的示例代码，当你填入参数时，示例代码将会生成对应的代码块。
![image](assets/debugAPI.png)   
4. 按照下图所示的步骤，开发者通过API插件在线调试，完成车牌识别应用。url参数使用云端车牌图片地址：https://hezhiwei666.obs.cn-north-4.myhuaweicloud.com/car.png。完成后将调试的结果粘贴到*/onlineDebugResult.json中。
![image](assets/onlineDebug.png)  

![image](assets/onlineDebug2.png)  
5. 结果验证：将调试的结果粘贴到*/onlineDebugResult.json后，进入到文件DemoTest.java中，运行方法“onlineDebugResultShouldEqualSpecificValue”，运行通过。
![image](assets/onlineDebug3.png)  

## 5、中阶版：云端车牌图片识别编码

华为云API插件代码补全功能介绍

华为云API插件提供华为云SDK代码片段补全功能，SDK依赖包自动引入，加速用户集成华为云API。在代码编辑区输入相关API关键字，如“recogize”，点击联想到的API后可将相关代码自动补全。

在IDE代码编辑区右键“启动华为云SDK代码补全助手”后，利用API代码补全功能在DiyUtil中完成代码补全。
![image](assets/5.0.1.png)

### 5.1、为程序配置AK/SK
代码访问API需要AK/SK，[参考文档](https://support.huaweicloud.com/iam_faq/iam_01_0618.html)获取用户AK/SK，将其输入到resources文件夹下的credentials.properties文件中的ak、sk中
![image](assets/5.1.1.png)

### 5.2、通过SDK助手开发应用程序
开发者通过编码实现应用，集成华为云OCR SDK，开发应用。
1.	在左侧的工程目录中，打开src目录下DiyUtil.java，在editor中点击右键打开菜单，选择启用**“华为云SDK代码补全助手”**。然后找到debugByUrl方法，在方法里输入recognizeLicensePlate，华为云API插件会自动帮你联想出合适的API，选择recognizeLicensePlate，华为云API插件会帮你自动把SDK调用实现，点击编辑API参数，填入相关参数后，点击确定，生成API参数的代码
![image](assets/5.2.1.png)

2. 代码生成后，将debugByUrl方法的包括“pictureUrl”在内的入参替换到代码中，并将response返回。并且修改函数，在try括号中进行语句添加return response，将调用API的代码client.recognizeLicensePlate返回的结果response作为函数的返回。
![image](assets/5.2.2.png)

3. **结果验证：**进入5.1中的配置文件credentials.properties，填入pictureUrl字段（pictureUrl使用“https://hezhiwei666.obs.cn-north-4.myhuaweicloud.com/car.png”。
运行方法“ApiDebugResultShouldEqualSpecificValueByUrl”，运行通过。
![image](assets/5.2.3.png)

## 6、高阶版：本地车牌图片识别编码
对应用进行改造，将本地图片转码成base64编码，再进行识别。
1. 在DiyUtil类中base64Encode方法里，实现本地图片转换base64编码的方法
![image](assets/6.1.1.png)
2. debugByLocalPath方法内，输入“recognizeLicensePlate”，点击车牌识别API，生成补全代码。import完成后，点击编辑API参数，在下方的窗口中输入API对应的参数，这里我们填入刚刚转换得到的base64编码到image（补全后再替换该参数），再点击确定后API参数代码自动生成。
![image](assets/6.1.3.png)

![image](assets/6.1.2.png)
**3. 结果验证**：进入DemoTest类中的diy方法，运行方法“ApiDebugResultShouldEqualSpecificValueByBase64”，运行通过。
![image](assets/6.1.4.png)