package com.huawei.sis;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.sis.v1.SisClient;
import com.huaweicloud.sdk.sis.v1.model.PostCustomTTSReq;
import com.huaweicloud.sdk.sis.v1.model.RunTtsRequest;
import com.huaweicloud.sdk.sis.v1.model.RunTtsResponse;
import com.huaweicloud.sdk.sis.v1.model.TtsConfig;
import com.huaweicloud.sdk.sis.v1.region.SisRegion;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SisService {
    private static final String REGION_ID = PropertiesUtil.getInstance().getValue("region");

    private static final Logger logger = LoggerFactory.getLogger(SisService.class);

    /**
     * 将text合成语音，并把结果返回
     *
     * @param ak 用户的accessKey
     * @param sk 用户的secretKey
     */
    public RunTtsResponse textToSpeech(String ak, String sk) {
        SisClient client = this.buildSisClient(ak, sk);

        /*
         *  在这里添加语音合成的功能
         */
        RunTtsRequest request = new RunTtsRequest();
        PostCustomTTSReq body = new PostCustomTTSReq();
        TtsConfig configbody = new TtsConfig();
        configbody.withAudioFormat(TtsConfig.AudioFormatEnum.fromValue("wav"))
                .withSampleRate(TtsConfig.SampleRateEnum.fromValue("16000"))
                .withProperty(TtsConfig.PropertyEnum.fromValue("chinese_xiaoyu_common"));
        body.withConfig(configbody);
        body.withText("软件开发生产线");
        request.withBody(body);
        try {
            RunTtsResponse response = client.runTts(request);
            System.out.println(response.toString());
            return response;
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }

        return null;
    }

    public RunTtsResponse readResponseJson() {
        URL url = Resources.getResource("APIResponse.json");
        try {
            String response = Resources.toString(url, Charsets.UTF_8);
            return JsonUtils.toBean(response, RunTtsResponse.class);
        } catch (FileNotFoundException notFoundException) {
            System.out.println("get sdk template file failed,can't find the specified file.");
        } catch (IOException ex) {
            System.out.println("get sdk template file failed,can't find the specified file");
        }
        return null;
    }

    private SisClient buildSisClient(String ak, String sk) {
        return SisClient.newBuilder()
            .withCredential(auth(ak, sk))
            .withRegion(SisRegion.valueOf(REGION_ID))
            .build();
    }

    private BasicCredentials auth(String ak, String sk) {
        return new BasicCredentials().withAk(ak).withSk(sk);
    }

    /**
     * 将base64字符解码保存文件
     * @param response response
     * @param targetPath targetPath
     */

    public static void decoderBase64File(RunTtsResponse response, String targetPath) {
        try (FileOutputStream out = new FileOutputStream(targetPath);) {
            byte[] buffer = Base64.getDecoder().decode((response.getResult().getData()));
            out.write(buffer);
            out.close();
        } catch (IOException e) {
            logger.error("解析base64失败", e.getMessage());
        }
    }

}

