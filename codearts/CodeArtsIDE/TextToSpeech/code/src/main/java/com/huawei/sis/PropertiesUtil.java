package com.huawei.sis;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil {
    private static final PropertiesUtil INSTANCE = new PropertiesUtil();

    private final Properties prop = new Properties();

    private PropertiesUtil() {
        try (InputStream  inputStream = SisService.class.getResourceAsStream("/credentials.properties")) {
            prop.load(inputStream);
        } catch (IOException e) {
            System.out.println("load properties fail.");
        }
    }
    public static PropertiesUtil getInstance() {
        return INSTANCE;
    }

    public String getValue(String key) {
        return String.valueOf(prop.get(key));
    }
}
