package com.huawei.sis;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.Normalizer;
import java.util.Objects;

public class JsonUtils {

    private static ObjectMapper defaultObjMapper = new ObjectMapper();

    public static <T> T toBean(String jsonStr, Class<T> clazz) {
        if (Objects.isNull(jsonStr) || Objects.isNull(clazz)) {
            throw new IllegalArgumentException();
        }
        T body = null;
        try {
            body = (T)defaultObjMapper.readValue((Normalizer.normalize(jsonStr, Normalizer.Form.NFKC)), clazz);
        } catch (Exception ex) {
            throw new IllegalArgumentException();
        }
        return body;
    }
}
