package sis;

import com.huawei.sis.PropertiesUtil;
import com.huawei.sis.SisService;
import com.huaweicloud.sdk.sis.v1.model.RunTtsResponse;
import org.junit.Test;


/**
 * Unit test for simple SisService.
 */
public class SisServiceTest {
    /**
     * 测试语音合成
     */
    @Test
    public void should_return_audio_when_call_api()
        throws Exception {
        String ak = getAccessKey();
        String sk = getSecretKey();
        String fileName = "devkit.wav";

        SisService app = new SisService();
        SisService.decoderBase64File(app.textToSpeech(ak, sk), fileName);
    }

    @Test
    public void should_return_audio_when_response_json()
            throws Exception {
        String fileName = "devkit.mp3";

        SisService app = new SisService();
        SisService.decoderBase64File(app.readResponseJson(), fileName);
    }

    @Test
    public void should_get_response_successfully_when_input_is_valid() {
        SisService app = new SisService();
        RunTtsResponse resp = app.readResponseJson();
    }


    private String getAccessKey() {
        return PropertiesUtil.getInstance().getValue("ak");
    }

    private String getSecretKey() {
        return PropertiesUtil.getInstance().getValue("sk");
    }

    private String getTargetPath() {
        return PropertiesUtil.getInstance().getValue("target_path");
    }

}
