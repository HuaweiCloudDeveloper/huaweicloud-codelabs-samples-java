## 1、功能介绍

CodeArts IDE for Java是一个集成开发环境，将文本编辑器和强大的开发工具（如智能代码补全、导航、重构和调试）集成在一起。
 
- 调试热替换：支持在调试模式下运行程序。当程序挂起时，在“运行”和“调试”视图中检查其输出。找到错误并更正它，然后重新运行程序。在Java上下文中，可通过热代码替换功能来动态修改和重新加载类，无需停止调试会话。
 
- 强大的编码辅助能力：CodeArts IDE for Java 可以深入理解代码，提供上下文感知的机器学习辅助补全功能，可以补全单条语句或整行代码。对于代码编辑器中的任何符号，开发者可以查看其定义和相关文档。集成开发环境可高亮显示错误，并让开发者直接在代码编辑器中对错误采取行动。"问题 "视图会列出当前打开的文件中检测到的所有问题。
 
- Java构建工具集成：CodeArts IDE for Java 提供对 Maven 和 Gradle 构建系统的内置支持，包括项目和依赖关系解析，以及运行 Maven 目标和 Gradle 任务的能力。
 
- 丰富的插件生态：CodeArts IDE for Java 不仅支持华为云CodeArts插件市场，还支持Open VSX插件市场，开发者也可以通过安装自己喜爱的插件，把CodeArts IDE变成个人开发的“定制桌面”。华为是Eclipse Open VSX 工作组的发起成员和主要Sponsor之一。

- 集成华为云丰富API：CodeArts IDE for Java内置了华为云API，全面覆盖华为云200+云服务、7000+API，支持用户检索API、查看API文档、调试API、以及提供SDK示例代码供用户学习如何使用API。同时提供华为云SDK代码片段补全功能，SDK依赖包自动引入，加速用户集成华为云API。

您将学到什么？

本实验基于华为云自研CodeArts IDE，指导用户通过使用华为云API，来实现一个文字合成语音的应用。用户只需要输入一段文字，通过调用华为云SIS服务的语音合成接口RunTTS，将对应的文字生成一段音频。
- 通过完成此实验让开发者学习使用华为云CodeArts IDE，通过其强大功能完成应用开发
- 通过完成此实验让开发者更加了解华为云API，基于华为云强大的API可以实现更多复杂的功能和应用


## 2、前置条件

- 已注册华为账号或华为云账号，并且进行实名认证。具体请参见[华为云官网](https://www.huaweicloud.com/intl/zh-cn/)。
- 下载并安装CodeArts IDE for Java，了解IDE的基础功能，并且使用华为云账号登录。具体请参见[CodeArts IDE for Java产品页](https://devcloud.cn-north-4.huaweicloud.com/codeartside/home?product=java)。
- Java语言基础
- 安装Java JDK 1.8及其以上版本
- 打开示例代码程序

## 3、初阶：体验API

1. 搜索API，在CodeArts IDE客户端内，打开**“华为云API”**插件，搜索**“语音合成”**，选择第一个搜索结果**“RunTts”**
![image](assets/searchAPI.png)
2.	右侧弹出RunTts详细情况，可以查看API相关信息，包括（API是干啥的，有哪些参数）然后选择“**调试API”**页签
![image](assets/APIDetail.png)
3.	输入需要合成的文字，例如“华为云软件开发生产线”，选择audio_format为.mp3，点击“调试”按钮，可以看到API的调试结果，可以看到“调试成功”，查看右下“响应结果”的“响应体”页签，可以看到返回的结果，“data”是音频的base64编码。
![image](assets/debugAPI.png)   
4.	点击响应体右侧的“拷贝”图标按钮，再将左边侧边栏的华为云API页面切换到“工程页面”，找到src/resources目录，将结果备份到工程目录src/resources/APIResponse.json中
![image](assets/copy2Json.png)
5.	找到test目录下的测试类SisServiceTest.java，点击找到方法“should_return_audio_when_response_json（）”，点击“Run‘All in SisServiceTest’”，最后可以看到目录下生成的音频文件。  
![image](assets/debugProgram.png)
## 4、高阶版：通过编写代码实现语音合成的应用

### 4.1、为程序配置AK/SK
代码访问API需要AK/SK，[参考文档](https://support.huaweicloud.com/iam_faq/iam_01_0618.html)获取用户AK/SK，将其输入到resources文件夹下的credentials.properties文件中的ak、sk中
![image](assets/setAKSK.png)

### 4.2、通过SDK助手开发应用程序
1.	在左侧的工程目录中，打开src目录下SisServic.java，在editor中点击右键打开菜单，选择启用**“华为云SDK代码补全助手”**。然后找到textToSpeech方法，在方法里输入RunTts，华为云API插件会自动帮你联想出合适的API，选择RunTts，华为云API插件会帮你自动把SDK调用实现，并且将代码中涉及到的依赖自动import
![image](assets/startSDKHelper.png)

![image](assets/textEditor.png)

![image](assets/SDKCoderInsert.png)
2. 点击编辑API参数，填入相关参数后，点击确定，生成API参数的代码
![image](assets/editParam.png)
3. 修改函数，在try括号中进行语句添加return response，将调用API的代码client.runTts返回的结果response作为函数的返回。
![image](assets/completeProgram.png)
4. 找到test目录下的测试类SisServiceTest.java，找到方法“should_return_audio_when_call_api”，点击“Run‘All in SisServiceTest’”，最后可以看到目录下生成的音频文件。

### 4.3、关键代码片段
SisService类textToSpeech方法：
```java
/**
     * 将text合成语音，并把结果返回
     *
     * @param ak 用户的accessKey
     * @param sk 用户的secretKey
     */
    public RunTtsResponse textToSpeech(String ak, String sk) {
        SisClient client = this.buildSisClient(ak, sk);

        /*
         *  在这里添加语音合成的功能
         */
        RunTtsRequest request = new RunTtsRequest();
        PostCustomTTSReq body = new PostCustomTTSReq();
        TtsConfig configbody = new TtsConfig();
        configbody.withAudioFormat(TtsConfig.AudioFormatEnum.fromValue("wav"))
                .withSampleRate(TtsConfig.SampleRateEnum.fromValue("16000"))
                .withProperty(TtsConfig.PropertyEnum.fromValue("chinese_xiaoyu_common"));
        body.withConfig(configbody);
        body.withText("软件开发生产线");
        request.withBody(body);
        try {
            RunTtsResponse response = client.runTts(request);
            System.out.println(response.toString());
            return response;
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }

        return null;
    }
```
### 4.4、相关配置文件：
src\main\resources\APIResponse.json
src\main\resources\credentials.properties
