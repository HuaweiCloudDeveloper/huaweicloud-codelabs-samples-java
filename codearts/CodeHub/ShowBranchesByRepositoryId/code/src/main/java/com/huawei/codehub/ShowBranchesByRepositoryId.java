package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.ShowBranchesByRepositoryIdRequest;
import com.huaweicloud.sdk.codehub.v3.model.ShowBranchesByRepositoryIdResponse;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowBranchesByRepositoryId {

    private static final Logger logger = LoggerFactory.getLogger(ShowBranchesByRepositoryId.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential showBranchesByRepositoryIdAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // Create a service client and set the corresponding region to CodeHubRegion.CN_NORTH_4.
        CodeHubClient client = CodeHubClient.newBuilder()
            .withCredential(showBranchesByRepositoryIdAuth)
            .withRegion(CodeHubRegion.CN_NORTH_4)
            .build();
        // Construction request
        ShowBranchesByRepositoryIdRequest request = new ShowBranchesByRepositoryIdRequest();
        // Set the short ID of the repository. The value can be obtained from the upper left corner of the code repository page. The value of Repository ID is the short ID of the repository.
        request.withRepositoryId("7111111");
        try {
            // Obtaining Results
            ShowBranchesByRepositoryIdResponse response = client.showBranchesByRepositoryId(request);
            // Print Results
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}