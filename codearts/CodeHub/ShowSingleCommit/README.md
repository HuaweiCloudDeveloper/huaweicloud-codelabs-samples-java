### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeHub/debug?api=ShowSingleCommit) 中直接运行调试该接口。

2.在本样例中，您可以查询某个仓库的特定提交信息。

3.通过查询某个仓库的特定提交信息，可以了解提交时间、作者、提交信息、修改的文件列表等。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目，并且创建代码仓库。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codehub”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codehub</artifactId>
    <version>3.1.119</version>
</dependency>
```

### 代码示例
``` java
public class ShowSingleCommit {

    private static final Logger logger = LoggerFactory.getLogger(ShowSingleCommit.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String showSingleCommitAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showSingleCommitSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(showSingleCommitAk)
                .withSk(showSingleCommitSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeHubRegion.CN_NORTH_4
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求，设置请求参数仓库主键id，commit id
        ShowSingleCommitRequest request = new ShowSingleCommitRequest();
        // 仓库主键id，代码托管点击某个仓库的链接中的repositoryId，使用时替换成实际的仓库主键id
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/home?ref=master
        Integer repoId = 1234567;
        request.withRepoId(repoId);
        // commit id，仓库的branch名或tag名
        String commitId = "<YOUR COMMIT ID>";
        request.withSha(commitId);
        try {
            ShowSingleCommitResponse response = client.showSingleCommit(request);
            logger.info("ShowSingleCommit: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ShowSingleCommit connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ShowSingleCommit RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ShowSingleCommit ServiceResponseException", e);
        }
    }
}
```

### 返回结果示例
```
{
 "result": {
  "id": "2369****",
  "title": "更新 HelloWorld.java",
  "message": "更新 HelloWorld.java",
  "stats": {
   "additions": 0,
   "deletions": 0,
   "total": 0
  },
  "short_id": "2369****",
  "author_name": "demouser",
  "author_email": "f1ac****@huawei.com",
  "committer_name": "demouser",
  "committer_email": "f1ac****@huawei.com",
  "created_at": "2024-09-27T06:42:32.000Z",
  "parent_ids": [
   "b1be****"
  ],
  "committed_date": "2024-09-27T06:42:32.000Z",
  "authored_date": "2024-09-27T06:42:32.000Z",
  "last_pipeline": null,
  "project_id": 268****,
  "code_changes": null
 },
 "status": "success"
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-10-29 | 1.0 | 文档首次发布 |