package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.ListUserAllRepositoriesRequest;
import com.huaweicloud.sdk.codehub.v3.model.ListUserAllRepositoriesResponse;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListUserAllRepositories {

    private static final Logger logger = LoggerFactory.getLogger(ListUserAllRepositories.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        // 注意，如下的 regionId 请使用项目所在的局点，例如华北-北京四区域为：cn-north-4
        String regionId = "<YOUR REGION ID>";
        // 创建服务客户端
        CodeHubClient client = CodeHubClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeHubRegion.valueOf(regionId))
            .build();
        // 构建请求头
        ListUserAllRepositoriesRequest request = new ListUserAllRepositoriesRequest();
        try {
            // 查询用户的所有仓库
            ListUserAllRepositoriesResponse response = client.listUserAllRepositories(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(String.valueOf(e.getRequestId()));
            logger.error(String.valueOf(e.getErrorCode()));
            logger.error(String.valueOf(e.getErrorMsg()));
        }
    }
}