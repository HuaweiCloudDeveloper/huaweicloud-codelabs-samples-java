### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeHub/doc?api=ListUserAllRepositories) 中直接运行调试该接口。

2.在本样例中，您可以查询到当前用户的所有仓库列表

3.仓库列表包含所有项目下的内容，通过该样例可以快速查询当前用户的所有仓库以及仓库的信息，包括仓库的创建者、仓库的状态、仓库 LFS 容量、仓库的地址等等。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codehub”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codehub</artifactId>
    <version>3.1.108</version>
</dependency>
```

### 代码示例
以下代码展示如何查询到当前用户的所有仓库列表：
``` java
package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.ListUserAllRepositoriesRequest;
import com.huaweicloud.sdk.codehub.v3.model.ListUserAllRepositoriesResponse;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListUserAllRepositories {

    private static final Logger logger = LoggerFactory.getLogger(ListUserAllRepositories.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        // 注意，如下的 regionId 请使用项目所在的局点，例如华北-北京四区域为：cn-north-4
        String regionId = "<YOUR REGION ID>";
        // 创建服务客户端
        CodeHubClient client = CodeHubClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeHubRegion.valueOf(regionId))
            .build();
        // 构建请求头
        ListUserAllRepositoriesRequest request = new ListUserAllRepositoriesRequest();
        try {
            // 查询用户的所有仓库
            ListUserAllRepositoriesResponse response = client.listUserAllRepositories(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(String.valueOf(e.getRequestId()));
            logger.error(String.valueOf(e.getErrorCode()));
            logger.error(String.valueOf(e.getErrorMsg()));
        }
    }
}
```

### 返回结果示例
#### ListUserAllRepositories
```
{
    error: null
    result: class RepoListInfoV2 {
        repositories: [class RepoInfoV2 {
            createdAt: 2023-06-26 15:46:17
            creatorName: ******
            domainName: ******
            groupName: ******
            httpsUrl: https://codehub.devcloud.cn-north-4.huaweicloud.com/Demo******/code_test.git
            iamUserUuid: ******
            isOwner: 1
            lfsSize: 0.00M
            projectIsDeleted: false
            projectUuid: ******
            repositoryId: ******
            repositoryName: ******
            repositorySize: 0.14M
            repositoryUuid: ******
            sshUrl: git@codehub.devcloud.cn-north-4.huaweicloud.com:Demo******/code_test.git
            star: false
            status: 0
            updatedAt: 2023-06-26 15:46:17
            userRole: null
            visibilityLevel: 0
            webUrl: https://devcloud.cn-north-4.huaweicloud.com/codehub/******/home
        }, class RepoInfoV2 {
            createdAt: 2023-10-31 10:22:59
            creatorName: ******
            domainName: ******
            groupName: Scrum-******
            httpsUrl: https://codehub.devcloud.cn-north-4.huaweicloud.com/Scrum-******/repo-test.git
            iamUserUuid: ******
            isOwner: 1
            lfsSize: 0.00M
            projectIsDeleted: false
            projectUuid: ******
            repositoryId: ******
            repositoryName: repo-******
            repositorySize: 0.71M
            repositoryUuid: ******
            sshUrl: git@codehub.devcloud.cn-north-4.huaweicloud.com:Scrum-******/repo-******.git
            star: false
            status: 0
            updatedAt: 2023-10-31 10:22:59
            userRole: null
            visibilityLevel: 0
            webUrl: https://devcloud.cn-north-4.huaweicloud.com/codehub/******/home
        }, class RepoInfoV2 {
            createdAt: 2023-12-18 10:02:36
            creatorName: ******
            domainName: ******
            groupName: ******
            httpsUrl: https://codehub.devcloud.cn-north-4.huaweicloud.com/xy******/school-******.git
            iamUserUuid: ******
            isOwner: 1
            lfsSize: 0.00M
            projectIsDeleted: false
            projectUuid: ******
            repositoryId: ******
            repositoryName: school-******
            repositorySize: 123.40M
            repositoryUuid: ******
            sshUrl: git@codehub.devcloud.cn-north-4.huaweicloud.com:xy******/school-******.git
            star: false
            status: 0
            updatedAt: 2023-12-18 10:02:41
            userRole: null
            visibilityLevel: 0
            webUrl: https://devcloud.cn-north-4.huaweicloud.com/codehub/******/home
        }, class RepoInfoV2 {
            createdAt: 2020-03-27 20:32:39
            creatorName: ******
            domainName: ******
            groupName: ******
            httpsUrl: https://codehub.devcloud.cn-north-4.huaweicloud.com/DevOps******/taohuadao-******.git
            iamUserUuid: ******
            isOwner: 1
            lfsSize: 0.00M
            projectIsDeleted: false
            projectUuid: ******
            repositoryId: ******
            repositoryName: taohuadao-******
            repositorySize: 18.29M
            repositoryUuid: ******
            sshUrl: git@codehub.devcloud.cn-north-4.huaweicloud.com:taohuadao-******/taohuadao-******.git
            star: false
            status: 0
            updatedAt: 2021-07-14 00:27:23
            userRole: null
            visibilityLevel: 0
            webUrl: https://devcloud.cn-north-4.huaweicloud.com/codehub/******/home
        }]
        total: 4
    }
    status: success
}
```
### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: |:----:| :------:  
 2024/08/09 | 1.0  | 文档首次发布