package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.ShowStatisticCommitV3Request;
import com.huaweicloud.sdk.codehub.v3.model.ShowStatisticCommitV3Response;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ShowStatisticCommitV3 {

    private static final Logger logger = LoggerFactory.getLogger(ShowStatisticCommitV3.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String showStatisticCommitV3Ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String showStatisticCommitV3Sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(showStatisticCommitV3Ak)
                .withSk(showStatisticCommitV3Sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeHubRegion.CN_NORTH_4
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求，设置请求参数仓库主键id，分支名称，起始提交日期，终止提交日期
        ShowStatisticCommitV3Request request = new ShowStatisticCommitV3Request();
        // 仓库主键id，代码托管点击某个仓库的链接中的repositoryId，使用时替换成实际的仓库主键id
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/home?ref=master
        Integer repositoryId = 1234567;
        request.withRepositoryId(repositoryId);
        // 分支名称
        String refName = "<YOUR REF NAME>";
        request.withRefName(refName);
        // 获取30天内代码提交行数
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        // 终止提交日期，格式为yyyy-MM-dd
        String endDate = simpleDateFormat.format(System.currentTimeMillis());
        calendar.add(Calendar.DATE, -30);
        // 起始提交日期，格式为yyyy-MM-dd
        String beginDate = simpleDateFormat.format(calendar.getTime());
        request.withBeginDate(beginDate);
        request.withEndDate(endDate);
        try {
            ShowStatisticCommitV3Response response = client.showStatisticCommitV3(request);
            logger.info("ShowStatisticCommitV3: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ShowStatisticCommitV3 connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ShowStatisticCommitV3 RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ShowStatisticCommitV3 ServiceResponseException", e);
        }
    }
}