package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.ShowRepoIdRequest;
import com.huaweicloud.sdk.codehub.v3.model.ShowRepoIdResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowRepoId {

    private static final Logger logger = LoggerFactory.getLogger(ShowRepoId.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String showRepoIdAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showRepoIdSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(showRepoIdAk)
                .withSk(showRepoIdSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeHubRegion.CN_NORTH_4
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求，设置请求参数仓库组名，仓库名
        ShowRepoIdRequest request = new ShowRepoIdRequest();
        // 仓库组名(克隆地址中域名后面仓库名前的一段 示例:git@repo.alpha.devcloud.inhuawei.com:Demo00228/testword.git  组名:Demo00228 )
        // 或者ListUserAllRepositories接口返回值中ssh_url中的域名后面仓库名前的一段
        String groupName = "<YOUR GROUP NAME>";
        request.withGroupName(groupName);
        // 仓库名
        String repositoryName = "<YOUR REPOSITORY NAME>";
        request.withRepositoryName(repositoryName);
        try {
            ShowRepoIdResponse response = client.showRepoId(request);
            logger.info("ShowRepoId: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ShowRepoId connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ShowRepoId RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ShowRepoId ServiceResponseException", e);
        }
    }
}