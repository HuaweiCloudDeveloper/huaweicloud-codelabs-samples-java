package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.ShowDiffCommitRequest;
import com.huaweicloud.sdk.codehub.v3.model.ShowDiffCommitResponse;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowDiffCommit {

    private static final Logger logger = LoggerFactory.getLogger(ShowDiffCommit.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String showDiffCommitAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showDiffCommitSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(showDiffCommitAk)
                .withSk(showDiffCommitSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CodeHubRegion.CN_NORTH_4.
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request and set the primary key ID and commit ID of the request parameter repository.
        ShowDiffCommitRequest request = new ShowDiffCommitRequest();
        // Specifies the primary key ID of a warehouse. The value of this parameter is the repositoryId in the link of a warehouse. Replace it with the actual primary key ID of the warehouse.
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/home?ref=master
        Integer repositoryId = 1234567;
        request.withRepoId(repositoryId);
        // commit ID, which indicates the branch name or tag name of the repository.
        String commitId = "<YOUR COMMIT ID>";
        request.withSha(commitId);
        try {
            ShowDiffCommitResponse response = client.showDiffCommit(request);
            logger.info("ShowDiffCommit: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ShowDiffCommit connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ShowDiffCommit RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ShowDiffCommit ServiceResponseException", e);
        }
    }
}