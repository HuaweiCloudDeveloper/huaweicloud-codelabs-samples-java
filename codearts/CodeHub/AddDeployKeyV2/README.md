### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeHub/sdk?api=AddDeployKeyV2) 中直接运行调试该接口。

2.在本样例中，您可以添加部署密钥。

3.添加部署密钥，可以使用部署密钥通过SSH协议以只读的方式克隆仓库，主要在仓库部署，持续集成等场景中使用。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目。

6.已在代码托管创建代码仓库。

7.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codehub”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codehub</artifactId>
    <version>3.1.113</version>
</dependency>
```

### 代码示例
``` java
public class AddDeployKeyV2 {

    private static final Logger logger = LoggerFactory.getLogger(AddDeployKeyV2.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String addDeployKeyV2Ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String addDeployKeyV2Sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(addDeployKeyV2Ak)
                .withSk(addDeployKeyV2Sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeHubRegion.CN_NORTH_4
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头，设置请求参数仓库主键id，部署使用的SSH密钥名称，部署使用的SSH密钥，部署使用的SSH密钥的来源
        AddDeployKeyV2Request request = new AddDeployKeyV2Request();
        // 仓库主键id，代码托管点击某个仓库的链接中的repositoryId，使用时替换成实际的仓库主键id
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/home?ref=master
        Integer repositoryId = 1234567;
        request.withRepositoryId(repositoryId);
        AddDeployKeyRequestBody body = new AddDeployKeyRequestBody();
        // 部署使用的SSH密钥名称
        body.withKeyTitle("<YOUR KEY TITLE>");
        // 部署使用的SSH密钥
        // SSH密钥的生成参考https://support.huaweicloud.com/usermanual-codeartsrepo/codeartsrepo_03_0011.html
        body.withKey("<YOUR KEY>");
        // 部署使用的SSH密钥是否可以推送代码，true可以推送，false不可以推送
        body.withCanPush(true);
        // 部署使用的SSH密钥的来源
        body.withApplication("<YOUR APPLICATION>");
        request.withBody(body);
        try {
            AddDeployKeyV2Response response = client.addDeployKeyV2(request);
            logger.info("AddDeployKeyV2: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("AddDeployKeyV2 connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("AddDeployKeyV2 RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("AddDeployKeyV2 ServiceResponseException", e);
        }
    }
}
```

### 返回结果示例
```
{
 "result": {
  "key": "xxxx",
  "key_id": "38****",
  "key_title": "deploykey1",
  "can_push": true,
  "created_at": "2024-09-18T10:56:40.000+08:00"
 },
 "status": "success"
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-09-18 | 1.0 | 文档首次发布 |