package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.AddDeployKeyRequestBody;
import com.huaweicloud.sdk.codehub.v3.model.AddDeployKeyV2Request;
import com.huaweicloud.sdk.codehub.v3.model.AddDeployKeyV2Response;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddDeployKeyV2 {

    private static final Logger logger = LoggerFactory.getLogger(AddDeployKeyV2.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String addDeployKeyV2Ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String addDeployKeyV2Sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(addDeployKeyV2Ak)
                .withSk(addDeployKeyV2Sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeHubRegion.CN_NORTH_4
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头，设置请求参数仓库主键id，部署使用的SSH密钥名称，部署使用的SSH密钥，部署使用的SSH密钥的来源
        AddDeployKeyV2Request request = new AddDeployKeyV2Request();
        // 仓库主键id，代码托管点击某个仓库的链接中的repositoryId，使用时替换成实际的仓库主键id
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/home?ref=master
        Integer repositoryId = 1234567;
        request.withRepositoryId(repositoryId);
        AddDeployKeyRequestBody body = new AddDeployKeyRequestBody();
        // 部署使用的SSH密钥名称
        body.withKeyTitle("<YOUR KEY TITLE>");
        // 部署使用的SSH密钥
        // SSH密钥的生成参考https://support.huaweicloud.com/usermanual-codeartsrepo/codeartsrepo_03_0011.html
        body.withKey("<YOUR KEY>");
        // 部署使用的SSH密钥是否可以推送代码，true可以推送，false不可以推送
        body.withCanPush(true);
        // 部署使用的SSH密钥的来源
        body.withApplication("<YOUR APPLICATION>");
        request.withBody(body);
        try {
            AddDeployKeyV2Response response = client.addDeployKeyV2(request);
            logger.info("AddDeployKeyV2: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("AddDeployKeyV2 connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("AddDeployKeyV2 RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("AddDeployKeyV2 ServiceResponseException", e);
        }
    }
}