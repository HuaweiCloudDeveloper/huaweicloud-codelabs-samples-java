### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeHub/sdk?api=ListFilesByQuery).

2.In this example, you can query the file information of a warehouse.

3.Query the file information of a repository, including the file name, file path, file size, and Base64-encoded file content.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. 
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. 
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A project and a code repository have been created on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-codehub. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codehub</artifactId>
    <version>3.1.122</version>
</dependency>
```

### Code example
``` java
package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.ListFilesByQueryRequest;
import com.huaweicloud.sdk.codehub.v3.model.ListFilesByQueryResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListFilesByQuery {

    private static final Logger logger = LoggerFactory.getLogger(ListFilesByQuery.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listFilesByQueryAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listFilesByQuerySk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listFilesByQueryAk)
                .withSk(listFilesByQuerySk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CodeHubRegion.CN_NORTH_4.
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request and set the request parameter repository primary key ID, complete file path, commit ID, and repository branch name or tag name.
        ListFilesByQueryRequest request = new ListFilesByQueryRequest();
        // Specifies the primary key ID of a warehouse. The value of this parameter is the repositoryId in the link of a warehouse. Replace it with the actual primary key ID of the warehouse.
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/home?ref=master
        Integer repositoryId = 1234567;
        request.withRepoId(repositoryId);
        // Full path to the file
        String filePath = "<YOUR FILE PATH>";
        request.withFilePath(filePath);
        // commit ID, which indicates the branch name or tag name of the repository.
        String ref = "<YOUR REF>";
        request.withRef(ref);
        try {
            ListFilesByQueryResponse response = client.listFilesByQuery(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
```
{
 "result": {
  "size": 3,
  "encoding": "base64",
  "ref": "master",
  "content": "YWJj",
  "file_name": "class.rb",
  "file_path": "lib/class.rb",
  "content_sha256": "ba78****",
  "blob_id": "f2ba****",
  "commit_id": "a60e****",
  "last_commit_id": "a60e****"
 },
 "status": "success"
}
```
### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/11/18 | 1.0 | This document is released for the first time.