package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.AddProtectAccessLevel;
import com.huaweicloud.sdk.codehub.v3.model.AddProtectBranchV2Request;
import com.huaweicloud.sdk.codehub.v3.model.AddProtectBranchV2Response;
import com.huaweicloud.sdk.codehub.v3.model.AddProtectRequest;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddProtectBranch {

    private static final Logger logger = LoggerFactory.getLogger(AddProtectBranch.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        // 注意，如下的 regionId 请使用项目所在的局点，例如华北-北京四区域为：cn-north-4
        String regionId = "<YOUR REGION ID>";
        // 创建服务客户端
        CodeHubClient client = CodeHubClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeHubRegion.valueOf(regionId))
            .build();
        // 构建请求
        AddProtectBranchV2Request request = new AddProtectBranchV2Request();
        // 设置仓库的短id，该值可以在代码仓库页面的左上方获取，Repository ID的值即为仓库的短id
        request.withRepositoryId(111111);
        // 保护分支名称，替换为仓库实际值
        request.withBranchName("dev");
        AddProtectRequest body = new AddProtectRequest();
        // 设置新建保护分支权限
        AddProtectAccessLevel accessLevelbody = new AddProtectAccessLevel();
        // 提交权限 0:任何人不允许提交,30:开发者及管理员可提交,40:管理员可提交
        accessLevelbody.withPushAccessLevel(AddProtectAccessLevel.PushAccessLevelEnum.NUMBER_30)
            // 合并权限 0:任何人不允许合并,30:开发者及管理员可合并,40:管理员可合并,合并权限必须大于等于提交权限
            .withMergeAccessLevel(AddProtectAccessLevel.MergeAccessLevelEnum.NUMBER_30);
        body.withAccessLevel(accessLevelbody);
        request.withBody(body);
        try {
            // 获取结果
            AddProtectBranchV2Response response = client.addProtectBranchV2(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}