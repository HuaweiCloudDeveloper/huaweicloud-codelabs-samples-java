### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeHub/doc?api=AddProtectBranchV2) 中直接运行调试该接口。

2.在本样例中，您可以指定某个代码仓库新建保护分支

3.新建保护分支时，可以配置该分支的提交权限以及合并权限，避免一些重要分支的内容被项目成员随意修改或者是误删除，可以更好地保护代码仓库的内容

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codehub”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codehub</artifactId>
    <version>3.1.108</version>
</dependency>
```

### 代码示例
以下代码展示如何新建保护分支：
``` java
package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.AddProtectAccessLevel;
import com.huaweicloud.sdk.codehub.v3.model.AddProtectBranchV2Request;
import com.huaweicloud.sdk.codehub.v3.model.AddProtectBranchV2Response;
import com.huaweicloud.sdk.codehub.v3.model.AddProtectRequest;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddProtectBranch {

    private static final Logger logger = LoggerFactory.getLogger(AddProtectBranch.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        // 注意，如下的 regionId 请使用项目所在的局点，例如华北-北京四区域为：cn-north-4
        String regionId = "<YOUR REGION ID>";
        // 创建服务客户端
        CodeHubClient client = CodeHubClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeHubRegion.valueOf(regionId))
            .build();
        // 构建请求
        AddProtectBranchV2Request request = new AddProtectBranchV2Request();
        // 设置仓库的短id，该值可以在代码仓库页面的左上方获取，Repository ID的值即为仓库的短id
        request.withRepositoryId(111111);
        // 保护分支名称，替换为仓库实际值
        request.withBranchName("dev");
        AddProtectRequest body = new AddProtectRequest();
        // 设置新建保护分支权限
        AddProtectAccessLevel accessLevelbody = new AddProtectAccessLevel();
        // 提交权限 0:任何人不允许提交,30:开发者及管理员可提交,40:管理员可提交
        accessLevelbody.withPushAccessLevel(AddProtectAccessLevel.PushAccessLevelEnum.NUMBER_30)
            // 合并权限 0:任何人不允许合并,30:开发者及管理员可合并,40:管理员可合并,合并权限必须大于等于提交权限
            .withMergeAccessLevel(AddProtectAccessLevel.MergeAccessLevelEnum.NUMBER_30);
        body.withAccessLevel(accessLevelbody);
        request.withBody(body);
        try {
            // 获取结果
            AddProtectBranchV2Response response = client.addProtectBranchV2(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例
#### AddProtectBranch
```
{
    error: null
    result: class AddProtectResponse {
        name: dev
        commit: class CommitRepoV2 {
            id: xxxxxx
            shortId: xxxxxx
            createdAt: 2024-08-20T01:46:15.000Z
            title: fix #6xxxxxx 作为用户应该可以查找产品 
            parentIds: [xxxxxx]
            message: fix #6xxxxxx 作为用户应该可以查找产品 
            authorName: xxxxxx
            committerName: xxxxxx
            committedDate: 2024-08-20T01:46:15Z
        }
        _protected: true
        developersCanPush: true
        developersCanMerge: true
        masterCanPush: true
        masterCanMerge: true
        noOneCanPush: false
        noOneCanMerge: false
        inAnOpenedMergeRequest: false
    }
    status: success
}
```
### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: |:----:| :------:  
 2024/08/26 | 1.0  | 文档首次发布