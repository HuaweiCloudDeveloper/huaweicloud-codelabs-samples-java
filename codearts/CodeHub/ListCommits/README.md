### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeHub/doc?api=ListCommits) 中直接运行调试该接口。

2.在本样例中，您可以获取到CodeArts某个代码仓库的提交信息

3.获取仓库的提交信息后，可以根据提供的数据分析项目中代码提交的情况，根据实际情况制定一些提交规范等等。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codehub”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codehub</artifactId>
    <version>3.1.108</version>
</dependency>
```

### 代码示例
以下代码展示如何根据CodeArts的仓库Id获取该代码仓库的提交信息：
``` java
package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.ListCommitsRequest;
import com.huaweicloud.sdk.codehub.v3.model.ListCommitsResponse;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListCommits {

    private static final Logger logger = LoggerFactory.getLogger(ListCommits.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        // 注意，如下的 regionId 请使用项目所在的局点，例如华北-北京四区域为：cn-north-4
        String regionId = "<YOUR REGION ID>";
        // 创建服务客户端
        CodeHubClient client = CodeHubClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeHubRegion.valueOf(regionId))
            .build();
        // 构建请求头
        ListCommitsRequest request = new ListCommitsRequest();
        // 必选参数，设置仓库的短id，该值可以在代码仓库页面的左上方获取，Repository ID的值即为仓库的短id
        request.withRepoId(111);
        // 可选参数，仓库的branch名或tag名,如果为空则查询默认分支
        request.withRefName("<YOUR BRANCH OR TAG NAME>");
        try {
            // 获取仓库的提交信息
            ListCommitsResponse response = client.listCommits(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(String.valueOf(e.getRequestId()));
            logger.error(String.valueOf(e.getErrorCode()));
            logger.error(String.valueOf(e.getErrorMsg()));
        }
    }
}
```

### 返回结果示例
#### ListCommits
```
{
    error: null
    result: [class CommitInfo {
        id: ******
        shortId: ******
        title: merge develop into master
        authorName: ******
        authorEmail: null
        committerName: ******
        committerEmail: null
        createdAt: 2024-07-23T02:04:45.000Z
        message: merge develop into master
        
        merge "develop" into "master"
        fix #2051887 作为管理员应该可以设置会员级别,
        
        Created-by: ******
        Author-id: ******
        MR-id: ******
        Commit-by: ******
        Merged-by: ******
        Description: merge "develop" into "master"
        fix #2051887 作为管理员应该可以设置会员级别,
        
        See merge request: ******
        parentIds: [******, ******]
        committedDate: 2024-07-23T02:04:45Z
        authoredDate: 2024-07-23T02:04:45Z
    }, class CommitInfo {
        id: ******
        shortId: ******
        title: update
        authorName: ******
        authorEmail: null
        committerName: ******
        committerEmail: null
        createdAt: 2024-07-15T03:25:07.000Z
        message: update
        parentIds: [******]
        committedDate: 2024-07-15T03:25:07Z
        authoredDate: 2024-07-15T03:25:07Z
    }]
    status: success
}
```
### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: |:----:| :------:  
 2024/08/08 | 1.0  | 文档首次发布