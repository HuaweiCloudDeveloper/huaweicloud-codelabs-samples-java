package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.ShowRepositoryNameExistRequest;
import com.huaweicloud.sdk.codehub.v3.model.ShowRepositoryNameExistResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowRepositoryNameExist {

    private static final Logger logger = LoggerFactory.getLogger(ShowRepositoryNameExist.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String showRepositoryNameExistAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showRepositoryNameExistSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(showRepositoryNameExistAk)
                .withSk(showRepositoryNameExistSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CodeHubRegion.CN_NORTH_4.
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request and set the project ID and repository name in the request.
        ShowRepositoryNameExistRequest request = new ShowRepositoryNameExistRequest();
        // Project ID. Note: Use the project ID on the home page of the corresponding project in CodeArts. Click the ID of a project link.
        // Example: https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/List
        String projectUuid = "<YOUR PROJECT UUID>";
        request.withProjectUuid(projectUuid);
        // Repository Name
        String repositoryName = "<YOUR REPOSITORY NAME>";
        request.withRepositoryName(repositoryName);
        try {
            ShowRepositoryNameExistResponse response = client.showRepositoryNameExist(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}