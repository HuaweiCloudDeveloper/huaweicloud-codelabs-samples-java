package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v4.CodeHubClient;
import com.huaweicloud.sdk.codehub.v4.model.ListTrustedIpAddressesRequest;
import com.huaweicloud.sdk.codehub.v4.model.ListTrustedIpAddressesResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.codehub.v4.region.CodeHubRegion;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListTrustedIpAddresses {

    private static final Logger logger = LoggerFactory.getLogger(ListTrustedIpAddresses.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listTrustedIpAddressesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listTrustedIpAddressesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(listTrustedIpAddressesAk)
                .withSk(listTrustedIpAddressesSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeHubRegion.CN_NORTH_4
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求，设置请求参数仓库主键id
        ListTrustedIpAddressesRequest request = new ListTrustedIpAddressesRequest();
        // 仓库主键id，代码托管点击某个仓库的链接中的repositoryId，使用时替换成实际的仓库主键id
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/home?ref=master
        Integer repositoryId = 1234567;
        request.withId(repositoryId);
        try {
            ListTrustedIpAddressesResponse response = client.listTrustedIpAddresses(request);
            logger.info("ListTrustedIpAddresses: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListTrustedIpAddresses connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListTrustedIpAddresses RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListTrustedIpAddresses ServiceResponseException", e);
        }
    }
}