package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.ApprovalActionTypeDto;
import com.huaweicloud.sdk.codehub.v3.model.UpdateMergeRequestApprovalStateRequest;
import com.huaweicloud.sdk.codehub.v3.model.UpdateMergeRequestApprovalStateResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateMergeRequestApprovalState {

    private static final Logger logger = LoggerFactory.getLogger(UpdateMergeRequestApprovalState.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String updateMergeRequestAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String updateMergeRequestSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(updateMergeRequestAk)
                .withSk(updateMergeRequestSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeHubRegion.CN_NORTH_4
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();

        // 构建请求，设置请求参数仓库主键id，合并请求的短id，操作类型
        UpdateMergeRequestApprovalStateRequest request = new UpdateMergeRequestApprovalStateRequest();
        // 仓库主键id，代码托管点击某个仓库的链接中的repositoryId，使用时替换成实际的仓库主键id
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/home?ref=master
        String repositoryId = "<YOUR REPOSITORY ID>";
        request.withRepositoryId(repositoryId);
        // 合并请求的短id，点击某个合并请求中的iid，使用时替换成实际的合并请求的短id
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/{iid}/mergedetail?source=dev&target=master
        Integer iid = 1;
        request.withMergeRequestIid(iid);
        ApprovalActionTypeDto body = new ApprovalActionTypeDto();
        // 操作类型，取值，通过：approve；拒绝：reject； 撤销：reset
        String actionType = "<YOUR ACTION TYPE>";
        body.withActionType(actionType);
        request.withBody(body);
        try {
            UpdateMergeRequestApprovalStateResponse response = client.updateMergeRequestApprovalState(request);
            logger.info("UpdateMergeRequestApprovalState: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("UpdateMergeRequestApprovalState connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("UpdateMergeRequestApprovalState RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("UpdateMergeRequestApprovalState ServiceResponseException", e);
        }
    }
}