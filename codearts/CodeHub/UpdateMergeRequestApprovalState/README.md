### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeHub/debug?api=UpdateMergeRequestApprovalState) 中直接运行调试该接口。

2.在本样例中，您可以进行合并请求代码审核。

3.通过调用合并请求代码审核，可以更方便地进行合并请求的操作，对合并请求进行通过、拒绝、或者撤销处理。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目，已在代码托管创建代码仓库。

6.调用API的用户需要是合并请求的审核人。

7.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codehub”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codehub</artifactId>
    <version>3.1.117</version>
</dependency>
```

### 代码示例
``` java
public class UpdateMergeRequestApprovalState {

    private static final Logger logger = LoggerFactory.getLogger(UpdateMergeRequestApprovalState.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String updateMergeRequestAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String updateMergeRequestSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(updateMergeRequestAk)
                .withSk(updateMergeRequestSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeHubRegion.CN_NORTH_4
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();

        // 构建请求，设置请求参数仓库主键id，合并请求的短id，操作类型
        UpdateMergeRequestApprovalStateRequest request = new UpdateMergeRequestApprovalStateRequest();
        // 仓库主键id，代码托管点击某个仓库的链接中的repositoryId，使用时替换成实际的仓库主键id
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/home?ref=master
        String repositoryId = "<YOUR REPOSITORY ID>";
        request.withRepositoryId(repositoryId);
        // 合并请求的短id，点击某个合并请求中的iid，使用时替换成实际的合并请求的短id
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/{iid}/mergedetail?source=dev&target=master
        Integer iid = 1;
        request.withMergeRequestIid(iid);
        ApprovalActionTypeDto body = new ApprovalActionTypeDto();
        // 操作类型，取值，通过：approve；拒绝：reject； 撤销：reset
        String actionType = "<YOUR ACTION TYPE>";
        body.withActionType(actionType);
        request.withBody(body);
        try {
            UpdateMergeRequestApprovalStateResponse response = client.updateMergeRequestApprovalState(request);
            logger.info("UpdateMergeRequestApprovalState: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("UpdateMergeRequestApprovalState connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("UpdateMergeRequestApprovalState RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("UpdateMergeRequestApprovalState ServiceResponseException", e);
        }
    }
}
```

### 返回结果示例
```
{
 "result": {
  "id": 271438,
  "name": "demouser",
  "username": "f1ac****",
  "name_cn": "demouser",
  "email": "f1ac****@xxx.com",
  "state": "reject",
  "updated_at": "2024-10-12T15:23:23.731+08:00",
  "avatar_url": "",
  "nick_name": "Developer",
  "tenant_name": "demouser"
 },
 "status": "success"
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-10-17 | 1.0 | 文档首次发布 |