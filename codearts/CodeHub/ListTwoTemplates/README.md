### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeHub/sdk?api=ListTwoTemplates) 中直接运行调试该接口。

2.在本样例中，您可以获取语言类型是Java，支持自动创建流水线的公开示例模板列表。

3.获取公开示例模板列表，可以通过模板进行创建代码仓库等操作。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codehub”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codehub</artifactId>
    <version>3.1.113</version>
</dependency>
```

### 代码示例
``` java
public class ListTwoTemplates {

    private static final Logger logger = LoggerFactory.getLogger(ListTwoTemplates.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listTemplatesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listTemplatesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(listTemplatesAk)
                .withSk(listTemplatesSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeHubRegion.CN_NORTH_4
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求，设置请求参数语言类型，是否支持流水线，分页页数，每页数据数
        ListTwoTemplatesRequest request = new ListTwoTemplatesRequest();
        // 语言类型
        request.withLanguage("java");
        // 是否支持流水线，"UnsupportedPipeline"，"SupportPipeline"
        request.withPipeline("SupportPipeline");
        // 分页页数
        request.withPageNo(1);
        // 每页数据数
        request.withPageSize(10);
        try {
            ListTwoTemplatesResponse response = client.listTwoTemplates(request);
            logger.info("ListTwoTemplates: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListTwoTemplates connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListTwoTemplates RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListTwoTemplates ServiceResponseException", e);
        }
    }
}
```

### 返回结果示例
```
{
 "result": {
  "projects": [
   {
    "id": 534743,
    "name": "CodeArts Demo",
    "template_name": "DevCloudDemo",
    "tags": [
     "Java",
     "Web网站",
     "SupportPipeline"
    ],
    "description": "软件开发生产线Scrum演示项目",
    "brief_introduction": "软件开发生产线Scrum演示项目",
    "auto_pending_pipelines": 0,
    "language": "Java",
    "created_at": "2019-07-23T07:11:47.000Z",
    "used_times": 75068,
    "liked_times": 1152,
    "creator_name": "华为云CodeArts",
    "https_url": "https://codehub.devcloud.cn-north-4.huaweicloud.com/demo_group_name/DevCloudDemo.git"
   },
   {
    "id": 534746,
    "name": "Java Web Demo",
    "template_name": "javaWebDemo",
    "tags": [
     "Java",
     "Web网站",
     "SupportPipeline"
    ],
    "description": "本工程是基于Spring boot的Web工程示例，演示如何通过maven编译构建，并使用Docker进行部署",
    "brief_introduction": "基于Spring boot的Java Web工程",
    "auto_pending_pipelines": 0,
    "language": "Java",
    "created_at": "2019-07-23T07:23:24.000Z",
    "used_times": 6157,
    "liked_times": 52,
    "creator_name": "华为云CodeArts",
    "https_url": "https://codehub.devcloud.cn-north-4.huaweicloud.com/demo_group_name/javaWebDemo.git"
   },
   {
    "id": 534748,
    "name": "Android Gradle Demo",
    "template_name": "androidGradleDemo",
    "tags": [
     "Java",
     "Android",
     "SupportPipeline"
    ],
    "description": "基于Gradle编译的android apk小程序。",
    "brief_introduction": "基于Gradle编译的android apk小程序。",
    "auto_pending_pipelines": 0,
    "language": "Java",
    "created_at": "2019-07-23T07:24:41.000Z",
    "used_times": 502,
    "liked_times": 54,
    "creator_name": "华为云CodeArts",
    "https_url": "https://codehub.devcloud.cn-north-4.huaweicloud.com/demo_group_name/androidGradleDemo.git"
   }
  ],
  "total": 3
 },
 "status": "success"
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-09-14 | 1.0 | 文档首次发布 |