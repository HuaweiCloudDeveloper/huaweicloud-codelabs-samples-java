### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeHub/doc?api=CreateNewBranch).

2.In this example, you can create a new branch in the specified repository.

3.This API is used to create a branch based on the repository ID and source branch name for future development and test.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. 
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. 
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A project has been created on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-codehub. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codehub</artifactId>
    <version>3.1.117</version>
</dependency>
```

### Code example
The following code shows how to create a branch:
``` java
package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.CreateNewBranchRequest;
import com.huaweicloud.sdk.codehub.v3.model.CreateNewBranchRequestBody;
import com.huaweicloud.sdk.codehub.v3.model.CreateNewBranchResponse;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateNewBranch {

    private static final Logger logger = LoggerFactory.getLogger(CreateNewBranch.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential createNewBranchAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // Create a service client and set the corresponding region to CodeHubRegion.CN_NORTH_4.
        CodeHubClient client = CodeHubClient.newBuilder()
            .withCredential(createNewBranchAuth)
            .withRegion(CodeHubRegion.CN_NORTH_4)
            .build();
        // Construction request
        CreateNewBranchRequest request = new CreateNewBranchRequest();
        // Set the short ID of the repository. The value can be obtained from the upper left corner of the code repository page. The value of Repository ID is the short ID of the repository.
        request.withRepositoryId("2111111");
        // Setting Request Parameters
        CreateNewBranchRequestBody body = new CreateNewBranchRequestBody();
        // Source Branch Name
        body.withRef("master");
        // New Branch Name
        body.withBranchName("test");
        request.withBody(body);
        try {
            // Obtaining Results
            CreateNewBranchResponse response = client.createNewBranch(request);
            // Print Results
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
#### CreateNewBranch
```
{
    error: null
    result: class AddProtectResponse {
        name: test
        commit: class CommitRepoV2 {
            id: xxxxxx
            shortId: xxxxxx
            createdAt: xxxxxx
            title: initial commit
            parentIds: []
            message: initial commit
            
            authorName: Repo
            committerName: Repo
            committedDate: xxxxxx
        }
        _protected: false
        developersCanPush: false
        developersCanMerge: false
        masterCanPush: false
        masterCanMerge: false
        noOneCanPush: false
        noOneCanMerge: false
        inAnOpenedMergeRequest: false
    }
    status: success
}
```
### Change History

 Released On  |  Issue   | Description
 :----: |:---:| :------:  
 2024/10/16 | 1.0 | This document is released for the first time.