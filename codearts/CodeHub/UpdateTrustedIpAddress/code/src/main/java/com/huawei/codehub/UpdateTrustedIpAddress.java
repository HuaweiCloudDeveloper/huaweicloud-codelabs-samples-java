package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v4.CodeHubClient;
import com.huaweicloud.sdk.codehub.v4.model.AddTrustedIpAddressRequestBody;
import com.huaweicloud.sdk.codehub.v4.model.UpdateTrustedIpAddressRequest;
import com.huaweicloud.sdk.codehub.v4.model.UpdateTrustedIpAddressResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.codehub.v4.region.CodeHubRegion;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateTrustedIpAddress {

    private static final Logger logger = LoggerFactory.getLogger(UpdateTrustedIpAddress.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String updateTrustedIpAddressAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String updateTrustedIpAddressSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(updateTrustedIpAddressAk)
                .withSk(updateTrustedIpAddressSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeHubRegion.CN_NORTH_4
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求，设置请求参数仓库主键id，ip的id，添加ip白名单请求体
        UpdateTrustedIpAddressRequest request = new UpdateTrustedIpAddressRequest();
        // 仓库主键id，代码托管点击某个仓库的链接中的repositoryId，使用时替换成实际的仓库主键id
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/home?ref=master
        Integer repositoryId = 1234567;
        request.withId(repositoryId);
        // ip的id，(ListTrustedIpAddresses接口返回值中的id)，使用时替换成实际的ip的id
        Integer ipId = 1234567;
        request.withIpId(ipId);
        AddTrustedIpAddressRequestBody body = new AddTrustedIpAddressRequestBody();

        // 是否允许提交代码，0：不允许提交代码，1：允许提交代码
        body.withUploadFlag(0);
        // 是否允许下载代码，0：不允许下载代码，1：允许下载代码
        body.withDownloadFlag(0);
        // 是否允许访问代码仓库，0：不允许访问代码仓库，1：允许访问代码仓库
        body.withViewFlag(1);

        // 格式类型，0：指定ip:1：ip范围:2：CIDR
        body.withIpType(0);
        // 起始ip
        body.withIpStart("<IP>");
        // 结束ip
        body.withIpEnd("<IP>");
        request.withBody(body);
        try {
            UpdateTrustedIpAddressResponse response = client.updateTrustedIpAddress(request);
            logger.info("UpdateTrustedIpAddress: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("UpdateTrustedIpAddress connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("UpdateTrustedIpAddress RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("UpdateTrustedIpAddress ServiceResponseException", e);
        }
    }
}