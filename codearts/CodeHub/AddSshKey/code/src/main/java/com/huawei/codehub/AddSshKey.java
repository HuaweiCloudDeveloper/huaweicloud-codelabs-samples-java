package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.AddSshKeyRequest;
import com.huaweicloud.sdk.codehub.v3.model.AddSshKeyRequestBody;
import com.huaweicloud.sdk.codehub.v3.model.AddSshKeyResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddSshKey {

    private static final Logger logger = LoggerFactory.getLogger(AddSshKey.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String addSshKeyAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String addSshKeySk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(addSshKeyAk)
                .withSk(addSshKeySk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeHubRegion.CN_NORTH_4
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头，设置请求参数添加SSH密钥时输入的标题，用户在本地生成的SSH密钥
        AddSshKeyRequest request = new AddSshKeyRequest();
        AddSshKeyRequestBody body = new AddSshKeyRequestBody();
        // 添加SSH密钥时输入的标题，由用户自定义输入。
        String title = "<YOUR TITLE>";
        body.withTitle(title);
        // 用户在本地生成的SSH密钥，通过配置SSH密钥生成
        // 参考https://support.huaweicloud.com/usermanual-codeartsrepo/codeartsrepo_03_0011.html
        String sshKey = "<YOUR SSH KEY>";
        body.withKey(sshKey);
        request.withBody(body);
        try {
            AddSshKeyResponse response = client.addSshKey(request);
            logger.info("AddSshKey: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("AddSshKey connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("AddSshKey RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("AddSshKey ServiceResponseException", e);
        }
    }
}