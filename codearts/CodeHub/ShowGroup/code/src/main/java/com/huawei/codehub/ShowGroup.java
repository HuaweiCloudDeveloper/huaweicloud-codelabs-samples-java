package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v4.CodeHubClient;
import com.huaweicloud.sdk.codehub.v4.model.ShowGroupRequest;
import com.huaweicloud.sdk.codehub.v4.model.ShowGroupResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.codehub.v4.region.CodeHubRegion;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowGroup {

    private static final Logger logger = LoggerFactory.getLogger(ShowGroup.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String showGroupAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showGroupSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(showGroupAk)
                .withSk(showGroupSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeHubRegion.CN_NORTH_4
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求，设置请求参数项目id，代码组id
        ShowGroupRequest request = new ShowGroupRequest();
        // 项目id。注意,如下的 projectId 请使用CodeArts对应项目首页，点击某个项目链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/List
        String projectId = "<YOUR PROJECT ID>";
        request.withProjectId(projectId);
        // 代码组id。可以通过项目下拥有创建权限的代码组列表ListManageableGroups接口获取，返回值中有代码组id的信息
        // 例如：1234567，实际使用时，需要改成自己实际的代码组id
        Integer groupId = 1234567;
        request.withGroupId(groupId);
        try {
            ShowGroupResponse response = client.showGroup(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}