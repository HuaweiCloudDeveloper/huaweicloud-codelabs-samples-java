package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.model.ListRepoMembersRequest;
import com.huaweicloud.sdk.codehub.v3.model.ListRepoMembersResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListRepoMembers {

    private static final Logger logger = LoggerFactory.getLogger(ListRepoMembers.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listRepoMembersAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listRepoMembersSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(listRepoMembersAk)
                .withSk(listRepoMembersSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeHubRegion.CN_NORTH_4
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求，设置请求参数仓库uuid
        ListRepoMembersRequest request = new ListRepoMembersRequest();
        // 仓库uuid(由CreateRepository接口，或者ListUserAllRepositories接口返回),用来指定删除的仓库
        String repositoryUuid = "<YOUR REPOSITORY UUID>";
        request.withRepositoryUuid(repositoryUuid);
        try {
            ListRepoMembersResponse response = client.listRepoMembers(request);
            logger.info("ListRepoMembers: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListRepoMembers connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListRepoMembers RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListRepoMembers ServiceResponseException", e);
        }
    }
}