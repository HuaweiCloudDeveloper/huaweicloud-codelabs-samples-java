### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeHub/sdk?api=ListBranchesByRepositoryId) 中直接运行调试该接口。

2.在本样例中，您可以获取仓库分支列表。

3.获取仓库分支列表，可以对分支进行操作。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目。

6.已在代码托管创建代码仓库。

7.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codehub”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codehub</artifactId>
    <version>3.1.113</version>
</dependency>
```

### 代码示例
``` java
public class ListBranchesByRepositoryId {

    private static final Logger logger = LoggerFactory.getLogger(ListBranchesByRepositoryId.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listBranchesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listBranchesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(listBranchesAk)
                .withSk(listBranchesSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeHubRegion.CN_NORTH_4
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求，设置请求参数仓库主键id，分页页数，每页数据数
        ListBranchesByRepositoryIdRequest request = new ListBranchesByRepositoryIdRequest();
        // 仓库主键id，代码托管点击某个仓库的链接中的repositoryId，使用时替换成实际的仓库主键id
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/home?ref=master
        Integer repositoryId = 1234567;
        request.withRepositoryId(repositoryId);
        // 分页页数
        request.withPage("1");
        // 每页数据数
        request.withPerPage("10");
        try {
            ListBranchesByRepositoryIdResponse response = client.listBranchesByRepositoryId(request);
            logger.info("ListBranchesByRepositoryId: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListBranchesByRepositoryId connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListBranchesByRepositoryId RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListBranchesByRepositoryId ServiceResponseException", e);
        }
    }
}
```

### 返回结果示例
```
{
 "result": {
  "total": 1,
  "branches": [
   {
    "name": "master",
    "commit": {
     "id": "a27****",
     "title": "initial commit",
     "message": "initial commit\n",
     "committed_date": "2023-03-24T03:10:11.000Z",
     "short_id": "a274abfb",
     "committer_name": "Repo"
    },
    "diverging_commit_counts": {
     "behind": 0,
     "ahead": 0
    }
   }
  ]
 },
 "status": "success"
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-09-18 | 1.0 | 文档首次发布 |