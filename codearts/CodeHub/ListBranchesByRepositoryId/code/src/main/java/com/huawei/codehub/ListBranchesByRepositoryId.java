package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.ListBranchesByRepositoryIdRequest;
import com.huaweicloud.sdk.codehub.v3.model.ListBranchesByRepositoryIdResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListBranchesByRepositoryId {

    private static final Logger logger = LoggerFactory.getLogger(ListBranchesByRepositoryId.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listBranchesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listBranchesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(listBranchesAk)
                .withSk(listBranchesSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeHubRegion.CN_NORTH_4
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求，设置请求参数仓库主键id，分页页数，每页数据数
        ListBranchesByRepositoryIdRequest request = new ListBranchesByRepositoryIdRequest();
        // 仓库主键id，代码托管点击某个仓库的链接中的repositoryId，使用时替换成实际的仓库主键id
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/home?ref=master
        Integer repositoryId = 1234567;
        request.withRepositoryId(repositoryId);
        // 分页页数
        request.withPage("1");
        // 每页数据数
        request.withPerPage("10");
        try {
            ListBranchesByRepositoryIdResponse response = client.listBranchesByRepositoryId(request);
            logger.info("ListBranchesByRepositoryId: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListBranchesByRepositoryId connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListBranchesByRepositoryId RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListBranchesByRepositoryId ServiceResponseException", e);
        }
    }
}