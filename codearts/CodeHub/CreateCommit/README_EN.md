### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeHub/sdk?api=CreateCommit).

2.In this example, you can create a submission.

3.Create a submission interface. Multiple files in different directories can be submitted at a time. If the directory does not exist, the directory can be automatically created. The forcible overwrite option is supported. When the forcible overwrite flag is set to true, the conflict is ignored and the task is forcibly submitted.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. 
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. 
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A project and a code repository have been created on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-codehub. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codehub</artifactId>
    <version>3.1.122</version>
</dependency>
```

### Code example
``` java
package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.CommitAction;
import com.huaweicloud.sdk.codehub.v3.model.CreateCommitRequest;
import com.huaweicloud.sdk.codehub.v3.model.CreateCommitRequestBody;
import com.huaweicloud.sdk.codehub.v3.model.CreateCommitResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.ArrayList;

public class CreateCommit {

    private static final Logger logger = LoggerFactory.getLogger(CreateCommit.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String createCommitAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String createCommitSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(createCommitAk)
                .withSk(createCommitSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CodeHubRegion.CN_NORTH_4.
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request, set the primary key ID of the request parameter warehouse, submit the processing list, submit information, and target branch.
        CreateCommitRequest request = new CreateCommitRequest();
        // Specifies the primary key ID of a warehouse. The value of this parameter is the repositoryId in the link of a warehouse. Replace it with the actual primary key ID of the warehouse.
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/home?ref=master
        Integer repositoryId = 1234567;
        request.withRepoId(repositoryId);
        CreateCommitRequestBody body = new CreateCommitRequestBody();
        // Submission Processing List
        List<CommitAction> listbodyActions = new ArrayList<>();
        listbodyActions.add(
            new CommitAction()
                .withAction("create") // Operations to perform: create, delete, move, update, chmod
                .withFilePath("<YOUR FILE PATH>") // The full path of the file. For example, lib/class.rb
        );
        body.withActions(listbodyActions);
        // Submitting Information
        body.withCommitMessage("<YOUR COMMIT MESSAGE>");
        // Target Branch
        body.withBranch("<YOUR BRANCH>");
        request.withBody(body);
        try {
            CreateCommitResponse response = client.createCommit(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
```
{
 "result": {
  "id": "a60e****",
  "title": "add",
  "message": "add",
  "stats": {
   "additions": 1,
   "deletions": 0,
   "total": 1
  },
  "short_id": "a60e****",
  "author_name": "demouser",
  "author_email": "f1ac****@huawei.com",
  "committer_name": "demouser",
  "committer_email": "f1ac****@huawei.com",
  "created_at": "2024-11-15T06:26:43.000Z",
  "parent_ids": [
   "195b****"
  ],
  "committed_date": "2024-11-15T06:26:43.000Z",
  "authored_date": "2024-11-15T06:26:43.000Z"
 },
 "status": "success"
}
```
### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/11/15 | 1.0 | This document is released for the first time.