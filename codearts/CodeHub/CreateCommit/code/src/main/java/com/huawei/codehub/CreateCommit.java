package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.CommitAction;
import com.huaweicloud.sdk.codehub.v3.model.CreateCommitRequest;
import com.huaweicloud.sdk.codehub.v3.model.CreateCommitRequestBody;
import com.huaweicloud.sdk.codehub.v3.model.CreateCommitResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.ArrayList;

public class CreateCommit {

    private static final Logger logger = LoggerFactory.getLogger(CreateCommit.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String createCommitAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String createCommitSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(createCommitAk)
                .withSk(createCommitSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CodeHubRegion.CN_NORTH_4.
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request, set the primary key ID of the request parameter warehouse, submit the processing list, submit information, and target branch.
        CreateCommitRequest request = new CreateCommitRequest();
        // Specifies the primary key ID of a warehouse. The value of this parameter is the repositoryId in the link of a warehouse. Replace it with the actual primary key ID of the warehouse.
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/home?ref=master
        Integer repositoryId = 1234567;
        request.withRepoId(repositoryId);
        CreateCommitRequestBody body = new CreateCommitRequestBody();
        // Submission Processing List
        List<CommitAction> listbodyActions = new ArrayList<>();
        listbodyActions.add(
            new CommitAction()
                .withAction("create") // Operations to perform: create, delete, move, update, chmod
                .withFilePath("<YOUR FILE PATH>") // The full path of the file. For example, lib/class.rb
        );
        body.withActions(listbodyActions);
        // Submitting Information
        body.withCommitMessage("<YOUR COMMIT MESSAGE>");
        // Target Branch
        body.withBranch("<YOUR BRANCH>");
        request.withBody(body);
        try {
            CreateCommitResponse response = client.createCommit(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}