### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeHub/debug?api=ShowRepositoryByUuid) 中直接运行调试该接口。

2.在本样例中，您可以查询某个仓库的详细信息。

3.查询某个仓库的详细信息， 可以获得仓库名、创建时间、使用 ssh 方式克隆仓库时所使用的 url、使用 https 克隆仓库时所使用的 url等信息，通过这些信息，可以进行代码仓库克隆等操作。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目，已在代码托管创建代码仓库。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codehub”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codehub</artifactId>
    <version>3.1.115</version>
</dependency>
```

### 代码示例
``` java
public class ShowRepositoryByUuid {

    private static final Logger logger = LoggerFactory.getLogger(ShowRepositoryByUuid.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String showRepositoryByUuidAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showRepositoryByUuidSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(showRepositoryByUuidAk)
                .withSk(showRepositoryByUuidSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeHubRegion.CN_NORTH_4
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求，设置请求参数仓库的uuid
        ShowRepositoryByUuidRequest request = new ShowRepositoryByUuidRequest();
        // 仓库uuid(由CreateRepository接口，或者ListUserAllRepositories接口返回)
        String repositoryUuid = "<YOUR REPOSITORY UUID>";
        request.withRepositoryUuid(repositoryUuid);
        try {
            ShowRepositoryByUuidResponse response = client.showRepositoryByUuid(request);
            logger.info("ShowRepositoryByUuid: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ShowRepositoryByUuid connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ShowRepositoryByUuid RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ShowRepositoryByUuid ServiceResponseException", e);
        }
    }
}
```

### 返回结果示例
```
{
 "result": {
  "star": null,
  "status": 0,
  "userRole": null,
  "repository_uuid": "4884****",
  "repository_id": 26****,
  "repository_name": "javawebdemo",
  "ssh_url": "git@codehub.devcloud.cn-north-4.huaweicloud.com:ad16****/javawebdemo.git",
  "https_url": "https://codehub.devcloud.cn-north-4.huaweicloud.com/ad16****/javawebdemo.git",
  "group_name": "ad16****",
  "web_url": "https://devcloud.cn-north-4.huaweicloud.com/codehub/26****/home",
  "description": "",
  "visibility_level": 0,
  "created_at": "2024-08-27 19:10:50",
  "updated_at": null,
  "repository_size": null,
  "lfs_size": null,
  "creator_name": null,
  "domain_name": null,
  "is_owner": null,
  "iam_user_uuid": null,
  "project_uuid": "ad16****",
  "project_is_deleted": "false"
 },
 "status": "success"
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-09-27 | 1.0 | 文档首次发布 |