package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v4.CodeHubClient;
import com.huaweicloud.sdk.codehub.v4.model.LockRepositoryRequest;
import com.huaweicloud.sdk.codehub.v4.model.LockRepositoryResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.codehub.v4.region.CodeHubRegion;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LockRepository {

    private static final Logger logger = LoggerFactory.getLogger(LockRepository.class.getName());

    public static void main(String[] args) {

        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String lockRepositoryAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String lockRepositorySk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(lockRepositoryAk)
                .withSk(lockRepositorySk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeHubRegion.CN_NORTH_4
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头，设置请求参数项目id，仓库主键id
        LockRepositoryRequest request = new LockRepositoryRequest();
        // 项目id，注意，如下的 projectId 请使用CodeArts对应项目首页，点击某个项目链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/List
        String projectId = "<YOUR PROJECT ID>";
        request.withProjectId(projectId);
        // 仓库主键id，代码托管点击某个仓库的链接中的repositoryId，使用时替换成实际的仓库主键id
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/home?ref=master
        Integer repositoryId = 1234567;
        request.withRepositoryId(repositoryId);
        try {
            LockRepositoryResponse response = client.lockRepository(request);
            logger.info("LockRepository: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("LockRepository connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("LockRepository RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("LockRepository ServiceResponseException", e);
        }
    }
}