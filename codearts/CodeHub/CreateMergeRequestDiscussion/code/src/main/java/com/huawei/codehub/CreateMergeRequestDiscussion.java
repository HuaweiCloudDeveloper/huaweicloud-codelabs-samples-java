package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.model.CreateMergeRequestDiscussionBodyDto;
import com.huaweicloud.sdk.codehub.v3.model.CreateMergeRequestDiscussionRequest;
import com.huaweicloud.sdk.codehub.v3.model.CreateMergeRequestDiscussionResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateMergeRequestDiscussion {

    private static final Logger logger = LoggerFactory.getLogger(CreateMergeRequestDiscussion.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String createMergeRequestDiscussionAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String createMergeRequestDiscussionSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(createMergeRequestDiscussionAk)
                .withSk(createMergeRequestDiscussionSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CodeHubRegion.CN_NORTH_4.
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request, set the primary key ID of the request parameter warehouse, combine the request ID, and review comment content.
        CreateMergeRequestDiscussionRequest request = new CreateMergeRequestDiscussionRequest();
        // Specifies the primary key ID of a warehouse. The value of this parameter is the repositoryId in the link of a warehouse. Replace it with the actual primary key ID of the warehouse.
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/home?ref=master
        Integer repositoryId = 1234567;
        request.withRepositoryId(repositoryId);
        // iid of the merge request. The ListMergeRequest interface obtains the value of iid in merge_requests.
        // Replace it with the actual combination request iid.
        Integer mergeRequestIid = 1;
        request.withMergeRequestIid(mergeRequestIid);
        CreateMergeRequestDiscussionBodyDto body = new CreateMergeRequestDiscussionBodyDto();
        // Review comments, for example, please modify the parameters of the method.
        body.withBody("请修改方法的参数");
        request.withBody(body);
        try {
            CreateMergeRequestDiscussionResponse response = client.createMergeRequestDiscussion(request);
            logger.info("CreateMergeRequestDiscussion: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("CreateMergeRequestDiscussion connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("CreateMergeRequestDiscussion RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("CreateMergeRequestDiscussion ServiceResponseException", e);
        }
    }
}