### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeHub/doc?api=CreateMergeRequestDiscussion).

2.In this example, you can create MR review comments.

3.By creating MR review comments, developers and reviewers can create and manage review comments in MRs. This makes the code review process more efficient.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. 
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. 
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A project and a code repository have been created on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-codehub. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codehub</artifactId>
    <version>3.1.117</version>
</dependency>
```

### Code example
``` java
package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.model.CreateMergeRequestDiscussionBodyDto;
import com.huaweicloud.sdk.codehub.v3.model.CreateMergeRequestDiscussionRequest;
import com.huaweicloud.sdk.codehub.v3.model.CreateMergeRequestDiscussionResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateMergeRequestDiscussion {

    private static final Logger logger = LoggerFactory.getLogger(CreateMergeRequestDiscussion.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String createMergeRequestDiscussionAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String createMergeRequestDiscussionSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(createMergeRequestDiscussionAk)
                .withSk(createMergeRequestDiscussionSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CodeHubRegion.CN_NORTH_4.
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request, set the primary key ID of the request parameter warehouse, combine the request ID, and review comment content.
        CreateMergeRequestDiscussionRequest request = new CreateMergeRequestDiscussionRequest();
        // Specifies the primary key ID of a warehouse. The value of this parameter is the repositoryId in the link of a warehouse. Replace it with the actual primary key ID of the warehouse.
        // https://devcloud.cn-north-4.huaweicloud.com/codehub/project/{projectId}/codehub/{repositoryId}/home?ref=master
        Integer repositoryId = 1234567;
        request.withRepositoryId(repositoryId);
        // iid of the merge request. The ListMergeRequest interface obtains the value of iid in merge_requests.
        // Replace it with the actual combination request iid.
        Integer mergeRequestIid = 1;
        request.withMergeRequestIid(mergeRequestIid);
        CreateMergeRequestDiscussionBodyDto body = new CreateMergeRequestDiscussionBodyDto();
        // Review comments, for example, please modify the parameters of the method.
        body.withBody("Modify the parameters of the method.");
        request.withBody(body);
        try {
            CreateMergeRequestDiscussionResponse response = client.createMergeRequestDiscussion(request);
            logger.info("CreateMergeRequestDiscussion: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("CreateMergeRequestDiscussion connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("CreateMergeRequestDiscussion RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("CreateMergeRequestDiscussion ServiceResponseException", e);
        }
    }
}
```

### Example of the returned result
```
{
 "result": {
  "id": "0098****",
  "individual_note": false,
  "notes": [
   {
    "id": 136****,
    "type": "DiscussionNote",
    "body": "请修改",
    "author": {
     "id": 27****,
     "name": "demouser",
     "username": "f1ac****",
     "state": "active",
     "avatar_url": "",
     "email": "f1ac****@huawei.com",
     "name_cn": "demouser",
     "web_url": "https://devcloud.cn-north-4.huaweicloud.com/f1ac****",
     "nick_name": "Developer",
     "tenant_name": "demouser"
    },
    "created_at": "2024-10-29T14:06:43.000+08:00",
    "updated_at": "2024-10-29T14:06:43.000+08:00",
    "system": false,
    "noteable_id": 125****,
    "noteable_type": "MergeRequest",
    "resolvable": true,
    "is_reply": false,
    "noteable_iid": 1,
    "discussion_id": "0098****",
    "project": "ad16****/javawebdemo",
    "diff": "",
    "archived": false,
    "review_categories_cn": "",
    "review_categories_en": "",
    "severity": "suggestion",
    "severity_cn": "建议",
    "severity_en": "Suggestion",
    "assignee": {
     "id": 27****,
     "name": "demouser",
     "username": "f1ac****",
     "state": "active",
     "avatar_url": "",
     "email": "f1ac****@huawei.com",
     "name_cn": "demouser",
     "web_url": "https://devcloud.cn-north-4.huaweicloud.com/f1ac****",
     "nick_name": "Developer",
     "tenant_name": "demouser"
    },
    "proposer": {
     "id": 27****,
     "name": "demouser",
     "username": "f1ac****",
     "state": "active",
     "avatar_url": "",
     "email": "f1ac****@huawei.com",
     "name_cn": "demouser",
     "web_url": "https://devcloud.cn-north-4.huaweicloud.com/f1ac****",
     "nick_name": "Developer",
     "tenant_name": "demouser"
    },
    "resolved": false
   }
  ],
  "project_id": 268****,
  "noteable_type": "MergeRequest",
  "project_full_path": "ad16****/javawebdemo",
  "resolved": false,
  "archived": false,
  "review_categories_cn": "",
  "review_categories_en": "",
  "severity": "suggestion",
  "severity_cn": "建议",
  "severity_en": "Suggestion",
  "assignee": {
   "id": 27****,
   "name": "demouser",
   "username": "f1ac****",
   "state": "active",
   "avatar_url": "",
   "email": "f1ac****@huawei.com",
   "name_cn": "demouser",
   "web_url": "https://devcloud.cn-north-4.huaweicloud.com/f1ac****",
   "nick_name": "Developer",
   "tenant_name": "demouser"
  },
  "proposer": {
   "id": 27****,
   "name": "demouser",
   "username": "f1ac****",
   "state": "active",
   "avatar_url": "",
   "email": "f1ac****@huawei.com",
   "name_cn": "demouser",
   "web_url": "https://devcloud.cn-north-4.huaweicloud.com/f1ac****",
   "nick_name": "Developer",
   "tenant_name": "demouser"
  },
  "diff_file": ""
 },
 "status": "success"
}
```
### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/10/29 | 1.0 | This document is released for the first time.