package com.huawei.codehub;

import com.huaweicloud.sdk.codehub.v3.CodeHubClient;
import com.huaweicloud.sdk.codehub.v3.model.DeleteRepositoryRequest;
import com.huaweicloud.sdk.codehub.v3.model.DeleteRepositoryResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.codehub.v3.region.CodeHubRegion;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteRepository {

    private static final Logger logger = LoggerFactory.getLogger(DeleteRepository.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String deleteRepositoryAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String deleteRepositorySk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(deleteRepositoryAk)
                .withSk(deleteRepositorySk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeHubRegion.CN_NORTH_4
        CodeHubClient client = CodeHubClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeHubRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();

        // 构建请求头，设置请求参数仓库uuid
        DeleteRepositoryRequest request = new DeleteRepositoryRequest();
        // 仓库uuid(由CreateRepository接口，或者ListUserAllRepositories接口返回)，用来指定删除的仓库
        String repositoryUuid = "<YOUR REPOSITORY UUID>";
        request.withRepositoryUuid(repositoryUuid);
        try {
            DeleteRepositoryResponse response = client.deleteRepository(request);
            logger.info("DeleteRepository: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("DeleteRepository connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("DeleteRepository RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("DeleteRepository ServiceResponseException", e);
        }
    }
}