package com.huawei.codeartscheck;

import com.huaweicloud.sdk.codeartscheck.v2.CodeArtsCheckClient;
import com.huaweicloud.sdk.codeartscheck.v2.model.CreateRulesetRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.CreateRulesetResponse;
import com.huaweicloud.sdk.codeartscheck.v2.model.Ruleset;
import com.huaweicloud.sdk.codeartscheck.v2.region.CodeArtsCheckRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateRuleset {

    private static final Logger logger = LoggerFactory.getLogger(CreateRuleset.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 注意，如下的 projectId rulesetId请使用对应CodeArts项目中代码检查任务规则集详情链接中的ID
        //https://devcloud.cn-north-4.huaweicloud.com/codechecknew/project/{projectId}/codecheck/ruleset/{rulesetId}/config
        String projectId = "<YOUR PROJECT ID>";
        // templateId为规则集ID ruleIds为启用的id
        // 这两个参数可从下面接口返回值中获取
        // https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsCheck/doc?api=ListRulesets
        // 例:template_id = "9e3c555c8c2611edab16fa163********";rule_ids = "c1bc23b36d6411edab16fa1********,c3b162516d6411edab16fa1********"
        String templateId = "<YOUR TEMPLATE ID>";
        String ruleIds = "<YOUR RULE IDS>";
        // 配置认证信息
        ICredential iCredential = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CodeArtsCheckRegion.CN_NORTH_4
        CodeArtsCheckClient client = CodeArtsCheckClient.newBuilder()
            .withCredential(iCredential)
            .withRegion(CodeArtsCheckRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // 创建请求头
        CreateRulesetRequest request = new CreateRulesetRequest();
        Ruleset body = new Ruleset();
        // 设置基于的规则集ID
        body.withTemplateId(templateId);
        // 设置启用的规则
        body.withRuleIds(ruleIds);
        // 如果有基于的规则集则是1,没有基于的规则集则是0
        body.withIsDefault("1");
        // 规则集语言 例:java cpp
        body.withLanguage("java");
        // 设置创建的自定义规则集名称
        body.withTemplateName("自定义规则集名称");
        // 设置projectId
        body.withProjectId(projectId);
        request.withBody(body);
        try {
            // 创建自定义规则集
            CreateRulesetResponse response = client.createRuleset(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}