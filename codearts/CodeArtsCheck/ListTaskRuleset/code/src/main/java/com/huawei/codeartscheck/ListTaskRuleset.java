package com.huawei.codeartscheck;

import com.huaweicloud.sdk.codeartscheck.v2.CodeArtsCheckClient;
import com.huaweicloud.sdk.codeartscheck.v2.model.ListTaskRulesetRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.ListTaskRulesetResponse;
import com.huaweicloud.sdk.codeartscheck.v2.region.CodeArtsCheckRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListTaskRuleset {

    private static final Logger logger = LoggerFactory.getLogger(ListTaskRuleset.class.getName());

    public static void main(String[] args) throws InterruptedException {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 注意，如下的projectId taskId 请使用对应CodeArts项目中代码检查任务详情链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/codechecknew/project/{projectId}/codecheck/task/{taskId}/detail
        String projectId = "<YOUR PROJECT ID>";
        String taskId = "<YOUR TASK ID>";
        // 配置认证信息
        ICredential credential = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CodeArtsCheckRegion.CN_NORTH_4
        CodeArtsCheckClient client = CodeArtsCheckClient.newBuilder()
            .withCredential(credential)
            .withRegion(CodeArtsCheckRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // 构建请求头并设置projectId,taskId
        ListTaskRulesetRequest request = new ListTaskRulesetRequest();
        request.withProjectId(projectId);
        request.withTaskId(taskId);
        try {
            // 查询CodeArtsCheckRegion.CN_NORTH_4下项目CodeArtsCheck检查任务的已选规则集列表
            ListTaskRulesetResponse response = client.listTaskRuleset(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}