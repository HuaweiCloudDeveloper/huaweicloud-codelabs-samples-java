package com.huawei.codeartscheck;

import com.huaweicloud.sdk.codeartscheck.v2.CodeArtsCheckClient;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowTasklogRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowTasklogResponse;
import com.huaweicloud.sdk.codeartscheck.v2.region.CodeArtsCheckRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowTasklog {

    private static final Logger logger = LoggerFactory.getLogger(ShowTasklog.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Note that the following projectId taskId executeId is the ID of the code check task check log page in the corresponding CodeArts project.
        // Example:https://devcloud.cn-north-4.huaweicloud.com/codechecknew/project/{projectId}/codecheck/task/{taskId}/tasklog?jobId={executeId}
        String projectId = "<YOUR PROJECT ID>";
        String taskId = "<YOUR TASK ID>";
        String executeId = "<YOUR EXECUTE ID>";
        // Configuring Authentication Information
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // Create a service client and set the corresponding region to CodeArtsCheckRegion.CN_NORTH_4.
        CodeArtsCheckClient client = CodeArtsCheckClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeArtsCheckRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // Check the request header and set projectId, taskId, and executeId.
        ShowTasklogRequest request = new ShowTasklogRequest();
        request.withProjectId(projectId);
        request.withTaskId(taskId);
        request.withExecuteId(executeId);
        try {
            // CodeArtsCheck Query Task Check Logs in CodeArtsCheckRegion.CN_NORTH_4
            ShowTasklogResponse response = client.showTasklog(request);
            // Print Results
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}