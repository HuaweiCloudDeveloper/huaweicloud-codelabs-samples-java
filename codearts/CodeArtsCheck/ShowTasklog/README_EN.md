### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeArtsCheck/doc?api=ShowTasklog).

2.In this example, you can query the CodeArtsCheck task check failure logs.

3.You can use this interface to obtain CodeArtsCheck task check failure logs and display task execution results.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.You have created a project, created a check task on the CodeArts platform, and run the check task.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-codeartscheck. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartscheck</artifactId>
    <version>3.1.108</version>
</dependency>
```

### Code example

In this example, you can query the CodeArtsCheck task check failure logs.

``` java
package com.huawei.codeartscheck;

import com.huaweicloud.sdk.codeartscheck.v2.CodeArtsCheckClient;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowTasklogRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowTasklogResponse;
import com.huaweicloud.sdk.codeartscheck.v2.region.CodeArtsCheckRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowTasklog {

    private static final Logger logger = LoggerFactory.getLogger(ShowTasklog.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Note that the following projectId taskId executeId is the ID of the code check task check log page in the corresponding CodeArts project.
        // Example:https://devcloud.cn-north-4.huaweicloud.com/codechecknew/project/{projectId}/codecheck/task/{taskId}/tasklog?jobId={executeId}
        String projectId = "<YOUR PROJECT ID>";
        String taskId = "<YOUR TASK ID>";
        String executeId = "<YOUR EXECUTE ID>";
        // Configuring Authentication Information
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // Create a service client and set the corresponding region to CodeArtsCheckRegion.CN_NORTH_4.
        CodeArtsCheckClient client = CodeArtsCheckClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeArtsCheckRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // Check the request header and set projectId, taskId, and executeId.
        ShowTasklogRequest request = new ShowTasklogRequest();
        request.withProjectId(projectId);
        request.withTaskId(taskId);
        request.withExecuteId(executeId);
        try {
            // CodeArtsCheck Query Task Check Logs in CodeArtsCheckRegion.CN_NORTH_4
            ShowTasklogResponse response = client.showTasklog(request);
            // Print Results
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}

```

### Example of the returned result

#### ShowTasklog

```
{
 "param_info": {
  "url": "git@codehub.devcloud.cn-north-4.huaweicloud.**************0230821-springboot.git",
  "branch": "develop",
  "language": "JAVA",
  "exclude_dir": "",
  "encode": "",
  "compile_config": "{\"cmetrics\":{\"is_scan_all_languages\":\"false\"}}",
  "rule_template": ""
 },
 "log_info": [
  {
   "log": "04/09/2024 14:37:39 GMT+08:00 Running on server:21.137.***.***\n04/09/2024 14:37:39 GMT+08:00 waiting incJob to execute\n04/09/2024 14:38:48 GMT+08:00 Job Finish!\n",
   "level": "",
   "analysis": "04/09/2024 14:37:39 GMT+08:00 Running on server:21.137.***.***\n04/09/2024 14:37:39 GMT+08:00 waiting incJob to execute\n04/09/2024 14:38:48 GMT+08:00 Job Finish!\n",
   "faq": "",
   "display_name": ""
  }
 ]
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/09/19 | 1.0 | This document is released for the first time.