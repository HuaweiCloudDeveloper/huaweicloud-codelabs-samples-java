### 产品介绍

1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsCheck/doc?api=ShowTasklog)
中直接运行调试该接口。

2.在本样例中，您可以查询CodeArtsCheck任务检查失败日志。

3.我们可以通过此接口获取CodeArtsCheck任务检查失败日志从而展示任务执行结果。

### 前置条件

1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目创建检查任务并运行。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartscheck”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartscheck</artifactId>
    <version>3.1.108</version>
</dependency>
```

### 代码示例

在本样例中，您可以查询CodeArtsCheck任务检查失败日志：

``` java
package com.huawei.codeartscheck;

import com.huaweicloud.sdk.codeartscheck.v2.CodeArtsCheckClient;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowTasklogRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowTasklogResponse;
import com.huaweicloud.sdk.codeartscheck.v2.region.CodeArtsCheckRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowTasklog {

    private static final Logger logger = LoggerFactory.getLogger(ShowTasklog.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 注意，如下的 projectId taskId executeId请使用对应CodeArts项目中代码检查任务检查日志页面链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/codechecknew/project/{projectId}/codecheck/task/{taskId}/tasklog?jobId={executeId}
        String projectId = "<YOUR PROJECT ID>";
        String taskId = "<YOUR TASK ID>";
        String executeId = "<YOUR EXECUTE ID>";
        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CodeArtsCheckRegion.CN_NORTH_4
        CodeArtsCheckClient client = CodeArtsCheckClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeArtsCheckRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // 构建检查请求头并设置projectId和taskId和executeId
        ShowTasklogRequest request = new ShowTasklogRequest();
        request.withProjectId(projectId);
        request.withTaskId(taskId);
        request.withExecuteId(executeId);
        try {
            // CodeArtsCheckRegion.CN_NORTH_4下CodeArtsCheck查询任务检查日志
            ShowTasklogResponse response = client.showTasklog(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例

#### ShowTasklog

```
{
 "param_info": {
  "url": "git@codehub.devcloud.cn-north-4.huaweicloud.**************0230821-springboot.git",
  "branch": "develop",
  "language": "JAVA",
  "exclude_dir": "",
  "encode": "",
  "compile_config": "{\"cmetrics\":{\"is_scan_all_languages\":\"false\"}}",
  "rule_template": ""
 },
 "log_info": [
  {
   "log": "04/09/2024 14:37:39 GMT+08:00 Running on server:21.137.***.***\n04/09/2024 14:37:39 GMT+08:00 waiting incJob to execute\n04/09/2024 14:38:48 GMT+08:00 Job Finish!\n",
   "level": "",
   "analysis": "04/09/2024 14:37:39 GMT+08:00 Running on server:21.137.***.***\n04/09/2024 14:37:39 GMT+08:00 waiting incJob to execute\n04/09/2024 14:38:48 GMT+08:00 Job Finish!\n",
   "faq": "",
   "display_name": ""
  }
 ]
}
```

### 修订记录

    发布日期    | 文档版本 | 修订说明

:----------:|:----:| :------:  
2024/09/09 | 1.0 | 文档首次发布