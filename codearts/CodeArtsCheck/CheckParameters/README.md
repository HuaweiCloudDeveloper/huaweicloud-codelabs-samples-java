### 产品介绍

1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsCheck/doc?api=CheckParameters)
中直接运行调试该接口。

2.在本样例中，您可以查询CodeArtsCheck任务规则集的检查参数。

3.我们可以通过此接口检查任务规则集的检查参数是否符合我们预期。

### 前置条件

1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目创建检查任务。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartscheck”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartscheck</artifactId>
    <version>3.1.108</version>
</dependency>
```

### 代码示例

在本样例中，您可以查询CodeArtsCheck任务配置检查参数：

``` java
package com.huawei.codeartscheck;

import com.huaweicloud.sdk.codeartscheck.v2.CodeArtsCheckClient;
import com.huaweicloud.sdk.codeartscheck.v2.model.CheckParametersRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.CheckParametersResponse;
import com.huaweicloud.sdk.codeartscheck.v2.region.CodeArtsCheckRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckParameters {

    private static final Logger logger = LoggerFactory.getLogger(CheckParameters.class.getName());

    public static void main(String[] args) throws InterruptedException {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 注意，如下的projectId taskId 请使用对应CodeArts项目中代码检查任务详情链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/codechecknew/project/{projectId}/codecheck/task/{taskId}/detail
        String projectId = "<YOUR PROJECT ID>";
        String taskId = "<YOUR TASK ID>";
        // 注意，如下的rulesetId请使用包含有检查参数设置的规则集的rulesetId
        //https://devcloud.cn-north-4.huaweicloud.com/codechecknew/project/{projectId}/codecheck/ruleset/{rulesetId}/config
        String rulesetId = "<YOUR RULESET ID>";
        // 规则集语言可在规则集详情页面获取例: java
        String language = "<LANGUAGE>";
        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CodeArtsCheckRegion.CN_NORTH_4
        CodeArtsCheckClient client = CodeArtsCheckClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeArtsCheckRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // 构建执行检查任务请求头并设置projectId,taskId,rulesetId,language
        CheckParametersRequest request = new CheckParametersRequest();
        request.withProjectId(projectId);
        request.withTaskId(taskId);
        request.withRulesetId(rulesetId);
        request.withLanguage(language);
        try {
            // 查询CodeArtsCheckRegion.CN_NORTH_4下项目下检查任务的规则集的检查参数
            CheckParametersResponse response = client.checkParameters(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例

#### CheckParameters

```
[
 {
  "check_id": 0,
  "name": "compile_tool",
  "checker_configs": [
   {
    "value": "",
    "name": "编译工具版本",
    "cfg_key": "buildToolVersion",
    "default_value": "",
    "option_value": "gradle4.9-jdk1.8,gradle4.8-jdk1.8,gradle4.7-jdk1.8,gradle4.6-jdk1.8,gradle4.5-jdk1.8,gradle4.4-jdk1.8,gradle4.3-jdk1.8,gradle4.2-jdk1.8,gradle4.1-jdk1.8,gradle3.3-jdk1.8,gradle2.14.1-jdk1.8,gradle2.13-jdk1.8,gradle4.9-jdk1.9,gradle4.8-jdk1.9,gradle_wrapper-jdk1.8,gradle5.0-jdk1.8,gradle7.6.2-jdk1.8,gradle7.6.2-jdk1.11,maven3.5.3-jdk1.8,maven3.5.3-jdk1.7,maven3.5.3-jdk1.9,maven3.5.3-jdk1.11",
    "is_required": 0,
    "description": "",
    "type": 2,
    "status": "off"
   },
   {
    "value": "",
    "name": "编译命令",
    "cfg_key": "compileScriptCmd",
    "default_value": "",
    "option_value": "",
    "is_required": 0,
    "description": "勾选编译工具后会自动填充默认的工具版本和命令；如需自定义，可以自行修改",
    "type": 0,
    "status": "off"
   }
  ]
 }
]
```

### 修订记录

    发布日期    | 文档版本 | 修订说明

:----------:|:----:| :------:  
2024/09/05 | 1.0 | 文档首次发布