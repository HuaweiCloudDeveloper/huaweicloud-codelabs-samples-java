package com.huawei.codeartscheck;

import com.huaweicloud.sdk.codeartscheck.v2.CodeArtsCheckClient;
import com.huaweicloud.sdk.codeartscheck.v2.model.CreateTaskRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.CreateTaskRequestV2;
import com.huaweicloud.sdk.codeartscheck.v2.model.CreateTaskResponse;
import com.huaweicloud.sdk.codeartscheck.v2.model.IncConfigV2;
import com.huaweicloud.sdk.codeartscheck.v2.model.RunRequestV2;
import com.huaweicloud.sdk.codeartscheck.v2.model.RunTaskRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.RunTaskResponse;
import com.huaweicloud.sdk.codeartscheck.v2.region.CodeArtsCheckRegion;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CreateRunMergeTask {

    private static final Logger logger = LoggerFactory.getLogger(CreateRunMergeTask.class.getName());

    public static void main(String[] args) throws InterruptedException {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 注意，如下的 projectId taskId请使用对应CodeArts项目中对应gitlab仓库代码检查任务详情链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/codechecknew/project/{projectId}/codecheck/task/{taskId}/detail
        String projectId = "<YOUR PROJECT ID>";
        String taskId = "<YOUR TASK ID>";
        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CodeArtsCheckRegion.CN_NORTH_4
        CodeArtsCheckClient client = CodeArtsCheckClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeArtsCheckRegion.CN_NORTH_4).withHttpConfig(httpConfig)
            .build();
        String mergeTaskId = createMergeTask(projectId, taskId, client);
        runTask(mergeTaskId, client);

    }

    private static String createMergeTask(String projectId, String taskId, CodeArtsCheckClient client) {
        // 构建执行检查任务请求头并设置请求参数
        CreateTaskRequest request = new CreateTaskRequest();
        request.withProjectId(projectId);
        CreateTaskRequestV2 body = new CreateTaskRequestV2();
        IncConfigV2 incConfigbody = new IncConfigV2();
        incConfigbody.withParentTaskId(taskId)
            // 此次合入的gitSourceBranch gitTargetBranch mergeId 在codehub合入请求详情页面,如下
            // http://gitlab.****:8083/root/ceshi/-/merge_requests/{mergeId}
            .withGitSourceBranch("<GIT SOURCE BRANCH>")
            .withGitTargetBranch("<GIT TARGET BRANCH>")
            .withMergeId("<MERGE ID>")
            // webhook触发事件类型,merge_request/push_request
            .withEventType("merge_request")
            // webhook事件状态,open/close/update
            .withAction("open")
            // MR标题,可自定义起名不重复即可
            .withTitle("title");
        List<String> listbodyLanguage = new ArrayList<>();
        // 检查语言,支持cpp,java,js,python,php,css,html,go,typescript
        listbodyLanguage.add("java");
        body.withIncConfig(incConfigbody);
        // 检查类型,支持full/inc两种类型,full表示全量检查,inc表示mr检查
        body.withTaskType("inc");
        body.withLanguage(listbodyLanguage);
        // 仓库默认分支 例:main master
        body.withGitBranch("main");
        // codehub仓库地址 例: http://gitlab.******:**83/root/ceshi.git
        body.withGitUrl("<GIT URL>");
        request.withBody(body);
        String mergeTaskId = "";
        try {
            // 创建mergeTask并获取mergeTaskId
            CreateTaskResponse response = client.createTask(request);
            mergeTaskId = response.getTaskId();
            logger.info(response.toString());
        }  catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
        return mergeTaskId;
    }

    private static void runTask(String mergeTaskId, CodeArtsCheckClient client) {
        // 构建执行检查任务请求头并设置taskId
        RunTaskRequest runTaskRequest = new RunTaskRequest();
        runTaskRequest.withTaskId(mergeTaskId);
        RunRequestV2 runBody = new RunRequestV2();
        runTaskRequest.withBody(runBody);
        try {
            // 运行CodeArtsCheckRegion.CN_NORTH_4下新建的mergeTaskId的代码检查任务
            RunTaskResponse response = client.runTask(runTaskRequest);
            // 打印结果
            logger.info(response.toString());
        }  catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}