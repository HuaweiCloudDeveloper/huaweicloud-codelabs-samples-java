### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeArtsCheck/doc?api=ShowTaskDetail).

2.In this example, you can view the defect summary of the code check results.

3.After a code check task is executed, you can use this interface to determine whether the code check is passed.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A project and host cluster have been created on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-codeartscheck. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartscheck</artifactId>
    <version>3.1.108</version>
</dependency>
```

### Code example

The following code shows how to query the defect summary of the code check result.

``` java
package com.huawei.codeartscheck;

import com.huaweicloud.sdk.codeartscheck.v2.CodeArtsCheckClient;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowTaskDetailRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowTaskDetailResponse;
import com.huaweicloud.sdk.codeartscheck.v2.region.CodeArtsCheckRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowTaskDetail {

    private static final Logger logger = LoggerFactory.getLogger(ShowTaskDetail.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Note: For the following taskId, use the ID in the code check task details link of the corresponding CodeArts project.
        // Example:https://devcloud.cn-north-4.huaweicloud.com/codechecknew/project/{projectId}/codecheck/task/{taskId}/detail
        String taskId = "<YOUR TASK ID>";
        // Configuring Authentication Information
        ICredential icredential = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // Create a service client and set the corresponding region to CodeArtsCheckRegion.CN_NORTH_4.
        CodeArtsCheckClient client = CodeArtsCheckClient.newBuilder()
            .withCredential(icredential)
            .withRegion(CodeArtsCheckRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // Create Query Defect Summary Request Header
        ShowTaskDetailRequest request = new ShowTaskDetailRequest();
        request.withTaskId(taskId);
        try {
            ShowTaskDetailResponse response = client.showTaskDetail(request);
            // Print Results
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}

```

### Example of the returned result

#### ShowTaskDetail

```
{
 "git_url": "git@codehub.devcloud.cn-north-4.huaw******************gboot.git",
 "git_branch": "master",
 "task_id": "bd8c8589fc3b4d308f6b***********",
 "task_name": "test-20230821-springboo**********",
 "code_line_total": 50,
 "code_line": 35,
 "code_quality": 100,
 "creator_id": "49d9a304ab704e1782961******",
 "last_check_time": "2024-08-21T07:17:12Z",
 "issue_count": 2,
 "new_count": 0,
 "solve_count": 0,
 "risk_coefficient": 100,
 "comment_lines": 0,
 "comment_ratio": "0.0",
 "duplication_ratio": "0.0",
 "complexity_count": 4,
 "duplicated_lines": 0,
 "duplicated_blocks": 0,
 "last_exec_time": "2024-08-21T07:17:12Z",
 "check_type": "source",
 "created_at": "2023-12-19T02:22:48Z",
 "cyclomatic_complexity_per_method": "1.0",
 "cyclomatic_complexity_per_file": "0.0",
 "critical_count": "0",
 "major_count": "0",
 "minor_count": "2",
 "suggestion_count": "0",
 "is_access": "1",
 "review_result": "success",
 "trigger_type": "0",
 "file_duplication_ratio": "0.0",
 "duplicated_files": 0,
 "new_critical_count": "0",
 "new_major_count": "0",
 "new_minor_count": "0",
 "new_suggestion_count": "0"
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/09/19 | 1.0 | This document is released for the first time.