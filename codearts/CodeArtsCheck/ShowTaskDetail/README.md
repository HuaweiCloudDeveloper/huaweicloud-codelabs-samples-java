### 产品介绍

1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsCheck/doc?api=ShowTaskDetail)
中直接运行调试该接口。

2.在本样例中，您可以查看代码检查结果的缺陷概要。

3.我们可以代码检查任务执行后，通过此接口判断代码检查是否通过。

### 前置条件

1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartscheck”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartscheck</artifactId>
    <version>3.1.108</version>
</dependency>
```

### 代码示例

以下代码展示如何查询代码检查结果的缺陷概要：

``` java
package com.huawei.codeartscheck;

import com.huaweicloud.sdk.codeartscheck.v2.CodeArtsCheckClient;
import com.huaweicloud.sdk.codeartscheck.v2.model.RunRequestV2;
import com.huaweicloud.sdk.codeartscheck.v2.model.RunTaskRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.RunTaskResponse;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowProgressDetailRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowProgressDetailResponse;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowTaskDetailRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowTaskDetailResponse;
import com.huaweicloud.sdk.codeartscheck.v2.region.CodeArtsCheckRegion;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class ShowTaskDetail {

    private static final Logger logger = LoggerFactory.getLogger(ShowTaskDetail.class.getName());

    public static void main(String[] args) throws InterruptedException {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 注意，如下的 taskId 请使用对应CodeArts项目中代码检查任务详情链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/codechecknew/project/{projectId}/codecheck/task/{taskId}/detail
        String taskId = "<YOUR TASK ID>";
        // 配置认证信息
        ICredential icredential = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CodeArtsCheckRegion.CN_NORTH_4
        CodeArtsCheckClient client = CodeArtsCheckClient.newBuilder()
            .withCredential(icredential)
            .withRegion(CodeArtsCheckRegion.CN_NORTH_4).withHttpConfig(httpConfig)
            .build();
        // 创建查询缺陷概要请求头
        ShowTaskDetailRequest request = new ShowTaskDetailRequest();
        request.withTaskId(taskId);
        try {
            ShowTaskDetailResponse response = client.showTaskDetail(request);
            // 打印结果
            logger.info(response.toString());
        }  catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例

#### ShowTaskDetail

```
{
 "git_url": "git@codehub.devcloud.cn-north-4.huaw******************gboot.git",
 "git_branch": "master",
 "task_id": "bd8c8589fc3b4d308f6b***********",
 "task_name": "test-20230821-springboo**********",
 "code_line_total": 50,
 "code_line": 35,
 "code_quality": 100,
 "creator_id": "49d9a304ab704e1782961******",
 "last_check_time": "2024-08-21T07:17:12Z",
 "issue_count": 2,
 "new_count": 0,
 "solve_count": 0,
 "risk_coefficient": 100,
 "comment_lines": 0,
 "comment_ratio": "0.0",
 "duplication_ratio": "0.0",
 "complexity_count": 4,
 "duplicated_lines": 0,
 "duplicated_blocks": 0,
 "last_exec_time": "2024-08-21T07:17:12Z",
 "check_type": "source",
 "created_at": "2023-12-19T02:22:48Z",
 "cyclomatic_complexity_per_method": "1.0",
 "cyclomatic_complexity_per_file": "0.0",
 "critical_count": "0",
 "major_count": "0",
 "minor_count": "2",
 "suggestion_count": "0",
 "is_access": "1",
 "review_result": "success",
 "trigger_type": "0",
 "file_duplication_ratio": "0.0",
 "duplicated_files": 0,
 "new_critical_count": "0",
 "new_major_count": "0",
 "new_minor_count": "0",
 "new_suggestion_count": "0"
}
```

### 修订记录

    发布日期    | 文档版本 | 修订说明

:----------:|:----:| :------:  
2024/08/08 | 1.0 | 文档首次发布