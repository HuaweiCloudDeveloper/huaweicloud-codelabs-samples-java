### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsCheck/doc?api=ShowTaskListByProjectId) 中直接运行调试该接口。

2.在本样例中，您可以获取到CodeArts某个项目下所有的代码检查任务列表。

3.获取本列表后，您可以根据列表提供的数据排查代码检查的最后执行时间，后续也可以根据列表提供的数据对某个任务进行数据详查。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartscheck”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartscheck</artifactId>
    <version>3.1.108</version>
</dependency>
```

### 代码示例
以下代码展示如何根据项目Id查询该项目下的任务列表：
``` java
package com.huawei.codeartscheck;

import com.huaweicloud.sdk.codeartscheck.v2.CodeArtsCheckClient;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowTaskListByProjectIdRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowTaskListByProjectIdResponse;
import com.huaweicloud.sdk.codeartscheck.v2.region.CodeArtsCheckRegion;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowTaskListByProjectId {

    private static final Logger logger = LoggerFactory.getLogger(ShowTaskListByProjectId.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 注意，如下的 projectId 请使用CodeArts对应项目首页链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/list
        String projectId = "<YOUR PROJECT ID>";
        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CodeArtsCheckRegion.CN_NORTH_4
        CodeArtsCheckClient client = CodeArtsCheckClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeArtsCheckRegion.CN_NORTH_4).withHttpConfig(httpConfig)
            .build();
        // 构建请求头并设置项目ID
        ShowTaskListByProjectIdRequest request = new ShowTaskListByProjectIdRequest();
        request.withProjectId(projectId);
        try {
            // 获取CodeArtsCheckRegion.CN_NORTH_4下projectId的代码检查任务列表
            ShowTaskListByProjectIdResponse response = client.showTaskListByProjectId(request);
            // 打印列表结果
            logger.info(response.toString());
        }  catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例
#### ShowTaskListByProjectId
```
{
    "tasks": [{
        "task_id": "4427a438088543a0ac295711b2f5****",
        "task_name": "********",
        "creator_id": "05c7cfe8df0025e71fc7c005a483****",
        "git_url": "git@*******",
        "git_branch": "master",
        "created_at": "2024-06-10T09:50:50Z",
        "last_check_time": "未检查"
    },
    {
        "task_id": "75a2023b7e56425faa95c7b0f874****",
        "task_name": "Gostudy",
        "creator_id": "05c7cfe8df0025e71fc7c005a483****",
        "git_url": "git@*******",
        "git_branch": "master",
        "created_at": "2023-11-02T06:24:16Z",
        "last_check_time": "2023-11-02T06:24:56Z"
    }
    "total": 2
}


```
### 修订记录
 
 发布日期  | 文档版本 | 修订说明
 :----: |:----:| :------:  
 2024/08/06 | 1.0  | 文档首次发布