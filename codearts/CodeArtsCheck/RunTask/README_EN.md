### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeArtsCheck/doc?api=ShowTaskDetail).

2.In this example, you can perform a code check task and query the task execution status.

3.After the code is submitted, you can use the code to trigger the code to check the task and query the task execution
result.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A project and host cluster have been created on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-codeartscheck. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartscheck</artifactId>
    <version>3.1.108</version>
</dependency>
```

### Code example

The following code shows how to execute a code check task and query the task execution status:

``` java
package com.huawei.codeartscheck;

import com.huaweicloud.sdk.codeartscheck.v2.CodeArtsCheckClient;
import com.huaweicloud.sdk.codeartscheck.v2.model.RunRequestV2;
import com.huaweicloud.sdk.codeartscheck.v2.model.RunTaskRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.RunTaskResponse;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowProgressDetailRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowProgressDetailResponse;
import com.huaweicloud.sdk.codeartscheck.v2.region.CodeArtsCheckRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class RunTask {

    private static final Logger logger = LoggerFactory.getLogger(RunTask.class.getName());

    public static void main(String[] args) throws InterruptedException {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Note: For the following taskId, use the ID in the code check task details link of the corresponding CodeArts project.
        // Example:https://devcloud.cn-north-4.huaweicloud.com/codechecknew/project/{projectId}/codecheck/task/{taskId}/detail
        String taskId = "<YOUR TASK ID>";
        // Configuring Authentication Information
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // Create a service client and set the corresponding region to CodeArtsCheckRegion.CN_NORTH_4.
        CodeArtsCheckClient client = CodeArtsCheckClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeArtsCheckRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // Construct and execute the check task request header and set taskId.
        RunTaskRequest runTaskRequest = new RunTaskRequest();
        runTaskRequest.withTaskId(taskId);
        RunRequestV2 body = new RunRequestV2();
        runTaskRequest.withBody(body);
        try {
            // Run the code check task of taskId in CodeArtsCheckRegion.CN_NORTH_4.
            RunTaskResponse response = client.runTask(runTaskRequest);
            // Print Results
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
        // Construct the request header for querying the task execution status and set taskId.
        ShowProgressDetailRequest request = new ShowProgressDetailRequest();
        request.withTaskId(taskId);
        Integer status = 0;
        // If the value of taskStatus in the cyclic traversal task execution result is 0, the task is being executed. Other values indicate that the task is complete.
        while (status == 0) {
            try {
                // Obtain the code of taskId in CodeArtsCheckRegion.CN_NORTH_4 and check the task execution status.
                ShowProgressDetailResponse response = client.showProgressDetail(request);
                status = response.getTaskStatus();
                // Print Results
                logger.info(response.toString());
                // Cycle every 5 seconds
                TimeUnit.SECONDS.sleep(5);
            } catch (ClientRequestException | ServerResponseException e) {
                logger.error(String.valueOf(e.getHttpStatusCode()));
            }
        }

    }
}

```

### Example of the returned result

#### RunTask

```
{
	"exec_id": "8076c25bab4543059fe5880b0c70****"
}
```

#### ShowProgressDetail

```
{
	"task_status": 0,
	"progress": {
		"ratio": "0%",
		"info": "running"
	}
}

{
	"task_status": 2,
	"progress": {
		"ratio": "100%",
		"info": "success"
	}
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/09/19 | 1.0 | This document is released for the first time.