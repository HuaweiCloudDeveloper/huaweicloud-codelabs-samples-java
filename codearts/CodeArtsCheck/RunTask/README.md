### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsCheck/doc?api=RunTask) 中直接运行调试该接口。

2.在本样例中，您可以执行某个代码检查任务，并查询任务执行状态。

3.我们可以在提交代码后，使用这段代码去触发代码检查任务及查询任务执行结果。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartscheck”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartscheck</artifactId>
    <version>3.1.108</version>
</dependency>
```

### 代码示例
以下代码展示如何执行某个代码检查任务，并查询任务执行状态：
``` java
package com.huawei.codeartscheck;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.huaweicloud.sdk.codeartscheck.v2.CodeArtsCheckClient;
import com.huaweicloud.sdk.codeartscheck.v2.model.RunRequestV2;
import com.huaweicloud.sdk.codeartscheck.v2.model.RunTaskRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.RunTaskResponse;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowProgressDetailRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowProgressDetailResponse;
import com.huaweicloud.sdk.codeartscheck.v2.region.CodeArtsCheckRegion;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class RunTask {

    private static final Logger logger = LoggerFactory.getLogger(RunTask.class.getName());

    public static void main(String[] args) throws InterruptedException, JsonProcessingException {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 注意，如下的 taskId 请使用对应CodeArts项目中代码检查任务详情链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/codechecknew/project/{projectId}/codecheck/task/{taskId}/detail
        String taskId = "<YOUR TASK ID>";
        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CodeArtsCheckRegion.CN_NORTH_4
        CodeArtsCheckClient client = CodeArtsCheckClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeArtsCheckRegion.CN_NORTH_4).withHttpConfig(httpConfig)
            .build();
        // 构建执行检查任务请求头并设置taskId
        RunTaskRequest request = new RunTaskRequest();
        request.withTaskId(taskId);
        RunRequestV2 body = new RunRequestV2();
        request.withBody(body);
        try {
            // 运行CodeArtsCheckRegion.CN_NORTH_4下taskId的代码检查任务
            RunTaskResponse response = client.runTask(request);
            // 打印结果
            logger.info(response.toString());
        }  catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
        // 构建查询任务执行状态请求头并设置taskId
        ShowProgressDetailRequest  showProgressDetailRequest = new ShowProgressDetailRequest();
        showProgressDetailRequest.withTaskId(taskId);
        Integer taskStatus = 0;
        // 循环遍历任务执行结果taskStatus值为0时，表示任务正在执行中。为其他值时表示任务执行结束
        while (taskStatus == 0 ){
            try {
                // 获取CodeArtsCheckRegion.CN_NORTH_4下taskId的代码检查任务执行状态
                ShowProgressDetailResponse response = client.showProgressDetail(showProgressDetailRequest);
                taskStatus = response.getTaskStatus();
                // 打印结果
                logger.info(response.toString());
                // 每5秒一次循环
                TimeUnit.SECONDS.sleep(5);
            } catch (ClientRequestException | ServerResponseException e) {
                logger.error(String.valueOf(e.getHttpStatusCode()));
            }
        }

    }
}
```

### 返回结果示例
#### RunTask
```
{
	"exec_id": "8076c25bab4543059fe5880b0c70****"
}
```

#### ShowProgressDetail
```
{
	"task_status": 0,
	"progress": {
		"ratio": "0%",
		"info": "running"
	}
}

{
	"task_status": 2,
	"progress": {
		"ratio": "100%",
		"info": "success"
	}
}
```

### 修订记录
 
 发布日期  | 文档版本 | 修订说明
 :----: |:----:| :------:  
 2024/08/08 | 1.0  | 文档首次发布