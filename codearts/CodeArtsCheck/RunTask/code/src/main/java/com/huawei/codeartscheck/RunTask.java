package com.huawei.codeartscheck;

import com.huaweicloud.sdk.codeartscheck.v2.CodeArtsCheckClient;
import com.huaweicloud.sdk.codeartscheck.v2.model.RunRequestV2;
import com.huaweicloud.sdk.codeartscheck.v2.model.RunTaskRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.RunTaskResponse;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowProgressDetailRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowProgressDetailResponse;
import com.huaweicloud.sdk.codeartscheck.v2.region.CodeArtsCheckRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class RunTask {

    private static final Logger logger = LoggerFactory.getLogger(RunTask.class.getName());

    public static void main(String[] args) throws InterruptedException {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Note: For the following taskId, use the ID in the code check task details link of the corresponding CodeArts project.
        // Example:https://devcloud.cn-north-4.huaweicloud.com/codechecknew/project/{projectId}/codecheck/task/{taskId}/detail
        String taskId = "<YOUR TASK ID>";
        // Configuring Authentication Information
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // Create a service client and set the corresponding region to CodeArtsCheckRegion.CN_NORTH_4.
        CodeArtsCheckClient client = CodeArtsCheckClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeArtsCheckRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // Construct and execute the check task request header and set taskId.
        RunTaskRequest runTaskRequest = new RunTaskRequest();
        runTaskRequest.withTaskId(taskId);
        RunRequestV2 body = new RunRequestV2();
        runTaskRequest.withBody(body);
        try {
            // Run the code check task of taskId in CodeArtsCheckRegion.CN_NORTH_4.
            RunTaskResponse response = client.runTask(runTaskRequest);
            // Print Results
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
        // Construct the request header for querying the task execution status and set taskId.
        ShowProgressDetailRequest request = new ShowProgressDetailRequest();
        request.withTaskId(taskId);
        Integer status = 0;
        // If the value of taskStatus in the cyclic traversal task execution result is 0, the task is being executed. Other values indicate that the task is complete.
        while (status == 0) {
            try {
                // Obtain the code of taskId in CodeArtsCheckRegion.CN_NORTH_4 and check the task execution status.
                ShowProgressDetailResponse response = client.showProgressDetail(request);
                status = response.getTaskStatus();
                // Print Results
                logger.info(response.toString());
                // Cycle every 5 seconds
                TimeUnit.SECONDS.sleep(5);
            } catch (ClientRequestException | ServerResponseException e) {
                logger.error(String.valueOf(e.getHttpStatusCode()));
            }
        }

    }
}