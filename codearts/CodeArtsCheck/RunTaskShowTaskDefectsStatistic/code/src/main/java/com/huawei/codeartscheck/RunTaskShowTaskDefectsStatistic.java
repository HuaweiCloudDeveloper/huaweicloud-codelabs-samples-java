package com.huawei.codeartscheck;

import com.huaweicloud.sdk.codeartscheck.v2.CodeArtsCheckClient;
import com.huaweicloud.sdk.codeartscheck.v2.model.RunRequestV2;
import com.huaweicloud.sdk.codeartscheck.v2.model.RunTaskRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.RunTaskResponse;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowProgressDetailRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowProgressDetailResponse;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowTaskDefectsStatisticRequest;
import com.huaweicloud.sdk.codeartscheck.v2.model.ShowTaskDefectsStatisticResponse;
import com.huaweicloud.sdk.codeartscheck.v2.region.CodeArtsCheckRegion;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class RunTaskShowTaskDefectsStatistic {

    private static final Logger logger = LoggerFactory.getLogger(RunTaskShowTaskDefectsStatistic.class.getName());

    public static void main(String[] args) throws InterruptedException {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 注意，如下的 taskId 请使用对应CodeArts项目中代码检查任务详情链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/codechecknew/project/{projectId}/codecheck/task/{taskId}/detail
        String taskId = "<YOUR TASK ID>";
        // 配置认证信息
        ICredential iCredential = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CodeArtsCheckRegion.CN_NORTH_4
        CodeArtsCheckClient client = CodeArtsCheckClient.newBuilder()
            .withCredential(iCredential)
            .withRegion(CodeArtsCheckRegion.CN_NORTH_4).withHttpConfig(config)
            .build();
        runTask(taskId, client);
        showProgressDetail(taskId, client);
        showTaskDefectsStatistic(taskId, client);
    }

    private static void runTask(String taskId, CodeArtsCheckClient client) {
        // 构建执行检查任务请求头并设置taskId
        RunTaskRequest request = new RunTaskRequest();
        request.withTaskId(taskId);
        RunRequestV2 body = new RunRequestV2();
        request.withBody(body);
        try {
            // 运行CodeArtsCheckRegion.CN_NORTH_4下taskId的代码检查任务
            RunTaskResponse response = client.runTask(request);
            // 打印结果
            logger.info(response.toString());
        }  catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }

    private static void showProgressDetail(String taskId, CodeArtsCheckClient client) throws InterruptedException {
        // 构建查询任务执行状态请求头并设置taskId
        ShowProgressDetailRequest  showProgressDetailRequest = new ShowProgressDetailRequest();
        showProgressDetailRequest.withTaskId(taskId);
        Integer taskStatus = 0;
        // 循环遍历任务执行结果taskStatus值为0时，表示任务正在执行中。为其他值时表示任务执行结束
        while (taskStatus == 0 ) {
            try {
                // 获取CodeArtsCheckRegion.CN_NORTH_4下taskId的代码检查任务执行状态
                ShowProgressDetailResponse response = client.showProgressDetail(showProgressDetailRequest);
                taskStatus = response.getTaskStatus();
                // 打印结果
                logger.info(response.toString());
                // 每5秒一次循环
                TimeUnit.SECONDS.sleep(5);
            } catch (ClientRequestException | ServerResponseException e) {
                logger.error(String.valueOf(e.getHttpStatusCode()));
            }
        }
    }

    private static void showTaskDefectsStatistic(String taskId, CodeArtsCheckClient client) {
        // 构建检查任务缺陷详情的统计的请求头并设置taskId
        ShowTaskDefectsStatisticRequest showTaskDefectsStatisticRequest = new ShowTaskDefectsStatisticRequest();
        showTaskDefectsStatisticRequest.setTaskId(taskId);
        try {
            // 获取CodeArtsCheckRegion.CN_NORTH_4下taskId任务缺陷详情的统计
            ShowTaskDefectsStatisticResponse response = client.showTaskDefectsStatistic(showTaskDefectsStatisticRequest);
            // 打印结果
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}