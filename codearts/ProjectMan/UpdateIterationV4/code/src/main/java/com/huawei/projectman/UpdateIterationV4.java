package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.UpdateIterationRequestV4;
import com.huaweicloud.sdk.projectman.v4.model.UpdateIterationV4Request;
import com.huaweicloud.sdk.projectman.v4.model.UpdateIterationV4Response;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateIterationV4 {

    private static final Logger logger = LoggerFactory.getLogger(UpdateIterationV4.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential updateIterationV4Auth = new BasicCredentials().withAk(ak).withSk(sk);
        // 创建服务客户端并配置对应region为ProjectManRegion.CN_NORTH_4
        ProjectManClient client = ProjectManClient.newBuilder()
            .withCredential(updateIterationV4Auth)
            .withRegion(ProjectManRegion.CN_NORTH_4)
            .build();
        // 注意，如下的 projectId 请使用CodeArts对应项目首页链接中的{projectId}变量的值
        // 例如：https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/backlog
        String projectId = "<YOUR PROJECT ID>";
        // 构建请求并设置项目ID
        UpdateIterationV4Request request = new UpdateIterationV4Request();
        request.withProjectId(projectId);
        // 迭代id的值可通过“获取指定项目的迭代列表”接口获取，也可以在查看迭代页面的链接中获取
        // 例如：https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/task/sprint/{sprintId}/list 中{sprintId}变量的值
        request.withIterationId(111111);
        // 设置请求体参数
        UpdateIterationRequestV4 body = new UpdateIterationRequestV4();
        // 标题
        body.withName("发布迭代");
        // 开始时间，年-月-日
        body.withBeginTime("2024-10-12");
        // 结束时间，年-月-日
        body.withEndTime("2024-10-20");
        request.withBody(body);
        try {
            // 获取结果
            UpdateIterationV4Response response = client.updateIterationV4(request);
            // 打印结果
            logger.info(String.valueOf(response.getHttpStatusCode()));
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}