### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/ProjectMan/doc?api=UpdateMembesRoleV4).

2.In this example, you can batch update the roles of members in a project.

3.This interface is used to modify the roles of project members in a specified project in batches based on the user ID set and role ID. After the modification is successful, no information is returned and the status code is 204.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. 
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. 
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A project has been created on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-projectman. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-projectman</artifactId>
    <version>3.1.113</version>
</dependency>
```

### Code example
The following code shows how to bulk update the roles of members in a project:
``` java
package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.UpdateMembesRoleV4Request;
import com.huaweicloud.sdk.projectman.v4.model.UpdateMembesRoleV4RequestBody;
import com.huaweicloud.sdk.projectman.v4.model.UpdateMembesRoleV4Response;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class UpdateMembesRoleV4 {

    private static final Logger logger = LoggerFactory.getLogger(UpdateMembesRoleV4.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential updateMembesRoleV4Auth = new BasicCredentials().withAk(ak).withSk(sk);
        // Create a service client and set the corresponding region to ProjectManRegion.CN_NORTH_4.
        ProjectManClient client = ProjectManClient.newBuilder()
            .withCredential(updateMembesRoleV4Auth)
            .withRegion(ProjectManRegion.CN_NORTH_4)
            .build();
        // Note: Use the value of the {projectId} variable in the link of the corresponding project page on CodeArts.
        // Example: https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/backlog
        String projectId = "<YOUR PROJECT ID>";
        // Build the request and set the project ID
        UpdateMembesRoleV4Request request = new UpdateMembesRoleV4Request();
        request.withProjectId(projectId);
        // Construct the request body and set parameters.
        UpdateMembesRoleV4RequestBody body = new UpdateMembesRoleV4RequestBody();
        // Set of user IDs to modify
        List<String> listBodyUserIds = new ArrayList<>();
        // User ID, which can be obtained on the member management page of project settings.
        listBodyUserIds.add("{userId}");
        body.withUserIds(listBodyUserIds);
        // Member Roles：- 1 Project Creator, 3 Project Manager, 4 Developer, 5 Test Manager, 6 Tester, 7 Participants, 8 Viewers, 9 O&M Manager
        body.withRoleId(6);
        request.withBody(body);
        try {
            // Obtaining Results
            UpdateMembesRoleV4Response response = client.updateMembesRoleV4(request);
            // Print Results
            logger.info(String.valueOf(response.getHttpStatusCode()));
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
#### UpdateMembesRoleV4
```
204
class UpdateMembesRoleV4Response {
}
```
### Change History

 Released On  |  Issue   | Description
 :----: |:---:| :------:  
 2024/10/29 | 1.0 | This document is released for the first time.