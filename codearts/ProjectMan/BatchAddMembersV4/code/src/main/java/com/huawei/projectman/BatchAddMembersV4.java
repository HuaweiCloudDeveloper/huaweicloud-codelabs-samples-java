package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.BatchAddMemberRequestV4;
import com.huaweicloud.sdk.projectman.v4.model.BatchAddMembersV4Request;
import com.huaweicloud.sdk.projectman.v4.model.BatchAddMembersV4RequestBody;
import com.huaweicloud.sdk.projectman.v4.model.BatchAddMembersV4Response;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class BatchAddMembersV4 {

    private static final Logger logger = LoggerFactory.getLogger(BatchAddMembersV4.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential batchAddMembersV4Auth = new BasicCredentials().withAk(ak).withSk(sk);
        // 创建服务客户端并配置对应region为ProjectManRegion.CN_NORTH_4
        ProjectManClient client = ProjectManClient.newBuilder()
            .withCredential(batchAddMembersV4Auth)
            .withRegion(ProjectManRegion.CN_NORTH_4)
            .build();
        // 注意，如下的 projectId 请使用CodeArts对应项目首页链接中的{projectId}变量的值
        // 例如：https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/backlog
        String projectId = "<YOUR PROJECT ID>";
        // 构建请求并设置项目ID
        BatchAddMembersV4Request request = new BatchAddMembersV4Request();
        request.withProjectId(projectId);
        BatchAddMembersV4RequestBody body = new BatchAddMembersV4RequestBody();
        // 添加的用户信息
        List<BatchAddMemberRequestV4> listBodyUsers = new ArrayList<>();
        // 参数RoleId为用户在项目中的角色，-1 项目创建者, 3 项目经理, 4 开发人员, 5 测试经理, 6 测试人员, 7 参与者, 8 浏览者, 9 运维经理
        // 参数UserId为用户id，获取方式为：登录华为云管理控制台，单击用户名，在下拉列表中单击“我的凭证”，在“API凭证”页面中查看，IAM用户ID即为用户id。
        listBodyUsers.add(new BatchAddMemberRequestV4().withRoleId(3).withUserId("<YOUR USER ID>"));
        body.withUsers(listBodyUsers);
        request.withBody(body);
        try {
            // 获取结果
            BatchAddMembersV4Response response = client.batchAddMembersV4(request);
            // 打印结果
            logger.info(String.valueOf(response.getHttpStatusCode()));
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}