package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.ListProjectBugStaticsV4Request;
import com.huaweicloud.sdk.projectman.v4.model.ListProjectBugStaticsV4Response;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListProjectBugStaticsV4 {

    private static final Logger logger = LoggerFactory.getLogger(ListProjectBugStaticsV4.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listProjectBugStaticsV4Ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String listProjectBugStaticsV4Sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential listProjectBugStaticsV4Auth = new BasicCredentials().withAk(listProjectBugStaticsV4Ak)
            .withSk(listProjectBugStaticsV4Sk);
        // 注意，如下的 projectId 请使用CodeArts对应项目首页链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/bug
        String projectId = "<YOUR PROJECT ID>";
        // 注意，如下的 regionId 请使用项目所在的局点，例如华北-北京四区域为：cn-north-4
        String regionId = "<YOUR REGION ID>";
        // 创建服务客户端
        ProjectManClient client = ProjectManClient.newBuilder()
            .withCredential(listProjectBugStaticsV4Auth)
            .withRegion(ProjectManRegion.valueOf(regionId))
            .build();
        // 构建请求头并设置项目ID
        ListProjectBugStaticsV4Request request = new ListProjectBugStaticsV4Request();
        request.withProjectId(projectId);
        try {
            // 获取对应局点下projectId项目的bug统计信息
            ListProjectBugStaticsV4Response response = client.listProjectBugStaticsV4(request);
            // 打印列表结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(String.valueOf(e.getRequestId()));
            logger.error(String.valueOf(e.getErrorCode()));
            logger.error(String.valueOf(e.getErrorMsg()));
        }
    }
}