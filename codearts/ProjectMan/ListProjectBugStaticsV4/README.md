### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/ProjectMan/doc?api=ListProjectBugStaticsV4) 中直接运行调试该接口。

2.在本样例中，您可以获取到CodeArts某个项目下所有未关闭bug的统计信息，bug按照模块进行统计，其中状态为已拒绝和已关闭的bug不在统计范围之内。

3.获取统计信息后，可以根据提供的数据分析项目中bug的数量以及分布情况，然后根据数据分析项目的进展，以及分配后续的项目任务。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-projectman”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-projectman</artifactId>
    <version>3.1.108</version>
</dependency>
```

### 代码示例
以下代码展示如何根据CodeArts的项目Id获取该项目下的bug统计信息：
``` java
package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.ListProjectBugStaticsV4Request;
import com.huaweicloud.sdk.projectman.v4.model.ListProjectBugStaticsV4Response;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListProjectBugStaticsV4 {

    private static final Logger logger = LoggerFactory.getLogger(ListProjectBugStaticsV4.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listProjectBugStaticsV4Ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String listProjectBugStaticsV4Sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential listProjectBugStaticsV4Auth = new BasicCredentials().withAk(listProjectBugStaticsV4Ak)
            .withSk(listProjectBugStaticsV4Sk);
        // 注意，如下的 projectId 请使用CodeArts对应项目首页链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/bug
        String projectId = "<YOUR PROJECT ID>";
        // 注意，如下的 regionId 请使用项目所在的局点，例如华北-北京四区域为：cn-north-4
        String regionId = "<YOUR REGION ID>";
        // 创建服务客户端
        ProjectManClient client = ProjectManClient.newBuilder()
            .withCredential(listProjectBugStaticsV4Auth)
            .withRegion(ProjectManRegion.valueOf(regionId))
            .build();
        // 构建请求头并设置项目ID
        ListProjectBugStaticsV4Request request = new ListProjectBugStaticsV4Request();
        request.withProjectId(projectId);
        try {
            // 获取对应局点下projectId项目的bug统计信息
            ListProjectBugStaticsV4Response response = client.listProjectBugStaticsV4(request);
            // 打印列表结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(String.valueOf(e.getRequestId()));
            logger.error(String.valueOf(e.getErrorCode()));
            logger.error(String.valueOf(e.getErrorMsg()));
        }
    }
}
```

### 返回结果示例
#### ListProjectBugStaticsV4
```
{
    bugStatistics: [class BugStatisticResponseV4 {
        criticalNum: 0
        defectIndex: 4.0
        module: 总计
        normalNum: 4
        seriousNum: 0
        tipNum: 0
        total: 4
    }, class BugStatisticResponseV4 {
        criticalNum: 0
        defectIndex: 2.0
        module: 会员管理
        normalNum: 2
        seriousNum: 0
        tipNum: 0
        total: 2
    }, class BugStatisticResponseV4 {
        criticalNum: 0
        defectIndex: 1.0
        module: 门店网络
        normalNum: 1
        seriousNum: 0
        tipNum: 0
        total: 1
    }, class BugStatisticResponseV4 {
        criticalNum: 0
        defectIndex: 1.0
        module: 订单管理
        normalNum: 1
        seriousNum: 0
        tipNum: 0
        total: 1
    }]
}
```
### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: |:----:| :------:  
 2024/08/07 | 1.0  | 文档首次发布