package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.BatchDeleteMembersV4Request;
import com.huaweicloud.sdk.projectman.v4.model.BatchDeleteMembersV4RequestBody;
import com.huaweicloud.sdk.projectman.v4.model.BatchDeleteMembersV4Response;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class BatchDeleteMembersV4 {

    private static final Logger logger = LoggerFactory.getLogger(BatchDeleteMembersV4.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential batchDeleteMembersV4Auth = new BasicCredentials().withAk(ak).withSk(sk);
        // Create a service client and set the corresponding region to ProjectManRegion.CN_NORTH_4.
        ProjectManClient client = ProjectManClient.newBuilder()
            .withCredential(batchDeleteMembersV4Auth)
            .withRegion(ProjectManRegion.CN_NORTH_4)
            .build();
        // Note: Use the value of the {projectId} variable in the link of the corresponding project page on CodeArts.
        // Example: https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/backlog
        String projectId = "<YOUR PROJECT ID>";
        // Build the request and set the project ID
        BatchDeleteMembersV4Request request = new BatchDeleteMembersV4Request();
        request.withProjectId(projectId);
        // Construct the request body and set parameters.
        BatchDeleteMembersV4RequestBody body = new BatchDeleteMembersV4RequestBody();
        // User ID set to be deleted.
        List<String> listBodyUserIds = new ArrayList<>();
        // User ID, which can be obtained on the member management page of project settings.
        listBodyUserIds.add("{userId}");
        body.withUserIds(listBodyUserIds);
        request.withBody(body);
        try {
            // Obtaining Results
            BatchDeleteMembersV4Response response = client.batchDeleteMembersV4(request);
            // Print Results
            logger.info(String.valueOf(response.getHttpStatusCode()));
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}