### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/ProjectMan/debug?api=ListIssuesSfV4) 中直接运行调试该接口。
2.在本样例中，您可以获取到CodeArts某个Scrum项目下Feature类型的工作项。
3.获取工作项，您可以根据获取的工作项，进行任务分配等操作。

### 前置条件
1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
2.已具备开发环境 ，支持Java JDK 1.8及其以上版本。
3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
4.已在CodeArts平台创建项目。
5.已在项目中创建了Feature类型的工作项。
6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-projectman”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-projectman</artifactId>
    <version>3.1.108</version>
</dependency>
```

### 代码示例
```java
public class ListIssuesSfV4 {

    private static final Logger logger = LoggerFactory.getLogger(ListIssuesSfV4.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listIssuesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listIssuesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(listIssuesAk)
                .withSk(listIssuesSk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        ProjectManClient client = ProjectManClient.newBuilder()
                .withCredential(auth)
                .withRegion(ProjectManRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        ListIssuesSfV4Request request = new ListIssuesSfV4Request();
        //注意,如下的 projectId 请使用CodeArts对应项目首页，点击某个项目链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/List
        String projectId = "<YOUR PROJECT ID>";
        request.withProjectId(projectId);
        // 偏移量 从0开始
        request.withOffset(0);
        // 每页数量 最小1,最大100
        request.withLimit(10);
        // 工作项类型，6表示Feature。2：任务/Task,3：缺陷/Bug,5：Epic,6：Feature,7：Story
        request.withTrackerId(ListIssuesSfV4Request.TrackerIdEnum.NUMBER_6);
        try {
            ListIssuesSfV4Response response = client.listIssuesSfV4(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ListIssuesSfV4 connection error",e);
        } catch (RequestTimeoutException e) {
            logger.error("ListIssuesSfV4 RequestTimeoutException",e);
        } catch (ServiceResponseException e) {
            logger.error("ListIssuesSfV4 ServiceResponseException",e);
        }
    }
}
```

### 返回结果示例
```
{
 "total": 2,
 "issues": [
  {
   "id": 4091,
   "subject": "促销管理",
   "release_version": null,
   "developer": null,
   "assigned_user": {
    "user_id": "f1ac4cd0c49***",
    "user_num_id": 2826,
    "nick_name": "Developer",
    "name": "demo_user_name"
   },
   "author": {
    "user_id": "f1ac4cd0c49***",
    "user_num_id": 2826,
    "nick_name": "Developer",
    "name": "demo_user_name"
   },
   "discover_version": null,
   "iteration": null,
   "story_point": null,
   "status": {
    "id": 1,
    "name": "新建"
   },
   "module": null,
   "priority": {
    "id": 2,
    "name": "中"
   },
   "severity": {
    "id": 12,
    "name": "一般"
   },
   "domain": null,
   "tracker": {
    "id": 6,
    "name": "Feature"
   },
   "updated_time": 1723012251000,
   "created_time": 1723012251000,
   "begin_time": null,
   "end_time": null,
   "closed_time": null,
   "order": 1,
   "parent_issue_id": 66858078,
   "root_issue_id": 66858078,
   "expected_work_hours": 0,
   "actual_work_hours": 0,
   "done_ratio": 0,
   "env_id": null,
   "custom_feilds": []
  },
  {
   "id": 66858076,
   "subject": "会员管理",
   "release_version": null,
   "developer": null,
   "assigned_user": {
    "user_id": "f1ac4cd0c49***",
    "user_num_id": 2826,
    "nick_name": "Developer",
    "name": "demo_user_name"
   },
   "author": {
    "user_id": "f1ac4cd0c49***",
    "user_num_id": 2826,
    "nick_name": "Developer",
    "name": "demo_user_name"
   },
   "discover_version": null,
   "iteration": null,
   "story_point": null,
   "status": {
    "id": 1,
    "name": "新建"
   },
   "module": null,
   "priority": {
    "id": 2,
    "name": "中"
   },
   "severity": {
    "id": 12,
    "name": "一般"
   },
   "domain": null,
   "tracker": {
    "id": 6,
    "name": "Feature"
   },
   "updated_time": 1723012251000,
   "created_time": 1723012251000,
   "begin_time": null,
   "end_time": null,
   "closed_time": null,
   "order": 1,
   "parent_issue_id": 66858078,
   "root_issue_id": 66858078,
   "expected_work_hours": 132,
   "actual_work_hours": 0,
   "done_ratio": 0,
   "env_id": null,
   "custom_feilds": []
  }
 ]
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-08-07 | 1.0 | 文档首次发布 |