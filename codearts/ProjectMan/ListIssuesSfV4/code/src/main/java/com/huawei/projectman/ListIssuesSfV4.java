package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.ListIssuesSfV4Response;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;

import com.huaweicloud.sdk.projectman.v4.model.ListIssuesSfV4Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListIssuesSfV4 {

    private static final Logger logger = LoggerFactory.getLogger(ListIssuesSfV4.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listIssuesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listIssuesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(listIssuesAk)
                .withSk(listIssuesSk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为ProjectManRegion.CN_NORTH_4
        ProjectManClient client = ProjectManClient.newBuilder()
                .withCredential(auth)
                .withRegion(ProjectManRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头，设置请求参数项目ID，偏移量，每页数量，工作项类型
        ListIssuesSfV4Request request = new ListIssuesSfV4Request();
        // 注意,如下的 projectId 请使用CodeArts对应项目首页，点击某个项目链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/List
        String projectId = "<YOUR PROJECT ID>";
        request.withProjectId(projectId);
        // 偏移量 从0开始
        request.withOffset(0);
        // 每页数量 最小1,最大100
        request.withLimit(10);
        // 工作项类型，6表示Feature。2：任务/Task,3：缺陷/Bug,5：Epic,6：Feature,7：Story
        request.withTrackerId(ListIssuesSfV4Request.TrackerIdEnum.NUMBER_6);
        try {
            // 获取ProjectManRegion.CN_NORTH_4下projectId的项目工作项
            ListIssuesSfV4Response response = client.listIssuesSfV4(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ListIssuesSfV4 connection error",e);
        } catch (RequestTimeoutException e) {
            logger.error("ListIssuesSfV4 RequestTimeoutException",e);
        } catch (ServiceResponseException e) {
            logger.error("ListIssuesSfV4 ServiceResponseException",e);
        }
    }
}