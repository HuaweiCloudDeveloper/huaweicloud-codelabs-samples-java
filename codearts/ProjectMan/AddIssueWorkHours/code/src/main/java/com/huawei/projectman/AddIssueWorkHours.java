package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.AddIssueWorkHoursRequest;
import com.huaweicloud.sdk.projectman.v4.model.AddIssueWorkHoursRequestBody;
import com.huaweicloud.sdk.projectman.v4.model.AddIssueWorkHoursResponse;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddIssueWorkHours {

    private static final Logger logger = LoggerFactory.getLogger(AddIssueWorkHours.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String addIssueWorkHoursAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String addIssueWorkHoursSk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential addIssueWorkHoursAuth = new BasicCredentials().withAk(addIssueWorkHoursAk)
            .withSk(addIssueWorkHoursSk);
        // 注意，如下的 projectId 请使用CodeArts对应项目首页链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/bug
        String projectId = "<YOUR PROJECT ID>";
        // 注意，如下的 regionId 请使用项目所在的局点，例如华北-北京四区域为：cn-north-4
        String regionId = "<YOUR REGION ID>";
        // 创建服务客户端
        ProjectManClient client = ProjectManClient.newBuilder()
            .withCredential(addIssueWorkHoursAuth)
            .withRegion(ProjectManRegion.valueOf(regionId))
            .build();
        // 构建请求并设置项目ID
        AddIssueWorkHoursRequest request = new AddIssueWorkHoursRequest();
        request.withProjectId(projectId);
        // 注意，工作项的编号即为issueId的值，此处请替换为实际的编号
        request.withIssueId(111111);
        AddIssueWorkHoursRequestBody body = new AddIssueWorkHoursRequestBody();
        // 工时总数
        body.withWorkHours((double) 16);
        // 工时开始日期，年-月-日，例如："2024-08-18"
        body.withStartDate("");
        // 工时结束日期，年-月-日，例如："2024-08-19"
        body.withDueDate("");
        request.withBody(body);
        try {
            // 获取结果
            AddIssueWorkHoursResponse response = client.addIssueWorkHours(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}