package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.ListProjectModulesRequest;
import com.huaweicloud.sdk.projectman.v4.model.ListProjectModulesResponse;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListProjectModules {

    private static final Logger logger = LoggerFactory.getLogger(ListProjectModules.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listProjectAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listProjectSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(listProjectAk)
                .withSk(listProjectSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为ProjectManRegion.CN_NORTH_4
        ProjectManClient client = ProjectManClient.newBuilder()
                .withCredential(auth)
                .withRegion(ProjectManRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头，设置请求参数项目ID
        ListProjectModulesRequest request = new ListProjectModulesRequest();
        String projectId = "<YOUR PROJECT ID>";
        request.withProjectId(projectId);
        try {
            // 获取ProjectManRegion.CN_NORTH_4下指定projectId的项目的模块列表
            ListProjectModulesResponse response = client.listProjectModules(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("ListProjectModules connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListProjectModules RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListProjectModules ServiceResponseException", e);
        }
    }
}