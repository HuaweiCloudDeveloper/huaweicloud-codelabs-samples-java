package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.*;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;
import com.huaweicloud.sdk.projectman.v4.*;
import com.huaweicloud.sdk.projectman.v4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateIterationV4 {
    private static final Logger logger = LoggerFactory.getLogger(CreateIterationV4.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 注意，如下的 projectId 请使用CodeArts对应项目首页链接中的32位ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/list
        String projectId = "<YOUR PROJECT ID>";
        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为ProjectManRegion.CN_NORTH_4
        ProjectManClient client = ProjectManClient.newBuilder()
                .withCredential(auth)
                .withRegion(ProjectManRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        CreateIterationV4Request request = new CreateIterationV4Request();
        request.withProjectId(projectId);
        CreateIterationRequestV4 body = new CreateIterationRequestV4();
        body.withName("IterationName");//迭代名称
        body.withBeginTime("BeginTime");//迭代开始时间,年-月-日 如2024-09-21
        body.withEndTime("EndTime");//迭代结束时间,年-月-日 如2024-09-30

        request.withBody(body);

        try {
            // 创建ProjectManRegion.CN_NORTH_4下projectId的迭代
            CreateIterationV4Response response = client.createIterationV4(request);
            // 打印返回结果
            logger.info(response.toString());
        }  catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }

    }
}
