package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.UpdateNickNameV4Request;
import com.huaweicloud.sdk.projectman.v4.model.UpdateNickNameV4Response;
import com.huaweicloud.sdk.projectman.v4.model.UpdateUserNickNameRequestV4;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateNickNameV4 {

    private static final Logger logger = LoggerFactory.getLogger(UpdateNickNameV4.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential updateNickNameV4Auth = new BasicCredentials().withAk(ak).withSk(sk);
        // 创建服务客户端并配置对应region为ProjectManRegion.CN_NORTH_4
        ProjectManClient client = ProjectManClient.newBuilder()
            .withCredential(updateNickNameV4Auth)
            .withRegion(ProjectManRegion.CN_NORTH_4)
            .build();
        // 构建请求
        UpdateNickNameV4Request request = new UpdateNickNameV4Request();
        UpdateUserNickNameRequestV4 body = new UpdateUserNickNameRequestV4();
        // 用户昵称
        body.withNickName("developer");
        request.withBody(body);
        try {
            // 获取结果
            UpdateNickNameV4Response response = client.updateNickNameV4(request);
            // 打印结果
            logger.info(String.valueOf(response.getHttpStatusCode()));
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}