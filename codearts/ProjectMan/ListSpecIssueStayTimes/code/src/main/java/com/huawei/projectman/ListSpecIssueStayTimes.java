package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.ListSpecIssueStayTimesRequest;
import com.huaweicloud.sdk.projectman.v4.model.ListSpecIssueStayTimesRequestBody;
import com.huaweicloud.sdk.projectman.v4.model.ListSpecIssueStayTimesResponse;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ListSpecIssueStayTimes {

    private static final Logger logger = LoggerFactory.getLogger(ListSpecIssueStayTimes.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential listSpecIssueStayTimesAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // 创建服务客户端并配置对应region为ProjectManRegion.CN_NORTH_4
        ProjectManClient client = ProjectManClient.newBuilder()
            .withCredential(listSpecIssueStayTimesAuth)
            .withRegion(ProjectManRegion.CN_NORTH_4)
            .build();
        // 注意，如下的 projectId 请使用CodeArts对应项目首页链接中的{projectId}变量的值
        // 例如：https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/backlog
        String projectId = "<YOUR PROJECT ID>";
        // 构建请求
        ListSpecIssueStayTimesRequest request = new ListSpecIssueStayTimesRequest();
        ListSpecIssueStayTimesRequestBody body = new ListSpecIssueStayTimesRequestBody();
        // 工作项id字符串列表
        List<String> issueIds = new ArrayList<>();
        // 注意，工作项的编号即为issueId的值，此处请替换为实际的编号
        issueIds.add("111111");
        issueIds.add("222222");
        issueIds.add("333333");
        body.withIssueIds(issueIds);
        // 项目ID
        body.withProjectId(projectId);
        request.withBody(body);
        try {
            // 获取结果
            ListSpecIssueStayTimesResponse response = client.listSpecIssueStayTimes(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}