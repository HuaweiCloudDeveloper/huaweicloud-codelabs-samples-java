package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.ListIssueRequestV4;
import com.huaweicloud.sdk.projectman.v4.model.ListIssuesV4Request;
import com.huaweicloud.sdk.projectman.v4.model.ListIssuesV4Response;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ListIssuesV4 {

    private static final Logger logger = LoggerFactory.getLogger(ListIssuesV4.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential listIssuesV4Auth = new BasicCredentials().withAk(ak).withSk(sk);
        // 创建服务客户端并配置对应region为ProjectManRegion.CN_NORTH_4
        ProjectManClient client = ProjectManClient.newBuilder()
            .withCredential(listIssuesV4Auth)
            .withRegion(ProjectManRegion.CN_NORTH_4)
            .build();
        // 注意，如下的 projectId 请使用CodeArts对应项目首页链接中的{projectId}变量的值
        // 例如：https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/backlog
        String projectId = "<YOUR PROJECT ID>";
        // 构建请求并设置项目ID
        ListIssuesV4Request request = new ListIssuesV4Request();
        request.withProjectId(projectId);
        // 构建请求体并设置查询参数
        ListIssueRequestV4 body = new ListIssueRequestV4();
        // 工作项类型，2：任务/Task，3：缺陷/Bug，5：Epic，6：Feature，7：Story
        List<Integer> trackerIds = new ArrayList<>();
        trackerIds.add(2);
        trackerIds.add(7);
        // 状态，新建 1, 进行中 2, 已解决 3, 测试中 4, 已关闭 5, 已拒绝 6
        List<Integer> statusIds = new ArrayList<>();
        statusIds.add(1);
        statusIds.add(2);
        // 重要程度，10 关键, 11 重要, 12 一般, 13 提示
        List<Integer> severityIds = new ArrayList<>();
        severityIds.add(12);
        severityIds.add(13);
        // 优先级，1 低, 2 中, 3 高
        List<Integer> priorityIds = new ArrayList<>();
        priorityIds.add(2);
        body.withTrackerIds(trackerIds);
        body.withStatusIds(statusIds);
        body.withSeverityIds(severityIds);
        body.withPriorityIds(priorityIds);
        request.withBody(body);
        try {
            // 获取结果
            ListIssuesV4Response response = client.listIssuesV4(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}