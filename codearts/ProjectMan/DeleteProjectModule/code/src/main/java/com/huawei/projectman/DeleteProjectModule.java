package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.DeleteProjectModuleRequest;
import com.huaweicloud.sdk.projectman.v4.model.DeleteProjectModuleResponse;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteProjectModule {
    private static final Logger logger = LoggerFactory.getLogger(DeleteProjectModule.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential deleteProjectModuleAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // 创建服务客户端并配置对应region为ProjectManRegion.CN_NORTH_4
        ProjectManClient client = ProjectManClient.newBuilder()
            .withCredential(deleteProjectModuleAuth)
            .withRegion(ProjectManRegion.CN_NORTH_4)
            .build();
        // 注意，如下的 projectId 请使用CodeArts对应项目首页链接中的ID
        // 例如：https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/list
        String projectId = "<YOUR PROJECT ID>";
        // 构建请求并设置项目ID
        DeleteProjectModuleRequest request = new DeleteProjectModuleRequest();
        request.withProjectId(projectId);
        // 模块ID，可通过“查询项目的模块列表”接口获取
        request.withModuleId(1234567);
        try {
            // 获取结果
            DeleteProjectModuleResponse response = client.deleteProjectModule(request);
            // 打印结果
            logger.info(String.valueOf(response.getHttpStatusCode()));
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}