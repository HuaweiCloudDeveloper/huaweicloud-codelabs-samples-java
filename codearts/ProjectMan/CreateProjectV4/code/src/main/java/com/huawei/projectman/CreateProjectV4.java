package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.CreateProjectV4Request;
import com.huaweicloud.sdk.projectman.v4.model.CreateProjectV4RequestBody;
import com.huaweicloud.sdk.projectman.v4.model.CreateProjectV4Response;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class CreateProjectV4 {

    private static final Logger logger = LoggerFactory.getLogger(CreateProjectV4.class.getName());
    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String createProjectAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String createProjectSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(createProjectAk)
                .withSk(createProjectSk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为ProjectManRegion.CN_NORTH_4
        ProjectManClient client = ProjectManClient.newBuilder()
                .withCredential(auth)
                .withRegion(ProjectManRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头，设置请求参数项目类型，项目名称
        CreateProjectV4Request request = new CreateProjectV4Request();
        CreateProjectV4RequestBody body = new CreateProjectV4RequestBody();
        // 项目类型 scrum, xboard(看板项目), basic, phoenix(凤凰项目)
        body.withProjectType("scrum");

        UUID uuid = UUID.randomUUID();
        // 项目名称
        String projectName = "demo" + uuid;
        body.withProjectName(projectName);
        request.withBody(body);
        try {
            // 创建ProjectManRegion.CN_NORTH_4下的项目
            CreateProjectV4Response response = client.createProjectV4(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error("CreateProjectV4 connection error",e);
        } catch (RequestTimeoutException e) {
            logger.error("CreateProjectV4 RequestTimeoutException",e);
        } catch (ServiceResponseException e) {
            logger.error("CreateProjectV4 ServiceResponseException",e);
        }
    }
}