package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.BatchUpdateChildNickNamesRequest;
import com.huaweicloud.sdk.projectman.v4.model.BatchUpdateChildNickNamesResponse;
import com.huaweicloud.sdk.projectman.v4.model.BatchUpdateChildUserNickNamesRequestBody;
import com.huaweicloud.sdk.projectman.v4.model.UpdateChildUserNickNameRequestBody;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class BatchUpdateChildNickNames {
    private static final Logger logger = LoggerFactory.getLogger(BatchUpdateChildNickNames.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential batchUpdateChildNickNamesAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // 创建服务客户端并配置对应region为ProjectManRegion.CN_NORTH_4
        ProjectManClient client = ProjectManClient.newBuilder()
            .withCredential(batchUpdateChildNickNamesAuth)
            .withRegion(ProjectManRegion.CN_NORTH_4)
            .build();
        // 构建请求
        BatchUpdateChildNickNamesRequest request = new BatchUpdateChildNickNamesRequest();
        BatchUpdateChildUserNickNamesRequestBody body = new BatchUpdateChildUserNickNamesRequestBody();
        // 修改的用户列表
        List<UpdateChildUserNickNameRequestBody> listBodyUsers = new ArrayList<>();
        // 用户昵称和用户id
        listBodyUsers.add(new UpdateChildUserNickNameRequestBody().withNickName("developer").withUserId("{userId}"));
        listBodyUsers.add(new UpdateChildUserNickNameRequestBody().withNickName("developer").withUserId("{userId}"));
        body.withUsers(listBodyUsers);
        request.withBody(body);
        try {
            // 获取结果
            BatchUpdateChildNickNamesResponse response = client.batchUpdateChildNickNames(request);
            // 打印结果
            logger.info(String.valueOf(response.getHttpStatusCode()));
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}