package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.CreateCustomfieldV1Req;
import com.huaweicloud.sdk.projectman.v4.model.CreateCustomfieldsRequest;
import com.huaweicloud.sdk.projectman.v4.model.CreateCustomfieldsResponse;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateCustomfields {

    private static final Logger logger = LoggerFactory.getLogger(CreateCustomfields.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential createCustomfieldsAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // 创建服务客户端并配置对应region为ProjectManRegion.CN_NORTH_4
        ProjectManClient client = ProjectManClient.newBuilder()
            .withCredential(createCustomfieldsAuth)
            .withRegion(ProjectManRegion.CN_NORTH_4)
            .build();
        // 注意，如下的 projectId 请使用CodeArts对应项目首页链接中的{projectId}变量的值
        // 例如：https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/backlog
        String projectId = "<YOUR PROJECT ID>";
        // 构建请求并设置项目ID
        CreateCustomfieldsRequest request = new CreateCustomfieldsRequest();
        request.withProjectId(projectId);
        CreateCustomfieldV1Req body = new CreateCustomfieldV1Req();
        // 工作项类型，Epic|Feature|Story|Task|Bug
        body.withScrumType("Story");
        // 自定义字段名称
        body.withName("自定义字段");
        // 自定义字段类型，textArea 多行文本，select 下拉框，radio 单选框，text 单行文本，checkbox 多选框，date 日期，time_date 日期（包含时分，字段显示格式为：2024/10/10 10:10），number 数字
        body.withType("select");
        // 自定义字段选项
        body.withOptions("自定义选项1,自定义选项2");
        // 自定义字段描述
        body.withMemo("自定义字段");
        request.withBody(body);
        try {
            // 获取结果
            CreateCustomfieldsResponse response = client.createCustomfields(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}