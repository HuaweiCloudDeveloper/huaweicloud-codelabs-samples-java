### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/ProjectMan/doc?api=ListProjectMembersV4).

2.In this example, you can get a list of member users for the specified project.

3.In the user information list, you can view information about all members in a specified project, including the tenant ID, user ID, member role, user nickname, and whether the member is a disabled account.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. 
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. 
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A project has been created on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-projectman. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-projectman</artifactId>
    <version>3.1.113</version>
</dependency>
```

### Code example
The following code shows how to get a list of member users for a specified project:
``` java
package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.ListProjectMembersV4Request;
import com.huaweicloud.sdk.projectman.v4.model.ListProjectMembersV4Response;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListProjectMembersV4 {

    private static final Logger logger = LoggerFactory.getLogger(ListProjectMembersV4.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential listProjectMembersV4Auth = new BasicCredentials().withAk(ak).withSk(sk);
        // Create a service client and set the corresponding region to ProjectManRegion.CN_NORTH_4.
        ProjectManClient client = ProjectManClient.newBuilder()
            .withCredential(listProjectMembersV4Auth)
            .withRegion(ProjectManRegion.CN_NORTH_4)
            .build();
        // Note: Use the value of the {projectId} variable in the link of the corresponding project page on CodeArts.
        // Example: https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/backlog
        String projectId = "<YOUR PROJECT ID>";
        // Build the request and set the project ID
        ListProjectMembersV4Request request = new ListProjectMembersV4Request();
        request.withProjectId(projectId);
        try {
            // Obtaining Results
            ListProjectMembersV4Response response = client.listProjectMembersV4(request);
            // Print Results
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
#### ListProjectMembersV4
```
{
    members: [class MemberListV4Members {
        domainId: 082e8exxxxxx
        domainName: hwxxxxxx
        userId: 082e8exxxxxx
        userName: hwxxxxxx
        userNumId: 2xxxxxx
        roleId: -1
        nickName: null
        roleName: Creator
        userType: null
        forbidden: 0
    }, class MemberListV4Members {
        domainId: 082e8exxxxxx
        domainName: hwxxxxxx
        userId: a63214xxxxxx
        userName: hhxxxxxx
        userNumId: 2xxxxxx
        roleId: 4
        nickName: null
        roleName: Developer
        userType: null
        forbidden: 0
    }]
    total: 2
}
```
### Change History

 Released On  |  Issue   | Description
 :----: |:---:| :------:  
 2024/10/24 | 1.0 | This document is released for the first time.