package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.UpdateProjectModuleRequest;
import com.huaweicloud.sdk.projectman.v4.model.UpdateProjectModuleRequestBody;
import com.huaweicloud.sdk.projectman.v4.model.UpdateProjectModuleResponse;
import com.huaweicloud.sdk.projectman.v4.model.UserRequest;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateProjectModule {
    private static final Logger logger = LoggerFactory.getLogger(UpdateProjectModule.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential updateProjectModuleAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // 创建服务客户端并配置对应region为ProjectManRegion.CN_NORTH_4
        ProjectManClient client = ProjectManClient.newBuilder()
            .withCredential(updateProjectModuleAuth)
            .withRegion(ProjectManRegion.CN_NORTH_4)
            .build();
        // 注意，如下的 projectId 请使用CodeArts对应项目首页链接中的ID
        // 例如：https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/list
        String projectId = "<YOUR PROJECT ID>";
        // 构建请求并设置项目ID
        UpdateProjectModuleRequest request = new UpdateProjectModuleRequest();
        request.withProjectId(projectId);
        // 模块ID，可通过“查询项目的模块列表”接口获取
        request.withModuleId(1234567);
        // 构建请求体并设置参数
        UpdateProjectModuleRequestBody body = new UpdateProjectModuleRequestBody();
        // 设置模块责任人
        UserRequest ownerBody = new UserRequest();
        // IAM用户ID
        ownerBody.withUserId("{userId}");
        body.withOwner(ownerBody);
        // 模块名称
        body.withModuleName("用户模块");
        // 模块描述
        body.withDescription("用户模块描述");
        request.withBody(body);
        try {
            // 获取结果
            UpdateProjectModuleResponse response = client.updateProjectModule(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}