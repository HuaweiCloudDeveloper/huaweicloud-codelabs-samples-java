### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/ProjectMan/doc?api=UpdateIssueV4) 中直接运行调试该接口。

2.在本样例中，您可以更新Scrum项目的指定工作项。

3.通过工作项的编号，可以更新该工作项的详细信息，包括工作项的实际工时、描述信息、标题、状态、父工作项等等内容。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-projectman”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-projectman</artifactId>
    <version>3.1.113</version>
</dependency>
```

### 代码示例
以下代码展示如何更新工作项：
``` java
package com.huawei.projectman;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.IssueRequestV4;
import com.huaweicloud.sdk.projectman.v4.model.UpdateIssueV4Request;
import com.huaweicloud.sdk.projectman.v4.model.UpdateIssueV4Response;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateIssueV4 {

    private static final Logger logger = LoggerFactory.getLogger(UpdateIssueV4.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential updateIssueV4Auth = new BasicCredentials().withAk(ak).withSk(sk);
        // 创建服务客户端并配置对应region为ProjectManRegion.CN_NORTH_4
        ProjectManClient client = ProjectManClient.newBuilder()
            .withCredential(updateIssueV4Auth)
            .withRegion(ProjectManRegion.CN_NORTH_4)
            .build();
        // 注意，如下的 projectId 请使用CodeArts对应项目首页链接中的{projectId}变量的值
        // 例如：https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/backlog
        String projectId = "<YOUR PROJECT ID>";
        // 构建请求并设置项目ID
        UpdateIssueV4Request request = new UpdateIssueV4Request();
        request.withProjectId(projectId);
        // 注意，工作项的编号即为issueId的值，此处请替换为实际的编号
        request.withIssueId(111111);
        // 设置请求体参数
        IssueRequestV4 body = new IssueRequestV4();
        // 状态id,新建 1,进行中 2,已解决 3,测试中 4,已关闭 5,已解决 6
        body.withStatusId(2);
        request.withBody(body);
        try {
            // 获取结果
            UpdateIssueV4Response response = client.updateIssueV4(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例
#### UpdateIssueV4
```
{
    actualWorkHours: 8.0
    assignedCcUser: null
    assignedUser: class IssueUser {
        userId: null
        userNumId: null
        id: xxxxxx
        name: xxxxxx
        nickName: xxxxxx
    }
    beginTime: 2024-09-18
    createdTime: 2024-09-18 09:48:36
    creator: class IssueUser {
        userId: null
        userNumId: null
        id: xxxxxx
        name: xxxxxx
        nickName: xxxxxx
    }
    customFields: []
    newCustomFields: null
    developer: null
    domain: null
    doneRatio: 0
    endTime: 2024-09-20
    expectedWorkHours: 0.0
    id: xxxxxx
    project: class IssueProjectResponseV4 {
        projectId: xxxxxx
        projectName: DevOps示例项目
        projectNumId: xxxxxx
    }
    iteration: null
    module: null
    name: 作为用户应该可以查看产品列表
    parentIssue: null
    priority: class IssueItemSfV4Priority {
        id: 2
        name: 中
    }
    order: class IssueResponseV4Order {
        id: 1
        name: 1
    }
    severity: class IssueItemSfV4Severity {
        id: 12
        name: 一般
    }
    status: class IssueItemSfV4Status {
        id: 2
        name: 进行中
    }
    releaseDev: 
    findReleaseDev: null
    env: null
    tracker: class CreateIssueResponseV4Tracker {
        id: 7
        name: Story
    }
    updatedTime: 2024-09-26 15:49:16
    closedTime: null
}
```
### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: |:----:| :------:  
 2024/09/26 | 1.0  | 文档首次发布