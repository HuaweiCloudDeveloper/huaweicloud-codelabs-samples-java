package com.huawei.servicestage;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.servicestage.v2.ServiceStageClient;
import com.huaweicloud.sdk.servicestage.v2.model.ListEnvironmentsRequest;
import com.huaweicloud.sdk.servicestage.v2.model.ListEnvironmentsResponse;
import com.huaweicloud.sdk.servicestage.v2.region.ServiceStageRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListEnvironments {

    private static final Logger logger = LoggerFactory.getLogger(ListEnvironments.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listEnvironmentsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listEnvironmentsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listEnvironmentsAk)
                .withSk(listEnvironmentsSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to ServiceStageRegion.CN_NORTH_4.
        ServiceStageClient client = ServiceStageClient.newBuilder()
                .withCredential(auth)
                .withRegion(ServiceStageRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();

        // Construct a request.
        ListEnvironmentsRequest request = new ListEnvironmentsRequest();
        try {
            ListEnvironmentsResponse response = client.listEnvironments(request);
            logger.info("ListEnvironments: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListEnvironments connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListEnvironments RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListEnvironments ServiceResponseException", e);
        }
    }
}