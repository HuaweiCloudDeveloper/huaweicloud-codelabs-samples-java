package com.huawei.servicestage;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.servicestage.v2.region.ServiceStageRegion;
import com.huaweicloud.sdk.servicestage.v2.ServiceStageClient;
import com.huaweicloud.sdk.servicestage.v2.model.InstanceCreate;
import com.huaweicloud.sdk.servicestage.v2.model.CreateInstanceRequest;
import com.huaweicloud.sdk.servicestage.v2.model.CreateInstanceResponse;
import com.huaweicloud.sdk.servicestage.v2.model.ReferResourceCreate;
import com.huaweicloud.sdk.servicestage.v2.model.ResourceType;
import com.huaweicloud.sdk.servicestage.v2.model.FlavorId;
import com.huaweicloud.sdk.servicestage.v2.model.SourceOrArtifact;

public class CreateInstance {

    /* 以下部分请替换成用户实际参数 */
    // 客户端相关参数
    static String projectId = "<YOUR PROJECT ID>"; // 华为云账号项目ID
    static String userRegion = "<YOUR REGION>";    // 服务所在区域，eg: cn-north-4

    // 创建组件实例相关参数
    static String applicationId = "<YOUR APPLICATION ID>";       // 应用ID
    static String componentId = "<YOUR COMPONENT ID>";           // 组件ID
    static String environmentId = "<YOUR ENVIRONMENT ID>";       // 指定环境的ID
    static String componentName = "<YOUR COMPONENT NAME>";       // 组件名称
    static String instanceName = "<YOUR INSTANCE NAME>";         // 实例名称，不可重复
    static String version = "<YOUR VERSION>";                    // 应用组件版本号
    static String description = "<YOUR INSTANCE DESCRIPTION>";   // 描述信息
    static String storage = "obs";                               // 存储方式，支持软件仓库swr和对象存储obs
    static String artifactsType = "package";                     // 类别，支持package
    static String url = "obs://ss-test-jdk/weather-1.0.0.jar";   // 软件包/源码地址
    static String obsAuth = "iam";                               // 认证方式，支持iam，none，默认是iam
    static String flavorId = "CUSTOM-10G:250m-250m:0.5Gi-0.5Gi"; // 资源规格
    static String referResourceId = "<YOUR REFERRESOURCE ID>";   // 部署资源ID
    static String referResourceType = "ecs";                     // 部署资源类型，可选cce、ecs
    static Integer replica = 2;                                  // 实例数目

    private static final Logger logger = LoggerFactory.getLogger(CreateInstance.class);

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK"); // 华为云账号Access Key
        String sk = System.getenv("HUAWEICLOUD_SDK_SK"); // 华为云账号Secret Access Key

        // 创建认证
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // 创建ServiceStageClient实例并初始化
        ServiceStageClient client = ServiceStageClient.newBuilder()
            .withCredential(auth)
            .withRegion(ServiceStageRegion.valueOf(userRegion))
            .build();

        // 创建request实例并初始化
        CreateInstanceRequest request = new CreateInstanceRequest()
            .withApplicationId(applicationId)
            .withComponentId(componentId)
            .withBody(getRequestBody());

        // 创建组件实例
        try {
            CreateInstanceResponse response = client.createInstance(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(e.toString());
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("RequestId: " + e.getRequestId());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static InstanceCreate getRequestBody() {
        // 将所需参数集合到Body中
        Map<String, Object> artifactsBody = new HashMap<>();
        List<ReferResourceCreate> referResourcesBody = new ArrayList<>();
        SourceOrArtifact artifactsArtifacts = new SourceOrArtifact();
        artifactsArtifacts.withStorage(SourceOrArtifact.StorageEnum.fromValue(storage))
            .withType(SourceOrArtifact.TypeEnum.fromValue(artifactsType))
            .withUrl(url)
            .withAuth(obsAuth);
        artifactsBody.put(componentName, artifactsArtifacts);
        referResourcesBody.add(
            new ReferResourceCreate()
                .withId(referResourceId)
                .withType(ResourceType.fromValue(referResourceType))
        );
        InstanceCreate body = new InstanceCreate()
            .withVersion(version)
            .withReplica(replica)
            .withName(instanceName)
            .withDescription(description)
            .withEnvironmentId(environmentId)
            .withArtifacts(artifactsBody)
            .withReferResources(referResourcesBody)
            .withFlavorId(FlavorId.fromValue(flavorId));

        return body;
    }
}
