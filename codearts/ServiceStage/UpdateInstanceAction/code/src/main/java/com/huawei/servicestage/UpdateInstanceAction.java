package com.huawei.servicestage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.servicestage.v2.model.InstanceModify;
import com.huaweicloud.sdk.servicestage.v2.region.ServiceStageRegion;
import com.huaweicloud.sdk.servicestage.v2.ServiceStageClient;
import com.huaweicloud.sdk.servicestage.v2.model.UpdateInstanceActionRequest;
import com.huaweicloud.sdk.servicestage.v2.model.UpdateInstanceActionResponse;
import com.huaweicloud.sdk.servicestage.v2.model.InstanceAction;
import com.huaweicloud.sdk.servicestage.v2.model.InstanceActionParameters;
import com.huaweicloud.sdk.servicestage.v2.model.InstanceActionType;

public class UpdateInstanceAction {

    /* 以下部分请替换成用户实际参数 */
    // 客户端相关参数
    static String projectId = "<YOUR PROJECT ID>"; // 华为云账号项目ID
    static String userRegion = "<YOUR REGION>";    // 服务所在区域，eg: cn-north-4

    // 对组件实例进行操作相关参数
    static String applicationId = "<YOUR APPLICATION ID>"; // 应用ID
    static String componentId = "<YOUR COMPONENT ID>";     // 组件ID
    static String instanceId = "<YOUR INSTANCE ID>";       // 实例ID
    static String actionType = "scale";                    // 对实例的操作，支持start、stop、restart、scale、rollback
    static Integer replica = 1;                            // 实例数目, 仅对实例进行scale操作时需要

    private static final Logger logger = LoggerFactory.getLogger(UpdateInstanceAction.class);

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK"); // 华为云账号Access Key
        String sk = System.getenv("HUAWEICLOUD_SDK_SK"); // 华为云账号Secret Access Key

        // 创建认证
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // 创建ServiceStageClient实例并初始化
        ServiceStageClient client = ServiceStageClient.newBuilder()
            .withCredential(auth)
            .withRegion(ServiceStageRegion.valueOf(userRegion))
            .build();

        // 创建request实例并初始化
        UpdateInstanceActionRequest request = new UpdateInstanceActionRequest()
            .withApplicationId(applicationId)
            .withComponentId(componentId)
            .withInstanceId(instanceId);
        request.withBody(getRequestBody());

        // 对实例进行操作，以伸缩实例数目为例
        try {
            UpdateInstanceActionResponse response = client.updateInstanceAction(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(e.toString());
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("RequestId: " + e.getRequestId());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    private static InstanceAction getRequestBody() {
        // 将所需参数集合到Body中
        InstanceActionParameters parametersBody = new InstanceActionParameters()
            .withReplica(replica);
        InstanceAction body = new InstanceAction()
            .withParameters(parametersBody)
            .withAction(InstanceActionType.fromValue(actionType));
        return body;
    }
}
