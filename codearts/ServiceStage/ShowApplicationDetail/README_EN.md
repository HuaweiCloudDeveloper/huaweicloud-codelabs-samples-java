### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/ServiceStage/doc?version=v2&api=ShowApplicationDetail).

2.In this example, you can obtain application details based on the application ID.

3.Query details about a specified application on ServiceStage. The detailed information includes the application name, application description, application creation time, and application update time.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.The current account has the permission to use ServiceStage.

6.An application has been created on ServiceStage.

7.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-servicestage. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-servicestage</artifactId>
    <version>3.1.118</version>
</dependency>

```

### Code example

``` java
package com.huawei.servicestage;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.servicestage.v2.model.ShowApplicationDetailRequest;
import com.huaweicloud.sdk.servicestage.v2.model.ShowApplicationDetailResponse;
import com.huaweicloud.sdk.servicestage.v2.region.ServiceStageRegion;
import com.huaweicloud.sdk.servicestage.v2.ServiceStageClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowApplicationDetail {

    private static final Logger logger = LoggerFactory.getLogger(ShowApplicationDetail.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String showApplicationDetailAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showApplicationDetailSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(showApplicationDetailAk)
                .withSk(showApplicationDetailSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to ServiceStageRegion.CN_NORTH_4.
        ServiceStageClient client = ServiceStageClient.newBuilder()
                .withCredential(auth)
                .withRegion(ServiceStageRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();

        // Construct a request and set the request parameter application ID.
        ShowApplicationDetailRequest request = new ShowApplicationDetailRequest();
        // Application ID. (Returned by the ListApplications interface, where the id attribute is the application ID)
        String applicationId = "<YOUR APPLICATIONID>";
        request.withApplicationId(applicationId);
        try {
            ShowApplicationDetailResponse response = client.showApplicationDetail(request);
            logger.info("ShowApplicationDetail: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ShowApplicationDetail connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ShowApplicationDetail RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ShowApplicationDetail ServiceResponseException", e);
        }
    }
}
```

### Example of the returned result

#### ShowApplicationDetail

```
{
 "id": "4dfe****",
 "name": "servicecomb",
 "description": "",
 "project_id": "16d5****",
 "enterprise_project_id": "ee9f****",
 "creator": "demouser",
 "create_time": 1723172059043,
 "update_time": 1723172059043,
 "unified_model": null,
 "labels": [],
 "component_count": 15
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/10/25 | 1.0 | This document is released for the first time.