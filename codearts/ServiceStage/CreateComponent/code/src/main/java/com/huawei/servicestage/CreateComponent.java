package com.huawei.servicestage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.servicestage.v2.ServiceStageClient;
import com.huaweicloud.sdk.servicestage.v2.model.ComponentCategory;
import com.huaweicloud.sdk.servicestage.v2.model.ComponentCreate;
import com.huaweicloud.sdk.servicestage.v2.model.CreateComponentRequest;
import com.huaweicloud.sdk.servicestage.v2.model.CreateComponentResponse;
import com.huaweicloud.sdk.servicestage.v2.model.ObsProperties;
import com.huaweicloud.sdk.servicestage.v2.model.RuntimeType;
import com.huaweicloud.sdk.servicestage.v2.model.SourceKind;
import com.huaweicloud.sdk.servicestage.v2.model.SourceObject;
import com.huaweicloud.sdk.servicestage.v2.model.SourceOrArtifact;
import com.huaweicloud.sdk.servicestage.v2.region.ServiceStageRegion;

public class CreateComponent {

    /* 以下部分请替换成用户实际参数 */
    // 客户端相关参数
    static String projectId = "<YOUR PROJECT ID>"; // 华为云账号项目ID
    static String userRegion = "<YOUR REGION>";    // 服务所在区域，eg: cn-north-4

    // 创建组件相关参数
    static String applicationId = "<YOUR APPLICATION ID>";           // 应用ID
    static String componentName = "<YOUR COMPONENT NAME>";           // 组件名称
    static String description = "<YOUR COMPONENT DESCRIPTION>";      // 描述信息
    static String runtimeType = "Java8";                             // 运行时类型，包括Tomcat8、Java8、Php7、Nodejs8、Docker、Python3
    static String category = "Webapp";                               // 应用组件类型，包括Webapp、MicroService、Common
    static String sourceKind = "artifact";                           // 组件来源，可选择源码code或artifact软件包部署
    static String storage = "obs";                                   // 存储方式，支持软件仓库swr和对象存储obs
    static String artifactsType = "package";                         // 类别，支持package
    static String url = "obs://ss-test-jdk/weather-1.0.0.jar";       // 软件包/源码地址
    static String obsAuth = "iam";                                   // 认证方式，支持iam，none，默认是iam
    static String endpoint = "obs.cn-southwest-2.myhuaweicloud.com"; // obs的终端地址
    static String bucketName = "ss-test-jdk";                        // 软件包在obs的桶名
    static String obsKey = "weather-1.0.0.jar";                      // obs桶中的对象，一般是软件包名，有文件夹的话要加上文件夹的路径，eg: demo/test.jar

    private static final Logger logger = LoggerFactory.getLogger(CreateComponent.class);

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK"); // 华为云账号Access Key
        String sk = System.getenv("HUAWEICLOUD_SDK_SK"); // 华为云账号Secret Access Key

        // 创建认证
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // 创建ServiceStageClient实例并初始化
        ServiceStageClient client = ServiceStageClient.newBuilder()
            .withCredential(auth)
            .withRegion(ServiceStageRegion.valueOf(userRegion))
            .build();

        // 将所需参数集合到Body中
        ObsProperties propertiesSpec = new ObsProperties()
            .withEndpoint(endpoint)
            .withBucket(bucketName)
            .withKey(obsKey);
        SourceOrArtifact specSource = new SourceOrArtifact()
            .withStorage(SourceOrArtifact.StorageEnum.fromValue(storage))
            .withType(SourceOrArtifact.TypeEnum.fromValue(artifactsType))
            .withUrl(url)
            .withAuth(obsAuth)
            .withProperties(propertiesSpec);
        SourceObject sourceBody = new SourceObject()
            .withKind(SourceKind.fromValue(sourceKind))
            .withSpec(specSource);
        ComponentCreate body = new ComponentCreate()
            .withName(componentName)
            .withRuntime(RuntimeType.fromValue(runtimeType))
            .withCategory(ComponentCategory.fromValue(category))
            .withDescription(description)
            .withSource(sourceBody);

        // 创建request实例并初始化
        CreateComponentRequest request = new CreateComponentRequest()
            .withApplicationId(applicationId)
            .withBody(body);

        // 在指定应用中创建组件
        try {
            CreateComponentResponse response = client.createComponent(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(e.toString());
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("RequestId: " + e.getRequestId());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }
}
