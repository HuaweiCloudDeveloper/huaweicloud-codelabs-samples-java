## 1、功能介绍
**什么是 ServiceStage ？**

[ServiceStage](https://support.huaweicloud.com/productdesc-servicestage/servicestage_01_0007.html) 是面向企业的应用管理与运维平台，提供应用开发、构建、发布、监控及运维等一站式解决方案。提供Java、Go、PHP、Node.js、Docker、Tomcat等运行环境，支持微服务应用、Web应用以及通用应用的托管与治理，让企业应用上云更简单。

- ServiceStage 实现了与源码仓库的对接（如CodeArts、Gitee、GitHub、GitLab、Bitbucket），绑定源码仓库后，可以直接从源码仓库拉取源码进行构建。

- ServiceStage 集成了部署源管理功能，可以将构建完成的软件包（或者镜像包）归档对应的仓库和组织。

- ServiceStage 集成了相关的基础资源（如VPC、CCE、ECS、EIP、ELB），在部署应用时可以直接使用已有或者新建所需的基础资源。

- ServiceStage 集成了微服务引擎，进入 ServiceStage 控制台可以进行微服务治理相关的操作。

- ServiceStage 集成了应用运维管理及应用性能管理服务，可以进行应用运维及性能监控相关的操作。

- ServiceStage 集成了存储、数据库、缓存等服务，通过简单配置即可实现数据持久化存储。

**您将学到什么？**

如何通过 ServiceStage 的 SDK 实现组件管理，创建一个新的组件。

## 2、前置条件
- 已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

- 已具备开发环境 ，支持 Java JDK 1.8 及其以上版本。

- 已获取华为云账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

- 已获取对应的服务所在地区（region），详情请查看 [地区和终端节点](https://developer.huaweicloud.com/endpoint)。

- 已创建应用，详情请查看“创建应用”示例。

## 3、SDK的获取与安装
[ServiceStage Java SDK](https://console.huaweicloud.com/apiexplorer/#/sdkcenter/ServiceStage?lang=Java) 支持 Java JDK 1.8 及其以上版本。
推荐您通过 Maven 安装依赖的方式使用 Java SDK：

首先您需要在您的操作系统中下载并安装 Maven，安装完成后您只需在 Maven 项目的 pom.xml 文件加入相应的依赖项即可。

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-servicestage</artifactId>
    <version>3.1.64</version>
</dependency>
```

## 4、示例代码
以下代码展示如何在指定应用中创建一个新的组件，调用前请根据实际情况替换客户端相关参数和创建组件相关参数。

```java
public class CreateComponent {

    /* 以下部分请替换成用户实际参数 */
    // 客户端相关参数
    static String projectId = "<YOUR PROJECT ID>"; // 华为云账号项目ID
    static String userRegion = "<YOUR REGION>";    // 服务所在区域，eg: cn-north-4

    // 创建组件相关参数
    static String applicationId = "<YOUR APPLICATION ID>";           // 应用ID
    static String componentName = "<YOUR COMPONENT NAME>";           // 组件名称
    static String description = "<YOUR COMPONENT DESCRIPTION>";      // 描述信息
    static String runtimeType = "Java8";                             // 运行时类型，包括Tomcat8、Java8、Php7、Nodejs8、Docker、Python3
    static String category = "Webapp";                               // 应用组件类型，包括Webapp、MicroService、Common
    static String sourceKind = "artifact";                           // 组件来源，可选择源码code或artifact软件包部署
    static String storage = "obs";                                   // 存储方式，支持软件仓库swr和对象存储obs
    static String artifactsType = "package";                         // 类别，支持package
    static String url = "obs://ss-test-jdk/weather-1.0.0.jar";       // 软件包/源码地址
    static String obsAuth = "iam";                                   // 认证方式，支持iam，none，默认是iam
    static String endpoint = "obs.cn-southwest-2.myhuaweicloud.com"; // obs的终端地址
    static String bucketName = "ss-test-jdk";                        // 软件包在obs的桶名
    static String obsKey = "weather-1.0.0.jar";                      // obs桶中的对象，一般是软件包名，有文件夹的话要加上文件夹的路径，eg: demo/test.jar

    private static final Logger logger = LoggerFactory.getLogger(CreateComponent.class);

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK"); // 华为云账号Access Key
        String sk = System.getenv("HUAWEICLOUD_SDK_SK"); // 华为云账号Secret Access Key

        // 创建认证
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // 创建ServiceStageClient实例并初始化
        ServiceStageClient client = ServiceStageClient.newBuilder()
            .withCredential(auth)
            .withRegion(ServiceStageRegion.valueOf(userRegion))
            .build();

        // 将所需参数集合到Body中
        ObsProperties propertiesSpec = new ObsProperties()
            .withEndpoint(endpoint)
            .withBucket(bucketName)
            .withKey(obsKey);
        SourceOrArtifact specSource = new SourceOrArtifact()
            .withStorage(SourceOrArtifact.StorageEnum.fromValue(storage))
            .withType(SourceOrArtifact.TypeEnum.fromValue(artifactsType))
            .withUrl(url)
            .withAuth(obsAuth)
            .withProperties(propertiesSpec);
        SourceObject sourceBody = new SourceObject()
            .withKind(SourceKind.fromValue(sourceKind))
            .withSpec(specSource);
        ComponentCreate body = new ComponentCreate()
            .withName(componentName)
            .withRuntime(RuntimeType.fromValue(runtimeType))
            .withCategory(ComponentCategory.fromValue(category))
            .withDescription(description)
            .withSource(sourceBody);
        
        // 创建request实例并初始化
        CreateComponentRequest request = new CreateComponentRequest()
            .withApplicationId(appId)
            .withBody(body);

        // 在指定应用中创建组件
        try {
            CreateComponentResponse response = client.createComponent(request);
            logger.info(response.toString());
        } catch (ConnectionException e) {
            logger.error(e.toString());
        } catch (RequestTimeoutException e) {
            logger.error(e.toString());
        } catch (ServiceResponseException e) {
            logger.error(e.toString());
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("RequestId: " + e.getRequestId());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }
}
```

## 5. 运行结果
以下代码为响应示例，在指定应用中创建了一个名为demo的组件，采用artifact软件包部署，软件包存储方式为obs。
```
class CreateComponentResponse {
    id: 91bed26a-9e59-40c4-9094-9d0e76341152
    name: demo
    status: 0
    runtime: Java8
    category: Webapp
    subCategory: null
    description: null
    projectId: 04aee398dc00257b2fc4c010ed1008c8
    applicationId: ff0970dc-f2ae-4628-9fc6-322cab42539a
    source: class SourceObject {
        kind: artifact
        spec: class SourceOrArtifact {
            storage: obs
            type: package
            url: obs://ss-test-jdk/weather-1.0.0
            webUrl: null
            auth: iam
            properties: class ObsProperties {
                endpoint: obs.cn-southwest-2.myhuaweicloud.com
                bucket: ss-test-jdk
                key: weather-1.0.0.jar
            }
            repoType: null
            repoUrl: null
            repoRef: null
            repoAuth: null
        }
    }
    build: null
    pipelineIds: null
    createTime: 1699259927400
    creator: anonymous
    updateTime: 1699259927400
}
```


## 6. 参考

更多信息请参考：[应用管理与运维平台 ServiceStage](https://support.huaweicloud.com/servicestage/index.html)

## 7. 修订记录

|   发布日期    | 文档版本 | 修订说明 |
|:---------:| :------: | :----------: |
| 2023-11-7 | 1.0 | 文档首次发布 |