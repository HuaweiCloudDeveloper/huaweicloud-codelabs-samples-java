package com.huawei.servicestage;

// 用户身份认证
import com.huaweicloud.sdk.core.auth.BasicCredentials;
// 请求异常类
import com.huaweicloud.sdk.core.exception.ClientRequestException;
// 导入相应产品的 {Service}Client
import com.huaweicloud.sdk.servicestage.v2.ServiceStageClient;
// 导入相应的创建应用类
import com.huaweicloud.sdk.servicestage.v2.model.ApplicationCreate;
// 导入待请求接口的 request 和 response 类
import com.huaweicloud.sdk.servicestage.v2.model.CreateApplicationRequest;
import com.huaweicloud.sdk.servicestage.v2.model.CreateApplicationResponse;
import com.huaweicloud.sdk.servicestage.v2.region.ServiceStageRegion;
// 日志打印
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateApplication {

    /* 以下部分请替换成用户实际参数 */
    // 客户端相关参数
    static String projectId = "<YOUR PROJECT ID>"; // 华为云账号项目ID
    static String userRegion = "<YOUR REGION>";    // 服务所在区域，eg: cn-north-4

    // 创建应用相关参数
    static String applicationName = "<YOUR APPLICATION NAME>"; // 组件名称
    static String description = "<YOUR INSTANCE DESCRIPTION>"; // 描述信息

    private static final Logger logger = LoggerFactory.getLogger(CreateApplication.class);

    // 测试是否能创建应用
    public static void createApplication(ServiceStageClient client, ApplicationCreate applicationCreate) {
        try {
            // 实例化CreateApplicationResponse请求对象，调用createApplication接口
            CreateApplicationResponse createApplicationResponse = client
                .createApplication(new CreateApplicationRequest()
                .withBody(applicationCreate));
            // 输出json格式的字符串响应
            logger.info(createApplicationResponse.toString());
        } catch (ClientRequestException e) {
            logger.error("HttpStatusCode: " + e.getHttpStatusCode());
            logger.error("RequestId: " + e.getRequestId());
            logger.error("ErrorCode: " + e.getErrorCode());
            logger.error("ErrorMsg: " + e.getErrorMsg());
        }
    }

    public static void main(String[] args) {
        // 认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK"); // 华为云账号Access Key
        String sk = System.getenv("HUAWEICLOUD_SDK_SK"); // 华为云账号Secret Access Key

        // 创建认证
        BasicCredentials auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // 创建ServiceStageClient实例并初始化
        ServiceStageClient serviceStageClient = ServiceStageClient.newBuilder()
            .withCredential(auth)
            .withRegion(ServiceStageRegion.valueOf(userRegion))
            .build();

        // 创建一个新的应用
        ApplicationCreate applicationCreate = new ApplicationCreate()
            .withDescription(description)
            .withName(applicationName);

        createApplication(serviceStageClient, applicationCreate);
    }
}