package com.huawei.codeartsartifact;

import com.huaweicloud.sdk.codeartsartifact.v2.CodeArtsArtifactClient;
import com.huaweicloud.sdk.codeartsartifact.v2.model.ShowRepositoryRequest;
import com.huaweicloud.sdk.codeartsartifact.v2.model.ShowRepositoryResponse;
import com.huaweicloud.sdk.codeartsartifact.v2.region.CodeArtsArtifactRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowRepository {

    private static final Logger logger = LoggerFactory.getLogger(ShowRepository.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential showRepositoryAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // Note that the following region ID must be the site where the project is located.
        String regionId = "<YOUR REGION ID>";
        // Creating a Service Client
        CodeArtsArtifactClient client = CodeArtsArtifactClient.newBuilder()
            .withCredential(showRepositoryAuth)
            .withRegion(CodeArtsArtifactRegion.valueOf(regionId))
            .build();
        // Construction request
        ShowRepositoryRequest request = new ShowRepositoryRequest();
        // Account ID. To obtain the ID, log in to the HUAWEI CLOUD management console, click the username, click My Credential from the drop-down list, and view the account ID on the API Credential page.
        request.withTenantId("{tenantId}");
        // Project ID, which corresponds to the unique project ID of the requirement management CodeArts Req project. It is the value of the projectId variable in the url https://{host}/cloudartifact/project/{projectId}/repository address bar on the home page of the private dependency library.
        request.withProjectId("{projectId}");
        // Repository ID, which is obtained from the repository address on the artifact repository page. The value is the value of repoId in https://devrepo.devcloud.cn-north-4.huaweicloud.com/artgalaxy/{repoId}/.
        request.withRepoId("{repoId}");
        request.withRegion(regionId);
        try {
            // Obtaining Results
            ShowRepositoryResponse response = client.showRepository(request);
            // Print Results
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
