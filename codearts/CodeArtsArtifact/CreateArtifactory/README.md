### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsArtifact/doc?api=CreateArtifactory) 中直接运行调试该接口。

2.在本样例中，您可以创建非maven类型的仓库。

3.创建时可以指定仓库归属的项目，创建仓库成功后，获取的返回信息中包含了仓库的id和地址等信息，便于后续对仓库的使用。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartsartifact”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartsartifact</artifactId>
    <version>3.1.112</version>
</dependency>
```

### 代码示例
以下代码展示如何创建非maven类型的仓库：
``` java
package com.huawei.codeartsartifact;

import com.huaweicloud.sdk.codeartsartifact.v2.CodeArtsArtifactClient;
import com.huaweicloud.sdk.codeartsartifact.v2.model.CreateArtifactoryRequest;
import com.huaweicloud.sdk.codeartsartifact.v2.model.CreateArtifactoryResponse;
import com.huaweicloud.sdk.codeartsartifact.v2.model.CreateNotMavenRepoDO;
import com.huaweicloud.sdk.codeartsartifact.v2.region.CodeArtsArtifactRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateArtifactory {

    private static final Logger logger = LoggerFactory.getLogger(CreateArtifactory.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential createArtifactoryAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // 注意，如下的 regionId 请使用项目所在的局点，例如华北-北京四区域为：cn-north-4
        String regionId = "<YOUR REGION ID>";
        // 创建服务客户端
        CodeArtsArtifactClient client = CodeArtsArtifactClient.newBuilder()
            .withCredential(createArtifactoryAuth)
            .withRegion(CodeArtsArtifactRegion.valueOf(regionId))
            .build();
        // 构建请求
        CreateArtifactoryRequest request = new CreateArtifactoryRequest();
        CreateNotMavenRepoDO body = new CreateNotMavenRepoDO();
        // 项目ID，对应"需求管理CodeArts Req"项目唯一标识，私有依赖库首页地址栏url https://{host}/cloudartifact/project/{projectId}/repository中projectId变量的值。
        body.withProjectId("{projectId}");
        // 路径白名单，**/*表示允许所有
        body.withIncludesPattern("**/*");
        // 仓库名称
        body.withRepositoryName("npm1");
        // 仓库类型
        body.withType("hosted");
        // 仓库格式
        body.withFormat("npm");
        request.withBody(body);
        try {
            // 获取结果
            CreateArtifactoryResponse response = client.createArtifactory(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例
#### CreateArtifactory
```
{
    status: success
    traceId: xxxxxx
    result: {artifactory={status=null, domainId=xxxxxx, region=cn-north-4, createdTime=null, modifiedTime=null, createdUserId=xxxxxx, createdUserName=xxxxxx, modifiedUserId=xxxxxx, modifiedUserName=xxxxxx, name=cn-north-4_xxxxxx_npm_6, disable=false, format=npm, type=hosted, policy=null, tabId=null, repositoryName=npm2, displayName=null, description=null, snapshot=null, release=null, npm=null, snapshotStatus=null, releaseStatus=null, projectId=xxxxxx, includesPattern=xxxxxx, repositoryIds=null, uri=null, deploymentPolicy=null, repositories=null, parentRepoName=null, userName=null, password=null, remoteUrl=null, pyPIRegistryUrl=null, defaultDeployRepository=null, remoteType=null, proxy=null, allowAnonymous=null, autoCleanSnapshot=null, snapshotAliveDays=null, maxUniqueSnapshots=null, shareRight=null, nexuRepo=false, migrateFlag=4, obsSpeedBucket=null, associateProject=null, url=https://devrepo.devcloud.cn-north-4.huaweicloud.com/artgalaxy/api/npm/cn-north-4_xxxxxx_npm_6/, packageType=npm}}
}
```
### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: |:----:| :------:  
 2024/09/10 | 1.0  | 文档首次发布