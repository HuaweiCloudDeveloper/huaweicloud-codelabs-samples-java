### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsArtifact/doc?api=ShowProjectReleaseFiles) 中直接运行调试该接口。

2.在本样例中，您可以获取项目下文件版本信息列表。

3.通过项目ID和文件名称，获取指定项目下满足条件的文件信息列表，列表内包含了文件的版本、文件的路径、文件的下载链接。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartsartifact”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartsartifact</artifactId>
    <version>3.1.118</version>
</dependency>
```

### 代码示例
以下代码展示如何获取项目下文件版本信息列表：
``` java
package com.huawei.codeartsartifact;

import com.huaweicloud.sdk.codeartsartifact.v2.CodeArtsArtifactClient;
import com.huaweicloud.sdk.codeartsartifact.v2.model.ShowProjectReleaseFilesRequest;
import com.huaweicloud.sdk.codeartsartifact.v2.model.ShowProjectReleaseFilesResponse;
import com.huaweicloud.sdk.codeartsartifact.v2.region.CodeArtsArtifactRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowProjectReleaseFiles {

    private static final Logger logger = LoggerFactory.getLogger(ShowProjectReleaseFiles.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential showProjectReleaseFilesAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // 创建服务客户端并配置对应region为CodeArtsArtifactRegion.CN_NORTH_4
        CodeArtsArtifactClient client = CodeArtsArtifactClient.newBuilder()
            .withCredential(showProjectReleaseFilesAuth)
            .withRegion(CodeArtsArtifactRegion.CN_NORTH_4)
            .build();
        // 构建请求
        ShowProjectReleaseFilesRequest request = new ShowProjectReleaseFilesRequest();
        // 项目ID，对应"需求管理CodeArts Req"项目唯一标识，私有依赖库首页地址栏url https://{host}/cloudartifact/project/{projectId}/repository中projectId变量的值。
        request.withProjectId("{projectId}");
        // 文件名称,模糊搜索
        request.withFileName("common-utils");
        try {
            // 获取结果
            ShowProjectReleaseFilesResponse response = client.showProjectReleaseFiles(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例
#### ShowProjectReleaseFiles
```
{
    result: class StandardResponseResult {
        data: [class ReleaseFileVersionDo {
            version: 20241017.5
            path: /demo-tour-cse-54598263/20241017.5/
            downloadUrl: https://devrepo.devcloud.cn-north-4.huaweicloud.com/DevRepoServer/v1/files/download?filename=xxxxxx/demo-tour-cse-54598263/20241017.5/common-utils-1.0-SNAPSHOT.jar
        }, class ReleaseFileVersionDo {
            version: 20241017.1
            path: /demo-tour-cse-54598263/20241017.1/
            downloadUrl: https://devrepo.devcloud.cn-north-4.huaweicloud.com/DevRepoServer/v1/files/download?filename=xxxxxx/demo-tour-cse-54598263/20241017.1/common-utils-1.0-SNAPSHOT.jar
        }]
        totalRecords: 2
        totalPages: 1
    }
}
```
### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: |:----:| :------:  
 2024/10/23 | 1.0  | 文档首次发布