### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeArtsArtifact/doc?api=ShowProjectReleaseFiles).

2.In this example, you can obtain the file version list of a project.

3.Obtains the list of files that meet the requirements in a specified project based on the project ID and file name. The list contains the file version, file path, and file download link.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account.
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console.
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A project has been created on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-codeartsartifact. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartsartifact</artifactId>
    <version>3.1.118</version>
</dependency>
```

### Code example
The following code shows how to obtain the list of file version information in a project:
``` java
package com.huawei.codeartsartifact;

import com.huaweicloud.sdk.codeartsartifact.v2.CodeArtsArtifactClient;
import com.huaweicloud.sdk.codeartsartifact.v2.model.ShowProjectReleaseFilesRequest;
import com.huaweicloud.sdk.codeartsartifact.v2.model.ShowProjectReleaseFilesResponse;
import com.huaweicloud.sdk.codeartsartifact.v2.region.CodeArtsArtifactRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowProjectReleaseFiles {

    private static final Logger logger = LoggerFactory.getLogger(ShowProjectReleaseFiles.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configure authentication information.
        ICredential showProjectReleaseFilesAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // Create a service client and set the corresponding region to CodeArtsArtifactRegion.CN_NORTH_4.
        CodeArtsArtifactClient client = CodeArtsArtifactClient.newBuilder()
            .withCredential(showProjectReleaseFilesAuth)
            .withRegion(CodeArtsArtifactRegion.CN_NORTH_4)
            .build();
        // Construction request
        ShowProjectReleaseFilesRequest request = new ShowProjectReleaseFilesRequest();
        // Project ID, which corresponds to the unique project ID of the requirement management CodeArts Req project. It is the value of the projectId variable in the url https://{host}/cloudartifact/project/{projectId}/repository address bar on the home page of the private dependency library.
        request.withProjectId("{projectId}");
        // File name, which is used for fuzzy search.
        request.withFileName("common-utils");
        try {
            // Obtaining Results
            ShowProjectReleaseFilesResponse response = client.showProjectReleaseFiles(request);
            // Print Results
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
#### ShowProjectReleaseFiles
```
{
    result: class StandardResponseResult {
        data: [class ReleaseFileVersionDo {
            version: 20241023.1
            path: /MavenDemo-80095478/20241023.1/
            downloadUrl: https://devrepo.devcloud.ap-southeast-3.huaweicloud.com/DevRepoServer/v1/files/download?filename=xxxxxx/MavenDemo-80095478/20241023.1/javaMavenDemo-1.0.jar
        }, class ReleaseFileVersionDo {
            version: 20241022.1
            path: /MavenDemo-80095478/20241022.1/
            downloadUrl: https://devrepo.devcloud.ap-southeast-3.huaweicloud.com/DevRepoServer/v1/files/download?filename=xxxxxx/MavenDemo-80095478/20241022.1/javaMavenDemo-1.0.jar
        }]
        totalRecords: 2
        totalPages: 1
    }
}
```
### Change History

 Released On  |  Issue   | Description
 :----: |:---:| :------:  
 2024/10/23 | 1.0 | This document is released for the first time.