package com.huawei.codeartsartifact;

import com.huaweicloud.sdk.codeartsartifact.v2.CodeArtsArtifactClient;
import com.huaweicloud.sdk.codeartsartifact.v2.model.AttentionDO;
import com.huaweicloud.sdk.codeartsartifact.v2.model.CreateAttentionRequest;
import com.huaweicloud.sdk.codeartsartifact.v2.model.CreateAttentionResponse;
import com.huaweicloud.sdk.codeartsartifact.v2.region.CodeArtsArtifactRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CreateAttention {

    private static final Logger logger = LoggerFactory.getLogger(CreateAttention.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential createAttentionAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // 注意，如下的 regionId 请使用项目所在的局点，例如华北-北京四区域为：cn-north-4
        String regionId = "<YOUR REGION ID>";
        // 创建服务客户端
        CodeArtsArtifactClient client = CodeArtsArtifactClient.newBuilder()
            .withCredential(createAttentionAuth)
            .withRegion(CodeArtsArtifactRegion.valueOf(regionId))
            .build();
        // 构建请求
        CreateAttentionRequest request = new CreateAttentionRequest();
        // 构建请求体
        AttentionDO body = new AttentionDO();
        // 组件id列表
        List<String> listBodyIds = new ArrayList<>();
        // 添加组件id，id的值为仓库id+文件全路径，仓库id从页面仓库地址中取，文件全路径即为组件概览页面相对路径的值
        // 例如：仓库地址https://devrepo.devcloud.cn-north-4.huaweicloud.com/artgalaxy/{仓库id}/
        listBodyIds.add("{仓库id}/com/huawei/demo/javaMavenDemo/1.0/javaMavenDemo-1.0.jar");
        body.withIds(listBodyIds);
        // 关注/取消关注，0 表示取消关注，1 表示关注
        body.withAttention("1");
        request.withBody(body);
        try {
            // 获取结果
            CreateAttentionResponse response = client.createAttention(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}