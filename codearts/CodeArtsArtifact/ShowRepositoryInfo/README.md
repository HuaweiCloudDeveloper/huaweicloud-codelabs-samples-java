### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsArtifact/debug?api=ShowRepositoryInfo) 中直接运行调试该接口。

2.在本样例中，您可以查看仓库信息。

3.我们可以创建制品仓库后，使用其查询创建仓库的详细信息。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartsartifact”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartsartifact</artifactId>
    <version>3.1.112</version>
</dependency>
```

### 代码示例
以下代码展示如何查看仓库信息示例:
``` java
package com.huawei.codeartsartifact;

import com.huaweicloud.sdk.codeartsartifact.v2.CodeArtsArtifactClient;
import com.huaweicloud.sdk.codeartsartifact.v2.model.ShowRepositoryInfoRequest;
import com.huaweicloud.sdk.codeartsartifact.v2.model.ShowRepositoryInfoResponse;
import com.huaweicloud.sdk.codeartsartifact.v2.region.CodeArtsArtifactRegion;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowRepositoryInfo {

    private static final Logger logger = LoggerFactory.getLogger(ShowRepositoryInfo.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量CLOUD_SDK_AK和CLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");
        // 注意，repoId请使用仓库概览页面的仓库地址中的repoId
        // 例:https://devrepo.devcloud.cn-north-4.huaweicloud.com/artgalaxy/{{repoId}}/
        String repoId = "YOUR REPO ID";
        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CodeArtsArtifactRegion.CN_NORTH_4
        CodeArtsArtifactClient client = CodeArtsArtifactClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeArtsArtifactRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // 创建请求头
        ShowRepositoryInfoRequest request = new ShowRepositoryInfoRequest();
        // 设置仓库Id
        request.withRepoId(repoId);
        try {
            //查看仓库信息
            ShowRepositoryInfoResponse response = client.showRepositoryInfo(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }

    }
}
```

### 返回结果示例
#### ShowRepositoryInfo
```
{
 "status": "success",
 "trace_id": "508958-172535*******",
 "result": {
  "status": null,
  "domainId": null,
  "region": null,
  "createdTime": null,
  "modifiedTime": "2024-05-27 22:37:03",
  "createdUserId": null,
  "createdUserName": null,
  "modifiedUserId": "05c7cfe8df0025e71********",
  "modifiedUserName": "****",
  "name": "cn-north-4_05c7cfe66a8025e90f1dc0058*****",
  "disable": false,
  "format": "maven2",
  "type": "hosted",
  "policy": null,
  "tabId": "05c7cfe66a8025e90f1dc0058406******",
  "repositoryName": "cesh*****",
  "displayName": null,
  "description": "",
  "snapshot": "ceshi111******",
  "release": "ceshi111111******",
  "npm": null,
  "snapshotStatus": "active",
  "releaseStatus": "active",
  "projectId": "08084fba7b65412aa70*******",
  "includesPattern": "**/*",
  "repositoryIds": null,
  "uri": null,
  "deploymentPolicy": "allowRedeploy",
  "repositories": null,
  "parentRepoName": null,
  "userName": null,
  "password": null,
  "remoteUrl": null,
  "pyPIRegistryUrl": null,
  "defaultDeployRepository": null,
  "remoteType": null,
  "proxy": null,
  "allowAnonymous": false,
  "autoCleanSnapshot": null,
  "snapshotAliveDays": null,
  "maxUniqueSnapshots": "10",
  "shareRight": "PROJECT",
  "nexuRepo": false,
  "migrateFlag": 0,
  "obsSpeedBucket": null,
  "associateProject": null,
  "url": "https://devrepo.devcloud.cn-north-4.huaweicloud.com/artgalaxy/cn-north-4_05c7cfe66a8025e90f1d***********",
  "packageType": "maven"
 }
}
```

### 修订记录
 
 发布日期  | 文档版本 | 修订说明
 :----: |:----:| :------:  
 2024/09/03 | 1.0  | 文档首次发布