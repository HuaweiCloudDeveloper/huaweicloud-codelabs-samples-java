package com.huawei.codeartsartifact;

import com.huaweicloud.sdk.codeartsartifact.v2.CodeArtsArtifactClient;
import com.huaweicloud.sdk.codeartsartifact.v2.model.ShowRepositoryInfoRequest;
import com.huaweicloud.sdk.codeartsartifact.v2.model.ShowRepositoryInfoResponse;
import com.huaweicloud.sdk.codeartsartifact.v2.region.CodeArtsArtifactRegion;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowRepositoryInfo{

    private static final Logger logger = LoggerFactory.getLogger(ShowRepositoryInfo.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量CLOUD_SDK_AK和CLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");
        // 注意，repoId请使用仓库概览页面的仓库地址中的repoId
        // 例:https://devrepo.devcloud.cn-north-4.huaweicloud.com/artgalaxy/{{repoId}}/
        String repoId = "YOUR REPO ID";
        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CodeArtsArtifactRegion.CN_NORTH_4
        CodeArtsArtifactClient client = CodeArtsArtifactClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeArtsArtifactRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // 创建请求头
        ShowRepositoryInfoRequest request = new ShowRepositoryInfoRequest();
        // 设置仓库Id
        request.withRepoId(repoId);
        try {
            //查看仓库信息
            ShowRepositoryInfoResponse response = client.showRepositoryInfo(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }

    }
}