### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsArtifact/doc?api=ShowAudit) 中直接运行调试该接口。

2.在本样例中，您可以查询仓库或文件的审计日志信息。

3.查询的日志信息中，包含了仓库或文件的操作记录，并且记录了操作的人员信息和IP地址，可以做到有效地追溯。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartsartifact”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartsartifact</artifactId>
    <version>3.1.112</version>
</dependency>
```

### 代码示例
以下代码展示如何查询仓库或文件的审计日志信息：
``` java
package com.huawei.codeartsartifact;

import com.huaweicloud.sdk.codeartsartifact.v2.CodeArtsArtifactClient;
import com.huaweicloud.sdk.codeartsartifact.v2.model.ShowAuditRequest;
import com.huaweicloud.sdk.codeartsartifact.v2.model.ShowAuditResponse;
import com.huaweicloud.sdk.codeartsartifact.v2.region.CodeArtsArtifactRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowAudit {

    private static final Logger logger = LoggerFactory.getLogger(ShowAudit.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential showAuditAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // 注意，如下的 regionId 请使用项目所在的局点，例如华北-北京四区域为：cn-north-4
        String regionId = "<YOUR REGION ID>";
        // 创建服务客户端
        CodeArtsArtifactClient client = CodeArtsArtifactClient.newBuilder()
            .withCredential(showAuditAuth)
            .withRegion(CodeArtsArtifactRegion.valueOf(regionId))
            .build();
        // 构建请求
        ShowAuditRequest request = new ShowAuditRequest();
        // 账号id，获取方式：登录华为云管理控制台，单击用户名，在下拉列表中单击“我的凭证”，在“API凭证”页面中查看账号ID。
        request.withTenantId("{tenantId}");
        // 项目ID，对应"需求管理CodeArts Req"项目唯一标识，私有依赖库首页地址栏url https://{host}/cloudartifact/project/{projectId}/repository中projectId变量的值。
        request.withProjectId("{projectId}");
        // 模块，取值范围是file和repository，file查文件，repository查仓库。
        request.withModule("file");
        // 仓库id，从制品仓库页面仓库地址中获取，https://devrepo.devcloud.cn-north-4.huaweicloud.com/artgalaxy/{repoId}/中repoId变量的值。
        request.withRepo("{repoId}");
        try {
            // 获取结果
            ShowAuditResponse response = client.showAudit(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例
#### ShowAudit
```
{
    status: success
    traceId: xxxxxx
    result: {auditInfoList=[{id=xxxxxx, module=file, operation=upload, time=2024/09/09 10:44:48, info=cn-north-4_xxxxxx_maven_1_2, data=com/huawei/demo/javaMavenDemo/2.0/javaMavenDemo-2.0.pom.md5, reason=null, userAgent=Apache-Maven/3.5.3 (Java 1.8.0_40; Linux 3.10.0-862.14.1.5.h520.eulerosv2r7.x86_64), operator=xxxxxx, ipSource=121.xxx.xxx.xxx}, {id=xxxxxx, module=file, operation=upload, time=2024/09/09 10:44:48, info=cn-north-4_xxxxxx_maven_1_2, data=com/huawei/demo/javaMavenDemo/2.0/javaMavenDemo-2.0.pom.sha1, reason=null, userAgent=Apache-Maven/3.5.3 (Java 1.8.0_40; Linux 3.10.0-862.14.1.5.h520.eulerosv2r7.x86_64), operator=xxxxxx, ipSource=121.xxx.xxx.xxx}], total=2}
}
```
### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: |:----:| :------:  
 2024/09/09 | 1.0  | 文档首次发布