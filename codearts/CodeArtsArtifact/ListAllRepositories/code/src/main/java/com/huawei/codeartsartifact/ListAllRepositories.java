package com.huawei.codeartsartifact;

import com.huaweicloud.sdk.codeartsartifact.v2.CodeArtsArtifactClient;
import com.huaweicloud.sdk.codeartsartifact.v2.model.ListAllRepositoriesRequest;
import com.huaweicloud.sdk.codeartsartifact.v2.model.ListAllRepositoriesResponse;
import com.huaweicloud.sdk.codeartsartifact.v2.region.CodeArtsArtifactRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListAllRepositories {

    private static final Logger logger = LoggerFactory.getLogger(ListAllRepositories.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential listAllRepositoriesAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // 注意，如下的 regionId 请使用项目所在的局点，例如华北-北京四区域为：cn-north-4
        String regionId = "<YOUR REGION ID>";
        // 创建服务客户端
        CodeArtsArtifactClient client = CodeArtsArtifactClient.newBuilder()
            .withCredential(listAllRepositoriesAuth)
            .withRegion(CodeArtsArtifactRegion.valueOf(regionId))
            .build();
        // 构建请求
        ListAllRepositoriesRequest request = new ListAllRepositoriesRequest();
        // 账号id，获取方式：登录华为云管理控制台，单击用户名，在下拉列表中单击“我的凭证”，在“API凭证”页面中查看账号ID。
        request.withTenantId("{tenantId}");
        // 项目ID，对应"需求管理CodeArts Req"项目唯一标识，私有依赖库首页地址栏url https://{host}/cloudartifact/project/{projectId}/repository中projectId变量的值。
        request.withProjectId("{projectId}");
        // 是否在项目中，true：项目中，false：不在项目中
        request.withInProject(true);
        try {
            // 获取结果
            ListAllRepositoriesResponse response = client.listAllRepositories(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}