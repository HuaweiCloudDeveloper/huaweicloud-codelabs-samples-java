package com.huawei.codeartsartifact;

import com.huaweicloud.sdk.codeartsartifact.v2.CodeArtsArtifactClient;
import com.huaweicloud.sdk.codeartsartifact.v2.model.ListArtifactoryStorageStatisticRequest;
import com.huaweicloud.sdk.codeartsartifact.v2.model.ListArtifactoryStorageStatisticResponse;
import com.huaweicloud.sdk.codeartsartifact.v2.region.CodeArtsArtifactRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListArtifactoryStorageStatistic {

    private static final Logger logger = LoggerFactory.getLogger(ListArtifactoryStorageStatistic.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configure authentication information.
        ICredential listArtifactoryStorageStatisticAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // Create a service client and set the corresponding region to CodeArtsArtifactRegion.CN_NORTH_4.
        CodeArtsArtifactClient client = CodeArtsArtifactClient.newBuilder()
            .withCredential(listArtifactoryStorageStatisticAuth)
            .withRegion(CodeArtsArtifactRegion.CN_NORTH_4)
            .build();
        // Construction request
        ListArtifactoryStorageStatisticRequest request = new ListArtifactoryStorageStatisticRequest();
        // Account ID. To obtain the ID, log in to the HUAWEI CLOUD management console, click the username, click My Credential from the drop-down list, and view the account ID on the API Credential page.
        request.withTenantId("{tenantId}");
        // Project ID, which corresponds to the unique project ID of the requirement management CodeArts Req project. It is the value of the projectId variable in the url https://{host}/cloudartifact/project/{projectId}/repository address bar on the home page of the private dependency library.
        request.withProjectId("{projectId}");
        // Start time
        request.withStartTime("2024-10-21");
        // End Time
        request.withEndTime("2024-10-22");
        try {
            // Obtaining Results
            ListArtifactoryStorageStatisticResponse response = client.listArtifactoryStorageStatistic(request);
            // Print Results
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}