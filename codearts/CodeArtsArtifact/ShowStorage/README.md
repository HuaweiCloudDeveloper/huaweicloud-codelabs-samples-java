### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsArtifact/sdk?api=ShowStorage) 中直接运行调试该接口。

2.在本样例中，您可以进行仓库用量查询。

3.可以根据仓库类型查询某种类型仓库的数量和占用的空间。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目。

6.已在项目下创建制品仓库。

7.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartsartifact”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartsartifact</artifactId>
    <version>3.1.113</version>
</dependency>
```

### 代码示例
``` java
public class ShowStorage {

    private static final Logger logger = LoggerFactory.getLogger(ShowStorage.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String showStorageAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showStorageSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(showStorageAk)
                .withSk(showStorageSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeArtsArtifactRegion.CN_NORTH_4
        CodeArtsArtifactClient client = CodeArtsArtifactClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeArtsArtifactRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头，设置请求参数仓库类型列表，是否在项目中
        ShowStorageRequest request = new ShowStorageRequest();
        // 仓库类型列表
        // 取值范围：maven2，docker，conan，debian，go，nuget，rpm，pypi，npm，cocoapods，generic。
        String artifactFormat = "maven2";
        request.withFormatList(artifactFormat);
        // 是否在项目中
        // 取值范围：true：项目中，false：项目外
        String inProject = "true";
        request.withInProject(inProject);
        try {
            ShowStorageResponse response = client.showStorage(request);
            logger.info("ShowStorage: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ShowStorage connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ShowStorage RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ShowStorage ServiceResponseException", e);
        }
    }
}
```

### 返回结果示例
```
{
 "status": "success",
 "trace_id": "508958-1725947891526-11925",
 "result": {
  "totalStorage": "254.66 MB",
  "totalCount": 10
 }
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-09-10 | 1.0 | 文档首次发布 |