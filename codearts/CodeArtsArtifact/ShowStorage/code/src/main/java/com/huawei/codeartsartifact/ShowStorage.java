package com.huawei.codeartsartifact;

import com.huaweicloud.sdk.codeartsartifact.v2.model.ShowStorageRequest;
import com.huaweicloud.sdk.codeartsartifact.v2.model.ShowStorageResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.codeartsartifact.v2.region.CodeArtsArtifactRegion;
import com.huaweicloud.sdk.codeartsartifact.v2.CodeArtsArtifactClient;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowStorage {

    private static final Logger logger = LoggerFactory.getLogger(ShowStorage.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String showStorageAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showStorageSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(showStorageAk)
                .withSk(showStorageSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeArtsArtifactRegion.CN_NORTH_4
        CodeArtsArtifactClient client = CodeArtsArtifactClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeArtsArtifactRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头，设置请求参数仓库类型列表，是否在项目中
        ShowStorageRequest request = new ShowStorageRequest();
        // 仓库类型列表
        // 取值范围：maven2，docker，conan，debian，go，nuget，rpm，pypi，npm，cocoapods，generic。
        String artifactFormat = "maven2";
        request.withFormatList(artifactFormat);
        // 是否在项目中
        // 取值范围：true：项目中，false：项目外
        String inProject = "true";
        request.withInProject(inProject);
        try {
            ShowStorageResponse response = client.showStorage(request);
            logger.info("ShowStorage: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ShowStorage connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ShowStorage RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ShowStorage ServiceResponseException", e);
        }
    }
}