package com.huawei.codeartsartifact;

import com.huaweicloud.sdk.codeartsartifact.v2.CodeArtsArtifactClient;
import com.huaweicloud.sdk.codeartsartifact.v2.model.CreateMavenRepoRequest;
import com.huaweicloud.sdk.codeartsartifact.v2.model.CreateMavenRepoResponse;
import com.huaweicloud.sdk.codeartsartifact.v2.model.IDERepositoryDO;
import com.huaweicloud.sdk.codeartsartifact.v2.region.CodeArtsArtifactRegion;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateMavenRepo{

    private static final Logger logger = LoggerFactory.getLogger(CreateMavenRepo.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量CLOUD_SDK_AK和CLOUD_SDK_SK。
        String ak = System.getenv("CLOUD_SDK_AK");
        String sk = System.getenv("CLOUD_SDK_SK");
        // 注意，如下的 projectId 请使用对应CodeArts项目ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{{projectId}}/workitem/list
        String projectId = "YOUR PROJECT ID";
        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CodeArtsArtifactRegion.CN_NORTH_4
        CodeArtsArtifactClient client = CodeArtsArtifactClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeArtsArtifactRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // 创建请求头
        CreateMavenRepoRequest request = new CreateMavenRepoRequest();
        IDERepositoryDO body = new IDERepositoryDO();
        // 设置maven仓库名称
        body.setRepositoryName("mavenrepo");
        // 设置仓库类型
        body.setFormat("maven2");
        // 设置release仓库名称
        body.setRelease("release");
        // 设置snapshot仓库名称
        body.setSnapshot("snapshot");
        body.setProjectId(projectId);
        // 设置maven仓库类别 本地仓
        body.setType("hosted");
        request.withBody(body);
        try {
            //创建私有依赖库
            CreateMavenRepoResponse response = client.createMavenRepo(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }

    }
}