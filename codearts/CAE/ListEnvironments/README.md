### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CAE/doc?api=ListEnvironments) 中直接运行调试该接口。

2.在本样例中，您可以获取所有的环境列表信息。

3.通过返回的环境列表，可以了解每个环境的状态，环境更新的时间，以及一些环境附加属性。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在云应用引擎CAE服务中创建环境。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-cae”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cae</artifactId>
    <version>3.1.111</version>
</dependency>
```

### 代码示例
以下代码展示如何获取环境列表：
``` java
package com.huawei.cae;

import com.huaweicloud.sdk.cae.v1.CaeClient;
import com.huaweicloud.sdk.cae.v1.model.ListEnvironmentsRequest;
import com.huaweicloud.sdk.cae.v1.model.ListEnvironmentsResponse;
import com.huaweicloud.sdk.cae.v1.region.CaeRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListEnvironments {

    private static final Logger logger = LoggerFactory.getLogger(ListEnvironments.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk);
        // 注意，如下的 regionId 请使用项目所在的局点，例如华北-北京四区域为：cn-north-4
        String regionId = "<YOUR REGION ID>";
        // 创建服务客户端
        CaeClient client = CaeClient.newBuilder().withCredential(auth).withRegion(CaeRegion.valueOf(regionId)).build();
        // 构建请求
        ListEnvironmentsRequest request = new ListEnvironmentsRequest();
        try {
            // 获取结果
            ListEnvironmentsResponse response = client.listEnvironments(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例
#### ListEnvironments
```
{
    apiVersion: v1
    kind: Environment
    items: [class EnvironmentItem {
        id: xxxxxx
        name: product
        jobId: xxxxxx
        status: finish
        annotations: {cluster_id=xxxxxx, engine_id=xxxxxx, enterprise_project_id=xxx, env_category=xxx, group_name=cloud-xxx, inbound_eip_addr=xxxxxx, namespace=xxxxxx, public_elb_id=xxxxxx, security_group_id=xxxxxx, subnet_id=xxxxxx, type=exclusive, vpc_id=xxxxxx1e}
        createdAt: 2024-08-28T05:58:25.005181Z
        updatedAt: 2024-08-28T05:59:02.213257Z
    }]
}
```
### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: |:----:| :------:  
 2024/08/28 | 1.0  | 文档首次发布