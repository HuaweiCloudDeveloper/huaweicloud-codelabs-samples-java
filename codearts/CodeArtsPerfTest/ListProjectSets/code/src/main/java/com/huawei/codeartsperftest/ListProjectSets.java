package com.huawei.codeartsperftest;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.cpts.v1.CptsClient;
import com.huaweicloud.sdk.cpts.v1.model.ListProjectSetsRequest;
import com.huaweicloud.sdk.cpts.v1.model.ListProjectSetsResponse;
import com.huaweicloud.sdk.cpts.v1.region.CptsRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListProjectSets {

    private static final Logger logger = LoggerFactory.getLogger(ListProjectSets.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 配置认证信息
        ICredential listProjectSetsAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // 注意，如下的 regionId 请使用项目所在的局点，例如华北-北京四区域为：cn-north-4
        String regionId = "<YOUR REGION ID>";
        // 创建服务客户端
        CptsClient client = CptsClient.newBuilder()
            .withCredential(listProjectSetsAuth)
            .withRegion(CptsRegion.valueOf(regionId))
            .build();
        // 构建请求
        ListProjectSetsRequest request = new ListProjectSetsRequest();
        try {
            // 获取结果
            ListProjectSetsResponse response = client.listProjectSets(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}