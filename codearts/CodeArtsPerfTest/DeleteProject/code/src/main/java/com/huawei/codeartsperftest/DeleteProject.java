package com.huawei.codeartsperftest;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.cpts.v1.CptsClient;
import com.huaweicloud.sdk.cpts.v1.model.DeleteProjectRequest;
import com.huaweicloud.sdk.cpts.v1.model.DeleteProjectResponse;
import com.huaweicloud.sdk.cpts.v1.region.CptsRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteProject {

    private static final Logger logger = LoggerFactory.getLogger(DeleteProject.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential deleteProjectAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // Create a service client and set the corresponding region to CptsRegion.CN_NORTH_4.
        CptsClient client = CptsClient.newBuilder()
            .withCredential(deleteProjectAuth)
            .withRegion(CptsRegion.CN_NORTH_4)
            .build();
        // Construction request
        DeleteProjectRequest request = new DeleteProjectRequest();
        // Test project ID. Use the ID in the PerfTest test project details link.
        // Example: https://console-intl.huaweicloud.com/cpts/?locale=zh-cn&region=ap-southeast-3#/project/test-case?id={id}&caseId={caseId}&type=0
        request.withTestSuiteId(111111);
        try {
            // Obtaining Results
            DeleteProjectResponse response = client.deleteProject(request);
            // Print Results
            logger.info(String.valueOf(response.getHttpStatusCode()));
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}