package com.huawei.codeartsperftest;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.cpts.v1.CptsClient;
import com.huaweicloud.sdk.cpts.v1.model.CreateVariableRequest;
import com.huaweicloud.sdk.cpts.v1.model.CreateVariableRequestBody;
import com.huaweicloud.sdk.cpts.v1.model.CreateVariableResponse;
import com.huaweicloud.sdk.cpts.v1.region.CptsRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CreateVariable {

    private static final Logger logger = LoggerFactory.getLogger(CreateVariable.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential createVariableAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // Create a service client and set the corresponding region to CptsRegion.CN_NORTH_4.
        CptsClient client = CptsClient.newBuilder()
            .withCredential(createVariableAuth)
            .withRegion(CptsRegion.CN_NORTH_4)
            .build();
        // Construction request
        CreateVariableRequest request = new CreateVariableRequest();
        // Test project ID. Use the ID in the PerfTest test project details link.
        // Example: https://console-intl.huaweicloud.com/cpts/?locale=zh-cn&region=ap-southeast-3#/project/test-case?id={id}&caseId={caseId}&type=0
        request.withTestSuiteId(111111);
        // Set the value of a global variable.
        List<Object> variable = new ArrayList<>();
        variable.add(5);
        variable.add(10);
        // Construct the request body and set parameters.
        List<CreateVariableRequestBody> body = new ArrayList<>();
        body.add(new CreateVariableRequestBody()
            // Variable Name
            .withName("num")
            // Variable Value
            .withVariable(variable)
            // Referenced or Not
            .withIsQuoted(false)
            // Variable type (1: integer; 2: enumeration; 5: text)
            .withVariableType(1));
        request.withBody(body);
        try {
            // Obtaining Results
            CreateVariableResponse response = client.createVariable(request);
            // Print Results
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}