### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CPTS/doc?api=CreateVariable).

2.In this example, you can create global variables.

3.Create a global variable in a specified project based on the test project ID. The global variable supports multiple types, such as integer, enumeration, and text. After the global variable is created successfully, the variable ID is returned for future use.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. 
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. 
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.You have enabled the performance test service and have the operation permission.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-cpts. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cpts</artifactId>
    <version>3.1.119</version>
</dependency>
```

### Code example
The following code shows how to create a variable:
``` java
package com.huawei.codeartsperftest;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.cpts.v1.CptsClient;
import com.huaweicloud.sdk.cpts.v1.model.CreateVariableRequest;
import com.huaweicloud.sdk.cpts.v1.model.CreateVariableRequestBody;
import com.huaweicloud.sdk.cpts.v1.model.CreateVariableResponse;
import com.huaweicloud.sdk.cpts.v1.region.CptsRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CreateVariable {

    private static final Logger logger = LoggerFactory.getLogger(CreateVariable.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Configuring Authentication Information
        ICredential createVariableAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // Create a service client and set the corresponding region to CptsRegion.CN_NORTH_4.
        CptsClient client = CptsClient.newBuilder()
            .withCredential(createVariableAuth)
            .withRegion(CptsRegion.CN_NORTH_4)
            .build();
        // Construction request
        CreateVariableRequest request = new CreateVariableRequest();
        // Test project ID. Use the ID in the PerfTest test project details link.
        // Example: https://console-intl.huaweicloud.com/cpts/?locale=zh-cn&region=ap-southeast-3#/project/test-case?id={id}&caseId={caseId}&type=0
        request.withTestSuiteId(111111);
        // Set the value of a global variable.
        List<Object> variable = new ArrayList<>();
        variable.add(5);
        variable.add(10);
        // Construct the request body and set parameters.
        List<CreateVariableRequestBody> body = new ArrayList<>();
        body.add(new CreateVariableRequestBody()
            // Variable Name
            .withName("num")
            // Variable Value
            .withVariable(variable)
            // Referenced or Not
            .withIsQuoted(false)
            // Variable type (1: integer; 2: enumeration; 5: text)
            .withVariableType(1));
        request.withBody(body);
        try {
            // Obtaining Results
            CreateVariableResponse response = client.createVariable(request);
            // Print Results
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
#### CreateVariable
```
{
    code: SVCSTG.CPTS.0000000
    json: class CreateVariableResultJson {
        variableId: 15xxxx
    }
    message: success
}
```
### Change History

 Released On  |  Issue   | Description
 :----: |:---:| :------:  
 2024/10/31 | 1.0 | This document is released for the first time.