### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CloudTest/doc?api=DeleteService) 中直接运行调试该接口。
2.在本样例中，您可以删除已注册服务。
3.注册服务默认有数量限制，可以通过删除已注册服务，删除不需要的注册服务，以方便新增其他注册服务。

### 前置条件
1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
2.已具备开发环境 ，支持Java JDK 1.8及其以上版本。
3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
4.已在CodeArts平台创建项目。
5.已通过新测试类型服务注册API注册服务。
6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-cloudtest”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cloudtest</artifactId>
    <version>3.1.110</version>
</dependency>
```

### 代码示例
``` java
public class DeleteService {

    private static final Logger logger = LoggerFactory.getLogger(DeleteService.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK
        String deleteServiceAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String deleteServiceSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(deleteServiceAk)
                .withSk(deleteServiceSk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CloudtestRegion.CN_NORTH_4
        CloudtestClient client = CloudtestClient.newBuilder()
                .withCredential(auth)
                .withRegion(CloudtestRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();

        // 构建请求头，设置请求参数服务ID
        DeleteServiceRequest request = new DeleteServiceRequest();

        // 注册服务唯一标识，该值由新测试类型服务注册接口返回。
        // 注意：使用时，需要替换成自己注册的服务ID，例如：11
        Integer serviceId = -1;
        request.withServiceId(serviceId);
        try {
            DeleteServiceResponse response = client.deleteService(request);
            logger.info("DeleteService: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("DeleteService connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("DeleteService RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("DeleteService ServiceResponseException", e);
        }
    }
}
```

### 返回结果示例
```
空
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-08-22 | 1.0 | 文档首次发布 |