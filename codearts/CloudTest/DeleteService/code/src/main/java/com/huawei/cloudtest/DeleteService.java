package com.huawei.cloudtest;

import com.huaweicloud.sdk.cloudtest.v1.CloudtestClient;
import com.huaweicloud.sdk.cloudtest.v1.model.DeleteServiceRequest;
import com.huaweicloud.sdk.cloudtest.v1.model.DeleteServiceResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.cloudtest.v1.region.CloudtestRegion;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteService {

    private static final Logger logger = LoggerFactory.getLogger(DeleteService.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK
        String deleteServiceAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String deleteServiceSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(deleteServiceAk)
                .withSk(deleteServiceSk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CloudtestRegion.CN_NORTH_4
        CloudtestClient client = CloudtestClient.newBuilder()
                .withCredential(auth)
                .withRegion(CloudtestRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();

        // 构建请求头，设置请求参数服务ID
        DeleteServiceRequest request = new DeleteServiceRequest();

        // 注册服务唯一标识，该值由新测试类型服务注册接口返回。
        // 注意：使用时，需要替换成自己注册的服务ID，例如：11
        Integer serviceId = -1;
        request.withServiceId(serviceId);
        try {
            DeleteServiceResponse response = client.deleteService(request);
            logger.info("DeleteService: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("DeleteService connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("DeleteService RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("DeleteService ServiceResponseException", e);
        }
    }
}