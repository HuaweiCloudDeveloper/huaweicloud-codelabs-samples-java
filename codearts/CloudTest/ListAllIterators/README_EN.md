### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CloudTest/doc?api=ListAllIterators).

2.In this example, you can query all iteration plans of a project.

3.Query all iteration plans of a project. You can learn about the start time, end time, and resource URI of the iteration. This helps you better understand the test progress and plan and better arrange and manage the test work.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A project has been created on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-cloudtest. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cloudtest</artifactId>
    <version>3.1.118</version>
</dependency>

```

### Code example

``` java
package com.huawei.cloudtest;

import com.huaweicloud.sdk.cloudtest.v1.CloudtestClient;
import com.huaweicloud.sdk.cloudtest.v1.model.ListAllIteratorsRequest;
import com.huaweicloud.sdk.cloudtest.v1.model.ListAllIteratorsResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.cloudtest.v1.region.CloudtestRegion;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListAllIterators {

    private static final Logger logger = LoggerFactory.getLogger(ListAllIterators.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listAllIteratorsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listAllIteratorsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listAllIteratorsAk)
                .withSk(listAllIteratorsSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CloudtestRegion.CN_NORTH_4.
        CloudtestClient client = CloudtestClient.newBuilder()
                .withCredential(auth)
                .withRegion(CloudtestRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request and set the request parameter project ID.
        ListAllIteratorsRequest request = new ListAllIteratorsRequest();
        // Project ID. For the following project ID, use the project ID on the homepage of CodeArts. Click a project link.
        // Example: https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/List
        String projectId = "<YOUR PROJECT ID>";
        request.withProjectId(projectId);
        try {
            ListAllIteratorsResponse response = client.listAllIterators(request);
            logger.info("ListAllIterators: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListAllIterators connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListAllIterators RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListAllIterators ServiceResponseException", e);
        }
    }
}
```

### Example of the returned result

#### ListAllIterators

```
{
    status: success
    result: class ResultValueListTestVersionVo {
        total: null
        value: [class TestVersionVo {
            uri: vd0v0000vv33lt5r
            type: TestVersion
            author: f1ac****
            name: abc
            rank: null
            version: null
            owner: null
            creator: null
            iterations: null
            description: null
            region: cn-north-4
            lastModifier: f1ac****
            lastModified: 2024-10-22T14:40:12Z
            lastModifiedTimestamp: 1729579212000
            lastChangeTime: 2024-10-22T14:40:12Z
            versionUri: ad16****
            originUri: null
            parentUri: ad16****
            parentPath: /ad16****/
            creationVersionUri: ad16****
            creationDate: 2024-10-22T14:40:12Z
            creationDateTimestamp: 1729579212000
            authorName: Developer
            comment: null
            number: null
            isMaster: 0
            isIterator: 0
            planStartDate: null
            planEndDate: null
            serviceId: ad16****
            serviceName: Scrum-后端技术
            pbiId: null
            pbiName: null
            planId: null
            metricPbiIds: null
            metricPbiIdNames: null
            lastSynDate: null
            isClosed: null
            asynGit: null
            schemaNo: 3
            finishDate: null
            ownerName: null
            creatorName: null
            currentStage: null
            serviceTypes: null
            riskRating: 0
            riskDes: null
            projectUuid: ad16****
            domainId: null
            piId: null
        }]
        reason: null
        pageSize: null
        pageNo: null
        hasMore: null
    }
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/10/23 | 1.0 | This document is released for the first time.