### 产品介绍

1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CloudTest/sdk?api=ListAllIterators) 中直接运行调试该接口。

2.在本样例中，您可以查询项目下所有迭代计划。

3.查询项目下所有迭代计划，可以了解迭代的开始时间，结束时间，资源URI等信息，帮助您更好地了解测试的进度和计划，从而更好地安排和管理测试工作。

### 前置条件

1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。

2.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已在CodeArts平台创建项目。

5.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-cloudtest”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cloudtest</artifactId>
    <version>3.1.118</version>
</dependency>
```

### 代码示例
```java
public class ListAllIterators {

    private static final Logger logger = LoggerFactory.getLogger(ListAllIterators.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listAllIteratorsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listAllIteratorsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(listAllIteratorsAk)
                .withSk(listAllIteratorsSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CloudtestRegion.CN_NORTH_4
        CloudtestClient client = CloudtestClient.newBuilder()
                .withCredential(auth)
                .withRegion(CloudtestRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求，设置请求参数项目ID
        ListAllIteratorsRequest request = new ListAllIteratorsRequest();
        // 项目ID。注意,如下的 projectId 请使用CodeArts对应项目首页，点击某个项目链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/List
        String projectId = "<YOUR PROJECT ID>";
        request.withProjectId(projectId);
        try {
            ListAllIteratorsResponse response = client.listAllIterators(request);
            logger.info("ListAllIterators: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListAllIterators connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListAllIterators RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListAllIterators ServiceResponseException", e);
        }
    }
}
```

### 返回结果示例
```
{
    status: success
    result: class ResultValueListTestVersionVo {
        total: null
        value: [class TestVersionVo {
            uri: vd100000vv2olagn
            type: TestVersion
            author: f1ac****
            name: testplan1
            rank: null
            version: null
            owner: f1ac****
            creator: null
            iterations: []
            description: null
            region: cn-north-4
            lastModifier: f1ac****
            lastModified: 2024-10-22T11:31:23Z
            lastModifiedTimestamp: 1729567883000
            lastChangeTime: 2024-10-22T11:31:23Z
            versionUri: vd160000vkup0pbu
            originUri: null
            parentUri: vd160000vkup0pbu
            parentPath: /ad16****/vd160000vkup0pbu/
            creationVersionUri: vd160000vkup0pbu
            creationDate: 2024-10-22T11:27:39Z
            creationDateTimestamp: 1729567659000
            authorName: Developer
            comment: null
            number: null
            isMaster: 0
            isIterator: 1
            planStartDate: 2024-10-22T00:00Z
            planEndDate: 2024-10-31T23:59:59Z
            serviceId: ad16****
            serviceName: Scrum-后端技术
            pbiId: null
            pbiName: null
            planId: null
            metricPbiIds: null
            metricPbiIdNames: null
            lastSynDate: null
            isClosed: null
            asynGit: null
            schemaNo: 3
            finishDate: null
            ownerName: Developer
            creatorName: null
            currentStage: execute
            serviceTypes: ["0","11","1","58"]
            riskRating: 0
            riskDes: null
            projectUuid: ad16****
            domainId: null
            piId: notSelected
        }]
        reason: null
        pageSize: null
        pageNo: null
        hasMore: null
    }
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-10-23 | 1.0 | 文档首次发布 |