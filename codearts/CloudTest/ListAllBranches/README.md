### 产品介绍

1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/Cloudtest/sdk?api=ListAllBranches) 中直接运行调试该接口。

2.在本样例中，您可以获取分支列表。

3.获取分支列表，可以根据分支管理测试计划，掌握分支的创建时间，创建人信息等。

### 前置条件

1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。

2.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已在CodeArts平台创建项目。

5.本功能属于CodeArts TestPlan高阶特性，提供65天试用期，后续用户需开通测试计划专业版套餐或CodeArts专业版以上套餐，可参考[套餐说明](https://support.huaweicloud.com/price-testman/cloudtest_bill_2003.html)。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-cloudtest”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cloudtest</artifactId>
    <version>3.1.118</version>
</dependency>
```

### 代码示例
```java
public class ListAllBranches {

    private static final Logger logger = LoggerFactory.getLogger(ListAllBranches.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listAllBranchesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listAllBranchesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(listAllBranchesAk)
                .withSk(listAllBranchesSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CloudtestRegion.CN_NORTH_4
        CloudtestClient client = CloudtestClient.newBuilder()
                .withCredential(auth)
                .withRegion(CloudtestRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求，设置请求参数项目ID
        ListAllBranchesRequest request = new ListAllBranchesRequest();
        // 项目ID。注意,如下的 projectId 请使用CodeArts对应项目首页，点击某个项目链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/List
        String projectId = "<YOUR PROJECT ID>";
        request.withProjectUuid(projectId);
        try {
            ListAllBranchesResponse response = client.listAllBranches(request);
            logger.info("ListAllBranches: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListAllBranches connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListAllBranches RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListAllBranches ServiceResponseException", e);
        }
    }
}
```

### 返回结果示例
```
{
    status: success
    result: class ResultValueListTestVersionVo {
        total: null
        value: [class TestVersionVo {
            uri: vd0v0000vv33lt5r
            type: TestVersion
            author: f1ac****
            name: abc
            rank: null
            version: null
            owner: null
            creator: null
            iterations: null
            description: null
            region: cn-north-4
            lastModifier: f1ac****
            lastModified: 2024-10-22T14:40:12Z
            lastModifiedTimestamp: 1729579212000
            lastChangeTime: 2024-10-22T14:40:12Z
            versionUri: ad16****
            originUri: null
            parentUri: ad16****
            parentPath: /ad16****/
            creationVersionUri: ad16****
            creationDate: 2024-10-22T14:40:12Z
            creationDateTimestamp: 1729579212000
            authorName: Developer
            comment: null
            number: null
            isMaster: 0
            isIterator: 0
            planStartDate: null
            planEndDate: null
            serviceId: ad16****
            serviceName: Scrum-后端技术
            pbiId: null
            pbiName: null
            planId: null
            metricPbiIds: null
            metricPbiIdNames: null
            lastSynDate: null
            isClosed: null
            asynGit: null
            schemaNo: 3
            finishDate: null
            ownerName: null
            creatorName: null
            currentStage: null
            serviceTypes: null
            riskRating: 0
            riskDes: null
            projectUuid: ad16****
            domainId: null
            piId: null
        }]
        reason: null
        pageSize: null
        pageNo: null
        hasMore: null
    }
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-10-22 | 1.0 | 文档首次发布 |