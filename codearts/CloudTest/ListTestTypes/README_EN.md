### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CloudTest/doc?api=ListTestTypes).

2.In this example, you can get a list of test types.

3.This API is used to obtain the test type list of the current test service.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A project has been created on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-cloudtest. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cloudtest</artifactId>
    <version>3.1.118</version>
</dependency>

```

### Code example

``` java
package com.huawei.cloudtest;

import com.huaweicloud.sdk.cloudtest.v1.CloudtestClient;
import com.huaweicloud.sdk.cloudtest.v1.model.ListTestTypesRequest;
import com.huaweicloud.sdk.cloudtest.v1.model.ListTestTypesResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.cloudtest.v1.region.CloudtestRegion;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListTestTypes {

    private static final Logger logger = LoggerFactory.getLogger(ListTestTypes.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listTestTypesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listTestTypesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listTestTypesAk)
                .withSk(listTestTypesSk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CloudtestRegion.CN_NORTH_4.
        CloudtestClient client = CloudtestClient.newBuilder()
                .withCredential(auth)
                .withRegion(CloudtestRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();

        // Construct a request and set the request parameter project ID.
        ListTestTypesRequest request = new ListTestTypesRequest();
        // Project ID. For the following project ID, use the project ID on the homepage of CodeArts. Click a project link.
        // Example: https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/List
        String projectId = "<YOUR PROJECT ID>";
        request.withProjectId(projectId);
        try {
            ListTestTypesResponse response = client.listTestTypes(request);
            logger.info("ListTestTypes: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListTestTypes connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListTestTypes RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListTestTypes ServiceResponseException", e);
        }
    }
}
```

### Example of the returned result

#### ListTestTypes

```
{
 "status": "success",
 "result": {
  "value": [
   {
    "id": 1,
    "name": "Function Test"
   },
   {
    "id": 4,
    "name": "Performance Test"
   },
   {
    "id": 2,
    "name": "Compatibility Test"
   },
   {
    "id": 15,
    "name": "Usability Test"
   },
   {
    "id": 22,
    "name": "Reliability Test"
   },
   {
    "id": 14,
    "name": "Security Test"
   },
   {
    "id": 21,
    "name": "Serviceability Test"
   }
  ]
 }
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/10/22 | 1.0 | This document is released for the first time.