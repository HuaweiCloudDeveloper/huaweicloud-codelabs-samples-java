package com.huawei.cloudtest;

import com.huaweicloud.sdk.cloudtest.v1.CloudtestClient;
import com.huaweicloud.sdk.cloudtest.v1.model.ListTestTypesRequest;
import com.huaweicloud.sdk.cloudtest.v1.model.ListTestTypesResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.cloudtest.v1.region.CloudtestRegion;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListTestTypes {

    private static final Logger logger = LoggerFactory.getLogger(ListTestTypes.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listTestTypesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listTestTypesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(listTestTypesAk)
                .withSk(listTestTypesSk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CloudtestRegion.CN_NORTH_4.
        CloudtestClient client = CloudtestClient.newBuilder()
                .withCredential(auth)
                .withRegion(CloudtestRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();

        // Construct a request and set the request parameter project ID.
        ListTestTypesRequest request = new ListTestTypesRequest();
        // Project ID. For the following project ID, use the project ID on the homepage of CodeArts. Click a project link.
        // Example: https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/List
        String projectId = "<YOUR PROJECT ID>";
        request.withProjectId(projectId);
        try {
            ListTestTypesResponse response = client.listTestTypes(request);
            logger.info("ListTestTypes: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListTestTypes connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListTestTypes RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListTestTypes ServiceResponseException", e);
        }
    }
}