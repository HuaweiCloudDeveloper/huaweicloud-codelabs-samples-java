### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CloudTest/sdk?api=CreateService) 中直接运行调试该接口。
2.在本样例中，您可以实现新测试类型服务注册。
3.获取工作项，您可以根据新测试类型服务的注册，进行项目下创建计划等操作。

### 前置条件
1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
2.已具备开发环境 ，支持Java JDK 1.8及其以上版本。
3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
4.已在CodeArts平台创建项目。
5.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-cloudtest”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-cloudtest</artifactId>
    <version>3.1.110</version>
</dependency>
```

### 代码示例
``` java
public class CreateService {

    private static final Logger logger = LoggerFactory.getLogger(CreateService.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CloudtestRegion.CN_NORTH_4
        CloudtestClient client = CloudtestClient.newBuilder()
                .withCredential(auth)
                .withRegion(CloudtestRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头，设置请求参数被测试服务域名，测试类型名称
        CreateServiceRequest request = new CreateServiceRequest();
        ServiceRequestBody body = new ServiceRequestBody();
        // server_host是由用户提供的域名。 即需要被测试的服务的域名。我们会通过此域名进行接口调用,请以https/http开头,长度小于等于128位字符。
        // TestHub将会通过此域名下的接口,保证服务数据与用户系统数据的一致性。
        String serverHost = "<YOUR SERVER HOST>";
        body.withServerHost(serverHost);
        // 测试类型名称,用于界面显示,不能使用当前保留名,长度小于等于16位字符
        String serviceName = "<YOUR SERVICE NAME>";
        body.withServiceName(serviceName);
        request.withBody(body);
        try {
            CreateServiceResponse response = client.createService(request);
            logger.info("CreateService: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("CreateService connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("CreateService RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("CreateService ServiceResponseException", e);
        }
    }
}
```

### 返回结果示例
```
{
 "service_name": "testhw11",
 "service_id": 55
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-08-16 | 1.0 | 文档首次发布 |