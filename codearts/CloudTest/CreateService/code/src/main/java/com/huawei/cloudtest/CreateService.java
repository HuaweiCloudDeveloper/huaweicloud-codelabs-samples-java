package com.huawei.cloudtest;

import com.huaweicloud.sdk.cloudtest.v1.CloudtestClient;
import com.huaweicloud.sdk.cloudtest.v1.model.CreateServiceRequest;
import com.huaweicloud.sdk.cloudtest.v1.model.CreateServiceResponse;
import com.huaweicloud.sdk.cloudtest.v1.model.ServiceRequestBody;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.cloudtest.v1.region.CloudtestRegion;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateService {

    private static final Logger logger = LoggerFactory.getLogger(CreateService.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CloudtestRegion.CN_NORTH_4
        CloudtestClient client = CloudtestClient.newBuilder()
                .withCredential(auth)
                .withRegion(CloudtestRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头，设置请求参数被测试服务域名，测试类型名称
        CreateServiceRequest request = new CreateServiceRequest();
        ServiceRequestBody body = new ServiceRequestBody();
        // server_host是由用户提供的域名。 即需要被测试的服务的域名。我们会通过此域名进行接口调用,请以https/http开头,长度小于等于128位字符。
        // TestHub将会通过此域名下的接口,保证服务数据与用户系统数据的一致性。
        String serverHost = "<YOUR SERVER HOST>";
        body.withServerHost(serverHost);
        // 测试类型名称,用于界面显示,不能使用当前保留名,长度小于等于16位字符
        String serviceName = "<YOUR SERVICE NAME>";
        body.withServiceName(serviceName);
        request.withBody(body);
        try {
            CreateServiceResponse response = client.createService(request);
            logger.info("CreateService: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("CreateService connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("CreateService RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("CreateService ServiceResponseException", e);
        }
    }
}