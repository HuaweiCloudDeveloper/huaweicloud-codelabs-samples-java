package com.huawei.cloudtest;

import com.huaweicloud.sdk.cloudtest.v1.CloudtestClient;
import com.huaweicloud.sdk.cloudtest.v1.model.ShowRegisterServiceRequest;
import com.huaweicloud.sdk.cloudtest.v1.model.ShowRegisterServiceResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.cloudtest.v1.region.CloudtestRegion;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowRegisterService {

    private static final Logger logger = LoggerFactory.getLogger(ShowRegisterService.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK
        String showRegisterServiceAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showRegisterServiceSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(showRegisterServiceAk)
                .withSk(showRegisterServiceSk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CloudtestRegion.CN_NORTH_4
        CloudtestClient client = CloudtestClient.newBuilder()
                .withCredential(auth)
                .withRegion(CloudtestRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求头
        ShowRegisterServiceRequest request = new ShowRegisterServiceRequest();
        try {
            // 获取自己当前已经注册的服务
            ShowRegisterServiceResponse response = client.showRegisterService(request);
            logger.info("ShowRegisterService: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ShowRegisterService connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ShowRegisterService RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ShowRegisterService ServiceResponseException", e);
        }
    }
}