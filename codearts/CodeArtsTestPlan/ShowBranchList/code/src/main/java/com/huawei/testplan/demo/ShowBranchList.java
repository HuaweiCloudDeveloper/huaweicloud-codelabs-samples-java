package com.huawei.testplan.demo;

import com.huaweicloud.sdk.cloudtest.v1.CloudtestClient;
import com.huaweicloud.sdk.cloudtest.v1.model.ListBranchesRequest;
import com.huaweicloud.sdk.cloudtest.v1.model.ListBranchesResponse;
import com.huaweicloud.sdk.cloudtest.v1.region.CloudtestRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

public class ShowBranchList {
    public static void main(String[] args)
        throws Exception {

        // Directly writing AK/SK in code is risky. For security,
        // encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication.
        // Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // Creating a Service Client
        CloudtestClient client = CloudtestClient.newBuilder()
            .withCredential(auth)
            .withRegion(CloudtestRegion.valueOf("cn-north-4"))
            .build();

        // Send a request and get a response
        ListBranchesRequest request = new ListBranchesRequest();
        request.setProjectId("{*** project id ***}");
        // Set paging offset
        request.setOffset(0);
        // Set paging limit
        request.setLimit(10);
        try {
            ListBranchesResponse response = client.listBranches(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException exception) {
            // solve exception
            System.out.println(exception.getMessage());
            throw exception;
        } catch (ServiceResponseException exception) {
            System.out.println(exception.getHttpStatusCode());
            System.out.println(exception.getRequestId());
            System.out.println(exception.getErrorCode());
            System.out.println(exception.getErrorMsg());
            // solve exception
            throw exception;
        }
    }
}