
## 1、功能介绍
此示例通过CloudTest-SDK进行项目分支信息查询。展示如何根据项目id，调用具体的sdk方法查询项目TestPlan分支列表信息。

## 2、前置条件
要使用华为云CloudTest JAVA SDK访问指定服务的 API ，
您需要确认已在 华为云控制台 开通当前服务，开通地址：https://www.huaweicloud.com/product/cloudtest.html
CloudTest Java SDK 支持 Java JDK 1.8 及其以上版本。


## 3、SDK的获取与安装
推荐您通过 Maven 安装依赖的方式安装CloudTest Java SDK：

首先您需要在您的操作系统中下载并安装Maven，安装完成后您只需在
Maven项目的pom.xml文件加入相应的依赖项即可。
指定依赖时请选择特定的版本号，否则可能会在构建时导致不可预见的问题。
独立服务包：
根据需要引入SDK依赖包 。

``` 
<dependency>
  <groupId>com.huaweicloud.sdk</groupId>
  <artifactId>huaweicloud-sdk-cloudtest</artifactId>
  <version>3.1.74</version>
</dependency>
``` 


## 4、示例代码
``` java
package com.huawei.testplan.demo;

import com.huaweicloud.sdk.cloudtest.v1.CloudtestClient;
import com.huaweicloud.sdk.cloudtest.v1.model.ListBranchesRequest;
import com.huaweicloud.sdk.cloudtest.v1.model.ListBranchesResponse;
import com.huaweicloud.sdk.cloudtest.v1.region.CloudtestRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

public class ShowBranchList {
    public static void main(String[] args)
        throws Exception {

        // Directly writing AK/SK in code is risky. For security,
        // encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication.
        // Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // Creating a Service Client
        CloudtestClient client = CloudtestClient.newBuilder()
            .withCredential(auth)
            .withRegion(CloudtestRegion.valueOf("cn-north-4"))
            .build();

        // Send a request and get a response
        ListBranchesRequest request = new ListBranchesRequest();
        request.setProjectId("{*** 测试计划项目id ***}");
        // Set paging offset
        request.setOffset(0);
        // Set paging limit
        request.setLimit(10);
        try {
            ListBranchesResponse response = client.listBranches(request);
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            throw new Exception();
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
            throw new Exception();
        }
    }
}


```
您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CloudTest/doc?api=ListBranches) 中直接运行调试该接口。
 
### 修订记录
 
 发布日期  | 文档版本 | 修订说明
 :----: | :-----: | :------:  
 2023/12/27 |1.0 | 本文档为首次发布。                  