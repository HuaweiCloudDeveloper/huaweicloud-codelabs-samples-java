
## 1、Function Description
**What is MAS-SDK?？**

The MAS JAVA SDK allows you to quickly use the mas service without paying attention to details.

**What You'll Learn？**

How Do I Use MAS-SDK to Query Namespaces Based on Specified Conditions?

## 2、Prerequisites
To use HUAWEI CLOUD MAS Java SDK to access APIs of a specified service,
You have enabled the service on the HUAWEI CLOUD console at https://www.huaweicloud.com/product/mas.html.
The MAS Java SDK supports Java JDK 1.8 or later.

## 3、Obtaining and Installing the SDK
You are advised to install the MAS Java SDK in Maven installation dependency mode.

First, you need to download and install Maven in your operating system. After the installation is complete, you only need to install Maven in the
Add dependencies to the pom.xml file of the Maven project.
Select a specific version number when specifying dependencies. Otherwise, unexpected problems may occur during build.
Independent service package:
Import the SDK dependency package as required.
``` 
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-mas</artifactId>
    <version>3.1.64</version>
</dependency>
``` 
## 4、Sample Code
``` 
package com.huawei.codeartssdk;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.mas.v1.MasClient;
import com.huaweicloud.sdk.mas.v1.model.ShowNameSpaceListRequest;
import com.huaweicloud.sdk.mas.v1.model.ShowNameSpaceListResponse;
import com.huaweicloud.sdk.mas.v1.region.MasRegion;

public class ShowNameSpaceListByType {
    public static void main(String[] args) {
        // Basic authentication information:
        // ak: access key of the HUAWEI CLOUD account
        // SK: secret access key of the HUAWEI CLOUD account
        // The AK and SK used for authentication are directly written to the code, which has great security risks. 
        // It is recommended that the AK and SK be stored in ciphertext in the configuration file or environment variable and decrypted during use to ensure security.
        // In this example, AK and SK are stored in environment variables for authentication. 
        // Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // enterpriseProjectId:enterprise Project id
        String enterpriseProjectId = "{******enterprise project id to be list******}";

        // 1.Initializing the SDK
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // 2.Creating a MasClient Instance
        MasClient client =
            MasClient.newBuilder().withCredential(auth).withRegion(MasRegion.valueOf("cn-north-4")).build();

        // 3.Create a request and return a response.
        ShowNameSpaceListRequest request = new ShowNameSpaceListRequest();
        try {
            ShowNameSpaceListResponse response = client.showNameSpaceList(request.withEnterpriseProjectId(enterpriseProjectId));
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        }
    }
}
```


## 5. Example of Running Result
``` json 
[
    {
        "id":"41feadfsasdfdsa",
        "name":"namespace001",
        "type":1,
        "description":"Test01",
        "tenant_id":"eardfadsf",
        "user_id":"user001",
        "project_id":"0945132",
        "enterprise_project_id":"0",
        "modules":[
            "mysql"
        ],
        "service_module_id":"",
        "created_date":"2023-12-08 16:24:00",
        "updated_date":null,
        "deleted_date":null,
        "region_id":"cn-north-",
        "multi_active_zone":[]
    }
]
```
## 6. Change History

| Release Date | Document Version | Revision Description |
|:------------:| :------: | :----------: |
|  2023-12-22  | 1.0 | This document is released for the first time. |

