package com.huawei.codeartssdk;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.mas.v1.MasClient;
import com.huaweicloud.sdk.mas.v1.model.ShowNameSpaceListRequest;
import com.huaweicloud.sdk.mas.v1.model.ShowNameSpaceListResponse;
import com.huaweicloud.sdk.mas.v1.region.MasRegion;

public class ShowNameSpaceListByType {
    public static void main(String[] args) {
        // 基础认证信息：
        // ak: 华为云账号Access Key
        // sk: 华为云账号Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // namespaceType:命名空间类型
        String namespaceType = "{******Namespace type to be list******}";

        // 1.初始化sdk
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // 2.创建MasClient实例
        MasClient client =
            MasClient.newBuilder().withCredential(auth).withRegion(MasRegion.valueOf("cn-north-4")).build();

        // 3.创建请求并返回响应
        ShowNameSpaceListRequest request = new ShowNameSpaceListRequest();
        try {
            ShowNameSpaceListResponse response = client.showNameSpaceList(request.withType(namespaceType));
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        }
    }
}