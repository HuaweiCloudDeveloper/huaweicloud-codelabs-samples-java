
## 1、功能介绍
**什么是 MAS-SDK ？**

MAS JAVA SDK让你无需关心细节即可快速使用mas服务。

**您将学到什么？**

如何通过MAS-SDK进行指定条件的命名空间查询

## 2、前置条件
要使用华为云MAS JAVA SDK访问指定服务的 API ，
您需要确认已在 华为云控制台 开通当前服务，开通地址：https://www.huaweicloud.com/product/mas.html
 MAS Java SDK 支持 Java JDK 1.8 及其以上版本。


## 3、SDK的获取与安装
推荐您通过 Maven 安装依赖的方式安装MAS Java SDK：

首先您需要在您的操作系统中下载并安装Maven，安装完成后您只需在
Maven项目的pom.xml文件加入相应的依赖项即可。
指定依赖时请选择特定的版本号，否则可能会在构建时导致不可预见的问题。
独立服务包：
根据需要引入SDK依赖包 。

``` 
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-mas</artifactId>
    <version>3.1.64</version>
</dependency>
``` 


## 4、示例代码
``` 
package com.huawei.codeartssdk;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.mas.v1.MasClient;
import com.huaweicloud.sdk.mas.v1.model.ShowNameSpaceListRequest;
import com.huaweicloud.sdk.mas.v1.model.ShowNameSpaceListResponse;
import com.huaweicloud.sdk.mas.v1.region.MasRegion;

public class ShowNameSpaceListByType {
    public static void main(String[] args) {
        // 基础认证信息：
        // ak: 华为云账号Access Key
        // sk: 华为云账号Secret Access Key
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        // namespaceType:命名空间类型
        String namespaceType = "{******Namespace type to be list******}";

        // 1.初始化sdk
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // 2.创建MasClient实例
        MasClient client =
            MasClient.newBuilder().withCredential(auth).withRegion(MasRegion.valueOf("cn-north-4")).build();

        // 3.创建请求并返回响应
        ShowNameSpaceListRequest request = new ShowNameSpaceListRequest();
        try {
            ShowNameSpaceListResponse response = client.showNameSpaceList(request.withType(namespaceType));
            System.out.println(response.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        }
    }
}

```
## 5. 运行结果示例
``` json 
[
    {
        "id":"fdsf***dfss",
        "name":"namespace001",
        "type":1,
        "description":"Test01",
        "tenant_id":"*****fdsaff",
        "user_id":"sfdasa*****",
        "project_id":"dsfafasd*8dsfoo",
        "enterprise_project_id":"0",
        "modules":[
            "mysql"
        ],
        "service_module_id":"",
        "created_date":"2023-12-08 16:24:00",
        "updated_date":null,
        "deleted_date":null,
        "region_id":"cn-north-4",
        "multi_active_zone":[

        ]
    },
    {
        "id":"99dsaf**",
        "name":"002",
        "type":1,
        "description":"DSL_Test_Same_City1111456",
        "tenant_id":"99932432fdsgds****",
        "user_id":"dfsgdfgdfs**fdgsg",
        "project_id":"sdsfa***55",
        "enterprise_project_id":"1",
        "modules":[
            "mysql"
        ],
        "service_module_id":"",
        "created_date":"2023-12-08 10:57:56",
        "updated_date":null,
        "deleted_date":null,
        "region_id":"cn-north-",
        "multi_active_zone":[
            {
                "id":"dfgsg***dsfa",
                "name":"203",
                "type":1,
                "region":"cn-north-7",
                "description":"",
                "spec":{
                    "type":"HUAWEICLOUD",
                    "credentials_id":null,
                    "project_id":"sdsfa***55"
                },
                "region_name":"dsfaf",
                "is_master":true,
                "available_zone":[
                    "cn-north-7a",
                    "cn-north-7b",
                    "cn-north-7c"
                ],
                "namespace_id":"755a7bff-**8dsfafa",
                "created_date":"2023-12-08 10:57:56",
                "updated_date":null
            }
        ]
    }
]
```
## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-12-20 | 1.0 | 文档首次发布 |

