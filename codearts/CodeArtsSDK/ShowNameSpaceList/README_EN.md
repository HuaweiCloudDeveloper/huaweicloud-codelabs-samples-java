
## 1、Function Description
**What is MAS-SDK?？**

The MAS JAVA SDK allows you to quickly use the mas service without paying attention to details.

**What You'll Learn？**

How Do I Query the Namespace List Using the MAS-SDK?

## 2、Prerequisites
To use HUAWEI CLOUD MAS Java SDK to access APIs of a specified service,
You have enabled the service on the HUAWEI CLOUD console at https://www.huaweicloud.com/product/mas.html.
The MAS Java SDK supports Java JDK 1.8 or later.

## 3、Obtaining and Installing the SDK
You are advised to install the MAS Java SDK in Maven installation dependency mode.

First, you need to download and install Maven in your operating system. After the installation is complete, you only need to install Maven in the
Add dependencies to the pom.xml file of the Maven project.
Select a specific version number when specifying dependencies. Otherwise, unexpected problems may occur during build.
Independent service package:
Import the SDK dependency package as required.
``` 
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-mas</artifactId>
    <version>3.1.64</version>
</dependency>
``` 
## 4、Sample Code
``` 
//import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.mas.v1.MasClient;
import com.huaweicloud.sdk.mas.v1.model.ShowNameSpaceListRequest;
import com.huaweicloud.sdk.mas.v1.model.ShowNameSpaceListResponse;
import com.huaweicloud.sdk.mas.v1.region.MasRegion;

public class ShowNameSpaceList {
    public static void main(String[] args) {
    
        // Directly writing AK/SK in code is risky. For security,
        // encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication.
        // Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // Creating Service Client
        MasClient client=MasClient.newBuilder().withCredential(auth).withRegion(MasRegion.valueOf("cn-north-4")).build();

        // Send a request and get a response
        ShowNameSpaceListRequest request=new ShowNameSpaceListRequest();
        try {
            ShowNameSpaceListResponse response=client.showNameSpaceList(request);
            System.out.println(response.toString());
        }catch (ConnectionException | RequestTimeoutException e){
            e.printStackTrace();
        }catch (ServiceResponseException e){
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}


```


## 5. Example of Running Result
class ShowNameSpaceListResponse {
body: null
}
## 6. References


## 7. Change History

|   Release Date    | Document Version | Revision Description |
|:---------:| :------: | :----------: |
| 2023-11-9 | 1.0 | This document is released for the first time. |

