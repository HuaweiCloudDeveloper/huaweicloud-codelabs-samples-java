
## 1、功能介绍
**什么是 MAS-SDK ？**

MAS JAVA SDK让你无需关心细节即可快速使用mas服务。

**您将学到什么？**

如何通过MAS-SDK进行命名空间列表查询

## 2、前置条件
要使用华为云MAS JAVA SDK访问指定服务的 API ，
您需要确认已在 华为云控制台 开通当前服务，开通地址：https://www.huaweicloud.com/product/mas.html
 MAS Java SDK 支持 Java JDK 1.8 及其以上版本。


## 3、SDK的获取与安装
推荐您通过 Maven 安装依赖的方式安装MAS Java SDK：

首先您需要在您的操作系统中下载并安装Maven，安装完成后您只需在
Maven项目的pom.xml文件加入相应的依赖项即可。
指定依赖时请选择特定的版本号，否则可能会在构建时导致不可预见的问题。
独立服务包：
根据需要引入SDK依赖包 。

``` 
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-mas</artifactId>
    <version>3.1.64</version>
</dependency>
``` 


## 4、示例代码
``` 
//import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.mas.v1.MasClient;
import com.huaweicloud.sdk.mas.v1.model.ShowNameSpaceListRequest;
import com.huaweicloud.sdk.mas.v1.model.ShowNameSpaceListResponse;
import com.huaweicloud.sdk.mas.v1.region.MasRegion;

public class ShowNameSpaceList {
    public static void main(String[] args) {
        
        // Directly writing AK/SK in code is risky. For security,
        // encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication.
        // Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        // Creating a Service Client
        MasClient client=MasClient.newBuilder().withCredential(auth).withRegion(MasRegion.valueOf("cn-north-4")).build();

        // Send a request and get a response
        ShowNameSpaceListRequest request=new ShowNameSpaceListRequest();
        try {
            ShowNameSpaceListResponse response=client.showNameSpaceList(request);
            System.out.println(response.toString());
        }catch (ConnectionException | RequestTimeoutException e){
            e.printStackTrace();
        }catch (ServiceResponseException e){
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getRequestId());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}


```
## 5. 运行结果示例
class ShowNameSpaceListResponse {
body: null
}
## 6. 参考


## 7. 修订记录

|   发布日期    | 文档版本 | 修订说明 |
|:---------:| :------: | :----------: |
| 2023-11-9 | 1.0 | 文档首次发布 |

