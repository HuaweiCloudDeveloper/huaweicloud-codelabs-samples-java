### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeArtsDeploy/doc?api=DeleteHost).

2.In this example, you can query the host list.

3.You can use the API to query the host list to customize the host list.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.You have created a project, created a host cluster, and added hosts on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-codeartsdeploy. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartsdeploy</artifactId>
    <version>3.1.113</version>
</dependency>

```

### Code example

Query the host list.

``` java
package com.huawei.codeartsdeploy.demo;

import com.huaweicloud.sdk.codeartsdeploy.v2.CodeArtsDeployClient;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.ListNewHostsRequest;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.ListNewHostsResponse;
import com.huaweicloud.sdk.codeartsdeploy.v2.region.CodeArtsDeployRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListNewHosts {

    private static final Logger logger = LoggerFactory.getLogger(ListNewHosts.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Initialize the SDK and enter the authentication information and site information.
        BasicCredentials credentials = new BasicCredentials().withAk(ak).withSk(sk);
        CodeArtsDeployClient codeArtsDeployClient = CodeArtsDeployClient.newBuilder()
            .withCredential(credentials)
            .withRegion(CodeArtsDeployRegion.CN_NORTH_4)
            .build();
        // 2. Assemble the request body.
        ListNewHostsRequest request = new ListNewHostsRequest();
        // Obtain the group ID on the basic resource management host group details page of the project.
        // Example: https://devcloud.cn-north-4.huaweicloud.com/deploymentgroup/project/{projectId}/config/cn-north-4/manager/hostgroup/list?group_id={groupId}&action=detail
        request.withGroupId("groupId");
        // 3. Initiate an HTTP API request and handle the exception.
        try {
            ListNewHostsResponse response = codeArtsDeployClient.listNewHosts(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}

```

### Example of the returned result

#### ListNewHosts

```
{
 "status": "success",
 "result": [
  {
   "uuid": "7800b91f4c4940b7bd198********",
   "ip": "172.1*******",
   "os": null,
   "port": 22,
   "authorization": {
    "username": "ro*****",
    "password": null,
    "private_key": null,
    "trusted_type": 0
   },
   "permission": {
    "can_view": true,
    "can_edit": true,
    "can_delete": true,
    "can_add_host": true,
    "can_copy": true
   },
   "host_name": "172.16******-120.46******",
   "as_proxy": false,
   "group_id": "8acc4e676c404dd28***********",
   "proxy_host_id": "f8f40cdfe494***********",
   "owner_id": "49d9a304ab704e178***********",
   "owner_name": "*****",
   "proxy_host": {
    "uuid": "f8f40cdfe4944d04a4c2***********",
    "ip": "12*******",
    "os": null,
    "port": 22,
    "authorization": {
     "username": "*****",
     "password": null,
     "private_key": null,
     "trusted_type": 0
    },
    "permission": {
     "can_view": true,
     "can_edit": true,
     "can_delete": true,
     "can_add_host": true,
     "can_copy": true
    },
    "host_name": "122.***********",
    "as_proxy": true,
    "group_id": "8acc4e676c404dd285*******",
    "proxy_host_id": "",
    "owner_id": "49d9a304ab704e1782*******",
    "owner_name": "l*****",
    "proxy_host": null,
    "connection_status": "success",
    "create_time": "2024-04-01 09:05:31",
    "lastest_connection_time": "2024-04-01 09:05:38",
    "connection_result": "连接成功",
    "nick_name": "*****",
    "import_status": null,
    "env_count": null
   },
   "connection_status": "success",
   "create_time": "2024-04-01 09:06:34",
   "lastest_connection_time": "2024-04-01 09:06:42",
   "connection_result": "连接成功",
   "nick_name": "*****",
   "import_status": null,
   "env_count": 0
  }
 ],
 "total": 1
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/09/26 | 1.0 | This document is released for the first time.