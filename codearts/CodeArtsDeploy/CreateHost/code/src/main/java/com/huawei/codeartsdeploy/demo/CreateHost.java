/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.codeartsdeploy.demo;

import com.huaweicloud.sdk.codeartsdeploy.v2.CodeArtsDeployClient;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.CreateHostRequest;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.CreateHostRequestBody;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.CreateHostRequestBody.OsEnum;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.CreateHostResponse;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.HostAuthorizationBody;
import com.huaweicloud.sdk.codeartsdeploy.v2.region.CodeArtsDeployRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateHost {

    private static final Logger logger = LoggerFactory.getLogger(CreateHost.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "<YOUR IAM PROJECT ID>";
        // 1. Initialize the SDK and enter the authentication information and site information.
        BasicCredentials credentials = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
        CodeArtsDeployClient codeArtsDeployClient = CodeArtsDeployClient.newBuilder()
            .withCredential(credentials)
            .withRegion(CodeArtsDeployRegion.CN_NORTH_4)
            .build();

        // 2. Assemble the request body.
        CreateHostRequest request = new CreateHostRequest();
        request.setGroupId("{*** host cluster id ***}");

        CreateHostRequestBody host = new CreateHostRequestBody();
        host.setHostName("{*** host name ***}");
        host.setIp("{*** host ip ***}");
        // ssh port
        host.setPort(22);
        // operating system: windows or linux, which must be the same as that of the host cluster.
        host.setOs(OsEnum.valueOf("linux"));
        // whether a proxy host is used.
        host.setAsProxy(false);

        // log in to the host for authentication by password or key.
        HostAuthorizationBody authBody = new HostAuthorizationBody();
        authBody.setUsername("{*** username ***}");
        authBody.setPassword("{*** pwd ***}");
        // Authentication type. 0 indicates password authentication, and 1 indicates key authentication.
        authBody.setTrustedType(HostAuthorizationBody.TrustedTypeEnum.NUMBER_0);
        host.setAuthorization(authBody);

        // whether install ICAgent
        host.setInstallIcagent(Boolean.TRUE);

        request.withBody(host);

        // 3. Initiate an HTTP API request and handle the exception.
        try {
            CreateHostResponse response = codeArtsDeployClient.createHost(request);
            logger.info("Create Host success." + response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }
}