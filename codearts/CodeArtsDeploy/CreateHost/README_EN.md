### Version description
In this example, the SDK version is 3.1.58 or later.

### Function overview
This example shows how to add a host to a host cluster.

### Prerequisites
1.You have [registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%3Fservice%3Dhttps%253A%252F%252Fwww.huaweicloud.com%252Fintl%252Fen-us%252F%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin%3Fservice%3Dhttps%253A%252F%252Fwww.huaweicloud.com%252Fintl%252Fen-us%252F&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=d9febbf632b94a27a0241cbfcf6bc831&lang=en-us) with Huawei Cloud, and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth).

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.You have created a project on CodeArts.

### Code example
The following code shows how to use the CodeArts Deploy SDK to add a host:
``` java

package com.huawei.codeartsdeploy.demo;
 
import com.huaweicloud.sdk.codeartsdeploy.v2.CodeArtsDeployClient;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.CreateDeploymentHostRequest;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.CreateDeploymentHostResponse;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.DeploymentHost;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.DeploymentHostAuthorizationBody;
import com.huaweicloud.sdk.codeartsdeploy.v2.region.CodeArtsDeployRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 
public class CreateHost {
    private static final Logger logger = LoggerFactory.getLogger(CreateHost.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "<YOUR IAM PROJECT ID>";
        // 1. Initialize the SDK and enter the authentication information and site information.
        BasicCredentials credentials = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
        CodeArtsDeployClient codeArtsDeployClient = CodeArtsDeployClient.newBuilder()
            .withCredential(credentials)
            .withRegion(CodeArtsDeployRegion.CN_NORTH_4)
            .build();

        // 2. Assemble the request body.
        CreateHostRequest request = new CreateHostRequest();
        request.setGroupId("{*** host cluster id ***}");

        CreateHostRequestBody host = new CreateHostRequestBody();
        host.setHostName("{*** host name ***}");
        host.setIp("{*** host ip ***}");
        // ssh port
        host.setPort(22);
        // operating system: windows or linux, which must be the same as that of the host cluster.
        host.setOs(OsEnum.valueOf("linux"));
        // whether a proxy host is used.
        host.setAsProxy(false);

        // log in to the host for authentication by password or key.
        HostAuthorizationBody authBody = new HostAuthorizationBody();
        authBody.setUsername("{*** username ***}");
        authBody.setPassword("{*** password ***}");
        // Authentication type. 0 indicates password authentication, and 1 indicates key authentication.
        authBody.setTrustedType(HostAuthorizationBody.TrustedTypeEnum.NUMBER_0);
        host.setAuthorization(authBody);

        // whether install ICAgent
        host.setInstallIcagent(Boolean.TRUE);

        request.withBody(host);

        // 3. Initiate an HTTP API request and handle the exception.
        try {
            CreateHostResponse response = codeArtsDeployClient.createHost(request);
            logger.info("Create Host success." + response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }
}


```
You can directly run and debug the API in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeArtsDeploy/doc?api=CreateHost).


### Change History

 Released On  | Issue | Description
 :----: | :-----: | :------:  
 2023/9/13 |2.0 | Service Rename Amendment
