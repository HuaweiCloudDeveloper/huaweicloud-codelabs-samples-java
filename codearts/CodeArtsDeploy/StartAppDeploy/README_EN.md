### Version description
In this example, the SDK version is 3.1.58 or later.

### Function overview
This example shows how to deploy an application.

### Prerequisites
1.You have [registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%3Fservice%3Dhttps%253A%252F%252Fwww.huaweicloud.com%252Fintl%252Fen-us%252F%23&casLoginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin%3Fservice%3Dhttps%253A%252F%252Fwww.huaweicloud.com%252Fintl%252Fen-us%252F&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=d9febbf632b94a27a0241cbfcf6bc831&lang=en-us) with Huawei Cloud, and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth).

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.You have created a project on CodeArts.

### Code example
The following code shows how to use the CodeArts Deploy SDK to deploy an application:
``` java

package com.huawei.codeartsdeploy.demo;

import com.huaweicloud.sdk.codeartsdeploy.v2.CodeArtsDeployClient;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.EnvExecutionBody;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.StartDeployTaskRequest;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.StartDeployTaskResponse;
import com.huaweicloud.sdk.codeartsdeploy.v2.region.CodeArtsDeployRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StartAppDeploy {
    private static final Logger logger = LoggerFactory.getLogger(StartAppDeploy.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "<YOUR IAM PROJECT ID>";
        BasicCredentials credentials = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
        // 1. Initialize the SDK and enter the authentication information and site information.
        CodeArtsDeployClient codeArtsDeployClient = CodeArtsDeployClient.newBuilder()
            .withCredential(credentials)
            .withRegion(CodeArtsDeployRegion.CN_NORTH_4)
            .build();

        // 2. Assemble the request body.
        StartDeployTaskRequest request = new StartDeployTaskRequest();
        request.setTaskId("{*** task Id ***}");

        // assign parameters if needed
        EnvExecutionBody body = new EnvExecutionBody();
        request.setBody(body);

        // 3. Initiate an HTTP API request and handle the exception.
        try {
            StartDeployTaskResponse response = codeArtsDeployClient.startDeployTask(request);
            logger.info("execute success!" + response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }
}

```
You can directly run and debug the API in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeArtsDeploy/doc?api=StartDeployTask).

### Change History

 Released On | Issue | Description
 :----------:|:-----:| :------:  
 2023/9/13   |  2.0  | Service Rename Amendment
