/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.codeartsdeploy.demo;

import com.huaweicloud.sdk.codeartsdeploy.v2.CodeArtsDeployClient;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.CreateHostClusterRequest;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.CreateHostClusterRequestBody;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.CreateHostClusterRequestBody.OsEnum;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.CreateHostClusterResponse;
import com.huaweicloud.sdk.codeartsdeploy.v2.region.CodeArtsDeployRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateHostCluster {

    private static final Logger logger = LoggerFactory.getLogger(CreateHostCluster.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "<YOUR IAM PROJECT ID>";
        // 1. Initialize the SDK and enter the authentication information and site information.
        BasicCredentials credentials = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
        CodeArtsDeployClient codeArtsDeployClient = CodeArtsDeployClient.newBuilder()
            .withCredential(credentials)
            .withRegion(CodeArtsDeployRegion.CN_NORTH_4)
            .build();

        // 2. Assemble the request body.
        CreateHostClusterRequest request = new CreateHostClusterRequest();
        CreateHostClusterRequestBody deploymentGroup = new CreateHostClusterRequestBody()
            .withName("{*** host cluster name ***}")
            .withProjectId("{*** CodeArts project Id ***}")
            // Operating system
            .withOs(OsEnum.LINUX)
            .withDescription("{*** host cluster description ***}")
            // Whether the host cluster is in poxy access mode. 1: yes; 0: no.
            .withIsProxyMode(0);
        request.setBody(deploymentGroup);

        // 3. Initiate an HTTP API request and handle the exception.
        try {
            CreateHostClusterResponse response = codeArtsDeployClient.createHostCluster(request);
            logger.info("Create Host cluster success." + response.toString());
        } catch (ClientRequestException e) {
            logger.error("Create Host cluster request error, staus: " + String.valueOf(e.getHttpStatusCode()));
            logger.error(" error message: " + e.toString());
        } catch (ServerResponseException e) {
            logger.error("Create Host cluster returns error, staus: " + String.valueOf(e.getHttpStatusCode()));
            logger.error(" error message: " + e.getMessage());
        }
    }
}

