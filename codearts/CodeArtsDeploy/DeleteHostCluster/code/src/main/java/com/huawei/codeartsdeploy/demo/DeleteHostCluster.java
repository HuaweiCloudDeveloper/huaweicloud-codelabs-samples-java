package com.huawei.codeartsdeploy.demo;

import com.huaweicloud.sdk.codeartsdeploy.v2.CodeArtsDeployClient;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.DeleteHostClusterRequest;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.DeleteHostClusterResponse;
import com.huaweicloud.sdk.codeartsdeploy.v2.region.CodeArtsDeployRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteHostCluster {

    private static final Logger logger = LoggerFactory.getLogger(DeleteHostCluster.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Obtain the group ID on the basic resource management host group details page of the project.
        // Example: https://devcloud.cn-north-4.huaweicloud.com/deploymentgroup/project/{projectId}/config/cn-north-4/manager/hostgroup/list?group_id={groupId}&action=detail
        String groupId = "<YOUR GROUP ID>";
        // Initialize the SDK and enter the authentication information and site information.
        BasicCredentials credentials = new BasicCredentials().withAk(ak).withSk(sk);
        CodeArtsDeployClient codeArtsDeployClient = CodeArtsDeployClient.newBuilder()
            .withCredential(credentials)
            .withRegion(CodeArtsDeployRegion.CN_NORTH_4)
            .build();
        // 2. Assemble the request body.
        DeleteHostClusterRequest request = new DeleteHostClusterRequest();
        request.withGroupId(groupId);
        // 3. Initiate an HTTP API request and handle the exception.
        try {
            DeleteHostClusterResponse response = codeArtsDeployClient.deleteHostCluster(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}

