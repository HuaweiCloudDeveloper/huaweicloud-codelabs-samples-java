### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsDeploy/doc?api=ListDeployTaskHistoryByDate) 中直接运行调试该接口。

2.在本样例中，您可以根据开始时间和结束时间查询项目下指定应用的历史部署记录列表

3.可以查询某个部署任务在某段时间内的所有部署记录，查询部署的状态、部署的用时、部署的操作人，以及部署记录总数等等信息。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartsdeploy”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartsdeploy</artifactId>
    <version>3.1.109</version>
</dependency>
```

### 代码示例
以下代码展示如何根据开始时间和结束时间查询项目下指定应用的历史部署记录列表：
``` java
package com.huawei.codeartsdeploy.demo;

import com.huaweicloud.sdk.codeartsdeploy.v2.CodeArtsDeployClient;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.ListDeployTaskHistoryByDateRequest;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.ListDeployTaskHistoryByDateResponse;
import com.huaweicloud.sdk.codeartsdeploy.v2.region.CodeArtsDeployRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListDeployTaskHistoryByDate {

    private static final Logger logger = LoggerFactory.getLogger(ListDeployTaskHistoryByDate.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 注意，如下的 projectId 请使用CodeArts对应项目首页链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/list
        String projectId = "<YOUR PROJECT ID>";
        // 注意，如下的 regionId 请使用项目所在的局点，例如华北-北京四区域为：cn-north-4
        String regionId = "<YOUR REGION ID>";
        // 注意，如下的 taskId 为部署任务id，请使用某个部署任务详细信息页面链接中的taskId参数的值
        // 例:https://devcloud.cn-north-4.huaweicloud.com/deployman/project/{projectId}/deployman/xxxxxx/view/history?origin=project&taskId={taskId}
        String taskId = "<YOUR TASK ID>";
        // 配置认证信息
        ICredential listDeployTaskHistoryByDateAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // 创建服务客户端并配置对应region
        CodeArtsDeployClient client = CodeArtsDeployClient.newBuilder()
            .withCredential(listDeployTaskHistoryByDateAuth)
            .withRegion(CodeArtsDeployRegion.valueOf(regionId))
            .build();
        // 构建请求头
        ListDeployTaskHistoryByDateRequest request = new ListDeployTaskHistoryByDateRequest();
        // 设置项目id
        request.withProjectId(projectId);
        // 设置部署任务id
        request.withId(taskId);
        // 设置分页页码
        request.withPage(1);
        // 设置每页显示的条目数量
        request.withSize(10);
        // 设置区间开始时间。格式为yyyy-MM-dd。例如：2024-08-01。
        request.withStartDate("");
        // 设置区间结束时间。格式为yyyy-MM-dd。例如：2024-08-10。end_date需大于等于start_date，开始时间和结束时间间隔不能超过30天。
        request.withEndDate("");
        try {
            // 根据开始时间和结束时间查询项目下指定应用的历史部署记录列表
            ListDeployTaskHistoryByDateResponse response = client.listDeployTaskHistoryByDate(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例
#### ListDeployTaskHistoryByDate
```
{
    result: [class ExecuteRecordV2Body {
        duration: 00:00:11
        state: failed
        operator: hwxxxxxx
        executionId: xxxxxx
        startTime: 2024-08-12 15:13:35
        nickname: xxxxxx
        endTime: 2024-08-12 15:13:46
        releaseId: 3
        type: null
    }, class ExecuteRecordV2Body {
        duration: 00:00:04
        state: abort
        operator: hwxxxxxx
        executionId: xxxxxx
        startTime: 2024-08-09 14:49:22
        nickname: xxxxxx
        endTime: 2024-08-09 14:49:26
        releaseId: 2
        type: null
    }, class ExecuteRecordV2Body {
        duration: 00:00:13
        state: failed
        operator: hwxxxxxx
        executionId: xxxxxx
        startTime: 2024-08-09 14:43:05
        nickname: xxxxxx
        endTime: 2024-08-09 14:43:18
        releaseId: 1
        type: null
    }]
    totalNum: 3
}
```
### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: |:----:| :------:  
 2024/08/12 | 1.0  | 文档首次发布