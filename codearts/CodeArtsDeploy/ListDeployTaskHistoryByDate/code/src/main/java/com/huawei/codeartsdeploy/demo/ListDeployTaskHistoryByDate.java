package com.huawei.codeartsdeploy.demo;

import com.huaweicloud.sdk.codeartsdeploy.v2.CodeArtsDeployClient;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.ListDeployTaskHistoryByDateRequest;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.ListDeployTaskHistoryByDateResponse;
import com.huaweicloud.sdk.codeartsdeploy.v2.region.CodeArtsDeployRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListDeployTaskHistoryByDate {

    private static final Logger logger = LoggerFactory.getLogger(ListDeployTaskHistoryByDate.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 注意，如下的 projectId 请使用CodeArts对应项目首页链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/list
        String projectId = "<YOUR PROJECT ID>";
        // 注意，如下的 regionId 请使用项目所在的局点，例如华北-北京四区域为：cn-north-4
        String regionId = "<YOUR REGION ID>";
        // 注意，如下的 taskId 为部署任务id，请使用某个部署任务详细信息页面链接中的taskId参数的值
        // 例:https://devcloud.cn-north-4.huaweicloud.com/deployman/project/{projectId}/deployman/xxxxxx/view/history?origin=project&taskId={taskId}
        String taskId = "<YOUR TASK ID>";
        // 配置认证信息
        ICredential listDeployTaskHistoryByDateAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // 创建服务客户端并配置对应region
        CodeArtsDeployClient client = CodeArtsDeployClient.newBuilder()
            .withCredential(listDeployTaskHistoryByDateAuth)
            .withRegion(CodeArtsDeployRegion.valueOf(regionId))
            .build();
        // 构建请求头
        ListDeployTaskHistoryByDateRequest request = new ListDeployTaskHistoryByDateRequest();
        // 设置项目id
        request.withProjectId(projectId);
        // 设置部署任务id
        request.withId(taskId);
        // 设置分页页码
        request.withPage(1);
        // 设置每页显示的条目数量
        request.withSize(10);
        // 设置区间开始时间。格式为yyyy-MM-dd。例如：2024-08-01。
        request.withStartDate("");
        // 设置区间结束时间。格式为yyyy-MM-dd。例如：2024-08-10。end_date需大于等于start_date，开始时间和结束时间间隔不能超过30天。
        request.withEndDate("");
        try {
            // 根据开始时间和结束时间查询项目下指定应用的历史部署记录列表
            ListDeployTaskHistoryByDateResponse response = client.listDeployTaskHistoryByDate(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}