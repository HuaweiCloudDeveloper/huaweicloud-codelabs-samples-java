### 产品介绍

1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsDeploy/doc?api=ShowHostDetail)
中直接运行调试该接口。

2.在本样例中，您可以查询主机详情。

3.我们可以使用接口查询主机详情，用来自定义主机组管理的相关功能。

### 前置条件

1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目创建主机集群并添加主机。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartsdeploy”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartsdeploy</artifactId>
    <version>3.1.113</version>
</dependency>

```

### 代码示例

查询主机详情：

``` java
package com.huawei.codeartsdeploy.demo;

import com.huaweicloud.sdk.codeartsdeploy.v2.CodeArtsDeployClient;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.ShowHostDetailRequest;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.ShowHostDetailResponse;
import com.huaweicloud.sdk.codeartsdeploy.v2.region.CodeArtsDeployRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowHostDetail {

    private static final Logger logger = LoggerFactory.getLogger(ShowHostDetail.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 初始化SDK，填写认证信息和站点信息。
        BasicCredentials credentials = new BasicCredentials().withAk(ak).withSk(sk);
        CodeArtsDeployClient codeArtsDeployClient = CodeArtsDeployClient.newBuilder()
            .withCredential(credentials)
            .withRegion(CodeArtsDeployRegion.CN_NORTH_4)
            .build();
        // 组装请求体
        ShowHostDetailRequest request = new ShowHostDetailRequest();
        // 在项目的基础资源管理主机组详情页面获取分组ID。
        // 例: https://devcloud.cn-north-4.huaweicloud.com/deploymentgroup/project/{projectId}/config/cn-north-4/manager/hostgroup/list?group_id={groupId}&action=detail
        request.withGroupId("groupId");
        // 可以从以下接口的返回值中获取
        // https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsDeploy/doc?api=ListNewHosts
        request.withHostId("hostId");
        // 发起HTTP API请求，并处理异常。
        try {
            ShowHostDetailResponse response = codeArtsDeployClient.showHostDetail(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}

```

### 返回结果示例

#### ShowHostDetail

```
{
 "status": "success",
 "result": {
  "ip": "12*.1*.***",
  "os": "linux",
  "port": 2*,
  "authorization": {
   "username": "ro**",
   "password": null,
   "private_key": null,
   "trusted_type": 0
  },
  "permission": {
   "can_view": true,
   "can_edit": true,
   "can_delete": true,
   "can_add_host": true,
   "can_copy": true
  },
  "group_id": "8acc4e676c404dd********",
  "host_name": "12*.1*.***-代理机",
  "as_proxy": true,
  "owner_name": "li***",
  "connection_status": "success",
  "install_icagent": false,
  "update_time": "2024-04-01 09:05:31",
  "create_time": "2024-04-01 09:05:31",
  "lastest_connection_time": "2024-04-01 09:05:38",
  "connection_result": "连接成功",
  "nick_name": "liuyi",
  "host_id": "f8f40cdfe4944d04a4c******",
  "proxy_host_id": "",
  "proxy_host": null
 }
}
```

### 修订记录

    发布日期    | 文档版本 | 修订说明

:----------:|:----:| :------:  
2024/09/27 | 1.0 | 文档首次发布