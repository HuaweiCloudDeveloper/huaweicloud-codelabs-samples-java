package com.huawei.codeartsdeploy.demo;

import com.huaweicloud.sdk.codeartsdeploy.v2.CodeArtsDeployClient;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.BatchDeleteHostsRequest;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.BatchDeleteHostsResponse;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.DeploymentHostListEntity;
import com.huaweicloud.sdk.codeartsdeploy.v2.region.CodeArtsDeployRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class BatchDeleteHosts {

    private static final Logger logger = LoggerFactory.getLogger(BatchDeleteHosts.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Initialize the SDK and enter the authentication information and site information.
        BasicCredentials credentials = new BasicCredentials().withAk(ak).withSk(sk);
        CodeArtsDeployClient codeArtsDeployClient = CodeArtsDeployClient.newBuilder()
            .withCredential(credentials)
            .withRegion(CodeArtsDeployRegion.CN_NORTH_4)
            .build();
        // 2. Assemble the request body.
        BatchDeleteHostsRequest request = new BatchDeleteHostsRequest();
        // Obtain the group ID on the basic resource management host group details page of the project.
        // Example: https://devcloud.cn-north-4.huaweicloud.com/deploymentgroup/project/{projectId}/config/cn-north-4/manager/hostgroup/list?group_id={groupId}&action=detail
        request.withGroupId("groupId");
        DeploymentHostListEntity body = new DeploymentHostListEntity();
        List<String> listbodyHostIdList = new ArrayList<>();
        // The value can be obtained from the return value of the following interface
        // https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsDeploy/doc?api=ListNewHosts
        listbodyHostIdList.add("hostId");
        body.withHostIdList(listbodyHostIdList);
        request.withBody(body);
        // 3. Initiate an HTTP API request and handle the exception.
        try {
            BatchDeleteHostsResponse response = codeArtsDeployClient.batchDeleteHosts(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}