### 版本说明
本示例配套的SDK版本为：3.1.58及以上版本

### 功能介绍
本示例展示如何查询主机集群列表

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目。

### 代码示例
以下代码展示如何使用CodeArtsDeploy SDK查询主机集群列表：
``` java
package com.huawei.codeartsdeploy.demo;

import com.huaweicloud.sdk.codeartsdeploy.v2.CodeArtsDeployClient;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.ListHostGroupsRequest;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.ListHostGroupsResponse;
import com.huaweicloud.sdk.codeartsdeploy.v2.region.CodeArtsDeployRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListHostGroups {
    private static final Logger logger = LoggerFactory.getLogger(ListHostGroups.class.getName());

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "<YOUR IAM PROJECT ID>";
        // 1.始化SDK，传入认证信息及CodeArtsDeploy局点信息
        BasicCredentials credentials = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);
        CodeArtsDeployClient codeArtsDeployClient = CodeArtsDeployClient.newBuilder()
            .withCredential(credentials)
            .withRegion(CodeArtsDeployRegion.CN_NORTH_4)
            .build();

        // 2.组装请求体
        ListHostClustersRequest request = new ListHostClustersRequest();
        request.setProjectId("{*** CodeArts项目Id ***}");

        // 3.发起接口http请求，异常处理
        try {
            ListHostClustersResponse response = codeArtsDeployClient.listHostClusters(request);
            logger.info("get Host clusters success" + response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }
}

```
您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsDeploy/doc?api=ListHostClusters) 中直接运行调试该接口。

### 修订记录

 发布日期  | 文档版本 | 修订说明
 :----: | :-----: | :------:  
 2023/9/13 |2.0 | 服务更名修订
