package com.huawei.codeartsdeploy.demo;

import com.huaweicloud.sdk.codeartsdeploy.v2.CodeArtsDeployClient;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.CheckIsDuplicateAppNameRequest;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.CheckIsDuplicateAppNameResponse;
import com.huaweicloud.sdk.codeartsdeploy.v2.region.CodeArtsDeployRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckIsDuplicateAppName {

    private static final Logger logger = LoggerFactory.getLogger(CheckIsDuplicateAppName.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 注意，如下的 projectId 请使用CodeArts对应项目首页链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/list
        String projectId = "<YOUR PROJECT ID>";
        // 注意，如下的 regionId 请使用项目所在的局点，例如华北-北京四区域为：cn-north-4
        String regionId = "<YOUR REGION ID>";
        // 配置认证信息
        ICredential checkIsDuplicateAppNameAuth = new BasicCredentials().withAk(ak).withSk(sk);
        // 创建服务客户端并配置对应region
        CodeArtsDeployClient client = CodeArtsDeployClient.newBuilder()
            .withCredential(checkIsDuplicateAppNameAuth)
            .withRegion(CodeArtsDeployRegion.valueOf(regionId))
            .build();
        // 构建请求并设置项目ID
        CheckIsDuplicateAppNameRequest request = new CheckIsDuplicateAppNameRequest();
        request.withProjectId(projectId);
        // 应用名称，判断是否存在同名应用
        request.withName("");
        try {
            // 查询结果
            CheckIsDuplicateAppNameResponse response = client.checkIsDuplicateAppName(request);
            // 打印结果
            logger.info(response.toString());
        } catch (ServiceResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}