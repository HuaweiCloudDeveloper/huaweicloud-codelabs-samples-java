package com.huawei.codeartsdeploy.demo;

import com.huaweicloud.sdk.codeartsdeploy.v2.CodeArtsDeployClient;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.ListHostGroupBaseInfosRequest;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.ListHostGroupBaseInfosResponse;
import com.huaweicloud.sdk.codeartsdeploy.v2.region.CodeArtsDeployRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListHostGroupBaseInfos {

    private static final Logger logger = LoggerFactory.getLogger(ListHostGroupBaseInfos.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // You can obtain the values of applicationId and projectId on the basic information page of the deployment task.
        // Example: https://devcloud.cn-north-4.huaweicloud.com/deployman/project/{projectId}/deployman/{applicationId}/view/baseinfo
        String applicationId = "<YOUR APPLICATION ID>";
        String projectId = "<YOUR PROJECT ID>";
        // Initialize the SDK and enter the authentication information and site information.
        BasicCredentials credentials = new BasicCredentials().withAk(ak).withSk(sk);
        CodeArtsDeployClient codeArtsDeployClient = CodeArtsDeployClient.newBuilder()
            .withCredential(credentials)
            .withRegion(CodeArtsDeployRegion.CN_NORTH_4)
            .build();
        // 2. Assemble the request body.
        ListHostGroupBaseInfosRequest request = new ListHostGroupBaseInfosRequest();
        request.withApplicationId(applicationId);
        request.withProjectUuid(projectId);
        // 3. Initiate an HTTP API request and handle the exception.
        try {
            ListHostGroupBaseInfosResponse response = codeArtsDeployClient.listHostGroupBaseInfos(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}

