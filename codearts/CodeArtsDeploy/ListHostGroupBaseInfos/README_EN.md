### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeArtsDeploy/doc?api=ListHostGroupBaseInfos).

2.In this example, you can query the Basic Environment Information List of an Application.

3.You can use the interface to query the basic environment information list of an application for display.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A project and host cluster have been created on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-codeartsdeploy. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartsdeploy</artifactId>
    <version>3.1.113</version>
</dependency>

```

### Code example

The following code shows how to query the basic environment information list of an application.：

``` java
package com.huawei.codeartsdeploy.demo;

import com.huaweicloud.sdk.codeartsdeploy.v2.CodeArtsDeployClient;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.ListHostGroupBaseInfosRequest;
import com.huaweicloud.sdk.codeartsdeploy.v2.model.ListHostGroupBaseInfosResponse;
import com.huaweicloud.sdk.codeartsdeploy.v2.region.CodeArtsDeployRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListHostGroupBaseInfos {

    private static final Logger logger = LoggerFactory.getLogger(ListHostGroupBaseInfos.class.getName());

    public static void main(String[] args) {
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // You can obtain the values of applicationId and projectId on the basic information page of the deployment task.
        // Example: https://devcloud.cn-north-4.huaweicloud.com/deployman/project/{projectId}/deployman/{applicationId}/view/baseinfo
        String applicationId = "<YOUR APPLICATION ID>";
        String projectId = "<YOUR PROJECT ID>";
        // Initialize the SDK and enter the authentication information and site information.
        BasicCredentials credentials = new BasicCredentials().withAk(ak).withSk(sk);
        CodeArtsDeployClient codeArtsDeployClient = CodeArtsDeployClient.newBuilder()
            .withCredential(credentials)
            .withRegion(CodeArtsDeployRegion.CN_NORTH_4)
            .build();
        // 2. Assemble the request body.
        ListHostGroupBaseInfosRequest request = new ListHostGroupBaseInfosRequest();
        request.withApplicationId(applicationId);
        request.withProjectUuid(projectId);
        // 3. Initiate an HTTP API request and handle the exception.
        try {
            ListHostGroupBaseInfosResponse response = codeArtsDeployClient.listHostGroupBaseInfos(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}

```

### Example of the returned result

#### ListHostGroupBaseInfos

```
{
 "status": "success",
 "result": [
  {
   "name": "cluster1",
   "os": "linux",
   "uuid": "80e12117d10842feb04b258******",
   "group_id": "80e12117d10842feb04******",
   "host_count": 1
  }
 ],
 "total": 1
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/09/19 | 1.0 | This document is released for the first time.