package com.appstage.demos.jenkinsadapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
@ServletComponentScan(basePackages = {"com.appstage.demos.jenkinsadapter"})
public class JenkinsAdapterApplication {

    public static void main(String[] args) {
        SpringApplication.run(JenkinsAdapterApplication.class, args);
    }

}
