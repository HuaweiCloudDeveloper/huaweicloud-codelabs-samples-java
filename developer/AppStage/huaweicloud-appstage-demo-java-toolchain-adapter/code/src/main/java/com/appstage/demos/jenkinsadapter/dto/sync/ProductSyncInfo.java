package com.appstage.demos.jenkinsadapter.dto.sync;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ProductSyncInfo {
    private Product product;

    @JsonProperty("op_type")
    @JSONField(name = "op_type")
    private String opType;

    @JsonProperty("time_stamp")
    @JSONField(name = "time_stamp")
    private String timeStamp;

    @Data
    public static class Product {
        @JsonProperty("product_code")
        @JSONField(name = "product_code")
        private String productCode;

        @JsonProperty("product_id")
        @JSONField(name = "product_id")
        private String productId;

        @JsonProperty("dept_id")
        @JSONField(name = "dept_id")
        private String deptId;

        @JsonProperty("product_name")
        @JSONField(name = "product_name")
        private String productName;

        private String status;
    }
}
