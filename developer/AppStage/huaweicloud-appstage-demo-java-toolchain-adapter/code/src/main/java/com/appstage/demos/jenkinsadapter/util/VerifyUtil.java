package com.appstage.demos.jenkinsadapter.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.MGF1ParameterSpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@Slf4j
public class VerifyUtil {
    private static KeyFactory keyFactory;

    static {
        try {
            keyFactory = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            // 打印异常日志
            keyFactory = null;
        }
    }

    public static boolean verifySyncSign(Object body, String expectedSign, String accessKey) {
        // BeanUtils来自包commons-beanutils,其版本必须要>=1.9.4,
        // 否则需要执行paramsMap.remove("class"),移除参数中不存在的内容
        try {
            Map<String, String> paramsMap = BeanUtils.describe(body);
            String timeStamp = paramsMap.get("timeStamp");

            // 对剩下的参数进行排序，拼接成加密内容
            Map<String, String> sortedMap = new TreeMap<String, String>(paramsMap);
            StringBuilder strBuffer = new StringBuilder();
            Set<String> keySet = sortedMap.keySet();
            for (String key : keySet) {
                String value = sortedMap.get(key);
                if (StringUtils.isBlank(value)) {
                    continue;
                }
                strBuffer.append("&").append(key).append("=").append(value);
            }
            // 修正消息体,去除第一个参数前面的&
            String reqParams = strBuffer.substring(1);
            String key = accessKey + timeStamp;
            String signature = generateResponseBodySignature(key, reqParams);
            log.info("verifySyncSign expected: {}, exact: {}", expectedSign, signature);
            return StringUtils.equals(expectedSign, signature);
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
    }

    /**
     * 生成http响应消息体签名示例Demo
     *
     * @param key  用户在isv console分配的accessKey，请登录后查看
     * @param body http响应的报文
     * @return 加密结果
     */
    private static String generateResponseBodySignature(String key, String body)
            throws InvalidKeyException, NoSuchAlgorithmException,
            IllegalStateException, UnsupportedEncodingException {
        return new String(Base64.encodeBase64(hmacSHA256(key, body)));
    }

    /**
     * hamcSHA256加密算法
     *
     * @param macKey  秘钥key
     * @param macData 加密内容-响应消息体
     * @return 加密密文
     */
    private static byte[] hmacSHA256(String macKey, String macData)
            throws NoSuchAlgorithmException, InvalidKeyException,
            IllegalStateException, UnsupportedEncodingException {
        SecretKeySpec secret =
                new SecretKeySpec(macKey.getBytes(), "HmacSHA256");
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(secret);
        return mac.doFinal(macData.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * RSA私钥解密
     */
    public static String decryptByRsa(String cryptography, String privateKey) {
        try {
            PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(Base64.decodeBase64(privateKey));
            PrivateKey pk = keyFactory.generatePrivate(priPKCS8);

            Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPPadding");
            OAEPParameterSpec oaepParameterSpec = new OAEPParameterSpec("SHA-256", "MGF1",
                    new MGF1ParameterSpec("SHA-256"), PSource.PSpecified.DEFAULT);

            cipher.init(Cipher.DECRYPT_MODE, pk, oaepParameterSpec);

            byte[] b1 = Base64.decodeBase64(cryptography);

            byte[] b = cipher.doFinal(b1);
            return new String(b, StandardCharsets.UTF_8);
        } catch (Exception e) {
            log.error(e.getMessage());
            return cryptography;
        }
    }
}
