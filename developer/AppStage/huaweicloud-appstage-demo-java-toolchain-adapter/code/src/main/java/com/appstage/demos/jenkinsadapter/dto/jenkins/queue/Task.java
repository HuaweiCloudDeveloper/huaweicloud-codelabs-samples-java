package com.appstage.demos.jenkinsadapter.dto.jenkins.queue;

import lombok.Data;

@Data
public class Task {
    private String name;
    private String url;
}
