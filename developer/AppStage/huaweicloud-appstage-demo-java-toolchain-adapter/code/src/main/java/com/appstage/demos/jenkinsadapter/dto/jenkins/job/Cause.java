package com.appstage.demos.jenkinsadapter.dto.jenkins.job;

import lombok.Data;

@Data
public class Cause {
    private String clazz;
    private String shortDescription;
    private String userId;
    private String userName;
}
