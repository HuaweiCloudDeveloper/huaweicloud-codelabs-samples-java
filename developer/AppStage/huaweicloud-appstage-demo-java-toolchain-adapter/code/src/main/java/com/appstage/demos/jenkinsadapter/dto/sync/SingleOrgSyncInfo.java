package com.appstage.demos.jenkinsadapter.dto.sync;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SingleOrgSyncInfo extends OrgSyncInfo {
    private String instanceId;
    private String tenantId;
    private Integer flag;
    private Integer testFlag;
    private String timeStamp;
}
