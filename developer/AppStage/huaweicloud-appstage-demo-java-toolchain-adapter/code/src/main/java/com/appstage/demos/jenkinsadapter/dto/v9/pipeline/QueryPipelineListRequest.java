/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.appstage.demos.jenkinsadapter.dto.v9.pipeline;

import lombok.Data;
import org.springframework.validation.annotation.Validated;

/**
 * 功能描述 流水线列表接口入参
 *
 * @author l00572809
 * @since 2024/9/3
 */
@Validated
@Data
public class QueryPipelineListRequest {
    private Integer offset;

    private Integer limit;
}
