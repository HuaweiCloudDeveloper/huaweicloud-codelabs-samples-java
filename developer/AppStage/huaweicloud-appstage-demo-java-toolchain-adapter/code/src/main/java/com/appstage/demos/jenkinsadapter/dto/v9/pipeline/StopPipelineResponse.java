/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.appstage.demos.jenkinsadapter.dto.v9.pipeline;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * "停止流水线请求参数
 *
 * @author l00572809
 * @since 2024-09-03
 */
@Data
public class StopPipelineResponse {
    @JsonProperty("success")
    private Boolean success;
}
