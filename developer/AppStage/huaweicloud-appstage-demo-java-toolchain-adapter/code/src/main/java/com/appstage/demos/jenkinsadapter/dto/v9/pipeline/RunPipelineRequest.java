/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2023. All rights reserved.
 */

package com.appstage.demos.jenkinsadapter.dto.v9.pipeline;

import lombok.Data;

/**
 * 功能描述 运行流水线参数
 *
 * @author l00572809
 * @since 2024-09-03
 */
@Data
public class RunPipelineRequest {
}
