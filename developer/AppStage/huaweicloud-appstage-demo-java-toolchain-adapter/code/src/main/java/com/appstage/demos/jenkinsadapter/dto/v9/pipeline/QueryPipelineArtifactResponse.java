/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.appstage.demos.jenkinsadapter.dto.v9.pipeline;

import lombok.Data;

import java.util.List;

/**
 * 流水线产物信息
 *
 * @author l00572809
 * @since 2024/9/3
 */
@Data
public class QueryPipelineArtifactResponse {
    private List<PipelineArtifact> data;
}
