package com.appstage.demos.jenkinsadapter.dto.sync;

import lombok.Data;

@Data
public class ApplicationSyncInfo {
    private String instanceId;
    private String appId;
    private String tenantId;
    private String clientId;
    private String clientSecret;
    private Integer flag;
    private Integer testFlag;
    private String timeStamp;
}
