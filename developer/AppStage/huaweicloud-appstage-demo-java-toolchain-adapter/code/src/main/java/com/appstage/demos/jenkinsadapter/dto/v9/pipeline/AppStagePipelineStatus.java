/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.appstage.demos.jenkinsadapter.dto.v9.pipeline;

public enum AppStagePipelineStatus {
    COMPLETED,
    FAILED,
    RUNNING,
    INIT,
    UNSELECTED,
    CANCELED
}
