package com.appstage.demos.jenkinsadapter.dto.sync;

import lombok.Data;

@Data
public class AuthSyncInfo {
    private String instanceId;
    private String tenantId;
    private String appId;
    private String userList; // string of List<UserSyncInfo>
    private String currentSyncTime;
    private Integer flag;
    private Integer testFlag;
    private String timeStamp;
}
