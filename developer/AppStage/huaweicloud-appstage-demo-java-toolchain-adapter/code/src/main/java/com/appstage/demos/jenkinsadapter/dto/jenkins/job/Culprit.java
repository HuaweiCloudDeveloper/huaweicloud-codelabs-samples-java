package com.appstage.demos.jenkinsadapter.dto.jenkins.job;

import lombok.Data;

@Data
public class Culprit {

    private String absoluteUrl;

    private String fullName;
}
