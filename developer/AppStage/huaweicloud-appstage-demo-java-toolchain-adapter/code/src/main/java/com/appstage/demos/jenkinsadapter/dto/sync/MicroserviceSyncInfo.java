/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.appstage.demos.jenkinsadapter.dto.sync;


import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class MicroserviceSyncInfo {
    private Microservice microservice;

    @JsonProperty("op_type")
    @JSONField(name = "op_type")
    private String opType;

    @JsonProperty("time_stamp")
    @JSONField(name = "time_stamp")
    private String timeStamp;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Microservice {
        @JsonProperty("microservice_id")
        @JSONField(name = "microservice_id")
        private String microserviceId;

        @JsonProperty("service_id")
        @JSONField(name = "service_id")
        private String serviceId;

        @JsonProperty("product_id")
        @JSONField(name = "product_id")
        private String productId;

        @JsonProperty("dept_id")
        @JSONField(name = "dept_id")
        private String deptId;

        @JsonProperty("microservice_code")
        @JSONField(name = "microservice_code")
        private String microserviceCode;

        @JsonProperty("microservice_name")
        @JSONField(name = "microservice_name")
        private String microserviceName;

        private String status;
    }
}
