package com.appstage.demos.jenkinsadapter.dto.jenkins.job;

import lombok.Data;

import java.util.List;

@Data
public class ChangeSetList {
    private List<ChangeSet> items;
    private String kind;
}
