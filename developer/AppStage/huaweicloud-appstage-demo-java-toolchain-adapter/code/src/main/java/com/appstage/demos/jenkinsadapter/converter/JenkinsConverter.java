/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.appstage.demos.jenkinsadapter.converter;

import com.appstage.demos.jenkinsadapter.dto.jenkins.common.BooleanResponse;
import com.appstage.demos.jenkinsadapter.dto.jenkins.common.IntegerResponse;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.Action;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.Artifact;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.Branch;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.BuildInfo;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.Cause;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.Job;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.JobInfo;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.JobList;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.LastBuiltRevision;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.Stage;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.Workflow;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.AppStagePipelineStatus;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.ArtifactHashCode;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.PipelineArtifact;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.PipelineInfo;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.PipelineLatestRun;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.PipelineSource;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.PipelineStage;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.QueryPipeLineDetailResponse;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.QueryPipelineArtifactResponse;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.QueryPipelineListResponse;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.QueryPipelineRunDetailResponse;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.RunPipelineResponse;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.StopPipelineResponse;
import com.appstage.demos.jenkinsadapter.util.TimeUtil;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface JenkinsConverter {

    @Mapping(target = "total", source = "list.jobs", qualifiedByName = "PipelineTotalConverter")
    @Mapping(target = "data", source = "list.jobs")
    QueryPipelineListResponse convert(JobList list, @Context Map<String, BuildInfo> buildInfoMap, @Context Map<String, Workflow> workflowMap);

    List<PipelineInfo> convert(List<Job> jobs, @Context Map<String, BuildInfo> buildInfoMap, @Context Map<String, Workflow> workflowMap);

    @Mapping(target = "pipelineId", source = "job.name")
    @Mapping(target = "pipelineName", source = "job.name")
    @Mapping(target = "latestRun", source = "job.name", qualifiedByName = "BuildInfoConverter")
    PipelineInfo convert(Job job, @Context Map<String, BuildInfo> buildInfoMap, @Context Map<String, Workflow> workflowMap);


    @Mapping(target = "executorId", source = "info", qualifiedByName = "ExecutorIdConverter")
    @Mapping(target = "executorName", source = "info", qualifiedByName = "ExecutorNameConverter")
    @Mapping(target = "stageStatusList", source = "workflow.stages")
    @Mapping(target = "triggerType", source = "info", qualifiedByName = "TriggerTypeConverter")
    @Mapping(target = "buildParams.targetBranch", source = "info", qualifiedByName = "BranchNameConverter")
    @Mapping(target = "buildParams.gitUrl", source = "info", qualifiedByName = "GitUrlConverter")
    @Mapping(target = "buildParams.repoName", source = "info", qualifiedByName = "RepoNameConverter")
    @Mapping(target = "buildParams.commitId", source = "info", qualifiedByName = "CommitIdConverter")
    @Mapping(target = "startTime", source = "info.timestamp")
    @Mapping(target = "endTime", expression = "java(info.getTimestamp() + info.getDuration())")
    @Mapping(target = "status", source = "workflow.status", qualifiedByName = "StatusConverter")
    PipelineLatestRun convertLatestRun(BuildInfo info, Workflow workflow);

    @Named("StatusConverter")
    default String statusConverter(String jenkinsStatus) {
        if (jenkinsStatus == null) {
            return AppStagePipelineStatus.RUNNING.name();
        }

        switch (jenkinsStatus) {
            case "SUCCESS": return AppStagePipelineStatus.COMPLETED.name();
            case "FAILURE": // fall through
            case "FAILED": return AppStagePipelineStatus.FAILED.name();
            case "ABORTED": return AppStagePipelineStatus.CANCELED.name();
            case "RUNNING": return AppStagePipelineStatus.RUNNING.name();
            default: return AppStagePipelineStatus.UNSELECTED.name();
        }
    }

    default Action findAction(List<Action> actions, String qualifiedName) {
        return Optional.ofNullable(actions)
                .flatMap(list -> list.stream().filter(action -> qualifiedName.equals(action.get_class())).findFirst())
                .orElse(null);
    }

    default Action getLatestBuild(List<Action> actions) {
        return findAction(actions, "hudson.plugins.git.util.BuildData");
    }

    default Action getCauseAction(List<Action> actions) {
        return findAction(actions, "hudson.model.CauseAction");
    }

    @Named("BuildInfoConverter")
    default PipelineLatestRun convert(String jobName, @Context Map<String, BuildInfo> buildInfoMap, @Context Map<String, Workflow> workflowMap) {
        return convertLatestRun(buildInfoMap.get(jobName), workflowMap.get(jobName));
    }

    @Named(("RepoNameConverter"))
    default String getRepoName(BuildInfo info) {
        return Optional.ofNullable(getGitUrl(info))
                .map(url -> url.split("/"))
                .map(strings -> strings[strings.length - 1])
                .map(name -> name.replace(".git", ""))
                .orElse(null);
    }

    @Named("GitUrlConverter")
    default String getGitUrl(BuildInfo info) {
        return Optional.ofNullable(info)
                .map(BuildInfo::getActions)
                .map(this::getLatestBuild)
                .map(Action::getRemoteUrls)
                .map(list -> list.get(0))
                .orElse(null);
    }

    @Named("TriggerTypeConverter")
    default String getTriggerType(BuildInfo info) {
        return getExecutorName(info) == null ? "Scheduler" : "Manual";
    }

    @Named("ExecutorIdConverter")
    default String getExecutorId(BuildInfo info) {
        return Optional.ofNullable(info)
                .map(BuildInfo::getActions)
                .map(this::getCauseAction)
                .map(Action::getCauses)
                .map(list -> list.get(0))
                .map(Cause::getUserId)
                .orElse(null);
    }

    @Named("ExecutorNameConverter")
    default String getExecutorName(BuildInfo info) {
        return Optional.ofNullable(info)
                .map(BuildInfo::getActions)
                .map(this::getCauseAction)
                .map(Action::getCauses)
                .map(list -> list.get(0))
                .map(Cause::getUserName)
                .orElse(null);
    }

    @Named("BranchNameConverter")
    default String getBranchName(BuildInfo info) {
        return Optional.ofNullable(info)
                .map(BuildInfo::getActions)
                .map(this::getLatestBuild)
                .map(Action::getLastBuiltRevision)
                .map(LastBuiltRevision::getBranch)
                .map(list -> {
                    if (!list.isEmpty()) {
                        return list.get(0);
                    }
                    return null;
                })
                .map(Branch::getName)
                .map(branchName -> {
                    String[] branchs = branchName.split("/");
                    return branchs[branchs.length - 1];
                }).orElse(null);
    }

    @Named("CommitIdConverter")
    default String getCommitId(BuildInfo info) {
        return Optional.ofNullable(info)
                .map(BuildInfo::getActions)
                .map(this::getLatestBuild)
                .map(Action::getLastBuiltRevision)
                .map(LastBuiltRevision::getSHA1)
                .map(id -> id.substring(0, 8))
                .orElse(null);
    }

    List<PipelineStage> convertStages(List<Stage> stages);

    @Mapping(target = "sequence", source = "stage.id")
    @Mapping(target = "startTime", source = "stage", qualifiedByName = "StageStartTimeConverter")
    @Mapping(target = "endTime", source = "stage", qualifiedByName = "StageEndTimeConverter")
    @Mapping(target = "status", source = "status", qualifiedByName = "StatusConverter")
    PipelineStage convert(Stage stage);

    @Named("StageStartTimeConverter")
    default String getStartTime(Stage stage) {
        return TimeUtil.formatMilliSecond(stage.getStartTimeMillis());
    }

    @Named("StageEndTimeConverter")
    default String getEndTime(Stage stage) {
        return TimeUtil.formatMilliSecond(stage.getStartTimeMillis() + stage.getDurationMillis());
    }

    @Named("PipelineTotalConverter")
    default int getTotal(List<Job> jobs) {
        if (CollectionUtils.isEmpty(jobs)) {
            return 0;
        } else {
            return jobs.size();
        }
    }

    @Mapping(target = "pipelineId", source = "info.name")
    @Mapping(target = "pipelineName", source = "info.name")
    QueryPipeLineDetailResponse convert(JobInfo info);

    @Mapping(target = "pipelineRunId", source = "rsp.value")
    RunPipelineResponse convert(IntegerResponse rsp);

    @Mapping(target = "pipelineId", source = "pipelineId")
    @Mapping(target = "pipelineName", source = "pipelineId")
    @Mapping(target = "executorId", source = "info", qualifiedByName = "ExecutorIdConverter")
    @Mapping(target = "executorName", source = "info", qualifiedByName = "ExecutorNameConverter")
    @Mapping(target = "status", source = "info.result", qualifiedByName = "StatusConverter")
    @Mapping(target = "triggerType", source = "info", qualifiedByName = "TriggerTypeConverter")
    @Mapping(target = "startTime", source = "info.timestamp")
    @Mapping(target = "endTime", expression = "java(info.getTimestamp() + info.getDuration())")
    @Mapping(target = "sources", source = "info", qualifiedByName = "RunSourcesConverter")
    QueryPipelineRunDetailResponse convert(BuildInfo info, String pipelineId);

    @Mapping(target = "repoName", source = "info", qualifiedByName = "RepoNameConverter")
    @Mapping(target = "targetBranch", source = "info", qualifiedByName = "BranchNameConverter")
    @Mapping(target = "commitId", source = "info", qualifiedByName = "CommitIdConverter")
    @Mapping(target = "repoId", source = "info", qualifiedByName = "GitUrlConverter")
    @Mapping(target = "gitUrl", source = "info", qualifiedByName = "GitUrlConverter")
    PipelineSource convert(BuildInfo info);

    @Named(("RunSourcesConverter"))
    default List<PipelineSource> getRunSources(BuildInfo info) {
        return Collections.singletonList(convert(info));
    }

    @Mapping(target = "success", source = "value", defaultValue = "true")
    StopPipelineResponse convert(BooleanResponse rsp);

    @Mapping(target = "data", source = "artifacts")
    QueryPipelineArtifactResponse convertArts(BuildInfo info);

    List<PipelineArtifact> convert(List<Artifact> artifacts);

    @Mapping(target = "name", source = "art.fileName")
    @Mapping(target = "fileSize", constant = "-")
    @Mapping(target = "hashCodes", source = "art.fileName", qualifiedByName = "HashCodeConverter")
    @Mapping(target = "uploadTarget", constant = "TenantOBS")
    @Mapping(target = "uploadLocation", constant = "obstestojl")
    @Mapping(target = "artifactUri", source = "art.relativePath")
    PipelineArtifact convert(Artifact art);

    @Named("HashCodeConverter")
    default List<ArtifactHashCode> hashCodeConverter(String name) {
        ArtifactHashCode hashCode = new ArtifactHashCode();
        hashCode.setHashType("SHA256");
        hashCode.setHashValue("-");
        return Collections.singletonList(hashCode);
    }
}
