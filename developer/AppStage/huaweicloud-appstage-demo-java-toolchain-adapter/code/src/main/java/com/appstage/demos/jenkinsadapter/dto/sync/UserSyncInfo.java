package com.appstage.demos.jenkinsadapter.dto.sync;

import lombok.Data;

@Data
public class UserSyncInfo {
    private String username;
    private String name;
    private String position;
    private String orgCode;
    private String role;
    private String enable;
}
