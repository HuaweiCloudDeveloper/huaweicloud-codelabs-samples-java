package com.appstage.demos.jenkinsadapter.common;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class AdapterExceptionAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {AdapterException.class})
    protected AdapterResponse<?> handle(Exception ex, WebRequest request) {
        return AdapterResponse.builder().code(500).message(ex.getMessage()).build();
    }
}
