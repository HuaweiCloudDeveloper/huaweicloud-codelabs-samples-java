/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2023. All rights reserved.
 */

package com.appstage.demos.jenkinsadapter.dto.v9.pipeline;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 功能描述 运行流水线返回参数
 *
 * @author l00572809
 * @since 2024-09-03
 */
@Data
public class RunPipelineResponse {
    @JsonProperty("pipeline_run_id")
    private String pipelineRunId;
}
