package com.appstage.demos.jenkinsadapter.dto.jenkins.job;

import lombok.Data;

import java.util.List;

@Data
public class Action {

    private List<Cause> causes;

    private List<Parameter> parameters;

    private String text;

    private String iconPath;

    private String _class;

    private LastBuiltRevision lastBuiltRevision;

    private List<String> remoteUrls;
}

