package com.appstage.demos.jenkinsadapter.service;

import com.appstage.demos.jenkinsadapter.converter.JenkinsConverter;
import com.appstage.demos.jenkinsadapter.dto.jenkins.common.BooleanResponse;
import com.appstage.demos.jenkinsadapter.dto.jenkins.common.IntegerResponse;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.BuildInfo;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.Job;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.JobInfo;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.JobList;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.Workflow;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.PipelineInfo;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.PipelineLatestRun;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.QueryCodeCheckTaskResponse;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.QueryPipeLineDetailResponse;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.QueryPipelineArtifactResponse;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.QueryPipelineListResponse;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.QueryPipelineRunDetailResponse;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.RunPipelineResponse;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.StopPipelineResponse;
import com.appstage.demos.jenkinsadapter.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class JenkinsService {
    @Resource
    private JenkinsClient jenkinsClient;

    @Resource
    private JenkinsConverter converter;

    /**
     * 获取AutoValue_JobList
     */
    public QueryPipelineListResponse queryPipelineList(int from, int size) {
        JobList jobs = Optional.ofNullable(jenkinsClient.getJobList()).orElse(new JobList());
        List<Job> jobList;
        if (!CollectionUtils.isEmpty(jobs.getJobs()) && jobs.getJobs().size() >= size) {
            jobList = jobs.getJobs().subList(from, from + size);
        } else {
            jobList = jobs.getJobs();
        }
        jobs.setJobs(jobList);
        Map<String, BuildInfo> buildInfoMap = jobList.stream().map(job -> {
            BuildInfo info = Optional.ofNullable(queryLastBuild(job.getName())).orElse(new BuildInfo());
            info.setJobName(job.getName());
            return info;
        }).collect(Collectors.toMap(BuildInfo::getJobName, info -> info));

        Map<String, Workflow> workflowMap = jobList.stream().map(job -> {
            Workflow workflow = Optional.ofNullable(queryLastWorkflow(job.getName())).orElse(new Workflow());
            workflow.setJobName(job.getName());
            return workflow;
        }).collect(Collectors.toMap(Workflow::getJobName, info -> info));
        QueryPipelineListResponse result = converter.convert(jobs, buildInfoMap, workflowMap);
        log.info("query pipeline list: {}", JsonUtil.toString(result));
        return result;
    }

    public QueryPipelineListResponse queryRecentRunPipelineList(int from, int size) {
        QueryPipelineListResponse rsp = this.queryPipelineList(0, 50);
        QueryPipelineListResponse result = new QueryPipelineListResponse();
        List<PipelineInfo> sortedList = Optional.ofNullable(rsp)
                .map(QueryPipelineListResponse::getData)
                // 过滤没有执行过的流水线
                .map(list -> list.stream().filter(this::isPipelineRun))
                // 根据执行开始时间排序
                .map(list -> list.sorted(this::comparePipelineByStartTime).collect(Collectors.toList()))
                // 截取前offset, limit个流水线
                .map(sList -> sList.subList(from, Math.min(from + size, sList.size()))).orElse(Collections.emptyList());
        result.setTotal(sortedList.size());
        result.setData(sortedList);
        return result;
    }

    private boolean isPipelineRun(PipelineInfo info) {
        return Optional.ofNullable(info)
                .map(PipelineInfo::getLatestRun)
                .map(PipelineLatestRun::getStartTime)
                .map(timestamp -> timestamp > 0)
                .orElse(Boolean.FALSE);
    }

    private int comparePipelineByStartTime(PipelineInfo a, PipelineInfo b) {
        Long startA = Optional.ofNullable(a).map(PipelineInfo::getLatestRun).map(PipelineLatestRun::getStartTime).orElse(null);
        Long startB = Optional.ofNullable(b).map(PipelineInfo::getLatestRun).map(PipelineLatestRun::getStartTime).orElse(null);
        if (startA == null && startB == null) {
            return 0;
        }

        if (startA == null) {
            return 1;
        }

        if (startB == null) {
            return -1;
        }
        return startB.compareTo(startA);
    }

    /**
     * 获取流水线详情
     */
    public QueryPipeLineDetailResponse queryPipelineDetail(String pipelineId) {
        JobInfo jobInfo = jenkinsClient.getJobInfo(pipelineId);
        return converter.convert(jobInfo);
    }

    /**
     * 执行流水线
     */
    public RunPipelineResponse runPipeline(String pipelineId) throws InterruptedException {
        IntegerResponse rsp = jenkinsClient.runJob(pipelineId);
        if (rsp == null || rsp.getValue() == null) {
            return new RunPipelineResponse();
        }
        return converter.convert(rsp);
    }

    /**
     * 查询job执行详情
     */
    public QueryPipelineRunDetailResponse queryPipelineRunDetail(String pipelineId, Integer runId) {
        BuildInfo info = jenkinsClient.getJobRunInfo(pipelineId, runId);
        return converter.convert(info, pipelineId);
    }

    /**
     * 停止job执行
     */
    public StopPipelineResponse stopPipelineRun(String pipelineId, Integer runId) {
        BooleanResponse rsp = Optional.ofNullable(jenkinsClient.stopRunJob(pipelineId, runId)).orElse(new BooleanResponse());
        return converter.convert(rsp);
    }

    /**
     * 获取产物信息
     */
    public QueryPipelineArtifactResponse queryPipelineRunArtifact(String pipelineId, Integer runId) {
        BuildInfo info = jenkinsClient.getJobRunInfo(pipelineId, runId);
        QueryPipelineArtifactResponse result = converter.convertArts(info);
        log.info("query {}#{} pipeline run artifact: {}", pipelineId, runId, JsonUtil.toString(result));
        return result;
    }

    public QueryCodeCheckTaskResponse queryCodeCheckTask(String pipelineId, Integer runId) {
        QueryCodeCheckTaskResponse rsp = new QueryCodeCheckTaskResponse();
        rsp.setCheckTaskId("mockTaskId");
        return rsp;
    }

    /**
     * 获取最后一次构建信息
     */
    public BuildInfo queryLastBuild(String jobName) {
        try {
            return jenkinsClient.getLastBuild(jobName);
        } catch (Exception e) {
            log.error("query last build failed: {}, {}", jobName, e.getMessage());
        }
        return null;
    }

    public Workflow queryLastWorkflow(String jobName) {
        try {
            return jenkinsClient.getLastBuildWorkflow(jobName);
        } catch (Exception e) {
            log.error("query last work flow failed: {}, {}", jobName, e.getMessage());
        }
        return null;
    }
}
