package com.appstage.demos.jenkinsadapter.dto.sync;

import lombok.Data;

@Data
public class AllOrgSyncInfo {
    private String instanceId;
    private String tenantId;
    private String orgInfo; // string of List<OrgSyncInfo>
    private Integer flag;
    private Integer testFlag;
    private String timeStamp;
}
