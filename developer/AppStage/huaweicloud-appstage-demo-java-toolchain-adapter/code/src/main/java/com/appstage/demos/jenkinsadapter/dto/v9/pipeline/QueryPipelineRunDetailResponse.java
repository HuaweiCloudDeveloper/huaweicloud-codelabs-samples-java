/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.appstage.demos.jenkinsadapter.dto.v9.pipeline;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * 查询流水线运行详情参数
 *
 * @author l00572809
 * @since 2024-09-03
 */
@Data
public class QueryPipelineRunDetailResponse {
    private String id;

    @JsonProperty("pipeline_id")
    private String pipelineId;

    @JsonProperty("pipeline_name")
    private String pipelineName;

    @JsonProperty("executor_id")
    private String executorId;

    @JsonProperty("executor_name")
    private String executorName;

    private String status;

    @JsonProperty("trigger_type")
    private String triggerType;

    @JsonProperty("start_time")
    private Long startTime;

    @JsonProperty("end_time")
    private Long endTime;

    private List<PipelineSource> sources;
}
