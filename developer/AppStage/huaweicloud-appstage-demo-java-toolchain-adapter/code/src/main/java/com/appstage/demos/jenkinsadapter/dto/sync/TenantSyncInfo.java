package com.appstage.demos.jenkinsadapter.dto.sync;

import lombok.Data;

@Data
public class TenantSyncInfo {
    private String instanceId;
    private String orderId;
    private String tenantCode;
    private String name;
    private String domainName;
    private Integer flag;
    private Integer testFlag;
    private String timeStamp;
    private String tenantId;
}
