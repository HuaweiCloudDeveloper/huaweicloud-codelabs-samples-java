package com.appstage.demos.jenkinsadapter.util;

public class AppstageUrl {
    public static final String HOST = "https://orgid-beta.ulanqab.huawei.com";
    public static final String OAUTH2_USER_INFO = "/oauth2/userinfo";
}
