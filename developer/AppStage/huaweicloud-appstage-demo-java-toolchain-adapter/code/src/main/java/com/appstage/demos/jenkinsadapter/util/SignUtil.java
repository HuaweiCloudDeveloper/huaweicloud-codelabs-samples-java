package com.appstage.demos.jenkinsadapter.util;

import com.alibaba.fastjson.JSON;
import com.appstage.demos.jenkinsadapter.dto.sync.SignBody;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SignUtil {
    public static SignBody buildSignBody(Object body, String timeStamp) {
       return SignBody.builder().body(JSON.toJSONString(body)).timeStamp(timeStamp).build();
    }
}
