package com.appstage.demos.jenkinsadapter.dto.jenkins.job;

import lombok.Data;

import java.util.List;

@Data
public class BuildInfo {

    private List<Artifact> artifacts;

    private List<Action> actions;

    private boolean building;

    private String description;

    private String displayName;

    private long duration;

    private long estimatedDuration;

    private String fullDisplayName;

    private String id;

    private boolean keepLog;

    private int number;

    private int queueId;

    private String result;

    private long timestamp;

    private String url;

    private List<ChangeSetList> changeSets;

    private String builtOn;

    private List<Culprit> culprits;

    private BuildInfo nextBuild;

    private String jobName;
}
