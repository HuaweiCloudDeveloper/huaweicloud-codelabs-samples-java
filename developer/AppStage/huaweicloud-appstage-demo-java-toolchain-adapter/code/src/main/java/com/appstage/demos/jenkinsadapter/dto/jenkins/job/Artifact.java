package com.appstage.demos.jenkinsadapter.dto.jenkins.job;

import lombok.Data;

@Data
public class Artifact {

    private String displayPath;

    private String fileName;

    private String relativePath;
}
