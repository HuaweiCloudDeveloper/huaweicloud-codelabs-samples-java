package com.appstage.demos.jenkinsadapter.dto.jenkins.job;

import lombok.Data;

import java.util.List;

@Data
public class JobList {
    private String clazz;
    private List<Job> jobs;
    private String url;
}
