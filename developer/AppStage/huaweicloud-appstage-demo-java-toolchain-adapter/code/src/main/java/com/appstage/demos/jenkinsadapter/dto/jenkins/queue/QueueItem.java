package com.appstage.demos.jenkinsadapter.dto.jenkins.queue;

import lombok.Data;

@Data
public class QueueItem {
    private boolean blocked;

    private boolean buildable;

    private int id;

    private long inQueueSince;

    private boolean stuck;

    private Task task;

    private String url;

    private String why;

    /**
     * When did this job exit the Queue.waitingList phase?
     * For a Queue.NotWaitingItem
     *
     * @return The time expressed in milliseconds after January 1, 1970, 0:00:00 GMT.
     */
    private long buildableStartMilliseconds;

    private boolean cancelled;

    private Executable executable;

    // https://javadoc.jenkins.io/hudson/model/Queue.WaitingItem.html
    /**
     * This item can be run after this time.
     * For a Queue.WaitingItem
     *
     * @return The time expressed in milliseconds after January 1, 1970, 0:00:00 GMT.
     */
    private Long timestamp;
}
