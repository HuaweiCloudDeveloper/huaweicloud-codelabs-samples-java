/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.appstage.demos.jenkinsadapter.dto.v9.pipeline;

import lombok.Data;
import org.springframework.validation.annotation.Validated;

import java.util.List;

/**
 * 功能描述 流水线列表接口反参
 *
 * @author l00572809
 * @since 2024/9/3
 */
@Validated
@Data
public class QueryPipelineListResponse {
    public Integer total;

    public List<PipelineInfo> data;
}
