package com.appstage.demos.jenkinsadapter.common;

import com.appstage.demos.jenkinsadapter.dto.AppstageUserResponse;
import com.appstage.demos.jenkinsadapter.service.AppstageClient;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = "/v1/adapter/*")
public class AuthenticationWebFilter extends OncePerRequestFilter {
    private static final String AUTH_HEADER = "x-auth-token";

    @Resource
    private AppstageClient appstageClient;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        if (request.getHeader(AUTH_HEADER) == null) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        AppstageUserResponse user = appstageClient.getUserInfo(request.getHeader(AUTH_HEADER));
        if (user != null) {
            RequestContext.init();
            RequestContext.setUser(user);
            chain.doFilter(request, response);
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    @Override
    public void destroy() {
        RequestContext.remove();
    }
}

