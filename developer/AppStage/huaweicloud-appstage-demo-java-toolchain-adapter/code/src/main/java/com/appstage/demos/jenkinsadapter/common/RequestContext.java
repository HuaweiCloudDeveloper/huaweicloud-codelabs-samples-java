package com.appstage.demos.jenkinsadapter.common;

import com.appstage.demos.jenkinsadapter.dto.AppstageUserResponse;

public class RequestContext {
    private static final ThreadLocal<CommonContextContainer> holder = new ThreadLocal<>();

    public static void init() {
        holder.set(new CommonContextContainer());
    }

    public static void setUser(AppstageUserResponse loginUser) {
        holder.get().setUser(loginUser);
    }

    public static void setServiceId(String serviceId) {
        holder.get().setServiceId(serviceId);
    }

    public static void setSignature(String signature) {
        holder.get().setSignature(signature);
    }

    public static void setSensitivePassword(String pwd) {
        holder.get().setSensitivePassword(pwd);
    }

    public static String getSensitivePassword() {
        return holder.get().getSensitivePassword();
    }

    public static void setSignPassword(String signPassword) {
        holder.get().setSignPassword(signPassword);
    }

    public static String getSignPassword() {
        return holder.get().getSignPassword();
    }

    public static String getSignature() {
        return holder.get().getSignature();
    }

    public static String getServiceId() {
        return holder.get().getServiceId();
    }

    public static AppstageUserResponse getUser() {
        return holder.get().getUser();
    }

    public static void remove() {
        holder.remove();
    }
}
