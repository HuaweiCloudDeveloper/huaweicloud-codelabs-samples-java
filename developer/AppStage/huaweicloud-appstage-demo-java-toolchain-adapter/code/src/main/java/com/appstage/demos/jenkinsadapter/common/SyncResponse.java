package com.appstage.demos.jenkinsadapter.common;

import lombok.Data;

@Data
public class SyncResponse {
    private String resultCode;
    private String resultMsg;

    private SyncResponse(String code, String msg) {
        this.resultCode = code;
        this.resultMsg = msg;
    }

    public static SyncResponse success() {
        return new SyncResponse("000000", "Success");
    }

    public static SyncResponse fail(String code, String msg) {
        return new SyncResponse(code, msg);
    }
}
