package com.appstage.demos.jenkinsadapter.common;

import org.apache.hc.client5.http.HttpResponseException;
import org.apache.hc.core5.annotation.Contract;
import org.apache.hc.core5.annotation.ThreadingBehavior;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.core5.http.Header;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.HttpException;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.hc.core5.http.io.HttpClientResponseHandler;
import org.apache.hc.core5.http.io.entity.EntityUtils;

import java.io.IOException;

@SuppressWarnings("unchecked")
@Contract(threading = ThreadingBehavior.STATELESS)
public class JenkinsHttpClientResponseHandler<T extends JenkinsResponse<?>> implements HttpClientResponseHandler<T> {

    @Override
    public T handleResponse(ClassicHttpResponse response) throws HttpException, IOException {
        final HttpEntity entity = response.getEntity();
        if (response.getCode() >= HttpStatus.SC_REDIRECTION) {
            EntityUtils.consume(entity);
            throw new HttpResponseException(response.getCode(), response.getReasonPhrase());
        }

        JenkinsResponse.JenkinsResponseBuilder<?> builder = JenkinsResponse.builder();
        if (entity != null) {
            Header locHeader = response.getFirstHeader("Location");
            if (locHeader != null) {
                builder.headerLocation(locHeader.getValue());
            }
            return (T) builder
                    .dataStr(EntityUtils.toString(entity))
                    .build();
        }

        return null;
    }
}
