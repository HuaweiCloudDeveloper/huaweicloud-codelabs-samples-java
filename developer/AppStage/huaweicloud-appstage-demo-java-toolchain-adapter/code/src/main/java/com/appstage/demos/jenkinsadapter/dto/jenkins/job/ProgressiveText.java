package com.appstage.demos.jenkinsadapter.dto.jenkins.job;

import lombok.Data;

@Data
public class ProgressiveText {
    private String text;

    private int size;

    private boolean hasMoreData;
}
