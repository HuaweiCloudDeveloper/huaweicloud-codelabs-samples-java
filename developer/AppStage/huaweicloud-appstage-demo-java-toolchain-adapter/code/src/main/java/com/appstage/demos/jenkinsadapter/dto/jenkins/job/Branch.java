package com.appstage.demos.jenkinsadapter.dto.jenkins.job;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Branch {
    @JsonProperty("SHA1")
    private String SHA1;

    private String name;
}
