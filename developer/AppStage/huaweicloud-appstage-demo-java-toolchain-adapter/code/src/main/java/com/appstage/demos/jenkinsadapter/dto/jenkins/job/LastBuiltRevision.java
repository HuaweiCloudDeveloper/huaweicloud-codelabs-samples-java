package com.appstage.demos.jenkinsadapter.dto.jenkins.job;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class LastBuiltRevision {
    @JsonProperty("SHA1")
    private String SHA1;

    private List<Branch> branch;
}
