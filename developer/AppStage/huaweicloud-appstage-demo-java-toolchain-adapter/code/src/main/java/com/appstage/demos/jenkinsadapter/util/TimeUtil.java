package com.appstage.demos.jenkinsadapter.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class TimeUtil {
    public static LocalDateTime ofMilliSecond(long milliSecond) {
        Instant instant = Instant.ofEpochMilli(milliSecond);
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
    }

    public static String formatMilliSecond(long milliSecond) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return ofMilliSecond(milliSecond).format(formatter);
    }
}
