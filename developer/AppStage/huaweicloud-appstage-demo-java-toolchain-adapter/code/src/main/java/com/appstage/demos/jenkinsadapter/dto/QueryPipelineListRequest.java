package com.appstage.demos.jenkinsadapter.dto;

import lombok.Data;

@Data
public class QueryPipelineListRequest {
    private Integer offset = 1;
    private Integer limit = 10;
}
