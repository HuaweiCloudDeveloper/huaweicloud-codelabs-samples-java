package com.appstage.demos.jenkinsadapter.dto.jenkins.common;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IntegerResponse {
    private Integer value;
}
