package com.appstage.demos.jenkinsadapter.dto.sync;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SignBody {
    private String body;
    private String timeStamp;
}
