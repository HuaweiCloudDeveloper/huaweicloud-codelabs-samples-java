package com.appstage.demos.jenkinsadapter.dto.jenkins.job;

import java.util.List;

public class ChangeSet {
    private List<String> affectedPaths;

    private String commitId;

    private long timestamp;

    private Culprit author;

    private String authorEmail;

    private String comment;
}
