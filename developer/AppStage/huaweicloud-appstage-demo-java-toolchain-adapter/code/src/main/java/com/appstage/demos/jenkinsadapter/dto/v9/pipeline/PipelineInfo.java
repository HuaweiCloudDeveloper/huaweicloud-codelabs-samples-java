/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.appstage.demos.jenkinsadapter.dto.v9.pipeline;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

/**
 * 功能描述 
 *
 * @author fwx1233990
 * @since 2023/8/21 0021
 */
@Validated
@Data
public class PipelineInfo {
    @JsonProperty("pipeline_id")
    public String pipelineId;

    @JsonProperty("pipeline_name")
    public String pipelineName;

    @JsonProperty("latest_run")
    public PipelineLatestRun latestRun;
}
