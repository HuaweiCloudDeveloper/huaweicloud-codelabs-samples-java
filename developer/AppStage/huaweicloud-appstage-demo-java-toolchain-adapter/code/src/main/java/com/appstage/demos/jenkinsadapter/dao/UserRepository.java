package com.appstage.demos.jenkinsadapter.dao;

import com.appstage.demos.jenkinsadapter.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    User findUserByAppstageUserId(String appStageUserId);
}
