package com.appstage.demos.jenkinsadapter.dto.sync;

import lombok.Data;

@Data
public class OrgSyncInfo {
    private String orgCode;
    private String orgName;
    private String parentCode;
}
