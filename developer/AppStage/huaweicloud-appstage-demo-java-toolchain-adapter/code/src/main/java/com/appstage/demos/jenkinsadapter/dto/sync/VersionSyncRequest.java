package com.appstage.demos.jenkinsadapter.dto.sync;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
public class VersionSyncRequest {
    private Version version;

    @JsonProperty("op_type")
    @JSONField(name = "op_type")
    private String opType;

    @JsonProperty("time_stamp")
    @JSONField(name = "time_stamp")
    private String timeStamp;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Version {
        @JsonProperty("version_id")
        @JSONField(name = "version_id")
        private String versionId;

        @JsonProperty("service_id")
        @JSONField(name = "service_id")
        private String serviceId;

        @JsonProperty("product_id")
        @JSONField(name = "product_id")
        private String productId;

        @JsonProperty("dept_id")
        @JSONField(name = "dept_id")
        private String deptId;

        @JsonProperty("version_name")
        @JSONField(name = "version_name")
        private String versionName;

        private String status;

        @JsonProperty("begin_date")
        @JSONField(name = "begin_date")
        private Date beginDate;

        @JsonProperty("end_date")
        @JSONField(name = "end_date")
        private Date endDate;
    }
}
