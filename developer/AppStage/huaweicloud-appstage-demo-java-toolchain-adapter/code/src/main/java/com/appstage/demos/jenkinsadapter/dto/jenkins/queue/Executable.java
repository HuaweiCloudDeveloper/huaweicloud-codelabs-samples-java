package com.appstage.demos.jenkinsadapter.dto.jenkins.queue;

import lombok.Data;

@Data
public class Executable {

    private Integer number;

    private String url;
}
