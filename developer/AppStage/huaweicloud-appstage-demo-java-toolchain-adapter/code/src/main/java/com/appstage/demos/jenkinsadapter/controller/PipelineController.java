package com.appstage.demos.jenkinsadapter.controller;

import com.appstage.demos.jenkinsadapter.dto.QueryPipelineListRequest;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.QueryCodeCheckTaskResponse;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.QueryPipeLineDetailResponse;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.QueryPipelineArtifactResponse;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.QueryPipelineListResponse;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.QueryPipelineRunDetailResponse;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.RunPipelineResponse;
import com.appstage.demos.jenkinsadapter.dto.v9.pipeline.StopPipelineResponse;
import com.appstage.demos.jenkinsadapter.service.JenkinsService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Optional;

@RestController
public class PipelineController {

    @Resource
    private JenkinsService jenkinsService;

    @PostMapping(value = "/v1/adapter/{service_id}/pipeline/list", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public QueryPipelineListResponse getPipelineList(@PathVariable("service_id") String serviceId, @RequestBody QueryPipelineListRequest request) {
        return jenkinsService.queryPipelineList(Optional.ofNullable(request).map(QueryPipelineListRequest::getOffset).orElse(0) ,
                Optional.ofNullable(request).map(QueryPipelineListRequest::getLimit).orElse(10));
    }

    @PostMapping(value = "/v1/adapter/{service_id}/pipeline/recent-run-list", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public QueryPipelineListResponse getRecentRunPipelineList(@PathVariable("service_id") String serviceId, @RequestBody QueryPipelineListRequest request) {
        return jenkinsService.queryRecentRunPipelineList(Optional.ofNullable(request).map(QueryPipelineListRequest::getOffset).orElse(0),
                Optional.ofNullable(request).map(QueryPipelineListRequest::getLimit).orElse(5));
    }

    @GetMapping(value = "/v1/adapter/{service_id}/pipeline/{pipeline_id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public QueryPipeLineDetailResponse getPipelineDetail(@PathVariable("service_id") String serviceId, @PathVariable("pipeline_id") String pipelineId) {
        return jenkinsService.queryPipelineDetail(pipelineId);
    }

    @PostMapping(value = "/v1/adapter/{service_id}/pipeline/{pipeline_id}/run", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RunPipelineResponse runPipeline(@PathVariable("service_id") String serviceId, @PathVariable("pipeline_id") String pipelineId) throws InterruptedException {
        return jenkinsService.runPipeline(pipelineId);
    }

    @GetMapping(value = "/v1/adapter/{service_id}/pipeline/{pipeline_id}/run", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public QueryPipelineRunDetailResponse getPipelineRunDetail(@PathVariable("service_id") String serviceId, @PathVariable("pipeline_id") String pipelineId, @RequestParam("pipeline_run_id") Integer runId) {
        return jenkinsService.queryPipelineRunDetail(pipelineId, runId);
    }

    @PostMapping(value = "/v1/adapter/{service_id}/pipeline/{pipeline_id}/run/{run_id}/stop", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public StopPipelineResponse runPipeline(@PathVariable("service_id") String serviceId, @PathVariable("pipeline_id") String pipelineId, @PathVariable("run_id") Integer runId) throws InterruptedException {
        return jenkinsService.stopPipelineRun(pipelineId, runId);
    }

    @GetMapping(value = "/v1/adapter/{service_id}/pipeline/{pipeline_id}/run/{run_id}/artifact", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public QueryPipelineArtifactResponse getPipelineRunArtifact(@PathVariable("service_id") String serviceId, @PathVariable("pipeline_id") String pipelineId, @PathVariable("run_id") Integer runId) {
        return jenkinsService.queryPipelineRunArtifact(pipelineId, runId);
    }

    @GetMapping(value = "/v1/adapter/{service_id}/pipeline/{pipeline_id}/run/{run_id}/code-check", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public QueryCodeCheckTaskResponse getPipelineRunCodeCheck(@PathVariable("service_id") String serviceId, @PathVariable("pipeline_id") String pipelineId, @PathVariable("run_id") Integer runId) {
        return jenkinsService.queryCodeCheckTask(pipelineId, runId);
    }
}
