package com.appstage.demos.jenkinsadapter.controller;

import com.appstage.demos.jenkinsadapter.common.RequestContext;
import com.appstage.demos.jenkinsadapter.common.SyncResponse;
import com.appstage.demos.jenkinsadapter.dto.sync.AllOrgSyncInfo;
import com.appstage.demos.jenkinsadapter.dto.sync.ApplicationSyncInfo;
import com.appstage.demos.jenkinsadapter.dto.sync.AuthSyncInfo;
import com.appstage.demos.jenkinsadapter.dto.sync.MicroserviceSyncInfo;
import com.appstage.demos.jenkinsadapter.dto.sync.ProductSyncInfo;
import com.appstage.demos.jenkinsadapter.dto.sync.ServiceSyncInfo;
import com.appstage.demos.jenkinsadapter.dto.sync.SingleOrgSyncInfo;
import com.appstage.demos.jenkinsadapter.dto.sync.TenantSyncInfo;
import com.appstage.demos.jenkinsadapter.dto.sync.VersionSyncRequest;
import com.appstage.demos.jenkinsadapter.service.SyncService;
import com.appstage.demos.jenkinsadapter.util.SignUtil;
import com.appstage.demos.jenkinsadapter.util.VerifyUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Slf4j
@RestController(value = "")
public class SyncController {
    @Resource
    private SyncService syncService;

    private SyncResponse handle(String type, Object body) {
        if (VerifyUtil.verifySyncSign(body, RequestContext.getSignature(), RequestContext.getSignPassword())) {
            if (body instanceof ApplicationSyncInfo) {
                String rawStr = VerifyUtil.decryptByRsa(((ApplicationSyncInfo) body).getClientSecret(), RequestContext.getSensitivePassword());
                log.info("before decrypt {}, after decrypt {}", ((ApplicationSyncInfo) body).getClientSecret(), rawStr);
                ((ApplicationSyncInfo) body).setClientSecret(rawStr);
            }
            syncService.insertSyncLog(type, body);
            return SyncResponse.success();
        }

        return SyncResponse.fail("400001", "Signature verification failed");
    }

    @PostMapping(value = "/sync/v1/version", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SyncResponse syncVersions(@RequestBody VersionSyncRequest version) {
        log.info("sync version");
        return handle("Sync Version", SignUtil.buildSignBody(version, version.getTimeStamp()));
    }

    @PostMapping(value = "/sync/v1/product", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SyncResponse syncProducts(@RequestBody ProductSyncInfo product) {
        log.info("sync product");
        return handle("Sync Product", SignUtil.buildSignBody(product, product.getTimeStamp()));
    }

    @PostMapping(value = "/sync/v1/service", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SyncResponse syncServices(@RequestBody ServiceSyncInfo service) {
        log.info("sync service");
        return handle("Sync Service", SignUtil.buildSignBody(service, service.getTimeStamp()));
    }

    @PostMapping(value = "/sync/v1/micservice", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SyncResponse syncMicservice(@RequestBody MicroserviceSyncInfo micro) {
        log.info("sync microservice");
        return handle("Sync Microservice", SignUtil.buildSignBody(micro, micro.getTimeStamp()));
    }

    @PostMapping(value = "/wisedev/sync/produceAPI/tenantSync", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SyncResponse syncTenants(@RequestBody TenantSyncInfo tenantSyncInfo) {
        log.info("sync tenants");
        return handle("Sync Tenant", tenantSyncInfo);
    }

    @PostMapping(value = "/wisedev/sync/produceAPI/applicationSync", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SyncResponse syncApplications(@RequestBody ApplicationSyncInfo applicationSyncInfo) {
        log.info("sync applications");
        return handle("Sync Application", applicationSyncInfo);
    }

    @PostMapping(value = "/wisedev/sync/produceAPI/authSync", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SyncResponse syncAuths(@RequestBody AuthSyncInfo authSyncInfo) {
        log.info("sync auth");
        return handle("Sync Auth", authSyncInfo);
    }

    @PostMapping(value = "/wisedev/sync/produceAPI/singleOrgSync", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SyncResponse syncSingleOrg(@RequestBody SingleOrgSyncInfo singleOrgSyncInfo) {
        log.info("sync single org");
        return handle("Sync Single Org", singleOrgSyncInfo);
    }

    @PostMapping(value = "/wisedev/sync/produceAPI/allOrgSync", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SyncResponse syncAllOrgs(@RequestBody AllOrgSyncInfo allOrgSyncInfo) {
        log.info("sync all orgs");
        return handle("Sync All Orgs", allOrgSyncInfo);
    }
}
