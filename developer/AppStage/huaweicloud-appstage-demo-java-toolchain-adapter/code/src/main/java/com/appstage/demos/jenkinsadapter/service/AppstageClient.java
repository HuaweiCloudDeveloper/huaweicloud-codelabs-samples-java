package com.appstage.demos.jenkinsadapter.service;

import com.appstage.demos.jenkinsadapter.dto.AppstageUserResponse;
import com.appstage.demos.jenkinsadapter.util.AppstageUrl;
import com.appstage.demos.jenkinsadapter.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.BasicHttpClientResponseHandler;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManagerBuilder;
import org.apache.hc.client5.http.ssl.NoopHostnameVerifier;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactoryBuilder;
import org.apache.hc.client5.http.ssl.TrustAllStrategy;
import org.apache.hc.core5.ssl.SSLContextBuilder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

@Slf4j
@Service
public class AppstageClient {
    private static CloseableHttpClient httpClient;

    static {
        try {
            httpClient = HttpClients.custom()
                    .setConnectionManager(PoolingHttpClientConnectionManagerBuilder.create()
                            .setSSLSocketFactory(SSLConnectionSocketFactoryBuilder.create()
                                    .setSslContext(SSLContextBuilder.create()
                                            .loadTrustMaterial(TrustAllStrategy.INSTANCE)
                                            .build())
                                    .setHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                                    .build())
                            .build())
                    .build();
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            throw new RuntimeException(e);
        }
    }

    public AppstageUserResponse getUserInfo(String token) {
        HttpGet httpGet = new HttpGet(AppstageUrl.HOST + AppstageUrl.OAUTH2_USER_INFO);
        httpGet.addHeader("Authorization", "Bearer " + token);
        try {
             String rspStr = httpClient.execute(httpGet, new BasicHttpClientResponseHandler());
             return JsonUtil.fromString(rspStr, AppstageUserResponse.class);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }
}
