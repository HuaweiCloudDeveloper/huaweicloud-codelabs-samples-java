/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.appstage.demos.jenkinsadapter.dto.sync;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class ServiceSyncInfo {
    private Service service;

    @JsonProperty("op_type")
    @JSONField(name = "op_type")
    private String opType;

    @JsonProperty("time_stamp")
    @JSONField(name = "time_stamp")
    private String timeStamp;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Service {
        @JsonProperty("service_code")
        @JSONField(name = "service_code")
        private String serviceCode;

        @JsonProperty("service_id")
        @JSONField(name = "service_id")
        private String serviceId;

        @JsonProperty("product_id")
        @JSONField(name = "product_id")
        private String productId;

        @JsonProperty("dept_id")
        @JSONField(name = "dept_id")
        private String deptId;

        @JsonProperty("service_name")
        @JSONField(name = "service_name")
        private String serviceName;

        private String status;
    }
}
