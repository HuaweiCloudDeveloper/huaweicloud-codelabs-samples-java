package com.appstage.demos.jenkinsadapter.common;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JenkinsResponse<T> {
    private T data;
    private String dataStr;
    private String headerLocation;
}
