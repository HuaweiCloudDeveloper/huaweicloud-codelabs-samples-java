package com.appstage.demos.jenkinsadapter.common;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class AdapterResponse<T> {
    private int code;
    private String message;
    private T data;
}
