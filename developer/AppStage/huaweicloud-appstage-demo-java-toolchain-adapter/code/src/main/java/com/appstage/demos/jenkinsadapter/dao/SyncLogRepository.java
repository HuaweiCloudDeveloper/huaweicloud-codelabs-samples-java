package com.appstage.demos.jenkinsadapter.dao;

import com.appstage.demos.jenkinsadapter.model.SyncLog;
import org.springframework.data.repository.CrudRepository;

public interface SyncLogRepository extends CrudRepository<SyncLog, Long> {
}
