package com.appstage.demos.jenkinsadapter.dto.jenkins.job;

import lombok.Data;

@Data
public class Stage {
    private String id;

    private String name;

    private String status;

    private long startTimeMillis;

    private long pauseDurationMillis;

    private long durationMillis;
}
