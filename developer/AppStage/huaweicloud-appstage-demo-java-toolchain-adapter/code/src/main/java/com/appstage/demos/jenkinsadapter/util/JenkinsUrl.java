package com.appstage.demos.jenkinsadapter.util;

public class JenkinsUrl {
    public static final String JENKINS_HOST = "http://localhost:8080";
    public static final String JOB_LIST = "/api/json";
    public static final String JOB_LAST_BUILD = "/job/%s/lastBuild/api/json";
    public static final String JOB_LAST_BUILD_STEPS = "/job/%s/lastBuild/wfapi/describe";
    public static final String JOB_RUN_STEPS = "/job/%s/%d/wfapi/describe";
    public static final String JOB_DETAIL = "/job/%s/api/json";
    public static final String JOB_BUILD = "/job/%s/build";
    public static final String JOB_RUN_LIST = "/job/%s/wfapi/runs";
    public static final String JOB_RUN_WITH_PARAMS = "/job/%s/buildWithParameters";
    public static final String JOB_RUN_DETAIL = "/job/%s/%d/api/json";
    public static final String JOB_RUN_STOP = "/job/%s/%d/stop/api/json";
    public static final String JOB_ARTIFACTS = "/job/%s/%d/artifact/%s";
}
