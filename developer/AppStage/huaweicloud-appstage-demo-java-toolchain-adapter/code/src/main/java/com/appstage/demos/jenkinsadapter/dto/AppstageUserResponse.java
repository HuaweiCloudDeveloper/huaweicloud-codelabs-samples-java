package com.appstage.demos.jenkinsadapter.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AppstageUserResponse {
    @JsonProperty("tenant_name")
    private String tenantName;

    @JsonProperty("role")
    private String role;

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("name")
    private String name;

    @JsonProperty("mobile")
    private String mobile;

    @JsonProperty("email")
    private String email;

    @JsonProperty("tenant")
    private String tenant;
}
