package com.appstage.demos.jenkinsadapter.dto.jenkins.job;

import lombok.Data;

import java.util.List;

@Data
public class StageFlowNode {
    private String name;

    private String status;

    private long startTimeMillis;

    private long durationTimeMillis;

    private List<Long> parentNodes;
}
