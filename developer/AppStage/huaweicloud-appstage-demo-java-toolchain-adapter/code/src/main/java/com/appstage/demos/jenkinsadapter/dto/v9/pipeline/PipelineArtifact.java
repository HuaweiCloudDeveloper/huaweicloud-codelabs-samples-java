/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.appstage.demos.jenkinsadapter.dto.v9.pipeline;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * 流水线产物对象信息
 *
 * @author t00771521
 * @since 2024/2/18
 */
@Data
public class PipelineArtifact {
    private String name;

    @JsonProperty("file_size")
    private String fileSize;

    @JsonProperty("hash_code")
    private List<ArtifactHashCode> hashCodes;

    @JsonProperty("upload_target")
    private String uploadTarget;

    @JsonProperty("upload_location")
    private String uploadLocation;

    @JsonProperty("artifact_uri")
    private String artifactUri;
}
