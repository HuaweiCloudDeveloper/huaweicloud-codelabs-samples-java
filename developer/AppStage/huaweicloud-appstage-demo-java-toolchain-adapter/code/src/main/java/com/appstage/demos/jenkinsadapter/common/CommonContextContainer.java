package com.appstage.demos.jenkinsadapter.common;

import com.appstage.demos.jenkinsadapter.dto.AppstageUserResponse;
import lombok.Data;

@Data
public class CommonContextContainer {
    private AppstageUserResponse user;
    private String serviceId;
    private String signature;
    private String signPassword;
    private String sensitivePassword;
}
