/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.appstage.demos.jenkinsadapter.dto.v9.pipeline;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 查询流水线详情返回参数
 *
 * @author l00572809
 * @since 2024-09-03
 */
@Data
public class QueryPipeLineDetailResponse {
    @JsonProperty("pipeline_id")
    private String pipelineId;

    @JsonProperty("pipeline_name")
    private String pipelineName;
}
