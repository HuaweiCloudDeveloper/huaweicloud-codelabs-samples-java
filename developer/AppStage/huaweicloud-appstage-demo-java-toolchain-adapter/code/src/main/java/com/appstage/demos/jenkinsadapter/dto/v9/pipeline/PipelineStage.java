/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.appstage.demos.jenkinsadapter.dto.v9.pipeline;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

/**
 * 功能描述 流水线阶段信息
 *
 * @author l00572809
 * @since 2024/09/03
 */
@Validated
@Data
public class PipelineStage {
    public String name;

    public Integer sequence;

    public String status;

    @JsonProperty("start_time")
    public String startTime;

    @JsonProperty("end_time")
    public String endTime;

}
