package com.appstage.demos.jenkinsadapter.dto.jenkins.job;

import lombok.Data;

@Data
public class Job {
    private String clazz;

    private String name;

    private String url;

    private String color;
}
