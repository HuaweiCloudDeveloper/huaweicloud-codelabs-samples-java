/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.appstage.demos.jenkinsadapter.dto.v9.pipeline;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import java.util.List;

/**
 * 功能描述
 *
 * @author fwx1233990
 * @since 2023/8/21 0021
 */
@Validated
@Data
public class PipelineLatestRun {
    @JsonProperty("executor_id")
    public String executorId;

    @JsonProperty("executor_name")
    public String executorName;

    @JsonProperty("stage_status_list")
    public List<PipelineStage> stageStatusList;

    public String status;

    @JsonProperty("trigger_type")
    public String triggerType;

    @JsonProperty("build_params")
    public PipelineSource buildParams;

    @JsonProperty("start_time")
    public Long startTime;

    @JsonProperty("end_time")
    public Long endTime;
}
