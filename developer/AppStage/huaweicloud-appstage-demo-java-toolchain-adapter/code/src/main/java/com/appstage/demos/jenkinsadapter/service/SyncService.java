package com.appstage.demos.jenkinsadapter.service;

import com.appstage.demos.jenkinsadapter.dao.SyncLogRepository;
import com.appstage.demos.jenkinsadapter.model.SyncLog;
import com.appstage.demos.jenkinsadapter.util.JsonUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SyncService {
    @Resource
    private SyncLogRepository syncLogRepository;

    public void insertSyncLog(String type, Object content) {
        SyncLog syncLog = new SyncLog();
        syncLog.setType(type);
        syncLog.setContent(JsonUtil.toString(content));
        syncLogRepository.save(syncLog);
    }
}
