package com.appstage.demos.jenkinsadapter.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table(name = "t_adapter_user")
public class User implements Serializable {

    @Id
    @Column(name = "appstage_user_id", nullable = false)
    private String appstageUserId;

    @Column(name = "appstage_user_name")
    private String appstageUserName;

    @Column(name = "appstage_user_email")
    private String appstageUserEmail;

    @Column(name = "appstage_tenant_id")
    private String appstageTenantId;

    @Column(name = "jenkins_api_token")
    private String jenkinsApiToken;
}
