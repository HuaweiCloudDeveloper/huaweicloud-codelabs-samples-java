package com.appstage.demos.jenkinsadapter.common;

public class AdapterException extends Exception {
    public AdapterException(String msg) {
        super(msg);
    }
}
