package com.appstage.demos.jenkinsadapter.dto.jenkins.job;

import lombok.Data;

@Data
public class Parameter {
    private String clazz;

    private String name;

    private String value;
}
