package com.appstage.demos.jenkinsadapter.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = {"/sync/v1/*", "/wisedev/sync/*"})
public class SignVerifyWebFilter extends OncePerRequestFilter {
    private static final String SIGN_STR_HEADER = "authToken";

    @Value("${sync.signature.access-key}")
    private String ACCESS_KEY;

    @Value("${sync.signature.sensitive-key}")
    private String SENSITIVE_KEY;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (request.getHeader(SIGN_STR_HEADER) == null) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        RequestContext.init();
        RequestContext.setSignature(request.getHeader(SIGN_STR_HEADER));
        RequestContext.setSignPassword(ACCESS_KEY);
        RequestContext.setSensitivePassword(SENSITIVE_KEY);
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        RequestContext.remove();
    }
}
