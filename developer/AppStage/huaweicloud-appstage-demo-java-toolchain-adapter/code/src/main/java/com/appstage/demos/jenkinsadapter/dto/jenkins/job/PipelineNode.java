package com.appstage.demos.jenkinsadapter.dto.jenkins.job;

import lombok.Data;

import java.util.List;

@Data
public class PipelineNode {
    private String name;

    private String status;

    private long startTimeMillis;

    private long durationTimeMillis;

    private List<StageFlowNode> stageFlowNodes;
}
