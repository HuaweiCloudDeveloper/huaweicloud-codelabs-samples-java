package com.appstage.demos.jenkinsadapter.dto.jenkins.common;

import lombok.Data;

@Data
public class BooleanResponse {
    private Boolean value;
}
