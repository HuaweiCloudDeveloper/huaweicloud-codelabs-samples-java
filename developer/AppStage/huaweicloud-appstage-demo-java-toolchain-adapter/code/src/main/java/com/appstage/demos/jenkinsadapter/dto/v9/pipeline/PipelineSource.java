/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.appstage.demos.jenkinsadapter.dto.v9.pipeline;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 流水线运行源信息
 *
 * @author l00572809
 * @since 2024-09-03
 */
@Data
public class PipelineSource {
    @JsonProperty("target_branch")
    private String targetBranch;

    @JsonProperty("repo_id")
    private String repoId;

    @JsonProperty("git_url")
    private String gitUrl;

    @JsonProperty("repo_name")
    private String repoName;

    @JsonProperty("commit_id")
    private String commitId;
}
