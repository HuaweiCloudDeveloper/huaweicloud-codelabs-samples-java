/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.appstage.demos.jenkinsadapter.dto.v9.pipeline;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class QueryCodeCheckTaskResponse {
    @JsonProperty("check_task_id")
    private String checkTaskId;
}
