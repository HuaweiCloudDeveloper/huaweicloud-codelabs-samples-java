package com.appstage.demos.jenkinsadapter.service;

import com.appstage.demos.jenkinsadapter.common.AdapterException;
import com.appstage.demos.jenkinsadapter.common.JenkinsHttpClientResponseHandler;
import com.appstage.demos.jenkinsadapter.common.JenkinsResponse;
import com.appstage.demos.jenkinsadapter.common.RequestContext;
import com.appstage.demos.jenkinsadapter.dao.UserRepository;
import com.appstage.demos.jenkinsadapter.dto.jenkins.common.BooleanResponse;
import com.appstage.demos.jenkinsadapter.dto.jenkins.common.IntegerResponse;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.BuildInfo;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.JobInfo;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.JobList;
import com.appstage.demos.jenkinsadapter.dto.jenkins.job.Workflow;
import com.appstage.demos.jenkinsadapter.dto.jenkins.queue.QueueItem;
import com.appstage.demos.jenkinsadapter.model.User;
import com.appstage.demos.jenkinsadapter.util.JenkinsUrl;
import com.appstage.demos.jenkinsadapter.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManagerBuilder;
import org.apache.hc.client5.http.ssl.NoopHostnameVerifier;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactoryBuilder;
import org.apache.hc.client5.http.ssl.TrustAllStrategy;
import org.apache.hc.core5.http.ClassicHttpRequest;
import org.apache.hc.core5.ssl.SSLContextBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Optional;

@Slf4j
@Service
public class JenkinsClient {
    private static final int MAX_RETRIES = 15;

    @Resource
    private UserRepository userRepository;

    public JobList getJobList() {
        return doGet(JenkinsUrl.JENKINS_HOST + JenkinsUrl.JOB_LIST, JobList.class).getData();
    }

    public BuildInfo getLastBuild(String jobName) {
        return doGet(JenkinsUrl.JENKINS_HOST + String.format(JenkinsUrl.JOB_LAST_BUILD, jobName), BuildInfo.class).getData();
    }

    public Workflow getLastBuildWorkflow(String jobName) {
        return doGet(JenkinsUrl.JENKINS_HOST + String.format(JenkinsUrl.JOB_LAST_BUILD_STEPS, jobName), Workflow.class).getData();
    }

    public Workflow getJobRunWorkflow(String jobName, Integer buildNumber) {
        return doGet(JenkinsUrl.JENKINS_HOST + String.format(JenkinsUrl.JOB_RUN_STEPS, jobName, buildNumber), Workflow.class).getData();
    }

    public JobInfo getJobInfo(String jobName) {
        return doGet(JenkinsUrl.JENKINS_HOST + String.format(JenkinsUrl.JOB_DETAIL, jobName), JobInfo.class).getData();
    }

    public IntegerResponse runJob(String jobName) throws InterruptedException {
        JenkinsResponse<IntegerResponse> response = doPost(JenkinsUrl.JENKINS_HOST + String.format(JenkinsUrl.JOB_BUILD, jobName), IntegerResponse.class);
        String queueUrl = response.getHeaderLocation();
        if (StringUtils.isBlank(queueUrl)) {
            return null;
        }
        int tryCount = 1;
        while (tryCount <= MAX_RETRIES) {
            Thread.sleep(1000);
            tryCount++;
            JenkinsResponse<QueueItem> queueItemRsp = execute(new HttpGet(queueUrl + "api/json"), QueueItem.class);

            if (queueItemRsp != null && queueItemRsp.getData() != null
                    && queueItemRsp.getData().getExecutable() != null
                    && queueItemRsp.getData().getExecutable().getNumber() != null) {
                return IntegerResponse.builder().value(queueItemRsp.getData().getExecutable().getNumber()).build();
            }
        }
        return null;
    }

    public BooleanResponse stopRunJob(String jobName, Integer runId) {
        return doPost(JenkinsUrl.JENKINS_HOST + String.format(JenkinsUrl.JOB_RUN_STOP, jobName, runId), BooleanResponse.class).getData();
    }

    public BuildInfo getJobRunInfo(String jobName, Integer buildNumber) {
        return doGet(JenkinsUrl.JENKINS_HOST + String.format(JenkinsUrl.JOB_RUN_DETAIL, jobName, buildNumber), BuildInfo.class).getData();
    }

    public IntegerResponse runJobWithParams(String jobName) {
        return doPost(JenkinsUrl.JENKINS_HOST + String.format(JenkinsUrl.JOB_RUN_WITH_PARAMS, jobName), IntegerResponse.class).getData();
    }

    private String getApiToken() {
        User user = userRepository.findUserByAppstageUserId(RequestContext.getUser().getUserId());
        return Optional.ofNullable(user).orElse(new User()).getJenkinsApiToken();
    }

    private String getUserEmail() {
        User user = userRepository.findUserByAppstageUserId(RequestContext.getUser().getUserId());
        return Optional.ofNullable(user).orElse(new User()).getAppstageUserEmail();
    }

    private <T> JenkinsResponse<T> doGet(String url, Class<T> rspClz) {
        HttpGet httpGet = new HttpGet(url);
        return execute(httpGet, rspClz);
    }

    private <T> JenkinsResponse<T> doPost(String url, Class<T> rspClz) {
        HttpPost httpPost = new HttpPost(url);
        return execute(httpPost, rspClz);
    }

    private <T> JenkinsResponse<T> execute(ClassicHttpRequest req, Class<T> rspClz) {
        req.addHeader("Authorization", getBasicAuth());
        try (CloseableHttpClient client = client()) {
            JenkinsResponse<T> rsp = client.execute(req, new JenkinsHttpClientResponseHandler<JenkinsResponse<T>>());
            rsp.setData(JsonUtil.fromString(rsp.getDataStr(), rspClz));
            return rsp;
        } catch (IOException | AdapterException e) {
            log.error(e.getMessage());
            return (JenkinsResponse<T>) JenkinsResponse.builder().build();
        }
    }

    private String getBasicAuth() {
        String auth = getUserEmail() + ":" + getApiToken();
        return "Basic " + Base64.getEncoder().encodeToString(auth.getBytes());
    }

    private CloseableHttpClient client() throws AdapterException {
        try {
            return HttpClients.custom()
                    .setConnectionManager(PoolingHttpClientConnectionManagerBuilder.create()
                            .setSSLSocketFactory(SSLConnectionSocketFactoryBuilder.create()
                                    .setSslContext(SSLContextBuilder.create()
                                            .loadTrustMaterial(TrustAllStrategy.INSTANCE)
                                            .build())
                                    .setHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                                    .build())
                            .build())
                    .build();
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            log.info("error to create http client for jenkins");
        }
        throw new AdapterException("failed to create http client for jenkins");
    }
}
