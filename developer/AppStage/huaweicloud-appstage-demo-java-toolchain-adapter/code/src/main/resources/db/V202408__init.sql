CREATE TABLE IF NOT EXISTS `t_adapter_user`(
    `appstage_user_id`   VARCHAR(128)   NOT NULL COMMENT 'appstage 用户id',
    `appstage_user_name`   VARCHAR(128)   NOT NULL COMMENT 'appstage 用户名',
    `appstage_user_email`   VARCHAR(128)   NOT NULL COMMENT 'appstage 用户email',
    `appstage_tenant_id`   VARCHAR(128)   NOT NULL COMMENT 'appstage 租户id',
    `jenkins_api_token` VARCHAR(128)  NOT NULL COMMENT 'jenkins apiToken',
    `create_time`   TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP                             COMMENT '创建时间',
    `modify_time`   TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
    PRIMARY KEY (`appstage_user_id`),
    KEY `idx_appstage_user_id` (`appstage_user_id`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '开发中心租户集成工具链类型表'
  ROW_FORMAT = Dynamic;