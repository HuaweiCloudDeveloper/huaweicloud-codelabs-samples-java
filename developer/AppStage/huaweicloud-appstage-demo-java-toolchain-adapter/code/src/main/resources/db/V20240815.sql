CREATE TABLE IF NOT EXISTS `t_adapter_sync_log`(
    `id`            BIGINT      NOT NULL AUTO_INCREMENT,
    `type`          VARCHAR(32) NOT NULL,
    `content`       LONGTEXT    NOT NULL,
    `sync_time`     TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP  COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '同步集成记录' ROW_FORMAT = Dynamic;