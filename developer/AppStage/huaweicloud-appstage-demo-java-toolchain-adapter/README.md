## 1、示例简介
本示例是华为云开发者团队基于AppStage项目技术支持实践，采用SpringBoot服务架构，结合AppStage能力开发的开源项目，旨在为企业级开发者提供使用AppStage工具链适配能力，将服务开发、构建、上云的技术参考。

## 2、开发前准备
demo开发前资源准备、开发、构建、部署流程可以参考连接：https://support.huaweicloud.com/bestpractice-appstage/appstage_09_0018.html

项目运行需要执行机上存在以下环境变量：
* `MYSQL_IP`: MySql数据库地址
* `MYSQL_USER`: MySql数据库用户名
* `MYSQL_PWD`: MySql数据库密码
* `ACCESS_KEY`: 机机接口验签密码
* `SENSITIVE_KEY`: 机机接口敏感信息界面私钥（RSA私钥）

## 3、架构图
人机接口时序图
![人机接口时序图](assets/适配层人机接口时序图.png)

机机接口时序图
![机机接口时序图](assets/适配层机机接口时序图.png)

## 4、功能模块介绍
```
huaweicloud-appstage-demo-java-codeHub
├── code
│	├──src                                                   --代码目录
│	│	├──main
│	│	│	├──java                                          --java源码目录
│	│	│	├──resources                                     --资源文件目录
│	│	│	│	├──db                                        --数据库脚本目录
│	│	│	│	├──application.yml                           --Spring配置文件
│	├──.gitignore                                            --git提交忽略文件
│	└──pom.xml                                               --项目定义文件
└── README.md
```

详细信息可参考https://support.huaweicloud.com/bestpractice-appstage/appstage_09_0018.html

## 5、技术选型<自定义章节>
```
技术	说明	官网
Spring Boot	服务框架	https://spring.io/projects/spring-boot
Spring Data JPA	ORM框架	https://spring.io/projects/spring-data-jpa
Mysql	云数据库RDS	https://support.huaweicloud.com/rds/index.html
Lombok	简化对象封装工具	https://github.com/rzwitserloot/lombok
HttpClient5 Http客户端 https://hc.apache.org/httpcomponents-client-5.4.x/index.html
Flyway  数据库脚本版本管理框架 https://github.com/flyway/flyway
MapStruct 对象映射框架 https://mapstruct.org/documentation/reference-guide/
```

## 6、参考链接
华为云AppStage开发指导
https://support.huaweicloud.com/bestpractice-appstage/appstage_09_0018.html
