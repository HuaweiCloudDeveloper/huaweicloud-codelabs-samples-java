/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.demo.serviceb.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;

import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.huawei.demo.common.domain.BaseResponse;
import com.huawei.demo.serviceb.pojo.OfferingInfo;
import com.huawei.demo.serviceb.service.OfferingService;

/**
 * 商品对外接口
 *
 * @since 2023-12-06
 */
@RestController
@RequestMapping("/offering")
public class OfferingController {
    @Autowired
    private OfferingService offeringService;

    @GetMapping("/list")
    public BaseResponse<List<OfferingInfo>> getAllOrder(@RequestParam(value = "offset") @Max(1000) Long offset,
                                                        @RequestParam(value = "limit") @Max(100) Integer limit,
                                                        @RequestParam(value = "offeringName", required = false) String offeringName) {
        return offeringService.getAllOffering(offeringName, offset, limit);
    }

    @PostMapping("/add")
    public BaseResponse<String> addOffering(@RequestBody @Valid OfferingInfo offeringInfo) {
        offeringService.addOffering(offeringInfo);
        return BaseResponse.success("success");
    }

    @DeleteMapping("/delete")
    public BaseResponse<String> deleteOffering(@RequestParam(value = "offeringId") @Length(max = 32) String offeringId) {
        offeringService.deleteOffering(offeringId);
        return BaseResponse.success("success");
    }
}
