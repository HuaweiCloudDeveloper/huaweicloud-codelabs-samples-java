/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.demo.serviceb.config;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import com.huawei.nuwa.map.client.NuwaMapClient;
import com.huawei.nuwa.map.spring.boot.autoconfigure.NuwaCloudMapAutoConfiguration;
import com.huawei.rainbow.jdbc.DbGroupDataSource;
import com.huawei.wisesecurity.sts.springboot.security.configuration.StsEncryptablePropertiesConfiguration;

import lombok.extern.slf4j.Slf4j;

/**
 * rainbow启动
 */
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
@Import(value = {NuwaCloudMapAutoConfiguration.class, StsEncryptablePropertiesConfiguration.class})
@Slf4j
public class ApplicationAutoConfig implements EnvironmentAware {
    private Environment environment;

    /**
     * serviceB rainbow需要依赖sts和CloudMap启动，创建dataSource
     *
     * @param client NuwaMapClient值对象
     * @return DataSource 初始化完成的数据源数据源
     */
    @Bean
    @DependsOn({"stsBootStrap"})
    public DataSource createDataSource(NuwaMapClient client) throws SQLException {
        log.info("DemoServiceB init dataSource");
        DbGroupDataSource dataSource = new DbGroupDataSource();
        dataSource.setUseSts(true);
        String dataSourceName = environment.getProperty("wiseDba.dataSourceName");
        if (!StringUtils.isEmpty(dataSourceName)) {
            // wiseDba配置
            dataSource.setDataSourceName(environment.getProperty("wiseDba.dataSourceName"));
        } else {
            // 直连数据库配置
            log.info("DemoServiceB Directly connected database");
            dataSource.setInstanceName(environment.getProperty("wiseDba.instanceName"));
            dataSource.setDbName(environment.getProperty("wiseDba.dbName"));
            dataSource.setUserName(environment.getProperty("wiseDba.userName"));
            dataSource.setEncStsPasswd(environment.getProperty("wiseDba.encStsPasswd"));
            dataSource.setServers(environment.getProperty("wiseDba.servers"));
            String passwordCoderClass = environment.getProperty("wiseDba.passwordCoderClass");
            if (!StringUtils.isEmpty(passwordCoderClass)) {
                log.info("DemoServiceB passwordCoderClass is empty, use default coder class");
                dataSource.setPasswordCoderClass(environment.getProperty("wiseDba.passwordCoderClass"));
            }
        }
        dataSource.init();
        log.info("DemoServiceB init dataSource end");
        return dataSource;
    }

    @Bean
    public JdbcTemplate createJdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    /**
     * 事务管理创建
     *
     * @param dataSource 数据源
     * @return DataSourceTransactionManager 事务管理
     */
    @Bean
    public DataSourceTransactionManager createTransaction(DataSource dataSource) {
        log.info("DemoServiceB init transaction");
        DataSourceTransactionManager transaction = new DataSourceTransactionManager();
        transaction.setDataSource(dataSource);
        log.info("DemoServiceB init transaction end");
        return transaction;
    }

    @Bean("sqlSessionFactoryBean")
    public SqlSessionFactoryBean createMybatisSqlSessionFactoryBean(DataSource dataSource) {
        log.info("DemoServiceB init sessionFactoryBean config");
        SqlSessionFactoryBean sessionFactoryBean = new SqlSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource);

        // 数据源配置项
        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();

        // 允许jdbc自动生成建
        configuration.setUseGeneratedKeys(true);

        // 使用列标签代替列名
        configuration.setUseColumnLabel(true);

        // 打开下划线命名自动转换为驼峰命名开关
        configuration.setMapUnderscoreToCamelCase(true);
        sessionFactoryBean.setConfiguration(configuration);
        log.info("DemoServiceB init sessionFactoryBean config end");
        return sessionFactoryBean;
    }

    @Bean
    public TransactionTemplate createTransactionTemplate(DataSourceTransactionManager dataSourceTransactionManager) {
        log.info("DemoServiceB init transactionTemplate");
        TransactionTemplate template = new TransactionTemplate();
        template.setTransactionManager(dataSourceTransactionManager);
        log.info("DemoServiceB init transactionTemplate end");
        return template;
    }


    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Bean
    public MapperScannerConfigurer createMapperScannerConfigurer() {
        log.info("DemoServiceB init mapperScannerConfigurer");
        MapperScannerConfigurer configurer = new MapperScannerConfigurer();
        configurer.setBasePackage("com.huawei.demo");
        configurer.setSqlSessionFactoryBeanName("sqlSessionFactoryBean");
        log.info("DemoServiceB init mapperScannerConfigurer end");
        return configurer;
    }
}
