/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo.serviceb.constance;

/**
 * 常量
 *
 * @since 2024-01-26
 */
public interface Constance {
    String APP_OAUTH_SESSION = "APP_OAUTH_SESSION";
}