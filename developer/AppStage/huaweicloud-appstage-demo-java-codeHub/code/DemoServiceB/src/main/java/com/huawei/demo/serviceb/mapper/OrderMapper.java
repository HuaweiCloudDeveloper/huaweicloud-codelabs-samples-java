/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo.serviceb.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.huawei.demo.serviceb.pojo.OrderInfo;

/**
 * 用户查询
 *
 * @since 2023-12-06
 */
@Mapper
public interface OrderMapper {
    @Select("select * from demo_order_info where order_id = #{orderId}")
    OrderInfo getOrderById(String orderId);

    @Select({"<script>",
            "select * from demo_order_info where 1 = 1 ",
            "<if test=\"orderId != null and !orderId.equals('')\"> ",
            " and order_id like concat('%',#{orderId},'%')",
            "</if>",
            "</script>"})
    List<OrderInfo> getOrder(@Param("orderId") String orderId);
}
