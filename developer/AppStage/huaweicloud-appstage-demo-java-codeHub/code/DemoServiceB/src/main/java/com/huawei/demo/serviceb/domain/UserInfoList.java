/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo.serviceb.domain;

import java.util.List;

import lombok.Data;

/**
 * user信息列表
 *
 * @since 2023-12-06
 */
@Data
public class UserInfoList {
    private List<UserInfo> userInfoList;
}
