/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.demo.serviceb.pojo;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.Data;

/**
 * order信息
 *
 * @since 2023-12-06
 */
@Data
public class OfferingInfo {
    private Integer offeringId;

    @Length(max = 32)
    @NotNull
    private String offeringName;

    @Length(max = 64)
    @NotNull
    private String category;

    @NotNull
    private Integer price;

    @NotNull
    private Integer inventory;

    private Date createTime;
}
