/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo.serviceb.pojo;

import java.util.Date;

import com.huawei.demo.serviceb.domain.UserInfo;

import lombok.Data;

/**
 * order信息
 *
 * @since 2023-12-06
 */
@Data
public class OrderInfo {
    private String orderId;

    private String orderName;

    private String price;

    private Date createTime;

    private String address;

    private String userId;

    private UserInfo userInfo;
}
