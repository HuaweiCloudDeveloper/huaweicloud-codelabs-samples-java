/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo.serviceb.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.huawei.demo.common.domain.BaseResponse;
import com.huawei.demo.serviceb.pojo.OrderInfo;
import com.huawei.demo.serviceb.service.OrderService;

/**
 * 用户对外接口
 *
 * @since 2023-12-06
 */
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping("/{orderId}")
    public BaseResponse<OrderInfo> findOrder(@PathVariable String orderId, HttpServletRequest request) {
        return BaseResponse.success(orderService.findOrder(orderId, request));
    }

    @GetMapping("/list")
    public BaseResponse<List<OrderInfo>> getAllOrder(@RequestParam(value = "orderId", required = false) String orderId,
                                                     HttpServletRequest request) {
        return BaseResponse.success(orderService.getAllOrder(orderId, request));
    }
}
