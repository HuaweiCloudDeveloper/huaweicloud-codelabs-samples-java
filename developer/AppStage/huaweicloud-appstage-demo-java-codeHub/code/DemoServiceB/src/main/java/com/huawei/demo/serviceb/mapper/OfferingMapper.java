/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.demo.serviceb.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.huawei.demo.serviceb.pojo.OfferingInfo;

/**
 * 商品查询Mapper
 *
 * @since 2023-12-06
 */
@Mapper
public interface OfferingMapper {
    @Select({"<script>",
            "select * from demo_offering_info where 1 = 1 ",
            "<if test=\"offeringName != null and !offeringName.equals('')\"> ",
            " and offering_name like concat('%',#{offeringName},'%')",
            "</if>",
            " order by create_time desc ",
            " limit #{offset}, #{limit}",
            "</script>"})
    List<OfferingInfo> getAllOffering(@Param("offeringName") String offeringName, @Param("offset") long offset, @Param(
            "limit") int limit);

    @Select({"<script>",
            "select count(1) from demo_offering_info where 1 = 1 ",
            "<if test=\"offeringName != null and !offeringName.equals('')\"> ",
            " and offering_name like concat('%',#{offeringName},'%')",
            "</if>",
            "</script>"})
    long countAllOffering(@Param("offeringName") String offeringName);

    @Insert("insert into demo_offering_info (offering_name, category, price, inventory, create_time) values"
            + "(#{offeringName}, #{category}, #{price}, #{inventory}, #{createTime})")
    void insertOffering(OfferingInfo offeringInfo);

    @Delete("delete from demo_offering_info where offering_id = #{offeringId}")
    void deleteOffering(String offeringId);
}
