/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo.serviceb.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.huawei.demo.serviceb.domain.UserInfo;
import com.huawei.demo.serviceb.domain.UserInfoList;
import com.huawei.demo.serviceb.mapper.OrderMapper;
import com.huawei.demo.serviceb.pojo.OrderInfo;

/**
 * 查询order信息服务类
 *
 * @since 2023-12-06
 */
@Service
public class OrderService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private OrderMapper orderMapper;

    @Value("${demo.service-url.demoServiceA}")
    private String demoServiceAUrl;

    public OrderInfo findOrder(String orderId, HttpServletRequest request) {
        OrderInfo orderInfo = orderMapper.getOrderById(orderId);
        String url = demoServiceAUrl + "/user/" + orderInfo.getUserId();
        HttpHeaders headers = createHeaders(request);
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<UserInfo> responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, UserInfo.class); ;
        if (responseEntity != null) {
            orderInfo.setUserInfo(responseEntity.getBody());
        }
        return orderInfo;
    }

    public List<OrderInfo> getAllOrder(String orderId, HttpServletRequest request) {
        List<OrderInfo> orderInfoList = orderMapper.getOrder(orderId);
        if (CollectionUtils.isEmpty(orderInfoList)) {
            return orderInfoList;
        }
        String url = demoServiceAUrl + "/user/list";
        HttpHeaders headers = createHeaders(request);
        HttpEntity<List<String>> entity = new HttpEntity<>(orderInfoList
                .stream()
                .filter(orderInfo -> !StringUtils.isEmpty(orderInfo.getUserId()))
                .map(OrderInfo::getUserId)
                .distinct()
                .collect(Collectors.toList()), headers);
        ResponseEntity<UserInfoList> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity,
                UserInfoList.class);
        if (responseEntity != null && responseEntity.getBody() != null
            && !CollectionUtils.isEmpty(responseEntity.getBody().getUserInfoList())) {
            Map<String, UserInfo> userInfoMap =
                    responseEntity.getBody().getUserInfoList().stream()
                            .filter(userInfo -> userInfo != null && !StringUtils.isEmpty(userInfo.getUserId()))
                            .collect(HashMap::new,
                                    ((hashMap, userInfo) -> hashMap.put(userInfo.getUserId(), userInfo)),
                                    HashMap::putAll);
            orderInfoList.stream()
                    .filter(orderInfo -> !StringUtils.isEmpty(orderInfo.getUserId()))
                    .forEach(orderInfo -> orderInfo.setUserInfo(userInfoMap.get(orderInfo.getUserId())));
        }
        return orderInfoList;
    }

    private HttpHeaders createHeaders(HttpServletRequest request) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        Cookie[] cookies = request.getCookies();

        // 将cookie塞入请求头，将登录信息传递给下一个接口
        if (cookies != null && cookies.length > 0) {
            httpHeaders.add(HttpHeaders.COOKIE,
                    Arrays.stream(cookies).map(cookie -> cookie.getName() + "=" + cookie.getValue()).collect(Collectors.joining(
                            "; ")));
        }
        return httpHeaders;
    }
}
