/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo.serviceb.domain;

import lombok.Data;

/**
 * user信息
 *
 * @since 2023-12-06
 */
@Data
public class UserInfo {
    private String userId;

    private String userName;

    private String phone;

    private String address;
}
