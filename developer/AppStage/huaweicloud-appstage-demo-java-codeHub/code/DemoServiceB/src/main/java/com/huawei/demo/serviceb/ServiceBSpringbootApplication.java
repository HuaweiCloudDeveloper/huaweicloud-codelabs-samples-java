/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo.serviceb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

import com.huawei.wisesecurity.sts.springboot.security.annotation.EnableStsAutoInitialization;

/**
 * 服务启动类
 * 注解@EnableStsAutoInitialization启动STS
 * 注解@EnableDiscoveryClient启动cloudMap
 *
 * @since 2023-12-05
 */
@ComponentScan(basePackages = {"com.huawei.demo"})
@SpringBootApplication
@EnableStsAutoInitialization
@EnableDiscoveryClient
public class ServiceBSpringbootApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceBSpringbootApplication.class, args);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
