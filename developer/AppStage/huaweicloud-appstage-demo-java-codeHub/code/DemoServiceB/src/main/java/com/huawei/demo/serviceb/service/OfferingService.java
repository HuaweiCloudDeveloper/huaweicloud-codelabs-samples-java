/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.demo.serviceb.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.huawei.demo.common.domain.BaseResponse;
import com.huawei.demo.common.domain.Page;
import com.huawei.demo.serviceb.mapper.OfferingMapper;
import com.huawei.demo.serviceb.pojo.OfferingInfo;

/**
 * 查询offering信息服务类
 *
 * @since 2024-09-06
 */
@Service
public class OfferingService {
    @Autowired
    private OfferingMapper offeringMapper;

    /**
     * 查询所有商品
     *
     * @param offeringName 商品名称，支持模糊查询
     * @param offset       分页偏移量
     * @param limit        分页大小
     * @return BaseResponse<List < OfferingInfo>> 查询所有offering响应值对象
     */
    public BaseResponse<List<OfferingInfo>> getAllOffering(String offeringName, Long offset, Integer limit) {
        List<OfferingInfo> offeringInfos =
                offeringMapper.getAllOffering(offeringName, offset == null || offset <= 0 ? 0 : offset,
                        limit == null || limit <= 0 ? 10 : limit);
        Page page = new Page(offset == null || offset <= 0 ? 0 : offset, limit == null || limit <= 0 ? 10 : limit);
        page.setCount(offeringMapper.countAllOffering(offeringName));
        return BaseResponse.success(offeringInfos, page);
    }

    /**
     * 新增offering
     *
     * @param offeringInfo offering信息
     */
    public void addOffering(OfferingInfo offeringInfo) {
        if (offeringInfo == null) {
            return;
        }
        if (offeringInfo.getCreateTime() == null) {
            offeringInfo.setCreateTime(new Date());
        }
        offeringMapper.insertOffering(offeringInfo);
    }

    /**
     * 删除offering
     *
     * @param offeringId 商品Id
     */
    public void deleteOffering(String offeringId) {
        if (StringUtils.isEmpty(offeringId)) {
            return;
        }
        offeringMapper.deleteOffering(offeringId);
    }
}
