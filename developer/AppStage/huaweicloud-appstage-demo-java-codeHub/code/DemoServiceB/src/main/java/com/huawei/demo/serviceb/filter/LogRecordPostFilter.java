/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

package com.huawei.demo.serviceb.filter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import com.huawei.demo.common.exception.DemoException;

import lombok.extern.slf4j.Slf4j;

/**
 * 接口日志记录过滤器，实现日志监控告警功能
 *
 * @since 2024-02-05
 */
@Component
@Slf4j
public class LogRecordPostFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) {
        log.info("DemoServiceB log request filter");
        long startTime = System.currentTimeMillis();
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        try {
            chain.doFilter(request, response);
        } catch (Exception e) {
            log.error("request DemoServiceB failed,Exception=", e);
            throw new DemoException(e);
        } finally {
            log.info("Request DemoServiceB|{}|{}|{}|{}", httpServletRequest.getMethod(),
                    httpServletRequest.getRequestURL(), System.currentTimeMillis() - startTime,
                    httpServletResponse.getStatus());
        }
    }
}
