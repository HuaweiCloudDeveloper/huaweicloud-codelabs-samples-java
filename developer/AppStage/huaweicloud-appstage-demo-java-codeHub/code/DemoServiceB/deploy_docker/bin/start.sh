#!/bin/bash
cur_dir=$(
  cd $(dirname $0)
  pwd
)
echo "start.sh location is ${cur_dir}"

echo "running user is $(whoami)"


umask 077 # 修改当前用户的umask

app_home=${cur_dir}/..
cd ${app_home}
/opt/huawei/app/bin/configparser -meta /opt/huawei/app/configtemplate/meta.txt -log configparser.log -mode front -tempPath /opt/huawei/app/configtemplate/config-temp


conf_dir=${app_home}/service/config
if [ -d /home/service/tools ];then
  rm -rf /home/service/tools
fi

export SC_LIBRARY_PATH=${app_home}/tmp
if [ "${LD_LIBRARY_PATH}" ]; then
  echo "have LD_LIBRARY_PATH value "
  export LD_LIBRARY_PATH=$SC_LIBRARY_PATH:$LD_LIBRARY_PATH
else
  echo "not have LD_LIBRARY_PATH value "
  export LD_LIBRARY_PATH=$SC_LIBRARY_PATH
fi

echo "SC_LIBRARY_PATH=$SC_LIBRARY_PATH"
echo "LD_LIBRARY_PATH=$LD_LIBRARY_PATH"
if [ ! -d "${SC_LIBRARY_PATH}" ]; then
  echo "${SC_LIBRARY_PATH} not exists. mkdir"
  mkdir ${SC_LIBRARY_PATH}
fi

if [ ! -d "${app_home}/../logs/scc" ]; then
  echo "${app_home}/../logs/scc not exists. mkdir"
  mkdir -p ${app_home}/../logs/scc
fi


main_class="com.huawei.demo.serviceb.ServiceBSpringbootApplication"

#############################
# 检查状态
#############################
check_status() {
  echo "Begin to check application status"
  pid=$(ps -ef |grep DemoServiceBService | grep -v grep | awk '{print $2}')
  if [ $pid ]; then
    echo ">>> api PID = $pid <<<"
    return 1
  fi
}

#############################
# 启动应用
#############################
start_app() {
  echo "Begin to start application."


  check_status
  if [ $? -ne 0 ]; then
    echo -e "\330[31m ERROR: Application status is running. \330[0m"
    return 1
  fi

  CLASS_PATH=.
  CLASS_PATH=${CLASS_PATH}:./service/config


  if [ -d "./service/libs" ]; then
    for file in ./service/libs/*.jar; do
      CLASS_PATH=${CLASS_PATH}:${file}
    done
  fi
  for file in ./*.jar; do
    CLASS_PATH=${CLASS_PATH}:${file}
  done

  #设置虚拟机参数
  echo "start to set JVM params."

  #根据容器可用内存，设置75%为启动参数
  DOCKER_MEMORY=$(cat /sys/fs/cgroup/memory/memory.limit_in_bytes)
  DOCKER_MEMORY=$(expr $DOCKER_MEMORY / 1024 / 1024)
  MEMORY_LIMIT=$(($DOCKER_MEMORY * 70 / 100))m

  if [ -f "${app_home}/../configmap/jvm_debug" ]; then
    JAVA_OPTS="$JAVA_OPTS -Xms$MEMORY_LIMIT -Xmx$MEMORY_LIMIT -Dfile.encoding=UTF-8 -Duser.timezone=UTC -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5008 "
  else
    JAVA_OPTS="$JAVA_OPTS -Xms$MEMORY_LIMIT -Xmx$MEMORY_LIMIT -Dfile.encoding=UTF-8 -Duser.timezone=UTC "
  fi
  JAVA_OPTS="$JAVA_OPTS -Dlogging.file=/opt/huawei/logs/spring.log "

  java $JAVA_OPTS -cp ${CLASS_PATH} ${main_class}

  echo "Success start application."
}

start_app

# sleep 10m
