/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo.servicea.config;

import java.util.Arrays;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.huawei.demo.common.domain.BaseResponse;
import com.huawei.demo.common.exception.DemoException;
import com.huawei.demo.common.util.ApplicationContextUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 拦截器具体实现
 *
 * @since 2024-01-26
 */
@Configuration
@Slf4j
public class UserLoginInterceptor implements HandlerInterceptor, ApplicationContextAware {
    private static ApplicationContext applicationContext;


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        log.info("DemoServiceA login validate");
        Environment environment = applicationContext.getEnvironment();
        String needLogin = environment.getProperty("demo.login.enable", "true");
        if (!"true".equalsIgnoreCase(needLogin)) {
            log.info("DemoServiceA No login required");
            return true;
        }

        String url = environment.getProperty("demo.service-url.demoOrgidLogin") + "/is/login";
        RestTemplate restTemplate = (RestTemplate) ApplicationContextUtil.getBean("restTemplate");
        HttpHeaders headers = createHeaders(request);
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<BaseResponse> responseEntity = null;
        try {
            responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, BaseResponse.class);
            if (!responseEntity.hasBody()) {
                log.error("DemoServiceA login response body is null");
            }
        } catch (Exception e) {
            log.error("DemoServiceA login http request failure", e);
            throw new DemoException(e);
        }
        BaseResponse baseResponse = responseEntity.getBody();
        if (baseResponse.getData() != null) {
            return true;
        } else {
            log.info("DemoServiceA request user is not login, redirect to login");
            response.sendRedirect(environment.getProperty("demo.login.url"));
            return false;
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler
            , @Nullable ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
                                @Nullable Exception ex) throws Exception {
    }

    private HttpHeaders createHeaders(HttpServletRequest request) {
        log.info("DemoServiceA create validate login request headers");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        Cookie[] cookies = request.getCookies();

        if (cookies != null && cookies.length > 0) {
            log.info("DemoServiceA login request cookie build");
            httpHeaders.add(HttpHeaders.COOKIE,
                    Arrays.stream(cookies).map(cookie -> cookie.getName() + "=" + cookie.getValue()).collect(Collectors.joining(
                            "; ")));
        }
        return httpHeaders;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}