/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo.servicea.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.huawei.demo.servicea.mapper.UserMapper;
import com.huawei.demo.servicea.pojo.UserInfo;

/**
 * userService
 *
 * @since 2023-12-06
 */
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public UserInfo getUserById(String userId) {
        return userMapper.getUserById(userId);
    }

    public List<UserInfo> getUserByIds(List<String> userIdList) {
        return userMapper.getUserByIds(userIdList);
    }
}
