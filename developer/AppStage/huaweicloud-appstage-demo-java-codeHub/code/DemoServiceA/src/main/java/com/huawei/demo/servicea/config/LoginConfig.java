/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo.servicea.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import lombok.extern.slf4j.Slf4j;

/**
 * 全局未登录拦截器
 *
 * @since 2024-01-26
 */

@Configuration
@Slf4j
public class LoginConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry interceptorRegistry) {
        log.info("DemoServiceA add Interceptors");
        InterceptorRegistration interceptorRegistration =
                interceptorRegistry.addInterceptor(new UserLoginInterceptor());
        // 拦截所有路径
        interceptorRegistration.addPathPatterns("/**");
    }
}