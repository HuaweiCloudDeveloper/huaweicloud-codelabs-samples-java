/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.demo.servicea.config;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import com.huawei.nuwa.map.client.NuwaMapClient;
import com.huawei.nuwa.map.spring.boot.autoconfigure.NuwaCloudMapAutoConfiguration;
import com.huawei.rainbow.jdbc.DbGroupDataSource;
import com.huawei.wisesecurity.sts.springboot.security.configuration.StsEncryptablePropertiesConfiguration;

import lombok.extern.slf4j.Slf4j;

/**
 * rainbow启动
 */
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
@Import(value = {NuwaCloudMapAutoConfiguration.class, StsEncryptablePropertiesConfiguration.class})
@Slf4j
public class ApplicationAutoConfig implements EnvironmentAware {
    private Environment environment;

    /**
     * serviceA rainbow需要依赖sts和CloudMap启动，创建dataSource
     *
     * @param client NuwaMapClient值对象
     * @return DataSource 初始化完成的数据源数据源
     */
    @Bean
    @DependsOn({"stsBootStrap"})
    public DataSource createDataSource(NuwaMapClient client) throws SQLException {
        log.info("DemoServiceA init dataSource");
        DbGroupDataSource dataSource = new DbGroupDataSource();
        dataSource.setUseSts(true);
        String dataSourceName = environment.getProperty("wiseDba.dataSourceName");
        if (!StringUtils.isEmpty(dataSourceName)) {
            // wiseDba配置
            log.info("DemoServiceA wiseDba connected database");
            dataSource.setDataSourceName(environment.getProperty("wiseDba.dataSourceName"));
        } else {
            // 直连数据库配置
            log.info("DemoServiceA directly connected database");
            dataSource.setServers(environment.getProperty("wiseDba.servers"));
            dataSource.setInstanceName(environment.getProperty("wiseDba.instanceName"));
            dataSource.setDbName(environment.getProperty("wiseDba.dbName"));
            dataSource.setUserName(environment.getProperty("wiseDba.userName"));
            dataSource.setEncStsPasswd(environment.getProperty("wiseDba.encStsPasswd"));
            String passwordCoderClass = environment.getProperty("wiseDba.passwordCoderClass");
            if(!StringUtils.isEmpty(passwordCoderClass)) {
                log.info("DemoServiceA passwordCoderClass is empty, use default coder class");
                dataSource.setPasswordCoderClass(environment.getProperty("wiseDba.passwordCoderClass"));
            }
        }
        dataSource.init();
        log.info("DemoServiceA init dataSource end");
        return dataSource;
    }

    @Bean
    public DataSourceTransactionManager createTransaction(DataSource dataSource) {
        log.info("DemoServiceA init transaction");
        DataSourceTransactionManager transaction = new DataSourceTransactionManager();
        transaction.setDataSource(dataSource);
        log.info("DemoServiceA init transaction end");
        return transaction;
    }

    @Bean
    public JdbcTemplate createJdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean("sqlSessionFactoryBean")
    public SqlSessionFactoryBean createMybatisSqlSessionFactoryBean(DataSource dataSource) {
        log.info("DemoServiceA init sessionFactoryBean config");
        SqlSessionFactoryBean sessionFactoryBean = new SqlSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource);
        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();
        configuration.setUseGeneratedKeys(true);
        configuration.setUseColumnLabel(true);
        configuration.setMapUnderscoreToCamelCase(true);
        sessionFactoryBean.setConfiguration(configuration);
        log.info("DemoServiceA init sessionFactoryBean config end");
        return sessionFactoryBean;
    }

    @Bean
    public TransactionTemplate createTransactionTemplate(DataSourceTransactionManager dataSourceTransactionManager) {
        log.info("DemoServiceA init transactionTemplate");
        TransactionTemplate template = new TransactionTemplate();
        template.setTransactionManager(dataSourceTransactionManager);
        log.info("DemoServiceA init transactionTemplate end");
        return template;
    }

    @Bean
    public MapperScannerConfigurer createMapperScannerConfigurer() {
        log.info("DemoServiceA init mapperScannerConfigurer");
        MapperScannerConfigurer configurer = new MapperScannerConfigurer();
        configurer.setBasePackage("com.huawei.demo");
        configurer.setSqlSessionFactoryBeanName("sqlSessionFactoryBean");
        log.info("DemoServiceA init mapperScannerConfigurer end");
        return configurer;
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
