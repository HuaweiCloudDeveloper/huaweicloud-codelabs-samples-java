/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo.servicea.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.huawei.demo.servicea.pojo.UserInfo;
import com.huawei.demo.servicea.pojo.UserInfoList;
import com.huawei.demo.servicea.service.UserService;

/**
 * 用户对外接口
 *
 * @since 2023-12-06
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping(value = "/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserInfo getUserById(@PathVariable String userId) {
        return userService.getUserById(userId);
    }

    @PostMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserInfoList getUserById(@RequestBody @Valid List<String> userIdList) {
        return new UserInfoList(userService.getUserByIds(userIdList));
    }
}
