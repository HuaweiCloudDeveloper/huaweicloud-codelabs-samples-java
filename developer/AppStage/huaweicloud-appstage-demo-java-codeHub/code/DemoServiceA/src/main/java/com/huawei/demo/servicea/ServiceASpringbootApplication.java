/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo.servicea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

import com.huawei.wisesecurity.sts.springboot.security.annotation.EnableStsAutoInitialization;

/**
 * 启动类
 *
 * @since 2023-12-05
 */
@ComponentScan(basePackages = {"com.huawei.demo"})
@SpringBootApplication
@EnableStsAutoInitialization(value = "application.properties")
@EnableDiscoveryClient
public class ServiceASpringbootApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceASpringbootApplication.class, args);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
