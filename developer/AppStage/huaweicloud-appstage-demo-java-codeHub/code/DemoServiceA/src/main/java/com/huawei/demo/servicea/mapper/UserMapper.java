/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo.servicea.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.huawei.demo.servicea.pojo.UserInfo;

/**
 * 用户查询
 *
 * @since 2023-12-06
 */
@Mapper
public interface UserMapper {
    @Select("select * from demo_user_info where user_id = #{userId}")
    UserInfo getUserById(String userId);

    @Select("<script>"
            + "select * from demo_user_info where user_id in ("
            + "<foreach collection='userIdList' separator=',' item='userId'>"
            + "#{userId}"
            + "</foreach>"
            + ")</script>")
    List<UserInfo> getUserByIds(@Param("userIdList") List<String> userIdList);
}
