/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.demo.loginservice.api;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.huawei.demo.common.domain.BaseResponse;

/**
 * 登录接口
 *
 * @since 2024-01-26
 */
public interface UserLoginApi {
    @RequestMapping(value = "/login")
    String login();

    @RequestMapping(value = "/is/login")
    BaseResponse<String> isLogin(HttpServletRequest httpServletRequest) throws Exception;

    @RequestMapping(value = "/oauth2/toLogin")
    void toOauth2Login(HttpServletResponse httpServletResponse,
                       @RequestParam(value = "code", required = true) String code,
                       @RequestParam(value = "tenant", required = false) String tenant) throws IOException;

    @RequestMapping(value = "/app/logout")
    void logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception;

    @RequestMapping(value = "/callback/logout")
    void callBackLogout(HttpServletRequest request, HttpServletResponse httpServletResponse) throws Exception;
}