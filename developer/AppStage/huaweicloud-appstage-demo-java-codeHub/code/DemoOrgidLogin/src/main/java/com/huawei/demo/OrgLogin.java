/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import com.huawei.demo.common.exception.DemoException;
import com.huawei.wisesecurity.sts.springboot.security.annotation.EnableStsAutoInitialization;
import com.huawei.wisesecurity.sts.springboot.security.annotation.EnableStsEncryptableProperties;

/**
 * 启动类
 */
@SpringBootApplication
@EnableStsAutoInitialization(value = "application.properties")
@EnableStsEncryptableProperties
@EnableDiscoveryClient
public class OrgLogin {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrgLogin.class);

    public static void main(String[] args) {
        try {
            LOGGER.info("Start optical application...");
            SpringApplication.run(OrgLogin.class, args);
        } catch (Exception e) {
            LOGGER.error("Fail start optical application.", e);
            throw new DemoException("Fail start optical application.");
        }
    }
}
