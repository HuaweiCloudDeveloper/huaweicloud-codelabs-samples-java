/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

package com.huawei.demo.filter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import com.huawei.demo.common.exception.DemoException;

import lombok.extern.slf4j.Slf4j;

/**
 * 功能描述
 *
 * @since 2024-02-05
 */
@Component
@Slf4j
public class LogRecordPostFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) {
        log.info("DemoOrgidLogin log request filter");
        long startTime = System.currentTimeMillis();
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        try {
            chain.doFilter(request, response);
        } catch (Exception e) {
            log.error("request DemoOrgidLogin failed,Exception=", e);
            throw new DemoException(e);
        } finally {
            log.info("Request DemoOrgidLogin|{}|{}|{}|{}", httpServletRequest.getMethod(),
                    httpServletRequest.getRequestURL(), System.currentTimeMillis() - startTime,
                    httpServletResponse.getStatus());
        }
    }
}
