/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

package com.huawei.demo.loginservice.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ResponseBody;

import com.huawei.demo.common.domain.BaseResponse;
import com.huawei.demo.loginservice.api.UserLoginApi;
import com.huawei.demo.loginservice.domain.Constance;
import com.huawei.demo.loginservice.service.LoginService;
import com.huawei.demo.loginservice.utils.JwtUtil;

import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;

/**
 * 用户登录controller类
 *
 * @since 2024-01-26
 */
@Controller
@Slf4j
public class UserLoginController implements UserLoginApi {
    @Autowired
    private LoginService loginService;

    @Autowired
    private JwtUtil jwtUtil;

    @Value("${spring.redis.host}")
    private String redisHost;

    @Value("${spring.redis.port}")
    private Integer redisPort;
    @Value("${spring.redis.password:}")
    private String redisPwd;

    @Value("${spring.redis.user-name:}")
    private String redisUserName;

    @Override
    public String login() {
        log.info("user login");
        return "/login";
    }

    @Override
    @ResponseBody
    public BaseResponse<String> isLogin(HttpServletRequest httpServletRequest) throws Exception {
        log.info("spring.redis.user-name:{}", redisUserName);
        Jedis jedis = new Jedis(redisHost, redisPort);
        if (!StringUtils.isEmpty(redisUserName) && !StringUtils.isEmpty(redisPwd)) {
            jedis.auth(redisUserName, redisPwd);
        } else if (StringUtils.isEmpty(redisUserName) && !StringUtils.isEmpty(redisPwd)) {
            jedis.auth(redisPwd);
        }
        log.info("check user login");

        // 检查用户cookie
        Cookie[] cookies = httpServletRequest.getCookies();

        if (cookies != null) {
            String jwtUserId = Arrays.stream(cookies)
                    .filter(cookie -> Objects.equals(cookie.getName(), Constance.APP_OAUTH_SESSION))
                    .findFirst()
                    .map(Cookie::getValue)
                    .orElse(null);
            if (jwtUserId != null) {
                String userId = jwtUtil.parseJWT(jwtUserId).getSubject();
                String userInfo = jedis.get("login:" + userId);
                if (userInfo != null) {
                    // 返回用户信息
                    return BaseResponse.success(userInfo);
                }
            }
        }
        log.error("user is not login");
        return BaseResponse.success();
    }

    @Override
    public void toOauth2Login(HttpServletResponse httpServletResponse, String code, String tenant) throws IOException {
        loginService.toOauth2Login(httpServletResponse, code);
    }

    @Override
    public void logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
            throws Exception {
        loginService.logout(httpServletRequest, httpServletResponse);
    }

    @Override
    public void callBackLogout(HttpServletRequest request, HttpServletResponse httpServletResponse) throws Exception {
        loginService.logoutSelf(request, httpServletResponse);
    }
}