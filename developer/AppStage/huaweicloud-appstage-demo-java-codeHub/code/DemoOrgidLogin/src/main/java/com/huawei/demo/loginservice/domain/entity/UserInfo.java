
package com.huawei.demo.loginservice.domain.entity;

import lombok.Data;

import java.util.Map;

/**
 * oauth协议登录后用户信息
 *
 * @since 2024-01-26
 */
@Data
public class UserInfo {

    private String user_name;

    private String name;

    private String mobile;

    private String user_id;

    private String employee_code;

    private String role;

    private Map extensions;

    private String tenant;

    private String tenant_name;

    private boolean fromNewLogin;

    private boolean longTermAuthenticationRequestTokenUsed;

    private String authenticationDate;
}
