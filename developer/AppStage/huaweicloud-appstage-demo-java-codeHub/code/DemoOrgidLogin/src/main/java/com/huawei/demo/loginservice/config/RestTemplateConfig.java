/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2020-2025. All rights reserved.
 */

package com.huawei.demo.loginservice.config;

import lombok.extern.slf4j.Slf4j;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.net.ssl.SSLContext;

/**
 * RestTemplate配置
 *
 * @since 2024-01-26
 */
@Configuration
@Slf4j
public class RestTemplateConfig {

    @Bean
    public static RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();

        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        httpRequestFactory.setConnectionRequestTimeout(10 * 2000);
        httpRequestFactory.setConnectTimeout(10 * 2000);
        httpRequestFactory.setReadTimeout(10 * 2000);

        List<HttpMessageConverter<?>> list = restTemplate.getMessageConverters();
        for (HttpMessageConverter<?> httpMessageConverter : list) {
            if (httpMessageConverter instanceof StringHttpMessageConverter) {
                ((StringHttpMessageConverter) httpMessageConverter).setDefaultCharset(StandardCharsets.UTF_8);
                break;
            }
        }

        // 忽略SSL证书验证
        CloseableHttpClient httpClient = getHttpsClient();
        httpRequestFactory.setHttpClient(httpClient);

        restTemplate.setRequestFactory(httpRequestFactory);
        return restTemplate;
    }

    public static CloseableHttpClient getHttpsClient() {

        CloseableHttpClient httpClient;
        SSLContext sslContext = null;
        try {
            sslContext = SSLContexts.custom().loadTrustMaterial(null, (x509Certificates, s) -> true).build();
        } catch (NoSuchAlgorithmException e) {
            log.error("getHttpsClient faild:enccrypt faild.Msg is {}.", e.getMessage());
        } catch (KeyManagementException e) {
            log.error("getHttpsClient faild:get key faild.Msg is {}.", e.getMessage());
        } catch (KeyStoreException e) {
            log.error("getHttpsClient faild:get keyStore faild.Msg is {}.", e.getMessage());
        }
        httpClient = HttpClients.custom()
            .setSSLContext(sslContext)
            .setSSLHostnameVerifier(new NoopHostnameVerifier())
            .build();
        return httpClient;
    }
}
