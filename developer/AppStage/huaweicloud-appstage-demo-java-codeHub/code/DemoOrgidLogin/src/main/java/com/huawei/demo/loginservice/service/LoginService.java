/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2020-2025. All rights reserved.
 */

package com.huawei.demo.loginservice.service;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.huawei.demo.common.exception.DemoException;
import com.huawei.demo.loginservice.domain.Constance;
import com.huawei.demo.loginservice.domain.entity.UserInfo;
import com.huawei.demo.loginservice.utils.JwtUtil;
import com.huawei.demo.loginservice.utils.RedisUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 登录服务类
 *
 * @since 2024-01-26
 */
@Service
@Slf4j
public class LoginService {
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private JwtUtil jwtUtil;

    @Value("${org.url}")
    private String ORG_URL;

    @Value("${org.web-url}")
    private String ORG_WEB_URL;

    @Value("${org.app.ent-point-url}")
    private String ORG_APP_URL;

    private static final String OAUTH_CODE_URL = "/oauth2/token";

    private static final String OAUTH_INFO_URL = "/oauth2/userinfo";

    @Value("${org.app.protocol-login.oauth.clientId}")
    private String OAUTH_CLIENT_ID;

    @Value("${org.app.protocol-login.oauth.clientSecret}")
    private String OAUTH_CLIENT_SECRET;

    @Value("${org.app.cookie-domain}")
    private String cookieDomain;


    public void toOauth2Login(HttpServletResponse httpServletResponse, String code) throws IOException {
        log.info("user Oauth2Login");
        String callbackUrl = ORG_APP_URL + Constance.OAUTH_URI;

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        String tokenUrl = ORG_URL + OAUTH_CODE_URL;
        String formUrlEncodeRequest =
                MessageFormat.format("grant_type={0}&code={1}&client_id={2}&client_secret={3}&redirect_uri={4}",
                        "authorization_code", code, OAUTH_CLIENT_ID, OAUTH_CLIENT_SECRET, callbackUrl);
        HttpEntity<String> httpEntity = new HttpEntity<>(formUrlEncodeRequest, headers);
        ResponseEntity<Map> responseEntity = sendHttp(tokenUrl, HttpMethod.POST, httpEntity, Map.class);
        // 请求用户信息
        Map tokenMap = responseEntity.getBody();
        HttpHeaders userHeaders = new HttpHeaders();
        userHeaders.setBearerAuth((String) tokenMap.get("access_token"));
        HttpEntity<String> userEntity = new HttpEntity<>(userHeaders);
        String infoUrl = ORG_URL + OAUTH_INFO_URL;
        ResponseEntity<UserInfo> infoEntity = sendHttp(infoUrl, HttpMethod.GET, userEntity, UserInfo.class);
        UserInfo userInfo = infoEntity.getBody();
        String userId = userInfo.getUser_id();
        redisUtil.set("login:" + userId, userInfo, Constance.REDIS_EXPIRE_TIME);

        // 生成会话，登录首页
        Cookie cookie = produceCookie(userId, Constance.APP_OAUTH_SESSION);
        httpServletResponse.addCookie(cookie);
        httpServletResponse.sendRedirect(ORG_APP_URL + "/index.html");
    }

    public <T> ResponseEntity<T> sendHttp(String url, HttpMethod method, HttpEntity httpEntity, Class<T> c) {
        ResponseEntity<T> entity;
        try {
            entity = restTemplate.exchange(url, method, httpEntity, c);
        } catch (Exception e) {
            log.error("invoke verify ticket interface failure,result = {}", e.getMessage());
            throw new DemoException(e);
        }
        if (entity.getBody() == null) {
            log.error("invoke verify ticket interface failure, responseBody is null");
            throw new DemoException("invoke verify ticket interface failure, responseBody is null");
        }
        return entity;
    }

    public void logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
            throws Exception {
        log.info("user logOut");
        logoutSelf(httpServletRequest, httpServletResponse);
        // 重定向到orgId去退出.
        httpServletResponse.sendRedirect(ORG_WEB_URL + "/oauth/logout?service=" + ORG_APP_URL + "/index.html");
    }

    public void logoutSelf(HttpServletRequest request, HttpServletResponse httpServletResponse) throws Exception {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            List<String> cookieList = Arrays.stream(cookies)
                    .filter(cookie -> Objects.equals(cookie.getName(), Constance.APP_OAUTH_SESSION))
                    .map(Cookie::getValue)
                    .collect(Collectors.toList());
            for (String jwtUserId : cookieList) {
                String userId = jwtUtil.parseJWT(jwtUserId).getSubject();
                redisUtil.del("login:" + userId);
            }
            deleteCookie(httpServletResponse, cookies);
        }
    }

    public void deleteCookie(HttpServletResponse httpServletResponse, Cookie[] cookies) {
        List<Cookie> deleteCookies = Arrays.stream(cookies)
                .filter(cookie -> Objects.equals(cookie.getName(), Constance.APP_OAUTH_SESSION))
                .collect(Collectors.toList());
        for (Cookie cookie : deleteCookies) {
            cookie.setDomain(cookieDomain);
            cookie.setPath("/");
            cookie.setMaxAge(0);
            httpServletResponse.addCookie(cookie);
        }
    }

    private Cookie produceCookie(String userId, String cookieName) {
        String jwtUserId = jwtUtil.createJWT(userId);
        Cookie cookie = new Cookie(cookieName, jwtUserId);

        // demo应用没有申请域名，正常应用对接需设置应用对应域信息，防止出现跨域访问，造成安全问题
        cookie.setDomain(cookieDomain);
        cookie.setPath("/");
        cookie.setMaxAge(60 * 60 * 2);
        return cookie;
    }
}