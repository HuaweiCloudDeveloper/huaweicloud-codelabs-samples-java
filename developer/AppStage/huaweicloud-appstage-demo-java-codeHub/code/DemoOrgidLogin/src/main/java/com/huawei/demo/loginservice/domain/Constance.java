/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo.loginservice.domain;

/**
 * 常量
 *
 * @since 2024-01-26
 */
public interface Constance {
    String OAUTH_URI = "/oauth2/toLogin";

    String APP_OAUTH_SESSION = "APP_OAUTH_SESSION";

    Integer REDIS_EXPIRE_TIME = 7200;
}