/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.demo.loginservice.config;

import java.util.Arrays;
import java.util.Objects;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.huawei.demo.common.util.ApplicationContextUtil;
import com.huawei.demo.loginservice.domain.Constance;
import com.huawei.demo.loginservice.utils.JwtUtil;

import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;

/**
 * 拦截器具体实现
 *
 * @since 2024-01-26
 */
@Configuration
@Slf4j
public class UserLoginInterceptor implements HandlerInterceptor, ApplicationContextAware {
    private static ApplicationContext applicationContext;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        Environment environment = applicationContext.getEnvironment();
        Jedis jedis = new Jedis(environment.getProperty("spring.redis.host"),
                Integer.valueOf(environment.getProperty("spring.redis.port")));
        String redisPwd = environment.getProperty("spring.redis.password", "");
        log.info("spring.redis.password:{}", redisPwd);
        String redisUserName = environment.getProperty("spring.redis.user-name", "");
        log.info("spring.redis.user-name:{}", redisUserName);
        if (!StringUtils.isEmpty(redisUserName) && !StringUtils.isEmpty(redisPwd)) {
            jedis.auth(redisUserName, redisPwd);
        } else if (StringUtils.isEmpty(redisUserName) && !StringUtils.isEmpty(redisPwd)) {
            jedis.auth(redisPwd);
        }

        // 检查用户cookie
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            String jwtUserId = Arrays.stream(cookies)
                    .filter(cookie -> Objects.equals(cookie.getName(), Constance.APP_OAUTH_SESSION))
                    .findFirst()
                    .map(Cookie::getValue)
                    .orElse(null);
            if (jwtUserId != null) {
                JwtUtil jwtUtil = (JwtUtil) ApplicationContextUtil.getBean("jwtUtil");
                String userId = jwtUtil.parseJWT(jwtUserId).getSubject();
                String userInfo = jedis.get("login:" + userId);
                if (userInfo != null) {
                    return true;
                }
            }
        }

        response.sendRedirect(environment.getProperty("demo.login.url"));
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler
            , @Nullable ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
                                @Nullable Exception ex) throws Exception {
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}