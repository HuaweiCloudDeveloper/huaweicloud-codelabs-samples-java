#!/bin/bash

set -e
set -o pipefail
BASE_PATH=$(cd $(dirname $0);pwd)
PROJECT_PATH=$(cd $BASE_PATH/..;pwd)
# 待打包的服务名称,通过mvn的打包命令设置包的名称
ServiceName="SpringCloudDemo"
# snapshot构建的版本号配置
#iac_version="24.01.29.02-SNAPSHOT"
#snapshot_version="${iac_version}"

if [[ ${time} != "" ]] ;then
	  echo "This is release build"
	  snapshot_version="${time}"
	else
		echo "This is snapshot build"
		snapshot_version="24.01.30.01-SNAPSHOT"
	fi
rm -rf ${WORKSPACE}/*.zip
rm -rf ${PROJECT_PATH}/target/*.zip

#iac打包处理逻辑
function build_iac_package() {
  # 进入工作目录
  cd ${PROJECT_PATH}

  sed -i "s/snapshot_version/${snapshot_version}/g" springclouddemo/specs/cn_green_cbu_default/DemoServiceAService/values.yaml
  sed -i "s/snapshot_version/${snapshot_version}/g" springclouddemo/specs/cn_green_cbu_default/DemoServiceBService/values.yaml
  sed -i "s/snapshot_version/${snapshot_version}/g" springclouddemo/specs/cn_green_cbu_default/DemoOrgidLogin/values.yaml
  sed -i "s/snapshot_version/${snapshot_version}/g" springclouddemo/package.json

  # maven打包
	mvn clean package -Dpackage_version=${snapshot_version} -Dservice_name=${ServiceName}
	#将zip包复制到根目录
  cp ${PROJECT_PATH}/target/*.zip ${WORKSPACE}
}

#main函数入口
function main {
  build_iac_package
}

main