/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.demo.common.domain;

import java.beans.Transient;

import org.springframework.http.HttpStatus;

/**
 * 基础响应类
 *
 * @since 2024-01-26
 */
public class BaseResponse<T> {
    public static final String SUCCESS = "0";

    private String code;

    private String message;

    private T data;

    private Page page;

    public static <T> BaseResponse<T> success() {
        BaseResponse<T> rsp = new BaseResponse<>();
        rsp.setCode(String.valueOf(HttpStatus.OK.value()));
        rsp.setMessage(HttpStatus.OK.getReasonPhrase());
        return rsp;
    }

    public static <T> BaseResponse<T> success(T data, Page page) {
        BaseResponse<T> rsp = new BaseResponse<>();
        rsp.setCode(String.valueOf(HttpStatus.OK.value()));
        rsp.setMessage(HttpStatus.OK.getReasonPhrase());
        rsp.setData(data);
        rsp.setPage(page);
        return rsp;
    }

    public static <T> BaseResponse<T> success(T data) {
        BaseResponse<T> rsp = new BaseResponse<>();
        rsp.setCode(String.valueOf(HttpStatus.OK.value()));
        rsp.setMessage(HttpStatus.OK.getReasonPhrase());
        rsp.setData(data);
        return rsp;
    }

    @Transient
    public boolean isSuccessful() {
        return SUCCESS.equals(this.code);
    }

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    public T getData() {
        return this.data;
    }

    public BaseResponse<T> setCode(String code) {
        this.code = code;
        return this;
    }

    public BaseResponse<T> setMessage(String message) {
        this.message = message;
        return this;
    }

    public BaseResponse<T> setData(T data) {
        this.data = data;
        return this;
    }

    public BaseResponse<T> setPage(Page page) {
        this.page = page;
        return this;
    }

    public Page getPage() {
        return this.page;
    }
}