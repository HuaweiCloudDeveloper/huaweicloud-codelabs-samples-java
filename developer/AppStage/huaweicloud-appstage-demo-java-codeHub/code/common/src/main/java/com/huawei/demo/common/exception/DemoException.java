/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2023. All rights reserved.
 */

package com.huawei.demo.common.exception;

import java.text.MessageFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class DemoException extends RuntimeException {
    private static final long serialVersionUID = -3136099118955576754L;

    private int errorCode;

    private String errorDesc;

    public DemoException(int errorCode, String errorDesc) {
        super(errorDesc);
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
    }

    public DemoException(String errorDesc) {
        super(errorDesc);
        this.errorCode = 500;
        this.errorDesc = errorDesc;
    }


    /**
     * 构造函数
     *
     * @param pattern   消息格式
     * @param arguments 消息参数
     */
    public DemoException(String pattern, Object... arguments) {
        super(MessageFormat.format(pattern, arguments));
    }


    public DemoException(Exception e) {
        super(e);
        this.errorCode = 500;
        this.errorDesc = e.getMessage();
    }

    public DemoException(Throwable e) {
        super(e);
        this.errorCode = 500;
        this.errorDesc = e.getMessage();
    }
}
