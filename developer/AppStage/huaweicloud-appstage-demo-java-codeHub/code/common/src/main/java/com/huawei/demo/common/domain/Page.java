/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */

package com.huawei.demo.common.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 分页基础类
 *
 * @since 2023-07-31
 */
@Getter
@Setter
@ToString
public class Page implements Serializable {
    private static final int DEFAULT_INDEX = 1;

    private static final int DEFAULT_SIZE = 10;

    private Long offset = 0L;

    private Integer limit = 0;
    private Long count = 0L;

    public Page(Long offset, Integer limit) {
        this.limit = limit;
        this.offset = offset;
    }

    public void computeTotalPage(long totalCount) {
        this.count = totalCount;
    }
}
