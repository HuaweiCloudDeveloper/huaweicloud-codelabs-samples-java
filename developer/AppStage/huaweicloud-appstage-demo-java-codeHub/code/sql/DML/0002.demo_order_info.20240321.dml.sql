-- ----------------------------
-- Records of demo_order_info
-- ----------------------------
INSERT INTO `springclouddemob`.`demo_order_info` VALUES (1, '茅台', 2888, '北京故宫', now(), '1');
INSERT INTO `springclouddemob`.`demo_order_info` VALUES (2, '五粮液', 998, '北京圆明园', now(), '2');
INSERT INTO `springclouddemob`.`demo_order_info` VALUES (3, '剑南春', 1488, '北京颐和园', now(), '3');
INSERT INTO `springclouddemob`.`demo_order_info` VALUES (4, '哈尔滨啤酒', 8, '长沙市芯城科技园', now(), '4');