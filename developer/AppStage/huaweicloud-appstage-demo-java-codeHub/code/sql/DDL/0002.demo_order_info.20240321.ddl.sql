CREATE DATABASE `springclouddemob`;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for demo_order_info
-- ----------------------------
DROP TABLE IF EXISTS `springclouddemob`.`demo_order_info`;
CREATE TABLE `springclouddemob`.`demo_order_info`  (
  `order_id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `order_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单名称',
  `price` int UNSIGNED NOT NULL COMMENT '价格',
  `address` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配送地址',
  `create_time` datetime not NULL DEFAULT now() COMMENT '更新时间',
  `userId` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户Id',
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '订单demo表' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
