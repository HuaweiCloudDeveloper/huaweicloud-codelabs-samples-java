SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for demo_offering_info
-- ----------------------------
DROP TABLE IF EXISTS `demo_offering_info`;
CREATE TABLE `demo_offering_info`  (
   `offering_id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商品ID',
   `offering_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品名称',
   `category` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品类别',
   `price` int UNSIGNED NOT NULL COMMENT '价格',
   `inventory` int UNSIGNED NOT NULL COMMENT '价格',
   `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
   PRIMARY KEY (`offering_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品demo表' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;