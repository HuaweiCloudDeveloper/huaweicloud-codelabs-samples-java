import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'

export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@': resolve(__dirname, 'src'),
    },
  },

  server: {
    port: 5173,
    host: true,
    hmr: true,
    proxy: {
      '/offering': {
        target: 'http://10.132.59.27:8082',
        secure: false,
      },
      '/order': {
        target: 'http://10.132.59.27:8082',
        secure: false,
      },
    },
  },
})
