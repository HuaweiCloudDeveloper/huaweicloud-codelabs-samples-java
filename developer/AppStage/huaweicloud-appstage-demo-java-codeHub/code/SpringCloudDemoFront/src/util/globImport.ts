const iconModule = import.meta.glob('@/assets/icons/**/*', {
  eager: true, // 可tree-shaking,
  query: '?url'
});
    
 
const getTarget = (url:string, Arr:any) => {
  let target = {default:''};
  let ext = [ '', '.svg', '.png' ];
  ext.find(item => {
    target = Arr[url + item];
    return target;
  });
  
  return target?.default;
};


export const getIcon = (fileName: string) => {
  const url = `/src/assets/icons/${fileName}`;
  return getTarget(url, iconModule);
 
};
