import axios from "axios";
import { setCookie, getCookie, clearAllCookie } from "./tools";
const TIME_OUT_NUMBER = 12000; // 超时时间
const CONTENT_TYPE = "application/json";
const REQUEST_WITH = "XMLHttpRequest";
axios.interceptors.request.use((config) => {
  config.headers["Content-Type"] ??= CONTENT_TYPE;
  config.headers["X-Requested-With"] = REQUEST_WITH;
  return config;
});
class AppStageSurveyUIAxios {

  constructor({
    responsCallBack,
    responseErrorCallBack,
  }) {
    this.instance = axios.create({
      timeout: TIME_OUT_NUMBER,
      withCredentials: true, // 异步请求携带cookie
    });
    this.instance.interceptors.response.use(
      (response) => {
        // 对响应数据做些什么
        if (response.data.code && response.data.code !== '200') {
          if (responsCallBack) {
            responsCallBack(false, response);
          }
          return Promise.reject(response.data);
        } else {
          if (responsCallBack) {
            responsCallBack(true, response);
          }
          return Promise.resolve(response);
        }
      },
      (error) => {
        // 支持用户自定义错误拦截方法
        if (responseErrorCallBack) {
          responseErrorCallBack(error);
        }

        return Promise.reject(error);
      }
    );
  }

  get(url, params) {
    return this.instance.get(url, { params });
  }

  post(url, data, params) {
    return this.instance.post(url, data, { params });
  }

  delete(url, params) {
    return this.instance.delete(url, { params });
  }

  put(url, data, params) {
    return this.instance.put(url, data, { params });
  }
  postFile(url, data, params) {
    return this.instance.post(url, data, params);
  }

}

export default AppStageSurveyUIAxios;
