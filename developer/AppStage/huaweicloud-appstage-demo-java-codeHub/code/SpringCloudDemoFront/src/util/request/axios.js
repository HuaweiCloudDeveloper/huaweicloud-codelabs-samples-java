import AppStageSurveyUIAxios from './index.js';
import { Modal } from '@opentiny/vue';
import { get } from '@/util/utils';
// 接口请求响应code不为200处理
function responsCallback(flag, response) {
  if (!flag) {
    const msg = get(response, 'message', '请求异常');
    Modal.message({ message: msg, status: 'error' });
  }
}

// 错误处理 超时不提示错误
function responseErrorCallBack(err) {
  if (!err.message.includes('timeout')) {
    Modal.message({ message: err?.response?.data?.message || err.message || '接口异常', status: 'error', duration: '5000', top: 54, messageClosable: true });
  }
}
 
const axios = new AppStageSurveyUIAxios({ responsCallback: responsCallback, responseErrorCallBack: responseErrorCallBack });
export default axios;