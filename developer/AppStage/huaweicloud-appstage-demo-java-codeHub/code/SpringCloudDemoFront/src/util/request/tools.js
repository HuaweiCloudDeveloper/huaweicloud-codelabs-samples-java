export const setCookie = (name, value, path = '/') => {
  const exp = new Date();
  // 2 小时超时
  exp.setTime(exp.getTime() + 120 * 60 * 1000);
  document.cookie = `${name}=${escape(value)};expires=${exp.toGMTString()};path=${path};`;
};

// 设置cookie与超时时间，单位小时、Cookie使用路径,path: Cookie使用路径,默认为'/'，则本域名下contextPath都可以访问该Cookie
export const setCookieWithExpireTime = (name, value, expireTime, path = '/') => {
  const exp = new Date();
  // 1 小时超时
  exp.setTime(exp.getTime() + expireTime * 60 * 60 * 1000);
  document.cookie = `${name}=${escape(value)};expires=${exp.toGMTString()};path=${path};`;
};

export const getCookie = (name) => {
  const cookieArr = document.cookie.split(";");
  for (let i = 0; i < cookieArr.length; i++) {
    const cookiePair = cookieArr[i].split("=");
    const cookieName = cookiePair[0].trim();

    if (cookieName === name) {
      return decodeURIComponent(cookiePair[1]);
    }
  }
  return '';
}

// 清除所有cookie
export const clearAllCookie = () => {
  const keys = document.cookie.match(/[^ =;]+(?=)/g);
  if (keys) {
    for (let i = keys.length; i--; ) {
      document.cookie = `${keys[i]}=0;expires=${new Date(0).toUTCString()}`;
    }
  }
};

// 针对圆括号进行编码
export const encodeParentheses = str => {
  return str.replace(/\(/g, '%28').replace(/\)/g, '%29');
};