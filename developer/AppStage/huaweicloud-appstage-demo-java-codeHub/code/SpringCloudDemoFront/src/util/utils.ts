/**
 * lodash EOL
 *
 * You Might Not Need Lodash OpenSource Project
 *
 * 部分实现借鉴You Might Not Need Lodash，使用后请测试验证
 */

const _castPath = (path: string | string[] | number) => {
  if (!path) {
    throw new Error('invalid get path: ' + path);
  }

  if (typeof path === 'number') {
    path = String(path);
  }

  const pathArray = Array.isArray(path) ? path : path.match(/([^[.\]])+/g);
  if (!Array.isArray(pathArray)) {
    throw new Error('invalid get path: ' + path);
  }

  return pathArray;
}
// 获取第一个字符
export function getUserFirstChart(name: string) {
  // 判断name值的类型
  if (/^[\u4e00-\u9fa5]/.test(name)) {
    // 如果是中文，直接返回第一个字符
    return name.charAt(0);
  } else if (/^[a-zA-Z]/.test(name)) {
    // 如果是字母，返回第一个字母并转大写
    return name.charAt(0).toUpperCase();
  } else if (/^\d/.test(name)) {
    // 如果是数字，直接返回数字
    return parseInt(name.charAt(0));
  } else {
    // 如果不是中文、字母或数字，返回空字符串
    return "";
  }
}
export const timeToDate = function (timestamp:any) {
  const date = new Date(timestamp);
  const Y = date.getFullYear();
  const M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
  const D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
  const h = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
  const m = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
  const s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
  return Y + '/' + M + '/' + D + ' ' + h + ':' + m + ':' + s;
};
// 将毫秒数转换成天时分秒
export const formatDuring = function (mss) {
  if (mss === 0) return "0s";
  const days = parseInt(mss / (1000 * 60 * 60 * 24));
  const hours = parseInt((mss % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  const minutes = parseInt((mss % (1000 * 60 * 60)) / (1000 * 60));
  const seconds = (mss % (1000 * 60)) / 1000;
  return `${days ? days + "day" : ""}${hours ? hours + "h" : ""}${
    minutes ? minutes + "m" : ""
  }${seconds ? seconds + "s" : ""}`;
};

export function formatTime(str) {
  const year = str.substring(0, 4);
  const month = str.substring(4, 6);
  const day = str.substring(6, 8);
  const hour = str.substring(8, 10);
  const minute = str.substring(10, 12);
  const second = str.substring(12, 14);

  const date = `${year}/${month}/${day} ${hour}:${minute}:${second}`;
  return date;
}

export const cloneDeep = (value: any) => {
  return structuredClone(value)
}

/**
 * 防抖: 每次操作持续delay的时间内不会再次触发，每次操作会刷新delay，只支持trailing模式
 * @param func
 * @param delay
 */
export const debounce = (func: (args?: any[]) => any, delay: number) => {
  let timerId: number

  return (...args: any[]) => {
    clearTimeout(timerId)
    timerId = setTimeout(() => func(...args), delay)
  }
}

/**
 * 从obj中获取path下的值
 * @param obj
 * @param path
 * @param defValue
 */
export const get = (obj: any, path: string | string[], defValue: any) => {
  if(!obj){
    return defValue;
  }
  const pathArray = _castPath(path);
  const result = pathArray.reduce((prevObj: any, key: string) => prevObj && prevObj[key], obj);
  return result === undefined ? defValue : result
}
