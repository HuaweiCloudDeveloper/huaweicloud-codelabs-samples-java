import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import { getUserApi } from "@/api/common/index.ts";
import useGlobalStateStore from "@/store/modules/globalState";
import { Loading } from '@opentiny/vue'
const useGlobalState = useGlobalStateStore();
const routes: Array<RouteRecordRaw> = [
  {
    path: "",
    redirect: "/home",
  },
  {
    path: "/",
    redirect: "/home",
  },
  {
    path: "/home",
    redirect: "/orderManagement",
  },
  {
    path: "/orderManagement",
    name: "orderManagement",
    component: () => import("@/views/orderManagement/index.vue"),
  },
  {
    path: "/productInquiry",
    name: "productInquiry",
    component: () => import("@/views/myResearch/index.vue"),
  }
];

const router = createRouter({
  history: createWebHistory("demo"),
  routes,
});
// router.beforeEach(async (to, from, next) => {
//   if (useGlobalState.userInfo.userName) {
//     return next();
//   } else {
//     const loading = Loading.service({
//       lock: true,
//       text: '',
//       background: '#fff'
//     })
//     const res = await getUserApi();
//     useGlobalState.setUserInfo(res.data);
//     loading.close()
//     return next();
//   }
// });
export default router;
