import { defineStore } from "pinia"; 
interface GlobalState {
  userInfo: any;
}
 
 const useGlobalStateStore = defineStore("globalState", {
  state: (): GlobalState => ({
    userInfo:{},
  }),
  getters: {
    getUserInfo(): Object {
      return this.userInfo;
    },
  },
  actions: {
    setUserInfo(userInfo: Boolean) {
      this.userInfo = userInfo;
    },

  }
});

export default useGlobalStateStore