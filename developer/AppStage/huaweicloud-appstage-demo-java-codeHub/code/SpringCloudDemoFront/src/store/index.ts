import { createPinia } from "pinia";
import piniaPluginPersistedstate from "pinia-plugin-persistedstate";
import useGlobalStateStore from './modules/globalState'
 
// pinia persist
const pinia = createPinia();
useGlobalStateStore(pinia);
pinia.use(piniaPluginPersistedstate);
 
export default pinia;