import { createApp } from 'vue'
/** --- 新增代码 start --- */
import store from './store'
/** --- 新增代码 end --- */
import router from './router'

import ElementPlus from 'element-plus'

import 'element-plus/dist/index.css'

import './style.less'
import App from './App.vue'
import './assets/style/tinyRest.less';
import SvgIcon from './components/SvgIcon.vue'

const app = createApp(App)
/** --- 新增代码 start --- */
app.use(store)
/** --- 新增代码 end --- */
app.use(router)
app.use(ElementPlus)
app.component('SvgIcon', SvgIcon)
app.mount('#app')