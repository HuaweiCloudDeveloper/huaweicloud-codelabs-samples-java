import axios from "@/util/request/axios.js";

// 新增商品信息
export async function addShopsApi(params: Object) {
  const res = await axios.post("/offering/add", params);
  return res.data || {};
}
//查询订单列表
export async function getOrdersListApi(condition: any, orderId: any) {
  const res = await axios.get(
    `/order/list?offset=${(condition.index - 1) * condition.size}&limit=${condition.size}&orderId=${orderId}`
  );
  return res.data || {};
}

//查询商品列表
export async function getShopsListApi(condition: any, offeringName: any) {
  const res = await axios.get(
    `/offering/list?offset=${(condition.index - 1) * condition.size}&limit=${condition.size}&offeringName=${offeringName}`
  );
  return res.data || {};
}

//删除商品
export async function delShopApi(offeringId: any) {
  const res = await axios.delete(
    `/offering/delete?offeringId=${offeringId}`
  );
  return res.data || {};
}

