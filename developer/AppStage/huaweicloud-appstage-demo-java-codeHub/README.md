## 1、示例简介
本示例是华为云开发者团队基于AppStage项目技术支持实践，采用SpringCloud微服务架构，结合AppStage能力开发的开源项目，旨在为企业级开发者提供使用AppStage能力，将服务开发、构建、上云的技术参考，包括STS、CloudMap、WiseDb、SLB、OrgId登录认证等。更多AppStage相关技术细节可参考：华为云开发者文档中心AppStage应用平台开发指导（https://support.huaweicloud.com/bestpractice-appstage/appstage_09_0018.html）。

## 2、开发前准备
demo开发前资源准备、开发、构建、部署流程可以参考连接：https://support.huaweicloud.com/bestpractice-appstage/appstage_09_0018.html

以域名org-app.huawei.com、SLB绑定的公网IP为100.***.***.***为例,修改host文件,增加下列配置,实现域名映射:
100.***.***.*** org-app.huawei.com

浏览器访问demo地址: http://org-app.huawei.com:8080/index.html

## 3、架构图
![架构图](assets/Demo_architecture.PNG)
![登录流程图](assets/Demo_login_ process.PNG)

## 4、功能模块介绍
```
huaweicloud-appstage-demo-java-codeHub
├── code
│	├── common -- 公共依赖模块
│	│	├──src
│	│	├──pom.xml
│	├── DemoOrgidLogin                                           -- 登录功能模块
│	│	├──deploy_docker
│	│	│	├──bin                                               --启动脚本
│	│	│	├──configtemplate                                    --配置文件模板
│	│	│	├──resources                                         --资源文件
│	│	├──src                                                   --代码目录
│	│	├──.gitignore                                            --git提交忽略文件
│	│	├──Dockerfile                                            --docker构建脚本
│	│	├──package.json                                          --打包描述文件
│	│	├──pom.xml
│	├── DemoServiceA                                             --业务模块A，提供用户查询功能，目录结构与DemoOrgidLogin类似
│	├── DemoServiceB                                             --业务模块B，提供订单查询功能，目录结构与DemoOrgidLogin类似
│	├── iacspec  --iac脚本
│	│	├──script
│	│	└──springclouddemo
│	│		├── global/                                          -- global目录：放置所有规格目录所复用的配置文件
│	│       │
│	│		└── specs/                                           -- 环境特定的IaC描述，结构与global相同，但仅包含与global有差异的文件
│	│		    └── cn_product_cbu/                              -- 中国区生产环境，命名采用站点级Cloud Map的名称，可以在环境管理界面查看可选的站点级Cloud Map名称列表
│	│				├──DemoOrgidLogin
│	│				│	├── config                               -- 微服务配置目录
│	│				│	│    ├── config_records.yaml             -- 业务变量定义文件
│	│				│	│    └── config_schema.yaml
│	│				│	├── resources.yaml                       -- 微服务的资源列表
│	│				│	└── values.yaml                          -- 变量定义文件，被resources.yaml引用
│	│				├──DemoServiceA                              --目录结构与DemoOrgidLogin类似
│	│				└──DemoServiceB                              --目录结构与DemoOrgidLogin类似
│	├── sql  --sql脚本
│	├── .gitignore  --git提交忽略文件
│	└── pom.xml  --demo父pom
└── README.md
```

详细信息可参考https://support.huaweicloud.com/bestpractice-appstage/appstage_09_0018.html

## 5、技术选型<自定义章节>
```
技术	说明	官网
Spring-Cloud	微服务框架	https://spring.io/projects/spring-cloud
MyBatis-plus	ORM框架	https://baomidou.com/
K8S	华为云应用容器引擎CCE	https://support.huaweicloud.com/cce/index.html
Mysql	云数据库RDS	https://support.huaweicloud.com/rds/index.html
Redis	分布式缓存DCS服务	https://support.huaweicloud.com/intl/zh-cn/dcs/index.html
RabbitMQ	分布式消息队列 DMS	https://support.huaweicloud.com/intl/zh-cn/rabbitmq/index.html
Druid	数据库连接池	https://github.com/alibaba/druid
JWT	JWT登录支持	https://github.com/jwtk/jjwt
Lombok	简化对象封装工具	https://github.com/rzwitserloot/lombok
```

## 6、参考链接
华为云AppStage开发指导
https://support.huaweicloud.com/bestpractice-appstage/appstage_09_0018.html
