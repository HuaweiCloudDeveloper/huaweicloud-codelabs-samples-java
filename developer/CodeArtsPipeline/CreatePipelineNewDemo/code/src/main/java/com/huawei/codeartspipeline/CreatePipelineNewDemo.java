package com.huawei.codeartspipeline;

import com.huaweicloud.sdk.codeartspipeline.v2.CodeArtsPipelineClient;
import com.huaweicloud.sdk.codeartspipeline.v2.model.CodeSource;
import com.huaweicloud.sdk.codeartspipeline.v2.model.CodeSourceParams;
import com.huaweicloud.sdk.codeartspipeline.v2.model.CreatePipelineNewRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.CreatePipelineNewResponse;
import com.huaweicloud.sdk.codeartspipeline.v2.model.CustomVariable;
import com.huaweicloud.sdk.codeartspipeline.v2.model.PipelineDTO;
import com.huaweicloud.sdk.codeartspipeline.v2.region.CodeArtsPipelineRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.region.Region;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;

public class CreatePipelineNewDemo {

    private static final Logger logger = LoggerFactory.getLogger(CreatePipelineNewDemo.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String createPipeAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String createPipeSk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 注意，如下的 createPipeProjectId 请使用华为云console->iam我的凭证->对应region所属项目的项目ID
        String createPipeProjectId = "<Corresponding region Iam Project Id>";
        // 所需请求的region信息
        Region createPipeRegion = CodeArtsPipelineRegion.valueOf("cn-north-4");
        // 配置认证信息
        BasicCredentials createPipeAuth =
            new BasicCredentials().withAk(createPipeAk).withSk(createPipeSk).withProjectId(createPipeProjectId);
        // 创建服务客户端
        CodeArtsPipelineClient codeArtsPipelineClient = CodeArtsPipelineClient.newBuilder()
            .withCredential(createPipeAuth).withRegion(createPipeRegion).build();

        // 创建流水线
        try {
            logger.info("start to create a pipeline");
            CreatePipelineNewRequest createPipelineNewRequest = new CreatePipelineNewRequest();
            // 流水线所属的项目ID
            String codeArtsProjectId = "<ProjectId your pipeline belongs to>";
            createPipelineNewRequest.setProjectId(codeArtsProjectId);

            // 设置流水线基本信息
            PipelineDTO pipelineDTO = new PipelineDTO();
            String pipelineName = "<Name of the pipeline to be created>";
            String description = "<Description of the pipeline to be created>";
            // 是否为发布流水线
            Boolean isPublish = false;
            // 流水线版本信息，新版默认为3.0
            String manifestVersion = "<Pipeline Manifest Version, default to 3.0>";
            // 流水线定义信息
            String definition = "<Definition of pipeline structure>";
            pipelineDTO.setName(pipelineName);
            pipelineDTO.setDescription(description);
            pipelineDTO.setIsPublish(isPublish);
            pipelineDTO.setManifestVersion(manifestVersion);
            pipelineDTO.setDefinition(definition);

            // 设置流水线代码源
            CodeSource codeSource = generateCodeSource();
            pipelineDTO.setSources(Arrays.asList(codeSource));

            // 设置流水线自定义参数
            CustomVariable customVariable = generateCustomVariable();
            pipelineDTO.setVariables(Arrays.asList(customVariable));
            createPipelineNewRequest.setBody(pipelineDTO);

            CreatePipelineNewResponse response = codeArtsPipelineClient.createPipelineNew(createPipelineNewRequest);
            logger.info("create pipeline with projectId:{}, pipelineDto:{}, result:{}", codeArtsProjectId,
                pipelineDTO,response);
            logger.info("created pipeline ID: {}", response.getPipelineId());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }

    public static CodeSource generateCodeSource() {
        CodeSourceParams codeSourceParams = new CodeSourceParams();
        codeSourceParams.setGitType("<Git type>");
        codeSourceParams.setGitUrl("<Git link>");
        codeSourceParams.setSshGitUrl("<Ssh git link>");
        codeSourceParams.setWebUrl("<Web page url>");
        codeSourceParams.setRepoName("<Pipeline source name>");
        codeSourceParams.setDefaultBranch("<Default branch>");
        codeSourceParams.setCodehubId("<CodeHub code repository ID>");
        codeSourceParams.setEndpointId("<Extension point ID>");
        codeSourceParams.setAlias("<Code repository alias>");
        CodeSource codeSource = new CodeSource();
        codeSource.setType("<Pipeline Source Type>");
        codeSource.setParams(codeSourceParams);
        return codeSource;
    }

    public static CustomVariable generateCustomVariable() {
        CustomVariable customVariable = new CustomVariable();
        customVariable.setName("<Custom parameter name>");
        // User-defined Parameter Sequence
        customVariable.setSequence(1);
        customVariable.setType("<Custom Parameter Type>");
        customVariable.setValue("<Default values of customized parameters>");
        customVariable.setDescription("<User-defined Parameter Description>");
        // Indicates whether to set during running.
        customVariable.setIsRuntime(false);
        // Indicates whether the parameter is private.
        customVariable.setIsSecret(false);
        // Reset or Not
        customVariable.setIsReset(false);
        customVariable.setLatestValue("<Latest Parameter Value>");
        // Limited
        customVariable.setLimits(new ArrayList<>());
        return customVariable;
    }
}