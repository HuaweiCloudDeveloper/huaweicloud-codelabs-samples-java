## 1. 示例简介
华为云流水线CodeArtsPipeline服务可以帮助客户快速搭建和灵活编排从编译构建-代码检查-测试-部署的机制自动化持续交付能力，帮助研发团队有效减少手工操作和提升应用发布效率。

该示例展示如何通过 CodeArtsPipeline Java SDK 进行删除流水线操作。

## 2. 开发前准备
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 已在CodeArts平台创建流水线

## 3. 安装SDK
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartspipeline”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartspipeline</artifactId>
    <version>3.1.67</version>
</dependency>
```

## 4. 示例代码
```java
public class CreatePipelineNewDemo {

    private static final Logger logger = LoggerFactory.getLogger(CreatePipelineNewDemo.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String createPipeAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String createPipeSk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 注意，如下的 createPipeProjectId 请使用华为云console->iam我的凭证->对应region所属项目的项目ID
        String createPipeProjectId = "<Corresponding region Iam Project Id>";
        // 所需请求的region信息
        Region createPipeRegion = CodeArtsPipelineRegion.valueOf("cn-north-4");
        // 配置认证信息
        BasicCredentials createPipeAuth =
            new BasicCredentials().withAk(createPipeAk).withSk(createPipeSk).withProjectId(createPipeProjectId);
        // 创建服务客户端
        CodeArtsPipelineClient codeArtsPipelineClient = CodeArtsPipelineClient.newBuilder()
            .withCredential(createPipeAuth).withRegion(createPipeRegion).build();

        // 创建流水线
        try {
            logger.info("start to create a pipeline");
            CreatePipelineNewRequest createPipelineNewRequest = new CreatePipelineNewRequest();
            // 流水线所属的项目ID
            String codeArtsProjectId = "<ProjectId your pipeline belongs to>";
            createPipelineNewRequest.setProjectId(codeArtsProjectId);

            // 设置流水线基本信息
            PipelineDTO pipelineDTO = new PipelineDTO();
            String pipelineName = "<Name of the pipeline to be created>";
            String description = "<Description of the pipeline to be created>";
            // 是否为发布流水线
            Boolean isPublish = false;
            // 流水线版本信息，新版默认为3.0
            String manifestVersion = "<Pipeline Manifest Version, default to 3.0>";
            // 流水线定义信息
            String definition = "<Definition of pipeline structure>";
            pipelineDTO.setName(pipelineName);
            pipelineDTO.setDescription(description);
            pipelineDTO.setIsPublish(isPublish);
            pipelineDTO.setManifestVersion(manifestVersion);
            pipelineDTO.setDefinition(definition);

            // 设置流水线代码源
            CodeSource codeSource = generateCodeSource();
            pipelineDTO.setSources(Arrays.asList(codeSource));

            // 设置流水线自定义参数
            CustomVariable customVariable = generateCustomVariable();
            pipelineDTO.setVariables(Arrays.asList(customVariable));
            createPipelineNewRequest.setBody(pipelineDTO);

            CreatePipelineNewResponse response = codeArtsPipelineClient.createPipelineNew(createPipelineNewRequest);
            logger.info("create pipeline with projectId:{}, pipelineDto:{}, result:{}", codeArtsProjectId,
                pipelineDTO,response);
            logger.info("created pipeline ID: {}", response.getPipelineId());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }

    public static CodeSource generateCodeSource() {
        CodeSourceParams codeSourceParams = new CodeSourceParams();
        codeSourceParams.setGitType("<Git type>");
        codeSourceParams.setGitUrl("<Git link>");
        codeSourceParams.setSshGitUrl("<Ssh git link>");
        codeSourceParams.setWebUrl("<Web page url>");
        codeSourceParams.setRepoName("<Pipeline source name>");
        codeSourceParams.setDefaultBranch("<Default branch>");
        codeSourceParams.setCodehubId("<CodeHub code repository ID>");
        codeSourceParams.setEndpointId("<Extension point ID>");
        codeSourceParams.setAlias("<Code repository alias>");
        CodeSource codeSource = new CodeSource();
        codeSource.setType("<Pipeline Source Type>");
        codeSource.setParams(codeSourceParams);
        return codeSource;
    }

    public static CustomVariable generateCustomVariable() {
        CustomVariable customVariable = new CustomVariable();
        customVariable.setName("<Custom parameter name>");
        // User-defined Parameter Sequence
        customVariable.setSequence(1);
        customVariable.setType("<Custom Parameter Type>");
        customVariable.setValue("<Default values of customized parameters>");
        customVariable.setDescription("<User-defined Parameter Description>");
        // Indicates whether to set during running.
        customVariable.setIsRuntime(false);
        // Indicates whether the parameter is private.
        customVariable.setIsSecret(false);
        // Reset or Not
        customVariable.setIsReset(false);
        customVariable.setLatestValue("<Latest Parameter Value>");
        // Limited
        customVariable.setLimits(new ArrayList<>());
        return customVariable;
    }
}
```

相关参数说明如下所示：
- createPipeAk：华为云账号Access Key。
- createPipeSk：华为云账号Secret Access Key。
- createPipeRegion：服务所在区域。

## 5. 参考
更多api及参数详细说明信息请参考如下链接[流水线 CodeArtsPipeline Api详细说明文档](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsPipeline/sdk?api=ListPipelineRuns)
中的"流水线管理--新"目录下内容。

## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-11-15 | 1.0 | 文档首次发布 |