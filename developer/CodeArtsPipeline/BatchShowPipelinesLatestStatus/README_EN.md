### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeArtsPipeline/debug?api=BatchShowPipelinesLatestStatus).

2.In this example, you can obtain pipeline status in batches.

3.By obtaining pipeline status in batches, you can learn about the running status of each pipeline in a timely manner, including running, completed, and failed, facilitating exception handling in a timely manner.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. 
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. 
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A project and pipeline have been created on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-codeartspipeline. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartspipeline</artifactId>
    <version>3.1.123</version>
</dependency>
```

### Code example
``` java
package com.huawei.codeartspipeline;

import com.huaweicloud.sdk.codeartspipeline.v2.CodeArtsPipelineClient;
import com.huaweicloud.sdk.codeartspipeline.v2.model.BatchShowPipelinesLatestStatusRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.BatchShowPipelinesLatestStatusResponse;
import com.huaweicloud.sdk.codeartspipeline.v2.region.CodeArtsPipelineRegion;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.ArrayList;

public class BatchShowPipelinesLatestStatus {

    private static final Logger logger = LoggerFactory.getLogger(BatchShowPipelinesLatestStatus.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String showPipelinesLatestStatusAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showPipelinesLatestStatusSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(showPipelinesLatestStatusAk)
                .withSk(showPipelinesLatestStatusSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CodeArtsPipelineRegion.CN_NORTH_4.
        CodeArtsPipelineClient client = CodeArtsPipelineClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeArtsPipelineRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request and set the request parameter project ID and pipeline ID list.
        BatchShowPipelinesLatestStatusRequest request = new BatchShowPipelinesLatestStatusRequest();
        // Project ID. Note: Use the project ID on the home page of the corresponding project in CodeArts. Click the ID of a project link.
        // Example: https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/List
        String projectId = "<YOUR PROJECT ID>";
        request.withProjectId(projectId);
        List<String> listbodyBody = new ArrayList<>();
        // Pipeline ID. Pipeline_ID in the link on the pipeline details page
        // https://devcloud.cn-north-4.huaweicloud.com/cicd/project/{{PROJECT_ID}}/pipeline/detail/{{PIPELINE_ID}}/b11c06071fc14384a92a95*********?v=1
        String pipelineId = "<YOUR PIPELINE ID>";
        listbodyBody.add(pipelineId);
        request.withBody(listbodyBody);
        try {
            BatchShowPipelinesLatestStatusResponse response = client.batchShowPipelinesLatestStatus(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
```
[
 {
  "pipeline_id": "4e21****",
  "pipeline_run_id": "a6b8****",
  "executor_id": "f1ac****",
  "executor_name": "Developer",
  "executor_user_name": "demouser",
  "stage_status_list": [
   {
    "id": "4c4d****",
    "name": "构建和检查",
    "sequence": 0,
    "status": "COMPLETED",
    "start_time": "2024-10-12 09:52:56",
    "end_time": "2024-10-12 09:54:52"
   },
   {
    "id": "62e9****",
    "name": "部署和测试",
    "sequence": 1,
    "status": "FAILED",
    "start_time": "2024-10-12 09:54:53",
    "end_time": "2024-10-12 09:55:09"
   }
  ],
  "status": "FAILED",
  "run_number": 1,
  "trigger_type": "Manual",
  "build_params": {
   "action": null,
   "only_code_update": null,
   "build_type": "branch",
   "commit_id": "2369****",
   "event_type": "Manual",
   "merge_id": null,
   "message": "代码托管图标",
   "source_branch": null,
   "tag": null,
   "target_branch": "master",
   "codehub_id": "268****",
   "source_codehub_id": null,
   "source_codehub_url": null,
   "source_codehub_http_url": null,
   "target_codehub_id": null,
   "target_codehub_url": null,
   "target_codehub_http_url": null,
   "git_url": "https://codehub.devcloud.cn-north-4.huaweicloud.com/ad16****/javawebdemo.git",
   "human_name": null
  },
  "artifact_params": null,
  "start_time": 1728697976000,
  "end_time": 1728698109000,
  "modify_url": "https://devcloud.cn-north-4.huaweicloud.com/cicd/project/ad16****/pipeline/modify/4e21****?from=in-project&v=1",
  "detail_url": "https://devcloud.cn-north-4.huaweicloud.com/cicd/project/ad16****/pipeline/detail/4e21****/a6b8****?from=in-project&v=1"
 }
]
```
### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/11/25 | 1.0 | This document is released for the first time.