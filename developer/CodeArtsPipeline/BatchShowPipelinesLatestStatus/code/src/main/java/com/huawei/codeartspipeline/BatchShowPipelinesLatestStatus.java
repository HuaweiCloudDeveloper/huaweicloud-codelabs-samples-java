package com.huawei.codeartspipeline;

import com.huaweicloud.sdk.codeartspipeline.v2.CodeArtsPipelineClient;
import com.huaweicloud.sdk.codeartspipeline.v2.model.BatchShowPipelinesLatestStatusRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.BatchShowPipelinesLatestStatusResponse;
import com.huaweicloud.sdk.codeartspipeline.v2.region.CodeArtsPipelineRegion;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.ArrayList;

public class BatchShowPipelinesLatestStatus {

    private static final Logger logger = LoggerFactory.getLogger(BatchShowPipelinesLatestStatus.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String showPipelinesLatestStatusAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showPipelinesLatestStatusSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(showPipelinesLatestStatusAk)
                .withSk(showPipelinesLatestStatusSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CodeArtsPipelineRegion.CN_NORTH_4.
        CodeArtsPipelineClient client = CodeArtsPipelineClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeArtsPipelineRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request and set the request parameter project ID and pipeline ID list.
        BatchShowPipelinesLatestStatusRequest request = new BatchShowPipelinesLatestStatusRequest();
        // Project ID. Note: Use the project ID on the home page of the corresponding project in CodeArts. Click the ID of a project link.
        // Example: https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/List
        String projectId = "<YOUR PROJECT ID>";
        request.withProjectId(projectId);
        List<String> listbodyBody = new ArrayList<>();
        // Pipeline ID. Pipeline_ID in the link on the pipeline details page
        // https://devcloud.cn-north-4.huaweicloud.com/cicd/project/{{PROJECT_ID}}/pipeline/detail/{{PIPELINE_ID}}/b11c06071fc14384a92a95*********?v=1
        String pipelineId = "<YOUR PIPELINE ID>";
        listbodyBody.add(pipelineId);
        request.withBody(listbodyBody);
        try {
            BatchShowPipelinesLatestStatusResponse response = client.batchShowPipelinesLatestStatus(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}