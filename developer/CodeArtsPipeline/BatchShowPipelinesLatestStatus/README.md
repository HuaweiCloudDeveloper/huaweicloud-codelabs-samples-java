### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsPipeline/doc?api=BatchShowPipelinesLatestStatus) 中直接运行调试该接口。

2.在本样例中，您可以批量获取流水线状态。

3.通过批量获取流水线状态，可以及时了解每个流水线的运行情况，包括运行中、已完成、失败等状态，方便及时处理异常情况。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目，并且创建流水线。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartspipeline”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartspipeline</artifactId>
    <version>3.1.123</version>
</dependency>
```

### 代码示例
``` java
public class BatchShowPipelinesLatestStatus {

    private static final Logger logger = LoggerFactory.getLogger(BatchShowPipelinesLatestStatus.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String showPipelinesLatestStatusAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showPipelinesLatestStatusSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(showPipelinesLatestStatusAk)
                .withSk(showPipelinesLatestStatusSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeArtsPipelineRegion.CN_NORTH_4
        CodeArtsPipelineClient client = CodeArtsPipelineClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeArtsPipelineRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求，设置请求参数项目ID，流水线ID列表
        BatchShowPipelinesLatestStatusRequest request = new BatchShowPipelinesLatestStatusRequest();
        // 项目ID。注意,如下的 projectId 请使用CodeArts对应项目首页，点击某个项目链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/List
        String projectId = "<YOUR PROJECT ID>";
        request.withProjectId(projectId);
        List<String> listbodyBody = new ArrayList<>();
        // 流水线ID。流水线详情页面获取链接中的PIPELINE_ID部分
        // https://devcloud.cn-north-4.huaweicloud.com/cicd/project/{{PROJECT_ID}}/pipeline/detail/{{PIPELINE_ID}}/b11c06071fc14384a92a95*********?v=1
        String pipelineId = "<YOUR PIPELINE ID>";
        listbodyBody.add(pipelineId);
        request.withBody(listbodyBody);
        try {
            BatchShowPipelinesLatestStatusResponse response = client.batchShowPipelinesLatestStatus(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例
```
[
 {
  "pipeline_id": "4e21****",
  "pipeline_run_id": "a6b8****",
  "executor_id": "f1ac****",
  "executor_name": "Developer",
  "executor_user_name": "demouser",
  "stage_status_list": [
   {
    "id": "4c4d****",
    "name": "构建和检查",
    "sequence": 0,
    "status": "COMPLETED",
    "start_time": "2024-10-12 09:52:56",
    "end_time": "2024-10-12 09:54:52"
   },
   {
    "id": "62e9****",
    "name": "部署和测试",
    "sequence": 1,
    "status": "FAILED",
    "start_time": "2024-10-12 09:54:53",
    "end_time": "2024-10-12 09:55:09"
   }
  ],
  "status": "FAILED",
  "run_number": 1,
  "trigger_type": "Manual",
  "build_params": {
   "action": null,
   "only_code_update": null,
   "build_type": "branch",
   "commit_id": "2369****",
   "event_type": "Manual",
   "merge_id": null,
   "message": "代码托管图标",
   "source_branch": null,
   "tag": null,
   "target_branch": "master",
   "codehub_id": "268****",
   "source_codehub_id": null,
   "source_codehub_url": null,
   "source_codehub_http_url": null,
   "target_codehub_id": null,
   "target_codehub_url": null,
   "target_codehub_http_url": null,
   "git_url": "https://codehub.devcloud.cn-north-4.huaweicloud.com/ad16****/javawebdemo.git",
   "human_name": null
  },
  "artifact_params": null,
  "start_time": 1728697976000,
  "end_time": 1728698109000,
  "modify_url": "https://devcloud.cn-north-4.huaweicloud.com/cicd/project/ad16****/pipeline/modify/4e21****?from=in-project&v=1",
  "detail_url": "https://devcloud.cn-north-4.huaweicloud.com/cicd/project/ad16****/pipeline/detail/4e21****/a6b8****?from=in-project&v=1"
 }
]
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-11-25 | 1.0 | 文档首次发布 |