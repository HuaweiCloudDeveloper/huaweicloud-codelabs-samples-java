package com.huawei.codeartspipeline;

import com.huaweicloud.sdk.codeartspipeline.v2.CodeArtsPipelineClient;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelineRunsQuery;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelineRunsRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelineRunsResponse;
import com.huaweicloud.sdk.codeartspipeline.v2.region.CodeArtsPipelineRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.region.Region;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class ListPipelineRunsDemo {

    private static final Logger logger = LoggerFactory.getLogger(ListPipelineRunsDemo.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listPipeRunsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listPipeRunsSk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 注意，如下的listPipeRunsProjectId请使用华为云console->iam我的凭证->对应region所属项目的项目ID
        String listPipeRunsProjectId = "<Corresponding region Iam Project Id>";
        // 所需请求的region信息
        Region listPipeRunsRegion = CodeArtsPipelineRegion.valueOf("cn-north-4");
        // 配置认证信息
        BasicCredentials listPipeRunsAuth =
            new BasicCredentials().withAk(listPipeRunsAk).withSk(listPipeRunsSk).withProjectId(listPipeRunsProjectId);
        // 创建服务客户端
        CodeArtsPipelineClient codeArtsPipelineClient = CodeArtsPipelineClient.newBuilder()
                .withCredential(listPipeRunsAuth).withRegion(listPipeRunsRegion).build();

        // 获取流水线执行记录信息
        try {
            logger.info("start to get pipeline run records");
            ListPipelineRunsRequest listPipelineRunsRequest = new ListPipelineRunsRequest();
            // 流水线所属的项目ID
            String codeArtsProjectId = "<Project ID your pipeline belongs to>";
            // 查询执行记录的流水线ID
            String pipelineId = "<Pipeline ID for querying pipeline run records>";
            listPipelineRunsRequest.setProjectId(codeArtsProjectId);
            listPipelineRunsRequest.setPipelineId(pipelineId);

            // 查询的执行记录开始时间到结束时间
            String startTime = "<Start time of the pipeline run records>";
            String endTime = "<End time of the pipeline run records>";
            // 执行记录排序字段和排序方向
            String sortKey = "<Field used for sorting>";
            String sortDir = "<Sort direction>";
            // 分页参数
            // Pagination parameters. Number of records per page
            Long limit = 10L;
            // Pagination parameters. Page Record Offset
            Long offset = 0L;
            // 用于筛选的流水线状态。
            // Pipeline Status for Filtering
            List<String> statusList = Arrays.asList("COMPLETED", "RUNNING", "FAILED",
                "CANCELED", "PAUSED", "SUSPEND");
            ListPipelineRunsQuery listPipelineRunsQuery = new ListPipelineRunsQuery();
            listPipelineRunsQuery.setStartTime(startTime);
            listPipelineRunsQuery.setEndTime(endTime);
            listPipelineRunsQuery.setSortKey(sortKey);
            listPipelineRunsQuery.setSortDir(sortDir);
            listPipelineRunsQuery.setLimit(limit);
            listPipelineRunsQuery.setOffset(offset);
            listPipelineRunsQuery.setStatus(statusList);
            listPipelineRunsRequest.setBody(listPipelineRunsQuery);

            // 查询流水线执行记录
            ListPipelineRunsResponse listPipelineRunsResponse = codeArtsPipelineClient.listPipelineRuns(
                listPipelineRunsRequest);
            logger.info("query pipeline run records with projectId:{}, pipelineId:{}, query: {}, result:{}",
                codeArtsProjectId, pipelineId, listPipelineRunsQuery, listPipelineRunsResponse);
            if (listPipelineRunsResponse.getPipelineRuns().isEmpty()) {
                logger.info("pipeline run list is empty");
            }
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}