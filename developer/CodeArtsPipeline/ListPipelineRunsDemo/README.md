## 1. 示例简介
华为云流水线CodeArtsPipeline服务可以帮助客户快速搭建和灵活编排从编译构建-代码检查-测试-部署的机制自动化持续交付能力，帮助研发团队有效减少手工操作和提升应用发布效率。

该示例展示如何通过 CodeArtsPipeline Java SDK 进行查询流水线执行记录操作。

## 2. 开发前准备
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 已在CodeArts平台创建流水线

## 3. 安装SDK
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartspipeline”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartspipeline</artifactId>
    <version>3.1.67</version>
</dependency>
```

## 4. 示例代码
```java
public class ListPipelineRunsDemo {

    private static final Logger logger = LoggerFactory.getLogger(ListPipelineRunsDemo.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listPipeRunsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listPipeRunsSk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 注意，如下的listPipeRunsProjectId请使用华为云console->iam我的凭证->对应region所属项目的项目ID
        String listPipeRunsProjectId = "<Corresponding region Iam Project Id>";
        // 所需请求的region信息
        Region listPipeRunsRegion = CodeArtsPipelineRegion.valueOf("cn-north-4");
        // 配置认证信息
        BasicCredentials listPipeRunsAuth =
            new BasicCredentials().withAk(listPipeRunsAk).withSk(listPipeRunsSk).withProjectId(listPipeRunsProjectId);
        // 创建服务客户端
        CodeArtsPipelineClient codeArtsPipelineClient = CodeArtsPipelineClient.newBuilder()
                .withCredential(listPipeRunsAuth).withRegion(listPipeRunsRegion).build();

        // 获取流水线执行记录信息
        try {
            logger.info("start to get pipeline run records");
            ListPipelineRunsRequest listPipelineRunsRequest = new ListPipelineRunsRequest();
            // 流水线所属的项目ID
            String codeArtsProjectId = "<Project ID your pipeline belongs to>";
            // 查询执行记录的流水线ID
            String pipelineId = "<Pipeline ID for querying pipeline run records>";
            listPipelineRunsRequest.setProjectId(codeArtsProjectId);
            listPipelineRunsRequest.setPipelineId(pipelineId);

            // 查询的执行记录开始时间到结束时间
            String startTime = "<Start time of the pipeline run records>";
            String endTime = "<End time of the pipeline run records>";
            // 执行记录排序字段和排序方向
            String sortKey = "<Field used for sorting>";
            String sortDir = "<Sort direction>";
            // 分页参数
            // Pagination parameters. Number of records per page
            Long limit = 10L;
            // Pagination parameters. Page Record Offset
            Long offset = 0L;
            // 用于筛选的流水线状态。
            // Pipeline Status for Filtering
            List<String> statusList = Arrays.asList("COMPLETED", "RUNNING", "FAILED",
                "CANCELED", "PAUSED", "SUSPEND");
            ListPipelineRunsQuery listPipelineRunsQuery = new ListPipelineRunsQuery();
            listPipelineRunsQuery.setStartTime(startTime);
            listPipelineRunsQuery.setEndTime(endTime);
            listPipelineRunsQuery.setSortKey(sortKey);
            listPipelineRunsQuery.setSortDir(sortDir);
            listPipelineRunsQuery.setLimit(limit);
            listPipelineRunsQuery.setOffset(offset);
            listPipelineRunsQuery.setStatus(statusList);
            listPipelineRunsRequest.setBody(listPipelineRunsQuery);

            // 查询流水线执行记录
            ListPipelineRunsResponse listPipelineRunsResponse = codeArtsPipelineClient.listPipelineRuns(
                listPipelineRunsRequest);
            logger.info("query pipeline run records with projectId:{}, pipelineId:{}, query: {}, result:{}",
                codeArtsProjectId, pipelineId, listPipelineRunsQuery, listPipelineRunsResponse);
            if (listPipelineRunsResponse.getPipelineRuns().isEmpty()) {
                logger.info("pipeline run list is empty");
            }
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

相关参数说明如下所示：
- listPipeRunsAk：华为云账号Access Key。
- listPipeRunsSk：华为云账号Secret Access Key。
- listPipeRunsRegion：服务所在区域。

## 5. 参考
更多api及参数详细说明信息请参考如下链接[流水线 CodeArtsPipeline Api详细说明文档](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsPipeline/sdk?api=ListPipelineRuns)
中的"流水线管理--新"目录下内容。

## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-11-15 | 1.0 | 文档首次发布 |