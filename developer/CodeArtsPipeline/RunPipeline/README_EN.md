### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeArtsPipeline/doc?api=RunPipeline).

2.In this example, you can start the pipeline directly.
3.We can run the pipeline directly through this interface and embed the merge task of the custom code repository through
scripts.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A project and pipeline have been created on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-codeartspipeline. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartspipeline</artifactId>
    <version>3.1.119</version>
</dependency>

```

### Code example

``` java
package com.huawei.codeartspipeline;

import com.huaweicloud.sdk.codeartspipeline.v2.CodeArtsPipelineClient;
import com.huaweicloud.sdk.codeartspipeline.v2.model.RunPipelineDTO;
import com.huaweicloud.sdk.codeartspipeline.v2.model.RunPipelineRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.RunPipelineResponse;
import com.huaweicloud.sdk.codeartspipeline.v2.region.CodeArtsPipelineRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RunPipeline {

    private static final Logger logger = LoggerFactory.getLogger(RunPipeline.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String listPipelineTemplatesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listPipelineTemplatesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials().withAk(listPipelineTemplatesAk).withSk(listPipelineTemplatesSk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // Create a service client and set the corresponding region to CloudPipelineRegion.CN_NORTH_4.
        CodeArtsPipelineClient client = CodeArtsPipelineClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeArtsPipelineRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // Create Request Header
        RunPipelineRequest request = new RunPipelineRequest();
        // Set the project ID and pipeline ID on the pipeline details page, and obtain the project ID and pipeline ID in the link.
        // https://devcloud.cn-north-4.huaweicloud.com/cicd/project/{{PROJECT_ID}}/pipeline/detail/{{PIPELINE_ID}}/b11c06071fc14384a92a95*********?v=1
        String projectId = "<YOUR PROJECT ID>";
        String pipelineId = "<YOUR PIPELINE ID>";
        request.withProjectId(projectId);
        request.withPipelineId(pipelineId);
        RunPipelineDTO body = new RunPipelineDTO();
        request.withBody(body);
        try {
            RunPipelineResponse response = client.runPipeline(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result

```
{
 "pipeline_run_id": "b11c06071fc14384a92a95a*******"
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/10/28 | 1.0 | This document is released for the first time.