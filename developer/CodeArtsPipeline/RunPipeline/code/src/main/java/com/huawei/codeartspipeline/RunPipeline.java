package com.huawei.codeartspipeline;

import com.huaweicloud.sdk.codeartspipeline.v2.CodeArtsPipelineClient;
import com.huaweicloud.sdk.codeartspipeline.v2.model.RunPipelineDTO;
import com.huaweicloud.sdk.codeartspipeline.v2.model.RunPipelineRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.RunPipelineResponse;
import com.huaweicloud.sdk.codeartspipeline.v2.region.CodeArtsPipelineRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RunPipeline {

    private static final Logger logger = LoggerFactory.getLogger(RunPipeline.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String listPipelineTemplatesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listPipelineTemplatesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials().withAk(listPipelineTemplatesAk).withSk(listPipelineTemplatesSk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // Create a service client and set the corresponding region to CloudPipelineRegion.CN_NORTH_4.
        CodeArtsPipelineClient client = CodeArtsPipelineClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeArtsPipelineRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();
        // Create Request Header
        RunPipelineRequest request = new RunPipelineRequest();
        // Set the project ID and pipeline ID on the pipeline details page, and obtain the project ID and pipeline ID in the link.
        // https://devcloud.cn-north-4.huaweicloud.com/cicd/project/{{PROJECT_ID}}/pipeline/detail/{{PIPELINE_ID}}/b11c06071fc14384a92a95*********?v=1
        String projectId = "<YOUR PROJECT ID>";
        String pipelineId = "<YOUR PIPELINE ID>";
        request.withProjectId(projectId);
        request.withPipelineId(pipelineId);
        RunPipelineDTO body = new RunPipelineDTO();
        request.withBody(body);
        try {
            RunPipelineResponse response = client.runPipeline(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}