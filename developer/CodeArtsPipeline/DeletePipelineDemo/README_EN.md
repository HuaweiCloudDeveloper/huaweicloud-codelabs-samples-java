## 1. Example Introduction

HUAWEI CLOUD pipeline CodeArtsPipeline helps customers quickly set up and flexibly orchestrate automatic continuous
delivery capabilities from compilation and building, code check, test, and deployment, helping R&D teams reduce manual
operations and improve application release efficiency.
This example shows how to use the CodeArtsPipeline Java SDK to delete a pipeline.

## 2. Preparations Before Development

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A pipeline has been created on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-codeartspipeline. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartspipeline</artifactId>
    <version>3.1.67</version>
</dependency>
```

## 4. Sample Code

```java
package com.huawei.codeartspipeline;

import com.huaweicloud.sdk.codeartspipeline.v2.CodeArtsPipelineClient;
import com.huaweicloud.sdk.codeartspipeline.v2.model.DeletePipelineRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.DeletePipelineResponse;
import com.huaweicloud.sdk.codeartspipeline.v2.region.CodeArtsPipelineRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.region.Region;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeletePipelineDemo {

    private static final Logger logger = LoggerFactory.getLogger(DeletePipelineDemo.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String deletePipeAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String deletePipeSk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Note: For the following deletePipeProjectId, choose HUAWEI CLOUD Console > IAM My Credential > the project ID of the corresponding region.
        String deletePipeProjectId = "<Corresponding region Iam Project ID>";
        // Requested region information
        Region deletePipeRegion = CodeArtsPipelineRegion.valueOf("cn-north-4");
        // Configuring Authentication Information
        BasicCredentials deletePipeAuth = new BasicCredentials().withAk(deletePipeAk)
            .withSk(deletePipeSk)
            .withProjectId(deletePipeProjectId);
        // Creating a Service Client
        CodeArtsPipelineClient codeArtsPipelineClient = CodeArtsPipelineClient.newBuilder()
            .withCredential(deletePipeAuth)
            .withRegion(deletePipeRegion)
            .build();
        // Deleting a pipeline
        try {
            logger.info("start to delete a pipeline");
            DeletePipelineRequest deletePipelineRequest = new DeletePipelineRequest();
            // ID of the pipeline to be deleted.
            String pipelineId = "<Pipeline ID that needs to be deleted>";
            deletePipelineRequest.setPipelineId(pipelineId);
            // ID of the project to which the pipeline belongs.
            String codeArtsProjectId = "<Project ID your pipeline belongs to>";
            deletePipelineRequest.setProjectId(codeArtsProjectId);
            DeletePipelineResponse deletePipelineResponse = codeArtsPipelineClient.deletePipeline(
                deletePipelineRequest);
            logger.info("delete pipeline with projectId:{}, pipelineId:{}, result:{}", codeArtsProjectId, pipelineId,
                deletePipelineResponse);
            logger.info("deleted pipelineId: {}", deletePipelineResponse.getPipelineId());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

The parameters are described as follows:：

- deletePipeAk：HUAWEI CLOUD account access key。
- deletePipeSk：Secret Access Key of HUAWEI CLOUD Account。
- deletePipeRegion：Region where the service is located。

## 5. References

You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeArtsPipeline/doc?api=ListPipelineRuns).

## 6. Change History

| Release Date | Document Version |             Revision Description              |
|:------------:|:----------------:|:---------------------------------------------:|
|  2024-10-28  |       1.0        | This document is released for the first time. |