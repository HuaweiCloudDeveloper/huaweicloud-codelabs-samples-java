package com.huawei.codeartspipeline;

import com.huaweicloud.sdk.codeartspipeline.v2.CodeArtsPipelineClient;
import com.huaweicloud.sdk.codeartspipeline.v2.model.DeletePipelineRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.DeletePipelineResponse;
import com.huaweicloud.sdk.codeartspipeline.v2.region.CodeArtsPipelineRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.region.Region;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeletePipelineDemo {

    private static final Logger logger = LoggerFactory.getLogger(DeletePipelineDemo.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String deletePipeAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String deletePipeSk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Note: For the following deletePipeProjectId, choose HUAWEI CLOUD Console > IAM My Credential > the project ID of the corresponding region.
        String deletePipeProjectId = "<Corresponding region Iam Project ID>";
        // Requested region information
        Region deletePipeRegion = CodeArtsPipelineRegion.valueOf("cn-north-4");
        // Configuring Authentication Information
        BasicCredentials deletePipeAuth = new BasicCredentials().withAk(deletePipeAk)
            .withSk(deletePipeSk)
            .withProjectId(deletePipeProjectId);
        // Creating a Service Client
        CodeArtsPipelineClient codeArtsPipelineClient = CodeArtsPipelineClient.newBuilder()
            .withCredential(deletePipeAuth)
            .withRegion(deletePipeRegion)
            .build();
        // Deleting a pipeline
        try {
            logger.info("start to delete a pipeline");
            DeletePipelineRequest deletePipelineRequest = new DeletePipelineRequest();
            // ID of the pipeline to be deleted.
            String pipelineId = "<Pipeline ID that needs to be deleted>";
            deletePipelineRequest.setPipelineId(pipelineId);
            // ID of the project to which the pipeline belongs.
            String codeArtsProjectId = "<Project ID your pipeline belongs to>";
            deletePipelineRequest.setProjectId(codeArtsProjectId);
            DeletePipelineResponse deletePipelineResponse = codeArtsPipelineClient.deletePipeline(
                deletePipelineRequest);
            logger.info("delete pipeline with projectId:{}, pipelineId:{}, result:{}", codeArtsProjectId, pipelineId,
                deletePipelineResponse);
            logger.info("deleted pipelineId: {}", deletePipelineResponse.getPipelineId());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}