### Product Description

1.You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeArtsPipeline/doc?api=ListPipelineTemplates).

2.In this example, you can obtain the pipeline template list of a CodeArts project.

3.This interface is used to obtain the template list of a project, query template details, and create pipelines based on
the template.

### Prerequisites

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A project has been created on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-codeartspipeline. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartspipeline</artifactId>
    <version>3.1.119</version>
</dependency>

```

### Code example

``` java
package com.huawei.codeartspipeline;

import com.huaweicloud.sdk.codeartspipeline.v2.CodeArtsPipelineClient;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelineTemplatesQuery;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelineTemplatesRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelineTemplatesResponse;
import com.huaweicloud.sdk.codeartspipeline.v2.region.CodeArtsPipelineRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListPipelineTemplates {

    private static final Logger logger = LoggerFactory.getLogger(ListPipelineTemplates.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String listPipelineTemplatesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listPipelineTemplatesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials().withAk(listPipelineTemplatesAk).withSk(listPipelineTemplatesSk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // Create a service client and set the corresponding region to CloudPipelineRegion.CN_NORTH_4.
        CodeArtsPipelineClient client = CodeArtsPipelineClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeArtsPipelineRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();

        // Construct the request header and set the request parameter tenant ID.
        ListPipelineTemplatesRequest request = new ListPipelineTemplatesRequest();
        // Tenant ID. Log in to HUAWEI CLOUD and click Console in the upper right corner. On the Console page, move the cursor to the username in the upper right corner and select My Credential from the drop-down list.
        // On the My Credential page, view the value of Account ID on the API Credential page.
        String tenantId = "<YOUR TENANT ID>";
        request.withTenantId(tenantId);
        ListPipelineTemplatesQuery body = new ListPipelineTemplatesQuery();
        request.withBody(body);
        try {
            // Querying the Template List
            ListPipelineTemplatesResponse response = client.listPipelineTemplates(request);
            logger.info("ListPipelineTemplates: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListPipelineTemplates connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListPipelineTemplates RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListPipelineTemplates ServiceResponseException", e);
        }
    }
}
```

### Example of the returned result

```
{
 "status": "success",
 "result": {
  "id": "8acc4e676c404dd285dd************",
  "name": "ceshiname",
  "description": "",
  "os": "linux",
  "permission": {
   "can_view": true,
   "can_delete": true,
   "can_edit": true,
   "can_add_host": true,
   "can_manage": true
  },
  "created_by": {
   "user_id": "49d9a304ab704e178**********",
   "user_name": "ceshi"
  },
  "slave_cluster_id": "",
  "nick_name": "ceshi",
  "is_proxy_mode": 1,
  "updated_time": "2024-04-01 17:03:57",
  "created_time": "2024-04-01 17:03:57"
 }
}
```

### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/10/28 | 1.0 | This document is released for the first time.