### 产品介绍

1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsPipeline/sdk?api=ListPipelineTemplates)
中直接运行调试该接口。
2.在本样例中，您可以获取到CodeArts某个项目下的流水线模板列表。
3.获取项目下的模模板列表，可以查询模板详情，根据模板创建流水线等操作。

### 前置条件

1.已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register)
华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
2.已具备开发环境 ，支持Java JDK 1.8及其以上版本。
3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 >
访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
4.已在CodeArts平台创建项目。
5.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartspipeline”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartspipeline</artifactId>
    <version>3.1.119</version>
</dependency>
```

### 代码示例

```java
package com.huawei.codeartspipeline;

import com.huaweicloud.sdk.codeartspipeline.v2.CodeArtsPipelineClient;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelineTemplatesQuery;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelineTemplatesRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelineTemplatesResponse;
import com.huaweicloud.sdk.codeartspipeline.v2.region.CodeArtsPipelineRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListPipelineTemplates {

    private static final Logger logger = LoggerFactory.getLogger(ListPipelineTemplates.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String listPipelineTemplatesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listPipelineTemplatesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials().withAk(listPipelineTemplatesAk).withSk(listPipelineTemplatesSk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // 创建服务客户端并配置对应region为CloudPipelineRegion.CN_NORTH_4
        CodeArtsPipelineClient client = CodeArtsPipelineClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeArtsPipelineRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();

        // 构建请求头，设置请求参数租户ID
        ListPipelineTemplatesRequest request = new ListPipelineTemplatesRequest();
        // 租户ID，登录华为云，在右上角单击“控制台”。在“控制台”页面，鼠标移动至右上方的用户名，在下拉列表中选择“我的凭证”。
        // 在“我的凭证”页面，查看“API凭证”页面中账号ID的值。
        String tenantId = "<YOUR TENANT ID>";
        request.withTenantId(tenantId);
        ListPipelineTemplatesQuery body = new ListPipelineTemplatesQuery();
        request.withBody(body);
        try {
            // 查询模板列表
            ListPipelineTemplatesResponse response = client.listPipelineTemplates(request);
            logger.info("ListPipelineTemplates: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListPipelineTemplates connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListPipelineTemplates RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListPipelineTemplates ServiceResponseException", e);
        }
    }
}
```

### 返回结果示例

```
{
 "offset": 0,
 "limit": 10,
 "total": 12,
 "templates": [
  {
   "id": "8ea4d****",
   "name": "新手上路",
   "icon": null,
   "manifest_version": "3.0",
   "language": "java",
   "description": "流水线新手体验模板",
   "is_system": true,
   "region": "system",
   "domain_id": "system",
   "creator_id": "system",
   "creator_name": "system",
   "updater_id": "Cloud Pipeline",
   "create_time": 1680877027000,
   "update_time": 1690813335000,
   "is_collect": false,
   "is_show_source": null,
   "stages": [
    {
     "name": "代码检查",
     "sequence": 0
    },
    {
     "name": "构建阶段",
     "sequence": 1
    },
    {
     "name": "部署发布",
     "sequence": 2
    }
   ]
  },
  {
   "id": "5e1f7****",
   "name": "SpringBoot编译部署",
   "icon": "ca60-springbot",
   "manifest_version": "3.0",
   "language": "Java",
   "description": "针对SpringBoot框架的编译构建、代码检查、打包部署、接口测试模板",
   "is_system": true,
   "region": "system",
   "domain_id": "system",
   "creator_id": "system",
   "creator_name": "system",
   "updater_id": "5e1f7****",
   "create_time": 1673639836000,
   "update_time": 1689838244000,
   "is_collect": false,
   "is_show_source": null,
   "stages": [
    {
     "name": "构建和检查",
     "sequence": 0
    },
    {
     "name": "部署和测试",
     "sequence": 1
    }
   ]
  }
 ]
}
```

### 修订记录

|    发布日期    | 文档版本 |  修订说明  |
|:----------:|:----:|:------:|
| 2024-08-26 | 1.0  | 文档首次发布 |