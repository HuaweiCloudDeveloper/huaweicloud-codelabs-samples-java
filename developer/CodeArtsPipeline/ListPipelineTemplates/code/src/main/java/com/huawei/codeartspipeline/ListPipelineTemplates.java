package com.huawei.codeartspipeline;

import com.huaweicloud.sdk.codeartspipeline.v2.CodeArtsPipelineClient;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelineTemplatesQuery;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelineTemplatesRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelineTemplatesResponse;
import com.huaweicloud.sdk.codeartspipeline.v2.region.CodeArtsPipelineRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListPipelineTemplates {

    private static final Logger logger = LoggerFactory.getLogger(ListPipelineTemplates.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String listPipelineTemplatesAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listPipelineTemplatesSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials().withAk(listPipelineTemplatesAk).withSk(listPipelineTemplatesSk);

        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();
        // Create a service client and set the corresponding region to CloudPipelineRegion.CN_NORTH_4.
        CodeArtsPipelineClient client = CodeArtsPipelineClient.newBuilder()
            .withCredential(auth)
            .withRegion(CodeArtsPipelineRegion.CN_NORTH_4)
            .withHttpConfig(httpConfig)
            .build();

        // Construct the request header and set the request parameter tenant ID.
        ListPipelineTemplatesRequest request = new ListPipelineTemplatesRequest();
        // Tenant ID. Log in to HUAWEI CLOUD and click Console in the upper right corner. On the Console page, move the cursor to the username in the upper right corner and select My Credential from the drop-down list.
        // On the My Credential page, view the value of Account ID on the API Credential page.
        String tenantId = "<YOUR TENANT ID>";
        request.withTenantId(tenantId);
        ListPipelineTemplatesQuery body = new ListPipelineTemplatesQuery();
        request.withBody(body);
        try {
            // Querying the Template List
            ListPipelineTemplatesResponse response = client.listPipelineTemplates(request);
            logger.info("ListPipelineTemplates: " + response.toString());
        } catch (ConnectionException e) {
            logger.error("ListPipelineTemplates connection error", e);
        } catch (RequestTimeoutException e) {
            logger.error("ListPipelineTemplates RequestTimeoutException", e);
        } catch (ServiceResponseException e) {
            logger.error("ListPipelineTemplates ServiceResponseException", e);
        }
    }
}