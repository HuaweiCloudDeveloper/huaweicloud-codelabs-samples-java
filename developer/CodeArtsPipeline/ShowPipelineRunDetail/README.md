### 产品介绍
1.您可以在 [API Explorer](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsPipeline/debug?api=ShowPipelineRunDetail) 中直接运行调试该接口。

2.在本样例中，您可以获取流水线状态/获取流水线执行详情。

3.获取流水线状态/获取流水线执行详情，可以实时了解到流水线的当前状态，包括是否正在运行、是否已经完成等。此外，这个接口还可以提供关于流水线执行过程中的详细信息，如执行时间、执行步骤等。

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)。

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

5.已在CodeArts平台创建项目，创建流水线。

6.您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartspipeline”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartspipeline</artifactId>
    <version>3.1.123</version>
</dependency>
```

### 代码示例
``` java
public class ShowPipelineRunDetail {

    private static final Logger logger = LoggerFactory.getLogger(ShowPipelineRunDetail.class.getName());

    public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String showPipelineRunDetailAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showPipelineRunDetailSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // 配置认证信息
        ICredential auth = new BasicCredentials()
                .withAk(showPipelineRunDetailAk)
                .withSk(showPipelineRunDetailSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // 创建服务客户端并配置对应region为CodeArtsPipelineRegion.CN_NORTH_4
        CodeArtsPipelineClient client = CodeArtsPipelineClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeArtsPipelineRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // 构建请求，设置请求参数项目ID，流水线ID，流水线运行实例ID
        ShowPipelineRunDetailRequest request = new ShowPipelineRunDetailRequest();

        // 项目ID。注意,如下的 projectId 请使用CodeArts对应项目首页，点击某个项目链接中的ID
        // 例:https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/List
        String projectId = "<YOUR PROJECT ID>";
        request.withProjectId(projectId);
        // 流水线ID。流水线详情页面获取链接中的PIPELINE_ID部分
        // https://devcloud.cn-north-4.huaweicloud.com/cicd/project/{{PROJECT_ID}}/pipeline/detail/{{PIPELINE_ID}}/{{PIPELINE_RUN_ID}}?v=1
        String pipelineId = "<YOUR PIPELINE ID>";
        request.withPipelineId(pipelineId);
        // 流水线运行实例ID。流水线详情页面获取链接中的PIPELINE_RUN_ID部分
        // https://devcloud.cn-north-4.huaweicloud.com/cicd/project/{{PROJECT_ID}}/pipeline/detail/{{PIPELINE_ID}}/{{PIPELINE_RUN_ID}}?v=1
        String pipelineRunId = "<YOUR PIPELINE RUN ID>";
        request.withPipelineRunId(pipelineRunId);
        try {
            ShowPipelineRunDetailResponse response = client.showPipelineRunDetail(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### 返回结果示例
```
{
 "id": "d303****",
 "pipeline_id": "4e21****",
 "manifest_version": "3.0",
 "name": "javawebdemo-pipeline",
 "description": "",
 "is_publish": false,
 "executor_id": "f1ac****",
 "executor_name": "Developer",
 "executor_user_name": "demouser",
 "status": "COMPLETED",
 "trigger_type": "Manual",
 "run_number": 2,
 "start_time": 1732498157000,
 "end_time": 1732498306000,
 "current_system_time": 1732671170269,
 "stages": [
  {
   "id": "d92b****",
   "category": null,
   "name": "构建和检查",
   "identifier": "0",
   "run_always": false,
   "parallel": null,
   "is_select": true,
   "sequence": 0,
   "depends_on": [],
   "condition": null,
   "status": "COMPLETED",
   "start_time": 1732498158000,
   "end_time": 1732498306000,
   "pre": [
    {
     "name": null,
     "task": "official_devcloud_autoTrigger",
     "business_type": null,
     "inputs": null,
     "sequence": 0,
     "official_task_version": null,
     "identifier": null,
     "multi_step_editable": 0,
     "endpoint_ids": null,
     "id": "8eaa****",
     "last_dispatch_id": "c2fe****",
     "status": "COMPLETED",
     "message": null,
     "start_time": 1732498158000,
     "end_time": 1732498158000,
     "daily_build_number": null
    }
   ],
   "post": [],
   "jobs": [
    {
     "id": "9d33****",
     "category": null,
     "sequence": 0,
     "async": null,
     "name": "构建",
     "identifier": "1668****",
     "depends_on": [],
     "condition": "${{ completed() }}",
     "resource": "{\"type\":\"system\",\"arch\":\"x86\"}",
     "is_select": true,
     "timeout": "",
     "last_dispatch_id": "b518****",
     "status": "COMPLETED",
     "message": null,
     "start_time": 1732498158000,
     "end_time": 1732498220000,
     "exec_id": "j_fY****",
     "steps": [
      {
       "name": "构建",
       "task": "official_devcloud_cloudBuild",
       "business_type": "Build",
       "inputs": [
        {
         "key": "codeBranch",
         "value": "master"
        },
        {
         "key": "jobId",
         "value": "d06e****"
        },
        {
         "key": "__repository__",
         "value": "javawebdemo"
        },
        {
         "key": "artifactIdentifier",
         "value": ""
        }
       ],
       "sequence": 0,
       "official_task_version": null,
       "identifier": null,
       "multi_step_editable": 0,
       "endpoint_ids": [],
       "id": "5b64****",
       "last_dispatch_id": "b1f7****",
       "status": "COMPLETED",
       "message": null,
       "start_time": 1732498158000,
       "end_time": 1732498220000,
       "daily_build_number": "20241125.1"
      }
     ]
    },
    {
     "id": "10f9****",
     "category": null,
     "sequence": 1,
     "async": null,
     "name": "代码检查",
     "identifier": "1668****",
     "depends_on": [],
     "condition": "${{ completed() }}",
     "resource": null,
     "is_select": true,
     "timeout": "",
     "last_dispatch_id": "c7ca****",
     "status": "COMPLETED",
     "message": null,
     "start_time": 1732498158000,
     "end_time": 1732498306000,
     "exec_id": "j_Jp****",
     "steps": [
      {
       "name": "代码检查",
       "task": "official_devcloud_codeCheck",
       "business_type": "Build",
       "inputs": [
        {
         "key": "module_or_template_id",
         "value": "d7df****"
        },
        {
         "key": "jobId",
         "value": "7c6e****"
        },
        {
         "key": "repository_readonly",
         "value": "javawebdemo"
        },
        {
         "key": "checkMode",
         "value": "full"
        },
        {
         "key": "codeBranch",
         "value": "master"
        }
       ],
       "sequence": 0,
       "official_task_version": null,
       "identifier": null,
       "multi_step_editable": 0,
       "endpoint_ids": null,
       "id": "9eb1****",
       "last_dispatch_id": "9896****",
       "status": "COMPLETED",
       "message": null,
       "start_time": 1732498158000,
       "end_time": 1732498306000,
       "daily_build_number": null
      }
     ]
    }
   ]
  },
  {
   "id": "ef37****",
   "category": null,
   "name": "部署和测试",
   "identifier": null,
   "run_always": null,
   "parallel": null,
   "is_select": false,
   "sequence": 1,
   "depends_on": [
    "0"
   ],
   "condition": null,
   "status": "UNSELECTED",
   "start_time": null,
   "end_time": null,
   "pre": [
    {
     "name": null,
     "task": "official_devcloud_autoTrigger",
     "business_type": null,
     "inputs": null,
     "sequence": 0,
     "official_task_version": null,
     "identifier": null,
     "multi_step_editable": 0,
     "endpoint_ids": null,
     "id": "b89d****",
     "last_dispatch_id": null,
     "status": "UNSELECTED",
     "message": null,
     "start_time": null,
     "end_time": null,
     "daily_build_number": null
    }
   ],
   "post": [],
   "jobs": [
    {
     "id": "4a7a****",
     "category": null,
     "sequence": 0,
     "async": null,
     "name": "部署",
     "identifier": "1668****",
     "depends_on": [],
     "condition": "${{ completed() }}",
     "resource": "{\"type\":\"system\",\"arch\":\"x86\"}",
     "is_select": false,
     "timeout": "",
     "last_dispatch_id": null,
     "status": "UNSELECTED",
     "message": null,
     "start_time": null,
     "end_time": null,
     "exec_id": null,
     "steps": [
      {
       "name": "部署",
       "task": "official_devcloud_deploy",
       "business_type": "Deploy",
       "inputs": [
        {
         "key": "host_group",
         "value": "2315****"
        },
        {
         "key": "package_url",
         "value": "/javawebdemo-build/"
        },
        {
         "key": "service_port",
         "value": "8080"
        },
        {
         "key": "package_name",
         "value": "demoapp"
        },
        {
         "key": "relatedPipelineTask",
         "value": ""
        },
        {
         "key": "SYSTEM_DEVCLOUD_SUBPIPELINE_TRIGGER_ID",
         "value": "e0b4****"
        },
        {
         "key": "_OFFICIAL_DEVCLOUD_JOB_NAME_",
         "value": "javawebdemo-deploy"
        }
       ],
       "sequence": 0,
       "official_task_version": null,
       "identifier": null,
       "multi_step_editable": 0,
       "endpoint_ids": [],
       "id": "e797****",
       "last_dispatch_id": null,
       "status": "UNSELECTED",
       "message": null,
       "start_time": null,
       "end_time": null,
       "daily_build_number": null
      }
     ]
    },
    {
     "id": "5816****",
     "category": null,
     "sequence": 0,
     "async": null,
     "name": "测试",
     "identifier": "1668****",
     "depends_on": [
      "1668****"
     ],
     "condition": "${{ completed() }}",
     "resource": null,
     "is_select": false,
     "timeout": "",
     "last_dispatch_id": null,
     "status": "UNSELECTED",
     "message": null,
     "start_time": null,
     "end_time": null,
     "exec_id": null,
     "steps": [
      {
       "name": "测试",
       "task": "official_devcloud_apiTest",
       "business_type": "Test",
       "inputs": [
        {
         "key": "is_build_in",
         "value": "system"
        },
        {
         "key": "module_or_template_id",
         "value": "d7df****"
        },
        {
         "key": "jobId",
         "value": "vd0v****"
        },
        {
         "key": "_OFFICIAL_DEVCLOUD_JOB_NAME_",
         "value": "javawebdemo-apitest-b5557d"
        },
        {
         "key": "hostIp",
         "value": "0.0.0.0"
        }
       ],
       "sequence": 0,
       "official_task_version": null,
       "identifier": null,
       "multi_step_editable": 0,
       "endpoint_ids": null,
       "id": "e878****",
       "last_dispatch_id": null,
       "status": "UNSELECTED",
       "message": null,
       "start_time": null,
       "end_time": null,
       "daily_build_number": null
      }
     ]
    }
   ]
  }
 ],
 "domain_id": "c67b****",
 "project_id": "ad16****",
 "region": "cn-north-4",
 "component_id": "",
 "language": "zh-cn",
 "sources": [
  {
   "type": "code",
   "params": {
    "git_type": "codehub",
    "git_url": "https://codehub.devcloud.cn-north-4.huaweicloud.com/ad16****/javawebdemo.git",
    "ssh_git_url": "git@codehub.devcloud.cn-north-4.huaweicloud.com:ad16****/javawebdemo.git",
    "web_url": "https://devcloud.cn-north-4.huaweicloud.com/codehub/project/ad16****/codehub/268****/home",
    "repo_name": "javawebdemo",
    "human_name": null,
    "default_branch": "master",
    "endpoint_id": null,
    "codehub_id": "268****",
    "alias": null,
    "workspace_id": null,
    "workspace_name": null,
    "build_params": {
     "action": null,
     "only_code_update": null,
     "build_type": "branch",
     "commit_id": "a60e****",
     "event_type": "Manual",
     "merge_id": null,
     "message": "add",
     "source_branch": null,
     "tag": null,
     "target_branch": "master",
     "codehub_id": "268****",
     "source_codehub_id": null,
     "source_codehub_url": null,
     "source_codehub_http_url": null,
     "target_codehub_id": null,
     "target_codehub_url": null,
     "target_codehub_http_url": null
    }
   }
  }
 ],
 "artifacts": [
  {
   "name": "demoapp.jar",
   "packageType": "jar",
   "version": "20241125.1",
   "downloadUrl": "61d0****"
  }
 ],
 "group_id": null,
 "group_name": null,
 "subject_id": "d303****"
}
```

### 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-11-27 | 1.0 | 文档首次发布 |