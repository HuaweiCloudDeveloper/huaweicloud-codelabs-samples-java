### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeArtsPipeline/debug?api=ShowPipelineRunDetail).

2.In this example, you can obtain the pipeline status and obtain the pipeline execution details.

3.Obtaining the pipeline status/Obtaining the pipeline execution details can obtain the current status of the pipeline in real time, including whether the pipeline is running, whether the pipeline is completed, and the like. In addition, this interface can provide detailed information about pipeline execution, such as execution time, execution steps, and so on.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. 
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. 
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A project and pipeline have been created on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-codeartspipeline. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartspipeline</artifactId>
    <version>3.1.123</version>
</dependency>
```

### Code example
``` java
package com.huawei.codeartspipeline;

import com.huaweicloud.sdk.codeartspipeline.v2.CodeArtsPipelineClient;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ShowPipelineRunDetailRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ShowPipelineRunDetailResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.codeartspipeline.v2.region.CodeArtsPipelineRegion;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowPipelineRunDetail {

    private static final Logger logger = LoggerFactory.getLogger(ShowPipelineRunDetail.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String showPipelineRunDetailAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String showPipelineRunDetailSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(showPipelineRunDetailAk)
                .withSk(showPipelineRunDetailSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CodeArtsPipelineRegion.CN_NORTH_4.
        CodeArtsPipelineClient client = CodeArtsPipelineClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeArtsPipelineRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request and set the request parameters Project ID, pipeline ID, and pipeline running instance ID.
        ShowPipelineRunDetailRequest request = new ShowPipelineRunDetailRequest();

        // Project ID. Note: Use the project ID on the home page of the corresponding project in CodeArts. Click the ID of a project link.
        // Example: https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/List
        String projectId = "<YOUR PROJECT ID>";
        request.withProjectId(projectId);
        // Pipeline ID. Pipeline_ID in the link on the pipeline details page
        // https://devcloud.cn-north-4.huaweicloud.com/cicd/project/{{PROJECT_ID}}/pipeline/detail/{{PIPELINE_ID}}/{{PIPELINE_RUN_ID}}?v=1
        String pipelineId = "<YOUR PIPELINE ID>";
        request.withPipelineId(pipelineId);
        // ID of a pipeline running instance. Pipeline_RUN_ID in the link on the pipeline details page
        // https://devcloud.cn-north-4.huaweicloud.com/cicd/project/{{PROJECT_ID}}/pipeline/detail/{{PIPELINE_ID}}/{{PIPELINE_RUN_ID}}?v=1
        String pipelineRunId = "<YOUR PIPELINE RUN ID>";
        request.withPipelineRunId(pipelineRunId);
        try {
            ShowPipelineRunDetailResponse response = client.showPipelineRunDetail(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
```
{
 "id": "d303****",
 "pipeline_id": "4e21****",
 "manifest_version": "3.0",
 "name": "javawebdemo-pipeline",
 "description": "",
 "is_publish": false,
 "executor_id": "f1ac****",
 "executor_name": "Developer",
 "executor_user_name": "demouser",
 "status": "COMPLETED",
 "trigger_type": "Manual",
 "run_number": 2,
 "start_time": 1732498157000,
 "end_time": 1732498306000,
 "current_system_time": 1732671170269,
 "stages": [
  {
   "id": "d92b****",
   "category": null,
   "name": "构建和检查",
   "identifier": "0",
   "run_always": false,
   "parallel": null,
   "is_select": true,
   "sequence": 0,
   "depends_on": [],
   "condition": null,
   "status": "COMPLETED",
   "start_time": 1732498158000,
   "end_time": 1732498306000,
   "pre": [
    {
     "name": null,
     "task": "official_devcloud_autoTrigger",
     "business_type": null,
     "inputs": null,
     "sequence": 0,
     "official_task_version": null,
     "identifier": null,
     "multi_step_editable": 0,
     "endpoint_ids": null,
     "id": "8eaa****",
     "last_dispatch_id": "c2fe****",
     "status": "COMPLETED",
     "message": null,
     "start_time": 1732498158000,
     "end_time": 1732498158000,
     "daily_build_number": null
    }
   ],
   "post": [],
   "jobs": [
    {
     "id": "9d33****",
     "category": null,
     "sequence": 0,
     "async": null,
     "name": "构建",
     "identifier": "1668****",
     "depends_on": [],
     "condition": "${{ completed() }}",
     "resource": "{\"type\":\"system\",\"arch\":\"x86\"}",
     "is_select": true,
     "timeout": "",
     "last_dispatch_id": "b518****",
     "status": "COMPLETED",
     "message": null,
     "start_time": 1732498158000,
     "end_time": 1732498220000,
     "exec_id": "j_fY****",
     "steps": [
      {
       "name": "构建",
       "task": "official_devcloud_cloudBuild",
       "business_type": "Build",
       "inputs": [
        {
         "key": "codeBranch",
         "value": "master"
        },
        {
         "key": "jobId",
         "value": "d06e****"
        },
        {
         "key": "__repository__",
         "value": "javawebdemo"
        },
        {
         "key": "artifactIdentifier",
         "value": ""
        }
       ],
       "sequence": 0,
       "official_task_version": null,
       "identifier": null,
       "multi_step_editable": 0,
       "endpoint_ids": [],
       "id": "5b64****",
       "last_dispatch_id": "b1f7****",
       "status": "COMPLETED",
       "message": null,
       "start_time": 1732498158000,
       "end_time": 1732498220000,
       "daily_build_number": "20241125.1"
      }
     ]
    },
    {
     "id": "10f9****",
     "category": null,
     "sequence": 1,
     "async": null,
     "name": "代码检查",
     "identifier": "1668****",
     "depends_on": [],
     "condition": "${{ completed() }}",
     "resource": null,
     "is_select": true,
     "timeout": "",
     "last_dispatch_id": "c7ca****",
     "status": "COMPLETED",
     "message": null,
     "start_time": 1732498158000,
     "end_time": 1732498306000,
     "exec_id": "j_Jp****",
     "steps": [
      {
       "name": "代码检查",
       "task": "official_devcloud_codeCheck",
       "business_type": "Build",
       "inputs": [
        {
         "key": "module_or_template_id",
         "value": "d7df****"
        },
        {
         "key": "jobId",
         "value": "7c6e****"
        },
        {
         "key": "repository_readonly",
         "value": "javawebdemo"
        },
        {
         "key": "checkMode",
         "value": "full"
        },
        {
         "key": "codeBranch",
         "value": "master"
        }
       ],
       "sequence": 0,
       "official_task_version": null,
       "identifier": null,
       "multi_step_editable": 0,
       "endpoint_ids": null,
       "id": "9eb1****",
       "last_dispatch_id": "9896****",
       "status": "COMPLETED",
       "message": null,
       "start_time": 1732498158000,
       "end_time": 1732498306000,
       "daily_build_number": null
      }
     ]
    }
   ]
  },
  {
   "id": "ef37****",
   "category": null,
   "name": "部署和测试",
   "identifier": null,
   "run_always": null,
   "parallel": null,
   "is_select": false,
   "sequence": 1,
   "depends_on": [
    "0"
   ],
   "condition": null,
   "status": "UNSELECTED",
   "start_time": null,
   "end_time": null,
   "pre": [
    {
     "name": null,
     "task": "official_devcloud_autoTrigger",
     "business_type": null,
     "inputs": null,
     "sequence": 0,
     "official_task_version": null,
     "identifier": null,
     "multi_step_editable": 0,
     "endpoint_ids": null,
     "id": "b89d****",
     "last_dispatch_id": null,
     "status": "UNSELECTED",
     "message": null,
     "start_time": null,
     "end_time": null,
     "daily_build_number": null
    }
   ],
   "post": [],
   "jobs": [
    {
     "id": "4a7a****",
     "category": null,
     "sequence": 0,
     "async": null,
     "name": "部署",
     "identifier": "1668****",
     "depends_on": [],
     "condition": "${{ completed() }}",
     "resource": "{\"type\":\"system\",\"arch\":\"x86\"}",
     "is_select": false,
     "timeout": "",
     "last_dispatch_id": null,
     "status": "UNSELECTED",
     "message": null,
     "start_time": null,
     "end_time": null,
     "exec_id": null,
     "steps": [
      {
       "name": "部署",
       "task": "official_devcloud_deploy",
       "business_type": "Deploy",
       "inputs": [
        {
         "key": "host_group",
         "value": "2315****"
        },
        {
         "key": "package_url",
         "value": "/javawebdemo-build/"
        },
        {
         "key": "service_port",
         "value": "8080"
        },
        {
         "key": "package_name",
         "value": "demoapp"
        },
        {
         "key": "relatedPipelineTask",
         "value": ""
        },
        {
         "key": "SYSTEM_DEVCLOUD_SUBPIPELINE_TRIGGER_ID",
         "value": "e0b4****"
        },
        {
         "key": "_OFFICIAL_DEVCLOUD_JOB_NAME_",
         "value": "javawebdemo-deploy"
        }
       ],
       "sequence": 0,
       "official_task_version": null,
       "identifier": null,
       "multi_step_editable": 0,
       "endpoint_ids": [],
       "id": "e797****",
       "last_dispatch_id": null,
       "status": "UNSELECTED",
       "message": null,
       "start_time": null,
       "end_time": null,
       "daily_build_number": null
      }
     ]
    },
    {
     "id": "5816****",
     "category": null,
     "sequence": 0,
     "async": null,
     "name": "测试",
     "identifier": "1668****",
     "depends_on": [
      "1668****"
     ],
     "condition": "${{ completed() }}",
     "resource": null,
     "is_select": false,
     "timeout": "",
     "last_dispatch_id": null,
     "status": "UNSELECTED",
     "message": null,
     "start_time": null,
     "end_time": null,
     "exec_id": null,
     "steps": [
      {
       "name": "测试",
       "task": "official_devcloud_apiTest",
       "business_type": "Test",
       "inputs": [
        {
         "key": "is_build_in",
         "value": "system"
        },
        {
         "key": "module_or_template_id",
         "value": "d7df****"
        },
        {
         "key": "jobId",
         "value": "vd0v****"
        },
        {
         "key": "_OFFICIAL_DEVCLOUD_JOB_NAME_",
         "value": "javawebdemo-apitest-b5557d"
        },
        {
         "key": "hostIp",
         "value": "0.0.0.0"
        }
       ],
       "sequence": 0,
       "official_task_version": null,
       "identifier": null,
       "multi_step_editable": 0,
       "endpoint_ids": null,
       "id": "e878****",
       "last_dispatch_id": null,
       "status": "UNSELECTED",
       "message": null,
       "start_time": null,
       "end_time": null,
       "daily_build_number": null
      }
     ]
    }
   ]
  }
 ],
 "domain_id": "c67b****",
 "project_id": "ad16****",
 "region": "cn-north-4",
 "component_id": "",
 "language": "zh-cn",
 "sources": [
  {
   "type": "code",
   "params": {
    "git_type": "codehub",
    "git_url": "https://codehub.devcloud.cn-north-4.huaweicloud.com/ad16****/javawebdemo.git",
    "ssh_git_url": "git@codehub.devcloud.cn-north-4.huaweicloud.com:ad16****/javawebdemo.git",
    "web_url": "https://devcloud.cn-north-4.huaweicloud.com/codehub/project/ad16****/codehub/268****/home",
    "repo_name": "javawebdemo",
    "human_name": null,
    "default_branch": "master",
    "endpoint_id": null,
    "codehub_id": "268****",
    "alias": null,
    "workspace_id": null,
    "workspace_name": null,
    "build_params": {
     "action": null,
     "only_code_update": null,
     "build_type": "branch",
     "commit_id": "a60e****",
     "event_type": "Manual",
     "merge_id": null,
     "message": "add",
     "source_branch": null,
     "tag": null,
     "target_branch": "master",
     "codehub_id": "268****",
     "source_codehub_id": null,
     "source_codehub_url": null,
     "source_codehub_http_url": null,
     "target_codehub_id": null,
     "target_codehub_url": null,
     "target_codehub_http_url": null
    }
   }
  }
 ],
 "artifacts": [
  {
   "name": "demoapp.jar",
   "packageType": "jar",
   "version": "20241125.1",
   "downloadUrl": "61d0****"
  }
 ],
 "group_id": null,
 "group_name": null,
 "subject_id": "d303****"
}
```
### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/11/27 | 1.0 | This document is released for the first time.