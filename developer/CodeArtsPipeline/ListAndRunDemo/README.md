## 1. 示例简介
华为云流水线CodeArtsPipeline服务可以帮助客户快速搭建和灵活编排从编译构建-代码检查-测试-部署的机制自动化持续交付能力，帮助研发团队有效减少手工操作和提升应用发布效率。

本示例展示了如何通过java版SDK进行研发作业流的基本操作，包含流水线的查询、触发、停止等日常高频操作。

## 2. 开发前准备
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 已在CodeArts平台创建流水线

## 3. 安装SDK
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-codeartspipeline”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartspipeline</artifactId>
    <version>3.1.59</version>
</dependency>
```

## 4. 示例代码
```java
public static void main(String[] args) {
        // 初始化必要参数及客户端
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 注意，如果查询当前租户特定项目下或指定流水线id范围内的结果，请使用ListPipelineQuery参数中的project_ids等参数
        String projectId = UUID.randomUUID().toString();
        // 所需请求的region信息
        Region region = CodeArtsPipelineRegion.valueOf("cn-north-4");

        // 配置认证信息
        BasicCredentials auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId(projectId);

        // 创建服务客户端
        CodeArtsPipelineClient codeArtsPipelineClient = CodeArtsPipelineClient.newBuilder()
                .withCredential(auth)
                .withRegion(region)
                .build();

        // 业务逻辑 1.查询配置的pipeline列表 2.根据返回结果运行pipeline 3.必要时进行停止流水线操作
        try {
            logger.info("start to list user pipeline list");
            // 1. list当前租户下的pipeline
            ListPipelinesRequest listPipelinesRequest = new ListPipelinesRequest();
            ListPipelineQuery body = new ListPipelineQuery();
            // 添加query参数
            listPipelinesRequest.setBody(body);
            ListPipelinesResponse listPipelinesResponse = codeArtsPipelineClient.listPipelines(listPipelinesRequest);
            // 获取其中pipelineId 和 projectId，如下以
            List<ListPipelinesPagePipelines> pipelines = listPipelinesResponse.getPipelines();
            if (pipelines.isEmpty()) {
                logger.info("current user pipeline list is empty");
                return;
            }
            String triggerPipelineId = pipelines.get(0).getPipelineId();
            String triggerProjectId = pipelines.get(0).getProjectId();

            logger.info("start to run pipeline with projectId:{}, pipelineId:{}", triggerProjectId, triggerPipelineId);
            // 2.选择一条pipeline运行
            RunPipelineRequest runPipelineRequest = new RunPipelineRequest();
            runPipelineRequest.setPipelineId(triggerPipelineId);
            runPipelineRequest.setProjectId(triggerProjectId);
            RunPipelineResponse runPipelineResponse = codeArtsPipelineClient.runPipeline(runPipelineRequest);
            // 获取返回结果的pipelineRunId，为本次流水线的运行时uuid
            String pipelineRunId = runPipelineResponse.getPipelineRunId();
            logger.info("pipeline run with uuid:{}", pipelineRunId);

            // 3.如果需要停止流水线，使用上述三个值的组合进行确定，即projectId + pipelineId + pipelineRunId
            logger.info("start to stop user pipeline with projectId:{}, pipelineId:{}, pipelineRunId:{}", triggerProjectId, triggerPipelineId, pipelineRunId);
            StopPipelineRunRequest stopPipelineRunRequest = new StopPipelineRunRequest();
            stopPipelineRunRequest.setPipelineId(triggerPipelineId);
            stopPipelineRunRequest.setPipelineRunId(pipelineRunId);
            stopPipelineRunRequest.setProjectId(triggerProjectId);
            StopPipelineRunResponse stopPipelineRunResponse = codeArtsPipelineClient.stopPipelineRun(stopPipelineRunRequest);
            logger.info("stop pipeline with status:{}", stopPipelineRunResponse.getSuccess());

        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }
```

相关参数说明如下所示：
- ak：华为云账号Access Key。
- sk：华为云账号Secret Access Key。
- region：服务所在区域。

## 5. 参考
更多api及参数详细说明信息请参考如下链接[流水线 CodeArtsPipeline Api详细说明文档](https://console.huaweicloud.com/apiexplorer/#/openapi/CodeArtsPipeline/sdk?api=ListPipelineRuns)
中的"流水线管理--新"目录下内容。

## 6. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-09-16 | 1.0 | 文档首次发布 |