## 1. Example Introduction

HUAWEI CLOUD pipeline CodeArtsPipeline helps customers quickly set up and flexibly orchestrate automatic continuous
delivery capabilities from compilation, build, code check, test, and deployment, helping R&D teams reduce manual
operations and improve application release efficiency.
This example shows how to use the Java SDK to perform basic operations on R&D workflows, including routine frequent
operations such as pipeline query, triggering, and stopping.

## 2. Preparations Before Development

1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)
HUAWEI
CLOUD，and
completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. You can create and
view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. For details,
see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A pipeline has been created on the CodeArts platform.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After
the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-codeartspipeline. For details about the SDK version
number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).

```xml

<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartspipeline</artifactId>
    <version>3.1.67</version>
</dependency>
```

## 4. Sample Code

```java
package com.huawei.codeartspipeline;

import com.huaweicloud.sdk.codeartspipeline.v2.CodeArtsPipelineClient;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelineQuery;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelinesPagePipelines;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelinesRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelinesResponse;
import com.huaweicloud.sdk.codeartspipeline.v2.model.RunPipelineDTO;
import com.huaweicloud.sdk.codeartspipeline.v2.model.RunPipelineRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.RunPipelineResponse;
import com.huaweicloud.sdk.codeartspipeline.v2.model.StopPipelineRunRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.StopPipelineRunResponse;
import com.huaweicloud.sdk.codeartspipeline.v2.region.CodeArtsPipelineRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.region.Region;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class PipelineDemo {

    private static final Logger logger = LoggerFactory.getLogger(PipelineDemo.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Note: For the following project IDs, choose HUAWEI CLOUD console > IAM My Credential > the project ID of the corresponding region.
        String projectId = "<Corresponding region Iam Project Id>";
        // Requested region information
        Region region = CodeArtsPipelineRegion.valueOf("cn-north-4");

        // Configuring Authentication Information
        BasicCredentials auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);

        // Creating a Service Client
        CodeArtsPipelineClient codeArtsPipelineClient = CodeArtsPipelineClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        // Service logic: 1. Query the configured pipeline. 2. Run the pipeline based on the returned result. 3. Stop the pipeline if necessary.
        try {
            logger.info("start to list user pipeline list");
            // 1. Lists pipelines of the current tenant.
            ListPipelinesRequest listPipelinesRequest = new ListPipelinesRequest();
            ListPipelineQuery body = new ListPipelineQuery();
            // Enter the required pipeline name in the body.
            String pipelineName = "<Your pipeline name>";
            body.setName(pipelineName);
            listPipelinesRequest.setBody(body);
            String codeArtsProjectId = "<ProjectId your pipeline belongs to>";
            listPipelinesRequest.setProjectId(codeArtsProjectId);
            ListPipelinesResponse listPipelinesResponse = codeArtsPipelineClient.listPipelines(listPipelinesRequest);
            logger.info("pipeline list with projectId:{}, name:{}, result:{}", codeArtsProjectId, pipelineName,
                listPipelinesResponse);
            List<ListPipelinesPagePipelines> pipelines = listPipelinesResponse.getPipelines();
            if (pipelines.isEmpty()) {
                logger.info("current user pipeline list is empty");
                return;
            }
            String triggerPipelineId = pipelines.get(0).getPipelineId();
            String triggerProjectId = pipelines.get(0).getProjectId();

            logger.info("start to run pipeline with projectId:{}, pipelineId:{}", triggerProjectId, triggerPipelineId);
            // 2. Select a pipeline and select a task to run.
            RunPipelineRequest runPipelineRequest = new RunPipelineRequest();
            runPipelineRequest.setPipelineId(triggerPipelineId);
            runPipelineRequest.setProjectId(triggerProjectId);
            RunPipelineDTO runPipelineDTO = new RunPipelineDTO();
            runPipelineDTO.setChooseJobs(Arrays.asList("<JobIds you want to run with current pipeline>"));
            runPipelineRequest.setBody(runPipelineDTO);
            RunPipelineResponse runPipelineResponse = codeArtsPipelineClient.runPipeline(runPipelineRequest);
            // Obtains the pipelineRunId of the returned result, which is the UUID of the current pipeline running.
            String pipelineRunId = runPipelineResponse.getPipelineRunId();
            logger.info("pipeline run with uuid:{}", pipelineRunId);

            // 3. If the pipeline needs to be stopped, use the combination of the preceding three values, that is, projectId + pipelineId + pipelineRunId.
            logger.info("start to stop user pipeline with projectId:{}, pipelineId:{}, pipelineRunId:{}",
                triggerProjectId, triggerPipelineId, pipelineRunId);
            StopPipelineRunRequest stopPipelineRunRequest = new StopPipelineRunRequest();
            stopPipelineRunRequest.setPipelineId(triggerPipelineId);
            stopPipelineRunRequest.setPipelineRunId(pipelineRunId);
            stopPipelineRunRequest.setProjectId(triggerProjectId);
            StopPipelineRunResponse stopPipelineRunResponse = codeArtsPipelineClient.stopPipelineRun(
                stopPipelineRunRequest);
            logger.info("stop pipeline with status:{}", stopPipelineRunResponse.getSuccess());

        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }
}
```

The related parameters are described as follows:

- ak: indicates the access key of the HUAWEI CLOUD account.
- sk: specifies the secret access key of the HUAWEI CLOUD account.
- region: indicates the region where the service is located.

## 5. References

You can run and debug the interface directly in
the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeArtsPipeline/doc?api=ListPipelineRuns).

## 6. Change History

| Release Date | Document Version |             Revision Description              |
|:------------:|:----------------:|:---------------------------------------------:|
|  2024-10-28  |       1.0        | This document is released for the first time. |