package com.huawei.codeartspipeline;

import com.huaweicloud.sdk.codeartspipeline.v2.CodeArtsPipelineClient;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelineQuery;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelinesPagePipelines;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelinesRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.ListPipelinesResponse;
import com.huaweicloud.sdk.codeartspipeline.v2.model.RunPipelineDTO;
import com.huaweicloud.sdk.codeartspipeline.v2.model.RunPipelineRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.RunPipelineResponse;
import com.huaweicloud.sdk.codeartspipeline.v2.model.StopPipelineRunRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.StopPipelineRunResponse;
import com.huaweicloud.sdk.codeartspipeline.v2.region.CodeArtsPipelineRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.region.Region;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class PipelineDemo {

    private static final Logger logger = LoggerFactory.getLogger(PipelineDemo.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Writing the AK and SK used for authentication to the code has great security risks. It is recommended that the AK and SK be stored in ciphertext in configuration files or environment variables and decrypted during use to ensure security.
        // In this example, the AK and SK are stored in environment variables for authentication. Before running this example, set the environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK in the local environment.
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // Note: For the following project IDs, choose HUAWEI CLOUD console > IAM My Credential > the project ID of the corresponding region.
        String projectId = "<Corresponding region Iam Project Id>";
        // Requested region information
        Region region = CodeArtsPipelineRegion.valueOf("cn-north-4");

        // Configuring Authentication Information
        BasicCredentials auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);

        // Creating a Service Client
        CodeArtsPipelineClient codeArtsPipelineClient = CodeArtsPipelineClient.newBuilder()
            .withCredential(auth)
            .withRegion(region)
            .build();

        // Service logic: 1. Query the configured pipeline. 2. Run the pipeline based on the returned result. 3. Stop the pipeline if necessary.
        try {
            logger.info("start to list user pipeline list");
            // 1. Lists pipelines of the current tenant.
            ListPipelinesRequest listPipelinesRequest = new ListPipelinesRequest();
            ListPipelineQuery body = new ListPipelineQuery();
            // Enter the required pipeline name in the body.
            String pipelineName = "<Your pipeline name>";
            body.setName(pipelineName);
            listPipelinesRequest.setBody(body);
            String codeArtsProjectId = "<ProjectId your pipeline belongs to>";
            listPipelinesRequest.setProjectId(codeArtsProjectId);
            ListPipelinesResponse listPipelinesResponse = codeArtsPipelineClient.listPipelines(listPipelinesRequest);
            logger.info("pipeline list with projectId:{}, name:{}, result:{}", codeArtsProjectId, pipelineName,
                listPipelinesResponse);
            List<ListPipelinesPagePipelines> pipelines = listPipelinesResponse.getPipelines();
            if (pipelines.isEmpty()) {
                logger.info("current user pipeline list is empty");
                return;
            }
            String triggerPipelineId = pipelines.get(0).getPipelineId();
            String triggerProjectId = pipelines.get(0).getProjectId();

            logger.info("start to run pipeline with projectId:{}, pipelineId:{}", triggerProjectId, triggerPipelineId);
            // 2. Select a pipeline and select a task to run.
            RunPipelineRequest runPipelineRequest = new RunPipelineRequest();
            runPipelineRequest.setPipelineId(triggerPipelineId);
            runPipelineRequest.setProjectId(triggerProjectId);
            RunPipelineDTO runPipelineDTO = new RunPipelineDTO();
            runPipelineDTO.setChooseJobs(Arrays.asList("<JobIds you want to run with current pipeline>"));
            runPipelineRequest.setBody(runPipelineDTO);
            RunPipelineResponse runPipelineResponse = codeArtsPipelineClient.runPipeline(runPipelineRequest);
            // Obtains the pipelineRunId of the returned result, which is the UUID of the current pipeline running.
            String pipelineRunId = runPipelineResponse.getPipelineRunId();
            logger.info("pipeline run with uuid:{}", pipelineRunId);

            // 3. If the pipeline needs to be stopped, use the combination of the preceding three values, that is, projectId + pipelineId + pipelineRunId.
            logger.info("start to stop user pipeline with projectId:{}, pipelineId:{}, pipelineRunId:{}",
                triggerProjectId, triggerPipelineId, pipelineRunId);
            StopPipelineRunRequest stopPipelineRunRequest = new StopPipelineRunRequest();
            stopPipelineRunRequest.setPipelineId(triggerPipelineId);
            stopPipelineRunRequest.setPipelineRunId(pipelineRunId);
            stopPipelineRunRequest.setProjectId(triggerProjectId);
            StopPipelineRunResponse stopPipelineRunResponse = codeArtsPipelineClient.stopPipelineRun(
                stopPipelineRunRequest);
            logger.info("stop pipeline with status:{}", stopPipelineRunResponse.getSuccess());

        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
            logger.error(e.toString());
        }
    }
}