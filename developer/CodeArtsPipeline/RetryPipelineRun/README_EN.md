### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/CodeArtsPipeline/debug?api=RetryPipelineRun).

2.In this example, you can retry running the pipeline.

3.Pipelines may face various problems, such as network outages, server failures, task execution timeouts, and so on. When an error or failure occurs in a task in the pipeline, the re-run pipeline is called to ensure the smooth running of the pipeline.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. 
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. 
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.A project and pipeline have been created on the CodeArts platform, and a pipeline that fails to be executed has been created.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-codeartspipeline. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-codeartspipeline</artifactId>
    <version>3.1.123</version>
</dependency>
```

### Code example
``` java
package com.huawei.codeartspipeline;

import com.huaweicloud.sdk.codeartspipeline.v2.CodeArtsPipelineClient;
import com.huaweicloud.sdk.codeartspipeline.v2.model.RetryPipelineRunRequest;
import com.huaweicloud.sdk.codeartspipeline.v2.model.RetryPipelineRunResponse;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.codeartspipeline.v2.region.CodeArtsPipelineRegion;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RetryPipelineRun {

    private static final Logger logger = LoggerFactory.getLogger(RetryPipelineRun.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String createProjectAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String createProjectSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ICredential auth = new BasicCredentials()
                .withAk(createProjectAk)
                .withSk(createProjectSk);
        HttpConfig httpConfig = HttpConfig.getDefaultHttpConfig();

        // Create a service client and set the corresponding region to CodeArtsPipelineRegion.CN_NORTH_4.
        CodeArtsPipelineClient client = CodeArtsPipelineClient.newBuilder()
                .withCredential(auth)
                .withRegion(CodeArtsPipelineRegion.CN_NORTH_4)
                .withHttpConfig(httpConfig)
                .build();
        // Construct a request and set the request parameters Project ID, pipeline ID, and pipeline running instance ID.
        RetryPipelineRunRequest request = new RetryPipelineRunRequest();
        // Project ID. Note: Use the project ID on the home page of the corresponding project in CodeArts. Click the ID of a project link.
        // Example: https://devcloud.cn-north-4.huaweicloud.com/projectman/scrum/{projectId}/workitem/List
        String projectId = "<YOUR PROJECT ID>";
        request.withProjectId(projectId);
        // Pipeline ID. Pipeline_ID in the link on the pipeline details page
        // https://devcloud.cn-north-4.huaweicloud.com/cicd/project/{{PROJECT_ID}}/pipeline/detail/{{PIPELINE_ID}}/{{PIPELINE_RUN_ID}}?v=1
        String pipelineId = "<YOUR PIPELINE ID>";
        request.withPipelineId(pipelineId);
        // ID of a pipeline running instance. Pipeline_RUN_ID in the link on the pipeline details page
        // https://devcloud.cn-north-4.huaweicloud.com/cicd/project/{{PROJECT_ID}}/pipeline/detail/{{PIPELINE_ID}}/{{PIPELINE_RUN_ID}}?v=1
        String pipelineRunId = "<YOUR PIPELINE RUN ID>";
        request.withPipelineRunId(pipelineRunId);
        try {
            RetryPipelineRunResponse response = client.retryPipelineRun(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
```
{
 "success": true
}
```
### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/11/26 | 1.0 | This document is released for the first time.