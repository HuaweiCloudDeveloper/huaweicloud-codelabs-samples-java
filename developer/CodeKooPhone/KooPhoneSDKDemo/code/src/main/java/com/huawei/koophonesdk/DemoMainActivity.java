/**
 * 版权所有 (c) 华为技术有限公司 2022-2023
 * 功能说明：koophone 云手机演示demo
 */

package com.huawei.koophonesdk;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cloudapp.client.api.CloudAppEnv;
import com.example.demo.api.KPDriverCallback;
import com.example.demo.api.KPDriverInstance;

import appsdk.components.net.bean.response.InstancePrepareRsp;

/**
 * demo app主页面
 *
 * @since 2023-12-05
 */
public class DemoMainActivity extends AppCompatActivity implements KPDriverCallback {

    private KPDriverInstance instance; // kp instance

    private static String loginUrl; // 登录h5 url，联系koophone支撑人员获取
    private WebView webView; // hw登录页
    private String ticket; // 串流凭证
    private CookieManager cookieManager;
    private Button button; // demo 控件
    private EditText pkg; // demo 控件
    private String pkgName; // 打开指定包名应用

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // 生命周期方法
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // 初始化demo控件
        initViews();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull String name, @NonNull Context context, @NonNull AttributeSet attrs) {
        return super.onCreateView(name, context, attrs);
    }

    private void initViews() {
        // demo控件件模拟登陆
        webView = findViewById(R.id.kp_webView);
        button = findViewById(R.id.button);
        pkg = findViewById(R.id.pkg);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 初始化sdk实例
                initKPInstance();
            }
        });

        // webJS控件设置
        WebSettings webSettings = webView.getSettings();
        // webview支持多窗口
        webSettings.setSupportMultipleWindows(true);
        // webview设置支持js脚本
        webSettings.setJavaScriptEnabled(true);
        // webview设置dom缓存
        webSettings.setDomStorageEnabled(true);
        // 禁止webview滑动
        webView.setScrollContainer(false);
        // 禁止webview垂直滑动
        webView.setVerticalScrollBarEnabled(false);
        // 禁止webview水平滑动
        webView.setHorizontalScrollBarEnabled(false);

        cookieManager = CookieManager.getInstance();
        // webview不允许跨域访问cookies
        cookieManager.setAcceptThirdPartyCookies(webView, true);
        // webview加载登录链接 联系koophone支撑人员获取url
        webView.loadUrl(loginUrl);
        // webview设置代理
        webView.setWebViewClient(new KpWebViewClient());
    }

    private void initKPInstance() {
        // 云手机sdk实例
        instance.clientPrepare(ticket, this, DemoApplication.getContext(), CloudAppEnv.TEST);
    }

    // 销毁实例
    private void destroy() {
        // stop方法会销毁实例
        instance.stopMediaStream();
    }

    // 查询云手机实例回调 返回查询状态和云机数量
    @Override
    public void onCloudPhoneQuery(boolean success, int num) {
        if (success) {
            // 获取第一台云机实例截图
            instance.getCloudPhoneScreen(0);
            // 初始化第一台实例串流
            instance.initMediaStream(0);
        } else {
            // 查询失败 没有云机实例
            destroy();
        }
    }

    // 云机状态查询
    @Override
    public void onCloudPhoneStatus(InstancePrepareRsp rsp) {
        // 获取第一台云机状态
        InstancePrepareRsp.DevicePrepareInfo info = rsp.getData().getDevicePrepareInfos().get(0);
        int status = info.getStatus();
        if (status == 0) {
            // 状态0 云机正常
            if (this.pkgName.equals("")) {
                // 是否打开指定应用
                this.pkgName = null;
            }
            // 云机状态正常开启回调，pkgName可直接打开云机指定应用
            instance.startMediaStream(info.getInstanceId(), this.pkgName);
        } else {
            // 查询失败 没有云机实例
            destroy();
        }
    }

    // sdk内部开启串流回调
    @Override
    public void onStreamStarted(Bundle bundle) {
        Intent intent = new Intent(this, DemoCloudPhoneActivity.class);
        // sdk开启串流 启动播放器页面
        startActivity(intent);
    }

    // sdk串流失败回调
    @Override
    public void onStreamFailure(String message, int type, String status) {
        // 销毁实例
        destroy();
    }

    // 解析url
    private void analysisParam(String url) {
        Uri uri = Uri.parse(url);
        String ticket = uri.getQueryParameter("code");
        this.ticket = ticket;
        webView.setVisibility(View.GONE);
        button.setVisibility(View.VISIBLE);
        pkg.setVisibility(View.VISIBLE);
    }

    private void getCookies() {
        CookieManager.getInstance().flush();
    }

    // webview代理方法
    private class KpWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            String currentUrl = request.getUrl().toString();
            if (currentUrl.contains("https://koophone.huaweiapaas.com/login") && !currentUrl.contains("orgid.macroverse.huaweicloud.com/oauth2/authorize")) {
                analysisParam(currentUrl);
                getCookies();
                return true;
            } else {
                return false;
            }
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
        }

        @Override
        public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            super.onReceivedHttpError(view, request, errorResponse);
        }
    }
}