/**
 * 版权所有 (c) 华为技术有限公司 2022-2023
 * 功能说明：koophone 云手机演示demo
 */

package com.huawei.koophonesdk;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import androidx.annotation.Nullable;

import com.example.demo.api.KPDriverInstance;
import com.example.demo.api.KPPlayerCallback;
import com.nbc.acsdk.widget.PlayerFragment;
import appsdk.components.Utils.LogDebugUtils;

import static android.view.KeyEvent.KEYCODE_BACK;
import static com.cloudphone.client.api.CloudPhoneConst.PROFILE_HIGH;
import static com.cloudphone.client.api.CloudPhoneConst.PROFILE_NORMAL;
import static com.cloudphone.client.api.CloudPhoneConst.PROFILE_SPEED;
import static com.cloudphone.client.api.CloudPhoneConst.CLOUD_PHONE_RET_KICKED;
import static com.cloudphone.client.api.CloudPhoneConst.CLOUD_PHONE_RET_INVALID_TOKEN;
import static com.cloudphone.client.api.CloudPhoneConst.CLOUD_PHONE_RET_EXPIRED;
import static com.cloudphone.client.api.CloudPhoneConst.CLOUD_PHONE_RET_ADMIN_EXIT;
import static com.cloudphone.client.api.CloudPhoneConst.CLOUD_PHONE_RET_STOP_STREAMING;
import static com.cloudphone.client.api.CloudPhoneConst.CLOUD_PHONE_RET_USER_EXIT;


/**
 * demo app 播放器页面
 *
 * @since 2023-12-21
 */
public class DemoCloudPhoneActivity extends Activity implements KPPlayerCallback {
    private static final String TAG = "CloudPhoneActivity";

    private PlayerFragment mPlayerFragment; // 播放组件 textureview实现

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        // activity生命周期方法 初始化播放器
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cps_activity_cloud_phone);
        // 全屏显示
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            lp.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        LogDebugUtils.i(TAG, "=================进入云机=================");
        getWindow().setAttributes(lp);

        // 初始化播放器
        initPlayer();

        // demo控件 模拟手机返回操作
        Button back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // demo按钮 模拟手机返回操作
                KPDriverInstance instance = KPDriverInstance.getInstance();
                instance.sendKeyEvent(KEYCODE_BACK);
            }
        });

        // demo控件 模拟手机设置清晰度
        Button profile = findViewById(R.id.profile);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KPDriverInstance instance = KPDriverInstance.getInstance();
                // 设置画质 标清
                instance.setVideoQuality(PROFILE_SPEED);
            }
        });

        // demo控件 模拟手机设置清晰度
        Button profile1 = findViewById(R.id.profile1);
        profile1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KPDriverInstance instance = KPDriverInstance.getInstance();
                // 设置画质 超清
                instance.setVideoQuality(PROFILE_HIGH);
            }
        });

        // demo控件 模拟手机设置清晰度
        Button profile2 = findViewById(R.id.profile2);
        profile2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KPDriverInstance instance = KPDriverInstance.getInstance();
                // 设置画质 高清
                instance.setVideoQuality(PROFILE_NORMAL);
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle extras = intent.getExtras();
        if (extras == null) {
            return;
        }
        LogDebugUtils.e(TAG, "=====onNewIntent启动云机====");
        // 初始化播放器
        initPlayer();
    }

    // 初始化播放器
    private void initPlayer() {
        mPlayerFragment = (PlayerFragment) getFragmentManager().findFragmentById(R.id.fragment_player);
        // 获取单一实例
        KPDriverInstance instance = KPDriverInstance.getInstance();
        // 设置回调
        instance.setPlayerCallBack(this);
        // 启动播放器前先停止串流
        instance.stopMediaStream();
        // 启动播放器
        instance.showPlayer(mPlayerFragment, getFragmentManager());
    }

    // 打印外部传入bundle数据
    public static void showBundleData(Bundle bundle) {
        if (bundle == null) {
            return;
        }
        String string = "";
        for (String key : bundle.keySet()) {
            string += key +  bundle.get(key);
        }
        LogDebugUtils.i(TAG, "bundleData" + string);
    }

    // 实例销毁流程
    @Override
    protected void onDestroy() {
        super.onDestroy();
        KPDriverInstance instance = KPDriverInstance.getInstance();
        // 停止串流
        instance.stopMediaStream();
        // 摧毁实例
        instance.destroyMediaStream();
    }

    // 播放器成功回调
    @Override
    public void onSuccess() {
        LogDebugUtils.i(TAG, "开始启动");
    }

    // 串流终止回调
    @Override
    public void onTerminated() {
        LogDebugUtils.i(TAG, "停止串流");
    }

    // 串流失败回调
    @Override
    public void onFailure(int code, String message) {
        LogDebugUtils.i(TAG, "串流失败" + code);
        switch (code) {
            case CloudPhoneConst.CLOUD_PHONE_RET_KICKED:
                // 服务器踢云机下线
                break;
            case CloudPhoneConst.CLOUD_PHONE_RET_INVALID_TOKEN:
                // 用户重复登录
                break;
            case CloudPhoneConst.CLOUD_PHONE_RET_EXPIRED:
                // token失效
                break;
            case CLOUD_PHONE_RET_ADMIN_EXIT:
                // 时间到期
                break;
            case CloudPhoneConst.CLOUD_PHONE_RET_STOP_STREAMING:
                // 时间到期
                break;
            case CloudPhoneConst.CLOUD_PHONE_RET_USER_EXIT:
                // 后端主动断开连接
                break;
            default:
                break;
        }
    }

    // 播放器屏幕旋转回调
    @Override
    public void onOrientationChange(int orientation) {
        if (orientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
            // 设置横屏
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            // 设置竖屏
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    // 被推流方改变
    @Override
    public void onSlotsInfo(String value) {
        LogDebugUtils.i(TAG, "连接用户改变");
    }

    // 房间信息改变
    @Override
    public void onRoomInfoUpdate(String s) {
        LogDebugUtils.i(TAG, "房间信息改变" + s);
    }

    // 主动权限请求
    @Override
    public void onPermissionRequest(String s) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Context context = getApplication();
                if (context == null) {
                    LogDebugUtils.i(TAG, "无法获取当前Activity 无法显示弹框");
                    return;
                }
                LogDebugUtils.i(TAG, "onPermissionRequest: 确认授权 ：per = " + finalPerStr + "  token = " + info.from.token);
                // demo权限申请弹窗
                CommonDialog commonDialog = new CommonDialog(CloudPhoneActivity.this);
                commonDialog.setMessage("云应用请求使用本设备硬件数据，是否同意?");
                commonDialog.setListener(new CommonDialog.OnDialogClickListener() {
                    @Override
                    public void cancelClick(CommonDialog dialog) {
                        // 取消操作
                    }

                    @Override
                    public void confirmClick(CommonDialog dialog) {
                        // 确认操作
                        CloudPhoneClient.rightsAudit(finalPerStr, info.from.token, 1, "AUDITOR");
                        LogDebugUtils.i(TAG, "onPermissionRequest: 确认授权 ：per = " + finalPerStr + "  token = " + info.from.token);
                    }
                });
                commonDialog.show();
            }
        }, 500);
    }

    // 权限申请结果回调
    @Override
    public void onPermissionResult(String s) {
        LogDebugUtils.i(TAG, "权限申请结果" + s);
    }

    // 画质自动调节回调
    @Override
    public void onProfileReceived(String profile) {
        KPDriverInstance instance = KPDriverInstance.getInstance();
        // 设置画质
        instance.setVideoQuality(profile);
    }

    // 画质切换成功回调
    @Override
    public void onProfileChanged(int i) {
        if (i == PROFILE_HIGH) {
            // 弹出toast
            CommonUtil.showToast(this, "已切换至720p清晰度");
        } else if (i == PROFILE_NORMAL) {
            CommonUtil.showToast(this, "已切换至540p清晰度");
        } else if (i == PROFILE_SPEED) {
            CommonUtil.showToast(this, "已切换至360p清晰度");
        } else if (i == PROFILE_AUTO) {
            CommonUtil.showToast(this, "已切换至自动清晰度");
        }
    }

    // 确认退出回调
    @Override
    public void onExitConfirm(int paramInt) {
        LogDebugUtils.i(TAG, "确认退出");
    }

    // 用户退出回调
    @Override
    public void onUserExit() {
        LogDebugUtils.i(TAG, "用户退出");
        // 停止串流
        instance.stopMediaStream();
        // 摧毁实例
        instance.destroyMediaStream();
    }

    // 权限校验回调
    @Override
    public void onPermissionDenied(String[] strings) {
        for (int i = 0; i < strings.length; i++) {
            boolean contain = strings[i].contains("READ_EXTERNAL_STORAGE") || strings[i].contains("WRITE_EXTERNAL_STORAGE");
            if (contain) {
                dismissLoading();
                dismissDialog();
                PlayerWidgetManager.getInstance().sendMessageToUi(MSG_EXIT, null);
                mCommonDialog = null;
                finish();
                CommonUtil.showToast(this,"缺少该权限云机无法正常运行");
                break;
            }
        }
    }

    // 云机操控回调
    @Override
    public void onActionMessage(String s) {
        LogDebugUtils.i(TAG, "云机触控" + s);
    }

    // 被动授权
    @Override
    public void onApplyPhonePermission(String s) {
        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray permission = jsonObject.getJSONArray("permission");

            String[] p = new String[permission.length()];
            for (int i = 0; i < permission.length(); i++) {
                p[i] = (String) permission.get(i);
                Log.i(TAG, "进程===>" + p[i]);
            }
            ActivityCompat.requestPermissions(this, p, CODE_PERMISSION_REQUEST_CODE);
            mPermissionTrackId = jsonObject.getInt("trackId");
        } catch (JSONException e) {
            LogDebugUtils.i(TAG, "授权失败");
        }
    }

    // 接收来自云机端的自定义字串信息和接受的信息的activity名称
    @Override
    public void onExtMessageReceived(Activity activity, String msgStr) {
        LogDebugUtils.i(TAG, "云机自定义消息" + msgStr);
    }

    // 用户确认系统授权
    @Override
    public void onPermissionGranted(String[] permissions) {
        LogDebugUtils.i(TAG, "用户确认系统授权");
    }
}

