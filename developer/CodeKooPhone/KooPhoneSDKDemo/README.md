## 1. 示例简介
本文档描述云手机服务KooPhone开放SDK提供的接入方式和接口说明，用于下游进行二次开发。

## 2. 开发前准备
使用前准备：用户需注册华为帐号，并在在华为云官网搜索云手机服务koophone订购服务。
华为登录页集成：用户App内需集成华为登录页，登录页为H5页面。
登录页url需要在华为云官网搜索云手机服务koophone订购服务后支撑人员提供。

## 3. android项目集成aar
aar包名为KooPhoneSdk-xxxx，下载地址为"https://download.koophone.myhuaweicloud.com/sdk/KooPhoneSdk-23.11.aar"。
1. aar包建议放在App根目录libs下。
2. 根目录build.gradle，在android{}作用域下添加repositories {flatDir {dirs 'libs'}}。
3. 根目录build.gradle，添加目录树和API路径：
      −	dependencies{ implementation fileTree(dir: 'libs', include: ['*.jar']) }；
      −	API(name:'aar包名为KooPhoneSdk-xxxx',ext:'aar')。
4. 使用前准备：用户需注册华为帐号，并在华为云官网搜索云手机服务koophone订购服务。
5. 华为登录页集成：用户App内需集成华为登录页，登录页为H5页面。
6. 云机准备接口：用户初始化SDK引擎前需调用clientPrepare接口，入参为登录页登录完成返回ticket字段，具体逻辑查看demo。
7. 引擎初始化：云机准备接口调用成功，返回回调，此时调用getInstance接口，初始化引擎。
8. 串流过程：aar包内部提供默认渲染组件PlayerFragment，内部使用TextureView进行画面渲染，集成音视频编解码功能。调用startMediaStream接口进行串流，启动参数bundle具体传递查看demo。
9. 销毁过程：aar包内部调用停止串流接口，内部会自动销毁实例，销毁成功调用回调。

## 4. 示例代码
```java
public class DemoMainActivity extends AppCompatActivity implements KPDriverCallback {

      private KPDriverInstance instance; // kp instance

      private static String loginUrl; // 登录h5 url，联系koophone支撑人员获取
      private WebView webView; // hw登录页
      private String ticket; // 串流凭证
      private CookieManager cookieManager;
      private Button button; // demo 控件
      private EditText pkg; // demo 控件
      private String pkgName; // 打开指定包名应用

      @Override
      protected void onCreate(Bundle savedInstanceState) {
            // 生命周期方法
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            // 初始化demo控件
            initViews();
      }

      @Nullable
      @Override
      public View onCreateView(@NonNull String name, @NonNull Context context, @NonNull AttributeSet attrs) {
            return super.onCreateView(name, context, attrs);
      }

      private void initViews() {
            // demo控件件模拟登陆
            webView = findViewById(R.id.kp_webView);
            button = findViewById(R.id.button);
            pkg = findViewById(R.id.pkg);

            button.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        // 初始化sdk实例
                        initKPInstance();
                  }
            });

            // webJS控件设置
            WebSettings webSettings = webView.getSettings();
            // webview支持多窗口
            webSettings.setSupportMultipleWindows(true);
            // webview设置支持js脚本
            webSettings.setJavaScriptEnabled(true);
            // webview设置dom缓存
            webSettings.setDomStorageEnabled(true);
            // 禁止webview滑动
            webView.setScrollContainer(false);
            // 禁止webview垂直滑动
            webView.setVerticalScrollBarEnabled(false);
            // 禁止webview水平滑动
            webView.setHorizontalScrollBarEnabled(false);

            cookieManager = CookieManager.getInstance();
            // webview不允许跨域访问cookies
            cookieManager.setAcceptThirdPartyCookies(webView, true);
            // webview加载登录链接 联系koophone支撑人员获取url
            webView.loadUrl(loginUrl);
            // webview设置代理
            webView.setWebViewClient(new KpWebViewClient());
      }

      private void initKPInstance() {
            // 云手机sdk实例
            instance.clientPrepare(ticket, this, DemoApplication.getContext(), CloudAppEnv.TEST);
      }

      // 销毁实例
      private void destroy() {
            // stop方法会销毁实例
            instance.stopMediaStream();
      }

      // 查询云手机实例回调 返回查询状态和云机数量
      @Override
      public void onCloudPhoneQuery(boolean success, int num) {
            if (success) {
                  // 获取第一台云机实例截图
                  instance.getCloudPhoneScreen(0);
                  // 初始化第一台实例串流
                  instance.initMediaStream(0);
            } else {
                  // 查询失败 没有云机实例
                  destroy();
            }
      }

      // 云机状态查询
      @Override
      public void onCloudPhoneStatus(InstancePrepareRsp rsp) {
            // 获取第一台云机状态
            InstancePrepareRsp.DevicePrepareInfo info = rsp.getData().getDevicePrepareInfos().get(0);
            int status = info.getStatus();
            if (status == 0) {
                  // 状态0 云机正常
                  if (this.pkgName.equals("")) {
                        // 是否打开指定应用
                        this.pkgName = null;
                  }
                  // 云机状态正常开启回调，pkgName可直接打开云机指定应用
                  instance.startMediaStream(info.getInstanceId(), this.pkgName);
            } else {
                  // 查询失败 没有云机实例
                  destroy();
            }
      }

      // sdk内部开启串流回调
      @Override
      public void onStreamStarted(Bundle bundle) {
            Intent intent = new Intent(this, DemoCloudPhoneActivity.class);
            // sdk开启串流 启动播放器页面
            startActivity(intent);
      }

      // sdk串流失败回调
      @Override
      public void onStreamFailure(String message, int type, String status) {
            // 销毁实例
            destroy();
      }

      // 解析url
      private void analysisParam(String url) {
            Uri uri = Uri.parse(url);
            String ticket = uri.getQueryParameter("code");
            this.ticket = ticket;
            webView.setVisibility(View.GONE);
            button.setVisibility(View.VISIBLE);
            pkg.setVisibility(View.VISIBLE);
      }

      private void getCookies() {
            CookieManager.getInstance().flush();
      }

      // webview代理方法
      private class KpWebViewClient extends WebViewClient {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                  String currentUrl = request.getUrl().toString();
                  if (currentUrl.contains("https://koophone.huaweiapaas.com/login") && !currentUrl.contains("orgid.macroverse.huaweicloud.com/oauth2/authorize")) {
                        analysisParam(currentUrl);
                        getCookies();
                        return true;
                  } else {
                        return false;
                  }
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                  super.onReceivedError(view, request, error);
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                  super.onReceivedHttpError(view, request, errorResponse);
            }
      }
}

/**
 * demo app 播放器页面
 *
 * @since 2023-12-21
 */
public class DemoCloudPhoneActivity extends Activity implements KPPlayerCallback {
      private static final String TAG = "CloudPhoneActivity";

      private PlayerFragment mPlayerFragment; // 播放组件 textureview实现

      @Override
      protected void onCreate(@Nullable Bundle savedInstanceState) {
            // activity生命周期方法 初始化播放器
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            super.onCreate(savedInstanceState);
            setContentView(R.layout.cps_activity_cloud_phone);
            // 全屏显示
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            WindowManager.LayoutParams lp = getWindow().getAttributes();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                  lp.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
            }
            LogDebugUtils.i(TAG, "=================进入云机=================");
            getWindow().setAttributes(lp);

            // 初始化播放器
            initPlayer();

            // demo控件 模拟手机返回操作
            Button back = findViewById(R.id.back);
            back.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        // demo按钮 模拟手机返回操作
                        KPDriverInstance instance = KPDriverInstance.getInstance();
                        instance.sendKeyEvent(KEYCODE_BACK);
                  }
            });

            // demo控件 模拟手机设置清晰度
            Button profile = findViewById(R.id.profile);
            profile.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        KPDriverInstance instance = KPDriverInstance.getInstance();
                        // 设置画质 标清
                        instance.setVideoQuality(PROFILE_SPEED);
                  }
            });

            // demo控件 模拟手机设置清晰度
            Button profile1 = findViewById(R.id.profile1);
            profile1.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        KPDriverInstance instance = KPDriverInstance.getInstance();
                        // 设置画质 超清
                        instance.setVideoQuality(PROFILE_HIGH);
                  }
            });

            // demo控件 模拟手机设置清晰度
            Button profile2 = findViewById(R.id.profile2);
            profile2.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        KPDriverInstance instance = KPDriverInstance.getInstance();
                        // 设置画质 高清
                        instance.setVideoQuality(PROFILE_NORMAL);
                  }
            });
      }

      @Override
      protected void onNewIntent(Intent intent) {
            super.onNewIntent(intent);
            Bundle extras = intent.getExtras();
            if (extras == null) {
                  return;
            }
            LogDebugUtils.e(TAG, "=====onNewIntent启动云机====");
            // 初始化播放器
            initPlayer();
      }

      // 初始化播放器
      private void initPlayer() {
            mPlayerFragment = (PlayerFragment) getFragmentManager().findFragmentById(R.id.fragment_player);
            // 获取单一实例
            KPDriverInstance instance = KPDriverInstance.getInstance();
            // 设置回调
            instance.setPlayerCallBack(this);
            // 启动播放器前先停止串流
            instance.stopMediaStream();
            // 启动播放器
            instance.showPlayer(mPlayerFragment, getFragmentManager());
      }

      // 打印外部传入bundle数据
      public static void showBundleData(Bundle bundle) {
            if (bundle == null) {
                  return;
            }
            String string = "";
            for (String key : bundle.keySet()) {
                  string += key +  bundle.get(key);
            }
            LogDebugUtils.i(TAG, "bundleData" + string);
      }

      // 实例销毁流程
      @Override
      protected void onDestroy() {
            super.onDestroy();
            KPDriverInstance instance = KPDriverInstance.getInstance();
            // 停止串流
            instance.stopMediaStream();
            // 摧毁实例
            instance.destroyMediaStream();
      }

      // 播放器成功回调
      @Override
      public void onSuccess() {
            LogDebugUtils.i(TAG, "开始启动");
      }

      // 串流终止回调
      @Override
      public void onTerminated() {
            LogDebugUtils.i(TAG, "停止串流");
      }

      // 串流失败回调
      @Override
      public void onFailure(int code, String message) {
            LogDebugUtils.i(TAG, "串流失败" + code);
            switch (code) {
                  case CloudPhoneConst.CLOUD_PHONE_RET_KICKED:
                        // 服务器踢云机下线
                        break;
                  case CloudPhoneConst.CLOUD_PHONE_RET_INVALID_TOKEN:
                        // 用户重复登录
                        break;
                  case CloudPhoneConst.CLOUD_PHONE_RET_EXPIRED:
                        // token失效
                        break;
                  case CLOUD_PHONE_RET_ADMIN_EXIT:
                        // 时间到期
                        break;
                  case CloudPhoneConst.CLOUD_PHONE_RET_STOP_STREAMING:
                        // 时间到期
                        break;
                  case CloudPhoneConst.CLOUD_PHONE_RET_USER_EXIT:
                        // 后端主动断开连接
                        break;
                  default:
                        break;
            }
      }

      // 播放器屏幕旋转回调
      @Override
      public void onOrientationChange(int orientation) {
            if (orientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                  // 设置横屏
                  this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            } else {
                  // 设置竖屏
                  this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
      }

      // 被推流方改变
      @Override
      public void onSlotsInfo(String value) {
            LogDebugUtils.i(TAG, "连接用户改变");
      }

      // 房间信息改变
      @Override
      public void onRoomInfoUpdate(String s) {
            LogDebugUtils.i(TAG, "房间信息改变" + s);
      }

      // 主动权限请求
      @Override
      public void onPermissionRequest(String s) {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                  @Override
                  public void run() {
                        Context context = getApplication();
                        if (context == null) {
                              LogDebugUtils.i(TAG, "无法获取当前Activity 无法显示弹框");
                              return;
                        }
                        LogDebugUtils.i(TAG, "onPermissionRequest: 确认授权 ：per = " + finalPerStr + "  token = " + info.from.token);
                        // demo权限申请弹窗
                        CommonDialog commonDialog = new CommonDialog(CloudPhoneActivity.this);
                        commonDialog.setMessage("云应用请求使用本设备硬件数据，是否同意?");
                        commonDialog.setListener(new CommonDialog.OnDialogClickListener() {
                              @Override
                              public void cancelClick(CommonDialog dialog) {
                                    // 取消操作
                              }

                              @Override
                              public void confirmClick(CommonDialog dialog) {
                                    // 确认操作
                                    CloudPhoneClient.rightsAudit(finalPerStr, info.from.token, 1, "AUDITOR");
                                    LogDebugUtils.i(TAG, "onPermissionRequest: 确认授权 ：per = " + finalPerStr + "  token = " + info.from.token);
                              }
                        });
                        commonDialog.show();
                  }
            }, 500);
      }

      // 权限申请结果回调
      @Override
      public void onPermissionResult(String s) {
            LogDebugUtils.i(TAG, "权限申请结果" + s);
      }

      // 画质自动调节回调
      @Override
      public void onProfileReceived(String profile) {
            KPDriverInstance instance = KPDriverInstance.getInstance();
            // 设置画质
            instance.setVideoQuality(profile);
      }

      // 画质切换成功回调
      @Override
      public void onProfileChanged(int i) {
            if (i == PROFILE_HIGH) {
                  // 弹出toast
                  CommonUtil.showToast(this, "已切换至720p清晰度");
            } else if (i == PROFILE_NORMAL) {
                  CommonUtil.showToast(this, "已切换至540p清晰度");
            } else if (i == PROFILE_SPEED) {
                  CommonUtil.showToast(this, "已切换至360p清晰度");
            } else if (i == PROFILE_AUTO) {
                  CommonUtil.showToast(this, "已切换至自动清晰度");
            }
      }

      // 确认退出回调
      @Override
      public void onExitConfirm(int paramInt) {
            LogDebugUtils.i(TAG, "确认退出");
      }

      // 用户退出回调
      @Override
      public void onUserExit() {
            LogDebugUtils.i(TAG, "用户退出");
            // 停止串流
            instance.stopMediaStream();
            // 摧毁实例
            instance.destroyMediaStream();
      }

      // 权限校验回调
      @Override
      public void onPermissionDenied(String[] strings) {
            for (int i = 0; i < strings.length; i++) {
                  boolean contain = strings[i].contains("READ_EXTERNAL_STORAGE") || strings[i].contains("WRITE_EXTERNAL_STORAGE");
                  if (contain) {
                        dismissLoading();
                        dismissDialog();
                        PlayerWidgetManager.getInstance().sendMessageToUi(MSG_EXIT, null);
                        mCommonDialog = null;
                        finish();
                        CommonUtil.showToast(this,"缺少该权限云机无法正常运行");
                        break;
                  }
            }
      }

      // 云机操控回调
      @Override
      public void onActionMessage(String s) {
            LogDebugUtils.i(TAG, "云机触控" + s);
      }

      // 被动授权
      @Override
      public void onApplyPhonePermission(String s) {
            try {
                  JSONObject jsonObject = new JSONObject(s);
                  JSONArray permission = jsonObject.getJSONArray("permission");

                  String[] p = new String[permission.length()];
                  for (int i = 0; i < permission.length(); i++) {
                        p[i] = (String) permission.get(i);
                        Log.i(TAG, "进程===>" + p[i]);
                  }
                  ActivityCompat.requestPermissions(this, p, CODE_PERMISSION_REQUEST_CODE);
                  mPermissionTrackId = jsonObject.getInt("trackId");
            } catch (JSONException e) {
                  LogDebugUtils.i(TAG, "授权失败");
            }
      }

      // 接收来自云机端的自定义字串信息和接受的信息的activity名称
      @Override
      public void onExtMessageReceived(Activity activity, String msgStr) {
            LogDebugUtils.i(TAG, "云机自定义消息" + msgStr);
      }

      // 用户确认系统授权
      @Override
      public void onPermissionGranted(String[] permissions) {
            LogDebugUtils.i(TAG, "用户确认系统授权");
      }
}
```

## 5. 修订记录

|   发布日期    | 文档版本 | 修订说明 |
|:---------:| :------: | :----------: |
| 2023-12-1 | 1.0 | 文档首次发布 |