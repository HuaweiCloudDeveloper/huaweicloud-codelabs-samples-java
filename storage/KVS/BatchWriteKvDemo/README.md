## 1、介绍
华为云提供了KVS服务端SDK，您可以直接集成服务端SDK来调用KVS的相关API，从而实现对KVS服务的快速操作。KVS服务提供完全托管的键值存储及索引服务，主要用于应用的键值类数据（如：元数据、描述数据、管理参数、状态数据）的存储。接下来将介绍如何通过java版SDK批量插入、删除KV数据，扫描KV数据，以及扫描指定分区键下的KV数据。

## 2、前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云租户账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 3、endpoint 华为云各服务应用区域和各服务的终端节点，KVS的endpoint需要购买VPC节点，创建内网域名获取，参考[VPC终端节点对接KVS](https://support.huaweicloud.com/usermanual-kvs/kvs_01_0110.html) 。
- 4、华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。
- 5、用户需要在镜像服务创建镜像。

## 3、SDK获取和安装
您可以通过如下方式获取和安装 SDK： 通过 Maven 安装依赖（推荐） 通过 Maven 安装项目依赖是使用 Java SDK 的推荐方法，首先您需要在您的操作系统中下载并安装 Maven ，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
本示例使用KVS SDK：
```xml
<dependencies>
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-kvs</artifactId>
        <version>${version}</version>
    </dependency>
</dependencies>
```

## 4、代码示例
以下代码展示如何使用批量写KV和扫描KV的SDK
### 4.1 导入依赖模块

```java
//用户身份认证
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
//异常类
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
//Kvs接口类
import com.huaweicloud.sdk.kvs.v1.KvsClient;
import com.huaweicloud.sdk.kvs.v1.model.*;
//日志类
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
```

### 4.2 初始化认证信息
```java
String ak = System.getenv("HUAWEICLOUD_SDK_AK");
String sk = System.getenv("HUAWEICLOUD_SDK_SK");
// 创建认证
BasicCredentials credentials = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);
```   
相关参数说明如下所示：
- HUAWEICLOUD_SDK_AK：华为云账号Access Key。
- HUAWEICLOUD_SDK_SK：华为云账号Secret Access Key 。

### 4.3 初始化KVS服务的客户端
```java
// 配置客户端属性
HttpConfig config = HttpConfig.getDefaultHttpConfig();
config.withIgnoreSSLVerification(true);
// 创建KvsClient实例并初始化
String regionId = "${YOUR_REGION_ID}";
String endpoint = "${KVS_ENDPOINT}";
Region region = new Region(regionId, endpoint);
KvsClient kvsClient = KvsClient.newBuilder()
        .withCredential(credentials)
        .withRegion(region)
        .withHttpConfig(config)
        .build();
```
相关参数说明如下所示：
- credentials： 用户认证信息，见4.2的初始化认证信息。
- regionId：KVS集群所在区域Id。
- endpoint: KVS服务域名。
- config: 客户端属性。

### 4.4 发送请求
构造请求，然后通过4.3初始化好的客户端发送请求。
#### 4.4.1 创建表
发送创建表请求，向指定仓中创建表，如果仓不存在，将会在自动创建仓。
```java
/**
 * 创建表
 *
 * @param client kvs客户端
 */
public void createTable(KvsClient client) {
    // 设置分区键的名称和排序（必选）
    List<Field> shardKeyFields = new ArrayList<>();
    shardKeyFields.add(new Field().withName(${shardKeyName}).withOrder(true));
    // 设置排序键的名称和排序（非必选）
    List<Field> sortKeyFields = new ArrayList<>();
    sortKeyFields.add(new Field().withName(${sortKeyName}).withOrder(true));
    // 设置主键
    PrimaryKeySchema primaryKeySchema = new PrimaryKeySchema().
            withSortKeyFields(sortKeyFields).
            withShardKeyFields(shardKeyFields);
    // 设置全局二级索引表的分区键名称和排序（非必选）
    List<Field> globalIndexShardKeyFields = new ArrayList<>();
    globalIndexShardKeyFields.add(new Field().withName(globalShardKeyName).withOrder(true));
    List<Field> globalIndexSortKeyFields = new ArrayList<>();
    // 设置全局二级索引表的排序键名称和排序（非必选）
    globalIndexSortKeyFields.add(new Field().withName(${globalSortKeyName}).withOrder(true));
    // 设置全局二级索引的摘要字段（非必选）
    List<String> abstractFields = new ArrayList<>();
    abstractFields.add(${globalAbstractName});
    // 构造全局二级索引，设置索引名称、分区键、排序键、分区模式，以及摘要字段（非必选）
    GlobalSecondaryIndex globalSecondaryIndex = new GlobalSecondaryIndex().
            withIndexName(globalIndexName).
            withShardKeyFields(globalIndexShardKeyFields).
            withShardMode("range").
            withSortKeyFields(globalIndexSortKeyFields).
            withAbstractFields(abstractFields);
    List<GlobalSecondaryIndex> globalSecondaryIndices = new ArrayList<>();
    globalSecondaryIndices.add(globalSecondaryIndex);
    // 设置本地二级索引的排序键名称和排序（非必选）
    List<Field> localSortKeyFields = new ArrayList<>();
    localSortKeyFields.add(new Field().withName(${localSortKeyName}).withOrder(true));
    // 构造本地二级索引，设置本地二级索引的索引名称和排序键（非必选）
    SecondaryIndex secondaryIndex = new SecondaryIndex().
        withIndexName(localIndexName).
        withSortKeyFields(localSortKeyFields);
    List<SecondaryIndex> secondaryIndices = new ArrayList<>();
    secondaryIndices.add(secondaryIndex);
    // 构造创表请求，指定存储仓和表名
    CreateTableRequestBody createTableRequestBody = new CreateTableRequestBody().
            withTableName(tableName).
            withPrimaryKeySchema(primaryKeySchema);
    CreateTableRequest createTableRequest = new CreateTableRequest().withBody(createTableRequestBody).withStoreName(storeName);
    createTableRequestBody.withGlobalSecondaryIndexSchema(globalSecondaryIndices).withLocalSecondaryIndexSchema(secondaryIndices);
    try {
        // 发送创表请求
        CreateTableResponse createTableResponse = client.createTable(createTableRequest);
        LOGGER.info(createTableResponse.toString());
    } catch (ClientRequestException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.toString());
    } catch (ServerResponseException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.getMessage());
    }
}
```
相关参数说明如下所示（后面出现不再重复说明）：
- ${shardKeyName}：分区键名称
- ${sortKeyName}：排序键名称
- ${globalShardKeyName}：全局二级索引分区键名称
- ${globalSortKeyName}：全局二级索引排序键名称
- ${globalAbstractName}：摘要字段名称
- ${globalIndexName}：全局二级索引名称
- ${localSortKeyName}：本地二级索引排序键名称
- ${localIndexName}：本地二级索引名称
- ${tableName}：表名
- ${storeName}：仓名
#### 4.4.2 上传KV数据
向4.4.1创建的表中插入kv数据，为了后续演示批量删除操作。
```java
/**
 * 上传KV
 *
 * @param client kvs客户端
 */
public void putKv(KvsClient client) {
    // 设置主键
    Document kvDoc = new Document();
    kvDoc.put(${shardKeyName}, "{your shard key value}");
    kvDoc.put(${sortKeyName}, "{your sort key value}");
    kvDoc.put(globalShardKeyName, "{your global index shard key value1}");
    kvDoc.put(${globalSortKeyName}, "{your global index sort key value1}");
    kvDoc.put(${globalAbstractName}, "{{your global index abstract field value}");
    kvDoc.put(${localSortKeyName}, "{your local index sort key value1}");
    kvDoc.put("{your other key}", "{your other value}");
    // 构造插入kv请求
    PutKvRequest putKvRequest = new PutKvRequest().withBody(new PutKvRequestBody().withKvDoc(kvDoc).
                    withTableName(tableName)).
            withStoreName(storeName);
    try {
        // 发送插入kv请求
        PutKvResponse putKvResponse = client.putKv(putKvRequest);
        LOGGER.info(putKvResponse.toString());
    } catch (ClientRequestException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.toString());
    } catch (ServerResponseException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.getMessage());
    }
}
```

#### 4.4.3 批量上传与删除KV数据
发送批量写请求，向4.4.1创建的表中批量写入两组kv数据，并将4.4.2插入的kv数据删除。
```java
/**
 * 批量上传kv
 *
 * @param client kvs客户端
 */
public void batchWriteKv(KvsClient client) {
    // 构造两组kv数据
    Document kvDoc1 = new Document();
    kvDoc1.put(${shardKeyName}, "{your shard key value1}");
    kvDoc1.put(${sortKeyName}, "{your sort key value1}");
    kvDoc1.put(${globalShardKeyName}, "{your global index shard key value1}");
    kvDoc1.put(${globalSortKeyName}, "{your global index sort key value1}");
    kvDoc1.put(${globalAbstractName}, "{{your global index abstract field value1}");
    kvDoc1.put(${localSortKeyName}, "{your local index sort key value1}");
    kvDoc1.put("{your other key1}", "{your other value1}");
    Document kvDoc2 = new Document();
    kvDoc2.put(${shardKeyName}, "{your shard key value2}");
    kvDoc2.put(${sortKeyName}, "{your sort key value2}");
    kvDoc2.put(${globalShardKeyName}, "{your global index shard key value2}");
    kvDoc2.put(${globalSortKeyName}, "{your global index sort key value2}");
    kvDoc2.put(${globalAbstractName}, "{your global index abstract field value2}");
    kvDoc2.put(${localSortKeyName}, "{your local index sort key value2}");
    kvDoc2.put("{your other key2}", "{your other value2}");
    // 构造上传kv操作
    OperItem putKvOper1 = new OperItem().withPutKv(new PutKv().withOperId(0).withKvDoc(kvDoc1));
    OperItem putKvOper2 = new OperItem().withPutKv(new PutKv().withOperId(1).withKvDoc(kvDoc2));
    // 构造删除kv操作
    Document primaryKey = new Document();
    primaryKey.put(${shardKeyName}, "{your shard key value}");
    primaryKey.put(${sortKeyName}, "{your sort key value}");
    OperItem deleteKvOper = new OperItem().withDeleteKv(new DeleteKv().
                withOperId(2).
                withPrimaryKey(primaryKey));
    // 将两个上传和一个删除kv操作添加到操作数组
    List<OperItem> kvopers = new ArrayList<>();
    kvopers.add(putKvOper1);
    kvopers.add(putKvOper2);
    kvopers.add(deleteKvOper);
    // 构造表的批量操作，设置表名和批量操作
    TableBatch tableBatch = new TableBatch().withTableName(${tableName}).withKvOpers(kvopers);
    // 将表的批量操作添加到所有表的批量操作数组
    List<TableBatch> tableopers = new ArrayList<>();
    tableopers.add(tableBatch);
    // 构造批量写请求
    BatchWriteKvRequest batchWriteKvRequest = new BatchWriteKvRequest().withBody(new BatchWriteKvRequestBody().
                    withTableOpers(tableopers)).
            withStoreName(${storeName});
    try {
        // 发送批量写请求
        BatchWriteKvResponse batchWriteKvResponse = client.batchWriteKv(batchWriteKvRequest);
        LOGGER.info(batchWriteKvResponse.toString());
    } catch (ClientRequestException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.toString());
    } catch (ServerResponseException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.getMessage());
    }
}
```

#### 4.4.4 扫描KV数据
发送扫描kv请求，查看4.4.3批量操作以后的结果。
```java
/**
 * 扫描KV
 *
 * @param client kvs客户端
 */
public void scanKv(KvsClient client) {
    // 设置条件值
    Document value = new Document();
    value.put("{any value}", "{your shard key value1}");
    // 设置扫描条件为分区键等于指定值（非必选）
    SingleFieldExpression singleFieldExpression = new SingleFieldExpression().
            withField(${shardKeyName}).
            withFunc("$eq").
            withValue(value);
    ConditionExpression conditionExpression = new ConditionExpression().withSingleFieldExpression(singleFieldExpression);
    // 设置扫描的起始位置（非必选）
    Document startKey = new Document();
    startKey.put(${shardKeyName}, "{your shard key value1}");
    startKey.put(${sortKeyName}, "{your sort key value1}");
    // 设置扫描的结束位置（非必选）
    Document endKey = new Document();
    endKey.put(${shardKeyName}, "{your shard key value3}");
    endKey.put(${sortKeyName}, "{your sort key value3}");
    // 构造扫描kv请求，设置查询条件，以及扫描的起始和结束位置
    ScanKvRequest scanKvRequest = new ScanKvRequest().withBody(new ScanKvRequestBody().
                    withTableName(${tableName}).
                    withFilterExpression(conditionExpression).
                    withStartKey(startKey).
                    withEndKey(endKey)).
            withStoreName(${storeName});
    try {
        // 发送扫描kv请求
        ScanKvResponse scanKvResponse = client.scanKv(scanKvRequest);
        LOGGER.info(scanKvResponse.toString());
    } catch (ClientRequestException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.toString());
    } catch (ServerResponseException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.getMessage());
    }
}
```

#### 4.4.5 扫描指定分区键下的KV数据
除了执行4.4.4以外，还可以发送扫描指定分区键下的kv请求，查看4.4.3批量操作以后的结果。
```java
/**
 * 扫描指定分区键下的KV
 *
 * @param client kvs客户端
 */
public void scanSkeyKv(KvsClient client) {
    // 设置指定的分区键（必选）
    Document shardKey = new Document();
    shardKey.put(${shardKeyName}, "{your shard key value2}");
    // 设置扫描的sort key的起始和结束范围（非必选）
    Document startSortKey = new Document();
    startSortKey.put(${sortKeyName}, "{your sort key value1}");
    Document endSortKey = new Document();
    endSortKey.put(${sortKeyName}, "{your sort key value3}");
    // 构造scanSkeyKv请求，设置扫描的分区键，以及起始和结束排序键
    ScanSkeyKvRequest scanSkeyKvRequest = new ScanSkeyKvRequest().withBody(new ScanSkeyKvRequestBody().
                    withTableName(${tableName}).
                    withShardKey(shardKey).
                    withStartSortKey(startSortKey).
                    withEndSortKey(endSortKey)).
            withStoreName(${storeName});
    try {
        // 发送插入kv请求
        ScanSkeyKvResponse scanSkeyKvResponse = client.scanSkeyKv(scanSkeyKvRequest);
        LOGGER.info(scanSkeyKvResponse.toString());
    } catch (ClientRequestException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.toString());
    } catch (ServerResponseException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.getMessage());
    }
}
```
#### 4.4.6 扫描本地二级索引表
通过指定本地二级索引名称，扫描本地二级索引表。
```java
/**
 * 扫描本地二级索引表
 *
 * @param client kvs客户端
 */
public void scanIndexTable(KvsClient client) {
    // 构造扫描kv请求，指定索引名称，扫描本地二级索引表（非必选）
    ScanKvRequest scanLocalIndexRequest = new ScanKvRequest().withBody(new ScanKvRequestBody().
                    withHintIndexName(${localIndexName}).
                    withTableName(${tableName})).
            withStoreName(${storeName});
    try {
        // 发送扫描kv请求
        ScanKvResponse scanLocalIndexResponse = client.scanKv(scanLocalIndexRequest);
        LOGGER.info(scanLocalIndexResponse.toString());
    } catch (ClientRequestException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.toString());
    } catch (ServerResponseException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.getMessage());
    }
}
```

## 5、修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-05-09 | 1.0 | 文档首次发布 |