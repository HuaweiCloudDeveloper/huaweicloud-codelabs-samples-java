package com.huawei.kvs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.kvs.v1.KvsClient;
import com.huaweicloud.sdk.kvs.v1.model.*;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BatchWriteDemo {
    static String storeName = "{prefix}-{region-id}-{domain-id}";
    static String tableName = "{your table name}";
    static String shardkeyName = "{your shard key}";
    static String sortkeyName = "{your sort key}";
    static String globalShardKeyName = "score";
    static String globalSortKeyName = "date";
    static String localSortKeyName = "level";
    static String globalAbstractName = "role";
    static String globalIndexName = "gsiIndex";
    static String localIndexName = "lsiIndex";
    static String regionId = "{your regionId string}";
    static String endpoint = "{your endpoint string}";
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchWriteDemo.class);

    public static void main(String[] args) {
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 创建认证
        BasicCredentials credentials = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        // 配置客户端属性
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        // 创建KvsClient实例并初始化
        Region region = new Region(regionId, endpoint);
        KvsClient kvsClient = KvsClient.newBuilder()
                .withCredential(credentials)
                .withRegion(region)
                .withHttpConfig(config)
                .build();
        BatchWriteDemo batchWriteDemo = new BatchWriteDemo();
        try {
            // 创建表
            batchWriteDemo.createTable(kvsClient);
            // 上传kv
            batchWriteDemo.putKv(kvsClient);
            // 向表中批量写入和删除kv数据
            batchWriteDemo.batchWriteKv(kvsClient);
            // 扫描kv
            batchWriteDemo.scanKv(kvsClient);
            // 扫描指定分区键下的kv
            batchWriteDemo.scanSkeyKv(kvsClient);
            // 扫描本地二级索引表
            batchWriteDemo.scanIndexTable(kvsClient);
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * 创建表
     *
     * @param client kvs客户端
     */
    public void createTable(KvsClient client) {
        // 设置分区键的名称和排序
        List<Field> shardKeyFields = new ArrayList<>();
        shardKeyFields.add(new Field().withName(shardkeyName).withOrder(true));
        // 设置排序键的名称和排序（非必选）
        List<Field> sortKeyFields = new ArrayList<>();
        sortKeyFields.add(new Field().withName(sortkeyName).withOrder(true));
        // 设置主键
        PrimaryKeySchema primaryKeySchema = new PrimaryKeySchema().
                withSortKeyFields(sortKeyFields).
                withShardKeyFields(shardKeyFields);
        // 构造全局二级索引的分区键和排序键（非必选）
        List<Field> globalIndexShardKeyFields = new ArrayList<>();
        globalIndexShardKeyFields.add(new Field().withName(globalShardKeyName).withOrder(true));
        List<Field> globalIndexSortKeyFields = new ArrayList<>();
        globalIndexSortKeyFields.add(new Field().withName(globalSortKeyName).withOrder(true));
        // 设置全局二级索引的摘要字段（非必选）
        List<String> abstractFields = new ArrayList<>();
        abstractFields.add(globalAbstractName);
        // 构造全局二级索引，设置索引名称、分区键、排序键、分区模式，以及摘要字段（非必选）
        GlobalSecondaryIndex globalSecondaryIndex = new GlobalSecondaryIndex().
                withIndexName(globalIndexName).
                withShardKeyFields(globalIndexShardKeyFields).
                withShardMode("range").
                withSortKeyFields(globalIndexSortKeyFields).
                withAbstractFields(abstractFields);
        List<GlobalSecondaryIndex> globalSecondaryIndices = new ArrayList<>();
        globalSecondaryIndices.add(globalSecondaryIndex);
        // 设置本地二级索引表的排序键（非必选）
        List<Field> localSortKeyFields = new ArrayList<>();
        localSortKeyFields.add(new Field().withName(localSortKeyName).withOrder(true));
        // 构造本地二级索引，设置本地二级索引的索引名称和排序键（非必选）
        SecondaryIndex secondaryIndex = new SecondaryIndex().
                withIndexName(localIndexName).
                withSortKeyFields(localSortKeyFields);
        List<SecondaryIndex> secondaryIndices = new ArrayList<>();
        secondaryIndices.add(secondaryIndex);
        // 构造创表请求，指定存储仓和表名
        CreateTableRequest createTableRequest = new CreateTableRequest().withBody(new CreateTableRequestBody()
                        .withTableName(tableName)
                        .withPrimaryKeySchema(primaryKeySchema)
                        .withGlobalSecondaryIndexSchema(globalSecondaryIndices)
                        .withLocalSecondaryIndexSchema(secondaryIndices))
                .withStoreName(storeName);
        // 发送创表请求
        CreateTableResponse createTableResponse = client.createTable(createTableRequest);
        LOGGER.info(createTableResponse.toString());
    }

    /**
     * 上传KV
     *
     * @param client kvs客户端
     */
    public void putKv(KvsClient client) {
        // 设置主键
        Document kvDoc = new Document();
        kvDoc.put(shardkeyName, "{your shard key value}");
        kvDoc.put(sortkeyName, "{your sort key value}");
        // 设置kv数据
        kvDoc.put("{your other key}","{your other value}");
        // 构造插入kv请求
        PutKvRequest putKvRequest = new PutKvRequest().withBody(new PutKvRequestBody().withKvDoc(kvDoc).
                        withTableName(tableName)).
                withStoreName(storeName);
        // 发送插入kv请求
        PutKvResponse putKvResponse = client.putKv(putKvRequest);
        LOGGER.info(putKvResponse.toString());
    }

    /**
     * 批量上传kv
     *
     * @param client kvs客户端
     */
    public void batchWriteKv(KvsClient client) {
        // 构造两个批量上传的kv数据
        Document kvDoc1 = new Document();
        kvDoc1.put(shardkeyName, "{your shard key value1}");
        kvDoc1.put(sortkeyName, "{your sort key value1}");
        kvDoc1.put(globalShardKeyName, "{your global shard key value1}");
        kvDoc1.put(globalSortKeyName, "{your global sort key value1}");
        kvDoc1.put(globalAbstractName, "{your global abstract field value1}");
        kvDoc1.put(localSortKeyName, "{your local sort key value1}");
        kvDoc1.put("{your other key1}", "{your other value1}");
        Document kvDoc2 = new Document();
        kvDoc2.put(shardkeyName, "{your shard key value2}");
        kvDoc2.put(sortkeyName, "{your sort key value2}");
        kvDoc1.put(globalShardKeyName, "{your global shard key value2}");
        kvDoc1.put(globalSortKeyName, "{your global sort key value2}");
        kvDoc1.put(globalAbstractName, "{your global abstract field value2}");
        kvDoc1.put(localSortKeyName, "{your local sort key value2}");
        kvDoc2.put("{your other key2}", "{your other value2}");
        // 构造上传kv操作
        OperItem putKvOper1 = new OperItem().withPutKv(new PutKv().withOperId(0).withKvDoc(kvDoc1));
        OperItem putKvOper2 = new OperItem().withPutKv(new PutKv().withOperId(1).withKvDoc(kvDoc2));
        // 构造删除kv操作
        Document primaryKey = new Document();
        primaryKey.put(shardkeyName, "{your shard key value}");
        primaryKey.put(sortkeyName, "{your sort key value}");
        OperItem deleteKvOper = new OperItem().withDeleteKv(new DeleteKv().withOperId(2).withPrimaryKey(primaryKey));
        // 将两个上传和一个删除kv操作添加到操作数组
        List<OperItem> kvopers = new ArrayList<>();
        kvopers.add(putKvOper1);
        kvopers.add(putKvOper2);
        kvopers.add(deleteKvOper);
        // 构造表的批量操作，设置表名和批量操作
        TableBatch tableBatch = new TableBatch().withTableName(tableName).withKvOpers(kvopers);
        // 将表的批量操作添加到所有表的批量操作数组
        List<TableBatch> tableopers = new ArrayList<>();
        tableopers.add(tableBatch);
        // 构造批量写请求
        BatchWriteKvRequest batchWriteKvRequest = new BatchWriteKvRequest().withBody(new BatchWriteKvRequestBody().
                        withTableOpers(tableopers)).
                withStoreName(storeName);
        // 发送批量写请求
        BatchWriteKvResponse batchWriteKvResponse = client.batchWriteKv(batchWriteKvRequest);
        LOGGER.info(batchWriteKvResponse.toString());
    }

    /**
     * 扫描KV
     *
     * @param client kvs客户端
     */
    public void scanKv(KvsClient client) {
        // 设置条件值
        Document value = new Document();
        value.put("{any value}", "{your shard key value1}");
        // 设置扫描条件为分区键等于指定值（非必选）
        SingleFieldExpression singleFieldExpression = new SingleFieldExpression().withField(shardkeyName).withFunc("$eq").withValue(value);
        ConditionExpression conditionExpression = new ConditionExpression().withSingleFieldExpression(singleFieldExpression);
        // 设置扫描的起始位置（非必选）
        Document startKey = new Document();
        startKey.put(shardkeyName, "{your shard key value1}");
        startKey.put(sortkeyName, "{your sort key value1}");
        // 设置扫描的结束位置（非必选）
        Document endKey = new Document();
        endKey.put(shardkeyName, "{your shard key value3}");
        endKey.put(sortkeyName, "{your sort key value3}");
        // 构造扫描kv请求
        ScanKvRequest scanKvRequest = new ScanKvRequest().withBody(new ScanKvRequestBody().withTableName(tableName).
                        withFilterExpression(conditionExpression).
                        withStartKey(startKey).
                        withEndKey(endKey)).
                withStoreName(storeName);
        // 发送扫描kv请求
        ScanKvResponse scanKvResponse = client.scanKv(scanKvRequest);
        LOGGER.info(scanKvResponse.toString());
    }


    /**
     * 扫描指定分区键下的KV
     *
     * @param client kvs客户端
     */
    public void scanSkeyKv(KvsClient client) {
        // 设置指定的分区键
        Document shardKey = new Document();
        shardKey.put(shardkeyName, "{your shard key value2}");
        // 设置扫描的sort key的起始和结束范围（非必选）
        Document startSortKey = new Document();
        startSortKey.put(sortkeyName, "{your sort key value1}");
        Document endSortKey = new Document();
        endSortKey.put(sortkeyName, "{your sort key value3}");
        // 构造scanSkeyKv请求
        ScanSkeyKvRequest scanSkeyKvRequest = new ScanSkeyKvRequest().withBody(new ScanSkeyKvRequestBody().
                        withTableName(tableName).
                        withShardKey(shardKey).
                        withStartSortKey(startSortKey).
                        withEndSortKey(endSortKey)).
                withStoreName(storeName);
        // 发送插入kv请求
        ScanSkeyKvResponse scanSkeyKvResponse = client.scanSkeyKv(scanSkeyKvRequest);
        LOGGER.info(scanSkeyKvResponse.toString());
    }

    /**
     * 扫描索引表
     *
     * @param client kvs客户端
     */
    public void scanIndexTable(KvsClient client) {
        // 构造扫描kv请求，扫描本地二级索引表（非必选）
        ScanKvRequest scanLocalIndexRequest = new ScanKvRequest().withBody(new ScanKvRequestBody().
                        withHintIndexName(localIndexName).
                        withTableName(tableName)).
                withStoreName(storeName);
        try {
            // 发送扫描kv请求
            ScanKvResponse scanLocalIndexResponse = client.scanKv(scanLocalIndexRequest);
            LOGGER.info(scanLocalIndexResponse.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }
}