package com.huawei.kvs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.kvs.v1.KvsClient;
import com.huaweicloud.sdk.kvs.v1.model.*;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PutKvDemo {
    static String storeName = "{prefix}-{region-id}-{domain-id}";
    static String tableName = "{your table name}";
    static String shardkeyName = "{your shard key}";
    static String sortkeyName = "{your sort key}";
    static String regionId = "{your regionId string}";
    static String endpoint = "{your endpoint string}";
    private static final Logger LOGGER = LoggerFactory.getLogger(PutKvDemo.class);
    public static void main(String[] args) {
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 创建认证
        BasicCredentials credentials = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        // 配置客户端属性
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        // 创建KvsClient实例并初始化
        Region region = new Region(regionId, endpoint);
        KvsClient kvsClient = KvsClient.newBuilder()
                .withCredential(credentials)
                .withRegion(region)
                .withHttpConfig(config)
                .build();
        PutKvDemo putKvDemo = new PutKvDemo();
        try {
            // 创建表
            putKvDemo.createTable(kvsClient);
            // 插入kv数据
            putKvDemo.putKv(kvsClient);
            // 查询插入的kv数据
            putKvDemo.getKv(kvsClient);
            // 删除插入的kv数据
            putKvDemo.deleteKv(kvsClient);
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * 创建表
     *
     * @param client kvs客户端
     */
    public void createTable(KvsClient client) {
        // 设置分区键的名称和排序(必选）
        List<Field> shardKeyFields = new ArrayList<>();
        shardKeyFields.add(new Field().withName(shardkeyName).withOrder(true));
        // 设置排序键的名称和排序（非必选）
        List<Field> sortKeyFields = new ArrayList<>();
        sortKeyFields.add(new Field().withName(sortkeyName).withOrder(true));
        // 设置主键（必选）
        PrimaryKeySchema primaryKeySchema = new PrimaryKeySchema().
                withSortKeyFields(sortKeyFields).
                withShardKeyFields(shardKeyFields);
        // 构造创表请求，指定存储仓和表名
        CreateTableRequest createTableRequest = new CreateTableRequest().withBody(new CreateTableRequestBody()
                        .withTableName(tableName)
                        .withPrimaryKeySchema(primaryKeySchema))
                .withStoreName(storeName);
        // 发送创表请求
        CreateTableResponse createTableResponse = client.createTable(createTableRequest);
        LOGGER.info(createTableResponse.toString());
    }

    /**
     * 插入KV
     *
     * @param client kvs客户端
     */
    public void putKv(KvsClient client) {
        // 设置主键（必选）
        Document kvDoc = new Document();
        kvDoc.put(shardkeyName, "{your shard key value}");
        kvDoc.put(sortkeyName, "{your sort key value}");
        // 设置kv数据
        kvDoc.put("{your other key}","{your other value}");
        // 构造插入kv请求
        PutKvRequest putKvRequest = new PutKvRequest().withBody(new PutKvRequestBody().withKvDoc(kvDoc).
                withTableName(tableName)).
                withStoreName(storeName);
        // 发送插入kv请求
        PutKvResponse putKvResponse = client.putKv(putKvRequest);
        LOGGER.info(putKvResponse.toString());
    }


    /**
     * 查询KV
     *
     * @param client kvs客户端
     */
    public void getKv(KvsClient client) {
        // 设置主键（必选）
        Document primaryKey = new Document();
        primaryKey.put(shardkeyName, "{your shard key value}");
        primaryKey.put(sortkeyName, "{your sort key value}");
        // 构造查询kv请求
        GetKvRequest getKvRequest = new GetKvRequest().withBody(new GetKvRequestBody().withTableName(tableName).
                withPrimaryKey(primaryKey))
                .withStoreName(storeName);
        // 发送插入kv请求
        GetKvResponse getKvResponse = client.getKv(getKvRequest);
        LOGGER.info(getKvResponse.toString());
    }

    /**
     * 删除KV
     *
     * @param client kvs客户端
     */
    public void deleteKv(KvsClient client) {
        // 设置主键，删除主键指定的kv文档（必选）
        Document primaryKey = new Document();
        primaryKey.put(shardkeyName, "{your shard key value}");
        primaryKey.put(sortkeyName, "{your sort key value}");
        DeleteKvRequest deleteKvRequest = new DeleteKvRequest().withBody(new DeleteKvRequestBody().
                        withTableName(tableName).
                        withPrimaryKey(primaryKey)).
                        withStoreName(storeName);
        // 发送删除kv请求
        DeleteKvResponse deleteKvResponse = client.deleteKv(deleteKvRequest);
        LOGGER.info(deleteKvResponse.toString());
    }
}