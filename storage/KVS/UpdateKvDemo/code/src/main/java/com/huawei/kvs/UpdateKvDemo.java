package com.huawei.kvs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.kvs.v1.KvsClient;
import com.huaweicloud.sdk.kvs.v1.model.*;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateKvDemo {
    static String storeName = "{prefix}-{region id}-{domain id}";
    static String tableName = "{your table name}";
    static String shardkeyName = "{your shard key}";
    static String sortkeyName = "{your sort key}";
    static String regionId = "{your regionId string}";
    static String endpoint = "{your endpoint string}";
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateKvDemo.class);

    public static void main(String[] args) {
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 创建认证
        BasicCredentials credentials = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        // 配置客户端属性
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        // 创建KvsClient实例并初始化
        Region region = new Region(regionId, endpoint);
        KvsClient kvsClient = KvsClient.newBuilder()
                .withCredential(credentials)
                .withRegion(region)
                .withHttpConfig(config)
                .build();
        UpdateKvDemo updateKvDemo = new UpdateKvDemo();
        // 创建表
        updateKvDemo.createTable(kvsClient);
        // 上传kv
        updateKvDemo.putKv(kvsClient);
        // 新增多个字段
        updateKvDemo.updateSetKv(kvsClient);
        // 对多个字段做加法运算
        updateKvDemo.updateAddKv(kvsClient);
        // 删除一个字段
        updateKvDemo.updateRmvKv(kvsClient);
        // 下载kv
        updateKvDemo.getKv(kvsClient);
    }

    /**
     * 创建表
     *
     * @param client kvs客户端
     */
    public void createTable(KvsClient client) {
        // 设置分区键的名称和排序
        List<Field> shardKeyFields = new ArrayList<>();
        shardKeyFields.add(new Field().withName(shardkeyName).withOrder(true));
        // 设置排序键的名称和排序
        List<Field> sortKeyFields = new ArrayList<>();
        sortKeyFields.add(new Field().withName(sortkeyName).withOrder(true));
        // 设置主键
        PrimaryKeySchema primaryKeySchema = new PrimaryKeySchema().
                withSortKeyFields(sortKeyFields).
                withShardKeyFields(shardKeyFields);
        // 构造创表请求，指定存储仓和表名
        CreateTableRequest createTableRequest = new CreateTableRequest().withBody(new CreateTableRequestBody()
                        .withTableName(tableName)
                        .withPrimaryKeySchema(primaryKeySchema))
                .withStoreName(storeName);
        // 发送创表请求
        try {
            CreateTableResponse createTableResponse = client.createTable(createTableRequest);
            LOGGER.info(createTableResponse.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * 插入KV
     *
     * @param client kvs客户端
     */
    public void putKv(KvsClient client) {
        Document kvDoc = new Document();
        // 设置主键
        kvDoc.put(shardkeyName, "{your shard key value}");
        kvDoc.put(sortkeyName, "{your sort key value}");
        // 插入其他kv数据
        kvDoc.put("{your other key1}", 1);
        kvDoc.put("{your other key2}", 2);
        // 构造插入kv请求
        PutKvRequest putKvRequest = new PutKvRequest().withBody(new PutKvRequestBody().withKvDoc(kvDoc).
                        withTableName(tableName)).
                withStoreName(storeName);
        // 发送插入kv请求
        PutKvResponse putKvResponse = client.putKv(putKvRequest);
        LOGGER.info(putKvResponse.toString());
    }

    /**
     * 新增或覆盖一个或多个字段
     *
     * @param client kvs客户端
     */
    public void updateSetKv(KvsClient client) {
        // 设置要更新doc的主键
        Document primaryKey = new Document();
        primaryKey.put(shardkeyName, "{your shard key value}");
        primaryKey.put(sortkeyName, "{your sort key value}");
        // 设置要在doc中新增的多个字段
        Document set = new Document();
        set.put("{your new key1", "{your new value1}");
        set.put("your new key2", "{your new value2}");
        // updateFields设置set字段，表示这次更新是一个set操作
        UpdateFields updateFields = new UpdateFields().withSet(set);
        // 创建更新kv请求
        UpdateKvRequest updateKvRequest = new UpdateKvRequest().withBody(new UpdateKvRequestBody()
                        .withPrimaryKey(primaryKey)
                        .withUpdateFields(updateFields)
                        .withTableName(tableName))
                .withStoreName(storeName);
        // 发送更新kv请求
        try {
            UpdateKvResponse updateKvSetResponse = client.updateKv(updateKvRequest);
            LOGGER.info(updateKvSetResponse.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * 对一个或多个字段做加法运算
     *
     * @param client kvs客户端
     */
    public void updateAddKv(KvsClient client) {
        // 设置要更新doc的主键
        Document primaryKey = new Document();
        primaryKey.put(shardkeyName, "{your shard key value}");
        primaryKey.put(sortkeyName, "{your sort key value}");
        // 指定key，以及key对应的value要增加的值
        Document add = new Document();
        add.put("{your other key1}", 10);
        add.put("{your other key2}", 20);
        // updateFields设置add字段，表示这次更新是一个add操作
        UpdateFields updateFields = new UpdateFields().withAdd(add);
        // 创建更新kv请求
        UpdateKvRequest updateKvRequest = new UpdateKvRequest().withBody(new UpdateKvRequestBody()
                        .withTableName(tableName)
                        .withPrimaryKey(primaryKey)
                        .withUpdateFields(updateFields))
                .withStoreName(storeName);
        // 发送更新kv请求
        try {
            UpdateKvResponse updateKvAddResponse = client.updateKv(updateKvRequest);
            LOGGER.info(updateKvAddResponse.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * 删除一个或多个字段
     *
     * @param client kvs客户端
     */
    public void updateRmvKv(KvsClient client) {
        // 设置要更新doc的主键
        Document primaryKey = new Document();
        primaryKey.put(shardkeyName, "{your shard key value}");
        primaryKey.put(sortkeyName, "{your sort key value}");
        // 将要删除的字段添加到数组中
        List<String> rmv = new ArrayList<>();
        rmv.add("{your other key1}");
        // UpdateField设置rmv字段
        UpdateFields updateFields = new UpdateFields().withRmv(rmv);
        // 创建更新kv请求
        UpdateKvRequest updateKvRequest = new UpdateKvRequest().withBody(new UpdateKvRequestBody()
                        .withPrimaryKey(primaryKey)
                        .withTableName(tableName)
                        .withUpdateFields(updateFields))
                .withStoreName(storeName);
        // 发送更新kv请求
        try {
            UpdateKvResponse updateKvRmvResponse = client.updateKv(updateKvRequest);
            LOGGER.info(updateKvRmvResponse.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * 查询KV
     *
     * @param client kvs客户端
     */
    public void getKv(KvsClient client) {
        // 设置主键
        Document primaryKey = new Document();
        primaryKey.put(shardkeyName, "{your shard key value}");
        primaryKey.put(sortkeyName, "{your sort key value}");
        // 构造查询kv请求
        GetKvRequest getKvRequest = new GetKvRequest().withBody(new GetKvRequestBody().withTableName(tableName).
                        withPrimaryKey(primaryKey))
                .withStoreName(storeName);
        // 发送插入kv请求
        try {
            GetKvResponse getKvResponse = client.getKv(getKvRequest);
            LOGGER.info(getKvResponse.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }
}