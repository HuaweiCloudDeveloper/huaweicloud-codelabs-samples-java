## 1、介绍
华为云提供了KVS服务端SDK，您可以直接集成服务端SDK来调用KVS的相关API，从而实现对KVS服务的快速操作。KVS服务提供完全托管的键值存储及索引服务，主要用于应用的键值类数据（如：元数据、描述数据、管理参数、状态数据）的存储。接下来将介绍如何通过java版SDK实现添加或覆盖一个或多个字段，对多个字段做加法运算，以及删除一个或多个字段。

## 2、前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云租户账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 3、endpoint 华为云各服务应用区域和各服务的终端节点，KVS的endpoint需要购买VPC节点，创建内网域名获取，参考[VPC终端节点对接KVS](https://support.huaweicloud.com/usermanual-kvs/kvs_01_0110.html) 。
- 4、华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。
- 5、用户需要在镜像服务创建镜像。

## 3、SDK获取和安装
您可以通过如下方式获取和安装 SDK： 通过 Maven 安装依赖（推荐） 通过 Maven 安装项目依赖是使用 Java SDK 的推荐方法，首先您需要在您的操作系统中下载并安装 Maven ，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
本示例使用KVS SDK：
```xml
<dependencies>
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-kvs</artifactId>
        <version>${version}</version>
    </dependency>
</dependencies>
```

## 4、代码示例
以下代码展示如何使用更新KV相关SDK
### 4.1 导入依赖模块

```java
//用户身份认证
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
//异常类
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
//Kvs接口类
import com.huaweicloud.sdk.kvs.v1.KvsClient;
import com.huaweicloud.sdk.kvs.v1.model.*;
//日志类
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
```

### 4.2 初始化认证信息
```java
String ak = System.getenv("HUAWEICLOUD_SDK_AK");
String sk = System.getenv("HUAWEICLOUD_SDK_SK");
// 创建认证
BasicCredentials credentials = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);
```   
相关参数说明如下所示：
- HUAWEICLOUD_SDK_AK：华为云账号Access Key。
- HUAWEICLOUD_SDK_SK：华为云账号Secret Access Key 。

### 4.3 初始化KVS服务的客户端
```java
// 配置客户端属性
HttpConfig config = HttpConfig.getDefaultHttpConfig();
config.withIgnoreSSLVerification(true);
// 创建KvsClient实例并初始化
String regionId = "${YOUR_REGION_ID}";
String endpoint = "${KVS_ENDPOINT}";
Region region = new Region(regionId, endpoint);
KvsClient kvsClient = KvsClient.newBuilder()
        .withCredential(credentials)
        .withRegion(region)
        .withHttpConfig(config)
        .build();
```
相关参数说明如下所示：
- credentials： 用户认证信息，见4.2的初始化认证信息。
- regionId：KVS集群所在区域Id。
- endpoint: KVS服务域名。
- config: 客户端属性。

### 4.4 发送请求
构造请求，然后通过4.3初始化好的客户端发送请求。
#### 4.4.1 创建表
发送创建表请求，向指定仓中创建表，如果仓不存在，将会在自动创建仓。
```java
/**
 * 创建表
 *
 * @param client kvs客户端
 */
public void createTable(KvsClient client) {
    // 设置分区键的名称和排序（必选）
    List<Field> shardKeyFields = new ArrayList<>();
    shardKeyFields.add(new Field().withName("{your shard key name}").withOrder(true));
    // 设置排序键的名称和排序（非必选）
    List<Field> sortKeyFields = new ArrayList<>();
    sortKeyFields.add(new Field().withName("{your sort key name}").withOrder(true));
    // 设置主键
    PrimaryKeySchema primaryKeySchema = new PrimaryKeySchema().
            withSortKeyFields(sortKeyFields).
            withShardKeyFields(shardKeyFields);
    // 构造创表请求，指定存储仓和表名
    CreateTableRequest createTableRequest = new CreateTableRequest().withBody(new CreateTableRequestBody()
                    .withTableName("{your table name}")
                    .withPrimaryKeySchema(primaryKeySchema))
            .withStoreName("{your store name}");
    try {
        // 发送创表请求
        CreateTableResponse createTableResponse = client.createTable(createTableRequest);
        LOGGER.info(createTableResponse.toString());
    } catch (ClientRequestException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.toString());
    } catch (ServerResponseException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.getMessage());
    }
}
```

#### 4.4.2 插入KV
为了后续步骤演示更新操作，先在表中插入kv数据。
```java
/**
 * 插入KV
 *
 * @param client kvs客户端
 */
public void putKv(KvsClient client) {
    Document kvDoc = new Document();
    // 设置主键（必须）
    kvDoc.put("{your shard key name}", "{your shard key value}");
    kvDoc.put("{your shard key name}", "{your sort key value}");
    // 插入其他kv数据
    kvDoc.put("{your other key1}", 1);
    kvDoc.put("{your other key2}", 2);
    // 构造插入kv请求
    PutKvRequest putKvRequest = new PutKvRequest().withBody(new PutKvRequestBody().withKvDoc(kvDoc).
                    withTableName("{your table name}")).
            withStoreName("{your store name}");
    try {
        // 发送插入kv请求
        PutKvResponse putKvResponse = client.putKv(putKvRequest);
        LOGGER.info(putKvResponse.toString());
    } catch (ClientRequestException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.toString());
    } catch (ServerResponseException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.getMessage());
    }
}
```

#### 4.4.3 新增或覆盖一个或多个字段
发送更新kv请求，向4.4.2插入的doc中新增一个或多个字段；如果字段已经存在，则覆盖原来的字段。
```java
/**
 * 新增或覆盖一个或多个字段
 *
 * @param client kvs客户端
 */
public void updateSetKv(KvsClient client) {
    // 设置要更新kv的主键（必选）
    Document primaryKey = new Document();
    primaryKey.put("{your shard key name}", "{your shard key value}");
    primaryKey.put("{your sort key name}", "{your sort key value}");
    // 设置新增的字段
    Document set = new Document();
    set.put("{your new key1", "{your new value1}");
    set.put("your new key2", "{your new value2}");
    // updateFields设置set字段，表示这次更新是一个set操作
    UpdateFields updateFields = new UpdateFields().withSet(set);
    // 构造更新kv请求
    UpdateKvRequest updateKvRequest = new UpdateKvRequest().withBody(new UpdateKvRequestBody()
                    .withPrimaryKey(primaryKey)
                    .withUpdateFields(updateFields)
                    .withTableName("{your table name}"))
            .withStoreName("{your store name}");
    try {
        // 发送更新kv请求
        UpdateKvResponse updateKvSetResponse = client.updateKv(updateKvRequest);
        LOGGER.info(updateKvSetResponse.toString());
    } catch (ClientRequestException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.toString());
    } catch (ServerResponseException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.getMessage());
    }
}
```

#### 4.4.4 对一个或多个字段做加法运算
发送更新kv请求，对4.4.2插入的多个字段的value做加法运算。
```java
/**
 * 对一个或多个字段做加法运算
 *
 * @param client kvs客户端
 */
public void updateAddKv(KvsClient client) {
    // 设置要更新kv的主键（必选）
    Document primaryKey = new Document();
    primaryKey.put("{your shard key name}", "{your shard key value}");
    primaryKey.put("{your sort key name}", "{your sort key value}");
    // 指定key，以及对应value增加的值
    Document add = new Document();
    add.put("{your other key1}", 10);
    add.put("{your other key2}", 20);
    // updateFields设置add字段，表示这次更新是一个add操作
    UpdateFields updateFields = new UpdateFields().withAdd(add);
    // 构造更新kv请求
    UpdateKvRequest updateKvRequest = new UpdateKvRequest().withBody(new UpdateKvRequestBody()
                .withTableName(tableName)
                .withPrimaryKey(primaryKey)
                .withUpdateFields(updateFields))
            .withStoreName(storeName);
    try {
        // 发送更新kv请求
        UpdateKvResponse updateKvAddResponse = client.updateKv(updateKvRequest);
        LOGGER.info(updateKvAddResponse.toString());
    } catch (ClientRequestException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.toString());
    } catch (ServerResponseException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.getMessage());
    }
}
```

#### 4.4.5 删除一个或多个字段
发送更新kv请求，将4.4.2上传一个字段删除。
```java
/**
 * 删除一个或多个字段
 *
 * @param client kvs客户端
 */
public void updateRmvKv(KvsClient client) {
    // 设置要更新kv的主键（必选）
    Document primaryKey = new Document();
    primaryKey.put("{your shard key name}", "{your shard key value}");
    primaryKey.put("{your sort key name}", "{your sort key value}");
    // 将要删除的字段添加到数组中（必选）
    List<String> rmv = new ArrayList<>();
    rmv.add("{your other key1}");
    // UpdateField设置rmv字段，表示这次更新是一个rmv操作
    UpdateFields updateFields = new UpdateFields().withRmv(rmv);
    // 创建更新kv请求
    UpdateKvRequest updateKvRequest = new UpdateKvRequest().withBody(new UpdateKvRequestBody()
                    .withPrimaryKey(primaryKey)
                    .withTableName("your table name")
                    .withUpdateFields(updateFields))
            .withStoreName("{your store name}");
    try {
        // 发送更新kv请求
        UpdateKvResponse updateKvRmvResponse = client.updateKv(updateKvRequest);
        LOGGER.info(updateKvRmvResponse.toString());
    } catch (ClientRequestException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.toString());
    } catch (ServerResponseException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.getMessage());
    }
}
```

#### 4.4.6 查询KV
发送查询kv请求，检查4.4.3到4.4.5的更新结果。
```java
/**
 * 查询KV
 *
 * @param client kvs客户端
 */
public void getKv(KvsClient client) {
    // 设置主键（必选）
    Document primaryKey = new Document();
    primaryKey.put(shardkeyName, "{your shard key value}");
    primaryKey.put(sortkeyName, "{your sort key value}");
    // 构造查询kv请求
    GetKvRequest getKvRequest = new GetKvRequest().withBody(new GetKvRequestBody().
                    withTableName("{your table name}").
                    withPrimaryKey(primaryKey))
            .withStoreName("{your store name}");
    try {
        // 发送插入kv请求
        GetKvResponse getKvResponse = client.getKv(getKvRequest);
        LOGGER.info(getKvResponse.toString());
    } catch (ClientRequestException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.toString());
    } catch (ServerResponseException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.getMessage());
    }
}
```


## 5、修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-04-11 | 1.0 | 文档首次发布 |