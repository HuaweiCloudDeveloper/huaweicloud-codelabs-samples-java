## 1、介绍
华为云提供了KVS服务端SDK，您可以直接集成服务端SDK来调用KVS的相关API，从而实现对KVS服务的快速操作。KVS服务提供完全托管的键值存储及索引服务，主要用于应用的键值类数据（如：元数据、描述数据、管理参数、状态数据）的存储。接下来将介绍如何通过java版SDK查询表的属性信息，以及列举仓中表。

## 2、前置条件
- 1、获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。
- 2、您需要拥有华为云租户账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 3、endpoint 华为云各服务应用区域和各服务的终端节点，KVS的endpoint需要购买VPC节点，创建内网域名获取，参考[VPC终端节点对接KVS](https://support.huaweicloud.com/usermanual-kvs/kvs_01_0110.html) 。
- 4、华为云 Java SDK 支持 **Java JDK 1.8** 及其以上版本。
- 5、用户需要在镜像服务创建镜像。

## 3、SDK获取和安装
您可以通过如下方式获取和安装 SDK： 通过 Maven 安装依赖（推荐） 通过 Maven 安装项目依赖是使用 Java SDK 的推荐方法，首先您需要在您的操作系统中下载并安装 Maven ，安装完成后您只需在 Java 项目的 pom.xml 文件加入相应的依赖项即可。
本示例使用KVS SDK：
```xml
<dependencies>
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-kvs</artifactId>
        <version>${version}</version>
    </dependency>
</dependencies>
```

## 4、代码示例
以下代码展示如何使用查询、列举表相关SDK
### 4.1 导入依赖模块

```java
//用户身份认证
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
//异常类
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
//Kvs接口类
import com.huaweicloud.sdk.kvs.v1.KvsClient;
import com.huaweicloud.sdk.kvs.v1.model.*;
//日志类
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
```

### 4.2 初始化认证信息
```java
String ak = System.getenv("HUAWEICLOUD_SDK_AK");
String sk = System.getenv("HUAWEICLOUD_SDK_SK");
// 创建认证
BasicCredentials credentials = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);
```   
相关参数说明如下所示：
- HUAWEICLOUD_SDK_AK：华为云账号Access Key。
- HUAWEICLOUD_SDK_SK：华为云账号Secret Access Key 。

### 4.3 初始化KVS服务的客户端
```java
// 配置客户端属性
HttpConfig config = HttpConfig.getDefaultHttpConfig();
config.withIgnoreSSLVerification(true);
// 创建KvsClient实例并初始化
String regionId = "${YOUR_REGION_ID}";
String endpoint = "${KVS_ENDPOINT}";
Region region = new Region(regionId, endpoint);
KvsClient kvsClient = KvsClient.newBuilder()
        .withCredential(credentials)
        .withRegion(region)
        .withHttpConfig(config)
        .build();
```
相关参数说明如下所示：
- credentials： 用户认证信息，见4.2的初始化认证信息。
- regionId：KVS集群所在区域Id。
- endpoint: KVS服务域名。
- config: 客户端属性。

### 4.4 发送请求
构造请求，然后通过4.3初始化好的客户端发送请求。
#### 4.4.1 创建表
发送创建表请求，向指定仓中创建表，如果仓不存在，将会在自动创建仓。
```java
/**
 * 创建表
 *
 * @param client kvs客户端
 */
public void createTable(KvsClient client) {
    // 设置分区键的名称和排序（必选）
    List<Field> shardKeyFields = new ArrayList<>();
    shardKeyFields.add(new Field().withName("{your shard key name}").withOrder(true));
    // 设置排序键的名称和排序（非必选）
    List<Field> sortKeyFields = new ArrayList<>();
    sortKeyFields.add(new Field().withName("{your sort key name}").withOrder(true));
    // 设置主键
    PrimaryKeySchema primaryKeySchema = new PrimaryKeySchema().
            withSortKeyFields(sortKeyFields).
            withShardKeyFields(shardKeyFields);
    // 构造创表请求，指定存储仓名和表名（必选）
    CreateTableRequest createTableRequest = new CreateTableRequest().withBody(new CreateTableRequestBody()
                    .withTableName("{your table name}")
                    .withPrimaryKeySchema(primaryKeySchema))
            .withStoreName("{your store name}");
    try {
        // 发送创表请求
        CreateTableResponse createTableResponse = client.createTable(createTableRequest);
        LOGGER.info(createTableResponse.toString());
    } catch (ClientRequestException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.toString());
    } catch (ServerResponseException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.getMessage());
    }
}
```

#### 4.4.2 查询表
发送查询表请求，查询4.4.1创建的表的属性信息。
```java
/**
 * 查询表
 *
 * @param client kvs客户端
 */
public void describeTable(KvsClient client) {
    // 构造查询表请求，指定查询的仓名和表名。（必选）
    DescribeTableRequest describeTableRequest = new DescribeTableRequest().withBody(new DescribeTableRequestBody().
                    withTableName("{your table name}")).
            withStoreName("{your store name}");
    try {
        // 发送查询表请求
        DescribeTableResponse describeTableResponse = client.describeTable(describeTableRequest);
        LOGGER.info(describeTableResponse.toString());
    } catch (ClientRequestException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.toString());
    } catch (ServerResponseException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.getMessage());
    }
}
```

#### 4.4.3 列举表
发送列举表请求，列举指定仓中的表的表名。
```java
/**
 * 列举表
 *
 * @param client kvs客户端
 */
public void listTable(KvsClient client) {
    // 构造列举表请求，从指定cursor位置列举表，并指定返回的表名的数量（非必选）
    ListTableRequest listTableRequest = new ListTableRequest().withBody(new ListTableRequestBody().
                    withCursorName("{your cursor table name}").
                    withLimit(1)).
            withStoreName("{your store name}");
    try {
        // 发送列举表请求
        ListTableResponse listTableResponse = client.listTable(listTableRequest);
        LOGGER.info(listTableResponse.toString());
    } catch (ClientRequestException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.toString());
    } catch (ServerResponseException e) {
        LOGGER.error(String.valueOf(e.getHttpStatusCode()));
        LOGGER.error(e.getMessage());
    }
}
```


## 5、修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2024-04-11 | 1.0 | 文档首次发布 |