package com.huawei.kvs;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import com.huaweicloud.sdk.kvs.v1.KvsClient;
import com.huaweicloud.sdk.kvs.v1.model.*;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DescribeTableDemo {
    static String storeName = "{prefix}-{region-id}-{domain-id}";
    static String tableName = "{your table name}";
    static String shardkeyName = "{your shard key}";
    static String sortkeyName = "{your sort key}";
    static String regionId = "{your regionId string}";
    static String endpoint = "{your endpoint string}";
    private static final Logger LOGGER = LoggerFactory.getLogger(DescribeTableDemo.class);

    public static void main(String[] args) {
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        // 创建认证
        BasicCredentials credentials = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        // 配置客户端属性
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        // 创建KvsClient实例并初始化
        Region region = new Region(regionId, endpoint);
        KvsClient kvsClient = KvsClient.newBuilder()
                .withCredential(credentials)
                .withRegion(region)
                .withHttpConfig(config)
                .build();
        try {
            DescribeTableDemo describeTableDemo = new DescribeTableDemo();
            // 创建表
            describeTableDemo.createTable(kvsClient);
            // 查询表
            describeTableDemo.describeTable(kvsClient);
            // 列举表
            describeTableDemo.listTable(kvsClient);
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * 创建表
     *
     * @param client kvs客户端
     */
    public void createTable(KvsClient client) {
        // 设置分区键的名称和排序（必选）
        List<Field> shardKeyFields = new ArrayList<>();
        shardKeyFields.add(new Field().withName(shardkeyName).withOrder(true));
        // 设置排序键的名称和排序（非必选）
        List<Field> sortKeyFields = new ArrayList<>();
        sortKeyFields.add(new Field().withName(sortkeyName).withOrder(true));
        // 设置主键
        PrimaryKeySchema primaryKeySchema = new PrimaryKeySchema().
                withSortKeyFields(sortKeyFields).
                withShardKeyFields(shardKeyFields);
        // 构造创表请求，指定存储仓和表名
        CreateTableRequest createTableRequest = new CreateTableRequest().withBody(new CreateTableRequestBody()
                        .withTableName(tableName)
                        .withPrimaryKeySchema(primaryKeySchema))
                .withStoreName(storeName);
        // 发送创表请求
        CreateTableResponse createTableResponse = client.createTable(createTableRequest);
        LOGGER.info(createTableResponse.toString());
    }


    /**
     * 查询表
     *
     * @param client kvs客户端
     */
    public void describeTable(KvsClient client) {
        // 设置要查询的表名和所在仓名（必选）
        DescribeTableRequest describeTableRequest = new DescribeTableRequest().withBody(new DescribeTableRequestBody().
                withTableName(tableName)).
                withStoreName(storeName);
        // 发送查询表请求
        DescribeTableResponse describeTableResponse = client.describeTable(describeTableRequest);
        LOGGER.info(describeTableResponse.toString());
    }

    /**
     * 列举表
     *
     * @param client kvs客户端
     */
    public void listTable(KvsClient client) {
        // 指定从cursor位置列举表，并指定返回的表的数量（非必选）
        ListTableRequest listTableRequest = new ListTableRequest().withBody(new ListTableRequestBody().
                withCursorName("{your cursor table name}").
                withLimit(1)).
                withStoreName(storeName);
        // 发送列举表请求
        ListTableResponse listTableResponse = client.listTable(listTableRequest);
        LOGGER.info(listTableResponse.toString());
    }
}