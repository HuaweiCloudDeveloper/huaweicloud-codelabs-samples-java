## 1. 简介
华为云提供了CBR服务端SDK，您可以直接集成服务端SDK来调用CBR的相关API，从而实现对CBR服务的快速操作。该示例展示了如何通过java版SDK对备份进行管理,其中包括增（创建存储库，创建策略），删（删除存储库，删除策略），查（查询存储局），和存储库绑定策略。

## 2. 开发前准备
- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 已获取CBR服务对应区域的项目ID，请在华为云控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见 [API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html) 。

## 3. 安装SDK
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-core”和“huaweicloud-sdk-cbr”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
```xml
<dependencies>
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-core</artifactId>
        <version>3.1.56</version>
    </dependency>
    <dependency>
        <groupId>com.huaweicloud.sdk</groupId>
        <artifactId>huaweicloud-sdk-cbr</artifactId>
        <version>3.1.56</version>
    </dependency>
</dependencies>
```

## 4. 开始使用
### 4.1 导入依赖模块
``` java
import com.huaweicloud.sdk.cbr.v1.CbrClient;
import com.huaweicloud.sdk.cbr.v1.model.BillingCreate;
import com.huaweicloud.sdk.cbr.v1.model.BillingCreate.ConsistentLevelEnum;
import com.huaweicloud.sdk.cbr.v1.model.BillingCreate.ObjectTypeEnum;
import com.huaweicloud.sdk.cbr.v1.model.BillingCreate.ProtectTypeEnum;
import com.huaweicloud.sdk.cbr.v1.model.CreateVaultRequest;
import com.huaweicloud.sdk.cbr.v1.model.VaultCreate;
import com.huaweicloud.sdk.cbr.v1.model.VaultCreateReq;
import com.huaweicloud.sdk.cbr.v1.model.CreatePolicyRequest;
import com.huaweicloud.sdk.cbr.v1.model.CreateVaultResponse;
import com.huaweicloud.sdk.cbr.v1.model.VaultAssociate;
import com.huaweicloud.sdk.cbr.v1.model.AssociateVaultPolicyRequest;
import com.huaweicloud.sdk.cbr.v1.model.PolicyTriggerPropertiesReq;
import com.huaweicloud.sdk.cbr.v1.model.PolicyTriggerReq;
import com.huaweicloud.sdk.cbr.v1.model.PolicyoODCreate;
import com.huaweicloud.sdk.cbr.v1.model.ShowVaultResponse;
import com.huaweicloud.sdk.cbr.v1.model.DeleteVaultResponse;
import com.huaweicloud.sdk.cbr.v1.model.DeleteVaultRequest;
import com.huaweicloud.sdk.cbr.v1.model.PolicyCreate;
import com.huaweicloud.sdk.cbr.v1.model.DeletePolicyRequest;
import com.huaweicloud.sdk.cbr.v1.model.PolicyCreateReq;
import com.huaweicloud.sdk.cbr.v1.model.ShowVaultRequest;
import com.huaweicloud.sdk.cbr.v1.model.AssociateVaultPolicyResponse;
import com.huaweicloud.sdk.cbr.v1.model.CreatePolicyResponse;
import com.huaweicloud.sdk.cbr.v1.model.DeletePolicyResponse;
import com.huaweicloud.sdk.cbr.v1.region.CbrRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

```

### 4.2 初始化认证信息
``` java
// 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
// 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
String ak = System.getenv("HUAWEICLOUD_SDK_AK");
String sk = System.getenv("HUAWEICLOUD_SDK_SK");
String projectId = "${project_id}";

//初始化认证信息
BasicCredentials credentials = new BasicCredentials()
        .withAk(ak)
        .withSk(sk)
        .withProjectId(projectId);
```
相关参数说明如下所示：
- HUAWEICLOUD_SDK_AK ：华为云账号Access Key。
- HUAWEICLOUD_SDK_SK ：华为云账号Secret Access Key。
- ${project_id}：华为云账号在目标region的 Project ID。

### 4.3 初始化CBR服务的客户端
4.3.1 使用Endpoint创建CBR服务客户端
``` java
HttpConfig config = HttpConfig.getDefaultHttpConfig();
config.withIgnoreSSLVerification(true);
cbrClient cbrClient = cbrClient.newBuilder()
    .withCredential(credentials)
    .withEndpoint("${cbr_endpoint}")
    .withHttpConfig(config)
    .build();
```
相关参数说明如下所示：

Credential： 用户认证信息，见4.2的初始化认证信息

Endpoint: CBR服务域名，当前CBR服务支持的region信息见 [地区和终端节点信息](https://developer.huaweicloud.com/endpoint?CBR)

HttpConfig: 客户端属性

4.3.2 使用SDK中的region信息窗
``` java
// 设置您项目所在的region信息
Region region = CbrRegion.CN_NORTH_4;

HttpConfig config = HttpConfig.getDefaultHttpConfig();
config.withIgnoreSSLVerification(true);

//初始化客户端
CbrClient cbrClient = CbrClient.newBuilder()
        .withCredential(credentials)
        .withRegion(region)
        .withHttpConfig(config)
        .build();
```
相关参数说明如下所示：
Region：客户端使用的region信息，CBR服务SDK内置支持Region详情请查看SDK中：[com.huaweicloud.sdk.cbr.v1.region.CbrRegion](https://github.com/huaweicloud/huaweicloud-sdk-java-v3/blob/master/services/cbr/src/main/java/com/huaweicloud/sdk/cbr/v1/region/CbrRegion.java) 

### 4.4 配置请求参数
以创建存储库为例：
``` java
private static CreateVaultRequest createVaultRequest() {
        BillingCreate billingCreate = new BillingCreate()
                .withConsistentLevel(ConsistentLevelEnum.CRASH_CONSISTENT)
                .withObjectType(ObjectTypeEnum.SERVER)
                .withProtectType(ProtectTypeEnum.BACKUP)
                .withSize(40);
        List<ResourceCreate> resourceCreateList = new LinkedList<>();
        VaultCreate vaultCreate = new VaultCreate().withName("demoVault")
                .withName("demoVault")
                .withBilling(billingCreate)
                .withResources(resourceCreateList);
        VaultCreateReq vaultCreateReq = new VaultCreateReq()
                .withVault(vaultCreate);
        return new CreateVaultRequest()
                .withBody(vaultCreateReq);
    }
```
相关demo见 5. SDK demo代码解析

### 4.5 发送请求
以创建存储库为例：
``` java
CreateVaultResponse response = cbrClient.createVault(request);
```

## 5. SDK demo代码解析
### 5.1 创建存储库接口
demo请见PolicyVaultDemo.createVault()方法

5.1.1 请求参数说明
``` java
    /**
     * 构建创建按需服务器备份存储库参数
     * @return 创建的存储库对对象
     */
    private static CreateVaultRequest createVaultRequest() {
        BillingCreate billingCreate = new BillingCreate()
                .withConsistentLevel(ConsistentLevelEnum.CRASH_CONSISTENT)
                .withObjectType(ObjectTypeEnum.SERVER)
                .withProtectType(ProtectTypeEnum.BACKUP)
                .withSize(40);
        List<ResourceCreate> resourceCreateList = new LinkedList<>();
        VaultCreate vaultCreate = new VaultCreate().withName("demoVault")
                .withName("demoVault")
                .withBilling(billingCreate)
                .withResources(resourceCreateList);
        VaultCreateReq vaultCreateReq = new VaultCreateReq()
                .withVault(vaultCreate);
        return new CreateVaultRequest()
                .withBody(vaultCreateReq);
    }
```

5.1.2 发送请求
``` java
    /**
     * 创建存储库
     * @param cbrClient CBR 客户端
     */
    public void createVault(CbrClient cbrClient) {
        CreateVaultRequest request = createVaultRequest();
        try {
            CreateVaultResponse response = cbrClient.createVault(request);
            vaultId = response.getVault().getId();
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }
```

5.1.3 接口及参数说明

请见 [创建存储库接口](https://support.huaweicloud.com/api-cbr/CreateVault.html)

### 5.2 创建策略
demo请见PolicyVaultDemo.createPolicy()方法

5.2.1 构造请求参数
``` java
    /**
     * 构造 创建策略 参数
     * @return 创建策略 参数对象
     */
    private static CreatePolicyRequest createPolicyRequest() {
        List<String> pattern = new LinkedList<>();
        pattern.add("FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR,SA,SU;BYHOUR=14;BYMINUTE=00");
        PolicyTriggerPropertiesReq triggerPropertiesReq = new PolicyTriggerPropertiesReq()
                .withPattern(pattern);
        PolicyTriggerReq policyTriggerReq = new PolicyTriggerReq()
                .withProperties(triggerPropertiesReq);
        PolicyoODCreate policyoODCreate = new PolicyoODCreate()
                .withDayBackups(0)
                .withMonthBackups(0)
                .withRetentionDurationDays(1)
                .withTimezone("UTC+08:00")
                .withWeekBackups(0)
                .withYearBackups(0);
        PolicyCreate policyCreate = new PolicyCreate()
                .withEnabled(true)
                .withName("default_policy")
                .withOperationDefinition(policyoODCreate)
                .withOperationType(PolicyCreate.OperationTypeEnum.BACKUP)
                .withTrigger(policyTriggerReq);
        PolicyCreateReq policyCreateReq = new PolicyCreateReq()
                .withPolicy(policyCreate);
        return new CreatePolicyRequest().withBody(policyCreateReq);
    }
```

5.2.2 发送请求
``` java
    /**
     * 创建策略
     * @param cbrClient CBR 客户端
     */
    public void createPolicy(CbrClient cbrClient) {
        CreatePolicyRequest request = createPolicyRequest();
        try {
            CreatePolicyResponse response = cbrClient.createPolicy(request);
            policyId = response.getPolicy().getId();
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }
```

5.2.3 接口及参数说明

请见 [创建策略接口](https://support.huaweicloud.com/api-cbr/CreatePolicy.html)

### 5.3 存储库绑定策略
demo请见PolicyVaultDemo.associateVaultPolicy()方法

5.3.1 构造请求参数
``` java
//创建请求
AssociateVaultPolicyRequest request = new AssociateVaultPolicyRequest();
VaultAssociate body = new VaultAssociate()
        .withPolicyId(policyId);
request.withBody(body)
        .withVaultId(vaultId);
```

5.3.2 发送请求
``` java
AssociateVaultPolicyResponse response = cbrClient.associateVaultPolicy(request);
```

5.3.3 接口及参数说明

请见 [查询设置存储库策略详情接口](https://support.huaweicloud.com/api-cbr/AssociateVaultPolicy.html)


### 5.4 查询指定存储库
demo请见PolicyVaultDemo.getVault()方法

5.4.1 构造请求参数
``` java
//创建请求
ShowVaultRequest request = new ShowVaultRequest().withVaultId(vaultId);
```

5.4.2 发送请求
``` java
ShowVaultResponse response = cbrClient.showVault(request);
```

5.4.3 接口及参数说明

请见 [查询指定存储库接口](https://support.huaweicloud.com/api-cbr/ShowVault.html)


### 5.5 删除存储库
demo请见PolicyVaultDemo.deleteVault()方法

5.5.1 构造请求参数
``` java
//创建请求
DeleteVaultRequest request = new DeleteVaultRequest().withVaultId(vaultId);
```

5.5.2 发送请求
``` java
DeleteVaultResponse response = cbrClient.deleteVault(request);
```

5.5.3 接口及参数说明
请见 [删除存储库接口](https://support.huaweicloud.com/api-cbr/DeleteVault.html)

### 5.6 删除策略
demo请见PolicyVaultDemo.deleteVault()方法

5.6.1 构造请求参数
``` java
//创建请求
DeletePolicyRequest request = new DeletePolicyRequest().withPolicyId(policyId);
```

5.6.2 发送请求
``` java
DeletePolicyResponse response = cbrClient.deletePolicy(request);
```

5.6.3 接口及参数说明
请见 [删除策略接口](https://support.huaweicloud.com/api-cbr/DeleteBackup.html)

## 6. FAQ
暂无

## 7. 参考
更多信息请参考 [CBR服务文档](https://support.huaweicloud.com/cbr/)

## 8. 修订记录

|    发布日期    | 文档版本 |  修订说明  |
|:----------:| :------: |:------:|
|  2023/9/1  | 1.0 | 文档首次发布 |    


