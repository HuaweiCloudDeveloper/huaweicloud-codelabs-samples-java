package com.huawei.cbr;

import com.huaweicloud.sdk.cbr.v1.CbrClient;
import com.huaweicloud.sdk.cbr.v1.model.BillingCreate;
import com.huaweicloud.sdk.cbr.v1.model.BillingCreate.ConsistentLevelEnum;
import com.huaweicloud.sdk.cbr.v1.model.BillingCreate.ObjectTypeEnum;
import com.huaweicloud.sdk.cbr.v1.model.BillingCreate.ProtectTypeEnum;
import com.huaweicloud.sdk.cbr.v1.model.CreateVaultRequest;
import com.huaweicloud.sdk.cbr.v1.model.VaultCreate;
import com.huaweicloud.sdk.cbr.v1.model.VaultCreateReq;
import com.huaweicloud.sdk.cbr.v1.model.CreatePolicyRequest;
import com.huaweicloud.sdk.cbr.v1.model.CreateVaultResponse;
import com.huaweicloud.sdk.cbr.v1.model.VaultAssociate;
import com.huaweicloud.sdk.cbr.v1.model.AssociateVaultPolicyRequest;
import com.huaweicloud.sdk.cbr.v1.model.PolicyTriggerPropertiesReq;
import com.huaweicloud.sdk.cbr.v1.model.PolicyTriggerReq;
import com.huaweicloud.sdk.cbr.v1.model.PolicyoODCreate;
import com.huaweicloud.sdk.cbr.v1.model.ShowVaultResponse;
import com.huaweicloud.sdk.cbr.v1.model.DeleteVaultResponse;
import com.huaweicloud.sdk.cbr.v1.model.DeleteVaultRequest;
import com.huaweicloud.sdk.cbr.v1.model.PolicyCreate;
import com.huaweicloud.sdk.cbr.v1.model.DeletePolicyRequest;
import com.huaweicloud.sdk.cbr.v1.model.PolicyCreateReq;
import com.huaweicloud.sdk.cbr.v1.model.ShowVaultRequest;
import com.huaweicloud.sdk.cbr.v1.model.AssociateVaultPolicyResponse;
import com.huaweicloud.sdk.cbr.v1.model.CreatePolicyResponse;
import com.huaweicloud.sdk.cbr.v1.model.DeletePolicyResponse;
import com.huaweicloud.sdk.cbr.v1.model.ResourceCreate;
import com.huaweicloud.sdk.cbr.v1.region.CbrRegion;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.core.region.Region;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

public class PolicyVaultDemo {

    private String vaultId = null;

    private String policyId = null;

    private static final Logger LOGGER = LoggerFactory.getLogger(PolicyVaultDemo.class);

    /**
     * 构建创建按需服务器备份存储库参数
     *
     * @return 创建的存储库对象
     */
    private static CreateVaultRequest createVaultRequest() {
        BillingCreate billingCreate = new BillingCreate()
            .withConsistentLevel(ConsistentLevelEnum.CRASH_CONSISTENT)
            .withObjectType(ObjectTypeEnum.SERVER)
            .withProtectType(ProtectTypeEnum.BACKUP)
            .withSize(40);
        List<ResourceCreate> resourceCreateList = new LinkedList<>();
        VaultCreate vaultCreate = new VaultCreate()
            .withName("demoVault")
            .withBilling(billingCreate)
            .withResources(resourceCreateList);
        VaultCreateReq vaultCreateReq = new VaultCreateReq()
            .withVault(vaultCreate);
        return new CreateVaultRequest()
            .withBody(vaultCreateReq);
    }

    /**
     * 创建存储库
     *
     * @param cbrClient CBR 客户端
     */
    public void createVault(CbrClient cbrClient) {
        // 创建请求
        CreateVaultRequest request = createVaultRequest();

        try {
            // 发送请求
            CreateVaultResponse response = cbrClient.createVault(request);

            vaultId = response.getVault().getId();
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * 构造 创建策略 参数
     *
     * @return 创建策略 参数对象
     */
    private static CreatePolicyRequest createPolicyRequest() {
        List<String> pattern = new LinkedList<>();
        pattern.add("FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR,SA,SU;BYHOUR=14;BYMINUTE=00");
        PolicyTriggerPropertiesReq triggerPropertiesReq = new PolicyTriggerPropertiesReq()
            .withPattern(pattern);
        PolicyTriggerReq policyTriggerReq = new PolicyTriggerReq()
            .withProperties(triggerPropertiesReq);
        PolicyoODCreate policyoODCreate = new PolicyoODCreate()
            .withDayBackups(0)
            .withMonthBackups(0)
            .withRetentionDurationDays(1)
            .withTimezone("UTC+08:00")
            .withWeekBackups(0)
            .withYearBackups(0);
        PolicyCreate policyCreate = new PolicyCreate()
            .withEnabled(true)
            .withName("default_policy")
            .withOperationDefinition(policyoODCreate)
            .withOperationType(PolicyCreate.OperationTypeEnum.BACKUP)
            .withTrigger(policyTriggerReq);
        PolicyCreateReq policyCreateReq = new PolicyCreateReq()
            .withPolicy(policyCreate);
        return new CreatePolicyRequest().withBody(policyCreateReq);
    }

    /**
     * 创建策略
     *
     * @param cbrClient CBR 客户端
     */
    public void createPolicy(CbrClient cbrClient) {
        // 创建请求
        CreatePolicyRequest request = createPolicyRequest();

        try {
            // 发送请求
            CreatePolicyResponse response = cbrClient.createPolicy(request);

            policyId = response.getPolicy().getId();
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * 给存储库绑定策略
     *
     * @param cbrClient CBR 客户端
     */
    public void associateVaultPolicy(CbrClient cbrClient) {
        // 创建请求
        AssociateVaultPolicyRequest request = new AssociateVaultPolicyRequest();
        VaultAssociate body = new VaultAssociate()
            .withPolicyId(policyId);
        request.withBody(body)
            .withVaultId(vaultId);

        try {
            // 发送请求
            AssociateVaultPolicyResponse response = cbrClient.associateVaultPolicy(request);

            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * 查询单个存储库详情
     *
     * @param cbrClient CBR 客户端对象
     */
    public void getVault(CbrClient cbrClient) {
        //创建请求
        ShowVaultRequest request = new ShowVaultRequest().withVaultId(vaultId);

        try {
            // 发送请求
            ShowVaultResponse response = cbrClient.showVault(request);

            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * 删除策略
     *
     * @param cbrClient CBR 客户端
     */
    public void deletePolicy(CbrClient cbrClient) {
        //创建请求
        DeletePolicyRequest request = new DeletePolicyRequest().withPolicyId(policyId);

        try {
            // 发送请求
            DeletePolicyResponse response = cbrClient.deletePolicy(request);

            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * 删除存储库
     *
     * @param cbrClient CBR 客户端
     */
    public void deleteVault(CbrClient cbrClient) {
        //创建请求
        DeleteVaultRequest request = new DeleteVaultRequest().withVaultId(vaultId);

        try {
            // 发送请求
            DeleteVaultResponse response = cbrClient.deleteVault(request);

            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        String projectId = "${project_id}";
        // 设置您项目所在的region信息
        Region region = CbrRegion.CN_NORTH_4;

        //初始化认证信息
        BasicCredentials credentials = new BasicCredentials()
            .withAk(ak)
            .withSk(sk)
            .withProjectId(projectId);

        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        //初始化客户端
        CbrClient cbrClient = CbrClient.newBuilder()
            .withCredential(credentials)
            .withRegion(region)
            .withHttpConfig(config)
            .build();

        PolicyVaultDemo demo = new PolicyVaultDemo();
        // 创建按需存储库
        demo.createVault(cbrClient);

        // 创建策略
        demo.createPolicy(cbrClient);

        // 给存储库绑定策略
        demo.associateVaultPolicy(cbrClient);

        // 查询存储库
        demo.getVault(cbrClient);

        // 删除存储库
        demo.deleteVault(cbrClient);

        // 删除策略
        demo.deletePolicy(cbrClient);
    }
}
