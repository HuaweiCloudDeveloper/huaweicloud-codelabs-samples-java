package com.huawei.obs;

import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.obs.v1.ObsClient;
import com.huaweicloud.sdk.obs.v1.ObsCredentials;
import com.huaweicloud.sdk.obs.v1.model.ListBucketsRequest;
import com.huaweicloud.sdk.obs.v1.model.ListBucketsResponse;
import com.huaweicloud.sdk.obs.v1.region.ObsRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListBuckets {

    private static final Logger logger = LoggerFactory.getLogger(ListBuckets.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String listBucketsAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String listBucketsSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ObsCredentials credentials = new ObsCredentials()
                .withAk(listBucketsAk)
                .withSk(listBucketsSk);

        // Create a service client and set the corresponding region to CN_NORTH_4.
        ObsClient obsClient = ObsClient.newBuilder()
                .withCredential(credentials)
                .withRegion(ObsRegion.CN_NORTH_4)
                .build();

        // Construct a request.
        ListBucketsRequest request = new ListBucketsRequest();

        try {
            ListBucketsResponse response = obsClient.listBuckets(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}