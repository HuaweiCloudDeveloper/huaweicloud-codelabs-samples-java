### Product Description
1.You can run and debug the interface directly in the [API Explorer](https://console-intl.huaweicloud.com/apiexplorer/#/openapi/OBS/debug?api=DeleteBucketCustomdomain).

2.In this example, you can delete the user-defined domain name of a bucket.

3.OBS uses the DELETE operation to delete the tag of a specified bucket. To perform this operation correctly, ensure that you have the PutBucketcustomdomain permission. By default, only the bucket owner can perform this operation. You can also set a bucket policy or user policy to authorize other users to perform this operation.

### Prerequisites
1.[Registered](https://id5.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyemail.html)HUAWEI CLOUD，and completed [real-name authentication](https://account-intl.huaweicloud.com/usercenter/?region=ap-southeast-3&locale=en-us#/accountindex/realNameAuth)。

2.You have obtained the Huawei Cloud SDK. You can also install the Java SDK.

3.You have obtained the access key ID (AK) and secret access key (SK) of your Huawei Cloud account. 
You can create and view your AK/SK on the My Credentials > Access Keys page of the Huawei Cloud console. 
For details, see [Access Keys](https://support.huaweicloud.com/intl/en-us/devg-apisign/api-sign-provide-aksk.html).

4.You have set up the development environment with Java JDK 1.8 or later.

5.You have enabled OBS, created a bucket, and created a user-defined domain name.

6.You can obtain and install the SDK in Maven mode. You need to download and install Maven in your operating system.
After the installation is complete, you only need to add dependencies to the pom.xml file of the Java project.
Before using the server SDK, you need to install huaweicloud-sdk-obs. For details about the SDK version number, see [SDK Developer Center](https://console-intl.huaweicloud.com/apiexplorer/#/sdkcenter?language=java).
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-obs</artifactId>
    <version>3.1.125</version>
</dependency>
```

### Code example
``` java
package com.huawei.obs;

import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.obs.v1.ObsClient;
import com.huaweicloud.sdk.obs.v1.ObsCredentials;
import com.huaweicloud.sdk.obs.v1.model.DeleteBucketCustomdomainRequest;
import com.huaweicloud.sdk.obs.v1.model.DeleteBucketCustomdomainResponse;
import com.huaweicloud.sdk.obs.v1.region.ObsRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteBucketCustomdomain {

    private static final Logger logger = LoggerFactory.getLogger(DeleteBucketCustomdomain.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String deleteCustomdomainAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String deleteCustomdomainSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ObsCredentials credentials = new ObsCredentials()
                .withAk(deleteCustomdomainAk)
                .withSk(deleteCustomdomainSk);

        // Create a service client and set the corresponding region to CN_NORTH_4.
        ObsClient obsClient = ObsClient.newBuilder()
                .withCredential(credentials)
                .withRegion(ObsRegion.CN_NORTH_4)
                .build();

        // Construct a request and set the bucket name.
        DeleteBucketCustomdomainRequest request = new DeleteBucketCustomdomainRequest();
        // Bucket Name
        request.setBucketName("<YOUR BUCKET NAME>");
        // Indicates the user-defined domain name to be deleted.
        // Type: character string, which must meet the domain name rules.
        request.setCustomdomain("<YOUR CUSTOM DOMAIN>");

        try {
            DeleteBucketCustomdomainResponse response = obsClient.deleteBucketCustomdomain(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}
```

### Example of the returned result
```
{
}
```
### Change History

Released On | Issue | Description

:----------:|:----:| :------:  
2024/12/16 | 1.0 | This document is released for the first time.