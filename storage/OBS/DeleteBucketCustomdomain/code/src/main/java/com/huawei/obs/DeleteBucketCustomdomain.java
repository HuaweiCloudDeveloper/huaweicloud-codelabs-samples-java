package com.huawei.obs;

import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.obs.v1.ObsClient;
import com.huaweicloud.sdk.obs.v1.ObsCredentials;
import com.huaweicloud.sdk.obs.v1.model.DeleteBucketCustomdomainRequest;
import com.huaweicloud.sdk.obs.v1.model.DeleteBucketCustomdomainResponse;
import com.huaweicloud.sdk.obs.v1.region.ObsRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteBucketCustomdomain {

    private static final Logger logger = LoggerFactory.getLogger(DeleteBucketCustomdomain.class.getName());

    public static void main(String[] args) {
        // Initializing Necessary Parameters and Clients
        // Directly writing AK/SK in code is risky. For security, encrypt your AK/SK and store them in the configuration file or environment variables.
        // In this example, the AK/SK are stored in environment variables for identity authentication. Before running this example, set environment variables HUAWEICLOUD_SDK_AK and HUAWEICLOUD_SDK_SK.
        String deleteCustomdomainAk = System.getenv("HUAWEICLOUD_SDK_AK");
        String deleteCustomdomainSk = System.getenv("HUAWEICLOUD_SDK_SK");

        // Configuring Authentication Information
        ObsCredentials credentials = new ObsCredentials()
                .withAk(deleteCustomdomainAk)
                .withSk(deleteCustomdomainSk);

        // Create a service client and set the corresponding region to CN_NORTH_4.
        ObsClient obsClient = ObsClient.newBuilder()
                .withCredential(credentials)
                .withRegion(ObsRegion.CN_NORTH_4)
                .build();

        // Construct a request and set the bucket name.
        DeleteBucketCustomdomainRequest request = new DeleteBucketCustomdomainRequest();
        // Bucket Name
        request.setBucketName("<YOUR BUCKET NAME>");
        // Indicates the user-defined domain name to be deleted.
        // Type: character string, which must meet the domain name rules.
        request.setCustomdomain("<YOUR CUSTOM DOMAIN>");

        try {
            DeleteBucketCustomdomainResponse response = obsClient.deleteBucketCustomdomain(request);
            logger.info(response.toString());
        } catch (ClientRequestException | ServerResponseException e) {
            logger.error(String.valueOf(e.getHttpStatusCode()));
        }
    }
}