/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 */

package com.huawei.sfsturbo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.sfsturbo.v1.SFSTurboClient;
import com.huaweicloud.sdk.sfsturbo.v1.model.CreateShareRequest;
import com.huaweicloud.sdk.sfsturbo.v1.model.CreateShareRequestBody;
import com.huaweicloud.sdk.sfsturbo.v1.model.CreateShareResponse;
import com.huaweicloud.sdk.sfsturbo.v1.model.Share;
import com.huaweicloud.sdk.sfsturbo.v1.model.Metadata;
import com.huaweicloud.sdk.sfsturbo.v1.model.DeleteShareRequest;
import com.huaweicloud.sdk.sfsturbo.v1.model.DeleteShareResponse;
import com.huaweicloud.sdk.sfsturbo.v1.model.ShowShareRequest;
import com.huaweicloud.sdk.sfsturbo.v1.model.ShowShareResponse;
import com.huaweicloud.sdk.sfsturbo.v1.model.ListSharesRequest;
import com.huaweicloud.sdk.sfsturbo.v1.model.ListSharesResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ManageShare {
    private static final Logger LOGGER = LoggerFactory.getLogger(ManageShare.class);
    //创建文件系统
    public void createShare(SFSTurboClient sfsTurboClient) {
        //创建请求
        Metadata metadata = new Metadata()
                .withExpandType("${expand_type}")
                .withHpcBw("${hpc_bw}");
        Share share = new Share()
                .withAvailabilityZone("${availability_zone}")
                .withName("${name}")
                .withSecurityGroupId("${SecurityGroupId}")
                .withShareProto("NFS")
                .withShareType("STANDARD")
                .withSize(3686)
                .withSubnetId("${subnet_id}")
                .withVpcId("${vpc_id}")
                .withMetadata(metadata);
        CreateShareRequest request = new CreateShareRequest()
                .withBody(new CreateShareRequestBody()
                        .withShare(share));

        try {
            CreateShareResponse response = sfsTurboClient.createShare(request);
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }

    //删除文件系统
    public void deleteShare(SFSTurboClient sfsTurboClient) {
        //创建请求
        DeleteShareRequest request = new DeleteShareRequest()
                .withShareId("${share_id}");

        try {
            DeleteShareResponse response = sfsTurboClient.deleteShare(request);
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }

    //查询文件系统详细信息
    public void showShare(SFSTurboClient sfsTurboClient) {
        //创建请求
        ShowShareRequest request = new ShowShareRequest()
                .withShareId("${share_id}");

        try {
            ShowShareResponse response = sfsTurboClient.showShare(request);
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }

    //获取文件系统列表
    public void listShares(SFSTurboClient sfsTurboClient) {
        //创建请求
        Long limit = 10L;
        Long offset = 10L;
        ListSharesRequest request = new ListSharesRequest()
                .withLimit(limit)
                .withOffset(offset);

        try {
            ListSharesResponse response = sfsTurboClient.listShares(request);
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
    }

    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");
        BasicCredentials credentials = new BasicCredentials()
                .withAk(ak)
                .withSk(sk)
                .withProjectId("${project_id}");

        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        //初始化客户端
        SFSTurboClient sfsTurboClient = SFSTurboClient.newBuilder()
                .withCredential(credentials)
                .withEndpoint("${endpoint}")
                .withHttpConfig(config)
                .build();

        ManageShare ms = new ManageShare();
        ms.createShare(sfsTurboClient);
        ms.deleteShare(sfsTurboClient);
        ms.showShare(sfsTurboClient);
        ms.listShares(sfsTurboClient);
    }
}
