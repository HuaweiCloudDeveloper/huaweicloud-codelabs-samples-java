## 1. 示例简介

华为云提供了SFSTurbo服务端SDK，您可以直接集成服务端SDK来调用SFSTurbo服务相关API，从而实现对SFSTurbo服务的快速操作。该示例展示了如何通过Java版本的SDK进行创建SFSTurbo文件系统、删除SFSTurbo文件系统、查询文件系统详细信息、获取文件系统列表。


## 2. 开发前准备
- 要使用华为云 Java SDK ，您需要拥有云账号以及该账号对应的 Access Key（AK）、 Secret Access Key（SK）和项目ID (PROJECT ID)。请在华为云控制台“我的凭证-访问密钥”页面上创建和查看您的 AK&SK，更多信息请查看 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。 请在华为云控制台“我的凭证-API凭证”页面上查看您的项目ID，更多信息请查看 [API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html) 。
- 要使用华为云 Java SDK 访问指定服务的 API ，您需要确认已在 [华为云控制台](https://auth.huaweicloud.com/authui/login?service=https%3A%2F%2Fconsole.huaweicloud.com%2Fconsole%2F%3Flocale%3Dzh-cn%26region%3Dcn-north-4%26cloud_route_state%3D%2Fhome) 开通当前服务。
- 华为云 Java SDK 支持Java JDK 1.8及其以上版本。

## 3. 安装SDK、
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-core”和“huaweicloud-sdk-sfsturbo”，具体的SDK版本号请参见 [SDK开发中心](https://console.huaweicloud.com/apiexplorer/#/sdkcenter?language=java) 。
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-core</artifactId>
    <version>3.0.55</version>
</dependency>
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-sfsturbo</artifactId>
    <version>3.1.56</version>
</dependency>
```

## 4. 开始使用
### 4.1 导入依赖模块

```java
//用户身份认证
import com.huaweicloud.sdk.core.auth.BasicCredentials;
//异常类
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
//SFSTurbo客户端
import com.huaweicloud.sdk.sfsturbo.v1.SFSTurboClient;
//SFSTurbo文件系统管理相关类
import com.huaweicloud.sdk.sfsturbo.v1.model.CreateShareRequest;
import com.huaweicloud.sdk.sfsturbo.v1.model.CreateShareRequestBody;
import com.huaweicloud.sdk.sfsturbo.v1.model.CreateShareResponse;
import com.huaweicloud.sdk.sfsturbo.v1.model.Share;
import com.huaweicloud.sdk.sfsturbo.v1.model.Metadata;
import com.huaweicloud.sdk.sfsturbo.v1.model.DeleteShareRequest;
import com.huaweicloud.sdk.sfsturbo.v1.model.DeleteShareResponse;
import com.huaweicloud.sdk.sfsturbo.v1.model.ShowShareRequest;
import com.huaweicloud.sdk.sfsturbo.v1.model.ShowShareResponse;
import com.huaweicloud.sdk.sfsturbo.v1.model.ListSharesRequest;
import com.huaweicloud.sdk.sfsturbo.v1.model.ListSharesResponse;
//日志打印
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
```


### 4.2 初始化认证信息
```java
// 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
// 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
String ak = System.getenv("HUAWEICLOUD_SDK_AK");
String sk = System.getenv("HUAWEICLOUD_SDK_SK");
BasicCredentials credentials = new BasicCredentials()
    .withAk(ak)
    .withSk(sk)
    .withProjectId("${project_id}");
```
相关参数说明如下所示：
- HUAWEICLOUD_SDK_AK：华为云账号Access Key。
- HUAWEICLOUD_SDK_SK：华为云账号Secret Access Key 。

### 4.3 初始化SFSTurbo服务的客户端
```java
HttpConfig config = HttpConfig.getDefaultHttpConfig();
config.withIgnoreSSLVerification(true);
//初始化客户端
SFSTurboClient sfsTurboClient = SFSTurboClient.newBuilder()
        .withCredential(credentials)
        .withEndpoint("${endpoint}")
        .withHttpConfig(config)
        .build();
```
相关参数说明如下所示：
credentials： 用户认证信息，见4.2的初始化认证信息
${endpoint}: SFSTurbo服务域名，当前SFSTurbo服务支持的region信息见 [地区和终端节点信息](https://developer.huaweicloud.com/endpoint?SFS)
config: 客户端属性

### 4.4 配置请求参数
#### 4.4.1 以创建文件系统为例：
```java
Metadata metadata = new Metadata()
        .withExpandType("${expand_type}")
        .withHpcBw("${hpc_bw}");
Share share = new Share()
        .withAvailabilityZone("${availability_zone}")
        .withName("${name}")
        .withSecurityGroupId("${security_group_id}")
        .withShareProto("NFS")
        .withShareType("STANDARD")
        .withSize(3686)
        .withSubnetId("${subnet_id}")
        .withVpcId("${vpc_id}")
        .withMetadata(metadata);
CreateShareRequest request = new CreateShareRequest()
        .withBody(new CreateShareRequestBody()
                .withShare(share));
```
相关参数说明如下所示（详情请看 [SFSTurbo帮助文档](https://support.huaweicloud.com/api-sfs/CreateShare.html) ）：
- expand_type：扩展类型。
- hpc_bw：文件系统的带宽规格。
- availability_zone：可用区。
- name：文件系统名字。
- security_group_id： 安全组ID。
- subnet_id： 子网ID。
- vpc_id： VPC ID。
- metadata：创建文件系统的metadata信息，一到多个字典形式组织的键值对组成。

#### 4.4.2 以删除文件系统为例：
```java
DeleteShareRequest request = new DeleteShareRequest()
        .withShareId("${share_id}");
```
相关参数说明如下所示（详情请看 [SFSTurbo帮助文档](https://support.huaweicloud.com/api-sfs/DeleteShare.html)
- ${share_id}: 文件系统ID

#### 4.4.3 以查询文件系统详细信息为例：
```java
ShowShareRequest request = new ShowShareRequest()
        .withShareId("${share_id}");
```
相关参数说明如下所示（详情请看 [SFSTurbo帮助文档](https://support.huaweicloud.com/api-sfs/ShowShare.html)
- ${share_id}: 文件系统ID

#### 4.4.4 以获取文件系统列表为例：
```java
Long limit = 10L;
Long offset = 10L;
ListSharesRequest request = new ListSharesRequest()
        .withLimit(limit)
        .withOffset(offset);
```
相关参数说明如下所示（详情请看 [SFSTurbo帮助文档](https://support.huaweicloud.com/api-sfs/ListShares.html)
- limit：返回的文件系统个数，最大值为200。最小值：0, 最大值：200。
- offset：文件系统查询个数的偏移量。最小值：0。

### 4.5 发送请求
#### 4.5.1 以创建文件系统为例：
```java
CreateShareResponse response = sfsTurboClient.createShare(request);
```

#### 4.5.2 以删除文件系统为例：
```java
DeleteShareResponse response = sfsTurboClient.deleteShare(request);
```

#### 4.5.3 以查询文件系统详细信息为例：
```java
ShowShareResponse response = sfsTurboClient.showShare(request);
```

#### 4.5.4 以获取文件系统列表为例：
```java
ListSharesResponse response = sfsTurboClient.listShares(request);
```

## 5. FAQ
暂无

## 6. 参考

更多信息请参考 [SFSTurbo](https://support.huaweicloud.com/sfs/index.html)

## 7. 修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:| :------: | :----------: |
| 2023-08-28 | 1.0 | 文档首次发布 |