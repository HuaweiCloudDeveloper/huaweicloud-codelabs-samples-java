## 1. 功能介绍
- 客户可以查询指定月份的月度摊销成本。当前仅支持查询近18个月的摊销成本。
- 客户在自建平台查询成本分析数据。

## 2. 前置条件
- 注册经销商合作伙伴账号和实名认证，并且加入经销商计划。参考[注册经销商伙伴](https://www.huaweicloud.com/partners/resale/)
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

## 3. SDK获取和安装
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-bss”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

## 4. 接口参数说明
#### 4.1 查询月度成本

接口详情请参考[查询月度成本](https://support.huaweicloud.com/api-oce/mbc_00010.html)

#### 4.2 查询成本数据

接口详情请参考[查询成本数据](https://support.huaweicloud.com/api-oce/costm_00014.html)

## 5. 代码示例
```java
package com.huawei.bss;

import com.huaweicloud.sdk.bss.v2.BssClient;
import com.huaweicloud.sdk.bss.v2.model.ListCustomerBillsMonthlyBreakDownRequest;
import com.huaweicloud.sdk.bss.v2.model.ListCostsRequest;
import com.huaweicloud.sdk.bss.v2.model.ListCustomerBillsMonthlyBreakDownResponse;
import com.huaweicloud.sdk.bss.v2.model.ListCostsResponse;
import com.huaweicloud.sdk.bss.v2.model.ListCostsReq;
import com.huaweicloud.sdk.bss.v2.model.TimeCondition;
import com.huaweicloud.sdk.bss.v2.model.GroupBy;
import com.huaweicloud.sdk.bss.v2.region.BssRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import java.util.LinkedList;
import java.util.List;

public class BSSManagingCustomerCostDemo {
    public static void main(String[] args) {

        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        BssClient client = BssClient.newBuilder()
            .withCredential(auth)
            .withRegion(BssRegion.valueOf("cn-north-1"))
            .build();

        // 1.组装查询月度成本请求体示例
        ListCustomerBillsMonthlyBreakDownRequest listCustomerBillsMonthlyBreakDownRequest = buildListCustomerBillsMonthlyBreakDownRequest();

        // 2.组装查询成本数据请求体示例
        ListCostsRequest listCostsRequest = buildListCostsRequest();

        try {
            ListCustomerBillsMonthlyBreakDownResponse listCustomerBillsMonthlyBreakDownResponse =
                client.listCustomerBillsMonthlyBreakDown(listCustomerBillsMonthlyBreakDownRequest);
            System.out.println("查询月度成本响应" + listCustomerBillsMonthlyBreakDownResponse.toString());

            ListCostsResponse listCostsResponse = client.listCosts(listCostsRequest);
            System.out.println("查询成本数据响应" + listCostsResponse.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    /**
     * 组装查询月度成本请求体示例
     *
     * @return ListCustomerBillsMonthlyBreakDownRequest
     */
    private static ListCustomerBillsMonthlyBreakDownRequest buildListCustomerBillsMonthlyBreakDownRequest() {
        ListCustomerBillsMonthlyBreakDownRequest request = new ListCustomerBillsMonthlyBreakDownRequest();
        request.setXLanguage("<LANGUAGE>");
        // 格式：YYYY-MM
        request.setSharedMonth("<MONTH>");
        request.setChargingMode(1);
        request.setServiceTypeCode("<SERVICE_TYPE_CODE>");
        request.setResourceTypeCode("<RESOURCE_TYPE_CODE>");
        request.setRegionCode("cn-north-1");
        request.setBillType(null);
        request.setOffset(0);
        request.setLimit(10);
        request.setResourceId("<RESOURCE_ID>");
        request.setResourceName("<RESOURCE_NAME>");
        request.setEnterpriseProjectId("<ENTERPRISE_PROJECT_ID>");
        request.setMethod("all");
        // 如果method取值为sub_customer，则该参数不能为空。
        request.setSubCustomerId("");
        return request;
    }

    /**
     * 组装查询成本数据请求体
     *
     * @return ListCostsRequest
     */
    private static ListCostsRequest buildListCostsRequest() {
        ListCostsRequest request = new ListCostsRequest();
        request.setXLanguage("<LANGUAGE>");
        ListCostsReq listCostsReq = new ListCostsReq();
        TimeCondition timeCondition = new TimeCondition();
        // 1：天 2：月
        timeCondition.setTimeMeasureId(1);
        timeCondition.setBeginTime("<BEGIN_TIME>");
        timeCondition.setEndTime("<END_TIME>");
        listCostsReq.setTimeCondition(timeCondition);
        List<GroupBy> groupByList = new LinkedList<>();
        GroupBy groupby = new GroupBy();
        groupby.setKey("GROUPBY_KEY");
        groupby.setType("GROUPBY_VALUE");
        groupByList.add(groupby);
        listCostsReq.setGroupby(groupByList);
        listCostsReq.setCostType("<COST_TYPE>");
        listCostsReq.setAmountType("<AMOUNT_TYPE>");
        listCostsReq.setFilters(null);
        listCostsReq.setOffset(0);
        listCostsReq.setLimit(10);
        request.setBody(listCostsReq);
        return request;
    }

}
```

## 修订记录
| 发布日期 | 文档版本 | 修订说明 |
| :----:| :----:| :----:|
|2024-02-29|1.0.0|管理成本（客户）场景示例第一个版本发布|