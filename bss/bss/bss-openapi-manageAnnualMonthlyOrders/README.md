## 1. 功能介绍
客户购买包年/包月资源后，在伙伴销售平台可以：

- 查看待审核、处理中、已取消、已完成和待支付状态等状态的包年/包月订单。
- 查询待支付订单可用的优惠券或可用折扣，支付或取消待支付状态的订单，退订已订购的包年/包月产品。
- 查询某次退订订单或者降配订单的退款金额来自哪些资源和对应订单。
 
## 2. 前置条件
- 注册经销商合作伙伴账号和实名认证，并且加入经销商计划。参考[注册经销商伙伴](https://www.huaweicloud.com/partners/resale/)
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
 
## 3. SDK获取和安装
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
 
使用服务端SDK前，您需要安装“huaweicloud-sdk-bss”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

## 4. 调用流程
 ![图1: 订单图例1](./assets/orderLib001.png "订单图例1")

## 5. 接口参数说明
#### 5.1 查询订单列表
客户购买包年/包月资源后，可以查看待审核、处理中、已取消、已完成和待支付等状态的订单。

接口参数的详细说明可参见：[查询订单列表](https://support.huaweicloud.com/api-bpconsole/api_order_00013.html)

当入参status=6查询订单列表时，可查询出待支付订单的列表，获得订单编号(order_id)，可作为查询订单可用优惠券/折扣接口的入参，也可以作为支付订单/取消待支付订单接口的入参。

当入参order_type=4查询订单列表时，可查询出退订订单的列表，获得订单编号(order_id)，可作为查询退款订单的金额详情接口的入参。

#### 5.2 查询订单详情
客户可以在伙伴销售平台查看订单详情。

接口参数的详细说明可参见：[查询订单详情](https://support.huaweicloud.com/api-bpconsole/api_order_00014.html)

查询订单列表时系统会返回的订单ID可作为接口的入参。

#### 5.3 查询订单可用优惠券
客户在伙伴销售平台支付待支付订单时，查询可使用的优惠券列表。

调用查询订单可使用优惠券接口时，可获得优惠券id(coupon_id)和优惠卷类型type(coupon_type)。支付订单时，输入该参数的值，即可使用优惠券。

接口参数的详细说明可参见：[查询订单可用优惠券](https://support.huaweicloud.com/api-bpconsole/api_order_00015.html)

#### 5.4 查询订单可用折扣
客户在伙伴销售平台支付待支付订单时，查询可使用的折扣信息。

调用查询订单可使用折扣接口时，可获得订单的可用折扣id(discount_id)和折扣类型type(discount_type)。支付订单时，输入该参数的值，即可使用折扣。

接口参数的详细说明可参见：[查询订单可用折扣](https://support.huaweicloud.com/api-oce/api_order_00025.html)

#### 5.5 支付订单
客户可以对待支付状态的包年/包月产品订单进行支付。

接口参数的详细说明可参见：[支付包年/包月产品订单](https://support.huaweicloud.com/api-bpconsole/api_order_00030.html)

说明：
- 余额支付包括现金账户和信用账户两种支付方式，如果两个账户都有余额，则优先现金账户支付。
- 同时使用订单折扣和优惠券的互斥规则如下：
    如果优惠券的限制属性上存在simultaneousUseWithEmpowerDiscount字段，并且值为0，则优惠券和商务授权折扣及伙伴授予折扣不能同时使用。
    如果优惠券的限制属性上存在minConsumeDiscount字段，当折扣ID包含的所有订单项上的折扣率discount_ratio都小于minConsumeDiscount字段时，则折扣ID和优惠券不能同时使用。
    如果优惠券的限制属性上存在simultaneousUseWithPromotionDiscount字段，并且值为0，则优惠券和促销折扣不能同时使用。
- 财务托管企业子调用该接口时，若企业主帐号存在订单可用的优惠券，则支付订单时会自动使用，无需在请求参数中携带优惠券ID。

#### 5.6 取消待支付订单
客户可以对待支付的订单进行取消操作。

关于接口参数的详细说明可参见：[取消待支付订单](https://support.huaweicloud.com/api-bpconsole/api_order_00017.html)

说明：只有订单状态是“待支付”的时候，才能取消订单。

#### 5.7 查询退款订单的金额详情
客户在伙伴销售平台查询某次退订订单或者降配订单的退款金额来自哪些资源和对应订单。

接口参数的详细说明可参见：[查询退款订单的金额详情](https://support.huaweicloud.com/api-bpconsole/api_order_00020.html)

说明：
- 可以在调用完[退订包年/包月资源](https://support.huaweicloud.com/api-bpconsole/api_order_00019.html) 接口生成退订订单ID后，调用该接口查询退订订单对应的金额所属资源和订单。例如，调用“退订包年/包月资源”接口退订资源及其已续费周期后，您可以调用本小节的接口查询到退订金额归属的原开通订单ID和原续费订单ID。
- 2018年5月份之后退订的订单才能查询到归属的原订单ID。

## 6. 代码示例
```java
package com.huawei.bss;

import java.util.Collections;

import com.huaweicloud.sdk.bss.v2.BssClient;
import com.huaweicloud.sdk.bss.v2.model.ListCustomerOrdersRequest;
import com.huaweicloud.sdk.bss.v2.model.ShowCustomerOrderDetailsRequest;
import com.huaweicloud.sdk.bss.v2.model.ListOrderCouponsByOrderIdRequest;
import com.huaweicloud.sdk.bss.v2.model.ListOrderDiscountsRequest;
import com.huaweicloud.sdk.bss.v2.model.PayOrdersRequest;
import com.huaweicloud.sdk.bss.v2.model.CancelCustomerOrderRequest;
import com.huaweicloud.sdk.bss.v2.model.ShowRefundOrderDetailsRequest;
import com.huaweicloud.sdk.bss.v2.model.ListCustomerOrdersResponse;
import com.huaweicloud.sdk.bss.v2.model.ShowCustomerOrderDetailsResponse;
import com.huaweicloud.sdk.bss.v2.model.ListOrderCouponsByOrderIdResponse;
import com.huaweicloud.sdk.bss.v2.model.ListOrderDiscountsResponse;
import com.huaweicloud.sdk.bss.v2.model.PayOrdersResponse;
import com.huaweicloud.sdk.bss.v2.model.CancelCustomerOrderResponse;
import com.huaweicloud.sdk.bss.v2.model.ShowRefundOrderDetailsResponse;
import com.huaweicloud.sdk.bss.v2.model.PayCustomerOrderV3Req;
import com.huaweicloud.sdk.bss.v2.model.CouponSimpleInfoOrderPayV3;
import com.huaweicloud.sdk.bss.v2.model.DiscountSimpleInfoV3;
import com.huaweicloud.sdk.bss.v2.model.CancelCustomerOrderReq;
import com.huaweicloud.sdk.bss.v2.region.BssRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

public class BSSManageAnnualMonthlyOrdersDemo {
    public static void main(String[] args) {

        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        BssClient client = BssClient.newBuilder()
            .withCredential(auth)
            .withRegion(BssRegion.valueOf("cn-north-1"))
            .build();

        // 1 查询订单列表请求体示例
        ListCustomerOrdersRequest requestForListCustomerOrders = buildListCustomerOrdersRequest();
        // 2 查询订单详情请求体示例
        ShowCustomerOrderDetailsRequest requestForShowCustomerOrderDetails = buildShowCustomerOrderDetailsRequest();
        // 3 查询订单可用优惠券请求体示例
        ListOrderCouponsByOrderIdRequest requestForListOrderCoupons = buildListOrderCouponsByOrderIdRequest();
        // 4 查询订单可用折扣请求体示例
        ListOrderDiscountsRequest requestForListOrderDiscounts = buildListOrderDiscountsRequestRequest();
        // 5 支付包年或者包月的产品订单请求体示例
        PayOrdersRequest requestForPayOrders = buildPayOrdersRequest();
        // 6 取消待支付订单请求体示例
        CancelCustomerOrderRequest requestForCancelCustomerOrder = buildCancelCustomerOrderRequest();
        // 7 查询退款订单的金额详情请求体示例
        ShowRefundOrderDetailsRequest requestForShowRefundOrderDetails = buildShowRefundOrderDetailsRequest();

        try {
            ListCustomerOrdersResponse responseForListCustomerOrders = client.listCustomerOrders(requestForListCustomerOrders);
            System.out.println("查询订单列表响应" + responseForListCustomerOrders.toString());
            ShowCustomerOrderDetailsResponse responseForShowCustomerOrderDetails = client.showCustomerOrderDetails(requestForShowCustomerOrderDetails);
            System.out.println("查询订单详情响应" + responseForShowCustomerOrderDetails.toString());
            ListOrderCouponsByOrderIdResponse responseForListOrderCoupons = client.listOrderCouponsByOrderId(requestForListOrderCoupons);
            System.out.println("查询订单可用优惠券响应" + responseForListOrderCoupons.toString());
            ListOrderDiscountsResponse responseForListOrderDiscounts = client.listOrderDiscounts(requestForListOrderDiscounts);
            System.out.println("查询订单可用折扣响应" + responseForListOrderDiscounts.toString());
            PayOrdersResponse responseForPayOrders = client.payOrders(requestForPayOrders);
            System.out.println("支付包年/包月产品订单响应" + responseForPayOrders.toString());
            CancelCustomerOrderResponse responseForCancelCustomerOrder = client.cancelCustomerOrder(requestForCancelCustomerOrder);
            System.out.println("取消待支付订单响应" + responseForCancelCustomerOrder.toString());
            ShowRefundOrderDetailsResponse responseForShowRefundOrderDetails = client.showRefundOrderDetails(requestForShowRefundOrderDetails);
            System.out.println("查询退款订单的金额详情响应" + responseForShowRefundOrderDetails.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    /**
     * 组装查询订单列表请求体示例
     *
     * @return ListCustomerOrdersRequest
     */
    private static ListCustomerOrdersRequest buildListCustomerOrdersRequest() {
        ListCustomerOrdersRequest request = new ListCustomerOrdersRequest();
        // 当入参status=6查询订单列表时，可查询出待支付订单的列表，获得订单编号(order_id)，可作为查询订单可用优惠券/折扣接口的入参，也可以作为支付订单/取消待支付订单接口的入参。
        // 当入参order_type=4查询订单列表时，可查询出退订订单的列表，获得订单编号(order_id)，可作为查询退款订单的金额详情接口的入参。
        request.setStatus(6);
        request.setOrderType("4");
        return request;
    }

    /**
     * 组装查询订单详情请求体示例
     *
     * @return ShowCustomerOrderDetailsRequest
     */
    private static ShowCustomerOrderDetailsRequest buildShowCustomerOrderDetailsRequest() {
        ShowCustomerOrderDetailsRequest request = new ShowCustomerOrderDetailsRequest();
        // 查询订单列表时系统会返回的订单ID可作为接口的入参
        request.setOrderId("<YOUR_ORDER_ID>");
        return request;
    }

    /**
     * 组装查询订单可用优惠券请求体示例
     *
     * @return ListOrderCouponsByOrderIdRequest
     */
    private static ListOrderCouponsByOrderIdRequest buildListOrderCouponsByOrderIdRequest() {
        ListOrderCouponsByOrderIdRequest request = new ListOrderCouponsByOrderIdRequest();
        request.setOrderId("<YOUR_ORDER_ID>");
        return request;
    }

    /**
     * 组装查询订单可用折扣请求体示例
     *
     * @return ListOrderDiscountsRequest
     */
    private static ListOrderDiscountsRequest buildListOrderDiscountsRequestRequest() {
        ListOrderDiscountsRequest request = new ListOrderDiscountsRequest();
        request.setOrderId("<YOUR_ORDER_ID>");
        return request;
    }

    /**
     * 组装支付包年或者包月的产品订单请求体示例
     *
     * @return PayOrdersRequest
     */
    private static PayOrdersRequest buildPayOrdersRequest() {
        PayOrdersRequest request = new PayOrdersRequest();
        PayCustomerOrderV3Req payCustomerOrderReq = new PayCustomerOrderV3Req();
        // 是否使用优惠卷YES/NO
        payCustomerOrderReq.setUseCoupon("YES");
        CouponSimpleInfoOrderPayV3 couponSimpleInfo = new CouponSimpleInfoOrderPayV3();
        couponSimpleInfo.setId("<YOUR_COUPON_ID>");
        couponSimpleInfo.setType(301);
        payCustomerOrderReq.setCouponInfos(Collections.singletonList(couponSimpleInfo));
        // 是否使用折扣YES/NO
        payCustomerOrderReq.setUseDiscount("YES");
        DiscountSimpleInfoV3 discountSimpleInfo = new DiscountSimpleInfoV3();
        discountSimpleInfo.setId("<YOUR_DISCOUNT_ID>");
        discountSimpleInfo.setType(1);
        payCustomerOrderReq.setDiscountInfos(Collections.singletonList(discountSimpleInfo));
        payCustomerOrderReq.setOrderId("<YOUR_ORDER_ID>");
        request.setBody(payCustomerOrderReq);
        return request;
    }

    /**
     * 组装取消待支付订单请求体示例
     *
     * @return CancelCustomerOrderRequest
     */
    private static CancelCustomerOrderRequest buildCancelCustomerOrderRequest() {
        CancelCustomerOrderRequest request = new CancelCustomerOrderRequest();
        CancelCustomerOrderReq cancelCustomerOrderReq = new CancelCustomerOrderReq();
        cancelCustomerOrderReq.setOrderId("<YOUR_ORDER_ID>");
        request.setBody(cancelCustomerOrderReq);
        return request;
    }

    /**
     * 组装查询退款订单的金额详情请求体示例
     *
     * @return ShowRefundOrderDetailsRequest
     */
    private static ShowRefundOrderDetailsRequest buildShowRefundOrderDetailsRequest() {
        ShowRefundOrderDetailsRequest request = new ShowRefundOrderDetailsRequest();
        request.setOrderId("<YOUR_ORDER_ID>");
        return request;
    }
}
```

## 修订记录
| 发布日期 | 文档版本 | 修订说明 |
| :----:| :----:| :----:|
|2024-02-01|1.0.0|管理包年/包月订单场景示例第一个版本发布|