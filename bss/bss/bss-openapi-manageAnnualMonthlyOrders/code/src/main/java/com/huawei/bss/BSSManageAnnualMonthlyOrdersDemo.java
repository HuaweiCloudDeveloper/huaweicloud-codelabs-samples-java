package com.huawei.bss;

import java.util.Collections;

import com.huaweicloud.sdk.bss.v2.BssClient;
import com.huaweicloud.sdk.bss.v2.model.ListCustomerOrdersRequest;
import com.huaweicloud.sdk.bss.v2.model.ShowCustomerOrderDetailsRequest;
import com.huaweicloud.sdk.bss.v2.model.ListOrderCouponsByOrderIdRequest;
import com.huaweicloud.sdk.bss.v2.model.ListOrderDiscountsRequest;
import com.huaweicloud.sdk.bss.v2.model.PayOrdersRequest;
import com.huaweicloud.sdk.bss.v2.model.CancelCustomerOrderRequest;
import com.huaweicloud.sdk.bss.v2.model.ShowRefundOrderDetailsRequest;
import com.huaweicloud.sdk.bss.v2.model.ListCustomerOrdersResponse;
import com.huaweicloud.sdk.bss.v2.model.ShowCustomerOrderDetailsResponse;
import com.huaweicloud.sdk.bss.v2.model.ListOrderCouponsByOrderIdResponse;
import com.huaweicloud.sdk.bss.v2.model.ListOrderDiscountsResponse;
import com.huaweicloud.sdk.bss.v2.model.PayOrdersResponse;
import com.huaweicloud.sdk.bss.v2.model.CancelCustomerOrderResponse;
import com.huaweicloud.sdk.bss.v2.model.ShowRefundOrderDetailsResponse;
import com.huaweicloud.sdk.bss.v2.model.PayCustomerOrderV3Req;
import com.huaweicloud.sdk.bss.v2.model.CouponSimpleInfoOrderPayV3;
import com.huaweicloud.sdk.bss.v2.model.DiscountSimpleInfoV3;
import com.huaweicloud.sdk.bss.v2.model.CancelCustomerOrderReq;
import com.huaweicloud.sdk.bss.v2.region.BssRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

public class BSSManageAnnualMonthlyOrdersDemo {
    public static void main(String[] args) {

        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        BssClient client = BssClient.newBuilder()
            .withCredential(auth)
            .withRegion(BssRegion.valueOf("cn-north-1"))
            .build();

        // 1 查询订单列表请求体示例
        ListCustomerOrdersRequest requestForListCustomerOrders = buildListCustomerOrdersRequest();
        // 2 查询订单详情请求体示例
        ShowCustomerOrderDetailsRequest requestForShowCustomerOrderDetails = buildShowCustomerOrderDetailsRequest();
        // 3 查询订单可用优惠券请求体示例
        ListOrderCouponsByOrderIdRequest requestForListOrderCoupons = buildListOrderCouponsByOrderIdRequest();
        // 4 查询订单可用折扣请求体示例
        ListOrderDiscountsRequest requestForListOrderDiscounts = buildListOrderDiscountsRequestRequest();
        // 5 支付包年或者包月的产品订单请求体示例
        PayOrdersRequest requestForPayOrders = buildPayOrdersRequest();
        // 6 取消待支付订单请求体示例
        CancelCustomerOrderRequest requestForCancelCustomerOrder = buildCancelCustomerOrderRequest();
        // 7 查询退款订单的金额详情请求体示例
        ShowRefundOrderDetailsRequest requestForShowRefundOrderDetails = buildShowRefundOrderDetailsRequest();

        try {
            ListCustomerOrdersResponse responseForListCustomerOrders = client.listCustomerOrders(requestForListCustomerOrders);
            System.out.println("查询订单列表响应" + responseForListCustomerOrders.toString());
            ShowCustomerOrderDetailsResponse responseForShowCustomerOrderDetails = client.showCustomerOrderDetails(requestForShowCustomerOrderDetails);
            System.out.println("查询订单详情响应" + responseForShowCustomerOrderDetails.toString());
            ListOrderCouponsByOrderIdResponse responseForListOrderCoupons = client.listOrderCouponsByOrderId(requestForListOrderCoupons);
            System.out.println("查询订单可用优惠券响应" + responseForListOrderCoupons.toString());
            ListOrderDiscountsResponse responseForListOrderDiscounts = client.listOrderDiscounts(requestForListOrderDiscounts);
            System.out.println("查询订单可用折扣响应" + responseForListOrderDiscounts.toString());
            PayOrdersResponse responseForPayOrders = client.payOrders(requestForPayOrders);
            System.out.println("支付包年/包月产品订单响应" + responseForPayOrders.toString());
            CancelCustomerOrderResponse responseForCancelCustomerOrder = client.cancelCustomerOrder(requestForCancelCustomerOrder);
            System.out.println("取消待支付订单响应" + responseForCancelCustomerOrder.toString());
            ShowRefundOrderDetailsResponse responseForShowRefundOrderDetails = client.showRefundOrderDetails(requestForShowRefundOrderDetails);
            System.out.println("查询退款订单的金额详情响应" + responseForShowRefundOrderDetails.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    /**
     * 组装查询订单列表请求体示例
     *
     * @return ListCustomerOrdersRequest
     */
    private static ListCustomerOrdersRequest buildListCustomerOrdersRequest() {
        ListCustomerOrdersRequest request = new ListCustomerOrdersRequest();
        // 当入参status=6查询订单列表时，可查询出待支付订单的列表，获得订单编号(order_id)，可作为查询订单可用优惠券/折扣接口的入参，也可以作为支付订单/取消待支付订单接口的入参。
        // 当入参order_type=4查询订单列表时，可查询出退订订单的列表，获得订单编号(order_id)，可作为查询退款订单的金额详情接口的入参。
        request.setStatus(6);
        request.setOrderType("4");
        return request;
    }

    /**
     * 组装查询订单详情请求体示例
     *
     * @return ShowCustomerOrderDetailsRequest
     */
    private static ShowCustomerOrderDetailsRequest buildShowCustomerOrderDetailsRequest() {
        ShowCustomerOrderDetailsRequest request = new ShowCustomerOrderDetailsRequest();
        // 查询订单列表时系统会返回的订单ID可作为接口的入参
        request.setOrderId("<YOUR_ORDER_ID>");
        return request;
    }

    /**
     * 组装查询订单可用优惠券请求体示例
     *
     * @return ListOrderCouponsByOrderIdRequest
     */
    private static ListOrderCouponsByOrderIdRequest buildListOrderCouponsByOrderIdRequest() {
        ListOrderCouponsByOrderIdRequest request = new ListOrderCouponsByOrderIdRequest();
        request.setOrderId("<YOUR_ORDER_ID>");
        return request;
    }

    /**
     * 组装查询订单可用折扣请求体示例
     *
     * @return ListOrderDiscountsRequest
     */
    private static ListOrderDiscountsRequest buildListOrderDiscountsRequestRequest() {
        ListOrderDiscountsRequest request = new ListOrderDiscountsRequest();
        request.setOrderId("<YOUR_ORDER_ID>");
        return request;
    }

    /**
     * 组装支付包年或者包月的产品订单请求体示例
     *
     * @return PayOrdersRequest
     */
    private static PayOrdersRequest buildPayOrdersRequest() {
        PayOrdersRequest request = new PayOrdersRequest();
        PayCustomerOrderV3Req payCustomerOrderReq = new PayCustomerOrderV3Req();
        // 是否使用优惠卷YES/NO
        payCustomerOrderReq.setUseCoupon("YES");
        CouponSimpleInfoOrderPayV3 couponSimpleInfo = new CouponSimpleInfoOrderPayV3();
        couponSimpleInfo.setId("<YOUR_COUPON_ID>");
        couponSimpleInfo.setType(301);
        payCustomerOrderReq.setCouponInfos(Collections.singletonList(couponSimpleInfo));
        // 是否使用折扣YES/NO
        payCustomerOrderReq.setUseDiscount("YES");
        DiscountSimpleInfoV3 discountSimpleInfo = new DiscountSimpleInfoV3();
        discountSimpleInfo.setId("<YOUR_DISCOUNT_ID>");
        discountSimpleInfo.setType(1);
        payCustomerOrderReq.setDiscountInfos(Collections.singletonList(discountSimpleInfo));
        payCustomerOrderReq.setOrderId("<YOUR_ORDER_ID>");
        request.setBody(payCustomerOrderReq);
        return request;
    }

    /**
     * 组装取消待支付订单请求体示例
     *
     * @return CancelCustomerOrderRequest
     */
    private static CancelCustomerOrderRequest buildCancelCustomerOrderRequest() {
        CancelCustomerOrderRequest request = new CancelCustomerOrderRequest();
        CancelCustomerOrderReq cancelCustomerOrderReq = new CancelCustomerOrderReq();
        cancelCustomerOrderReq.setOrderId("<YOUR_ORDER_ID>");
        request.setBody(cancelCustomerOrderReq);
        return request;
    }

    /**
     * 组装查询退款订单的金额详情请求体示例
     *
     * @return ShowRefundOrderDetailsRequest
     */
    private static ShowRefundOrderDetailsRequest buildShowRefundOrderDetailsRequest() {
        ShowRefundOrderDetailsRequest request = new ShowRefundOrderDetailsRequest();
        request.setOrderId("<YOUR_ORDER_ID>");
        return request;
    }
}