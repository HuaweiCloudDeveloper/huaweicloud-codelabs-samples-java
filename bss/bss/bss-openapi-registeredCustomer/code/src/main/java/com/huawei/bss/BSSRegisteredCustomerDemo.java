
package com.huawei.bss;

import com.huaweicloud.sdk.bss.v2.BssClient;
import com.huaweicloud.sdk.bss.v2.model.CheckUserIdentityRequest;
import com.huaweicloud.sdk.bss.v2.model.SendVerificationMessageCodeRequest;
import com.huaweicloud.sdk.bss.v2.model.CreateSubCustomerRequest;
import com.huaweicloud.sdk.bss.v2.model.ListSubCustomersRequest;
import com.huaweicloud.sdk.bss.v2.model.CheckUserIdentityResponse;
import com.huaweicloud.sdk.bss.v2.model.SendVerificationMessageCodeResponse;
import com.huaweicloud.sdk.bss.v2.model.CreateSubCustomerResponse;
import com.huaweicloud.sdk.bss.v2.model.ListSubCustomersResponse;
import com.huaweicloud.sdk.bss.v2.model.CheckSubcustomerUserReq;
import com.huaweicloud.sdk.bss.v2.model.SendVerificationCodeV2Req;
import com.huaweicloud.sdk.bss.v2.model.CreateCustomerV2Req;
import com.huaweicloud.sdk.bss.v2.model.QuerySubCustomerListReq;
import com.huaweicloud.sdk.bss.v2.region.BssRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

public class BSSRegisteredCustomerDemo {
    public static void main(String[] args) {

        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        BssClient client = BssClient.newBuilder()
            .withCredential(auth)
            .withRegion(BssRegion.valueOf("cn-north-1"))
            .build();

        // 1.组装校验客户注册信息请求体
        CheckUserIdentityRequest checkUserIdentityRequest = buildCheckUserIdentityRequest();

        // 2.组装发送验证码请求体
        SendVerificationMessageCodeRequest sendVerificationMessageCodeRequest = buildSendVerificationMessageCodeRequest();

        // 3.组装创建客户请求体示例
        CreateSubCustomerRequest createSubCustomerRequest = buildCreateSubCustomerRequest();

        // 4.组装查询客户列表请求体示例
        ListSubCustomersRequest listSubCustomersRequest = buildListSubCustomersRequest();

        try {

            CheckUserIdentityResponse checkUserIdentityResponse = client.checkUserIdentity(checkUserIdentityRequest);
            System.out.println("校验客户注册信息响应" + checkUserIdentityResponse.toString());

            SendVerificationMessageCodeResponse sendVerificationMessageCodeResponse = client.sendVerificationMessageCode(sendVerificationMessageCodeRequest);
            System.out.println("发送验证码响应" + sendVerificationMessageCodeResponse.toString());

            CreateSubCustomerResponse createSubCustomerResponse = client.createSubCustomer(createSubCustomerRequest);
            System.out.println("创建客户响应" + createSubCustomerResponse.toString());

            ListSubCustomersResponse listSubCustomersResponse = client.listSubCustomers(listSubCustomersRequest);
            System.out.println("查询客户列表响应" + listSubCustomersResponse.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    /**
     * 组装校验客户注册信息请求体示例
     *
     * @return CheckUserIdentityRequest
     */
    private static CheckUserIdentityRequest buildCheckUserIdentityRequest() {
        CheckUserIdentityRequest request = new CheckUserIdentityRequest();
        CheckSubcustomerUserReq checkSubcustomerUserReq = new CheckSubcustomerUserReq();
        checkSubcustomerUserReq.setSearchType("<YOUR_SEARCH_TYPE>");
        checkSubcustomerUserReq.setSearchValue("<YOUR_SEARCH_VALUE>");
        request.setBody(checkSubcustomerUserReq);
        return request;
    }

    /**
     * 组装发送验证码请求体
     *
     * @return SendVerificationMessageCodeRequest
     */
    private static SendVerificationMessageCodeRequest buildSendVerificationMessageCodeRequest() {
        SendVerificationMessageCodeRequest request = new SendVerificationMessageCodeRequest();
        SendVerificationCodeV2Req sendVerificationCodeV2Req = new SendVerificationCodeV2Req();
        sendVerificationCodeV2Req.setReceiverType(1);
        sendVerificationCodeV2Req.setMobilePhone("<YOUR MOBILE PHONE>");
        sendVerificationCodeV2Req.setScene(29);
        sendVerificationCodeV2Req.setCustomerId(null);
        request.setBody(sendVerificationCodeV2Req);
        return request;
    }

    /**
     * 组装创建客户请求体示例
     *
     * @return CreateSubCustomerRequest
     */
    private static CreateSubCustomerRequest buildCreateSubCustomerRequest() {
        CreateSubCustomerRequest request = new CreateSubCustomerRequest();
        CreateCustomerV2Req createCustomerV2Req = new CreateCustomerV2Req();
        createCustomerV2Req.setXaccountId("<YOUR XACCOUNT ID>");
        createCustomerV2Req.setXaccountType("<YOUR XACCOUNT TYPE>");
        createCustomerV2Req.setDomainName("<YOUR DOMAINNAME>");
        createCustomerV2Req.setMobilePhone("<YOUR MOBILE PHONE>");
        createCustomerV2Req.setVerificationCode("<YOUR VERIFICATION CODE>");
        createCustomerV2Req.setPassword("<YOUR PASSWORD>");
        createCustomerV2Req.setIsCloseMarketMs("true");
        createCustomerV2Req.setCooperationType("1");
        // 创建云经销商的子客户，必须携带该字段
        createCustomerV2Req.setIndirectPartnerId(null);
        createCustomerV2Req.setIncludeAssociationResult(true);
        request.setBody(createCustomerV2Req);
        return request;
    }

    /**
     * 组装查询客户列表请求体示例
     *
     * @return ListSubCustomersRequest
     */
    private static ListSubCustomersRequest buildListSubCustomersRequest() {
        ListSubCustomersRequest request = new ListSubCustomersRequest();
        QuerySubCustomerListReq querySubCustomerListReq = new QuerySubCustomerListReq();
        querySubCustomerListReq.setAccountName("<YOUR ACCOUNTNAME>");
        querySubCustomerListReq.setCustomer("<YOUR CUSTOMER>");
        querySubCustomerListReq.setLabel("<YOUR LABEL>");
        querySubCustomerListReq.setAssociationType("1");
        // 关联时间区间段不支持空串
        querySubCustomerListReq.setAssociatedOnBegin("2023-05-06T08:05:01Z");
        querySubCustomerListReq.setAssociatedOnEnd("2023-06-06T08:05:01Z");
        querySubCustomerListReq.setOffset(0);
        querySubCustomerListReq.setLimit(10);
        querySubCustomerListReq.setCustomerId("<YOUR CUSTOMER ID>");
        // 查询云经销商的子客户列表，必须携带该字段
        querySubCustomerListReq.setIndirectPartnerId(null);
        request.setBody(querySubCustomerListReq);
        return request;
    }

}