## 1. 功能介绍
客户在合作伙伴销售平台可完成注册，伙伴销售平台将同步创建华为云账号，并将客户在伙伴销售平台上的账号与华为云账号建立映射关系。同时，创建的华为云账号与伙伴账号关联绑定，成为伙伴的客户。
华为云总经销商（一级经销商）可以注册云经销商（二级经销商）的子客户。注册完成后，子客户可以自动和云经销商绑定。

## 2. 前置条件
- 注册经销商合作伙伴账号和实名认证，并且加入经销商计划。参考[注册经销商伙伴](https://www.huaweicloud.com/partners/resale/)
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

## 3. SDK获取和安装
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-bss”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

## 4. 调用流程
#### 4.1 注册客户流程：
 ![图1: 注册客户流程](./assets/manageCustomer01.png "注册客户图例1")
 
#### 4.2 查询客户列表流程： 
 ![图2: 查询客户列表流程](./assets/manageCustomer02.png "查询客户列表图例1")

## 5. 接口参数说明
#### 5.1 校验客户注册信息
客户注册时可检查客户的登录名称、手机号或者邮箱是否可以用于注册。

接口详情请参考[校验客户注册信息](https://support.huaweicloud.com/api-bpconsole/mc_00023.html)

#### 5.2 发送验证码
客户注册时，如果填写了手机号，可以向对应的手机发送注册验证码，校验信息的正确性。使用个人银行卡方式进行实名认证时，通过该接口向指定的手机发送验证码。

接口详情请参考[发送验证码](https://support.huaweicloud.com/api-bpconsole/mc_00024.html)

#### 5.3 创建客户
在伙伴销售平台创建客户时同步创建华为云账号，并将客户在伙伴销售平台上的账号与华为云账号进行映射。同时，创建的华为云账号与伙伴账号关联绑定。

华为云总经销商（一级经销商）可以注册云经销商（二级经销商）的子客户。注册完成后，子客户可以自动和云经销商绑定。

接口详情请参考[创建客户](https://support.huaweicloud.com/api-bpconsole/mc_00016.html)

#### 5.4 查询客户列表
伙伴可以查询合作伙伴的客户信息列表。

接口详情请参考[查询客户列表](https://support.huaweicloud.com/api-bpconsole/mc_00021.html)

## 6. 代码示例
```java

package com.huawei.bss;

import com.huaweicloud.sdk.bss.v2.BssClient;
import com.huaweicloud.sdk.bss.v2.model.CheckUserIdentityRequest;
import com.huaweicloud.sdk.bss.v2.model.SendVerificationMessageCodeRequest;
import com.huaweicloud.sdk.bss.v2.model.CreateSubCustomerRequest;
import com.huaweicloud.sdk.bss.v2.model.ListSubCustomersRequest;
import com.huaweicloud.sdk.bss.v2.model.CheckUserIdentityResponse;
import com.huaweicloud.sdk.bss.v2.model.SendVerificationMessageCodeResponse;
import com.huaweicloud.sdk.bss.v2.model.CreateSubCustomerResponse;
import com.huaweicloud.sdk.bss.v2.model.ListSubCustomersResponse;
import com.huaweicloud.sdk.bss.v2.model.CheckSubcustomerUserReq;
import com.huaweicloud.sdk.bss.v2.model.SendVerificationCodeV2Req;
import com.huaweicloud.sdk.bss.v2.model.CreateCustomerV2Req;
import com.huaweicloud.sdk.bss.v2.model.QuerySubCustomerListReq;
import com.huaweicloud.sdk.bss.v2.region.BssRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

public class BSSRegisteredCustomerDemo {
    public static void main(String[] args) {

        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        BssClient client = BssClient.newBuilder()
            .withCredential(auth)
            .withRegion(BssRegion.valueOf("cn-north-1"))
            .build();

        // 1.组装校验客户注册信息请求体
        CheckUserIdentityRequest checkUserIdentityRequest = buildCheckUserIdentityRequest();

        // 2.组装发送验证码请求体
        SendVerificationMessageCodeRequest sendVerificationMessageCodeRequest = buildSendVerificationMessageCodeRequest();

        // 3.组装创建客户请求体示例
        CreateSubCustomerRequest createSubCustomerRequest = buildCreateSubCustomerRequest();

        // 4.组装查询客户列表请求体示例
        ListSubCustomersRequest listSubCustomersRequest = buildListSubCustomersRequest();

        try {

            CheckUserIdentityResponse checkUserIdentityResponse = client.checkUserIdentity(checkUserIdentityRequest);
            System.out.println("校验客户注册信息响应" + checkUserIdentityResponse.toString());

            SendVerificationMessageCodeResponse sendVerificationMessageCodeResponse = client.sendVerificationMessageCode(sendVerificationMessageCodeRequest);
            System.out.println("发送验证码响应" + sendVerificationMessageCodeResponse.toString());

            CreateSubCustomerResponse createSubCustomerResponse = client.createSubCustomer(createSubCustomerRequest);
            System.out.println("创建客户响应" + createSubCustomerResponse.toString());

            ListSubCustomersResponse listSubCustomersResponse = client.listSubCustomers(listSubCustomersRequest);
            System.out.println("查询客户列表响应" + listSubCustomersResponse.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    /**
     * 组装校验客户注册信息请求体示例
     *
     * @return CheckUserIdentityRequest
     */
    private static CheckUserIdentityRequest buildCheckUserIdentityRequest() {
        CheckUserIdentityRequest request = new CheckUserIdentityRequest();
        CheckSubcustomerUserReq checkSubcustomerUserReq = new CheckSubcustomerUserReq();
        checkSubcustomerUserReq.setSearchType("<YOUR_SEARCH_TYPE>");
        checkSubcustomerUserReq.setSearchValue("<YOUR_SEARCH_VALUE>");
        request.setBody(checkSubcustomerUserReq);
        return request;
    }

    /**
     * 组装发送验证码请求体
     *
     * @return SendVerificationMessageCodeRequest
     */
    private static SendVerificationMessageCodeRequest buildSendVerificationMessageCodeRequest() {
        SendVerificationMessageCodeRequest request = new SendVerificationMessageCodeRequest();
        SendVerificationCodeV2Req sendVerificationCodeV2Req = new SendVerificationCodeV2Req();
        sendVerificationCodeV2Req.setReceiverType(1);
        sendVerificationCodeV2Req.setMobilePhone("<YOUR MOBILE PHONE>");
        sendVerificationCodeV2Req.setScene(29);
        sendVerificationCodeV2Req.setCustomerId(null);
        request.setBody(sendVerificationCodeV2Req);
        return request;
    }

    /**
     * 组装创建客户请求体示例
     *
     * @return CreateSubCustomerRequest
     */
    private static CreateSubCustomerRequest buildCreateSubCustomerRequest() {
        CreateSubCustomerRequest request = new CreateSubCustomerRequest();
        CreateCustomerV2Req createCustomerV2Req = new CreateCustomerV2Req();
        createCustomerV2Req.setXaccountId("<YOUR XACCOUNT ID>");
        createCustomerV2Req.setXaccountType("<YOUR XACCOUNT TYPE>");
        createCustomerV2Req.setDomainName("<YOUR DOMAINNAME>");
        createCustomerV2Req.setMobilePhone("<YOUR MOBILE PHONE>");
        createCustomerV2Req.setVerificationCode("<YOUR VERIFICATION CODE>");
        createCustomerV2Req.setPassword("<YOUR PASSWORD>");
        createCustomerV2Req.setIsCloseMarketMs("true");
        createCustomerV2Req.setCooperationType("1");
        // 创建云经销商的子客户，必须携带该字段
        createCustomerV2Req.setIndirectPartnerId(null);
        createCustomerV2Req.setIncludeAssociationResult(true);
        request.setBody(createCustomerV2Req);
        return request;
    }

    /**
     * 组装查询客户列表请求体示例
     *
     * @return ListSubCustomersRequest
     */
    private static ListSubCustomersRequest buildListSubCustomersRequest() {
        ListSubCustomersRequest request = new ListSubCustomersRequest();
        QuerySubCustomerListReq querySubCustomerListReq = new QuerySubCustomerListReq();
        querySubCustomerListReq.setAccountName("<YOUR ACCOUNTNAME>");
        querySubCustomerListReq.setCustomer("<YOUR CUSTOMER>");
        querySubCustomerListReq.setLabel("<YOUR LABEL>");
        querySubCustomerListReq.setAssociationType("1");
        // 关联时间区间段不支持空串
        querySubCustomerListReq.setAssociatedOnBegin("2023-05-06T08:05:01Z");
        querySubCustomerListReq.setAssociatedOnEnd("2023-06-06T08:05:01Z");
        querySubCustomerListReq.setOffset(0);
        querySubCustomerListReq.setLimit(10);
        querySubCustomerListReq.setCustomerId("<YOUR CUSTOMER ID>");
        // 查询云经销商的子客户列表，必须携带该字段
        querySubCustomerListReq.setIndirectPartnerId(null);
        request.setBody(querySubCustomerListReq);
        return request;
    }

}
```

## 6. 开发建议及注意事项
- 登录账号和手机号需先调用校验客户注册信息接口，验证信息在华为云是否可用。如果手机号不填写，则客户在后续交易等环节将收不到验证码和资源到期等提醒，创建客户时，也不需要输入验证码。
- 创建客户时，可以考虑将登录名称和伙伴销售平台上的账号名称一致。登录名称如果不填写，华为云会随机生成一个华为云账号登录名称。
- 创建客户时，“xaccount_id”字段用于建立伙伴销售平台账号和华为云账号的关联，是全局唯一的。客户通过伙伴销售平台跳转到华为云时，通过该字段标识客户身份，需要与创建客户时填写的值一致。
- 创建客户时，“password”字段为华为云账号的密码，可以不填写。此时，华为云会随机生成一个密码。
- 调用“创建客户”接口时，华为云会同步创建华为云客户账号，将客户ID及账号名返回给伙伴平台，然后华为云异步完成客户与伙伴的关联。伙伴与客户的关联结果可通过“查询客户列表”查询。
- 如果客户在伙伴销售平台已创建了账号，在华为云没有账号，需利用Web UI方式跳转到华为云页面开通并绑定华为云账号。
- 如果客户在伙伴销售平台创建账号前，已经在华为云创建了账号，需利用Web UI方式跳转到华为云页面绑定已有的华为云账号。


## 修订记录
| 发布日期 | 文档版本 | 修订说明 |
| :----:| :----:| :----:|
|2024-02-22|1.0.0|注册客户（伙伴）场景示例第一个版本发布|