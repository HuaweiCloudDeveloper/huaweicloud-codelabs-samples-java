## 1. 功能介绍
客户购买包年/包月资源后，在伙伴销售平台可以：

- 查看已完成的订单的到期时间，对于快到期的订单或资源，可以完成续订操作。
- 对于已订购的包年/包月资源，客户可以设置或取消资源是否自动续费。
- 查询某次退订订单或者降配订单的退款金额来自哪些资源和对应订单。
 
## 2. 前置条件
- 注册经销商合作伙伴账号和实名认证，并且加入经销商计划。参考[注册经销商伙伴](https://www.huaweicloud.com/partners/resale/)
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
 
## 3. SDK获取和安装
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
 
使用服务端SDK前，您需要安装“huaweicloud-sdk-bss”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

## 4. 调用流程
 ![图1: 资源图例1](./assets/resourceLib001.png "资源图例1")

## 5. 接口参数说明
#### 5.1 查询客户包年/包月资源列表
客户在伙伴销售平台查询某个或所有的包年/包月资源。

接口参数的详细说明可参见：[查询客户包年/包月资源列表](https://support.huaweicloud.com/api-bpconsole/api_order_00021.html)

#### 5.2 续订包年/包月资源
客户的包年/包月资源即将到期时，可进行包年/包月资源的续订。

接口参数的详细说明可参见：[续订包年/包月资源](https://support.huaweicloud.com/api-bpconsole/api_order_00018.html)

#### 5.3 退订包年/包月资源
客户购买包年/包月资源后，支持客户退订包年/包月实例。退订资源实例包括资源续费部分和当前正在使用的部分，退订后资源将无法使用。

接口参数的详细说明可参见：[退订包年/包月资源](https://support.huaweicloud.com/api-bpconsole/api_order_00019.html)

#### 5.4 设置包年/包月资源自动续费
为防止资源到期被删除，客户可为长期使用的包年/包月资源开通自动续费。

接口参数的详细说明可参见：[设置包年/包月资源自动续费](https://support.huaweicloud.com/api-bpconsole/api_order_00022.html)

#### 5.5 取消包年/包月资源自动续费
客户设置自动续费后，还可以执行取消自动续费的操作。关闭自动续费后，资源到期将不会被自动续费。

接口参数的详细说明可参见：[取消包年/包月资源自动续费](https://support.huaweicloud.com/api-bpconsole/api_order_00023.html)

#### 5.6 设置或取消包年/包月资源到期转按需
客户可以设置包年/包月资源到期后转为按需资源计费。包年/包月计费模式到期后，按需的计费模式即生效。

接口参数的详细说明可参见：[设置或取消包年/包月资源到期转按需](https://support.huaweicloud.com/api-bpconsole/api_order_00024.html)

## 6. 代码示例
```java
package com.huawei.bss;

import java.util.LinkedList;
import java.util.List;

import com.huaweicloud.sdk.bss.v2.BssClient;
import com.huaweicloud.sdk.bss.v2.model.ListPayPerUseCustomerResourcesRequest;
import com.huaweicloud.sdk.bss.v2.model.RenewalResourcesRequest;
import com.huaweicloud.sdk.bss.v2.model.CancelResourcesSubscriptionRequest;
import com.huaweicloud.sdk.bss.v2.model.AutoRenewalResourcesRequest;
import com.huaweicloud.sdk.bss.v2.model.CancelAutoRenewalResourcesRequest;
import com.huaweicloud.sdk.bss.v2.model.UpdatePeriodToOnDemandRequest;
import com.huaweicloud.sdk.bss.v2.model.ListPayPerUseCustomerResourcesResponse;
import com.huaweicloud.sdk.bss.v2.model.RenewalResourcesResponse;
import com.huaweicloud.sdk.bss.v2.model.CancelResourcesSubscriptionResponse;
import com.huaweicloud.sdk.bss.v2.model.AutoRenewalResourcesResponse;
import com.huaweicloud.sdk.bss.v2.model.CancelAutoRenewalResourcesResponse;
import com.huaweicloud.sdk.bss.v2.model.UpdatePeriodToOnDemandResponse;
import com.huaweicloud.sdk.bss.v2.model.QueryResourcesReq;
import com.huaweicloud.sdk.bss.v2.model.RenewalResourcesReq;
import com.huaweicloud.sdk.bss.v2.model.UnsubscribeResourcesReq;
import com.huaweicloud.sdk.bss.v2.model.PeriodToOnDemandReq;
import com.huaweicloud.sdk.bss.v2.region.BssRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

public class BSSManageAnnualMonthlyResourcesDemo {
    public static void main(String[] args) {

        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        BssClient client = BssClient.newBuilder()
            .withCredential(auth)
            .withRegion(BssRegion.valueOf("cn-north-1"))
            .build();

        // 1 查询客户包年/包月资源列表请求体示例
        ListPayPerUseCustomerResourcesRequest requestForListPayPerUseCustomerResources = buildListPayPerUseCustomerResourcesRequest();

        // 2 续订包年/包月资源请求体示例
        RenewalResourcesRequest requestForRenewalResources = buildRenewalResourcesRequest();

        // 3 退订包年/包月资源请求体示例
        CancelResourcesSubscriptionRequest requestForCancelResourcesSubscription = buildCancelResourcesSubscriptionRequest();

        // 4 设置包年/包月资源自动续费请求体示例
        AutoRenewalResourcesRequest requestForAutoRenewalResources = buildAutoRenewalResourcesRequest();

        // 5 取消包年/包月资源自动续费请求体示例
        CancelAutoRenewalResourcesRequest requestForCancelAutoRenewalResources = buildCancelAutoRenewalResourcesRequest();

        // 6 设置或取消包年/包月资源到期转按需请求体示例
        UpdatePeriodToOnDemandRequest requestForUpdatePeriodToOnDemand = buildUpdatePeriodToOnDemandRequest();

        try {
            ListPayPerUseCustomerResourcesResponse listPayPerUseCustomerResourcesResponse =
                client.listPayPerUseCustomerResources(requestForListPayPerUseCustomerResources);
            System.out.println("查询客户包年/包月资源列表响应" + listPayPerUseCustomerResourcesResponse.toString());

            RenewalResourcesResponse renewalResourcesResponse =
                client.renewalResources(requestForRenewalResources);
            System.out.println("续订包年/包月资源响应" + renewalResourcesResponse.toString());

            CancelResourcesSubscriptionResponse cancelResourcesSubscriptionResponse =
                client.cancelResourcesSubscription(requestForCancelResourcesSubscription);
            System.out.println("退订包年/包月资源响应" + cancelResourcesSubscriptionResponse.toString());

            AutoRenewalResourcesResponse autoRenewalResourcesResponse =
                client.autoRenewalResources(requestForAutoRenewalResources);
            System.out.println("设置包年/包月资源自动续费响应" + autoRenewalResourcesResponse.toString());

            CancelAutoRenewalResourcesResponse cancelAutoRenewalResourcesResponse =
                client.cancelAutoRenewalResources(requestForCancelAutoRenewalResources);
            System.out.println("取消包年/包月资源自动续费响应" + cancelAutoRenewalResourcesResponse.toString());

            UpdatePeriodToOnDemandResponse updatePeriodToOnDemandResponse =
                client.updatePeriodToOnDemand(requestForUpdatePeriodToOnDemand);
            System.out.println("设置或取消包年/包月资源到期转按需响应" + updatePeriodToOnDemandResponse.toString());

        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    /**
     * 组装查询客户包年/包月资源列表请求体示例
     *
     * @return ListPayPerUseCustomerResourcesRequest
     */
    private static ListPayPerUseCustomerResourcesRequest buildListPayPerUseCustomerResourcesRequest() {
        ListPayPerUseCustomerResourcesRequest request = new ListPayPerUseCustomerResourcesRequest();
        QueryResourcesReq queryResourcesReq = new QueryResourcesReq();
        queryResourcesReq.setOnlyMainResource(1);
        request.setBody(queryResourcesReq);
        return request;
    }

    /**
     * 组装续订包年/包月资源请求体示例
     *
     * @return RenewalResourcesRequest
     */
    private static RenewalResourcesRequest buildRenewalResourcesRequest() {
        RenewalResourcesRequest request = new RenewalResourcesRequest();
        RenewalResourcesReq renewalResourcesReq = new RenewalResourcesReq();
        List<String> resourceIds = new LinkedList<>();
        resourceIds.add("<YOUR_RESOURCE_ID>");
        renewalResourcesReq.setResourceIds(resourceIds);
        renewalResourcesReq.setPeriodType(2);
        renewalResourcesReq.setPeriodNum(1);
        renewalResourcesReq.setExpirePolicy(1);
        renewalResourcesReq.setIsAutoPay(0);
        request.setBody(renewalResourcesReq);
        return request;
    }

    /**
     * 组装退订包年/包月资源请求体示例
     *
     * @return CancelResourcesSubscriptionRequest
     */
    private static CancelResourcesSubscriptionRequest buildCancelResourcesSubscriptionRequest() {
        CancelResourcesSubscriptionRequest request = new CancelResourcesSubscriptionRequest();
        UnsubscribeResourcesReq unsubscribeResourcesReq = new UnsubscribeResourcesReq();
        List<String> resourceIds = new LinkedList<>();
        resourceIds.add("<YOUR_RESOURCE_ID>");
        unsubscribeResourcesReq.setResourceIds(resourceIds);
        unsubscribeResourcesReq.setUnsubscribeType(2);
        unsubscribeResourcesReq.setUnsubscribeReasonType(1);
        request.setBody(unsubscribeResourcesReq);
        return request;
    }

    /**
     * 组装设置包年/包月资源自动续费请求体示例
     *
     * @return AutoRenewalResourcesRequest
     */
    private static AutoRenewalResourcesRequest buildAutoRenewalResourcesRequest() {
        AutoRenewalResourcesRequest request = new AutoRenewalResourcesRequest();
        request.setResourceId("<YOUR_RESOURCE_ID>");
        return request;
    }

    /**
     * 组装取消包年/包月资源自动续费请求体示例
     *
     * @return CancelAutoRenewalResourcesRequest
     */
    private static CancelAutoRenewalResourcesRequest buildCancelAutoRenewalResourcesRequest() {
        CancelAutoRenewalResourcesRequest request = new CancelAutoRenewalResourcesRequest();
        request.setResourceId("<YOUR_RESOURCE_ID>");
        return request;
    }

    /**
     * 组装设置或取消包年/包月资源到期转按需请求体示例
     *
     * @return UpdatePeriodToOnDemandRequest
     */
    private static UpdatePeriodToOnDemandRequest buildUpdatePeriodToOnDemandRequest() {
        UpdatePeriodToOnDemandRequest request = new UpdatePeriodToOnDemandRequest();
        PeriodToOnDemandReq periodToOnDemandReq = new PeriodToOnDemandReq();
        List<String> resourceIds = new LinkedList<>();
        resourceIds.add("<YOUR_RESOURCE_ID>");
        // SET_UP：设置/CANCEL：取消
        periodToOnDemandReq.setOperation("SET_UP");
        periodToOnDemandReq.setResourceIds(resourceIds);
        request.setBody(periodToOnDemandReq);
        return request;
    }
}
```

## 7. 开发建议及注意事项
- 本场景的前提是客户已购买包年/包月的云服务资源，然后根据订单列表中的订单ID完成查询包年/包月资源。
- 续订或退订包年/包月资源和设置或取消包年/包月自动续费操作是按照资源的维度进行，故需先通过查询包年/包月资源列表接口获取资源ID。
- 续订或退订包年/包月资源时，如果某个主资源ID有对应的从资源ID，系统会将主资源ID和从资源ID一起续订/退订。存在主从资源关系的资源为：云主机为主资源，对应的从资源为云硬盘、虚拟私有云、弹性公网IP和带宽。
- 自动续费将于产品到期前7天的凌晨3:00开始扣款。若由于账户中余额不足等原因导致第一次未扣费成功，系统将在保留产品资源到期之前，每天凌晨3:00再次尝试进行扣款，直到扣款成功。

## 修订记录
| 发布日期 | 文档版本 | 修订说明 |
| :----:| :----:| :----:|
|2024-02-22|1.0.0|管理包年/包月资源场景示例场景示例第一个版本发布|