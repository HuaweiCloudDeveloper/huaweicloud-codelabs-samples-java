package com.huawei.bss;

import java.util.LinkedList;
import java.util.List;

import com.huaweicloud.sdk.bss.v2.BssClient;
import com.huaweicloud.sdk.bss.v2.model.ListPayPerUseCustomerResourcesRequest;
import com.huaweicloud.sdk.bss.v2.model.RenewalResourcesRequest;
import com.huaweicloud.sdk.bss.v2.model.CancelResourcesSubscriptionRequest;
import com.huaweicloud.sdk.bss.v2.model.AutoRenewalResourcesRequest;
import com.huaweicloud.sdk.bss.v2.model.CancelAutoRenewalResourcesRequest;
import com.huaweicloud.sdk.bss.v2.model.UpdatePeriodToOnDemandRequest;
import com.huaweicloud.sdk.bss.v2.model.ListPayPerUseCustomerResourcesResponse;
import com.huaweicloud.sdk.bss.v2.model.RenewalResourcesResponse;
import com.huaweicloud.sdk.bss.v2.model.CancelResourcesSubscriptionResponse;
import com.huaweicloud.sdk.bss.v2.model.AutoRenewalResourcesResponse;
import com.huaweicloud.sdk.bss.v2.model.CancelAutoRenewalResourcesResponse;
import com.huaweicloud.sdk.bss.v2.model.UpdatePeriodToOnDemandResponse;
import com.huaweicloud.sdk.bss.v2.model.QueryResourcesReq;
import com.huaweicloud.sdk.bss.v2.model.RenewalResourcesReq;
import com.huaweicloud.sdk.bss.v2.model.UnsubscribeResourcesReq;
import com.huaweicloud.sdk.bss.v2.model.PeriodToOnDemandReq;
import com.huaweicloud.sdk.bss.v2.region.BssRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

public class BSSManageAnnualMonthlyResourcesDemo {
    public static void main(String[] args) {

        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        BssClient client = BssClient.newBuilder()
            .withCredential(auth)
            .withRegion(BssRegion.valueOf("cn-north-1"))
            .build();

        // 1 查询客户包年/包月资源列表请求体示例
        ListPayPerUseCustomerResourcesRequest requestForListPayPerUseCustomerResources = buildListPayPerUseCustomerResourcesRequest();

        // 2 续订包年/包月资源请求体示例
        RenewalResourcesRequest requestForRenewalResources = buildRenewalResourcesRequest();

        // 3 退订包年/包月资源请求体示例
        CancelResourcesSubscriptionRequest requestForCancelResourcesSubscription = buildCancelResourcesSubscriptionRequest();

        // 4 设置包年/包月资源自动续费请求体示例
        AutoRenewalResourcesRequest requestForAutoRenewalResources = buildAutoRenewalResourcesRequest();

        // 5 取消包年/包月资源自动续费请求体示例
        CancelAutoRenewalResourcesRequest requestForCancelAutoRenewalResources = buildCancelAutoRenewalResourcesRequest();

        // 6 设置或取消包年/包月资源到期转按需请求体示例
        UpdatePeriodToOnDemandRequest requestForUpdatePeriodToOnDemand = buildUpdatePeriodToOnDemandRequest();

        try {
            ListPayPerUseCustomerResourcesResponse listPayPerUseCustomerResourcesResponse =
                client.listPayPerUseCustomerResources(requestForListPayPerUseCustomerResources);
            System.out.println("查询客户包年/包月资源列表响应" + listPayPerUseCustomerResourcesResponse.toString());

            RenewalResourcesResponse renewalResourcesResponse =
                client.renewalResources(requestForRenewalResources);
            System.out.println("续订包年/包月资源响应" + renewalResourcesResponse.toString());

            CancelResourcesSubscriptionResponse cancelResourcesSubscriptionResponse =
                client.cancelResourcesSubscription(requestForCancelResourcesSubscription);
            System.out.println("退订包年/包月资源响应" + cancelResourcesSubscriptionResponse.toString());

            AutoRenewalResourcesResponse autoRenewalResourcesResponse =
                client.autoRenewalResources(requestForAutoRenewalResources);
            System.out.println("设置包年/包月资源自动续费响应" + autoRenewalResourcesResponse.toString());

            CancelAutoRenewalResourcesResponse cancelAutoRenewalResourcesResponse =
                client.cancelAutoRenewalResources(requestForCancelAutoRenewalResources);
            System.out.println("取消包年/包月资源自动续费响应" + cancelAutoRenewalResourcesResponse.toString());

            UpdatePeriodToOnDemandResponse updatePeriodToOnDemandResponse =
                client.updatePeriodToOnDemand(requestForUpdatePeriodToOnDemand);
            System.out.println("设置或取消包年/包月资源到期转按需响应" + updatePeriodToOnDemandResponse.toString());

        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    /**
     * 组装查询客户包年/包月资源列表请求体示例
     *
     * @return ListPayPerUseCustomerResourcesRequest
     */
    private static ListPayPerUseCustomerResourcesRequest buildListPayPerUseCustomerResourcesRequest() {
        ListPayPerUseCustomerResourcesRequest request = new ListPayPerUseCustomerResourcesRequest();
        QueryResourcesReq queryResourcesReq = new QueryResourcesReq();
        queryResourcesReq.setOnlyMainResource(1);
        request.setBody(queryResourcesReq);
        return request;
    }

    /**
     * 组装续订包年/包月资源请求体示例
     *
     * @return RenewalResourcesRequest
     */
    private static RenewalResourcesRequest buildRenewalResourcesRequest() {
        RenewalResourcesRequest request = new RenewalResourcesRequest();
        RenewalResourcesReq renewalResourcesReq = new RenewalResourcesReq();
        List<String> resourceIds = new LinkedList<>();
        resourceIds.add("<YOUR_RESOURCE_ID>");
        renewalResourcesReq.setResourceIds(resourceIds);
        renewalResourcesReq.setPeriodType(2);
        renewalResourcesReq.setPeriodNum(1);
        renewalResourcesReq.setExpirePolicy(1);
        renewalResourcesReq.setIsAutoPay(0);
        request.setBody(renewalResourcesReq);
        return request;
    }

    /**
     * 组装退订包年/包月资源请求体示例
     *
     * @return CancelResourcesSubscriptionRequest
     */
    private static CancelResourcesSubscriptionRequest buildCancelResourcesSubscriptionRequest() {
        CancelResourcesSubscriptionRequest request = new CancelResourcesSubscriptionRequest();
        UnsubscribeResourcesReq unsubscribeResourcesReq = new UnsubscribeResourcesReq();
        List<String> resourceIds = new LinkedList<>();
        resourceIds.add("<YOUR_RESOURCE_ID>");
        unsubscribeResourcesReq.setResourceIds(resourceIds);
        unsubscribeResourcesReq.setUnsubscribeType(2);
        unsubscribeResourcesReq.setUnsubscribeReasonType(1);
        request.setBody(unsubscribeResourcesReq);
        return request;
    }

    /**
     * 组装设置包年/包月资源自动续费请求体示例
     *
     * @return AutoRenewalResourcesRequest
     */
    private static AutoRenewalResourcesRequest buildAutoRenewalResourcesRequest() {
        AutoRenewalResourcesRequest request = new AutoRenewalResourcesRequest();
        request.setResourceId("<YOUR_RESOURCE_ID>");
        return request;
    }

    /**
     * 组装取消包年/包月资源自动续费请求体示例
     *
     * @return CancelAutoRenewalResourcesRequest
     */
    private static CancelAutoRenewalResourcesRequest buildCancelAutoRenewalResourcesRequest() {
        CancelAutoRenewalResourcesRequest request = new CancelAutoRenewalResourcesRequest();
        request.setResourceId("<YOUR_RESOURCE_ID>");
        return request;
    }

    /**
     * 组装设置或取消包年/包月资源到期转按需请求体示例
     *
     * @return UpdatePeriodToOnDemandRequest
     */
    private static UpdatePeriodToOnDemandRequest buildUpdatePeriodToOnDemandRequest() {
        UpdatePeriodToOnDemandRequest request = new UpdatePeriodToOnDemandRequest();
        PeriodToOnDemandReq periodToOnDemandReq = new PeriodToOnDemandReq();
        List<String> resourceIds = new LinkedList<>();
        resourceIds.add("<YOUR_RESOURCE_ID>");
        // SET_UP：设置/CANCEL：取消
        periodToOnDemandReq.setOperation("SET_UP");
        periodToOnDemandReq.setResourceIds(resourceIds);
        request.setBody(periodToOnDemandReq);
        return request;
    }
}