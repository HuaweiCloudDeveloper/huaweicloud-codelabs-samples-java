package com.huawei.bss;

import com.huaweicloud.sdk.bss.v2.BssClient;
import com.huaweicloud.sdk.bss.v2.model.ListResourceTypesRequest;
import com.huaweicloud.sdk.bss.v2.model.ListServiceResourcesRequest;
import com.huaweicloud.sdk.bss.v2.model.ListServiceTypesRequest;
import com.huaweicloud.sdk.bss.v2.model.ListUsageTypesRequest;
import com.huaweicloud.sdk.bss.v2.model.ListMeasureUnitsRequest;
import com.huaweicloud.sdk.bss.v2.model.ListConversionsRequest;
import com.huaweicloud.sdk.bss.v2.model.ListServiceTypesResponse;
import com.huaweicloud.sdk.bss.v2.model.ListResourceTypesResponse;
import com.huaweicloud.sdk.bss.v2.model.ListServiceResourcesResponse;
import com.huaweicloud.sdk.bss.v2.model.ListUsageTypesResponse;
import com.huaweicloud.sdk.bss.v2.model.ListMeasureUnitsResponse;
import com.huaweicloud.sdk.bss.v2.model.ListConversionsResponse;
import com.huaweicloud.sdk.bss.v2.region.BssRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

public class BSSQueryProductInformationDemo {
    public static void main(String[] args) {

        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        BssClient client = BssClient.newBuilder()
            .withCredential(auth)
            .withRegion(BssRegion.valueOf("cn-north-1"))
            .build();

        // 1 查询云服务类型列表
        ListServiceTypesRequest listServiceTypesRequest = buildListServiceTypesRequest();

        // 2 查询资源类型列表
        ListResourceTypesRequest listResourceTypesRequest = buildListResourceTypesRequest();

        // 3 根据云服务类型查询资源列表
        ListServiceResourcesRequest listServiceResourcesRequest = buildListServiceResourcesRequest();

        // 4 查询使用量类型列表
        ListUsageTypesRequest listUsageTypesRequest = buildListUsageTypesRequest();

        // 5 查询度量单位列表
        ListMeasureUnitsRequest listMeasureUnitsRequest = buildListMeasureUnitsRequest();

        // 6 查询度量单位进制
        ListConversionsRequest listConversionsRequest = buildListConversionsRequest();

        try {
            ListServiceTypesResponse responseForListServiceTypes = client.listServiceTypes(listServiceTypesRequest);
            System.out.println("查询云服务类型列表响应" + responseForListServiceTypes.toString());

            ListResourceTypesResponse responseForListResourceTypes = client.listResourceTypes(listResourceTypesRequest);
            System.out.println("查询资源类型列表响应" + responseForListResourceTypes.toString());

            ListServiceResourcesResponse responseForListServiceResources = client.listServiceResources(listServiceResourcesRequest);
            System.out.println("根据云服务类型查询资源列表响应" + responseForListServiceResources.toString());

            ListUsageTypesResponse responseForListUsageTypes = client.listUsageTypes(listUsageTypesRequest);
            System.out.println("查询使用量类型列表响应" + responseForListUsageTypes.toString());

            ListMeasureUnitsResponse responseForListMeasureUnits = client.listMeasureUnits(listMeasureUnitsRequest);
            System.out.println("查询度量单位列表响应" + responseForListMeasureUnits.toString());

            ListConversionsResponse responseForListConversions = client.listConversions(listConversionsRequest);
            System.out.println("查询度量单位进制响应" + responseForListConversions.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    /**
     * 组装查询云服务类型列表请求体示例
     *
     * @return ListServiceTypesRequest
     */
    private static ListServiceTypesRequest buildListServiceTypesRequest() {
        ListServiceTypesRequest request = new ListServiceTypesRequest();
        request.setLimit(10);
        request.setOffset(1);
        request.setXLanguage("zh_CN");
        return request;
    }

    /**
     * 组装查询资源类型列表请求体示例
     *
     * @return ListResourceTypesRequest
     */
    private static ListResourceTypesRequest buildListResourceTypesRequest() {
        ListResourceTypesRequest request = new ListResourceTypesRequest();
        request.setLimit(10);
        request.setOffset(1);
        request.setXLanguage("zh_CN");
        return request;
    }

    /**
     * 组装根据云服务类型查询资源列表请求体示例
     *
     * @return ListServiceResourcesRequest
     */
    private static ListServiceResourcesRequest buildListServiceResourcesRequest() {
        ListServiceResourcesRequest request = new ListServiceResourcesRequest();
        request.setLimit(10);
        request.setOffset(1);
        request.setServiceTypeCode("<SERVICE_TYPE_CODE>");
        request.setXLanguage("zh_CN");
        return request;
    }

    /**
     * 组装查询使用量类型列表请求体示例
     *
     * @return ListUsageTypesRequest
     */
    private static ListUsageTypesRequest buildListUsageTypesRequest() {
        ListUsageTypesRequest request = new ListUsageTypesRequest();
        request.setLimit(10);
        request.setOffset(1);
        request.setResourceTypeCode("<RESOURCE_TYPE_CODE>");
        request.setXLanguage("zh_CN");
        return request;
    }

    /**
     * 组装查询度量单位列表请求体示例
     *
     * @return ListMeasureUnitsRequest
     */
    private static ListMeasureUnitsRequest buildListMeasureUnitsRequest() {
        ListMeasureUnitsRequest request = new ListMeasureUnitsRequest();
        request.setXLanguage("zh_CN");
        return request;
    }

    /**
     * 组装查询度量单位进制请求体示例
     *
     * @return ListConversionsRequest
     */
    private static ListConversionsRequest buildListConversionsRequest() {
        ListConversionsRequest request = new ListConversionsRequest();
        request.setMeasureType(1);
        return request;
    }
}