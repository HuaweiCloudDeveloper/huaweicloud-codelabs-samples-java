## 1. 功能介绍
客户/伙伴在自建平台可以：

- 查询云服务类型和资源类型。
- 根据云服务类型查询资源列表。
- 查询使用量类型、度量单位及度量单位进制。
 
## 2. 前置条件
- 注册经销商合作伙伴账号和实名认证，并且加入经销商计划。参考[注册经销商伙伴](https://www.huaweicloud.com/partners/resale/)
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
 
## 3. SDK获取和安装
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
 
使用服务端SDK前，您需要安装“huaweicloud-sdk-bss”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

## 4. 调用流程
 ![图1: 查询产品信息图例1](./assets/queringProductInformation001.png "查询产品信息图例1")

## 5. 接口参数说明
#### 5.1 查询云服务类型列表

接口参数的详细说明可参见：[查询云服务类型列表](https://support.huaweicloud.com/api-oce/zh-cn_topic_0000001256679455.html)

#### 5.2 查询资源类型列表

接口参数的详细说明可参见：[查询资源类型列表](https://support.huaweicloud.com/api-oce/zh-cn_topic_0000001256519451.html)

#### 5.3 根据云服务类型查询资源列表

接口参数的详细说明可参见：[根据云服务类型查询资源列表](https://support.huaweicloud.com/api-oce/qct_00003.html)

#### 5.4 查询使用量类型列表

接口参数的详细说明可参见：[查询使用量类型列表](https://support.huaweicloud.com/api-oce/qct_00004.html)

#### 5.5 查询度量单位列表

接口参数的详细说明可参见：[查询度量单位列表](https://support.huaweicloud.com/api-oce/qct_00006.html)

#### 5.6 查询度量单位进制

接口参数的详细说明可参见：[查询度量单位进制](https://support.huaweicloud.com/api-oce/qct_00007.html)


## 6. 代码示例
```java
package com.huawei.bss;

import com.huaweicloud.sdk.bss.v2.BssClient;
import com.huaweicloud.sdk.bss.v2.model.ListResourceTypesRequest;
import com.huaweicloud.sdk.bss.v2.model.ListServiceResourcesRequest;
import com.huaweicloud.sdk.bss.v2.model.ListServiceTypesRequest;
import com.huaweicloud.sdk.bss.v2.model.ListUsageTypesRequest;
import com.huaweicloud.sdk.bss.v2.model.ListMeasureUnitsRequest;
import com.huaweicloud.sdk.bss.v2.model.ListConversionsRequest;
import com.huaweicloud.sdk.bss.v2.model.ListServiceTypesResponse;
import com.huaweicloud.sdk.bss.v2.model.ListResourceTypesResponse;
import com.huaweicloud.sdk.bss.v2.model.ListServiceResourcesResponse;
import com.huaweicloud.sdk.bss.v2.model.ListUsageTypesResponse;
import com.huaweicloud.sdk.bss.v2.model.ListMeasureUnitsResponse;
import com.huaweicloud.sdk.bss.v2.model.ListConversionsResponse;
import com.huaweicloud.sdk.bss.v2.region.BssRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

public class BSSQueryProductInformationDemo {
    public static void main(String[] args) {

        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        BssClient client = BssClient.newBuilder()
            .withCredential(auth)
            .withRegion(BssRegion.valueOf("cn-north-1"))
            .build();

        // 1 查询云服务类型列表
        ListServiceTypesRequest listServiceTypesRequest = buildListServiceTypesRequest();

        // 2 查询资源类型列表
        ListResourceTypesRequest listResourceTypesRequest = buildListResourceTypesRequest();

        // 3 根据云服务类型查询资源列表
        ListServiceResourcesRequest listServiceResourcesRequest = buildListServiceResourcesRequest();

        // 4 查询使用量类型列表
        ListUsageTypesRequest listUsageTypesRequest = buildListUsageTypesRequest();

        // 5 查询度量单位列表
        ListMeasureUnitsRequest listMeasureUnitsRequest = buildListMeasureUnitsRequest();

        // 6 查询度量单位进制
        ListConversionsRequest listConversionsRequest = buildListConversionsRequest();

        try {
            ListServiceTypesResponse responseForListServiceTypes = client.listServiceTypes(listServiceTypesRequest);
            System.out.println("查询云服务类型列表响应" + responseForListServiceTypes.toString());

            ListResourceTypesResponse responseForListResourceTypes = client.listResourceTypes(listResourceTypesRequest);
            System.out.println("查询资源类型列表响应" + responseForListResourceTypes.toString());

            ListServiceResourcesResponse responseForListServiceResources = client.listServiceResources(listServiceResourcesRequest);
            System.out.println("根据云服务类型查询资源列表响应" + responseForListServiceResources.toString());

            ListUsageTypesResponse responseForListUsageTypes = client.listUsageTypes(listUsageTypesRequest);
            System.out.println("查询使用量类型列表响应" + responseForListUsageTypes.toString());

            ListMeasureUnitsResponse responseForListMeasureUnits = client.listMeasureUnits(listMeasureUnitsRequest);
            System.out.println("查询度量单位列表响应" + responseForListMeasureUnits.toString());

            ListConversionsResponse responseForListConversions = client.listConversions(listConversionsRequest);
            System.out.println("查询度量单位进制响应" + responseForListConversions.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    /**
     * 组装查询云服务类型列表请求体示例
     *
     * @return ListServiceTypesRequest
     */
    private static ListServiceTypesRequest buildListServiceTypesRequest() {
        ListServiceTypesRequest request = new ListServiceTypesRequest();
        request.setLimit(10);
        request.setOffset(1);
        request.setXLanguage("zh_CN");
        return request;
    }

    /**
     * 组装查询资源类型列表请求体示例
     *
     * @return ListResourceTypesRequest
     */
    private static ListResourceTypesRequest buildListResourceTypesRequest() {
        ListResourceTypesRequest request = new ListResourceTypesRequest();
        request.setLimit(10);
        request.setOffset(1);
        request.setXLanguage("zh_CN");
        return request;
    }

    /**
     * 组装根据云服务类型查询资源列表请求体示例
     *
     * @return ListServiceResourcesRequest
     */
    private static ListServiceResourcesRequest buildListServiceResourcesRequest() {
        ListServiceResourcesRequest request = new ListServiceResourcesRequest();
        request.setLimit(10);
        request.setOffset(1);
        request.setServiceTypeCode("<SERVICE_TYPE_CODE>");
        request.setXLanguage("zh_CN");
        return request;
    }

    /**
     * 组装查询使用量类型列表请求体示例
     *
     * @return ListUsageTypesRequest
     */
    private static ListUsageTypesRequest buildListUsageTypesRequest() {
        ListUsageTypesRequest request = new ListUsageTypesRequest();
        request.setLimit(10);
        request.setOffset(1);
        request.setResourceTypeCode("<RESOURCE_TYPE_CODE>");
        request.setXLanguage("zh_CN");
        return request;
    }

    /**
     * 组装查询度量单位列表请求体示例
     *
     * @return ListMeasureUnitsRequest
     */
    private static ListMeasureUnitsRequest buildListMeasureUnitsRequest() {
        ListMeasureUnitsRequest request = new ListMeasureUnitsRequest();
        request.setXLanguage("zh_CN");
        return request;
    }

    /**
     * 组装查询度量单位进制请求体示例
     *
     * @return ListConversionsRequest
     */
    private static ListConversionsRequest buildListConversionsRequest() {
        ListConversionsRequest request = new ListConversionsRequest();
        request.setXLanguage("zh_CN");
        request.setMeasureType(1);
        return request;
    }
}
```

## 修订记录
| 发布日期 | 文档版本 | 修订说明 |
| :----:| :----:| :----:|
|2024-02-22|1.0.0|查询产品信息场景示例第一个版本发布|