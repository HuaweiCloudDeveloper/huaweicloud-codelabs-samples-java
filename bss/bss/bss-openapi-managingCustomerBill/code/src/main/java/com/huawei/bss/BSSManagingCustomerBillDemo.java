
package com.huawei.bss;

import com.huaweicloud.sdk.bss.v2.BssClient;
import com.huaweicloud.sdk.bss.v2.model.ShowCustomerMonthlySumRequest;
import com.huaweicloud.sdk.bss.v2.model.ListCustomerselfResourceRecordDetailsRequest;
import com.huaweicloud.sdk.bss.v2.model.ListCustomerselfResourceRecordsRequest;
import com.huaweicloud.sdk.bss.v2.model.ShowCustomerMonthlySumResponse;
import com.huaweicloud.sdk.bss.v2.model.ListCustomerselfResourceRecordDetailsResponse;
import com.huaweicloud.sdk.bss.v2.model.ListCustomerselfResourceRecordsResponse;
import com.huaweicloud.sdk.bss.v2.model.QueryResRecordsDetailReq;
import com.huaweicloud.sdk.bss.v2.region.BssRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

public class BSSManagingCustomerBillDemo {
    public static void main(String[] args) {

        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        BssClient client = BssClient.newBuilder()
            .withCredential(auth)
            .withRegion(BssRegion.valueOf("cn-north-1"))
            .build();

        // 1.组装查询汇总账单请求体示例
        ShowCustomerMonthlySumRequest showCustomerMonthlySumRequest = buildShowCustomerMonthlySumRequest();

        // 2.组装查询资源详单请求体示例
        ListCustomerselfResourceRecordDetailsRequest listCustomerselfResourceRecordDetailsRequest = buildListCustomerselfResourceRecordDetailsRequest();

        // 3.组装查询资源消费记录请求体示例
        ListCustomerselfResourceRecordsRequest listCustomerselfResourceRecordsRequest = buildListCustomerselfResourceRecordsRequest();

        try {
            ShowCustomerMonthlySumResponse showCustomerMonthlySumResponse =
                client.showCustomerMonthlySum(showCustomerMonthlySumRequest);
            System.out.println("查询汇总账单响应" + showCustomerMonthlySumResponse.toString());

            ListCustomerselfResourceRecordDetailsResponse listCustomerselfResourceRecordDetailsResponse =
                client.listCustomerselfResourceRecordDetails(listCustomerselfResourceRecordDetailsRequest);
            System.out.println("查询资源详单响应" + listCustomerselfResourceRecordDetailsResponse.toString());

            ListCustomerselfResourceRecordsResponse listCustomerselfResourceRecordsResponse =
                client.listCustomerselfResourceRecords(listCustomerselfResourceRecordsRequest);
            System.out.println("查询资源消费记录响应" + listCustomerselfResourceRecordsResponse.toString());

        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    /**
     * 组装查询汇总账单请求体示例
     *
     * @return ShowCustomerMonthlySumRequest
     */
    private static ShowCustomerMonthlySumRequest buildShowCustomerMonthlySumRequest() {
        ShowCustomerMonthlySumRequest request = new ShowCustomerMonthlySumRequest();
        request.setBillCycle("<BILL_CYCLE>");
        request.setServiceTypeCode("<SERVICE_TYPE_CODE>");
        request.setEnterpriseProjectId("<ENTERPRISE_PROJECT_ID>");
        request.setMethod("all");
        // 如果method取值为sub_customer，则该参数不能为空。
        request.setSubCustomerId("");
        request.setOffset(0);
        request.setLimit(10);
        return request;
    }

    /**
     * 组装查询资源详单请求体
     *
     * @return ListCustomerselfResourceRecordDetailsRequest
     */
    private static ListCustomerselfResourceRecordDetailsRequest buildListCustomerselfResourceRecordDetailsRequest() {
        ListCustomerselfResourceRecordDetailsRequest request = new ListCustomerselfResourceRecordDetailsRequest();
        QueryResRecordsDetailReq queryResRecordsDetailReq = new QueryResRecordsDetailReq();
        queryResRecordsDetailReq.setCycle("<CYCLE>");
        queryResRecordsDetailReq.setCloudServiceType("<CLOUD_SERVICE_TYPE>");
        queryResRecordsDetailReq.setResourceType("<RESOURCE_TYPE>");
        queryResRecordsDetailReq.setRegion("cn-north-1");
        queryResRecordsDetailReq.setResInstanceId("<RES_INSTANCE_ID>");
        queryResRecordsDetailReq.setChargeMode(null);
        queryResRecordsDetailReq.setBillType(null);
        queryResRecordsDetailReq.setEnterpriseProjectId("ENTERPRISE_PROJECT_ID");
        queryResRecordsDetailReq.setIncludeZeroRecord(null);
        queryResRecordsDetailReq.setMethod("all");
        // 如果method取值为sub_customer，则此参数不能为空。
        queryResRecordsDetailReq.setSubCustomerId("");
        queryResRecordsDetailReq.setOffset(0);
        queryResRecordsDetailReq.setLimit(10);
        queryResRecordsDetailReq.setStatisticType(1);
        queryResRecordsDetailReq.setQueryType("BILLCYCLE");
        queryResRecordsDetailReq.setBillCycleBegin(null);
        queryResRecordsDetailReq.setBillCycleEnd(null);
        request.setBody(queryResRecordsDetailReq);
        return request;
    }

    /**
     * 组装查询资源消费记录请求体
     *
     * @return ListCustomerselfResourceRecordsRequest
     */
    private static ListCustomerselfResourceRecordsRequest buildListCustomerselfResourceRecordsRequest() {
        ListCustomerselfResourceRecordsRequest request = new ListCustomerselfResourceRecordsRequest();
        request.setXLanguage("<LANGUAGE>");
        request.setCycle("<CYCLE>");
        request.setChargeMode(null);
        request.setCloudServiceType("<CLOUD_SERVICE_TYPE>");
        request.setRegion("cn-north-1");
        request.setBillType(null);
        request.setOffset(0);
        request.setLimit(10);
        request.setResourceId("<RESOURCE_ID>");
        request.setEnterpriseProjectId("ENTERPRISE_PROJECT_ID");
        request.setIncludeZeroRecord(null);
        request.setMethod("all");
        // 如果method取值为sub_customer，则该参数不能为空。
        request.setSubCustomerId("");
        request.setTradeId(null);
        request.setBillDateBegin(null);
        request.setBillDateEnd(null);
        return request;
    }

}