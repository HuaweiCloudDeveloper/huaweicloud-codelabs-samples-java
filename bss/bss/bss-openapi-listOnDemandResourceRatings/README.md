## 1. 功能介绍

伙伴/客户在销售平台按照条件查询按需产品的价格。

如果购买该产品的租户享受折扣，可以在查询结果中返回折扣金额以及扣除折扣后的最后成交价。

如果该租户享受多种折扣，系统会优先返回客户享受的商务折扣的折扣金额和最终成交价。

`注意事项`：华为云根据云服务类型、资源类型、云服务区和资源规格四个条件来查询产品，查询时请确认这4个查询条件均输入正确，否则该接口会返回无法找到产品的错误

## 2. 前置条件
- 注册经销商合作伙伴账号和实名认证，并且加入经销商计划。参考[注册经销商伙伴](https://www.huaweicloud.com/partners/resale/)
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

## 3. SDK获取和安装
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-bss”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

## 4. 接口参数说明
关于接口参数的详细说明可参见：[查询按需产品价格](https://support.huaweicloud.com/api-bpconsole/bcloud_01001.html)

## 5. 代码示例
```java
package com.huawei.bss;

import com.huaweicloud.sdk.bss.v2.BssClient;
import com.huaweicloud.sdk.bss.v2.model.DemandProductInfo;
import com.huaweicloud.sdk.bss.v2.model.ListOnDemandResourceRatingsRequest;
import com.huaweicloud.sdk.bss.v2.model.ListOnDemandResourceRatingsResponse;
import com.huaweicloud.sdk.bss.v2.model.RateOnDemandReq;
import com.huaweicloud.sdk.bss.v2.region.BssRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import java.util.Collections;
import java.util.List;

public class BSSListOnDemandResourceRatingsDemo {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials().withAk(ak).withSk(sk);

        BssClient client =
            BssClient.newBuilder().withCredential(auth).withRegion(BssRegion.valueOf("cn-north-1")).build();

        // 1 组装按需查询云服务器产品价格请求体示例
        ListOnDemandResourceRatingsRequest requestForCloudServer = buildRequestForCloudServer();
        // 2 组装按需查询硬盘产品价格请求体示例
        ListOnDemandResourceRatingsRequest requestForCloudDisk = buildRequestForCloudDisk();
        // 3 组装按需查询弹性IP产品价格请求体示例
        ListOnDemandResourceRatingsRequest requestForEIP = buildRequestForEIP();
        // 4 组装按需查询带宽产品价格请求体示例
        ListOnDemandResourceRatingsRequest requestForBandwidth = buildRequestForBandwidth();
        try {
            ListOnDemandResourceRatingsResponse responseForCloudServer =
                client.listOnDemandResourceRatings(requestForCloudServer);
            System.out.println("查询云服务器产品价格响应" + responseForCloudServer.toString());
            ListOnDemandResourceRatingsResponse responseForCloudDisk =
                client.listOnDemandResourceRatings(requestForCloudDisk);
            System.out.println("查询硬盘产品价格响应" + responseForCloudDisk.toString());
            ListOnDemandResourceRatingsResponse responseForEIP = client.listOnDemandResourceRatings(requestForEIP);
            System.out.println("查询弹性IP产品价格响应" + responseForEIP.toString());
            ListOnDemandResourceRatingsResponse responseForBandwidth =
            client.listOnDemandResourceRatings(requestForBandwidth);
            System.out.println("查询带宽产品价格响应" + responseForBandwidth.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    /**
     * 组装按需查询云服务器产品价格请求体示例
     *
     * @return ListOnDemandResourceRatingsRequest
     */
    private static ListOnDemandResourceRatingsRequest buildRequestForCloudServer() {
        ListOnDemandResourceRatingsRequest request = new ListOnDemandResourceRatingsRequest();
        RateOnDemandReq rateOnDemandReq = new RateOnDemandReq();
        rateOnDemandReq.setProjectId("<YOUR PROJECT ID>");
        rateOnDemandReq.setInquiryPrecision(0);
        DemandProductInfo demandProductInfo = new DemandProductInfo();
        demandProductInfo.setId("1");
        demandProductInfo.setCloudServiceType("hws.service.type.ec2");
        demandProductInfo.setResourceType("hws.resource.type.vm");
        demandProductInfo.setResourceSpec("c3.large.2.linux");
        demandProductInfo.setRegion("cn-north-1");
        demandProductInfo.setUsageFactor("Duration");
        demandProductInfo.setUsageValue(1.0);
        demandProductInfo.setUsageMeasureId(4);
        demandProductInfo.setSubscriptionNum(1);
        List<DemandProductInfo> demandProductInfoList = Collections.singletonList(demandProductInfo);
        rateOnDemandReq.setProductInfos(demandProductInfoList);
        request.setBody(rateOnDemandReq);
        return request;
    }

    /**
     * 组装按需查询硬盘产品价格请求体示例
     *
     * @return ListOnDemandResourceRatingsRequest
     */
    private static ListOnDemandResourceRatingsRequest buildRequestForCloudDisk() {
        ListOnDemandResourceRatingsRequest request = new ListOnDemandResourceRatingsRequest();
        RateOnDemandReq rateOnDemandReq = new RateOnDemandReq();
        rateOnDemandReq.setProjectId("<YOUR PROJECT ID>");
        rateOnDemandReq.setInquiryPrecision(0);
        DemandProductInfo demandProductInfo = new DemandProductInfo();
        demandProductInfo.setId("1");
        demandProductInfo.setCloudServiceType("hws.service.type.ebs");
        demandProductInfo.setResourceType("hws.resource.type.volume");
        demandProductInfo.setResourceSpec("SAS");
        demandProductInfo.setResourceSize(500);
        demandProductInfo.setRegion("cn-north-1");
        demandProductInfo.setUsageFactor("Duration");
        demandProductInfo.setSizeMeasureId(17);
        demandProductInfo.setUsageValue(1.0);
        demandProductInfo.setUsageMeasureId(4);
        demandProductInfo.setSubscriptionNum(1);
        List<DemandProductInfo> demandProductInfoList = Collections.singletonList(demandProductInfo);
        rateOnDemandReq.setProductInfos(demandProductInfoList);
        request.setBody(rateOnDemandReq);
        return request;
    }

    /**
     * 组装按需查询弹性IP产品价格请求体示例
     *
     * @return ListOnDemandResourceRatingsRequest
     */
    private static ListOnDemandResourceRatingsRequest buildRequestForEIP() {
        ListOnDemandResourceRatingsRequest request = new ListOnDemandResourceRatingsRequest();
        RateOnDemandReq rateOnDemandReq = new RateOnDemandReq();
        rateOnDemandReq.setProjectId("<YOUR PROJECT ID>");
        rateOnDemandReq.setInquiryPrecision(0);
        DemandProductInfo demandProductInfo = new DemandProductInfo();
        demandProductInfo.setId("1");
        demandProductInfo.setCloudServiceType("hws.service.type.vpc");
        demandProductInfo.setResourceType("hws.resource.type.ip");
        demandProductInfo.setResourceSpec("5_bgp");
        demandProductInfo.setRegion("cn-east-3");
        demandProductInfo.setUsageFactor("Duration");
        demandProductInfo.setUsageValue(1.0);
        demandProductInfo.setUsageMeasureId(4);
        demandProductInfo.setSubscriptionNum(1);
        List<DemandProductInfo> demandProductInfoList = Collections.singletonList(demandProductInfo);
        rateOnDemandReq.setProductInfos(demandProductInfoList);
        request.setBody(rateOnDemandReq);
        return request;
    }

    /**
     * 组装按需查询带宽产品价格请求体示例
     *
     * @return ListOnDemandResourceRatingsRequest
     */
    private static ListOnDemandResourceRatingsRequest buildRequestForBandwidth() {
        ListOnDemandResourceRatingsRequest request = new ListOnDemandResourceRatingsRequest();
        RateOnDemandReq rateOnDemandReq = new RateOnDemandReq();
        rateOnDemandReq.setProjectId("<YOUR PROJECT ID>");
        rateOnDemandReq.setInquiryPrecision(0);
        DemandProductInfo demandProductInfo = new DemandProductInfo();
        demandProductInfo.setId("1");
        demandProductInfo.setCloudServiceType("hws.service.type.vpc");
        demandProductInfo.setResourceType("hws.resource.type.bandwidth");
        demandProductInfo.setResourceSpec("12_sbgp");
        demandProductInfo.setResourceSize(1);
        demandProductInfo.setRegion("cn-north-1");
        demandProductInfo.setAvailableZone("cn-north-1a");
        demandProductInfo.setUsageFactor("upflow");
        demandProductInfo.setSizeMeasureId(15);
        demandProductInfo.setUsageValue(1.0);
        demandProductInfo.setUsageMeasureId(15);
        demandProductInfo.setSubscriptionNum(1);
        List<DemandProductInfo> demandProductInfoList = Collections.singletonList(demandProductInfo);
        rateOnDemandReq.setProductInfos(demandProductInfoList);
        request.setBody(rateOnDemandReq);
        return request;
    }

}
```

## 修订记录
| 发布日期 | 文档版本 | 修订说明 |
| :----:| :----:| :----:|
|2024-02-01|1.0.0|查询按需产品价格场景示例第一个版本发布|