package com.huawei.bss;

import com.huaweicloud.sdk.bss.v2.BssClient;
import com.huaweicloud.sdk.bss.v2.model.DemandProductInfo;
import com.huaweicloud.sdk.bss.v2.model.ListOnDemandResourceRatingsRequest;
import com.huaweicloud.sdk.bss.v2.model.ListOnDemandResourceRatingsResponse;
import com.huaweicloud.sdk.bss.v2.model.RateOnDemandReq;
import com.huaweicloud.sdk.bss.v2.region.BssRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import java.util.Collections;
import java.util.List;

public class BSSListOnDemandResourceRatingsDemo {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials().withAk(ak).withSk(sk);

        BssClient client =
            BssClient.newBuilder().withCredential(auth).withRegion(BssRegion.valueOf("cn-north-1")).build();

        // 1 组装按需查询云服务器产品价格请求体示例
        ListOnDemandResourceRatingsRequest requestForCloudServer = buildRequestForCloudServer();
        // 2 组装按需查询硬盘产品价格请求体示例
        ListOnDemandResourceRatingsRequest requestForCloudDisk = buildRequestForCloudDisk();
        // 3 组装按需查询弹性IP产品价格请求体示例
        ListOnDemandResourceRatingsRequest requestForEIP = buildRequestForEIP();
        // 4 组装按需查询带宽产品价格请求体示例
        ListOnDemandResourceRatingsRequest requestForBandwidth = buildRequestForBandwidth();
        try {
            ListOnDemandResourceRatingsResponse responseForCloudServer =
                client.listOnDemandResourceRatings(requestForCloudServer);
            System.out.println("查询云服务器产品价格响应" + responseForCloudServer.toString());
            ListOnDemandResourceRatingsResponse responseForCloudDisk =
                client.listOnDemandResourceRatings(requestForCloudDisk);
            System.out.println("查询硬盘产品价格响应" + responseForCloudDisk.toString());
            ListOnDemandResourceRatingsResponse responseForEIP = client.listOnDemandResourceRatings(requestForEIP);
            System.out.println("查询弹性IP产品价格响应" + responseForEIP.toString());
            ListOnDemandResourceRatingsResponse responseForBandwidth =
                client.listOnDemandResourceRatings(requestForBandwidth);
            System.out.println("查询带宽产品价格响应" + responseForBandwidth.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    /**
     * 组装按需查询云服务器产品价格请求体示例
     *
     * @return ListOnDemandResourceRatingsRequest
     */
    private static ListOnDemandResourceRatingsRequest buildRequestForCloudServer() {
        ListOnDemandResourceRatingsRequest request = new ListOnDemandResourceRatingsRequest();
        RateOnDemandReq rateOnDemandReq = new RateOnDemandReq();
        rateOnDemandReq.setProjectId("<YOUR PROJECT ID>");
        rateOnDemandReq.setInquiryPrecision(0);
        DemandProductInfo demandProductInfo = new DemandProductInfo();
        demandProductInfo.setId("1");
        demandProductInfo.setCloudServiceType("hws.service.type.ec2");
        demandProductInfo.setResourceType("hws.resource.type.vm");
        demandProductInfo.setResourceSpec("c3.large.2.linux");
        demandProductInfo.setRegion("cn-north-1");
        demandProductInfo.setUsageFactor("Duration");
        demandProductInfo.setUsageValue(1.0);
        demandProductInfo.setUsageMeasureId(4);
        demandProductInfo.setSubscriptionNum(1);
        List<DemandProductInfo> demandProductInfoList = Collections.singletonList(demandProductInfo);
        rateOnDemandReq.setProductInfos(demandProductInfoList);
        request.setBody(rateOnDemandReq);
        return request;
    }

    /**
     * 组装按需查询硬盘产品价格请求体示例
     *
     * @return ListOnDemandResourceRatingsRequest
     */
    private static ListOnDemandResourceRatingsRequest buildRequestForCloudDisk() {
        ListOnDemandResourceRatingsRequest request = new ListOnDemandResourceRatingsRequest();
        RateOnDemandReq rateOnDemandReq = new RateOnDemandReq();
        rateOnDemandReq.setProjectId("<YOUR PROJECT ID>");
        rateOnDemandReq.setInquiryPrecision(0);
        DemandProductInfo demandProductInfo = new DemandProductInfo();
        demandProductInfo.setId("1");
        demandProductInfo.setCloudServiceType("hws.service.type.ebs");
        demandProductInfo.setResourceType("hws.resource.type.volume");
        demandProductInfo.setResourceSpec("SAS");
        demandProductInfo.setResourceSize(500);
        demandProductInfo.setRegion("cn-north-1");
        demandProductInfo.setUsageFactor("Duration");
        demandProductInfo.setSizeMeasureId(17);
        demandProductInfo.setUsageValue(1.0);
        demandProductInfo.setUsageMeasureId(4);
        demandProductInfo.setSubscriptionNum(1);
        List<DemandProductInfo> demandProductInfoList = Collections.singletonList(demandProductInfo);
        rateOnDemandReq.setProductInfos(demandProductInfoList);
        request.setBody(rateOnDemandReq);
        return request;
    }

    /**
     * 组装按需查询弹性IP产品价格请求体示例
     *
     * @return ListOnDemandResourceRatingsRequest
     */
    private static ListOnDemandResourceRatingsRequest buildRequestForEIP() {
        ListOnDemandResourceRatingsRequest request = new ListOnDemandResourceRatingsRequest();
        RateOnDemandReq rateOnDemandReq = new RateOnDemandReq();
        rateOnDemandReq.setProjectId("<YOUR PROJECT ID>");
        rateOnDemandReq.setInquiryPrecision(0);
        DemandProductInfo demandProductInfo = new DemandProductInfo();
        demandProductInfo.setId("1");
        demandProductInfo.setCloudServiceType("hws.service.type.vpc");
        demandProductInfo.setResourceType("hws.resource.type.ip");
        demandProductInfo.setResourceSpec("5_bgp");
        demandProductInfo.setRegion("cn-east-3");
        demandProductInfo.setUsageFactor("Duration");
        demandProductInfo.setUsageValue(1.0);
        demandProductInfo.setUsageMeasureId(4);
        demandProductInfo.setSubscriptionNum(1);
        List<DemandProductInfo> demandProductInfoList = Collections.singletonList(demandProductInfo);
        rateOnDemandReq.setProductInfos(demandProductInfoList);
        request.setBody(rateOnDemandReq);
        return request;
    }

    /**
     * 组装按需查询带宽产品价格请求体示例
     *
     * @return ListOnDemandResourceRatingsRequest
     */
    private static ListOnDemandResourceRatingsRequest buildRequestForBandwidth() {
        ListOnDemandResourceRatingsRequest request = new ListOnDemandResourceRatingsRequest();
        RateOnDemandReq rateOnDemandReq = new RateOnDemandReq();
        rateOnDemandReq.setProjectId("<YOUR PROJECT ID>");
        rateOnDemandReq.setInquiryPrecision(0);
        DemandProductInfo demandProductInfo = new DemandProductInfo();
        demandProductInfo.setId("1");
        demandProductInfo.setCloudServiceType("hws.service.type.vpc");
        demandProductInfo.setResourceType("hws.resource.type.bandwidth");
        demandProductInfo.setResourceSpec("12_sbgp");
        demandProductInfo.setResourceSize(1);
        demandProductInfo.setRegion("cn-north-1");
        demandProductInfo.setAvailableZone("cn-north-1a");
        demandProductInfo.setUsageFactor("upflow");
        demandProductInfo.setSizeMeasureId(15);
        demandProductInfo.setUsageValue(1.0);
        demandProductInfo.setUsageMeasureId(15);
        demandProductInfo.setSubscriptionNum(1);
        List<DemandProductInfo> demandProductInfoList = Collections.singletonList(demandProductInfo);
        rateOnDemandReq.setProductInfos(demandProductInfoList);
        request.setBody(rateOnDemandReq);
        return request;
    }

}