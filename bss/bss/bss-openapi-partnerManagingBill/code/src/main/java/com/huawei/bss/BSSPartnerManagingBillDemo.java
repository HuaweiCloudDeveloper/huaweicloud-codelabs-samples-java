
package com.huawei.bss;

import com.huaweicloud.sdk.bss.v2.BssClient;
import com.huaweicloud.sdk.bss.v2.model.ListConsumeSubCustomersRequest;
import com.huaweicloud.sdk.bss.v2.model.ListConsumeSubCustomersReq;
import com.huaweicloud.sdk.bss.v2.model.ListConsumeSubCustomersResponse;
import com.huaweicloud.sdk.bss.v2.model.ListSubcustomerMonthlyBillsRequest;
import com.huaweicloud.sdk.bss.v2.model.ListSubcustomerMonthlyBillsResponse;
import com.huaweicloud.sdk.bss.v2.model.ListSubCustomerBillDetailResponse;
import com.huaweicloud.sdk.bss.v2.model.ListSubCustomerBillDetailRequest;
import com.huaweicloud.sdk.bss.v2.region.BssRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

public class BSSPartnerManagingBillDemo {
    public static void main(String[] args) {

        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        BssClient client = BssClient.newBuilder()
            .withCredential(auth)
            .withRegion(BssRegion.valueOf("cn-north-1"))
            .build();

        // 1.组装查询伙伴消费子客户列表请求体示例
        ListConsumeSubCustomersRequest listConsumeSubCustomersRequest = buildListConsumeSubCustomersRequest();

        // 2.组装查询客户月度消费账单请求体示例
        ListSubcustomerMonthlyBillsRequest listSubcustomerMonthlyBillsRequest = buildListSubcustomerMonthlyBillsRequest();

        // 3.组装查询伙伴子客户消费记录请求体示例
        ListSubCustomerBillDetailRequest listSubCustomerBillDetailRequest = buildListSubCustomerBillDetailRequest();

        try {
            ListConsumeSubCustomersResponse listConsumeSubCustomersResponse =
                client.listConsumeSubCustomers(listConsumeSubCustomersRequest);
            System.out.println("查询伙伴消费子客户列表响应" + listConsumeSubCustomersResponse.toString());

            ListSubcustomerMonthlyBillsResponse listSubcustomerMonthlyBillsResponse =
                client.listSubcustomerMonthlyBills(listSubcustomerMonthlyBillsRequest);
            System.out.println("查询客户月度消费账单响应" + listSubcustomerMonthlyBillsResponse.toString());

            ListSubCustomerBillDetailResponse listSubCustomerBillDetailResponse =
                client.listSubCustomerBillDetail(listSubCustomerBillDetailRequest);
            System.out.println("查询伙伴子客户消费记录响应" + listSubCustomerBillDetailResponse.toString());

        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    /**
     * 组装查询伙伴消费子客户列表请求体示例
     *
     * @return ListConsumeSubCustomersRequest
     */
    private static ListConsumeSubCustomersRequest buildListConsumeSubCustomersRequest() {
        ListConsumeSubCustomersRequest request = new ListConsumeSubCustomersRequest();
        ListConsumeSubCustomersReq listConsumeSubCustomersReq = new ListConsumeSubCustomersReq();
        listConsumeSubCustomersReq.setBillCycle("<CYCLE>");
        listConsumeSubCustomersReq.setOffset(0);
        listConsumeSubCustomersReq.setLimit(10);
        request.setBody(listConsumeSubCustomersReq);
        return request;
    }

    /**
     * 组装查询客户月度消费账单请求体
     *
     * @return ListSubcustomerMonthlyBillsRequest
     */
    private static ListSubcustomerMonthlyBillsRequest buildListSubcustomerMonthlyBillsRequest() {
        ListSubcustomerMonthlyBillsRequest request = new ListSubcustomerMonthlyBillsRequest();
        request.setCustomerId("<CUSTOMER_ID>");
        request.setCycle("<CYCLE>");
        request.setCloudServiceType("<CLOUD_SERVICE_TYPE>");
        request.setChargeMode("1");
        request.setOffset(0);
        request.setLimit(10);
        request.setBillType(null);
        // 查询云经销商伙伴的子客户的消费汇总账单时携带
        request.setIndirectPartnerId(null);
        return request;
    }

    /**
     * 组装查询伙伴子客户消费记录请求体
     *
     * @return ListSubCustomerBillDetailRequest
     */
    private static ListSubCustomerBillDetailRequest buildListSubCustomerBillDetailRequest() {
        ListSubCustomerBillDetailRequest request = new ListSubCustomerBillDetailRequest();
        request.setXLanguage("<LANGUAGE>");
        request.setBillCycle("<CYCLE>");
        request.setCustomerId("<CUSTOMER_ID>");
        request.setServiceTypeCode("<SERVICE_TYPE_CODE>");
        request.setRegionCode("<REGION_CODE>");
        request.setChargingMode(null);
        request.setBillDetailType(null);
        request.setResourceId("<RESOURCE_ID>");
        request.setResourceName("<RESOURCE_NAME>");
        request.setTradeId(null);
        request.setAccountManagerId("ACCOUNT_MANAGER_ID");
        request.setAssociationType("ASSOCIATION_TYPE");
        request.setOffset(0);
        request.setLimit(10);
        request.setIndirectPartnerId(null);
        request.setBillDateBegin(null);
        request.setBillDateEnd(null);
        return request;
    }

}