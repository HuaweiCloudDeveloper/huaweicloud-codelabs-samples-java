## 1. 功能介绍
- 伙伴在伙伴销售平台可实时查询消费账期内的子客户列表，可以用于查询子客户的消费记录。
- 伙伴在伙伴销售平台可实时查询子客户的消费记录，了解客户的资源消耗情况。
- 合作伙伴可查询客户的消费汇总账单，消费按月汇总。

## 2. 前置条件
- 注册经销商合作伙伴账号和实名认证，并且加入经销商计划。参考[注册经销商伙伴](https://www.huaweicloud.com/partners/resale/)
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

## 3. SDK获取和安装
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-bss”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。
 
## 4. 接口参数说明
#### 4.1 查询伙伴消费子客户列表
接口详情请参考[查询伙伴消费子客户列表](https://support.huaweicloud.com/api-bpconsole/mt_000503.html)

#### 4.2 查询客户月度消费账单
接口详情请参考[查询客户月度消费账单](https://support.huaweicloud.com/api-bpconsole/cb_020001.html)

#### 4.3 查询伙伴子客户消费记录
接口详情请参考[查询伙伴子客户消费记录](https://support.huaweicloud.com/api-bpconsole/mt_000502.html)

## 5. 代码示例
```java
package com.huawei.bss;

import com.huaweicloud.sdk.bss.v2.BssClient;
import com.huaweicloud.sdk.bss.v2.model.ListConsumeSubCustomersRequest;
import com.huaweicloud.sdk.bss.v2.model.ListConsumeSubCustomersReq;
import com.huaweicloud.sdk.bss.v2.model.ListConsumeSubCustomersResponse;
import com.huaweicloud.sdk.bss.v2.model.ListSubcustomerMonthlyBillsRequest;
import com.huaweicloud.sdk.bss.v2.model.ListSubcustomerMonthlyBillsResponse;
import com.huaweicloud.sdk.bss.v2.model.ListSubCustomerBillDetailResponse;
import com.huaweicloud.sdk.bss.v2.model.ListSubCustomerBillDetailRequest;
import com.huaweicloud.sdk.bss.v2.region.BssRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

public class BSSPartnerManagingBillDemo {
    public static void main(String[] args) {

        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        BssClient client = BssClient.newBuilder()
            .withCredential(auth)
            .withRegion(BssRegion.valueOf("cn-north-1"))
            .build();

        // 1.组装查询伙伴消费子客户列表请求体示例
        ListConsumeSubCustomersRequest listConsumeSubCustomersRequest = buildListConsumeSubCustomersRequest();

        // 2.组装查询客户月度消费账单请求体示例
        ListSubcustomerMonthlyBillsRequest listSubcustomerMonthlyBillsRequest = buildListSubcustomerMonthlyBillsRequest();

        // 3.组装查询伙伴子客户消费记录请求体示例
        ListSubCustomerBillDetailRequest listSubCustomerBillDetailRequest = buildListSubCustomerBillDetailRequest();

        try {
            ListConsumeSubCustomersResponse listConsumeSubCustomersResponse =
                client.listConsumeSubCustomers(listConsumeSubCustomersRequest);
            System.out.println("查询伙伴消费子客户列表响应" + listConsumeSubCustomersResponse.toString());

            ListSubcustomerMonthlyBillsResponse listSubcustomerMonthlyBillsResponse =
                client.listSubcustomerMonthlyBills(listSubcustomerMonthlyBillsRequest);
            System.out.println("查询客户月度消费账单响应" + listSubcustomerMonthlyBillsResponse.toString());

            ListSubCustomerBillDetailResponse listSubCustomerBillDetailResponse =
                client.listSubCustomerBillDetail(listSubCustomerBillDetailRequest);
            System.out.println("查询伙伴子客户消费记录响应" + listSubCustomerBillDetailResponse.toString());

        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    /**
     * 组装查询伙伴消费子客户列表请求体示例
     *
     * @return ListConsumeSubCustomersRequest
     */
    private static ListConsumeSubCustomersRequest buildListConsumeSubCustomersRequest() {
        ListConsumeSubCustomersRequest request = new ListConsumeSubCustomersRequest();
        ListConsumeSubCustomersReq listConsumeSubCustomersReq = new ListConsumeSubCustomersReq();
        listConsumeSubCustomersReq.setBillCycle("<CYCLE>");
        listConsumeSubCustomersReq.setOffset(0);
        listConsumeSubCustomersReq.setLimit(10);
        request.setBody(listConsumeSubCustomersReq);
        return request;
    }

    /**
     * 组装查询客户月度消费账单请求体
     *
     * @return ListSubcustomerMonthlyBillsRequest
     */
    private static ListSubcustomerMonthlyBillsRequest buildListSubcustomerMonthlyBillsRequest() {
        ListSubcustomerMonthlyBillsRequest request = new ListSubcustomerMonthlyBillsRequest();
        request.setCustomerId("<CUSTOMER_ID>");
        request.setCycle("<CYCLE>");
        request.setCloudServiceType("<CLOUD_SERVICE_TYPE>");
        request.setChargeMode("1");
        request.setOffset(0);
        request.setLimit(10);
        request.setBillType(null);
        // 查询云经销商伙伴的子客户的消费汇总账单时携带
        request.setIndirectPartnerId(null);
        return request;
    }

    /**
     * 组装查询伙伴子客户消费记录请求体
     *
     * @return ListSubCustomerBillDetailRequest
     */
    private static ListSubCustomerBillDetailRequest buildListSubCustomerBillDetailRequest() {
        ListSubCustomerBillDetailRequest request = new ListSubCustomerBillDetailRequest();
        request.setXLanguage("<LANGUAGE>");
        request.setBillCycle("<CYCLE>");
        request.setCustomerId("<CUSTOMER_ID>");
        request.setServiceTypeCode("<SERVICE_TYPE_CODE>");
        request.setRegionCode("<REGION_CODE>");
        request.setChargingMode(null);
        request.setBillDetailType(null);
        request.setResourceId("<RESOURCE_ID>");
        request.setResourceName("<RESOURCE_NAME>");
        request.setTradeId(null);
        request.setAccountManagerId("ACCOUNT_MANAGER_ID");
        request.setAssociationType("ASSOCIATION_TYPE");
        request.setOffset(0);
        request.setLimit(10);
        request.setIndirectPartnerId(null);
        request.setBillDateBegin(null);
        request.setBillDateEnd(null);
        return request;
    }

}
```

## 修订记录
| 发布日期 | 文档版本 | 修订说明 |
| :----:| :----:| :----:|
|2024-02-29|1.0.0|管理账单（伙伴）场景示例第一个版本发布|