## 1. 介绍

客户在自建平台按照条件查询包年/包月产品开通时候的价格。

如果购买该产品的客户享受折扣，可以在查询结果中返回折扣金额以及扣除折扣后的最后成交价。

如果该客户享受多种折扣，系统会返回每种折扣的批价结果。如果客户在下单的时候选择自动支付，则系统会优先应用商务折扣的批价结果。

`注意事项`：华为云根据云服务类型、资源类型、云服务区和资源规格四个条件来查询产品，查询时请确认这4个查询条件均输入正确，否则该接口会返回无法找到产品的错误。

## 2. 前置条件
- 注册经销商合作伙伴账号和实名认证，并且加入经销商计划。参考[注册经销商伙伴](https://www.huaweicloud.com/partners/resale/)
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

## 3. SDK获取和安装
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-bss”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

## 4. 接口参数说明
关于接口参数的详细说明可参见：[查询包年/包月产品价格](https://support.huaweicloud.com/api-bpconsole/bcloud_01002.html)

## 5. 代码示例
```java
package com.huawei.bss;

import com.huaweicloud.sdk.bss.v2.BssClient;
import com.huaweicloud.sdk.bss.v2.model.ListRateOnPeriodDetailRequest;
import com.huaweicloud.sdk.bss.v2.model.ListRateOnPeriodDetailResponse;
import com.huaweicloud.sdk.bss.v2.model.PeriodProductInfo;
import com.huaweicloud.sdk.bss.v2.model.RateOnPeriodReq;
import com.huaweicloud.sdk.bss.v2.region.BssRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import java.util.Collections;
import java.util.List;

public class BSSListRateOnPeriodDetailDemo {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        BssClient client = BssClient.newBuilder()
            .withCredential(auth)
            .withRegion(BssRegion.valueOf("cn-north-1"))
            .build();

        // 1 组装包年/包月查询云服务器产品价格请求体示例
        ListRateOnPeriodDetailRequest requestForCloudServer = buildRequestForCloudServer();
        // 2 组装包年/包月查询硬盘产品价格请求体示例
        ListRateOnPeriodDetailRequest requestForCloudDisk = buildRequestForCloudDisk();
        // 3 组装包年/包月查询弹性IP产品价格请求体示例
        ListRateOnPeriodDetailRequest requestForEIP = buildRequestForEIP();
        // 4 组装包年/包月查询带宽产品价格请求体示例
        ListRateOnPeriodDetailRequest requestForBandwidth = buildRequestForBandwidth();
        // 5 支持IES基础服务按照半预付模式付款请求体示例
        ListRateOnPeriodDetailRequest requestForIESServer = buildRequestForIESServer();

        try {
            ListRateOnPeriodDetailResponse responseForCloudServer = client.listRateOnPeriodDetail(requestForCloudServer);
            System.out.println("查询云服务器产品价格响应" + responseForCloudServer.toString());
            ListRateOnPeriodDetailResponse responseForCloudDisk = client.listRateOnPeriodDetail(requestForCloudDisk);
            System.out.println("查询硬盘产品价格响应" + responseForCloudDisk.toString());
            ListRateOnPeriodDetailResponse responseForEIP = client.listRateOnPeriodDetail(requestForEIP);
            System.out.println("查询弹性IP产品价格响应" + responseForEIP.toString());
            ListRateOnPeriodDetailResponse responseForBandwidth = client.listRateOnPeriodDetail(requestForBandwidth);
            System.out.println("查询带宽产品价格响应" + responseForBandwidth.toString());
            ListRateOnPeriodDetailResponse responseForIESServer = client.listRateOnPeriodDetail(requestForIESServer);
            System.out.println("查询半预付模式付款响应" + responseForIESServer.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }


    /**
     * 组装查询包周期云服务器产品价格请求体示例
     *
     * @return ListRateOnPeriodDetailRequest
     */
    private static ListRateOnPeriodDetailRequest buildRequestForCloudServer() {
        ListRateOnPeriodDetailRequest request = new ListRateOnPeriodDetailRequest();
        RateOnPeriodReq rateOnPeriodReq = new RateOnPeriodReq();
        rateOnPeriodReq.setProjectId("<YOUR_PROJECT_ID>");
        PeriodProductInfo periodProductInfo = new PeriodProductInfo();
        periodProductInfo.setId("1");
        periodProductInfo.setCloudServiceType("hws.service.type.ec2");
        periodProductInfo.setResourceType("hws.resource.type.vm");
        periodProductInfo.setResourceSpec("s3.medium.4.linux");
        periodProductInfo.setRegion("cn-east-3");
        periodProductInfo.setPeriodType(2);
        periodProductInfo.setPeriodNum(1);
        periodProductInfo.setSubscriptionNum(1);
        List<PeriodProductInfo> productInfos = Collections.singletonList(periodProductInfo);
        rateOnPeriodReq.setProductInfos(productInfos);
        request.setBody(rateOnPeriodReq);
        return request;
    }

    /**
     * 组装查询包周期硬盘产品价格请求体示例
     *
     * @return ListRateOnPeriodDetailRequest
     */
    private static ListRateOnPeriodDetailRequest buildRequestForCloudDisk() {
        ListRateOnPeriodDetailRequest request = new ListRateOnPeriodDetailRequest();
        RateOnPeriodReq rateOnPeriodReq = new RateOnPeriodReq();
        rateOnPeriodReq.setProjectId("<YOUR_PROJECT_ID>");
        PeriodProductInfo periodProductInfo = new PeriodProductInfo();
        periodProductInfo.setId("2");
        periodProductInfo.setCloudServiceType("hws.service.type.ebs");
        periodProductInfo.setResourceType("hws.resource.type.volume");
        periodProductInfo.setResourceSpec("SAS");
        periodProductInfo.setResourceSize(20);
        periodProductInfo.setRegion("cn-north-1");
        periodProductInfo.setSizeMeasureId(17);
        periodProductInfo.setPeriodType(2);
        periodProductInfo.setPeriodNum(1);
        periodProductInfo.setSubscriptionNum(1);
        List<PeriodProductInfo> productInfos = Collections.singletonList(periodProductInfo);
        rateOnPeriodReq.setProductInfos(productInfos);
        request.setBody(rateOnPeriodReq);
        return request;
    }

    /**
     * 组装查询包周期弹性IP产品价格请求体示例
     *
     * @return ListRateOnPeriodDetailRequest
     */
    private static ListRateOnPeriodDetailRequest buildRequestForEIP() {
        ListRateOnPeriodDetailRequest request = new ListRateOnPeriodDetailRequest();
        RateOnPeriodReq rateOnPeriodReq = new RateOnPeriodReq();
        rateOnPeriodReq.setProjectId("<YOUR_PROJECT_ID>");
        PeriodProductInfo periodProductInfo = new PeriodProductInfo();
        periodProductInfo.setId("3");
        periodProductInfo.setCloudServiceType("hws.service.type.vpc");
        periodProductInfo.setResourceType("hws.resource.type.ip");
        periodProductInfo.setResourceSpec("5_bgp");
        periodProductInfo.setRegion("cn-east-3");
        periodProductInfo.setSizeMeasureId(15);
        periodProductInfo.setPeriodType(2);
        periodProductInfo.setSubscriptionNum(1);
        periodProductInfo.setPeriodNum(1);
        List<PeriodProductInfo> productInfos = Collections.singletonList(periodProductInfo);
        rateOnPeriodReq.setProductInfos(productInfos);
        request.setBody(rateOnPeriodReq);
        return request;
    }

    /**
     * 组装查询包周期带宽产品价格请求体示例
     *
     * @return ListRateOnPeriodDetailRequest
     */
    private static ListRateOnPeriodDetailRequest buildRequestForBandwidth() {
        ListRateOnPeriodDetailRequest request = new ListRateOnPeriodDetailRequest();
        RateOnPeriodReq rateOnPeriodReq = new RateOnPeriodReq();
        rateOnPeriodReq.setProjectId("<YOUR_PROJECT_ID>");
        PeriodProductInfo periodProductInfo = new PeriodProductInfo();
        periodProductInfo.setId("4");
        periodProductInfo.setCloudServiceType("hws.service.type.vpc");
        periodProductInfo.setResourceType("hws.resource.type.bandwidth");
        periodProductInfo.setResourceSpec("19_bgp");
        periodProductInfo.setRegion("cn-east-3");
        periodProductInfo.setAvailableZone("cn-east-3a");
        periodProductInfo.setResourceSize(40);
        periodProductInfo.setSizeMeasureId(15);
        periodProductInfo.setPeriodType(2);
        periodProductInfo.setSubscriptionNum(1);
        periodProductInfo.setPeriodNum(1);
        List<PeriodProductInfo> productInfos = Collections.singletonList(periodProductInfo);
        rateOnPeriodReq.setProductInfos(productInfos);
        request.setBody(rateOnPeriodReq);
        return request;
    }

    /**
     * 支持IES基础服务按照半预付模式付款请求体示例
     *
     * @return ListRateOnPeriodDetailRequest
     */
    private static ListRateOnPeriodDetailRequest buildRequestForIESServer() {
        ListRateOnPeriodDetailRequest request = new ListRateOnPeriodDetailRequest();
        RateOnPeriodReq rateOnPeriodReq = new RateOnPeriodReq();
        rateOnPeriodReq.setProjectId("<YOUR_PROJECT_ID>");
        PeriodProductInfo periodProductInfo = new PeriodProductInfo();
        periodProductInfo.setId("5");
        periodProductInfo.setCloudServiceType("hws.service.type.ies");
        periodProductInfo.setResourceType("hws.resource.type.iesservertest");
        periodProductInfo.setResourceSpec("halfpaytest3");
        periodProductInfo.setRegion("global-cbc-1");
        periodProductInfo.setPeriodType(3);
        periodProductInfo.setPeriodNum(1);
        periodProductInfo.setSubscriptionNum(1);
        periodProductInfo.setResourceSize(10);
        periodProductInfo.setSizeMeasureId(10);
        periodProductInfo.setFeeInstallmentMode("HALF_PAY");
        List<PeriodProductInfo> productInfos = Collections.singletonList(periodProductInfo);
        rateOnPeriodReq.setProductInfos(productInfos);
        request.setBody(rateOnPeriodReq);
        return request;
    }

}
```

## 6. 常见问题

#### project_id

项目ID与您询价的产品region无关，填写为当前询价客户名下任意一个有效的project_id即可。

#### resource_spec参数如何获取

参数为云服务类型的资源规格，接口资料上例举了常见云服务的资源规格示例，如您想询价更多的云服务产品，可查看对应云服务的官网资料或联系云服务产品部获取最新的资源规格。

#### resource_size、size_measure_id如何填写

询价线性产品时，resource_size、size_measure_id两个参数为必填。线性产品为包括硬盘，带宽等在订购时需要指定大小的产品。如您分辨不出产品是否线性，可咨询对应的云服务产品部。

两者需搭配使用，resource_size代表资源容量大小，例如云硬盘有10G、20G区分等等；size_measure_id代表资源容量度量标识，例如云硬盘度量标识为GB，此参数需填写17。
#### 如何通过页面快速获取询价参数 [云硬盘包周期询价示例](https://support.huaweicloud.com/api-bpconsole/offl_110009.html)

## 修订记录

| 发布日期 | 文档版本 | 修订说明 |
| :----:| :----:| :----:|
|2024-02-01|1.0.0|查询包年/包月产品价格场景示例第一个版本发布|