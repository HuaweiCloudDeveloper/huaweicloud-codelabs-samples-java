
package com.huawei.bss;

import com.huaweicloud.sdk.bss.v2.BssClient;
import com.huaweicloud.sdk.bss.v2.model.ListRateOnPeriodDetailRequest;
import com.huaweicloud.sdk.bss.v2.model.ListRateOnPeriodDetailResponse;
import com.huaweicloud.sdk.bss.v2.model.PeriodProductInfo;
import com.huaweicloud.sdk.bss.v2.model.RateOnPeriodReq;
import com.huaweicloud.sdk.bss.v2.region.BssRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import java.util.Collections;
import java.util.List;

public class BSSListRateOnPeriodDetailDemo {
    public static void main(String[] args) {
        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        BssClient client = BssClient.newBuilder()
            .withCredential(auth)
            .withRegion(BssRegion.valueOf("cn-north-1"))
            .build();

        // 1 组装包年/包月查询云服务器产品价格请求体示例
        ListRateOnPeriodDetailRequest requestForCloudServer = buildRequestForCloudServer();
        // 2 组装包年/包月查询硬盘产品价格请求体示例
        ListRateOnPeriodDetailRequest requestForCloudDisk = buildRequestForCloudDisk();
        // 3 组装包年/包月查询弹性IP产品价格请求体示例
        ListRateOnPeriodDetailRequest requestForEIP = buildRequestForEIP();
        // 4 组装包年/包月查询带宽产品价格请求体示例
        ListRateOnPeriodDetailRequest requestForBandwidth = buildRequestForBandwidth();
        // 5 支持IES基础服务按照半预付模式付款请求体示例
        ListRateOnPeriodDetailRequest requestForIESServer = buildRequestForIESServer();

        try {
            ListRateOnPeriodDetailResponse responseForCloudServer = client.listRateOnPeriodDetail(requestForCloudServer);
            System.out.println("查询云服务器产品价格响应" + responseForCloudServer.toString());
            ListRateOnPeriodDetailResponse responseForCloudDisk = client.listRateOnPeriodDetail(requestForCloudDisk);
            System.out.println("查询硬盘产品价格响应" + responseForCloudDisk.toString());
            ListRateOnPeriodDetailResponse responseForEIP = client.listRateOnPeriodDetail(requestForEIP);
            System.out.println("查询弹性IP产品价格响应" + responseForEIP.toString());
            ListRateOnPeriodDetailResponse responseForBandwidth = client.listRateOnPeriodDetail(requestForBandwidth);
            System.out.println("查询带宽产品价格响应" + responseForBandwidth.toString());
            ListRateOnPeriodDetailResponse responseForIESServer = client.listRateOnPeriodDetail(requestForIESServer);
            System.out.println("查询半预付模式付款响应" + responseForIESServer.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }


    /**
     * 组装查询包周期云服务器产品价格请求体示例
     *
     * @return ListRateOnPeriodDetailRequest
     */
    private static ListRateOnPeriodDetailRequest buildRequestForCloudServer() {
        ListRateOnPeriodDetailRequest request = new ListRateOnPeriodDetailRequest();
        RateOnPeriodReq rateOnPeriodReq = new RateOnPeriodReq();
        rateOnPeriodReq.setProjectId("<YOUR_PROJECT_ID>");
        PeriodProductInfo periodProductInfo = new PeriodProductInfo();
        periodProductInfo.setId("1");
        periodProductInfo.setCloudServiceType("hws.service.type.ec2");
        periodProductInfo.setResourceType("hws.resource.type.vm");
        periodProductInfo.setResourceSpec("s3.medium.4.linux");
        periodProductInfo.setRegion("cn-east-3");
        periodProductInfo.setPeriodType(2);
        periodProductInfo.setPeriodNum(1);
        periodProductInfo.setSubscriptionNum(1);
        List<PeriodProductInfo> productInfos = Collections.singletonList(periodProductInfo);
        rateOnPeriodReq.setProductInfos(productInfos);
        request.setBody(rateOnPeriodReq);
        return request;
    }

    /**
     * 组装查询包周期硬盘产品价格请求体示例
     *
     * @return ListRateOnPeriodDetailRequest
     */
    private static ListRateOnPeriodDetailRequest buildRequestForCloudDisk() {
        ListRateOnPeriodDetailRequest request = new ListRateOnPeriodDetailRequest();
        RateOnPeriodReq rateOnPeriodReq = new RateOnPeriodReq();
        rateOnPeriodReq.setProjectId("<YOUR_PROJECT_ID>");
        PeriodProductInfo periodProductInfo = new PeriodProductInfo();
        periodProductInfo.setId("2");
        periodProductInfo.setCloudServiceType("hws.service.type.ebs");
        periodProductInfo.setResourceType("hws.resource.type.volume");
        periodProductInfo.setResourceSpec("SAS");
        periodProductInfo.setResourceSize(20);
        periodProductInfo.setRegion("cn-north-1");
        periodProductInfo.setSizeMeasureId(17);
        periodProductInfo.setPeriodType(2);
        periodProductInfo.setPeriodNum(1);
        periodProductInfo.setSubscriptionNum(1);
        List<PeriodProductInfo> productInfos = Collections.singletonList(periodProductInfo);
        rateOnPeriodReq.setProductInfos(productInfos);
        request.setBody(rateOnPeriodReq);
        return request;
    }

    /**
     * 组装查询包周期弹性IP产品价格请求体示例
     *
     * @return ListRateOnPeriodDetailRequest
     */
    private static ListRateOnPeriodDetailRequest buildRequestForEIP() {
        ListRateOnPeriodDetailRequest request = new ListRateOnPeriodDetailRequest();
        RateOnPeriodReq rateOnPeriodReq = new RateOnPeriodReq();
        rateOnPeriodReq.setProjectId("<YOUR_PROJECT_ID>");
        PeriodProductInfo periodProductInfo = new PeriodProductInfo();
        periodProductInfo.setId("3");
        periodProductInfo.setCloudServiceType("hws.service.type.vpc");
        periodProductInfo.setResourceType("hws.resource.type.ip");
        periodProductInfo.setResourceSpec("5_bgp");
        periodProductInfo.setRegion("cn-east-3");
        periodProductInfo.setSizeMeasureId(15);
        periodProductInfo.setPeriodType(2);
        periodProductInfo.setSubscriptionNum(1);
        periodProductInfo.setPeriodNum(1);
        List<PeriodProductInfo> productInfos = Collections.singletonList(periodProductInfo);
        rateOnPeriodReq.setProductInfos(productInfos);
        request.setBody(rateOnPeriodReq);
        return request;
    }

    /**
     * 组装查询包周期带宽产品价格请求体示例
     *
     * @return ListRateOnPeriodDetailRequest
     */
    private static ListRateOnPeriodDetailRequest buildRequestForBandwidth() {
        ListRateOnPeriodDetailRequest request = new ListRateOnPeriodDetailRequest();
        RateOnPeriodReq rateOnPeriodReq = new RateOnPeriodReq();
        rateOnPeriodReq.setProjectId("<YOUR_PROJECT_ID>");
        PeriodProductInfo periodProductInfo = new PeriodProductInfo();
        periodProductInfo.setId("4");
        periodProductInfo.setCloudServiceType("hws.service.type.vpc");
        periodProductInfo.setResourceType("hws.resource.type.bandwidth");
        periodProductInfo.setResourceSpec("19_bgp");
        periodProductInfo.setRegion("cn-east-3");
        periodProductInfo.setAvailableZone("cn-east-3a");
        periodProductInfo.setResourceSize(40);
        periodProductInfo.setSizeMeasureId(15);
        periodProductInfo.setPeriodType(2);
        periodProductInfo.setSubscriptionNum(1);
        periodProductInfo.setPeriodNum(1);
        List<PeriodProductInfo> productInfos = Collections.singletonList(periodProductInfo);
        rateOnPeriodReq.setProductInfos(productInfos);
        request.setBody(rateOnPeriodReq);
        return request;
    }

    /**
     * 支持IES基础服务按照半预付模式付款请求体示例
     *
     * @return ListRateOnPeriodDetailRequest
     */
    private static ListRateOnPeriodDetailRequest buildRequestForIESServer() {
        ListRateOnPeriodDetailRequest request = new ListRateOnPeriodDetailRequest();
        RateOnPeriodReq rateOnPeriodReq = new RateOnPeriodReq();
        rateOnPeriodReq.setProjectId("<YOUR_PROJECT_ID>");
        PeriodProductInfo periodProductInfo = new PeriodProductInfo();
        periodProductInfo.setId("5");
        periodProductInfo.setCloudServiceType("hws.service.type.ies");
        periodProductInfo.setResourceType("hws.resource.type.iesservertest");
        periodProductInfo.setResourceSpec("halfpaytest3");
        periodProductInfo.setRegion("global-cbc-1");
        periodProductInfo.setPeriodType(3);
        periodProductInfo.setPeriodNum(1);
        periodProductInfo.setSubscriptionNum(1);
        periodProductInfo.setResourceSize(10);
        periodProductInfo.setSizeMeasureId(10);
        periodProductInfo.setFeeInstallmentMode("HALF_PAY");
        List<PeriodProductInfo> productInfos = Collections.singletonList(periodProductInfo);
        rateOnPeriodReq.setProductInfos(productInfos);
        request.setBody(rateOnPeriodReq);
        return request;
    }

}