## 1. 功能介绍
客户在合作伙伴销售平台可以通过营业执照、个人证件等方式完成实名认证。华为云负责审核客户认证信息及附件，并将结果返回给伙伴销售平台。

如果客户由个人认证转为企业认证，可在伙伴销售平台重新完成实名认证。

## 2. 前置条件
- 注册经销商合作伙伴账号和实名认证，并且加入经销商计划。参考[注册经销商伙伴](https://www.huaweicloud.com/partners/resale/)
- 已具备开发环境 ，支持Java JDK 1.8及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

## 3. SDK获取和安装
您可以通过Maven方式获取和安装SDK，首先需要在您的操作系统中下载并安装Maven ，安装完成后您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。

使用服务端SDK前，您需要安装“huaweicloud-sdk-bss”，具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java) 。

## 4. 调用流程
#### 4.1 个人证件实名认证：
 ![图1: 个人证件实名认证](./assets/realnameAuth01.png "个人证件实名认证图例")
 
#### 4.2 个人银行卡实名认证：
 ![图2: 个人银行卡实名认证](./assets/realnameAuth02.png "个人银行卡实名认证图例")
 
#### 4.3 企业实名认证：
 ![图3: 企业实名认证](./assets/realnameAuth03.png "企业实名认证图例")
 
#### 4.4 实名认证变更：
 ![图4: 实名认证变更](./assets/realnameAuth04.png "实名认证变更图例")
 

## 5. 接口参数说明
#### 5.1 申请个人实名认证
个人客户可以进行个人实名认证申请。

接口参数的详细说明可参见：[申请个人实名认证](https://support.huaweicloud.com/api-bpconsole/mc_00017.html)

#### 5.2 申请企业实名认证
企业客户可以进行企业实名认证申请。

接口参数的详细说明可参见：[申请企业实名认证](https://support.huaweicloud.com/api-bpconsole/mc_00018.html)

#### 5.3 申请实名认证变更
客户可以进行实名认证变更申请。

接口参数的详细说明可参见：[申请实名认证变更](https://support.huaweicloud.com/api-bpconsole/mc_00019.html)

#### 5.4 查询实名认证审核结果
如果实名认证申请或实名认证变更申请的响应中，显示需要人工审核，使用该接口查询审核结果。

接口参数的详细说明可参见：[查询实名认证审核结果](https://support.huaweicloud.com/api-bpconsole/mc_00020.html)


## 6.  java代码示例
```java
package com.huawei.bss;

import com.huaweicloud.sdk.bss.v2.BssClient;
import com.huaweicloud.sdk.bss.v2.model.CreatePersonalRealnameAuthRequest;
import com.huaweicloud.sdk.bss.v2.model.CreateEnterpriseRealnameAuthenticationRequest;
import com.huaweicloud.sdk.bss.v2.model.ChangeEnterpriseRealnameAuthenticationRequest;
import com.huaweicloud.sdk.bss.v2.model.ShowRealnameAuthenticationReviewResultRequest;
import com.huaweicloud.sdk.bss.v2.model.CreatePersonalRealnameAuthResponse;
import com.huaweicloud.sdk.bss.v2.model.CreateEnterpriseRealnameAuthenticationResponse;
import com.huaweicloud.sdk.bss.v2.model.ChangeEnterpriseRealnameAuthenticationResponse;
import com.huaweicloud.sdk.bss.v2.model.ShowRealnameAuthenticationReviewResultResponse;
import com.huaweicloud.sdk.bss.v2.model.ApplyIndividualRealnameAuthsReq;
import com.huaweicloud.sdk.bss.v2.model.ApplyEnterpriseRealnameAuthsReq;
import com.huaweicloud.sdk.bss.v2.model.EnterprisePersonNew;
import com.huaweicloud.sdk.bss.v2.model.ChangeEnterpriseRealnameAuthsReq;
import com.huaweicloud.sdk.bss.v2.model.BankCardInfoV2;
import com.huaweicloud.sdk.bss.v2.region.BssRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import java.util.Arrays;
import java.util.Collections;

public class BSSRealNameAuthDemo {

    public static void main(String[] args) {

        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        BssClient client = BssClient.newBuilder()
            .withCredential(auth)
            .withRegion(BssRegion.valueOf("cn-north-1"))
            .build();

        // 1.组装个人证件实名认证请求体
        CreatePersonalRealnameAuthRequest createPersonalCertificatesRealnameAuthRequest = buildCreatePersonalCertificatesRealnameAuthRequest();

        // 2.组装个人银行卡实名认证请求体
        CreatePersonalRealnameAuthRequest createPersonalBankCardRealnameAuthRequest = buildCreatePersonalBankCardRealnameAuthRequest();

        // 3.组装企业实名认证请求体
        CreateEnterpriseRealnameAuthenticationRequest createEnterpriseRealnameAuthenticationRequest = buildCreateEnterpriseRealnameAuthenticationRequest();

        // 4.组装实名认证变更请求体
        ChangeEnterpriseRealnameAuthenticationRequest changeEnterpriseRealnameAuthenticationRequest = buildChangeEnterpriseRealnameAuthenticationRequest();

        // 5.组装查询实名认证审核结果请求体
        ShowRealnameAuthenticationReviewResultRequest showRealnameAuthenticationReviewResultRequest = buildShowRealnameAuthenticationReviewResultRequest();

        try {
            CreatePersonalRealnameAuthResponse createPersonalRealnameAuthResponse =
                client.createPersonalRealnameAuth(createPersonalCertificatesRealnameAuthRequest);
            System.out.println("申请个人证件实名认证响应" + createPersonalRealnameAuthResponse.toString());

            CreatePersonalRealnameAuthResponse createPersonalBankCardRealnameAuthResponse =
                client.createPersonalRealnameAuth(createPersonalBankCardRealnameAuthRequest);
            System.out.println("申请个人银行卡实名认证响应" + createPersonalBankCardRealnameAuthResponse.toString());

            CreateEnterpriseRealnameAuthenticationResponse createEnterpriseRealnameAuthenticationResponse =
                client.createEnterpriseRealnameAuthentication(createEnterpriseRealnameAuthenticationRequest);
            System.out.println("申请企业实名认证响应" + createEnterpriseRealnameAuthenticationResponse.toString());

            ChangeEnterpriseRealnameAuthenticationResponse changeEnterpriseRealnameAuthenticationResponse =
                client.changeEnterpriseRealnameAuthentication(changeEnterpriseRealnameAuthenticationRequest);
            System.out.println("申请实名认证变更响应" + changeEnterpriseRealnameAuthenticationResponse.toString());

            ShowRealnameAuthenticationReviewResultResponse showRealnameAuthenticationReviewResultResponse =
                client.showRealnameAuthenticationReviewResult(showRealnameAuthenticationReviewResultRequest);
            System.out.println("查询实名认证审核结果响应" + showRealnameAuthenticationReviewResultResponse.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    /**
     * 组装个人证件实名认证请求体
     *
     * @return CreatePersonalRealnameAuthRequest
     */
    private static CreatePersonalRealnameAuthRequest buildCreatePersonalCertificatesRealnameAuthRequest() {
        CreatePersonalRealnameAuthRequest request = new CreatePersonalRealnameAuthRequest();
        ApplyIndividualRealnameAuthsReq applyIndividualRealnameAuthsReq = new ApplyIndividualRealnameAuthsReq();
        applyIndividualRealnameAuthsReq.setCustomerId("<YOUR_CUSTOMER_ID>");
        applyIndividualRealnameAuthsReq.setIdentifyType(0);
        applyIndividualRealnameAuthsReq.setVerifiedFileUrl(Arrays.asList("shimingrenzheng/zhengmian001.jpg",
            "shimingrenzheng/fanmian002.jpg", "shimingrenzheng/chizheng003.jpg"));
        applyIndividualRealnameAuthsReq.setName("<YOUR_NAME>");
        applyIndividualRealnameAuthsReq.setVerifiedNumber("<YOUR_VERIFIED_NUMBER>");
        applyIndividualRealnameAuthsReq.setXaccountType("<YOUR_XACCOUNT_TYPE>");
        applyIndividualRealnameAuthsReq.setVerifiedType(0);
        applyIndividualRealnameAuthsReq.setChangeType(-1);
        request.setBody(applyIndividualRealnameAuthsReq);
        return request;
    }

    /**
     * 组装个人银行卡实名认证请求体
     *
     * @return CreatePersonalRealnameAuthRequest
     */
    private static CreatePersonalRealnameAuthRequest buildCreatePersonalBankCardRealnameAuthRequest() {
        CreatePersonalRealnameAuthRequest request = new CreatePersonalRealnameAuthRequest();
        ApplyIndividualRealnameAuthsReq applyIndividualRealnameAuthsReq = new ApplyIndividualRealnameAuthsReq();
        applyIndividualRealnameAuthsReq.setCustomerId("<YOUR_CUSTOMER_ID>");
        applyIndividualRealnameAuthsReq.setIdentifyType(4);
        applyIndividualRealnameAuthsReq.setVerifiedFileUrl(Collections.singletonList("shimingrenzheng/gerensaolian001.jpg"));
        applyIndividualRealnameAuthsReq.setName("<YOUR_NAME>");
        applyIndividualRealnameAuthsReq.setVerifiedNumber("<YOUR_VERIFIED_NUMBER>");
        applyIndividualRealnameAuthsReq.setXaccountType("<YOUR_XACCOUNT_TYPE>");
        applyIndividualRealnameAuthsReq.setVerifiedType(null);
        applyIndividualRealnameAuthsReq.setChangeType(-1);
        BankCardInfoV2 bankCardInfo = new BankCardInfoV2();
        bankCardInfo.setBankAccount("<YOUR_BANK_ACCOUNT>");
        bankCardInfo.setAreacode("0086");
        bankCardInfo.setMobile("<YOUR_MOBILE>");
        bankCardInfo.setVerificationCode("<VERIFICATION_CODE>");
        applyIndividualRealnameAuthsReq.setBankCardInfo(bankCardInfo);
        request.setBody(applyIndividualRealnameAuthsReq);
        return request;
    }

    /**
     * 组装企业实名认证请求体
     *
     * @return CreateEnterpriseRealnameAuthenticationRequest
     */
    private static CreateEnterpriseRealnameAuthenticationRequest buildCreateEnterpriseRealnameAuthenticationRequest() {
        CreateEnterpriseRealnameAuthenticationRequest request = new CreateEnterpriseRealnameAuthenticationRequest();
        ApplyEnterpriseRealnameAuthsReq applyEnterpriseRealnameAuthsReq = new ApplyEnterpriseRealnameAuthsReq();
        applyEnterpriseRealnameAuthsReq.setCustomerId("<YOUR_CUSTOMER_ID>");
        applyEnterpriseRealnameAuthsReq.setIdentifyType(1);
        applyEnterpriseRealnameAuthsReq.setVerifiedFileUrl(Arrays.asList("shimingrenzheng/danweizhengjian001.jpg",
            "shimingrenzheng/renxiang002.jpg", "shimingrenzheng/guohui003.jpg"));
        applyEnterpriseRealnameAuthsReq.setCorpName("<YOUR VERIFIED_NUMBER>");
        applyEnterpriseRealnameAuthsReq.setVerifiedNumber("<YOUR_VERIFIED_NUMBER>");
        applyEnterpriseRealnameAuthsReq.setXaccountType("<YOUR XACCOUNT_TYPE>");
        applyEnterpriseRealnameAuthsReq.setCertificateType(0);
        applyEnterpriseRealnameAuthsReq.setRegCountry("CN");
        applyEnterpriseRealnameAuthsReq.setRegAddress("<YOUR_REG_ADDRESS>");
        EnterprisePersonNew enterprisePerson = new EnterprisePersonNew();
        enterprisePerson.setLegelName("<YOUR_LEGEL_NAME>");
        enterprisePerson.setLegelIdNumber("<YOUR_LEGEL_IDNUMBER>");
        enterprisePerson.setCertifierRole("legalPerson");
        applyEnterpriseRealnameAuthsReq.setEnterprisePerson(enterprisePerson);
        request.setBody(applyEnterpriseRealnameAuthsReq);
        return request;
    }

    /**
     * 组装实名认证变更请求体
     *
     * @return ChangeEnterpriseRealnameAuthenticationRequest
     */
    private static ChangeEnterpriseRealnameAuthenticationRequest buildChangeEnterpriseRealnameAuthenticationRequest() {
        ChangeEnterpriseRealnameAuthenticationRequest request = new ChangeEnterpriseRealnameAuthenticationRequest();
        ChangeEnterpriseRealnameAuthsReq changeEnterpriseRealnameAuthsReq = new ChangeEnterpriseRealnameAuthsReq();
        changeEnterpriseRealnameAuthsReq.setCustomerId("<YOUR_CUSTOMER_ID>");
        changeEnterpriseRealnameAuthsReq.setIdentifyType(1);
        changeEnterpriseRealnameAuthsReq.setVerifiedFileUrl(Arrays.asList("shimingrenzheng/danweizhengjian001.jpg",
            "shimingrenzheng/renxiang002.jpg", "shimingrenzheng/guohui003.jpg"));
        changeEnterpriseRealnameAuthsReq.setCorpName("<YOUR_CORP_NAME>");
        changeEnterpriseRealnameAuthsReq.setVerifiedNumber("<YOUR_VERIFIED_NUMBER>");
        changeEnterpriseRealnameAuthsReq.setChangeType(1);
        changeEnterpriseRealnameAuthsReq.setXaccountType("<YOUR_XACCOUNT_TYPE>");
        changeEnterpriseRealnameAuthsReq.setCertificateType(0);
        changeEnterpriseRealnameAuthsReq.setRegCountry("CN");
        changeEnterpriseRealnameAuthsReq.setRegAddress("<YOUR_REG_ADDRESS>");
        EnterprisePersonNew enterprisePerson = new EnterprisePersonNew();
        enterprisePerson.setLegelName("<YOUR_LEGEL_NAME>");
        enterprisePerson.setLegelIdNumber("<YOUR_LEGEL_IDNUMBER>");
        enterprisePerson.setCertifierRole("legalPerson");
        changeEnterpriseRealnameAuthsReq.setEnterprisePerson(enterprisePerson);
        request.setBody(changeEnterpriseRealnameAuthsReq);
        return request;
    }

    /**
     * 组装查询实名认证审核结果请求体
     *
     * @return ShowRealnameAuthenticationReviewResultRequest
     */
    private static ShowRealnameAuthenticationReviewResultRequest buildShowRealnameAuthenticationReviewResultRequest() {
        ShowRealnameAuthenticationReviewResultRequest request = new ShowRealnameAuthenticationReviewResultRequest();
        request.setCustomerId("<YOUR CUSTOMER_ID>");
        return request;
    }

}
```

## 修订记录
| 发布日期 | 文档版本 | 修订说明 |
| :----:| :----:| :----:|
|2024-02-22|1.0.0|实名认证（伙伴）场景示例第一个版本发布|