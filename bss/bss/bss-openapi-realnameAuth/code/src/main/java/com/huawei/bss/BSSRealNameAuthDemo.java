package com.huawei.bss;

import com.huaweicloud.sdk.bss.v2.BssClient;
import com.huaweicloud.sdk.bss.v2.model.CreatePersonalRealnameAuthRequest;
import com.huaweicloud.sdk.bss.v2.model.CreateEnterpriseRealnameAuthenticationRequest;
import com.huaweicloud.sdk.bss.v2.model.ChangeEnterpriseRealnameAuthenticationRequest;
import com.huaweicloud.sdk.bss.v2.model.ShowRealnameAuthenticationReviewResultRequest;
import com.huaweicloud.sdk.bss.v2.model.CreatePersonalRealnameAuthResponse;
import com.huaweicloud.sdk.bss.v2.model.CreateEnterpriseRealnameAuthenticationResponse;
import com.huaweicloud.sdk.bss.v2.model.ChangeEnterpriseRealnameAuthenticationResponse;
import com.huaweicloud.sdk.bss.v2.model.ShowRealnameAuthenticationReviewResultResponse;
import com.huaweicloud.sdk.bss.v2.model.ApplyIndividualRealnameAuthsReq;
import com.huaweicloud.sdk.bss.v2.model.ApplyEnterpriseRealnameAuthsReq;
import com.huaweicloud.sdk.bss.v2.model.EnterprisePersonNew;
import com.huaweicloud.sdk.bss.v2.model.ChangeEnterpriseRealnameAuthsReq;
import com.huaweicloud.sdk.bss.v2.model.BankCardInfoV2;
import com.huaweicloud.sdk.bss.v2.region.BssRegion;
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;

import java.util.Arrays;
import java.util.Collections;

public class BSSRealNameAuthDemo {

    public static void main(String[] args) {

        // 认证用的ak和sk直接写到代码中有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
        // 本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
        String ak = System.getenv("HUAWEICLOUD_SDK_AK");
        String sk = System.getenv("HUAWEICLOUD_SDK_SK");

        ICredential auth = new GlobalCredentials()
            .withAk(ak)
            .withSk(sk);

        BssClient client = BssClient.newBuilder()
            .withCredential(auth)
            .withRegion(BssRegion.valueOf("cn-north-1"))
            .build();

        // 1.组装个人证件实名认证请求体
        CreatePersonalRealnameAuthRequest createPersonalCertificatesRealnameAuthRequest = buildCreatePersonalCertificatesRealnameAuthRequest();

        // 2.组装个人银行卡实名认证请求体
        CreatePersonalRealnameAuthRequest createPersonalBankCardRealnameAuthRequest = buildCreatePersonalBankCardRealnameAuthRequest();

        // 3.组装企业实名认证请求体
        CreateEnterpriseRealnameAuthenticationRequest createEnterpriseRealnameAuthenticationRequest = buildCreateEnterpriseRealnameAuthenticationRequest();

        // 4.组装实名认证变更请求体
        ChangeEnterpriseRealnameAuthenticationRequest changeEnterpriseRealnameAuthenticationRequest = buildChangeEnterpriseRealnameAuthenticationRequest();

        // 5.组装查询实名认证审核结果请求体
        ShowRealnameAuthenticationReviewResultRequest showRealnameAuthenticationReviewResultRequest = buildShowRealnameAuthenticationReviewResultRequest();

        try {
            CreatePersonalRealnameAuthResponse createPersonalRealnameAuthResponse =
                client.createPersonalRealnameAuth(createPersonalCertificatesRealnameAuthRequest);
            System.out.println("申请个人证件实名认证响应" + createPersonalRealnameAuthResponse.toString());

            CreatePersonalRealnameAuthResponse createPersonalBankCardRealnameAuthResponse =
                client.createPersonalRealnameAuth(createPersonalBankCardRealnameAuthRequest);
            System.out.println("申请个人银行卡实名认证响应" + createPersonalBankCardRealnameAuthResponse.toString());

            CreateEnterpriseRealnameAuthenticationResponse createEnterpriseRealnameAuthenticationResponse =
                client.createEnterpriseRealnameAuthentication(createEnterpriseRealnameAuthenticationRequest);
            System.out.println("申请企业实名认证响应" + createEnterpriseRealnameAuthenticationResponse.toString());

            ChangeEnterpriseRealnameAuthenticationResponse changeEnterpriseRealnameAuthenticationResponse =
                client.changeEnterpriseRealnameAuthentication(changeEnterpriseRealnameAuthenticationRequest);
            System.out.println("申请实名认证变更响应" + changeEnterpriseRealnameAuthenticationResponse.toString());

            ShowRealnameAuthenticationReviewResultResponse showRealnameAuthenticationReviewResultResponse =
                client.showRealnameAuthenticationReviewResult(showRealnameAuthenticationReviewResultRequest);
            System.out.println("查询实名认证审核结果响应" + showRealnameAuthenticationReviewResultResponse.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    /**
     * 组装个人证件实名认证请求体
     *
     * @return CreatePersonalRealnameAuthRequest
     */
    private static CreatePersonalRealnameAuthRequest buildCreatePersonalCertificatesRealnameAuthRequest() {
        CreatePersonalRealnameAuthRequest request = new CreatePersonalRealnameAuthRequest();
        ApplyIndividualRealnameAuthsReq applyIndividualRealnameAuthsReq = new ApplyIndividualRealnameAuthsReq();
        applyIndividualRealnameAuthsReq.setCustomerId("<YOUR_CUSTOMER_ID>");
        applyIndividualRealnameAuthsReq.setIdentifyType(0);
        applyIndividualRealnameAuthsReq.setVerifiedFileUrl(Arrays.asList("shimingrenzheng/zhengmian001.jpg",
            "shimingrenzheng/fanmian002.jpg", "shimingrenzheng/chizheng003.jpg"));
        applyIndividualRealnameAuthsReq.setName("<YOUR_NAME>");
        applyIndividualRealnameAuthsReq.setVerifiedNumber("<YOUR_VERIFIED_NUMBER>");
        applyIndividualRealnameAuthsReq.setXaccountType("<YOUR_XACCOUNT_TYPE>");
        applyIndividualRealnameAuthsReq.setVerifiedType(0);
        applyIndividualRealnameAuthsReq.setChangeType(-1);
        request.setBody(applyIndividualRealnameAuthsReq);
        return request;
    }

    /**
     * 组装个人银行卡实名认证请求体
     *
     * @return CreatePersonalRealnameAuthRequest
     */
    private static CreatePersonalRealnameAuthRequest buildCreatePersonalBankCardRealnameAuthRequest() {
        CreatePersonalRealnameAuthRequest request = new CreatePersonalRealnameAuthRequest();
        ApplyIndividualRealnameAuthsReq applyIndividualRealnameAuthsReq = new ApplyIndividualRealnameAuthsReq();
        applyIndividualRealnameAuthsReq.setCustomerId("<YOUR_CUSTOMER_ID>");
        applyIndividualRealnameAuthsReq.setIdentifyType(4);
        applyIndividualRealnameAuthsReq.setVerifiedFileUrl(Collections.singletonList("shimingrenzheng/gerensaolian001.jpg"));
        applyIndividualRealnameAuthsReq.setName("<YOUR_NAME>");
        applyIndividualRealnameAuthsReq.setVerifiedNumber("<YOUR_VERIFIED_NUMBER>");
        applyIndividualRealnameAuthsReq.setXaccountType("<YOUR_XACCOUNT_TYPE>");
        applyIndividualRealnameAuthsReq.setVerifiedType(null);
        applyIndividualRealnameAuthsReq.setChangeType(-1);
        BankCardInfoV2 bankCardInfo = new BankCardInfoV2();
        bankCardInfo.setBankAccount("<YOUR_BANK_ACCOUNT>");
        bankCardInfo.setAreacode("0086");
        bankCardInfo.setMobile("<YOUR_MOBILE>");
        bankCardInfo.setVerificationCode("<VERIFICATION_CODE>");
        applyIndividualRealnameAuthsReq.setBankCardInfo(bankCardInfo);
        request.setBody(applyIndividualRealnameAuthsReq);
        return request;
    }

    /**
     * 组装企业实名认证请求体
     *
     * @return CreateEnterpriseRealnameAuthenticationRequest
     */
    private static CreateEnterpriseRealnameAuthenticationRequest buildCreateEnterpriseRealnameAuthenticationRequest() {
        CreateEnterpriseRealnameAuthenticationRequest request = new CreateEnterpriseRealnameAuthenticationRequest();
        ApplyEnterpriseRealnameAuthsReq applyEnterpriseRealnameAuthsReq = new ApplyEnterpriseRealnameAuthsReq();
        applyEnterpriseRealnameAuthsReq.setCustomerId("<YOUR_CUSTOMER_ID>");
        applyEnterpriseRealnameAuthsReq.setIdentifyType(1);
        applyEnterpriseRealnameAuthsReq.setVerifiedFileUrl(Arrays.asList("shimingrenzheng/danweizhengjian001.jpg",
            "shimingrenzheng/renxiang002.jpg", "shimingrenzheng/guohui003.jpg"));
        applyEnterpriseRealnameAuthsReq.setCorpName("<YOUR VERIFIED_NUMBER>");
        applyEnterpriseRealnameAuthsReq.setVerifiedNumber("<YOUR_VERIFIED_NUMBER>");
        applyEnterpriseRealnameAuthsReq.setXaccountType("<YOUR XACCOUNT_TYPE>");
        applyEnterpriseRealnameAuthsReq.setCertificateType(0);
        applyEnterpriseRealnameAuthsReq.setRegCountry("CN");
        applyEnterpriseRealnameAuthsReq.setRegAddress("<YOUR_REG_ADDRESS>");
        EnterprisePersonNew enterprisePerson = new EnterprisePersonNew();
        enterprisePerson.setLegelName("<YOUR_LEGEL_NAME>");
        enterprisePerson.setLegelIdNumber("<YOUR_LEGEL_IDNUMBER>");
        enterprisePerson.setCertifierRole("legalPerson");
        applyEnterpriseRealnameAuthsReq.setEnterprisePerson(enterprisePerson);
        request.setBody(applyEnterpriseRealnameAuthsReq);
        return request;
    }

    /**
     * 组装实名认证变更请求体
     *
     * @return ChangeEnterpriseRealnameAuthenticationRequest
     */
    private static ChangeEnterpriseRealnameAuthenticationRequest buildChangeEnterpriseRealnameAuthenticationRequest() {
        ChangeEnterpriseRealnameAuthenticationRequest request = new ChangeEnterpriseRealnameAuthenticationRequest();
        ChangeEnterpriseRealnameAuthsReq changeEnterpriseRealnameAuthsReq = new ChangeEnterpriseRealnameAuthsReq();
        changeEnterpriseRealnameAuthsReq.setCustomerId("<YOUR_CUSTOMER_ID>");
        changeEnterpriseRealnameAuthsReq.setIdentifyType(1);
        changeEnterpriseRealnameAuthsReq.setVerifiedFileUrl(Arrays.asList("shimingrenzheng/danweizhengjian001.jpg",
            "shimingrenzheng/renxiang002.jpg", "shimingrenzheng/guohui003.jpg"));
        changeEnterpriseRealnameAuthsReq.setCorpName("<YOUR_CORP_NAME>");
        changeEnterpriseRealnameAuthsReq.setVerifiedNumber("<YOUR_VERIFIED_NUMBER>");
        changeEnterpriseRealnameAuthsReq.setChangeType(1);
        changeEnterpriseRealnameAuthsReq.setXaccountType("<YOUR_XACCOUNT_TYPE>");
        changeEnterpriseRealnameAuthsReq.setCertificateType(0);
        changeEnterpriseRealnameAuthsReq.setRegCountry("CN");
        changeEnterpriseRealnameAuthsReq.setRegAddress("<YOUR_REG_ADDRESS>");
        EnterprisePersonNew enterprisePerson = new EnterprisePersonNew();
        enterprisePerson.setLegelName("<YOUR_LEGEL_NAME>");
        enterprisePerson.setLegelIdNumber("<YOUR_LEGEL_IDNUMBER>");
        enterprisePerson.setCertifierRole("legalPerson");
        changeEnterpriseRealnameAuthsReq.setEnterprisePerson(enterprisePerson);
        request.setBody(changeEnterpriseRealnameAuthsReq);
        return request;
    }

    /**
     * 组装查询实名认证审核结果请求体
     *
     * @return ShowRealnameAuthenticationReviewResultRequest
     */
    private static ShowRealnameAuthenticationReviewResultRequest buildShowRealnameAuthenticationReviewResultRequest() {
        ShowRealnameAuthenticationReviewResultRequest request = new ShowRealnameAuthenticationReviewResultRequest();
        request.setCustomerId("<YOUR CUSTOMER_ID>");
        return request;
    }

}